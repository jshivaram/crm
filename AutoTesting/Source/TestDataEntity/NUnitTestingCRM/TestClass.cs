﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using TestDataEntity;
using TestDataEntity.Executors;
using TestDataEntity.Models;

namespace NUnitTestingCRM
{
    [TestFixture]
    public class TestClass
    {
        [Test]
        public void TestMethod()
        {
            List<RunnerModel> tests = TestRunner.GetJsonList();

            TestRunner.RunAllTests(tests);
        }
    }
}
