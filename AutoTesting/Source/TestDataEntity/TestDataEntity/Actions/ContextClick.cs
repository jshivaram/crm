﻿using OpenQA.Selenium;
using System;
using System.Threading;

namespace TestDataEntity.Actions
{
    public class ContextClick
    {
        public static void RightClick(IWebDriver driver, IWebElement record)
        {
            OpenQA.Selenium.Interactions.Actions rclick = new OpenQA.Selenium.Interactions.Actions(driver);

            rclick.ContextClick(record).Build().Perform();
            Thread.Sleep(1000);
            rclick.SendKeys(OpenQA.Selenium.Keys.Down).Build().Perform();
            Thread.Sleep(1000);
            rclick.SendKeys(OpenQA.Selenium.Keys.Enter).Build().Perform();
            Thread.Sleep(1000);
        }
    }
} 
