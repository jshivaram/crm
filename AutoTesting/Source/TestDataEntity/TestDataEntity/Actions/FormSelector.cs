﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace TestDataEntity.Actions
{
    class FormSelector
    {
        public static void SelectForm(IWebDriver driver, WebDriverWait waitDriver, int count, string formSelector, string menuItemSelector)
        {   
            try
            {// #formselectorcontainer .ms-crm-FormSelector
                IWebElement correctFrame = waitDriver.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(formSelector)));
                correctFrame.Click();
                Thread.Sleep(2000);
                // ul.ms-crm-FS-Menu li a.ms-crm-FS-MenuItem-Anchor[title=" + CorrectForm + "] nobr                
                IWebElement eCheck = waitDriver.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(menuItemSelector)));
                eCheck.Click();

                return;
            }
            catch (Exception)
            {
                if (count > 0)
                {
                    SelectForm(driver, waitDriver, --count, formSelector, menuItemSelector);
                }
                else
                {
                    throw new Exception("No such selected form");
                }
            }
        }
    }
}
