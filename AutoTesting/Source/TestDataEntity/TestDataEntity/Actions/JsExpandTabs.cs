﻿using OpenQA.Selenium;
using System;
using System.Threading;


namespace TestDataEntity.Actions
{
    public class JsExpandTabs
    {        
        public static void ExpandTabs(IWebDriver driver, string[] tabs)
        {
            Thread.Sleep(3000);
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            string result = "";
            foreach (string tab in tabs)
            {
                result += "$(\"[id='" + tab + "']\").css('display', '');";
            }

            js.ExecuteScript(result);
        }

        public static void CheckJQuery(IWebDriver driver, string tab, int count, TimeSpan time)
        {
            int milliseconds = Convert.ToInt32(time.TotalMilliseconds);
            Thread.Sleep(milliseconds);

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            try
            {
                string javaScript = "$(\"[id='" + tab + "']\");";
                js.ExecuteScript(javaScript);

                return;
            }
            catch(InvalidOperationException)
            {
                if (count > 0)
                {
                    CheckJQuery(driver, tab, --count, time);
                }
                else
                {
                    throw new InvalidOperationException("jQuery is not exist");
                }
            }
        }
    }
}
