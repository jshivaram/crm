﻿using AventStack.ExtentReports;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TestDataEntity.Models;

namespace TestDataEntity.Executors
{
    public class CorrectFormatExecutor
    {
        public static List<string> CheckFieldsFormat(WebDriverWait waitDriver, List<CorrectFormatModel> fieldsCorrectFormat, ref bool isSuccessful, string resultFileName)
        {
            List<string> result = new List<string>();
            Console.WriteLine("\nChecking fields format...");
            ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Fields foramt", "Checking fields format");

            foreach (CorrectFormatModel fieldCorrectFormat in fieldsCorrectFormat)
            {
                try
                {
                    IWebElement field = waitDriver.Until(
                        ExpectedConditions.ElementIsVisible(By.CssSelector(fieldCorrectFormat.FieldSelector))
                    );
                

                    Match match = Regex.Match(field.Text, fieldCorrectFormat.Format);
                    string message = "";
                    if (match.Success)
                    {
                        message = "Successful, field " + fieldCorrectFormat.FieldSelector + " = " + field.Text
                            + " has correct format - " + fieldCorrectFormat.Format;
                        ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "Fields foramt", "Field " + fieldCorrectFormat.FieldSelector + " : " + field.Text
                            + " has correct format - " + fieldCorrectFormat.Format);
                    }
                    else
                    {
                        message = "********* UNSUCCESSFUL, field " + fieldCorrectFormat.FieldSelector + " = " + field.Text
                            + " has INCORRECT format, expected format - " + fieldCorrectFormat.Format;
                        ExtendReportExecuter.AutoTestWriteLog(Status.Fail, "Fields foramt", "<span style =\"color: red;\"> Field " + fieldCorrectFormat.FieldSelector + " = " + field.Text
                            + " has INCORRECT format, expected format - " + fieldCorrectFormat.Format + "</span>");
                        isSuccessful = false;
                    }

                    Console.WriteLine(message);
                    result.Add(message);
                }
                catch (Exception ex)
                {
                    string message = " *********UNSUCCESSFUL, field " + fieldCorrectFormat.FieldSelector + " does not exist ";
                    Console.WriteLine(message);
                    isSuccessful = false;
                    result.Add(message);
                }
               
            }

            return result;
        }
    }
}
