﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Model;
using AventStack.ExtentReports.Reporter;
using System.Collections.Generic;

namespace TestDataEntity.Executors
{
    public static class ExtendReportExecuter
    {
        private static ExtentReports _autoTestReport;
        private static ExtentTest _test;

        static ExtendReportExecuter()
        {
            //"mongodb://192.168.1.207:27017"
            //"DESKTOP-8UH68Q9", 27017);
            var reporter = new ExtentXReporter("mongodb://localhost:27017"); //var reporter = new ExtentXReporter("mongodb://195.88.73.175:27017");
            _autoTestReport = new ExtentReports();
            _autoTestReport.AttachReporter(reporter);
        }

        /// <summary>
        /// Static method for the test report creation
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        public static void StartReportExecuter(string name, string description = "")
        {
            _test = _autoTestReport.CreateTest(name, description);
        }

        /// <summary>
        /// Static method for the log writing
        /// </summary>
        /// <param name="status"></param>
        /// <param name="stepName"></param>
        /// <param name="message"></param>
        public static void AutoTestWriteLog(Status status, string stepName, string message)
        {
            _test.AssignCategory(stepName);
            _test.Log(status, message);
        }

        //public static void EndReportExecuter()
        //{
        //    _autoTestReport.EndTest(_test);
        //}

        public static void FinishReport()
        {
            _autoTestReport.Flush();
        }
    }
}
