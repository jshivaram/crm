﻿using AventStack.ExtentReports;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataEntity.Executors
{
    public class TabNameExecutor
    {
        public static List<string> CheckTabs(IWebDriver driver, string[] visibleTabNames, ref bool isSuccessful)
        {
            ReadOnlyCollection<IWebElement> tabs = driver.FindElements(
                By.CssSelector("#crmFormTabContainer .ms-crm-InlineTab-Read"));

            List<IWebElement> tabList = tabs.Where(
                tab => tab.GetAttribute("id").Contains("tab") && tab.GetCssValue("display") == "block"
            ).ToList();

            List<string> tabNames = tabList.Select(
                tab => tab.FindElement(By.CssSelector(".ms-crm-InlineTabHeaderText h2")).Text
            ).ToList();

            string[] copyTabNamesArr = new string[tabNames.Count];
            tabNames.CopyTo(copyTabNamesArr, 0);
            List<string> copyTabNames = copyTabNamesArr.ToList();

            List<string> result = new List<string>();
            Console.WriteLine("\nChecking exist tab names...");
            ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Correct form tabs", "Checking tab names");

            string message = "";

            for (int i = 0; i < visibleTabNames.Count(); i++)
            {
                try
                {
                    if (visibleTabNames[i] == tabNames[i])
                    {
                        // successful message
                        message = "Successful, expected = " + (i + 1) + ". " + visibleTabNames[i]
                            + " ||| actual = " + (i + 1) + ". " + tabNames[i];
                        ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "Correct form tabs", "Tab number: "  + (i + 1) + ". " + tabNames[i]);
                    }
                    else
                    {
                        // unsuccessful message
                        message = "********* UNSUCCESSFUL, expected = " + (i + 1) + ". " + visibleTabNames[i]
                            + " ||| actual = " + (i + 1) + ". " + tabNames[i];
                        ExtendReportExecuter.AutoTestWriteLog(Status.Fail, "Correct form tabs", "<span style =\"color: red;\"> Tab number: " + (i + 1) + ". " + tabNames[i] + "</span>");
                        isSuccessful = false;
                    }
                }
                catch (Exception)
                {
                    // if tabNames are not contains element
                    message = "********* UNSUCCESSFUL, tab " + (i + 1) + ". " + visibleTabNames[i] + " is missing";
                    isSuccessful = false;
                }
                finally
                {
                    Console.WriteLine(message);
                    result.Add(message);
                    copyTabNames.Remove(visibleTabNames[i]);
                }
            }

            if (copyTabNames.Count > 0)
            {
                foreach (string tab in copyTabNames)
                {
                    message = "********* UNSUCCESSFUL, extra tab " + (tabNames.IndexOf(tab) + 1) + ". " + tab;
                    Console.WriteLine(message);
                    result.Add(message);
                    isSuccessful = false;
                }
            }

            return result;
        }
    }
}
