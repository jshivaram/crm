﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TestDataEntity.Actions;
using TestDataEntity.Models;
using TestDataEntity.Tools;
using Newtonsoft.Json;
using System.Drawing.Imaging;
using System.Threading;
using Newtonsoft.Json.Linq;
using AventStack.ExtentReports;
using AutoIt;

namespace TestDataEntity.Executors
{

    public class TestEntityExecutor
    {
        private string _fileName;
        private string _json;
        private bool _isExecute;

        public TestEntityExecutor(string fileName, string json)
        {
            _fileName = fileName;
            _json = json;

            _isExecute = true;
        }

        public void Execute()
        {

            #region Mapping

            ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Mapping", "Mapping is started");

            ConfigModel config = null;
            string resultFileName = _fileName /*+ "_" + DateTime.Now.ToString("MM-dd-yyyy_hh-mm-ss-tt")
                    + "_build_" + _buildNumber*/ + ".txt";
            StartMapping(_json, resultFileName, ref config);

            #endregion

            #region Open browser

            IWebDriver driver = null;
            OpenBrowser(config, resultFileName, ref driver);

            #endregion

            #region Mapping and Execute upload

            if (_isExecute && config.UploadConfig.IsExecute)
            {
                ExtendReportExecuter.AutoTestWriteLog(Status.Info, "File Upload", "Starting file upload on the server");
                StartUpload(driver, config);
            }

            #endregion

            #region Go to entity page

            ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Login", "Login to CRM server");
            WebDriverWait waitDriver = null;
            GoToEntityPage(config, driver, ref waitDriver);

            #endregion

            #region Select needed view

            ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Required View", "Selecting needed view");
            SelectNeededView(driver, config, resultFileName);

            #endregion

            #region Search record

            ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Record search", "Searching for record");
            IWebElement row = null;
            SearchRecord(driver, waitDriver, config, resultFileName, ref row);
            IWebElement record = null;
            CompareSearchResult(row, config, resultFileName, ref record);

            #endregion

            #region Open record in new tab
            ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Opening record", "Opening record in new tab");
            OpenRecordInNewTab(driver, waitDriver, config, record);

            #endregion

            #region Select needed form

            ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Required form", "Selecting needed form");
            SelectNeededForm(driver, config, resultFileName);

            #endregion

            #region Check jQuery and open tabs

            ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Check jQuery", "Checking jQuery");
            TimeSpan iterationTime = TimeSpan.FromSeconds(config.JQConfig.IterationTime);
            CheckJQueryAndOpenTabs(driver, config, resultFileName, iterationTime);

            #endregion

            #region Transitions (go to related entities, one by one)

            ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Record Transition", "Starting trasition between entities within the current record");
            StartTransitions(driver, waitDriver, config, resultFileName, iterationTime);

            #endregion

            #region Compare actual with expected result and check tables

            if (!_isExecute)
            {
                return;
            }

            bool isSuccessful = true;
            ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Field Compare", "Compare actual fields with existing");
            Thread.Sleep(10000);
            Dictionary<string, object> expected = config.Fields;
            List<string> compareResultList = CompareActualWithExpectedResult(expected, waitDriver, ref isSuccessful);
            List<string> checkTablesList = CheckVisibleElements(config.VisibleElements, 
                waitDriver, ref isSuccessful);
            List<string> checkInvisibleFields = CheckinvisibleFields(config.InvisibleFields, driver, waitDriver,
                ref isSuccessful);

            #endregion

            #region Checking existing tab names

            List<string> tabNamesResult = TabNameExecutor.CheckTabs(driver, config.VisibleTabs, ref isSuccessful);

            #endregion

            #region Checking fields format

            List<string> fieldsFormatResult =
                CorrectFormatExecutor.CheckFieldsFormat(waitDriver, config.FieldsCorrectFormat, ref isSuccessful, resultFileName);

            #endregion

            #region Write result to log file

            List<string> resultList = new List<string>();
            resultList.Add("Checking form data...");
            resultList.AddRange(compareResultList);

            resultList.Add("");
            resultList.Add("Checking visible elements...");
            resultList.AddRange(checkTablesList);

            resultList.Add("");
            resultList.Add("Checking invisible fields...");
            resultList.AddRange(checkInvisibleFields);

            resultList.Add("");
            resultList.Add("Checking existing tab names...");
            resultList.AddRange(tabNamesResult);

            resultList.Add("");
            resultList.Add("Checking fields format...");
            resultList.AddRange(fieldsFormatResult);

            string[] resultForFile = resultList.ToArray();

            string prefix = "Successful_";
            if (!isSuccessful)
            {
                prefix = "UNSUCCESSFUL_";
            }

            File.WriteAllLines(config.ResultFolder + prefix + resultFileName, resultForFile);

            #endregion

            driver.Quit();
        }

        private void StartMapping(string json, string resultFileName, ref ConfigModel config)
        {
            if (!_isExecute)
            { 
                return;
            }

            try
            {
                config = Mapping.MapJson(_json);
                ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "Mapping", "Mapping is successfully complete");
            }
            catch (Exception ex)
            {
                Console.WriteLine("****** ERROR_Config structure is wrong\n" + ex.Message);
                ExtendReportExecuter.AutoTestWriteLog(Status.Fail, "Mapping", "< span style =\"color: red;\">" + " Mapping is fail </span>");
                _isExecute = false;
                throw new Exception("Config structure is wrong\n" + ex.Message);
            }
        }

        private void OpenBrowser(ConfigModel config, string resultFileName, ref IWebDriver driver)
        {
            if (!_isExecute)
            {
                return;
            }

            try
            {
                driver = new ChromeDriver(config.SeleniumDriverPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("****** ERROR_SeleniumDriverPath is wrong\n" + ex.Message);
                File.WriteAllText(config.ResultFolder + "ERROR_" + resultFileName, "SeleniumDriverPath is wrong\n" + ex.Message);
                _isExecute = false;
            }
        }

        private void GoToEntityPage(ConfigModel config, IWebDriver driver, ref WebDriverWait waitDriver)
        {
            if (!_isExecute)
            {
                return;
            }
            driver.Navigate().GoToUrl(config.Url);

            AutoItX.WinWait("Untitled - Google Chrome", "", 3); 
            AutoItX.WinActivate("Untitled - Google Chrome"); 
            AutoItX.Send(config.Login);
            AutoItX.Send("{TAB}");
            AutoItX.Send(config.Password);
            AutoItX.Send("{Enter}");



            ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "Login", "Login to CRM server is successfully complete");

            waitDriver = new WebDriverWait(driver, TimeSpan.FromSeconds(config.TimeSpan));
            waitDriver.Until(ExpectedConditions.TitleIs(config.PageTitle));
            waitDriver.Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(By.Id(config.Frame)));
        }

        private void SelectNeededView(IWebDriver driver, ConfigModel config, string resultFileName)
        {
            if (!_isExecute)
            {
                return;
            }

            try
            {
                Thread.Sleep(3000);
                WebDriverWait waitDriver = new WebDriverWait(driver, TimeSpan.FromSeconds(config.JQConfig.IterationTime));
                Thread.Sleep(3000);
                FormSelector.SelectForm(driver, waitDriver, config.JQConfig.IterationCount,
                    config.CorrectView.DropDownSelector, config.CorrectView.MenuItemSelector);
                ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "Required View", "Needed view is exist and selected");
            }
            catch (Exception)
            {
                File.WriteAllText(config.ResultFolder + "UNSUCCESSFUL_" + resultFileName, "Wrong Entity View");
                Console.WriteLine("****** UNSUCCESSFUL Wrong Entity View");
                ExtendReportExecuter.AutoTestWriteLog(Status.Fail, "Required View", " < span style =\"color: red;\">" + "Needed View is missing </span>");
                _isExecute = false;
            }
        }

        private void SearchRecord(IWebDriver driver, WebDriverWait waitDriver, ConfigModel config, string resultFileName,
            ref IWebElement row)
        {
            if (!_isExecute)
            {
                return;
            }

            IWebElement search = waitDriver.Until(ExpectedConditions.ElementIsVisible(By.Id("crmGrid_findCriteria")));
            search.SendKeys(config.RecordName);

            IWebElement searchImg = waitDriver.Until(ExpectedConditions.ElementToBeClickable(By.Id("crmGrid_findCriteriaImg")));
            searchImg.Click();
            
            try
            {
                row = driver.FindElement(By.CssSelector(".ms-crm-List-Data .ms-crm-List-Row:nth-child(1)"));
                ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "Record search", "Record is found");
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("****** UNSUCCESSFUL " + config.RecordName + " is not found");
                File.WriteAllText(config.ResultFolder + "UNSUCCESSFUL_" + resultFileName, config.RecordName + " is not found");
                ExtendReportExecuter.AutoTestWriteLog(Status.Fail, "Record search", "< span style =\"color: red;\">" + config.RecordName + " is not found </span>");
                driver.Quit();
                _isExecute = false;
            }
        }

        private void CompareSearchResult(IWebElement row, ConfigModel config, string resultFileName, ref IWebElement record)
        {
            if (!_isExecute)
            {
                return;
            }

            record = row.FindElement(By.CssSelector(config.RowAccSelector));
            string recordName = config.RecordName.Replace("*", "");
            
            if (!record.Text.Contains(recordName))
            {
                Console.WriteLine("****** UNSUCCESSFUL " + config.RecordName + " is not found");
                File.WriteAllText(config.ResultFolder + "UNSUCCESSFUL_" + resultFileName, config.RecordName + " is not found");
                ExtendReportExecuter.AutoTestWriteLog(Status.Fail, "Record search", "< span style =\"color: red;\"> Record does not exist in the system </span>");
                _isExecute = false;
            }
        }

        private void OpenRecordInNewTab(IWebDriver driver, WebDriverWait waitDriver, ConfigModel config, IWebElement record)
        {
            if (!_isExecute)
            {
                return;
            }

            driver.SwitchTo().DefaultContent();
            waitDriver.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("#crmAppMessageBar")));
            waitDriver.Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(By.Id(config.Frame)));

            ContextClick.RightClick(driver, record);

            string newWindow = driver.WindowHandles.Last();
            driver.SwitchTo().Window(newWindow);

            waitDriver.Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(By.Id(config.Frame)));
            ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "Opening record", "Opening record in new tab is successfully complete");

        }

        private void SelectNeededForm(IWebDriver driver, ConfigModel config, string resultFileName)
        {
            if (!_isExecute)
            {
                return;
            }

            if (config.IsSelectedForm)
            {
                try
                {
                    WebDriverWait waitDriver = new WebDriverWait(driver, TimeSpan.FromSeconds(config.JQConfig.IterationTime));
                    Thread.Sleep(80000);
                    FormSelector.SelectForm(driver, waitDriver, config.JQConfig.IterationCount,
                        config.CorrectForm.DropDownSelector, config.CorrectForm.MenuItemSelector);
                    ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "Required Form", "Needed form is exist and selected");
                }
                catch (Exception)
                {
                    File.WriteAllText(config.ResultFolder + "UNSUCCESSFUL_" + resultFileName, "Wrong Entity Form");
                    Console.WriteLine("****** UNSUCCESSFUL Wrong Entity Form");
                    ExtendReportExecuter.AutoTestWriteLog(Status.Fail, "Required Form", "< span style =\"color: red;\"> Needed form is missing </span>");
                    _isExecute = false;
                }
            }
        }

        private void CheckJQueryAndOpenTabs(IWebDriver driver, ConfigModel config, string resultFileName, TimeSpan iterationTime)
        {
            if (!_isExecute)
            {
                return;
            }

            if (config.Tabs.Count() != 0)
            {
                string tab = config.Tabs.First();
                try
                {
                    JsExpandTabs.CheckJQuery(driver, tab, config.JQConfig.IterationCount, iterationTime);
                    JsExpandTabs.ExpandTabs(driver, config.Tabs);
                    ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "Check jQuery", "jQuery is ready for scripts");
                }
                catch (InvalidOperationException)
                {
                    File.WriteAllText(config.ResultFolder + "ERROR_" + resultFileName, "jQuery is not ready for scripts");
                    Console.WriteLine("****** ERROR jQuery is not exist");
                    ExtendReportExecuter.AutoTestWriteLog(Status.Fail, "Check jQuery", "< span style =\"color: red;\"> jQuery check failed </span>");
                    _isExecute = false;

                }
            }
        }

        private void StartTransitions(IWebDriver driver, WebDriverWait waitDriver, ConfigModel config, string resultFileName,
            TimeSpan iterationTime)
        {
            if (!_isExecute)
            {
                return;
            }

            try
            {
                TransitionExecutor.Execute(driver, waitDriver, config.Transitions,
                    config.JQConfig.IterationCount, iterationTime, TimeSpan.FromSeconds(config.TimeSpan));
                ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "Record Transition", "Record trasition between entities is successfully complete");
            }
            catch (Exception ex)
            {

                File.WriteAllText(config.ResultFolder + "UNSUCCESSFUL_" + resultFileName, "Transistion error. " + ex.Message);
                Console.WriteLine("****** UNSUCCESSFUL Transistion error. " + ex.Message);
                ExtendReportExecuter.AutoTestWriteLog(Status.Fail, "Record Transition", "< span style =\"color: red;\"> Record trasition between entities is failed </span>");
                _isExecute = false;
            }
        }

        private List<string> CompareActualWithExpectedResult(Dictionary<string, object> expected,
                     WebDriverWait waitDriver, ref bool isSuccessful)
        {
            Console.WriteLine("\nChecking form data...");

            List<string> result = new List<string>();
            string compareResult = "";

            foreach (KeyValuePair<string, object> item in expected)
            {
                IWebElement field = null;
                try
                {
                    field = waitDriver.Until(ExpectedConditions.ElementIsVisible(By.Id(item.Key)));
                }
                catch (WebDriverTimeoutException)
                {
                    compareResult =  item.Key + " is not found";
                    ExtendReportExecuter.AutoTestWriteLog(Status.Fail,
                        "Field Compare", "<span style=\"color: red;\">" + compareResult + "</span>");

                    isSuccessful = false;

                    Console.WriteLine(compareResult);
                    result.Add(compareResult);

                    continue;
                }

                try
                {
                    string expectedResult;

                    if (item.Value is string)
                    {
                        expectedResult = (string)item.Value;
                        if (field.Text != expectedResult)
                        {
                            ExtendReportExecuter.AutoTestWriteLog(Status.Fail,
                            "Field Compare", "<span style=\"color: red;\">" + "Field" + item.Key + "has wrong expected value:" + "expected = " + item.Value
                            + " ||| actual = " + field.Text + "</span>");
                            throw new Exception("***** UNSUCCESSFUL *****: " + item.Key + ": " + "expected = " + item.Value
                                + " ||| actual = " + field.Text);

                        }
                    }
                    else if (item.Value is JArray)
                    {
                        var jArrValues = (JArray)item.Value;
                        var values = jArrValues.Select(v => v.ToString()).ToList();
                        expectedResult = "[" + string.Join(", ", values) + "]";

                        if (!values.Contains(field.Text))
                        {
                            ExtendReportExecuter.AutoTestWriteLog(Status.Fail,
                            "Field Compare", "<span style=\"color: red;\">" + item.Key + ": " + "expected = " + expectedResult
                                + " ||| actual = " + field.Text + "</span>");
                            throw new Exception("***** UNSUCCESSFUL *****: " + item.Key + ": " + "expected = " + expectedResult
                                + " ||| actual = " + field.Text);
                        }
                    }
                    else
                    {
                        ExtendReportExecuter.AutoTestWriteLog(Status.Fatal, "Field Compare", "Wrong config structure: Value is not string or array string");
                        throw new Exception("***** UNSUCCESSFUL *****: Wrong config structure: Value is not string or array string");
                    }
                    compareResult = "Successful: " + item.Key + ": " + "expected = " + expectedResult
                        + " ||| actual = " + field.Text;
                    ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "Field Compare", "<b>" + "Field:" + "</b>" + item.Key   + "<b>" + "Value:" + "</b> " + field.Text);
                }
                catch (Exception ex)
                {
                    compareResult = ex.Message;

                    isSuccessful = false;
                }
                finally
                {
                    Console.WriteLine(compareResult);
                    result.Add(compareResult);
                }
            }

            return result;
        }

        private List<string> CheckVisibleElements(string[] elements, WebDriverWait waitDriver, ref bool isSuccessful)
        {
            Console.WriteLine("\nChecking visible elements...");
            ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Visible fields", "Checking visible fields");

            List <string> result = new List<string>();
            string visibleResult = "";

            foreach (string element in elements)
            {
                try
                {
                    waitDriver.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(element)));

                    visibleResult = "Successful. Element - " + element + " is visible";
                    ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "Visible fields", "Field - " + element + " is visible");
                }
                catch (WebDriverTimeoutException)
                {
                    visibleResult = "***** UNSUCCESSFUL *****. Element - " + element + " is not visible";
                    ExtendReportExecuter.AutoTestWriteLog(Status.Fail, "< span style =\"color: red;\"> Visible fields", "Field - " + element + " is not visible </span>");

                    isSuccessful = false;
                }
                finally
                {
                    result.Add(visibleResult);
                    Console.WriteLine(visibleResult);
                }
            }

            return result;
        }

        private List<string> CheckinvisibleFields(string[] invisibleFields, IWebDriver driver, WebDriverWait waitDriver, ref bool isSuccessful)
        {
            Console.WriteLine("\nChecking invisible fields...");
            ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Invisible fields", "Checking invisible fields");

            waitDriver = new WebDriverWait(driver, TimeSpan.FromSeconds(5));

            List<string> result = new List<string>();
            string invisibleFieldResult = "";

            foreach (string field in invisibleFields)
            {
                try
                {
                    waitDriver.Until(ExpectedConditions.ElementIsVisible(By.Id(field)));

                    invisibleFieldResult = "***** UNSUCCESSFUL *****. Field: " + field + " is visible";
                    isSuccessful = false;
                    ExtendReportExecuter.AutoTestWriteLog(Status.Fail, "Invisible fields", "< span style =\"color: red;\"> Field: " + field + " is visible </span>");
                }
                catch (WebDriverTimeoutException)
                {
                    invisibleFieldResult = "Successful. Field: " + field + " is not visible";
                    ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "Invisible fields", "Field: " + field + " is not visible");
                }
                finally
                {
                    result.Add(invisibleFieldResult);
                    Console.WriteLine(invisibleFieldResult);
                }
            }

            return result;
        }

        
            private void StartUpload(IWebDriver driver, ConfigModel config)
        {
            UploadModel uploadConfig = null;

            try
            {
                string jsonUpload = File.ReadAllText(config.UploadConfig.Path + ".json");
                Console.WriteLine("Upload Json:\n" + jsonUpload + "\n");

                uploadConfig = JsonConvert.DeserializeObject<UploadModel>(jsonUpload);
                Console.WriteLine("Upload Model:" + uploadConfig + "\n"); 
            }
            catch (Exception ex)
            {
                ExtendReportExecuter.AutoTestWriteLog(Status.Fatal, "File Upload", "< span style =\"color: red;\">  upload config is wrong\n" + ex.Message + "</span>");
                Console.WriteLine("****** ERROR, upload config is wrong\n" + ex.Message);
                File.WriteAllText(config.ResultFolder + "ERROR_" + config.UploadConfig.Path + ".txt", "Upload config is wrong\n" + ex.Message);
                return;
            }

            try
            {
                UploadExecutor.Upload(driver, uploadConfig, config);

                string message = uploadConfig.ProgramName + " program has been successful uploaded";
                Console.WriteLine(message);
                File.WriteAllText(config.ResultFolder + "Successful_" + config.UploadConfig.Path + ".txt", message);
                ExtendReportExecuter.AutoTestWriteLog(Status.Pass, "File Upload", "" + uploadConfig.ProgramName + " program has been successful uploaded");
            }
            catch (Exception ex)
            {

                string message = uploadConfig.ProgramName + " upload program has been failed";
                Console.WriteLine("****** UNSUCCESSFUL, " + message + "\n" + ex.Message);
                File.WriteAllText(config.ResultFolder + "UNSUCCESSFUL_" + config.UploadConfig.Path + ".txt", message + "\n" + ex.Message);
                ExtendReportExecuter.AutoTestWriteLog(Status.Error, "File Upload", "< span style =\"color: red;\">" + uploadConfig.ProgramName + " upload program has been failed" + "</span>");

                Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
                ss.SaveAsFile("UNSUCCESSFUL_" + config.UploadConfig.Path + ".png", ImageFormat.Png);
            }
        }
    }
}
