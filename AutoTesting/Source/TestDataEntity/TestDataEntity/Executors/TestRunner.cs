﻿using AventStack.ExtentReports;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataEntity.Models;

namespace TestDataEntity.Executors
{
    public class TestRunner
    {
        /// <summary>
        /// Static method for taking and returning information from config.json file. Deserialize Json to array.
        /// </summary>
        /// <returns></returns>
        public static string[] GetFilesByConfig()
        {
            string config = File.ReadAllText("config.json");
            string[] fileList = JsonConvert.DeserializeObject<string[]>(config);

            return fileList;
        }

        /// <summary>
        /// Static method for taking aret_upload.json, aret_account.json, aret_financial.json etc. and returning list of 
        /// </summary>
        /// <returns></returns>
        public static List<RunnerModel> GetJsonList()
        {
            string[] files = GetFilesByConfig();
            List<RunnerModel> jsonList = new List<RunnerModel>();
            RunnerModel model = null;
            string json = null;
            string fileName = null;

            foreach (string path in files)
            {
                fileName = GetFileNameByConfig(path);
                json = File.ReadAllText(path);

                model = new RunnerModel
                {
                    FileName = fileName,
                    Json = json
                };
                
                jsonList.Add(model);
            }

            return jsonList;
        }

        /// <summary>
        /// Static method for taking aret_upload, aret_account, aret_financial without extension .json
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static string GetFileNameByConfig(string path)
        {
            string[] result = path.Split('.');
            string filename = result.FirstOrDefault();

            return filename;
        }

        /// <summary>
        /// Static method for the report starting and test execution
        /// </summary>
        /// <param name="tests"></param>
        public static void RunAllTests(List<RunnerModel> tests)
        {
            foreach (RunnerModel test in tests)
            {
                ExtendReportExecuter.StartReportExecuter(test.FileName);
                ExtendReportExecuter.AutoTestWriteLog(Status.Info, "Test starting", "Test is started");

                try
                {
                    TestEntityExecutor executor = new TestEntityExecutor(test.FileName, test.Json);
                    executor.Execute();
                }
                catch(Exception ex)
                {
                    Console.WriteLine("****** " + ex.Message);
                    File.WriteAllText(test.FileName + ".txt", ex.Message);
                    ExtendReportExecuter.AutoTestWriteLog(Status.Error, "Test starting", ex.Message);
                }
            }

            ExtendReportExecuter.FinishReport();
        }
    }
}
