﻿ using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using TestDataEntity.Actions;
using TestDataEntity.Models;

namespace TestDataEntity.Executors
{
    public class TransitionExecutor
    {
        public static void Execute(IWebDriver driver, WebDriverWait waitDriver, List<TransitionModel> transitionParameters,
            int iterationsCount, TimeSpan iterationTime, TimeSpan mainTime)
        {
            foreach (TransitionModel param in transitionParameters)
            {
                TransitionToRelatedEntity(driver, waitDriver, param, iterationsCount, iterationTime, mainTime);
            }
        }

        private static void TransitionToRelatedEntity(IWebDriver driver, WebDriverWait waitDriver, TransitionModel model,
            int iterationsCount, TimeSpan iterationTime, TimeSpan mainTime)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            Thread.Sleep(10000);
            string command = "var loadMsg = document.getElementById('" + model.SubgridId + "').querySelector('#LoadOnDemandMessage');";
            command += "if (loadMsg) { loadMsg.click(); }";

            js.ExecuteScript(command);

            IWebElement record = GetRecord(driver, model, iterationsCount, iterationTime);

            // Open new window and switch to it
            ContextClick.RightClick(driver, record);

            string newWindow = driver.WindowHandles.Last();
            driver.SwitchTo().Window(newWindow);

            waitDriver.Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(By.Id(model.FormFrameId)));

            if (model.IsSelectedForm)
            {
                // Select needed form
                waitDriver = new WebDriverWait(driver, iterationTime);
                FormSelector.SelectForm(driver, waitDriver, iterationsCount,
                    model.CorrectTransistForm.DropDownSelector, model.CorrectTransistForm.MenuItemSelector);
                waitDriver = new WebDriverWait(driver, mainTime);
            }

            driver.SwitchTo().DefaultContent();
            waitDriver.Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(By.Id(model.SubgridFrameId)));

            // Expand tabs
            string tab = model.Tabs.FirstOrDefault();
            JsExpandTabs.CheckJQuery(driver, tab, iterationsCount, iterationTime);
            JsExpandTabs.ExpandTabs(driver, model.Tabs);
        }

        public static IWebElement GetRecord(IWebDriver driver, TransitionModel model, int iterationsCount, TimeSpan iterationTime)
        {
            try
            {
                Thread.Sleep(iterationTime.Milliseconds);
                ReadOnlyCollection<IWebElement> records = driver.FindElements(By.CssSelector("#" + model.SubgridId + " tr"));

                // Find needed record
                IWebElement record = records
                    .Where(r => r.Text != "")
                    .Select(r => r.FindElement(By.CssSelector("td:nth-child(" + model.ColumnNumber + ")")))
                    .FirstOrDefault(r => r.Text.Contains(model.SubgridRecordName));

                if (record == null)
                {
                    throw new NotFoundException("Record is not match to config");
                }

                return record;
            }
            catch (Exception)
            {

                if (iterationsCount > 0)
                {
                    return GetRecord(driver, model, --iterationsCount, iterationTime);
                }
                else
                {
                    throw new WebDriverTimeoutException("Record is not found");
                }
            }
        }
    }
}
