﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using TestDataEntity.Models;
using AutoIt;

namespace TestDataEntity.Executors
{
    public class UploadExecutor
    {
        public static void Upload(IWebDriver driver, UploadModel config, ConfigModel config2)
        {
            Console.WriteLine("\nUpload is starting, steps:");

            Console.WriteLine("Go to uploader page");
            driver.Navigate().GoToUrl(config.Url);

            AutoItX.WinWait("Untitled - Google Chrome", "", 3);
            AutoItX.WinActivate("Untitled - Google Chrome");
            AutoItX.Send(config2.Login);
            AutoItX.Send("{TAB}");
            AutoItX.Send(config2.Password);
            AutoItX.Send("{Enter}");

            Console.WriteLine("Expand drop down list");
            // Expand drop down list
            WebDriverWait waitDriver = new WebDriverWait(driver, TimeSpan.FromSeconds(config.ExpandListTime));
            IWebElement select = waitDriver.Until(ExpectedConditions.ElementToBeClickable(
                By.Id("Combo_TypeofUploadedData-trigger-picker")));
            select.Click();
            // ----------------------------------------------------------------------------

            Console.WriteLine("Select item in drop down list");
            // Select item in drop down list
            string programName = config.ProgramName;
            string RecalculationType = config.RecalculationType;

            waitDriver.Until(ExpectedConditions.ElementIsVisible(
                By.Id("Combo_TypeofUploadedData-bodyEl")
            ));
            By selectorForOptions = By.CssSelector("#boundlist-1012-listEl .x-boundlist-item");
            ReadOnlyCollection<IWebElement> programNameOptions = driver.FindElements(selectorForOptions);

            IWebElement element = programNameOptions.SingleOrDefault(item => item.Text == programName);
            element.Click();

            Console.WriteLine("Expand drop down list for recalculation");
            // Expand drop down list
            IWebElement selectRecalculation = waitDriver.Until(ExpectedConditions.ElementToBeClickable(
                By.Id("Combo_ddsm_DataUploaderESPRecalculation-inputEl")));
            selectRecalculation.Click();
            // ----------------------------------------------------------------------------

            By selectorForOptionsConfigurations = By.CssSelector("#boundlist-1013-listEl .x-boundlist-item");
            ReadOnlyCollection<IWebElement> recalculationTypeOptions = driver.FindElements(selectorForOptionsConfigurations);

            IWebElement RecalculationTypeMenu = recalculationTypeOptions.SingleOrDefault(item => item.Text == RecalculationType);
            RecalculationTypeMenu.Click();

            //{02/15/2017 myroshnyk
            IWebElement uploadElementConfiguration = driver.FindElement(By.Id("uploadBtn-fileInputEl"));
            uploadElementConfiguration.SendKeys(config.FilePath);

            //Click on Create & Save new configuration
            IWebElement saveSettings = waitDriver.Until(ExpectedConditions.ElementToBeClickable(
                  By.CssSelector(config.SaveButton)));
            saveSettings.Click();

            //------------------------- Stop 2/20/17

            CheckDialogWindow(driver, waitDriver, config.DialogWindowConfig.IterationCount, TimeSpan.FromSeconds(config.DialogWindowConfig.IterationTime)); //<<<<==== added

            string childWindow = driver.WindowHandles.Last();
            driver.SwitchTo().Window(childWindow);
            //driver.Manage().Window.Maximize();

            // Expand drop down list
            WebDriverWait waitDriverUpload = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            IWebElement selectUploadProgramMenu = waitDriver.Until(ExpectedConditions.ElementToBeClickable(
                By.Id("Combo_TypeofUploadedData-trigger-picker")));
            selectUploadProgramMenu.Click();

            waitDriver.Until(ExpectedConditions.ElementIsVisible(
                By.Id("Combo_TypeofUploadedData-bodyEl")
            ));
            By selectorUploadProgram = By.CssSelector("#boundlist-1012-listEl .x-boundlist-item");
            ReadOnlyCollection<IWebElement> uploadProgramOptions = driver.FindElements(selectorUploadProgram);

            IWebElement elementProgramName = uploadProgramOptions.SingleOrDefault(item => item.Text == programName); //<<<<<<<<<<<<<<<<<<<<==================
            elementProgramName.Click();
            // ----------------------------------------------------------------------------
            IWebElement uploadElement = driver.FindElement(By.Id("uploadBtn-fileInputEl"));
            uploadElement.SendKeys(config.FilePath);

            Console.WriteLine("Wait until dialog window wasn't show");
                        // Wait until dialog window wasn't show
                        CheckDialogWindow(driver, waitDriver, config.DialogWindowConfig.IterationCount, TimeSpan.FromSeconds(config.DialogWindowConfig.IterationTime));
                        // ----------------------------------------------------------------------------

                        Console.WriteLine("Wait until status is not equal <param>");
                        // Wait until status is not equal <param>
                        string childWindowUploder = driver.WindowHandles.Last();
                        driver.SwitchTo().Window(childWindowUploder);
                        //driver.Manage().Window.Maximize();
                        waitDriver.Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(By.Id(config.UploadFrame))); 

                        string status = config.UploadStatus; 
                        CheckStatus(driver, config.StatusConfig.IterationCount, TimeSpan.FromSeconds(config.StatusConfig.IterationTime), status);

           

            // ----------------------------------------------------------------------------
            Console.WriteLine();
        }

        public static void CheckDialogWindow(IWebDriver driver, WebDriverWait waitDriver, int count, TimeSpan time)
        {
            Thread.Sleep(time.Milliseconds);

            string childWindow = driver.WindowHandles.Last();
            driver.SwitchTo().Window(childWindow);

            try
            {
                IWebElement btn = waitDriver.Until(ExpectedConditions.ElementToBeClickable(
                    By.CssSelector("#alertJs-tdDialogFooter button") 
                ));
                btn.Click();

                return;
            }
            catch (WebDriverTimeoutException)
            {
                if (count > 0)
                {
                    CheckDialogWindow(driver, waitDriver, --count, time);
                }
                else
                {
                    throw new WebDriverTimeoutException();
                }
            }
        }

        public static void CheckStatus(IWebDriver driver, int iterationCount, TimeSpan time, string status)
        {
            int milliseconds = Convert.ToInt32(time.TotalMilliseconds);
            Thread.Sleep(milliseconds);
            //WebDriverWait waitDriver = new WebDriverWait(driver, time);

            IWebElement statusElement = null;
            try
            {
                //IWebElement statusElement = waitDriver.Until(ExpectedConditions.ElementIsVisible(By.Id(
                //    "ddsm_statusfiledatauploading")));
                //waitDriver.Until(ExpectedConditions.TextToBePresentInElement(statusElement, status));

                statusElement = driver.FindElement(By.Id("ddsm_statusfiledatauploading"));
                if (statusElement.Text != status)
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                if (iterationCount > 0)
                {
                    CheckStatus(driver, --iterationCount, time, status);
                }
                else
                {
                    throw new Exception("************** Upload status is wrong, expected - " + status
                        + ", actual - " + statusElement.Text);
                }
            }
        }
    }
}

