﻿using System.Collections.Generic;

namespace TestDataEntity.Models
{
    public class ConfigModel
    {
        public string Login { get; set; }
        public string Password { get; set; }

        public string SeleniumDriverPath { get; set; }
        public string RecordName { get; set; }
        public string Url { get; set; }
        public string Frame { get; set; }
        public string PageTitle { get; set; }
        public string ResultFolder { get; set; }

        public CorrectItemModel CorrectView { get; set; }

        public bool IsSelectedForm { get; set; }
        public CorrectItemModel CorrectForm { get; set; }

        public double TimeSpan { get; set; }
        public IterationModel JQConfig { get; set; }

        public Dictionary<string, object> Fields { get; set; }

        public List<TransitionModel> Transitions { get; set; }
        public string[] VisibleElements { get; set; }
        public string[] Tabs { get; set; }
        public string[] VisibleTabs { get; set; }
        public string RowAccSelector { get; set; }

        public string[] InvisibleFields { get; set; }
        public UploadReferenceModel UploadConfig { get; set; }

        public List<CorrectFormatModel> FieldsCorrectFormat { get; set; }
    }
}
