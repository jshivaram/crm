﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataEntity.Models
{
    public class CorrectFormatModel
    {
        public string FieldSelector { get; set; }
        public string Format { get; set; }
    }
}
