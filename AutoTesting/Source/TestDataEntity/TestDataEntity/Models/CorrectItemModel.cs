﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataEntity.Models
{
    public class CorrectItemModel
    {
        public string DropDownSelector { get; set; }
        public string MenuItemSelector { get; set; }
    }
}
