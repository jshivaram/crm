﻿namespace TestDataEntity.Models
{
    public class IterationModel
    {
        public int IterationCount { get; set; }
        public int IterationTime { get; set; }

        public override string ToString()
        {
            string result = "";

            result += "\nIterationCount: " + IterationCount;
            result += "\nIterationTime: " + IterationTime;

            return result;
        }
    }
}
