﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataEntity.Models
{
    public class TransitionModel
    {
        public string SubgridId { get; set; }
        public int ColumnNumber { get; set; }
        public string SubgridRecordName { get; set; }
        public string SubgridFrameId { get; set; }
        public string FormFrameId { get; set; }
        public string[] Tabs { get; set; }
        public CorrectItemModel CorrectTransistForm { get; set; }
        public bool IsSelectedForm { get; set; }
    }
}
