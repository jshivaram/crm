﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataEntity.Models
{
    public class UploadModel
    {
        public string Url { get; set; }
        public int ExpandListTime { get; set; }
        public string ProgramName { get; set; }
        public string FilePath { get; set; }
        public IterationModel DialogWindowConfig { get; set; }
        public IterationModel StatusConfig { get; set; }
        public string UploadFrame { get; set; }
        public string UploadStatus { get; set; }
        public string RecalculationType { get; set; }
        public string SaveButton { get; set; }

        public override string ToString()
        {
            string result = "";

            result += "\nUrl: " + Url;
            result += "\nExpandListTime: " + ExpandListTime;
            result += "\nProgramName: " + ProgramName;
            result += "\nFilePath: " + FilePath;
            result += "\nDialogWindowConfig: " + DialogWindowConfig;
            result += "\nStatusConfig: " + StatusConfig;
            result += "\nUploadFrame: " + UploadFrame;
            result += "\nUploadStatus: " + UploadStatus;

            return result;
        }
    }
}
