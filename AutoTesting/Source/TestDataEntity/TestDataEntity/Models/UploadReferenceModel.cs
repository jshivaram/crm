﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataEntity.Models
{
    public class UploadReferenceModel
    {
        public bool IsExecute { get; set; }
        public string Path { get; set; }
    }
}
