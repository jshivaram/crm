﻿using System;
using System.Collections.Generic;
using System.IO;
using TestDataEntity.Executors;
using TestDataEntity.Models;

namespace TestDataEntity
{
    class Program
    {
        static void Main(string[] args)
        {
            List<RunnerModel> tests = TestRunner.GetJsonList();

            TestRunner.RunAllTests(tests);

            //if (args.Length != 1)
            //{
            //    Console.WriteLine("****** ERROR_" + DateTime.Now.ToString("MM-dd-yyyy_hh-mm-ss-tt")
            //        + ".txt", "Config file name is require");
            //    File.WriteAllText("ERROR_" + DateTime.Now.ToString("MM-dd-yyyy_hh-mm-ss-tt")
            //        + ".txt", "Config file name is require");
            //    return;
            //}

            //string json = "";
            //string fileName = args[0];

            //string resultFileName = "ERROR_" + fileName 
            //    + "_" + DateTime.Now.ToString("MM-dd-yyyy_hh-mm-ss-tt")
            //    + ".txt";

            //try
            //{
            //    json = File.ReadAllText(fileName + ".json");
            //}
            //catch (FileNotFoundException)
            //{
            //    Console.WriteLine("****** Json config is not found (" + fileName + ")");
            //    File.WriteAllText(resultFileName, "Json config is not found (" + fileName + ")");
            //    return;
            //}

            //TestEntityExecutor executor = new TestEntityExecutor(fileName, json);
            //executor.Execute();
        }
    }
}