﻿using System.Collections.Generic;
using Newtonsoft.Json;
using TestDataEntity.Models;

namespace TestDataEntity.Tools
{
    static class Mapping
    {
        public static ConfigModel MapJson(string json)
        {
            ConfigModel result = new ConfigModel();

            Dictionary<string, object> config =
                JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

            string jsonUploadConfig = config["uploadConfig"].ToString();
            result.UploadConfig =
                JsonConvert.DeserializeObject<UploadReferenceModel>(jsonUploadConfig);

            result.Login = config["login"].ToString();
            result.Password = config["password"].ToString();

            result.SeleniumDriverPath = config["seleniumDriverPath"].ToString();
            result.RecordName = config["recordName"].ToString();
            result.Url = config["url"].ToString();
            result.Frame = config["frame"].ToString();
            result.PageTitle = config["pageTitle"].ToString();
            result.TimeSpan = double.Parse(config["timeSpan"].ToString());
            result.RowAccSelector = config["rowAccSelector"].ToString();
            result.ResultFolder = config["resultFolder"].ToString();

            string jsonCorrectView = config["correctView"].ToString();
            result.CorrectView = JsonConvert.DeserializeObject<CorrectItemModel>(jsonCorrectView);

            result.IsSelectedForm = bool.Parse(config["isSelectedForm"].ToString());
            string jsonCorrectForm = config["correctForm"].ToString();
            result.CorrectForm = JsonConvert.DeserializeObject<CorrectItemModel>(jsonCorrectForm);

            //take string, create object list that have type of our model.
            string jsonTransist = config["transist"].ToString();
            List<TransitionModel> transistFields =
                JsonConvert.DeserializeObject<List<TransitionModel>>(jsonTransist);

            result.Transitions = transistFields;

            string jsonJQFields = config["jqFields"].ToString();
            IterationModel jqFields =
                JsonConvert.DeserializeObject<IterationModel>(jsonJQFields);

            result.JQConfig = jqFields;

            string jsonFields = config["fields"].ToString();
            Dictionary<string, object> fields =
                JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonFields);

            result.Fields = fields;

            string jsonVisibleElements = config["visibleElements"].ToString();
            result.VisibleElements = JsonConvert.DeserializeObject<string[]>(jsonVisibleElements);

            string jsonTabs = config["tabs"].ToString();
            result.Tabs = JsonConvert.DeserializeObject<string[]>(jsonTabs);

            string jsonVisibleTabs = config["visibleTabs"].ToString();
            result.VisibleTabs = JsonConvert.DeserializeObject<string[]>(jsonVisibleTabs);

            string jsonInvisibleFields = config["invisibleFields"].ToString();
            result.InvisibleFields = JsonConvert.DeserializeObject<string[]>(jsonInvisibleFields);

            string jsonFieldsCorrectFromat = config["fieldsCorrectFormat"].ToString();
            result.FieldsCorrectFormat = JsonConvert.DeserializeObject<List<CorrectFormatModel>>(jsonFieldsCorrectFromat);

            return result;
        }
    }
}
