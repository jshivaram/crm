﻿using System;
using System.Activities;
using System.Collections.ObjectModel;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;


//namespace DDSMCore
//{
public class CreatePORIndicatorFromPOR : CodeActivity
{
    #region params

    [RequiredArgument]
    [Input("Measure")]
    [ReferenceTarget("ddsm_measure")]
    public InArgument<EntityReference> Measure { get; set; }

    [RequiredArgument]
    [Input("Program offering requirement")]
    [ReferenceTarget("ddsm_programofferingrequirement")]
    public InArgument<EntityReference> ProgramOfferingRequirementParent { get; set; }

    #endregion
    protected override void Execute(CodeActivityContext executionContext)
    {
        //Create the tracing service
        ITracingService tracingService = executionContext.GetExtension<ITracingService>();

        //Create the context
        IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();
        IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

        EntityReference Measure = this.Measure.Get(executionContext);
        EntityReference ProgramOfferingRequirementParent = this.ProgramOfferingRequirementParent.Get(executionContext);
        EntityCollection IndicatorTemplates = GetMeasureIndicatorTemplates(service, Measure);
        for (int i = 0; i < IndicatorTemplates.Entities.Count; i++)
        {
            CreateEntity(service, ProgramOfferingRequirementParent, InitFromRelation(service, IndicatorTemplates[i].ToEntityReference()));
        }

    }
    Entity InitFromRelation(IOrganizationService service, EntityReference ParentMeasure)
    {
        InitializeFromRequest initialize = new InitializeFromRequest();
        initialize.TargetEntityName = "ddsm_pormeasureindicator";
        initialize.EntityMoniker = ParentMeasure;
        InitializeFromResponse initialized = (InitializeFromResponse)service.Execute(initialize);
        return initialized.Entity;
    }
    EntityCollection GetMeasureIndicatorTemplates(IOrganizationService service, EntityReference Measure)
    {
        QueryExpression query = new QueryExpression
        {
            EntityName = "ddsm_measureindicatortemplate",
            ColumnSet = new ColumnSet("ddsm_name", "ddsm_value", "ddsm_measureid", "ddsm_indicatortypeid"),

            Criteria =
                    {
                        FilterOperator = LogicalOperator.Or,
                        Conditions =
                        {
            new ConditionExpression
                {
                    AttributeName = "ddsm_measureid",
                    Operator = ConditionOperator.Equal,
                    Values = {Measure.Id}
                },
                        }
                    }
        };
        return service.RetrieveMultiple(query);
    }
    void CreateEntity(IOrganizationService service, EntityReference ProgramOfferingRequirementParent, Entity indicator)
    {
        indicator["ddsm_programofferingrequirememntid"] = ProgramOfferingRequirementParent;
        var indicatorid = service.Create(indicator);
    }
}
//}
