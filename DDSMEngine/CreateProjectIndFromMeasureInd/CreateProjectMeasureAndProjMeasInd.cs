﻿using System;
using System.Activities;
using System.Collections.ObjectModel;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
//namespace DDSMCore
//{
public class CreateProjectMeasureAndProjMeasInd : CodeActivity
{
    #region

    [RequiredArgument]
    [Input("Project")]
    [ReferenceTarget("ddsm_project")]
    public InArgument<EntityReference> ProjectParental { get; set; }

    [RequiredArgument]
    [Input("Program offering")]
    [ReferenceTarget("ddsm_programoffering")]
    public InArgument<EntityReference> ProgramOffering { get; set; }

    #endregion
    protected override void Execute(CodeActivityContext executionContext)
    {
        //Create the tracing service
        ITracingService tracingService = executionContext.GetExtension<ITracingService>();

        //Create the context
        IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();
        IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

        EntityReference ProjectParental = this.ProjectParental.Get(executionContext);
        EntityReference ProgramOffering = this.ProgramOffering.Get(executionContext);

        EntityCollection ProgramOfferingRequirement = GetProgramOfferingRequirements(service, ProgramOffering);
        for (int i = 0; i < ProgramOfferingRequirement.Entities.Count; i++)
        {
            Guid var = GetCreatedEntity(service, ProjectParental, InitFromRelation(service, ProgramOfferingRequirement[i].ToEntityReference()));
            EntityCollection PORMeasureIndicators = GetPORMeasureIndicators(service, ProgramOfferingRequirement[i].ToEntityReference());
            Entity ProjectMeasure = GetProjectMeasure(service, var);
            for (int j=0;j< PORMeasureIndicators.Entities.Count; j++)
            {
                CreateEntity(service, ProjectMeasure.ToEntityReference(), InitFromRelationProjectMeasureIndicator(service, PORMeasureIndicators[j].ToEntityReference()));
            }
        }

    }

    Entity InitFromRelation(IOrganizationService service, EntityReference ProgramOfferingRequirement)
    {
        InitializeFromRequest initialize = new InitializeFromRequest();
        initialize.TargetEntityName = "ddsm_projectmeasure";
        initialize.EntityMoniker = ProgramOfferingRequirement;
        InitializeFromResponse initialized = (InitializeFromResponse)service.Execute(initialize);
        return initialized.Entity;
    }

    Entity InitFromRelationProjectMeasureIndicator(IOrganizationService service, EntityReference PORMeasureIndicator)
    {
        InitializeFromRequest initialize = new InitializeFromRequest();
        initialize.TargetEntityName = "ddsm_projectmeasureindicator";
        initialize.EntityMoniker = PORMeasureIndicator;
        InitializeFromResponse initialized = (InitializeFromResponse)service.Execute(initialize);
        return initialized.Entity;
    }

    EntityCollection GetProgramOfferingRequirements(IOrganizationService service, EntityReference ProgramOffering)
    {
        QueryExpression query = new QueryExpression
        {
            EntityName = "ddsm_programofferingrequirement",
            ColumnSet = new ColumnSet("ddsm_programofferingid", "ddsm_measureid"),

            Criteria =
                    {
                        FilterOperator = LogicalOperator.Or,
                        Conditions =
                        {
            new ConditionExpression
                {
                    AttributeName = "ddsm_programofferingid",
                    Operator = ConditionOperator.Equal,
                    Values = { ProgramOffering.Id}
                },
                        }
                    }
        };
        return service.RetrieveMultiple(query);
    }
    EntityCollection GetPORMeasureIndicators(IOrganizationService service, EntityReference ProgramOfferingRequirement)
    {
        QueryExpression query = new QueryExpression
        {
            EntityName = "ddsm_pormeasureindicator",
            ColumnSet = new ColumnSet("ddsm_programofferingrequirememntid"),

            Criteria =
                    {
                        FilterOperator = LogicalOperator.Or,
                        Conditions =
                        {
            new ConditionExpression
                {
                    AttributeName = "ddsm_programofferingrequirememntid",
                    Operator = ConditionOperator.Equal,
                    Values = { ProgramOfferingRequirement.Id}
                },
                        }
                    }
        };
        return service.RetrieveMultiple(query);
    }
    Entity GetProjectMeasure(IOrganizationService service, Guid PORMesureIndicatorId)
    {

        ColumnSet attributes = new ColumnSet(new string[] { "ddsm_name", "ddsm_projectmeasureid" });

        Entity ProjectMeasure = service.Retrieve("ddsm_projectmeasure", PORMesureIndicatorId, attributes);
        return ProjectMeasure;
    }
    void CreateEntity(IOrganizationService service, EntityReference ProjectParental, Entity ProjectMeasure)
    {
        ProjectMeasure["ddsm_projectmeasureid"] = ProjectParental;
        var indicatorid = service.Create(ProjectMeasure);
    }
    Guid GetCreatedEntity(IOrganizationService service, EntityReference ProjectParental, Entity ProjectMeasure)
    {
        ProjectMeasure["ddsm_projectid"] = ProjectParental;
        var indicatorid = service.Create(ProjectMeasure);
        return indicatorid;
    }
}
//}
