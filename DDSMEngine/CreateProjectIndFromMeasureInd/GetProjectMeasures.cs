﻿using System;
using System.Activities;
using System.Collections.ObjectModel;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

//namespace DDSMCore
//{
public class GetMeasureForGrid : CodeActivity
{
    #region params
    [RequiredArgument]
    [Input("Project")]
    [ReferenceTarget("ddsm_project")]
    public InArgument<EntityReference> Project { get; set; }

    [RequiredArgument]
    [Output("ProjectMeasureColinJSON")]
    public OutArgument<String> ProjectMeasures { get; set; }
    #endregion

    protected override void Execute(CodeActivityContext executionContext)
    {
        // Create the tracing service
        ITracingService tracingService = executionContext.GetExtension<ITracingService>();
        //Create the context
        IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();
        IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
        EntityReference Project = this.Project.Get(executionContext);
        EntityCollection ProjectMeas = GetProjectMeasures(service, Project);
        var result = new List<Object>();
        /*result.Add(new { ddsm_name= "test name", ddsm_phasename="2"});
        result.Add(new { ddsm_name = "test name2", ddsm_phasename = "255" });*/
        //var bbb = result.Select(x => x.ToList());

        foreach (var record in ProjectMeas.Entities)
        {
            var dd = new Dictionary<String, Object>();
            foreach (var atr in record.Attributes)
            {
                foreach (var fv in record.FormattedValues)
                {
                    if (atr.Key == fv.Key || dd.ContainsKey(atr.Key) ||dd.ContainsKey(fv.Key)) { continue; }
                    else { dd.Add(fv.Key, fv.Value); }
                }
                dd.Add(atr.Key, atr.Value);
            }
            result.Add(dd);
        }
        String json = JsonConvert.SerializeObject(result);
        ProjectMeasures.Set(executionContext, json);
        //serialize ProjectMeas.entities 
        //output string
        //NewtoneSoft JSON 
        //JSON.Net
        //this.ProjectMeasures.Set(executionContext, ProjectMeas);
        //
    }
    EntityCollection GetProjectMeasures(IOrganizationService service, EntityReference project)
    {
        QueryExpression query = new QueryExpression
        {
            EntityName = "ddsm_projectmeasure",
            ColumnSet = new ColumnSet("ddsm_name", "ddsm_phasename", "createdon"),
            Criteria =
                    {
                        FilterOperator = LogicalOperator.Or,
                        Conditions =
                        {
            new ConditionExpression
                {
                    AttributeName = "ddsm_projectid",
                    Operator = ConditionOperator.Equal,
                    Values = {project.Id}
                },
                        }
                    }
        };
        return service.RetrieveMultiple(query);
    }
}
//}
