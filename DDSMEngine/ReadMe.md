**Test js websocket example**

    //connect
    var ws = new WebSocket('wss://dmnengine.ddsm.online:85/Esp');
    //subscribe to all messages for this channel
    ws.onmessage = (ev) => console.log(ev); 
    //send 'qwe' message to post controller
    $.post('/api/test/sendmessage')
    ws.send();

**Socket Channel**
 1. Esp
 2. Dmn