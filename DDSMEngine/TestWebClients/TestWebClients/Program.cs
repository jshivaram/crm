﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace TestWebClients
{
    class Program
    {
        static void Main(string[] args)
        {
            var tester = new Tester();
            tester.Test().Wait();
            Console.ReadKey();
        }
    }

    public class Tester
    {
        public async Task Test()
        {
            var client = new HttpClient();

            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri("http://localhost:54307/api/test/GetValueWithAuth"),
                Method = HttpMethod.Get
            };

            request.Headers.Add("username", "abc");

            var response = await client.SendAsync(request);
            string result = await response.Content.ReadAsStringAsync();
        }
    }

}
