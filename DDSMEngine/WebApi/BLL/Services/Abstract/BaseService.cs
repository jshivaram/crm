﻿using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using System;
using System.Configuration;

namespace BLL.Services.Abstract
{
    //this is the base class for all our services in bll
    //all services are disposable
    //if you want to dispose OrgService you need desspose Service
    //you can dispose Service call Dispose on the instance of this class or
    //you can use: <using(var service = new MyService()){}>

    public abstract class BaseService : IDisposable
    {
        protected readonly IOrganizationService _orgService; //need for all requests to Dynamics CRM
        protected readonly string _connectionStringName; //connection string name in webconfig
        protected readonly string _connectionString;

        public BaseService(IOrganizationService orgService)
        {
            _orgService = orgService;
        }

        public BaseService(string connectionStringName)
        {
            var crmConnection = new CrmConnection(connectionStringName);
            _orgService = new OrganizationService(crmConnection);
            _connectionStringName = connectionStringName;
            _connectionString = ConfigurationManager.ConnectionStrings[_connectionStringName].ToString();
        }

        //call this method when your calculation finished
        protected void OnCalculationEnd()
        {
            //Dispose();
        }

        //This method is necessary for unmanaged resources 
        //such as connecting to a remote data source or reading a file
        public void Dispose()
        {
            //IOrganizationService not realized Dispose method
            OrganizationService orgService = _orgService as OrganizationService;
            //if orgService not null dispose
            orgService?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}