﻿using System;
using System.Threading.Tasks;
using BLL.Services.Abstract;
using BLL.Services.Interfaces;
using CertLibSync.App.Dlc;
using CertLibSync.App.Es;
using CertLibSync.Options;
using Microsoft.Xrm.Sdk;

namespace BLL.Services.Concrete
{
    public class CertLibService : BaseService, ICertLibSync
    {
        //public CertLibService(IOrganizationService orgService) : base(orgService)
        //{
        //}

        public CertLibService(string connectionStringName) : base(connectionStringName)
        {
        }

        public CertLibService() : base(SingletonBaseService.GetInstance()._orgService)
        {
        }

        public Task LoadDlcMetadata(CertLibOptions options)
        {
            var app = new AppLoadDLCMetadata(_orgService, options );
            return app.RunAsync();
        }

        public Task LoadDlcProducts(CertLibOptions options)
        {
          
            var app = new AppLoadDLCProducts(_orgService,options);
            return app.RunAsync();
        }

        public Task LoadEsMetadata(CertLibOptions options)
        {
            var app = new AppLoadEsMetadata(_orgService,options);
            return app.RunAsync();
        }

        public Task LoadEsProducts(CertLibOptions options)
        {
            var app = new AppLoadEsProducts(_orgService, options);
            return app.RunAsync();
        }

        
    }
}
