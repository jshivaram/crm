﻿using BLL.Services.Abstract;
using BLL.Services.Interfaces;
using DataUploader;
using DataUploaderApp.Hellper;
using System.Threading.Tasks;
using System;

namespace BLL.Services.Concrete
{
    public class DataUploaderService : BaseService, IDataUploaderService
    {
        public DataUploaderService(string conn): base(conn)
        {

        }
        //Parser
        public Task RunDataUploaderParserAsync(DataUploaderSettings thisSettings)
        {
            return Task.Run(() => RunDataUploaderParser(thisSettings));
        }

        public void RunDataUploaderParser(DataUploaderSettings thisSettings)
        {
            DataUploaderApp.DataUploaderApp.startParserApp(thisSettings, _orgService);
            OnCalculationEnd();
        }

        public Task RunDataUploaderParserAsync(AppArg jsonInput)
        {
            return Task.Run(() => RunDataUploaderParser(jsonInput));
        }

        public void RunDataUploaderParser(AppArg jsonInput)
        {
            DataUploaderApp.DataUploaderApp.startParserApp(jsonInput, _orgService);
            OnCalculationEnd();
        }

        //Creator
        public void RunDataUploaderCreator(DataUploaderSettings thisSettings)
        {
            DataUploaderApp.DataUploaderApp.startCreatorApp(thisSettings, _orgService);
            OnCalculationEnd();
        }

        public void RunDataUploaderCreator(AppArg jsonInput)
        {
            DataUploaderApp.DataUploaderApp.startCreatorApp(jsonInput, _orgService);
            OnCalculationEnd();
        }

        public Task RunDataUploaderCreatorAsync(DataUploaderSettings thisSettings)
        {
            return Task.Run(() => RunDataUploaderCreator(thisSettings));
        }

        public Task RunDataUploaderCreatorAsync(AppArg jsonInput)
        {
            return Task.Run(() => RunDataUploaderCreator(jsonInput));
        }
    }
}
