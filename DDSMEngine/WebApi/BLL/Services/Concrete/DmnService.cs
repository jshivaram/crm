﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using BLL.Services.Interfaces;
using DmnEngineApp;
using DmnEngineApp.Dto;
using DmnEngineApp.Service;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using RedisServer;
using Serilog;
using SocketClients.Dto;

namespace BLL.Services.Concrete
{
    public class DmnService : IDmnService
    {
        private static int cnt = 0;
        protected IOrganizationService _orgService;

        public string LaunchDmn(InputArgsDto dmnInputArgs)
        {
            var dmnDto = new DmnDto();
            try
            {
                var sw = Stopwatch.StartNew();

                var baseService = SingletonBaseService.GetInstance();
                _orgService = baseService._orgService;
                Log.Debug($"Sync dmnInputArgs= {JsonConvert.SerializeObject(dmnInputArgs)}");

                var dmnEntryPoint = new DmnEntryPoint(dmnInputArgs);
                dmnEntryPoint.LaunchDmn(_orgService);
                dmnDto = dmnEntryPoint.DmnDto;

                Log.Debug($"Sync DMN Calculation is finish.  Execute time:{sw.Elapsed}");
                return dmnDto.ResultDto.Result;
            }
            catch (Exception ex)
            {
                var wsMessageService =
                    new WSMessageService(new WsCreateMsgDto
                    {
                        UserId = dmnDto.InputArgs.UserId,
                        EntityLogicalName = dmnDto.InputArgs.TargetEntityName
                    });

                wsMessageService.SendMessage(WSMessageService.MessageType.ERROR, ex.Message,
                    WSMessageService.CalcMessageStatus.START);
                Log.Error(ex, "RunDmn");
                throw new Exception();
            }
        }

        public Task RunDmnAsync(InputArgsDto dmnInputArgs)
        {
            return Task.Run(() => RunDmn(dmnInputArgs));
        }

        public void RunDmn(InputArgsDto dmnInputArgs)
        {
            try
            {
                //var sw = Stopwatch.StartNew();

                var baseService = SingletonBaseService.GetInstance();
                _orgService = baseService._orgService;
                var request = JsonConvert.SerializeObject(dmnInputArgs);

                Log.Debug($" request= {JsonConvert.SerializeObject(dmnInputArgs)}");

                RedisService.OrgService = _orgService;
                //RedisService.SaveRequest(request, "DMN_BATCH_REQUEST");

                if (dmnInputArgs.IsDataUploader)
                    RedisService.SaveRequest(request, "DMN_BATCH_REQUEST");
                else
                    RedisService.SaveRequest(request, "DMN_REQUEST");

                //var dmnEntryPoint = new DmnEntryPoint(dmnInputArgs);
                //dmnEntryPoint.LaunchDmn(_orgService);

                //Dispose OrgService
                //OnCalculationEnd();

                //Log.Debug($"Async DMN Calculation is finish.  Execute time:{sw.Elapsed}");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "RunDmn");
            }
        }
    }
}