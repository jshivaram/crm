﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using BLL.Services.Abstract;
using BLL.Services.Interfaces;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace BLL.Services.Concrete
{
    public sealed class EToolsService : BaseService, IEToolsService
    {
        private readonly string _pathToAppData = AppDomain.CurrentDomain.BaseDirectory + "App_Data";
        private readonly string _eToolLogicalName = "ddsm_etool";
        private readonly string _attachmentDocumentLogicalName = "ddsm_attachmentdocument";
        private readonly string _annotationLogicalName = "annotation";

        public EToolsService(IOrganizationService orgService) : base(orgService)
        {
        }

        public EToolsService(string connectionStringName) : base(connectionStringName)
        {
        }

        public Task<byte[]> GetFileAsync(string fileName)
        {
            return Task.Run(() => GetFile(fileName));
        }

        public byte[] GetFile(string fileName)
        {
            string filesForDownloadFolderPath = _pathToAppData + "\\" + "FilesForDownload";
            byte[] bytes = File.ReadAllBytes(filesForDownloadFolderPath + "\\" + fileName);
            return bytes;
        }

        public Task SetFileAsync(Guid eToolsId)
        {
            return Task.Run(() => SetFile(eToolsId));
        }

        public void SetFile(Guid eToolsId)
        {
            if (eToolsId == Guid.Empty)
            {
                throw new ArgumentException(nameof(eToolsId));
            }

            var annotationEntities = GetAnnotations(eToolsId);
            SaveFiles(annotationEntities, eToolsId);
        }

        private void SaveFiles(IEnumerable<Entity> annotationEntities, Guid eToolsId)
        {
            if (annotationEntities == null)
            {
                return;
            }

            foreach (var annotationEntity in annotationEntities)
            {
                if (annotationEntity == null)
                {
                    throw new Exception("Cant find annotation");
                }

                string documentBody = annotationEntity.GetAttributeValue<string>("documentbody");

                if (string.IsNullOrEmpty(documentBody))
                {
                    continue;
                }

                string fileName = annotationEntity.GetAttributeValue<string>("filename");

                string eToolfolderPath = $"{_pathToAppData}\\UploadedFiles\\{eToolsId}";

                string dateTime = DateTime.Now.ToString(CultureInfo.InvariantCulture).Replace('/', '-').Replace(' ', ';').Replace(":", "_");
                string dateTimeFolder = $"{eToolfolderPath}\\{dateTime}";

                if (!Directory.Exists(dateTimeFolder))
                {
                    Directory.CreateDirectory(dateTimeFolder);
                }

                string uploadedFilesPath = $"{dateTimeFolder}\\{fileName}";
                byte[] bytes = Convert.FromBase64String(documentBody);

                File.WriteAllBytes(uploadedFilesPath, bytes);
            }
        }

        private IEnumerable<Entity> GetAnnotations(Guid eToolsId)
        {
            var query = new QueryExpression(_annotationLogicalName)
            {
                ColumnSet = new ColumnSet("filename", "subject", "documentbody")
            };

            string attachDocEToolRel = "ddsm_attachdoc_" + _eToolLogicalName;

            var annotationLink = new LinkEntity
            {
                LinkFromEntityName = _annotationLogicalName,
                LinkFromAttributeName = "objectid",
                LinkToEntityName = _attachmentDocumentLogicalName,
                LinkToAttributeName = _attachmentDocumentLogicalName + "id",
                JoinOperator = JoinOperator.Inner
            };

            var attachDocunentLink = new LinkEntity
            {
                LinkFromEntityName = _attachmentDocumentLogicalName,
                LinkFromAttributeName = _attachmentDocumentLogicalName + "id",
                LinkToEntityName = attachDocEToolRel,
                LinkToAttributeName = _attachmentDocumentLogicalName + "id",
                JoinOperator = JoinOperator.Inner
            };

            var attachdocEtoolLink = new LinkEntity
            {
                LinkFromEntityName = attachDocEToolRel,
                LinkFromAttributeName = _eToolLogicalName + "id",
                LinkToEntityName = _eToolLogicalName,
                LinkToAttributeName = _eToolLogicalName + "id",
                JoinOperator = JoinOperator.Inner
            };

            annotationLink.LinkEntities.Add(attachDocunentLink);
            attachDocunentLink.LinkEntities.Add(attachdocEtoolLink);
            query.LinkEntities.Add(annotationLink);

            query.Criteria.AddCondition(_annotationLogicalName, "objecttypecode", ConditionOperator.Equal, _attachmentDocumentLogicalName);
            query.Criteria.AddCondition(_eToolLogicalName, _eToolLogicalName + "id", ConditionOperator.Equal, eToolsId);

            var entities = _orgService.RetrieveMultiple(query).Entities;

            return entities;
        }
    }
}