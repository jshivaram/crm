﻿using System;
using System.Threading.Tasks;
using BLL.Services.Abstract;
using BLL.Services.Interfaces;
using EspCalculation.App;
using EspCalculation.Options;
using Microsoft.Xrm.Sdk;

namespace BLL.Services.Concrete
{
    public class EspService : BaseService, IEspService
    {
        public EspService(string conn) : base(conn)
        {
        }

        public EspService(IOrganizationService service) : base(service)
        {
        }

        public EspService() : base(SingletonBaseService.GetInstance()._orgService)
        {
        }

        public Task CalculateMeasureAsync(CalculateOptions options)
        {
            var app = new AppCalculateMeasure(_orgService, options);
            return app.RunAsync();
        }

        public Task GroupCalculateMeasuresAsync(GroupCalculateOptions groupcalCulateOptions)
        {
            if (groupcalCulateOptions == null)
                throw new ArgumentNullException("groupcalCulateOptions");

            var app = new AppGroupCalculateMeasure(_orgService, groupcalCulateOptions, OnCalculationEnd);
            return app.RunAsync();
        }
    }
}