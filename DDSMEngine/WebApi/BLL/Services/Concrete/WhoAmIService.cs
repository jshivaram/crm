﻿using System;
using Microsoft.Crm.Sdk.Messages;
using Serilog;

namespace BLL.Services.Concrete
{
    public class WhoAmIService
    {
        public Guid GetWhoAmI()
        {
            try
            {
                var baseService = SingletonBaseService.GetInstance();
                var service = baseService._orgService;
                var userid = ((WhoAmIResponse) service.Execute(new WhoAmIRequest())).UserId;
                return userid;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "WhoAmIService");
                return Guid.Empty;
            }
        }

        public string GetOrganizationUrl()
        {
            try
            {
                var baseService = SingletonBaseService.GetInstance();
                return baseService._connectionString;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "WhoAmIService");
                return string.Empty;
            }
        }
    }
}