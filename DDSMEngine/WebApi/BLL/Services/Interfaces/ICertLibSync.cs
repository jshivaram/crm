﻿using System.Threading.Tasks;
using CertLibSync.Options;
using Microsoft.Xrm.Sdk;

namespace BLL.Services.Interfaces
{
    public interface ICertLibSync
    {
        Task LoadDlcMetadata(CertLibOptions options);
        Task LoadDlcProducts(CertLibOptions options);
        Task LoadEsMetadata(CertLibOptions options);
        Task LoadEsProducts(CertLibOptions options);
    }
}
