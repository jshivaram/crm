﻿using DataUploader;
using DataUploaderApp.Hellper;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
    public interface IDataUploaderService
    {
        Task RunDataUploaderParserAsync(DataUploaderSettings thisSettings);
        void RunDataUploaderParser(DataUploaderSettings thisSettings);
        Task RunDataUploaderParserAsync(AppArg jsonInput);
        void RunDataUploaderParser(AppArg jsonInput);

        void RunDataUploaderCreator(DataUploaderSettings thisSettings);
        void RunDataUploaderCreator(AppArg jsonInput);
        Task RunDataUploaderCreatorAsync(DataUploaderSettings thisSettings);
        Task RunDataUploaderCreatorAsync(AppArg jsonInput);
    }
}
