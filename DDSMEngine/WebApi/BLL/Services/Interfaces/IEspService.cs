﻿using DDSM.SmartMeasureCalculationPlugin.Options;
using System.Threading.Tasks;
using EspCalculation.Options;

namespace BLL.Services.Interfaces
{
    public interface IEspService
    {
        Task CalculateMeasureAsync(CalculateOptions json);
        Task GroupCalculateMeasuresAsync(GroupCalculateOptions groupcalCulateOptions);
    }
}
