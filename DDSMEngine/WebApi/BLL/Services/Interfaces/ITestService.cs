﻿using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
    public interface ITestService
    {
        Task CreateRecords();
        string GetAppDomain();
        string GetAppDomain(int number);
        Task TestSocket();
    }
}
