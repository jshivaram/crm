﻿using System;
using System.Collections.Generic;
using System.Net;
using CertLibSync.Models.DLC;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Net.Http;
using System.Net.Http.Headers;
using CertLibSync.App;
using CertLibSync.Models.Es;
using Newtonsoft.Json.Linq;

namespace CertLibSync.Utils
{
    public class DlcApiDriver : IApiDriver
    {
        #region internal fields
        public IApiConfig Config { get; set; }
        private readonly IOrganizationService _service;
        private readonly string[] _columns = new string[] { "ddsm_dlcurl", "ddsm_dlcaccesskey", "ddsm_dlcaccesskeysecret", "ddsm_dlcportion", "ddsm_dlcproductmodel" };
        #endregion
        public DlcApiDriver(IOrganizationService service)
        {
            _service = service;
            GetConfig();
        }

        public void GetConfig()
        {
            var defConfName = "Admin Data";
            try
            {
                var expr = new QueryExpression
                {
                    EntityName = "ddsm_admindata",
                    ColumnSet = new ColumnSet(_columns),
                    Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, defConfName) }
                    }
                };
                var adminData = _service.RetrieveMultiple(expr);
                if (adminData != null && adminData.Entities?.Count >= 1)
                {
                    Config = new DlcConfig(adminData.Entities[0]);
                }
                else
                {
                    throw new Exception("Can't get DLC configuration. Check Admin data record.");
                }
            }
            catch (Exception ex)
            {
                // return null;
                throw new Exception("error in getting Config: " + ex.Message + ex.Data);
            }
        }

        public Dictionary<ICategory, IView> GetMetadata()
        {
            var categories = new Dictionary<ICategory, IView>();

            var config = Config as DlcConfig;

            if (config == null)
            {
                return new Dictionary<ICategory, IView>();
            }

            var dataPortion = config.Portion > 0 ? config.Portion : 100;
            using (var client = new HttpClient())
            {
                //specify to use TLS 1.2 as default connection
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                client.BaseAddress = new Uri(Config.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Access-Key", config.AccessKey);
                client.DefaultRequestHeaders.Add("Access-Key-Secret", config.AccessKeySecret);
                HttpResponseMessage response = client.GetAsync("category/?P:Show=" + dataPortion).Result;
                if (response.IsSuccessStatusCode)
                {
                    var dataTask = response.Content.ReadAsAsync<DlcResponse>();
                    var data = dataTask.Result;
                    //first portion
                    foreach (var cat in data.pageRecords)
                    {
                        categories.Add(cat, new View());
                    }
                    //all pages 
                    var currentPage = data.currentPage;
                    var pageCount = data.totalPages;
                    while (data.currentPage < pageCount)
                    {
                        currentPage++;
                        HttpResponseMessage catPortion =
                            client.GetAsync("category/?P:Show=" + dataPortion + "&P:Current=" + currentPage).Result;
                        if (catPortion.IsSuccessStatusCode)
                        {
                            var dataTask1 = catPortion.Content.ReadAsAsync<DlcResponse>();
                            data = dataTask1.Result;

                            foreach (var cat in data.pageRecords)
                            {
                                categories.Add(cat, new View());
                            }
                        }
                    }
                }
            }

            return categories;
        }

        public Dictionary<Tab, List<JObject>> GetProducts(List<Tab> selectedCategories)
        {
            Dictionary<Tab, List<JObject>> result = new Dictionary<Tab, List<JObject>>();

            foreach (var categorie in selectedCategories)
            {
                List<JObject> catProdData = new List<JObject>();
                var processedPruducts = 0;
                var getProductsResponse = getProductsByCategorie(categorie);
                catProdData.AddRange(getProductsResponse.pageRecords);
                processedPruducts += (int)getProductsResponse.pageRecordsCount;
                while (processedPruducts < getProductsResponse.recordsCount)
                {
                    getProductsResponse = getProductsByCategorie(categorie, getProductsResponse.currentPage + 1);
                    if (getProductsResponse == null) continue;
                    catProdData.AddRange(getProductsResponse.pageRecords);
                    processedPruducts += (int)getProductsResponse.pageRecordsCount;
                }
                result.Add(categorie, catProdData);
            }
            return result;
        }

        private DlcProductResponse getProductsByCategorie(Tab categorie, decimal pageIndex = 0)
        {
            var config = Config as DlcConfig;

            var dataPortion = config.Portion > 0 ? config.Portion : 100;
            DlcProductResponse result;
            using (var client = new HttpClient())
            {
                try
                {
                    //specify to use TLS 1.2 as default connection
                    System.Net.ServicePointManager.SecurityProtocol =
                        SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                    client.BaseAddress = new Uri(Config.Url);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Access-Key", config.AccessKey);
                    client.DefaultRequestHeaders.Add("Access-Key-Secret", config.AccessKeySecret);

                    var url = "product/?propertyIdentifiers=mainCategory.categoryName,primaryUseCategory.categoryName,generalApplicationCategory.categoryName&F:categories.categoryID:EQ="
                        + categorie.MetadataId + "&P:Show=" + dataPortion;

                    if (pageIndex > 0)
                    {
                        url += $"&P:Current={(int)pageIndex}";
                    }


                    HttpResponseMessage response = client.GetAsync(url).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<DlcProductResponse>()?.Result;
                    }
                    else
                    {
                        throw new Exception(response.StatusCode.ToString());
                    }
                }
                catch (Exception e)
                {
                    //  Console.WriteLine(e);
                    throw;
                }
            }
            return result;

        }

    }
}
