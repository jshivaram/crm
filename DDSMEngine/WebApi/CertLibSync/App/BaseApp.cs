﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CertLibSync.Options;
using CertLibSync.Utils;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Client.Services;
using Serilog;

namespace CertLibSync.App
{
    public class BaseApp<T> : IBaseApp<T> where T : IApiDriver
    {
        #region Base props
        internal DateTime _startSync = DateTime.UtcNow;
        public string ConnctionString { get; set; }
        public bool Completed { get; set; }
        public string Result { get; set; }
        public string JSON { get; set; }
        public IOrganizationService OrganizationService { get; set; }
        internal Helper _helper;
        private CertLibOptions _options;
        internal Tracer Logger;
        internal EntityReference _target;
        internal virtual EntityReference DataUploader { get; set; }
        #endregion

        public T ApiDriver { get; set; }

        internal List<string> _processingLog;
        public BaseApp(string connectionSting, string Json)
        {
            Log.Debug("Connction string: " + connectionSting);
            ConnctionString = connectionSting;
            JSON = Json;
            OrganizationService = GetOrgService();
            Init();
        }
        public BaseApp(IOrganizationService orgService)
        {
            OrganizationService = orgService;
            Init();
        }
        public BaseApp(IOrganizationService orgService, CertLibOptions options)
        {
            OrganizationService = orgService;
            _options = options;
            _target = _options.Target;
            Init();
        }

        void Init()
        {
            Log.Debug("In base ctor");
            Logger = new Tracer();
            InitDriver();
        }

        protected virtual void InitDriver()
        {
            throw new NotImplementedException();
        }

        private IOrganizationService GetOrgService()
        {
            CrmConnection connection = CrmConnection.Parse(ConnctionString);
            OrganizationService organisationservice = new OrganizationService(connection);
            return organisationservice;
        }

        public virtual void Run()
        {
            var apiDriver = new DlcApiDriver(OrganizationService);

#if !DEBUG

            Helper.DeleteCategories(_target.Id, OrganizationService);
#endif
            try
            {
                var categories = apiDriver.GetMetadata();
                Log.Debug("count of categories: " + categories.Count);
                ProcessCategories(_target, categories);

                Result = "";
                Completed = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "AppLoadDLCMetadata");
            }
        }

        protected virtual void ProcessCategories(EntityReference certOrg, Dictionary<ICategory, IView> categoriesMetadata)
        {
            throw new NotImplementedException();
        }

        public Task RunAsync()
        {
            return Task.Run(() => Run());
        }
    }
}
