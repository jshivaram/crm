﻿using System;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk.Messages;
using CertLibSync.Models.DLC;
using CertLibSync.Models.Es;
using CertLibSync.Options;
using CertLibSync.Utils;
using Serilog;

namespace CertLibSync.App.Dlc
{
    public class AppLoadDLCMetadata : BaseApp<DlcApiDriver>
    {
        private EntityReference _certOrg;
        readonly DateTime startSync = DateTime.UtcNow;
        public AppLoadDLCMetadata(IOrganizationService orgService, CertLibOptions options) : base(orgService, options)
        {

        }
        public AppLoadDLCMetadata(string connectionSting, string Json) : base(connectionSting, Json)
        {
        }
        public AppLoadDLCMetadata(IOrganizationService orgService, string json) : base(orgService)
        {
        }

        public AppLoadDLCMetadata(IOrganizationService orgService) : base(orgService)
        {
        }

        protected override void InitDriver()
        {
            ApiDriver = new DlcApiDriver(this.OrganizationService);
        }

        protected override void ProcessCategories(EntityReference certOrg, Dictionary<ICategory, IView> categoriesMetadata)
        {
            Log.Debug("in ProcessCategories");

            var config = ApiDriver?.Config as DlcConfig;

            if (config == null)
            {
                Log.Error("ApiDriver.Config is null");
                return;
            }
            
            var emRequest = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };

            emRequest.Requests.Add(new UpdateRequest()
            {
                Target = new Entity(certOrg.LogicalName, certOrg.Id)
                {
                    Attributes = { new KeyValuePair<string, object>("ddsm_lastsyncronization", startSync) }
                }
            });

            var columns1 = from column in config.ProductModel
                           select new Entity("ddsm_certifieditemfield")
                           {
                               Attributes =
                                    {
                                        new KeyValuePair<string, object>("ddsm_logicalname", column.Key),
                                        new KeyValuePair<string, object>("ddsm_name", column.Value.Label),
                                        new KeyValuePair<string, object>("ddsm_datatype", column.Value.Type),
                                    }
                           };
            var columns = columns1.ToList();

            var processed = 0;
            var portion = 50;
            while (processed < categoriesMetadata.Count)
            {
                var portionOfCategories = categoriesMetadata.Skip(processed).Take(portion).ToList();

                foreach (var catBase in portionOfCategories)
                {
                    var cat = catBase.Key as DlcCategory;

                    if (cat == null)
                    {
                        continue;
                    }

                    EntityCollection relatedColumns = new EntityCollection();
                    Relationship rel = new Relationship("ddsm_ddsm_certifiedcategory_ddsm_certifieditem");

                    var category = new Entity("ddsm_certifiedcategory")
                    {
                        Attributes =
                        {
                            new KeyValuePair<string, object>("ddsm_name", cat.categoryName?.Trim()),
                            new KeyValuePair<string, object>("ddsm_category", cat.urlTitle?.Trim()),
                            new KeyValuePair<string, object>("ddsm_metadataid", cat.categoryID?.Trim()),
                            new KeyValuePair<string, object>("ddsm_description", cat.categoryDescription?.Trim()),
                            new KeyValuePair<string, object>("ddsm_certifyingorganizationid", certOrg)
                        }
                    };

                    relatedColumns.Entities.AddRange(columns);
                    category.RelatedEntities.Add(rel, relatedColumns);
                    emRequest.Requests.Add(new CreateRequest() { Target = category });
                }

                if (emRequest.Requests.Count > 0)
                {
                    var result = (ExecuteMultipleResponse)OrganizationService.Execute(emRequest);
                    processed += emRequest.Requests.Count;
                    foreach (ExecuteMultipleResponseItem responseItem in result.Responses)
                    {
                        // An error has occurred.
                        if (responseItem.Fault != null)
                        {
                            var msg = "Error: Name: " + emRequest.Requests[responseItem.RequestIndex].RequestName +
                                      " Index: " + (responseItem.RequestIndex + 1) + " Message: " +
                                      responseItem.Fault.Message;
                            Log.Debug(msg);
                            //   _projectGroup.ObjCommon.TracingService.Trace(msg);
                            //  Result.Set(executionContext, msg);
                        }
                    }
                }
            }
        }

    }
}