﻿using System;
using System.Collections.Generic;
using System.Linq;
using CertLibSync.Models;
using CertLibSync.Models.Es;
using CertLibSync.Options;
using CertLibSync.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;


namespace CertLibSync.App.Dlc
{
    public class AppLoadDLCProducts : BaseApp<DlcApiDriver>
    {
        public string MainEntity = "ddsm_certlibsync";
        private DlcApiDriver apiDriver;

        public AppLoadDLCProducts(string connectionSting, string Json) : base(connectionSting, Json)
        {

        }

        public override void Run()
        {
            try
            {
                List<Tab> tabs;
                var mappingDict = Helper.GetCurrentMapping(_target, OrganizationService, out tabs);
                // get data from API

                var driver = ApiDriver as DlcApiDriver;

                if (driver == null)
                {
                    return;
                }

                var apiData = driver.GetProducts(tabs);
                ProcessData(apiData, mappingDict, tabs);

                Helper.UpdateParentEntity(OrganizationService, _target, _startSync, tabs);
                Completed = true;
            }
            catch (Exception ex)
            {
                Completed = false;
                Log.Error(ex, "AppLoadEsProducts Run");
            }
        }

        public AppLoadDLCProducts(IOrganizationService orgService, CertLibOptions options) : base(orgService, options)
        {

        }

        protected override void InitDriver()
        {
            ApiDriver = new DlcApiDriver(this.OrganizationService);
        }

        private void ProcessData(Dictionary<Tab, List<JObject>> catProducts, Dictionary<Guid, List<MappingField>> mappingDict,List<Tab> selectedCategories)
        {
            foreach (var catData in catProducts)
            {
                var query = catData.Value
                    .Select(x => new UpsertRequest
                    {
                        Target = FillItemEntity(x, _target, mappingDict, selectedCategories, catData.Key)
                    })
                    .ToList();

                //prepare entities for processing
                var portion = 100;
                var processed = 0;

                var multipleRequestRow = new ExecuteMultipleRequest()
                {
                    Settings = new ExecuteMultipleSettings()
                    {
                        ContinueOnError = false,
                        ReturnResponses = true
                    },
                    Requests = new OrganizationRequestCollection()
                };

                while (processed < query.Count)
                {
                    multipleRequestRow.Requests.AddRange(query.Skip(processed).Take(portion));
                    var multipleResponseRow = (ExecuteMultipleResponse)OrganizationService.Execute(multipleRequestRow);
                    if (multipleResponseRow.IsFaulted)
                    {
                        var errorList = new List<string>();
                        foreach (var responseItem in multipleResponseRow.Responses)
                        {
                            // An error has occurred.
                            if (responseItem.Fault == null) continue;
                            var msg = "Error: Name: " + multipleRequestRow.Requests[responseItem.RequestIndex].RequestName +
                                      " Index: " + (responseItem.RequestIndex + 1) + " Message: " +
                                      responseItem.Fault.Message;
                            errorList.Add(msg);
                            //_projectGroup.ObjCommon.TracingService.Trace(msg);
                            // Result.Set(executionContext, msg);
                        }
                        throw new Exception("Creating items failed: " + String.Join(@"\n", errorList));
                    }
                    processed += multipleRequestRow.Requests.Count;
                    multipleRequestRow.Requests.Clear();
                }
            }


        }

        private Entity FillItemEntity(JObject item, EntityReference mainEntityRef,
            Dictionary<Guid, List<MappingField>> mappingDict, List<Tab> tabs, Tab categorie)
        {
            //var catMetadataId = item.Value<string>("type_product");
            List<MappingField> mapping = GetMappingForCurrentCategorie(categorie.MetadataId, mappingDict, tabs);
            //var MainEntity = "ddsm_certlibsync";

            var energystarcertified = new Entity(MainEntity, "ddsm_externaluniqueid2",
                item.SelectToken("productID").ToString())
            {
                Attributes =
                {
                    ["ddsm_certlibsyncid"] = mainEntityRef
                }
            };

            foreach (var prop in item.Properties())
            {
                var propName = prop.Name;

                var propVal = string.Empty;

                if (prop.HasValues)
                {
                    propVal = prop.Value.ToString().Length > 300
                        ? prop.Value.ToString().Substring(0, 300)
                        : prop.Value.ToString().Trim();
                }
                else
                {
                    propVal = string.Empty;
                }
                var mappingData = mapping.FirstOrDefault(
                    x => !string.IsNullOrEmpty(x.FieldName) && x.FieldName.Equals(propName));
                if (mappingData?.Attributes == null || string.IsNullOrWhiteSpace(propVal)) continue;
                foreach (var crmField in mappingData?.Attributes)
                {

                    if (string.IsNullOrEmpty(crmField?.AttrLogicalName)) continue;
                    var crmFieldName = crmField.AttrLogicalName?.ToLower();
                    var attrType = crmField.AttrType;
                    try
                    {
                        switch (attrType)
                        {
                            case "String":
                                energystarcertified[crmFieldName] = propVal;
                                break;
                            case "Decimal":
                                energystarcertified[crmFieldName] = Decimal.Parse(propVal);
                                break;
                            case "Integer":
                                energystarcertified[crmFieldName] = Int32.Parse(propVal);
                                break;
                            case "DateTime":
                                energystarcertified[crmFieldName] = DateTime.Parse(propVal);
                                break;
                        }
                    }
                    catch
                    {
                        var msg =
                            $"Can't convert field type. CRM Field name: {crmFieldName} has type {crmField.AttrType} but API value is '{propVal}'. Please Check the mapping and try again.";
                        throw new Exception(msg);
                    }
                }
            }
            //// var brandName = energystarcertified.Contains("ddsm_brand_name") ? energystarcertified.Attributes["ddsm_brand_name"].ToString() : string.Empty;
            //var modName = energystarcertified.Contains("ddsm_model_name") ? energystarcertified.Attributes["ddsm_model_name"].ToString() : string.Empty;
            ////var fullName = brandName + " - " + modName;

            //energystarcertified.Attributes["ddsm_name"] = modName;
            energystarcertified["ddsm_type_product"] = categorie.MetadataId;
            energystarcertified["ddsm_source"] = categorie.Label;
            energystarcertified["ddsm_jsondata"] = item.ToString(Newtonsoft.Json.Formatting.Indented);
            return energystarcertified;
            // return new Entity();
        }

        private static List<MappingField> GetMappingForCurrentCategorie(string catMetadataId,
            Dictionary<Guid, List<MappingField>> mappingDict, List<Tab> tabs)
        {
            var cat = tabs.FirstOrDefault(x => x.MetadataId.Equals(catMetadataId));
            var catId = cat?.LogicalName ?? Guid.Empty;
            return mappingDict[catId].Where(x => x.Attributes?.Count > 0).ToList();
        }


    }

}