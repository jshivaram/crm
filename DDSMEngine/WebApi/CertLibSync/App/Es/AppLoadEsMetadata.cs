﻿using System.Collections.Generic;
using System.Linq;
using CertLibSync.Models;
using CertLibSync.Models.Es;
using CertLibSync.Options;
using Newtonsoft.Json;
using Microsoft.Xrm.Sdk;
using CertLibSync.Utils;
using Microsoft.Xrm.Sdk.Messages;

namespace CertLibSync.App.Es
{
    public class AppLoadEsMetadata : BaseApp<EsApiDriver>
    {
        public AppLoadEsMetadata(string connectionSting, string Json) : base(connectionSting, Json)
        {
        }
        public AppLoadEsMetadata(IOrganizationService orgService) : base(orgService)
        {
        }
        public AppLoadEsMetadata(IOrganizationService orgService, CertLibOptions options) : base(orgService, options)
        {
        }

        protected override void InitDriver()
        {
            ApiDriver = new EsApiDriver(this.OrganizationService);
        }

        protected override void ProcessCategories(EntityReference certOrg, Dictionary<ICategory, IView> categoriesMetadata) 
        {
            var multipleRequestFields = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };
            var processed = 0;
            var insertPortion = 50;

            multipleRequestFields.Requests.Add(new UpdateRequest() { Target = new Entity(certOrg.LogicalName, certOrg.Id) { Attributes = { new KeyValuePair<string, object>("ddsm_lastsyncronization", _startSync) } } });

            foreach (var catMetadata in categoriesMetadata)
            {
                var key = catMetadata.Key as EsCategory;
                var value = catMetadata.Value as View;

                if (key == null || value == null)
                {
                    continue;
                }

                var newCatRecord = GetCategory(key, certOrg);
                var columns = GetColumns(value);
                Relationship rel = new Relationship("ddsm_ddsm_certifiedcategory_ddsm_certifieditem");
                EntityCollection relatedColumns = new EntityCollection();
                relatedColumns.Entities.AddRange(columns);
                newCatRecord.RelatedEntities.Add(rel, relatedColumns);
                multipleRequestFields.Requests.Add(new CreateRequest() { Target = newCatRecord });
            }

            while (processed < multipleRequestFields.Requests.Count)
            {
                var request = new ExecuteMultipleRequest()
                {
                    Settings = new ExecuteMultipleSettings()
                    {
                        ContinueOnError = true,
                        ReturnResponses = true
                    },
                    Requests = new OrganizationRequestCollection()
                };
                request.Requests.AddRange(multipleRequestFields.Requests.Skip(processed).Take(insertPortion));
                var result = OrganizationService.Execute(request);
                processed += request.Requests.Count;
            }
        }

        private Entity GetCategory(EsCategory cat, EntityReference certOrg)
        {
            return new Entity("ddsm_certifiedcategory")
            {
                Attributes =
                {
                    new KeyValuePair<string, object>("ddsm_name", cat.Name?.Trim()),
                    new KeyValuePair<string, object>("ddsm_category", cat.Category?.Trim()),
                    new KeyValuePair<string, object>("ddsm_metadataid", cat.Id?.Trim()),
                    new KeyValuePair<string, object>("ddsm_description", cat.Description?.Trim()),
                    new KeyValuePair<string, object>("ddsm_certifyingorganizationid",certOrg)

                }
            };
        }

        private IEnumerable<Entity> GetColumns(View metadata)
        {
            var result = from column in metadata.Columns
                         select new Entity("ddsm_certifieditemfield")
                         {
                             Attributes =
                    {
                        new KeyValuePair<string, object>("ddsm_logicalname", column.FieldName?.Trim()),
                        new KeyValuePair<string, object>("ddsm_name", column.Name?.Trim()),
                        new KeyValuePair<string, object>("ddsm_datatype", column.DataTypeName?.Trim()),
                        new KeyValuePair<string, object>("ddsm_metadataid", column.Id?.Trim()),
                    }
                         };
            return result;
        }
    }
}
