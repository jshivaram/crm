﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using CertLibSync.Models;
using Newtonsoft.Json;
using CertLibSync.Models.Es;
using CertLibSync.Options;
using CertLibSync.Utils;
using Serilog;

namespace CertLibSync.App.Es
{
    public partial class AppLoadEsProducts : BaseApp<EsApiDriver>
    {
        #region Const

        public const string MainEntity = "ddsm_certlib";
        public const int LimitMultiRequst = 250;

        #endregion
        #region internal fields 
        private JArray JArrayMetaDataFilter;
        private bool IsKeepUncheckedCategories;

        #endregion


        public AppLoadEsProducts(string connectionSting, string Json) : base(connectionSting, Json)
        {
        }

        public AppLoadEsProducts(IOrganizationService orgService, CertLibOptions options) : base(orgService, options)
        {
        }

        protected override void InitDriver()
        {
            ApiDriver = new EsApiDriver(this.OrganizationService);
        }

        public override void Run()
        {
            try
            {
                List<Tab> tabs;
                var mappingDict = Helper.GetCurrentMapping(_target, OrganizationService, out tabs);
                // get data from API

                var driver = ApiDriver as EsApiDriver;

                if (driver == null)
                {
                    return;
                }

                var apiData = driver.GetProducts(tabs);
                ProcessData(apiData, mappingDict, tabs);

                Helper.UpdateParentEntity(OrganizationService, _target, _startSync, tabs);
                Completed = true;
            }
            catch (Exception ex)
            {
                Completed = false;
                Log.Error(ex, "AppLoadEsProducts Run");
            }
        }

        private void ProcessData(JArray arrCertAllData, Dictionary<Guid, List<MappingField>> mappingDict, List<Tab> tabs)
        {
            #region Create new Items
            var multipleRequestRow = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };

            if (arrCertAllData?.Count > 0)
            {
                var countObjNew = arrCertAllData.Count;
                var jsonDataItems = arrCertAllData.Children<JObject>();
                Log.Debug("Count certified Items for creting in crm: " + countObjNew);

                var allProcessedCount = 0;

                //prepare entities for processing
                List<UpsertRequest> entitiesForProcessing = jsonDataItems.Select(x => new UpsertRequest { Target = FillItemEntity(x, _target, mappingDict, tabs) }).ToList();

                //process entities by portion
                while (allProcessedCount < countObjNew)
                {
                    var range = entitiesForProcessing.Skip(allProcessedCount).Take(LimitMultiRequst).ToList();
                    allProcessedCount += range.Count;
                    multipleRequestRow.Requests.AddRange(range);
                    var multipleResponseRow = (ExecuteMultipleResponse)OrganizationService.Execute(multipleRequestRow);
                    if (multipleResponseRow.IsFaulted)
                    {
                        var errorList = new List<string>();
                        foreach (ExecuteMultipleResponseItem responseItem in multipleResponseRow.Responses)
                        {
                            // An error has occurred.
                            if (responseItem.Fault != null)
                            {
                                var msg = "Error: Name: " + multipleRequestRow.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message;
                                errorList.Add(msg);
                                //_projectGroup.ObjCommon.TracingService.Trace(msg);
                                // Result.Set(executionContext, msg);
                            }
                        }
                        throw new Exception("Creating items failed: " + String.Join(@"\n", errorList));
                    }
                    multipleRequestRow.Requests.Clear();
                }
            }
            #endregion

        }

        private Entity FillItemEntity(JObject item, EntityReference mainEntityRef, Dictionary<Guid, List<MappingField>> mappingDict, List<Tab> tabs)
        {
            var catMetadataId = item.Value<string>("type_product");
            var cat = tabs.FirstOrDefault(x => x.MetadataId.Equals(catMetadataId));
            List<MappingField> mapping = GetMappingForCurrentCategorie(catMetadataId, mappingDict, tabs);

            var energystarcertified = new Entity(MainEntity, "ddsm_externaluniqueid2", item.Value<string>("pd_id") ?? item.Value<string>("id"))
            {
                Attributes =
                        {
                            ["ddsm_certlibsyncid"] = mainEntityRef
                        }
            };

            foreach (var prop in item.Properties())
            {
                var propName = prop.Name;

                var propVal = string.Empty;

                if (prop.HasValues)
                {
                    propVal = prop.Value.ToString().Length > 300 ? prop.Value.ToString().Substring(0, 300) : prop.Value.ToString();
                }
                else
                {
                    propVal = string.Empty;
                }
                var mappingData = mapping.FirstOrDefault(x => !string.IsNullOrEmpty(x.FieldName) && x.FieldName.Equals(propName));
                if (mappingData?.Attributes == null) continue;
                foreach (var crmField in mappingData?.Attributes)
                {
                    if (string.IsNullOrEmpty(crmField?.AttrLogicalName)) continue;
                    var crmFieldName = crmField.AttrLogicalName.ToLower();
                    if (crmField.AttrType.Equals("String"))
                    {
                        energystarcertified[crmFieldName] = propVal;
                    }
                    if (crmField.AttrType.Equals("Decimal"))
                    {
                        energystarcertified[crmFieldName] = Decimal.Parse(propVal);
                    }
                    if (crmField.AttrType.Equals("Integer"))
                    {
                        energystarcertified[crmFieldName] = Int32.Parse(propVal);
                    }
                    if (crmField.AttrType.Equals("DateTime"))
                    {
                        energystarcertified[crmFieldName] = DateTime.Parse(propVal);
                    }
                }
            }
            // var modName = energystarcertified.Contains("ddsm_model_name") ? energystarcertified.Attributes["ddsm_model_name"].ToString() : string.Empty;
            // energystarcertified.Attributes["ddsm_name"] = modName;
            energystarcertified["ddsm_type_product"] = catMetadataId.ToLower();
            energystarcertified["ddsm_source"] = cat?.Label;
            energystarcertified["ddsm_jsondata"] = item.ToString(Formatting.Indented);
            return energystarcertified;
        }

        private List<MappingField> GetMappingForCurrentCategorie(string catMetadataId, Dictionary<Guid, List<MappingField>> mappingDict, List<Tab> tabs)
        {
            var cat = tabs.FirstOrDefault(x => x.MetadataId.Equals(catMetadataId));
            var catId = cat?.LogicalName ?? Guid.Empty;
            return mappingDict[catId].Where(x => x.Attributes?.Count > 0).ToList();
        }

    }
}

