﻿using System.Collections.Generic;
using CertLibSync.Models.DLC;
using CertLibSync.Models.Es;
using Newtonsoft.Json.Linq;

namespace CertLibSync.App
{
    public interface IApiDriver
    {
        void GetConfig();
        Dictionary<ICategory, IView> GetMetadata();
        Dictionary<Tab, List<JObject>> GetProducts(List<Tab> selectedCategories);
        IApiConfig Config { get; set; }
    }
}