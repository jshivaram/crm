﻿using System.Threading.Tasks;

namespace CertLibSync.App
{
    public interface IBaseApp<T> where T : IApiDriver
    {
        string ConnctionString { get; set; }
        bool Completed { get; set; }
        string Result { get; set; }
        string JSON { get; set; }
        void Run();
        Task RunAsync();
        T ApiDriver { get; set; }
    }
}