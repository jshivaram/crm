﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace CertLibSync.App
{
    public class Tracer : ITracingService
    {
        public List<String> Logs { get; set; }

        public Tracer()
        {
            Logs = new List<string>();
        }

        public void Trace(string logMmsg)
        {
            Logs?.Add(logMmsg);
        }

        public void Trace(string format, params object[] args)
        {
            Logs?.Add(string.Format(format, args));
        }
    }
}