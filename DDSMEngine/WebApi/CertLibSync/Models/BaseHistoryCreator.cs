﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CertLibSync.Models.Es;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace CertLibSync.Models
{
    public class BaseHistoryCreator : IHistoryCreator
    {
        public virtual string CreateHistory( IOrganizationService service, EntityReference mainEntityRef,
            DateTime syncDate, IEnumerable<Tab> categoriesList, out int newCount, out int updatedCount, out int deletedCount, List<EntityReference> proseccedRefs)
        {
            throw new NotImplementedException();
        }

        public virtual Helper.History GetNew(IOrganizationService service, DateTime syncDate, EntityReference mainEntityRef, Tab cat, List<EntityReference> proseccedRefs)
        {
            throw new NotImplementedException();
        }

        public enum StateCode
        {
            Active = 0, Inactive = 1
        }

        public List<UpdateRequest> UpdateMNStatus(IEnumerable<Entity> disabledRecords, IOrganizationService service)
        {
            var result = new List<UpdateRequest>();

            foreach (var recordForDisabling in disabledRecords)
            {
                var record = recordForDisabling;
                string entity1 = "ddsm_modelnumber";
                string entity2 = "ddsm_certlib";

                string relationshipEntityName = "ddsm_ddsm_modelnumber_ddsm_certlib";
                var query = new QueryExpression(entity1) { ColumnSet = new ColumnSet(true) };
                var linkEntity1 = new LinkEntity(entity1, relationshipEntityName, "ddsm_modelnumberid", "ddsm_modelnumberid", JoinOperator.Inner);

                var linkEntity2 = new LinkEntity(relationshipEntityName, entity2, "ddsm_certlibid", "ddsm_certlibid", JoinOperator.Inner);

                linkEntity1.LinkEntities.Add(linkEntity2);
                query.LinkEntities.Add(linkEntity1);

                linkEntity2.LinkCriteria = new FilterExpression()
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_certlibid", ConditionOperator.Equal, record.Id),
                        new ConditionExpression("statecode", ConditionOperator.Equal, (int)StateCode.Active)
                    }
                };

                EntityCollection collRecords = service.RetrieveMultiple(query);

                foreach (var mn in collRecords.Entities)
                {
                    var request = new UpdateRequest
                    {
                        Target = new Entity(mn.LogicalName, mn.Id)
                        {
                            Attributes = {
                                new KeyValuePair<string, object>("ddsm_processstatus", new OptionSetValue((int)Helper.ProcessStatus.Disabled)),
                               // new KeyValuePair<string, object>("ddsm_datedelisted",record.GetAttributeValue<DateTime>("ddsm_datedelisted") ),
                            }
                        }
                    };
                   if (record.GetAttributeValue<DateTime>("ddsm_datedelisted") > DateTime.MinValue)
                    {
                        request.Target["ddsm_datedelisted"] = record.GetAttributeValue<DateTime>("ddsm_datedelisted");
                    }
                    result.Add(request);
                }
            }
            return result;
        }

        /// <summary>
        ///Def impl for ES 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="syncDate"></param>
        /// <param name="cat"></param>
        /// <param name="proseccedRefs"></param>
        /// <returns></returns>
        public virtual List<Entity> GetDisabledRecords(IOrganizationService service, DateTime syncDate, Tab cat, List<EntityReference> proseccedRefs)
        {
            int page = 1;
            int recordPerPage = 5000;
            var ids = proseccedRefs.Select(x => x.Id).ToList().Where(x => !x.Equals(Guid.Empty)).ToList();
            List<Entity> all = new List<Entity>();//allResp.Entities;

            if (ids?.Count > 0)
            {
                var query = new QueryExpression
                {
                    EntityName = "ddsm_certlib",
                    ColumnSet = new ColumnSet("createdon", "modifiedon", "ddsm_processstatus", "ddsm_datedelisted"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("ddsm_certlibid",ConditionOperator.In,ids ),
                            new ConditionExpression("ddsm_processstatus",ConditionOperator.NotEqual, (int)Helper.ProcessStatus.Disabled ),
                            new ConditionExpression("ddsm_type_product",ConditionOperator.Equal,cat.MetadataId.ToLower()),
                            new ConditionExpression("statecode", ConditionOperator.Equal, (int)StateCode.Active)
                        }
                    },
                    PageInfo = new PagingInfo
                    {
                        Count = recordPerPage,
                        PageNumber = page
                    }
                };
                // var allResp = service.RetrieveMultiple(query);
                var entityList = new EntityCollection();

                do
                {
                    query.PageInfo.PagingCookie = (page == 1) ? null : entityList.PagingCookie;
                    query.PageInfo.PageNumber = page++;

                    entityList = service.RetrieveMultiple(query);
                    all.AddRange(entityList?.Entities);
                    // Do something with the results here
                }
                while (entityList.MoreRecords);

            }
            return all;
        }

        public List<Entity> GetAllProducByCategory(IOrganizationService service, DateTime syncDate, Tab cat)
        {
            int page = 1;
            int recordPerPage = 5000;
            var query = new QueryExpression
            {
                EntityName = "ddsm_certlib",
                ColumnSet = new ColumnSet("createdon", "modifiedon", "ddsm_processstatus", "ddsm_datedelisted"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("statecode",ConditionOperator.Equal,(int)StateCode.Active),
                        //  new ConditionExpression("ddsm_certlibid",ConditionOperator.In,ids ),
                        //  new ConditionExpression("ddsm_certlibsyncid",ConditionOperator.Equal,mainEntityRef.Id ),
                        new ConditionExpression("ddsm_type_product",ConditionOperator.Equal,cat.MetadataId.ToLower() )
                    }
                },
                PageInfo = new PagingInfo
                {
                    Count = recordPerPage,
                    PageNumber = page
                }
            };
            // var allResp = service.RetrieveMultiple(query);
            var entityList = new EntityCollection();
            List<Entity> all = new List<Entity>();//allResp.Entities;
            do
            {
                query.PageInfo.PagingCookie = (page == 1) ? null : entityList.PagingCookie;
                query.PageInfo.PageNumber = page++;

                entityList = service.RetrieveMultiple(query);
                all.AddRange(entityList?.Entities);
                // Do something with the results here
            }
            while (entityList.MoreRecords);

            return all;

        }


        public void UpdateStatus(IEnumerable<Entity> newRecords, IEnumerable<EntityReference> updatedRecords, IEnumerable<Entity> disabledRecords, IOrganizationService service)
        {
            var listRequests = new List<UpdateRequest>();



            //foreach (var product in newRecords)
            //{
            //    product["ddsm_processstatus"] = new OptionSetValue((int)Helper.ProcessStatus.ToBeReviewed);
            //    listRequests.Add(new UpdateRequest
            //    {
            //        Target = product
            //    });
            //}

            //foreach (var product in updatedRecords)
            //{
            //    //product["ddsm_processstatus"] = new OptionSetValue((int)Helper.ProcessStatus.Active);
            //    listRequests.Add(new UpdateRequest
            //    {
            //        Target = new Entity(product.LogicalName, product.Id)
            //        {
            //            Attributes = {
            //        new KeyValuePair<string, object>("ddsm_processstatus", (int)Helper.ProcessStatus.)
            //    }
            //        }
            //    });
            //}
            //TODO: fix hardcode
            // disabledRecords = new List<EntityReference>() { { new EntityReference("ddsm_certlib", Guid.Parse("{0EB21AFB-3578-E711-80CD-326539636430}")) } };


            foreach (var product in disabledRecords)
            {
                //product[] = new OptionSetValue((int)Helper.ProcessStatus.Disabled);
                listRequests.Add(new UpdateRequest
                {
                    Target = new Entity(product.LogicalName, product.Id)
                    {
                        Attributes = {
                            new KeyValuePair<string, object>("ddsm_processstatus", new OptionSetValue((int)Helper.ProcessStatus.Disabled))
                        }
                    }
                });
            }



            var disableMnRequest = UpdateMNStatus(disabledRecords, service);
            listRequests.AddRange(disableMnRequest);

            var portion = 250;
            var processed = 0;

            while (processed < listRequests.Count)
            {
                var dataPortion = listRequests.Skip(processed).Take(portion);
                processed += dataPortion.Count();
                var requestWithResults = new ExecuteMultipleRequest
                {
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = true,
                        ReturnResponses = true
                    },
                    Requests = new OrganizationRequestCollection()
                };

                requestWithResults.Requests.AddRange(dataPortion);

                var result = (ExecuteMultipleResponse)service.Execute(requestWithResults);
                requestWithResults.Requests.Clear();
            }
        }

    }

}
