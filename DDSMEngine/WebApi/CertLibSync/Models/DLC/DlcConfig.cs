﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;

namespace CertLibSync.Models.DLC
{
    public interface IApiConfig
    {
        Guid Id { get; set; }
        string LogicalName { get; set; }
        string Url { get; set; }
    }

    public class DlcConfig : IApiConfig
    {
        public DlcConfig(Entity data)
        {
            this.Id = data.Id;
            this.LogicalName = data.LogicalName;
            //
            this.Url = data.GetAttributeValue<string>("ddsm_dlcurl");
            if (string.IsNullOrEmpty(Url))
            {
                throw new Exception("FATAL Error: DLC Url was not specified. Check Configuration.");
            }
            AccessKey = data.GetAttributeValue<string>("ddsm_dlcaccesskey");
            AccessKeySecret = data.GetAttributeValue<string>("ddsm_dlcaccesskeysecret");
            Portion = data.GetAttributeValue<int>("ddsm_dlcportion");
            var modelJson = data.GetAttributeValue<string>("ddsm_dlcproductmodel");

            if (string.IsNullOrEmpty(modelJson)) throw new Exception("DLC Product Model is Empty. Check Admin Data record.");

            ProductModel = JsonConvert.DeserializeObject<Dictionary<string, ProductItemFieldMetadata>>(modelJson);
        }

        public Dictionary<string, ProductItemFieldMetadata> ProductModel { get; set; }
        public string LogicalName { get; set; }
        public string Url { get; set; }
        public string AccessKey { get; set; }
        public string AccessKeySecret { get; set; }
        public int Portion { get; set; }
        public Guid Id { get; set; }
    }
}
