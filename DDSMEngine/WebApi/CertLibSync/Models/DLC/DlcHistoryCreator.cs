﻿using System;
using System.Collections.Generic;
using System.Linq;
using CertLibSync.Models.Es;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;

namespace CertLibSync.Models.DLC
{
    public class DlcHistoryCreator : BaseHistoryCreator
    {
        public override List<Entity> GetDisabledRecords(IOrganizationService service, DateTime syncDate, Tab cat, List<EntityReference> proseccedRefs)
        {
            int page = 1;
            int recordPerPage = 5000;
            var ids = proseccedRefs.Select(x => x.Id).ToList().Where(x => !x.Equals(Guid.Empty)).ToList();
            List<Entity> all = new List<Entity>();//allResp.Entities;// var allResp = service.RetrieveMultiple(query);

            if (ids?.Count > 0)
            {
                var query = new QueryExpression
                {
                    EntityName = "ddsm_certlib",
                    ColumnSet = new ColumnSet("createdon", "modifiedon", "ddsm_processstatus", "ddsm_datedelisted"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("statecode",ConditionOperator.Equal,(int)StateCode.Active),
                            new ConditionExpression("ddsm_datedelisted",ConditionOperator.NotNull),
                            new ConditionExpression("ddsm_certlibid",ConditionOperator.In,ids ),
                            new ConditionExpression("ddsm_processstatus",ConditionOperator.NotEqual, (int)Helper.ProcessStatus.Disabled ),
                            new ConditionExpression("ddsm_type_product",ConditionOperator.Equal,cat.MetadataId.ToLower())
                        }
                    },
                    PageInfo = new PagingInfo
                    {
                        Count = recordPerPage,
                        PageNumber = page
                    }
                };

                var entityList = new EntityCollection();
                do
                {
                    query.PageInfo.PagingCookie = (page == 1) ? null : entityList.PagingCookie;
                    query.PageInfo.PageNumber = page++;

                    entityList = service.RetrieveMultiple(query);
                    all.AddRange(entityList?.Entities);
                    // Do something with the results here
                }
                while (entityList.MoreRecords);
            }
            return all;
        }

        public override string CreateHistory(IOrganizationService service, EntityReference mainEntityRef,
            DateTime syncDate, IEnumerable<Tab> categoriesList, out int newCount, out int updatedCount, out int deletedCount, List<EntityReference> proseccedRefs)
        {
            newCount = 0;
            updatedCount = 0;
            deletedCount = 0;

            var result = new
            {
                labels = categoriesList.Select(x => x.Label).ToList(),
                datasets = new List<Column>()
            };

            var newItems = new List<int>();
            var updatesItems = new List<int>();
            var disabledItems = new List<int>();

            var ds = new List<Column>();
            foreach (var cat in categoriesList)
            {
                var historyData = GetNew(service, syncDate, mainEntityRef, cat, proseccedRefs);
                newItems.Add(historyData.New);
                updatesItems.Add(historyData.Updated);
                disabledItems.Add(historyData.Disabled);

                newCount += historyData.New;
                updatedCount += historyData.Updated;
                deletedCount += historyData.Disabled;
            }//TODO: remove label and backgroundColor. 
            ds.Add(new Column { data = newItems, backgroundColor = "#5B97D5", label = "New", name = "New" });
            ds.Add(new Column { data = updatesItems, backgroundColor = "#70AD47", label = "Updated", name = "Updated" });
            ds.Add(new Column { data = disabledItems, backgroundColor = "#A7ADBA", label = "Disabled", name = "Disabled" });
            result.datasets.AddRange(ds);
            var history = JsonConvert.SerializeObject(result);
            return history;
        }

        public override Helper.History GetNew(IOrganizationService service, DateTime syncDate, EntityReference mainEntityRef, Tab cat, List<EntityReference> proseccedRefs)
        {
            int page = 1;
            int recordPerPage = 5000;
            var ids = proseccedRefs.Select(x => x.Id).ToList().Where(x => !x.Equals(Guid.Empty)).ToList();
            var all = new List<Entity>();//allResp.Entities;

            if (ids?.Count > 0)
            {
                var query = new QueryExpression
                {
                    EntityName = "ddsm_certlib",
                    ColumnSet = new ColumnSet("createdon", "modifiedon", "ddsm_processstatus", "ddsm_datedelisted"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("statecode",ConditionOperator.Equal,(int)StateCode.Active),
                            new ConditionExpression("ddsm_certlibid",ConditionOperator.In,ids ),
                            new ConditionExpression("ddsm_certlibsyncid",ConditionOperator.Equal,mainEntityRef.Id ),
                            new ConditionExpression("ddsm_type_product",ConditionOperator.Equal,cat.MetadataId.ToLower() )
                        }
                    },
                    PageInfo = new PagingInfo
                    {
                        Count = recordPerPage,
                        PageNumber = page
                    }
                };
                // var allResp = service.RetrieveMultiple(query);
                var entityList = new EntityCollection();

                do
                {
                    query.PageInfo.PagingCookie = (page == 1) ? null : entityList.PagingCookie;
                    query.PageInfo.PageNumber = page++;

                    entityList = service.RetrieveMultiple(query);
                    all.AddRange(entityList?.Entities);
                    // Do something with the results here
                }
                while (entityList.MoreRecords);
            }

            var newRecords = all.Where(x => x.GetAttributeValue<DateTime>("createdon").Equals(x.GetAttributeValue<DateTime>("modifiedon"))
                                            && x.GetAttributeValue<OptionSetValue>("ddsm_processstatus")?.Value == (int)Helper.ProcessStatus.ToBeReviewed);

            var updatedRecordsRefs = all.Select(x => x.ToEntityReference()).Where(i => !newRecords.Select(x => x.ToEntityReference()).Contains(i)); //proseccedRefs.Where(i => !newRecords.Select(x => x.ToEntityReference()).Contains(i)).ToList();

            var unChangedRecords = updatedRecordsRefs;

            var recordsNotInCurrentSync = GetAllProducByCategory(service, syncDate, cat).Select(x => x.ToEntityReference()).Where(i => !proseccedRefs.Contains(i));

            //from DB
            var disabledRecords = GetDisabledRecords(service, syncDate, cat, recordsNotInCurrentSync.ToList()); //.Select(x => x.ToEntityReference())
            var disabledRecords2 = all.Where(x => x.Attributes.Contains("ddsm_datedelisted") && !x.GetAttributeValue<DateTime>("ddsm_datedelisted").Equals(DateTime.MinValue) && x.GetAttributeValue<OptionSetValue>("ddsm_processstatus")?.Value != (int)Helper.ProcessStatus.Disabled).ToList();

            disabledRecords.AddRange(disabledRecords2);

            UpdateStatus(newRecords, updatedRecordsRefs, disabledRecords, service);

            return new Helper.History()
            {
                New = newRecords.ToList().Count,
                Updated = updatedRecordsRefs.ToList().Count,
                Unchanged = unChangedRecords.ToList().Count,
                Disabled = disabledRecords.ToList().Count
            };
        }
    }
}
