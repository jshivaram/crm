﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace CertLibSync.Models.DLC
{
    public class DlcProductResponse: DlcResponse
    {
        public List<JObject> pageRecords { get; set; }
    }
}
