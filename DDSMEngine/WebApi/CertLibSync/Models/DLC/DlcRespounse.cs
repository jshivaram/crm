﻿using System.Collections.Generic;

namespace CertLibSync.Models.DLC
{
    public class DlcResponse
    {
        public decimal totalPages { get; set; }
        public bool dirtyReadFlag { get; set; }
        public decimal pageRecordsEnd { get; set; }
        public decimal pageRecordsStart { get; set; }
        public decimal recordsCount { get; set; }
        public string collectionObject { get; set; }
        public string collectionConfig { get; set; }
        public List<DlcCategory> pageRecords { get; set; }
        public string collectionName { get; set; }
        public List<object> messages { get; set; }
        public string collectionDescription { get; set; }
        public string collectionID { get; set; }
        public decimal pageRecordsCount { get; set; }
        public decimal pageRecordsShow { get; set; }
        public decimal currentPage { get; set; }
        public string collectionCode { get; set; }
    }
}
