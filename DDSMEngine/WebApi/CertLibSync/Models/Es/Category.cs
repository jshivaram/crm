﻿namespace CertLibSync.Models.Es
{
    public static class EsCategoryActive
    {
        public static string ActiveSpecifications { get { return "Active Specifications"; } }
        public static string UpcomingAPIRevisions { get { return "Upcoming API Revisions"; } }

    }
}
