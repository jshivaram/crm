﻿namespace CertLibSync.Models.Es
{
    public static class EsCategoryProvenance
    {
        public static string Official { get { return "official"; } }
        public static string Community { get { return "community"; } }

    }
}