﻿using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using CertLibSync.Models.DLC;

namespace CertLibSync.Models.Es
{
    public class EsConfig : IApiConfig
    {
        public EsConfig(Entity data)
        {
            object value;
            this.Id = data.Id;
            this.LogicalName = data.LogicalName;
            
            if (data.Attributes.TryGetValue("ddsm_esurl", out value))
            {
                this.Url = (string)value;
            }
            if (data.Attributes.TryGetValue("ddsm_eslogin", out value))
            {
                this.Login = (string)value;
            }
            if (data.Attributes.TryGetValue("ddsm_espass", out value))
            {
                this.Pass = (string)value;
            }
            if (data.Attributes.TryGetValue("ddsm_estoken", out value))
            {
                this.Token = (string)value;
            }
            if (data.Attributes.TryGetValue("ddsm_esuniqcategories", out value))
            {
                this.Categories = JsonConvert.DeserializeObject<List<EsCategory>>((string)value);
            }
            this.OnlyOfficialCategories = data.GetAttributeValue<bool>("ddsm_esonlyofficialcategories");
        }

        public string Url { get; set; }
        public string Login { get; set; }
        public string Pass { get; set; }
        public string Token { get; set; }
        public string LogicalName { get; set; }
        public bool OnlyOfficialCategories { get; set; }

        public Guid Id { get; set; }
        public List<EsCategory> Categories { get; set; }

    }
}
