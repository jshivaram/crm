using System;

namespace CertLibSync.Models.Es
{
    public class Tab2
    {
        public Guid Id { get; set; }
        public string Label { get; set; }
        public string LogicalName { get; set; }
    }
}