﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CertLibSync.Models.Es;
using Microsoft.Xrm.Sdk;

namespace CertLibSync.Models
{
    public interface IHistoryCreator
    {
        string CreateHistory(IOrganizationService service, EntityReference mainEntityRef,
            DateTime syncDate, IEnumerable<Tab> categoriesList, out int newCount, out int updatedCount,
            out int deletedCount, List<EntityReference> proseccedRefs);

        Helper.History GetNew(IOrganizationService service, DateTime syncDate, EntityReference mainEntityRef, Tab cat, List<EntityReference> proseccedRefs);

        void UpdateStatus(IEnumerable<Entity> newRecords, IEnumerable<EntityReference> updatedRecords,
            IEnumerable<Entity> disabledRecords, IOrganizationService service);
    }
}
