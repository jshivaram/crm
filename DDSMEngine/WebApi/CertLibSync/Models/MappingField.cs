﻿using System.Collections.Generic;

namespace CertLibSync.Models
{
    public class MappingField
    {
        public  MappingField()
        {
            Attributes = new List<MappingAttribute>();
        }

        public string FieldName { get; set; }
        public string FieldDisplayName { get; set; }
        public string FieldType { get; set; }
        public List<MappingAttribute> Attributes { get; set; }
    }
}