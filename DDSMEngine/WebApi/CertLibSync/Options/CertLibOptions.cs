﻿using System;
using Microsoft.Xrm.Sdk;

namespace CertLibSync.Options
{
    public class CertLibOptions
    {
        public Guid UserId { get; set; }
        public EntityReference Target { get; set; }
    }
}
