﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Activities;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Messages;
using System.Linq;
using CertLibSync.App.Es;
using CertLibSync.Models;
using CertLibSync.Models.Es;
using Newtonsoft.Json;
using Serilog;

namespace CertLibSync
{
    public class Helper
    {
        public static Dictionary<Guid, List<MappingField>> GetCurrentMapping(EntityReference mainEntityRef, IOrganizationService _service, out List<Tab> Tabs)
        {
            Tabs = new List<Tab>();
            var result = new Dictionary<Guid, List<MappingField>>();
            var query = new QueryExpression
            {
                EntityName = mainEntityRef.LogicalName,

                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = { new ConditionExpression("ddsm_certlibsyncid", ConditionOperator.Equal, mainEntityRef.Id) }
                },
                LinkEntities =
                {
                    new LinkEntity(mainEntityRef.LogicalName,"ddsm_certlibsyncconf","ddsm_certlibsyncconfid","ddsm_certlibsyncconfid", JoinOperator.Inner)
                    {
                        LinkEntities =
                        {
                            new LinkEntity("ddsm_certlibsyncconf","ddsm_mapping", "ddsm_mappingid", "ddsm_mappingid", JoinOperator.Inner)
                            {
                                Columns = new ColumnSet("ddsm_jsondata","ddsm_entities")
                            }
                        }
                    }
                }
            };
            var crmData = _service.RetrieveMultiple(query);
            if (crmData.Entities.Count > 0)
            {
                var admindata = crmData.Entities[0];

                object value;
                if (admindata.Attributes.TryGetValue("ddsm_mapping2.ddsm_jsondata", out value))
                {
                    var json = (string)((AliasedValue)value).Value;
                    result = JsonConvert.DeserializeObject<Dictionary<Guid, List<MappingField>>>(json);
                }
                if (admindata.Attributes.TryGetValue("ddsm_mapping2.ddsm_entities", out value))
                {
                    var json = (string)((AliasedValue)value).Value;
                    Tabs = JsonConvert.DeserializeObject<List<Tab>>(json);
                }

            }
            else
            {
                throw new Exception("Can't receive mapping data.");
            }
            return result;
        }

        public static Entity GetCertOrg(IOrganizationService service, string orgName)
        {
            //  var orgName = ;

            var query = new QueryExpression()
            {
                EntityName = "ddsm_certifyingorganization",
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, orgName) }
                }
            };

            var data = service.RetrieveMultiple(query);
            if (data.Entities.Count > 0)
            {
                return data[0];
            }
            else
            {
                throw new Exception(string.Format("Certified Organization with name {0} doesn't exist.", orgName));
            }

        }

        public static void DeleteCategories(Guid orgId, IOrganizationService service)
        {
            var query = new QueryExpression()
            {
                EntityName = "ddsm_certifiedcategory",
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = { new ConditionExpression("ddsm_certifyingorganizationid", ConditionOperator.Equal, orgId) }
                }
            };

            var data = service.RetrieveMultiple(query);
            if (data.Entities.Count > 0)
            {
                var delRequest = new ExecuteMultipleRequest()
                {
                    Settings = new ExecuteMultipleSettings()
                    {
                        ContinueOnError = false,
                        ReturnResponses = true
                    },
                    Requests = new OrganizationRequestCollection()
                };
                var ddd = from ent in data.Entities
                          select new DeleteRequest() { Target = ent.ToEntityReference() };

                delRequest.Requests.AddRange(ddd);

                service.Execute(delRequest);
            }
        }

        public static EntityReference GetTargetData(CodeActivityContext objDdsm)
        {
            IWorkflowContext context = objDdsm.GetExtension<IWorkflowContext>();

            //objDdsm.TracingService.Trace("in GetTargetData:");
            EntityReference target = new EntityReference();
            if (context.InputParameters.ContainsKey("Target"))
            {
                // objDdsm.Trace("objDdsm.Context.InputParameters.ContainsKey(Target)");
                // objDdsm.TracingService.Trace("in GetTargetData:");
                if (context.InputParameters["Target"] is Entity)
                {
                    target = ((Entity)context.InputParameters["Target"]).ToEntityReference();
                }
                else if (context.InputParameters["Target"] is EntityReference)
                {
                    target = (EntityReference)context.InputParameters["Target"];
                }
            }


            //objDdsm.TracingService.Trace("Target LogicalName: " + target.LogicalName + " Id: " + target.Id.ToString());
            // throw new Exception("FAKE Exception::::: " + string.Join(",", ((MoskTraceService)objDdsm.TracingService).Logs));
            return target;
        }

        public static void UpdateParentEntity(IOrganizationService service, EntityReference mainEntityRef, DateTime begTime, List<Tab> categoriesList)
        {
            //var jaFilter = JArrayMetaDataFilter;
            try
            {
                int NewCount; //count added items
                int UpdatedCount; //count updated items
                int DeletedCount; // count deleted items

                Log.Debug("---------Update1 parent history------------");
                var energyStarUpdate = new Entity(mainEntityRef.LogicalName, mainEntityRef.Id)
                {
                    ["ddsm_historyjson"] = CreateHistory(service, mainEntityRef, begTime, categoriesList, out NewCount, out UpdatedCount, out DeletedCount),
                    ["ddsm_enddate"] = DateTime.UtcNow,
                    ["ddsm_startdate"] = begTime,
                    // Total items
                    ["ddsm_newcount"] = NewCount,
                    ["ddsm_updatecount"] = UpdatedCount,
                    // ["ddsm_unchangedcount"] = UnchangedCount,
                    ["ddsm_deletecount"] = DeletedCount,
                    ["ddsm_status"] = new OptionSetValue((int)EnergyStar.CerLibSyncStatus.Completed),
                };



                //if (IsKeepUncheckedCategories)
                //{
                //    foreach (var objFilter in jaFilter.Children<JObject>())
                //    {
                //        foreach (var propFilter in objFilter.Properties())
                //        {
                //            energyStarUpdate[propFilter.Value.ToString()] = true;
                //        }
                //    }
                //}
                service.Update(energyStarUpdate);
            }
            catch (Exception e)
            {
                Log.Debug($"Update.{Environment.NewLine}{e.Message} ");
            }
        }

        public static string CreateHistory(IOrganizationService service, EntityReference mainEntityRef, DateTime syncDate, IEnumerable<Tab> categoriesList, out int NewCount, out int UpdatedCount, out int DeletedCount)
        {
            NewCount = 0;
            UpdatedCount = 0;
            DeletedCount = 0;

            var result = new
            {
                labels = categoriesList.Select(x => x.Label).ToList(),
                datasets = new List<Column>()
            };

            var newItems = new List<int>();
            var updatesItems = new List<int>();
            var disabledItems = new List<int>();

            var ds = new List<Column>();
            foreach (var cat in categoriesList)
            {
                var historyData = getNew(service, syncDate, mainEntityRef, cat);
                newItems.Add(historyData.New);
                updatesItems.Add(historyData.Updated);
                disabledItems.Add(historyData.Disabled);

                NewCount += historyData.New;
                UpdatedCount += historyData.Updated;
                DeletedCount += historyData.Disabled;
            }
            ds.Add(new Column { data = newItems, backgroundColor = "#5B97D5", label = "New" });
            ds.Add(new Column { data = updatesItems, backgroundColor = "#70AD47", label = "Updated" });
            ds.Add(new Column { data = disabledItems, backgroundColor = "#A7ADBA", label = "Disabled" });
            result.datasets.AddRange(ds);
            var history = JsonConvert.SerializeObject(result);
            return history;
        }

        public struct History
        {
            public int New { get; set; }
            public int Updated { get; set; }
            public int Unchanged { get; set; }
            public int Disabled { get; set; }
        }

        public static History getNew(IOrganizationService service, DateTime syncDate, EntityReference mainEntityRef, Tab cat)
        {
            int page = 1;
            int recordPerPage = 5000;


            var query = new QueryExpression
            {
                EntityName = "ddsm_certlib",
                ColumnSet = new ColumnSet("createdon", "modifiedon"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_certlibsyncid",ConditionOperator.Equal,mainEntityRef.Id ),
                        new ConditionExpression("ddsm_type_product",ConditionOperator.Equal,cat.MetadataId.ToLower() )
                    }
                },
                PageInfo = new PagingInfo
                {
                    Count = recordPerPage,
                    PageNumber = page
                }
            };
            // var allResp = service.RetrieveMultiple(query);
            var entityList = new EntityCollection();
            List<Entity> all = new List<Entity>();//allResp.Entities;
            do
            {
                query.PageInfo.PagingCookie = (page == 1) ? null : entityList.PagingCookie;
                query.PageInfo.PageNumber = page++;

                entityList = service.RetrieveMultiple(query);
                all.AddRange(entityList?.Entities);
                // Do something with the results here
            }
            while (entityList.MoreRecords);

            var updatedRecords = all.Count(x => !x.GetAttributeValue<DateTime>("createdon").Equals(x.GetAttributeValue<DateTime>("modifiedon")) &&
                                                 x.GetAttributeValue<DateTime>("modifiedon") >= syncDate);
            var newRecords = all.Count(x => (x.GetAttributeValue<DateTime>("createdon")).Equals(x.GetAttributeValue<DateTime>("modifiedon")) &&
                                                 x.GetAttributeValue<DateTime>("createdon") >= syncDate);
            var unChangedRecords = updatedRecords;
            var disabledRecords = all.Where(x => !x.GetAttributeValue<DateTime>("createdon").Equals(x.GetAttributeValue<DateTime>("modifiedon")) && x.GetAttributeValue<DateTime>("modifiedon") < syncDate);



            return new History()
            {
                New = newRecords,
                Updated = updatedRecords,
                Unchanged = unChangedRecords,
                Disabled = disabledRecords.Count()
            };
        }
    }
}
