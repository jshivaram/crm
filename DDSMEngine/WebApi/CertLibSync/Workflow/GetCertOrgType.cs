﻿using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;


public class GetCertOrg : CodeActivity
{
    [Output("CertOrg")]
    [ArgumentEntity("ddsm_certifyingorganization")]
    [ReferenceTarget("ddsm_certifyingorganization")]
    public OutArgument<EntityReference> CertOrg { get; set; }
   
    [Input("Cert Lib Sync Conf")]
    [ArgumentEntity("ddsm_certlibsyncconf")]
    [ReferenceTarget("ddsm_certlibsyncconf")]
    public InArgument<EntityReference> CertLibSyncConf { get; set; }

    protected override void Execute(CodeActivityContext executionContext)
    {
        var logger = executionContext.GetExtension<ITracingService>();
        var context = executionContext.GetExtension<IWorkflowContext>();
        var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        var service = serviceFactory.CreateOrganizationService(context.UserId);


        var confRef = CertLibSyncConf.Get(executionContext);
        var conf = service.Retrieve(confRef.LogicalName, confRef.Id, new ColumnSet("ddsm_mappingid"));
        var mappingRef = conf.GetAttributeValue<EntityReference>("ddsm_mappingid");
        var mapping = service.Retrieve(mappingRef.LogicalName, mappingRef.Id, new ColumnSet("ddsm_certifyingorganizationid"));
        var certOrgRef = mapping.GetAttributeValue<EntityReference>("ddsm_certifyingorganizationid");
        var result = service.Retrieve(certOrgRef.LogicalName, certOrgRef.Id, new ColumnSet(true));
        CertOrg.Set(executionContext, result.ToEntityReference());
    }
}

