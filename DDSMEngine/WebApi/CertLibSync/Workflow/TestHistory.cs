﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CertLibSync;
using CertLibSync.Models;
using CertLibSync.Models.Es;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;


public class TestHistory : CodeActivity
{

    [Input("Cert Lib Sync")]
    [ArgumentEntity("ddsm_certlibsync")]
    [ReferenceTarget("ddsm_certlibsync")]
    public InArgument<EntityReference> CertLibSync { get; set; }

    protected override void Execute(CodeActivityContext executionContext)
    {
        var tracer = executionContext.GetExtension<ITracingService>();
        var context = executionContext.GetExtension<IWorkflowContext>();
        var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        var service = serviceFactory.CreateOrganizationService(context.UserId);

        var CommonHelper = new Helper();

        var syncDate = new DateTime(2017, 03, 31);

        EntityReference mainEntityRef = CertLibSync.Get(executionContext);
        var Tabs = new List<Tab>();
        var mappingDict = Helper.GetCurrentMapping(mainEntityRef, service, out Tabs);


        CommonHelper.UpdateParentEntity(tracer, service, mainEntityRef, syncDate, Tabs);

    }
}

