﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreUtils.DataUploader.Dto
{
    public class EntityMapping
    {
        public string Name { get; set; }
        public string Guid { get; set; }
    }
    public class AttrJson
    {
        public string ColumnName { get; set; }
        [JsonProperty("ExcelColumn")]
        public string Name { get; set; }
        public string AttrDisplayName { get; set; }
        public string AttrLogicalName { get; set; }
        public string AttrTargetEntity { get; set; }
        public string AttrType { get; set; }
        public string TypeField { get; set; }

        public string IndicatorId { get; set; }
        public string IEntity { get; set; }
        public string IRelationName { get; set; }
        public string IName { get; set; }
        public string IFieldTargetEntity { get; set; }
        public string IFieldLogicalName { get; set; }
        public string IAdditionalAttr { get; set; }
    }

    public class EntityJson
    {
        [JsonProperty("LogicalName")]
        public string Name { get; set; }
        [JsonProperty("Attributes")]
        public Dictionary<string, AttrJson> Attributes { get; set; }
    }

    public class UploaderSettingsJson
    {
        [JsonProperty("DU_ExcelIsHeader")]
        public bool ExcelIsHeader { get; set; }
        [JsonProperty("DU_RequestsCount")]
        public int GlobalRequestsCount { get; set; }
        [JsonProperty("DU_PageRecordsCount")]
        public int GlobalPageRecordsCount { get; set; }
        [JsonProperty("DU_CallExecuteMultiple")]
        public bool CallExecuteMultiple { get; set; }
        [JsonProperty("DU_DuplicateDetection")]
        public bool DuplicateDetect { get; set; }
        [JsonProperty("DU_DeduplicationRules")]
        public int DedupRules { get; set; }
        [JsonProperty("DU_AllowSimultaneous")]
        public int AllowSimultaneous { get; set; }

        [JsonProperty("DU_ParserRemoteCalculationApiUrl")]
        public string ParserRemoteCalculationApiUrl { get; set; }

        [JsonProperty("DU_CreatorRemoteCalculationApiUrl")]
        public string CreatorRemoteCalculationApiUrl { get; set; }

        [JsonProperty("DU_OrderEntitiesList")]
        public string[] OrderEntitiesList { get; set; }

        [JsonProperty("DU_MappingImplementation")]
        public string[] DU_MappingImplementation { get; set; }
    }

    public class UserSettingsJson
    {
        [JsonProperty("UserId")]
        public string UserId { get; set; }
        [JsonProperty("TransactionCurrencyId")]
        public string TransactionCurrencyId { get; set; }
        [JsonProperty("TimezoneCode")]
        public int TimezoneCode { get; set; }
    }

}
