﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreUtils.Model
{
    public interface IModuleConfig
    {
        string ConfigName { get; }
    }
}
