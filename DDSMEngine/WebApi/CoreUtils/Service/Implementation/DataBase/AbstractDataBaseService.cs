﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using CoreUtils.Service.Interfaces.DataBase;
using DmnEngineApp.Service;
using Microsoft.Xrm.Sdk;

namespace CoreUtils.Service.Implementation.DataBase
{
    /// <summary>
    ///     Class implemented IDataBaseService and provides CRUD operation into data base
    /// </summary>
    public abstract class AbstractDataBaseService : IDataBaseService
    {
        /// <summary>
        ///     Update each object from the set of entitycollection
        /// </summary>
        /// <param name="entities"></param>
        public void UpdateEntities(List<Entity> entities)
        {
            try
            {
                if (IsEntityCollecntionEmpty(entities))
                    return;
                Update(entities);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        ///     Create each object from the set of entitycollection
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="totalEntitiesCount"></param>
        /// <returns>List Ids of created objects  </returns>
        public ConcurrentDictionary<int, Guid> CreateEntities(List<Entity> entities, int totalEntitiesCount)
        {
            try
            {
                if (IsEntityCollecntionEmpty(entities))
                    return new ConcurrentDictionary<int, Guid>();
                return Create(entities,totalEntitiesCount);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        ///     Update the entity
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateEntity(Entity entity)
        {
            try
            {
                if (IsEntityNull(entity))
                    return;

                Update(entity);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        ///     Create entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Object Id</returns>
        public Guid CreateEntity(Entity entity)
        {
            try
            {
                if (IsEntityNull(entity))
                    return Guid.Empty;
                return Create(entity);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public abstract void Update(List<Entity> entities);
        public abstract void Update(Entity entity);
        public abstract Guid Create(Entity entity);
        public abstract ConcurrentDictionary<int, Guid> Create(List<Entity> entities, int totalEntitiesCount);

        private bool IsEntityNull(Entity entity)
        {
            return entity == null;
        }

        private bool IsEntityCollecntionEmpty(List<Entity> entities)
        {
            if (entities == null)
                return true;
            return entities.Count == 0;
        }

        public WSMessageService WsMessageService { get; set; }
    }
}