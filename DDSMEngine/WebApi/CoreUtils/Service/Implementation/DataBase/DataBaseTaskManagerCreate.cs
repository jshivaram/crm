﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CoreUtils.Service.Interfaces.DataBase;
using CoreUtils.Wrap;
using DmnEngineApp.Service;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

namespace CoreUtils.Service.Implementation.DataBase
{
    internal class DataBaseTaskManagerCreate : IDataBaseTaskManagerCreate
    {
        private readonly BatchRequestService _batchRequestService;
        private readonly IDataBaseService _dataBaseService;
        //Provides programmatic access to the metadata and data for an organization.
        private readonly IOrganizationService _organizationService;
        public ConcurrentDictionary<string, int> CountEntities;
        public ConcurrentDictionary<string, List<OneToManyRelationshipMetadata>> OneToManyRelationshipMetadata;


        public DataBaseTaskManagerCreate(IDataBaseService dataBaseService, IOrganizationService organizationService)
        {
            _dataBaseService = dataBaseService;
            CreateResult = new ConcurrentDictionary<int, Guid>();

            _batchRequestService = new BatchRequestService();
            _organizationService = organizationService;
            OneToManyRelationshipMetadata = new ConcurrentDictionary<string, List<OneToManyRelationshipMetadata>>();
            CountEntities = new ConcurrentDictionary<string, int>();
        }

        private ConcurrentDictionary<int, Guid> CreateResult { get; }


        public ConcurrentDictionary<int, Guid> Create(List<Entity> entities, int totalEntitiesCount)
        {
            try
            {
                if (entities.Count == 0) return new ConcurrentDictionary<int, Guid>();

                var totalTime = Stopwatch.StartNew();
                var start = 0;
                var tasks = new List<Task>();
                var outputEntities = new List<Entity>();
                outputEntities.AddRange(entities);
                var outputEntitiesCount = outputEntities.Count;
                var processedEntities = 0;

                var offset = _batchRequestService.DefaultEntitiesIntoStep;

                while (outputEntities.Count != 0)
                {
                    var btachEntities = outputEntities.Skip(start).Take(offset).ToList();
                    var entities1 = processedEntities;
                    var bufTask = Task.Factory.StartNew(() => CreateRequestTask(btachEntities, entities1));
                    tasks.Add(bufTask);
                    //
                    var swProcess = Stopwatch.StartNew();
                    //execute tasks
                    Task.WaitAll(tasks.ToArray(), -1);
                    var currentExecutionTime = swProcess.Elapsed;
                    processedEntities += btachEntities.Count;
                    Log.Debug(
                        $"***Processed Entities:{btachEntities.Count}/{outputEntities.Count}. {processedEntities * 100 / outputEntitiesCount}%.  time:{currentExecutionTime}***");
                    Console.WriteLine(
                        $"***Processed Entities:{btachEntities.Count}/{outputEntities.Count}. {processedEntities * 100 / outputEntitiesCount}%.  time:{currentExecutionTime}***");

                    RemoveDataFromList(outputEntities, offset);

                    UpdateCountEntities(btachEntities);

                    ShowEstimateTime(btachEntities[0].LogicalName, currentExecutionTime.TotalMilliseconds, btachEntities.Count,
                        totalEntitiesCount);

                    offset = _batchRequestService.GetDynamicallyOffset(currentExecutionTime, offset);
                }

                Log.Debug(
                    $"***Total Execution Time:{totalTime.Elapsed}. Processed:{CreateResult.Count} of {outputEntities.Count}***");
                Console.WriteLine(
                    $"***Total Execution Time:{totalTime.Elapsed}. Processed:{CreateResult.Count} of {outputEntities.Count}***");
                return CreateResult;
            }
            catch (Exception e)
            {
                Log.Error(e, "Create");
                Console.WriteLine(e);
                throw;
            }
        }

        private void UpdateCountEntities(IReadOnlyList<Entity> entities)
        {
            if (entities.Count == 0) return;
            var entityLogicalName = entities[0].LogicalName;

            if (CountEntities.Count > 0)
                if (CountEntities.ContainsKey(entityLogicalName))
                {
                    var count = CountEntities[entityLogicalName];
                    CountEntities[entityLogicalName] = count + entities.Count;
                    return;
                }
                else
                {
                    CountEntities.Clear();
                }

            CountEntities.TryAdd(entityLogicalName, entities.Count);
        }

        private void ShowEstimateTime(string entity, double currentExecutionTime, int processedEntities,
            int totalEntitiesCount)
        {
            //var leftTimeInd = (decimal) processedEntities / currentExecutionTime *
            //                  (totalEntitiesCount - totalProcessedEntities);

            var leftTimeInd = (totalEntitiesCount - CountEntities[entity]) / processedEntities *
                              currentExecutionTime;


            var t = TimeSpan.FromMilliseconds(leftTimeInd);
            var time = t.ToString("c");

            var pr = (decimal) CountEntities[entity] * 100 / totalEntitiesCount;

            var msg =
                $"Entity:{entity}. all records={CountEntities[entity]} ({Math.Round(pr, 2)}%) of {totalEntitiesCount}. Estimate Time:{time}";

            Console.WriteLine($"{msg}");

            _dataBaseService.WsMessageService.SendMessage(WSMessageService.MessageType.INFORMATION, msg,
                WSMessageService.CalcMessageStatus.FINISH);
        }

        private void ShowFinishMsg(string msg = "")
        {
            if (!string.IsNullOrEmpty(msg))
                _dataBaseService.WsMessageService.SendMessage(WSMessageService.MessageType.INFORMATION, msg,
                    WSMessageService.CalcMessageStatus.FINISH);
        }


        public OneToManyRelationshipMetadata GetReferencingAttrOneToMany(string targetEntity, string referencingEntity)
        {
            var currentEntityMetadata = new List<OneToManyRelationshipMetadata>();


            var oneToManyRelationshipMetadata = OneToManyRelationshipMetadata;
            if (oneToManyRelationshipMetadata.ContainsKey(targetEntity))
            {
                var tmp = new List<OneToManyRelationshipMetadata>();
                if (oneToManyRelationshipMetadata.TryGetValue(targetEntity, out tmp))
                    currentEntityMetadata = tmp;
            }
            else
            {
                var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
                {
                    EntityFilters = EntityFilters.Relationships,
                    LogicalName = targetEntity
                };

                var retrieveBankEntityResponse =
                    (RetrieveEntityResponse) _organizationService.Execute(retrieveBankAccountEntityRequest);

                currentEntityMetadata = retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships.ToList();

                OneToManyRelationshipMetadata.TryAdd(targetEntity, currentEntityMetadata);
            }
            var rs = currentEntityMetadata.Where(r => r.ReferencingEntity == referencingEntity).ToList();
            return rs.Count == 0 ? null : rs[0];
        }


        private void RemoveDataFromList(List<Entity> outputEntities, int count)
        {
            var listCount = outputEntities.Count;
            if (listCount < count)
                count = listCount;

            outputEntities.RemoveRange(0, count);
        }


        private void CreateRequestTask(List<Entity> entities, int entityIndex)
        {
            var tasks = new List<Task>();
            var cnt = 0 + entityIndex;
            foreach (var entity in entities)
            {
                var cnt1 = cnt;
                var bufTask = Task.Factory.StartNew(() => Create(entity, cnt1));
                tasks.Add(bufTask);
                cnt++;
            }
            Task.WaitAll(tasks.ToArray(), -1);
        }

        private void Create(Entity entity, int entityIndex)
        {
            var id = _dataBaseService.CreateEntity(entity);
            CreateResult.TryAdd(entityIndex, id);
        }
    }
}