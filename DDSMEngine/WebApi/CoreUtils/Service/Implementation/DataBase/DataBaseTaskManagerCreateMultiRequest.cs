﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CoreUtils.Service.Interfaces.DataBase;
using CoreUtils.Wrap;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

namespace CoreUtils.Service.Implementation.DataBase
{
    internal class DataBaseTaskManagerCreateMultiRequest : IDataBaseTaskManagerCreate
    {
        private readonly BatchRequestService _batchRequestService;
        private readonly IDataBaseService _dataBaseService;
        //Provides programmatic access to the metadata and data for an organization.
        private readonly IOrganizationService _organizationService;
        public ConcurrentBag<Entity> ChildEntities;
        public ConcurrentDictionary<string, ConcurrentBag<OneToManyRelationshipMetadata>> OneToManyRelationshipMetadata;
        public ConcurrentBag<Entity> SimpleEntities;


        public DataBaseTaskManagerCreateMultiRequest(IDataBaseService dataBaseService,
            IOrganizationService organizationService)
        {
            _dataBaseService = dataBaseService;
            CreateResult = new ConcurrentDictionary<int, Guid>();

            _batchRequestService = new BatchRequestService();
            _organizationService = organizationService;
            OneToManyRelationshipMetadata =
                new ConcurrentDictionary<string, ConcurrentBag<OneToManyRelationshipMetadata>>();
            ChildEntities = new ConcurrentBag<Entity>();
            SimpleEntities = new ConcurrentBag<Entity>();
        }

        private ConcurrentDictionary<int, Guid> CreateResult { get; }


        public ConcurrentDictionary<int, Guid> Create(List<Entity> entities, int totalEntitiesCount)
        {
            //TODO
            //Clear refactor it
            Entity someItem;


            while (!SimpleEntities.IsEmpty)
                SimpleEntities.TryTake(out someItem);

            var totalTime = Stopwatch.StartNew();
            var start = 0;

            var outputEntities = new List<Entity>();
            outputEntities.AddRange(entities);
            var outputEntitiesCount = outputEntities.Count;
            var processedEntities = 0;

            var offset = _batchRequestService.DefaultEntitiesIntoStep;

            while (outputEntities.Count != 0)
            {
                var batchEntities = outputEntities.Skip(start).Take(offset).ToList();

                ExecuteBatch(batchEntities, processedEntities);

                //
                var swProcess = Stopwatch.StartNew();
                //execute tasks

                var currentExecutionTime = swProcess.Elapsed;
                processedEntities += batchEntities.Count;
                Log.Debug(
                    $"***Processed Entities:{batchEntities.Count}/{outputEntities.Count}. {processedEntities * 100 / outputEntitiesCount}%.  time:{currentExecutionTime}***");
                Console.WriteLine(
                    $"***Processed Entities:{batchEntities.Count}/{outputEntities.Count}. {processedEntities * 100 / outputEntitiesCount}%.  time:{currentExecutionTime}***");

                RemoveDataFromList(outputEntities, offset);

                offset = 10;
            }

            if (SimpleEntities.Count != 0)
                CreateEntityBatch();

            Log.Debug(
                $"***Total Execution Time:{totalTime.Elapsed}. Processed:{CreateResult.Count} of {entities.Count}***");
            Console.WriteLine(
                $"***Total Execution Time:{totalTime.Elapsed}. Processed:{CreateResult.Count} of {entities.Count}***");
            return CreateResult;
        }

        private void ExecuteBatch(List<Entity> entities, int entityIndex)
        {
            var cnt = entityIndex;
            foreach (var entity in entities)
                CreateMainEntity(entity, cnt++);
        }

        private void CreateMainEntity(Entity entity, int index)
        {
            var relatedEntities = GetRelatedEntities(entity);

            var newEntity = new Entity(entity.LogicalName);
            newEntity.Attributes.AddRange(entity.Attributes);

            var id = _dataBaseService.CreateEntity(newEntity);
            newEntity.Id = id;
            CreateResult.TryAdd(index, id);
            if (relatedEntities.Count == 0) return;
            CreateRelatedEntities(newEntity, relatedEntities);
        }

        private void CreateRelatedEntities(Entity entity, RelatedEntityCollection relatedEntityCollection)
        {
            try
            {
                foreach (var relatedEntities in relatedEntityCollection)
                {
                    var childEntities = new List<Entity>();
                    var entities = relatedEntityCollection[relatedEntities.Key];
                    foreach (var child in entities.Entities)
                    {
                        var referencingAttribute = GetReferencingAttrOneToMany(entity.LogicalName, child.LogicalName,
                            relatedEntities.Key.SchemaName);
                        if (string.IsNullOrEmpty(referencingAttribute)) continue;
                        child[referencingAttribute] = entity.ToEntityReference();

                        if (GetRelatedEntities(child).Count == 0)
                        {
                            SimpleEntities.Add(child);
                            continue;
                        }
                        childEntities.Add(child);
                    }
                    if (childEntities.Count != 0)
                        CreateChildEntities(childEntities);
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "CreateRelatedEntities");
                Console.WriteLine(e);
                throw;
            }
        }

        private void CreateChildEntities(List<Entity> childEntities)
        {
            foreach (var entity in childEntities)
            {
                var relatedEntities = GetRelatedEntities(entity);

                var newEntity = new Entity(entity.LogicalName);
                newEntity.Attributes.AddRange(entity.Attributes);

                var id = _dataBaseService.CreateEntity(newEntity);
                newEntity.Id = id;

                if (relatedEntities.Count == 0) return;
                CreateRelatedEntities(newEntity, relatedEntities);
            }
        }

        public void CreateEntityBatch()
        {
            try
            {
                var outputEntities = SimpleEntities;
                Console.WriteLine($"CreateEntityBatch={outputEntities.Count}");


                var start = 0;
                var offset = 50;
                var counter = 1;

                if (outputEntities.Count > offset)
                {
                    counter = outputEntities.Count / offset;
                    var remainder = outputEntities.Count % offset;
                    if (remainder > 0)
                        counter++;
                }
                var processedEntities = 0;
                for (var i = 0; i < counter; i++)
                {
                    var batchEntities = outputEntities.Skip(start).Take(offset).ToList();
                    var sw = Stopwatch.StartNew();
                    CreateRequest(batchEntities);
                    processedEntities += batchEntities.Count;
                    var pros = (double) processedEntities * 100 / outputEntities.Count;
                    Console.WriteLine(
                        $"***Saved Entities:{processedEntities} of {outputEntities.Count} ({Math.Round(pros, 1)}%). time:{sw.Elapsed}***");
                    start += offset;
                }
            }
            catch
                (Exception e)
            {
                Log.Error(e, "CreateEntityBatch");
                throw;
            }
        }

        /// <summary>
        ///     Execute multiple update request
        /// </summary>
        /// <param name="entities"></param>
        public void CreateRequest(List<Entity> entities)
        {
            try
            {
                var requestWithNoResults = new ExecuteMultipleRequest
                {
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = false,
                        ReturnResponses = true
                    },
                    Requests = new OrganizationRequestCollection()
                };

                foreach (var item in entities)
                {
                    var createRequest = new CreateRequest {Target = item};
                    requestWithNoResults.Requests.Add(createRequest);
                }
                _organizationService.Execute(requestWithNoResults);
            }
            catch (Exception e)
            {
                Log.Error(e, "CreateRequest");
                throw new Exception(e.Message);
            }
        }

        private RelatedEntityCollection GetRelatedEntities(Entity entity)
        {
            var rs = new RelatedEntityCollection();
            if (!string.IsNullOrEmpty(entity?.LogicalName))
                return entity.RelatedEntities;
            return rs;
        }

        public string GetReferencingAttrOneToMany(string targetEntity, string referencingEntity, string schemaName)
        {
            try
            {
                var currentEntityMetadata = new ConcurrentBag<OneToManyRelationshipMetadata>();


                var oneToManyRelationshipMetadata = OneToManyRelationshipMetadata;

                if (oneToManyRelationshipMetadata.ContainsKey(targetEntity))
                {
                    var tmp = new ConcurrentBag<OneToManyRelationshipMetadata>();
                    if (oneToManyRelationshipMetadata.TryGetValue(targetEntity, out tmp))
                        currentEntityMetadata = tmp;
                }
                else
                {
                    var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
                    {
                        EntityFilters = EntityFilters.Relationships,
                        LogicalName = targetEntity
                    };

                    var retrieveBankEntityResponse =
                        (RetrieveEntityResponse) _organizationService.Execute(retrieveBankAccountEntityRequest);

                    foreach (var item in retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships)
                        currentEntityMetadata.Add(item);


                    OneToManyRelationshipMetadata.TryAdd(targetEntity, currentEntityMetadata);
                }

                foreach (var item in currentEntityMetadata)
                    if (item.ReferencingEntity == referencingEntity &&
                        item.ReferencedEntityNavigationPropertyName == schemaName)
                        return item.ReferencingAttribute;

                return string.Empty;
            }
            catch (Exception e)
            {
                Log.Error(e, "GetReferencingAttrOneToMany");
                throw;
            }
        }


        private void RemoveDataFromList(List<Entity> outputEntities, int count)
        {
            var listCount = outputEntities.Count;
            if (listCount < count)
                count = listCount;

            outputEntities.RemoveRange(0, count);
        }


        private void CreateRequestTask(List<Entity> entities, int entityIndex)
        {
            var tasks = new List<Task>();
            var cnt = 0 + entityIndex;
            foreach (var entity in entities)
            {
                var cnt1 = cnt;
                var bufTask = Task.Factory.StartNew(() => Create(entity, cnt1));
                tasks.Add(bufTask);
                cnt++;
            }
            Task.WaitAll(tasks.ToArray(), -1);
        }

        private void Create(Entity entity, int entityIndex)
        {
            var id = _dataBaseService.CreateEntity(entity);
            Console.WriteLine($"Create:{entityIndex}");
            CreateResult.TryAdd(entityIndex, id);
        }
    }
}