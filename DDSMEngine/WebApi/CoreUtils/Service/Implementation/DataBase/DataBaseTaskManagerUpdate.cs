﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CoreUtils.Service.Interfaces.DataBase;
using Microsoft.Xrm.Sdk;

namespace CoreUtils.Service.Implementation.DataBase
{
    internal class DataBaseTaskManagerUpdate : IDataBaseTaskManagerUpdate
    {
        private readonly IDataBaseService _dataBaseService;

        public DataBaseTaskManagerUpdate(IDataBaseService dataBaseService)
        {
            _dataBaseService = dataBaseService;
        }

        public void Update(List<Entity> outputEntities, int offset = 10)
        {
            var start = 0;
            var counter = 1;
            var tasks = new List<Task>();
            if (outputEntities.Count > offset)
            {
                counter = outputEntities.Count / offset;
                var remainder = outputEntities.Count % offset;
                if (remainder > 0)
                    counter++;
            }

            for (var i = 0; i < counter; i++)
            {
                var btachEntities = outputEntities.Skip(start).Take(offset).ToList();
                var bufTask = Task.Factory.StartNew(() => UpdateRequestTask(btachEntities));
                tasks.Add(bufTask);
                start += offset;
                Task.WaitAll(tasks.ToArray(), -1);
#if DEBUG
                var sw = Stopwatch.StartNew();
                Console.WriteLine($"---saved {start * 100 / outputEntities.Count}%");
                Console.WriteLine($"{btachEntities.Count}. time saving to db:{sw.Elapsed}");
#endif
            }
        }


        private void UpdateRequestTask(List<Entity> entities)
        {
            var tasks = new List<Task>();
            var cnt = 0;
            foreach (var entity in entities)
            {
                var bufTask = Task.Factory.StartNew(() => Update(entity));
                tasks.Add(bufTask);
                cnt++;
            }
            Task.WaitAll(tasks.ToArray(), -1);
        }

        private void Update(Entity entity)
        {
            _dataBaseService.UpdateEntity(entity);
        }
    }
}