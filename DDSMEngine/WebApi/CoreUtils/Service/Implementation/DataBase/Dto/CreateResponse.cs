﻿using System;

namespace CoreUtils.Service.Implementation.DataBase.Dto
{
    internal class CreateResponse
    {
        public CreateResponse(Guid id)
        {
            Id = id;
        }

        public CreateResponse()
        {
            
        }
        public string ErrorMessage;
        public Exception FullMessage;

        public Guid Id;
        public bool Success;
    }
}