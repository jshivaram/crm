﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreUtils.Service.Implementation
{
    public class GenerationNameService
    {
        public enum AccountType
        {
            UnassignedValue = -2147483648,
            Business = 962080000,
            Residential = 962080002,
        }
        private readonly IOrganizationService _orgService;
        private readonly string _logicalName;
        private readonly string _primaryAttributeName;
        //Cashe
        private ConcurrentDictionary<string, string> _casheDictionary;

        public GenerationNameService(IOrganizationService orgService, string logicalName, string primaryAttributeName)
        {
            _orgService = orgService;
            _logicalName = logicalName;
            _primaryAttributeName = primaryAttributeName;
            _casheDictionary = new ConcurrentDictionary<string, string>();
        }

        public string GetName(Entity enExcel, Entity enMapping)
        {
            string newName = string.Empty;

            switch (_logicalName)
            {
                case "account":
                    if (enExcel.Attributes.ContainsKey("ddsm_accounttype"))
                    {
                        if (enExcel.GetAttributeValue<OptionSetValue>("ddsm_accounttype").Value == (int)AccountType.Residential)
                        {
                            newName = (enExcel.Attributes.ContainsKey("ddsm_firstname")) ? enExcel.GetAttributeValue<string>("ddsm_firstname") : string.Empty;
                            if (enMapping.Attributes.ContainsKey("ddsm_lastname"))
                            {
                                newName = (!string.IsNullOrEmpty(newName))
                                    ? newName + " " + enMapping.GetAttributeValue<string>("ddsm_lastname")
                                    : enMapping.GetAttributeValue<string>("ddsm_lastname");
                            }
                        }
                        if (enExcel.GetAttributeValue<OptionSetValue>("ddsm_accounttype").Value == (int) AccountType.Business)
                        {

                            newName = (enExcel.Attributes.ContainsKey("ddsm_companyname"))?enExcel.GetAttributeValue<string>("ddsm_companyname"): string.Empty;
                        }
                    }
                    break;

                case "ddsm_site":
                    newName = (enExcel.Attributes.ContainsKey("ddsm_address1")) ? enExcel.GetAttributeValue<string>("ddsm_address1") : string.Empty;
                    var addr2 = (enExcel.Attributes.ContainsKey("ddsm_address2")) ? enExcel.GetAttributeValue<string>("ddsm_address2") : string.Empty;
                    var city = (enExcel.Attributes.ContainsKey("ddsm_city")) ? enExcel.GetAttributeValue<string>("ddsm_city") : string.Empty;
                    newName = (string.IsNullOrEmpty(newName) && enMapping.Attributes.ContainsKey("ddsm_address1")) ? enMapping.GetAttributeValue<string>("ddsm_address1") : string.Empty;
                    addr2 = (string.IsNullOrEmpty(addr2) && enMapping.Attributes.ContainsKey("ddsm_address2")) ? enMapping.GetAttributeValue<string>("ddsm_address2") : string.Empty;
                    city = (string.IsNullOrEmpty(city) && enMapping.Attributes.ContainsKey("ddsm_city")) ? enMapping.GetAttributeValue<string>("ddsm_city") : string.Empty;

                    newName = (!string.IsNullOrEmpty(newName))
                        ? newName + ((!string.IsNullOrEmpty(addr2)) ? (" " + addr2) : string.Empty)
                        : ((!string.IsNullOrEmpty(addr2)) ? addr2 : string.Empty);

                    newName = (!string.IsNullOrEmpty(newName))
                        ? newName + ((!string.IsNullOrEmpty(city)) ? (" " + city) : string.Empty)
                        : ((!string.IsNullOrEmpty(city)) ? city : string.Empty);

                    Guid parentAccount = Guid.Empty;
                    parentAccount = (enExcel.Attributes.ContainsKey("ddsm_parentaccount"))
                        ? enExcel.GetAttributeValue<EntityReference>("ddsm_parentaccount").Id
                        : Guid.Empty;

                    if (parentAccount == Guid.Empty)
                        parentAccount = (enMapping.Attributes.ContainsKey("ddsm_parentaccount"))
                            ? enMapping.GetAttributeValue<EntityReference>("ddsm_parentaccount").Id
                            : Guid.Empty;

                    string accName = (parentAccount != Guid.Empty) ? GetAccountName(parentAccount) : string.Empty;

                    newName = (!string.IsNullOrEmpty(newName))
                        ? newName + ((!string.IsNullOrEmpty(accName)) ? ("-" + accName) : string.Empty)
                        : ((!string.IsNullOrEmpty(accName)) ? accName : string.Empty);

                    break;

                case "ddsm_projectgroup":
                    newName = (enExcel.Attributes.ContainsKey("ddsm_autonumbering"))?enExcel.GetAttributeValue<string>("ddsm_autonumbering"): string.Empty;
                    if (enMapping.Attributes.ContainsKey(_primaryAttributeName))
                    {
                        newName = (!string.IsNullOrEmpty(newName))
                            ? newName + ((enExcel.Attributes.ContainsKey(_primaryAttributeName)) ? ("-" + enMapping.GetAttributeValue<string>(_primaryAttributeName)) : string.Empty)
                            : ((enExcel.Attributes.ContainsKey(_primaryAttributeName)) ? enMapping.GetAttributeValue<string>(_primaryAttributeName) : string.Empty);
                    }
                    break;

                case "ddsm_project":
                    newName = (enExcel.Attributes.ContainsKey("ddsm_autonumbering"))?enExcel.GetAttributeValue<string>("ddsm_autonumbering"): string.Empty;
                    if (enMapping.Attributes.ContainsKey(_primaryAttributeName))
                    {
                        newName = (!string.IsNullOrEmpty(newName))
                            ? newName + ((enExcel.Attributes.ContainsKey(_primaryAttributeName)) ? ("-" + enMapping.GetAttributeValue<string>(_primaryAttributeName)) : string.Empty)
                            : ((enExcel.Attributes.ContainsKey(_primaryAttributeName)) ? enMapping.GetAttributeValue<string>(_primaryAttributeName) : string.Empty);
                    }
                    //Get ParentSite.Address1 
                    Guid parentSite = Guid.Empty;
                    parentSite = (enExcel.Attributes.ContainsKey("ddsm_parentsite"))
                        ? enExcel.GetAttributeValue<EntityReference>("ddsm_parentsite").Id
                        : Guid.Empty;

                    if (parentSite == Guid.Empty)
                        parentSite = (enMapping.Attributes.ContainsKey("ddsm_parentsite"))
                            ? enMapping.GetAttributeValue<EntityReference>("ddsm_parentsite").Id
                            : Guid.Empty;

                    string addr = (parentSite != Guid.Empty)? GetSiteAddress(parentSite) : string.Empty;

                    newName = (!string.IsNullOrEmpty(newName))
                        ? newName + ((!string.IsNullOrEmpty(addr)) ? ("-" + addr) : string.Empty)
                        : ((!string.IsNullOrEmpty(addr)) ? addr : string.Empty);
                    break;

                case "ddsm_financial":
                    newName = (enExcel.Attributes.ContainsKey("ddsm_autonumbering")) ? enExcel.GetAttributeValue<string>("ddsm_autonumbering") : string.Empty;
                    if (enMapping.Attributes.ContainsKey(_primaryAttributeName))
                    {
                        newName = (!string.IsNullOrEmpty(newName))
                            ? newName + ((enExcel.Attributes.ContainsKey(_primaryAttributeName)) ? ("-" + enMapping.GetAttributeValue<string>(_primaryAttributeName)) : string.Empty)
                            : ((enExcel.Attributes.ContainsKey(_primaryAttributeName)) ? enMapping.GetAttributeValue<string>(_primaryAttributeName) : string.Empty);
                    }
                    break;
            }

            return newName;
        }

        private string GetSiteAddress(Guid siteId)
        {
            string addr = string.Empty;
            if (siteId != Guid.Empty)
            {
                if (_casheDictionary.ContainsKey(siteId.ToString()))
                {
                    addr = _casheDictionary[siteId.ToString()];
                }
                else
                {
                    Entity retrieve = _orgService.Retrieve("ddsm_site", siteId, new ColumnSet("ddsm_address1"));
                    addr = (retrieve != null && retrieve.Attributes.ContainsKey("ddsm_address1")) ? retrieve.GetAttributeValue<string>("ddsm_address1") : string.Empty;
                    _casheDictionary.AddOrUpdate(siteId.ToString(), addr, (oldkey, oldvalue) => addr);
                }
            }
            return addr;
        }

        private string GetAccountName(Guid accountId)
        {
            string name = string.Empty;
            if (accountId != Guid.Empty)
            {
                if (_casheDictionary.ContainsKey(accountId.ToString()))
                {
                    name = _casheDictionary[accountId.ToString()];
                }
                else
                {
                    Entity retrieve = _orgService.Retrieve("account", accountId, new ColumnSet("name"));
                    name = (retrieve != null && retrieve.Attributes.ContainsKey("name")) ? retrieve.GetAttributeValue<string>("name") : string.Empty;
                    _casheDictionary.AddOrUpdate(accountId.ToString(), name, (oldkey, oldvalue) => name);
                }
            }
            return name;
        }

    }
}
