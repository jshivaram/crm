﻿using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace CoreUtils.Service.Interfaces.DataBase
{
    internal interface IDataBaseTaskManagerUpdate
    {
        void Update(List<Entity> outputEntities, int offset = 10);
    }
}