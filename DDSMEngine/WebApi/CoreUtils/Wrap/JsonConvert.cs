﻿using System;
using Newtonsoft.Json;

namespace CoreUtils.Wrap
{
    public static class JsonConvert
    {
        public static string SerializeObject(Object target)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(target);
        }

        public static string SerializeObject(Object target, Formatting formatting)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(target, formatting);
        }

        public static T DeserializeObject<T>(string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }
    }
}
