﻿using System;
using Serilog;

namespace CoreUtils.Wrap
{
    public static class Log
    {
        public static void Debug(string text)
        {
            Serilog.Log.Debug(text);
        }

        public static void Error(string text)
        {
            Serilog.Log.Error(text);
        }
     

        public static void Error(Exception ex, string text)
        {
            Serilog.Log.Error(ex, text);
        }

        public static void Fatal(string text)
        {
            Serilog.Log.Fatal(text);
        }
    }
}
