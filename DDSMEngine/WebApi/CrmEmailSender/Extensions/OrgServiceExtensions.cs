﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using CrmEmailSender.Enums;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace CrmEmailSender.Extensions
{
    public static class OrgServiceExtensions
    {
        public static IEnumerable<Guid> GetDraftedEmailsIdList(this OrganizationService orgService)
        {
            string emailSubject = ConfigurationManager.AppSettings["EmailSubject"];

            if (string.IsNullOrEmpty(emailSubject))
            {
                throw new ArgumentNullException(nameof(emailSubject));
            }

            var query = new QueryExpression
            {
                EntityName = "email",
                ColumnSet = new ColumnSet("scheduledend")
            };

            query.Criteria.AddCondition("statuscode", ConditionOperator.Equal, (int)EmailStatus.Draft);
            query.Criteria.AddCondition("scheduledend", ConditionOperator.Today);
            query.Criteria.AddCondition("subject", ConditionOperator.Equal, emailSubject);

            var emails = orgService.RetrieveMultiple(query).Entities;
            var result = emails.Where(e => e != null).Select(e => e.Id).ToList();
            return result;
        }

        public static ExecuteMultipleResponse SendDraftedEmails(this OrganizationService orgService, IEnumerable<Guid> emailsIdList)
        {
            if (emailsIdList == null)
            {
                throw new ArgumentNullException(nameof(emailsIdList));
            }

            var requests = FormRequestsForSendEmail(emailsIdList);

            if (requests?.Count() == null)
            {
                return null;
            }

            var orgRequestsCollection = new OrganizationRequestCollection();
            foreach (SendEmailRequest emailRequest in requests)
            {
                orgRequestsCollection.Add(emailRequest);
            }

            var multipleEmailCreationRequest = new ExecuteMultipleRequest
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                },
                Requests = orgRequestsCollection
            };

            var responseWithResults = (ExecuteMultipleResponse)orgService.Execute(multipleEmailCreationRequest);
            return responseWithResults;
        }

        private static IEnumerable<SendEmailRequest> FormRequestsForSendEmail(IEnumerable<Guid> emailsIdList)
        {
            if (emailsIdList == null)
            {
                throw new ArgumentNullException(nameof(emailsIdList));
            }

            var requests = new List<SendEmailRequest>();

            foreach (Guid emailId in emailsIdList)
            {
                SendEmailRequest request = FormRequestForSendEmail(emailId);

                if (request == null)
                {
                    continue;
                }

                requests.Add(request);
            }

            return requests;
        }

        private static SendEmailRequest FormRequestForSendEmail(Guid emailId)
        {
            if (emailId == Guid.Empty)
            {
                return null;
            }

            var sendEmailReq = new SendEmailRequest
            {
                EmailId = emailId,
                TrackingToken = "",
                IssueSend = true
            };

            return sendEmailReq;
        }
    }
}
