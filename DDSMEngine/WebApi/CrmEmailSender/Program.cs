﻿using System;
using System.Collections.Generic;
using CrmEmailSender.Extensions;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;

namespace CrmEmailSender
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionStringName = "CRMConnectionString";

            try
            {
                using (var orgService = new OrganizationService(connectionStringName))
                {
                    IEnumerable<Guid> draftedEmailsIdList = orgService.GetDraftedEmailsIdList();
                    var result = orgService.SendDraftedEmails(draftedEmailsIdList);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Environment.Exit(0);
        }
    }
}
