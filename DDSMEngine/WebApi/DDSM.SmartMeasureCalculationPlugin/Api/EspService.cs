﻿using System;
using EspCalculation.API;
using EspCalculation.Model;
using Microsoft.Xrm.Sdk;
using Serilog;

namespace EspCalculation.Api
{
    public class EspService
    {
        #region internal fields
        private CalculatorWebServiceConsumer _espDriver;
        private ESPConfig _config;
        private IOrganizationService _service;
        #endregion

        public EspService(ESPConfig config, IOrganizationService service)
        {
            if (config == null)
            {
                var msg= "Can't get ESP config.";
                Log.Fatal(msg);
                throw new Exception(msg);
            }
            _espDriver = new CalculatorWebServiceConsumer();
            _config = config;
            _service = service;
            //Log.Debug("_config: " + JsonConvert.SerializeObject(_config));

            if (_config.IsTokenNeedUpdate())
            {
                Log.Debug("Token must update");
                var authResp = _espDriver.Authenticate(_config.Url, _config.Login, _config.Pass);
                if (authResp.ResponseHeader.StatusOk)
                {
                    _config.LastToken = authResp.ResponseHeader.AuthenticationTokenBytes;
                    _config.RequestReceivedDate = authResp.ResponseHeader.ResponseDateTime;
                    UpdateConfig();
                }

            }
        }

        /// <summary>
        /// Update last token and last request dateTime  
        /// </summary>
        public void UpdateConfig()
        {
            if (_config.RequestReceivedDate != DateTime.MinValue) ///&& LastRequestReceivedData != (DateTime)Config.Attributes["ddsm_requestreceiveddate"]
            {
                var newEntity = new Entity("ddsm_admindata", _config.ConfigId); //,new Guid(adminDataID)
                newEntity.Attributes["ddsm_requestreceiveddate"] = _config.RequestReceivedDate;
                newEntity.Attributes["ddsm_esplasttoken"] = Convert.ToBase64String(_config.LastToken);
                _service.Update(newEntity);
            }
        }

        /// <summary>
        /// Get Available Calculators
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Return Available Calculators</returns>
        public AvailableCalculatorsResponse GetAvailableCalculators(AvailableCalculatorsRequest request)
        {
            Log.Debug("In GetAvailableCalculators");
            return _espDriver.GetAvailableCalculators(_config.Url, request);
        }

        /// <summary>
        /// Retriev Metadata from ESP
        /// </summary>
        public void GetMetadata()
        {
            Log.Debug("In GetMetadata");

        }

        /// <summary>
        /// Calculate Smart Measure
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Return result of calculation</returns>
        public CalculationResponse Calculate(CalculationRequest request)
        {
            Log.Debug("In Calculate");
            return _espDriver.Calculate(_config.Url, request);
        }

    }
}
