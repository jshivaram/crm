﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EspCalculation.Api;
using EspCalculation.Model;
using EspCalculation.Options;
using EspCalculation.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using Serilog;

namespace EspCalculation.App
{
    public class AppCalculateMeasure : BaseApp
    {
        #region internal fields
        private readonly string _mainEntity = "ddsm_measure";
        internal EntityReference DataUploader;
        private string _url = "";
        private EspService _espService;
        private CalculateOptions _options;
        #endregion

        public AppCalculateMeasure(IOrganizationService service, CalculateOptions options, Action callbackAction = null) : base(service, callbackAction)
        {
            _options = options;
        }

        public override void Run()
        {
            if (_options == null)
            {
                Completed = false;
                var msg = "Calculate Options is null.";
                Log.Fatal(msg);
                return;
                // throw new Exception(msg);
                // Result = JsonConvert.SerializeObject(new { MeasureName = "", MeasureID = _options.MeasureId, ErrorMsg= "Calculate Options is null." });
            }
            var espConf = _crmHelper.GetConfig();
            _options.Config = new ESPConfig(espConf);

            _espService = new EspService(_options.Config, OrganizationService);

            var userInput = GetUserObj();
            var measureTemplate = GetMeasureTemplateMAD(userInput.MeasureTplID);
            //var measureId = CreateMeasure(userInput, measureTemplate);
            //userInput.MeasureID = measureId;
            RecalculateMeasure(userInput, measureTemplate);
            Completed = true;
            Result = JsonConvert.SerializeObject(new { MeasureName = "", MeasureID = _options.MeasureId });
        }

        #region Internal methods

        private void RecalculateMeasure(UserInputObj userInput, Entity measureTemplate)
        {
            try
            {
                var aCalculationRequest = CreateCalculationRequest(measureTemplate, _options.Config.LastToken, userInput);
                if (aCalculationRequest != null)
                {
                    var resultCalculationResponse = _espService.Calculate(aCalculationRequest);
                    //update Measure using calculation result
                    UpdateMeasure(resultCalculationResponse, userInput, measureTemplate);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "error");
            }
        }

        private UserInputObj GetUserObj()
        {
            UserInputObj result;
            var userInputJsonB64 = _options.UserInput;

            if (!string.IsNullOrEmpty(userInputJsonB64))
            {
                var userInputJsonBytes = Convert.FromBase64String(userInputJsonB64);
                var userInputJson = Encoding.UTF8.GetString(userInputJsonBytes);
                result = JsonConvert.DeserializeObject<UserInputObj>(userInputJson);
            }
            else
            {
                throw new Exception("User Input is Empty. Nothing to processing.");
            }
            return result;
        }

        private Entity GetMeasureTemplateMAD(Guid id)
        {
            Entity result = new Entity();
            var query = new QueryExpression()
            {
                EntityName = "ddsm_measuretemplate",
                ColumnSet = new ColumnSet("ddsm_madfields", "ddsm_smartmeasureversionid", "ddsm_espsmartmeasureid", "ddsm_programoffering", "ddsm_msdfields", "ddsm_mappingid", "ddsm_qddmappingid", "ddsm_measurecalculationtype"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = {
                        new ConditionExpression("ddsm_measuretemplateid",ConditionOperator.Equal,id)
                    }
                },
                LinkEntities = {
                    new LinkEntity("ddsm_measuretemplate", "ddsm_mapping", "ddsm_mappingid", "ddsm_mappingid", JoinOperator.Inner) { Columns = new ColumnSet("ddsm_jsondata") },
                    new LinkEntity("ddsm_measuretemplate", "ddsm_mapping", "ddsm_qddmappingid", "ddsm_mappingid", JoinOperator.Inner) { Columns = new ColumnSet("ddsm_jsondata") },
                }
            };
            var resultCollection = OrganizationService.RetrieveMultiple(query);
            if (resultCollection.Entities.Count > 0)
            {
                result = resultCollection.Entities[0];
            }
            return result;
        }
      
        private void UpdateMeasure(CalculationResponse resultCalculationResponse, UserInputObj userInput, Entity measureTempl)
        {
            try
            {
                if (userInput == null)
                {
                    throw new Exception("Can't get user input object.");
                }

                if (!resultCalculationResponse.ResponseHeader.StatusOk)
                {
                    throw new Exception(resultCalculationResponse.GetFullESPError());
                }

                var mappingQddData = _crmHelper.GetQddMapping(measureTempl);

                var newMeasure = new Entity(_mainEntity, _options.MeasureId)
                {
                    Attributes =
                        {
                            ["ddsm_smartmeasureid"] = userInput.EspSmartMeasureID,
                            ["ddsm_esptargetdate"] = resultCalculationResponse.ResponseHeader.ResponseDateTime,
                        }
                };

                if (mappingQddData != null)
                {
                    //process qdd
                    foreach (var qdd in resultCalculationResponse.ESPWSCalculatorCalculationList[0].ESPWSVariableCalculationList)
                    {
                        var crmName = mappingQddData.FirstOrDefault(x => x.Key.Equals(qdd.Name) && !string.IsNullOrEmpty(x.Value.AttrName)).Value;
                        if (crmName != null)
                        {
                            if (crmName.Entity.Equals(_mainEntity))
                            {
                                _crmHelper.SetAttributeValue(crmName, qdd, newMeasure);
                            }
                        }
                    }
                }
                OrganizationService.Update(newMeasure);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
            }
        }

        private CalculationRequest CreateCalculationRequest(Entity measureTempl, byte[] token, UserInputObj userInput)
        {
            var aCalculationRequest = new CalculationRequest { RequestHeader = { AuthenticationTokenBytes = token } };
            var mappings = _crmHelper.GetMadMapping(measureTempl);

            if (measureTempl?.Id == Guid.Empty)
            {
                Log.Fatal("Can't get actual Measure Template MAD fields. Check mapping between ESP SmartMeasure and Measure Template.");
                return null;
                // throw new Exception("Can't get actual Measure Template MAD fields. Check mapping between ESP SmartMeasure and Measure Template.");
            }

            var madfields = measureTempl.GetAttributeValue<string>("ddsm_madfields");

            if (string.IsNullOrEmpty(madfields))
            {
                Log.Fatal("ESP calculation aborted. Can't get MT Mad Fields.");
                return null;
                //  throw new Exception("ESP calculation aborted. Can't get MT Mad Fields.");
            }

            var calculatorCalculationRequest = new CalculatorCalculationRequest
            {
                CalculatorID = new Guid(measureTempl.Attributes["ddsm_espsmartmeasureid"].ToString()),
                TargetDateTime = DateTime.UtcNow
            };
            var curentMad = JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(measureTempl.Attributes["ddsm_madfields"].ToString());

            foreach (var field in userInput.DataFields)
            {
                var mad = curentMad.FirstOrDefault(x => x.Name.ToLower().Equals(field.Key.ToLower()));

                var mappingInfo = mappings.FirstOrDefault(x => x.Key.ToLower().Equals(field.Key.ToLower()));

                if (mad != null && !string.IsNullOrEmpty(mappingInfo.Key) && mappingInfo.Value != null)
                {
                    var variableValueRequest = _crmHelper.GenerateESPWSVariableValueRequestForManual(mappingInfo, mad, field); //field, mad, mappingInfo?.Value

                    var sss = calculatorCalculationRequest.InputVariables.FirstOrDefault(x => x.Name.Equals(mad.Name));
                    if (sss == null)
                        calculatorCalculationRequest.InputVariables.Add(variableValueRequest);
                    else
                    {
                        calculatorCalculationRequest.InputVariables.Remove(sss);
                        calculatorCalculationRequest.InputVariables.Add(variableValueRequest);
                    }
                }
            }
            if (curentMad.Count != calculatorCalculationRequest.InputVariables.Count)
                throw new Exception("User input is damaged. The number of fields in the calculator request is not equal to the current measure template mad.\n Please contact to your administrator");

            aCalculationRequest.CalculatorCalculationRequestList.Add(calculatorCalculationRequest);
            //}
            //}
            //else { throw new Exception("Can't get actual Measure Template MAD fields. Check mapping between ESP SmartMeasure and Measure Template."); }

            return aCalculationRequest;
        }

        #endregion
    }
}
