﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DmnEngineApp.Service;
using EspCalculation.Api;
using EspCalculation.Model;
using EspCalculation.Options;
using EspCalculation.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using Serilog;
using SocketClients.Dto;

namespace EspCalculation.App
{
    public class AppGroupCalculateMeasure : BaseApp
    {
        #region internal fields
        private GroupCalculateOptions _options;
        const string mainEntity = "ddsm_measure";
        private List<KeyValuePair<Guid, Guid>> processedMeasures;
        private ConcurrentBag<Guid> processedProject;
        private ConcurrentBag<Guid> processedProgOff;
        private ConcurrentBag<Guid> processedMasures;
        private int processedItemsCount = 0;
        internal WSMessageService wsMessageService;
        private ConcurrentDictionary<Guid, IDictionary<string, MappingItem>> _personalMads;
        private ConcurrentDictionary<Guid, IDictionary<string, MappingItem>> _personalQdds;
        private ConcurrentDictionary<string, MappingItem> _mappingData;
        private ConcurrentDictionary<string, MappingItem> _qddMappingData;
        private EspService _espService;
        internal override TaskQueue.StatusFileDataUploading CurrentOperationStatus
        {
            get { return TaskQueue.StatusFileDataUploading.ESPRecalculation; }
        }
        public enum CalculationRecordStatus
        {
            NotReady = 962080000,
            ReadyToCalculation = 962080001
        };
        public enum CreatorRecordType
        {
            Front = 962080000,
            DataUploader = 962080001,
            Portal = 962080002
        };
        #endregion

        public AppGroupCalculateMeasure(IOrganizationService orgService, GroupCalculateOptions options, Action callbackAction) : base(orgService, callbackAction, options.Target)
        {
            _options = options;
            wsMessageService = new WSMessageService(new WsCreateMsgDto { UserId = _options.UserId, EntityLogicalName = _options?.Target?.LogicalName });
            Init();
        }
        public override void Run()
        {
            wsMessageService?.SendMessage(WSMessageService.MessageType.INFORMATION, "Group ESP Calculation is started.", WSMessageService.CalcMessageStatus.START);

            Log.Debug("AppGroupCalculateMeasure: in Run()");

#if DEBUG
            Stopwatch tStopwatch = new Stopwatch();
            tStopwatch.Start();
#endif
            try
            {
                var espConf = _crmHelper.GetConfig();
                _options.Config = new ESPConfig(espConf);

                _espService = new EspService(_options.Config, OrganizationService);

                var measures = GetMeasureForCalculate();

                if (measures?.SmartMeasures?.Count > 0)
                {
                    CalculateMeasures(measures?.SmartMeasures);
                }
                Completed = true;
                Result = "";
            }
            catch (Exception e)
            {
                Logger.Trace("--------------------------------------------------");
                Logger.Trace("*** Error. AppGroupCalculateMeasure: in Run()." + e.Message);
                Log.Error(e, "Error. AppGroupCalculateMeasure: in Run()");
                UpdateDACurrentOperationStatus(TaskQueue.StatusFileDataUploading.importFailed);
            }
            finally
            {
                processCollectedData(processedProject);

                #region Update TQ and DA logs
                if (_options?.Target?.LogicalName == "ddsm_taskqueue" && Logger?.Logs?.Count > 0)
                {
                    OrganizationService?.Update(new Entity(_options.Target.LogicalName, _options.Target.Id)
                    {
                        Attributes = { new KeyValuePair<string, object>("ddsm_log", string.Join(Environment.NewLine, Logger.Logs)) }
                    });
                }

                if (DataUploader?.Id != Guid.Empty && Logger?.Logs?.Count > 0)
                {
                    var prevDaRecord = OrganizationService.Retrieve(DataUploader.LogicalName, DataUploader.Id, new ColumnSet("ddsm_log"));
                    var prevLog = prevDaRecord.GetAttributeValue<String>("ddsm_log");

                    var daRecord = new Entity(DataUploader.LogicalName, DataUploader.Id);

                    if (!string.IsNullOrEmpty(prevLog))
                    {
                        prevLog += Environment.NewLine + string.Join(Environment.NewLine, Logger.Logs);
                        daRecord.Attributes.Add(new KeyValuePair<string, object>("ddsm_log", prevLog));
                    }
                    OrganizationService.Update(daRecord);
                }


                #endregion
                wsMessageService?.SendMessage(WSMessageService.MessageType.SUCCESS, "Group ESP Calculation is finished.", WSMessageService.CalcMessageStatus.FINISH);
#if DEBUG
                tStopwatch.Stop();
                Log.Debug("Elapsed: " + tStopwatch.ElapsedMilliseconds + " ms");
#endif
                Log.Debug("Group SM calc finish: Completed: " + Completed);
                if (Callback != null)
                {
                    Log.Debug("_callback is not null. In Execute callback");
                    Callback();
                }
            }
        }

        #region Internal metods
        private void Init()
        {
            processedMeasures = new List<KeyValuePair<Guid, Guid>>();
            processedProject = new ConcurrentBag<Guid>();
            processedProgOff = new ConcurrentBag<Guid>();
            processedMasures = new ConcurrentBag<Guid>();
            _personalMads = new ConcurrentDictionary<Guid, IDictionary<string, MappingItem>>();
            _personalQdds = new ConcurrentDictionary<Guid, IDictionary<string, MappingItem>>();
            _mappingData = new ConcurrentDictionary<string, MappingItem>();
            _qddMappingData = new ConcurrentDictionary<string, MappingItem>();
            Logger = new Tracer();
        }

        private void CalculateMeasures(IReadOnlyCollection<Guid> smartMeasures)
        {
            Log.Debug("in CalculateMeasures: ");

            var options = _options.Config;
            var processedCount = 0;
            //var tasks = new List<Task>();
            while (processedCount < smartMeasures.Count)
            {
                //get data portion 
                var dataPortion = smartMeasures.Skip(processedCount).Take((int)options.DataPortion).ToList();
                Log.Debug($"Portion from:{processedCount}");
                ProcessPortion(dataPortion);
                //  var bufTask = Task.Factory.StartNew(() => ProcessPortion(dataPortion));
                processedCount += dataPortion.Count;
                //tasks.Add(bufTask);
            }
            // Task.WaitAll(tasks.ToArray(), -1);
            Log.Debug("End CalculateMeasures");
        }

        void ProcessPortion(List<Guid> dataPortion)
        {
            var smartMeasureData = GetMeasuresInfo(dataPortion); //TODO: try cache MT data
            wsMessageService?.SendMessage(WSMessageService.MessageType.INFORMATION, $"Group ESP Processed Measures Count:{smartMeasureData?.Entities.Count}", WSMessageService.CalcMessageStatus.START);
            if (smartMeasureData?.Entities.Count > 0)
            {
                var calcRequest = BuildCalculationRequest(smartMeasureData);

                var calcResult = _espService.Calculate(calcRequest);
                UpdateMeasures(calcResult, OrganizationService);
            }
            wsMessageService?.SendMessage(WSMessageService.MessageType.SUCCESS, $"Group ESP Processed Measures Count:{smartMeasureData?.Entities.Count}", WSMessageService.CalcMessageStatus.FINISH);
        }

        private void UpdateMeasures(CalculationResponse calcResult, IOrganizationService service)
        {
            Log.Debug("In UpdateMeasures. processedMeasures Count: " + processedMasures?.Count);
            var UpdateMeasureResuest = new List<UpdateRequest>();
            var processedCalcs = 0;
            var error = new List<string>();

            Log.Debug("calcResult.ESPWSCalculatorCalculationList.Count = " + calcResult?.ESPWSCalculatorCalculationList?.Count);
            Log.Debug("processedMeasures.Count = " + processedMeasures?.Count);


            for (int i = 0; i < calcResult?.ESPWSCalculatorCalculationList?.Count; i++)
            {
                //Log.Debug("i=" + i);
                var measureCalculation = calcResult.ESPWSCalculatorCalculationList[i];

                if (!measureCalculation.ResponseHeader.StatusOk)
                {
                    var msg = $"Updating measure with error. Error code: {calcResult.ResponseHeader.ErrorCode}. \nError: {calcResult.GetFullESPError()}.";
                    error.Add(msg);
                    Log.Error(msg);
                    continue;
                }
                var rrr = processedMeasures[i];

                if (rrr.Value != measureCalculation.ID)
                {
                    Log.Error("Can't get Measure Id for updating");
                    Log.Debug("calcId: " + measureCalculation.ID + " but ProcessedMesure calcId: " + rrr.Value);
                    continue;
                }

                IDictionary<string, MappingItem> maDmappingData = _personalMads.FirstOrDefault(x => x.Key.Equals(rrr.Key)).Value;
                IDictionary<string, MappingItem> qdDmappingData = _personalQdds.FirstOrDefault(x => x.Key.Equals(rrr.Key)).Value;
                Entity measureForUpdate = new Entity(mainEntity, rrr.Key);

                processedCalcs++;

                // set mads
                foreach (var result in measureCalculation.ESPWSVariableCalculationList)
                {
                    var mapItem = maDmappingData.FirstOrDefault(x => x.Key.ToLower().Equals(result.Name.ToLower()) && !string.IsNullOrEmpty(x.Value.AttrName));

                    if (mapItem.Value != null && mapItem.Key != null && mapItem.Value.Entity.Equals(mainEntity))
                    {
                        _crmHelper.SetAttributeValue(mapItem.Value, result, measureForUpdate);
                    }
                }
                //set qdds
                foreach (var result in measureCalculation.ESPWSVariableCalculationList)
                {
                    try
                    {
                        var mapItem = qdDmappingData.FirstOrDefault(x => x.Key.ToLower().Equals(result.Name.ToLower()) && !string.IsNullOrEmpty(x.Value.AttrName));

                        if (mapItem.Value != null && mapItem.Key != null && mapItem.Value.Entity.Equals(mainEntity))
                        {
                            _crmHelper.SetAttributeValue(mapItem.Value, result, measureForUpdate);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(" error in fill qdd: " + ex.Message + ex.StackTrace);
                        throw;
                    }
                }
                measureForUpdate["ddsm_recalculationdate"] = measureForUpdate["ddsm_esptargetdate"] = DateTime.UtcNow;
                measureForUpdate["ddsm_espcalculationsuccessful"] = true;
                measureForUpdate["ddsm_calculationrecordstatus"] = new OptionSetValue((int)CalculationRecordStatus.NotReady);
                var updateRequest = new UpdateRequest { Target = measureForUpdate };
                UpdateMeasureResuest.Add(updateRequest);
            }

            var processed = 0;
            var updatePortion = 100;

            var tasks = new List<Task>();
            while (processed < UpdateMeasureResuest.Count)
            {
                var portion = UpdateMeasureResuest.Skip(processed).Take(updatePortion);
                processed += updatePortion;

                var bufTask = Task.Factory.StartNew(() => updateMeasurePortion(portion, service));
                tasks.Add(bufTask);//
            }
            Task.WaitAll(tasks.ToArray(), -1);
        }

        private void updateMeasurePortion(IEnumerable<OrganizationRequest> portion, IOrganizationService service)
        {
            var request = new ExecuteMultipleRequest
            {
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };
            request.Requests.AddRange(portion);

            // Execute all the requests in the request collection using a single web method call.
            ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)service.Execute(request);
            if (responseWithResults.IsFaulted)
            {
                foreach (var resp in responseWithResults.Responses)
                {
                    if (resp.Fault != null)
                    {
                        Log.Debug("in ExecuteMultipleResponse: " + resp.Fault.Message);
                    }
                }
            }
        }

        private void processCollectedData(ConcurrentBag<Guid> processedProject)
        {
            // Log.Debug("In processCollectedData()");
            var taskQueue = new TaskQueue(OrganizationService);

            if (processedProject?.Count > 0)
            {
                wsMessageService?.SendMessage(WSMessageService.MessageType.INFORMATION, $"Creating the Task Queue for the Projects. All Projects:{processedProject?.Count}", WSMessageService.CalcMessageStatus.START);
                taskQueue.Create(new DDSM_Task(TaskQueue.TaskEntity.Project)
                {
                    ProcessedItems0 = processedProject.Select(x => x).Distinct().ToList()
                }, TaskQueue.TaskEntity.Project, dataUploader: DataUploader
                );
                wsMessageService?.SendMessage(WSMessageService.MessageType.SUCCESS, $"Creating the Task Queue for the Projects. All Projects:{processedProject?.Count}", WSMessageService.CalcMessageStatus.FINISH);
            }
        }

        private CalculationRequest BuildCalculationRequest(EntityCollection measureData)
        {
            Log.Debug("In BuildCalculationRequest");
            var aCalculationRequest = new CalculationRequest { RequestHeader = { AuthenticationTokenBytes = _options.Config.LastToken } };
            var processedCount = 0;

            processedMeasures = new List<KeyValuePair<Guid, Guid>>();
            foreach (var measureItem in measureData.Entities)
            {
                var calcIdValue = measureItem.GetAttributeValue<AliasedValue>("ddsm_measuretemplate1.ddsm_espsmartmeasureid");

                if (calcIdValue?.Value == null || string.IsNullOrEmpty(calcIdValue?.Value?.ToString()))
                {
                    var msg = $"ESP calculation for measure: {measureItem?.Id} aborted. Can't get ESP Calculation Id from MT.";
                    Logger.Trace(msg);
                    Log.Error(msg);
                    continue;
                }

                var calculatorCalculationRequest = new CalculatorCalculationRequest
                {
                    CalculatorID = new Guid(calcIdValue.Value.ToString()),
                    TargetDateTime = _options.Config.RequestReceivedDate
                };
                //TODO: get current MAD from MT
                var mtMadsFieldData = measureItem.GetAttributeValue<AliasedValue>("ddsm_measuretemplate1.ddsm_madfields");
                if (mtMadsFieldData?.Value == null)
                {
                    var msg = $"ESP calculation for measure: {measureItem.Id} aborted. Can't get MT Mad Fields.";
                    Logger.Trace(msg);
                    Log.Error(msg);
                    continue;
                }
                var mtMadsFieldJson = mtMadsFieldData.Value.ToString();
                var curentMad = JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(mtMadsFieldJson);

                _mappingData = _crmHelper.GetMadMapping(measureItem, groupRecalc: true);
                _qddMappingData = _crmHelper.GetQddMapping(measureItem, groupRecalc: true);

                _personalMads.TryAdd(measureItem.Id, _mappingData); //store personal mads to list
                _personalQdds.TryAdd(measureItem.Id, _qddMappingData);
                // get data
                var data = GetMeasureData(curentMad, _mappingData, measureItem.Id);


                // fill request from DB values
                foreach (var mad in curentMad)
                {
                    var crmFiled = _mappingData.FirstOrDefault(x => x.Key.ToLower().Equals(mad.Name.ToLower()));
                    var variableValueRequest = _crmHelper.GenerateESPWSVariableValueRequest(crmFiled, mad, data, mainEntity);

                    if (calculatorCalculationRequest.InputVariables.FirstOrDefault(x => x.Name.Equals(mad.Name)) == null)
                    {
                        calculatorCalculationRequest.InputVariables.Add(variableValueRequest);
                    }
                }
                processedCount++;
#if WITH_TASKQ
                collectProcessedData(data, processedProject, processedProgOff, processedMasures);
#endif
                if (curentMad.Count == calculatorCalculationRequest.InputVariables.Count)
                {
                    aCalculationRequest.CalculatorCalculationRequestList.Add(calculatorCalculationRequest);
                    processedMeasures.Add(new KeyValuePair<Guid, Guid>(measureItem.Id, calculatorCalculationRequest.CalculatorID));
                }
                else
                {
                    var msg = $"Mapping MAD are not successfully. Measure: {measureItem.GetAttributeValue<string>("ddsm_name")}. Current MAD count: {curentMad.Count}, mapped only: {calculatorCalculationRequest.InputVariables.Count}. Please check MAD mapping for current Smart Measure Template Or contact your administrator.";
                    Log.Warning(msg);
                    Logger.Trace("WARN: " + msg);
                }
                //Log.Debug("M#:" + processedCount+" was prepared");
            }


            return aCalculationRequest;
        }

        private void collectProcessedData(Entity data, ConcurrentBag<Guid> processedProject, ConcurrentBag<Guid> processedProgOff, ConcurrentBag<Guid> processedMasures)
        {
            //Log.Debug("in collectProcessedData");

            var projRef = data?.GetAttributeValue<EntityReference>("ddsm_projecttomeasureid");
            processedProject?.Add(projRef.Id);

            var progOffRef = data?.GetAttributeValue<EntityReference>("ddsm_programofferingsid");
            processedProgOff?.Add(progOffRef.Id);

            //collect measure ids  
            processedMasures?.Add(data.Id);
        }

        private Entity GetMeasureData(List<ESPWSVariableDefinitionResponse> curentMad, IDictionary<string, MappingItem> _mappingData, Guid measureId)
        {
            var sortedMadList = (from x in curentMad
                                 join y in _mappingData on x.Name.ToLower() equals y.Key.ToLower()
                                 select new KeyValuePair<string, string>(y.Value.AttrName.ToLower(), y.Value.Entity));

            var sortedMad = new Dictionary<string, string>();
            foreach (var item in sortedMadList)
            {
                if (!sortedMad.ContainsKey(item.Key))
                {
                    sortedMad.Add(item.Key, item.Value);
                }
            }

            var queryMain = new QueryExpression
            {
                EntityName = mainEntity
            };

            var relatedEntity = new RelationshipQueryCollection();

            foreach (var key in sortedMad.Values.GroupBy(x => x))
            {
                //fill measure columns
                if (key.Key.Equals(mainEntity))
                {
                    foreach (var item in sortedMad)
                    {
                        if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                            queryMain.ColumnSet.AddColumn(item.Key);
                    }
                }//add relates
                else
                {
                    if (key.Key.Equals("ddsm_project"))
                    {
                        var relationship = new Relationship { SchemaName = "ddsm_ddsm_project_ddsm_measure" };

                        var query = new QueryExpression
                        {
                            EntityName = key.Key
                        };

                        foreach (var item in sortedMad)
                        {
                            if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                                query.ColumnSet.AddColumn(item.Key);
                        }

                        relatedEntity.Add(relationship, query);
                    }
                    if (key.Key.Equals("account"))
                    {
                        var relationship = new Relationship { SchemaName = "ddsm_account_ddsm_measure" };

                        var query = new QueryExpression
                        {
                            EntityName = key.Key
                        };

                        foreach (var item in sortedMad)
                        {
                            if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                                query.ColumnSet.AddColumn(item.Key);
                        }
                        relatedEntity.Add(relationship, query);

                    }
                    if (key.Key.Equals("ddsm_site"))
                    {
                        var relationship = new Relationship { SchemaName = "ddsm_ddsm_site_ddsm_measure" };

                        var query = new QueryExpression
                        {
                            EntityName = key.Key
                        };

                        foreach (var item in sortedMad)
                        {
                            if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                                query.ColumnSet.AddColumn(item.Key);
                        }

                        relatedEntity.Add(relationship, query);
                    }
                }
#if WITH_TASKQ
                //get ProjId,POId,
                if (!queryMain.ColumnSet.Columns.Contains("ddsm_projecttomeasureid"))
                    queryMain.ColumnSet.AddColumn("ddsm_projecttomeasureid");

                if (!queryMain.ColumnSet.Columns.Contains("ddsm_programofferingsid"))
                    queryMain.ColumnSet.AddColumn("ddsm_programofferingsid");
#endif

            }

            var request = new RetrieveRequest
            {
                ColumnSet = queryMain.ColumnSet,
                Target = new EntityReference { Id = measureId, LogicalName = mainEntity },
                RelatedEntitiesQuery = relatedEntity
            };

            var response = (RetrieveResponse)OrganizationService.Execute(request);

            var result = new Dictionary<string, object>();

            foreach (var relEnt in response.Entity.RelatedEntities)
            {
                result.Add(relEnt.Value.EntityName, relEnt.Value.Entities[0].Attributes);
            }
            return response.Entity;
        }

        private UserInputObj2 GetMeasureForCalculate()
        {
            if (_options.Target?.LogicalName == "ddsm_taskqueue")
            {
                var taskData = OrganizationService.Retrieve(_options.Target.LogicalName, _options.Target.Id, new ColumnSet("ddsm_processeditems0"));
                var userInputJson = taskData?.GetAttributeValue<string>("ddsm_processeditems0");
                return JsonConvert.DeserializeObject<UserInputObj2>(userInputJson);
            }
            else if (_options.Target?.LogicalName == "ddsm_project")
            {
                return _crmHelper.GetMeasureByTarget(_options.Target);
            }
            return null;
        }

        private EntityCollection GetMeasuresInfo(List<Guid> IDs, bool isSmartMeasure = true)
        {
            // clear 
            var measureIds = IDs.Where(x => !x.Equals(Guid.Empty)).ToList();
            var expr = new QueryExpression
            {
                EntityName = mainEntity,
                ColumnSet = new ColumnSet("ddsm_madfields", "ddsm_name")
            };
            expr.LinkEntities.Add(new LinkEntity(mainEntity, "ddsm_measuretemplate", "ddsm_measureselector", "ddsm_measuretemplateid", JoinOperator.Inner)
            {
                LinkEntities = {
                    new LinkEntity("ddsm_measuretemplate","ddsm_mapping","ddsm_mappingid","ddsm_mappingid",JoinOperator.Inner)  { Columns = new ColumnSet("ddsm_jsondata")  },
                    new LinkEntity("ddsm_measuretemplate","ddsm_mapping","ddsm_qddmappingid","ddsm_mappingid",JoinOperator.Inner)  { Columns = new ColumnSet("ddsm_jsondata")  },
                }
            });
            expr.LinkEntities[0].Columns.AddColumns("ddsm_madfields", "ddsm_espsmartmeasureid", "ddsm_mappingid");

            Guid calcTypeId = _crmHelper.GetMeasureCalcType();

            if (_options.FromMeasureGrid)
            {
                if (isSmartMeasure)
                {
                    expr.Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_measureid", ConditionOperator.In, measureIds),
                            new ConditionExpression("ddsm_calculationtype", ConditionOperator.Equal, calcTypeId),
                            new ConditionExpression("ddsm_disablerecalculation", ConditionOperator.NotEqual, true),
                            //   new ConditionExpression("ddsm_recalculatemeasure", ConditionOperator.Equal, true),
                        }
                    };
                }
                else
                {
                    Log.Debug("get data for non smart measure");
                    expr.Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_measureid", ConditionOperator.In, measureIds),
                            new ConditionExpression("ddsm_calculationtype", ConditionOperator.NotEqual, calcTypeId),
                            new ConditionExpression("ddsm_disablerecalculation", ConditionOperator.NotEqual, true),
                            new ConditionExpression("ddsm_recalculatemeasure", ConditionOperator.Equal, true),
                        }
                    };
                }
            }
            else
            {
                expr.Criteria = new FilterExpression
                {
                    FilterOperator = LogicalOperator.And,
                    Conditions = { new ConditionExpression("ddsm_projecttomeasureid", ConditionOperator.In, measureIds),
                        new ConditionExpression("ddsm_calculationtype", ConditionOperator.Equal, calcTypeId),
                        new ConditionExpression("ddsm_disablerecalculation", ConditionOperator.NotEqual, true)
                    }
                };
            }
            return OrganizationService.RetrieveMultiple(expr);
        }
        #endregion
    }
}
