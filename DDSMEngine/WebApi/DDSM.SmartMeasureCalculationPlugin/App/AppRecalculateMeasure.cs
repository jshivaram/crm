﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using DDSM.SmartMeasureCalculationPlugin.Options;
//using EspCalculation.Model;
//using EspCalculation.Utils;
//using Microsoft.Xrm.Sdk;
//using Microsoft.Xrm.Sdk.Messages;
//using Microsoft.Xrm.Sdk.Query;
//using Newtonsoft.Json;

//namespace EspCalculation.App
//{
//  public  class AppRecalculateMeasure : BaseApp
//    {
//        #region Internal Fileds

//        private string _url = "";
//        // readonly List<String> _processingLog = new List<string>();
//        Dictionary<string, MappingItem> _mappingData;
//        readonly string _mainEntity = "ddsm_measure";
//        Dictionary<Guid, Guid> _processedMeasures = new Dictionary<Guid, Guid>(); // key crm ID, value esp ID
//        Guid _projectId;
//        private RecalculateOptions _options;
//        #endregion

//        //public AppRecalculateMeasure(string connectionSting, RecalculateOptions options,Action callbackAction) : base(connectionSting,"",callbackAction)
//        //{
//        //    _options = options;
//        //}

//        //public AppRecalculateMeasure(Common objCommon) : base()
//        //{
//        //}

//        public override void Run()
//        {
//            Guid measureId;

//            if (string.IsNullOrEmpty(_options.MeasureType) || !_options.MeasureType.Equals("ESP") || Guid.Empty.Equals(_options?.Measure?.Id))
//                return;

//            if (_options?.Measure!= null)
//            {
//                measureId = _options.Measure.Id;
//            }
//            else { throw new Exception("You have no measure for recalculation"); }

//            try
//            {
//                Helper.GetConfig();
//                var espConfig = new ESPConfig(Helper.Config);
//                if (Helper.IsConfigValID(espConfig))
//                {
//                    #region Get currentAuthenticationResponse
//                    if (Helper.IsTokenNeedUpdate())
//                    {
//                        AuthenticationResponse currentAuthenticationResponse;

//                        _url = Helper.GetAuthUrl();

//                        using (var serviceRequest = new WebClient())
//                        {
//                            var responseBytes = serviceRequest.DownloadString(_url);
//                            currentAuthenticationResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(responseBytes);
//                            if (currentAuthenticationResponse.ResponseHeader.StatusOk)
//                            {
//                                Helper.Token = currentAuthenticationResponse.ResponseHeader.AuthenticationTokenBytes;
//                                Helper.LastRequestReceivedData = currentAuthenticationResponse.ResponseHeader.RequestReceivedDateTime;
//                                Helper.UpdateConfig(espConfig);
//                            }
//                            else { throw new AccessViolationException("Can't get authorization token using esp login details"); }
//                        }
//                    }
//                    else
//                    {
//                        Helper.LastRequestReceivedData = (DateTime)Helper.Config.Attributes["ddsm_requestreceiveddate"];
//                        Helper.Token = Convert.FromBase64String(Helper.Config.Attributes["ddsm_esplasttoken"].ToString());
//                    }

//                    #endregion
//                    var measureData = GetMeasuresInfo(measureId);
//                    _projectId = measureData.Entities[0].GetAttributeValue<EntityReference>("ddsm_projecttomeasureid").Id;

//                    if (measureData?.Entities.Count > 0)
//                    {
//                        var calcRequest = BuildCalculationRequest(measureData);

//                        CalculationResponse resultCalculationResponse = null;

//                        _url = Helper.GetCalculatioUrl();
//                        var request = JsonConvert.SerializeObject(calcRequest);

//                        var client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } };
//                        var response = client.UploadString(_url, "POST", request);

//                        resultCalculationResponse = JsonConvert.DeserializeObject<CalculationResponse>(response);

//                        UpdateMeasures(resultCalculationResponse);
//                    }
//                    else { throw new Exception("The project does not have any smart measure for calculation"); }
//                }
//                else
//                {
//                    throw new AccessViolationException("Can't get authorization token using esp login details. Please contact to your administrator.");
//                }

//               Logger.Trace(string.Join("\n", _processingLog.OrderBy(x => x).ToArray()));
//                Result= JsonConvert.SerializeObject(new { MeasureName = "", MeasureID = "", ErrorMsg = string.Join("\n", _processingLog.OrderBy(x => x).ToArray()) });
//                Completed =true;
//            }
//            catch (Exception ex)
//            {
//                Logger.Trace("FATAL ERROR: " + ex.Message + string.Join("\n", _processingLog.OrderBy(x => x).ToArray()));
//                Result = JsonConvert.SerializeObject(new { MeasureName = "", MeasureID = "", ErrorMsg = string.Join("\n", _processingLog.OrderBy(x => x).ToArray()) });
//                Completed = false;
//            }
//        }
//#if WITH_TASKQ
//        private void ProcessCollectedData(Guid measureId)
//        {
//            var taskQueue = new TaskQueue(OrganizationService);

//            taskQueue.Create(
//                new DDSM_Task(TaskQueue.TaskEntity.ProgramInterval)
//                {
//                    ProcessedItems0 = { measureId }
//                },
//                TaskQueue.TaskEntity.Measure, dataUploader: DataUploader
//            );

//            taskQueue.Create(
//                new DDSM_Task(TaskQueue.TaskEntity.Project)
//                {
//                    ProcessedItems0 = { _projectId }
//                },
//                TaskQueue.TaskEntity.Project, dataUploader: DataUploader
//            );
//        }
//#endif

//        private CalculationRequest BuildCalculationRequest(EntityCollection measureData)
//        {
//            var aCalculationRequest = new CalculationRequest { RequestHeader = { AuthenticationTokenBytes = Helper.Token } };

//            _mappingData = JsonConvert.DeserializeObject<Dictionary<string, MappingItem>>(Helper.Config.Attributes["ddsm_espmapingdata"].ToString());

//            foreach (var measureItem in measureData.Entities)
//            {
//                var calculatorCalculationRequest = new CalculatorCalculationRequest
//                {
//                    CalculatorID = new Guid(measureItem.Attributes["ddsm_smartmeasureid"].ToString()),
//                    TargetDateTime = DateTime.UtcNow
//                };

//                //get mads from MT not from measure
//                if (!measureItem.Attributes.ContainsKey("ddsm_measuretemplate1.ddsm_madfields") &&
//                    (measureItem.Attributes["ddsm_measuretemplate1.ddsm_madfields"] as AliasedValue).Value != null)
//                    throw new Exception("Can't get ddsm_measuretemplate1.ddsm_madfields. Are you sure that measure has Measure Template or MAD Fields of Measure Template is not null ?? ");


//                var madsJson = (measureItem.Attributes["ddsm_measuretemplate1.ddsm_madfields"] as AliasedValue).Value;
//                var curentMad = JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(madsJson.ToString());
//                var data = GetMeasureData(curentMad, _mappingData, measureItem.Attributes["ddsm_measureid"].ToString());


//                // fill request from DB values
//                foreach (var mad in curentMad)
//                {
//                    var crmFiled = _mappingData.FirstOrDefault(x => x.Key.ToLower().Equals(mad.Name.ToLower()));
//                    var variableValueRequest = _crmHelper.GenerateESPWSVariableValueRequest(crmFiled, mad, data, _mainEntity);

//                    if (calculatorCalculationRequest.InputVariables.FirstOrDefault(x => x.Name.Equals(mad.Name)) == null)
//                    {
//                        calculatorCalculationRequest.InputVariables.Add(variableValueRequest);
//                    }
//                }
//                if (curentMad.Count == calculatorCalculationRequest.InputVariables.Count)
//                {
//                    aCalculationRequest.CalculatorCalculationRequestList.Add(calculatorCalculationRequest);
//                    _processedMeasures.Add(new Guid(measureItem.Attributes["ddsm_measureid"].ToString()),
//                        new Guid(measureItem.Attributes["ddsm_smartmeasureid"].ToString()));
//                }
//                else
//                {
//                    Logger.Trace("WARN: " + $"Mapping MAD are not successfully. Measure: {measureItem.Attributes["ddsm_name"]}. Current MAD count: {curentMad.Count}, mapped only: {calculatorCalculationRequest.InputVariables.Count}. Please check MAD mapping for current Smart Measure Template Or contact your administrator.");
//                }
//            }

//            if (aCalculationRequest.CalculatorCalculationRequestList.Count == 0)
//            {
//                throw new Exception("Can't build Calculation request. No one mapped Smart Measure");
//            }

//            return aCalculationRequest;
//        }

//        private Entity GetMeasureData(List<ESPWSVariableDefinitionResponse> curentMad, Dictionary<string, MappingItem> mappingData, string id)
//        {
//            var sortedMadList = (from x in curentMad
//                join y in mappingData on x.Name.ToLower() equals y.Key.ToLower()
//                select new KeyValuePair<string, string>(y.Value.AttrName.ToLower(), y.Value.Entity));

//            var sortedMad = new Dictionary<string, string>();
//            foreach (var item in sortedMadList)
//            {
//                if (!sortedMad.ContainsKey(item.Key))
//                {
//                    sortedMad.Add(item.Key, item.Value);
//                }
//            }

//            var queryMain = new QueryExpression
//            {
//                EntityName = _mainEntity
//            };

//            var relatedEntity = new RelationshipQueryCollection();

//            foreach (var key in sortedMad.Values.GroupBy(x => x))
//            {
//                //fill measure columns
//                if (key.Key.Equals(_mainEntity))
//                {
//                    foreach (var item in sortedMad)
//                    {
//                        if (item.Value == key.Key)
//                            queryMain.ColumnSet.AddColumn(item.Key);
//                    }
//                }//add relates
//                else
//                {
//                    if (key.Key.Equals("ddsm_project"))
//                    {
//                        var relationship = new Relationship { SchemaName = "ddsm_ddsm_project_ddsm_measure" };

//                        var query = new QueryExpression
//                        {
//                            EntityName = key.Key
//                        };

//                        foreach (var item in sortedMad)
//                        {
//                            if (item.Value == key.Key)
//                                query.ColumnSet.AddColumn(item.Key);
//                        }

//                        relatedEntity.Add(relationship, query);
//                    }
//                    if (key.Key.Equals("account"))
//                    {
//                        var relationship = new Relationship { SchemaName = "ddsm_account_ddsm_measure" };

//                        var query = new QueryExpression
//                        {
//                            EntityName = key.Key
//                        };

//                        foreach (var item in sortedMad)
//                        {
//                            if (item.Value == key.Key)
//                                query.ColumnSet.AddColumn(item.Key);
//                        }
//                        relatedEntity.Add(relationship, query);

//                    }
//                    if (key.Key.Equals("ddsm_site"))
//                    {
//                        var relationship = new Relationship { SchemaName = "ddsm_ddsm_site_ddsm_measure" };

//                        var query = new QueryExpression
//                        {
//                            EntityName = key.Key
//                        };

//                        foreach (var item in sortedMad)
//                        {
//                            if (item.Value == key.Key)
//                                query.ColumnSet.AddColumn(item.Key);
//                        }

//                        relatedEntity.Add(relationship, query);
//                    }
//                }
//            }

//            var request = new RetrieveRequest
//            {
//                ColumnSet = queryMain.ColumnSet,
//                Target = new EntityReference { Id = new Guid(id), LogicalName = _mainEntity },
//                RelatedEntitiesQuery = relatedEntity
//            };

//            var response = (RetrieveResponse)OrganizationService.Execute(request);

//            var result = new Dictionary<string, object>();

//            foreach (var relEnt in response.Entity.RelatedEntities)
//            {
//                result.Add(relEnt.Value.EntityName, relEnt.Value.Entities[0].Attributes);
//            }
//            return response.Entity;
//        }

//        private EntityCollection GetMeasuresInfo(Guid id)
//        {
//            var calcTypeId = _crmHelper.GetMeasureCalcType();
//            var expr = new QueryExpression
//            {
//                EntityName = _mainEntity,
//                ColumnSet = new ColumnSet("ddsm_measureid", "ddsm_smartmeasureid", "ddsm_madfields", "ddsm_name", "ddsm_projecttomeasureid"),
//                Criteria = new FilterExpression
//                {
//                    FilterOperator = LogicalOperator.And,
//                    Conditions = { new ConditionExpression("ddsm_measureid", ConditionOperator.Equal, id),
//                        new ConditionExpression("ddsm_calculationtype", ConditionOperator.Equal, calcTypeId) }
//                }
//            };

//            expr.LinkEntities.Add(new LinkEntity(_mainEntity, "ddsm_measuretemplate", "ddsm_measureselector", "ddsm_measuretemplateid", JoinOperator.Inner));
//            expr.LinkEntities[0].Columns.AddColumns("ddsm_madfields");

//            return OrganizationService.RetrieveMultiple(expr);
//        }

//        private void UpdateMeasures(CalculationResponse calcResult)
//        {
//            #region newImpl
//            var qdDmappingData = JsonConvert.DeserializeObject<Dictionary<string, MappingItem>>(Helper.Config.Attributes["ddsm_espqddmappingdata"].ToString());
//            var maDmappingData = JsonConvert.DeserializeObject<Dictionary<string, MappingItem>>(Helper.Config.Attributes["ddsm_espmapingdata"].ToString());


//            if (calcResult.ResponseHeader.StatusOk)
//            {
//                foreach (var measureCalculation in calcResult.ESPWSCalculatorCalculationList)
//                {
//                    Entity measureForUpdate;

//                    var rrr = _processedMeasures.FirstOrDefault(x => x.Value.Equals(measureCalculation.ID));
//                    measureForUpdate = new Entity(_mainEntity, rrr.Key);
//                    _processedMeasures.Remove(rrr.Key);

//                    // set mads
//                    foreach (var result in measureCalculation.ESPWSVariableCalculationList)
//                    {
//                        var mapItem = maDmappingData.FirstOrDefault(x => x.Key.ToLower().Equals(result.Name.ToLower()));

//                        if (mapItem.Value != null && mapItem.Key != null && mapItem.Value.Entity.Equals(_mainEntity))
//                        {
//                            _crmHelper.SetAttributeValue(mapItem.Value, result, measureForUpdate);
//                        }
//                    }

//                    //set qdds
//                    foreach (var result in measureCalculation.ESPWSVariableCalculationList)
//                    {
//                        var mapItem = qdDmappingData.FirstOrDefault(x => x.Key.ToLower().Equals(result.Name.ToLower()));

//                        if (mapItem.Value != null && mapItem.Key != null && mapItem.Value.Entity.Equals(_mainEntity))
//                        {
//                            _crmHelper.SetAttributeValue(mapItem.Value, result, measureForUpdate);
//                        }
//                    }

//                    measureForUpdate["ddsm_recalculationdate"] = measureForUpdate["ddsm_esptargetdate"] = DateTime.UtcNow;
//                    measureForUpdate["ddsm_espcalculationsuccessful"] = true;
//                    OrganizationService.Update(measureForUpdate);

//                    ProcessCollectedData(measureForUpdate.Id);
//                }
//            }
//            else
//            {
//                throw new Exception($"Upgrading measure impossible. Error code: {calcResult.ResponseHeader.ErrorCode}. \nError: {calcResult.GetFullESPError()}.");
//            }
//            #endregion
//        }
//    }
//}
