﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using EspCalculation.Model;
using EspCalculation.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using Serilog;

namespace EspCalculation.App
{
    public class AppSyncEspMetadata : BaseApp
    {
        #region internal fields
        private string url = "";
        private bool _ForceUpdate;
        private List<ESPWSVariableDefinitionResponse> UniqueMads = new List<ESPWSVariableDefinitionResponse>();
        private List<ESPWSVariableDefinitionResponse> UniqueQdds = new List<ESPWSVariableDefinitionResponse>();
        private int updated = 0;
        private int added = 0;
        #endregion

        public AppSyncEspMetadata(IOrganizationService orgService, Action callbackAction, EntityReference target = null) : base(orgService, callbackAction, target)
        {
        }

        public override void Run()
        {
            try
            {
                Helper.GetConfig();
                var espConfig = new ESPConfig(Helper.Config);
                if (Helper.IsConfigValID(espConfig))
                {

                    #region Get currentAuthenticationResponse

                    if (Helper.IsTokenNeedUpdate() || _ForceUpdate == true)
                    {
                        url = Helper.GetAuthUrl();

                        using (var serviceRequest = new WebClient())
                        {
                            var responseBytes = serviceRequest.DownloadString(url);
                            var currentAuthenticationResponse =
                                JsonConvert.DeserializeObject<AuthenticationResponse>(responseBytes);
                            if (currentAuthenticationResponse.ResponseHeader.StatusOk)
                            {
                                Helper.Token = currentAuthenticationResponse.ResponseHeader.AuthenticationTokenBytes;
                                Helper.LastRequestReceivedData = currentAuthenticationResponse.ResponseHeader
                                    .RequestReceivedDateTime;
                                Helper.UpdateConfig(espConfig);
                            }
                            else
                            {
                                throw new AccessViolationException(
                                    "Can't get authorization token using esp login details. Please contact to your administrator.");
                            }
                        }
                    }
                    else
                    {
                        Helper.LastRequestReceivedData =
                            (DateTime)Helper.Config.Attributes["ddsm_requestreceiveddate"];
                        Helper.Token =
                            Convert.FromBase64String(Helper.Config.Attributes["ddsm_esplasttoken"].ToString());
                    }

                    #endregion


                    #region get available calculators

                    var aAvailableCalculatorsRequest = new AvailableCalculatorsRequest
                    {
                        RequestHeader = { AuthenticationTokenBytes = Helper.Token },
                        TargetDateTime = Helper.LastRequestReceivedData
                    };
                    AvailableCalculatorsResponse measureLibrarysList = null;

                    url = Helper.GetAvailableCalculatorsUrl();

                    var requestCalculators = JsonConvert.SerializeObject(aAvailableCalculatorsRequest);

                    #endregion


                    #region get all mad defs by measure

                    var client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } };
                    var response = client.UploadString(url, "POST", requestCalculators);

                    measureLibrarysList = JsonConvert.DeserializeObject<AvailableCalculatorsResponse>(response);

                    var processedESPMEasureLibData = CreateEspMeasureLibrarys(measureLibrarysList);

                    var dataDefsRequest = GetDataDefsRequest(measureLibrarysList, Helper.Token);

                    //call calculator datadefinitions
                    CalculatorDataDefinitionsResponse result = null;

                    url = Helper.GetCalculatiorDataDefinitionsUrl();
                    var request = JsonConvert.SerializeObject(dataDefsRequest);

                    client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } };
                    response = client.UploadString(url, "POST", request);

                    result = JsonConvert.DeserializeObject<CalculatorDataDefinitionsResponse>(response);

                    #endregion

                    ProcessSmartMeasureData(result, processedESPMEasureLibData);

                    //Generate unique MAD and QDD
                    UpdateUniqueDatas();

                    Completed = true;
                    Result = JsonConvert.SerializeObject(
                        new { MeasureName = "", MeasureID = "", ErrorMsg = $"Updated: {updated}, Created: {added}" });
                }
                else
                {
                    throw new Exception("ESP connection details is not configured. Please fill it before continue.");
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "Error");
            }
            finally
            {
                if (Callback != null)
                {
                    Log.Debug("_callback in not null. In Execute callback");
                    Callback();
                }
            }


        }

        #region Internal methods

        private void UpdateUniqueDatas()
        {
            UniqueMads = UniqueMads.GroupBy(p => p.Name.ToUpper()).Select(g => g.First()).OrderBy(x => x.Name).ToList();
            UniqueQdds = UniqueQdds.GroupBy(p => p.Name.ToUpper()).Select(g => g.First()).OrderBy(x => x.Name).ToList();
            UniqueQdds = UniqueQdds.Except(UniqueMads, new QDDItemNameComparer()).ToList();

            if (!string.IsNullOrEmpty(Helper.adminDataID))
            {
                var newEntity = new Entity("ddsm_admindata", new Guid(Helper.adminDataID))
                {
                    Attributes =
                    {
                        ["ddsm_espuniquemad"] = JsonConvert.SerializeObject(UniqueMads),
                        ["ddsm_espuniqueqdd"] = JsonConvert.SerializeObject(UniqueQdds)
                    }
                };
                OrganizationService.Update(newEntity);
            }
        }

        private Dictionary<Guid, Guid> CreateEspMeasureLibrarys(AvailableCalculatorsResponse measureLibrarysList)
        {
            var crmMeasureLibID = new Guid();

            var result = new Dictionary<Guid, Guid>();
            if (measureLibrarysList != null && measureLibrarysList.CalculatorLibraryResponseList.Count > 0)
            {
                foreach (var lib in measureLibrarysList.CalculatorLibraryResponseList)
                {
                    var expr = new QueryExpression
                    {
                        EntityName = "ddsm_espmeasurelibrary",
                        ColumnSet = new ColumnSet("ddsm_espmeasurelibraryid"),
                        Criteria = new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions = { new ConditionExpression("ddsm_espmeasurelibid", ConditionOperator.Equal, lib.ID.ToString()) }
                        }
                    };

                    // var query = new FetchExpression(expr);
                    var measureLib = new Entity("ddsm_espmeasurelibrary");
                    var crmMt = OrganizationService.RetrieveMultiple(expr);

                    //update if found
                    if (crmMt != null && crmMt.Entities.Count > 0)
                    {
                        crmMeasureLibID = crmMt.Entities[0].Id;
                        measureLib = new Entity("ddsm_espmeasurelibrary", crmMeasureLibID)
                        {
                            ["ddsm_espmeasurelibid"] = lib.ID.ToString(),
                            ["ddsm_name"] = lib.Name,
                            ["ddsm_organizationname"] = lib.OrganizationName
                        };
                        OrganizationService.Update(measureLib);
                    }
                    else
                    {
                        measureLib = new Entity("ddsm_espmeasurelibrary")
                        {
                            ["ddsm_espmeasurelibid"] = lib.ID.ToString(),
                            ["ddsm_name"] = lib.Name,
                            ["ddsm_organizationname"] = lib.OrganizationName
                        };

                        crmMeasureLibID = OrganizationService.Create(measureLib);
                    }
                    foreach (var measure in lib.CalculatorList)
                    {
                        result.Add(measure.ID, crmMeasureLibID);
                    }
                }
            }
            else
            {
                throw new Exception("Can't create measure librarys. Receiver Measure Librarys list is empty.");
            }
            return result;
        }

        /// <summary>
        /// Return JSON Dictionary, when key = Measure Name, Value = list of MAD fields
        /// </summary>
        /// <returns></returns>
        private CalculatorDataDefinitionsRequest GetDataDefsRequest(AvailableCalculatorsResponse response, byte[] token)
        {
            var measureLibrarysList = response.CalculatorLibraryResponseList; //measureLibrarys
            var request = new CalculatorDataDefinitionsRequest
            {
                RequestHeader = { AuthenticationTokenBytes = token }
            };

            foreach (var measureLib in measureLibrarysList)
            {
                foreach (var measure in measureLib.CalculatorList)
                {
                    request.CalculatorDefinitionRequestList.Add(new CalculatorDefinitionRequest { CalculatorID = measure.ID, TargetDateTime = Helper.LastRequestReceivedData });
                }
            }
            return request;
        }

        private void ProcessSmartMeasureData(CalculatorDataDefinitionsResponse smartMeasureData, Dictionary<Guid, Guid> libData)
        {
            if (smartMeasureData == null)
            {
                throw new Exception("Nothing to do! in ProcessSmartMeasureData list is null((");
            }
            foreach (var measure in smartMeasureData.ESPWSCalculatorDataDefinitionResponseList)
            {
                var expr = new QueryExpression
                {
                    EntityName = "ddsm_smartmeasureversion",
                    ColumnSet = new ColumnSet(allColumns: true),
                    Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_calculationid", ConditionOperator.Equal, measure.ID.ToString()) }
                    },

                };
                expr.AddOrder("ddsm_startdatetime", OrderType.Descending);

                var crmMt = OrganizationService.RetrieveMultiple(expr);

                //update if found
                Entity newEntity;
                if (crmMt != null && crmMt.Entities.Count > 0)
                {
                    var lastVer = crmMt.Entities.FirstOrDefault(x => ((DateTime)x.Attributes["ddsm_startdatetime"]).ToString("HH:mm:ss tt").Equals(measure.StartDateTime.ToString("HH:mm:ss tt")));

                    if (lastVer == null)
                    {
                        newEntity = new Entity("ddsm_smartmeasureversion")
                        {
                            Attributes =
                            {
                                ["ddsm_name"] = measure.Name,
                                ["ddsm_madfields"] =
                                JsonConvert.SerializeObject(
                                    measure.ESPVariableDefinitionList.OrderBy(v => v.DisplayOrderRank)),
                                ["ddsm_calculationid"] = measure.ID.ToString(),
                                ["ddsm_startdatetime"] = measure.StartDateTime,
                                ["ddsm_espsmartmeasurelibraryid"] =
                                new EntityReference("ddsm_espmeasurelibrary", libData[measure.ID])
                            }
                        };
                        Guid clonedMT;
                        Entity clonedMeasureTemplate = null;
                        //if (crmMt.Entities[0].Attributes.ContainsKey("ddsm_measuretemplateid"))
                        //{
                        //  //  clonedMT = _crmHelper.CloneMTRecord((crmMt.Entities[0].Attributes["ddsm_measuretemplateid"] as EntityReference).Id);
                        //    newEntity.Attributes["ddsm_measuretemplateid"] = new EntityReference("ddsm_measuretemplate", clonedMT);
                        //    if (Guid.Empty != clonedMT)
                        //        clonedMeasureTemplate = new Entity("ddsm_measuretemplate", clonedMT);
                        //}
                        //set current
                        newEntity.Attributes["ddsm_iscurrent"] = true;

                        var newVersion = OrganizationService.Create(newEntity);
                        if (Guid.Empty != newVersion && clonedMeasureTemplate != null)
                        {
                            clonedMeasureTemplate.Attributes.Add(new KeyValuePair<string, object>("ddsm_smartmeasureversionid", new EntityReference("ddsm_smartmeasureversion", newVersion)));
                            OrganizationService.Update(clonedMeasureTemplate);
                            foreach (var item in crmMt.Entities)
                            {
                                if (item.Attributes.ContainsKey("ddsm_measuretemplateid"))
                                {
                                    var mt = OrganizationService.Retrieve("ddsm_measuretemplate", (item.Attributes["ddsm_measuretemplateid"] as EntityReference).Id, new ColumnSet("ddsm_name"));

                                    OrganizationService.Update(new Entity("ddsm_measuretemplate", (item.Attributes["ddsm_measuretemplateid"] as EntityReference).Id)
                                    {
                                        Attributes = new AttributeCollection {
                                            new KeyValuePair<string, object>("ddsm_name", mt["ddsm_name"].ToString().Replace(" (current)", ""))
                                        }
                                    });
                                }
                            }
                        }
                        //update prev version
                        OrganizationService.Update(new Entity("ddsm_smartmeasureversion", crmMt.Entities[0].Id)
                        {
                            Attributes = new AttributeCollection() {
                                new KeyValuePair<string, object>("ddsm_newversionid",new EntityReference("ddsm_smartmeasureversion", newVersion)),
                            }
                        });
                        updated++;
                    }
                }
                //create if not found
                else
                {
                    newEntity = new Entity("ddsm_smartmeasureversion")
                    {
                        Attributes = {
                            ["ddsm_name"] = measure.Name,
                            ["ddsm_madfields"] = JsonConvert.SerializeObject(measure.ESPVariableDefinitionList.OrderBy(v => v.DisplayOrderRank)),
                            ["ddsm_calculationid"] = measure.ID.ToString(),
                            ["ddsm_startdatetime"] = measure.StartDateTime
                        }
                    };

                    newEntity.Attributes["ddsm_espsmartmeasurelibraryid"] = new EntityReference("ddsm_espmeasurelibrary", libData[measure.ID]);


                    //newEntity.Attributes["ddsm_measuretemplateid"] = crmMt.Entities[0].Attributes["ddsm_measuretemplateid"];
                    OrganizationService.Create(newEntity);
                    added++;
                }
                UniqueMads.AddRange(measure.ESPVariableDefinitionList);
                UniqueQdds.AddRange(measure.ESPQddDefinitionList);
            }
        }
        #endregion
    }


}
