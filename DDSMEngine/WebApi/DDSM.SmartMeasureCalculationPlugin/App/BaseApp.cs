﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EspCalculation.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Serilog;

namespace EspCalculation.App
{
    public class Tracer : ITracingService
    {
        public List<String> Logs { get; set; }

        public Tracer()
        {
            Logs = new List<string>();
        }

        public void Trace(string logMmsg)
        {
            Logs?.Add(logMmsg);
        }

        public void Trace(string format, params object[] args)
        {
            Logs?.Add(string.Format(format, args));
        }
    }

    public class BaseApp : IBaseApp
    {
        #region Base props
        public string ConnctionString { get; set; }
        public bool Completed { get; set; }
        public string Result { get; set; }
        public string JSON { get; set; }
        public IOrganizationService OrganizationService { get; set; }
        internal Helper Helper;
        internal Action Callback;
        internal Tracer Logger;
        internal EntityReference DataUploader { get; set; }
        internal virtual TaskQueue.StatusFileDataUploading CurrentOperationStatus { get; set; }
        #endregion

        internal List<string> _processingLog;
        internal CrmHelper _crmHelper;

        public BaseApp(IOrganizationService orgService, Action callbackAction, EntityReference target = null)
        {
            OrganizationService = orgService;
            Init(callbackAction);
            DataUploader = GetDataUploaderRef(target);
        }

        void Init(Action callbackAction)
        {
            Callback = callbackAction;
            Logger = new Tracer();
            Helper = new Helper(OrganizationService);
            _crmHelper = new CrmHelper(OrganizationService);
        }

        //public BaseApp()
        //{
        //    OrganizationService = _objCommon?.GetOrgService();
        //    Init(null);
        //}

        private EntityReference GetDataUploaderRef(EntityReference target)
        {
            var result = new EntityReference();
            try
            {
                if (target?.LogicalName != "ddsm_taskqueue")
                    return new EntityReference();

                var taskQueue = OrganizationService.Retrieve(target.LogicalName, target.Id, new ColumnSet("ddsm_datauploader"));
                result = taskQueue?.GetAttributeValue<EntityReference>("ddsm_datauploader");
            }
            catch (Exception ex)
            {
                Log.Debug("Error on GetDataUploaderRef() " + ex.Message);
                return result;
            }
            return result;
        }

        internal void UpdateDACurrentOperationStatus(TaskQueue.StatusFileDataUploading status)
        {
            if (DataUploader == null)
            {
                Log.Debug("DataUploader is null");
                return;
            }
            Log.Debug("in UpdateDACurrentOperationStatus ");
            //  if (CurrentOperationStatus != null)
            {
                Log.Debug("Set Current Operation Status: " + CurrentOperationStatus.ToString() + " To DataUploader:" + DataUploader.Id);
                try
                {
                    if (DataUploader?.Id != Guid.Empty && CurrentOperationStatus != 0)
                    {
                        var prevDaRecord = OrganizationService.Retrieve(DataUploader.LogicalName, DataUploader.Id, new ColumnSet("ddsm_logdata"));
                        var prevLog = prevDaRecord.GetAttributeValue<String>("ddsm_logdata");

                        var daRecord = new Entity(DataUploader.LogicalName, DataUploader.Id)
                        {
                            Attributes = new AttributeCollection()
                            {
                                new KeyValuePair<string, object>("ddsm_statusfiledatauploading",new OptionSetValue((int) status)),
                            }
                        };
                        if (!string.IsNullOrEmpty(prevLog) && Logger?.Logs?.Count > 0)
                        {
                            prevLog += string.Join(Environment.NewLine, Logger.Logs);
                            daRecord.Attributes.Add(new KeyValuePair<string, object>("ddsm_logdata", prevLog));
                        }

                        OrganizationService.Update(daRecord);
                    }
                }
                catch (Exception e)
                {
                    Log.Error(e,"Error");
                    throw;
                }
            }
        }

        public virtual void Run()
        {
            Console.WriteLine("In BaseApp Run");
        }

        public Task RunAsync()
        {
            UpdateDACurrentOperationStatus(CurrentOperationStatus);
            return Task.Run(() => Run());
        }
    }
}
