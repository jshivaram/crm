﻿using System.Threading.Tasks;

namespace EspCalculation.App
{
    public interface IBaseApp
    {
        string ConnctionString { get; set; }
        bool Completed { get; set; }
        string Result { get; set; }
        string JSON { get; set; }
        void Run();
        Task RunAsync();
    }
}