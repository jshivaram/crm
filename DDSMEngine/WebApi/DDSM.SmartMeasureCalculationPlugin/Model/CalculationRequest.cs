﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EspCalculation.Model
{
    [DataContract]
    public class CalculationRequest
    {
        [DataMember]
        public RequestHeader RequestHeader { get; set; }

        [DataMember]
        public List<CalculatorCalculationRequest> CalculatorCalculationRequestList { get; set; }

        public CalculationRequest()
        {
            RequestHeader = new RequestHeader();
            CalculatorCalculationRequestList = new List<CalculatorCalculationRequest>();
        }
    }

    [DataContract]
    public class CalculatorCalculationRequest
    {
        [DataMember]
        public Guid CalculatorID { get; set; }

        [DataMember]
        public DateTime TargetDateTime { get; set; }

        [DataMember]
        public List<ESPWSVariableValueRequest> InputVariables { get; set; }

        public CalculatorCalculationRequest()
        {
            InputVariables = new List<ESPWSVariableValueRequest>();
        }
    }

    [DataContract]
    public class ESPWSVariableValueRequest
    {
        [DataMember]
        public ESPWSVariableType ESPWSVariableType { get; set; }

        [DataMember]
        public Guid ID { get; set; }

        [DataMember]
        public Guid? QDDID { get; set; }

        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Must be null if type is Double or DoubleList.
        /// </summary>
        [DataMember]
        public string StringValue { get; set; }

        /// <summary>
        /// Must be null if type is StringValue or StringList.
        /// </summary>
        [DataMember]
        public double? DoubleValue { get; set; }
    }
}