﻿using System;
using Microsoft.Xrm.Sdk;

namespace EspCalculation.Model
{
    public class ESPConfig
    {
        public ESPConfig(Entity config)
        {
            if (config != null)
            {
                Url = config.GetAttributeValue<string>("ddsm_espurl");
                Login = config.GetAttributeValue<string>("ddsm_esplogin");
                Pass = config.GetAttributeValue<string>("ddsm_esppassword");
                DataPortion = config.GetAttributeValue<decimal>("ddsm_espdataportion");
                LastToken = Convert.FromBase64String(config.GetAttributeValue<string>("ddsm_esplasttoken"));
                RequestReceivedDate = config.GetAttributeValue<DateTime>("ddsm_requestreceiveddate");
                EspMapingData = config.GetAttributeValue<string>("ddsm_espmapingdata");
                EspUniqueQdd = config.GetAttributeValue<string>("ddsm_espuniqueqdd");
                EspQddMappingData = config.GetAttributeValue<string>("ddsm_espqddmappingdata");
                EspRemoteCalculation = config.GetAttributeValue<bool>("ddsm_espremotecalculation");
                EspRemoteCalculationApiUrl = config.GetAttributeValue<string>("ddsm_espremotecalculationapiurl");
                ConfigId = config.Id;
            }
        }

        public bool IsTokenNeedUpdate()
        {
            try
            {
                if (LastToken?.Length == 0 || RequestReceivedDate == DateTime.MinValue || (DateTime.UtcNow - RequestReceivedDate).TotalHours >= 24)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public string Url { get; set; }
        public string Login { get; set; }
        public string Pass { get; set; }
        public decimal DataPortion { get; set; }
        public byte[] LastToken { get; set; }
        public string DefaultLibrary { get; set; }
        public bool AllMadAreRequired { get; set; }
        public DateTime RequestReceivedDate { get; set; }
        // mapping data
        public string EspMapingData { get; set; }
        public string EspUniqueQdd { get; set; }
        public string EspQddMappingData { get; set; }
        //remote calculation
        public bool EspRemoteCalculation { get; set; }
        public string EspRemoteCalculationApiUrl { get; set; }
        public Guid ConfigId { get; set; }
    }


}
