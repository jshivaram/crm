﻿namespace EspCalculation.Model
{
    public enum ESPWSVariableType
    {
        Double,
        Text,
        DoubleList,
        TextList
    }
}