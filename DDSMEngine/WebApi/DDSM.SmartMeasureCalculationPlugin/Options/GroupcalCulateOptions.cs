﻿using System;
using EspCalculation.Model;
using Microsoft.Xrm.Sdk;

namespace EspCalculation.Options
{
    public class GroupCalculateOptions
    {
        public EntityReference Project { get; set; }
        public bool FromMeasureGrid { get; set; }
        public string UserInput { get; set; }
        public Guid UserId { get; set; }
        public ESPConfig Config { get; set; }
        public EntityReference Target { get; set; }

        public GroupCalculateOptions()
        {
            Target = new EntityReference();
        }
    }

    public class CalculateOptions
    {
        public Guid MeasureId { get; set; }
        public string UserInput { get; set; }
        public Guid UserId { get; set; }
        public ESPConfig Config { get; set; }
    }
}