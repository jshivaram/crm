﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using EspCalculation.Model;

namespace EspCalculation.Utils
{
    public static class Extentions
    {
        public static string GetFullESPError(this CalculationResponse response)
        {
            var withError = new List<string>();
            if (!response.ResponseHeader.StatusOk)
            {
                withError.Add(response.ResponseHeader.ErrorMessage);
                foreach (var calc in response.ESPWSCalculatorCalculationList.Where(x => x.ResponseHeader.StatusOk == false))
                {
                    withError.Add(response.ESPWSCalculatorCalculationList[0].ResponseHeader.ErrorMessage);
                    //var calc1 = calcRequest.CalculatorCalculationRequestList.FirstOrDefault(x => x.CalculatorID.Equals(calc.ID));
                    //if (calc1 != null)
                    //{
                    //    withError.Add(calc1.CalculatorID + calc1.TargetDateTime);
                    //}

                    //withError.Add(calc.Name + " " + calc.Description + " " + calc.ID + " " + calc.StartDateTime);
                    foreach (var madVariable in calc.ESPWSVariableCalculationList.Where(x => x.ResponseHeader.StatusOk == false))
                    {
                        withError.Add($"Measure name: {calc.Name}, MAD field: {madVariable.Name}. Error: {madVariable.ResponseHeader.ErrorMessage}, MAD Json:{JsonConvert.SerializeObject(madVariable)}");
                    }
                }
            }
            return string.Join("\n", withError);
        }

        public static EntityReference GetTargetData(Common objDdsm)
        {
            objDdsm.TracingService.Trace("in GetTargetData:");
            EntityReference target = new EntityReference();
            if (objDdsm.Context.InputParameters.ContainsKey("Target"))
            {
                objDdsm.TracingService.Trace("objDdsm.Context.InputParameters.ContainsKey(Target)");
                objDdsm.TracingService.Trace("in GetTargetData:");
                if (objDdsm.Context.InputParameters["Target"] is Entity)
                {
                    target = ((Entity)objDdsm.Context.InputParameters["Target"]).ToEntityReference();
                }
                else if (objDdsm.Context.InputParameters["Target"] is EntityReference)
                {
                    target = (EntityReference)objDdsm.Context.InputParameters["Target"];
                }
            }

            objDdsm.TracingService.Trace("Target LogicalName: " + target.LogicalName + " Id: " + target.Id.ToString());
            return target;
        }
    }
}
