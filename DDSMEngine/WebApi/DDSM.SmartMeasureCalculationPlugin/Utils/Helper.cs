﻿using System;
using System.Collections.Generic;
using EspCalculation.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace EspCalculation.Utils
{
    public class Helper
    {
        IOrganizationService _service;
        public Entity Config { get; set; }
        public Dictionary<string, object> Credencial { get; set; }
        public Helper(IOrganizationService service)
        {
            _service = service;
        }

        public byte[] Token { get; set; }
        public DateTime LastRequestReceivedData;

        public string adminDataID { get; private set; } //"F56C3086-9AF2-E411-80EC-FC15B4284D68";

        public void GetConfig()
        {

            if (_service == null)
                throw new Exception("Error: Service is null ((");


            try
            {
                var expr = new QueryExpression
                {
                    EntityName = "ddsm_admindata",
                    //ColumnSet = new ColumnSet(true),
                    Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, "Admin Data") }
                    }
                };

                var adminData = _service.RetrieveMultiple(expr);


                if (adminData != null && adminData.Entities?.Count >= 1)
                {
                    adminDataID = adminData.Entities[0].Attributes["ddsm_admindataid"].ToString();
                }
                else
                {
                    adminDataID = "F56C3086-9AF2-E411-80EC-FC15B4284D68";
                }

                Config = _service.Retrieve("ddsm_admindata", Guid.Parse(adminDataID),
                        new ColumnSet("ddsm_espurl", "ddsm_esplogin", "ddsm_esppassword", "ddsm_espdataportion", "ddsm_esplasttoken", "ddsm_requestreceiveddate", "ddsm_espmapingdata", "ddsm_espuniqueqdd", "ddsm_espqddmappingdata"));
            }
            catch (Exception ex)
            {
                throw new Exception("error in getting Config: " + ex.Message + ex.Data);
            }
        }

        /// <summary>
        /// Update last token and last request dateTime  
        /// </summary>
        public void UpdateConfig(ESPConfig config)
        {
            if (LastRequestReceivedData != DateTime.MinValue) ///&& LastRequestReceivedData != (DateTime)Config.Attributes["ddsm_requestreceiveddate"]
            {
                var newEntity = new Entity("ddsm_admindata", config.ConfigId); //,new Guid(adminDataID)
                newEntity["ddsm_requestreceiveddate"] = LastRequestReceivedData;
                newEntity["ddsm_esplasttoken"] = Convert.ToBase64String(Token);
                _service.Update(newEntity);
            }
        }

        public bool IsTokenNeedUpdate()
        {
            try
            {
                if ((!Config.Attributes.ContainsKey("ddsm_esplasttoken") || !Config.Attributes.ContainsKey("ddsm_requestreceiveddate")) ||
                                   (DateTime.UtcNow - ((DateTime)Config.Attributes["ddsm_requestreceiveddate"])).TotalHours >= 24)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool IsConfigValID(ESPConfig Config)
        {
            if (Config != null && !string.IsNullOrEmpty(Config.Url) && !string.IsNullOrEmpty(Config.Login) && !string.IsNullOrEmpty(Config.Pass) && Config.DataPortion > 0)
            {
                Credencial = new Dictionary<string, object>
                        {
                            {"URI", Config.Url},
                            {"UserName", Config.Login},
                            {"Password", Config.Pass},
                            {"Portion", Config.DataPortion}
                        };

                return true;
            }
            return false;
        }


        public string GetAuthUrl()
        {
            return string.Format(Credencial["URI"]
                                              + "/API/Authentication"
                                              + "/" + Credencial["UserName"]
                                              + "/" + Credencial["Password"]);
        }

        public string GetAvailableCalculatorsUrl()
        {
            return string.Format(Credencial["URI"] + "/API/AvailableCalculators/");
        }

        public string GetCalculatiorDataDefinitionsUrl()
        {
            return string.Format(Credencial["URI"] + "/API/CalculatorDataDefinitions/");
        }
        public string GetCalculatioUrl()
        {
            return string.Format(Credencial["URI"] + "/API/Calculation/");
        }


    }
}
