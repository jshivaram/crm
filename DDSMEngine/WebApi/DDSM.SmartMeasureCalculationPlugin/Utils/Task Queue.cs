﻿using System;
using System.Collections.Generic;
using System.Linq;
using EspCalculation.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;

namespace EspCalculation.Utils
{
    public class TaskQueue
    {

        public enum StatusFileDataUploading
        {
            notUploaded = 962080000,
            fileUploadCompleted = 962080001,
            fileUploadFailed = 962080002,
            parsingFile = 962080003,
            parsingFileCompleted = 962080004,
            parsingFileFailed = 962080005,
            contactsProcessing = 962080006,
            accountsProcessing = 962080007,
            sitesProcessing = 962080008,
            projectGroupsProcessing = 962080009,
            projectsProcessing = 962080010,
            measuresProcessing = 962080011,
            projectReportingsProcessing = 962080012,
            measureReportingsProcessing = 962080013,
            relationshipsProcessing = 962080014,
            recordsUploadCompleted = 962080015,
            recalculationStarted = 962080016,
            ESPRecalculation = 962080017,
            recalculationBusinessRules = 962080018,
            rollupsRecalculation = 962080019,
            intervalsRecalculation = 962080020,
            importSuccessfully = 962080021,
            importFailed = 962080022,
        }



        public enum TaskEntity
        {
            Measure = 962080000,
            Project = 962080001,
            ProgramInterval = 962080002,
            ProgramOffering = 962080003,
            Portfolio = 962080004,
            Financial = 962080005,
            ProjectGroupFinancial = 962080006
        }

        #region internal fields
        IOrganizationService _service;
        static string MainEntity = "ddsm_taskqueue";
        #endregion


        public TaskQueue(IOrganizationService service)
        {
            _service = service;
        }

        #region Public Methods
        public Guid Create(DDSM_Task task, TaskEntity recordType, int startIndex = 0, EntityReference dataUploader= null)
        {
            var newTask = new Entity(MainEntity);
            #region Fill new Task
            newTask["ddsm_name"] = task.GetTaskName();
            newTask["ddsm_taskentity"] = new OptionSetValue((int)task.TaskEntity);
            newTask["ddsm_entityrecordtype"] = new OptionSetValue((int)recordType);

            if (dataUploader != null && !Guid.Empty.Equals(dataUploader?.Id))
                newTask["ddsm_datauploader"] = dataUploader;

            // if count of processed items more than 10 000 will be created two Records
            if (task.ProcessedItems0.Count > 10000)
            {
                newTask["ddsm_processeditems0"] = JsonConvert.SerializeObject(new UserInputObj2 { SmartMeasures = task.ProcessedItems0.GetPortion(startIndex).ToList() });
                Create(task, recordType, startIndex + 1);
            }
            else
            {
                newTask["ddsm_processeditems0"] = JsonConvert.SerializeObject(new UserInputObj2 { SmartMeasures = task.ProcessedItems0 });
            }
            #endregion
            var newTaskGuid = _service.Create(newTask);
            return newTaskGuid;
        }

        public void Update(DDSM_Task task)
        {
            var newTask = new Entity("ddsm_taskqueue", task.Id);
            #region Fill new Task
            newTask["ddsm_name"] = task.Name;
            newTask["ddsm_taskentity"] = new OptionSetValue((int)task.TaskEntity);
            #endregion
            _service.Update(newTask);
        }

        public Entity Get(Guid Id)
        {
            return _service.Retrieve(MainEntity, Id, new ColumnSet(allColumns: true));
        }
        #endregion

    }
    public class DDSM_Task
    {
        public DDSM_Task(TaskQueue.TaskEntity taskEntity)
        {
            TaskEntity = taskEntity;
            ProcessedItems0 = new List<Guid>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<Guid> ProcessedItems0 { get; set; }
        public TaskQueue.TaskEntity TaskEntity { get; set; }

        public string GetTaskName(int set = 0)
        {
            return Enum.GetName(typeof(TaskQueue.TaskEntity), TaskEntity) + " Task Queue - " + "/" + set + DateTime.UtcNow;
        }
    }

    public static class TaskHelper
    {
        public static IList<Guid> GetPortion(this IList<Guid> mylist, int page, int countRecOnPage = 10000)
        {
            return mylist.Skip(page * (page - 1)).Take(countRecOnPage).ToList();
        }
    }

}

