﻿using Microsoft.Crm.Sdk.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EspCalculation.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using Serilog;

namespace EspCalculation.Utils
{
    public class CrmHelper
    {
        readonly IOrganizationService service;
        public CrmHelper(IOrganizationService _service)
        {
            service = _service;
        }


        public void SetAttributeValue(MappingItem mapItem, ESPWSResponseVariableValue madResult, Entity newMeasure)
        {

            var name = mapItem.AttrName.ToLower();
            if (string.IsNullOrEmpty(name))
                throw new Exception("in SetAttributeValue() error: mapItem.AttrName is empty");

            var decVal = new decimal();
            if (null != madResult.Value && null != mapItem.FieldType)
                switch (mapItem.FieldType)
                {
                    case "Picklist":
                        var value = GetOptionsSetValueForLabel(mapItem.Entity, mapItem.AttrName, madResult.Value.ToString());
                        newMeasure.Attributes[name] = new OptionSetValue(value);
                        break;
                    case "Decimal":
                        newMeasure.Attributes[name.ToLower()] = decimal.Parse(madResult.Value.ToString());
                        break;
                    case "Double":
                        newMeasure.Attributes[name.ToLower()] = double.Parse(madResult.Value.ToString());
                        break;
                    case "Money":
                        newMeasure.Attributes[name.ToLower()] = new Money(decimal.Parse(madResult.Value.ToString()));
                        break;
                    case "DateTime":
                    case "String":
                    default:
                        newMeasure.Attributes.Add(name, madResult.Value);
                        break;
                }
        }

        public void SetMSDAttributeValue(MSDField mapItem, string msdValue, Entity newMeasure)
        {
            var name = mapItem?.AttrName?.ToLower();
            if (!string.IsNullOrEmpty(msdValue))
                switch (mapItem.FieldType)
                {
                    case "Picklist":
                        newMeasure.Attributes[name] = new OptionSetValue(int.Parse(msdValue));
                        break;
                    case "Decimal":
                        newMeasure.Attributes[name] = decimal.Parse(msdValue);
                        break;
                    case "Double":
                        newMeasure.Attributes[name] = double.Parse(msdValue);
                        break;
                    case "Money":
                        newMeasure.Attributes[name.ToLower()] = new Money(decimal.Parse(msdValue));
                        break;
                    case "Lookup":
                        var lookup = msdValue.Split('|');
                        if (lookup.Length >= 2 && !string.IsNullOrEmpty(lookup[0]) && !string.IsNullOrEmpty(lookup[1]))
                            newMeasure.Attributes.Add(name, new EntityReference(lookup[0], Guid.Parse(lookup[1])));
                        break;
                    case "DateTime":
                    case "String":
                    default:
                        newMeasure.Attributes.Add(name, msdValue);
                        break;
                }
        }

        public void SetMADAttributeValue(MappingItem mapItem, string madValue, Entity newMeasure)
        {
            var name = mapItem.AttrName.ToLower();
            var decVal = new decimal();
            if (!string.IsNullOrEmpty(madValue))
                switch (mapItem.FieldType)
                {
                    case "Picklist":
                        newMeasure[name] = new OptionSetValue(int.Parse(madValue));
                        break;
                    case "Decimal":
                        newMeasure[name] = decimal.Parse(madValue);
                        break;
                    case "Double":
                        newMeasure[name] = double.Parse(madValue);
                        break;
                    case "Money":
                        newMeasure[name] = new Money(decimal.Parse(madValue));
                        break;
                    case "Lookup":
                        var lookup = madValue.Split('|');
                        if (lookup.Length >= 2 && !string.IsNullOrEmpty(lookup[0]) && !string.IsNullOrEmpty(lookup[1]))
                            newMeasure[name] = new EntityReference(lookup[0], Guid.Parse(lookup[1]));
                        break;
                    case "DateTime":
                    case "String":
                    default:
                        newMeasure[name] = madValue;
                        break;
                }
        }

        public Entity GetConfig()
        {

            if (service == null)
                throw new Exception("Error: Service is null ((");
            try
            {
                var expr = new QueryExpression
                {
                    EntityName = "ddsm_admindata",
                    ColumnSet = new ColumnSet("ddsm_espurl", "ddsm_esplogin", "ddsm_esppassword", "ddsm_espdataportion", "ddsm_esplasttoken", "ddsm_requestreceiveddate", "ddsm_espmapingdata", "ddsm_espuniqueqdd", "ddsm_espuniquemad", "ddsm_espqddmappingdata", "ddsm_espremotecalculation", "ddsm_espremotecalculationapiurl"),
                    Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, "Admin Data") }
                    }
                };

                var adminData = service.RetrieveMultiple(expr);


                if (adminData != null && adminData.Entities?.Count >= 1)
                {

                    return adminData.Entities[0];
                }
                else return null;
            }
            catch (Exception ex)
            {
                throw new Exception("error in getting Config: " + ex.Message + ex.Data);
            }
        }

        public Guid GetMeasureCalcType(string name = "ESP")
        {
            try
            {
                var expr = new QueryExpression
                {
                    EntityName = "ddsm_measurecalculationtemplate",
                    ColumnSet = new ColumnSet("ddsm_name"),
                    Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, name) }
                    }
                };
                var result = service.RetrieveMultiple(expr);
                if (result.Entities.Count > 0)
                {
                    return result.Entities[0].Id;
                }
                throw new Exception("Calc type record with name " + name + " not found");
            }
            catch (Exception ex)
            {
                return Guid.Empty;
            }
        }

        public Entity GetDataRelationMapping(string targetEntity, string sourceEntity, Guid sourceGuid)
        {
            InitializeFromRequest initialize = new InitializeFromRequest();
            initialize.TargetEntityName = targetEntity;
            initialize.EntityMoniker = new EntityReference(sourceEntity, sourceGuid);
            initialize.TargetFieldType = TargetFieldType.All;
            InitializeFromResponse initialized = (InitializeFromResponse)service.Execute(initialize);
            return initialized.Entity;
        }

        public ESPWSVariableValueRequest GenerateESPWSVariableValueRequest(KeyValuePair<string, MappingItem> crmFiled, ESPWSVariableDefinitionResponse mad, Entity data, string MainEntity)
        {
            var variableValueRequest = new ESPWSVariableValueRequest
            {
                ESPWSVariableType = mad.ESPWSVariableType
            };

            //main entity
            if (!crmFiled.Equals(new KeyValuePair<string, MappingItem>()) && crmFiled.Value.Entity.Equals(MainEntity))
            {
                var crmFiledName = crmFiled.Value.AttrName.ToLower();
                switch (mad.ESPWSVariableType)
                {
                    case ESPWSVariableType.Double:
                        if (data.Attributes.ContainsKey(crmFiledName))
                        {
                            var filedValue = data[crmFiledName];
                            if (filedValue is Money)
                            {
                                variableValueRequest.DoubleValue = Double.Parse((filedValue as Money).Value.ToString(CultureInfo.InvariantCulture));
                            }
                            else
                            {
                                variableValueRequest.DoubleValue = Double.Parse(filedValue?.ToString());
                            }
                        }
                        break;
                    case ESPWSVariableType.Text:
                        if (data.Attributes.ContainsKey(crmFiledName))
                            variableValueRequest.StringValue = data.GetAttributeValue<string>(crmFiledName);
                        break;
                    case ESPWSVariableType.DoubleList:
                        if (data.Attributes.ContainsKey(crmFiledName))
                            variableValueRequest.DoubleValue = Double.Parse(GetOptionsSetTextForValue(data.LogicalName, crmFiledName.ToLower(), data.GetAttributeValue<OptionSetValue>(crmFiledName.ToLower()).Value));
                        break;
                    case ESPWSVariableType.TextList:
                        if (data.Attributes.ContainsKey(crmFiledName))
                            variableValueRequest.StringValue = GetOptionsSetTextForValue(data.LogicalName, crmFiledName.ToLower(), data.GetAttributeValue<OptionSetValue>(crmFiledName.ToLower()).Value);
                        break;
                    default:
                        break;
                }
            }
            // fill data from related tables
            else if (!crmFiled.Equals(new KeyValuePair<string, MappingItem>()))
            {
                var relEnt = data.RelatedEntities.FirstOrDefault(x => x.Key.SchemaName.Contains(crmFiled.Value.Entity));
                if (!relEnt.Equals(new KeyValuePair<Relationship, EntityCollection>()) && relEnt.Value.Entities.Count > 0)
                {
                    var crmFiledName = crmFiled.Value.AttrName.ToLower();

                    switch (mad.ESPWSVariableType)
                    {
                        case ESPWSVariableType.Double:
                            if (relEnt.Value.Entities[0].Attributes.ContainsKey(crmFiledName))
                            {
                                var filedValue = relEnt.Value.Entities[0][crmFiledName];
                                if (filedValue is Money)
                                {
                                    variableValueRequest.DoubleValue = Double.Parse((filedValue as Money).Value.ToString());
                                }
                                else
                                {
                                    variableValueRequest.DoubleValue = Double.Parse(filedValue.ToString());
                                }
                            }
                            //relEnt.Value.Entities[0].GetAttributeValue<double>(crmFiledName);// GetDoubleValue(relEnt.Value.Entities[0], crmFiledName);
                            break;
                        case ESPWSVariableType.Text:
                            variableValueRequest.StringValue = relEnt.Value.Entities[0].GetAttributeValue<string>(crmFiledName);//GetStringValue(relEnt.Value.Entities[0], crmFiledName, mad);
                            break;
                        case ESPWSVariableType.DoubleList:
                            var optsetValue = relEnt.Value.Entities[0].GetAttributeValue<OptionSetValue>(crmFiledName);
                            if (optsetValue != null)
                            {
                                variableValueRequest.DoubleValue = Double.Parse(GetOptionsSetTextForValue(relEnt.Value.Entities[0].LogicalName, crmFiledName, optsetValue.Value));
                            }
                            //  variableValueRequest.DoubleValue = Double.Parse(GetOptionsSetTextForValue(relEnt.Value.Entities[0].LogicalName, crmFiledName.ToLower(), relEnt.Value.Entities[0].GetAttributeValue<OptionSetValue>(crmFiledName.ToLower()).Value));
                            break;
                        case ESPWSVariableType.TextList:
                            var optsetValue1 = relEnt.Value.Entities[0].GetAttributeValue<OptionSetValue>(crmFiledName);
                            if (optsetValue1 != null)
                            {
                                variableValueRequest.StringValue = GetOptionsSetTextForValue(relEnt.Value.Entities[0].LogicalName, crmFiledName, optsetValue1.Value);
                            }
                            // variableValueRequest.StringValue = GetOptionsSetTextForValue(relEnt.Value.Entities[0].LogicalName, crmFiledName.ToLower(), relEnt.Value.Entities[0].GetAttributeValue<OptionSetValue>(crmFiledName.ToLower()).Value);
                            break;
                        default:
                            break;
                    }
                }
            }

            variableValueRequest.ESPWSVariableType = mad.ESPWSVariableType;
            variableValueRequest.ID = mad.ID;
            variableValueRequest.Name = mad.Name;
            variableValueRequest.QDDID = mad.QDDID;

            return variableValueRequest;
        }

        public ESPWSVariableValueRequest GenerateESPWSVariableValueRequestForManual(KeyValuePair<string, MappingItem> crmFiled, ESPWSVariableDefinitionResponse mad, KeyValuePair<string, string> field)
        {
            var variableValueRequest = new ESPWSVariableValueRequest
            {
                ESPWSVariableType = mad.ESPWSVariableType
            };

            //main entity
            if (!field.Equals(new KeyValuePair<string, MappingItem>()))
            {
                double doubleVal;
                var crmFiledName = crmFiled.Value.AttrName.ToLower();
                switch (mad.ESPWSVariableType)
                {
                    case ESPWSVariableType.Double:
                        if (double.TryParse(field.Value, out doubleVal))
                            variableValueRequest.DoubleValue = doubleVal;
                        break;
                    case ESPWSVariableType.Text:
                        if (!string.IsNullOrEmpty(field.Value))
                            variableValueRequest.StringValue = field.Value;
                        break;
                    case ESPWSVariableType.DoubleList:
                        variableValueRequest.DoubleValue = Double.Parse(GetOptionsSetTextForValue(crmFiled.Value.Entity, crmFiledName.ToLower(), Int32.Parse(field.Value)));
                        break;
                    case ESPWSVariableType.TextList:
                        variableValueRequest.StringValue = GetOptionsSetTextForValue(crmFiled.Value.Entity, crmFiledName.ToLower(), Int32.Parse(field.Value));
                        break;
                    default:
                        break;
                }
            }

            variableValueRequest.ESPWSVariableType = mad.ESPWSVariableType;
            variableValueRequest.ID = mad.ID;
            variableValueRequest.Name = mad.Name;
            variableValueRequest.QDDID = mad.QDDID;

            return variableValueRequest;
        }

        public UserInputObj2 GetMeasureByTarget(EntityReference target)
        {
            Log.Debug("in GetMeasureByTarget()");
            Log.Debug("target name: " + target.LogicalName);
            Log.Debug("target Id: " + target.Id);

            var query = new QueryExpression()
            {
                EntityName = "ddsm_measure",
                // ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_projecttomeasureid", ConditionOperator.Equal, target.Id)
                    }
                }
            };
            var result = service.RetrieveMultiple(query);
            Log.Debug("count of measures: " + result.Entities.Count);

            return new UserInputObj2() { SmartMeasures = result.Entities.Select(x => x.Id).ToList<Guid>() };
        }

        #region OptionSet
        public int GetOptionsSetValueForIndex(string entityName, string attributeName, int index)
        {
            int selectedOptionValue = -1;

            try
            {
                var retrieveAttributeRequest = new RetrieveAttributeRequest
                {
                    EntityLogicalName = entityName,
                    LogicalName = attributeName.ToLower(),
                    RetrieveAsIfPublished = true
                };
                // Execute the request.
                RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
                // Access the retrieved attribute.
                PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
                // Get the current options list for the retrieved attribute.
                OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();

                if (optionList.Count() > index)
                    selectedOptionValue = (int)optionList[index].Value;
            }
            catch (Exception ex)
            {
                return selectedOptionValue;
            }

            return selectedOptionValue;
        }

        public int GetOptionsSetIndexByValue(string entityName, string attributeName, int value)
        {
            int selectedOptionValue = -1;
            try
            {
                RetrieveAttributeRequest retrieveAttributeRequest = new
                    RetrieveAttributeRequest
                {
                    EntityLogicalName = entityName,
                    LogicalName = attributeName.ToLower(),
                    RetrieveAsIfPublished = true
                };
                // Execute the request.
                var retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
                // Access the retrieved attribute.
                var retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
                // Get the current options list for the retrieved attribute.
                var optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();

                for (int i = 0; i < optionList.Count(); i++)
                {
                    if (optionList[i].Value == value)
                    {
                        selectedOptionValue = i;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                return selectedOptionValue;
            }

            return selectedOptionValue;
        }

        public int GetOptionsSetValueForLabel(string entityName, string attributeName, string selectedLabel)
        {
            var selectedOptionValue = -1;
            try
            {
                var retrieveAttributeRequest = new RetrieveAttributeRequest
                {
                    EntityLogicalName = entityName,
                    LogicalName = attributeName.ToLower(),
                    RetrieveAsIfPublished = true
                };
                // Execute the request.
                var retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
                // Access the retrieved attribute.
                var retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
                // Get the current options list for the retrieved attribute.
                var optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();

                foreach (var oMD in optionList)
                {
                    if (oMD.Label.LocalizedLabels[0].Label.Trim().ToLower() == selectedLabel.Trim().ToLower())
                    {
                        selectedOptionValue = oMD.Value.Value;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                return selectedOptionValue;
            }
            return selectedOptionValue;
        }

        public string GetOptionsSetTextForValue(string entityName, string attributeName, int selectedValue)
        {
            var retrieveAttributeRequest = new
                RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = attributeName,
                RetrieveAsIfPublished = true
            };
            // Execute the request.
            var retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
            // Access the retrieved attribute.
            var retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)
                retrieveAttributeResponse.AttributeMetadata;// Get the current options list for the retrieved attribute.
            var optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
            string selectedOptionLabel = null;
            foreach (var oMD in optionList)
            {
                if (oMD.Value == selectedValue)
                {
                    selectedOptionLabel = oMD.Label.LocalizedLabels[0].Label;
                    break;
                }
            }
            return selectedOptionLabel;
        }
        #endregion

        #region Get Mapping
        public ConcurrentDictionary<string, MappingItem> GetMadMapping(Entity targetEntity, bool groupRecalc = false)
        {
            ConcurrentDictionary<string, MappingItem> result = new ConcurrentDictionary<string, MappingItem>();
            var jsonField = groupRecalc ? "ddsm_mapping2.ddsm_jsondata" : "ddsm_mapping1.ddsm_jsondata";
            try
            {
                var json = targetEntity.GetAttributeValue<AliasedValue>(jsonField);
                if (json?.Value == null)
                {
                    throw new Exception("No MAD mapping data in Mesaure Template. Please select mapping and try again.");
                }
                var mappingJson = (string)json.Value;
                var mapping = JsonConvert.DeserializeObject<Dictionary<string, List<MappingItem2>>>(mappingJson);
                foreach (var map in mapping)
                {
                    foreach (var mad in map.Value)
                    {
                        if (!string.IsNullOrEmpty(mad.AttrLogicalName))
                            result[mad.FieldDisplayName] = new MappingItem { AttrName = mad.AttrLogicalName, Entity = map.Key, FieldType = mad.FieldType };
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public ConcurrentDictionary<string, MappingItem> GetQddMapping(Entity targetEntity, bool groupRecalc = false)
        {
            ConcurrentDictionary<string, MappingItem> result = new ConcurrentDictionary<string, MappingItem>();
            var jsonField = groupRecalc ? "ddsm_mapping3.ddsm_jsondata" : "ddsm_mapping2.ddsm_jsondata";
            try
            {
                var _value = targetEntity.GetAttributeValue<AliasedValue>(jsonField);
                if (_value?.Value == null)
                {
                    throw new Exception("No QDD mapping data in Mesaure Template. Please select mapping and try again.");
                }
                var mappingJson = (string)_value.Value;
                var mapping = JsonConvert.DeserializeObject<Dictionary<string, List<MappingItem2>>>(mappingJson);
                foreach (var map in mapping)
                {
                    foreach (var mad in map.Value)
                    {
                        result[mad.FieldDisplayName] = new MappingItem { AttrName = mad.AttrLogicalName, Entity = map.Key, FieldType = mad.FieldType };
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public Dictionary<string, MSDField> GetMSDMapping(Entity measureTempl)
        {
            string msdValue = measureTempl.GetAttributeValue<string>("ddsm_msdfields");
            if (!string.IsNullOrEmpty(msdValue))
            {
                return JsonConvert.DeserializeObject<Dictionary<string, MSDField>>(msdValue);
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}
