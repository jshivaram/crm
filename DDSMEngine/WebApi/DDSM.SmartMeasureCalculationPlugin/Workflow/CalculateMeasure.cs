﻿using System;
using System.Activities;
using DDSM.CommonProvider;
using DDSM.SmartMeasureCalculationPlugin.App;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using DDSM.SmartMeasureCalculationPlugin.Workflow;


/// <summary>
    /// The Plugin provide opportunity to Create SmartMeasure from Grid in Project
    /// </summary>
    public class CalculateMeasure : BaseCodeActivity
    {
        #region Internal fields
        private string _url = "";
        private readonly string _mainEntity = "ddsm_measure";

        internal override TaskQueue.StatusFileDataUploading CurrentOperationStatus
        {
            get { return TaskQueue.StatusFileDataUploading.ESPRecalculation; }

            set
            {
                throw new NotImplementedException();
            }
        }

        public enum CalculationRecordStatus
        {
            NotReady = 962080000,
            ReadyToCalculation = 962080001
        };
        public enum CreatorRecordType
        {
            Front = 962080000,
            DataUploader = 962080001,
            Portal = 962080002
        };

        #endregion

        protected override void ExecuteActivity(CodeActivityContext context)
        {
            //  var conf = DDSM.CommonProvider.Utils.CrmHelper.GetConfig(ObjCommon.GetOrgService());
            var objCommon = new Common(context);
       
            var app = new AppCalculateMeasure(objCommon);
            app.Run();


            Complete.Set(context, app?.Completed);
            Result.Set(context, app?.Result);
        }




    
}
