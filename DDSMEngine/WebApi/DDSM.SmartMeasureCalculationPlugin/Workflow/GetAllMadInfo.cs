﻿using System;
using System.Activities;
using DDSM.CommonProvider;
using DDSM.SmartMeasureCalculationPlugin.App;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using DDSM.SmartMeasureCalculationPlugin.Workflow;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;


/// <summary>
/// Provide an opportunity to Load all Mrtadata From ESP (SM Library,SM,MAD,QDD) 
/// </summary>
public class GetAllMadInfo : BaseCodeActivity
{
    #region "Parameter Definition"

    [Input("ForceUpdate")]
    public InArgument<bool> ForceUpdate { get; set; }

    internal override TaskQueue.StatusFileDataUploading CurrentOperationStatus
    {
        get { return TaskQueue.StatusFileDataUploading.ESPRecalculation; }

        set
        {
            throw new NotImplementedException();
        }
    }

    #endregion




    protected override void ExecuteActivity(CodeActivityContext context)
    {
        ObjCommon = new Common(context);
        try 
        {
            var app = new AppSyncEspMetadata(ObjCommon);
            app.Run();
        }
        catch (Exception ex)
        {
            SetErrorStatusForDA();
            // ObjCommon.TracingService.Trace("FATAL ERROR: " + ex.Message + string.Join("\n", _processingLog.OrderBy(x => x).ToArray()));
            Complete.Set(context, false);
            Result.Set(context, JsonConvert.SerializeObject(new { MeasureName = "", MeasureID = "", ErrorMsg = ex })); //string.Join("\n", _processingLog.OrderBy(x => x).ToArray())
        }
    }


}
