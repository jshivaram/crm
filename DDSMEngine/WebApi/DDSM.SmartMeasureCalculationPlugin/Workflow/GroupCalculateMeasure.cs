﻿using System;
using System.Activities;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

namespace DDSM.SmartMeasureCalculationPlugin.Workflow
{
    public class GroupCalculateMeasure : BaseCodeActivity
    {
        #region "Parameter Definition"

        [Input("Project")]
        [ArgumentEntity("ddsm_project")]
        [ReferenceTarget("ddsm_project")]
        public InArgument<EntityReference> Project { get; set; }

        [Input("From Measure Grid")]
        public InArgument<bool> FromMeasureGrid { get; set; }

        [Output("Processed Projects")]
        public OutArgument<string> ProcessedProject { get; set; }

        #endregion

        internal override TaskQueue.StatusFileDataUploading CurrentOperationStatus
        {
            get { return TaskQueue.StatusFileDataUploading.ESPRecalculation; }
            set { throw new NotImplementedException(); }
        }

        protected override void ExecuteActivity(CodeActivityContext executionContext)
        {
            //var objCommon = new Common(executionContext);

            //var app = new AppGroupCalculateMeasure(objCommon, new GroupCalculateOptions()
            //{
            //    FromMeasureGrid = FromMeasureGrid.Get(executionContext),
            //    Project = Project.Get(executionContext)
            
            //});
            //ProcessedProject.Set(executionContext, app?.Result);
            //objCommon.TracingService.Trace("in ExecuteActivity");
        }


    }
}

