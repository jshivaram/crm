﻿using System;
using System.Activities;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

namespace DDSM.SmartMeasureCalculationPlugin.Workflow
{
    public class RecalculateMeasure : BaseCodeActivity
    {
        #region "Parameter Definition"

        [RequiredArgument]
        [Input("Measure")]
        [ReferenceTarget("ddsm_measure")]
        public InArgument<EntityReference> Measure { get; set; }

        [RequiredArgument]
        [Input("Measure Type")]
        public InArgument<string> MeasureType { get; set; }

        [RequiredArgument]
        [Input("DisableRecalculation")]
        public InArgument<bool> DisableRecalculation { get; set; }
        #endregion

     

        internal override TaskQueue.StatusFileDataUploading CurrentOperationStatus
        {
            get { return TaskQueue.StatusFileDataUploading.ESPRecalculation; }

            set
            {
                throw new NotImplementedException();
            }
        }

        protected override void ExecuteActivity(CodeActivityContext context)
        {
          
        }

        public void Run()
        {

        }



    }
}

