﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataUploader;
using Microsoft.Crm.Sdk.Messages;
using System.ServiceModel;
using static DataUploader.Model.Enums;
using static DataUploader.Model.Errors;

#if RELESEPLUGIN
#else
using SocketClients;
using DataUploader.Service;
#endif

namespace DataUploader.Creator
{
    public class CreatedEntity
    {
        public string Name { get; set; }
        public string addInfo { get; set; }
        public Guid EntityGuid { get; set; }
        public string PrimaryKey { get; set; }
        public string UniqueParentGUID { get; set; }
        public Guid SourceID { get; set; }
        public bool Status { get; set; }
        public bool StatusDB { get; set; }
    }
    public class StatisticRecords
    {
        public int Created { get; set; } = 0;
        public int Updated { get; set; } = 0;
        public int DoNothing { get; set; } = 0;
        public int All { get; set; } = 0;
    }
    public class ProjectBusinessProcessStage
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }
    public class ProjectBusinessProcess
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public List<ProjectBusinessProcessStage> Stages { get; set; }
    }

    public class PaginationRetriveMultiple
    {
        public EntityCollection RetrieveCollection { get; set; } = new EntityCollection();
        public int pageNumber { get; set; } = 1;
        public dynamic PagingCookie { get; set; } = null;
        public bool MoreRecords { get; set; } = false;
    }

    public class UserInputObj2
    {
        public List<Guid> SmartMeasures { get; set; }
        public Dictionary<string, string> DataFields { get; set; }
    }

    //Sort meas Ids
    public class compareGuidString : IComparer<CreatedEntity>
    {
        public int Compare(CreatedEntity x, CreatedEntity y)
        {
            return x.addInfo.CompareTo(y.addInfo);
        }
    }

    //Parent/Relation Data Attributes
    public class RelationDataAttributes
    {
        public Entity Data_Attributes_1 { get; set; } = null;
        public Entity Data_Attributes_2 { get; set; } = null;
        public DataCollection<Entity> Data_Collection_1 { get; set; } = null;
        public DataCollection<Entity> Data_Collection_2 { get; set; } = null;
        public ProjectBusinessProcess BusinessProcess { get; set; } = null;
        public string tplId { get; set; } = string.Empty;
        public string tplName { get; set; } = string.Empty;
        public string bpfName { get; set; } = string.Empty;
        public int intVal { get; set; } = 1;
    }

    public abstract class BaseMethod : IDisposable
    {
        protected PaginationRetriveMultiple entityCollectionPagination;
        protected StatisticRecords statisticRecords;
        protected ConcurrentDictionary<string, StatisticRecords> statisticsRecords = new ConcurrentDictionary<string, StatisticRecords>();
        protected ConcurrentDictionary<string, EntityJson> configObject;
        protected ConcurrentDictionary<string, RelationDataAttributes> CasheRelationDataAttributes = new ConcurrentDictionary<string, RelationDataAttributes>();
        protected ConcurrentDictionary<string, CreatedEntity> SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
        protected ConcurrentDictionary<string, object> CollectionAttrValue = new ConcurrentDictionary<string, object>();
        protected List<string> Logger2;

#if RELESEPLUGIN
#else
        protected WSMessageUploaderService wsMessageService { get; set; }
#endif


        private bool disposed = false;

        //Generation Entity Object
        protected Entity GenerationEntity(IOrganizationService _orgService, ConcurrentDictionary<string, EntityJson> objects, String TableName, Entity dr, Guid _recordUploaded)
        {
            List<string> dataLogger = new List<string>();
            Entity EntityObj = new Entity(objects[TableName].Name.ToLower());
            string EntityUniqueID = string.Empty;
            try
            {
                foreach (var attr in objects[TableName].Attrs)
                {
                    if (dr.Attributes.ContainsKey("ddsm_" + (attr.Key).ToLower()))
                    {
                        if (attr.Value.Header.ToLower() == (TableName + " GUID").ToLower())
                        {
                            EntityUniqueID = dr["ddsm_" + (attr.Key).ToLower()].ToString();
                        }
                        if ((attr.Value.Header.ToLower()).IndexOf("guid") != -1)
                        {
                        }
                        else if ((attr.Value.Header.ToLower()).IndexOf("template") != -1)
                        {
                        }
                        else if ((attr.Value.Header.ToLower()).IndexOf("milestone") != -1)
                        {
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(dr["ddsm_" + (attr.Key).ToLower()])))
                            {
                                if (attr.Value.AttrType.ToString() != "Lookup")
                                {
                                    dynamic tmpEntityObj = AttributeValue(_orgService, objects[TableName].Name.ToString().ToLower(), attr.Value.Name.ToString().ToLower(), attr.Value.AttrType.ToString(), dr["ddsm_" + (attr.Key).ToLower()], string.Empty);
                                    if (tmpEntityObj != null)
                                    {
                                        EntityObj[attr.Value.Name.ToLower()] = tmpEntityObj;
                                    }
                                    else
                                    {
                                        dataLogger.Add(TableName + "; GUID: " + EntityUniqueID + "; " + "Excel column: " + attr.Key + " / Header: " + attr.Value.Header.ToString() + " / Value: " + dr["ddsm_" + (attr.Key).ToLower()] + " - " + GetTextError(380016));
                                    }
                                }
                                else
                                {
                                    dynamic tmpEntityObj = AttributeValue(_orgService, objects[TableName].Name.ToString().ToLower(), (attr.Value.Name.ToString().ToLower()).Split(':')[0], attr.Value.AttrType.ToString(), dr["ddsm_" + (attr.Key).ToLower()], (attr.Value.Name.ToLower()).Split(':')[1]);
                                    if (tmpEntityObj != null)
                                    {
                                        EntityObj[(attr.Value.Name.ToLower()).Split(':')[0]] = tmpEntityObj;
                                    }
                                    else
                                    {
                                        dataLogger.Add(TableName + "; GUID: " + EntityUniqueID + "; " + "Excel column: " + attr.Key + " / Header: " + attr.Value.Header.ToString() + " / Value: " + dr["ddsm_" + (attr.Key).ToLower()] + " / LookupEntity: " + (attr.Value.Name.ToLower()).Split(':')[1] + " - " + GetTextError(380015));
                                    }
                                }
                            }
                        }
                    }
                }

                if (dataLogger.Count > 0)
                {
                    DataUploader.SaveLog(_orgService, _recordUploaded, dataLogger, "ddsm_logdata");
                }

            }
            catch (Exception e)
            {
                EntityObj = new Entity(objects[TableName].Name.ToLower());
                return EntityObj;
            }
            return EntityObj;

        }

        //Create value attribute entity
        protected dynamic AttributeValue(IOrganizationService _orgService, string entityName, string AttrName, string AttrType, object AttrValue, string LookupEntity)
        {
            try
            {
                //Get cache value
                string _cacheValKey = entityName + AttrName + AttrType + AttrValue.ToString() + LookupEntity;
                if (CollectionAttrValue.ContainsKey(_cacheValKey))
                {
                    return CollectionAttrValue[_cacheValKey];
                }


                switch (AttrType)
                {
                    case "String":
                        return AttrValue.ToString();

                    case "Memo":
                        return AttrValue.ToString();

                    case "Picklist":

                        RetrieveAttributeRequest retrieveAttributeRequest = new RetrieveAttributeRequest
                        {
                            EntityLogicalName = entityName,
                            LogicalName = AttrName,
                            RetrieveAsIfPublished = true
                        };
                        RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)_orgService.Execute(retrieveAttributeRequest);
                        Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata)
                        retrieveAttributeResponse.AttributeMetadata;
                        OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
                        int selectedOptionValue = 0;
                        foreach (OptionMetadata oMD in optionList)
                        {
                            if (oMD.Label.LocalizedLabels[0].Label.ToString().ToLower() == AttrValue.ToString().ToLower())
                            {
                                selectedOptionValue = oMD.Value.Value;
                                break;
                            }
                        }
                        if (selectedOptionValue != 0)
                        {
                            //add value to cache
                            CollectionAttrValue.AddOrUpdate(_cacheValKey, new OptionSetValue(selectedOptionValue), (oldkey, oldvalue) => new OptionSetValue(selectedOptionValue));

                            return new OptionSetValue(selectedOptionValue);
                        }
                        else
                        {
                            //add value to cache
                            /*
                            CollectionAttrValue.AddOrUpdate(_cacheValKey, new OptionSetValue(retrievedPicklistAttributeMetadata.DefaultFormValue.Value), (oldkey, oldvalue) => new OptionSetValue(retrievedPicklistAttributeMetadata.DefaultFormValue.Value));
                            // return default value
                            return new OptionSetValue(retrievedPicklistAttributeMetadata.DefaultFormValue.Value);
                            */
                        }

                        CollectionAttrValue.AddOrUpdate(_cacheValKey, null, (oldkey, oldvalue) => null);
                        return null;

                    case "Lookup":
                        QueryExpression LookupQuery;
                        if (LookupEntity != string.Empty)
                        {
                            if (LookupEntity.IndexOf("ddsm_") != -1)
                            {
                                if (LookupEntity == "ddsm_projecttask")
                                {
                                    LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("subject", "activityid") };
                                    LookupQuery.Criteria.AddCondition("subject", ConditionOperator.Equal, AttrValue.ToString());
                                }
                                else if (LookupEntity == "ddsm_site")
                                {
                                    LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("ddsm_name", LookupEntity + "id") };
                                    //LookupQuery.Criteria.AddCondition("ddsm_siteprimarykey", ConditionOperator.Equal, AttrValue.ToString());
                                    LookupQuery.Criteria.AddCondition("ddsm_siteprimarykey", ConditionOperator.Equal, (AttrValue.ToString()).ToLower());
                                }
                                else
                                {
                                    LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("ddsm_name", LookupEntity + "id") };
                                    LookupQuery.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, AttrValue.ToString());
                                }
                            }
                            else if (LookupEntity == "systemuser")
                            {
                                LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("fullname", LookupEntity + "id") };
                                LookupQuery.Criteria.AddCondition("fullname", ConditionOperator.Equal, AttrValue.ToString());
                            }
                            else if (LookupEntity == "contact")
                            {
                                LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("fullname", LookupEntity + "id") };
                                //LookupQuery.Criteria.AddCondition("ddsm_contactprimarykey", ConditionOperator.Equal, AttrValue.ToString());
                                LookupQuery.Criteria.AddCondition("ddsm_contactprimarykey", ConditionOperator.Equal, (AttrValue.ToString()).ToLower());
                            }
                            else if (LookupEntity == "account")
                            {
                                LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("name", LookupEntity + "id") };
                                //LookupQuery.Criteria.AddCondition("ddsm_accountprimarykey", ConditionOperator.Equal, AttrValue.ToString());
                                LookupQuery.Criteria.AddCondition("ddsm_accountprimarykey", ConditionOperator.Equal, (AttrValue.ToString()).ToLower());
                            }
                            else if (LookupEntity == "task")
                            {
                                LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("subject", "activityid") };
                                LookupQuery.Criteria.AddCondition("subject", ConditionOperator.Equal, AttrValue.ToString());
                            }
                            else if (LookupEntity == "annotation")
                            {
                                LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("subject", "annotationid") };
                                LookupQuery.Criteria.AddCondition("subject", ConditionOperator.Equal, AttrValue.ToString());
                            }
                            else
                            {
                                LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("name", LookupEntity + "id") };
                                LookupQuery.Criteria.AddCondition("name", ConditionOperator.Equal, AttrValue.ToString());
                            }

                            EntityCollection LookupRetrieve = _orgService.RetrieveMultiple(LookupQuery);
                            if (LookupRetrieve != null && LookupRetrieve.Entities.Count == 1)
                            {
                                //add value to cache
                                CollectionAttrValue.AddOrUpdate(_cacheValKey, new EntityReference(LookupEntity, new Guid(LookupRetrieve.Entities[0].Attributes[LookupEntity + "id"].ToString())), (oldkey, oldvalue) => new EntityReference(LookupEntity, new Guid(LookupRetrieve.Entities[0].Attributes[LookupEntity + "id"].ToString())));

                                return new EntityReference(LookupEntity, new Guid(LookupRetrieve.Entities[0].Attributes[LookupEntity + "id"].ToString()));
                            }
                            else
                            {
                                if (LookupEntity == "contact")
                                {
                                    LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("fullname", LookupEntity + "id") };
                                    LookupQuery.Criteria.AddCondition("fullname", ConditionOperator.Equal, AttrValue.ToString());
                                    LookupRetrieve = _orgService.RetrieveMultiple(LookupQuery);
                                    if (LookupRetrieve != null && LookupRetrieve.Entities.Count == 1)
                                    {
                                        //add value to cache
                                        CollectionAttrValue.AddOrUpdate(_cacheValKey, new EntityReference(LookupEntity, new Guid(LookupRetrieve.Entities[0].Attributes[LookupEntity + "id"].ToString())), (oldkey, oldvalue) => new EntityReference(LookupEntity, new Guid(LookupRetrieve.Entities[0].Attributes[LookupEntity + "id"].ToString())));

                                        return new EntityReference(LookupEntity, new Guid(LookupRetrieve.Entities[0].Attributes[LookupEntity + "id"].ToString()));
                                    }
                                    else
                                    {
                                        //add value to cache
                                        CollectionAttrValue.AddOrUpdate(_cacheValKey, null, (oldkey, oldvalue) => null);

                                        return null;
                                    }
                                }
                                else if (LookupEntity == "account")
                                {
                                    LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("name", LookupEntity + "id") };
                                    LookupQuery.Criteria.AddCondition("name", ConditionOperator.Equal, AttrValue.ToString());
                                    LookupRetrieve = _orgService.RetrieveMultiple(LookupQuery);
                                    if (LookupRetrieve != null && LookupRetrieve.Entities.Count == 1)
                                    {
                                        //add value to cache
                                        CollectionAttrValue.AddOrUpdate(_cacheValKey, new EntityReference(LookupEntity, new Guid(LookupRetrieve.Entities[0].Attributes[LookupEntity + "id"].ToString())), (oldkey, oldvalue) => new EntityReference(LookupEntity, new Guid(LookupRetrieve.Entities[0].Attributes[LookupEntity + "id"].ToString())));

                                        return new EntityReference(LookupEntity, new Guid(LookupRetrieve.Entities[0].Attributes[LookupEntity + "id"].ToString()));
                                    }
                                    else
                                    {
                                        //add value to cache
                                        CollectionAttrValue.AddOrUpdate(_cacheValKey, null, (oldkey, oldvalue) => null);

                                        return null;
                                    }
                                }
                                else if (LookupEntity == "ddsm_site")
                                {
                                    LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("ddsm_name", LookupEntity + "id") };
                                    LookupQuery.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, AttrValue.ToString());
                                    LookupRetrieve = _orgService.RetrieveMultiple(LookupQuery);
                                    if (LookupRetrieve != null && LookupRetrieve.Entities.Count == 1)
                                    {
                                        //add value to cache
                                        CollectionAttrValue.AddOrUpdate(_cacheValKey, new EntityReference(LookupEntity, new Guid(LookupRetrieve.Entities[0].Attributes[LookupEntity + "id"].ToString())), (oldkey, oldvalue) => new EntityReference(LookupEntity, new Guid(LookupRetrieve.Entities[0].Attributes[LookupEntity + "id"].ToString())));

                                        return new EntityReference(LookupEntity, new Guid(LookupRetrieve.Entities[0].Attributes[LookupEntity + "id"].ToString()));
                                    }
                                    else
                                    {
                                        //add value to cache
                                        CollectionAttrValue.AddOrUpdate(_cacheValKey, null, (oldkey, oldvalue) => null);

                                        return null;
                                    }
                                }
                                else
                                {
                                    //add value to cache
                                    CollectionAttrValue.AddOrUpdate(_cacheValKey, null, (oldkey, oldvalue) => null);

                                    return null;
                                }
                            }
                        }
                        else
                        {
                            return null;
                        }
                    case "Decimal":
                        if (DataUploader.IsNumeric(AttrValue))
                        {
                            return Convert.ToDecimal(AttrValue);
                        }
                        else
                            return null;

                    case "Double":
                        if (DataUploader.IsNumeric(AttrValue))
                        {
                            return Convert.ToDouble(AttrValue);
                        }
                        else
                            return null;

                    case "Money":
                        if (DataUploader.IsNumeric(AttrValue))
                        {
                            return new Money(Convert.ToDecimal(AttrValue));
                        }
                        else
                            return null;

                    case "Boolean":

                        if (AttrValue.ToString().ToLower() == "true") return true;
                        if (AttrValue.ToString().ToLower() == "false") return false;

                        RetrieveAttributeRequest retrieveAttributeBoolRequest = new RetrieveAttributeRequest
                        {
                            EntityLogicalName = entityName,
                            LogicalName = AttrName,
                            RetrieveAsIfPublished = true
                        };
                        RetrieveAttributeResponse retrieveAttributeBoolResponse = (RetrieveAttributeResponse)_orgService.Execute(retrieveAttributeBoolRequest);
                        Microsoft.Xrm.Sdk.Metadata.BooleanAttributeMetadata retrievedBooleanAttributeMetadata = (Microsoft.Xrm.Sdk.Metadata.BooleanAttributeMetadata)
                        retrieveAttributeBoolResponse.AttributeMetadata;
                        if (retrievedBooleanAttributeMetadata.OptionSet.FalseOption.Label.LocalizedLabels[0].Label.ToString().ToLower() == AttrValue.ToString().ToLower())
                        {
                            //add value to cache
                            CollectionAttrValue.AddOrUpdate(_cacheValKey, false, (oldkey, oldvalue) => false);

                            return false;
                        }
                        else if (retrievedBooleanAttributeMetadata.OptionSet.TrueOption.Label.LocalizedLabels[0].Label.ToString().ToLower() == AttrValue.ToString().ToLower())
                        {
                            //add value to cache
                            CollectionAttrValue.AddOrUpdate(_cacheValKey, true, (oldkey, oldvalue) => true);

                            return true;
                        }

                        //add value to cache
                        CollectionAttrValue.AddOrUpdate(_cacheValKey, retrievedBooleanAttributeMetadata.DefaultValue.Value, (oldkey, oldvalue) => retrievedBooleanAttributeMetadata.DefaultValue.Value);

                        return retrievedBooleanAttributeMetadata.DefaultValue.Value;

                    case "DateTime":

                        return DataUploader.ConverAttributToDateTimeUtc(AttrValue);

                    case "Integer":
                        if (DataUploader.IsNumeric(AttrValue))
                        {
                            return Convert.ToInt32(AttrValue);
                        }
                        else
                            return null;

                    case "BigInt":
                        if (DataUploader.IsNumeric(AttrValue))
                        {
                            return Convert.ToInt64(AttrValue);
                        }
                        else
                            return null;
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //Verify Duplicate Records
        protected string VerifyDuplicateRecord(IOrganizationService _orgService, Entity EntityObj, string entityName)
        {
            string ResponseEntityId = string.Empty;
            string ResponseEntityName = string.Empty;
            RetrieveDuplicatesRequest Request = new RetrieveDuplicatesRequest();
            Request.BusinessEntity = EntityObj;
            Request.MatchingEntityName = entityName;
            Request.PagingInfo = new PagingInfo() { PageNumber = 1, Count = 50 };
            try
            {
                RetrieveDuplicatesResponse Response = (RetrieveDuplicatesResponse)_orgService.Execute(Request);
                foreach (Entity ResponseEntity in Response.DuplicateCollection.Entities)
                {
                    if (entityName.IndexOf("ddsm_") != -1)
                    {
                        ResponseEntityId = ResponseEntity.Id.ToString() + "::" + ((ResponseEntity.Attributes.ContainsKey("ddsm_name")) ? ResponseEntity["ddsm_name"].ToString() : "");
                    }
                    else
                    if (entityName.IndexOf("contact") != -1)
                    {
                        ResponseEntityId = ResponseEntity.Id.ToString() + "::" + ((ResponseEntity.Attributes.ContainsKey("fullname")) ? ResponseEntity["fullname"].ToString() : "");
                    }
                    else
                    {
                        ResponseEntityId = ResponseEntity.Id.ToString() + "::" + ((ResponseEntity.Attributes.ContainsKey("name")) ? ResponseEntity["name"].ToString() : "");
                    }
                    //ResponseEntityName = ResponseEntity["name"].ToString();
                    //Console.WriteLine(ResponseEntity.Id.ToString() + " - " + ResponseEntity["name"]);
                    break;
                }
                return ResponseEntityId;
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                return string.Empty;
            }
        }

        //Upadate Source Records
        protected void UpdateSourceRecords(IOrganizationService _orgService, ConcurrentDictionary<string, EntityJson> objects, ConcurrentDictionary<string, CreatedEntity> dictionaryEntities, string entityName, bool callExecuteMultiple = true)
        {
            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == entityName).Key;

            var updateRecords = dictionaryEntities.Where(x => x.Value.Status == true && x.Value.StatusDB != true).ToDictionary(x => x.Key, x => x.Value);

            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = false
                }
            };

            foreach (var key in updateRecords.Keys)
            {
                if (updateRecords[key].EntityGuid != null && updateRecords[key].EntityGuid != new Guid())
                    if (updateRecords[key].SourceID != null && updateRecords[key].SourceID != new Guid())
                    {
                        Entity updateEntity = new Entity("ddsm_exceldata", updateRecords[key].SourceID);
                        updateEntity["ddsm_statusxlsrecord"] = new OptionSetValue((int)StatusXLSRecord.Processed);

                        Dictionary<string, CreatedEntity> tmpDictionaryEntity = new Dictionary<string, CreatedEntity>();
                        tmpDictionaryEntity.Add(key, updateRecords[key]);

                        string jsonObject = Newtonsoft.Json.JsonConvert.SerializeObject(tmpDictionaryEntity);

                        updateEntity["ddsm_entityobjectdictionary"] = jsonObject;

                        switch (entityName)
                        {
                            case "contact":
                                updateEntity["ddsm_contact"] = new EntityReference(entityName, updateRecords[key].EntityGuid);
                                break;
                            case "account":
                                updateEntity["ddsm_account"] = new EntityReference(entityName, updateRecords[key].EntityGuid);
                                break;
                            default:
                                updateEntity[entityName] = new EntityReference(entityName, updateRecords[key].EntityGuid);
                                break;
                        }

                        //_orgService.Update(updateEntity);

                        
                        UpdateRequest updateRequest = new UpdateRequest();
                        updateRequest.Target = updateEntity;
                        emRequest.Requests.Add(updateRequest);
                        
                    }
            }
            
            if (emRequest.Requests.Count > 0)
            {
#if RELESEPLUGIN
#else
                wsMessageService.SendMessage(MessageType.INFORMATION, "Started updating of basic excel records (sheet: " + keyEntity + ")", MessageStatus.START);
#endif

                if (!callExecuteMultiple)
                {
                    //Single
                    foreach (var req in emRequest.Requests)
                    {
                        try
                        {

                            if (req.RequestName == "Update")
                            {
                                _orgService.Update(((UpdateRequest)req).Target);
                            }
                        }
                        catch (Exception e)
                        {
                            //Logger.Add("Error Message: " + e.Message);
                        }
                    }
                }
                else {
                    ExecuteMultipleResponse emResponse = (ExecuteMultipleResponse)_orgService.Execute(emRequest);
                    /*
                    foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                    {
                        // A valid response.
                        if (responseItem.Response != null)
                        {
                            //Logger.Add("Name " + ((Entity)emRequest.Requests[responseItem.RequestIndex].Parameters["Target"]).GetAttributeValue<string>("ddsm_name"));

                            if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                            {
                                //Logger.Add(" Id: " + ((OrganizationResponse)responseItem.Response).Results["id"].ToString());
                            }
                        }

                        // An error has occurred.
                        else if (responseItem.Fault != null)
                        {
                            //Logger.Add("Error: Name: " + emRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message);
                        }
                    }
                    */
                }
#if RELESEPLUGIN
#else
                wsMessageService.SendMessage(MessageType.SUCCESS, "Completed updating of basic excel records (sheet: " + keyEntity + ")", MessageStatus.START);
#endif

            }


        }

        protected void CreateUpdateEntityRecords(IOrganizationService _orgService, ExecuteMultipleRequest emRequest, List<string> setUniqueID, ConcurrentDictionary<string, CreatedEntity> dictionaryEntities, ConcurrentDictionary<string, CreatedEntity> requestEntities, StatisticRecords statisticRecords, List<string> Logger, bool callExecuteMultiple = true)
        {

            int indexReq = -1;
            CreatedEntity recordCreate;
            string logicalName = string.Empty;
            if (emRequest.Requests[0].RequestName == "Update")
            {
                logicalName = ((UpdateRequest)emRequest.Requests[0]).Target.LogicalName;
            }
            else
            {
                logicalName = ((CreateRequest)emRequest.Requests[0]).Target.LogicalName;
            }


            Int32 countRecords = emRequest.Requests.Count;

#if RELESEPLUGIN
#else
            wsMessageService.SendMessage(MessageType.INFORMATION, "Started the creation of " + Convert.ToString(countRecords) + " Entity Records: " + logicalName, MessageStatus.START);
            //wsMessageService.SendMessage(MessageType.INFORMATION, "Started the creation of " + Convert.ToString(countRecords) + " entity records", MessageStatus.START);
#endif

            if (!callExecuteMultiple)
            {
                //Single
                foreach (var req in emRequest.Requests)
                {
                    indexReq++;
                    try
                    {

                        if (req.RequestName == "Update")
                        {
                            _orgService.Update(((UpdateRequest)req).Target);
                            statisticRecords.Updated++;
                        }
                        else if (req.RequestName == "Create")
                        {
                            Guid entityId = _orgService.Create(((CreateRequest)req).Target);
                            statisticRecords.Created++;
                            dictionaryEntities[setUniqueID[indexReq]].EntityGuid = entityId;
                        }
                        dictionaryEntities[setUniqueID[indexReq]].Status = true;
                        recordCreate = dictionaryEntities[setUniqueID[indexReq]];
                        requestEntities.AddOrUpdate(setUniqueID[indexReq], recordCreate, (oldkey, oldvalue) => recordCreate);
                    }
                    catch (Exception e)
                    {
                        Logger.Add("Error: Name: " + setUniqueID[indexReq] + " Index: " + (indexReq + 1) + " Message: " + e.Message);
#if RELESEPLUGIN
#else
                        wsMessageService.SendMessage(MessageType.ERROR, "Error: Name: " + setUniqueID[indexReq] + " Index: " + (indexReq + 1) + " Message: " + e.Message, MessageStatus.START);
#endif

                    }
                }
            }
            else {
                //ExecuteMultiple
                ExecuteMultipleResponse emResponse = (ExecuteMultipleResponse)_orgService.Execute(emRequest);
                foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                {
                    if (responseItem.Response != null)
                    {
                        dictionaryEntities[setUniqueID[responseItem.RequestIndex]].Status = true;

                        if ((responseItem.Response).Results.ContainsKey("id"))
                        {
                            statisticRecords.Created++;
                            dictionaryEntities[setUniqueID[responseItem.RequestIndex]].EntityGuid = new Guid(((OrganizationResponse)responseItem.Response).Results["id"].ToString());
                        }
                        else
                        {
                            statisticRecords.Updated++;
                        }

                        recordCreate = dictionaryEntities[setUniqueID[responseItem.RequestIndex]];
                        requestEntities.AddOrUpdate(setUniqueID[responseItem.RequestIndex], recordCreate, (oldkey, oldvalue) => recordCreate);
                    }

                    // An error has occurred.
                    else if (responseItem.Fault != null)
                    {
                        Logger.Add("Error: Name: " + emRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message);

#if RELESEPLUGIN
#else
                        wsMessageService.SendMessage(MessageType.ERROR, "Error: Name: " + emRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message, MessageStatus.START);
#endif

                    }
                }
            }

#if RELESEPLUGIN
#else
            wsMessageService.SendMessage(MessageType.SUCCESS, "Completed creation of " + Convert.ToString(countRecords) + " Entity Records: " + logicalName, MessageStatus.START);
            //wsMessageService.SendMessage(MessageType.SUCCESS, "Completed creation of " + Convert.ToString(countRecords) + " entity records", MessageStatus.START);
#endif

        }

        protected Entity GetDataRelationMapping(IOrganizationService _orgService, string targetEntity, string sourceEntity, Guid sourceGuid)
        {
            InitializeFromRequest initialize = new InitializeFromRequest();
            initialize.TargetEntityName = targetEntity;
            initialize.EntityMoniker = new EntityReference(sourceEntity, sourceGuid);
            initialize.TargetFieldType = TargetFieldType.All;
            InitializeFromResponse initialized = (InitializeFromResponse)_orgService.Execute(initialize);
            return initialized.Entity;
        }

        //Get Entity Attributes Value
        protected Entity GetEntityAttrsValue(IOrganizationService _orgService, Guid recordGuid, string entityName, ColumnSet Columns)
        {
            Entity attrCollection = new Entity(entityName);
            attrCollection = _orgService.Retrieve(entityName, recordGuid, Columns);
            return attrCollection;
        }


        protected EntityCollection GetDataByEntityName(IOrganizationService _orgService, string entityName, ConcurrentDictionary<string, EntityJson> objects, Guid recordUploaded, List<string> Logger, bool StatusRecord)
        {
            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == entityName).Key;

            if (keyEntity != null)
            {
                EntityCollection retriveCollection = new EntityCollection();

                try
                {
                    QueryExpression query = new QueryExpression();
                    query.EntityName = "ddsm_exceldata";
                    query.ColumnSet = new ColumnSet(allColumns: true);
                    query.Criteria = new FilterExpression(LogicalOperator.And);
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_name", ConditionOperator.Equal, keyEntity));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_statusxlsrecord", ConditionOperator.Equal, (!StatusRecord) ? (int)StatusXLSRecord.NotProcessed : (int)StatusXLSRecord.Processed));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_datauploaderfile", ConditionOperator.Equal, recordUploaded));

                    int pageNumber = 1;
                    RetrieveMultipleRequest multiRequest;
                    RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();

                    do
                    {
                        query.PageInfo.Count = 5000;
                        query.PageInfo.PagingCookie = (pageNumber == 1) ? null : multiResponse.EntityCollection.PagingCookie;
                        query.PageInfo.PageNumber = pageNumber++;

                        multiRequest = new RetrieveMultipleRequest();
                        multiRequest.Query = query;
                        multiResponse = (RetrieveMultipleResponse)_orgService.Execute(multiRequest);

                        retriveCollection.Entities.AddRange(multiResponse.EntityCollection.Entities);
                    }
                    while (multiResponse.EntityCollection.MoreRecords);

                    return (retriveCollection.Entities.Count > 0) ? retriveCollection : null;
                }
                catch (Exception e)
                {
                    Logger.Add("Error! Method getDataByEntityName; keyEntity: " + keyEntity + "; " + e.Message);
                }

            }

            return null;
        }
        protected PaginationRetriveMultiple GetDataByEntityName(IOrganizationService _orgService, string entityName, ConcurrentDictionary<string, EntityJson> objects, Guid recordUploaded, List<string> Logger, bool StatusRecord, int pageNumber, dynamic PagingCookie, bool MoreRecords, Int32 _recordCount)
        {
            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == entityName).Key;

            PaginationRetriveMultiple paginationRetriveMultiple = new PaginationRetriveMultiple();
            paginationRetriveMultiple.RetrieveCollection = new EntityCollection();
            paginationRetriveMultiple.pageNumber = pageNumber;
            paginationRetriveMultiple.PagingCookie = PagingCookie;
            paginationRetriveMultiple.MoreRecords = MoreRecords;

            if (keyEntity != null)
            {

                try
                {
                    QueryExpression query = new QueryExpression();
                    query.EntityName = "ddsm_exceldata";
                    query.ColumnSet = new ColumnSet(allColumns: true);
                    query.Criteria = new FilterExpression(LogicalOperator.And);
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_name", ConditionOperator.Equal, keyEntity));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_statusxlsrecord", ConditionOperator.Equal, (!StatusRecord) ? (int)StatusXLSRecord.NotProcessed : (int)StatusXLSRecord.Processed));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_datauploaderfile", ConditionOperator.Equal, recordUploaded));

                    RetrieveMultipleRequest multiRequest;
                    RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();

                    query.PageInfo.Count = _recordCount;
                    query.PageInfo.PagingCookie = (pageNumber == 1) ? null : PagingCookie;
                    query.PageInfo.PageNumber = pageNumber++;

                    multiRequest = new RetrieveMultipleRequest();
                    multiRequest.Query = query;
                    multiResponse = (RetrieveMultipleResponse)_orgService.Execute(multiRequest);

                    paginationRetriveMultiple.PagingCookie = multiResponse.EntityCollection.PagingCookie;
                    paginationRetriveMultiple.pageNumber = pageNumber;
                    paginationRetriveMultiple.MoreRecords = multiResponse.EntityCollection.MoreRecords;

                    paginationRetriveMultiple.RetrieveCollection.Entities.AddRange(multiResponse.EntityCollection.Entities);

                    return paginationRetriveMultiple;

                }
                catch (Exception e)
                {
                    Logger.Add("Error! Method getDataByEntityName; keyEntity: " + keyEntity + "; " + e.Message);
                }

            }

            return null;
        }

        protected void GetDictionaryObjectbyGUID(IOrganizationService _orgService, string uniqueGUID, string entityName, ConcurrentDictionary<string, EntityJson> objects, ConcurrentDictionary<string, CreatedEntity> dictionaryEntity, Guid _recordUploaded)
        {
            if (string.IsNullOrEmpty(entityName)) return;
            if (string.IsNullOrEmpty(uniqueGUID)) return;

            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == entityName).Key;
            if (string.IsNullOrEmpty(keyEntity)) return;
            string uniqueIdColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " GUID").ToLower()).Key;
            if (string.IsNullOrEmpty(uniqueIdColumnKey)) return;
            string jsonObject = string.Empty;
            if (dictionaryEntity.ContainsKey(uniqueGUID))
            {
            }
            else
            {
                if (keyEntity != null)
                {
                    QueryExpression query = new QueryExpression();
                    query.EntityName = "ddsm_exceldata";
                    query.ColumnSet = new ColumnSet("ddsm_entityobjectdictionary");
                    query.Criteria = new FilterExpression(LogicalOperator.And);
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_name", ConditionOperator.Equal, keyEntity));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_" + uniqueIdColumnKey.ToLower(), ConditionOperator.Equal, uniqueGUID));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_statusxlsrecord", ConditionOperator.Equal, (int)StatusXLSRecord.Processed));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_datauploaderfile", ConditionOperator.Equal, _recordUploaded));

                    EntityCollection queryRetrieve = _orgService.RetrieveMultiple(query);
                    if (queryRetrieve != null && queryRetrieve.Entities.Count == 1)
                    {
                        if (queryRetrieve[0].Attributes.ContainsKey("ddsm_entityobjectdictionary"))
                            jsonObject = (string)queryRetrieve[0].GetAttributeValue<string>("ddsm_entityobjectdictionary");
                    }
                    if (string.IsNullOrEmpty(jsonObject)) return;
                    var dictionaryObject = Newtonsoft.Json.JsonConvert.DeserializeObject<ConcurrentDictionary<string, CreatedEntity>>(jsonObject);
                    if (dictionaryObject != null)
                    {
                        dictionaryEntity.AddOrUpdate(uniqueGUID, dictionaryObject[uniqueGUID], (oldkey, oldvalue) => dictionaryObject[uniqueGUID]);
                    }
                }
            }

        }

        //for Update record
        protected bool ComparingAttributeValues(object oAttribute, object oAttribute2)
        {
            bool _val = false;

            try
            {
                if (oAttribute.GetType().Equals(typeof(Microsoft.Xrm.Sdk.OptionSetValue))
                    && oAttribute2.GetType().Equals(typeof(Microsoft.Xrm.Sdk.OptionSetValue))
                    && !string.IsNullOrEmpty(Convert.ToString((oAttribute as Microsoft.Xrm.Sdk.OptionSetValue).Value))
                    && !string.IsNullOrEmpty(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.OptionSetValue).Value))
                    && Int32.Parse(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.OptionSetValue).Value)) != Int32.Parse(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.OptionSetValue).Value)))
                {
                    _val = true;
                }
                else if (oAttribute.GetType().Equals(typeof(Microsoft.Xrm.Sdk.Money))
                  && oAttribute2.GetType().Equals(typeof(Microsoft.Xrm.Sdk.Money))
                  && !string.IsNullOrEmpty(Convert.ToString((oAttribute as Microsoft.Xrm.Sdk.Money).Value))
                  && !string.IsNullOrEmpty(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.Money).Value))
                  && Decimal.Parse(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.Money).Value)) != Decimal.Parse(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.Money).Value)))
                {
                    _val = true;
                }
                else if (oAttribute.GetType().Equals(typeof(Microsoft.Xrm.Sdk.EntityReference))
                  && oAttribute2.GetType().Equals(typeof(Microsoft.Xrm.Sdk.EntityReference))
                  && !string.IsNullOrEmpty(Convert.ToString((oAttribute as Microsoft.Xrm.Sdk.EntityReference).Id))
                  && !string.IsNullOrEmpty(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.EntityReference).Id))
                  && Convert.ToString((oAttribute as Microsoft.Xrm.Sdk.EntityReference).Id) != Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.EntityReference).Id))
                {
                    _val = true;
                }
                else if (oAttribute.GetType().Equals(typeof(System.Int32))
                  && oAttribute2.GetType().Equals(typeof(System.Int32)))
                {
                    if (Convert.ToInt32(oAttribute) != Convert.ToInt32(oAttribute2)) {
                        _val = true;
                    }
                }
                else if (oAttribute.GetType().Equals(typeof(System.Decimal))
                  && oAttribute2.GetType().Equals(typeof(System.Decimal)))
                {
                    if (Convert.ToDecimal(oAttribute) != Convert.ToDecimal(oAttribute2))
                    {
                        _val = true;
                    }
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(oAttribute))
                  && !string.IsNullOrEmpty(Convert.ToString(oAttribute2))
                  && Convert.ToString(oAttribute) != Convert.ToString(oAttribute2))
                {
                    _val = true;
                }
            }
            catch (Exception e)
            {
                _val = false;
            }

            return _val;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (CollectionAttrValue != null)
                    {
                        CollectionAttrValue?.Clear();
                    }
                    if (SkippedEntities != null)
                    {
                        SkippedEntities?.Clear();
                    }
                    if (statisticsRecords != null)
                    {
                        statisticsRecords?.Clear();
                    }
                    if (CasheRelationDataAttributes != null)
                    {
                        CasheRelationDataAttributes?.Clear();
                    }
                    if (entityCollectionPagination != null)
                    {
                        entityCollectionPagination = null;
                    }
                    if (statisticRecords != null)
                    {
                        statisticRecords = null;
                    }
                    if (configObject != null)
                    {
                        configObject?.Clear();
                    }
                    if (Logger2 != null)
                    {
                        Logger2?.Clear();
                    }
                }
                this.disposed = true;
            }
        }
    }
}
