﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataUploader;
using static DataUploader.Model.Enums;
using static DataUploader.Model.Errors;
using static DataUploader.Model.PrimaryKey;
using static DataUploader.Model.AutoNumberingManager;
using Microsoft.Xrm.Sdk.Messages;
using System.Text.RegularExpressions;
using Microsoft.Xrm.Sdk.Query;

#if RELESEPLUGIN
#else
using SocketClients;
using DataUploader.Service;
#endif

namespace DataUploader.Creator
{
    public class CreateAccounts : BaseMethod
    {

        //Create New
        public void CreateAccount(IOrganizationService _orgService, ConcurrentDictionary<string, CreatedEntity> AccountEntities, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            Logger2 = new List<string>();
            configObject = Newtonsoft.Json.JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);

            var keyEntity = configObject.FirstOrDefault(x => x.Value.Name.ToLower() == "account").Key;
            if (string.IsNullOrEmpty(keyEntity))
            {
                return;
            }

#if RELESEPLUGIN
#else
            wsMessageService = new WSMessageUploaderService(_thisSettings);
            wsMessageService.SendMessage(MessageType.INFORMATION, GetTextError(380031), MessageStatus.START);
#endif

            statisticRecords = new StatisticRecords();
            statisticsRecords.AddOrUpdate("stat", statisticRecords, (oldkey, oldvalue) => statisticRecords);

            entityCollectionPagination = new PaginationRetriveMultiple();
            DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.accountsProcessing, _thisSettings.TargetEntity.Id);
            do
            {
                entityCollectionPagination = GetDataByEntityName(_orgService, "account", configObject, _thisSettings.TargetEntity.Id, Logger, false, entityCollectionPagination.pageNumber, entityCollectionPagination.PagingCookie, entityCollectionPagination.MoreRecords, _thisSettings.GlobalPageRecordsCount);
                if (entityCollectionPagination != null && entityCollectionPagination.RetrieveCollection.Entities.Count > 0)
                {
                    statisticsRecords["stat"].All += entityCollectionPagination.RetrieveCollection.Entities.Count;
                    CreateRecordSet(_orgService, configObject, AccountEntities, entityCollectionPagination.RetrieveCollection, statisticsRecords["stat"], Logger2, _thisSettings);

                }
            }
            while (entityCollectionPagination != null && entityCollectionPagination.MoreRecords);
            if (statisticsRecords["stat"].All > 0)
            {
                Logger2.Add(GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";");

#if RELESEPLUGIN
#else
                wsMessageService.SendMessage(MessageType.SUCCESS, "Accounts: " + GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";", MessageStatus.FINISH);
#endif

                DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger2, "ddsm_log1");
            }
        }

        private void CreateRecordSet(IOrganizationService _orgService, ConcurrentDictionary<string, EntityJson> objects, ConcurrentDictionary<string, CreatedEntity> AccountEntities, EntityCollection table, StatisticRecords statisticRecords, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "account").Key;
            string EntityUniqueID = string.Empty;
            string addressComposite = string.Empty;

            Int64 requestsCount = _thisSettings.GlobalRequestsCount, itemsRequestCount = 0, requestSets = 1;

            int EntityDataRow = 0, insertDataRow = 0, updateDataRow = 0, doNothing = 0;
            string uniqueIdColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " GUID").ToLower()).Key;
            if (string.IsNullOrEmpty(uniqueIdColumnKey)) return;

            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            List<string> setUniqueID = new List<string>();
            foreach (Entity dr in table.Entities)
            {

                EntityDataRow++;
                if (EntityDataRow >= 1)
                {
                    try
                    {
                        EntityUniqueID = string.Empty;

                        EntityUniqueID = dr["ddsm_" + uniqueIdColumnKey.ToLower()].ToString();
                        if (string.IsNullOrEmpty(EntityUniqueID)) continue;

                        //Verify duplicate record GUID
                        if (AccountEntities.ContainsKey(EntityUniqueID))
                        {
                            Logger.Add("ERROR >>> GUID: " + EntityUniqueID + " Not unique in sheet Accounts list");

                            continue;
                        }

                        Entity newEntiy = GenerationEntity(_orgService, objects, keyEntity, dr, _thisSettings.TargetEntity.Id);
                        if (newEntiy.Attributes.Count == 0) continue;
                        if (!newEntiy.Attributes.ContainsKey("transactioncurrencyid") && _thisSettings.CurrencyGuid != Guid.Empty) {
                            newEntiy["transactioncurrencyid"] = new EntityReference("transactioncurrency", _thisSettings.CurrencyGuid);
                        }

                        if (newEntiy.Attributes.Contains("name") && !string.IsNullOrEmpty(newEntiy["name"].ToString())) { }
                        else
                            if ((newEntiy.Attributes.Contains("ddsm_accounttype") && newEntiy["ddsm_accounttype"] != null && newEntiy.GetAttributeValue<OptionSetValue>("ddsm_accounttype").Value != 962080002) && (newEntiy.Attributes.Contains("ddsm_companyname") && newEntiy["ddsm_companyname"] != null && newEntiy["ddsm_companyname"].ToString() != ""))
                            newEntiy["name"] = newEntiy["ddsm_companyname"];
                        else
                            newEntiy["name"] = ((newEntiy["ddsm_firstname"] != null) ? newEntiy["ddsm_firstname"].ToString() : "") + " " + ((newEntiy["ddsm_lastname"] != null) ? newEntiy["ddsm_lastname"].ToString() : "");

                        newEntiy["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);
                        newEntiy["ddsm_typeofuploadeddata"] = new OptionSetValue(_thisSettings.TypeConfig);
                        //Business //Partner
                        if (newEntiy.Attributes.ContainsKey("ddsm_accounttype") && newEntiy["ddsm_accounttype"] != null && newEntiy.GetAttributeValue<OptionSetValue>("ddsm_accounttype").Value != (int)AccountTypes.Residential)
                        {

                            if (newEntiy.Attributes.ContainsKey("ddsm_accountprimarykey") && !string.IsNullOrEmpty(newEntiy["ddsm_accountprimarykey"].ToString()))
                            {
                                newEntiy["ddsm_accountprimarykey"] = (newEntiy.GetAttributeValue<string>("ddsm_accountprimarykey")).ToLower();
                            }
                            else
                            {
                                addressComposite = string.Empty;
                                addressComposite = ((newEntiy.Attributes.ContainsKey("address1_line1")) ? newEntiy.GetAttributeValue<string>("address1_line1") : "")
                                    + ((newEntiy.Attributes.ContainsKey("address1_line2")) ? newEntiy.GetAttributeValue<string>("address1_line2") : "")
                                    + ((newEntiy.Attributes.ContainsKey("address1_city")) ? newEntiy.GetAttributeValue<string>("address1_city") : "");
                                newEntiy["ddsm_accountprimarykey"] = GetBusinessAccPrimaryKey((newEntiy.Attributes.ContainsKey("ddsm_companyname")) ? newEntiy.GetAttributeValue<string>("ddsm_companyname") : "", addressComposite);
                            }
                        }
                        else
                        {
                            //Residential
                            if (newEntiy.Attributes.ContainsKey("ddsm_accountprimarykey") && !string.IsNullOrEmpty(newEntiy["ddsm_accountprimarykey"].ToString()))
                            {
                                newEntiy["ddsm_accountprimarykey"] = (newEntiy.GetAttributeValue<string>("ddsm_accountprimarykey")).ToLower();
                            }
                            else
                            {
                                newEntiy["ddsm_accountprimarykey"] = GetResidentialAccPrimaryKey((newEntiy.Attributes.ContainsKey("ddsm_lastname")) ? newEntiy.GetAttributeValue<string>("ddsm_lastname") : "", (newEntiy.Attributes.ContainsKey("ddsm_firstname")) ? newEntiy.GetAttributeValue<string>("ddsm_firstname") : "", (newEntiy.Attributes.ContainsKey("telephone1")) ? newEntiy.GetAttributeValue<string>("telephone1") : "");
                            }
                            newEntiy["ddsm_accounttype"] = new OptionSetValue(962080002);
                        }

                        //Duplicate detection
                        string duplicateRecordId = string.Empty;
                        if (_thisSettings.DuplicateDetect)
                        {
                            duplicateRecordId = VerifyDuplicateRecord(_orgService, newEntiy, objects[keyEntity].Name.ToString().ToLower());
                        }

                        CreatedEntity AccEntity = new CreatedEntity();
                        Boolean Successful = false;
                        if (!string.IsNullOrEmpty(duplicateRecordId))
                        {

                            string[] reqDuplicate = Regex.Split(duplicateRecordId, "::");
                            AccEntity.EntityGuid = new Guid(reqDuplicate[0]);
                            AccEntity.Name = reqDuplicate[1];
                            AccEntity.PrimaryKey = newEntiy.GetAttributeValue<string>("ddsm_accountprimarykey");
                            AccEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            AccEntity.Status = false;
                            AccEntity.StatusDB = false;

                            AccountEntities.AddOrUpdate(EntityUniqueID, AccEntity, (oldkey, oldvalue) => AccEntity);

                            switch (_thisSettings.DedupRules)
                            {
                                case 962080000: //Update
                                    if (newEntiy.Attributes.Count > 0)
                                    {
                                        Entity updateEntity = new Entity("account", AccountEntities[EntityUniqueID].EntityGuid);

                                        ColumnSet colsSet = new ColumnSet();
                                        foreach (var Attribute in newEntiy.Attributes)
                                        {
                                            colsSet.AddColumn(Attribute.Key);
                                        }

                                        if (colsSet.Columns.Count > 0)
                                        {
                                            Entity accountAttributes = GetEntityAttrsValue(_orgService, AccEntity.EntityGuid, "account", colsSet);

                                            if (accountAttributes.Attributes.Count > 0)
                                            {
                                                foreach (KeyValuePair<String, Object> Attribute in newEntiy.Attributes)
                                                {
                                                    if (accountAttributes.Attributes.Contains(Attribute.Key))
                                                    {
                                                        var _comparing = ComparingAttributeValues(newEntiy.Attributes[Attribute.Key], accountAttributes.Attributes[Attribute.Key]);

                                                        if (_comparing)
                                                        {
                                                            updateEntity[Attribute.Key] = newEntiy.Attributes[Attribute.Key];
                                                        }
                                                    }
                                                    else
                                                    {
                                                        updateEntity[Attribute.Key] = newEntiy.Attributes[Attribute.Key];
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                foreach (KeyValuePair<String, Object> Attribute in newEntiy.Attributes)
                                                {
                                                    updateEntity[Attribute.Key] = newEntiy.Attributes[Attribute.Key];
                                                }
                                            }
                                        }

                                        if (updateEntity.Attributes.Count > 0)
                                        {
                                            UpdateRequest updateRequest = new UpdateRequest();
                                            updateRequest.Target = updateEntity;
                                            emRequest.Requests.Add(updateRequest);
                                            setUniqueID.Add(EntityUniqueID);
                                            itemsRequestCount++;
                                            //statisticRecords.Updated++;
                                        }
                                        else
                                        {
                                            AccountEntities[EntityUniqueID].Status = true;
                                            SkippedEntities.AddOrUpdate(EntityUniqueID, AccountEntities[EntityUniqueID], (oldkey, oldvalue) => AccountEntities[EntityUniqueID]);
                                            statisticRecords.DoNothing++;

                                            itemsRequestCount++;
                                        }

                                    }
                                    else
                                    {
                                        AccountEntities[EntityUniqueID].Status = true;
                                        SkippedEntities.AddOrUpdate(EntityUniqueID, AccountEntities[EntityUniqueID], (oldkey, oldvalue) => AccountEntities[EntityUniqueID]);
                                        statisticRecords.DoNothing++;

                                        itemsRequestCount++;
                                    }
                                    break;

                                case 962080001:
                                    AccountEntities[EntityUniqueID].Status = true;
                                    SkippedEntities.AddOrUpdate(EntityUniqueID, AccountEntities[EntityUniqueID], (oldkey, oldvalue) => AccountEntities[EntityUniqueID]);
                                    statisticRecords.DoNothing++;

                                    itemsRequestCount++;
                                    break;
                            }

                        }
                        else
                        {
                            //AutoNumbering only on create
                            if (newEntiy.Attributes.ContainsKey("accountnumber") && !string.IsNullOrEmpty(newEntiy["accountnumber"].ToString())) { }
                            else
                                newEntiy["accountnumber"] = GetAutoNumber("account", _orgService);

                            if (_thisSettings.TypeConfig == (int)TypeProjectProgram.ARET)
                                newEntiy["ddsm_payeenumber"] = newEntiy["accountnumber"];

                            CreateRequest createRequest = new CreateRequest();
                            createRequest.Target = newEntiy;
                            emRequest.Requests.Add(createRequest);
                            setUniqueID.Add(EntityUniqueID);
                            itemsRequestCount++;

                            AccEntity.EntityGuid = new Guid();
                            AccEntity.Name = newEntiy["name"].ToString();
                            AccEntity.PrimaryKey = newEntiy.GetAttributeValue<string>("ddsm_accountprimarykey");
                            AccEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            AccEntity.Status = false;
                            AccEntity.StatusDB = false;

                            //AccountEntities.Add(EntityUniqueID, AccEntity);
                            AccountEntities.AddOrUpdate(EntityUniqueID, AccEntity, (oldkey, oldvalue) => AccEntity);

                        }

                    }
                    catch (Exception e)
                    {
                        itemsRequestCount++;

                        Logger.Add("ERROR >>> UniqueGUID: " + EntityUniqueID + ";; " + e.Message);
#if RELESEPLUGIN
#else
                        wsMessageService.SendMessage(MessageType.ERROR, "Account UniqueGUID: " + EntityUniqueID + "; Message: " + e.Message, MessageStatus.FINISH);
#endif
                    }
                }

                //Create Multiple
                if (itemsRequestCount == (requestSets * requestsCount))
                {
                    if (emRequest.Requests.Count > 0)
                    {
                        ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                        CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, AccountEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                        //Update Source records
                        UpdateSourceRecords(_orgService, objects, RequestEntities, "account", _thisSettings.CallExecuteMultiple);
                    }

                    //Update Source records (Skipped)
                    if (SkippedEntities.Count > 0)
                    {
                        UpdateSourceRecords(_orgService, objects, SkippedEntities, "account");
                    }
                    SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();

                    requestSets++;

                    setUniqueID = new List<string>();
                    emRequest = new ExecuteMultipleRequest
                    {
                        Requests = new OrganizationRequestCollection(),
                        Settings = new ExecuteMultipleSettings
                        {
                            ContinueOnError = true,
                            ReturnResponses = true
                        }
                    };

                }

            }
            if (setUniqueID.Count > 0 && emRequest.Requests.Count > 0)
            {
                ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, AccountEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                //Update Source records
                UpdateSourceRecords(_orgService, objects, RequestEntities, "account", _thisSettings.CallExecuteMultiple);
            }

            if (SkippedEntities.Count > 0)
            {
                //Update Source records (Skipped)
                UpdateSourceRecords(_orgService, objects, SkippedEntities, "account", _thisSettings.CallExecuteMultiple);
                SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
            }

        }

    }
}
