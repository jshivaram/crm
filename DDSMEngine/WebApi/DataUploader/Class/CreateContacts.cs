﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataUploader;
using static DataUploader.Model.PrimaryKey;
using static DataUploader.Model.Enums;
using static DataUploader.Model.Errors;
using System.Text.RegularExpressions;
using Microsoft.Xrm.Sdk.Query;

#if RELESEPLUGIN
#else
using SocketClients;
using DataUploader.Service;
#endif

namespace DataUploader.Creator
{
    public class CreateContacts : BaseMethod
    {
        //Create New
        public void CreateContact(IOrganizationService _orgService, ConcurrentDictionary<string, CreatedEntity> ContactEntities, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            Logger2 = new List<string>();
            configObject = Newtonsoft.Json.JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);

            var keyEntity = configObject.FirstOrDefault(x => x.Value.Name.ToLower() == "contact").Key;
            if (string.IsNullOrEmpty(keyEntity))
            {
                return;
            }

#if RELESEPLUGIN
#else
            wsMessageService = new WSMessageUploaderService(_thisSettings);
            wsMessageService.SendMessage(MessageType.INFORMATION, GetTextError(380030), MessageStatus.START);
#endif

            statisticRecords = new StatisticRecords();
            statisticsRecords.AddOrUpdate("stat", statisticRecords, (oldkey, oldvalue) => statisticRecords);

            entityCollectionPagination = new PaginationRetriveMultiple();
            DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.contactsProcessing, _thisSettings.TargetEntity.Id);
            do
            {
                entityCollectionPagination = GetDataByEntityName(_orgService, "contact", configObject, _thisSettings.TargetEntity.Id, Logger, false, entityCollectionPagination.pageNumber, entityCollectionPagination.PagingCookie, entityCollectionPagination.MoreRecords, _thisSettings.GlobalPageRecordsCount);
                if (entityCollectionPagination != null && entityCollectionPagination.RetrieveCollection.Entities.Count > 0)
                {
                    statisticsRecords["stat"].All += entityCollectionPagination.RetrieveCollection.Entities.Count;
                    CreateRecordSet(_orgService, configObject, ContactEntities, entityCollectionPagination.RetrieveCollection, statisticsRecords["stat"], Logger2, _thisSettings);
                }
            }
            while (entityCollectionPagination != null && entityCollectionPagination.MoreRecords);
            if (statisticsRecords["stat"].All > 0)
            {
                Logger2.Add(GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";");

#if RELESEPLUGIN
#else
                wsMessageService.SendMessage(MessageType.SUCCESS, "Contacts: " + GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";", MessageStatus.FINISH);
#endif

                DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger2, "ddsm_log0");
            }

        }

        private void CreateRecordSet(IOrganizationService _orgService, ConcurrentDictionary<string, EntityJson> objects, ConcurrentDictionary<string, CreatedEntity> ContactEntities, EntityCollection table, StatisticRecords statisticRecords, List<string> Logger, DataUploaderSettings _thisSettings)
        {

            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "contact").Key;

            Int64 requestsCount = _thisSettings.GlobalRequestsCount, itemsRequestCount = 0, requestSets = 1;

            int EntityDataRow = 0, insertDataRow = 0, updateDataRow = 0, doNothing = 0;
            string uniqueIdColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " GUID").ToLower()).Key;
            if (string.IsNullOrEmpty(uniqueIdColumnKey)) return;
            string EntityUniqueID = string.Empty;

            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            List<string> setUniqueID = new List<string>();
            foreach (Entity dr in table.Entities)
            {

                EntityDataRow++;
                if (EntityDataRow >= 1)
                {
                    try
                    {

                        EntityUniqueID = string.Empty;

                        EntityUniqueID = dr["ddsm_" + uniqueIdColumnKey.ToLower()].ToString();
                        if (string.IsNullOrEmpty(EntityUniqueID)) continue;

                        //Verify duplicate record GUID
                        if (ContactEntities.ContainsKey(EntityUniqueID))
                        {
                            Logger.Add("ERROR > " + EntityUniqueID + " Not unique in sheet Contacts list");
                            continue;
                        }

                        Entity newEntiy = GenerationEntity(_orgService, objects, keyEntity, dr, _thisSettings.TargetEntity.Id);
                        if (newEntiy.Attributes.Count == 0) return;
                        if (!newEntiy.Attributes.ContainsKey("transactioncurrencyid") && _thisSettings.CurrencyGuid != Guid.Empty)
                        {
                            newEntiy["transactioncurrencyid"] = new EntityReference("transactioncurrency", _thisSettings.CurrencyGuid);
                        }

                        if (newEntiy.Attributes.Contains("fullname") && !string.IsNullOrEmpty(newEntiy["fullname"].ToString())) { }
                        else
                        {
                            if (newEntiy.Attributes.Contains("firstname") && newEntiy.Attributes.Contains("lastname")) 
                            {
                                newEntiy["fullname"] = newEntiy["firstname"].ToString() + " " + newEntiy["lastname"].ToString();
                            }
                            else if (newEntiy.Attributes.Contains("firstname"))
                            {
                                newEntiy["fullname"] = newEntiy["firstname"].ToString();
                            }
                            else if (newEntiy.Attributes.Contains("lastname"))
                            {
                                newEntiy["fullname"] = newEntiy["lastname"].ToString();
                            }
                        }

                        newEntiy["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);

                        if (newEntiy.Attributes.ContainsKey("ddsm_contactprimarykey") && !string.IsNullOrEmpty(newEntiy["ddsm_contactprimarykey"].ToString()))
                        {
                            newEntiy["ddsm_contactprimarykey"] = (newEntiy.GetAttributeValue<string>("ddsm_contactprimarykey")).ToLower();
                        }
                        else
                            newEntiy["ddsm_contactprimarykey"] = GetContactPrimaryKey((newEntiy.Attributes.ContainsKey("lastname")) ? newEntiy.GetAttributeValue<string>("lastname") : "", (newEntiy.Attributes.ContainsKey("firstname")) ? newEntiy.GetAttributeValue<string>("firstname") : "", (newEntiy.Attributes.ContainsKey("address1_telephone1")) ? newEntiy.GetAttributeValue<string>("address1_telephone1") : "");

                        //Duplicate detection
                        string duplicateRecordId = string.Empty;
                        if (_thisSettings.DuplicateDetect)
                        {
                            duplicateRecordId = VerifyDuplicateRecord(_orgService, newEntiy, objects[keyEntity].Name.ToString().ToLower());
                        }

                        CreatedEntity ContEntity = new CreatedEntity();
                        Boolean Successful = false;
                        if (!string.IsNullOrEmpty(duplicateRecordId))
                        {

                            string[] reqDuplicate = Regex.Split(duplicateRecordId, "::");
                            ContEntity.EntityGuid = new Guid(reqDuplicate[0]);
                            ContEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            ContEntity.Status = false;
                            ContEntity.StatusDB = false;
                            ContEntity.Name = reqDuplicate[1];

                            ContactEntities.AddOrUpdate(EntityUniqueID, ContEntity, (oldkey, oldvalue) => ContEntity);

                            switch (_thisSettings.DedupRules)
                            {
                                case 962080000: //Update

                                    if (newEntiy.Attributes.Count > 0)
                                    {
                                        Entity updateEntity = new Entity("contact", ContactEntities[EntityUniqueID].EntityGuid);

                                        ColumnSet colsSet = new ColumnSet();
                                        foreach (var Attribute in newEntiy.Attributes)
                                        {
                                            colsSet.AddColumn(Attribute.Key);
                                        }
                                        if (colsSet.Columns.Count > 0)
                                        {
                                            Entity contactAttributes = GetEntityAttrsValue(_orgService, ContEntity.EntityGuid, "contact", colsSet);

                                            if (contactAttributes.Attributes.Count > 0)
                                            {
                                                foreach (KeyValuePair<String, Object> Attribute in newEntiy.Attributes)
                                                {

                                                    if (contactAttributes.Attributes.Contains(Attribute.Key))
                                                    {
                                                        var _comparing = ComparingAttributeValues(newEntiy.Attributes[Attribute.Key], contactAttributes.Attributes[Attribute.Key]);

                                                        if (_comparing)
                                                        {
                                                            updateEntity[Attribute.Key] = newEntiy.Attributes[Attribute.Key];
                                                        }
                                                    }
                                                    else
                                                    {
                                                        updateEntity[Attribute.Key] = newEntiy.Attributes[Attribute.Key];
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                foreach (KeyValuePair<String, Object> Attribute in newEntiy.Attributes)
                                                {
                                                    updateEntity[Attribute.Key] = newEntiy.Attributes[Attribute.Key];
                                                }
                                            }

                                            if (updateEntity.Attributes.Count > 0)
                                            {
                                                UpdateRequest updateRequest = new UpdateRequest();
                                                updateRequest.Target = updateEntity;
                                                emRequest.Requests.Add(updateRequest);
                                                setUniqueID.Add(EntityUniqueID);
                                                itemsRequestCount++;
                                                //statisticRecords.Updated++;
                                            }
                                            else
                                            {
                                                ContactEntities[EntityUniqueID].Status = true;
                                                SkippedEntities.AddOrUpdate(EntityUniqueID, ContactEntities[EntityUniqueID], (oldkey, oldvalue) => ContactEntities[EntityUniqueID]);
                                                statisticRecords.DoNothing++;

                                                itemsRequestCount++;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ContactEntities[EntityUniqueID].Status = true;
                                        SkippedEntities.AddOrUpdate(EntityUniqueID, ContactEntities[EntityUniqueID], (oldkey, oldvalue) => ContactEntities[EntityUniqueID]);
                                        statisticRecords.DoNothing++;

                                        itemsRequestCount++;
                                    }
                                    break;

                                case 962080001:
                                    ContactEntities[EntityUniqueID].Status = true;
                                    SkippedEntities.AddOrUpdate(EntityUniqueID, ContactEntities[EntityUniqueID], (oldkey, oldvalue) => ContactEntities[EntityUniqueID]);
                                    statisticRecords.DoNothing++;

                                    itemsRequestCount++;
                                    break;
                            }

                        }
                        else
                        {

                            CreateRequest createRequest = new CreateRequest();
                            createRequest.Target = newEntiy;
                            emRequest.Requests.Add(createRequest);
                            setUniqueID.Add(EntityUniqueID);
                            itemsRequestCount++;

                            ContEntity.EntityGuid = new Guid();
                            ContEntity.Name = newEntiy["fullname"].ToString();
                            ContEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            ContEntity.Status = false;
                            ContEntity.StatusDB = false;

                            //ContactEntities.Add(EntityUniqueID, ContEntity);
                            ContactEntities.AddOrUpdate(EntityUniqueID, ContEntity, (oldkey, oldvalue) => ContEntity);
                        }

                    }
                    catch (Exception e)
                    {
                        itemsRequestCount++;

                        Logger.Add("ERROR > UniqueGUID: " + EntityUniqueID + "; Message: " + e.Message);
#if RELESEPLUGIN
#else
                        wsMessageService.SendMessage(MessageType.ERROR, "Contact UniqueGUID: " + EntityUniqueID + "; Message: " + e.Message, MessageStatus.FINISH);
#endif

                    }

                }

                //Create Multiple
                if (itemsRequestCount == (requestSets * requestsCount))
                {
                    if (emRequest.Requests.Count > 0)
                    {
                        ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                        CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, ContactEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                        //Update Source records
                        UpdateSourceRecords(_orgService, objects, RequestEntities, "contact", _thisSettings.CallExecuteMultiple);
                    }

                    //Update Source records (Skipped)
                    if (SkippedEntities.Count > 0)
                    {
                        UpdateSourceRecords(_orgService, objects, SkippedEntities, "contact", _thisSettings.CallExecuteMultiple);
                    }
                    SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();

                    requestSets++;

                    setUniqueID = new List<string>();
                    emRequest = new ExecuteMultipleRequest
                    {
                        Requests = new OrganizationRequestCollection(),
                        Settings = new ExecuteMultipleSettings
                        {
                            ContinueOnError = true,
                            ReturnResponses = true
                        }
                    };

                }

            }
            if (setUniqueID.Count > 0 && emRequest.Requests.Count > 0)
            {
                ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, ContactEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                //Update Source records
                UpdateSourceRecords(_orgService, objects, RequestEntities, "contact", _thisSettings.CallExecuteMultiple);
            }

            if (SkippedEntities.Count > 0)
            {
                //Update Source records (Skipped)
                UpdateSourceRecords(_orgService, objects, SkippedEntities, "contact", _thisSettings.CallExecuteMultiple);
                SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
            }
        }

        //Update CompanyName (Account) field
        public void UpdateCompanyName(IOrganizationService _orgService, DataUploaderSettings _thisSettings)
        {
            var objects = Newtonsoft.Json.JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);

            if (!string.IsNullOrEmpty(objects.FirstOrDefault(x => x.Value.Name.ToLower() == "contact").Key) && !string.IsNullOrEmpty(objects[objects.FirstOrDefault(x => x.Value.Name.ToLower() == "contact").Key].Attrs.FirstOrDefault(y => (y.Value.Name.ToLower()).Split(':')[0] == ("parentcustomerid").ToLower()).Key))
            {
                entityCollectionPagination = new PaginationRetriveMultiple();
                do
                {
                    List<string> Logger = new List<string>();
                    entityCollectionPagination = GetDataByEntityName(_orgService, "contact", objects, _thisSettings.TargetEntity.Id, Logger, true, entityCollectionPagination.pageNumber, entityCollectionPagination.PagingCookie, entityCollectionPagination.MoreRecords, _thisSettings.GlobalPageRecordsCount);
                    if (entityCollectionPagination != null && entityCollectionPagination.RetrieveCollection.Entities.Count > 0)
                    {
                        UpdateRecordSet(_orgService, objects, entityCollectionPagination.RetrieveCollection, Logger, _thisSettings);
                    }
                }
                while (entityCollectionPagination != null && entityCollectionPagination.MoreRecords);
            }

        }

        private void UpdateRecordSet(IOrganizationService _orgService, ConcurrentDictionary<string, EntityJson> objects, EntityCollection table, List<string> Logger, DataUploaderSettings _thisSettings)
        {

            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "contact").Key;
            string EntityUniqueID = string.Empty;

            string uniqueIdColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " GUID").ToLower()).Key;
            if (string.IsNullOrEmpty(uniqueIdColumnKey)) return;

            string companyNameColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => (x.Value.Name.ToLower()).Split(':')[0] == ("parentcustomerid").ToLower()).Key;
            if (string.IsNullOrEmpty(companyNameColumnKey)) return;

            Int64 requestsCount = _thisSettings.GlobalRequestsCount, itemsRequestCount = 0, requestSets = 1;

            int EntityDataRow = 0, insertDataRow = 0, updateDataRow = 0, doNothing = 0;


            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            foreach (Entity dr in table.Entities)
            {

                EntityDataRow++;
                if (EntityDataRow >= 1)
                {
                    try
                    {
                        string CompanyNameColumnValue = string.Empty;
                        EntityUniqueID = string.Empty;

                        EntityUniqueID = dr["ddsm_" + uniqueIdColumnKey.ToLower()].ToString();
                        CompanyNameColumnValue = (dr.Attributes.ContainsKey("ddsm_" + companyNameColumnKey.ToLower())) ? dr["ddsm_" + companyNameColumnKey.ToLower()].ToString() : null;

                        if (string.IsNullOrEmpty(EntityUniqueID)) continue;
                        if (string.IsNullOrEmpty(CompanyNameColumnValue)) continue;
                        if (!dr.Attributes.ContainsKey("ddsm_contact")) continue;
                        if (dr.GetAttributeValue<EntityReference>("ddsm_contact").Id == null || dr.GetAttributeValue<EntityReference>("ddsm_contact").Id == Guid.Empty) continue;

                        QueryExpression CompanyQuery = new QueryExpression { EntityName = "account", ColumnSet = new ColumnSet("name", "accountid") };
                        CompanyQuery.Criteria.AddCondition("ddsm_accountprimarykey", ConditionOperator.Equal, CompanyNameColumnValue.ToLower());
                        EntityCollection CompanyRetrieve = _orgService.RetrieveMultiple(CompanyQuery);

                        if (CompanyRetrieve != null && CompanyRetrieve.Entities.Count == 1)
                        {
                            Entity updateContact = new Entity("contact", dr.GetAttributeValue<EntityReference>("ddsm_contact").Id);
                            updateContact["parentcustomerid"] = new EntityReference("account", new Guid(CompanyRetrieve.Entities[0].Attributes["accountid"].ToString()));

                            //_orgService.Update(updateContact);

                            UpdateRequest updateRequest = new UpdateRequest();
                            updateRequest.Target = updateContact;
                            emRequest.Requests.Add(updateRequest);
                            itemsRequestCount++;
                            updateDataRow++;
                        }
                        else if (CompanyRetrieve == null)
                        {
                            CompanyQuery = new QueryExpression { EntityName = "account", ColumnSet = new ColumnSet("name", "accountid") };
                            CompanyQuery.Criteria.AddCondition("name", ConditionOperator.Equal, CompanyNameColumnValue);
                            CompanyRetrieve = _orgService.RetrieveMultiple(CompanyQuery);

                            if (CompanyRetrieve != null && CompanyRetrieve.Entities.Count == 1)
                            {
                                Entity updateContact = new Entity("contact", dr.GetAttributeValue<EntityReference>("ddsm_contact").Id);
                                updateContact["parentcustomerid"] = new EntityReference("account", new Guid(CompanyRetrieve.Entities[0].Attributes["accountid"].ToString()));

                                //_orgService.Update(updateContact);

                                UpdateRequest updateRequest = new UpdateRequest();
                                updateRequest.Target = updateContact;
                                emRequest.Requests.Add(updateRequest);
                                itemsRequestCount++;
                                updateDataRow++;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Add("ERROR > UniqueGUID: " + EntityUniqueID + ";; " + e.Message);
                    }
                }


                //Create Multiple

                if (itemsRequestCount == (requestSets * requestsCount))
                {
                    int indexReq = -1;

                    if (!_thisSettings.CallExecuteMultiple)
                    {
                        //Single
                        foreach (var req in emRequest.Requests)
                        {
                            indexReq++;
                            try
                            {

                                if (req.RequestName == "Update")
                                {
                                    _orgService.Update(((UpdateRequest)req).Target);
                                    statisticRecords.Updated++;
                                }
                            }
                            catch (Exception e)
                            {
                                Logger.Add("Error: Name: " + emRequest.Requests[indexReq].RequestName + " Index: " + (indexReq + 1) + " Message: " + e.Message);
#if RELESEPLUGIN
#else
                                wsMessageService.SendMessage(MessageType.ERROR, "Error: Name: " + emRequest.Requests[indexReq].RequestName + " Index: " + (indexReq + 1) + " Message: " + e.Message, MessageStatus.FINISH);
#endif

                            }
                        }

                    }
                    else
                    {
                        ExecuteMultipleResponse emResponse = (ExecuteMultipleResponse)_orgService.Execute(emRequest);

                        foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                        {
                            // A valid response.
                            if (responseItem.Response != null)
                            {
                                //Logger.Add("Name " + ((Entity)emRequest.Requests[responseItem.RequestIndex].Parameters["Target"]).GetAttributeValue<string>("name")


                                if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                                {
                                    insertDataRow++;

                                    //Logger.Add(" Id: " + ((OrganizationResponse)responseItem.Response).Results["id"].ToString());
                                }
                            }

                            // An error has occurred.
                            else if (responseItem.Fault != null)
                            {
                                Logger.Add("Error: Name: " + emRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message);

#if RELESEPLUGIN
#else
                                wsMessageService.SendMessage(MessageType.ERROR, "Error: Name: " + emRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message, MessageStatus.FINISH);
#endif

                            }
                        }
                    }

                    requestSets++;
                    emRequest = new ExecuteMultipleRequest
                    {
                        Requests = new OrganizationRequestCollection(),
                        Settings = new ExecuteMultipleSettings
                        {
                            ContinueOnError = true,
                            ReturnResponses = true
                        }
                    };

                }

            }


            if (emRequest.Requests.Count > 0)
            {

                int indexReq = -1;

                if (!_thisSettings.CallExecuteMultiple)
                {
                    //Single
                    foreach (var req in emRequest.Requests)
                    {
                        indexReq++;
                        try
                        {

                            if (req.RequestName == "Update")
                            {
                                _orgService.Update(((UpdateRequest)req).Target);
                                statisticRecords.Updated++;
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.Add("Error: Name: " + emRequest.Requests[indexReq].RequestName + " Index: " + (indexReq + 1) + " Message: " + e.Message);
#if RELESEPLUGIN
#else
                            wsMessageService.SendMessage(MessageType.ERROR, "Error: Name: " + emRequest.Requests[indexReq].RequestName + " Index: " + (indexReq + 1) + " Message: " + e.Message, MessageStatus.FINISH);
#endif

                        }
                    }

                }
                else
                {
                    ExecuteMultipleResponse emResponse = (ExecuteMultipleResponse)_orgService.Execute(emRequest);
                    //Logger.Add("Set Inserts");
                    foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                    {
                        // A valid response.
                        if (responseItem.Response != null)
                        {
                            //Logger.Add("Name " + ((Entity)emRequest.Requests[responseItem.RequestIndex].Parameters["Target"]).GetAttributeValue<string>("ddsm_name"));

                            if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                            {
                                insertDataRow++;
                                //Logger.Add(" Id: " + ((OrganizationResponse)responseItem.Response).Results["id"].ToString());
                            }
                        }

                        // An error has occurred.
                        else if (responseItem.Fault != null)
                        {
                            Logger.Add("Error: Name: " + emRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message);

#if RELESEPLUGIN
#else
                            wsMessageService.SendMessage(MessageType.ERROR, "Error: Name: " + emRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message, MessageStatus.FINISH);
#endif

                        }
                    }
                }


            }

        }

    }

}
