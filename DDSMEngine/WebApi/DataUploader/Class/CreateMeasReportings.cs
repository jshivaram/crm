﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataUploader;
using static DataUploader.Model.Enums;
using static DataUploader.Model.Errors;
using static DataUploader.Model.PrimaryKey;
using static DataUploader.Model.AutoNumberingManager;
using Microsoft.Xrm.Sdk.Messages;
using System.Text.RegularExpressions;
using System.Collections.Concurrent;
using Microsoft.Xrm.Sdk;

#if RELESEPLUGIN
#else
using SocketClients;
using DataUploader.Service;
#endif

namespace DataUploader.Creator
{
    public class CreateMeasReportings : BaseMethod
    {
        public void CreateMeasReporting(IOrganizationService _orgService, ConcurrentDictionary<string, CreatedEntity> ProjectEntities, ConcurrentDictionary<string, CreatedEntity> MeasureEntities, ConcurrentDictionary<string, CreatedEntity> MeasReportEntities, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            Logger2 = new List<string>();
            configObject = Newtonsoft.Json.JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);

            var keyEntity = configObject.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_measurereporting").Key;
            if (string.IsNullOrEmpty(keyEntity))
            {
                return;
            }

#if RELESEPLUGIN
#else
            wsMessageService = new WSMessageUploaderService(_thisSettings);
            wsMessageService.SendMessage(MessageType.INFORMATION, GetTextError(380036), MessageStatus.START);
#endif

            statisticRecords = new StatisticRecords();
            statisticsRecords.AddOrUpdate("stat", statisticRecords, (oldkey, oldvalue) => statisticRecords);

            entityCollectionPagination = new PaginationRetriveMultiple();
            DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.measureReportingsProcessing, _thisSettings.TargetEntity.Id);
            do
            {
                entityCollectionPagination = GetDataByEntityName(_orgService, "ddsm_measurereporting", configObject, _thisSettings.TargetEntity.Id, Logger, false, entityCollectionPagination.pageNumber, entityCollectionPagination.PagingCookie, entityCollectionPagination.MoreRecords, _thisSettings.GlobalPageRecordsCount);
                if (entityCollectionPagination != null && entityCollectionPagination.RetrieveCollection.Entities.Count > 0)
                {
                    statisticsRecords["stat"].All += entityCollectionPagination.RetrieveCollection.Entities.Count;
                    createMeasReportRecords(_orgService, configObject, ProjectEntities, MeasureEntities, MeasReportEntities, entityCollectionPagination.RetrieveCollection, statisticsRecords["stat"], Logger2, _thisSettings);
                }
            }
            while (entityCollectionPagination != null && entityCollectionPagination.MoreRecords);
            if (statisticsRecords["stat"].All > 0)
            {
                Logger2.Add(GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";");
#if RELESEPLUGIN
#else
                wsMessageService.SendMessage(MessageType.SUCCESS, "Measure Reporting: " + GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";", MessageStatus.FINISH);
#endif
                DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger2, "ddsm_log7");
            }

        }

        //Create Measure Reporting Record
        private void createMeasReportRecords(IOrganizationService _orgService, ConcurrentDictionary<string, EntityJson> objects, ConcurrentDictionary<string, CreatedEntity> ProjectEntities, ConcurrentDictionary<string, CreatedEntity> MeasureEntities, ConcurrentDictionary<string, CreatedEntity> MeasReportEntities, EntityCollection table, StatisticRecords statisticRecords, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_measurereporting").Key;
            string EntityUniqueID = string.Empty;

            int EntityDataRow = 0, updateDataRow = 0, insertDataRow = 0, doNothing = 0;
            string uniqueIdColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " GUID").ToLower()).Key;
            //Project Column
            var projectTableName = string.Empty;
            projectTableName = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_project").Key;
            string projectColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (projectTableName + " GUID").ToLower()).Key;

            //Measure Column
            var measureTableName = string.Empty;
            measureTableName = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_measure").Key;
            string measureColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (measureTableName + " GUID").ToLower()).Key;

            if (string.IsNullOrEmpty(uniqueIdColumnKey)) return;
            //if (string.IsNullOrEmpty(projectColumnKey)) return;
            if (string.IsNullOrEmpty(measureColumnKey)) return;
            Int64 requestsCount = _thisSettings.GlobalRequestsCount, itemsRequestCount = 0, requestSets = 1;
            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            List<string> setUniqueID = new List<string>();
            foreach (Entity dr in table.Entities)
            {

                EntityDataRow++;
                if (EntityDataRow >= 1)
                {
                    try
                    {
                        EntityUniqueID = string.Empty;

                        EntityUniqueID = dr["ddsm_" + uniqueIdColumnKey.ToLower()].ToString();
                        if (string.IsNullOrEmpty(EntityUniqueID)) continue;

                        Entity newEntiy = new Entity("ddsm_measurereporting");
                        //Logger.Add(">>>>>");
                        Entity newEntiyXLSX = GenerationEntity(_orgService, objects, keyEntity, dr, _thisSettings.TargetEntity.Id);
                        if (newEntiyXLSX.Attributes.Count == 0) continue;
                        //Logger.Add("<<<<<");
                        foreach (KeyValuePair<String, Object> Attribute in newEntiyXLSX.Attributes)
                        {
                            newEntiy[Attribute.Key] = Attribute.Value;
                            //newEntiy[Attribute.Key] = newEntiyXLSX.GetAttributeValue(Attribute.Key);
                        }

                        if (!string.IsNullOrEmpty(measureColumnKey) && !string.IsNullOrEmpty(Convert.ToString(dr["ddsm_" + measureColumnKey.ToLower()])))
                        {
                            GetDictionaryObjectbyGUID(_orgService, dr["ddsm_" + measureColumnKey.ToLower()].ToString(), "ddsm_measure", objects, MeasureEntities, _thisSettings.TargetEntity.Id);
                            if (MeasureEntities.ContainsKey(Convert.ToString(dr["ddsm_" + measureColumnKey.ToLower()])) && (MeasureEntities[Convert.ToString(dr["ddsm_" + measureColumnKey.ToLower()])].EntityGuid != new Guid() || MeasureEntities[Convert.ToString(dr["ddsm_" + measureColumnKey.ToLower()])].EntityGuid != Guid.Empty))
                            {
                                if (MeasureEntities[dr["ddsm_" + measureColumnKey.ToLower()].ToString()].EntityGuid.ToString() == (new Guid()).ToString()) continue;
                                newEntiy["ddsm_measure"] = new EntityReference("ddsm_measure", MeasureEntities[dr["ddsm_" + measureColumnKey.ToLower()].ToString()].EntityGuid);

                                Entity rmEntity = GetDataRelationMapping(_orgService, "ddsm_measurereporting", "ddsm_measure", MeasureEntities[dr["ddsm_" + measureColumnKey.ToLower()].ToString()].EntityGuid);
                                if (rmEntity != null)
                                {
                                    foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                                    {
                                        newEntiy[Attribute.Key] = Attribute.Value;
                                    }
                                }
                            }
                            else continue;

                        }
                        else continue;

                        if (!string.IsNullOrEmpty(projectColumnKey) && !string.IsNullOrEmpty(Convert.ToString(dr["ddsm_" + projectColumnKey.ToLower()])))
                        {
                            GetDictionaryObjectbyGUID(_orgService, dr["ddsm_" + projectColumnKey.ToLower()].ToString(), "ddsm_project", objects, ProjectEntities, _thisSettings.TargetEntity.Id);
                            if (ProjectEntities.ContainsKey(Convert.ToString(dr["ddsm_" + projectColumnKey.ToLower()])) && (ProjectEntities[Convert.ToString(dr["ddsm_" + projectColumnKey.ToLower()])].EntityGuid != new Guid() || ProjectEntities[Convert.ToString(dr["ddsm_" + projectColumnKey.ToLower()])].EntityGuid != Guid.Empty))
                            {
                                newEntiy["ddsm_project"] = new EntityReference("ddsm_project", ProjectEntities[dr["ddsm_" + projectColumnKey.ToLower()].ToString()].EntityGuid);
                            }
                        }

                        if (!newEntiy.Attributes.Contains("ddsm_name"))
                            newEntiy["ddsm_name"] = MeasureEntities[dr["ddsm_" + measureColumnKey.ToLower()].ToString()].Name;

                        newEntiy["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);

                        string duplicateRecordId = string.Empty;
                        if (_thisSettings.DuplicateDetect)
                        {
                            //duplicateRecordId = VerifyDuplicateRecord(_orgService, newEntiy, objects[keyEntity].Name.ToString().ToLower());
                        }

                        CreatedEntity MeasReportEntity = new CreatedEntity();

                        Boolean Successful = false;
                        if (!string.IsNullOrEmpty(duplicateRecordId))
                        {
                            string[] reqDuplicate = Regex.Split(duplicateRecordId, "::");
                            MeasReportEntity.EntityGuid = new Guid(reqDuplicate[0]);
                            MeasReportEntity.Name = reqDuplicate[1];
                            MeasReportEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            MeasReportEntity.Status = false;
                            MeasReportEntity.StatusDB = false;
                            MeasReportEntities.AddOrUpdate(EntityUniqueID, MeasReportEntity, (oldkey, oldvalue) => MeasReportEntity);

                            switch (_thisSettings.DedupRules)
                            {
                                case 962080000: //Update
                                    if (newEntiy.Attributes.Count > 0)
                                    {
                                        Entity updateEntity = new Entity("ddsm_measurereporting", new Guid(reqDuplicate[0]));
                                        foreach (KeyValuePair<String, Object> Attribute in newEntiy.Attributes)
                                        {
                                            updateEntity[Attribute.Key] = Attribute.Value;
                                        }

                                        UpdateRequest updateRequest = new UpdateRequest();
                                        updateRequest.Target = updateEntity;
                                        emRequest.Requests.Add(updateRequest);
                                        setUniqueID.Add(EntityUniqueID);
                                        itemsRequestCount++;
                                        //statisticRecords.Updated++;
                                    }
                                    else
                                    {
                                        MeasReportEntities[EntityUniqueID].Status = true;
                                        SkippedEntities.AddOrUpdate(EntityUniqueID, MeasReportEntities[EntityUniqueID], (oldkey, oldvalue) => MeasReportEntities[EntityUniqueID]);
                                        statisticRecords.DoNothing++;

                                        itemsRequestCount++;
                                    }
                                    break;
                                case 962080001:
                                    MeasReportEntities[EntityUniqueID].Status = true;
                                    SkippedEntities.AddOrUpdate(EntityUniqueID, MeasReportEntities[EntityUniqueID], (oldkey, oldvalue) => MeasReportEntities[EntityUniqueID]);
                                    statisticRecords.DoNothing++;

                                    itemsRequestCount++;
                                    break;
                            }
                        }
                        else
                        {

                            CreateRequest createRequest = new CreateRequest();
                            createRequest.Target = newEntiy;
                            emRequest.Requests.Add(createRequest);
                            setUniqueID.Add(EntityUniqueID);
                            itemsRequestCount++;

                            MeasReportEntity.EntityGuid = new Guid();
                            MeasReportEntity.Name = newEntiy["ddsm_name"].ToString();
                            MeasReportEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            MeasReportEntity.Status = false;
                            MeasReportEntity.StatusDB = false;
                            //MeasReportEntities.Add(EntityUniqueID, MeasReportEntity);
                            MeasReportEntities.AddOrUpdate(EntityUniqueID, MeasReportEntity, (oldkey, oldvalue) => MeasReportEntity);

                        }
                    }
                    catch (Exception e)
                    {
                        itemsRequestCount++;

                        Logger.Add("ERROR >>> UniqueGUID: " + EntityUniqueID + ";; " + e.Message);
#if RELESEPLUGIN
#else
                        wsMessageService.SendMessage(MessageType.ERROR, "Meagure Reporting UniqueGUID: " + EntityUniqueID + "; Message: " + e.Message, MessageStatus.FINISH);
#endif
                    }
                }

                //Create Multiple
                if (itemsRequestCount == (requestSets * requestsCount))
                {
                    if (emRequest.Requests.Count > 0)
                    {
                        ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                        CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, MeasReportEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                        //Update Source records
                        UpdateSourceRecords(_orgService, objects, RequestEntities, "ddsm_measurereporting", _thisSettings.CallExecuteMultiple);
                    }

                    //Update Source records (Skipped)
                    if (SkippedEntities.Count > 0)
                    {
                        UpdateSourceRecords(_orgService, objects, SkippedEntities, "ddsm_measurereporting", _thisSettings.CallExecuteMultiple);
                    }
                    SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();

                    requestSets++;
                    emRequest = new ExecuteMultipleRequest
                    {
                        Requests = new OrganizationRequestCollection(),
                        Settings = new ExecuteMultipleSettings
                        {
                            ContinueOnError = true,
                            ReturnResponses = true
                        }
                    };

                }
            }

            if (emRequest.Requests.Count > 0 && emRequest.Requests.Count > 0)
            {
                ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, MeasReportEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                //Update Source records
                UpdateSourceRecords(_orgService, objects, RequestEntities, "ddsm_measurereporting", _thisSettings.CallExecuteMultiple);
            }

            if (SkippedEntities.Count > 0)
            {
                //Update Source records (Skipped)
                UpdateSourceRecords(_orgService, objects, SkippedEntities, "ddsm_measurereporting", _thisSettings.CallExecuteMultiple);
                SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
            }

        }

    }
}
