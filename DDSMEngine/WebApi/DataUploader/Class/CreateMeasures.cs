﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataUploader.Model.Enums;
using static DataUploader.Model.Errors;
using static DataUploader.Model.PrimaryKey;
using static DataUploader.Model.AutoNumberingManager;
using Microsoft.Xrm.Sdk.Messages;
using System.Collections.Concurrent;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Text.RegularExpressions;

#if RELESEPLUGIN
#else
using SocketClients;
using DataUploader.Service;
#endif

namespace DataUploader.Creator
{
    public class CreateMeasures : BaseMethod
    {
        public void CreateMeasure(IOrganizationService _orgService, ConcurrentDictionary<string, CreatedEntity> AccountEntities, ConcurrentDictionary<string, CreatedEntity> SiteEntities, ConcurrentDictionary<string, CreatedEntity> ProjectEntities, ConcurrentDictionary<string, CreatedEntity> MeasureEntities, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            Logger2 = new List<string>();
            configObject = Newtonsoft.Json.JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);

            var keyEntity = configObject.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_measure").Key;
            if (string.IsNullOrEmpty(keyEntity))
            {
                return;
            }

#if RELESEPLUGIN
#else
            wsMessageService = new WSMessageUploaderService(_thisSettings);
            wsMessageService.SendMessage(MessageType.INFORMATION, GetTextError(380035), MessageStatus.START);
#endif

            statisticRecords = new StatisticRecords();
            statisticsRecords.AddOrUpdate("stat", statisticRecords, (oldkey, oldvalue) => statisticRecords);

            entityCollectionPagination = new PaginationRetriveMultiple();
            DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.measuresProcessing, _thisSettings.TargetEntity.Id);
            do
            {
                entityCollectionPagination = GetDataByEntityName(_orgService, "ddsm_measure", configObject, _thisSettings.TargetEntity.Id, Logger, false, entityCollectionPagination.pageNumber, entityCollectionPagination.PagingCookie, entityCollectionPagination.MoreRecords, _thisSettings.GlobalPageRecordsCount);
                if (entityCollectionPagination != null && entityCollectionPagination.RetrieveCollection.Entities.Count > 0)
                {
                    statisticsRecords["stat"].All += entityCollectionPagination.RetrieveCollection.Entities.Count;
                    CreateRecordSet(_orgService, configObject, AccountEntities, SiteEntities, ProjectEntities, MeasureEntities, entityCollectionPagination.RetrieveCollection, statisticsRecords["stat"], Logger2, _thisSettings);
                }
            }
            while (entityCollectionPagination != null && entityCollectionPagination.MoreRecords);
            if (statisticsRecords["stat"].All > 0)
            {
                Logger2.Add(GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";");
#if RELESEPLUGIN
#else
                wsMessageService.SendMessage(MessageType.SUCCESS, "Measure: " + GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";", MessageStatus.FINISH);
#endif

                DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger2, "ddsm_log5");
            }


        }

        //Create New Measure Record
        private void CreateRecordSet(IOrganizationService _orgService, ConcurrentDictionary<string, EntityJson> objects, ConcurrentDictionary<string, CreatedEntity> AccountEntities, ConcurrentDictionary<string, CreatedEntity> SiteEntities, ConcurrentDictionary<string, CreatedEntity> ProjectEntities, ConcurrentDictionary<string, CreatedEntity> MeasureEntities, EntityCollection table, StatisticRecords statisticRecords, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_measure").Key;
            string EntityUniqueID = string.Empty;

            int EntityDataRow = 0, insertDataRow = 0, updateDataRow = 0, doNothing = 0;
            //Measure GUID Column
            string uniqueIdColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " GUID").ToLower()).Key;
            //Parent Account Column
            var accountTableName = string.Empty;
            accountTableName = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "account").Key;
            string accountColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (accountTableName + " GUID").ToLower()).Key;
            //Parent Site Column
            var siteTableName = string.Empty;
            siteTableName = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_site").Key;
            string siteColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (siteTableName + " GUID").ToLower()).Key;
            if (string.IsNullOrEmpty(siteColumnKey))
                siteColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == ("Parent " + siteTableName + " GUID").ToLower()).Key;
            //Implementation Site Column
            string implSiteColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == ("Implementation " + siteTableName + " GUID").ToLower()).Key;
            //Parent Project Column
            var projectTableName = string.Empty;
            projectTableName = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_project").Key;
            string projectColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (projectTableName + " GUID").ToLower()).Key;
            //Get Measure Template Number Column Key
            string measTplColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " Template").ToLower()).Key;

            if (string.IsNullOrEmpty(implSiteColumnKey)) implSiteColumnKey = siteColumnKey;

            if (string.IsNullOrEmpty(uniqueIdColumnKey)) return;
            if (string.IsNullOrEmpty(accountColumnKey)) return;
            if (string.IsNullOrEmpty(siteColumnKey)) return;
            if (string.IsNullOrEmpty(projectColumnKey)) return;
            if (string.IsNullOrEmpty(measTplColumnKey)) return;
            Int64 requestsCount = _thisSettings.GlobalRequestsCount, itemsRequestCount = 0, requestSets = 1;
            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            List<string> setUniqueID = new List<string>();
            foreach (Entity dr in table.Entities)
            {

                EntityDataRow++;
                if (EntityDataRow >= 1)
                {
                    try
                    {
                        EntityUniqueID = string.Empty;

                        EntityUniqueID = dr["ddsm_" + uniqueIdColumnKey.ToLower()].ToString();
                        if (string.IsNullOrEmpty(EntityUniqueID)) continue;

                        //Verify duplicate record GUID
                        if (MeasureEntities.ContainsKey(EntityUniqueID))
                        {
                            Logger.Add("ERROR >>> GUID: " + EntityUniqueID + " Not unique in sheet Measures list");
                            continue;
                        }

                        Entity newEntiy = new Entity("ddsm_measure");

                        string mnaColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Name.ToLower().IndexOf("ddsm_modelnumberapprovalid") != -1).Key;
                        string skuaColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Name.ToLower().IndexOf("ddsm_skuapprovalid") != -1).Key;

                        //Get xls data
                        //Remove Model Number Approval & SKU Approval column

                        Entity drMod = new Entity();
                        foreach (KeyValuePair<String, Object> Attribute in dr.Attributes)
                        {
                            drMod[Attribute.Key] = Attribute.Value;
                        }

                        if (!string.IsNullOrEmpty(mnaColumnKey) && drMod.Attributes.ContainsKey("ddsm_" + mnaColumnKey.ToLower()))
                        {
                            drMod.Attributes.Remove("ddsm_" + mnaColumnKey.ToLower());
                        }
                        if (!string.IsNullOrEmpty(skuaColumnKey) && drMod.Attributes.ContainsKey("ddsm_" + skuaColumnKey.ToLower()))
                        {
                            drMod.Attributes.Remove("ddsm_" + skuaColumnKey.ToLower());
                        }
                        
                        Entity newEntiyXLSX = GenerationEntity(_orgService, objects, keyEntity, drMod, _thisSettings.TargetEntity.Id);
                        DateTime InitialPhaseDate = DataUploader.ConverAttributToDateTimeUtc("today", _thisSettings.TimeZoneInfo);
                        if (newEntiyXLSX.Attributes.Contains("ddsm_initialphasedate"))
                            InitialPhaseDate = newEntiyXLSX.GetAttributeValue<DateTime>("ddsm_initialphasedate");

                        //Get Measure Template data
                        Entity rmEntity = new Entity();
                        string measTplID = null;
                        if (!string.IsNullOrEmpty(measTplColumnKey))
                        {
                            string measTpl = Convert.ToString(dr["ddsm_" + measTplColumnKey.ToLower()]);

                            if (!string.IsNullOrEmpty(measTpl))
                            {
                                //Get Measure Template ID
                                if (CasheRelationDataAttributes.ContainsKey(measTpl))
                                {
                                    measTplID = CasheRelationDataAttributes[measTpl].tplId;
                                }
                                else
                                {
                                    QueryExpression measQuery = new QueryExpression { EntityName = "ddsm_measuretemplate", ColumnSet = new ColumnSet("ddsm_measuretemplateid", "ddsm_name") };
                                    measQuery.Criteria.AddCondition("ddsm_measurenumber", ConditionOperator.Equal, measTpl);
                                    measQuery.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
                                    //measQuery.Criteria.AddCondition("ddsm_selectablestartdate", ConditionOperator.OnOrAfter, InitialPhaseDate);
                                    //measQuery.Criteria.AddCondition("ddsm_selectableenddate", ConditionOperator.OnOrBefore, InitialPhaseDate);
                                    EntityCollection ConfigRetrieve = _orgService.RetrieveMultiple(measQuery);
                                    if (ConfigRetrieve != null && ConfigRetrieve.Entities.Count == 1)
                                    {
                                        measTplID = ConfigRetrieve.Entities[0].Attributes["ddsm_measuretemplateid"].ToString();

                                        RelationDataAttributes relationDataAttributes = new RelationDataAttributes();
                                        relationDataAttributes.tplId = measTplID;
                                        CasheRelationDataAttributes.AddOrUpdate(measTpl, relationDataAttributes, (oldkey, oldvalue) => relationDataAttributes);
                                    }

                                }

                            }
                            else
                            {
                                Logger.Add("ERROR >>> GUID: " + EntityUniqueID + "; Measure Template not found!");
#if RELESEPLUGIN
#else
                                wsMessageService.SendMessage(MessageType.ERROR, "Measure UniqueGUID: " + EntityUniqueID + "; Measure Template not found!", MessageStatus.FINISH);
#endif

                                continue;
                            }

                            if (!string.IsNullOrEmpty(measTplID))
                            {
                                if (CasheRelationDataAttributes.ContainsKey(measTpl) && CasheRelationDataAttributes[measTpl].Data_Attributes_1 != null)
                                {
                                    rmEntity = CasheRelationDataAttributes[measTpl].Data_Attributes_1;
                                }
                                else
                                {
                                    rmEntity = GetDataRelationMapping(_orgService, "ddsm_measure", "ddsm_measuretemplate", new Guid(measTplID));
                                    CasheRelationDataAttributes[measTpl].Data_Attributes_1 = rmEntity;
                                }
                                if (rmEntity != null)
                                {
                                    foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                                    {
                                        newEntiy[Attribute.Key] = Attribute.Value;
                                        //newEntiy[Attribute.Key] = rmEntity.GetAttributeValue(Attribute.Key);
                                    }
                                }
                                newEntiy["ddsm_measureselector"] = new EntityReference("ddsm_measuretemplate", new Guid(measTplID));
                            }
                            else
                            {
                                Logger.Add("ERROR! >>> " + EntityUniqueID + " Measure Template not found!");
                                continue;
                            }
                        }
                        else
                        {
                            Logger.Add("ERROR! >>> " + EntityUniqueID + " Measure Template not found!");
                            continue;
                        }


                        //Parent Account
                        rmEntity = new Entity();
                        if (!string.IsNullOrEmpty(accountColumnKey) && !string.IsNullOrEmpty(dr["ddsm_" + accountColumnKey.ToLower()].ToString()))
                        {
                            GetDictionaryObjectbyGUID(_orgService, dr["ddsm_" + accountColumnKey.ToLower()].ToString(), "account", objects, AccountEntities, _thisSettings.TargetEntity.Id);
                            if (AccountEntities.ContainsKey(dr["ddsm_" + accountColumnKey.ToLower()].ToString()) && !string.IsNullOrEmpty(AccountEntities[dr["ddsm_" + accountColumnKey.ToLower()].ToString()].EntityGuid.ToString()))
                            {
                                if (AccountEntities[dr["ddsm_" + accountColumnKey.ToLower()].ToString()].EntityGuid.ToString() == (new Guid()).ToString())
                                {
                                    Logger.Add(EntityUniqueID + " Parent Account not found!");
                                    continue;
                                }

                                if (CasheRelationDataAttributes.ContainsKey(AccountEntities[dr["ddsm_" + accountColumnKey.ToLower()].ToString()].EntityGuid.ToString()))
                                {
                                    rmEntity = CasheRelationDataAttributes[AccountEntities[dr["ddsm_" + accountColumnKey.ToLower()].ToString()].EntityGuid.ToString()].Data_Attributes_1;
                                }
                                else
                                {
                                    Entity rmEntityMod = GetDataRelationMapping(_orgService, "ddsm_measure", "account", AccountEntities[dr["ddsm_" + accountColumnKey.ToLower()].ToString()].EntityGuid);

                                    if (rmEntityMod.Attributes.ContainsKey("ddsm_accountid"))
                                    {
                                        var parentAccount = rmEntityMod.GetAttributeValue<EntityReference>("ddsm_accountid");

                                        foreach (KeyValuePair<String, Object> Attribute in rmEntityMod.Attributes)
                                        {
                                            if (!(parentAccount.GetType() == Attribute.Value.GetType() && parentAccount.Id == (Attribute.Value as EntityReference).Id))
                                            {
                                                rmEntity[Attribute.Key] = Attribute.Value;
                                            }
                                        }
                                        rmEntity["ddsm_accountid"] = parentAccount;
                                    }
                                    else
                                    {
                                        rmEntity = rmEntityMod;
                                    }

                                    RelationDataAttributes relationDataAttributes = new RelationDataAttributes();
                                    relationDataAttributes.Data_Attributes_1 = rmEntity;
                                    CasheRelationDataAttributes.AddOrUpdate(AccountEntities[dr["ddsm_" + accountColumnKey.ToLower()].ToString()].EntityGuid.ToString(), relationDataAttributes, (oldkey, oldvalue) => relationDataAttributes);
                                }

                                if (rmEntity != null)
                                {
                                    foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                                    {
                                        newEntiy[Attribute.Key] = Attribute.Value;
                                    }
                                }
                            }
                            else
                            {
                                Logger.Add(EntityUniqueID + " Parent Account not found!");
                                continue;
                            }


                        }
                        else
                        {
                            Logger.Add(EntityUniqueID + " Parent Account not found!");
                            continue;
                        }

                        //Implementation Site
                        rmEntity = new Entity();
                        if (!string.IsNullOrEmpty(implSiteColumnKey) && !string.IsNullOrEmpty(dr["ddsm_" + implSiteColumnKey.ToLower()].ToString()))
                        {
                            GetDictionaryObjectbyGUID(_orgService, dr["ddsm_" + implSiteColumnKey.ToLower()].ToString(), "ddsm_site", objects, SiteEntities, _thisSettings.TargetEntity.Id);
                            if (SiteEntities.ContainsKey(dr["ddsm_" + implSiteColumnKey.ToLower()].ToString()) && !string.IsNullOrEmpty(SiteEntities[dr["ddsm_" + implSiteColumnKey.ToLower()].ToString()].EntityGuid.ToString()))
                            {
                                if (SiteEntities[dr["ddsm_" + implSiteColumnKey.ToLower()].ToString()].EntityGuid.ToString() == (new Guid()).ToString())
                                {
                                    Logger.Add(EntityUniqueID + " Implementation Site not found!");
                                    continue;
                                }
                                if (CasheRelationDataAttributes.ContainsKey(SiteEntities[dr["ddsm_" + implSiteColumnKey.ToLower()].ToString()].EntityGuid.ToString()))
                                {
                                    rmEntity = CasheRelationDataAttributes[SiteEntities[dr["ddsm_" + implSiteColumnKey.ToLower()].ToString()].EntityGuid.ToString()].Data_Attributes_1;
                                }
                                else
                                {
                                    Entity rmEntityMod = GetDataRelationMapping(_orgService, "ddsm_measure", "ddsm_site", SiteEntities[dr["ddsm_" + implSiteColumnKey.ToLower()].ToString()].EntityGuid);

                                    if (rmEntityMod.Attributes.ContainsKey("ddsm_implementationsite"))
                                    {
                                        var implSite = rmEntityMod.GetAttributeValue<EntityReference>("ddsm_implementationsite");

                                        foreach (KeyValuePair<String, Object> Attribute in rmEntityMod.Attributes)
                                        {
                                            if (!(implSite.GetType() == Attribute.Value.GetType() && implSite.Id == (Attribute.Value as EntityReference).Id))
                                            {
                                                rmEntity[Attribute.Key] = Attribute.Value;
                                            }
                                        }
                                        rmEntity["ddsm_implementationsite"] = implSite;
                                    }
                                    else
                                    {
                                        rmEntity = rmEntityMod;
                                    }

                                    RelationDataAttributes relationDataAttributes = new RelationDataAttributes();
                                    relationDataAttributes.Data_Attributes_1 = rmEntity;
                                    CasheRelationDataAttributes.AddOrUpdate(SiteEntities[dr["ddsm_" + implSiteColumnKey.ToLower()].ToString()].EntityGuid.ToString(), relationDataAttributes, (oldkey, oldvalue) => relationDataAttributes);
                                }

                                if (rmEntity != null)
                                {
                                    foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                                    {
                                        newEntiy[Attribute.Key] = Attribute.Value;
                                    }
                                }
                            }
                            else
                            {
                                Logger.Add(EntityUniqueID + " Implementation Site not found!");
                                continue;
                            }
                        }
                        else
                        {
                            Logger.Add(EntityUniqueID + " Implementation Site not found!");
                            continue;
                        }

                        //Site
                        if (!string.IsNullOrEmpty(siteColumnKey) && !string.IsNullOrEmpty(dr["ddsm_" + siteColumnKey.ToLower()].ToString()))
                        {
                            GetDictionaryObjectbyGUID(_orgService, dr["ddsm_" + siteColumnKey.ToLower()].ToString(), "ddsm_site", objects, SiteEntities, _thisSettings.TargetEntity.Id);
                            if (SiteEntities.ContainsKey(dr["ddsm_" + siteColumnKey.ToLower()].ToString()) && !string.IsNullOrEmpty(SiteEntities[dr["ddsm_" + siteColumnKey.ToLower()].ToString()].EntityGuid.ToString()))
                            {
                                if (SiteEntities[dr["ddsm_" + siteColumnKey.ToLower()].ToString()].EntityGuid.ToString() == (new Guid()).ToString())
                                {
                                    Logger.Add(EntityUniqueID + " Parent Site not found!");
                                    continue;
                                }
                                newEntiy["ddsm_parentsite"] = new EntityReference("ddsm_site", SiteEntities[dr["ddsm_" + siteColumnKey.ToLower()].ToString()].EntityGuid);
                            }
                            else
                            {
                                Logger.Add(EntityUniqueID + " Parent Site not found!");
                                continue;
                            }
                        }
                        else
                        {
                            Logger.Add(EntityUniqueID + " Parent Site not found!");
                            continue;
                        }

                        //Project
                        if (!string.IsNullOrEmpty(projectColumnKey) && !string.IsNullOrEmpty(dr["ddsm_" + projectColumnKey.ToLower()].ToString()))
                        {
                            GetDictionaryObjectbyGUID(_orgService, dr["ddsm_" + projectColumnKey.ToLower()].ToString(), "ddsm_project", objects, ProjectEntities, _thisSettings.TargetEntity.Id);
                            if (ProjectEntities.ContainsKey(dr["ddsm_" + projectColumnKey.ToLower()].ToString()) && !string.IsNullOrEmpty(ProjectEntities[dr["ddsm_" + projectColumnKey.ToLower()].ToString()].EntityGuid.ToString()))
                            {
                                if (ProjectEntities[dr["ddsm_" + projectColumnKey.ToLower()].ToString()].EntityGuid.ToString() == (new Guid()).ToString())
                                {
                                    Logger.Add(EntityUniqueID + " Parent Project not found!");
                                    continue;
                                }
                                if (CasheRelationDataAttributes.ContainsKey(ProjectEntities[dr["ddsm_" + projectColumnKey.ToLower()].ToString()].EntityGuid.ToString()))
                                {
                                    rmEntity = CasheRelationDataAttributes[ProjectEntities[dr["ddsm_" + projectColumnKey.ToLower()].ToString()].EntityGuid.ToString()].Data_Attributes_1;
                                }
                                else
                                {
                                    rmEntity = GetDataRelationMapping(_orgService, "ddsm_measure", "ddsm_project", ProjectEntities[dr["ddsm_" + projectColumnKey.ToLower()].ToString()].EntityGuid);
                                    RelationDataAttributes relationDataAttributes = new RelationDataAttributes();
                                    relationDataAttributes.Data_Attributes_1 = rmEntity;
                                    CasheRelationDataAttributes.AddOrUpdate(ProjectEntities[dr["ddsm_" + projectColumnKey.ToLower()].ToString()].EntityGuid.ToString(), relationDataAttributes, (oldkey, oldvalue) => relationDataAttributes);
                                }

                                if (rmEntity != null)
                                {
                                    foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                                    {
                                        newEntiy[Attribute.Key] = Attribute.Value;
                                    }
                                }
                            }
                            else
                            {
                                Logger.Add(EntityUniqueID + " Parent Project not found!");
                                continue;
                            }
                        }
                        else
                        {
                            Logger.Add(EntityUniqueID + " Parent Project not found!");
                            continue;
                        }

                        if (newEntiy.Attributes.Contains("ddsm_initialphasedate"))
                            InitialPhaseDate = newEntiy.GetAttributeValue<DateTime>("ddsm_initialphasedate");


                        //Set Currency
                            if (!newEntiy.Attributes.ContainsKey("transactioncurrencyid") && _thisSettings.CurrencyGuid != Guid.Empty)
                            {
                                newEntiy["transactioncurrencyid"] = new EntityReference("transactioncurrency", _thisSettings.CurrencyGuid);
                            }

                        //Model Number Approval
                        if (!string.IsNullOrEmpty(mnaColumnKey) && dr.Attributes.ContainsKey("ddsm_" + mnaColumnKey.ToLower()) && !string.IsNullOrEmpty(dr["ddsm_" + mnaColumnKey.ToLower()].ToString()) && !string.IsNullOrEmpty(measTplID))
                        {

                            Entity mnaEntity = GetMNApproval(_orgService, measTplID, Convert.ToString(dr["ddsm_" + mnaColumnKey.ToLower()]));

                            if (mnaEntity != null)
                            {
                                foreach (KeyValuePair<String, Object> Attribute in mnaEntity.Attributes)
                                {
                                    newEntiy[Attribute.Key] = Attribute.Value;
                                }
                            }
                        }

                        //SKU Approval
                        if (!string.IsNullOrEmpty(skuaColumnKey) && dr.Attributes.ContainsKey("ddsm_" + skuaColumnKey.ToLower()) && !string.IsNullOrEmpty(dr["ddsm_" + skuaColumnKey.ToLower()].ToString()) && !string.IsNullOrEmpty(measTplID))
                        {
                            Entity skuaEntity = GetSKUApproval(_orgService, measTplID, Convert.ToString(dr["ddsm_" + skuaColumnKey.ToLower()]));
                            if (skuaEntity != null)
                            {
                                foreach (KeyValuePair<String, Object> Attribute in skuaEntity.Attributes)
                                {
                                    newEntiy[Attribute.Key] = Attribute.Value;
                                }
                            }
                        }

                        //Add xls data
                        if (newEntiyXLSX.Attributes.Count == 0) return;
                        foreach (KeyValuePair<String, Object> Attribute in newEntiyXLSX.Attributes)
                        {
                            newEntiy[Attribute.Key] = Attribute.Value;
                        }

                        switch (_thisSettings.EspRecalcData)
                        {
                            case 962080000: //Default
                                break;
                            case 962080001: //Allow ESP recalculation
                                newEntiy["ddsm_recalculatemeasure"] = true;
                                break;
                            case 962080002: //Deny ESP recalculation
                                newEntiy["ddsm_recalculatemeasure"] = false;
                                break;
                        }

                        newEntiy["ddsm_calculationrecordstatus"] = new OptionSetValue(962080000);
                        newEntiy["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);

                        string duplicateRecordId = string.Empty;
                        if (_thisSettings.DuplicateDetect)
                        {
                            //duplicateRecordId = VerifyDuplicateRecord(_orgService, newEntiy, objects[keyEntity].Name.ToString().ToLower());
                        }

                        CreatedEntity MeasEntity = new CreatedEntity();
                        Boolean Successful = false;
                        if (!string.IsNullOrEmpty(duplicateRecordId))
                        {

                            string[] reqDuplicate = Regex.Split(duplicateRecordId, "::");
                            MeasEntity.EntityGuid = new Guid(reqDuplicate[0]);
                            MeasEntity.Name = reqDuplicate[1];
                            MeasEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            MeasEntity.Status = false;
                            MeasEntity.StatusDB = false;

                            //Parent Project ID for sort Meas Task Queue
                            MeasEntity.addInfo = newEntiy.GetAttributeValue<EntityReference>("ddsm_projecttomeasureid").Id.ToString();

                            MeasureEntities.AddOrUpdate(EntityUniqueID, MeasEntity, (oldkey, oldvalue) => MeasEntity);

                            switch (_thisSettings.DedupRules)
                            {
                                case 962080000: //Update
                                    if (newEntiy.Attributes.Count > 0)
                                    {
                                        Entity updateEntity = new Entity("ddsm_measure", MeasEntity.EntityGuid);
                                        foreach (KeyValuePair<String, Object> Attribute in newEntiy.Attributes)
                                        {
                                            updateEntity[Attribute.Key] = Attribute.Value;
                                        }

                                        UpdateRequest updateRequest = new UpdateRequest();
                                        updateRequest.Target = updateEntity;
                                        emRequest.Requests.Add(updateRequest);
                                        setUniqueID.Add(EntityUniqueID);
                                        itemsRequestCount++;
                                        //statisticRecords.Updated++;
                                    }
                                    else
                                    {
                                        MeasureEntities[EntityUniqueID].Status = true;
                                        SkippedEntities.AddOrUpdate(EntityUniqueID, MeasureEntities[EntityUniqueID], (oldkey, oldvalue) => MeasureEntities[EntityUniqueID]);
                                        statisticRecords.DoNothing++;

                                        itemsRequestCount++;
                                    }
                                    break;
                                case 962080001:
                                    MeasureEntities[EntityUniqueID].Status = true;
                                    SkippedEntities.AddOrUpdate(EntityUniqueID, MeasureEntities[EntityUniqueID], (oldkey, oldvalue) => MeasureEntities[EntityUniqueID]);
                                    statisticRecords.DoNothing++;

                                    itemsRequestCount++;
                                    break;
                            }

                        }
                        else
                        {

                            CreateRequest createRequest = new CreateRequest();
                            createRequest.Target = newEntiy;
                            emRequest.Requests.Add(createRequest);
                            setUniqueID.Add(EntityUniqueID);
                            itemsRequestCount++;

                            MeasEntity.EntityGuid = new Guid();
                            MeasEntity.Name = newEntiy["ddsm_name"].ToString();
                            MeasEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            //Parent Project ID for sort Meas Task Queue
                            MeasEntity.addInfo = newEntiy.GetAttributeValue<EntityReference>("ddsm_projecttomeasureid").Id.ToString();
                            MeasEntity.Status = false;
                            MeasEntity.StatusDB = false;
                            //MeasureEntities.Add(EntityUniqueID, MeasEntity);
                            MeasureEntities.AddOrUpdate(EntityUniqueID, MeasEntity, (oldkey, oldvalue) => MeasEntity);
                        }
                    }
                    catch (Exception e)
                    {
                        itemsRequestCount++;

                        Logger.Add("ERROR >>> UniqueGUID: " + EntityUniqueID + "; " + e.Message);
#if RELESEPLUGIN
#else
                        wsMessageService.SendMessage(MessageType.ERROR, "Measure UniqueGUID: " + EntityUniqueID + "; Message: " + e.Message, MessageStatus.FINISH);
#endif

                    }
                }
                //Create Multiple
                if (itemsRequestCount == (requestSets * requestsCount))
                {
                    if (emRequest.Requests.Count > 0)
                    {
                        ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                        CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, MeasureEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                        //Update Source records
                        UpdateSourceRecords(_orgService, objects, RequestEntities, "ddsm_measure", _thisSettings.CallExecuteMultiple);
                    }

                    //Update Source records (Skipped)
                    if (SkippedEntities.Count > 0)
                    {
                        UpdateSourceRecords(_orgService, objects, SkippedEntities, "ddsm_measure", _thisSettings.CallExecuteMultiple);
                    }
                    SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();

                    requestSets++;
                    setUniqueID = new List<string>();
                    emRequest = new ExecuteMultipleRequest
                    {
                        Requests = new OrganizationRequestCollection(),
                        Settings = new ExecuteMultipleSettings
                        {
                            ContinueOnError = true,
                            ReturnResponses = true
                        }
                    };

                }
            }

            if (setUniqueID.Count > 0 && emRequest.Requests.Count > 0)
            {
                ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, MeasureEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                //Update Source records
                UpdateSourceRecords(_orgService, objects, RequestEntities, "ddsm_measure", _thisSettings.CallExecuteMultiple);
            }

            if (SkippedEntities.Count > 0)
            {
                //Update Source records (Skipped)
                UpdateSourceRecords(_orgService, objects, SkippedEntities, "ddsm_measure", _thisSettings.CallExecuteMultiple);
                SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
            }

        }

        private Entity GetSKUApproval(IOrganizationService _orgService, string _measTplID, string val)
        {
            if (!string.IsNullOrEmpty(val))
            {
                try
                {
                    //Search value ddsm_skuapprovalid
                    QueryExpression query = new QueryExpression { EntityName = "ddsm_skuapproval", ColumnSet = new ColumnSet("ddsm_skuapprovalid", "ddsm_measuretemplateid") };
                    query.Criteria.AddCondition("ddsm_skuapprovalid", ConditionOperator.Equal, new Guid(val));
                    query.Criteria.AddCondition("ddsm_measuretemplateid", ConditionOperator.Equal, new Guid(_measTplID));
                    query.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
                    EntityCollection retrieve = _orgService.RetrieveMultiple(query);
                    if (retrieve != null && retrieve.Entities.Count == 1 && retrieve[0].Attributes.ContainsKey("ddsm_skuapprovalid"))
                    {
                        return GetDataRelationMapping(_orgService, "ddsm_measure", "ddsm_skuapproval", retrieve[0].GetAttributeValue<Guid>("ddsm_skuapprovalid"));
                    }

                    //Search value ddsm_name
                    query = new QueryExpression { EntityName = "ddsm_skuapproval", ColumnSet = new ColumnSet("ddsm_skuapprovalid") };
                    query.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, val);
                    query.Criteria.AddCondition("ddsm_measuretemplateid", ConditionOperator.Equal, new Guid(_measTplID));
                    query.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
                    retrieve = _orgService.RetrieveMultiple(query);
                    if (retrieve != null && retrieve.Entities.Count == 1 && retrieve[0].Attributes.ContainsKey("ddsm_modelnumberapprovalid"))
                    {
                        return GetDataRelationMapping(_orgService, "ddsm_measure", "ddsm_skuapproval", retrieve[0].GetAttributeValue<Guid>("ddsm_skuapprovalid"));
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }

            return null;
        }

        private Entity GetMNApproval(IOrganizationService _orgService, string _measTplID, string val)
        {
            if (!string.IsNullOrEmpty(val))
            {
                try
                {
                    //Search value ddsm_modelnumberapprovalid
                    QueryExpression query = new QueryExpression { EntityName = "ddsm_modelnumberapproval", ColumnSet = new ColumnSet("ddsm_modelnumberapprovalid", "ddsm_measuretemplateid") };
                    query.Criteria.AddCondition("ddsm_modelnumberapprovalid", ConditionOperator.Equal, new Guid(val));
                    query.Criteria.AddCondition("ddsm_measuretemplateid", ConditionOperator.Equal, new Guid(_measTplID));
                    query.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
                    EntityCollection retrieve = _orgService.RetrieveMultiple(query);
                    if (retrieve != null && retrieve.Entities.Count == 1 && retrieve[0].Attributes.ContainsKey("ddsm_modelnumberapprovalid"))
                    {
                        return GetDataRelationMapping(_orgService, "ddsm_measure", "ddsm_modelnumberapproval", retrieve[0].GetAttributeValue<Guid>("ddsm_modelnumberapprovalid"));
                    }

                    //Search value ddsm_name
                    query = new QueryExpression { EntityName = "ddsm_modelnumberapproval", ColumnSet = new ColumnSet("ddsm_modelnumberapprovalid") };
                    query.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, val);
                    query.Criteria.AddCondition("ddsm_measuretemplateid", ConditionOperator.Equal, new Guid(_measTplID));
                    query.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
                    retrieve = _orgService.RetrieveMultiple(query);
                    if (retrieve != null && retrieve.Entities.Count == 1 && retrieve[0].Attributes.ContainsKey("ddsm_modelnumberapprovalid"))
                    {
                        return GetDataRelationMapping(_orgService, "ddsm_measure", "ddsm_modelnumberapproval", retrieve[0].GetAttributeValue<Guid>("ddsm_modelnumberapprovalid"));
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }

            return null;
        }

    }
}
