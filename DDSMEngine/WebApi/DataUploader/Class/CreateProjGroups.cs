﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataUploader;
using static DataUploader.Model.Enums;
using static DataUploader.Model.Errors;
using static DataUploader.Model.PrimaryKey;
using static DataUploader.Model.AutoNumberingManager;
using Microsoft.Xrm.Sdk.Messages;
using System.Collections.Concurrent;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Text.RegularExpressions;

#if RELESEPLUGIN
#else
using SocketClients;
using DataUploader.Service;
#endif

namespace DataUploader.Creator
{
    public class CreateProjGroups: BaseMethod
    {
        public void CreateProjGroup(IOrganizationService _orgService, ConcurrentDictionary<string, CreatedEntity> ProjectGroupEntities, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            Logger2 = new List<string>();
            configObject = Newtonsoft.Json.JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);

            var keyEntity = configObject.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_projectgroup").Key;
            if (string.IsNullOrEmpty(keyEntity))
            {
                return;
            }

#if RELESEPLUGIN
#else
            wsMessageService = new WSMessageUploaderService(_thisSettings);
            wsMessageService.SendMessage(MessageType.INFORMATION, GetTextError(380033), MessageStatus.START);
#endif

            statisticRecords = new StatisticRecords();
            statisticsRecords.AddOrUpdate("stat", statisticRecords, (oldkey, oldvalue) => statisticRecords);

            entityCollectionPagination = new PaginationRetriveMultiple();
            DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.projectGroupsProcessing, _thisSettings.TargetEntity.Id);
            do
            {
                entityCollectionPagination = GetDataByEntityName(_orgService, "ddsm_projectgroup", configObject, _thisSettings.TargetEntity.Id, Logger, false, entityCollectionPagination.pageNumber, entityCollectionPagination.PagingCookie, entityCollectionPagination.MoreRecords, _thisSettings.GlobalPageRecordsCount);
                if (entityCollectionPagination != null && entityCollectionPagination.RetrieveCollection.Entities.Count > 0)
                {
                    statisticsRecords["stat"].All += entityCollectionPagination.RetrieveCollection.Entities.Count;
                    CreateRecordSet(_orgService, configObject, ProjectGroupEntities, entityCollectionPagination.RetrieveCollection, statisticsRecords["stat"], Logger2, _thisSettings);
                }
            }
            while (entityCollectionPagination != null && entityCollectionPagination.MoreRecords);
            if (statisticsRecords["stat"].All > 0)
            {
                Logger2.Add(GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";");
#if RELESEPLUGIN
#else
                wsMessageService.SendMessage(MessageType.SUCCESS, "Project Group: " + GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";", MessageStatus.FINISH);
#endif

                DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger2, "ddsm_log3");
            }

        }

        //Create Project Group
        private void CreateRecordSet(IOrganizationService _orgService, ConcurrentDictionary<string, EntityJson> objects, ConcurrentDictionary<string, CreatedEntity> ProjectGroupEntities, EntityCollection table, StatisticRecords statisticRecords, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_projectgroup").Key;
            string EntityUniqueID = string.Empty;
            DateTime ExecuteMultipleRequestTime, ExecuteMultipleResponseTime, StartMethodTime;

            int EntityDataRow = 0, updateDataRow = 0, insertDataRow = 0, doNothing = 0;
            string uniqueIdColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " GUID").ToLower()).Key;
            //DataTable rr = table.AsEnumerable().GroupBy(row => row.Field<string>("Spec")).Select(g => g.First());
            string projGroupTplColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " Template").ToLower()).Key, projGroupTplID = null, projGroupTplName = null;
            int projGroupInitialIdx = 1;
            if (string.IsNullOrEmpty(uniqueIdColumnKey)) return;
            if (string.IsNullOrEmpty(projGroupTplColumnKey)) return;
            Int64 requestsCount = _thisSettings.GlobalRequestsCount, itemsRequestCount = 0, requestSets = 1;
            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            List<string> setUniqueID = new List<string>();
            foreach (Entity dr in table.Entities)
            {

                EntityDataRow++;
                if (EntityDataRow >= 1)
                {
                    try
                    {
                        EntityUniqueID = string.Empty;

                        EntityUniqueID = dr["ddsm_" + uniqueIdColumnKey.ToLower()].ToString();
                        if (string.IsNullOrEmpty(EntityUniqueID)) continue;

                        //Verify duplicate record GUID
                        if (ProjectGroupEntities.ContainsKey(EntityUniqueID))
                        {
                            Logger.Add("ERROR >>> GUID: " + EntityUniqueID + " Not unique in sheet Project Groups list");
                            continue;
                        }

                        Entity newEntiy = new Entity("ddsm_projectgroup"); ;
                        //Get Project Group Template Column Key

                        if (dr.Attributes.ContainsKey("ddsm_" + projGroupTplColumnKey.ToLower()))
                        {
                            string projGroupTpl = Convert.ToString(dr["ddsm_" + projGroupTplColumnKey.ToLower()]);
                            //Get Project Template ID
                            if (string.IsNullOrEmpty(projGroupTpl))
                            {
                                Logger.Add("PG: " + EntityUniqueID + " PG Template not found!");
#if RELESEPLUGIN
#else
                                wsMessageService.SendMessage(MessageType.ERROR, "Project Goup UniqueGUID: " + EntityUniqueID + "; Project Group Template not found!", MessageStatus.FINISH);
#endif

                                continue;
                            }
                            if (CasheRelationDataAttributes.ContainsKey(projGroupTpl))
                            {
                                projGroupTplID = CasheRelationDataAttributes[projGroupTpl].tplId;
                                projGroupTplName = CasheRelationDataAttributes[projGroupTpl].tplName;
                            }
                            else
                            {
                                QueryExpression projQuery = new QueryExpression { EntityName = "ddsm_projectgrouptemplates", ColumnSet = new ColumnSet("ddsm_projectgrouptemplatesid", "ddsm_name", "ddsm_initialmilestoneindex") };
                                projQuery.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, projGroupTpl);
                                EntityCollection ConfigRetrieve = _orgService.RetrieveMultiple(projQuery);
                                if (ConfigRetrieve != null && ConfigRetrieve.Entities.Count == 1)
                                {
                                    projGroupTplID = ConfigRetrieve.Entities[0].Attributes["ddsm_projectgrouptemplatesid"].ToString();
                                    projGroupTplName = ConfigRetrieve.Entities[0].Attributes["ddsm_name"].ToString();
                                    if (ConfigRetrieve.Entities[0].Attributes.ContainsKey("ddsm_initialmilestoneindex"))
                                    {
                                        projGroupInitialIdx = (int)ConfigRetrieve.Entities[0].Attributes["ddsm_initialmilestoneindex"];
                                    }

                                    RelationDataAttributes relationDataAttributes = new RelationDataAttributes();
                                    relationDataAttributes.tplId = projGroupTplID;
                                    relationDataAttributes.tplName = projGroupTplName;
                                    CasheRelationDataAttributes.AddOrUpdate(projGroupTpl, relationDataAttributes, (oldkey, oldvalue) => relationDataAttributes);
                                }
                            }



                            if (!string.IsNullOrEmpty(projGroupTplID))
                            {
                                Entity rmEntity;
                                if (CasheRelationDataAttributes.ContainsKey(projGroupTpl) && CasheRelationDataAttributes[projGroupTpl].Data_Attributes_1 != null)
                                {
                                    rmEntity = CasheRelationDataAttributes[projGroupTpl].Data_Attributes_1;
                                }
                                else
                                {
                                    rmEntity = GetDataRelationMapping(_orgService, "ddsm_projectgroup", "ddsm_projectgrouptemplates", new Guid(projGroupTplID));
                                    CasheRelationDataAttributes[projGroupTpl].Data_Attributes_1 = rmEntity;
                                }
                                if (rmEntity != null)
                                {
                                    foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                                    {
                                        newEntiy[Attribute.Key] = Attribute.Value;
                                    }
                                }
                            }
                            else
                            {
                                Logger.Add("PG: " + EntityUniqueID + " - PG Template not found");
                                continue;
                            }
                            //Add XLSX Data
                            Entity newEntiyXLSX = GenerationEntity(_orgService, objects, keyEntity, dr, _thisSettings.TargetEntity.Id);
                            if (newEntiyXLSX.Attributes.Count == 0) continue;

                            DateTime projGroupStartDate = DataUploader.ConverAttributToDateTimeUtc("today", _thisSettings.TimeZoneInfo);

                            if (!newEntiy.Attributes.ContainsKey("transactioncurrencyid") && _thisSettings.CurrencyGuid != Guid.Empty)
                            {
                                newEntiy["transactioncurrencyid"] = new EntityReference("transactioncurrency", _thisSettings.CurrencyGuid);
                            }

                            //Add XLSX Data
                            foreach (KeyValuePair<String, Object> Attribute in newEntiyXLSX.Attributes)
                            {
                                newEntiy[Attribute.Key] = Attribute.Value;
                                //newEntiy[Attribute.Key] = newEntiyXLSX.GetAttributeValue(Attribute.Key);
                            }

                            if (newEntiy.Attributes.ContainsKey("ddsm_startdate"))
                                projGroupStartDate = newEntiy.GetAttributeValue<DateTime>("ddsm_startdate");

                            string milestoneActualEndKey = null;
                            milestoneActualEndKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == ("Milestone 1").ToLower()).Key;
                            if (milestoneActualEndKey != null && dr.Attributes.ContainsKey("ddsm_" + milestoneActualEndKey.ToLower()))
                            {
                                projGroupStartDate = DataUploader.ConverAttributToDateTimeUtc(dr["ddsm_" + milestoneActualEndKey.ToLower()], _thisSettings.TimeZoneInfo);
                            }
                            newEntiy["ddsm_startdate"] = projGroupStartDate;

                            //Get & ReCalc Project Group Milestone
                            EntityCollection milestoneCollection = CreateProjGroupMilestone(_orgService, projGroupTplID, projGroupTpl, projGroupStartDate, Logger, _thisSettings);
                            //Logger.Add("milestoneCollection Count: " + milestoneCollection.Entities.Count);

                            //Logger.Add("milestoneCollection Count: " + milestoneCollection.Entities.Count);
                            //Create & ReCalc Milestone Collection
                            //Get Business Process project
                            ProjectBusinessProcess projBP = new ProjectBusinessProcess();
                            if (!string.IsNullOrEmpty(projGroupTplName))
                            {
                                projBP = GetBProcessProjGroup(_orgService, projGroupTplName + " Project Group", projGroupTpl);
                            }
                            else
                            {
                                projBP.Id = null;
                            }
                            int activeIdx = projGroupInitialIdx;
                            string traversedpath = string.Empty, stageid = string.Empty, processid = string.Empty;

                            DateTime msPrevEndtDay = projGroupStartDate;

                            foreach (Entity ms in milestoneCollection.Entities)
                            {
                                if (Convert.ToInt32(ms["ddsm_index"]) == 1)
                                {
                                    milestoneActualEndKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == ("Milestone " + ms["ddsm_index"].ToString()).ToLower()).Key;
                                    if (milestoneActualEndKey != null && dr.Attributes.ContainsKey("ddsm_" + milestoneActualEndKey.ToLower()))
                                    {
                                        msPrevEndtDay = DataUploader.ConverAttributToDateTimeUtc(dr["ddsm_" + milestoneActualEndKey.ToLower()], _thisSettings.TimeZoneInfo);
                                    }
                                }
                                if (ms.Contains("ddsm_duration") && ms["ddsm_duration"] != null)
                                {
                                }
                                else ms["ddsm_duration"] = 0;

                                if (Convert.ToInt32(ms["ddsm_index"]) < activeIdx)
                                {

                                    ms["ddsm_actualstart"] = msPrevEndtDay;
                                    ms["ddsm_targetstart"] = msPrevEndtDay;
                                    ms["ddsm_targetend"] = msPrevEndtDay.AddDays(Convert.ToDouble(ms["ddsm_duration"]));

                                    ms["ddsm_status"] = new OptionSetValue(962080002);

                                    ms["ddsm_actualend"] = ms["ddsm_targetend"];

                                    milestoneActualEndKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == ("Milestone " + ms["ddsm_index"].ToString()).ToLower()).Key;
                                    if (milestoneActualEndKey != null && dr.Attributes.ContainsKey("ddsm_" + milestoneActualEndKey.ToLower()))
                                    {
                                        ms["ddsm_actualend"] = DataUploader.ConverAttributToDateTimeUtc(dr["ddsm_" + milestoneActualEndKey.ToLower()], _thisSettings.TimeZoneInfo);
                                    }
                                    msPrevEndtDay = Convert.ToDateTime(ms["ddsm_actualend"]);

                                    if (projBP.Id != null)
                                    {
                                        if (projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())) != null)
                                            traversedpath = traversedpath + "," + projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())).Id;
                                    }
                                }
                                else if (Convert.ToInt32(ms["ddsm_index"]) == activeIdx)
                                {
                                    projGroupStartDate = msPrevEndtDay;
                                    ms["ddsm_actualstart"] = msPrevEndtDay;
                                    ms["ddsm_targetstart"] = msPrevEndtDay;
                                    msPrevEndtDay = msPrevEndtDay.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                                    ms["ddsm_targetend"] = msPrevEndtDay;

                                    ms["ddsm_status"] = new OptionSetValue(962080001);
                                    newEntiy["ddsm_responsible"] = (ms.Attributes.Contains("ddsm_responsible")) ? ms["ddsm_responsible"] : string.Empty;
                                    newEntiy["ddsm_pendingstage"] = ms["ddsm_name"];

                                    if (projBP.Id != null)
                                    {
                                        if (projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())) != null)
                                            stageid = projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())).Id;
                                    }
                                }
                                else
                                {
                                    ms["ddsm_actualstart"] = msPrevEndtDay;
                                    ms["ddsm_targetstart"] = msPrevEndtDay;
                                    msPrevEndtDay = msPrevEndtDay.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                                    ms["ddsm_targetend"] = msPrevEndtDay;

                                    ms["ddsm_status"] = new OptionSetValue(962080000);
                                }

                            }

                            if (!string.IsNullOrEmpty(stageid))
                                traversedpath = traversedpath + "," + stageid;
                            traversedpath = traversedpath.TrimStart(',');

                            //Add Milestone Collection
                            if (milestoneCollection.Entities.Count > 0)
                            {
                                Relationship apRelationship = new Relationship("ddsm_ddsm_projectgroup_ddsm_projectgroupmilestone");
                                newEntiy.RelatedEntities.Add(apRelationship, milestoneCollection);
                            }

                            //Add Business Process
                            if (projBP.Id != null)
                            {
                                newEntiy["processid"] = new Guid(projBP.Id);
                                if (!string.IsNullOrEmpty(stageid))
                                    newEntiy["stageid"] = new Guid(stageid);
                                if (!string.IsNullOrEmpty(traversedpath))
                                    newEntiy["traversedpath"] = traversedpath;
                            }

                            //Create Financial
                            EntityCollection fnlcCollection = CreateProjectGroupFinancials(_orgService, EntityUniqueID, keyEntity, projGroupTplID, projGroupTpl, newEntiy, milestoneCollection, objects, Logger, _thisSettings);

                            if (fnlcCollection.Entities.Count > 0)
                            {
                                Relationship fnlcRelationship = new Relationship("ddsm_ddsm_projectgroup_ddsm_projectgroupfinanc");
                                newEntiy.RelatedEntities.Add(fnlcRelationship, fnlcCollection);
                            }

                            newEntiy["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);

                            string duplicateRecordId = string.Empty;
                            if (_thisSettings.DuplicateDetect)
                            {
                                //duplicateRecordId = VerifyDuplicateRecord(_orgService, newEntiy, objects[keyEntity].Name.ToString().ToLower());
                            }


                            CreatedEntity ProjGroupEntity = new CreatedEntity();
                            Boolean Successful = false;
                            if (!string.IsNullOrEmpty(duplicateRecordId))
                            {

                                string[] reqDuplicate = Regex.Split(duplicateRecordId, "::");
                                ProjGroupEntity.EntityGuid = new Guid(reqDuplicate[0]);
                                ProjGroupEntity.Name = reqDuplicate[1];
                                ProjGroupEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                                ProjGroupEntity.Status = false;
                                ProjGroupEntity.StatusDB = false;

                                ProjectGroupEntities.AddOrUpdate(EntityUniqueID, ProjGroupEntity, (oldkey, oldvalue) => ProjGroupEntity);

                                switch (_thisSettings.DedupRules)
                                {
                                    case 962080000: //Update
                                        if (newEntiy.Attributes.Count > 0)
                                        {
                                            Entity updateEntity = new Entity("ddsm_projectgroup", ProjGroupEntity.EntityGuid);
                                            foreach (KeyValuePair<String, Object> Attribute in newEntiy.Attributes)
                                            {
                                                updateEntity[Attribute.Key] = Attribute.Value;
                                                //updateEntity[Attribute.Key] = newEntiy.GetAttributeValue(Attribute.Key);
                                            }

                                            UpdateRequest updateRequest = new UpdateRequest();
                                            updateRequest.Target = updateEntity;
                                            emRequest.Requests.Add(updateRequest);
                                            setUniqueID.Add(EntityUniqueID);
                                            itemsRequestCount++;
                                            //statisticRecords.Updated++;
                                        }
                                        else
                                        {
                                            ProjectGroupEntities[EntityUniqueID].Status = true;
                                            SkippedEntities.AddOrUpdate(EntityUniqueID, ProjectGroupEntities[EntityUniqueID], (oldkey, oldvalue) => ProjectGroupEntities[EntityUniqueID]);
                                            statisticRecords.DoNothing++;

                                            itemsRequestCount++;
                                        }
                                        break;
                                    case 962080001:
                                        ProjectGroupEntities[EntityUniqueID].Status = true;
                                        SkippedEntities.AddOrUpdate(EntityUniqueID, ProjectGroupEntities[EntityUniqueID], (oldkey, oldvalue) => ProjectGroupEntities[EntityUniqueID]);
                                        statisticRecords.DoNothing++;

                                        itemsRequestCount++;
                                        break;
                                }

                            }
                            else
                            {

                                CreateRequest createRequest = new CreateRequest();
                                createRequest.Target = newEntiy;
                                emRequest.Requests.Add(createRequest);
                                setUniqueID.Add(EntityUniqueID);
                                itemsRequestCount++;
                                ProjGroupEntity.EntityGuid = new Guid();
                                ProjGroupEntity.Name = newEntiy["ddsm_name"].ToString();
                                ProjGroupEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                                ProjGroupEntity.Status = false;
                                ProjGroupEntity.StatusDB = false;
                                ProjectGroupEntities.AddOrUpdate(EntityUniqueID, ProjGroupEntity, (oldkey, oldvalue) => ProjGroupEntity);

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        itemsRequestCount++;

                        Logger.Add("ERROR >>> UniqueGUID: " + EntityUniqueID + ";; " + e.Message);
#if RELESEPLUGIN
#else
                        wsMessageService.SendMessage(MessageType.ERROR, "Project Group UniqueGUID: " + EntityUniqueID + "; Message: " + e.Message, MessageStatus.FINISH);
#endif

                    }
                }

                //Create Multiple
                if (itemsRequestCount == (requestSets * requestsCount))
                {
                    if (emRequest.Requests.Count > 0)
                    {
                        ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                        CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, ProjectGroupEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                        //Update Source records
                        UpdateSourceRecords(_orgService, objects, RequestEntities, "ddsm_projectgroup", _thisSettings.CallExecuteMultiple);
                    }

                    //Update Source records (Skipped)
                    if (SkippedEntities.Count > 0)
                    {
                        UpdateSourceRecords(_orgService, objects, SkippedEntities, "ddsm_projectgroup", _thisSettings.CallExecuteMultiple);
                    }
                    SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();

                    requestSets++;

                    setUniqueID = new List<string>();
                    emRequest = new ExecuteMultipleRequest
                    {
                        Requests = new OrganizationRequestCollection(),
                        Settings = new ExecuteMultipleSettings
                        {
                            ContinueOnError = true,
                            ReturnResponses = true
                        }
                    };

                }
            }

            if (setUniqueID.Count > 0 && emRequest.Requests.Count > 0)
            {
                ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();
                CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, ProjectGroupEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                //Update Source records
                UpdateSourceRecords(_orgService, objects, RequestEntities, "ddsm_projectgroup", _thisSettings.CallExecuteMultiple);
            }

            if (SkippedEntities.Count > 0)
            {
                //Update Source records (Skipped)
                UpdateSourceRecords(_orgService, objects, SkippedEntities, "ddsm_projectgroup", _thisSettings.CallExecuteMultiple);
                SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
            }

        }
        //Create Project Group Milestone Collection
        private EntityCollection CreateProjGroupMilestone(IOrganizationService _orgService, string projGroupTplID, string pgTplExcel, DateTime projGroupStartDate, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            EntityCollection msCollection = new EntityCollection();
            string startPhaseName = string.Empty, startStatusName = string.Empty;
            //Logger.Add("createProjGroupMilestone: " + projGroupTplID);
            try
            {
                if (!string.IsNullOrEmpty(projGroupTplID))
                {
                    DataCollection<Entity> RelatedEntitis;
                    if (CasheRelationDataAttributes.ContainsKey(pgTplExcel) && CasheRelationDataAttributes[pgTplExcel].Data_Collection_1 != null)
                    {
                        RelatedEntitis = CasheRelationDataAttributes[pgTplExcel].Data_Collection_1;
                    }
                    else
                    {
                        Relationship relationship = new Relationship();
                        relationship.SchemaName = "ddsm_ddsm_projectgrouptemplates_ddsm_projectgrou";

                        QueryExpression query = new QueryExpression();
                        query.EntityName = "ddsm_projectgroupmilestonetemplate";
                        query.ColumnSet = new ColumnSet("ddsm_projectgroupmilestonetemplateid", "ddsm_index");
                        query.Criteria = new FilterExpression();
                        query.AddOrder("ddsm_index", OrderType.Ascending);
                        query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

                        RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
                        relatedEntity.Add(relationship, query);

                        RetrieveRequest request = new RetrieveRequest();
                        request.ColumnSet = new ColumnSet("ddsm_projectgrouptemplatesid", "ddsm_name");
                        request.Target = new EntityReference { Id = new Guid(projGroupTplID), LogicalName = "ddsm_projectgrouptemplates" };
                        request.RelatedEntitiesQuery = relatedEntity;

                        RetrieveResponse response = (RetrieveResponse)_orgService.Execute(request);

                         RelatedEntitis = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship("ddsm_ddsm_projectgrouptemplates_ddsm_projectgrou")].Entities;

                        CasheRelationDataAttributes[pgTplExcel].Data_Collection_1 = RelatedEntitis;
                    }

                    foreach (var RelatedEntity in RelatedEntitis)
                    {
                        //Logger.Add("ddsm_projectgroupmilestonetemplateid: " + RelatedEntity["ddsm_projectgroupmilestonetemplateid"].ToString());
                        Entity ms = new Entity("ddsm_projectgroupmilestone");
                        Entity rmEntity = GetDataRelationMapping(_orgService, "ddsm_projectgroupmilestone", "ddsm_projectgroupmilestonetemplate", new Guid(RelatedEntity["ddsm_projectgroupmilestonetemplateid"].ToString()));
                        if (rmEntity != null)
                        {
                            foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                            {
                                ms[Attribute.Key] = Attribute.Value;
                                //ms[Attribute.Key] = rmEntity.GetAttributeValue(Attribute.Key);

                            }
                            if (ms.Attributes.Contains("ddsm_duration") && ms["ddsm_duration"] != null) { } else ms["ddsm_duration"] = 0;
                            if (Convert.ToInt32(ms["ddsm_index"]) == 1)
                            {
                                ms["ddsm_actualstart"] = projGroupStartDate;
                                ms["ddsm_status"] = new OptionSetValue(962080001);
                            }
                            else
                            {
                                ms["ddsm_status"] = new OptionSetValue(962080000);
                            }
                            //Logger.Add("ms[ddsm_duration]=" + ms["ddsm_duration"].ToString());
                            ms["ddsm_targetstart"] = projGroupStartDate;
                            projGroupStartDate = projGroupStartDate.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                            ms["ddsm_targetend"] = projGroupStartDate;

                            if (!ms.Attributes.ContainsKey("transactioncurrencyid") && _thisSettings.CurrencyGuid != Guid.Empty)
                            {
                                ms["transactioncurrencyid"] = new EntityReference("transactioncurrency", _thisSettings.CurrencyGuid);
                            }
                            ms["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);

                            msCollection.Entities.Add(ms);
                            msCollection.EntityName = ms.LogicalName;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Add(e.Message);
            }
            return msCollection;
        }
        //Create Project Group Financials Collection
        private EntityCollection CreateProjectGroupFinancials(IOrganizationService _orgService, string ProjGroupEntityUniqueID, string ProjGroupTableName, string projGroupTplID, string pgTplExcel, Entity parentPG, EntityCollection milestoneCollection, ConcurrentDictionary<string, EntityJson> objects, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            EntityCollection fnclCollection = new EntityCollection();
            DateTime projStartDate = DataUploader.ConverAttributToDateTimeUtc("today", _thisSettings.TimeZoneInfo);

            if (!string.IsNullOrEmpty(projGroupTplID))
            {
                DataCollection<Entity> RelatedEntitis;
                if (CasheRelationDataAttributes.ContainsKey(pgTplExcel) && CasheRelationDataAttributes[pgTplExcel].Data_Collection_2 != null)
                {
                    RelatedEntitis = CasheRelationDataAttributes[pgTplExcel].Data_Collection_2;
                }
                else
                {
                    Relationship relationship = new Relationship();
                    relationship.SchemaName = "ddsm_ddsm_projectgrouptemplates_ddsm_financialte";

                    QueryExpression query = new QueryExpression();
                    query.EntityName = "ddsm_financialtemplate";
                    query.ColumnSet = new ColumnSet("ddsm_financialtemplateid");
                    query.Criteria = new FilterExpression();
                    query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

                    RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
                    relatedEntity.Add(relationship, query);

                    RetrieveRequest request = new RetrieveRequest();
                    request.ColumnSet = new ColumnSet("ddsm_projectgrouptemplatesid", "ddsm_name");
                    request.Target = new EntityReference { Id = new Guid(projGroupTplID), LogicalName = "ddsm_projectgrouptemplates" };
                    request.RelatedEntitiesQuery = relatedEntity;

                    RetrieveResponse response = (RetrieveResponse)_orgService.Execute(request);

                    RelatedEntitis = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship("ddsm_ddsm_projectgrouptemplates_ddsm_financialte")].Entities;


                    CasheRelationDataAttributes[pgTplExcel].Data_Collection_2 = RelatedEntitis;
                }

                foreach (var RelatedEntity in RelatedEntitis)
                {
                    Entity newEntiy = new Entity("ddsm_projectgroupfinancials");

                    Entity rmEntity = GetDataRelationMapping(_orgService, "ddsm_projectgroupfinancials", "ddsm_financialtemplate", new Guid(RelatedEntity["ddsm_financialtemplateid"].ToString()));
                    if (rmEntity != null)
                    {
                        foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                        {
                            newEntiy[Attribute.Key] = Attribute.Value;
                        }

                        int initMsIdx = (newEntiy.Attributes.Contains("ddsm_initiatingmilestoneindex")) ? newEntiy.GetAttributeValue<int>("ddsm_initiatingmilestoneindex") : 1;

                        if (parentPG.Attributes.ContainsKey("ddsm_programofferingid"))
                            newEntiy["ddsm_programofferingid"] = new EntityReference(parentPG.GetAttributeValue<EntityReference>("ddsm_programofferingid").LogicalName, parentPG.GetAttributeValue<EntityReference>("ddsm_programofferingid").Id);

                        projStartDate = milestoneCollection.Entities[initMsIdx - 1].GetAttributeValue<DateTime>("ddsm_targetstart");

                        if (newEntiy.Attributes.ContainsKey("ddsm_stagename1"))
                        {
                            newEntiy["ddsm_duration1"] = (newEntiy.Attributes.ContainsKey("ddsm_duration1")) ? newEntiy.GetAttributeValue<int>("ddsm_duration1") : 0;
                            newEntiy["ddsm_targetstart1"] = projStartDate;
                            projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration1"));
                            newEntiy["ddsm_targetend1"] = projStartDate;
                        }
                        if (newEntiy.Attributes.ContainsKey("ddsm_stagename2"))
                        {
                            newEntiy["ddsm_duration2"] = (newEntiy.Attributes.ContainsKey("ddsm_duration2")) ? newEntiy.GetAttributeValue<int>("ddsm_duration2") : 0;
                            newEntiy["ddsm_targetstart2"] = projStartDate;
                            projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration2"));
                            newEntiy["ddsm_targetend2"] = projStartDate;
                        }
                        if (newEntiy.Attributes.ContainsKey("ddsm_stagename3"))
                        {
                            newEntiy["ddsm_duration3"] = (newEntiy.Attributes.ContainsKey("ddsm_duration3")) ? newEntiy.GetAttributeValue<int>("ddsm_duration3") : 0;
                            newEntiy["ddsm_targetstart3"] = projStartDate;
                            projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration3"));
                            newEntiy["ddsm_targetend3"] = projStartDate;
                        }
                        if (newEntiy.Attributes.ContainsKey("ddsm_stagename4"))
                        {
                            newEntiy["ddsm_duration4"] = (newEntiy.Attributes.ContainsKey("ddsm_duration4")) ? newEntiy.GetAttributeValue<int>("ddsm_duration4") : 0;
                            newEntiy["ddsm_targetstart4"] = projStartDate;
                            projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration4"));
                            newEntiy["ddsm_targetend4"] = projStartDate;
                        }
                        if (newEntiy.Attributes.ContainsKey("ddsm_stagename5"))
                        {
                            newEntiy["ddsm_duration5"] = (newEntiy.Attributes.ContainsKey("ddsm_duration5")) ? newEntiy.GetAttributeValue<int>("ddsm_duration5") : 0;
                            newEntiy["ddsm_targetstart5"] = projStartDate;
                            projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration5"));
                            newEntiy["ddsm_targetend5"] = projStartDate;
                        }

                        if (!newEntiy.Attributes.ContainsKey("transactioncurrencyid") && _thisSettings.CurrencyGuid != Guid.Empty)
                        {
                            newEntiy["transactioncurrencyid"] = new EntityReference("transactioncurrency", _thisSettings.CurrencyGuid);
                        }

                        if (!newEntiy.Attributes.Contains("ddsm_status"))
                            newEntiy["ddsm_status"] = new OptionSetValue(962080000);

                        newEntiy["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);

                        //AutoNumbering
                        if (newEntiy.Attributes.ContainsKey("ddsm_pgfinancialnumber") && !string.IsNullOrEmpty(newEntiy["ddsm_pgfinancialnumber"].ToString())) { }
                        else
                            newEntiy["ddsm_pgfinancialnumber"] = GetAutoNumber("ddsm_projectgroupfinancials", _orgService);

                        fnclCollection.Entities.Add(newEntiy);
                        fnclCollection.EntityName = newEntiy.LogicalName;
                    }
                    break;
                }
            }
            return fnclCollection;
        }
        private ProjectBusinessProcess GetBProcessProjGroup(IOrganizationService _orgService, string projTplName, string pgTplExcel)
        {
            //tracer.Trace("getBProcess: projTplName: " + projTplName);
            ProjectBusinessProcess projBP = new ProjectBusinessProcess();
            projBP.Id = null;
            //Get Project Template ID
            if (CasheRelationDataAttributes.ContainsKey(pgTplExcel) && CasheRelationDataAttributes[pgTplExcel].BusinessProcess != null)
            {
                projBP = CasheRelationDataAttributes[pgTplExcel].BusinessProcess;
            }
            else {
                try
                {
                    QueryExpression processQuery = new QueryExpression("workflow");
                    processQuery.ColumnSet = new ColumnSet("name", "workflowid");
                    processQuery.ColumnSet = new ColumnSet(true);
                    processQuery.Criteria.AddCondition("name", ConditionOperator.Equal, projTplName);
                    processQuery.Criteria.AddCondition("primaryentity", ConditionOperator.Equal, "ddsm_projectgroup");
                    //processQuery.Criteria.AddCondition("statecode", ConditionOperator.Equal, 1);
                    EntityCollection processRetrieve = _orgService.RetrieveMultiple(processQuery);
                    if (processRetrieve != null && processRetrieve.Entities.Count == 1)
                    {
                        projBP.Id = processRetrieve.Entities[0].Attributes["workflowid"].ToString();
                        projBP.Name = processRetrieve.Entities[0].Attributes["name"].ToString();
                        projBP.Stages = new List<ProjectBusinessProcessStage>();
                        QueryExpression stageQuery = new QueryExpression("processstage");
                        stageQuery.ColumnSet = new ColumnSet("stagename", "processstageid");
                        //stageQuery.ColumnSet = new ColumnSet(true);
                        stageQuery.Criteria.AddCondition("processid", ConditionOperator.Equal, new Guid(projBP.Id));
                        EntityCollection stageRetrieve = _orgService.RetrieveMultiple(stageQuery);
                        if (stageRetrieve != null && stageRetrieve.Entities.Count > 0)
                        {
                            foreach (var stage in stageRetrieve.Entities)
                            {
                                ProjectBusinessProcessStage stageObj = new ProjectBusinessProcessStage();
                                stageObj.Id = stage.Attributes["processstageid"].ToString();
                                stageObj.Name = stage.Attributes["stagename"].ToString();
                                projBP.Stages.Add(stageObj);
                            }
                        }
                        CasheRelationDataAttributes[pgTplExcel].BusinessProcess = projBP;
                    }
                }
                catch (Exception e)
                {
                    projBP = new ProjectBusinessProcess();
                    projBP.Id = null;
                }
            }
            return projBP;
        }

    }


}
