﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataUploader;
using static DataUploader.Model.Enums;
using static DataUploader.Model.Errors;
using static DataUploader.Model.PrimaryKey;
using static DataUploader.Model.AutoNumberingManager;
using Microsoft.Xrm.Sdk.Messages;
using System.Text.RegularExpressions;
using System.Collections.Concurrent;
using Microsoft.Xrm.Sdk;

#if RELESEPLUGIN
#else
using SocketClients;
using DataUploader.Service;
#endif

namespace DataUploader.Creator
{
    public class CreateProjReportings : BaseMethod
    {
        public void CreateProjReporting(IOrganizationService _orgService, ConcurrentDictionary<string, CreatedEntity> ProjectEntities, ConcurrentDictionary<string, CreatedEntity> ProjReportEntities, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            Logger2 = new List<string>();
            configObject = Newtonsoft.Json.JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);

            var keyEntity = configObject.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_projectreporting").Key;
            if (string.IsNullOrEmpty(keyEntity))
            {
                return;
            }

#if RELESEPLUGIN
#else
            wsMessageService = new WSMessageUploaderService(_thisSettings);
            wsMessageService.SendMessage(MessageType.INFORMATION, GetTextError(380036), MessageStatus.START);
#endif

            statisticRecords = new StatisticRecords();
            statisticsRecords.AddOrUpdate("stat", statisticRecords, (oldkey, oldvalue) => statisticRecords);

            entityCollectionPagination = new PaginationRetriveMultiple();
            DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.projectReportingsProcessing, _thisSettings.TargetEntity.Id);
            do
            {
                entityCollectionPagination = GetDataByEntityName(_orgService, "ddsm_projectreporting", configObject, _thisSettings.TargetEntity.Id, Logger, false, entityCollectionPagination.pageNumber, entityCollectionPagination.PagingCookie, entityCollectionPagination.MoreRecords, _thisSettings.GlobalPageRecordsCount);
                if (entityCollectionPagination != null && entityCollectionPagination.RetrieveCollection.Entities.Count > 0)
                {
                    statisticsRecords["stat"].All += entityCollectionPagination.RetrieveCollection.Entities.Count;
                    CreateRecordSet(_orgService, configObject, ProjectEntities, ProjReportEntities, entityCollectionPagination.RetrieveCollection, statisticsRecords["stat"], Logger2, _thisSettings);
                }
            }
            while (entityCollectionPagination != null && entityCollectionPagination.MoreRecords);
            if (statisticsRecords["stat"].All > 0)
            {
                Logger2.Add(GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";");
#if RELESEPLUGIN
#else
                wsMessageService.SendMessage(MessageType.SUCCESS, "Project Reporting: " + GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";", MessageStatus.FINISH);
#endif

                DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger2, "ddsm_log6");
            }
        }

        //Create Project Reporting Record
        private void CreateRecordSet(IOrganizationService _orgService, ConcurrentDictionary<string, EntityJson> objects, ConcurrentDictionary<string, CreatedEntity> ProjectEntities, ConcurrentDictionary<string, CreatedEntity> ProjReportEntities, EntityCollection table, StatisticRecords statisticRecords, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_projectreporting").Key;
            string EntityUniqueID = string.Empty;

            int EntityDataRow = 0, updateDataRow = 0, insertDataRow = 0, doNothing = 0;
            //Project Column
            string uniqueIdColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " GUID").ToLower()).Key;
            var projectTableName = string.Empty;
            projectTableName = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_project").Key;
            string projectColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (projectTableName + " GUID").ToLower()).Key;

            if (string.IsNullOrEmpty(uniqueIdColumnKey)) return;
            if (string.IsNullOrEmpty(projectColumnKey)) return;
            Int64 requestsCount = _thisSettings.GlobalRequestsCount, itemsRequestCount = 0, requestSets = 1;
            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            List<string> setUniqueID = new List<string>();
            foreach (Entity dr in table.Entities)
            {

                EntityDataRow++;
                if (EntityDataRow >= 1)
                {
                    try
                    {
                        EntityUniqueID = string.Empty;

                        EntityUniqueID = dr["ddsm_" + uniqueIdColumnKey.ToLower()].ToString();
                        if (string.IsNullOrEmpty(EntityUniqueID)) continue;

                        Entity newEntiy = new Entity("ddsm_projectreporting");

                        Entity newEntiyXLSX = GenerationEntity(_orgService, objects, keyEntity, dr, _thisSettings.TargetEntity.Id);
                        if (newEntiyXLSX.Attributes.Count == 0) continue;
                        foreach (KeyValuePair<String, Object> Attribute in newEntiyXLSX.Attributes)
                        {
                            newEntiy[Attribute.Key] = Attribute.Value;
                            //newEntiy[Attribute.Key] = newEntiyXLSX.GetAttributeValue(Attribute.Key);
                        }
                        if (!string.IsNullOrEmpty(projectColumnKey) && !string.IsNullOrEmpty(Convert.ToString(dr["ddsm_" + projectColumnKey.ToLower()])))
                        {
                            GetDictionaryObjectbyGUID(_orgService, dr["ddsm_" + projectColumnKey.ToLower()].ToString(), "ddsm_project", objects, ProjectEntities, _thisSettings.TargetEntity.Id);
                            if (ProjectEntities.ContainsKey(Convert.ToString(dr["ddsm_" + projectColumnKey.ToLower()])) && (ProjectEntities[Convert.ToString(dr["ddsm_" + projectColumnKey.ToLower()])].EntityGuid != new Guid() || ProjectEntities[Convert.ToString(dr["ddsm_" + projectColumnKey.ToLower()])].EntityGuid != Guid.Empty))
                            {
                                if (ProjectEntities[dr["ddsm_" + projectColumnKey.ToLower()].ToString()].EntityGuid.ToString() == (new Guid()).ToString()) continue;
                                newEntiy["ddsm_projectid"] = new EntityReference("ddsm_project", ProjectEntities[dr["ddsm_" + projectColumnKey.ToLower()].ToString()].EntityGuid);

                                Entity rmEntity = GetDataRelationMapping(_orgService, "ddsm_projectreporting", "ddsm_project", ProjectEntities[dr["ddsm_" + projectColumnKey.ToLower()].ToString()].EntityGuid);
                                if (rmEntity != null)
                                {
                                    foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                                    {
                                        newEntiy[Attribute.Key] = Attribute.Value;
                                    }
                                }
                            }
                            else continue;
                        }
                        else continue;

                        if (!newEntiy.Attributes.Contains("ddsm_name") && dr.Attributes.ContainsKey("ddsm_" + projectColumnKey.ToLower()) && !string.IsNullOrEmpty(dr["ddsm_" + projectColumnKey.ToLower()].ToString()) && ProjectEntities.ContainsKey(dr["ddsm_" + projectColumnKey.ToLower()].ToString()))
                            newEntiy["ddsm_name"] = ProjectEntities[dr["ddsm_" + projectColumnKey.ToLower()].ToString()].Name;
                        else if (!newEntiy.Attributes.Contains("ddsm_name") && dr.Attributes.ContainsKey("ddsm_" + projectColumnKey.ToLower()) && !string.IsNullOrEmpty(dr["ddsm_" + projectColumnKey.ToLower()].ToString()))
                            newEntiy["ddsm_name"] = dr["ddsm_" + projectColumnKey.ToLower()].ToString();
                        else continue;

                        newEntiy["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);

                        string duplicateRecordId = string.Empty;
                        if (_thisSettings.DuplicateDetect)
                        {
                            //duplicateRecordId = VerifyDuplicateRecord(_orgService, newEntiy, objects[keyEntity].Name.ToString().ToLower());
                        }

                        CreatedEntity ProjReportEntity = new CreatedEntity();

                        Boolean Successful = false;
                        if (!string.IsNullOrEmpty(duplicateRecordId))
                        {
                            string[] reqDuplicate = Regex.Split(duplicateRecordId, "::");
                            ProjReportEntity.EntityGuid = new Guid(reqDuplicate[0]);
                            ProjReportEntity.Name = reqDuplicate[1];
                            ProjReportEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            ProjReportEntity.Status = false;
                            ProjReportEntity.StatusDB = false;
                            ProjReportEntities.AddOrUpdate(EntityUniqueID, ProjReportEntity, (oldkey, oldvalue) => ProjReportEntity);

                            switch (_thisSettings.DedupRules)
                            {
                                case 962080000: //Update
                                    if (newEntiy.Attributes.Count > 0)
                                    {
                                        Entity updateEntity = new Entity("ddsm_projectreporting", new Guid(reqDuplicate[0]));
                                        foreach (KeyValuePair<String, Object> Attribute in newEntiy.Attributes)
                                        {
                                            updateEntity[Attribute.Key] = Attribute.Value;
                                            //updateEntity[Attribute.Key] = newEntiy.GetAttributeValue(Attribute.Key);
                                        }

                                        UpdateRequest updateRequest = new UpdateRequest();
                                        updateRequest.Target = updateEntity;
                                        emRequest.Requests.Add(updateRequest);
                                        setUniqueID.Add(EntityUniqueID);
                                        itemsRequestCount++;
                                        //statisticRecords.Updated++;
                                    }
                                    else
                                    {
                                        ProjReportEntities[EntityUniqueID].Status = true;
                                        SkippedEntities.AddOrUpdate(EntityUniqueID, ProjReportEntities[EntityUniqueID], (oldkey, oldvalue) => ProjReportEntities[EntityUniqueID]);
                                        statisticRecords.DoNothing++;

                                        itemsRequestCount++;
                                    }
                                    break;
                                case 962080001:
                                    ProjReportEntities[EntityUniqueID].Status = true;
                                    SkippedEntities.AddOrUpdate(EntityUniqueID, ProjReportEntities[EntityUniqueID], (oldkey, oldvalue) => ProjReportEntities[EntityUniqueID]);
                                    statisticRecords.DoNothing++;

                                    itemsRequestCount++;
                                    break;
                            }

                        }
                        else
                        {

                            CreateRequest createRequest = new CreateRequest();
                            createRequest.Target = newEntiy;
                            emRequest.Requests.Add(createRequest);
                            setUniqueID.Add(EntityUniqueID);
                            itemsRequestCount++;

                            ProjReportEntity.EntityGuid = new Guid();
                            ProjReportEntity.Name = newEntiy["ddsm_name"].ToString();
                            ProjReportEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            ProjReportEntity.Status = false;
                            ProjReportEntity.StatusDB = false;
                            //ProjReportEntities.Add(EntityUniqueID, ProjReportEntity);
                            ProjReportEntities.AddOrUpdate(EntityUniqueID, ProjReportEntity, (oldkey, oldvalue) => ProjReportEntity);

                        }
                    }
                    catch (Exception e)
                    {
                        itemsRequestCount++;

                        Logger.Add("ERROR >>> UniqueGUID: " + EntityUniqueID + "; " + e.Message);
#if RELESEPLUGIN
#else
                        wsMessageService.SendMessage(MessageType.ERROR, "Project Reporting UniqueGUID: " + EntityUniqueID + "; Message: " + e.Message, MessageStatus.FINISH);
#endif
                    }
                }

                //Create Multiple
                if (itemsRequestCount == (requestSets * requestsCount))
                {
                    if (emRequest.Requests.Count > 0)
                    {
                        ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                        CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, ProjReportEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                        //Update Source records
                        UpdateSourceRecords(_orgService, objects, RequestEntities, "ddsm_projectreporting", _thisSettings.CallExecuteMultiple);
                    }

                    //Update Source records (Skipped)
                    if (SkippedEntities.Count > 0)
                    {
                        UpdateSourceRecords(_orgService, objects, SkippedEntities, "ddsm_projectreporting", _thisSettings.CallExecuteMultiple);
                    }
                    SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();

                    requestSets++;
                    emRequest = new ExecuteMultipleRequest
                    {
                        Requests = new OrganizationRequestCollection(),
                        Settings = new ExecuteMultipleSettings
                        {
                            ContinueOnError = true,
                            ReturnResponses = true
                        }
                    };

                }
            }

            if (emRequest.Requests.Count > 0 && emRequest.Requests.Count > 0)
            {
                ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, ProjReportEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                //Update Source records
                UpdateSourceRecords(_orgService, objects, RequestEntities, "ddsm_projectreporting", _thisSettings.CallExecuteMultiple);
            }

            if (SkippedEntities.Count > 0)
            {
                //Update Source records (Skipped)
                UpdateSourceRecords(_orgService, objects, SkippedEntities, "ddsm_projectreporting", _thisSettings.CallExecuteMultiple);
                SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
            }

        }

    }
}
