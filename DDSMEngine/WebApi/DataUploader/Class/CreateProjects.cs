﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataUploader.Model.Enums;
using static DataUploader.Model.Errors;
using static DataUploader.Model.PrimaryKey;
using static DataUploader.Model.AutoNumberingManager;
using Microsoft.Xrm.Sdk.Messages;
using System.Collections.Concurrent;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Text.RegularExpressions;

#if RELESEPLUGIN
#else
using SocketClients;
using DataUploader.Service;
#endif

namespace DataUploader.Creator
{
    public class CreateProjects: BaseMethod
    {
        public void CreateProject(IOrganizationService _orgService, ConcurrentDictionary<string, CreatedEntity> AccountEntities, ConcurrentDictionary<string, CreatedEntity> SiteEntities, ConcurrentDictionary<string, CreatedEntity> ProjectGroupEntities, ConcurrentDictionary<string, CreatedEntity> ProjectEntities, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            Logger2 = new List<string>();
            configObject = Newtonsoft.Json.JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);

            var keyEntity = configObject.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_project").Key;
            if (string.IsNullOrEmpty(keyEntity))
            {
                return;
            }

#if RELESEPLUGIN
#else
            wsMessageService = new WSMessageUploaderService(_thisSettings);
            wsMessageService.SendMessage(MessageType.INFORMATION, GetTextError(380034), MessageStatus.START);
#endif

            statisticRecords = new StatisticRecords();
            statisticsRecords.AddOrUpdate("stat", statisticRecords, (oldkey, oldvalue) => statisticRecords);

            entityCollectionPagination = new PaginationRetriveMultiple();
            DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.projectsProcessing, _thisSettings.TargetEntity.Id);
            do
            {
                entityCollectionPagination = GetDataByEntityName(_orgService, "ddsm_project", configObject, _thisSettings.TargetEntity.Id, Logger, false, entityCollectionPagination.pageNumber, entityCollectionPagination.PagingCookie, entityCollectionPagination.MoreRecords, _thisSettings.GlobalPageRecordsCount);
                if (entityCollectionPagination != null && entityCollectionPagination.RetrieveCollection.Entities.Count > 0)
                {
                    statisticsRecords["stat"].All += entityCollectionPagination.RetrieveCollection.Entities.Count;
                    CreateRecordSet(_orgService, configObject, AccountEntities, SiteEntities, ProjectGroupEntities, ProjectEntities, entityCollectionPagination.RetrieveCollection, statisticsRecords["stat"], Logger2, _thisSettings);
                }
            }
            while (entityCollectionPagination != null && entityCollectionPagination.MoreRecords);
            if (statisticsRecords["stat"].All > 0)
            {
                Logger2.Add(GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";");

#if RELESEPLUGIN
#else
                wsMessageService.SendMessage(MessageType.SUCCESS, "Projects: " + GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";", MessageStatus.FINISH);
#endif

                DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger2, "ddsm_log4");
            }

        }

        //Create Project
        private void CreateRecordSet(IOrganizationService _orgService, ConcurrentDictionary<string, EntityJson> objects, ConcurrentDictionary<string, CreatedEntity> AccountEntities, ConcurrentDictionary<string, CreatedEntity> SiteEntities, ConcurrentDictionary<string, CreatedEntity> ProjectGroupEntities, ConcurrentDictionary<string, CreatedEntity> ProjectEntities, EntityCollection table, StatisticRecords statisticRecords, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_project").Key;
            string EntityUniqueID = string.Empty;

            string uniqueIdColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " GUID").ToLower()).Key;
            string projIdColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " ID").ToLower()).Key;

            //Get Account Column Key
            var accountTableName = string.Empty;
            accountTableName = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "account").Key;
            string accColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (accountTableName + " GUID").ToLower()).Key;

            //Get Site Column Key
            var siteTableName = string.Empty;
            siteTableName = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_site").Key;
            string siteColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (siteTableName + " GUID").ToLower()).Key;
            //Get Project Template Column Key
            string projTplColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " Template").ToLower()).Key;

            string propertyManagerColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => (x.Value.Name.ToLower()).Split(':')[0] == ("ddsm_propertymanager").ToLower()).Key;
            string primaryContactColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => (x.Value.Name.ToLower()).Split(':')[0] == ("ddsm_primarycontactid").ToLower()).Key;
            string partnerContactColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => (x.Value.Name.ToLower()).Split(':')[0] == ("ddsm_partnercontact").ToLower()).Key;

            if (string.IsNullOrEmpty(uniqueIdColumnKey)) return;
            if (string.IsNullOrEmpty(accColumnKey)) return;
            if (string.IsNullOrEmpty(siteColumnKey))
                siteColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == ("Parent " + siteTableName + " GUID").ToLower()).Key;
            if (string.IsNullOrEmpty(siteColumnKey)) return;
            if (string.IsNullOrEmpty(projTplColumnKey)) return;

            int EntityDataRow = 0, insertDataRow = 0, updateDataRow = 0, doNothing = 0; ;
            Int64 requestsCount = _thisSettings.GlobalRequestsCount, itemsRequestCount = 0, requestSets = 1;
            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            List<string> setUniqueID = new List<string>();

            foreach (Entity dr in table.Entities)
            {

                EntityUniqueID = string.Empty;
                EntityDataRow++;
                if (EntityDataRow >= 1)
                {
                    try
                    {
                        EntityUniqueID = dr["ddsm_" + uniqueIdColumnKey.ToLower()].ToString();
                        //Logger.Add("EntityUniqueID " + EntityUniqueID);
                        if (string.IsNullOrEmpty(EntityUniqueID)) continue;

                        //Verify duplicate record GUID
                        if (ProjectEntities.ContainsKey(EntityUniqueID))
                        {
                            Logger.Add("ERROR >>> GUID: " + EntityUniqueID + " Not unique in sheet Projects list");
                            continue;
                        }

                        //Parent Account
                        GetDictionaryObjectbyGUID(_orgService, dr["ddsm_" + accColumnKey.ToLower()].ToString(), "account", objects, AccountEntities, _thisSettings.TargetEntity.Id);
                        if (!AccountEntities.ContainsKey(dr["ddsm_" + accColumnKey.ToLower()].ToString())) continue;
                        if (string.IsNullOrEmpty(AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid.ToString()))
                        {
                            Logger.Add("ERROR >>> GUID: " + EntityUniqueID + " Account GUID not found!");
                            continue;
                        }
                        //Parent Site
                        GetDictionaryObjectbyGUID(_orgService, dr["ddsm_" + siteColumnKey.ToLower()].ToString(), "ddsm_site", objects, SiteEntities, _thisSettings.TargetEntity.Id);
                        if (!SiteEntities.ContainsKey(dr["ddsm_" + siteColumnKey.ToLower()].ToString()))
                        {
                            Logger.Add("ERROR >>> GUID: " + EntityUniqueID + " Site GUID not found!");
                            continue;
                        }
                        if (string.IsNullOrEmpty(SiteEntities[dr["ddsm_" + siteColumnKey.ToLower()].ToString()].EntityGuid.ToString())) continue;

                        string projTpl = null, projTplID = null, projTplName = null, bpfName = string.Empty;

                        //Get Project Template Column
                        if (dr.Attributes.ContainsKey("ddsm_" + projTplColumnKey.ToLower()))
                        {
                            projTpl = dr["ddsm_" + projTplColumnKey.ToLower()].ToString();
                        }
                        else
                        {
                            Logger.Add("ERROR >>> GUID: " + EntityUniqueID + "; Proj Tpl Columns = NULL");
                            continue;
                        }

                        //Get Project Template ID
                        if (string.IsNullOrEmpty(projTpl))
                        {
                            Logger.Add("ERROR >>> GUID: " + EntityUniqueID + "; Project Template not found!");
#if RELESEPLUGIN
#else
                            wsMessageService.SendMessage(MessageType.ERROR, "Project UniqueGUID: " + EntityUniqueID + "; Project Template not found!", MessageStatus.FINISH);
#endif
                            continue;
                        }
                        if (CasheRelationDataAttributes.ContainsKey(projTpl))
                        {
                            projTplID = CasheRelationDataAttributes[projTpl].tplId;
                            projTplName = CasheRelationDataAttributes[projTpl].tplName;
                            bpfName = CasheRelationDataAttributes[projTpl].bpfName;
                        }
                        else
                        {
                            QueryExpression projQuery = new QueryExpression { EntityName = "ddsm_projecttemplate", ColumnSet = new ColumnSet("ddsm_projecttemplateid", "ddsm_name", "ddsm_bpfname") };
                            projQuery.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, projTpl);
                            projQuery.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
                            EntityCollection ConfigRetrieve = _orgService.RetrieveMultiple(projQuery);
                            if (ConfigRetrieve != null && ConfigRetrieve.Entities.Count == 1)
                            {
                                projTplID = ConfigRetrieve.Entities[0].Attributes["ddsm_projecttemplateid"].ToString();
                                projTplName = ConfigRetrieve.Entities[0].Attributes["ddsm_name"].ToString();
                                RelationDataAttributes relationDataAttributes = new RelationDataAttributes();
                                relationDataAttributes.tplId = projTplID;
                                relationDataAttributes.tplName = projTplName;
                                if (ConfigRetrieve.Entities[0].Attributes.ContainsKey("ddsm_bpfname"))
                                {
                                    bpfName = ConfigRetrieve.Entities[0].Attributes["ddsm_bpfname"].ToString();
                                    relationDataAttributes.bpfName = bpfName;
                                }
                                CasheRelationDataAttributes.AddOrUpdate(projTpl, relationDataAttributes, (oldkey, oldvalue) => relationDataAttributes);
                            }
                        }

                        Entity newEntiy = new Entity("ddsm_project");
                        newEntiy["ddsm_phasenumber"] = 1;
                        int ln = 20;
                        if (!string.IsNullOrEmpty(projTplID))
                        {
                            Entity rmEntity;
                            if (CasheRelationDataAttributes.ContainsKey(projTpl) && CasheRelationDataAttributes[projTpl].Data_Attributes_1 != null)
                            {
                                rmEntity = CasheRelationDataAttributes[projTpl].Data_Attributes_1;
                            }
                            else {
                                rmEntity = GetDataRelationMapping(_orgService, "ddsm_project", "ddsm_projecttemplate", new Guid(projTplID));
                                CasheRelationDataAttributes[projTpl].Data_Attributes_1 = rmEntity;
                            }

                            if (rmEntity != null)
                            {
                                foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                                {
                                    newEntiy[Attribute.Key] = Attribute.Value;
                                }

                            }
                            else
                                newEntiy = new Entity("ddsm_project");
                        }
                        else
                        {
                            Logger.Add("ERROR >>> GUID: " + EntityUniqueID + "; Project Template '" + projTpl + "' not found in DB");
                            continue;
                        }

                        //Get relate fields Account
                        if (AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid.ToString() == (new Guid()).ToString()) continue;
                        Entity rmEntityAcc = new Entity();
                        if (CasheRelationDataAttributes.ContainsKey(AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid.ToString()))
                        {
                            rmEntityAcc = CasheRelationDataAttributes[AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid.ToString()].Data_Attributes_1;
                        }
                        else {
                            Entity rmEntityAccMod = GetDataRelationMapping(_orgService, "ddsm_project", "account", AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid);

                            if (rmEntityAccMod.Attributes.ContainsKey("ddsm_accountid"))
                            {
                                var parentAccount = rmEntityAccMod.GetAttributeValue<EntityReference>("ddsm_accountid");

                                foreach (KeyValuePair<String, Object> Attribute in rmEntityAccMod.Attributes)
                                {
                                    if (!(parentAccount.GetType() == Attribute.Value.GetType() && parentAccount.Id == (Attribute.Value as EntityReference).Id))
                                    {
                                        rmEntityAcc[Attribute.Key] = Attribute.Value;
                                    }
                                }
                                rmEntityAcc["ddsm_accountid"] = parentAccount;
                            }
                            else {
                                rmEntityAcc = rmEntityAccMod;
                            }

                            RelationDataAttributes relationDataAttributes = new RelationDataAttributes();
                            relationDataAttributes.Data_Attributes_1 = rmEntityAcc;
                            CasheRelationDataAttributes.AddOrUpdate(AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid.ToString(), relationDataAttributes, (oldkey, oldvalue) => relationDataAttributes);
                        }
                        foreach (KeyValuePair<String, Object> Attribute in rmEntityAcc.Attributes)
                        {
                            newEntiy[Attribute.Key] = Attribute.Value;
                        }

                        //Get relate fields Site
                        if (SiteEntities[dr["ddsm_" + siteColumnKey.ToLower()].ToString()].EntityGuid.ToString() == (new Guid()).ToString()) continue;
                        Entity rmEntitySite = new Entity();
                        if (CasheRelationDataAttributes.ContainsKey(SiteEntities[dr["ddsm_" + siteColumnKey.ToLower()].ToString()].EntityGuid.ToString()))
                        {
                            rmEntitySite = CasheRelationDataAttributes[SiteEntities[dr["ddsm_" + siteColumnKey.ToLower()].ToString()].EntityGuid.ToString()].Data_Attributes_1;
                        }
                        else
                        {
                            Entity rmEntitySiteMod = GetDataRelationMapping(_orgService, "ddsm_project", "ddsm_site", SiteEntities[dr["ddsm_" + siteColumnKey.ToLower()].ToString()].EntityGuid);

                            if (rmEntitySiteMod.Attributes.ContainsKey("ddsm_parentsiteid"))
                            {
                                var parentSite = rmEntitySiteMod.GetAttributeValue<EntityReference>("ddsm_parentsiteid");

                                foreach (KeyValuePair<String, Object> Attribute in rmEntitySiteMod.Attributes)
                                {
                                    if (!(parentSite.GetType() == Attribute.Value.GetType() && parentSite.Id == (Attribute.Value as EntityReference).Id))
                                    {
                                        rmEntitySite[Attribute.Key] = Attribute.Value;
                                    }
                                }
                                rmEntitySite["ddsm_parentsiteid"] = parentSite;
                            }
                            else
                            {
                                rmEntitySite = rmEntitySiteMod;
                            }

                            RelationDataAttributes relationDataAttributes = new RelationDataAttributes();
                            relationDataAttributes.Data_Attributes_1 = rmEntitySite;
                            CasheRelationDataAttributes.AddOrUpdate(SiteEntities[dr["ddsm_" + siteColumnKey.ToLower()].ToString()].EntityGuid.ToString(), relationDataAttributes, (oldkey, oldvalue) => relationDataAttributes);
                        }
                        foreach (KeyValuePair<String, Object> Attribute in rmEntitySite.Attributes)
                        {
                            newEntiy[Attribute.Key] = Attribute.Value;
                        }

                        //Get relate fields Program Offering
                        if (newEntiy.Attributes.ContainsKey("ddsm_programoffering"))
                        {
                            Entity rmEntityPO = new Entity();
                            if (CasheRelationDataAttributes.ContainsKey(newEntiy.GetAttributeValue<EntityReference>("ddsm_programoffering").Id.ToString()))
                            {
                                rmEntityPO = CasheRelationDataAttributes[newEntiy.GetAttributeValue<EntityReference>("ddsm_programoffering").Id.ToString()].Data_Attributes_1;
                            }
                            else
                            {
                                rmEntityPO = GetDataRelationMapping(_orgService, "ddsm_project", "ddsm_programoffering", newEntiy.GetAttributeValue<EntityReference>("ddsm_programoffering").Id);
                                RelationDataAttributes relationDataAttributes = new RelationDataAttributes();
                                relationDataAttributes.Data_Attributes_1 = rmEntityPO;
                                CasheRelationDataAttributes.AddOrUpdate(newEntiy.GetAttributeValue<EntityReference>("ddsm_programoffering").Id.ToString(), relationDataAttributes, (oldkey, oldvalue) => relationDataAttributes);
                            }
                            if (rmEntityPO != null)
                            {
                                foreach (KeyValuePair<String, Object> Attribute in rmEntityPO.Attributes)
                                {
                                    newEntiy[Attribute.Key] = Attribute.Value;
                                }
                            }
                        }

                        //Get XLSX Data
                        var projectID = string.Empty;
                        Entity newEntiyXLSX = GenerationEntity(_orgService, objects, keyEntity, dr, _thisSettings.TargetEntity.Id);
                        if (newEntiyXLSX.Attributes.Count == 0) continue;

                        //AutoNumbering
                        if (newEntiyXLSX.Attributes.ContainsKey("ddsm_projectnumber") && !string.IsNullOrEmpty(newEntiyXLSX["ddsm_projectnumber"].ToString()))
                            newEntiy["ddsm_projectnumber"] = newEntiyXLSX["ddsm_projectnumber"];
                        else
                            newEntiy["ddsm_projectnumber"] = GetAutoNumber("ddsm_project", _orgService);

                        //Create Project Name
                        projectID = newEntiy.GetAttributeValue<string>("ddsm_projectnumber");
                        var newProjName = string.Empty;
                        if (newEntiy.Attributes.ContainsKey("ddsm_name") && !string.IsNullOrEmpty(newEntiy.GetAttributeValue<string>("ddsm_name")))
                            newProjName = newEntiy.GetAttributeValue<string>("ddsm_name");
                        else
                            newProjName = projTplName;
                        if (!string.IsNullOrEmpty(projectID))
                            newEntiy["ddsm_name"] = projectID + "-" + newProjName + "-" + AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].Name;
                        else
                            newEntiy["ddsm_name"] = newProjName + "-" + AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].Name;
                        //Milestones
                        string milestoneActualEndKey = null;
                        milestoneActualEndKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == ("Milestone 1").ToLower()).Key;

                        if (newEntiyXLSX.Attributes.Contains("ddsm_startdate"))
                        {
                            newEntiy["ddsm_startdate"] = newEntiyXLSX.GetAttributeValue<DateTime>("ddsm_startdate");
                        }
                        else if (milestoneActualEndKey != null && dr.Attributes.ContainsKey("ddsm_" + milestoneActualEndKey.ToLower()))
                        {
                            newEntiy["ddsm_startdate"] = DataUploader.ConverAttributToDateTimeUtc(dr["ddsm_" + milestoneActualEndKey.ToLower()], _thisSettings.TimeZoneInfo);
                        }
                        else
                        {
                            newEntiy["ddsm_startdate"] = DataUploader.ConverAttributToDateTimeUtc("today", _thisSettings.TimeZoneInfo);
                        }

                        newEntiy["ddsm_accountid"] = new EntityReference("account", AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid);
                        newEntiy["ddsm_parentsiteid"] = new EntityReference("ddsm_site", SiteEntities[dr["ddsm_" + siteColumnKey.ToLower()].ToString()].EntityGuid);

                        //Set Currency
                        if (!newEntiy.Attributes.ContainsKey("transactioncurrencyid") && _thisSettings.CurrencyGuid != Guid.Empty)
                        {
                                newEntiy["transactioncurrencyid"] = new EntityReference("transactioncurrency", _thisSettings.CurrencyGuid);
                        }

                        //Create & ReCalc Milestone Collection
                        EntityCollection milestoneCollection = CreateMilestones(_orgService, projTplID, projTpl, newEntiy.GetAttributeValue<DateTime>("ddsm_startdate"), newEntiy, Logger, _thisSettings);
                        //Get Business Process project
                        ProjectBusinessProcess projBP = new ProjectBusinessProcess();
                        projBP.Id = null;

                        if (!string.IsNullOrEmpty(bpfName))
                        {
                            projBP = GetBProcessProj(_orgService, bpfName, projTpl);
                        }
                        if (string.IsNullOrEmpty(projBP.Id))
                        {
                            if (CasheRelationDataAttributes[projTpl].BusinessProcess == null)
                            {
                                Logger.Add("Business Process Flow (Project Template: '" + projTplName + "') is empty or not found. An attempt is made to find the BPF by the name of the current project template.");
                            }

                            if (!string.IsNullOrEmpty(projTplName))
                            {
                                projBP = GetBProcessProj(_orgService, projTplName, projTpl);
                            }
                            else
                            { projBP.Id = null; }
                        }

                        int activeIdx = 2000000000, activeIdxExcel = -2000000000;
                        string traversedpath = string.Empty, stageid = string.Empty, processid = string.Empty;
                        foreach (Entity ms in milestoneCollection.Entities)
                        {
                            if (Convert.ToInt32(newEntiy["ddsm_pendingmilestoneindex"]) == Convert.ToInt32(ms["ddsm_index"]))
                            {
                                activeIdx = Convert.ToInt32(ms["ddsm_index"]);
                                break;
                            }
                        }
                        foreach (Entity ms in milestoneCollection.Entities)
                        {
                            milestoneActualEndKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == ("Milestone " + ms["ddsm_index"].ToString()).ToLower()).Key;
                            if (milestoneActualEndKey != null && dr.Attributes.ContainsKey("ddsm_" + milestoneActualEndKey.ToLower()) && !string.IsNullOrEmpty(dr.GetAttributeValue<string>("ddsm_" + milestoneActualEndKey.ToLower())))
                            {
                                activeIdxExcel = Convert.ToInt32(ms["ddsm_index"]);
                            }
                        }
                        activeIdxExcel += 1;
                        if (activeIdxExcel > activeIdx)
                        {
                            activeIdx = activeIdxExcel;
                        }
                        string tmpPhaseName = null;
                        int ForecastCompleteLastRecalcIndex = -1;
                        if (newEntiy.Attributes.Contains("ddsm_forecastcompletelastrecalcindex"))
                            ForecastCompleteLastRecalcIndex = newEntiy.GetAttributeValue<int>("ddsm_forecastcompletelastrecalcindex");

                        DateTime msPrevEndtDay = DataUploader.ConverAttributToDateTimeUtc("today", _thisSettings.TimeZoneInfo), ForecastComplete = DataUploader.ConverAttributToDateTimeUtc("today", _thisSettings.TimeZoneInfo);

                        if (milestoneCollection.Entities.Count > 0)
                        {
                            newEntiy["ddsm_projectstatus"] = milestoneCollection.Entities[0].GetAttributeValue<string>("ddsm_projectstatus").ToString();
                            newEntiy["ddsm_phase"] = tmpPhaseName = milestoneCollection.Entities[0].GetAttributeValue<string>("ddsm_projectphasename").ToString();
                        }
                        foreach (Entity ms in milestoneCollection.Entities)
                        {
                            if (ms.Contains("ddsm_duration") && ms["ddsm_duration"] != null)
                            {
                            }
                            else ms["ddsm_duration"] = 0;

                            if (Convert.ToInt32(ms["ddsm_index"]) != 1 && Convert.ToInt32(ms["ddsm_index"]) <= activeIdx && tmpPhaseName.ToLower() != ms["ddsm_projectphasename"].ToString().ToLower())
                            {
                                newEntiy["ddsm_phasenumber"] = Convert.ToInt32(newEntiy["ddsm_phasenumber"].ToString()) + 1;
                                newEntiy["ddsm_phase"] = tmpPhaseName = ms["ddsm_projectphasename"].ToString();
                            }
                            if (Convert.ToInt32(ms["ddsm_index"]) == 1)
                                msPrevEndtDay = Convert.ToDateTime(ms["ddsm_actualstart"]);

                            if (Convert.ToInt32(ms["ddsm_index"]) < activeIdx)
                            {

                                ms["ddsm_actualstart"] = msPrevEndtDay;
                                ms["ddsm_targetstart"] = msPrevEndtDay;
                                msPrevEndtDay = msPrevEndtDay.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                                ms["ddsm_targetend"] = msPrevEndtDay;

                                ms["ddsm_status"] = new OptionSetValue(962080002);

                                ms["ddsm_actualend"] = ms["ddsm_actualstart"];
                                newEntiy["ddsm_completedmilestone"] = ms["ddsm_name"].ToString();
                                newEntiy["ddsm_completedstatus"] = ms["ddsm_projectstatus"].ToString();
                                newEntiy["ddsm_completephase"] = ms["ddsm_projectphasename"].ToString();

                                //newEntiy["ddsm_milestoneactualend"] = ms["ddsm_targetend"];
                                newEntiy["ddsm_responsible"] = ms["ddsm_responsible"];

                                newEntiy["ddsm_projectstatus"] = ms["ddsm_projectstatus"].ToString();
                                newEntiy["ddsm_phase"] = ms["ddsm_projectphasename"].ToString();

                                milestoneActualEndKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == ("Milestone " + ms["ddsm_index"].ToString()).ToLower()).Key;
                                if (milestoneActualEndKey != null && dr.Attributes.ContainsKey("ddsm_" + milestoneActualEndKey.ToLower()) && !string.IsNullOrEmpty(dr.GetAttributeValue<string>("ddsm_" + milestoneActualEndKey.ToLower())))
                                {
                                    ms["ddsm_actualend"] = DataUploader.ConverAttributToDateTimeUtc(dr["ddsm_" + milestoneActualEndKey.ToLower()], _thisSettings.TimeZoneInfo);
                                }
                                msPrevEndtDay = Convert.ToDateTime(ms["ddsm_actualend"]);

                                if (projBP.Id != null)
                                {
                                    if ((projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())) != null))
                                    {
                                        traversedpath = traversedpath + "," + projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())).Id;
                                    }
                                }

                                if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0 && ForecastCompleteLastRecalcIndex == Convert.ToInt32(ms["ddsm_index"]))
                                    ForecastComplete = ms.GetAttributeValue<DateTime>("ddsm_actualend");

                            }
                            else if (Convert.ToInt32(ms["ddsm_index"]) == activeIdx)
                            {
                                ms["ddsm_actualstart"] = msPrevEndtDay;
                                ms["ddsm_targetstart"] = msPrevEndtDay;
                                msPrevEndtDay = msPrevEndtDay.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                                ms["ddsm_targetend"] = msPrevEndtDay;

                                ms["ddsm_status"] = new OptionSetValue(962080001);
                                newEntiy["ddsm_pendingmilestone"] = ms["ddsm_name"].ToString();
                                newEntiy["ddsm_pendingmilestoneindex"] = Convert.ToInt32(ms["ddsm_index"]);
                                newEntiy["ddsm_pendingactualstart"] = ms["ddsm_actualstart"];
                                newEntiy["ddsm_pendingtargetend"] = ms["ddsm_targetend"];
                                newEntiy["ddsm_responsible"] = ms["ddsm_responsible"];

                                newEntiy["ddsm_projectstatus"] = ms["ddsm_projectstatus"].ToString();
                                newEntiy["ddsm_phase"] = ms["ddsm_projectphasename"].ToString();

                                if (projBP.Id != null)
                                {
                                    if ((projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())) != null))
                                    {
                                        stageid = projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())).Id;
                                    }

                                }
                                if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0 && ForecastCompleteLastRecalcIndex == Convert.ToInt32(ms["ddsm_index"]))
                                    ForecastComplete = ms.GetAttributeValue<DateTime>("ddsm_targetend");
                            }
                            else
                            {
                                ms["ddsm_actualstart"] = msPrevEndtDay;
                                ms["ddsm_targetstart"] = msPrevEndtDay;
                                msPrevEndtDay = msPrevEndtDay.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                                ms["ddsm_targetend"] = msPrevEndtDay;

                                ms["ddsm_status"] = new OptionSetValue(962080000);

                                if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0 && ForecastCompleteLastRecalcIndex == Convert.ToInt32(ms["ddsm_index"]))
                                    ForecastComplete = ms.GetAttributeValue<DateTime>("ddsm_targetend");
                            }

                            if (Convert.ToBoolean(newEntiy["ddsm_completeonlastmilestone"]) && Convert.ToInt32(ms["ddsm_index"]) == Convert.ToInt32(newEntiy["ddsm_lastmilestoneindex"]) && activeIdx == Convert.ToInt32(newEntiy["ddsm_lastmilestoneindex"]))
                            {
                                //ms["ddsm_status"] = new OptionSetValue(962080002);
                                //ms["ddsm_actualend"] = ms["ddsm_targetend"];

                                newEntiy["ddsm_completedmilestone"] = null;
                                //                            newEntiy["ddsm_milestoneactualend"] = null;
                                newEntiy["ddsm_responsible"] = null;

                                newEntiy["ddsm_pendingmilestone"] = null;
                                newEntiy["ddsm_pendingmilestoneindex"] = null;
                                newEntiy["ddsm_pendingactualstart"] = null;
                                newEntiy["ddsm_pendingtargetend"] = null;

                                newEntiy["ddsm_completedmilestone"] = null;
                                newEntiy["ddsm_completedstatus"] = null;
                                newEntiy["ddsm_completephase"] = null;

                                newEntiy["ddsm_projectstatus"] = ms["ddsm_projectstatus"].ToString();
                                newEntiy["ddsm_phase"] = ms["ddsm_projectphasename"].ToString();
                                newEntiy["ddsm_enddate"] = msPrevEndtDay;

                                break;
                            }
                        }
                        if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0)
                        {
                            newEntiy["ddsm_estimatedprojectcomplete"] = ForecastComplete;
                            newEntiy["ddsm_projectcompletiondateupdatedestimate"] = ForecastComplete;
                        }
                        else
                        {
                            newEntiy["ddsm_estimatedprojectcomplete"] = msPrevEndtDay;
                            newEntiy["ddsm_projectcompletiondateupdatedestimate"] = msPrevEndtDay;
                        }
                        newEntiy["ddsm_targetprojectcompletion"] = msPrevEndtDay;

                        if (!string.IsNullOrEmpty(stageid))
                        {
                            traversedpath = traversedpath + "," + stageid;
                        }
                        traversedpath = traversedpath.TrimStart(',');

                        //Add Project Owner to Milestone
                        foreach (Entity ms in milestoneCollection.Entities)
                        {
                            if (newEntiy.Attributes.ContainsKey("ownerid"))
                                ms["ownerid"] = newEntiy["ownerid"];
                        }

                        //Add Milestone Collection
                        if (milestoneCollection.Entities.Count > 0)
                        {

                            Relationship msRelationship = new Relationship("ddsm_ddsm_project_ddsm_milestone");
                            newEntiy.RelatedEntities.Add(msRelationship, milestoneCollection);
                        }

                        //Add Business Process
                        if (projBP.Id != null)
                        {
                            newEntiy["processid"] = new Guid(projBP.Id);
                            if (!string.IsNullOrEmpty(stageid))
                                newEntiy["stageid"] = new Guid(stageid);
                            if (!string.IsNullOrEmpty(traversedpath))
                                newEntiy["traversedpath"] = traversedpath;
                        }

                        //Add XLSX Data
                        foreach (KeyValuePair<String, Object> Attribute in newEntiyXLSX.Attributes)
                        {
                            newEntiy[Attribute.Key] = Attribute.Value;
                        }

                        //Parent Project Group Column
                        var projectGroupTableName = string.Empty;
                        projectGroupTableName = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_projectgroup").Key;
                        string projectGroupColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (projectGroupTableName + " GUID").ToLower()).Key;
                        if (!string.IsNullOrEmpty(projectGroupColumnKey) && !string.IsNullOrEmpty(dr["ddsm_" + projectGroupColumnKey.ToLower()].ToString()) && ProjectGroupEntities.ContainsKey(dr["ddsm_" + projectGroupColumnKey.ToLower()].ToString()))
                        {
                            newEntiy["ddsm_projectgroupid"] = new EntityReference("ddsm_projectgroup", ProjectGroupEntities[dr["ddsm_" + projectGroupColumnKey.ToLower()].ToString()].EntityGuid);
                        }

                        //Create & Add Financial Collection
                        EntityCollection financilCollection = CreateFinancials(_orgService, EntityUniqueID, keyEntity, projTplID, projTpl, newEntiy, AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid, SiteEntities[dr["ddsm_" + siteColumnKey.ToLower()].ToString()].EntityGuid, milestoneCollection, objects, _thisSettings);
                        //Add Project Owner to Financial
                        foreach (Entity fncl in financilCollection.Entities)
                        {
                            if (newEntiy.Attributes.ContainsKey("ownerid"))
                                fncl["ownerid"] = newEntiy["ownerid"];
                        }

                        if (financilCollection.Entities.Count > 0)
                        {
                            Relationship fnclRelationship = new Relationship("ddsm_ddsm_project_ddsm_financial");
                            newEntiy.RelatedEntities.Add(fnclRelationship, financilCollection);
                        }

                        /*
                        var tmp_key = string.Empty;

                        //Property Manager
                        tmp_key = (string.IsNullOrEmpty(propertyManagerColumnKey)) ? string.Empty : "ddsm_" + propertyManagerColumnKey.ToLower();
                        if (!string.IsNullOrEmpty(tmp_key) && dr.Attributes.ContainsKey(tmp_key) && !newEntiy.Attributes.ContainsKey("ddsm_propertymanager"))
                        {
                            QueryExpression propManagerQuery = new QueryExpression { EntityName = "contact", ColumnSet = new ColumnSet("fullname", "contactid") };
                            propManagerQuery.Criteria.AddCondition("ddsm_contactprimarykey", ConditionOperator.Equal, dr["ddsm_" + propertyManagerColumnKey.ToLower()].ToString().ToLower());
                            propManagerQuery.Criteria.AddCondition("parentcustomerid", ConditionOperator.Equal, newEntiy.GetAttributeValue<EntityReference>("ddsm_accountid").Id);
                            EntityCollection propManagerRetrieve = _orgService.RetrieveMultiple(propManagerQuery);
                            if (propManagerRetrieve != null && propManagerRetrieve.Entities.Count == 1)
                            {
                                newEntiy["ddsm_propertymanager"] = new EntityReference("contact", new Guid(propManagerRetrieve.Entities[0].Attributes["contactid"].ToString()));
                            }
                            else if (propManagerRetrieve == null)
                            {
                                propManagerQuery = new QueryExpression { EntityName = "contact", ColumnSet = new ColumnSet("fullname", "contactid") };
                                propManagerQuery.Criteria.AddCondition("fullname", ConditionOperator.Equal, dr["ddsm_" + propertyManagerColumnKey.ToLower()].ToString());
                                propManagerQuery.Criteria.AddCondition("parentcustomerid", ConditionOperator.Equal, newEntiy.GetAttributeValue<EntityReference>("ddsm_accountid").Id);
                                propManagerRetrieve = _orgService.RetrieveMultiple(propManagerQuery);
                                if (propManagerRetrieve != null && propManagerRetrieve.Entities.Count == 1)
                                {
                                    newEntiy["ddsm_propertymanager"] = new EntityReference("contact", new Guid(propManagerRetrieve.Entities[0].Attributes["contactid"].ToString()));
                                }
                            }
                        }

                        //Primary Contact
                        tmp_key = (string.IsNullOrEmpty(primaryContactColumnKey)) ? string.Empty : "ddsm_" + primaryContactColumnKey.ToLower();
                        if (!string.IsNullOrEmpty(tmp_key) && dr.Attributes.ContainsKey(tmp_key) && !newEntiy.Attributes.ContainsKey("ddsm_primarycontactid"))
                        {
                            QueryExpression propManagerQuery = new QueryExpression { EntityName = "contact", ColumnSet = new ColumnSet("fullname", "contactid") };
                            propManagerQuery.Criteria.AddCondition("ddsm_contactprimarykey", ConditionOperator.Equal, dr["ddsm_" + primaryContactColumnKey.ToLower()].ToString().ToLower());
                            propManagerQuery.Criteria.AddCondition("parentcustomerid", ConditionOperator.Equal, newEntiy.GetAttributeValue<EntityReference>("ddsm_accountid").Id);

                            EntityCollection propManagerRetrieve = _orgService.RetrieveMultiple(propManagerQuery);
                            if (propManagerRetrieve != null && propManagerRetrieve.Entities.Count == 1)
                            {
                                newEntiy["ddsm_primarycontactid"] = new EntityReference("contact", new Guid(propManagerRetrieve.Entities[0].Attributes["contactid"].ToString()));
                            }
                            else if (propManagerRetrieve == null)
                            {
                                propManagerQuery = new QueryExpression { EntityName = "contact", ColumnSet = new ColumnSet("fullname", "contactid") };
                                propManagerQuery.Criteria.AddCondition("fullname", ConditionOperator.Equal, dr["ddsm_" + primaryContactColumnKey.ToLower()].ToString());
                                propManagerQuery.Criteria.AddCondition("parentcustomerid", ConditionOperator.Equal, newEntiy.GetAttributeValue<EntityReference>("ddsm_accountid").Id);
                                propManagerRetrieve = _orgService.RetrieveMultiple(propManagerQuery);
                                if (propManagerRetrieve != null && propManagerRetrieve.Entities.Count == 1)
                                {
                                    newEntiy["ddsm_primarycontactid"] = new EntityReference("contact", new Guid(propManagerRetrieve.Entities[0].Attributes["contactid"].ToString()));
                                }
                            }
                        }

                        //Partner Contact
                        tmp_key = (string.IsNullOrEmpty(partnerContactColumnKey)) ? string.Empty : "ddsm_" + partnerContactColumnKey.ToLower();
                        if (!string.IsNullOrEmpty(tmp_key) && dr.Attributes.ContainsKey(tmp_key) && !newEntiy.Attributes.ContainsKey("ddsm_partnercontact") && newEntiy.Attributes.ContainsKey("ddsm_partner"))
                        {
                            QueryExpression propManagerQuery = new QueryExpression { EntityName = "contact", ColumnSet = new ColumnSet("fullname", "contactid") };
                            propManagerQuery.Criteria.AddCondition("ddsm_contactprimarykey", ConditionOperator.Equal, dr["ddsm_" + partnerContactColumnKey.ToLower()].ToString().ToLower());
                            propManagerQuery.Criteria.AddCondition("parentcustomerid", ConditionOperator.Equal, newEntiy.GetAttributeValue<EntityReference>("ddsm_partner").Id);

                            EntityCollection propManagerRetrieve = _orgService.RetrieveMultiple(propManagerQuery);
                            if (propManagerRetrieve != null && propManagerRetrieve.Entities.Count == 1)
                            {
                                newEntiy["ddsm_partnercontact"] = new EntityReference("contact", new Guid(propManagerRetrieve.Entities[0].Attributes["contactid"].ToString()));
                            }
                            else if (propManagerRetrieve == null)
                            {
                                propManagerQuery = new QueryExpression { EntityName = "contact", ColumnSet = new ColumnSet("fullname", "contactid") };
                                propManagerQuery.Criteria.AddCondition("fullname", ConditionOperator.Equal, dr["ddsm_" + partnerContactColumnKey.ToLower()].ToString());
                                propManagerQuery.Criteria.AddCondition("parentcustomerid", ConditionOperator.Equal, newEntiy.GetAttributeValue<EntityReference>("ddsm_accountid").Id);
                                propManagerRetrieve = _orgService.RetrieveMultiple(propManagerQuery);
                                if (propManagerRetrieve != null && propManagerRetrieve.Entities.Count == 1)
                                {
                                    newEntiy["ddsm_partnercontact"] = new EntityReference("contact", new Guid(propManagerRetrieve.Entities[0].Attributes["contactid"].ToString()));
                                }
                            }
                        }
                        */

                        newEntiy["ddsm_calculationrecordstatus"] = new OptionSetValue(962080000);
                        newEntiy["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);

                        string duplicateRecordId = string.Empty;
                        if (_thisSettings.DuplicateDetect)
                        {
                            //duplicateRecordId = VerifyDuplicateRecord(_orgService, newEntiy, objects[keyEntity].Name.ToString().ToLower());
                        }

                        CreatedEntity projectEntity = new CreatedEntity();
                        Boolean Successful = false;
                        if (!string.IsNullOrEmpty(duplicateRecordId))
                        {

                            string[] reqDuplicate = Regex.Split(duplicateRecordId, "::");
                            projectEntity.EntityGuid = new Guid(reqDuplicate[0]);
                            projectEntity.Name = reqDuplicate[1];
                            projectEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            projectEntity.Status = false;
                            projectEntity.StatusDB = false;
                            ProjectEntities.AddOrUpdate(EntityUniqueID, projectEntity, (oldkey, oldvalue) => projectEntity);

                            switch (_thisSettings.DedupRules)
                            {
                                case 962080000: //Update
                                    if (newEntiy.Attributes.Count > 0)
                                    {

                                        Entity updateEntity = new Entity("ddsm_project", projectEntity.EntityGuid);
                                        foreach (KeyValuePair<String, Object> Attribute in newEntiy.Attributes)
                                        {
                                            updateEntity[Attribute.Key] = Attribute.Value;
                                        }

                                        UpdateRequest updateRequest = new UpdateRequest();
                                        updateRequest.Target = updateEntity;
                                        emRequest.Requests.Add(updateRequest);
                                        setUniqueID.Add(EntityUniqueID);
                                        itemsRequestCount++;
                                        //statisticRecords.Updated++;

                                    }
                                    else
                                    {
                                        ProjectEntities[EntityUniqueID].Status = true;
                                        SkippedEntities.AddOrUpdate(EntityUniqueID, ProjectEntities[EntityUniqueID], (oldkey, oldvalue) => ProjectEntities[EntityUniqueID]);
                                        statisticRecords.DoNothing++;

                                        itemsRequestCount++;
                                    }
                                    break;
                                case 962080001:
                                    ProjectEntities[EntityUniqueID].Status = true;
                                    SkippedEntities.AddOrUpdate(EntityUniqueID, ProjectEntities[EntityUniqueID], (oldkey, oldvalue) => ProjectEntities[EntityUniqueID]);
                                    statisticRecords.DoNothing++;

                                    itemsRequestCount++;
                                    break;
                            }

                        }
                        else
                        {

                            CreateRequest createRequest = new CreateRequest();
                            createRequest.Target = newEntiy;
                            emRequest.Requests.Add(createRequest);
                            setUniqueID.Add(EntityUniqueID);
                            itemsRequestCount++;

                            projectEntity.EntityGuid = new Guid();
                            projectEntity.Name = newEntiy["ddsm_name"].ToString();
                            projectEntity.addInfo = (newEntiy.Attributes.ContainsKey("ddsm_projectnumber")) ? newEntiy["ddsm_projectnumber"].ToString() : "";
                            projectEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            projectEntity.Status = false;
                            projectEntity.StatusDB = false;
                            ProjectEntities.AddOrUpdate(EntityUniqueID, projectEntity, (oldkey, oldvalue) => projectEntity);

                        }
                    }
                    catch (Exception e)
                    {
                        itemsRequestCount++;

                        Logger.Add("ERROR >>> UniqueGUID: " + EntityUniqueID + "; " + e.Message);
#if RELESEPLUGIN
#else
                        wsMessageService.SendMessage(MessageType.ERROR, "Project UniqueGUID: " + EntityUniqueID + "; Message: " + e.Message, MessageStatus.FINISH);
#endif

                    }
                }
                //Create Multiple
                if (itemsRequestCount == (requestSets * requestsCount))
                {
                    if (emRequest.Requests.Count > 0)
                    {
                        ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                        CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, ProjectEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                        //Update Source records
                        UpdateSourceRecords(_orgService, objects, RequestEntities, "ddsm_project", _thisSettings.CallExecuteMultiple);
                    }

                    //Update Source records (Skipped)
                    if (SkippedEntities.Count > 0)
                    {
                        UpdateSourceRecords(_orgService, objects, SkippedEntities, "ddsm_project", _thisSettings.CallExecuteMultiple);
                    }
                    SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();

                    requestSets++;
                    setUniqueID = new List<string>();
                    emRequest = new ExecuteMultipleRequest
                    {
                        Requests = new OrganizationRequestCollection(),
                        Settings = new ExecuteMultipleSettings
                        {
                            ContinueOnError = true,
                            ReturnResponses = true
                        }
                    };

                }
            }
            if (setUniqueID.Count > 0 && emRequest.Requests.Count > 0)
            {
                ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, ProjectEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                //Update Source records
                UpdateSourceRecords(_orgService, objects, RequestEntities, "ddsm_project", _thisSettings.CallExecuteMultiple);
            }

            if (SkippedEntities.Count > 0)
            {
                //Update Source records (Skipped)
                UpdateSourceRecords(_orgService, objects, SkippedEntities, "ddsm_project", _thisSettings.CallExecuteMultiple);
                SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
            }

        }
        //Create Financials Collection
        private EntityCollection CreateFinancials(IOrganizationService _orgService, string ProjEntityUniqueID, string ProjTableName, string projTplID, string projTplExcel, Entity parentProj, Guid account, Guid site, EntityCollection milestoneCollection, ConcurrentDictionary<string, EntityJson> objects, DataUploaderSettings _thisSettings)
        {
            EntityCollection fnclCollection = new EntityCollection();

            Entity newEntiy = new Entity("ddsm_financial");
            //string uniqueIdColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " GUID").ToLower()).Key;
            string projColumnKey = null;
            DateTime projStartDate = DataUploader.ConverAttributToDateTimeUtc("today", _thisSettings.TimeZoneInfo);

            if (!string.IsNullOrEmpty(projTplID))
            {
                DataCollection<Entity> RelatedEntitis;
                if (CasheRelationDataAttributes.ContainsKey(projTplExcel) && CasheRelationDataAttributes[projTplExcel].Data_Collection_2 != null)
                {
                    RelatedEntitis = CasheRelationDataAttributes[projTplExcel].Data_Collection_2;
                }
                else
                {
                    Relationship relationship = new Relationship();
                    relationship.SchemaName = "ddsm_ddsm_projecttemplate_ddsm_financialtemplate";

                    QueryExpression query = new QueryExpression();
                    query.EntityName = "ddsm_financialtemplate";
                    query.ColumnSet = new ColumnSet("ddsm_financialtemplateid");
                    query.Criteria = new FilterExpression();
                    query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

                    RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
                    relatedEntity.Add(relationship, query);

                    RetrieveRequest request = new RetrieveRequest();
                    request.ColumnSet = new ColumnSet("ddsm_projecttemplateid", "ddsm_name");
                    request.Target = new EntityReference { Id = new Guid(projTplID), LogicalName = "ddsm_projecttemplate" };
                    request.RelatedEntitiesQuery = relatedEntity;

                    RetrieveResponse response = (RetrieveResponse)_orgService.Execute(request);

                     RelatedEntitis = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship("ddsm_ddsm_projecttemplate_ddsm_financialtemplate")].Entities;

                    CasheRelationDataAttributes[projTplExcel].Data_Collection_2 = RelatedEntitis;
                }

                foreach (var RelatedEntity in RelatedEntitis)
                {
                    Entity rmEntity = GetDataRelationMapping(_orgService, "ddsm_financial", "ddsm_financialtemplate", new Guid(RelatedEntity["ddsm_financialtemplateid"].ToString()));
                    if (rmEntity != null)
                    {
                        foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                        {
                            newEntiy[Attribute.Key] = Attribute.Value;
                        }
                        if (parentProj.Attributes.ContainsKey("ddsm_programoffering"))
                            newEntiy["ddsm_programofferingid"] = new EntityReference(parentProj.GetAttributeValue<EntityReference>("ddsm_programoffering").LogicalName, parentProj.GetAttributeValue<EntityReference>("ddsm_programoffering").Id);

                        if (parentProj.Attributes.ContainsKey("ddsm_projectnumber") && !string.IsNullOrEmpty(parentProj.GetAttributeValue<string>("ddsm_projectnumber")))
                            newEntiy["ddsm_name"] = newEntiy["ddsm_name"] + "-" + parentProj.GetAttributeValue<string>("ddsm_projectnumber");

                        int initMsIdx = (newEntiy.Attributes.ContainsKey("ddsm_initiatingmilestoneindex")) ? newEntiy.GetAttributeValue<int>("ddsm_initiatingmilestoneindex") : 1;
                        projStartDate = milestoneCollection[initMsIdx - 1].GetAttributeValue<DateTime>("ddsm_targetstart");
                        if (newEntiy.Attributes.ContainsKey("ddsm_stagename1"))
                        {
                            newEntiy["ddsm_duration1"] = (newEntiy.Attributes.ContainsKey("ddsm_duration1")) ? newEntiy.GetAttributeValue<int>("ddsm_duration1") : 0;
                            newEntiy["ddsm_targetstart1"] = projStartDate;
                            projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration1"));
                            newEntiy["ddsm_targetend1"] = projStartDate;
                        }
                        if (newEntiy.Attributes.ContainsKey("ddsm_stagename2"))
                        {
                            newEntiy["ddsm_duration2"] = (newEntiy.Attributes.ContainsKey("ddsm_duration2")) ? newEntiy.GetAttributeValue<int>("ddsm_duration2") : 0;
                            newEntiy["ddsm_targetstart2"] = projStartDate;
                            projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration2"));
                            newEntiy["ddsm_targetend2"] = projStartDate;
                        }
                        if (newEntiy.Attributes.ContainsKey("ddsm_stagename3"))
                        {
                            newEntiy["ddsm_duration3"] = (newEntiy.Attributes.ContainsKey("ddsm_duration3")) ? newEntiy.GetAttributeValue<int>("ddsm_duration3") : 0;
                            newEntiy["ddsm_targetstart3"] = projStartDate;
                            projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration3"));
                            newEntiy["ddsm_targetend3"] = projStartDate;
                        }
                        if (newEntiy.Attributes.ContainsKey("ddsm_stagename4"))
                        {
                            newEntiy["ddsm_duration4"] = (newEntiy.Attributes.ContainsKey("ddsm_duration4")) ? newEntiy.GetAttributeValue<int>("ddsm_duration4") : 0;
                            newEntiy["ddsm_targetstart4"] = projStartDate;
                            projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration4"));
                            newEntiy["ddsm_targetend4"] = projStartDate;
                        }
                        if (newEntiy.Attributes.ContainsKey("ddsm_stagename5"))
                        {
                            newEntiy["ddsm_duration5"] = (newEntiy.Attributes.ContainsKey("ddsm_duration5")) ? newEntiy.GetAttributeValue<int>("ddsm_duration5") : 0;
                            newEntiy["ddsm_targetstart5"] = projStartDate;
                            projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration5"));
                            newEntiy["ddsm_targetend5"] = projStartDate;
                        }

                        //Set Currency
                        if (!newEntiy.Attributes.ContainsKey("transactioncurrencyid") && _thisSettings.CurrencyGuid != Guid.Empty)
                        {
                                newEntiy["transactioncurrencyid"] = new EntityReference("transactioncurrency", _thisSettings.CurrencyGuid);
                        }

                        newEntiy["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);

                        //Get relate fields Account
                        /*
                        Entity rmEntityAcc;
                        if (CasheRelationDataAttributes.ContainsKey(projTplExcel) && CasheRelationDataAttributes[projTplExcel].Data_Attributes_2 != null)
                        {
                            rmEntityAcc = CasheRelationDataAttributes[projTplExcel].Data_Attributes_2;
                        }
                        else {
                            rmEntityAcc = GetDataRelationMapping(_orgService, "ddsm_financial", "account", account);
                            CasheRelationDataAttributes[projTplExcel].Data_Attributes_2 = rmEntityAcc;
                        }
                        foreach (KeyValuePair<String, Object> Attribute in rmEntityAcc.Attributes)
                        {
                            newEntiy[Attribute.Key] = Attribute.Value;
                        }
                        */
                        newEntiy["ddsm_accountid"] = new EntityReference("account", account);

                        if (!newEntiy.Attributes.Contains("ddsm_status"))
                            newEntiy["ddsm_status"] = new OptionSetValue(962080000);

                        //AutoNumbering
                        if (newEntiy.Attributes.ContainsKey("ddsm_financialnumber") && !string.IsNullOrEmpty(newEntiy["ddsm_financialnumber"].ToString())) { }
                        else
                            newEntiy["ddsm_financialnumber"] = GetAutoNumber("ddsm_financial", _orgService);

                        fnclCollection.Entities.Add(newEntiy);
                        fnclCollection.EntityName = newEntiy.LogicalName;
                    }
                    //break;
                }
            }


            return fnclCollection;

        }
        //Create Milestones Collection
        private EntityCollection CreateMilestones(IOrganizationService _orgService, string projTplID, string projTplExcel, DateTime projStartDate, Entity parentProj, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            EntityCollection msCollection = new EntityCollection();
            string startPhaseName = string.Empty, startStatusName = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(projTplID))
                {
                    DataCollection<Entity> RelatedEntitis;
                    if (CasheRelationDataAttributes.ContainsKey(projTplExcel) && CasheRelationDataAttributes[projTplExcel].Data_Collection_1 != null)
                    {
                        RelatedEntitis = CasheRelationDataAttributes[projTplExcel].Data_Collection_1;
                    }
                    else
                    {
                        Relationship relationship = new Relationship();
                        relationship.SchemaName = "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate";

                        QueryExpression query = new QueryExpression();
                        query.EntityName = "ddsm_milestonetemplate";
                        query.ColumnSet = new ColumnSet("ddsm_milestonetemplateid", "ddsm_index");
                        query.Criteria = new FilterExpression();
                        query.AddOrder("ddsm_index", OrderType.Ascending);
                        query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

                        RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
                        relatedEntity.Add(relationship, query);

                        RetrieveRequest request = new RetrieveRequest();
                        request.ColumnSet = new ColumnSet("ddsm_projecttemplateid", "ddsm_name");
                        request.Target = new EntityReference { Id = new Guid(projTplID), LogicalName = "ddsm_projecttemplate" };
                        request.RelatedEntitiesQuery = relatedEntity;

                        RetrieveResponse response = (RetrieveResponse)_orgService.Execute(request);

                        RelatedEntitis = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship("ddsm_ddsm_projecttemplate_ddsm_milestonetemplate")].Entities;

                        CasheRelationDataAttributes[projTplExcel].Data_Collection_1 = RelatedEntitis;
                    }

                    foreach (var RelatedEntity in RelatedEntitis)
                    {
                        Entity ms = new Entity("ddsm_milestone");
                        Entity rmEntity = GetDataRelationMapping(_orgService, "ddsm_milestone", "ddsm_milestonetemplate", new Guid(RelatedEntity["ddsm_milestonetemplateid"].ToString()));
                        if (rmEntity != null)
                        {
                            foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                            {
                                ms[Attribute.Key] = Attribute.Value;

                            }
                            if (ms.Attributes.Contains("ddsm_duration") && ms["ddsm_duration"] != null) { } else ms["ddsm_duration"] = 0;
                            if (Convert.ToInt32(ms["ddsm_index"]) == 1)
                            {
                                ms["ddsm_actualstart"] = projStartDate;
                                ms["ddsm_status"] = new OptionSetValue(962080001);
                            }
                            else
                            {
                                ms["ddsm_status"] = new OptionSetValue(962080000);
                            }
                            ms["ddsm_targetstart"] = projStartDate;

                            projStartDate = projStartDate.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                            ms["ddsm_targetend"] = projStartDate;

                            //Set Currency
                            if (!ms.Attributes.ContainsKey("transactioncurrencyid") && _thisSettings.CurrencyGuid != Guid.Empty)
                            {
                                    ms["transactioncurrencyid"] = new EntityReference("transactioncurrency", _thisSettings.CurrencyGuid);
                            }

                            ms["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);

                            msCollection.Entities.Add(ms);
                            msCollection.EntityName = ms.LogicalName;

                        }

                    }
                }
            }
            catch (Exception e)
            {
                Logger.Add(e.Message);
            }
            return msCollection;
        }
        //Get Business Process
        private ProjectBusinessProcess GetBProcessProj(IOrganizationService _orgService, string projTplName, string projTplExcel)
        {
            ProjectBusinessProcess projBP = new ProjectBusinessProcess();
            projBP.Id = null;
            //Get Project Template ID
            if (CasheRelationDataAttributes.ContainsKey(projTplExcel) && CasheRelationDataAttributes[projTplExcel].BusinessProcess != null)
            {
                projBP = CasheRelationDataAttributes[projTplExcel].BusinessProcess;
            }
            else {
                try
                {
                    QueryExpression processQuery = new QueryExpression("workflow");
                    processQuery.ColumnSet = new ColumnSet("name", "workflowid");
                    processQuery.ColumnSet = new ColumnSet(true);
                    processQuery.Criteria.AddCondition("name", ConditionOperator.Equal, projTplName);
                    processQuery.Criteria.AddCondition("primaryentity", ConditionOperator.Equal, "ddsm_project");
                    EntityCollection processRetrieve = _orgService.RetrieveMultiple(processQuery);
                    if (processRetrieve != null && processRetrieve.Entities.Count == 1)
                    {
                        projBP.Id = processRetrieve.Entities[0].Attributes["workflowid"].ToString();
                        projBP.Name = processRetrieve.Entities[0].Attributes["name"].ToString();
                        projBP.Stages = new List<ProjectBusinessProcessStage>();
                        QueryExpression stageQuery = new QueryExpression("processstage");
                        stageQuery.ColumnSet = new ColumnSet("stagename", "processstageid");
                        stageQuery.Criteria.AddCondition("processid", ConditionOperator.Equal, new Guid(projBP.Id));
                        EntityCollection stageRetrieve = _orgService.RetrieveMultiple(stageQuery);
                        if (stageRetrieve != null && stageRetrieve.Entities.Count > 0)
                        {
                            foreach (var stage in stageRetrieve.Entities)
                            {
                                ProjectBusinessProcessStage stageObj = new ProjectBusinessProcessStage();
                                stageObj.Id = stage.Attributes["processstageid"].ToString();
                                stageObj.Name = stage.Attributes["stagename"].ToString();
                                projBP.Stages.Add(stageObj);
                            }
                        }
                        CasheRelationDataAttributes[projTplExcel].BusinessProcess = projBP;
                    }
                }
                catch (Exception e)
                {
                    projBP = new ProjectBusinessProcess();
                    projBP.Id = null;
                }

            }
            return projBP;
        }


    }
}
