﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static DataUploader.Model.Enums;
using static DataUploader.Model.Errors;
using static DataUploader.Model.PrimaryKey;
using static DataUploader.Model.AutoNumberingManager;

#if RELESEPLUGIN
#else
using SocketClients;
using DataUploader.Service;
#endif

namespace DataUploader.Creator
{
    public class CreateSites : BaseMethod
    {
        public void CreateSite(IOrganizationService _orgService, ConcurrentDictionary<string, CreatedEntity> AccountEntities, ConcurrentDictionary<string, CreatedEntity> SiteEntities, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            Logger2 = new List<string>();
            configObject = Newtonsoft.Json.JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);

            var keyEntity = configObject.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_site").Key;
            if (string.IsNullOrEmpty(keyEntity))
            {
                return;
            }

#if RELESEPLUGIN
#else
            wsMessageService = new WSMessageUploaderService(_thisSettings);
            wsMessageService.SendMessage(MessageType.INFORMATION, GetTextError(380032), MessageStatus.START);
#endif

            statisticRecords = new StatisticRecords();
            statisticsRecords.AddOrUpdate("stat", statisticRecords, (oldkey, oldvalue) => statisticRecords);

            entityCollectionPagination = new PaginationRetriveMultiple();
            DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.sitesProcessing, _thisSettings.TargetEntity.Id);
            do
            {
                entityCollectionPagination = GetDataByEntityName(_orgService, "ddsm_site", configObject, _thisSettings.TargetEntity.Id, Logger, false, entityCollectionPagination.pageNumber, entityCollectionPagination.PagingCookie, entityCollectionPagination.MoreRecords, _thisSettings.GlobalPageRecordsCount);
                if (entityCollectionPagination != null && entityCollectionPagination.RetrieveCollection.Entities.Count > 0)
                {
                    statisticsRecords["stat"].All += entityCollectionPagination.RetrieveCollection.Entities.Count;
                    CreateRecordSet(_orgService, configObject, AccountEntities, SiteEntities, entityCollectionPagination.RetrieveCollection, statisticsRecords["stat"], Logger2, _thisSettings);
                }
            }
            while (entityCollectionPagination != null && entityCollectionPagination.MoreRecords);

            string uniqueParenIdColumnKey = configObject[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == ("Parent " + keyEntity + " GUID").ToLower()).Key;

            if (statisticsRecords["stat"].All > 0 && !string.IsNullOrEmpty(uniqueParenIdColumnKey))
            {
                Logger2.Add(GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";");
#if RELESEPLUGIN
#else
                wsMessageService.SendMessage(MessageType.INFORMATION, "Update Site: update Parent Site", MessageStatus.START);
#endif
                //DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger2, "ddsm_log2");

                //Update Parent Site Value
                //Logger2 = new List<string>();
                Logger2.Add(" ");
                Logger2.Add("Update Sites: update Parent Site");

                updateSiteToParentSite(_orgService, configObject, SiteEntities, Logger2, _thisSettings);

                #if RELESEPLUGIN
#else
                wsMessageService.SendMessage(MessageType.SUCCESS, "Site: " + GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";", MessageStatus.FINISH);
#endif

                DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger2, "ddsm_log2");
            }
            else {
                Logger2.Add(GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";");
#if RELESEPLUGIN
#else
                wsMessageService.SendMessage(MessageType.SUCCESS, "Site: " + GetTextError(380019) + statisticsRecords["stat"].All + "; " + GetTextError(380020) + statisticsRecords["stat"].Created + "; " + GetTextError(380021) + statisticsRecords["stat"].Updated + "; " + GetTextError(380022) + statisticsRecords["stat"].DoNothing + ";", MessageStatus.FINISH);
#endif
                DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger2, "ddsm_log2");
            }

        }

        private void CreateRecordSet(IOrganizationService _orgService, ConcurrentDictionary<string, EntityJson> objects, ConcurrentDictionary<string, CreatedEntity> AccountEntities, ConcurrentDictionary<string, CreatedEntity> SiteEntities, EntityCollection table, StatisticRecords statisticRecords, List<string> Logger, DataUploaderSettings _thisSettings)
        {
            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "ddsm_site").Key;
            Int64 requestsCount = _thisSettings.GlobalRequestsCount, itemsRequestCount = 0, requestSets = 1;
            string EntityUniqueID = string.Empty;
            var accountTableName = string.Empty;
            accountTableName = objects.FirstOrDefault(x => x.Value.Name.ToLower() == "account").Key;

            if (string.IsNullOrEmpty(accountTableName)) return;

            int EntityDataRow = 0, insertDataRow = 0, updateDataRow = 0, doNothing = 0;
            string uniqueIdColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (keyEntity + " GUID").ToLower()).Key;
            string uniqueParenIdColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == ("Parent " + keyEntity + " GUID").ToLower()).Key;
            string accColumnKey = objects[keyEntity].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (accountTableName + " GUID").ToLower()).Key;

            if (string.IsNullOrEmpty(uniqueIdColumnKey)) return;
            if (string.IsNullOrEmpty(accColumnKey)) return;

            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            List<string> setUniqueID = new List<string>();
            foreach (Entity dr in table.Entities)
            {

                EntityDataRow++;
                if (EntityDataRow >= 1)
                {
                    try
                    {
                        string ParenUniqueID = string.Empty;
                        EntityUniqueID = string.Empty;

                        if (dr.Attributes.ContainsKey("ddsm_" + uniqueIdColumnKey.ToLower()))
                            EntityUniqueID = dr["ddsm_" + uniqueIdColumnKey.ToLower()].ToString();

                        //Verify duplicate record GUID
                        if (SiteEntities.ContainsKey(EntityUniqueID))
                        {
                            Logger.Add("ERROR >>> GUID: " + EntityUniqueID + " Not unique in sheet Sites list");
                            continue;
                        }

                        if (!string.IsNullOrEmpty(uniqueParenIdColumnKey) && dr.Attributes.ContainsKey("ddsm_" + uniqueParenIdColumnKey.ToLower()))
                            ParenUniqueID = dr["ddsm_" + uniqueParenIdColumnKey.ToLower()].ToString();

                        if (string.IsNullOrEmpty(EntityUniqueID)) continue;
                        //Parent Account
                        if (string.IsNullOrEmpty(dr["ddsm_" + accColumnKey.ToLower()].ToString())) continue;
                        GetDictionaryObjectbyGUID(_orgService, dr["ddsm_" + accColumnKey.ToLower()].ToString(), "account", objects, AccountEntities, _thisSettings.TargetEntity.Id);
                        if (!AccountEntities.ContainsKey(dr["ddsm_" + accColumnKey.ToLower()].ToString())) continue;
                        if (string.IsNullOrEmpty(AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid.ToString())) continue;
                        if (AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid.ToString() == (new Guid()).ToString()) continue;

                        Entity newEntiy = new Entity("ddsm_site");
                        //Parent Account
                        Entity rmEntity;
                        if (CasheRelationDataAttributes.ContainsKey(AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid.ToString()))
                        {
                            rmEntity = CasheRelationDataAttributes[AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid.ToString()].Data_Attributes_1;
                        }
                        else {
                            rmEntity = GetDataRelationMapping(_orgService, "ddsm_site", "account", AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid);
                            RelationDataAttributes relationDataAttributes = new RelationDataAttributes();
                            relationDataAttributes.Data_Attributes_1 = rmEntity;
                            CasheRelationDataAttributes.AddOrUpdate(AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid.ToString(), relationDataAttributes, (oldkey, oldvalue) => relationDataAttributes);
                        }
                        if (rmEntity != null)
                        {
                            foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                            {
                                newEntiy[Attribute.Key] = Attribute.Value;
                            }
                        }

                        Entity newEntiyXLSX = GenerationEntity(_orgService, objects, keyEntity, dr, _thisSettings.TargetEntity.Id);
                        if (newEntiyXLSX.Attributes.Count == 0) continue;

                        foreach (KeyValuePair<String, Object> Attribute in newEntiyXLSX.Attributes)
                        {
                            newEntiy[Attribute.Key] = Attribute.Value;
                        }

                        newEntiy["ddsm_parentaccount"] = new EntityReference("account", AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].EntityGuid);

                        if (newEntiy.Attributes.Contains("ddsm_siteprimarykey") && !string.IsNullOrEmpty(newEntiy["ddsm_siteprimarykey"].ToString()))
                        {
                            newEntiy["ddsm_siteprimarykey"] = (newEntiy.GetAttributeValue<string>("ddsm_siteprimarykey")).ToLower();
                        }
                        else
                        {
                            var SitePrimaryKey = GetSitePrimaryKey(newEntiy.Attributes.Contains("ddsm_address1") ? newEntiy.GetAttributeValue<string>("ddsm_address1") : "", newEntiy.Attributes.Contains("ddsm_address2") ? newEntiy.GetAttributeValue<string>("ddsm_address2") : "", newEntiy.Attributes.Contains("ddsm_city") ? newEntiy.GetAttributeValue<string>("ddsm_city") : "", newEntiy.Attributes.Contains("ddsm_parentunitid") ? true : false);
                            SitePrimaryKey += AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].PrimaryKey;
                            newEntiy["ddsm_siteprimarykey"] = SitePrimaryKey;
                        }

                        //if (newEntiy.Attributes.Contains("ddsm_name") && !string.IsNullOrEmpty(newEntiy["ddsm_name"].ToString())) { }
                        //else
                        //{
                            if (!string.IsNullOrEmpty(AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].Name))
                            {
                                newEntiy["ddsm_name"] =
                                    CreateSiteName(
                                        newEntiy.Attributes.Contains("ddsm_address1")
                                            ? newEntiy.GetAttributeValue<string>("ddsm_address1")
                                            : "",
                                        newEntiy.Attributes.Contains("ddsm_address2")
                                            ? newEntiy.GetAttributeValue<string>("ddsm_address2")
                                            : "",
                                        newEntiy.Attributes.Contains("ddsm_city")
                                            ? newEntiy.GetAttributeValue<string>("ddsm_city")
                                            : "",
                                        AccountEntities[dr["ddsm_" + accColumnKey.ToLower()].ToString()].Name);
                            }
                            else
                            {
                                newEntiy["ddsm_name"] = CreateSiteName(
                                    newEntiy.Attributes.Contains("ddsm_address1") 
                                    ? newEntiy.GetAttributeValue<string>("ddsm_address1") 
                                    : "", 
                                    newEntiy.Attributes.Contains("ddsm_address2") 
                                    ? newEntiy.GetAttributeValue<string>("ddsm_address2") 
                                    : "", newEntiy.Attributes.Contains("ddsm_city") 
                                    ? newEntiy.GetAttributeValue<string>("ddsm_city") 
                                    : "", "");
                            }
                        //}

                        newEntiy["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);

                        string duplicateRecordId = string.Empty;
                        if (_thisSettings.DuplicateDetect)
                        {
                            duplicateRecordId = VerifyDuplicateRecord(_orgService, newEntiy, objects[keyEntity].Name.ToString().ToLower());
                        }

                        CreatedEntity siteEntity = new CreatedEntity();

                        Boolean Successful = false;
                        if (!string.IsNullOrEmpty(duplicateRecordId))
                        {
                            string[] reqDuplicate = Regex.Split(duplicateRecordId, "::");
                            siteEntity.EntityGuid = new Guid(reqDuplicate[0]);
                            siteEntity.Name = reqDuplicate[1];
                            siteEntity.UniqueParentGUID = ParenUniqueID;
                            siteEntity.PrimaryKey = newEntiy.GetAttributeValue<string>("ddsm_siteprimarykey");
                            siteEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            siteEntity.Status = false;
                            siteEntity.StatusDB = false;

                            SiteEntities.AddOrUpdate(EntityUniqueID, siteEntity, (oldkey, oldvalue) => siteEntity);

                            if (string.IsNullOrEmpty(SiteEntities[EntityUniqueID].addInfo))
                            {
                                Entity siteInfo = GetEntityAttrsValue(_orgService, siteEntity.EntityGuid, "ddsm_site", new ColumnSet("ddsm_address1"));
                                if (siteInfo.Attributes.Contains("ddsm_address1"))
                                    SiteEntities[EntityUniqueID].addInfo = siteInfo["ddsm_address1"].ToString();
                            }

                            switch (_thisSettings.DedupRules)
                            {
                                case 962080000: //Update
                                    if (newEntiy.Attributes.Count > 0)
                                    {
                                        Entity updateEntity = new Entity("ddsm_site", SiteEntities[EntityUniqueID].EntityGuid);


                                        ColumnSet colsSet = new ColumnSet();
                                        foreach (var Attribute in newEntiy.Attributes)
                                        {
                                            colsSet.AddColumn(Attribute.Key);
                                        }

                                        if (colsSet.Columns.Count > 0)
                                        {
                                            Entity siteAttributes = GetEntityAttrsValue(_orgService, siteEntity.EntityGuid, "ddsm_site", colsSet);

                                            if (siteAttributes.Attributes.Count > 0)
                                            {
                                                foreach (KeyValuePair<String, Object> Attribute in newEntiy.Attributes)
                                                {

                                                    if (siteAttributes.Attributes.Contains(Attribute.Key))
                                                    {
                                                        var _comparing = ComparingAttributeValues(newEntiy.Attributes[Attribute.Key], siteAttributes.Attributes[Attribute.Key]);

                                                        if (_comparing)
                                                        {
                                                            updateEntity[Attribute.Key] = newEntiy.Attributes[Attribute.Key];
                                                        }
                                                    }
                                                    else
                                                    {
                                                        updateEntity[Attribute.Key] = newEntiy.Attributes[Attribute.Key];
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                foreach (KeyValuePair<String, Object> Attribute in newEntiy.Attributes)
                                                {
                                                    updateEntity[Attribute.Key] = newEntiy.Attributes[Attribute.Key];
                                                }
                                            }
                                        }

                                        if (updateEntity.Attributes.Count > 0)
                                        {
                                            UpdateRequest updateRequest = new UpdateRequest();
                                            updateRequest.Target = updateEntity;
                                            emRequest.Requests.Add(updateRequest);
                                            setUniqueID.Add(EntityUniqueID);
                                            itemsRequestCount++;
                                            //statisticRecords.Updated++;
                                        }
                                        else
                                        {
                                            SiteEntities[EntityUniqueID].Status = true;
                                            SkippedEntities.AddOrUpdate(EntityUniqueID, SiteEntities[EntityUniqueID], (oldkey, oldvalue) => SiteEntities[EntityUniqueID]);
                                            statisticRecords.DoNothing++;

                                            itemsRequestCount++;
                                        }

                                    }
                                    else
                                    {
                                        SiteEntities[EntityUniqueID].Status = true;
                                        SkippedEntities.AddOrUpdate(EntityUniqueID, SiteEntities[EntityUniqueID], (oldkey, oldvalue) => SiteEntities[EntityUniqueID]);
                                        statisticRecords.DoNothing++;

                                        itemsRequestCount++;
                                    }

                                    break;
                                case 962080001:
                                    SiteEntities[EntityUniqueID].Status = true;
                                    SkippedEntities.AddOrUpdate(EntityUniqueID, SiteEntities[EntityUniqueID], (oldkey, oldvalue) => SiteEntities[EntityUniqueID]);
                                    statisticRecords.DoNothing++;

                                    itemsRequestCount++;
                                    break;
                            }
                        }
                        else
                        {
                            //AutoNumbering only on Create
                            if (newEntiy.Attributes.ContainsKey("ddsm_sitenumber") && !string.IsNullOrEmpty(newEntiy["ddsm_sitenumber"].ToString())) { }
                            else
                                newEntiy["ddsm_sitenumber"] = GetAutoNumber("ddsm_site", _orgService);

                            CreateRequest createRequest = new CreateRequest();
                            createRequest.Target = newEntiy;
                            emRequest.Requests.Add(createRequest);
                            setUniqueID.Add(EntityUniqueID);
                            itemsRequestCount++;

                            siteEntity.EntityGuid = new Guid();
                            siteEntity.PrimaryKey = newEntiy.GetAttributeValue<string>("ddsm_siteprimarykey");
                            if (newEntiy.Attributes.Contains("ddsm_name"))
                            {
                                siteEntity.Name = newEntiy["ddsm_name"].ToString();
                            }
                            siteEntity.UniqueParentGUID = ParenUniqueID;
                            siteEntity.SourceID = (Guid)dr["ddsm_exceldataid"];
                            siteEntity.Status = false;
                            siteEntity.StatusDB = false;
                            if (newEntiy.Attributes.Contains("ddsm_address1"))
                                siteEntity.addInfo = newEntiy["ddsm_address1"].ToString();
                            SiteEntities.AddOrUpdate(EntityUniqueID, siteEntity, (oldkey, oldvalue) => siteEntity);

                        }
                    }
                    catch (Exception e)
                    {
                        itemsRequestCount++;

                        Logger.Add("ERROR >>> UniqueGUID: " + EntityUniqueID + ";; " + e.Message);
#if RELESEPLUGIN
#else
                        wsMessageService.SendMessage(MessageType.ERROR, "Site UniqueGUID: " + EntityUniqueID + "; Message: " + e.Message, MessageStatus.FINISH);
#endif
                    }
                }

                //Create Multiple
                if (itemsRequestCount == (requestSets * requestsCount))
                {
                    if (emRequest.Requests.Count > 0)
                    {
                        ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                        CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, SiteEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                        //Update Source records
                        UpdateSourceRecords(_orgService, objects, RequestEntities, "ddsm_site", _thisSettings.CallExecuteMultiple);
                    }

                    //Update Source records (Skipped)
                    if (SkippedEntities.Count > 0)
                    {
                        UpdateSourceRecords(_orgService, objects, SkippedEntities, "ddsm_site", _thisSettings.CallExecuteMultiple);
                    }
                    SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();

                    requestSets++;

                    setUniqueID = new List<string>();
                    emRequest = new ExecuteMultipleRequest
                    {
                        Requests = new OrganizationRequestCollection(),
                        Settings = new ExecuteMultipleSettings
                        {
                            ContinueOnError = true,
                            ReturnResponses = true
                        }
                    };

                }
            }

            if (setUniqueID.Count > 0 && emRequest.Requests.Count > 0)
            {
                ConcurrentDictionary<string, CreatedEntity> RequestEntities = new ConcurrentDictionary<string, CreatedEntity>();

                CreateUpdateEntityRecords(_orgService, emRequest, setUniqueID, SiteEntities, RequestEntities, statisticRecords, Logger, _thisSettings.CallExecuteMultiple);

                //Update Source records
                UpdateSourceRecords(_orgService, objects, RequestEntities, "ddsm_site", _thisSettings.CallExecuteMultiple);
            }

            if (SkippedEntities.Count > 0)
            {
                //Update Source records (Skipped)
                UpdateSourceRecords(_orgService, objects, SkippedEntities, "ddsm_site", _thisSettings.CallExecuteMultiple);
                SkippedEntities = new ConcurrentDictionary<string, CreatedEntity>();
            }

        }

        private void updateSiteToParentSite(IOrganizationService _orgService, ConcurrentDictionary<string, EntityJson> objects, ConcurrentDictionary<string, CreatedEntity> SiteEntities, List<string> logger2, DataUploaderSettings _thisSettings)
        {
            int indexReq = -1;
            Int64 requestsCount = _thisSettings.GlobalRequestsCount, itemsRequestCount = 0, requestSets = 1;
            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            foreach (KeyValuePair<string, CreatedEntity> Site in SiteEntities)
            {

                if (!string.IsNullOrEmpty(Site.Value.UniqueParentGUID))
                    if (SiteEntities.ContainsKey(Site.Value.UniqueParentGUID))
                    {
                        if (SiteEntities[Site.Value.UniqueParentGUID].EntityGuid == Guid.Empty) continue;
                        if (Site.Key == Site.Value.UniqueParentGUID) continue;

                        Entity parentSite = new Entity("ddsm_site", Site.Value.EntityGuid);
                        parentSite["ddsm_parentunitid"] = new EntityReference("ddsm_site", SiteEntities[Site.Value.UniqueParentGUID].EntityGuid);
                        UpdateRequest updateRequest = new UpdateRequest();
                        updateRequest.Target = parentSite;
                        emRequest.Requests.Add(updateRequest);
                    }

                itemsRequestCount++;

                //Create Multiple

                if (itemsRequestCount == (requestSets * requestsCount))
                {

                    if (!_thisSettings.CallExecuteMultiple)
                    {
                        //Single
                        foreach (var req in emRequest.Requests)
                        {
                            indexReq++;
                            try
                            {

                                if (req.RequestName == "Update")
                                {
                                    _orgService.Update(((UpdateRequest)req).Target);
                                    statisticRecords.Updated++;
                                }
                            }
                            catch (Exception e)
                            {
                                logger2.Add("Error: Index: " + (indexReq + 1) + " Message: " + e.Message);
                            }
                        }
                    }
                    else {
                        ExecuteMultipleResponse emResponse = (ExecuteMultipleResponse)_orgService.Execute(emRequest);
                        //Logger.Add("Set Inserts");
                        foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                        {
                            // A valid response.
                            if (responseItem.Response != null)
                            {
                                if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                                {
                                }
                            }
                            // An error has occurred.
                            else if (responseItem.Fault != null)
                            {
                                logger2.Add("Error: Name: " + emRequest.Requests[indexReq].RequestName + " Index: " + (indexReq + 1) + " Message: " + responseItem.Fault.Message);
                            }
                        }
                    }

                    requestSets++;
                    emRequest = new ExecuteMultipleRequest
                    {
                        Requests = new OrganizationRequestCollection(),
                        Settings = new ExecuteMultipleSettings
                        {
                            ContinueOnError = true,
                            ReturnResponses = true
                        }
                    };

                }

            }
            if (emRequest.Requests.Count > 0)
            {
                if (!_thisSettings.CallExecuteMultiple)
                {
                    //Single
                    foreach (var req in emRequest.Requests)
                    {
                        indexReq++;
                        try
                        {

                            if (req.RequestName == "Update")
                            {
                                _orgService.Update(((UpdateRequest)req).Target);
                                statisticRecords.Updated++;
                            }
                        }
                        catch (Exception e)
                        {
                            logger2.Add("Error: Name: " + emRequest.Requests[indexReq].RequestName + " Index: " + (indexReq + 1) + " Message: " + e.Message);
                        }
                    }
                }
                else
                {
                    ExecuteMultipleResponse emResponse = (ExecuteMultipleResponse)_orgService.Execute(emRequest);
                    //Logger.Add("Set Inserts");
                    foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                    {
                        // A valid response.
                        if (responseItem.Response != null)
                        {
                            if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                            {
                            }
                        }
                        // An error has occurred.
                        else if (responseItem.Fault != null)
                        {
                            logger2.Add("Error: Name: " + emRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message);
                        }
                    }
                }

            }
        }

    }
}
