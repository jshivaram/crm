﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Crm.Sdk.Messages;

using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Query;
using System.ServiceModel;
using System.Globalization;
using System.Linq;

namespace DataUploader
{

    public class DataUploaderSettings
    {
        public bool CallExecuteMultiple { get; set; } = false;
        public int StartRowSheet { get; set; } = 1;
        public int RowNumberSheet { get; set; } = -1;
        public string WorkingSheet { get; set; } = string.Empty;
        public string ProcessedSheets { get; set; } = string.Empty;
        public int GlobalRequestsCount { get; set; } = 100;
        public int GlobalPageRecordsCount { get; set; } = 1000;
        public int GlobalErrorCount { get; set; } = 5;
        public int TypeConfig { get; set; } = -2147483648;
        public int TimeOutLimit { get; set; } = 60;
        public int TimeOffset { get; set; } = 30;
        public int CreatorRecordType { get; set; } = -2147483648;
        public int StatusFileDataUploading { get; set; } = -2147483648;
        public string JsonObjects { get; set; } = string.Empty;
        public string FileName { get; set; } = string.Empty;
        public int DedupRules { get; set; } = 962080002;
        public bool DuplicateDetect { get; set; } = true;
        public int EspRecalcData { get; set; } = 962080000;
        public bool FileUploaded { get; set; } = false;
        public bool FileParsed { get; set; } = false;

        public TimeZoneInfo TimeZoneInfo { get; set; } = TimeZoneInfo.Utc;
        public Guid CurrencyGuid { get; set; } = Guid.Empty;
        public Guid UserGuid { get; set; } = Guid.Empty;
        public EntityReference TargetEntity { get; set; } = null;

        public Int32 countErrors { get; set; } = 0;
    }

public class ParsedRecords
    {
        public int RequestCount { get; set; } = 0;
        public int ResponceCount { get; set; } = 0;
        public int ErrorCount { get; set; } = 0;
    }


    public class AttrJson
    {
        public string Header { get; set; }
        [JsonProperty("Attr")]
        public string Name { get; set; }
        public string AttrType { get; set; }
    }

    public class EntityJson
    {
        [JsonProperty("entity")]
        public string Name { get; set; }
        [JsonProperty("attrs")]
        public Dictionary<string, AttrJson> Attrs { get; set; }
    }

    public class DataUploader
    {

        public static string ParseAttributToDateTime(Object inputDate)
        {
            try
            {
                if (inputDate.GetType().Name == "DateTime")
                {
                    return (Convert.ToDateTime(inputDate)).ToShortDateString();
                }
                else if (inputDate.GetType().Name == "Double")
                {
                    return (DateTime.FromOADate(Convert.ToDouble(inputDate))).ToShortDateString();
                }
                else if (inputDate.GetType().Name == "String")
                {
                    if (inputDate.ToString() == "")
                    {
                        return null;
                    }
                    else if (inputDate.ToString() == "today")
                    {
                        return (DateTime.Now).ToShortDateString();
                    }
                    else
                    {
                        return (Convert.ToDateTime(inputDate.ToString())).ToShortDateString();
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //Conver to UTC Date Time from Exel Cell
        public static dynamic ConverAttributToDateTimeUtc(Object xlsxDate)
        {
            try
            {
                if (xlsxDate.GetType().Name == "DateTime")
                {
                    return xlsxDate;
                }
                else if (xlsxDate.GetType().Name == "Double")
                {
                    return DateTime.FromOADate(Convert.ToDouble(xlsxDate)).ToUniversalTime();
                }
                else if (xlsxDate.GetType().Name == "String")
                {
                    if (xlsxDate.ToString() == "")
                    {
                        return null;
                    }
                    else if (xlsxDate.ToString() == "today")
                    {
                        return DateTime.UtcNow;
                    }
                    else {
                        return Convert.ToDateTime(xlsxDate.ToString()).ToUniversalTime();
                    }
                }
                else {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static dynamic ConverAttributToDateTimeUtc(Object inputDate, TimeZoneInfo timeZoneInfo)
        {
            try
            {
                if (inputDate.GetType().Name == "DateTime")
                {
                    var convertDate = Convert.ToDateTime(inputDate);
                    return TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(convertDate.ToShortDateString()), timeZoneInfo);
                }
                else if (inputDate.GetType().Name == "Double")
                {
                    var convertDate = DateTime.FromOADate(Convert.ToDouble(inputDate));
                    return TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(convertDate.ToShortDateString()), timeZoneInfo);
                }
                else if (inputDate.GetType().Name == "String")
                {
                    if (inputDate.ToString() == "")
                    {
                        return null;
                    }
                    else if (inputDate.ToString() == "today")
                    {
                        var convertDate = DateTime.Now;
                        return TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(convertDate.ToShortDateString()), timeZoneInfo);
                    }
                    else
                    {
                        var convertDate = Convert.ToDateTime(inputDate.ToString());
                        return TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(convertDate.ToShortDateString()), timeZoneInfo);
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //Is Numeric Format
        public static Boolean IsNumeric(Object AttrValue)
        {
            try
            {
                if (AttrValue == null || AttrValue is DateTime)
                    throw new Exception("Value '" + AttrValue.ToString() + "' is not a number");

                if (AttrValue is Int16 || AttrValue is Int32 || AttrValue is Int64 || AttrValue is Decimal || AttrValue is Single || AttrValue is Double || AttrValue is Boolean)
                    return true;
                double number;
                if (Double.TryParse(Convert.ToString(AttrValue, CultureInfo.InvariantCulture), System.Globalization.NumberStyles.Any, NumberFormatInfo.InvariantInfo, out number))
                    return true;
                else
                    throw new Exception("Value '" + AttrValue.ToString() + "' is not a number");
            }
            catch (Exception e)
            {
                return false;
            }
        }

        //Save Log
        public static void SaveLog(IOrganizationService _orgService, Guid recordUploaded, List<string> Logger, string fieldName, bool isAddLog = true)
        {
            var tmpStr = string.Empty;
            if (isAddLog)
            {
                QueryExpression recordUploadedQuery = new QueryExpression { EntityName = "ddsm_datauploader", ColumnSet = new ColumnSet(fieldName) };
                recordUploadedQuery.Criteria.AddCondition("ddsm_datauploaderid", ConditionOperator.Equal, recordUploaded);
                EntityCollection recordUploadedRetrieve = _orgService.RetrieveMultiple(recordUploadedQuery);
                if (recordUploadedRetrieve[0].Attributes.ContainsKey(fieldName))
                    tmpStr = recordUploadedRetrieve[0].GetAttributeValue<string>(fieldName);
            }

            Entity dp = new Entity("ddsm_datauploader", recordUploaded);
            if(tmpStr != string.Empty)
                tmpStr = tmpStr + "\n" + string.Join("\n", Logger);
            else
                tmpStr = string.Join("\n", Logger);

            tmpStr = tmpStr.Substring((tmpStr.Length > 1000000) ? (tmpStr.Length - 1000000) : 0, (tmpStr.Length > 1000000) ? 1000000 : tmpStr.Length);
            dp[fieldName] = tmpStr;
            _orgService.Update(dp);
        }

        public static void SetStatusFileDataUploading(IOrganizationService _orgService, int status, Guid recordId)
        {
            Entity setStatus = new Entity("ddsm_datauploader", recordId);
            setStatus.Attributes.Add("ddsm_statusfiledatauploading", new OptionSetValue(status));
            _orgService.Update(setStatus);
        }

        public static OptionMetadata[] GetStatusFileDataUploading(IOrganizationService _orgService)
        {
            RetrieveAttributeRequest retrieveAttributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = "ddsm_datauploader",
                LogicalName = "ddsm_statusfiledatauploading",
                RetrieveAsIfPublished = true
            };
            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)_orgService.Execute(retrieveAttributeRequest);
            Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata)
            retrieveAttributeResponse.AttributeMetadata;
            return retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
        }

        public static string GetTextStatusFileDataUploading(int optionValue, OptionMetadata[] StatusFileDataUploading)
        {
            string selectedOptionText = string.Empty;
            foreach (OptionMetadata oMD in StatusFileDataUploading)
            {
                if (optionValue == oMD.Value.Value)
                {
                    selectedOptionText = oMD.Label.LocalizedLabels[0].Label.ToString();
                    break;
                }
            }
            return selectedOptionText;
        }
        /*
        public static Int32 getDateTimeDiff(DateTime startDate, DateTime endDate)
        {
            Int32 diff = 0;

            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diffStart = startDate.ToUniversalTime() - origin;
            TimeSpan diffEnd = endDate.ToUniversalTime() - origin;

            diff = Convert.ToInt32(Math.Floor(diffEnd.TotalSeconds) - Math.Floor(diffStart.TotalSeconds));
            if (diff < 0) diff = 0;
            return diff;
        }

        public static DateTime GetStartOnPluginTime(IOrganizationService SystemService, Guid RegardingGuid, string PluginName = "")
        {
            DateTime startOn = convertAttributToDateTimeUtc("today");


            QueryExpression queryAsync = new QueryExpression();
            queryAsync.EntityName = "asyncoperation";
            queryAsync.ColumnSet = new ColumnSet("startedon");
            queryAsync.AddOrder("createdon", OrderType.Descending);
            queryAsync.Criteria.AddCondition(new ConditionExpression("regardingobjectid", ConditionOperator.Equal, new object[] { RegardingGuid }));
            queryAsync.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, new object[] { 2 }));
            //queryAsync.Criteria.AddCondition(new ConditionExpression("statuscode", ConditionOperator.Equal, new object[] { 10 }));

            if (!string.IsNullOrEmpty(PluginName))
            {
                queryAsync.Criteria.AddCondition("name", ConditionOperator.BeginsWith, PluginName);
            }
            EntityCollection queryAsyncRetrieve = SystemService.RetrieveMultiple(queryAsync);


            if (queryAsyncRetrieve != null && queryAsyncRetrieve.Entities.Count > 0 && queryAsyncRetrieve[0].Attributes.ContainsKey("startedon"))
            {
                startOn = queryAsyncRetrieve[0].GetAttributeValue<DateTime>("startedon");
            }

            return startOn;
        }
        */

        /// <summary>
        /// Return OrgService from System User
        /// </summary>
        /// <returns></returns>
        private static Guid GetSystemUserId(string name, IOrganizationService orgService)
        {
            QueryByAttribute queryUsers = new QueryByAttribute
            {
                EntityName = "systemuser",
                ColumnSet = new ColumnSet("systemuserid")
            };

            queryUsers.AddAttributeValue("fullname", name);
            EntityCollection retrievedUsers = orgService.RetrieveMultiple(queryUsers);
            Guid systemUserId = ((Entity)retrievedUsers.Entities[0]).Id;

            return systemUserId;
        }

        public static IOrganizationService GetSystemOrgService(IOrganizationServiceFactory serviceFactory, IOrganizationService orgService)
        {
            IOrganizationService SystemService = serviceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", orgService));
            return SystemService;
            //return SystemService ?? (SystemService = serviceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", orgService)));
        }

    }
}
