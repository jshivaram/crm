﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using Excel;
using DocumentFormat.OpenXml.Packaging;
using Microsoft.Xrm.Sdk.Messages;
using DataUploader.Helper;
using System.Text.RegularExpressions;
using static DataUploader.Model.Enums;
using static DataUploader.Model.Errors;
using System.Diagnostics;

#if RELESEPLUGIN
#else
using SocketClients;
using DataUploader.Service;
#endif


namespace DataUploader
{
    public class ParseExcel
    {

        #if RELESEPLUGIN
#else
        private static WSMessageUploaderService wsMessageService { get; set; }
#endif

        public static bool ParseXLS(IOrganizationService _service, MemoryStream _msFile, ConcurrentDictionary<string, EntityJson> _objects, ConcurrentDictionary<string, ParsedRecords> _parsedRecords, List<string> _logger, DataUploaderSettings _settings)
        {
#if RELESEPLUGIN
#else
            wsMessageService = new WSMessageUploaderService(_settings);
            wsMessageService.SendMessage(MessageType.INFORMATION, GetTextError(380008), MessageStatus.START);
#endif


            IExcelDataReader excelReader2007 = ExcelReaderFactory.CreateBinaryReader(_msFile);
            //excelReader2007.IsFirstRowAsColumnNames = true;

            var resultNotCleared = excelReader2007.AsDataSet();
            excelReader2007.Close();

            if (!string.IsNullOrEmpty(_settings.WorkingSheet))
            {
                foreach (DataTable DT in resultNotCleared.Tables)
                {
                    if (_settings.WorkingSheet == _objects[DT.TableName].Name.ToLower())
                    {
                        var filteredValues = DT.AsEnumerable().Where(row => row.Field<dynamic>("A") != null && string.Compare((Convert.ToString(row.Field<dynamic>("A"))).Trim(), string.Empty) != 0).ToArray();
                        var _dt = filteredValues.CopyToDataTable();
                        _dt.TableName = DT.TableName;

                        if (_dt.Rows.Count == 0)
                        {
                            _settings.WorkingSheet = string.Empty;
                            continue;
                        }

                        //_logger = new List<string>();
                        //_logger.Add(GetTextError(380005) + "'" + _dt.TableName + "';");

                        if (_objects[_dt.TableName] != null && !string.IsNullOrEmpty(_objects[_dt.TableName].Name))
                        {
                            ParsedRecords pRecords = new ParsedRecords();
                            _parsedRecords.AddOrUpdate(_objects[_dt.TableName].Name.ToLower(), pRecords, (oldkey, oldvalue) => pRecords);
                            ReadAsDataTable(_service, _dt, _objects, _dt.TableName, _parsedRecords[_objects[_dt.TableName].Name.ToLower()], _logger, _objects[_dt.TableName].Name.ToLower(), _settings);
                        }

                        break;
                    }
                }
            }

            foreach (DataTable DT in resultNotCleared.Tables)
            {
                var filteredValues = DT.AsEnumerable().Where(row => row.Field<dynamic>("A") != null && string.Compare((Convert.ToString(row.Field<dynamic>("A"))).Trim(), string.Empty) != 0).ToArray();
                var _dt = filteredValues.CopyToDataTable();
                _dt.TableName = DT.TableName;

                if (_dt.Rows.Count == 0) continue;
                if (_settings.ProcessedSheets.IndexOf(_objects[_dt.TableName].Name.ToLower() + ",") == -1)
                {
                    //_logger = new List<string>();
                    //_logger.Add(GetTextError(380004) + "'" + _dt.TableName + "';");
#if RELESEPLUGIN
#else
                    wsMessageService.SendMessage(MessageType.INFORMATION, GetTextError(380004) + _dt.TableName, MessageStatus.START);
#endif
                    if (_objects[_dt.TableName] != null && !string.IsNullOrEmpty(_objects[_dt.TableName].Name))
                    {
                        ParsedRecords pRecords = new ParsedRecords();
                        _parsedRecords.AddOrUpdate(_objects[_dt.TableName].Name.ToLower(), pRecords, (oldkey, oldvalue) => pRecords);
                        ReadAsDataTable(_service, _dt, _objects, _dt.TableName, _parsedRecords[_objects[_dt.TableName].Name.ToLower()], _logger, _objects[_dt.TableName].Name.ToLower(), _settings);
                    }
                }

            }

#if RELESEPLUGIN
#else
            wsMessageService.SendMessage(MessageType.SUCCESS, GetTextError(380009), MessageStatus.FINISH);
#endif

            //Clear resultNotCleared (excel) DataSet
            resultNotCleared.Tables.Clear();
            resultNotCleared.Dispose();
            resultNotCleared = null;

            if (_settings.countErrors == 0)
                return false;
            else
                return true;
        }

        public static bool ParseXLSX(IOrganizationService _service, MemoryStream _msFile, ConcurrentDictionary<string, EntityJson> _objects, ConcurrentDictionary<string, ParsedRecords> _parsedRecords, List<string> _logger, DataUploaderSettings _settings)
        {
            using (var excelDoc = SpreadsheetDocument.Open(_msFile, true))
            {
                WorkbookPart workbookPart = excelDoc.WorkbookPart;
                IEnumerable<Sheet> sheets = excelDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                var sheetCount = 0;
                try
                {
                    sheetCount = sheets.Count();
                }
                catch
                {
                    sheetCount = 0;
                }

                if (sheetCount == 0)
                {
                    return false;
                }

                _logger.Add(" ");

                if (!string.IsNullOrEmpty(_settings.WorkingSheet))
                {
                    var sheetName = _objects.FirstOrDefault(x => x.Value.Name.ToLower() == _settings.WorkingSheet).Key;
                    var _sheet = sheets.Where(s => s.Name == sheetName).FirstOrDefault();
                    if (_sheet != null && _settings.ProcessedSheets.IndexOf(_settings.WorkingSheet) == -1)
                    {
                        WorksheetPart worksheetPart = (WorksheetPart)excelDoc.WorkbookPart.GetPartById(_sheet.Id.Value);

                        //_logger = new List<string>();
                        //_logger.Add(GetTextError(380005) + "'" + sheetName + "';");

                        ParsedRecords pRecords = new ParsedRecords();
                        _parsedRecords.AddOrUpdate(_settings.WorkingSheet, pRecords, (oldkey, oldvalue) => pRecords);
                        ReadAsDataTable(_service, excelDoc, worksheetPart, _objects, sheetName, _parsedRecords[_settings.WorkingSheet], _logger, _settings.WorkingSheet, _settings);

                    }
                }

                foreach (EntityJson sheetJson in _objects.Values)
                {
                    if (!string.IsNullOrEmpty(sheetJson.Name))
                    {

                    }

                    var sheetName = _objects.FirstOrDefault(x => x.Value.Name.ToLower() == sheetJson.Name.ToLower()).Key;
                    var _sheet = sheets.Where(s => s.Name == sheetName).FirstOrDefault();
                    if (_sheet != null && _settings.ProcessedSheets.IndexOf(sheetJson.Name.ToLower() + ",") == -1)
                    {
                        WorksheetPart worksheetPart = (WorksheetPart)excelDoc.WorkbookPart.GetPartById(_sheet.Id.Value);

                        //_logger = new List<string>();
                        //_logger.Add(GetTextError(380004) + "'" + sheetName + "';");
                        ParsedRecords pRecords = new ParsedRecords();
                        _parsedRecords.AddOrUpdate(sheetJson.Name.ToLower(), pRecords, (oldkey, oldvalue) => pRecords);
                        ReadAsDataTable(_service, excelDoc, worksheetPart, _objects, sheetName, _parsedRecords[sheetJson.Name.ToLower()], _logger, sheetJson.Name.ToLower(), _settings);
                    }
                }


                /*
                foreach (string key in objects.Keys)
                {
                    Sheet _sheet = null;

                    foreach (OpenXmlElement e in excelDoc.WorkbookPart.Workbook.Sheets.ChildElements)
                    {
                        _sheet = e as Sheet;
                        if (key.ToLower() == _sheet.Name.Value.ToLower() && settings.processedSheets.IndexOf(objects[key].Name.ToLower() + ",") == -1)
                        {
                            break;
                        }
                    }

                    if (_sheet != null)
                    {
                        WorksheetPart worksheetPart = (WorksheetPart)excelDoc.WorkbookPart.GetPartById(_sheet.Id.Value);

                        Logger.Add("> Start parsing sheet: " + key);
                        DataUploader.saveLog(service, _targetEntity.Id, Logger, "ddsm_log");

                        ReadAsDataTable(service, excelDoc, worksheetPart, objects, key, _targetEntity.Id, parsedRecords["rec"], Logger, objects[key].Name.ToLower(), settings);
                    }

                }
                */

            }
            if (_settings.countErrors == 0)
                return false;
            else
                return true;

        }

        private static void ReadAsDataTable(IOrganizationService _service, DataTable _dt, ConcurrentDictionary<string, EntityJson> _objects, string dtName, ParsedRecords _pRecords, List<string> _logger, string sheetJsonName, DataUploaderSettings _settings)
        {
            Guid recordUploaded = _settings.TargetEntity.Id;
            List<string> collectionUniqueGuids = new List<string>();
            List<string> collectionNotUniqueGuids = new List<string>();
            List<string> _loggerErrors = new List<string>();

            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };
            int NumberofRecords = 0, itemsRequestCount = 0, itemsResponceCount = 0, itemsErrorCount = 0, requestSets = 1;
            Entity updateDU;

            int rowNum = 0;
            string uniqueIdColumnKey = _objects[_dt.TableName].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (_dt.TableName + " GUID").ToLower()).Key;

            foreach (DataRow _dr in _dt.Rows)
            {
                rowNum++;
                if (rowNum > _settings.StartRowSheet && rowNum > _settings.RowNumberSheet)
                {
                    Entity newEntity = new Entity("ddsm_exceldata");
                    newEntity["ddsm_name"] = _dt.TableName;
                    newEntity["ddsm_statusxlsrecord"] = new OptionSetValue((int)StatusXLSRecord.NotProcessed);
                    newEntity["ddsm_datauploaderfile"] = new EntityReference("ddsm_datauploader", recordUploaded);
                    newEntity["ddsm_logicalname"] = sheetJsonName.ToLower();
                    newEntity["ddsm_config"] = _settings.JsonObjects;
                    newEntity["ddsm_duplicatedetection"] = _settings.DuplicateDetect;
                    newEntity["ddsm_deduplicationrules"] = new OptionSetValue(_settings.DedupRules);

                    if (!string.IsNullOrEmpty(uniqueIdColumnKey))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(_dr[uniqueIdColumnKey])))
                        {
                            if (collectionUniqueGuids.Contains(Convert.ToString(_dr[uniqueIdColumnKey])))
                            {
                                if (!collectionNotUniqueGuids.Contains(Convert.ToString(_dr[uniqueIdColumnKey])))
                                {
                                    collectionNotUniqueGuids.Add(Convert.ToString(_dr[uniqueIdColumnKey]));
                                }
                            }
                            else
                            {
                                collectionUniqueGuids.Add(Convert.ToString(_dr[uniqueIdColumnKey]));
                            }
                        }
                    }

                    foreach (var attr in _objects[_dt.TableName].Attrs)
                    {
                        if (_dr[attr.Key].ToString() != "")
                        {

                            if (attr.Value.AttrType.ToString() == "DateTime")
                                newEntity["ddsm_" + (attr.Key).ToLower()] = DataUploader.ParseAttributToDateTime(_dr[attr.Key]);
                            else if ((attr.Value.Name.ToString()).ToLower() == "ddsm_contactprimarykey" ||
                                (attr.Value.Name.ToString()).ToLower() == "ddsm_accountprimarykey" ||
                                (attr.Value.Name.ToString()).ToLower() == "ddsm_siteprimarykey")
                            {
                                newEntity["ddsm_" + (attr.Key).ToLower()] = Convert.ToString(_dr[attr.Key]).Trim().ToLower();
                            }
                            else
                            {
                                newEntity["ddsm_" + (attr.Key).ToLower()] = Convert.ToString(_dr[attr.Key]).Trim();
                            }
                        }
                    }

                    CreateRequest createRequest = new CreateRequest();
                    createRequest.Target = newEntity;
                    emRequest.Requests.Add(createRequest);
                    itemsRequestCount++;

                    //Create Multiple
                    if (itemsRequestCount == (requestSets * _settings.GlobalRequestsCount))
                    {
                        if (emRequest.Requests.Count > 0)
                            _settings.RowNumberSheet = _settings.RowNumberSheet + emRequest.Requests.Count;

                        GenerationEntity(_service, emRequest, _objects, _pRecords, _logger, _loggerErrors, _settings.CallExecuteMultiple);

                        requestSets++;
                        emRequest = new ExecuteMultipleRequest
                        {
                            Requests = new OrganizationRequestCollection(),
                            Settings = new ExecuteMultipleSettings
                            {
                                ContinueOnError = true,
                                ReturnResponses = true
                            }
                        };

                        //Update DataUploader record
                        updateDU = new Entity("ddsm_datauploader", recordUploaded);
                        _settings.WorkingSheet = sheetJsonName;
                        updateDU["ddsm_processedsheets"] = _settings.ProcessedSheets;
                        updateDU["ddsm_workingsheet"] = _settings.WorkingSheet;
                        updateDU["ddsm_rownumbersheet"] = _settings.RowNumberSheet;
                        _service.Update(updateDU);
                    }

                }
            }

            //Create Multiple
            if (emRequest.Requests.Count > 0)
            {
                if (emRequest.Requests.Count > 0)
                    _settings.RowNumberSheet = _settings.RowNumberSheet + emRequest.Requests.Count;

                GenerationEntity(_service, emRequest, _objects, _pRecords, _logger, _loggerErrors, _settings.CallExecuteMultiple);

                requestSets++;
                emRequest = new ExecuteMultipleRequest
                {
                    Requests = new OrganizationRequestCollection(),
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = true,
                        ReturnResponses = true
                    }
                };

                //Update DataUploader record
                updateDU = new Entity("ddsm_datauploader", recordUploaded);
                _settings.WorkingSheet = sheetJsonName;
                updateDU["ddsm_processedsheets"] = _settings.ProcessedSheets;
                updateDU["ddsm_workingsheet"] = _settings.WorkingSheet;
                updateDU["ddsm_rownumbersheet"] = _settings.RowNumberSheet;
                _service.Update(updateDU);
            }

            _pRecords.RequestCount += itemsRequestCount;
            //_pRecords.ResponceCount += itemsResponceCount;
            //_pRecords.ErrorCount += itemsErrorCount;

            if (collectionNotUniqueGuids.Count > 0)
            {
                _logger.Add(" ");
                _logger.Add(GetTextError(380010) + string.Join(", ", collectionNotUniqueGuids) + " not unique values in sheet " + _dt.TableName + " list");
                _settings.countErrors += collectionNotUniqueGuids.Count;
            }
            if (_loggerErrors.Count > 0)
            {
                _logger.Add(" ");
                _logger.AddRange(_loggerErrors);
            }

            _logger.Add(GetTextError(380006) + "'" + dtName + "'; " + " " + GetTextError(380055) + itemsRequestCount + "; " + GetTextError(380056) + itemsResponceCount + "; " + GetTextError(380057) + itemsErrorCount + ";");
            if (collectionNotUniqueGuids.Count > 0 || _loggerErrors.Count > 0)
            {
                _logger.Add(" ");
            }
            //DataUploader.saveLog(_service, recordUploaded, _logger, "ddsm_log");

            //Update DataUploader record

            updateDU = new Entity("ddsm_datauploader", recordUploaded);
            _settings.WorkingSheet = string.Empty;
            if (string.IsNullOrEmpty(_settings.ProcessedSheets))
                _settings.ProcessedSheets = sheetJsonName + ',';
            else
                _settings.ProcessedSheets += sheetJsonName + ',';
            updateDU["ddsm_processedsheets"] = _settings.ProcessedSheets;
            updateDU["ddsm_workingsheet"] = _settings.WorkingSheet;
            _settings.RowNumberSheet = -1;
            updateDU["ddsm_rownumbersheet"] = _settings.RowNumberSheet;
            _service.Update(updateDU);

        }

        private static void ReadAsDataTable(IOrganizationService _service, SpreadsheetDocument _spreadSheetDocument, WorksheetPart _worksheetPart, ConcurrentDictionary<string, EntityJson> _objects, string dtName, ParsedRecords _pRecords, List<string> _logger, string sheetJsonName, DataUploaderSettings _settings)
        {
            //_logger = new List<string>();
            Guid recordUploaded = _settings.TargetEntity.Id;
            List<string> collectionUniqueGuids = new List<string>();
            List<string> collectionNotUniqueGuids = new List<string>();
            List<string> _loggerErrors = new List<string>();

            Entity updateDU;
            OpenXmlReader reader = OpenXmlReader.Create(_worksheetPart);

            Int32 rowNum = 1, itemsRequestCount = 0, itemsResponceCount = 0, itemsErrorCount = 0, requestSets = 1;
            string uniqueIdColumnKey = _objects[dtName].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (dtName + " GUID").ToLower()).Key;

            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };

            while (reader.Read())
            {
                if (reader.ElementType == typeof(Row))
                {
                    if (reader.HasAttributes)
                        rowNum = Convert.ToInt32(reader.Attributes.First(a => a.LocalName == "r").Value);

                    if (rowNum > _settings.StartRowSheet && rowNum > _settings.RowNumberSheet)
                    {

                        reader.ReadFirstChild();

                        Entity newEntity = new Entity("ddsm_exceldata");
                        newEntity["ddsm_name"] = dtName;
                        newEntity["ddsm_logicalname"] = sheetJsonName.ToLower();
                        newEntity["ddsm_statusxlsrecord"] = new OptionSetValue((int)StatusXLSRecord.NotProcessed);
                        newEntity["ddsm_datauploaderfile"] = new EntityReference("ddsm_datauploader", recordUploaded);
                        newEntity["ddsm_config"] = _settings.JsonObjects;
                        newEntity["ddsm_duplicatedetection"] = _settings.DuplicateDetect;
                        newEntity["ddsm_deduplicationrules"] = new OptionSetValue(_settings.DedupRules);

                        do
                        {
                            if (reader.ElementType == typeof(Cell))
                            {
                                Cell cell = (Cell)reader.LoadCurrentElement();

                                var column = GetColumnName(cell.CellReference);
                                var attr = _objects[dtName].Attrs.FirstOrDefault(x => x.Key.ToLower() == column.ToLower());
                                var typeColmn = attr.Value.AttrType;

                                if (typeColmn.ToString() == "DateTime")
                                    newEntity["ddsm_" + column.ToLower()] = DataUploader.ParseAttributToDateTime(Convert.ToString(GetCellValue(_spreadSheetDocument, cell, true)).Trim());
                                else if ((attr.Value.Name.ToString()).ToLower() == "ddsm_contactprimarykey" ||
                                (attr.Value.Name.ToString()).ToLower() == "ddsm_accountprimarykey" ||
                                (attr.Value.Name.ToString()).ToLower() == "ddsm_siteprimarykey")
                                    newEntity["ddsm_" + column.ToLower()] = Convert.ToString(GetCellValue(_spreadSheetDocument, cell, false)).Trim().ToLower();
                                else
                                    newEntity["ddsm_" + column.ToLower()] = Convert.ToString(GetCellValue(_spreadSheetDocument, cell, false)).Trim();

                                //GUID
                                if (!string.IsNullOrEmpty(uniqueIdColumnKey) && uniqueIdColumnKey == column)
                                {
                                    string _uniqueGuidValue = newEntity.GetAttributeValue<string>("ddsm_" + column.ToLower());

                                    if (!string.IsNullOrEmpty(_uniqueGuidValue))
                                    {
                                        if (collectionUniqueGuids.Contains(_uniqueGuidValue))
                                        {
                                            if (!collectionNotUniqueGuids.Contains(_uniqueGuidValue))
                                            {
                                                collectionNotUniqueGuids.Add(_uniqueGuidValue);
                                            }
                                        }
                                        else
                                        {
                                            collectionUniqueGuids.Add(_uniqueGuidValue);
                                        }
                                    }
                                }
                            }

                        } while (reader.ReadNextSibling());

                            CreateRequest createRequest = new CreateRequest();
                            createRequest.Target = newEntity;
                            emRequest.Requests.Add(createRequest);
                            itemsRequestCount++;

                            //Create Multiple
                            if (itemsRequestCount == (requestSets * _settings.GlobalRequestsCount))
                            {

                                if (emRequest.Requests.Count > 0)
                                    _settings.RowNumberSheet = _settings.RowNumberSheet + emRequest.Requests.Count;

                                _settings.RowNumberSheet = rowNum;

                            GenerationEntity(_service, emRequest, _objects, _pRecords, _logger, _loggerErrors, _settings.CallExecuteMultiple);

                                requestSets++;
                                emRequest = new ExecuteMultipleRequest
                                {
                                    Requests = new OrganizationRequestCollection(),
                                    Settings = new ExecuteMultipleSettings
                                    {
                                        ContinueOnError = true,
                                        ReturnResponses = true
                                    }
                                };

                                //Update DataUploader record
                                updateDU = new Entity("ddsm_datauploader", recordUploaded);
                                _settings.WorkingSheet = sheetJsonName;
                                updateDU["ddsm_processedsheets"] = _settings.ProcessedSheets;
                                updateDU["ddsm_workingsheet"] = _settings.WorkingSheet;
                                updateDU["ddsm_rownumbersheet"] = _settings.RowNumberSheet;
                                _service.Update(updateDU);


                            }
                   

                    }
                }
            }

            //Create Multiple
            if (emRequest.Requests.Count > 0)
            {

                if (emRequest.Requests.Count > 0)
                    _settings.RowNumberSheet = _settings.RowNumberSheet + emRequest.Requests.Count;

                GenerationEntity(_service, emRequest, _objects, _pRecords, _logger, _loggerErrors, _settings.CallExecuteMultiple);

                requestSets++;
                emRequest = new ExecuteMultipleRequest
                {
                    Requests = new OrganizationRequestCollection(),
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = true,
                        ReturnResponses = true
                    }
                };

                //Update DataUploader record
                updateDU = new Entity("ddsm_datauploader", recordUploaded);
                _settings.WorkingSheet = sheetJsonName;
                updateDU["ddsm_processedsheets"] = _settings.ProcessedSheets;
                updateDU["ddsm_workingsheet"] = _settings.WorkingSheet;
                updateDU["ddsm_rownumbersheet"] = _settings.RowNumberSheet;
                _service.Update(updateDU);

            }

            //reader.Close();

            _pRecords.RequestCount += itemsRequestCount;
            _pRecords.ResponceCount += itemsResponceCount;
            _pRecords.ErrorCount += itemsErrorCount;

            if (collectionNotUniqueGuids.Count > 0)
            {
                _logger.Add(" ");
                _logger.Add(GetTextError(380010) + string.Join(", ", collectionNotUniqueGuids) + " not unique values in sheet " + dtName + " list");
                _settings.countErrors += collectionNotUniqueGuids.Count;
            }
            if (_loggerErrors.Count > 0)
            {
                _logger.Add(" ");
                _logger.AddRange(_loggerErrors);
            }

            _logger.Add(GetTextError(380006) + "'" + dtName + "'; " + " " + GetTextError(380055) + itemsRequestCount + "; " + GetTextError(380056) + itemsResponceCount + "; " + GetTextError(380057) + itemsErrorCount + ";");
            if (collectionNotUniqueGuids.Count > 0 || _loggerErrors.Count > 0)
            {
                _logger.Add(" ");
            }
            //DataUploader.saveLog(_service, recordUploaded, _logger, "ddsm_log");

            //Update DataUploader record
            updateDU = new Entity("ddsm_datauploader", recordUploaded);
            _settings.WorkingSheet = string.Empty;
            if (string.IsNullOrEmpty(_settings.ProcessedSheets))
                _settings.ProcessedSheets = sheetJsonName + ',';
            else
                _settings.ProcessedSheets += sheetJsonName + ',';
            updateDU["ddsm_processedsheets"] = _settings.ProcessedSheets;
            updateDU["ddsm_workingsheet"] = _settings.WorkingSheet;
            _settings.RowNumberSheet = -1;
            updateDU["ddsm_rownumbersheet"] = _settings.RowNumberSheet;
            _service.Update(updateDU);

        }

        private static string GetCellValue(SpreadsheetDocument spreadSheetDocument, Cell cell, bool isDate)
        {
            SharedStringTablePart stringTablePart = spreadSheetDocument.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;

            try
            {
                if (cell.DataType == null)
                {
                    value = GetCellValueWithoutConsideringDataType(spreadSheetDocument, cell, isDate);
                }

                if (cell.DataType != null && cell.DataType.HasValue)
                {
                    switch (cell.DataType.Value)
                    {
                        case CellValues.SharedString:
                            if (isDate)
                            {
                                value = ConverToDateValue(stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText);
                            }
                            else
                            {
                                value = stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                            }
                            break;
                        case CellValues.Boolean:
                            switch (value)
                            {
                                case "0":
                                    value = "FALSE";
                                    break;
                                default:
                                    value = "TRUE";
                                    break;
                            }
                            break;
                    }
                }
            }
            catch (ArgumentOutOfRangeException e)
            {
                value = null;
            }

            return value;
        }

        private static string GetColumnName(string cellName)
        {
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellName);

            return match.Value;
        }

        private static string GetFormatedValue(SpreadsheetDocument spreadSheetDocument, Cell cell, CellFormat cellformat, bool isDate)
        {
            string value = cell.InnerText;

            //Temporarily disabling the logic conversion value according to the format of the cell
            /*
            var NumberingFormats = spreadSheetDocument.WorkbookPart.WorkbookStylesPart.Stylesheet.NumberingFormats;
            if (cellformat.NumberFormatId != null && cellformat.NumberFormatId != 0) // 0 - General format
            {
                if (NumberingFormats != null)
                {
                    string format = spreadSheetDocument.WorkbookPart.WorkbookStylesPart.Stylesheet.NumberingFormats.Elements<NumberingFormat>()
                        .Where(i => i.NumberFormatId.Value == cellformat.NumberFormatId.Value)
                        .First().FormatCode;
                    //double number = double.Parse(cell.InnerText);
                    //value = number.ToString(format);
                }
                else
                {
                    var NewNumberingFormats = BuildFormatMappingsFromXlsx(spreadSheetDocument);
                }
            }
            */

            if (isDate)
            {
                value = ConverToDateValue(value);
            }

            return value;
        }

        private static string GetCellValueWithoutConsideringDataType(SpreadsheetDocument spreadSheetDocument, Cell cell, bool isDate)
        {
            CellFormat cellFormat = GetCellFormat(spreadSheetDocument, cell);
            if (cellFormat != null)
            {
                return GetFormatedValue(spreadSheetDocument, cell, cellFormat, isDate);
            }
            else
            {
                if (isDate)
                {
                    return ConverToDateValue(cell.InnerText);
                }
                else
                {
                    return cell.InnerText;
                }
            }
        }

        private static CellFormat GetCellFormat(SpreadsheetDocument spreadSheetDocument, Cell cell)
        {
            WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
            if (cell.StyleIndex != null && cell.StyleIndex.HasValue)
            {
                int styleIndex = (int)cell.StyleIndex.Value;
                CellFormat cellFormat = (CellFormat)workbookPart.WorkbookStylesPart.Stylesheet.CellFormats.ElementAt(styleIndex);
                return cellFormat;
            }
            else
            {
                return null;
            }
        }

        private Dictionary<uint, String> BuildFormatMappingsFromXlsx(SpreadsheetDocument spreadSheetDocument)
        {
            Dictionary<uint, String> formatMappings = new Dictionary<uint, String>();

            var stylePart = spreadSheetDocument.WorkbookPart.WorkbookStylesPart;

            var numFormatsParentNodes = stylePart.Stylesheet.ChildElements.OfType<NumberingFormats>();

            foreach (var numFormatParentNode in numFormatsParentNodes)
            {
                var formatNodes = numFormatParentNode.ChildElements.OfType<NumberingFormat>();
                foreach (var formatNode in formatNodes)
                {
                    formatMappings.Add(formatNode.NumberFormatId.Value, formatNode.FormatCode);
                }
            }

            return formatMappings;
        }

        private static string ConverToDateValue(string value)
        {
            try
            {
                if (DataUploader.IsNumeric(value))
                {
                    return Convert.ToString(DateTime.FromOADate(Convert.ToDouble(value)));
                }
                else
                {
                    return Convert.ToString(DataUploader.ParseAttributToDateTime(value));
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        /*
        private void InitialTimeOut(IOrganizationService _orgService, Settings _settings, Entity updateDU, bool sheetComleted, int timeOffset)
        {
            if (_orgService != null)
            {
                if (updateDU != null && sheetComleted)
                {
                    _orgService.Update(updateDU);
                }
            }

            DateTime pointParsingTime = DataUploader.convertAttributToDateTimeUtc("today");
            int timeOut = DataUploader.getDateTimeDiff(_settings.startPluginTime, pointParsingTime);
            if (timeOut > (_settings.timeOutLimit - timeOffset))
            {
                if (_orgService != null)
                {
                    if (updateDU != null && !sheetComleted)
                    {
                        _orgService.Update(updateDU);
                    }
                }

                throw new System.TimeoutException("TimeOut: " + Convert.ToString(timeOut) + " sec.");
            }
        }

        private bool VerifyingTimeOut(Settings _settings, int timeOffset = 0)
        {
            DateTime pointParsingTime = DataUploader.convertAttributToDateTimeUtc("today");
            int timeOut = DataUploader.getDateTimeDiff(_settings.startPluginTime, pointParsingTime);
            if (timeOut > (_settings.timeOutLimit - timeOffset))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        */
        private Entity GenerateObjectUpdating(Guid recordUploaded, string processedSheets, string workingSheet, int rowNumberSheet)
        {
            Entity updateDU = new Entity("ddsm_datauploader", recordUploaded);
            updateDU["ddsm_processedsheets"] = processedSheets;
            updateDU["ddsm_workingsheet"] = workingSheet;
            updateDU["ddsm_rownumbersheet"] = rowNumberSheet;
            return updateDU;

        }

        private static void GenerationEntity(IOrganizationService _service, ExecuteMultipleRequest emRequest, ConcurrentDictionary<string, EntityJson> _objects, ParsedRecords _pRecords, List<string> _logger, List<string> _loggerErrors, bool callExecuteMultiple = true)
        {
            ExecuteMultipleRequest errRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };


            int indexReq = -1;

            if (!callExecuteMultiple)
            {
                //Single
                foreach (var req in emRequest.Requests)
                {
                    indexReq++;
                    try
                    {
                        if (req.RequestName == "Create")
                        {
                            Guid entityId = _service.Create(((CreateRequest)req).Target);
                            _pRecords.ResponceCount++;
                        }
                    }
                    catch (Exception e)
                    {
                        List<string> headers = new List<string>();
                        Entity errRecord = (Entity)emRequest.Requests[indexReq].Parameters["Target"];
                        var sheetName = _objects.FirstOrDefault(x => x.Value.Name.ToLower() == errRecord.GetAttributeValue<string>("ddsm_name").ToLower()).Key;
                        string pattern = @"\'ddsm_\w\'";
                        MatchCollection matchs = Regex.Matches(e.Message, pattern);
                        foreach (Match match in matchs)
                        {
                            var fieldName = (match.Value).Replace("'", "");
                            string fieldKey = (fieldName.Replace("ddsm_", "")).ToUpper();
                            var fieldHeader = _objects[sheetName].Attrs.FirstOrDefault(x => x.Key == fieldKey).Value.Header;
                            headers.Add(fieldHeader);
                            errRecord.Attributes.Remove(fieldName);
                        }
                        //itemsErrorCount++;
                        _loggerErrors.Add(GetTextError(380050) + sheetName + GetTextError(380051) + string.Join(",", headers) + GetTextError(380052) + (indexReq + 1) + GetTextError(380053) + e.Message);

#if RELESEPLUGIN
#else
                        wsMessageService.SendMessage(MessageType.ERROR, GetTextError(380050) + sheetName + GetTextError(380051) + string.Join(",", headers) + GetTextError(380052) + (indexReq + 1) + GetTextError(380053) + e.Message, MessageStatus.START);
#endif

                        CreateRequest createRequest = new CreateRequest();
                        createRequest.Target = errRecord;
                        errRequest.Requests.Add(createRequest);

                    }
                }

                if (errRequest.Requests.Count > 0)
                {
                    _loggerErrors.Add(GetTextError(380007));
                    indexReq = -1;
                    foreach (var req in errRequest.Requests)
                    {
                        try
                        {
                            if (req.RequestName == "Create")
                            {
                                Guid entityId = _service.Create(((CreateRequest)req).Target);
                                _pRecords.ErrorCount++;
                            }

                        }
                        catch (Exception e)
                        {
                            _loggerErrors.Add(GetTextError(380054) + e.Message);
#if RELESEPLUGIN
#else
                            wsMessageService.SendMessage(MessageType.ERROR, GetTextError(380054) + e.Message, MessageStatus.START);
#endif
                        }
                    }

                }

            }
            else
            {

                ExecuteMultipleResponse emResponse = (ExecuteMultipleResponse)_service.Execute(emRequest);

                foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                {
                    // A valid response.
                    if (responseItem.Response != null)
                    {
                        _pRecords.ResponceCount++;
                        if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                        {
                        }
                    }

                    // An error has occurred.
                    else if (responseItem.Fault != null)
                    {
                        List<string> headers = new List<string>();
                        Entity errRecord = (Entity)emRequest.Requests[responseItem.RequestIndex].Parameters["Target"];
                        var sheetName = _objects.FirstOrDefault(x => x.Value.Name.ToLower() == errRecord.GetAttributeValue<string>("ddsm_name").ToLower()).Key;
                        string pattern = @"\'ddsm_\w\'";
                        MatchCollection matchs = Regex.Matches(responseItem.Fault.Message, pattern);
                        foreach (Match match in matchs)
                        {
                            var fieldName = (match.Value).Replace("'", "");
                            string fieldKey = (fieldName.Replace("ddsm_", "")).ToUpper();
                            var fieldHeader = _objects[sheetName].Attrs.FirstOrDefault(x => x.Key == fieldKey).Value.Header;
                            headers.Add(fieldHeader);
                            errRecord.Attributes.Remove(fieldName);
                        }
                        //itemsErrorCount++;
                        _loggerErrors.Add(GetTextError(380050) + sheetName + GetTextError(380051) + string.Join(",", headers) + GetTextError(380052) + (responseItem.RequestIndex + 1) + GetTextError(380053) + responseItem.Fault.Message);

#if RELESEPLUGIN
#else
                        wsMessageService.SendMessage(MessageType.ERROR, GetTextError(380050) + sheetName + GetTextError(380051) + string.Join(",", headers) + GetTextError(380052) + (indexReq + 1) + GetTextError(380053) + responseItem.Fault.Message, MessageStatus.START);
#endif

                        CreateRequest createRequest = new CreateRequest();
                        createRequest.Target = errRecord;
                        errRequest.Requests.Add(createRequest);
                    }
                }

                if (errRequest.Requests.Count > 0)
                {
                    _loggerErrors.Add(GetTextError(380007));
                    emResponse = (ExecuteMultipleResponse)_service.Execute(errRequest);
                    foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                    {
                        if (responseItem.Response != null)
                        {
                            _pRecords.ErrorCount++;
                            if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                            {
                            }
                        }
                        else if (responseItem.Fault != null)
                        {
                            _loggerErrors.Add(GetTextError(380054) + responseItem.Fault.Message);
#if RELESEPLUGIN
#else
                            wsMessageService.SendMessage(MessageType.ERROR, GetTextError(380054) + responseItem.Fault.Message, MessageStatus.START);
#endif

                        }
                    }
                }

            }

        }

    }
}
