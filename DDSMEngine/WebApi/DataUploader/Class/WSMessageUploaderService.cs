﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataUploader;
using static DataUploader.Model.Enums;
using Newtonsoft.Json;
using SocketClients;

namespace DataUploader.Service
{
    public class WSMessageUploaderService
    {

        private readonly WsMessageDU _wsMessageUploader;

        public WSMessageUploaderService(DataUploaderSettings _thisSettings)
        {
            var wsMessageUploader = new WsMessageDU();
            wsMessageUploader.PrimaryEntityId = _thisSettings.TargetEntity.Id;
            wsMessageUploader.MessageID = Guid.NewGuid();
            wsMessageUploader.PrimaryEntityName = "ddsm_datauploader";
            wsMessageUploader.UserId = _thisSettings.UserGuid;
            wsMessageUploader.FileName = _thisSettings.FileName;
            _wsMessageUploader = wsMessageUploader;
        }

        /// <summary>
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="message"></param>
        /// <param name="status"></param>
        public void SendMessage(MessageType messageType, string message, MessageStatus status)
        {
            _wsMessageUploader.MessageType = messageType.ToString();
            _wsMessageUploader.Message = _wsMessageUploader.FileName + "<br/>" + message;
            _wsMessageUploader.Status = status.ToString();
            try
            {
                StaticSocketClient.Send(_wsMessageUploader.UserId, JsonConvert.SerializeObject(_wsMessageUploader));
            }
            catch (Exception e)
            {
                //
            }
        }
    }

    public class WsMessageDU
    {
        public WsMessageDU()
        {
            RelatedEnityName = new List<string>();
            RelatedEntity = new List<RelatedEntityData>();
        }

        public string Message { get; set; }
        public Guid MessageID { get; set; }
        public string MessageType { get; set; }
        public string PrimaryEntityName { get; set; }
        public Guid PrimaryEntityId { get; set; }
        public string FileName { get; set; }

        public List<string> RelatedEnityName { get; set; }
        public List<RelatedEntityData> RelatedEntity { get; set; }
        public string Status { get; set; }
        public Guid UserId { get; set; }

        public class RelatedEntityData
        {
            public RelatedEntityData(Guid recordId, string entityLogicalName)
            {
                EntityLogicalName = entityLogicalName;
                RecordId = recordId;
            }

            public string EntityLogicalName { get; set; }
            public Guid RecordId { get; set; }
        }
    }

}
