﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Query;
using static DataUploader.Model.Enums;
using static DataUploader.Model.Errors;
using DataUploader;
using System.Net.Http;
using System.Text;
using System.Linq;
using System.ServiceModel;

namespace DataUploader.Helper
{

    public abstract class BasePlugin : IPlugin
    {
        /*
        #region Secure/Unsecure Configuration Setup
        private string _secureConfig = null;
        private string _unsecureConfig = null;

        public BasePlugin(string unsecureConfig, string secureConfig)
        {
            _secureConfig = secureConfig;
            _unsecureConfig = unsecureConfig;
        }
        #endregion
        */

        private IServiceProvider _ServiceProvider;
        private readonly string _usersettingsLogicalName = "usersettings";

        public void Execute(IServiceProvider serviceProvider)
        {
            _ServiceProvider = serviceProvider;
            Execute();
        }

        private void Execute()
        {

            List<string> Logger = new List<string>();
            string jsonStr = string.Empty;

            DataUploaderSettings thisSettings = new DataUploaderSettings();

            ITracingService tracer = (ITracingService)_ServiceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)_ServiceProvider.GetService(typeof(IPluginExecutionContext));
            if (context == null)
            {
                throw new ArgumentNullException("Context is NULL");
            }
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)_ServiceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService orgService = serviceFactory.CreateOrganizationService(context.UserId);
            IOrganizationService systemService = DataUploader.GetSystemOrgService(serviceFactory, orgService);

            Entity target = GetTargetEntity(context);

            try
            {
                if (target == null)
                {
                    throw new ArgumentNullException("Target is NULL");
                }

                //Guid InitiatingUser = _DataUploader.context.InitiatingUserId;
                Guid InitiatingUser = context.UserId;
                thisSettings.UserGuid = InitiatingUser;
                thisSettings.TargetEntity = new EntityReference(target.LogicalName, target.Id);

                thisSettings.TimeZoneInfo = TimeZoneInfo.Utc;
                if (thisSettings.UserGuid != Guid.Empty)
                {
                    string windowsTimeZoneName = GetTimeZoneByCRMConnection(systemService, thisSettings.UserGuid);
                    thisSettings.TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZoneName);
                }

                QueryExpression recordUploadedQuery = new QueryExpression
                {
                    EntityName = "ddsm_datauploader",
                    ColumnSet = new ColumnSet(
                        "ddsm_name"
                    ,"ddsm_config"
    , "ddsm_datauploaderconfiguration"
    , "ddsm_rownumbersheet"
    , "ddsm_workingsheet"
    , "ddsm_processedsheets"
    , "ddsm_datauploaderesprecalculation"
    , "ddsm_typeofuploadeddata"
    , "ddsm_globalrequestscount"
    , "ddsm_globalpagerecordscount"
    , "ddsm_startdatarowdu"
    , "ddsm_creatorrecordtype"
    , "ddsm_statusfiledatauploading"
    , "ddsm_deduplicationrules"
    , "ddsm_duplicatedetection"
    , "ddsm_fileuploaded"
    , "ddsm_fileparsed",
    "ddsm_callexecutemultiple"
                    )
                };
                recordUploadedQuery.Criteria = new FilterExpression(LogicalOperator.And);
                recordUploadedQuery.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));
                recordUploadedQuery.Criteria.AddCondition("ddsm_datauploaderid", ConditionOperator.Equal, target.Id);
                EntityCollection recordUploadedRetrieve = systemService.RetrieveMultiple(recordUploadedQuery);

                if (!(recordUploadedRetrieve != null && recordUploadedRetrieve.Entities.Count > 0))
                {
                    Logger.Add(GetTextError(380001));
                    tracer.Trace(GetTextError(380001));

                    Entity _dataUploader = new Entity("ddsm_datauploader", target.Id);
                    _dataUploader.Attributes.Add("ddsm_log", string.Join("\n", Logger));
                    systemService.Update(_dataUploader);

                    return;
                }

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_name"))
                    thisSettings.FileName = recordUploadedRetrieve[0].GetAttributeValue<string>("ddsm_name");

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_startdatarowdu"))
                    thisSettings.StartRowSheet = (int)recordUploadedRetrieve[0].GetAttributeValue<int>("ddsm_startdatarowdu");

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_typeofuploadeddata"))
                    thisSettings.TypeConfig = (int)recordUploadedRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_typeofuploadeddata").Value;

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_globalrequestscount"))
                    thisSettings.GlobalRequestsCount = (int)recordUploadedRetrieve[0].GetAttributeValue<int>("ddsm_globalrequestscount");

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_globalpagerecordscount"))
                    thisSettings.GlobalPageRecordsCount = (int)recordUploadedRetrieve[0].GetAttributeValue<int>("ddsm_globalpagerecordscount");

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_creatorrecordtype"))
                    thisSettings.CreatorRecordType = (int)recordUploadedRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_creatorrecordtype").Value;

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_fileuploaded"))
                    thisSettings.FileUploaded = (bool)recordUploadedRetrieve[0].GetAttributeValue<bool>("ddsm_fileuploaded");

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_fileparsed"))
                    thisSettings.FileParsed = (bool)recordUploadedRetrieve[0].GetAttributeValue<bool>("ddsm_fileparsed");

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_rownumbersheet"))
                    thisSettings.RowNumberSheet = (int)recordUploadedRetrieve[0].GetAttributeValue<int>("ddsm_rownumbersheet");

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_workingsheet"))
                    thisSettings.WorkingSheet = (string)recordUploadedRetrieve[0].GetAttributeValue<string>("ddsm_workingsheet");

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_processedsheets"))
                    thisSettings.ProcessedSheets = (string)recordUploadedRetrieve[0].GetAttributeValue<string>("ddsm_processedsheets");

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_statusfiledatauploading"))
                    thisSettings.StatusFileDataUploading = (int)recordUploadedRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_statusfiledatauploading").Value;

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_deduplicationrules"))
                    thisSettings.DedupRules = (int)recordUploadedRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_deduplicationrules").Value;

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_duplicatedetection"))
                    thisSettings.DuplicateDetect = (bool)recordUploadedRetrieve[0].GetAttributeValue<bool>("ddsm_duplicatedetection");

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_datauploaderesprecalculation"))
                    thisSettings.EspRecalcData = (int)recordUploadedRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_datauploaderesprecalculation").Value;

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_callexecutemultiple"))
                    thisSettings.CallExecuteMultiple = (bool)recordUploadedRetrieve[0].GetAttributeValue<bool>("ddsm_callexecutemultiple");

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_config"))
                {
                    thisSettings.JsonObjects = (string)recordUploadedRetrieve[0].GetAttributeValue<string>("ddsm_config");
                }
                else
                {
                    if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_datauploaderconfiguration") && recordUploadedRetrieve[0].GetAttributeValue<EntityReference>("ddsm_datauploaderconfiguration").Id != null) {
                        QueryExpression ConfigJson = new QueryExpression { EntityName = "ddsm_datauploaderconfiguration", ColumnSet = new ColumnSet("ddsm_config") };
                        ConfigJson.Criteria.AddCondition("ddsm_datauploaderconfigurationid", ConditionOperator.Equal, recordUploadedRetrieve[0].GetAttributeValue<EntityReference>("ddsm_datauploaderconfiguration").Id);
                        EntityCollection ConfigJsonRetrieve = systemService.RetrieveMultiple(ConfigJson);
                        if (ConfigJsonRetrieve != null && ConfigJsonRetrieve.Entities.Count == 1)
                        {
                            thisSettings.JsonObjects = (string)ConfigJsonRetrieve[0].GetAttributeValue<string>("ddsm_config");

                        }
                        else
                        {
                            Logger.Add(GetTextError(380002));
                            Entity _dataUploader = new Entity("ddsm_datauploader", target.Id);
                            _dataUploader.Attributes.Add("ddsm_log", string.Join("\n", Logger));
                            systemService.Update(_dataUploader);

                            throw new Exception(GetTextError(380002));
                        }
                    }
                    else
                    {
                        Logger.Add(GetTextError(380002));
                        Entity _dataUploader = new Entity("ddsm_datauploader", target.Id);
                        _dataUploader.Attributes.Add("ddsm_log", string.Join("\n", Logger));
                        systemService.Update(_dataUploader);

                        throw new Exception(GetTextError(380002));
                    }
                }


                if (thisSettings.CreatorRecordType == (int)CreatorRecordType.Front) { }
                else
                {
                    Logger.Add("Error: " + "*** FileUploaded: " + Convert.ToString(thisSettings.FileUploaded) + "; CreatorRecordType: " + Convert.ToString(thisSettings.CreatorRecordType) + " ***");
                    Entity _dataUploader = new Entity("ddsm_datauploader", target.Id);
                    _dataUploader.Attributes.Add("ddsm_log", string.Join("\n", Logger));
                    systemService.Update(_dataUploader);

                    throw new Exception("*** FileUploaded: " + Convert.ToString(thisSettings.FileUploaded) + "; CreatorRecordType: " + Convert.ToString(thisSettings.CreatorRecordType) + " ***");
                }

                thisSettings.CurrencyGuid = GetGuidofCurrency(systemService);

                Main(context, tracer, systemService, target, thisSettings);

            }
            catch (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex)
            {
                if (target != null)
                {
                    DataUploader.SetStatusFileDataUploading(systemService, (int)StatusFileDataUploading.importFailed, target.Id);
                }

                tracer.Trace($"The plugin {GetType().FullName} was stopped due to an error!");
                tracer.Trace("Timestamp: " + ex.Detail.Timestamp);
                tracer.Trace("Code: " + ex.Detail.ErrorCode);
                tracer.Trace("Message: " + ex.Detail.Message);
            }
            catch (Exception e)
            {
                if (target != null)
                {
                    DataUploader.SetStatusFileDataUploading(systemService, (int)StatusFileDataUploading.importFailed, target.Id);
                }
                tracer.Trace($"The plugin {GetType().FullName} was stopped due to an error!");
                tracer.Trace($"Error mesage: {e.Message}");

            }
        }

        /// <summary>
        ///     Start Executing a Action
        /// </summary>
        private void Main(IPluginExecutionContext context, ITracingService Tracer, IOrganizationService service, object target, DataUploaderSettings settings)
        {

            Run(context, Tracer, service, target, settings);

        }

        /// <summary>
        ///     Abstract method should be implemented in a child class
        /// </summary>
        protected abstract void Run(IPluginExecutionContext context, ITracingService Tracer, IOrganizationService service, object target, DataUploaderSettings settings);

        /// <summary>
        /// Returns the current "Target" entity that the plugin is executing against.
        /// </summary>
        /// <returns></returns>
        protected virtual Entity GetTargetEntity(IPluginExecutionContext _context)
        {
            if (_context.InputParameters.Contains("Target") && _context.InputParameters["Target"] is Entity)
            {
                return (Entity)_context.InputParameters["Target"];
            }
            return null;
        }

        /// <summary>
        /// Returns whether the plugin is currently enrolled within a database transaction.
        /// </summary>
        /// <returns></returns>
        protected virtual bool IsInTransaction()
        {
            var context = (IPluginExecutionContext)_ServiceProvider.GetService(typeof(IPluginExecutionContext));
            return context.IsInTransaction;
        }

        //Get User Currency Info
        private static Guid GetGuidofCurrency(IOrganizationService _orgService)
        {
            Guid CurrencyGuid = new Guid();

            QueryExpression queryCurrency = new QueryExpression("currency")
            {
                EntityName = "transactioncurrency",
                ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression()
            };
            queryCurrency.Criteria.AddCondition("isocurrencycode", ConditionOperator.Equal, "CAD");
            try
            {
                DataCollection<Entity> entityData = _orgService.RetrieveMultiple(queryCurrency).Entities;
                int count = entityData.Count;

                if (count > 0)
                {
                    CurrencyGuid = (Guid)entityData[0].Id;

                }
                else
                {
                    queryCurrency = new QueryExpression("currency")
                    {
                        EntityName = "transactioncurrency",
                        ColumnSet = new ColumnSet(true),
                        Criteria = new FilterExpression()
                    };
                    entityData = _orgService.RetrieveMultiple(queryCurrency).Entities;
                    count = entityData.Count;
                    if (count > 0)
                        CurrencyGuid = (Guid)entityData[0].Id;
                }
            }
            catch (Exception e)
            {
                return Guid.Empty;
            }

            return CurrencyGuid;
        }

        //Get User TimeZone info
        private string GetTimeZoneByCRMConnection(IOrganizationService _orgService, Guid userId)
        {
            string timeZoneCode = "92"; //UTC
            var query = new QueryExpression(_usersettingsLogicalName);
            query.ColumnSet = new ColumnSet("systemuserid", "timezonecode");
            query.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, userId);

            Entity userSettings = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();
            if (userSettings != null && userSettings.Attributes.ContainsKey("timezonecode"))
            {
                timeZoneCode = userSettings.Attributes["timezonecode"].ToString();
            }
            else
            {
                return TimeZone.CurrentTimeZone.StandardName; //return local
            }

            query = new QueryExpression("timezonedefinition");
            query.ColumnSet = new ColumnSet("timezonecode", "standardname");

            query.Criteria.AddCondition("timezonecode", ConditionOperator.Equal, timeZoneCode);
            Entity timeZoneDefinitions = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            return timeZoneDefinitions.Attributes["standardname"].ToString();
        }

        //Remote Data Uploader
        protected bool RunRemote(IOrganizationService orgService, DataUploaderSettings _thisSettings, bool isParser = true)
        {
            bool remoteUploadOfData = false;
            string remoteApiUrl = string.Empty, parserRemoteApiUrl = string.Empty, creatorRemoteApiUrl = string.Empty, websocketUrl = string.Empty;

            try
            {
                QueryExpression query = new QueryExpression
                {
                    EntityName = "ddsm_admindata",
                    ColumnSet = new ColumnSet("ddsm_admindataid", "ddsm_remoteuploadofdata", "ddsm_duremoteapiurl", "ddsm_duremoteapiurl2", "ddsm_duwebsocketurl")
                };

                query.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, "Admin Data");
                query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

                EntityCollection queryRetrieve = orgService.RetrieveMultiple(query);
                if (queryRetrieve != null && queryRetrieve.Entities.Count == 1)
                {
                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_remoteuploadofdata"))
                        remoteUploadOfData = queryRetrieve[0].GetAttributeValue<bool>("ddsm_remoteuploadofdata");

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_duremoteapiurl"))
                        parserRemoteApiUrl = queryRetrieve[0].GetAttributeValue<string>("ddsm_duremoteapiurl");

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_duremoteapiurl2"))
                        creatorRemoteApiUrl = queryRetrieve[0].GetAttributeValue<string>("ddsm_duremoteapiurl2");

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_duwebsocketurl"))
                        websocketUrl = queryRetrieve[0].GetAttributeValue<string>("ddsm_duwebsocketurl");

                    if (remoteUploadOfData && !string.IsNullOrEmpty(parserRemoteApiUrl))
                    {
                        /*
                        var json = Newtonsoft.Json.JsonConvert.SerializeObject(_thisSettings);

                        var client = new HttpClient();
                        var content = new StringContent(json, Encoding.UTF8, "application/json");
                        List<string> _logger = new List<string>();
                        if (isParser)
                        {
                            //Parser Api
                            remoteApiUrl = parserRemoteApiUrl + "RunDataUploaderParserWithSettings";
                        }
                        else
                        {
                            //Creator Api
                            remoteApiUrl = ((!string.IsNullOrEmpty(creatorRemoteApiUrl))?creatorRemoteApiUrl: parserRemoteApiUrl) + "RunDataUploaderCreatorWithSettings";
                        }

                        var postResult = client.PostAsync(remoteApiUrl, content);
                        postResult.Wait();
                        var _postResult = postResult.Result.IsSuccessStatusCode;
                        if (_postResult)
                        {
                            if (isParser)
                            {
                                //Parser Api
                                _logger.Add(GetTextError(380017));
                            }
                            else
                            {
                                //Creator Api
                                _logger.Add(" ");
                                _logger.Add(GetTextError(380018));
                            }
                            DataUploader.SaveLog(orgService, _thisSettings.TargetEntity.Id, _logger, "ddsm_log");
                        }
                        return _postResult;
                        */

                        List<string> _logger = new List<string>();
                        if (isParser)
                        {
                            //Parser Api
                            _logger.Add(GetTextError(380024));
                        }
                        else
                        {
                            //Creator Api
                            _logger.Add(" ");
                            _logger.Add(GetTextError(380025));
                        }
                        DataUploader.SaveLog(orgService, _thisSettings.TargetEntity.Id, _logger, "ddsm_log");

                        return true;
                    }

                }
                return false;

            }
            catch (Exception e)
            {
                return false;
                //
            }
        }

        //Verify started plugin uploader
        protected bool isStartedPlugin(IOrganizationService orgService, string flagName, Guid _guid) {
            bool isStarted = false;

            QueryExpression recordUploadedQuery = new QueryExpression
            {
                EntityName = "ddsm_datauploader",
                ColumnSet = new ColumnSet(flagName)
            };
            recordUploadedQuery.Criteria = new FilterExpression(LogicalOperator.And);
            recordUploadedQuery.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));
            recordUploadedQuery.Criteria.AddCondition("ddsm_datauploaderid", ConditionOperator.Equal, _guid);
            EntityCollection recordUploadedRetrieve = orgService.RetrieveMultiple(recordUploadedQuery);

            if (recordUploadedRetrieve != null && recordUploadedRetrieve.Entities.Count == 1)
            {
                if (recordUploadedRetrieve[0].Attributes.ContainsKey(flagName))
                    isStarted = (bool)recordUploadedRetrieve[0].GetAttributeValue<bool>(flagName);
            }

            return isStarted;
        }
    }
}
