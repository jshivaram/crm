﻿using System;
using System.Collections.Generic;

namespace DataUploader.Helper
{

    public class Errors
    {
        private static Dictionary<string, string> ErrorList { get; } = new Dictionary<string, string>()
        {
            { "380001", "Error > This record is not active"},
            { "380002", "Error > Configuration record not found"},
            { "380003", "Error > Excel file not found"},
            { "380004", "*** Start parsing a sheet: "},
            { "380005", "*** Parsing the sheet continues: "},
            { "380006", "*** End parsed a sheet: "},
            { "380007", "*** Repeated parsing with the exception of invalid data"},
            { "380008", "*** The parsing of the file was started"},
            { "380009", "*** Parsing of the file is finished"},
            { "380010", ""},
            { "380011", "*** The creation of records in the database was started"},
            { "380012", "*** The creation of records in the database was completed"},
            { "380013", "*** The recalculation of created/uploaded records in the database was started: "},
            { "380014", "*** Record was created in the entity 'Task Queue': - "},
            { "380015", "An entry with this value was not found in the DB or the quantity of this entry more than one!"},
            { "380016", "Value not found in DB or not converted!"},
            { "380017", "!!! Starting Remote Data Upload API (parsing file)"},
            { "380018", "!!! Starting Remote Data Upload API (creation of records)"},
            { "380019", "All: "},
            { "380020", "Created: "},
            { "380021", "Updated: "},
            { "380022", "Skipped/Doesn't need to be updated: "},
            { "380023", "*** Parsing of the file is finished with Errors"},
            { "380024", "!!! The work of the plugin (parsing file) was completed because the remote calculation was activated"},
            { "380025", "!!! The work of the plugin (creation of records) was completed because the remote calculation was activated"},

            { "380030", "The creation of "},
            { "380031", "s records in the database was started"},

            { "380050", "Error > Sheet: '"},
            { "380051", "'; Columns: '"},
            { "380052", "'; Index: "},
            { "380053", "; Message: "},
            { "380054", "Error > Message: "},
            { "380055", "Excel Records: "},
            { "380056", "; Parsed Records: "},
            { "380057", "; Parsed Records (excluded invalid data): "},
            { "380058", ""},
            { "380059", ""},
            { "380060", ""}
        };


        public static string GetTextError(Int32 errorCode)
        {

            return ErrorList[Convert.ToString(errorCode)];
        }
    }
}
