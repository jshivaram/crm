﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;

namespace DataUploader.Helper
{
    public class CreatedEntity
    {
        public string Name { get; set; }
        public Guid EntityGuid { get; set; }
        public string PrimaryKey { get; set; }
        public Guid SourceId { get; set; }
        public bool Status { get; set; }
        public bool StatusDb { get; set; }
    }
    public class StatisticRecords
    {
        public int Created { get; set; } = 0;
        public int Updated { get; set; } = 0;
        public int DoNothing { get; set; } = 0;
        public int Errors { get; set; } = 0;
        public int All { get; set; } = 0;
    }
    public class ProjectBusinessProcessStage
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }
    public class ProjectBusinessProcess
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public List<ProjectBusinessProcessStage> Stages { get; set; }
    }

    public class PaginationRetriveMultiple
    {
        public EntityCollection RetrieveCollection { get; set; } = new EntityCollection();
        public int PageNumber { get; set; } = 1;
        public dynamic PagingCookie { get; set; } = null;
        public bool MoreRecords { get; set; } = false;
    }

    public class UserInputObj2
    {
        public List<Guid> SmartMeasures { get; set; }
        public Dictionary<string, string> DataFields { get; set; }
    }

    //Sort meas Ids
    public class CompareGuidString : IComparer<CreatedEntity>
    {
        public int Compare(CreatedEntity x, CreatedEntity y)
        {
            return x.Name.CompareTo(y.Name);
            //return x.addInfo.CompareTo(y.addInfo);
        }
    }

    //Parent/Relation Data Attributes
    public class RelationDataAttributes
    {
        public Entity DataAttributes1 { get; set; } = null;
        public Entity DataAttributes2 { get; set; } = null;
        public DataCollection<Entity> DataCollection1 { get; set; } = null;
        public DataCollection<Entity> DataCollectio2 { get; set; } = null;
        public ProjectBusinessProcess BusinessProcess { get; set; } = null;
        public string TplId { get; set; } = string.Empty;
        public string TplName { get; set; } = string.Empty;
        public string BpfName { get; set; } = string.Empty;
        public int IntVal { get; set; } = 1;
    }

    public class IsNull
    {
        public bool Value { get; } = true;
    }


}
