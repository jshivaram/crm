﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;

namespace DataUploader.Helper
{
    public class DataUploaderSettings
    {
        public string FileName { get; set; } = string.Empty;

        public int StartRowSheet { get; set; } = 1;

        public bool CallExecuteMultiple { get; set; } = false;
        public int GlobalRequestsCount { get; set; } = 100;
        public int GlobalPageRecordsCount { get; set; } = 1000;
        public int DedupRules { get; set; } = 962080002;
        public bool DuplicateDetect { get; set; } = true;
        public int EspRecalcData { get; set; } = 962080000;
        public int TypeConfig { get; set; } = -2147483648;
        public int CreatorRecordType { get; set; } = -2147483648;
        public int UploadStatus { get; set; } = -2147483648;
        public string JsonObjects { get; set; } = string.Empty;

        public TimeZoneInfo TimeZoneInfo { get; set; }
        public Guid CurrencyGuid { get; set; } = Guid.Empty;
        public Guid UserGuid { get; set; } = Guid.Empty;
        public EntityReference TargetEntity { get; set; } = null;

        public int AllowSimultaneous { get; set; } = 962080001;
        public string ParserRemoteCalculationApiUrl { get; set; } = string.Empty;
        public string CreatorRemoteCalculationApiUrl { get; set; } = string.Empty;
        public List<string> OrderEntitiesList { get; set; } = new List<string>();

        public Dictionary<string, string> MappingImpl { get; set; } = new Dictionary<string, string>();

        public int GlobalErrorCount { get; set; } = 5;
    }

}
