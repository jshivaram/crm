﻿using Microsoft.Xrm.Sdk;
using static DataUploader.Helper.Enums;

namespace DataUploader.Interfaces
{
    public interface IBaseLogService
    {
        void AddMessage(string message = "", MessageType messageType = MessageType.INFORMATION, LogType logType = LogType.Log, bool onlySocket = true);

        void SaveLog(IOrganizationService orgService, bool isAddLog = true);
    }
}
