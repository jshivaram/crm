﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DataUploader.Model
{
    public class AutoNumberingManager
    {
        private static string _logicalName;
        private static string _autoNumberingEntityName = "ddsm_autonumbering";
        private static string _autoNumberingSearcAttrName = "ddsm_name";
        private static Entity _autoNumberingEntity = null;
        private static string newNumber = string.Empty;
        private static IOrganizationService _orgService;

        public static string GenAutoNumber(string logicalName, IOrganizationService orgService)
        {
            _logicalName = logicalName;
            _orgService = orgService;

            newNumber = GetNumber();
            UpdateCounter();

            return newNumber;
        }

        /// <summary>
        /// Prepare and return unique number of entity 
        /// </summary>
        /// <returns></returns>
        private static string GetNumber()
        {
            //Get auto numbering rules
            var autoNumberingMetaData = GetAutoNumberingRules();
            if (autoNumberingMetaData.Entities.Count == 0)
            {
                throw new InvalidPluginExecutionException("Auto Numbering Metadata does not exist.");
            }
            //Extract first record
            _autoNumberingEntity = autoNumberingMetaData.Entities[0];

            return GenerateNumber();
        }
        /// <summary>
        /// Generate unique number for entity by specific rules for each entity
        /// </summary>
        /// <param name="metaData"></param>
        /// <returns></returns>
        private static string GenerateNumber()
        {
            var number = "";
            try
            {
                //Get counter
                var counter = GetDataByField("ddsm_counter");
                //Get prefix
                var prefix = GetDataByField("ddsm_prefix");
                //Get length of number
                var length = Convert.ToInt32(GetDataByField("ddsm_length"));

                if (counter.Length < length)
                    number = counter.PadLeft(length, '0');
                else
                    number = counter;

                //Join prefix and counter
                number = prefix + number;

                //Replace specific character
                number = GetStringByPattern(number);
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.Message);
            }
            return number;
        }


        /// <summary>
        /// Get value by specific field
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private static string GetDataByField(string fieldName)
        {

            object value = null;
            try
            {
                if (_autoNumberingEntity.Attributes.TryGetValue(fieldName, out value))
                {
                    return value.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.Message);
            }
            return string.Empty;
        }

        /// <summary>
        /// Get rules for generate number
        /// </summary>
        /// <returns></returns>
        private static EntityCollection GetAutoNumberingRules()
        {
            // Construct query            
            var condition = new ConditionExpression
            {
                AttributeName = _autoNumberingSearcAttrName,
                Operator = ConditionOperator.Equal
            };
            condition.Values.Add(_logicalName);

            //Create a column set.
            var columns = new ColumnSet("ddsm_counter", "ddsm_length", "ddsm_prefix", "ddsm_increment");

            // Create query expression.
            var query = new QueryExpression
            {
                ColumnSet = columns,
                EntityName = _autoNumberingEntityName
            };
            query.Criteria.AddCondition(condition);

            var result = _orgService.RetrieveMultiple(query);
            return result;
        }

        /// <summary>
        /// Update Auto Numbering Entity to next value
        /// </summary>
        private static void UpdateCounter()
        {
            var counter = Convert.ToInt32(GetDataByField("ddsm_counter"));
            var increment = Convert.ToInt32(GetDataByField("ddsm_increment"));
            if (increment == int.MinValue)
            {
                increment = 1;
            }
            counter = counter + increment;
            _autoNumberingEntity["ddsm_counter"] = counter;
            _orgService.Update(_autoNumberingEntity);
        }

        /// <summary>
        /// Return string by Pattern
        /// </summary>
        /// <param name="value">input string</param>
        /// <param name="pattern"> default pattern is [^a-zA-Z0-9]</param>
        /// <returns></returns>
        private static string GetStringByPattern(string value, string pattern = "[^a-zA-Z0-9]")
        {
            var replacedString = Regex.Replace(value, pattern, "");
            return replacedString;

        }

        //Auto-Numbering
        public static string GetAutoNumber(string logicalName, IOrganizationService orgService)
        {
            return AutoNumberingManager.GenAutoNumber(logicalName, orgService);
        }

    }
}
