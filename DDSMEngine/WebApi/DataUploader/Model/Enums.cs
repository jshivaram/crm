﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataUploader.Model
{
    public class Enums
    {
        public enum MessageStatus
        {
            START,
            FINISH
        }

        public enum MessageType
        {
            SUCCESS,
            INFORMATION,
            ERROR
        }

        public enum AccountTypes
        {
            Business = 962080000,
            Residential = 962080002
        }
        public enum MilestoneStatus
        {
            NotStarted = 962080000,
            Active = 962080001,
            Completed = 962080002,
            Skipped = 962080003
        }

        public enum StatusXLSRecord
        {
            NotProcessed = 962080000,
            Processed = 962080001
        }

        public enum StatusFileDataUploading
        {
            notUploaded = 962080000,
            fileUploadCompleted = 962080001,
            fileUploadFailed = 962080002,
            parsingFile = 962080003,
            parsingFileCompleted = 962080004,
            parsingFileFailed = 962080005,
            contactsProcessing = 962080006,
            accountsProcessing = 962080007,
            sitesProcessing = 962080008,
            projectGroupsProcessing = 962080009,
            projectsProcessing = 962080010,
            measuresProcessing = 962080011,
            projectReportingsProcessing = 962080012,
            measureReportingsProcessing = 962080013,
            relationshipsProcessing = 962080014,
            recordsUploadCompleted = 962080015,
            recalculationStarted = 962080016,
            ESPRecalculation = 962080017,
            recalculationBusinessRules = 962080018,
            rollupsRecalculation = 962080019,
            intervalsRecalculation = 962080020,
            importSuccessfully = 962080021,
            importFailed = 962080022,
            recordsUploadFailed = 962080023,
            recordsDeleteStarted = 962080024,
            recordsDeleteCompleted = 962080025
        }

        public enum DataUploaderMethod
        {
            CreateRecord = 962080000,
            UpdateRecord = 962080001,
            DeleteRecord = 962080002,
            RecalcRecord = 962080003
        }

        public enum DataUploaderTypeRecord
        {
            Contact = 962080000,
            Account = 962080001,
            Site = 962080002,
            Project = 962080003,
            Measure = 962080004,
            ProjectGroup = 962080005,
            ProjectReporting = 962080006,
            MeasureReporting = 962080007,
            ExcelData = 962080008,
            ESPRecalculation = 962080009,
            recordsRecalculation = 962080010
        }

        public enum ParsedUploaded
        {
            Parsed = 962080000,
            Uploaded = 962080001
        }

        public enum TypeProjectProgram
        {
            ARET = 962080000,
            RDI = 962080001,
            RPC = 962080002
        }

        public enum CreatorRecordType
        {
            UnassignedValue = -2147483648,
            Front = 962080000,
            DataUploader = 962080001,
            Portal = 962080002,
            PrepTool = 962080003
        }


    }
}
