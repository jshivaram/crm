﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataUploader.Model
{
    public class PrimaryKey
    {
        //Generation Primery Key
        //Contact
        public static string GetContactPrimaryKey(string lastName, string firstName, string mainPhone)
        {
            var result = "";
            var lastNameKey = lastName.prepareKey(4, 'L');
            var firstNameKey = firstName.prepareKey(3, 'F');
            var mainPhoneKey = mainPhone.prepareMainPhoneKey();
            result = (lastNameKey + firstNameKey + mainPhoneKey);

            return result.ToLower();
        }

        //Account
        public static string GetResidentialAccPrimaryKey(string lastName, string firstName, string mainPhone)
        {
            var result = "";

            var lastNameKey = lastName.prepareKey(4, 'L');
            var firstNameKey = firstName.prepareKey(3, 'F');
            var mainPhoneKey = mainPhone.prepareMainPhoneKey();

            result = lastNameKey + firstNameKey + mainPhoneKey;

            return result.ToLower();
        }
        public static string GetBusinessAccPrimaryKey(string companyName, string addressComposite)
        {
            var accountPrimaryKey = "";
            int companyNameLenght = 15;

            //parse company name
            companyName = companyName.GetStringByPattern("[^a-zA-Z0-9]+");

            //First 15 characters of Company Name
            if (companyName.Length > companyNameLenght)
            {
                companyName = companyName.Substring(0, companyNameLenght);
            }

            //parse address
            addressComposite = addressComposite.GetStringByPattern("[^a-zA-Z0-9]+");

            //Account Primary Key
            accountPrimaryKey = companyName + addressComposite;

            return accountPrimaryKey.ToLower();
        }

        //Site
        public static string GetSitePrimaryKey(string _address1, string _address2, string _city, bool parentSite)
        {
            _address1 = string.IsNullOrEmpty(_address1) ? _address1 : _address1.GetStringByPattern();
            _address2 = string.IsNullOrEmpty(_address2) ? _address2 : _address2.GetStringByPattern();
            _city = string.IsNullOrEmpty(_city) ? _city : _city.GetStringByPattern();
            /*
            if(parentSite)
                return (_address1 + _city).ToLower();
            */
            return (_address1 + _address2 + _city).ToLower();
        }
        //Site Name
        public static string CreateSiteName(string _address1, string _address2, string _city, string _parentAccountName)
        {
            var delimer = "";
            var siteName = "";
            //_address1 = string.IsNullOrEmpty(_address1) ? _address1 : _address1.GetStringByPattern();
            //_address2 = string.IsNullOrEmpty(_address2) ? _address2 : _address2.GetStringByPattern();
            //_city = string.IsNullOrEmpty(_city) ? _city : _city.GetStringByPattern();

            var parentAccountName = _parentAccountName;
            if (_parentAccountName.Length > 15)
            {
                parentAccountName = _parentAccountName.Substring(0, 15);
            }
            if (!string.IsNullOrEmpty(_address1) || !string.IsNullOrEmpty(_address2) || !string.IsNullOrEmpty(_city))
            {
                delimer = "-";
            }
            siteName = _address1 + " " + _address2 + " " + _city + delimer + parentAccountName;
            siteName = siteName.Trim();

            return siteName;
        }

    }
}
