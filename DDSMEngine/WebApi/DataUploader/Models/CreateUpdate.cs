﻿using DataUploader.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Concurrent;
using static DataUploader.Helper.Enums;
using DataUploader.Interfaces;
using JsonConvert = CoreUtils.Wrap.JsonConvert;
using CoreUtils.Service.Interfaces.DataBase;
using CoreUtils.Service.Implementation.DataBase;

namespace DataUploader.Extentions
{
    public class CreateUpdate : IBaseCreateUpdate
    {
        private readonly IOrganizationService _orgService;
        private readonly string _sheetName;
        private readonly string _logicalName;
        private readonly bool _callExecuteMultiple = true;
        private readonly IDataBaseService _dbService;

        public CreateUpdate(IOrganizationService orgService, string sheetName, string logicalName, bool callExecuteMultiple = true)
        {
            _orgService = orgService;
            _sheetName = sheetName;
            _logicalName = logicalName;
            _callExecuteMultiple = callExecuteMultiple;
            _dbService = new DataBaseService(_orgService);
        }

        public void CreateUpdateEntityRecords(ExecuteMultipleRequest emRequest, List<string> setUniqueId, ConcurrentDictionary<string, CreatedEntity> dictionaryEntities, ConcurrentDictionary<string, CreatedEntity> requestEntities, StatisticRecords statisticRecords, LogService logService, int totalEntitiesCount = 0)
        {
            int indexReq = -1;

            string logicalName = (emRequest.Requests[0].RequestName == "Update") ? ((UpdateRequest)emRequest.Requests[0]).Target.LogicalName : ((CreateRequest)emRequest.Requests[0]).Target.LogicalName;

            Int32 countRecords = emRequest.Requests.Count;

            logService.AddMessage("*** Start create/update of " + Convert.ToString(countRecords) + " Entity Records: " + logicalName, MessageType.DEBUG);

            if (!_callExecuteMultiple)
            {
                List<Entity> createEntityList = new List<Entity>();
                List<Entity> updateEntityList = new List<Entity>();
                List<string> createUniqueId = new List<string>();
                List<string> updateUniqueId = new List<string>();
                try
                {

                    foreach (var req in emRequest.Requests)
                    {
                        indexReq++;

                        if (req.RequestName == "Update")
                        {
                            updateEntityList.Add(((UpdateRequest)req).Target);
                            updateUniqueId.Add(setUniqueId[indexReq]);
                        }
                        else if (req.RequestName == "Create")
                        {
                            createEntityList.Add(((CreateRequest)req).Target);
                            createUniqueId.Add(setUniqueId[indexReq]);
                        }

                    }

                    if (createEntityList.Count > 0)
                    {
                        var enList = _dbService.CreateEntities(createEntityList, totalEntitiesCount);
                        foreach (var oEn in enList)
                        {
                            statisticRecords.Created++;
                            dictionaryEntities[createUniqueId[oEn.Key]].EntityGuid = oEn.Value;

                            dictionaryEntities[createUniqueId[oEn.Key]].Status = true;
                            CreatedEntity recordCreate = dictionaryEntities[createUniqueId[oEn.Key]];
                            requestEntities.AddOrUpdate(createUniqueId[oEn.Key], recordCreate, (oldkey, oldvalue) => recordCreate);
                        }

                        logService.AddMessage("*** End the update of " + Convert.ToString(enList.Count) + " Entity Records: " + logicalName, MessageType.DEBUG);

                    }

                    if (updateEntityList.Count > 0)
                    {
                        _dbService.UpdateEntities(updateEntityList);
                        for (var i = 0; i < updateEntityList.Count; i++)
                        {
                            statisticRecords.Updated++;
                            dictionaryEntities[updateUniqueId[i]].Status = true;
                            CreatedEntity recordCreate = dictionaryEntities[updateUniqueId[i]];
                            requestEntities.AddOrUpdate(updateUniqueId[i], recordCreate, (oldkey, oldvalue) => recordCreate);
                        }
                        logService.AddMessage("*** End the update of " + Convert.ToString(updateEntityList.Count) + " Entity Records: " + logicalName, MessageType.DEBUG);
                    }
                }
                catch (Exception e)
                {
                    //LogService.AddMessage("Error > " + _sheetName + ": " + " Excel Unique GUID: " + setUniqueId[_indexReq] + "; Request: " + req.RequestName + "; Index: " + (_indexReq + 1) + "; Message: " + e.Message, MessageType.FATAL);
                }

            }
            else
            {
                //ExecuteMultiple
                ExecuteMultipleResponse emResponse = (ExecuteMultipleResponse)_orgService.Execute(emRequest);
                foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                {
                    if (responseItem.Response != null)
                    {
                        dictionaryEntities[setUniqueId[responseItem.RequestIndex]].Status = true;

                        if ((responseItem.Response).Results.ContainsKey("id"))
                        {
                            statisticRecords.Created++;
                            dictionaryEntities[setUniqueId[responseItem.RequestIndex]].EntityGuid = new Guid(((OrganizationResponse)responseItem.Response).Results["id"].ToString());
                        }
                        else
                        {
                            statisticRecords.Updated++;
                        }

                        CreatedEntity recordCreate = dictionaryEntities[setUniqueId[responseItem.RequestIndex]];
                        requestEntities.AddOrUpdate(setUniqueId[responseItem.RequestIndex], recordCreate, (oldkey, oldvalue) => recordCreate);
                    }

                    // An error has occurred.
                    else if (responseItem.Fault != null)
                    {
                        logService.AddMessage("Error > " + _sheetName + ": " + " Excel Unique GUID: " + setUniqueId[responseItem.RequestIndex] + "; Request: " + emRequest.Requests[responseItem.RequestIndex].RequestName + "; Index: " + (responseItem.RequestIndex + 1) + "; Message: " + responseItem.Fault.Message + "; TraceText: " + responseItem.Fault.TraceText, MessageType.FATAL);
                    }
                }
            }
            //LogService.AddMessage("Completed creation of " + Convert.ToString(countRecords) + " Entity Records: " + logicalName, MessageType.SUCCESS);
        }

        //Upadate Source Records
        public void UpdateSourceRecords(ConcurrentDictionary<string, CreatedEntity> dictionaryEntities, LogService logService)

        {
            List<Entity> updateEntityList = new List<Entity>();

            var updateRecords = dictionaryEntities.Where(x => x.Value.Status == true && x.Value.StatusDb != true).ToDictionary(x => x.Key, x => x.Value);

            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = false
                }
            };

            foreach (var key in updateRecords.Keys)
            {
                if (updateRecords[key].EntityGuid != null && updateRecords[key].EntityGuid != new Guid())
                    if (updateRecords[key].SourceId != null && updateRecords[key].SourceId != new Guid())
                    {
                        Entity updateEntity = new Entity("ddsm_exceldata", updateRecords[key].SourceId);
                        updateEntity["ddsm_processed"] = true;

                        Dictionary<string, CreatedEntity> tmpDictionaryEntity = new Dictionary<string, CreatedEntity>();
                        tmpDictionaryEntity.Add(key, updateRecords[key]);
                        string jsonObject = JsonConvert.SerializeObject(tmpDictionaryEntity);
                        updateEntity["ddsm_entityobjectdictionary"] = jsonObject;

                        updateEntity["regardingobjectid"] = new EntityReference(_logicalName, updateRecords[key].EntityGuid);

                        updateEntity["statecode"] = new OptionSetValue(1);
                        updateEntity["statuscode"] = new OptionSetValue(2);

                        updateEntityList.Add(updateEntity);

                        UpdateRequest updateRequest = new UpdateRequest();
                        updateRequest.Target = updateEntity;
                        emRequest.Requests.Add(updateRequest);

                    }
            }

            if (emRequest.Requests.Count > 0)
            {
                if (!_callExecuteMultiple)
                {

                        try
                        {
                            _dbService.UpdateEntities(updateEntityList);
                        }
                        catch (Exception e)
                        {
                        }

                }
                else
                {
                    ExecuteMultipleResponse emResponse = (ExecuteMultipleResponse)_orgService.Execute(emRequest);

                    foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                    {
                        if (responseItem.Response != null)
                        {
                            if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                            {
                            }
                        }

                        else if (responseItem.Fault != null)
                        {
                        }
                    }

                }
            }


        }

    }
}
