﻿using System;
using System.Linq;
using DataUploader.Interfaces;
using DataUploader.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;
using System.Collections.Concurrent;
using static DataUploader.Helper.Enums;
using EntityJson = CoreUtils.DataUploader.Dto.EntityJson;

namespace DataUploader.Extentions
{
    public class GetData: IBaseGetData
    {
        private readonly IOrganizationService _orgService;

        public GetData(IOrganizationService orgService)
        {
            _orgService = orgService;
        }

        public Entity GetDataRelationMapping(string targetEntity, string sourceEntity, Guid sourceGuid)
        {
            InitializeFromRequest initialize = new InitializeFromRequest();
            initialize.TargetEntityName = targetEntity;
            initialize.EntityMoniker = new EntityReference(sourceEntity, sourceGuid);
            initialize.TargetFieldType = TargetFieldType.All;
            InitializeFromResponse initialized = (InitializeFromResponse)_orgService.Execute(initialize);
            return initialized.Entity;
        }

        public Entity GetEntityAttrsValue(Guid recordGuid, string entityName, ColumnSet columns)
        {
            Entity attrCollection = _orgService.Retrieve(entityName, recordGuid, columns);
            return attrCollection;
        }

        public EntityCollection GetDataByEntityName(string entityName, ConcurrentDictionary<string, EntityJson> objects, Guid recordUploaded, bool statusRecord, LogService logService)
        {
            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == entityName).Key;

            if (keyEntity != null)
            {
                EntityCollection retriveCollection = new EntityCollection();

                try
                {
                    QueryExpression query = new QueryExpression();
                    query.EntityName = "ddsm_exceldata";
                    query.ColumnSet = new ColumnSet(allColumns: true);
                    query.Criteria = new FilterExpression(LogicalOperator.And);
                    query.Criteria.AddCondition(new ConditionExpression("subject", ConditionOperator.Equal, keyEntity));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_processed", ConditionOperator.Equal, statusRecord));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_datauploader", ConditionOperator.Equal, recordUploaded));
                    if (!statusRecord)
                    {
                        query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
                    }

                    int pageNumber = 1;
                    RetrieveMultipleRequest multiRequest;
                    RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();

                    do
                    {
                        query.PageInfo.Count = 5000;
                        query.PageInfo.PagingCookie = (pageNumber == 1) ? null : multiResponse.EntityCollection.PagingCookie;
                        query.PageInfo.PageNumber = pageNumber++;

                        multiRequest = new RetrieveMultipleRequest();
                        multiRequest.Query = query;
                        multiResponse = (RetrieveMultipleResponse)_orgService.Execute(multiRequest);

                        retriveCollection.Entities.AddRange(multiResponse.EntityCollection.Entities);
                    }
                    while (multiResponse.EntityCollection.MoreRecords);

                    return (retriveCollection.Entities.Count > 0) ? retriveCollection : null;
                }
                catch (Exception e)
                {
                    logService.AddMessage("Error > Method getDataByEntityName; Sheet Name: " + keyEntity + "; Message: " + e.Message, MessageType.FATAL);
                }

            }

            return null;
        }

        public PaginationRetriveMultiple GetDataByEntityName(string entityName, ConcurrentDictionary<string, EntityJson> objects, Guid recordUploaded, bool statusRecord, LogService logService, int pageNumber, dynamic pagingCookie, bool moreRecords, Int32 recordCount)
        {
            var keyEntity = objects.FirstOrDefault(x => x.Value.Name.ToLower() == entityName).Key;

            PaginationRetriveMultiple paginationRetriveMultiple = new PaginationRetriveMultiple();
            paginationRetriveMultiple.RetrieveCollection = new EntityCollection();
            paginationRetriveMultiple.PageNumber = pageNumber;
            paginationRetriveMultiple.PagingCookie = pagingCookie;
            paginationRetriveMultiple.MoreRecords = moreRecords;

            if (keyEntity != null)
            {

                try
                {
                    QueryExpression query = new QueryExpression();
                    query.EntityName = "ddsm_exceldata";
                    query.ColumnSet = new ColumnSet(allColumns: true);
                    query.Criteria = new FilterExpression(LogicalOperator.And);
                    query.Criteria.AddCondition(new ConditionExpression("subject", ConditionOperator.Equal, keyEntity));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_processed", ConditionOperator.Equal, statusRecord));
                    query.Criteria.AddCondition(new ConditionExpression("ddsm_datauploader", ConditionOperator.Equal, recordUploaded));
                    if (!statusRecord)
                    {
                        query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
                    } else {
                    }

                    RetrieveMultipleRequest multiRequest;
                    RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();

                    query.PageInfo.Count = recordCount;
                    query.PageInfo.PagingCookie = (pageNumber == 1) ? null : pagingCookie;
                    query.PageInfo.PageNumber = pageNumber++;

                    multiRequest = new RetrieveMultipleRequest();
                    multiRequest.Query = query;
                    multiResponse = (RetrieveMultipleResponse)_orgService.Execute(multiRequest);

                    paginationRetriveMultiple.PagingCookie = multiResponse.EntityCollection.PagingCookie;
                    paginationRetriveMultiple.PageNumber = pageNumber;
                    paginationRetriveMultiple.MoreRecords = multiResponse.EntityCollection.MoreRecords;

                    paginationRetriveMultiple.RetrieveCollection.Entities.AddRange(multiResponse.EntityCollection.Entities);

                    return paginationRetriveMultiple;

                }
                catch (Exception e)
                {
                    logService.AddMessage("Error > Method getDataByEntityName; Sheet Name: " + keyEntity + "; Message: " + e.Message, MessageType.FATAL);

                }

            }

            return null;
        }

        public string VerifyDuplicateRecord(Entity entityObj, string entityName, string sheetName, string uniqueId, LogService logService)
        {
            string responseEntityId = string.Empty;
            string responseEntityName = string.Empty;
            RetrieveDuplicatesRequest req = new RetrieveDuplicatesRequest();
            req.BusinessEntity = entityObj;
            req.MatchingEntityName = entityName;
            req.PagingInfo = new PagingInfo() { PageNumber = 1, Count = 50 };
            try
            {
                RetrieveDuplicatesResponse resp = (RetrieveDuplicatesResponse)_orgService.Execute(req);
                foreach (Entity respEntity in resp.DuplicateCollection.Entities)
                {
                    if (entityName.IndexOf("ddsm_") != -1)
                    {
                        responseEntityId = respEntity.Id.ToString() + "::" + ((respEntity.Attributes.ContainsKey("ddsm_name")) ? respEntity["ddsm_name"].ToString() : "");
                    }
                    else
                    if (entityName.IndexOf("contact") != -1)
                    {
                        responseEntityId = respEntity.Id.ToString() + "::" + ((respEntity.Attributes.ContainsKey("fullname")) ? respEntity["fullname"].ToString() : "");
                    }
                    else
                    {
                        responseEntityId = respEntity.Id.ToString() + "::" + ((respEntity.Attributes.ContainsKey("name")) ? respEntity["name"].ToString() : "");
                    }
                    //ResponseEntityName = ResponseEntity["name"].ToString();
                    //Console.WriteLine(ResponseEntity.Id.ToString() + " - " + ResponseEntity["name"]);
                    break;
                }
                return responseEntityId;
            }
            catch (Exception e)
            {
                logService.AddMessage("Error > Method VerifyDuplicateRecord; Sheet Name: " + sheetName + "; " + " Excel Unique GUID: " + uniqueId + "; Message: " + e.Message, MessageType.FATAL);

                return string.Empty;
            }
        }

        public bool ComparingAttributeValues(object oAttribute, object oAttribute2)
        {
            bool compared = false;

            try
            {
                if (oAttribute.GetType().Equals(typeof(Microsoft.Xrm.Sdk.OptionSetValue))
                    && oAttribute2.GetType().Equals(typeof(Microsoft.Xrm.Sdk.OptionSetValue))
                    && !string.IsNullOrEmpty(Convert.ToString((oAttribute as Microsoft.Xrm.Sdk.OptionSetValue).Value))
                    && !string.IsNullOrEmpty(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.OptionSetValue).Value))
                    && Int32.Parse(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.OptionSetValue).Value)) != Int32.Parse(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.OptionSetValue).Value)))
                {
                    compared = true;
                }
                else if (oAttribute.GetType().Equals(typeof(Microsoft.Xrm.Sdk.Money))
                  && oAttribute2.GetType().Equals(typeof(Microsoft.Xrm.Sdk.Money))
                  && !string.IsNullOrEmpty(Convert.ToString((oAttribute as Microsoft.Xrm.Sdk.Money).Value))
                  && !string.IsNullOrEmpty(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.Money).Value))
                  && Decimal.Parse(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.Money).Value)) != Decimal.Parse(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.Money).Value)))
                {
                    compared = true;
                }
                else if (oAttribute.GetType().Equals(typeof(Microsoft.Xrm.Sdk.EntityReference))
                  && oAttribute2.GetType().Equals(typeof(Microsoft.Xrm.Sdk.EntityReference))
                  && !string.IsNullOrEmpty(Convert.ToString((oAttribute as Microsoft.Xrm.Sdk.EntityReference).Id))
                  && !string.IsNullOrEmpty(Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.EntityReference).Id))
                  && Convert.ToString((oAttribute as Microsoft.Xrm.Sdk.EntityReference).Id) != Convert.ToString((oAttribute2 as Microsoft.Xrm.Sdk.EntityReference).Id))
                {
                    compared = true;
                }
                else if (oAttribute.GetType().Equals(typeof(System.Int32))
                  && oAttribute2.GetType().Equals(typeof(System.Int32)))
                {
                    if (Convert.ToInt32(oAttribute) != Convert.ToInt32(oAttribute2))
                    {
                        compared = true;
                    }
                }
                else if (oAttribute.GetType().Equals(typeof(System.Decimal))
                  && oAttribute2.GetType().Equals(typeof(System.Decimal)))
                {
                    if (Convert.ToDecimal(oAttribute) != Convert.ToDecimal(oAttribute2))
                    {
                        compared = true;
                    }
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(oAttribute))
                  && !string.IsNullOrEmpty(Convert.ToString(oAttribute2))
                  && Convert.ToString(oAttribute) != Convert.ToString(oAttribute2))
                {
                    compared = true;
                }
            }
            catch (Exception e)
            {
                compared = false;
            }

            return compared;
        }

    }
}
