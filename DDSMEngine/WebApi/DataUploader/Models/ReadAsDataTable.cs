﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using static DataUploader.Helper.Enums;
using static DataUploader.Helper.Errors;
using DataUploader.Helper;
using JsonConvert = CoreUtils.Wrap.JsonConvert;
using EntityJson = CoreUtils.DataUploader.Dto.EntityJson;
using Microsoft.Xrm.Sdk;
using System.Data;
using Microsoft.Xrm.Sdk.Messages;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;

namespace DataUploader.Extentions
{
    public class OutputReadAsDt
    {
        public Int64 TableParsingErrors { get; set; } = 0;
        public LogService LogService { get; set; }

    }

    public class ReadAsDataTable: IDisposable
    {
        private readonly IOrganizationService _orgService;
        private readonly DataUploaderSettings _thisSettings;
        private readonly ConcurrentDictionary<string, EntityJson> _configObject;
        private readonly DataTable _dt;
        private readonly string _dtName;
        private readonly SpreadsheetDocument _spreadSheetDocument;
        private readonly WorksheetPart _worksheetPart;
        private readonly LogService _logService;
        private readonly OutputReadAsDt _outputReadAsDt;
        private bool _disposed = false;
        private readonly int _limitItemColletcion;

        public ReadAsDataTable(IOrganizationService orgService, DataUploaderSettings thisSettings, ConcurrentDictionary<string, EntityJson> configObject, DataTable dt, string dtName)
        {
            _orgService = orgService;
            _dt = dt;
            _thisSettings = thisSettings;
            _configObject = configObject;
            _dtName = dtName;
            _logService = new LogService(_thisSettings);
            _outputReadAsDt = new OutputReadAsDt();
            _limitItemColletcion = _thisSettings.GlobalRequestsCount;

        }

        public ReadAsDataTable(IOrganizationService orgService, DataUploaderSettings thisSettings, ConcurrentDictionary<string, EntityJson> configObject, SpreadsheetDocument spreadSheetDocument, WorksheetPart worksheetPart, string dtName)
        {
            _orgService = orgService;
            _thisSettings = thisSettings;
            _configObject = configObject;
            _dtName = dtName;
            _spreadSheetDocument = spreadSheetDocument;
            _worksheetPart = worksheetPart;
            _logService = new LogService(_thisSettings);
            _outputReadAsDt = new OutputReadAsDt();
            _limitItemColletcion = _thisSettings.GlobalRequestsCount * 5;
        }

        public OutputReadAsDt Xls()
        {
            _outputReadAsDt.TableParsingErrors = 0;

            ParsedRecords pRecords = new ParsedRecords();

            List<string> loggerErrors = new List<string>();

            string sheetJsonName = _configObject[_dtName].Name.ToLower();
            ConcurrentDictionary<string, EntityJson> currentObject = new ConcurrentDictionary<string, EntityJson>();
            currentObject.AddOrUpdate(_dtName, _configObject[_dtName], (oldkey, oldvalue) => _configObject[_dtName]);

            Guid recordUploaded = _thisSettings.TargetEntity.Id;
            List<string> collectionUniqueGuids = new List<string>();
            List<string> collectionNotUniqueGuids = new List<string>();

            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };

            Int64 rowNum = 0, itemsRequestCount = 0, requestSets = 1;

            string uniqueIdColumnKey = _configObject[_dtName].Attributes.FirstOrDefault(x => x.Value.ColumnName.ToLower() == (_dtName + " GUID").ToLower()).Key;

            foreach (DataRow dr in _dt.Rows)
            {
                rowNum++;
                if (rowNum > _thisSettings.StartRowSheet)
                {
                    Entity newEntity = new Entity("ddsm_exceldata");
                    newEntity["subject"] = _dtName;
                    newEntity["ddsm_processed"] = false;
                    newEntity["ddsm_datauploader"] = new EntityReference("ddsm_datauploader", recordUploaded);
                    newEntity["ddsm_logicalname"] = sheetJsonName.ToLower();
                    newEntity["ddsm_jsondata"] = JsonConvert.SerializeObject(currentObject);

                    if (!string.IsNullOrEmpty(uniqueIdColumnKey))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dr[uniqueIdColumnKey])))
                        {
                            if (collectionUniqueGuids.Contains(Convert.ToString(dr[uniqueIdColumnKey])))
                            {
                                if (!collectionNotUniqueGuids.Contains(Convert.ToString(dr[uniqueIdColumnKey])))
                                {
                                    collectionNotUniqueGuids.Add(Convert.ToString(dr[uniqueIdColumnKey]));
                                }
                            }
                            else
                            {
                                collectionUniqueGuids.Add(Convert.ToString(dr[uniqueIdColumnKey]));
                            }
                        }
                    }

                    foreach (var attr in _configObject[_dtName].Attributes)
                    {
                        if (dr[attr.Key].ToString() != "")
                        {
                            newEntity["ddsm_" + (attr.Key).ToLower()] = Convert.ToString(dr[attr.Key]).Trim();
                            if (attr.Value.AttrType.ToString() == "DateTime")
                                newEntity["ddsm_" + (attr.Key).ToLower()] = DataUploader.ParseAttributToDateTime(dr[attr.Key]);
                        }
                    }

                    CreateRequest createRequest = new CreateRequest();
                    createRequest.Target = newEntity;
                    emRequest.Requests.Add(createRequest);
                    itemsRequestCount++;

                    //Create Multiple
                    if (itemsRequestCount == (requestSets * _limitItemColletcion))
                    {
                        GenerationEntity(emRequest, pRecords, loggerErrors, _thisSettings.CallExecuteMultiple);

                        requestSets++;
                        emRequest = new ExecuteMultipleRequest
                        {
                            Requests = new OrganizationRequestCollection(),
                            Settings = new ExecuteMultipleSettings
                            {
                                ContinueOnError = true,
                                ReturnResponses = true
                            }
                        };

                    }

                }
            }

            //Create Multiple
            if (emRequest.Requests.Count > 0)
            {
                GenerationEntity(emRequest, pRecords, loggerErrors, _thisSettings.CallExecuteMultiple);

                requestSets++;
                emRequest = new ExecuteMultipleRequest
                {
                    Requests = new OrganizationRequestCollection(),
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = true,
                        ReturnResponses = true
                    }
                };
            }

            pRecords.RequestCount += itemsRequestCount;

            if (collectionNotUniqueGuids.Count > 0)
            {
                _logService.AddMessage(GetTextError(380010) + string.Join(", ", collectionNotUniqueGuids) + " not unique values in sheet " + _dtName + " list", MessageType.ERROR, LogType.Log, false);
                _outputReadAsDt.TableParsingErrors += collectionNotUniqueGuids.Count;
            }
            if (loggerErrors.Count > 0)
            {
                for (var i = 0; i < loggerErrors.Count; i++)
                {
                    _logService.AddMessage(loggerErrors[i], MessageType.ERROR, LogType.Log, false);
                }
            }

            _logService.AddMessage(GetTextError(380006) + "'" + _dtName + "'; " + " " + GetTextError(380055) + pRecords.RequestCount + "; " + GetTextError(380056) + pRecords.ResponceCount + "; " + GetTextError(380057) + pRecords.ExcludedDataCount + ";", MessageType.INFORMATION, LogType.Log, false);
            _outputReadAsDt.LogService = _logService;

            return _outputReadAsDt;
        }

        public OutputReadAsDt Xlsx()
        {
            _outputReadAsDt.TableParsingErrors = 0;

            ParsedRecords pRecords = new ParsedRecords();

            List<string> loggerErrors = new List<string>();

            ConcurrentDictionary<string, EntityJson> currentObject = new ConcurrentDictionary<string, EntityJson>();
            currentObject.AddOrUpdate(_dtName, _configObject[_dtName], (oldkey, oldvalue) => _configObject[_dtName]);
            string sheetJsonName = _configObject[_dtName].Name.ToLower();

            Guid recordUploaded = _thisSettings.TargetEntity.Id;
            List<string> collectionUniqueGuids = new List<string>();
            List<string> collectionNotUniqueGuids = new List<string>();

            OpenXmlReader reader = OpenXmlReader.Create(_worksheetPart);

            Int64 rowNum = 0, itemsRequestCount = 0, requestSets = 1;

            string uniqueIdColumnKey = _configObject[_dtName].Attributes.FirstOrDefault(x => x.Value.ColumnName.ToLower() == (_dtName + " GUID").ToLower()).Key;

            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };

            while (reader.Read())
            {
                if (reader.ElementType == typeof(Row))
                {
                    if (reader.HasAttributes)
                        rowNum = Convert.ToInt32(reader.Attributes.First(a => a.LocalName == "r").Value);

                    if (rowNum > _thisSettings.StartRowSheet)
                    {

                        reader.ReadFirstChild();

                        Entity newEntity = new Entity("ddsm_exceldata");
                        newEntity["subject"] = _dtName;
                        newEntity["ddsm_logicalname"] = sheetJsonName.ToLower();
                        newEntity["ddsm_processed"] = false;
                        newEntity["ddsm_datauploader"] = new EntityReference("ddsm_datauploader", recordUploaded);
                        newEntity["ddsm_jsondata"] = JsonConvert.SerializeObject(currentObject);

                        do
                        {
                            if (reader.ElementType == typeof(Cell))
                            {
                                Cell cell = (Cell)reader.LoadCurrentElement();

                                var column = GetColumnName(cell.CellReference);
                                var attr = _configObject[_dtName].Attributes.FirstOrDefault(x => x.Key.ToLower() == column.ToLower());
                                var typeColmn = attr.Value.AttrType;

                                //GUID
                                if (!string.IsNullOrEmpty(uniqueIdColumnKey) && uniqueIdColumnKey == column)
                                {
                                    string uniqueGuidValue = newEntity.GetAttributeValue<string>("ddsm_" + column.ToLower());

                                    if (!string.IsNullOrEmpty(uniqueGuidValue))
                                    {
                                        if (collectionUniqueGuids.Contains(uniqueGuidValue))
                                        {
                                            if (!collectionNotUniqueGuids.Contains(uniqueGuidValue))
                                            {
                                                collectionNotUniqueGuids.Add(uniqueGuidValue);
                                            }
                                        }
                                        else
                                        {
                                            collectionUniqueGuids.Add(uniqueGuidValue);
                                        }
                                    }
                                }

                                newEntity["ddsm_" + column.ToLower()] = Convert.ToString(GetCellValue(_spreadSheetDocument, cell, false)).Trim();
                                if (typeColmn.ToString() == "DateTime")
                                    newEntity["ddsm_" + column.ToLower()] = DataUploader.ParseAttributToDateTime(Convert.ToString(GetCellValue(_spreadSheetDocument, cell, true)).Trim());

                            }

                        } while (reader.ReadNextSibling());

                        CreateRequest createRequest = new CreateRequest();
                        createRequest.Target = newEntity;
                        emRequest.Requests.Add(createRequest);
                        itemsRequestCount++;

                        //Create Multiple
                        if (itemsRequestCount == (requestSets * _limitItemColletcion))
                        {

                            GenerationEntity(emRequest, pRecords, loggerErrors, _thisSettings.CallExecuteMultiple);

                            requestSets++;
                            emRequest = new ExecuteMultipleRequest
                            {
                                Requests = new OrganizationRequestCollection(),
                                Settings = new ExecuteMultipleSettings
                                {
                                    ContinueOnError = true,
                                    ReturnResponses = true
                                }
                            };
                        }
                    }
                }
            }

            //Create Multiple
            if (emRequest.Requests.Count > 0)
            {
                GenerationEntity(emRequest, pRecords, loggerErrors, _thisSettings.CallExecuteMultiple);

                requestSets++;
                emRequest = new ExecuteMultipleRequest
                {
                    Requests = new OrganizationRequestCollection(),
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = true,
                        ReturnResponses = true
                    }
                };
            }

            //reader.Close();

            pRecords.RequestCount += itemsRequestCount;

            if (collectionNotUniqueGuids.Count > 0)
            {
                _logService.AddMessage(GetTextError(380010) + string.Join(", ", collectionNotUniqueGuids) + " not unique values in sheet " + _dtName + " list", MessageType.ERROR, LogType.Log, false);
                _outputReadAsDt.TableParsingErrors += collectionNotUniqueGuids.Count;
            }
            if (loggerErrors.Count > 0)
            {
                for (var i = 0; i < loggerErrors.Count; i++)
                {
                    _logService.AddMessage(loggerErrors[i], MessageType.ERROR, LogType.Log, false);
                }
            }

            _logService.AddMessage(GetTextError(380006) + "'" + _dtName + "'; " + " " + GetTextError(380055) + pRecords.RequestCount + "; " + GetTextError(380056) + pRecords.ResponceCount + "; " + GetTextError(380057) + pRecords.ExcludedDataCount + ";", MessageType.INFORMATION, LogType.Log, false);
            _outputReadAsDt.LogService = _logService;

            return _outputReadAsDt;
        }

        private string GetCellValue(SpreadsheetDocument spreadSheetDocument, Cell cell, bool isDate)
        {
            SharedStringTablePart stringTablePart = spreadSheetDocument.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;

            try
            {
                if (cell.DataType == null)
                {
                    value = GetCellValueWithoutConsideringDataType(spreadSheetDocument, cell, isDate);
                }

                if (cell.DataType != null && cell.DataType.HasValue)
                {
                    switch (cell.DataType.Value)
                    {
                        case CellValues.SharedString:
                            value = (isDate)?ConverToDateValue(stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText): stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                            break;
                        case CellValues.Boolean:
                            switch (value)
                            {
                                case "0":
                                    value = "FALSE";
                                    break;
                                default:
                                    value = "TRUE";
                                    break;
                            }
                            break;
                    }
                }
            }
            catch (ArgumentOutOfRangeException e)
            {
                value = null;
            }

            return value;
        }

        private string GetColumnName(string cellName)
        {
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellName);

            return match.Value;
        }

        private string GetFormatedValue(SpreadsheetDocument spreadSheetDocument, Cell cell, CellFormat cellformat, bool isDate)
        {
            string value = cell.InnerText;

            //Temporarily disabling the logic conversion value according to the format of the cell
            /*
            var NumberingFormats = spreadSheetDocument.WorkbookPart.WorkbookStylesPart.Stylesheet.NumberingFormats;
            if (cellformat.NumberFormatId != null && cellformat.NumberFormatId != 0) // 0 - General format
            {
                if (NumberingFormats != null)
                {
                    string format = spreadSheetDocument.WorkbookPart.WorkbookStylesPart.Stylesheet.NumberingFormats.Elements<NumberingFormat>()
                        .Where(i => i.NumberFormatId.Value == cellformat.NumberFormatId.Value)
                        .First().FormatCode;
                    //double number = double.Parse(cell.InnerText);
                    //value = number.ToString(format);
                }
                else
                {
                    var NewNumberingFormats = BuildFormatMappingsFromXlsx(spreadSheetDocument);
                }
            }
            */

            if (isDate)
            {
                value = ConverToDateValue(value);
            }

            return value;
        }

        private string GetCellValueWithoutConsideringDataType(SpreadsheetDocument spreadSheetDocument, Cell cell, bool isDate)
        {
            CellFormat cellFormat = GetCellFormat(spreadSheetDocument, cell);
            if (cellFormat != null)
            {
                return GetFormatedValue(spreadSheetDocument, cell, cellFormat, isDate);
            }
            else
            {
                if (isDate)
                {
                    return ConverToDateValue(cell.InnerText);
                }
                else
                {
                    return cell.InnerText;
                }
            }
        }

        private CellFormat GetCellFormat(SpreadsheetDocument spreadSheetDocument, Cell cell)
        {
            WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
            if (cell.StyleIndex != null && cell.StyleIndex.HasValue)
            {
                int styleIndex = (int)cell.StyleIndex.Value;
                CellFormat cellFormat = (CellFormat)workbookPart.WorkbookStylesPart.Stylesheet.CellFormats.ElementAt(styleIndex);
                return cellFormat;
            }
            else
            {
                return null;
            }
        }

        private Dictionary<uint, String> BuildFormatMappingsFromXlsx(SpreadsheetDocument spreadSheetDocument)
        {
            Dictionary<uint, String> formatMappings = new Dictionary<uint, String>();

            var stylePart = spreadSheetDocument.WorkbookPart.WorkbookStylesPart;

            var numFormatsParentNodes = stylePart.Stylesheet.ChildElements.OfType<NumberingFormats>();

            foreach (var numFormatParentNode in numFormatsParentNodes)
            {
                var formatNodes = numFormatParentNode.ChildElements.OfType<NumberingFormat>();
                foreach (var formatNode in formatNodes)
                {
                    formatMappings.Add(formatNode.NumberFormatId.Value, formatNode.FormatCode);
                }
            }

            return formatMappings;
        }

        private string ConverToDateValue(string value)
        {
            try
            {
                if (DataUploader.IsNumeric(value))
                {
                    return Convert.ToString(DateTime.FromOADate(Convert.ToDouble(value)));
                }
                else
                {
                    return Convert.ToString(DataUploader.ParseAttributToDateTime(value));
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private void GenerationEntity(ExecuteMultipleRequest emRequest, ParsedRecords pRecords, List<string> loggerErrors, bool callExecuteMultiple)
        {

            callExecuteMultiple = true;

            ExecuteMultipleRequest errRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };


            int indexReq = -1;

            if (!callExecuteMultiple)
            {
                //Single
                foreach (var req in emRequest.Requests)
                {
                    indexReq++;
                    try
                    {
                        if (req.RequestName == "Create")
                        {
                            Guid entityId = _orgService.Create(((CreateRequest)req).Target);
                            pRecords.ResponceCount++;
                        }
                    }
                    catch (Exception e)
                    {
                        List<string> headers = new List<string>();
                        Entity errRecord = (Entity)emRequest.Requests[indexReq].Parameters["Target"];
                        var sheetName = _configObject.FirstOrDefault(x => x.Value.Name.ToLower() == errRecord.GetAttributeValue<string>("ddsm_name").ToLower()).Key;
                        string pattern = @"\'ddsm_\w\'";
                        MatchCollection matchs = Regex.Matches(e.Message, pattern);
                        foreach (Match match in matchs)
                        {
                            var fieldName = (match.Value).Replace("'", "");
                            string fieldKey = (fieldName.Replace("ddsm_", "")).ToUpper();
                            var fieldHeader = _configObject[sheetName].Attributes.FirstOrDefault(x => x.Key == fieldKey).Value.ColumnName;
                            headers.Add(fieldHeader);
                            errRecord.Attributes.Remove(fieldName);
                        }
                        loggerErrors.Add(GetTextError(380050) + sheetName + GetTextError(380051) + string.Join(",", headers) + GetTextError(380052) + (indexReq + 1) + GetTextError(380053) + e.Message);

                        CreateRequest createRequest = new CreateRequest();
                        createRequest.Target = errRecord;
                        errRequest.Requests.Add(createRequest);

                    }
                }

                if (errRequest.Requests.Count > 0)
                {
                    loggerErrors.Add(GetTextError(380007));
                    indexReq = -1;
                    foreach (var req in errRequest.Requests)
                    {
                        try
                        {
                            if (req.RequestName == "Create")
                            {
                                Guid entityId = _orgService.Create(((CreateRequest)req).Target);
                                pRecords.ExcludedDataCount++;
                            }

                        }
                        catch (Exception e)
                        {
                            pRecords.ErrorCount++;
                            loggerErrors.Add(GetTextError(380054) + e.Message);
                        }
                    }

                }

            }
            else
            {

                ExecuteMultipleResponse emResponse = (ExecuteMultipleResponse)_orgService.Execute(emRequest);

                foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                {
                    // A valid response.
                    if (responseItem.Response != null)
                    {
                        pRecords.ResponceCount++;
                        if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                        {
                        }
                    }

                    // An error has occurred.
                    else if (responseItem.Fault != null)
                    {
                        List<string> headers = new List<string>();
                        Entity errRecord = (Entity)emRequest.Requests[responseItem.RequestIndex].Parameters["Target"];
                        var sheetName = _configObject.FirstOrDefault(x => x.Value.Name.ToLower() == errRecord.GetAttributeValue<string>("ddsm_name").ToLower()).Key;
                        string pattern = @"\'ddsm_\w\'";
                        MatchCollection matchs = Regex.Matches(responseItem.Fault.Message, pattern);
                        foreach (Match match in matchs)
                        {
                            var fieldName = (match.Value).Replace("'", "");
                            string fieldKey = (fieldName.Replace("ddsm_", "")).ToUpper();
                            var fieldHeader = _configObject[sheetName].Attributes.FirstOrDefault(x => x.Key == fieldKey).Value.ColumnName;
                            headers.Add(fieldHeader);
                            errRecord.Attributes.Remove(fieldName);
                        }
                        loggerErrors.Add(GetTextError(380050) + sheetName + GetTextError(380051) + string.Join(",", headers) + GetTextError(380052) + (responseItem.RequestIndex + 1) + GetTextError(380053) + responseItem.Fault.Message);

                        CreateRequest createRequest = new CreateRequest();
                        createRequest.Target = errRecord;
                        errRequest.Requests.Add(createRequest);
                    }
                }

                if (errRequest.Requests.Count > 0)
                {
                    loggerErrors.Add(GetTextError(380007));
                    emResponse = (ExecuteMultipleResponse)_orgService.Execute(errRequest);
                    foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                    {
                        if (responseItem.Response != null)
                        {
                            pRecords.ExcludedDataCount++;
                            if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                            {
                            }
                        }
                        else if (responseItem.Fault != null)
                        {
                            pRecords.ErrorCount++;
                            loggerErrors.Add(GetTextError(380054) + responseItem.Fault.Message);

                        }
                    }
                }

            }

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_configObject != null)
                    {
                        _configObject?.Clear();
                    }
                    _dt?.Clear();
                    _logService.Dispose();
                }
                _disposed = true;
            }
        }

    }
}
