﻿using System;
using System.Text.RegularExpressions;

namespace DataUploader.Models
{
    public static class TypeExtensionsDdsm
    {
        public static string PrepareKey(this string key, int keyLength, char additionalChar, bool isMainPhone = false)
        {
            var result = key.GetStringByPattern();
            if (!string.IsNullOrEmpty(key) && result.Length >= keyLength)
            {
                result = result.Substring(0, keyLength);
            }
            if (string.IsNullOrEmpty(key))
            {
                result = result.PadRight(keyLength, additionalChar);
            }

            return result;

        }

        public static string PrepareMainPhoneKey(this string key, int keyLength = 4, char additionalChar = '0')
        {
            var result = key.GetStringByPattern();
            if (!string.IsNullOrEmpty(key) && result.Length >= keyLength)
            {
                result = result.Reverse();
                result = result.Substring(0, keyLength);
                result = result.Reverse();
            }
            if (string.IsNullOrEmpty(key))
            {
                result = result.PadRight(keyLength, additionalChar);
            }

            return result;

        }

        public static string Reverse(this string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public static string GetStringByPattern(this string value, string pattern = "[^a-zA-Z0-9]")
        {
            var replacedString = Regex.Replace(value, pattern, "");
            return replacedString;

        }
    }

}
