﻿using DataUploader.Helper;
using Microsoft.Xrm.Sdk;
using System;
using DataUploader;
using DataUploader.Helper;
using DataUploader.Creator;
using static DataUploader.Model.Enums;
using static DataUploader.Model.Errors;
using System.Collections.Generic;
using System.Diagnostics;
using System.Collections.Concurrent;
using System.Linq;
using System.ServiceModel;

namespace DataUploader.ExcelToDB
{
    public class Creator : BasePlugin
    {
        protected override void Run(IPluginExecutionContext context, ITracingService tracer, IOrganizationService service, object target, DataUploaderSettings settings)
        {
            List<string> Logger = new List<string>();

            if (isStartedPlugin(service, "ddsm_iscreator", settings.TargetEntity.Id))
            {
                Logger.Add(">>> The CMR restarted the plugin: " + GetType().FullName);
                Logger.Add(">>> The work of this copy of the plugin is stopped");
                DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
                Logger = new List<string>();
                return;
            }
            else
            {
                Entity setParsed = new Entity("ddsm_datauploader", settings.TargetEntity.Id);
                setParsed.Attributes.Add("ddsm_iscreator", true);
                service.Update(setParsed);
            }

            var RemoteUploadOfData = RunRemote(service, settings, false);
            if (RemoteUploadOfData)
            {
                return;
            }

            var sw = Stopwatch.StartNew();
            long startSeconds = 0, endSeconds = 0;

            if (!(settings.FileUploaded && settings.FileParsed))
            {
                return;
            }

            Logger.Add(" ");
            Logger.Add(GetTextError(380011) + " " + GetType().FullName + " plugin;" + " *** " + sw.ElapsedMilliseconds / 1000 + " sec");
            Logger.Add("--------------------------------------------------");
            DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
            Logger = new List<string>();

            ConcurrentDictionary<string, CreatedEntity> ContactEntities = new ConcurrentDictionary<string, CreatedEntity>();
            ConcurrentDictionary<string, CreatedEntity> AccountEntities = new ConcurrentDictionary<string, CreatedEntity>();
            ConcurrentDictionary<string, CreatedEntity> SiteEntities = new ConcurrentDictionary<string, CreatedEntity>();
            ConcurrentDictionary<string, CreatedEntity> ProjectGroupEntities = new ConcurrentDictionary<string, CreatedEntity>();
            ConcurrentDictionary<string, CreatedEntity> ProjectEntities = new ConcurrentDictionary<string, CreatedEntity>();
            ConcurrentDictionary<string, CreatedEntity> MeasureEntities = new ConcurrentDictionary<string, CreatedEntity>();
            ConcurrentDictionary<string, CreatedEntity> ProjReportEntities = new ConcurrentDictionary<string, CreatedEntity>();
            ConcurrentDictionary<string, CreatedEntity> MeasReportEntities = new ConcurrentDictionary<string, CreatedEntity>();

            try {
                //Contacts
                var _CreateContacts = new CreateContacts();
                //startSeconds = sw.ElapsedMilliseconds / 1000;
                _CreateContacts.CreateContact(service, ContactEntities, Logger, settings);
                //endSeconds = sw.ElapsedMilliseconds / 1000;
                //Logger.Add("Contact: " + (endSeconds - startSeconds) + " sec");
                //DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
                //Logger = new List<string>();

                //Accounts
                var _CreateAccounts = new CreateAccounts();
                //startSeconds = sw.ElapsedMilliseconds / 1000;
                _CreateAccounts.CreateAccount(service, AccountEntities, Logger, settings);
                //endSeconds = sw.ElapsedMilliseconds / 1000;
                //Logger.Add("Account: " + (endSeconds - startSeconds) + " sec");
                //DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
                //Logger = new List<string>();

                //Update Contact CompanyName
                _CreateContacts.UpdateCompanyName(service, settings);

                //Dispose _CreateContacts & _CreateAccounts
                _CreateContacts.Dispose();
                _CreateAccounts.Dispose();

                //Site
                var _CreateSites = new CreateSites();
                //startSeconds = sw.ElapsedMilliseconds / 1000;
                _CreateSites.CreateSite(service, AccountEntities, SiteEntities, Logger, settings);
                //endSeconds = sw.ElapsedMilliseconds / 1000;
                //Logger.Add("Site: " + (endSeconds - startSeconds) + " sec");
                //DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
                //Logger = new List<string>();

                //Dispose _CreateSites
                _CreateSites.Dispose();

                //Project Group
                var _CreateProjectGroups = new CreateProjGroups();
                //startSeconds = sw.ElapsedMilliseconds / 1000;
                _CreateProjectGroups.CreateProjGroup(service, ProjectGroupEntities, Logger, settings);
                //endSeconds = sw.ElapsedMilliseconds / 1000;
                //Logger.Add("Project Group: " + (endSeconds - startSeconds) + " sec");
                //DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
                //Logger = new List<string>();

                //Dispose _CreateProjectGroups
                _CreateProjectGroups.Dispose();

                //Project
                var _CreateProjects = new CreateProjects();
                //startSeconds = sw.ElapsedMilliseconds / 1000;
                _CreateProjects.CreateProject(service, AccountEntities, SiteEntities, ProjectGroupEntities, ProjectEntities, Logger, settings);
                //endSeconds = sw.ElapsedMilliseconds / 1000;
                //Logger.Add("Project: " + (endSeconds - startSeconds) + " sec");
                //DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
                //Logger = new List<string>();

                //Dispose _CreateProjects
                _CreateProjects.Dispose();

                //Measure
                var _CreateMeasures = new CreateMeasures();
                //startSeconds = sw.ElapsedMilliseconds / 1000;
                _CreateMeasures.CreateMeasure(service, AccountEntities, SiteEntities, ProjectEntities, MeasureEntities, Logger, settings);
                //endSeconds = sw.ElapsedMilliseconds / 1000;
                //Logger.Add("Measure: " + (endSeconds - startSeconds) + " sec");
                //DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
                //Logger = new List<string>();

                //Dispose _CreateMeasures
                _CreateMeasures.Dispose();

                //Project Reporting
                var _CreateProjReportings = new CreateProjReportings();
                //startSeconds = sw.ElapsedMilliseconds / 1000;
                _CreateProjReportings.CreateProjReporting(service, ProjectEntities, ProjReportEntities, Logger, settings);
                //endSeconds = sw.ElapsedMilliseconds / 1000;
                //Logger.Add("Project Reporting: " + (endSeconds - startSeconds) + " sec");
                //DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
                //Logger = new List<string>();

                //Dispose _CreateProjReportings
                _CreateProjReportings.Dispose();

                //Measure Reporting
                var _CreateMeasReportings = new CreateMeasReportings();
                //startSeconds = sw.ElapsedMilliseconds / 1000;
                _CreateMeasReportings.CreateMeasReporting(service, ProjectEntities, MeasureEntities, MeasReportEntities, Logger, settings);
                //endSeconds = sw.ElapsedMilliseconds / 1000;
                //Logger.Add("Measure Reporting: " + (endSeconds - startSeconds) + " sec");

                //Dispose _CreateMeasReportings
                _CreateMeasReportings.Dispose();

                //Finish
                DataUploader.SetStatusFileDataUploading(service, (int)StatusFileDataUploading.recordsUploadCompleted, settings.TargetEntity.Id);

                Logger.Add("--------------------------------------------------");
                Logger.Add(GetTextError(380012) + ";" + " *** " + sw.ElapsedMilliseconds / 1000 + " sec");
                DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");

                Logger = new List<string>();

                if (settings.EspRecalcData != 962080002)
                {
                    MeasTaskQueue(service, MeasureEntities, Logger, settings);
                    DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
                }
                else
                    DataUploader.SetStatusFileDataUploading(service, (int)StatusFileDataUploading.importSuccessfully, settings.TargetEntity.Id);

                tracer.Trace($"The plugin {GetType().FullName} was successfully completed!");

            }
            catch (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex)
            {
                DataUploader.SetStatusFileDataUploading(service, (int)StatusFileDataUploading.parsingFileFailed, settings.TargetEntity.Id);

                tracer.Trace($"The plugin {GetType().FullName} was stopped due to an error!");
                tracer.Trace("Timestamp: " + ex.Detail.Timestamp);
                tracer.Trace("Code: " + ex.Detail.ErrorCode);
                tracer.Trace("Message: " + ex.Detail.Message);

                Logger.Add($"The plugin {GetType().FullName} was stopped due to an error!");
                Logger.Add("Message: " + ex.Detail.Message);
                DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");

            }
            catch (Exception ex)
            {
                DataUploader.SetStatusFileDataUploading(service, (int)StatusFileDataUploading.parsingFileFailed, settings.TargetEntity.Id);

                tracer.Trace($"The plugin {GetType().FullName} was stopped due to an error!");
                tracer.Trace($"Error mesage: {ex.Message}");

                Logger.Add($"The plugin {GetType().FullName} was stopped due to an error!");
                Logger.Add("Message: " + ex.Message);
                DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");

            }


            sw.Stop();

            //Clear Dictionary
            AccountEntities?.Clear();
            SiteEntities?.Clear();
            ContactEntities?.Clear();
            ProjectEntities?.Clear();
            MeasureEntities?.Clear();
            ProjectGroupEntities?.Clear();
            ProjReportEntities?.Clear();
            MeasReportEntities?.Clear();

            Logger?.Clear();



        }

        //Insert Measures Id to TaskQueue Entity
        private void MeasTaskQueue(IOrganizationService _orgService, ConcurrentDictionary<string, CreatedEntity> MeasureEntities, List<string> Logger, DataUploaderSettings _thisSettings)
        {

            Logger.Add(" ");
            Logger.Add(GetTextError(380013));
            //Sorting Meas Ids by Parent Proj
            IComparer<CreatedEntity> myComparerGuidString = new compareGuidString() as IComparer<CreatedEntity>;
            var sortMeasureEntities = MeasureEntities.OrderBy(x => (CreatedEntity)x.Value, myComparerGuidString);

            var measIds = new UserInputObj2();
            measIds.DataFields = null;
            measIds.SmartMeasures = new List<Guid>();
            int j = 1, i = 0; Entity TaskQueue;

            foreach (var key in sortMeasureEntities)
            {
                var _key = key.Key;
                measIds.SmartMeasures.Add(MeasureEntities[_key].EntityGuid);

                if (i == 9999 * j || i == (MeasureEntities.Count - 1))
                {
                    TaskQueue = new Entity("ddsm_taskqueue");
                    TaskQueue["ddsm_name"] = "Measures Task Queue-" + Convert.ToString(j) + " - " + Convert.ToString(DataUploader.ConverAttributToDateTimeUtc("today", _thisSettings.TimeZoneInfo) + " (UTC)");
                    TaskQueue["ddsm_taskentity"] = new OptionSetValue(962080000);//Measure
                    TaskQueue["ddsm_entityrecordtype"] = new OptionSetValue(962080000);//Measure
                    TaskQueue["ddsm_datauploader"] = new EntityReference("ddsm_datauploader", _thisSettings.TargetEntity.Id);
                    TaskQueue["ddsm_processeditems0"] = Newtonsoft.Json.JsonConvert.SerializeObject(measIds);

                    _orgService.Create(TaskQueue);
                    Logger.Add(GetTextError(380014) + "'" + TaskQueue["ddsm_name"].ToString() + "'");
                    j++;
                    measIds = new UserInputObj2();
                    measIds.DataFields = null;
                    measIds.SmartMeasures = new List<Guid>();
                }
                i++;
            }
        }


        /// <summary>
        ///     Get target entity
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private EntityReference GetTargetData(object Target, ITracingService Tracer)
        {
            try
            {
                if (Target is EntityReference)
                {
                    return (EntityReference)Target;
                }

                if (Target is Entity)
                {
                    var sourceEntity = (Entity)Target;
                    return new EntityReference(sourceEntity.LogicalName, sourceEntity.Id);
                }
            }
            catch (Exception)
            {
                Tracer.Trace("Can't get target from Context");
            }


            return null;
        }

        /// <summary>
        /// Returns whether the plugin is currently enrolled within a database transaction.
        /// </summary>
        /// <returns></returns>
        protected virtual bool IsInTransaction(IPluginExecutionContext _context)
        {
            return _context.IsInTransaction;
        }

    }
}
