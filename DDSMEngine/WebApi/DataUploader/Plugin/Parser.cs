﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Crm.Sdk.Messages;

using System;
using Excel;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Activities;
using System.Data;
using Microsoft.Xrm.Sdk.Query;
using System.IO;
using System.ServiceModel;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Linq;
using DataUploader;
using DataUploader.Helper;
using static DataUploader.Model.Enums;
using static DataUploader.ParseExcel;
using static DataUploader.Model.Errors;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace DataUploader.ExcelToDB
{
    public class Parser : BasePlugin
    {

        protected override void Run(IPluginExecutionContext context, ITracingService tracer, IOrganizationService service, object target, DataUploaderSettings settings)
        {
            bool parseError = false;

            List<string> Logger = new List<string>();

            if (isStartedPlugin(service, "ddsm_isparser", settings.TargetEntity.Id))
            {
                Logger.Add(">>> The CMR restarted the plugin: " + GetType().FullName);
                Logger.Add(">>> The work of this copy of the plugin is stopped");
                DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
                Logger = new List<string>();
                return;
            }
            else {
                Entity setParsed = new Entity("ddsm_datauploader", settings.TargetEntity.Id);
                setParsed.Attributes.Add("ddsm_isparser", true);
                service.Update(setParsed);
            }

            var RemoteUploadOfData = RunRemote(service, settings);
            if (RemoteUploadOfData)
            {
                return;
            }

            var sw = Stopwatch.StartNew();

            try {
                Logger.Add(GetTextError(380008) + " " + GetType().FullName + " plugin;" + " *** " + sw.ElapsedMilliseconds / 1000 + " sec");
                Logger.Add("--------------------------------------------------");
                DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
                Logger = new List<string>();

                ConcurrentDictionary<string, ParsedRecords> parsedRecords = new ConcurrentDictionary<string, ParsedRecords>();

                var objects = Newtonsoft.Json.JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(settings.JsonObjects);

                QueryExpression Notes = new QueryExpression { EntityName = "annotation", ColumnSet = new ColumnSet("filename", "subject", "annotationid", "documentbody") };
                Notes.Criteria.AddCondition("objectid", ConditionOperator.Equal, settings.TargetEntity.Id);
                EntityCollection NotesRetrieve = service.RetrieveMultiple(Notes);

                if (NotesRetrieve != null && NotesRetrieve.Entities.Count == 1)
                {
                    string _fileName = NotesRetrieve.Entities[0].Attributes["filename"].ToString();

                    if (settings.StatusFileDataUploading != (int)StatusFileDataUploading.parsingFile)
                        DataUploader.SetStatusFileDataUploading(service, (int)StatusFileDataUploading.parsingFile, settings.TargetEntity.Id);


                    byte[] fileContent = Convert.FromBase64String(NotesRetrieve.Entities[0].Attributes["documentbody"].ToString());
                    //Stream msFile = new MemoryStream(fileContent);

                    using (var msFile = new MemoryStream())
                    {
                        msFile.Write(fileContent, 0, fileContent.Length);

                        //string[] splitFileName = (_fileName.ToLower()).Split('.');

                        tracer.Trace($"File name: {_fileName}. Extension file: {Path.GetExtension(_fileName)}");

                        if (Path.GetExtension(_fileName).ToString().ToLower() == ".xlsx")
                        {
                            parseError = ParseXLSX(service, msFile, objects, parsedRecords, Logger, settings);
                            if (!parseError)
                            {
                                DataUploader.SetStatusFileDataUploading(service, (int)StatusFileDataUploading.parsingFileCompleted, settings.TargetEntity.Id);

                                //Set File Parsed True
                                Entity setParsed = new Entity("ddsm_datauploader", settings.TargetEntity.Id);
                                setParsed.Attributes.Add("ddsm_fileparsed", true);
                                service.Update(setParsed);
                            }
                        }
                        else if (Path.GetExtension(_fileName).ToString().ToLower() == ".xls")
                        {
                            parseError = ParseXLS(service, msFile, objects, parsedRecords, Logger, settings);
                            if (!parseError)
                            {
                                DataUploader.SetStatusFileDataUploading(service, (int)StatusFileDataUploading.parsingFileCompleted, settings.TargetEntity.Id);

                                //Set File Parsed True
                                Entity setParsed = new Entity("ddsm_datauploader", settings.TargetEntity.Id);
                                setParsed.Attributes.Add("ddsm_fileparsed", true);
                                service.Update(setParsed);
                            }

                        }
                        else
                        {
                            Logger.Add(GetTextError(380003));
                            parseError = true;
                        }
                    }
                }

                if (!parseError)
                {
                    //Logger = new List<string>();
                    Logger.Add("--------------------------------------------------");
                    Logger.Add(GetTextError(380009) + ";" + " *** " + sw.ElapsedMilliseconds / 1000 + " sec");
                    DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
                } else {
                    Logger.Add("--------------------------------------------------");
                    Logger.Add(GetTextError(380023) + ";" + " *** " + sw.ElapsedMilliseconds / 1000 + " sec");
                    DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
                    DataUploader.SetStatusFileDataUploading(service, (int)StatusFileDataUploading.parsingFileFailed, settings.TargetEntity.Id);
                }

                tracer.Trace($"The plugin {GetType().FullName} was successfully completed!");

            }
            catch (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex)
            {
                DataUploader.SetStatusFileDataUploading(service, (int)StatusFileDataUploading.parsingFileFailed, settings.TargetEntity.Id);

                tracer.Trace($"The plugin {GetType().FullName} was stopped due to an error!");
                tracer.Trace("Timestamp: " + ex.Detail.Timestamp);
                tracer.Trace("Code: " + ex.Detail.ErrorCode);
                tracer.Trace("Message: " + ex.Detail.Message);

                Logger.Add($"The plugin {GetType().FullName} was stopped due to an error!");
                Logger.Add("Message: " + ex.Detail.Message);
                DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");

            }
            catch (Exception ex)
            {
                DataUploader.SetStatusFileDataUploading(service, (int)StatusFileDataUploading.parsingFileFailed, settings.TargetEntity.Id);

                tracer.Trace($"The plugin {GetType().FullName} was stopped due to an error!");
                tracer.Trace($"Error mesage: {ex.Message}");

                Logger.Add($"The plugin {GetType().FullName} was stopped due to an error!");
                Logger.Add("Message: " + ex.Message);
                DataUploader.SaveLog(service, settings.TargetEntity.Id, Logger, "ddsm_log");
            }

            sw.Stop();
        }

        /// <summary>
        ///     Get target entity
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private EntityReference GetTargetData(object Target, ITracingService Tracer)
        {
            try
            {
                if (Target is EntityReference)
                {
                    return (EntityReference)Target;
                }

                if (Target is Entity)
                {
                    var sourceEntity = (Entity)Target;
                    return new EntityReference(sourceEntity.LogicalName, sourceEntity.Id);
                }
            }
            catch (Exception)
            {
                Tracer.Trace("Can't get target from Context");
            }


            return null;
        }

        /// <summary>
        /// Returns whether the plugin is currently enrolled within a database transaction.
        /// </summary>
        /// <returns></returns>
        protected virtual bool IsInTransaction(IPluginExecutionContext _context)
        {
            return _context.IsInTransaction;
        }

    }
}
