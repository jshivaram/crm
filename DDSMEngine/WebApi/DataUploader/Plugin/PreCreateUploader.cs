﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Crm.Sdk.Messages;

using System;
using Excel;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Activities;
using System.Data;
using Microsoft.Xrm.Sdk.Query;
using System.IO;
using System.ServiceModel;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Linq;
using static DataUploader.Model.Enums;

namespace DataUploader.ExcelToDB
{
    public class PreCreateUploader : IPlugin
    {
        private IServiceProvider _ServiceProvider;

        public void Execute(IServiceProvider serviceProvider)
        {
            _ServiceProvider = serviceProvider;
            Execute();
        }

        private void Execute()
        {

            ITracingService tracer = (ITracingService)_ServiceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)_ServiceProvider.GetService(typeof(IPluginExecutionContext));
            if (context == null)
            {
                throw new ArgumentNullException("Context is NULL");
            }
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)_ServiceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService orgService = serviceFactory.CreateOrganizationService(context.UserId);
            IOrganizationService systemService = DataUploader.GetSystemOrgService(serviceFactory, orgService);


            Entity _preDataUploader, _dataUploader;
            //DataUploader _DataUploader = new DataUploader();

            _dataUploader = (Entity)context.InputParameters["Target"];

            //if (!!context.PreEntityImages.Contains("Pre"))
            //{
            //_preDataUploader = context.PreEntityImages["Pre"];
            int typeofUploadedData = -2147483648, creatorRecordType = -2147483648;

            if (_dataUploader.Attributes.ContainsKey("ddsm_typeofuploadeddata"))
                typeofUploadedData = (int)_dataUploader.GetAttributeValue<OptionSetValue>("ddsm_typeofuploadeddata").Value;

            if (_dataUploader.Attributes.ContainsKey("ddsm_creatorrecordtype"))
                creatorRecordType = (int)_dataUploader.GetAttributeValue<OptionSetValue>("ddsm_creatorrecordtype").Value;

            if (typeofUploadedData != -2147483648)
            {
                QueryExpression query = new QueryExpression
                {
                    EntityName = "ddsm_admindata",
                    ColumnSet = new ColumnSet("ddsm_admindataid",
                    "ddsm_deduplicationrules", "ddsm_duplicatedetection", "ddsm_datauploaderdevmode", "ddsm_globalrequestscount", "ddsm_globalpagerecordscount", "ddsm_startdatarowdu", "ddsm_callexecutemultiple")
                };
                query.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, "Admin Data");
                query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));
                EntityCollection queryRetrieve = systemService.RetrieveMultiple(query);
                if (queryRetrieve != null && queryRetrieve.Entities.Count == 1)
                {
                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_deduplicationrules"))
                        _dataUploader["ddsm_deduplicationrules"] = queryRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_deduplicationrules");

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_duplicatedetection"))
                        _dataUploader["ddsm_duplicatedetection"] = queryRetrieve[0].GetAttributeValue<bool>("ddsm_duplicatedetection");

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_datauploaderdevmode"))
                        _dataUploader["ddsm_datauploaderdevmode"] = queryRetrieve[0].GetAttributeValue<bool>("ddsm_datauploaderdevmode");

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_globalrequestscount"))
                        _dataUploader["ddsm_globalrequestscount"] = queryRetrieve[0].GetAttributeValue<int>("ddsm_globalrequestscount");

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_globalpagerecordscount"))
                        _dataUploader["ddsm_globalpagerecordscount"] = queryRetrieve[0].GetAttributeValue<int>("ddsm_globalpagerecordscount");

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_startdatarowdu"))
                        _dataUploader["ddsm_startdatarowdu"] = queryRetrieve[0].GetAttributeValue<int>("ddsm_startdatarowdu") - 1;

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_callexecutemultiple"))
                        _dataUploader["ddsm_callexecutemultiple"] = queryRetrieve[0].GetAttributeValue<bool>("ddsm_callexecutemultiple");
                }

                query = new QueryExpression { EntityName = "ddsm_datauploaderconfiguration", ColumnSet = new ColumnSet("ddsm_datauploaderconfigurationid", "ddsm_datauploaderesprecalculation", "ddsm_config") };
                query.Criteria.AddCondition("ddsm_typeofuploadeddata", ConditionOperator.Equal, typeofUploadedData);
                query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));
                queryRetrieve = systemService.RetrieveMultiple(query);
                if (queryRetrieve != null && queryRetrieve.Entities.Count == 1)
                {
                    _dataUploader["ddsm_datauploaderconfiguration"] = new EntityReference("ddsm_datauploaderconfiguration", (Guid)queryRetrieve.Entities[0].Attributes["ddsm_datauploaderconfigurationid"]);

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_config"))
                        _dataUploader["ddsm_config"] = queryRetrieve[0].GetAttributeValue<string>("ddsm_config");

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_datauploaderesprecalculation"))
                        _dataUploader["ddsm_datauploaderesprecalculation"] = queryRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_datauploaderesprecalculation");
                    else
                        _dataUploader["ddsm_datauploaderesprecalculation"] = new OptionSetValue(962080001);
                }


                if (creatorRecordType == 962080000 || creatorRecordType == -2147483648)
                    _dataUploader["ddsm_fileparsed"] = false;
                else
                    _dataUploader["ddsm_fileparsed"] = true;

                /*
                _dataUploader["ddsm_createdcontacts"] = false;
                _dataUploader["ddsm_createdaccounts"] = false;
                _dataUploader["ddsm_createdsites"] = false;
                _dataUploader["ddsm_createdprojectgroups"] = false;
                _dataUploader["ddsm_createdprojects"] = false;
                _dataUploader["ddsm_createdmeasures"] = false;
                _dataUploader["ddsm_createdmeasurereports"] = false;
                _dataUploader["ddsm_createdprojectreports"] = false;

                _dataUploader["ddsm_updatedsites"] = false;
                _dataUploader["ddsm_updatedcontacts"] = false;
                */
                
                _dataUploader["ddsm_fileuploaded"] = false;
                _dataUploader["ddsm_triggerstart"] = false;
                
            }
            //}

        }

    }
}
