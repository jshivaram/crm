﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using static DataUploader.Model.Enums;

namespace DataUploader.ExcelToDB
{
    public class SetStatus : IPlugin
    {
        private IServiceProvider _ServiceProvider;

        public void Execute(IServiceProvider serviceProvider)
        {
            _ServiceProvider = serviceProvider;
            Execute();
        }
        private void Execute()
        {
            ITracingService tracer = (ITracingService)_ServiceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)_ServiceProvider.GetService(typeof(IPluginExecutionContext));
            if (context == null)
            {
                throw new ArgumentNullException("Context is NULL");
            }
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)_ServiceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService orgService = serviceFactory.CreateOrganizationService(context.UserId);
            IOrganizationService systemService = DataUploader.GetSystemOrgService(serviceFactory, orgService);

            try
            {
                Entity _dataUploader = (Entity)context.InputParameters["Target"];

                //Entity TargetEntity = GetTargetEntity(context);
                //Guid recordUploaded = TargetEntity.Id;

                QueryExpression query;
                EntityCollection queryRetrieve;

                int statusFileDataUploading = -2147483648, allowSimultaneousUploads = -2147483648;
                /*
                QueryExpression recordUploadedQuery = new QueryExpression { EntityName = "ddsm_datauploader", ColumnSet = new ColumnSet("ddsm_statusfiledatauploading") };
                recordUploadedQuery.Criteria.AddCondition("ddsm_datauploaderid", ConditionOperator.Equal, recordUploaded);
                EntityCollection recordUploadedRetrieve = orgService.RetrieveMultiple(recordUploadedQuery);

                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_statusfiledatauploading"))
                    statusFileDataUploading = (int)recordUploadedRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_statusfiledatauploading").Value;
                */

                if (_dataUploader.Attributes.ContainsKey("ddsm_statusfiledatauploading"))
                    statusFileDataUploading = (int)_dataUploader.GetAttributeValue<OptionSetValue>("ddsm_statusfiledatauploading").Value;


                if (statusFileDataUploading != -2147483648)
                {

                    switch (statusFileDataUploading)
                    {
                        case 962080002: // Excel File Upload Failed
                        case 962080005: // Parsing Excel File Failed
                        case 962080022: // Import Failed
                        case 962080023: // Records Upload Failed
                        case 962080021: // Import Completed Successfully
                                        //case 962080016:
                                        //case -2147483648:

                            /*
                            Entity _dataUploader = new Entity("ddsm_datauploader", recordUploaded);
                            _dataUploader["statecode"] = new OptionSetValue(1);
                            _dataUploader["statuscode"] = new OptionSetValue(2);
                            orgService.Update(_dataUploader);
                            */

                            query = new QueryExpression
                            {
                                EntityName = "ddsm_admindata",
                                ColumnSet = new ColumnSet("ddsm_admindataid", "ddsm_allowsimultaneousuploads")
                            };
                            query.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, "Admin Data");
                            query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));
                            queryRetrieve = orgService.RetrieveMultiple(query);
                            if (queryRetrieve != null && queryRetrieve.Entities.Count == 1)
                            {
                                if (queryRetrieve[0].Attributes.ContainsKey("ddsm_allowsimultaneousuploads"))
                                    allowSimultaneousUploads = (int)queryRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_allowsimultaneousuploads").Value;

                            }
                            if (allowSimultaneousUploads != -2147483648)
                            {
                                switch (allowSimultaneousUploads)
                                {
                                    case 962080001: // Forbidden
                                        break;
                                    case 962080000: // Allowed
                                    case 962080002: // Queued

                                        query = new QueryExpression();
                                        query.EntityName = "ddsm_datauploader";
                                        query.ColumnSet = new ColumnSet("ddsm_datauploaderid", "ddsm_creatorrecordtype", "ddsm_fileuploaded", "ddsm_fileparsed", "ddsm_statusfiledatauploading", "ddsm_name");
                                        query.Criteria = new FilterExpression(LogicalOperator.And);
                                        query.AddOrder("createdon", OrderType.Ascending);

                                        query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

                                        //query.Criteria.AddCondition(new ConditionExpression("ddsm_fileuploaded", ConditionOperator.Equal, true));

                                        //query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.Equal, 962080001));
                                        //query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.Equal, 962080004));


                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080000));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080002));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080003));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080004));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080005));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080006));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080007));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080008));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080010));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080011));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080012));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080013));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080014));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080015));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080016));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080017));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080018));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080019));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080020));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080021));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080022));
                                        query.Criteria.AddCondition(new ConditionExpression("ddsm_statusfiledatauploading", ConditionOperator.NotEqual, 962080023));


                                        //query.Criteria.AddCondition(new ConditionExpression("ddsm_fileuploaded", ConditionOperator.Equal, false));

                                        if (_dataUploader.Attributes.ContainsKey("ddsm_datauploaderid"))
                                            query.Criteria.AddCondition(new ConditionExpression("ddsm_datauploaderid", ConditionOperator.NotEqual, _dataUploader.GetAttributeValue<Guid>("ddsm_datauploaderid")));

                                        queryRetrieve = orgService.RetrieveMultiple(query);
                                        if (queryRetrieve != null && queryRetrieve.Entities.Count > 0)
                                        {
                                            int creatorRecordType = -2147483648;
                                            bool fileUploaded = false, fileParsed = false;
                                            Guid updateGuid = Guid.Empty;

                                            if (queryRetrieve[0].Attributes.ContainsKey("ddsm_creatorrecordtype"))
                                                creatorRecordType = (int)queryRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_creatorrecordtype").Value;
                                            if (queryRetrieve[0].Attributes.ContainsKey("ddsm_fileuploaded"))
                                                fileUploaded = (bool)queryRetrieve[0].GetAttributeValue<bool>("ddsm_fileuploaded");
                                            if (queryRetrieve[0].Attributes.ContainsKey("ddsm_fileparsed"))
                                                fileParsed = (bool)queryRetrieve[0].GetAttributeValue<bool>("ddsm_fileparsed");
                                            if (queryRetrieve[0].Attributes.ContainsKey("ddsm_datauploaderid"))
                                                updateGuid = (Guid)queryRetrieve[0].GetAttributeValue<Guid>("ddsm_datauploaderid");

                                            if (updateGuid != Guid.Empty)
                                            {
                                                //Entity startUpload = new Entity("ddsm_datauploader", updateGuid);
                                                Entity recordScheduler = new Entity("ddsm_datauploaderscheduler");
                                                if (!fileUploaded && !fileParsed && (creatorRecordType == (int)CreatorRecordType.Front || creatorRecordType == -2147483648))
                                                {
                                                    //startUpload["ddsm_triggerstart"] = true;
                                                    //orgService.Update(startUpload);
                                                    //recordScheduler["ddsm_name"] = queryRetrieve[0].GetAttributeValue<string>("ddsm_name");
                                                    //recordScheduler["ddsm_triggerfield"] = "ddsm_fileuploaded";
                                                    //recordScheduler["ddsm_datauploader"] = new EntityReference("ddsm_datauploader", updateGuid);
                                                    //Guid schedulerId = orgService.Create(recordScheduler);
                                                    //BulkDeleteQueued(orgService, updateGuid, queryRetrieve[0].GetAttributeValue<string>("ddsm_name"));
                                                }
                                                else if (fileUploaded && !fileParsed)
                                                {
                                                    //startUpload["ddsm_triggerstart"] = true;
                                                    //orgService.Update(startUpload);
                                                    //recordScheduler["ddsm_name"] = queryRetrieve[0].GetAttributeValue<string>("ddsm_name");
                                                    //recordScheduler["ddsm_triggerfield"] = "ddsm_fileparsed";
                                                    //recordScheduler["ddsm_datauploader"] = new EntityReference("ddsm_datauploader", updateGuid);
                                                    //Guid schedulerId = orgService.Create(recordScheduler);
                                                    //BulkDeleteQueued(orgService, updateGuid, queryRetrieve[0].GetAttributeValue<string>("ddsm_name"));
                                                }

                                            }

                                        }

                                        break;
                                }
                            }

                            _dataUploader["statecode"] = new OptionSetValue(1);
                            _dataUploader["statuscode"] = new OptionSetValue(2);

                            break;
                    }



                }


            }
            catch (Exception e)
            {
                //throw new InvalidPluginExecutionException(e.Message);
            }

        }
    }
}
