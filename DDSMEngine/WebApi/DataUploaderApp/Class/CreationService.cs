﻿using DataUploader;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using DataUploader;
using DataUploader.Creator;
using static DataUploader.Model.Enums;
using static DataUploader.Model.Errors;
using System.Diagnostics;

namespace DataUploaderApp.Service
{
    internal class CreationService
    {
        private readonly DataUploaderSettings _thisSettings;
        private readonly IOrganizationService _orgService;
        private bool disposed = false;

        private ConcurrentDictionary<string, CreatedEntity> ContactEntities { get; set; }
        private ConcurrentDictionary<string, CreatedEntity> AccountEntities { get; set; }
        private ConcurrentDictionary<string, CreatedEntity> SiteEntities { get; set; }
        private ConcurrentDictionary<string, CreatedEntity> ProjectGroupEntities { get; set; }
        private ConcurrentDictionary<string, CreatedEntity> ProjectEntities { get; set; }
        private ConcurrentDictionary<string, CreatedEntity> MeasureEntities { get; set; }
        private ConcurrentDictionary<string, CreatedEntity> ProjReportEntities { get; set; }
        private ConcurrentDictionary<string, CreatedEntity> MeasReportEntities { get; set; }

        public CreationService(UploaderService uploaderService)
        {
            _thisSettings = uploaderService.GetSettings();
            _orgService = uploaderService.GetOrgService();

            ContactEntities = new ConcurrentDictionary<string, CreatedEntity>();
            AccountEntities = new ConcurrentDictionary<string, CreatedEntity>();
            SiteEntities = new ConcurrentDictionary<string, CreatedEntity>();
            ProjectGroupEntities = new ConcurrentDictionary<string, CreatedEntity>();
            ProjectEntities = new ConcurrentDictionary<string, CreatedEntity>();
            MeasureEntities = new ConcurrentDictionary<string, CreatedEntity>();
            ProjReportEntities = new ConcurrentDictionary<string, CreatedEntity>();
            MeasReportEntities = new ConcurrentDictionary<string, CreatedEntity>();
        }

        public CreationService(DataUploaderSettings thisSettings, IOrganizationService orgService)
        {
            _thisSettings = thisSettings;
            _orgService = orgService;

            ContactEntities = new ConcurrentDictionary<string, CreatedEntity>();
            AccountEntities = new ConcurrentDictionary<string, CreatedEntity>();
            SiteEntities = new ConcurrentDictionary<string, CreatedEntity>();
            ProjectGroupEntities = new ConcurrentDictionary<string, CreatedEntity>();
            ProjectEntities = new ConcurrentDictionary<string, CreatedEntity>();
            MeasureEntities = new ConcurrentDictionary<string, CreatedEntity>();
            ProjReportEntities = new ConcurrentDictionary<string, CreatedEntity>();
            MeasReportEntities = new ConcurrentDictionary<string, CreatedEntity>();
        }

        public void Run()
        {
            StartingService();
        }

        private void StartingService()
        {
            var sw = Stopwatch.StartNew();
            long startSeconds = 0, endSeconds = 0;

            List<string> Logger = new List<string>();

            Logger.Add("*** Remote " + GetTextError(380011) + ";" + " *** " + sw.ElapsedMilliseconds / 1000 + " sec");
            Logger.Add("--------------------------------------------------");

            if (!(_thisSettings.FileUploaded && _thisSettings.FileParsed))
            {
                return;
            }

            DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");
            Logger = new List<string>();

            //Contacts
            var _CreateContacts = new CreateContacts();
            //startSeconds = sw.ElapsedMilliseconds / 1000;
            _CreateContacts.CreateContact(_orgService, ContactEntities, Logger, _thisSettings);
            //endSeconds = sw.ElapsedMilliseconds / 1000;
            //Logger.Add("Contact: " + (endSeconds-startSeconds) + " sec");
            //DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");
            //Logger = new List<string>();

            //Accounts
            var _CreateAccounts = new CreateAccounts();
            //startSeconds = sw.ElapsedMilliseconds / 1000;
            _CreateAccounts.CreateAccount(_orgService, AccountEntities, Logger, _thisSettings);
            //endSeconds = sw.ElapsedMilliseconds / 1000;
            //Logger.Add("Account: " + (endSeconds - startSeconds) + " sec");
            //DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");
            //Logger = new List<string>();

            //Update Contact CompanyName
            _CreateContacts.UpdateCompanyName(_orgService, _thisSettings);

            //Dispose _CreateContacts & _CreateAccounts
            _CreateContacts.Dispose();
            _CreateAccounts.Dispose();

            //Site
            var _CreateSites = new CreateSites();
            //startSeconds = sw.ElapsedMilliseconds / 1000;
            _CreateSites.CreateSite(_orgService, AccountEntities, SiteEntities, Logger, _thisSettings);
            //endSeconds = sw.ElapsedMilliseconds / 1000;
            //Logger.Add("Site: " + (endSeconds - startSeconds) + " sec");
            //DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");
            //Logger = new List<string>();

            //Dispose _CreateSites
            _CreateSites.Dispose();

            //Project Group
            var _CreateProjectGroups = new CreateProjGroups();
            //startSeconds = sw.ElapsedMilliseconds / 1000;
            _CreateProjectGroups.CreateProjGroup(_orgService, ProjectGroupEntities, Logger, _thisSettings);
            //endSeconds = sw.ElapsedMilliseconds / 1000;
            //Logger.Add("Project Group: " + (endSeconds - startSeconds) + " sec");
            //DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");
            //Logger = new List<string>();

            //Dispose _CreateProjectGroups
            _CreateProjectGroups.Dispose();

            //Project
            var _CreateProjects = new CreateProjects();
            //startSeconds = sw.ElapsedMilliseconds / 1000;
            _CreateProjects.CreateProject(_orgService, AccountEntities, SiteEntities, ProjectGroupEntities, ProjectEntities, Logger, _thisSettings);
            //endSeconds = sw.ElapsedMilliseconds / 1000;
            //Logger.Add("Project: " + (endSeconds - startSeconds) + " sec");
            //DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");
            //Logger = new List<string>();

            //Dispose _CreateProjects
            _CreateProjects.Dispose();

            //Measure
            var _CreateMeasures = new CreateMeasures();
            //startSeconds = sw.ElapsedMilliseconds / 1000;
            _CreateMeasures.CreateMeasure(_orgService, AccountEntities, SiteEntities, ProjectEntities, MeasureEntities, Logger, _thisSettings);
            //endSeconds = sw.ElapsedMilliseconds / 1000;
            //Logger.Add("Measure: " + (endSeconds - startSeconds) + " sec");
            //DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");
            //Logger = new List<string>();

            //Dispose _CreateMeasures
            _CreateMeasures.Dispose();

            //Project Reporting
            var _CreateProjReportings = new CreateProjReportings();
            //startSeconds = sw.ElapsedMilliseconds / 1000;
            _CreateProjReportings.CreateProjReporting(_orgService, ProjectEntities, ProjReportEntities, Logger, _thisSettings);
            //endSeconds = sw.ElapsedMilliseconds / 1000;
            //Logger.Add("Project Reporting: " + (endSeconds - startSeconds) + " sec");
            //DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");
            //Logger = new List<string>();

            //Dispose _CreateProjReportings
            _CreateProjReportings.Dispose();

            //Measure Reporting
            var _CreateMeasReportings = new CreateMeasReportings();
            //startSeconds = sw.ElapsedMilliseconds / 1000;
            _CreateMeasReportings.CreateMeasReporting(_orgService, ProjectEntities, MeasureEntities, MeasReportEntities, Logger, _thisSettings);
            //endSeconds = sw.ElapsedMilliseconds / 1000;
            //Logger.Add("Measure Reporting: " + (endSeconds - startSeconds) + " sec");

            //Dispose _CreateMeasReportings
            _CreateMeasReportings.Dispose();

            //Finish
            DataUploader.DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.recordsUploadCompleted, _thisSettings.TargetEntity.Id);

            Logger.Add("--------------------------------------------------");
            Logger.Add(GetTextError(380012) + ";" + " *** " + sw.ElapsedMilliseconds / 1000 + " sec");
            DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");

            Logger = new List<string>();

            if (_thisSettings.EspRecalcData != 962080002)
            {
                MeasTaskQueue(_orgService, MeasureEntities, Logger, _thisSettings);
                DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");
            }
            else
                DataUploader.DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.importSuccessfully, _thisSettings.TargetEntity.Id);

            Logger?.Clear();

            sw.Stop();


        }

        //Insert Measures Id to TaskQueue Entity
        private void MeasTaskQueue(IOrganizationService _orgService, ConcurrentDictionary<string, CreatedEntity> MeasureEntities, List<string> Logger, DataUploaderSettings _thisSettings)
        {

            Logger.Add(" ");
            Logger.Add(GetTextError(380013));
            //Sorting Meas Ids by Parent Proj
            IComparer<CreatedEntity> myComparerGuidString = new compareGuidString() as IComparer<CreatedEntity>;
            var sortMeasureEntities = MeasureEntities.OrderBy(x => (CreatedEntity)x.Value, myComparerGuidString);

            var measIds = new UserInputObj2();
            measIds.DataFields = null;
            measIds.SmartMeasures = new List<Guid>();
            int j = 1, i = 0; Entity TaskQueue;

            foreach (var key in sortMeasureEntities)
            {
                var _key = key.Key;
                measIds.SmartMeasures.Add(MeasureEntities[_key].EntityGuid);

                if (i == 9999 * j || i == (MeasureEntities.Count - 1))
                {
                    TaskQueue = new Entity("ddsm_taskqueue");
                    TaskQueue["ddsm_name"] = "Measures Task Queue-" + Convert.ToString(j) + " - " + Convert.ToString(DataUploader.DataUploader.ConverAttributToDateTimeUtc("today", _thisSettings.TimeZoneInfo) + " (UTC)");
                    TaskQueue["ddsm_taskentity"] = new OptionSetValue(962080000);//Measure
                    TaskQueue["ddsm_entityrecordtype"] = new OptionSetValue(962080000);//Measure
                    TaskQueue["ddsm_datauploader"] = new EntityReference("ddsm_datauploader", _thisSettings.TargetEntity.Id);
                    TaskQueue["ddsm_processeditems0"] = Newtonsoft.Json.JsonConvert.SerializeObject(measIds);

                    _orgService.Create(TaskQueue);
                    Logger.Add(GetTextError(380014) + "'" + TaskQueue["ddsm_name"].ToString() + "'");
                    j++;
                    measIds = new UserInputObj2();
                    measIds.DataFields = null;
                    measIds.SmartMeasures = new List<Guid>();
                }
                i++;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (AccountEntities != null)
                    {
                        AccountEntities?.Clear();
                    }
                    if (SiteEntities != null)
                    {
                        SiteEntities?.Clear();
                    }
                    if (ContactEntities != null)
                    {
                        ContactEntities?.Clear();
                    }
                    if (ProjectEntities != null)
                    {
                        ProjectEntities?.Clear();
                    }
                    if (MeasureEntities != null)
                    {
                        MeasureEntities?.Clear();
                    }
                    if (ProjectGroupEntities != null)
                    {
                        ProjectGroupEntities?.Clear();
                    }
                    if (ProjReportEntities != null)
                    {
                        ProjReportEntities?.Clear();
                    }
                    if (MeasReportEntities != null)
                    {
                        MeasReportEntities?.Clear();
                    }
                }
                this.disposed = true;
            }
        }

    }
}
