﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;

namespace DataUploaderApp.Hellper
{
    public class AppArg
    {
        public string TargetGuid { get; set; }
        public string UserGuid { get; set; }
    }

    internal class CrmConnect
    {
        //Provides the low-level interaction and wrapper methods to connect to Microsoft Dynamics CRM and execute actions.
        public CrmServiceClient CrmServiceClient { get; set; } = null;
        //
        public string ConnectionStrings { get; set; } = null;
        //Provides programmatic access to the metadata and data for an organization.
        public IOrganizationService OrgService { get; set; } = null;
    }

    internal class Hellper
    {

    }
}
