﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataUploader;
using Microsoft.Xrm.Sdk;
using System.Collections.Concurrent;
using Microsoft.Xrm.Sdk.Query;
using System.IO;
//using Newtonsoft.Json;
using static DataUploader.Model.Enums;
using static DataUploader.ParseExcel;
using static DataUploader.Model.Errors;
using System.Diagnostics;
using System.Net.Http;

namespace DataUploaderApp.Service
{
    internal class ParserService
    {
        private readonly DataUploaderSettings _thisSettings;
        private readonly IOrganizationService _orgService;
        private bool disposed = false;

        private ConcurrentDictionary<string, ParsedRecords> parsedRecords { get; set; }
        private ConcurrentDictionary<string, EntityJson> objects { get; set; }

        public ParserService(UploaderService uploaderService)
        {
            _thisSettings = uploaderService.GetSettings();
            _orgService = uploaderService.GetOrgService();

            parsedRecords = new ConcurrentDictionary<string, ParsedRecords>();
        }

        public void Run()
        {
            GetFile();
        }

        private void GetFile()
        {
            bool parseError = false;

            List<string> Logger = new List<string>();
            var sw = Stopwatch.StartNew();

            try
            {
                Logger.Add("*** Remote " + GetTextError(380008) + ";" + " *** " + sw.ElapsedMilliseconds / 1000 + " sec");
                Logger.Add("--------------------------------------------------");
                DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");
                Logger = new List<string>();

                //ParsedRecords pRecords = new ParsedRecords();
                //parsedRecords.AddOrUpdate("rec", pRecords, (oldkey, oldvalue) => pRecords);

                objects = Newtonsoft.Json.JsonConvert.DeserializeObject<ConcurrentDictionary<string, EntityJson>>(_thisSettings.JsonObjects);

                QueryExpression Notes = new QueryExpression { EntityName = "annotation", ColumnSet = new ColumnSet("filename", "subject", "annotationid", "documentbody") };
                Notes.Criteria.AddCondition("objectid", ConditionOperator.Equal, _thisSettings.TargetEntity.Id);
                EntityCollection NotesRetrieve = _orgService.RetrieveMultiple(Notes);

                if (NotesRetrieve != null && NotesRetrieve.Entities.Count == 1)
                {
                    string _fileName = NotesRetrieve.Entities[0].Attributes["filename"].ToString();

                    if (_thisSettings.StatusFileDataUploading != (int)StatusFileDataUploading.parsingFile)
                        DataUploader.DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.parsingFile, _thisSettings.TargetEntity.Id);


                    byte[] fileContent = Convert.FromBase64String(NotesRetrieve.Entities[0].Attributes["documentbody"].ToString());
                    //Stream msFile = new MemoryStream(fileContent);

                    using (var msFile = new MemoryStream())
                    {
                        msFile.Write(fileContent, 0, fileContent.Length);

                        if (Path.GetExtension(_fileName).ToString().ToLower() == ".xlsx")
                        {
                            parseError = ParseXLSX(_orgService, msFile, objects, parsedRecords, Logger, _thisSettings);
                            if (!parseError)
                            {
                                DataUploader.DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.parsingFileCompleted, _thisSettings.TargetEntity.Id);

                                //Set File Parsed True
                                /*
                                Entity setParsed = new Entity("ddsm_datauploader", _thisSettings.TargetEntity.Id);
                                setParsed.Attributes.Add("ddsm_fileparsed", true);
                                _orgService.Update(setParsed);
                                */
                                //StartNewService(_orgService, _thisSettings, false);

                            }

                        }
                        else if (Path.GetExtension(_fileName).ToString().ToLower() == ".xls")
                        {
                            parseError = ParseXLS(_orgService, msFile, objects, parsedRecords, Logger, _thisSettings);
                            if (!parseError)
                            {
                                DataUploader.DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.parsingFileCompleted, _thisSettings.TargetEntity.Id);

                                //Set File Parsed True
                                /*
                                Entity setParsed = new Entity("ddsm_datauploader", _thisSettings.TargetEntity.Id);
                                setParsed.Attributes.Add("ddsm_fileparsed", true);
                                _orgService.Update(setParsed);
                                */

                                //StartNewService(_orgService, _thisSettings, false);

                            }

                        }
                        else
                        {
                            Logger.Add(GetTextError(380003));
                            parseError = true;
                        }

                    }

                }
                if (!parseError)
                {
                    //Logger = new List<string>();
                    Logger.Add("--------------------------------------------------");
                    Logger.Add(GetTextError(380009) + ";" + " *** " + sw.ElapsedMilliseconds / 1000 + " sec");
                    DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");

                    //Start Creator Service
                    StartNewService(_orgService, _thisSettings, false);

                }
                else
                {
                    Logger.Add("--------------------------------------------------");
                    Logger.Add(GetTextError(380023) + ";" + " *** " + sw.ElapsedMilliseconds / 1000 + " sec");
                    DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");
                    DataUploader.DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.parsingFileFailed, _thisSettings.TargetEntity.Id);
                }

            }
            catch (Exception e)
            {
                Logger.Add(GetTextError(380010) + e.Message);
                DataUploader.DataUploader.SaveLog(_orgService, _thisSettings.TargetEntity.Id, Logger, "ddsm_log");
                DataUploader.DataUploader.SetStatusFileDataUploading(_orgService, (int)StatusFileDataUploading.parsingFileFailed, _thisSettings.TargetEntity.Id);

            }

            sw.Stop();
        }

        private void StartNewService(IOrganizationService _orgService, DataUploaderSettings _thisSettings, bool isParser = true)
        {
            bool remoteUploadOfData = false;
            string remoteApiUrl = string.Empty, parserRemoteApiUrl = string.Empty, creatorRemoteApiUrl = string.Empty, websocketUrl = string.Empty;

            try
            {
                QueryExpression query = new QueryExpression
                {
                    EntityName = "ddsm_admindata",
                    ColumnSet = new ColumnSet("ddsm_admindataid", "ddsm_remoteuploadofdata", "ddsm_duremoteapiurl", "ddsm_duremoteapiurl2", "ddsm_duwebsocketurl")
                };

                query.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, "Admin Data");
                query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

                EntityCollection queryRetrieve = _orgService.RetrieveMultiple(query);
                if (queryRetrieve != null && queryRetrieve.Entities.Count == 1)
                {
                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_remoteuploadofdata"))
                        remoteUploadOfData = queryRetrieve[0].GetAttributeValue<bool>("ddsm_remoteuploadofdata");

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_duremoteapiurl"))
                        parserRemoteApiUrl = queryRetrieve[0].GetAttributeValue<string>("ddsm_duremoteapiurl");

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_duremoteapiurl2"))
                        creatorRemoteApiUrl = queryRetrieve[0].GetAttributeValue<string>("ddsm_duremoteapiurl2");

                    if (queryRetrieve[0].Attributes.ContainsKey("ddsm_duwebsocketurl"))
                        websocketUrl = queryRetrieve[0].GetAttributeValue<string>("ddsm_duwebsocketurl");

                    if (remoteUploadOfData && !string.IsNullOrEmpty(parserRemoteApiUrl))
                    {
                        _thisSettings.FileUploaded = true;
                        _thisSettings.FileParsed = true;

                        var json = Newtonsoft.Json.JsonConvert.SerializeObject(_thisSettings);

                        var client = new HttpClient();
                        var content = new StringContent(json, Encoding.UTF8, "application/json");

                        //Creator Api
                        remoteApiUrl = ((!string.IsNullOrEmpty(creatorRemoteApiUrl)) ? creatorRemoteApiUrl : parserRemoteApiUrl) + "RunDataUploaderCreatorWithSettings";

                        var postResult = client.PostAsync(remoteApiUrl, content);
                        postResult.Wait();
                        //var _postResult = postResult.Result.IsSuccessStatusCode;
                    }

                }
            }
            catch (Exception e)
            {
                //
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (parsedRecords != null)
                    {
                        parsedRecords?.Clear();
                    }
                    if (objects != null)
                    {
                        objects?.Clear();
                    }
                }
                this.disposed = true;
            }
        }


    }
}
