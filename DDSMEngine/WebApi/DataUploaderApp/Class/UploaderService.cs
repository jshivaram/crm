﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataUploader;
using DataUploaderApp.Hellper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using static DataUploader.Model.Errors;

namespace DataUploaderApp.Service
{
    internal class UploaderService
    {
        private readonly DataUploaderSettings _thisSettings;
        private readonly CrmConnect _crmConnect;
        private readonly IOrganizationService _orgService;
        private readonly string _usersettingsLogicalName = "usersettings";

        public UploaderService(DataUploaderSettings thisSettings, IOrganizationService OrgService, bool isFullSettings = false) {
            _orgService = OrgService;
            if (!isFullSettings)
            {
                _thisSettings = UpdateSettings(thisSettings);
                
                if (_thisSettings.CurrencyGuid == Guid.Empty)
                {
                    _thisSettings.CurrencyGuid = GetGuidofCurrency(_orgService);
                }
            }
            else {
                _thisSettings = thisSettings;
            }
        }

        /*
        public void Run()
        {

        }
        */

        public DataUploaderSettings GetSettings()
        {
            return _thisSettings;
        }

        public IOrganizationService GetOrgService()
        {
            return _orgService;
        }

        private DataUploaderSettings UpdateSettings(DataUploaderSettings thisSettings)
        {
            List<string> Logger = new List<string>();
            string jsonStr = string.Empty;

            QueryExpression recordUploadedQuery = new QueryExpression
            {
                EntityName = "ddsm_datauploader",
                ColumnSet = new ColumnSet(
                    "ddsm_name"
                    , "ddsm_config"
                    , "ddsm_datauploaderconfiguration"
                    , "ddsm_rownumbersheet"
                    , "ddsm_workingsheet"
                    , "ddsm_processedsheets"
                    , "ddsm_datauploaderesprecalculation"
                    , "ddsm_typeofuploadeddata"
                    , "ddsm_globalrequestscount"
                    , "ddsm_globalpagerecordscount"
                    , "ddsm_startdatarowdu"
                    , "ddsm_creatorrecordtype"
                    , "ddsm_statusfiledatauploading"
                    , "ddsm_deduplicationrules"
                    , "ddsm_duplicatedetection"
                    , "ddsm_fileuploaded"
                    , "ddsm_fileparsed",
                    "ddsm_callexecutemultiple"
                )
            };
            recordUploadedQuery.Criteria = new FilterExpression(LogicalOperator.And);
            recordUploadedQuery.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));
            recordUploadedQuery.Criteria.AddCondition("ddsm_datauploaderid", ConditionOperator.Equal, thisSettings.TargetEntity.Id);
            EntityCollection recordUploadedRetrieve = _orgService.RetrieveMultiple(recordUploadedQuery);

            if (!(recordUploadedRetrieve != null && recordUploadedRetrieve.Entities.Count > 0))
            {
                
                Logger.Add(GetTextError(380001));

                Entity _dataUploader = new Entity("ddsm_datauploader", thisSettings.TargetEntity.Id);
                _dataUploader.Attributes.Add("ddsm_log", string.Join("\n", Logger));
                _orgService.Update(_dataUploader);

                throw new Exception(GetTextError(380001));

            }

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_name"))
                thisSettings.FileName = recordUploadedRetrieve[0].GetAttributeValue<string>("ddsm_name");

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_startdatarowdu"))
                thisSettings.StartRowSheet = (int)recordUploadedRetrieve[0].GetAttributeValue<int>("ddsm_startdatarowdu");

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_typeofuploadeddata"))
                thisSettings.TypeConfig = (int)recordUploadedRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_typeofuploadeddata").Value;

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_globalrequestscount"))
                thisSettings.GlobalRequestsCount = (int)recordUploadedRetrieve[0].GetAttributeValue<int>("ddsm_globalrequestscount");

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_globalpagerecordscount"))
                thisSettings.GlobalPageRecordsCount = (int)recordUploadedRetrieve[0].GetAttributeValue<int>("ddsm_globalpagerecordscount");

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_creatorrecordtype"))
                thisSettings.CreatorRecordType = (int)recordUploadedRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_creatorrecordtype").Value;

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_fileuploaded"))
                thisSettings.FileUploaded = (bool)recordUploadedRetrieve[0].GetAttributeValue<bool>("ddsm_fileuploaded");

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_fileparsed"))
                thisSettings.FileParsed = (bool)recordUploadedRetrieve[0].GetAttributeValue<bool>("ddsm_fileparsed");

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_rownumbersheet"))
                thisSettings.RowNumberSheet = (int)recordUploadedRetrieve[0].GetAttributeValue<int>("ddsm_rownumbersheet");

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_workingsheet"))
                thisSettings.WorkingSheet = (string)recordUploadedRetrieve[0].GetAttributeValue<string>("ddsm_workingsheet");

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_processedsheets"))
                thisSettings.ProcessedSheets = (string)recordUploadedRetrieve[0].GetAttributeValue<string>("ddsm_processedsheets");

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_statusfiledatauploading"))
                thisSettings.StatusFileDataUploading = (int)recordUploadedRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_statusfiledatauploading").Value;

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_deduplicationrules"))
                thisSettings.DedupRules = (int)recordUploadedRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_deduplicationrules").Value;

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_duplicatedetection"))
                thisSettings.DuplicateDetect = (bool)recordUploadedRetrieve[0].GetAttributeValue<bool>("ddsm_duplicatedetection");

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_datauploaderesprecalculation"))
                thisSettings.EspRecalcData = (int)recordUploadedRetrieve[0].GetAttributeValue<OptionSetValue>("ddsm_datauploaderesprecalculation").Value;

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_callexecutemultiple"))
                thisSettings.CallExecuteMultiple = (bool)recordUploadedRetrieve[0].GetAttributeValue<bool>("ddsm_callexecutemultiple");

            if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_config"))
            {
                thisSettings.JsonObjects = (string)recordUploadedRetrieve[0].GetAttributeValue<string>("ddsm_config");
            }
            else
            {
                if (recordUploadedRetrieve[0].Attributes.ContainsKey("ddsm_datauploaderconfiguration") && recordUploadedRetrieve[0].GetAttributeValue<EntityReference>("ddsm_datauploaderconfiguration").Id != null)
                {
                    QueryExpression ConfigJson = new QueryExpression { EntityName = "ddsm_datauploaderconfiguration", ColumnSet = new ColumnSet("ddsm_config") };
                    ConfigJson.Criteria.AddCondition("ddsm_datauploaderconfigurationid", ConditionOperator.Equal, recordUploadedRetrieve[0].GetAttributeValue<EntityReference>("ddsm_datauploaderconfiguration").Id);
                    EntityCollection ConfigJsonRetrieve = _orgService.RetrieveMultiple(ConfigJson);
                    if (ConfigJsonRetrieve != null && ConfigJsonRetrieve.Entities.Count == 1)
                    {
                        thisSettings.JsonObjects = (string)ConfigJsonRetrieve[0].GetAttributeValue<string>("ddsm_config");

                    }
                    else
                    {
                        Logger.Add(GetTextError(380002));
                        Entity _dataUploader = new Entity("ddsm_datauploader", thisSettings.TargetEntity.Id);
                        _dataUploader.Attributes.Add("ddsm_log", string.Join("\n", Logger));
                        _orgService.Update(_dataUploader);

                        throw new Exception(GetTextError(380002));
                    }
                }
                else
                {
                    Logger.Add(GetTextError(380002));
                    Entity _dataUploader = new Entity("ddsm_datauploader", thisSettings.TargetEntity.Id);
                    _dataUploader.Attributes.Add("ddsm_log", string.Join("\n", Logger));
                    _orgService.Update(_dataUploader);

                    throw new Exception(GetTextError(380002));
                }
            }
            thisSettings.TimeZoneInfo = TimeZoneInfo.Utc;
            if (thisSettings.UserGuid != Guid.Empty) {
                string windowsTimeZoneName = GetTimeZoneByCRMConnection(_orgService, thisSettings.UserGuid);
                thisSettings.TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZoneName);
            }

            return thisSettings;
        }

        //Get User TimeZone info
        private string GetTimeZoneByCRMConnection(IOrganizationService _orgService, Guid userId)
        {
            string timeZoneCode = "92"; //UTC
            var query = new QueryExpression(_usersettingsLogicalName);
            query.ColumnSet = new ColumnSet("systemuserid", "timezonecode");
            query.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, userId);

            Entity userSettings = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();
            if (userSettings != null && userSettings.Attributes.ContainsKey("timezonecode"))
            {
                timeZoneCode = userSettings.Attributes["timezonecode"].ToString();
            }
            else
            {
                return TimeZone.CurrentTimeZone.StandardName; //return local
            }

            query = new QueryExpression("timezonedefinition");
            query.ColumnSet = new ColumnSet("timezonecode", "standardname");

            query.Criteria.AddCondition("timezonecode", ConditionOperator.Equal, timeZoneCode);
            Entity timeZoneDefinitions = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            return timeZoneDefinitions.Attributes["standardname"].ToString();
        }

        //Get User Currency Info
        private static Guid GetGuidofCurrency(IOrganizationService _orgService)
        {
            Guid CurrencyGuid = new Guid();

            QueryExpression queryCurrency = new QueryExpression("currency")
            {
                EntityName = "transactioncurrency",
                ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression()
            };
            queryCurrency.Criteria.AddCondition("isocurrencycode", ConditionOperator.Equal, "CAD");
            try
            {
                DataCollection<Entity> entityData = _orgService.RetrieveMultiple(queryCurrency).Entities;
                int count = entityData.Count;

                if (count > 0)
                {
                    CurrencyGuid = (Guid)entityData[0].Id;

                }
                else
                {
                    queryCurrency = new QueryExpression("currency")
                    {
                        EntityName = "transactioncurrency",
                        ColumnSet = new ColumnSet(true),
                        Criteria = new FilterExpression()
                    };
                    entityData = _orgService.RetrieveMultiple(queryCurrency).Entities;
                    count = entityData.Count;
                    if (count > 0)
                        CurrencyGuid = (Guid)entityData[0].Id;
                }
            }
            catch (Exception e)
            {
                return Guid.Empty;
            }

            return CurrencyGuid;
        }

    }
}
