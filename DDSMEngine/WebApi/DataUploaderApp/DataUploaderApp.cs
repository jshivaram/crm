﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
//using Newtonsoft.Json;
using DataUploader;
using DataUploaderApp.Hellper;
using DataUploaderApp.Service;

namespace DataUploaderApp
{
    public class DataUploaderApp
    {

        static void Main(string[] args)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                Console.WriteLine($"***Start Program:{sw.ElapsedMilliseconds/1000}");
                /*
                if (args.Length == 0)
                    throw new ArgumentException("Input params is empty");
                    */
                var program = new DataUploaderApp();
                var thisSettings = new DataUploaderSettings();
                /*
                var input = args[0];
                var jsonImput = JsonConvert.DeserializeObject<AppArg>(input);
                */
                var jsonInput = new AppArg();
                jsonInput.TargetGuid = "BCA8DFC4-232C-E711-811D-080027639BB0";
                jsonInput.UserGuid = "B9776380-0E91-E611-80C7-363834656236";

                if (string.IsNullOrEmpty(jsonInput.TargetGuid))
                    throw new ArgumentException("Input params is empty");

                thisSettings.UserGuid = string.IsNullOrEmpty(jsonInput.UserGuid) ? Guid.Empty : new Guid(jsonInput.UserGuid);
                thisSettings.TargetEntity = new EntityReference("ddsm_datauploader", string.IsNullOrEmpty(jsonInput.TargetGuid) ? Guid.Empty : new Guid(jsonInput.TargetGuid));

                var crmConnect = new CrmConnect();

                program.InitCrmClient(crmConnect);

                program.RunDataUploaderParser(thisSettings, crmConnect.OrgService);
                Console.WriteLine($"***End Program:{sw.ElapsedMilliseconds/1000}");

            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                Console.WriteLine($"{message}");
                throw new FaultException(message);
            }
        }

        //Parser
        public static void startParserApp(DataUploaderSettings thisSettings, IOrganizationService orgService)
        {
            try
            {

                var program = new DataUploaderApp();

                program.RunDataUploaderParser(thisSettings, orgService, true);

            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                throw new FaultException(message);
            }
        }
        public static void startParserApp(AppArg jsonInput, IOrganizationService orgService)
        {
            try
            {
                var program = new DataUploaderApp();
                var thisSettings = new DataUploaderSettings();

                if (string.IsNullOrEmpty(jsonInput.TargetGuid))
                    throw new ArgumentException("*** DataUploader GUID is NULL");
                if (string.IsNullOrEmpty(jsonInput.UserGuid))
                    throw new ArgumentException("*** USER GUID is NULL");

                thisSettings.UserGuid = string.IsNullOrEmpty(jsonInput.UserGuid) ? Guid.Empty : new Guid(jsonInput.UserGuid);
                thisSettings.TargetEntity = new EntityReference("ddsm_datauploader", string.IsNullOrEmpty(jsonInput.TargetGuid) ? Guid.Empty : new Guid(jsonInput.TargetGuid));

                program.RunDataUploaderParser(thisSettings, orgService);

            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                throw new FaultException(message);
            }
        }
        private void RunDataUploaderParser(DataUploaderSettings thisSettings, IOrganizationService _orgService, bool isFullSettings = false)
        {
            if (thisSettings.TargetEntity.Id == Guid.Empty)
                return;
            var _uploaderService = new UploaderService(thisSettings, _orgService, isFullSettings);
            var _parserService = new ParserService(_uploaderService);
            _parserService.Run();
            _parserService.Dispose();
        }

        //Creator
        public static void startCreatorApp(DataUploaderSettings thisSettings, IOrganizationService orgService)
        {
            try
            {
                var program = new DataUploaderApp();

                program.RunDataUploaderCreator(thisSettings, orgService, true);

            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                throw new FaultException(message);
            }
        }
        public static void startCreatorApp(AppArg jsonInput, IOrganizationService orgService)
        {
            try
            {
                var program = new DataUploaderApp();
                var thisSettings = new DataUploaderSettings();

                if (string.IsNullOrEmpty(jsonInput.TargetGuid))
                    throw new ArgumentException("*** DataUploader GUID is NULL");
                if (string.IsNullOrEmpty(jsonInput.UserGuid))
                    throw new ArgumentException("*** USER GUID is NULL");

                thisSettings.UserGuid = string.IsNullOrEmpty(jsonInput.UserGuid) ? Guid.Empty : new Guid(jsonInput.UserGuid);
                thisSettings.TargetEntity = new EntityReference("ddsm_datauploader", string.IsNullOrEmpty(jsonInput.TargetGuid) ? Guid.Empty : new Guid(jsonInput.TargetGuid));

                program.RunDataUploaderCreator(thisSettings, orgService);

            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                throw new FaultException(message);
            }
        }
        private void RunDataUploaderCreator(DataUploaderSettings thisSettings, IOrganizationService _orgService, bool isFullSettings = false)
        {
            if (thisSettings.TargetEntity.Id == Guid.Empty)
                return;
            var _uploaderService = new UploaderService(thisSettings, _orgService, isFullSettings);
            var _creationService = new CreationService(_uploaderService);
            _creationService.Run();
            _creationService.Dispose();
        }



        private void InitCrmClient(CrmConnect crmConnect)
        {
            // Get the CRM connection string and connect to the CRM Organization
            var crmConnectionString = ConfigurationManager.ConnectionStrings["DataUploaderConnection"].ConnectionString;
            var client = new CrmServiceClient(crmConnectionString);
            var orgServiceProxy = client.OrganizationServiceProxy;

            crmConnect.ConnectionStrings = crmConnectionString;
            crmConnect.CrmServiceClient = client;
            crmConnect.OrgService = orgServiceProxy;
        }
    }
}
