﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DmnEngineApp.Core.Dto;
using DmnEngineApp.Core.Interfaces;
using DmnEngineApp.Dto;
using NCalc;

namespace DmnEngineApp.Core.CalculationHelpers
{
    public class FormulaCalculator
    {
        private readonly IDmnDecisionExecutorBase _dmnDecisionExecutorBase;

        public FormulaCalculator(IDmnDecisionExecutorBase dmnDecisionExecutorBase)
        {
            _dmnDecisionExecutorBase = dmnDecisionExecutorBase;
        }

        public string Calculate(string baseStr, string dataType, DmnDto dmnDto)
        {
            try
            {
                //tracer.Trace("formula="+baseStr);
                if (string.IsNullOrEmpty(baseStr))
                    return "";

                baseStr = baseStr.Replace("\r\n", "");
                var crmFunctionsStrList = GetDmnFunctionsStrList(baseStr).Distinct().ToArray();

                var dmnFunctions = GetDmnFunctions(baseStr, crmFunctionsStrList);
                var dmnFunctionsResults = CalculateDmnFunctions(dmnFunctions, dmnDto);

                var resultStr = FormStrFormMathCalculator(baseStr, dmnFunctionsResults);

                if (string.IsNullOrEmpty(resultStr))
                    return $"0";

                if (dataType == "DateTime")
                    return $"{resultStr}";
                var expression = new Expression(resultStr);
                var calculatorResult = expression.Evaluate().ToString();
                IsNan(calculatorResult);

                return calculatorResult;
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }

        private void IsNan(string calculatorResult)
        {
            if (calculatorResult == "NaN")
                throw new Exception("Cannot calculate the formula! Please check out  the formula! Error: NaN");
        }

        protected IEnumerable<string> GetDmnFunctionsStrList(string baseStr)
        {
            var crmFunctionsStrList = Regex.Matches(baseStr, "(?<=\\{\\{)(.*?)(?=\\}\\})")
                .Cast<Match>()
                .Select(m => m.Value)
                .ToList();
            return crmFunctionsStrList;
        }

        protected IEnumerable<DmnFunctionDTO> GetDmnFunctions(string baseStr, IEnumerable<string> crmFunctionsStrList)
        {
            var dmnFunctions = new List<DmnFunctionDTO>();

            var previousPosition = -1;

            foreach (var crmFunctionStr in crmFunctionsStrList)
            {
                var dmnFunction = new DmnFunctionDTO
                {
                    Base = crmFunctionStr,
                    Position = GetDmnFunctionPosition(baseStr, crmFunctionStr, previousPosition)
                };

                previousPosition = dmnFunction.Position;
                dmnFunctions.Add(dmnFunction);
            }
            return dmnFunctions;
        }

        protected int GetDmnFunctionPosition(string baseStr, string crmFunctionStr, int previousPosition)
        {
            var startIndex = previousPosition == -1 ? 0 : previousPosition;
            var result = baseStr.IndexOf(crmFunctionStr, startIndex);
            return result;
        }

        protected IEnumerable<DmnFunctionDTOResult> CalculateDmnFunctions(IEnumerable<DmnFunctionDTO> dmnFunctions,
            DmnDto dmnDto)
        {
            var dmnFunctionsResults = new List<DmnFunctionDTOResult>();
            foreach (var dmnFunction in dmnFunctions)
            {
                var result = _dmnDecisionExecutorBase.Run(dmnFunction?.Base, dmnDto);
                var dmnFunctionResult = new DmnFunctionDTOResult
                {
                    DmnFunction = dmnFunction,
                    Result = result
                };
                dmnFunctionsResults.Add(dmnFunctionResult);
            }
            return dmnFunctionsResults;
        }

        protected string FormStrFormMathCalculator(string baseStr, IEnumerable<DmnFunctionDTOResult> dmnFunctionsResults)
        {
            var resultStr = baseStr;

            foreach (var dmnFunctionResult in dmnFunctionsResults)
            {
                var dmnFunction = dmnFunctionResult.DmnFunction;
                resultStr = resultStr.Replace(dmnFunction.Base, "");
                //resultStr = resultStr.Remove(dmnFunction.Position, dmnFunction.Base.Count());

                //int bufSpaceCount = dmnFunction.Base.Count() - dmnFunctionResult.Result.ToString().Count() + 4;
                //resultStr = resultStr.Replace("{{}}", dmnFunctionResult.Result.ToString() + FormBufSpaces(bufSpaceCount));
                resultStr = resultStr.Replace("{{}}", dmnFunctionResult.Result);
            }

            resultStr = resultStr.Replace(" ", "");

            return resultStr;
        }

        protected string FormBufSpaces(int count)
        {
            var result = "";
            for (var i = 0; i < count; i++)
                result += " ";
            return result;
        }
    }
}