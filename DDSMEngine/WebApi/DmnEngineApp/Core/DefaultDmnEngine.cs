﻿using System.Linq;
using System.Xml;
using DmnEngineApp.Core.Dto;
using DmnEngineApp.Core.Dto.Rule;

namespace DmnEngine.Core

{
    public class DefaultDmnEngine
    {
        private readonly DecisionTableDto _decisionTableDto;
        private readonly string _decisionTableXml;
        private DecisionRuleDto _decisionRuleDto;
        //private static int _inputCounter;
        //private static int _outputCounter;
        private int _inputCounter;
        private int _outputCounter;

        public DefaultDmnEngine(string decisionTable)
        {
            _decisionTableXml = decisionTable;
            _decisionTableDto = new DecisionTableDto();
            _inputCounter = 0;
            _outputCounter = 0;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public DecisionTableDto ParseDecision()
        {
            var doc = new XmlDocument();
            doc.LoadXml(_decisionTableXml);
            var xRoot = doc.DocumentElement;
            var decision = GetDecision(xRoot);
            if (decision == null)
                return null;
            var decisionTable = GetDecisionTable(decision);
            if (decisionTable == null)
                return null;
            foreach (XmlNode decisionItem in decisionTable.ChildNodes)
                if (decisionItem != null)
                    PopulateDecisionTableDto(decisionItem);
            return _decisionTableDto;
        }

        /// <summary>
        /// </summary>
        /// <param name="decisionItem"></param>
        private void PopulateDecisionTableDto(XmlNode decisionItem)
        {
            if (decisionItem == null)
                return;

            switch (decisionItem.Name)
            {
                case "decision":
                    AddDecisionAttr(decisionItem);
                    break;
                case "decisionTable":
                    AddDecisionTableAttr(decisionItem);
                    break;
                case "input":
                    AddInputExpression(decisionItem);
                    break;
                case "output":
                    AddOutputExpression(decisionItem);
                    break;
                case "rule":
                    AddRule(decisionItem);
                    break;
                case "inputEntry":
                    AddInputEntry(decisionItem);
                    break;
                case "outputEntry":
                    AddOutputEntry(decisionItem);
                    break;
            }
        }


        private void AddDecisionAttr(XmlNode decision)
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="decisionTable"></param>
        private void AddDecisionTableAttr(XmlNode decisionTable)
        {
            if (decisionTable.Attributes == null) return;
            var id = decisionTable.Attributes.GetNamedItem("id");
            if (id != null)
                _decisionTableDto.Id = id.Value;
        }

        /// <summary>
        /// </summary>
        /// <param name="input"></param>
        private void AddInputExpression(XmlNode input)
        {
            if (input.Attributes == null) return;
            var id = input.Attributes.GetNamedItem("id");
            var label = input.Attributes.GetNamedItem("label");
            if (id != null)
            {
                var inputClause = new InputClauseDto();
                inputClause.Id = id.Value;
                if (label != null)
                    inputClause.Field = label.Value;
                foreach (XmlNode inputExpression in input.ChildNodes)
                {
                    var inputExpressionId = inputExpression.Attributes.GetNamedItem("id");
                    var inputExpressionTpe = inputExpression.Attributes.GetNamedItem("typeRef");
                    var inputExpressionDto = new InputExpressionDto();
                    inputExpressionDto.Id = inputExpressionId.Value;
                    inputExpressionDto.TypeRef = inputExpressionTpe.Value;

                    inputClause.InputExpression = inputExpressionDto;
                }

                _decisionTableDto.Input.Add(inputClause);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="output"></param>
        private void AddOutputExpression(XmlNode output)
        {
            if (output.Attributes == null) return;
            var id = output.Attributes.GetNamedItem("id");
            var label = output.Attributes.GetNamedItem("label");
            var name = output.Attributes.GetNamedItem("name");
            var typeRef = output.Attributes.GetNamedItem("typeRef");

            if (id != null)
            {
                var outputClause = new OutputClauseDto();
                outputClause.Id = id.Value;
                if (label != null)
                    outputClause.Label = label.Value;
                if (name != null)
                    outputClause.Name = name.Value;
                if (typeRef != null)
                    outputClause.TypeRef = typeRef.Value;

                _decisionTableDto.Output.Add(outputClause);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="rule"></param>
        private void AddRule(XmlNode rule)
        {
            if (rule.Attributes == null) return;
            var id = rule.Attributes.GetNamedItem("id");
            _decisionRuleDto = new DecisionRuleDto();
            if (id != null)
                _decisionRuleDto.Id = id.Value;
            _inputCounter = _outputCounter = 0;

            foreach (XmlNode decisionItem in rule.ChildNodes)
                if (decisionItem != null)
                    PopulateDecisionTableDto(decisionItem);
            _decisionTableDto.DecisionRule.Add(_decisionRuleDto);
        }

        /// <summary>
        /// </summary>
        /// <param name="input"></param>
        private void AddInputEntry(XmlNode input)
        {
            if (input.Attributes == null) return;
            var id = input.Attributes.GetNamedItem("id");
            var inputEntry = new InputEntryDto();
            if (id != null)
                inputEntry.Id = id.Value;
            foreach (XmlNode inputText in input.ChildNodes)
            {
                if (inputText != null)
                    inputEntry.Value = inputText.InnerText;
                inputEntry.DataType = _decisionTableDto.Input[_inputCounter].InputExpression.TypeRef;
                inputEntry.Field = _decisionTableDto.Input[_inputCounter].Field;
            }
            _decisionRuleDto.InputEntries.Add(inputEntry);
            _inputCounter++;
        }

        /// <summary>
        /// </summary>
        /// <param name="output"></param>
        private void AddOutputEntry(XmlNode output)
        {
            if (output.Attributes == null) return;
            var id = output.Attributes.GetNamedItem("id");
            var outputEntry = new OutputEntryDto();
            if (id != null)
                outputEntry.Id = id.Value;

            outputEntry.OutputFullPath = _decisionTableDto.Output[_outputCounter].Label;
            outputEntry.DataType = _decisionTableDto.Output[_outputCounter].TypeRef;
            foreach (XmlNode outputText in output.ChildNodes)
                if (outputText != null)
                    outputEntry.Value = outputText.InnerText;
            _outputCounter++;
            _decisionRuleDto.OutputEntries.Add(outputEntry);
        }

        /// <summary>
        /// </summary>
        /// <param name="decision"></param>
        /// <returns></returns>
        private XmlNode GetDecisionTable(XmlNode decision)
        {
            if (decision == null)
                return null;
            var decisionTable = decision.Cast<XmlNode>().FirstOrDefault();
            PopulateDecisionTableDto(decisionTable);
            return decisionTable;
        }

        /// <summary>
        /// </summary>
        /// <param name="xRoot"></param>
        /// <returns></returns>
        private XmlNode GetDecision(XmlElement xRoot)
        {
            if (xRoot == null)
                return null;
            var decision = xRoot.Cast<XmlNode>().FirstOrDefault();
            return decision;
        }

        public int GetInputCount()
        {
            return _decisionRuleDto.InputEntries.Count;
        }

        public int GetOutputCount()
        {
            return _decisionRuleDto.OutputEntries.Count;
        }
    }
}