﻿using System;
using System.Linq.Expressions;

namespace DmnEngineApp.Core.DmnExpression.Boolean
{
    internal class IsTrueFalse : BooleanExpression
    {
        public IsTrueFalse(string op, bool sourceValue, bool[] exprValue) : base(op, sourceValue, exprValue)
        {
        }

        /// <summary>
        ///     Returns itself when evaluating.
        /// </summary>
        /// <returns></returns>
        protected override bool Evaluate()
        {
            var sourceValue = _sourceValue;
            var exprValue = _exprValue[0];
            Expression equal = Expression.Equal(
                Expression.Constant(sourceValue),
                Expression.Constant(exprValue)
            );
            return Expression.Lambda<Func<bool>>(equal).Compile()();
        }

        protected override int GetParamCount()
        {
            return 1;
        }


        protected override void ValidateOperator()
        {
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        protected override void ValidateParameters()
        {
        }
    }
}