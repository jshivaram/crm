﻿using System;
using System.Linq.Expressions;

namespace DmnEngineApp.Core.DmnExpression.Date
{
    internal class Between : DateExpression
    {
        public Between(string op, DateTime sourceValue, string[] exprValue) : base(op, sourceValue, exprValue)
        {
        }

        /// <summary>
        ///     Returns itself when evaluating.
        /// </summary>
        /// <returns></returns>
        protected override bool Evaluate()
        {
            var sourceValue = _sourceValue;
            return GreaterThan(sourceValue, Convert.ToDateTime(_exprValue[0])) &&
                   LessThan(sourceValue, Convert.ToDateTime(_exprValue[1]));
        }

        private bool GreaterThan(DateTime input1, DateTime input2)
        {
            var sourceValue = _sourceValue;
            var exprValue = _exprValue[0];
            Expression greaterThan = Expression.GreaterThan(
                Expression.Constant(input1),
                Expression.Constant(input2)
            );
            return Expression.Lambda<Func<bool>>(greaterThan).Compile()();
        }

        private bool LessThan(DateTime input1, DateTime input2)
        {
            Expression lessThanExpr = Expression.LessThan(
                Expression.Constant(input1),
                Expression.Constant(input2)
            );
            return Expression.Lambda<Func<bool>>(lessThanExpr).Compile()();
        }

        protected override int GetParamCount()
        {
            return 2;
        }


        protected override void ValidateOperator()
        {
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        protected override void ValidateParameters()
        {
        }
    }
}