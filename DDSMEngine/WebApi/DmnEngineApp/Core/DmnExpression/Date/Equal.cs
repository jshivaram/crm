﻿using System;
using System.Linq.Expressions;

namespace DmnEngineApp.Core.DmnExpression.Date
{
    internal class Equal : DateExpression
    {
        public Equal(string op, DateTime sourceValue, string[] exprValue) : base(op, sourceValue, exprValue)
        {
        }

        /// <summary>
        ///     Returns itself when evaluating.
        /// </summary>
        /// <returns></returns>
        protected override bool Evaluate()
        {
            var sourceValue = _sourceValue.ToUniversalTime();
            var exprValue = Convert.ToDateTime(_exprValue[0]).ToUniversalTime();
            Expression equal = Expression.Equal(
                Expression.Constant(sourceValue),
                Expression.Constant(exprValue)
            );
            return Expression.Lambda<Func<bool>>(equal).Compile()();
        }

        protected override int GetParamCount()
        {
            return 1;
        }


        protected override void ValidateOperator()
        {
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        protected override void ValidateParameters()
        {
        }
    }
}