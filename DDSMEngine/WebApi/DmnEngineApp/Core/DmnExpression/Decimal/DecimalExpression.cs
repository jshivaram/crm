﻿namespace DmnEngineApp.Core.DmnExpression.Decimal
{
    internal class DecimalExpression : AbstractExpression
    {
        protected decimal[] _exprValue;

        private DecimalExpression _numericExpression;
        protected string _operator;
        protected decimal _sourceValue;

        public DecimalExpression(string op, decimal sourceValue, decimal[] exprValue)
        {
            _sourceValue = sourceValue;
            _exprValue = exprValue;
            _operator = op;
        }

        public override bool GetResult()
        {
            InitOperator();
            return Evaluate();
        }

        protected override void InitOperator()
        {
            switch (_operator)
            {
                case "$equals":
                    _numericExpression = new Equal(_operator, _sourceValue, _exprValue);
                    break;
                case "$not_equals":
                    _numericExpression = new NotEqual(_operator, _sourceValue, _exprValue);
                    break;
                case "$gt":
                    _numericExpression = new GreaterThan(_operator, _sourceValue, _exprValue);
                    break;
                case "$gte":
                    _numericExpression = new GreaterThanOrEqual(_operator, _sourceValue, _exprValue);
                    break;
                case "$lt":
                    _numericExpression = new LessThan(_operator, _sourceValue, _exprValue);
                    break;
                case "$lte":
                    _numericExpression = new LessThan(_operator, _sourceValue, _exprValue);
                    break;
                case "$in":
                    _numericExpression = new In(_operator, _sourceValue, _exprValue);
                    break;
                case "$between":
                    _numericExpression = new Between(_operator, _sourceValue, _exprValue);
                    break;
            }
        }

        protected virtual bool Evaluate()
        {
            if (_numericExpression == null)
                return false;
            _numericExpression.ValidateOperator();
            _numericExpression.ValidateParameters();
            return _numericExpression.Evaluate();
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        /// <returns></returns>
        protected virtual void ValidateParameters()
        {
        }


        protected virtual void ValidateOperator()
        {
        }
    }
}