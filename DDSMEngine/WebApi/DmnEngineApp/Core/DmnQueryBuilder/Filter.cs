﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DmnEngineApp.Core.DmnQueryBuilder
{
    public class Filter
    {
        public Filter(string filterType = "and")
        {
            Conditions = new List<FetchXMLCondition>();
            Type = filterType;
        }

        public List<FetchXMLCondition> Conditions { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }
}