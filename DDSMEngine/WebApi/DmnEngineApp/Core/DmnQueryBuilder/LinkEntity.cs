﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DmnEngineApp.Core.DmnQueryBuilder
{
    public class LinkEntity
    {
        [XmlAttribute("link-entity")] public List<LinkEntity> FetchLinkEntity;

        public LinkEntity()
        {
            LinkType = "inner";
            RelatedEntity = new Dictionary<string, LinkEntity>();
            Filters = new List<Filter>();
            FetchLinkEntity = new List<LinkEntity>();
        }

        [XmlAttribute("alias")]
        public string Alias { get; set; }

        [XmlAttribute("from")]
        public string From { get; set; }

        [XmlAttribute("link-type")]
        public string LinkType { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("to")]
        public string To { get; set; }

        [XmlAttribute("filter")]
        public List<Filter> Filters { get; set; }

        [XmlIgnore]
        public Dictionary<string, LinkEntity> RelatedEntity { get; set; }
    }
}