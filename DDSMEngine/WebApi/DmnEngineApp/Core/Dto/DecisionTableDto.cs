﻿using System.Collections.Generic;
using DmnEngineApp.Core.Dto.Rule;

namespace DmnEngineApp.Core.Dto
{
    public class DecisionTableDto
    {
        public DecisionTableDto()
        {
            Input = new List<InputClauseDto>();
            Output = new List<OutputClauseDto>();
            DecisionRule = new List<DecisionRuleDto>();
        }

        public string Id { get; set; }

        public string HitPolicy { get; set; }

        public List<InputClauseDto> Input { get; set; }

        public List<OutputClauseDto> Output { get; set; }

        public List<DecisionRuleDto> DecisionRule { get; set; }
    }
}