﻿namespace DmnEngineApp.Core.Dto
{
    public class DmnFunctionDTO
    {
        public string Base { get; set; }
        public int Position { get; set; }
    }
}