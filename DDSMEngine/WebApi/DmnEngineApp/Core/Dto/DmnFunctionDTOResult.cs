﻿namespace DmnEngineApp.Core.Dto
{
    public class DmnFunctionDTOResult
    {
        public DmnFunctionDTO DmnFunction { get; set; }
        public string Result { get; set; }
    }
}