﻿using System;
using System.Collections.Generic;
using DmnEngineApp.Dto;

namespace DmnEngineApp.Core.Executor.Function.DateTime
{
    public class AddYears : DmnFunction
    {
        public AddYears(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            args = GetValue(_dmnFunction, args);
            var date = System.DateTime.Parse(args[0]);
            var years = Convert.ToInt32(args[1]);
            date = date.AddYears(years);
            return date.ToString("MM/dd/yyyy");
        }

        private static string[] GetValue(DmnFunction _dmnFunction, string[] args)
        {
            var tmpList = new List<string>();
            foreach (var item in args)
            {
                if (item.IndexOf('.') != -1)
                {
                    var val = _dmnFunction.GetSourceValue(item)?.ToString() ?? item;
                    tmpList.Add(val);
                    continue;
                }
                if (item.ToLower() == "now")
                {
                    var now = System.DateTime.Now;
                    var nowStr = now.ToString("MM/dd/yyyy");
                    tmpList.Add(nowStr);
                    continue;
                }
                tmpList.Add(item);
            }
            return tmpList.ToArray();
        }
    }
}