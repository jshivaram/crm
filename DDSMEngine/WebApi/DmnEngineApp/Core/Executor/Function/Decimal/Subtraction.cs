﻿using System;
using System.Linq.Expressions;
using DmnEngineApp.Dto;

namespace DmnEngineApp.Core.Executor.Function.Decimal
{
    public class Subtraction : DmnFunction
    {
        public Subtraction(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            args = _dmnFunction.GetExpressionValue(args);


            //NOTE!!!!
            decimal x = 0;
            decimal y = 0;

            switch (args.Length)
            {
                case 1:
                    x = Convert.ToDecimal(args[0]);
                    break;
                case 2:
                    x = Convert.ToDecimal(args[0]);
                    y = Convert.ToDecimal(args[1]);
                    break;
            }

            var xParam = Expression.Parameter(typeof(decimal), "x");
            var yParam = Expression.Parameter(typeof(decimal), "y");
            Expression negative = Expression.Negate(yParam);
            Expression sum = Expression.Add(xParam, negative);
            var lambdaExpression = Expression.Lambda(sum, xParam, yParam);
            var newLambda = (Func<decimal, decimal, decimal>) lambdaExpression.Compile();

            return newLambda(x, y).ToString();
        }
    }
}