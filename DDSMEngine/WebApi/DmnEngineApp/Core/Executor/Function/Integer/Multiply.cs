﻿using System.Linq;
using DmnEngineApp.Dto;

namespace DmnEngineApp.Core.Executor.Function.Integer
{
    internal class Multiply : DmnFunction
    {
        public Multiply(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            args = _dmnFunction.GetExpressionValue(args);
            var res = 1;

            for (var i = 0; i < args.Count(); i++)
                res *= int.Parse(args[i]);

            return res.ToString();
        }
    }
}