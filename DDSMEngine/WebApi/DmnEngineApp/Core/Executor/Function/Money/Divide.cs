﻿using System;
using DmnEngineApp.Dto;

namespace DmnEngineApp.Core.Executor.Function.Money
{
    internal class Divide : DmnFunction
    {
        public Divide(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            args = _dmnFunction.GetExpressionValue(args);
            var rs = Convert.ToDecimal(args[0]) / Convert.ToDecimal(args[1]);
            return rs.ToString();
        }
    }
}