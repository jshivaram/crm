﻿using System;
using System.Linq;
using DmnEngineApp.Dto;

namespace DmnEngineApp.Core.Executor.Function.Money
{
    internal class Sum : DmnFunction
    {
        public Sum(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            args = _dmnFunction.GetExpressionValue(args);
            var sum = args.Sum(item => Convert.ToDecimal(item));
            return sum.ToString();
        }
    }
}