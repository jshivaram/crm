﻿using System.Linq;
using DmnEngineApp.Core.DmnQueryBuilder;
using DmnEngineApp.Dto;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace DmnEngineApp.Core.Executor.Function.Relationship
{
    internal class Count : AggregateFunction
    {
        public Count(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args, Entity targetEntity)
        {
            
            if (args.Count() == 0 || args.FirstOrDefault() == null)
                return string.Empty;

            var arg = args.First();
            var argsArr = arg.Split('\\');

            var select = argsArr.FirstOrDefault();
            if (select != null)
            {
                var selectArr = select.Split('.');
                AddMainEntity(selectArr);
            }

            if (argsArr.Length > 1)
                BuildConditions(argsArr.Skip(1).ToArray(), targetEntity);

            var fetchXML = CreateFetchXML();

            var query = new FetchExpression(fetchXML);

            var totals = _dmnDto.DataService.RetrieveMultipleQuery(query);
            if (totals.Entities.Count == 0)
            {
                SetResult("");
                return string.Empty;
            }

            return (totals.Entities[0].Attributes["rs"] as AliasedValue).Value.ToString();
        }

        public void AddMainEntity(string[] select)
        {
            if (select == null || select.Length < 1)
                throw new InvalidPluginExecutionException(
                    "Count cannot be calculated. Please check the arguments of the functions");
            var initEntity = select.FirstOrDefault();
            var att = select.LastOrDefault();
            for (var i = 1; i < select.Length; i++)
            {
                var field = select[i];
                var fieldMetadata = _dataService.GetAttributeMetadata(initEntity, field);
                if (IsAttributeLookup(fieldMetadata.FirstOrDefault()))
                    continue;
            }
            _mainEntity = new MainEntity(initEntity);

            var fetchAtt = new FetchXMLAttribute();
            fetchAtt.Name = initEntity + "id";
            fetchAtt.Aggregate = "count";
            fetchAtt.Alias = "rs";

            _mainEntity.Attributes.Add(fetchAtt);
        }
    }
}