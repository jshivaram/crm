﻿using System;

namespace DmnEngineApp.Core.Executor.Function.String
{
    internal class Executor : IExecutor
    {
        public string PerformOperation(string op, string[] args, DmnFunction dmnFunction)
        {
            return Execute(op, args, dmnFunction);
        }


        private string Execute(string op, string[] args, DmnFunction dmnFunction)
        {
            try
            {
                switch (op)
                {
                    case "concat":
                        var concat = new Concat(dmnFunction._dmnDto);
                        return concat.Evaluate(args);
                }
            }

            catch (Exception e)
            {
            }
            return string.Empty;
        }
    }
}