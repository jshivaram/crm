﻿namespace DmnEngineApp.Core.Interfaces
{
    public interface ICoreCalculator
    {
        string Calculate(string func);
    }
}