﻿namespace DmnEngineApp.Core.Interfaces
{
    public interface IFormulaCalculator
    {
        string Calculate(string baseStr);
    }
}