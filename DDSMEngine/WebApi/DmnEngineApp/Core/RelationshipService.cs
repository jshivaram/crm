﻿using System;
using System.Collections.Generic;
using System.Linq;
using DmnEngineApp.Dto;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;

namespace DmnEngineApp.Core
{
    internal class RelationshipService
    {
        private readonly DmnDto _dmnDto;

        public RelationshipService(DmnDto dmnDto, string linkName)
        {
            _dmnDto = dmnDto;
            _linkName = linkName;
        }

        private string _linkName { get; }


        /// <summary>
        ///     GetOneToManyRelationshipsMetadata
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Entity> GetOneToManyRelationshipsMetadata()
        {
            var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = _dmnDto.TargetEntity.LogicalName
            };

            var retrieveBankEntityResponse =
                (RetrieveEntityResponse) _dmnDto.CrmDto.OrgService.Execute(retrieveBankAccountEntityRequest);

            var oneToNRelationships = retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships;
            var rs =
                oneToNRelationships.Where(r => r.ReferencedEntityNavigationPropertyName == _linkName).ToList();
            return rs.Count != 0 ? GetOToMEntities(rs[0]) : null;
        }

        /// <summary>
        ///     GetManyToOneRelationshipsMetadata
        /// </summary>
        /// <returns></returns>
        public Entity GetManyToOneRelationshipsMetadata()
        {
            var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = _dmnDto.TargetEntity.LogicalName
            };

            var retrieveBankEntityResponse =
                (RetrieveEntityResponse) _dmnDto.CrmDto.OrgService.Execute(retrieveBankAccountEntityRequest);

            var manyToOneRelationships = retrieveBankEntityResponse.EntityMetadata.ManyToOneRelationships;
            var rs =
                manyToOneRelationships.Where(r => r.ReferencingAttribute == _linkName).ToList();
            return rs.Count != 0 ? GetMToOEntities(rs[0]) : null;
        }

        private IEnumerable<Entity> GetOToMEntities(OneToManyRelationshipMetadata relationshipMetadata)
        {
            var entities = new List<Entity>();
            var pageCount = 5000;
            var pageNumber = 1;
            var isRsNotEmpty = true;
            var orgService = _dmnDto.CrmDto.OrgService;

            while (isRsNotEmpty)
            {
                var query = new QueryExpression(relationshipMetadata.ReferencingEntity)
                {
                    ColumnSet = new ColumnSet()
                };
                // Assign the pageinfo properties to the query expression.
                query.PageInfo = new PagingInfo();
                query.PageInfo.Count = pageCount;
                query.PageInfo.PageNumber = pageNumber;
                AddOnlyActiveRecords(query);
                AddRelationCondition(query, relationshipMetadata.ReferencingAttribute, _dmnDto.TargetEntity.Id);
                var rs = orgService.RetrieveMultiple(query).Entities;
                if (rs.Count == 0)
                    isRsNotEmpty = false;
                entities.AddRange(rs);
                pageNumber++;
            }
            return entities;
        }

        private Entity GetMToOEntities(OneToManyRelationshipMetadata relationshipMetadata)
        {
            var orgService = _dmnDto.CrmDto.OrgService;
            var rs = orgService.Retrieve(_dmnDto.TargetEntity.LogicalName, _dmnDto.TargetEntity.Id,
                new ColumnSet(relationshipMetadata.ReferencingAttribute));
            object referecingEntity = null;
            if (rs.Attributes.TryGetValue(relationshipMetadata.ReferencingAttribute, out referecingEntity))
            {
                var refEntity = (EntityReference) referecingEntity;
                return orgService.Retrieve(refEntity.LogicalName, refEntity.Id, new ColumnSet(true));
            }
            return null;
        }

        private static void AddOnlyActiveRecords(QueryExpression query)
        {
            query.Criteria.AddCondition(
                "statecode",
                ConditionOperator.Equal,
                0);
        }

        private static void AddRelationCondition(QueryExpression query, string relationAttribute, Guid targetEntityId)
        {
            query.Criteria.AddCondition(
                relationAttribute,
                ConditionOperator.Equal,
                targetEntityId);
        }
    }
}