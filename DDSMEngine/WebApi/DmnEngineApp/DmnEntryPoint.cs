﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using DmnEngineApp.Dto;
using DmnEngineApp.Service;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using Serilog;
using SocketClients.Dto;

namespace DmnEngineApp
{
    public class DmnEntryPoint
    {
        public static bool IsDmnEngineFree = true;

        public DmnEntryPoint(InputArgsDto inputArgsDto)
        {
            InputArgsDto = inputArgsDto;
        }


        public DmnDto DmnDto { get; private set; }
        private InputArgsDto InputArgsDto { get; }

        /// <summary>
        ///     DMN engine main entry point
        /// </summary>
        /// <param name="orgService"></param>
        public void LaunchDmn(IOrganizationService orgService)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                var inputArgsDto = InputArgsDto;
                var request = JsonConvert.SerializeObject(inputArgsDto);
                Log.Debug($" LaunchDmn= {request}");
                if (inputArgsDto == null)
                    throw new ArgumentException("Input params is empty");
                var dmnEntryPoint = this;

                //create and init Dmn Dto
                var dmnDto = new DmnDto();

                //Create and Init Crm Client
                var crmDto = new CrmDto {OrgService = orgService};
                dmnDto.CrmDto = crmDto;

                dmnDto.InputArgs = inputArgsDto;
                IsDmnEngineFree = false;
                //Launch DMN engine
                dmnEntryPoint.RunDmnEngine(dmnDto);
                IsDmnEngineFree = true;
                DmnDto = dmnDto;
                var wsMessageService =
                    new WSMessageService(new WsCreateMsgDto
                    {
                        UserId = dmnDto.InputArgs.UserId,
                        EntityLogicalName = dmnDto.InputArgs.TargetEntityName
                    });
                Log.Debug($"Async DMN Calculation is finish.  Execute time:{sw.Elapsed}");
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                throw new FaultException(message);
            }
        }

        /// <summary>
        ///     Run DMN
        /// </summary>
        /// <param name="dmnDto"></param>
        private void RunDmnEngine(DmnDto dmnDto)
        {
            if (dmnDto.InputArgs.RecordIds.Count == 0)
                return;
            //create dmn decision service
            var dmnDecisionService = new DmnDecisionService(dmnDto);
            dmnDecisionService.Run();
        }
    }
}