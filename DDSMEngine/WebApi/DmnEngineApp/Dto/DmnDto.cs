﻿using System;
using System.Collections.Concurrent;
using DmnEngineApp.Service;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;

namespace DmnEngineApp.Dto
{
    public class DmnDto
    {
        //public static ConcurrentDictionary<Guid, Entity> EntityCache { get; set; }
        //public static ConcurrentDictionary<string, EntityMetadata> EntityMetadata { get; set; }
        //public static ConcurrentDictionary<string, string> EntityDisplayName { get; set; }


        public  ConcurrentDictionary<Guid, Entity> EntityCache = new ConcurrentDictionary<Guid, Entity>();

        public  ConcurrentDictionary<string, EntityMetadata> EntityMetadata =
            new ConcurrentDictionary<string, EntityMetadata>();

        public  ConcurrentDictionary<string, string> EntityDisplayName =
            new ConcurrentDictionary<string, string>();

        public EntityCollection BusinessRules;

        public ConcurrentDictionary<Guid, Entity> OutputEntities = new ConcurrentDictionary<Guid, Entity>();

        public DmnDto()
        {
            TargetEntity = null;

            BusinessRules = new EntityCollection();
            EntityMetadata = new ConcurrentDictionary<string, EntityMetadata>();
            EntityCache = new ConcurrentDictionary<Guid, Entity>();
            EntityDisplayName = new ConcurrentDictionary<string, string>();

            //EntityMetadata = new Dictionary<string, EntityMetadata>();
            //EntityCache = new Dictionary<Guid, Entity>();
            DataService = null;
            ResultDto = new ResultDto();
            OutputEntities = new ConcurrentDictionary<Guid, Entity>();
        }

        public InputArgsDto InputArgs { get; set; }
        public CrmDto CrmDto { get; set; }
        public Entity TargetEntity { get; set; }

        //public ConcurrentDictionary<Guid, Entity> EntityCache { get; set; }
        //public ConcurrentDictionary<string, EntityMetadata> EntityMetadata { get; set; }
        //public ConcurrentDictionary<string, string> EntityDisplayName { get; set; }

        //public Dictionary<Guid, Entity> EntityCache { get; set; }
        //public Dictionary<string, EntityMetadata> EntityMetadata { get; set; }
        public DataService DataService { get; set; }
        public ResultDto ResultDto { get; set; }
    }
}