﻿using System;
using System.Collections.Generic;

namespace DmnEngineApp.Dto
{
    public class InputArgsDto
    {
        public InputArgsDto()
        {
            BusinessRuleIds = new List<string>();
            IsExecuteRule = false;
            RecordIds = new List<string>();
            CallBackRecordId = Guid.Empty;
            IsDataUploader = false;
        }

        public string TargetEntityName { get; set; }
        public List<string> RecordIds { get; set; }

        public List<string> BusinessRuleIds { get; set; }

        //for callback
        public string CallBackWorkflowId { get; set; }
        public Guid CallBackRecordId { get; set; }
        public Guid UserId { get; set; }
        public bool IsExecuteRule { get; set; }
        public bool IsDataUploader { get; set; }
    }
}