﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using DmnEngineApp.Dto;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using Newtonsoft.Json;

namespace DmnEngineApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var sw = Stopwatch.StartNew();
            Console.WriteLine($"***Start Program:{sw.Elapsed}");
            try

            {
                if (args.Length == 0)
                    throw new ArgumentException("Input params is empty");
                var program = new Program();

                //create and init Dmn Dto
                var dmnDto = new DmnDto();
                program.InitCrmClient(dmnDto);
                dmnDto.InputArgs = program.ParseInputArgs(args);
                var dmnEntryPoint = new DmnEntryPoint(dmnDto.InputArgs);
                dmnEntryPoint.LaunchDmn(dmnDto.CrmDto.OrgService);

                //DmnDto.EntityCache.Clear();
                //DmnDto.EntityDisplayName.Clear();
                //DmnDto.EntityMetadata.Clear();
                //program.TestUpdateRC(dmnDto);
                Console.WriteLine($"***End Program:{sw.Elapsed}");
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                Console.WriteLine($"{message}");
                throw new FaultException(message);
            }
        }

        public void TestUpdateRC(DmnDto dmnDto)
        {
            var columns = new ColumnSet("ddsm_kwhgrosssavingsatmeter");


            var entities = new List<Entity>();
            var pageCount = 5000;
            var pageNumber = 1;
            var isRsNotEmpty = true;
            while (isRsNotEmpty)
            {
                var query = new QueryExpression("ddsm_rateclass")
                {
                    ColumnSet = new ColumnSet()
                };
                // Assign the pageinfo properties to the query expression.
                query.PageInfo = new PagingInfo();
                query.PageInfo.Count = pageCount;
                query.PageInfo.PageNumber = pageNumber;

                var rs = dmnDto.CrmDto.OrgService.RetrieveMultiple(query).Entities;
                if (rs.Count == 0)
                    isRsNotEmpty = false;
                entities.AddRange(rs);
                pageNumber++;
            }
            //CreateBulkUpdateRequest(entities, dmnDto);
            CreateUpdateRequest(entities, dmnDto);
        }

        public void CreateBulkUpdateRequest(List<Entity> entities, DmnDto dmnDto)
        {
            var sw = Stopwatch.StartNew();
            var start = 0;
            var offset = 1000;
            var counter = 1;
            if (entities.Count > offset)
            {
                counter = entities.Count / offset;
                var remainder = entities.Count % offset;
                if (remainder > 0)
                    counter++;
            }
            for (var i = 0; i < counter; i++)
            {
                var requestWithNoResults = new ExecuteMultipleRequest
                {
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = false,
                        ReturnResponses = false
                    },
                    Requests = new OrganizationRequestCollection()
                };
                var btachJobIds = entities.Skip(start).Take(offset);
                foreach (var jobId in btachJobIds)
                {
                    var entityRef = new Entity("ddsm_rateclass", jobId.Id);
                    entityRef["ddsm_kwhgrosssavingsatmeter"] = 13;
                    entityRef["ddsm_totalincentivedsm"] = 13;

                    entityRef["ddsm_kwgrosssavingsatgenerator"] = 13;

                    entityRef["ddsm_kwgrosssavingsatmeter"] = 13;

                    entityRef["ddsm_kwnetsavingsatgenerator"] = 13;

                    entityRef["ddsm_kwhgrosssavingsatgenerator"] = 13;

                    entityRef["ddsm_kwhgrosssavingsatmeter"] = 13;

                    entityRef["ddsm_kwhnetsavingsatgenerator"] = 13;
                    var updateRequest = new UpdateRequest {Target = entityRef};
                    requestWithNoResults.Requests.Add(updateRequest);
                }
                dmnDto.CrmDto.OrgService.Execute(requestWithNoResults);
                start += offset;
            }
            Console.WriteLine($"***end CreateBulkUpdateRequest:{sw.Elapsed}");
        }

        public void CreateUpdateRequest(List<Entity> entities, DmnDto dmnDto)
        {
            var sw = Stopwatch.StartNew();
            Console.WriteLine($"***start CreateUpdateRequest{DateTime.Now}");
            var req = new UpsertRequest();
            foreach (var entity in entities)
            {
                entity.Attributes["ddsm_kwhgrosssavingsatmeter"] = (decimal) 13;
                req.Target = entity;
                dmnDto.CrmDto.OrgService.Execute(req);
            }
            Console.WriteLine($"***end CreateUpdateRequest:{sw.Elapsed}");
        }


        private List<string> GetMeasures(IOrganizationService orgService)
        {
            var result = new List<string>();
            var pageCount = 5000;
            var pageNumber = 1;
            var isRsNotEmpty = true;
            while (isRsNotEmpty)
            {
                var query = new QueryExpression("ddsm_measure")
                {
                    ColumnSet = new ColumnSet(),
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression("ddsm_programofferingsid", ConditionOperator.Equal,
                                new Guid("07f2b366-55d8-e611-80fa-666665393634"))
                        }
                    }
                };

                query.PageInfo = new PagingInfo();
                query.PageInfo.Count = pageCount;
                query.PageInfo.PageNumber = pageNumber;
                query.Distinct = true;
                // Assign the pageinfo properties to the query expression.

                var entities = orgService.RetrieveMultiple(query).Entities;
                foreach (var entity in entities)
                {
                    result.Add(entity.Id.ToString());
                    if (result.Count == 5)
                        return result;
                }
            }
            return result;
        }

        private InputArgsDto ParseInputArgs(string[] args)
        {
            var input = args[0];
            return JsonConvert.DeserializeObject<InputArgsDto>(input);
        }

        private void InitCrmClient(DmnDto dmnDto)
        {
            var crmConnectionString = ConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString;


            // Get the CRM connection string and connect to the CRM Organization

            var client = new CrmServiceClient(crmConnectionString);
            var orgServiceProxy = client.OrganizationServiceProxy;
            //init crm dto
            var crmDto = new CrmDto();
            //crmDto.ConnectionStrings = crmConnectionString;
            //crmDto.CrmServiceClient = client;
            crmDto.OrgService = orgServiceProxy;

            dmnDto.CrmDto = crmDto;
        }
    }
}