﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using DmnEngineApp.Dto;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using Serilog;

namespace DmnEngineApp.Service
{
    public class DataService
    {
        private readonly DmnDto _dmnDto;
        public ConcurrentDictionary<string, object> _sourceValuesCache;

        public DataService()
        {
        }

        public DataService(DmnDto dmnDto)
        {
            _dmnDto = dmnDto;
            OrgService = dmnDto.CrmDto.OrgService;
            _sourceValuesCache = new ConcurrentDictionary<string, object>();
        }

        private IOrganizationService OrgService { get; set; }


        /// <summary>
        ///     Get target entity reference
        /// </summary>
        /// <param name="orgService"></param>
        /// <param name="logicalName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public EntityReference GetEntityReferenceById(IOrganizationService orgService, string logicalName, string id)
        {
            if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(logicalName))
                return null;

            var entity = RetreiveEntity(logicalName, id, new ColumnSet());
            return entity?.ToEntityReference();
        }

        /// <summary>
        ///     Retreive entity
        /// </summary>
        /// <param name="logicalName"></param>
        /// <param name="recordId"></param>
        /// <param name="columnSet"></param>
        /// <returns></returns>
        public Entity RetreiveEntity(string logicalName, string recordId, ColumnSet columnSet = null)
        {
            if (string.IsNullOrEmpty(recordId))
                return null;

            if (columnSet == null || columnSet.Columns.Count == 0)
                columnSet = new ColumnSet();

            return RetreiveEntity(logicalName, recordId);
        }

        /// <summary>
        ///     Retreive Entity
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <param name="recordId"></param>
        /// <returns></returns>
        public Entity RetreiveEntity(string entityLogicalName, Guid recordId)
        {
            CheckOrgServiceOnDispose();
            try
            {
                var entityCache = _dmnDto.EntityCache;
                //var entityCache = DmnDto.EntityCache;
                if (entityCache.ContainsKey(recordId))
                {
                    var tmp = new Entity();
                    if (entityCache.TryGetValue(recordId, out tmp))
                        return tmp;
                }
                /*if (entityCache.ContainsKey(recordId))
                    return entityCache[recordId];*/


                var entity = OrgService.Retrieve(entityLogicalName, recordId, new ColumnSet(true));
                //_dmnDto.EntityCache.Add(recordId, entity);
                //_dmnDto.EntityCache.TryAdd(recordId, entity);
                entityCache.TryAdd(recordId, entity);
                return entity;
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                throw new Exception();
            }
        }

        /// <summary>
        ///     Retreive Entity
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <param name="recordId"></param>
        /// <returns></returns>
        public Entity RetreiveEntity(string entityLogicalName, string recordId)
        {
            return RetreiveEntity(entityLogicalName, new Guid(recordId));
        }

        /// <summary>
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <param name="attName"></param>
        /// <returns></returns>
        public IEnumerable<AttributeMetadata> GetAttributeMetadata(string entityLogicalName, string attName)
        {
            try
            {
                var entityMetadata = GetEntityMetadata(entityLogicalName);

                return entityMetadata?.Attributes.Where(att => att.LogicalName.ToLower() == attName.ToLower());
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                throw new Exception();
            }
        }

        public string GetEntityDisplayName(string entityLogicalName)
        {
            try
            {
                var entityMetadata = GetEntityMetadata(entityLogicalName);

                return entityMetadata.DisplayName.UserLocalizedLabel.Label;
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                throw new Exception();
            }
        }

        public void RetreiveBatchEntities(List<Guid> entitiesGuids, string entityLogicalName, string fieldName)
        {
            CheckOrgServiceOnDispose();
            try
            {
                //var entityCache = DmnDto.EntityCache;
                var entityCache = _dmnDto.EntityCache;
                var query = new QueryExpression
                {
                    EntityName = entityLogicalName,
                    ColumnSet = new ColumnSet(true),
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression(fieldName, ConditionOperator.In, entitiesGuids)
                        }
                    }
                };
                var rs = OrgService.RetrieveMultiple(query).Entities;
                foreach (var item in rs)
                    entityCache.TryAdd(item.Id, item);
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                throw new Exception();
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <returns></returns>
        private EntityMetadata GetEntityMetadata(string entityLogicalName)
        {
            var currentEntityMetadata = new EntityMetadata();
            CheckOrgServiceOnDispose();
            try
            {
                //var entityMetadata = DmnDto.EntityMetadata;
                var entityMetadata = _dmnDto.EntityMetadata;
                if (entityMetadata.ContainsKey(entityLogicalName))
                {
                    //currentEntityMetadata = entityMetadata[entityLogicalName];
                    var tmp = new EntityMetadata();
                    if (entityMetadata.TryGetValue(entityLogicalName, out tmp))
                        currentEntityMetadata = tmp;
                }
                else
                {
                    var request = new RetrieveEntityRequest
                    {
                        EntityFilters = EntityFilters.Attributes,
                        LogicalName = entityLogicalName,
                        RetrieveAsIfPublished = true
                    };

                    var response = (RetrieveEntityResponse) OrgService.Execute(request);
                    currentEntityMetadata = response.EntityMetadata;
                    //_dmnDto.EntityMetadata.TryAdd(entityLogicalName, currentEntityMetadata);
                    entityMetadata.TryAdd(entityLogicalName, currentEntityMetadata);
                    _dmnDto.EntityDisplayName.TryAdd(entityLogicalName,
                        //DmnDto.EntityDisplayName.TryAdd(entityLogicalName,
                        response.EntityMetadata.DisplayName.UserLocalizedLabel.Label);
                }
                return currentEntityMetadata;
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                throw new Exception();
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public EntityCollection RetrieveMultipleQuery(FetchExpression query)
        {
            return OrgService.RetrieveMultiple(query);
        }


        public void CheckOrgServiceOnDispose()
        {
            try
            {
                var userRequest = new WhoAmIRequest();
                //If WhoAmIRequest FAILS, it would be due to "_proxy" object destroyed.
                OrgService.Execute(userRequest);
            }
            catch (Exception)
            {
                ReconectToCrm();
            }
        }

        private void ReconectToCrm()
        {
            var crmConnectionString = ConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString;
            // Get the CRM connection string and connect to the CRM Organization
            var client = new CrmServiceClient(crmConnectionString);
            OrgService = client.OrganizationServiceProxy;
            _dmnDto.CrmDto.OrgService = OrgService;
        }

        /// <summary>
        ///     Convert list of ids  to list  of Guid
        /// </summary>
        /// <param name="listIds"></param>
        /// <returns></returns>
        public List<Guid> ConvertListIdToListGuid(List<string> listIds)
        {
            var rs = new List<Guid>();
            foreach (var id in listIds)
                rs.Add(new Guid(id));
            return rs;
        }
    }
}