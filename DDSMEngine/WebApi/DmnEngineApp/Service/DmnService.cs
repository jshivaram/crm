﻿using System;
using System.Collections.Generic;
using System.Linq;
using DmnEngine.Core;
using DmnEngineApp.Core.Dto.Rule;
using DmnEngineApp.Dto;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace DmnEngineApp.Service
{
    internal class DmnService
    {
        private const string EntityName = "ddsm_businessrule";
        private readonly DmnDto _dmnDto;

        public DmnService(DmnDto dmnDto)
        {
            _dmnDto = dmnDto;
        }

        /// <summary>
        ///     Get DMN decisions per business rule
        /// </summary>
        /// <param name="businessRuleEntity"></param>
        /// <returns></returns>
        public List<OutputEntryDto> GetDecisions(Entity businessRuleEntity)
        {
            object targetentity = null;
            if (businessRuleEntity.Attributes.TryGetValue("ddsm_targetentity", out targetentity) &&
                !string.Equals(targetentity.ToString(), _dmnDto.TargetEntity.LogicalName,
                    StringComparison.CurrentCultureIgnoreCase))
                return null;


            object dmnRule = null;
            if (!businessRuleEntity.Attributes.TryGetValue("ddsm_dmnrule", out dmnRule))
                return null;

            var defaultDmnEngine = new DefaultDmnEngine(dmnRule.ToString());
            var decisionTable = defaultDmnEngine.ParseDecision();
            var dmnDecisionResult = new DmnDecisionResult(_dmnDto, decisionTable);
            return dmnDecisionResult.GetResult();
        }

        /// <summary>
        ///     Get business rule in xml format
        /// </summary>
        /// <returns></returns>
        public EntityCollection GetDmnSchema(List<Guid> businessRuleGuid)
        {
            var entityCollection = new EntityCollection();

            if (businessRuleGuid.Count == 0)
                return new EntityCollection();
            var columns = new ColumnSet("ddsm_targetentity", "ddsm_name", "ddsm_dmnrule",
                "ddsm_shortdescription");

            // Create query expression.
            var query = new QueryExpression
            {
                ColumnSet = columns,
                EntityName = EntityName,
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_businessruleid", ConditionOperator.In, businessRuleGuid)
                    }
                }
            };
            var resultSet = _dmnDto.CrmDto.OrgService.RetrieveMultiple(query);
            if (resultSet.Entities.Count == 0)
                return entityCollection;

            //sort entity
            foreach (var id in businessRuleGuid)
            {
                var entity = resultSet.Entities.FirstOrDefault(x => x.Id == id);
                entityCollection.Entities.Add(entity);
            }

            return entityCollection;
        }
    }
}