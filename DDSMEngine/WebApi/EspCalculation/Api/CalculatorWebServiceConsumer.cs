﻿using System.Net;
using CoreUtils.Wrap;
using EspCalculation.Model;

namespace EspCalculation.API
{
    public class CalculatorWebServiceConsumer
    {
        //static CalculatorWebServiceConsumer()
        //{
        //    //// If a specific certificate thumbprint has been defined in the app.config file, use it to validate the server certificate.
        //    //// NOTE: This is an override -- if the certificate is otherwise valid, e.g. from a recognized authority or added to the
        //    //// trusted list, then the thumbprint isn't checked.
        //    var validThumbprint = ConfigurationManager.AppSettings["CertThumbprint"] ?? "B098638EC17B960CC48CA5BBC0CA12F96F7E4DB8";

        //    if (!string.IsNullOrWhiteSpace(validThumbprint))
        //    {
        //        ServicePointManager.ServerCertificateValidationCallback = ((object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) =>
        //        {
        //            // Only check the thumbprint if the certificate is not otherwise valid
        //            var isValid = !chain.ChainStatus.Any(cs => cs.Status != X509ChainStatusFlags.NoError);

        //            if (!isValid)
        //            {
        //                var cert = certificate as X509Certificate2;
        //                if (cert != null)
        //                {
        //                    if (cert.Thumbprint.Equals(validThumbprint, StringComparison.OrdinalIgnoreCase))
        //                    {
        //                        isValid = true;
        //                    }
        //                }
        //            }
        //            return isValid;
        //        });
        //    }
        //}

        public AuthenticationResponse Authenticate(string a_sBaseURI, string a_sEmail, string a_sPassword)
        {
            AuthenticationResponse result = new AuthenticationResponse();

            var url = $"{a_sBaseURI}/API/Authentication";

            AuthenticationRequest authenticationRequest = new AuthenticationRequest() { UserName = a_sEmail, Password = a_sPassword };
            string request = JsonConvert.SerializeObject(authenticationRequest);
            string response;
            using (var client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } })
            {               
                response = client.UploadString(url, "POST", request);
            }
            result = JsonConvert.DeserializeObject<AuthenticationResponse>(response);
            return result;
        }

        //public AuthenticationResponse Authenticate(string aSBaseUri, string aSEmail, string aSPassword)
        //{
        //    var result = new AuthenticationResponse();

        //    var url = string.Format(aSBaseUri
        //        + "/API/Authentication"
        //        + "/" + aSEmail
        //        + "/" + aSPassword);

        //    using (var serviceRequest = new WebClient())
        //    {
        //        byte[] responseBytes = serviceRequest.DownloadData(url);
        //        var jsoNresponse = Encoding.UTF8.GetString(responseBytes);
        //        result =JsonConvert.DeserializeObject<AuthenticationResponse>(jsoNresponse); 
        //    }


        //    return result;
        //}

        public AvailableCalculatorsResponse GetAvailableCalculators(string aSBaseUri, AvailableCalculatorsRequest aAvailableCalculatorsRequest)
        {
            AvailableCalculatorsResponse result = null;
            var url = string.Format(aSBaseUri + "/API/AvailableCalculators/");
            var request = JsonConvert.SerializeObject(aAvailableCalculatorsRequest);

            string response;
            using (var client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } })
            {
                response = client.UploadString(url, "POST", request);
            }

            result = JsonConvert.DeserializeObject<AvailableCalculatorsResponse>(response);
            return result;
        }

        public CalculatorDataDefinitionsResponse GetCalculatorDataDefinitions(string aSBaseUri, CalculatorDataDefinitionsRequest aMadDefinitionRequest)
        {
            CalculatorDataDefinitionsResponse result = null;
            var url = string.Format(aSBaseUri + "/API/CalculatorDataDefinitions/");
            var request = JsonConvert.SerializeObject(aMadDefinitionRequest);

            string response;
            using (var client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } })
            {
                response = client.UploadString(url, "POST", request);
            }
            result = JsonConvert.DeserializeObject<CalculatorDataDefinitionsResponse>(response);
            return result;
        }

        public CalculationResponse Calculate(string aSBaseUri, CalculationRequest aCalculationRequest)
        {
            CalculationResponse result = null;
            var url = string.Format(aSBaseUri + "/API/Calculation/");
            var request = JsonConvert.SerializeObject(aCalculationRequest);
            string response;
            using (var client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } })
            {
                response = client.UploadString(url, "POST", request);
            }
            result = JsonConvert.DeserializeObject<CalculationResponse>(response);
            return result;
        }


    }
}
