﻿using System;
using CoreUtils.Utils;
using CoreUtils.Wrap;
using EspCalculation.API;
using EspCalculation.Model;
using Microsoft.Xrm.Sdk;

namespace EspCalculation.Api
{
    public class EspService
    {
        #region internal fields
        private readonly CalculatorWebServiceConsumer _espDriver;
        private readonly ESPConfig _config;
        private readonly IOrganizationService _service;
        #endregion

        public EspService(ESPConfig config, IOrganizationService service)
        {
            if (config == null)
            {
                var msg = "Can't get ESP config.";
                Log.Fatal(msg);
                throw new Exception(msg);
            }
            _espDriver = new CalculatorWebServiceConsumer();
            _config = config;
            _service = service;
            //Log.Debug("_config: " + JsonConvert.SerializeObject(_config));

            if (_config.IsTokenNeedUpdate())
            {
                Log.Debug("Token must update");
                var authResp = _espDriver.Authenticate(_config.Url, _config.Login, _config.Pass);
                if (authResp.ResponseHeader.StatusOk)
                {
                    _config.LastToken = authResp.ResponseHeader.AuthenticationTokenBytes;
                    _config.RequestReceivedDate = authResp.ResponseHeader.ResponseDateTime;
                    UpdateConfig();
                }
                else
                {
                    Log.Fatal("Can't get authorization token using esp login details. Please contact to your administrator." +
                                                       Environment.NewLine + "ESP Error: " + authResp.ResponseHeader.ErrorMessage);
                }

            }
        }

        /// <summary>
        /// Update last token and last request dateTime  
        /// </summary>
        public void UpdateConfig()
        {
            if (_config.RequestReceivedDate != DateTime.MinValue)
            {
                SettingsProvider.SaveConfig(_service, _config);
            }
        }

        /// <summary>
        /// Get Available Calculators
        /// </summary>
        public AvailableCalculatorsResponse GetAvailableCalculators(DateTime targetDate)
        {
            var request = new AvailableCalculatorsRequest()
            {
                RequestHeader = new RequestHeader
                {
                    AuthenticationTokenBytes = _config.LastToken

                },
                TargetDateTime = targetDate
            };

            Log.Debug("In GetAvailableCalculators");
            return _espDriver.GetAvailableCalculators(_config.Url, request);
        }
        
        public CalculatorDataDefinitionsResponse GetDataDefs(AvailableCalculatorsResponse response,DateTime targetDate)
        {
            var data = GetDataDefsRequest(response, _config.LastToken, targetDate);
            return _espDriver.GetCalculatorDataDefinitions(_config.Url, data);
        }

        public CalculatorDataDefinitionsRequest GetDataDefsRequest(AvailableCalculatorsResponse response, byte[] token, DateTime targetDate)
        {
            var measureLibrarysList = response.CalculatorLibraryResponseList; //measureLibrarys
            var request = new CalculatorDataDefinitionsRequest
            {
                RequestHeader = { AuthenticationTokenBytes = token }
            };

            foreach (var measureLib in measureLibrarysList)
            {
                foreach (var measure in measureLib.CalculatorList)
                {
                    request.CalculatorDefinitionRequestList.Add(new CalculatorDefinitionRequest { CalculatorID = measure.ID, TargetDateTime = targetDate });
                }
            }
            return request;
        }

        /// <summary>
        /// Retriev Metadata from ESP
        /// </summary>
        public void GetMetadata()
        {
            Log.Debug("In GetMetadata");

        }

        /// <summary>
        /// Calculate Smart Measure
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Return result of calculation</returns>
        public CalculationResponse Calculate(CalculationRequest request)
        {
            Log.Debug("In Calculate");
            return _espDriver.Calculate(_config.Url, request);
        }

    }
}
