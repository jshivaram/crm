﻿using System;
using System.Collections.Generic;
using CoreUtils.Wrap;
using EspCalculation.Model;
using EspCalculation.Options;

using Microsoft.Xrm.Sdk;

namespace EspCalculation.App
{
    public class AppGetLatestMtData : BaseApp
    {

        internal override string StartOperationMsg { get { return "Get Latest Mt Data is started. Target Date:{0}"; } }

        public AppGetLatestMtData(IOrganizationService orgService, CalculationOptions options, Action callbackAction, EntityReference target = null) : base(orgService, options, callbackAction, target)
        {

        }

        public override void Run()
        {
            var versions = CrmHelper.GetSMVerions(new EntityReference("ddsm_measuretemplate", CalcOptions.Target.Id));
            var latestVer = versions.GetLatestVersion(CalcOptions.TargetDate);

            var madsJson = latestVer?.Record?.GetAttributeValue<string>("ddsm_madfields");
            var madMappingRef = latestVer?.Record?.GetAttributeValue<EntityReference>("ddsm_madmappingid");
            var mads = JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(madsJson);
            var madMapping = CrmHelper.GetMapping(madMappingRef);

            var result = new
            {
                Mads = mads,
                MadMapping = madMapping,
            };
            var json = JsonConvert.SerializeObject(result);
        }
    }
}
