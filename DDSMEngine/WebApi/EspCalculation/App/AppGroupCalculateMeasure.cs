﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreUtils.Wrap;
using DmnEngineApp.Service;
using EspCalculation.Api;
using EspCalculation.Options;
using Microsoft.Xrm.Sdk;


namespace EspCalculation.App
{
    public class AppGroupCalculateMeasure : BaseApp
    {
        #region internal fields
        private EspService _espService;
        internal override string StartOperationMsg { get { return "Group ESP Calculation is started. Target Date: {0}"; } }
        #endregion

        public AppGroupCalculateMeasure(IOrganizationService orgService, CalculationOptions options, Action callbackAction) : base(orgService, options, callbackAction, options.Target)
        {
        }

        public override void Run()
        {
            Log.Debug("AppGroupCalculateMeasure: in Run()");

            try
            {
           
                _espService = new EspService(EspConfig, OrganizationService);

                var measuresIdForCalculate = CrmHelper.GetMeasureForCalculate(CalcOptions);//Id from TQ

                if (measuresIdForCalculate?.SmartMeasures?.Count == 0)
                {
                    Log.Fatal("Can't get measure list for processing from Task Queue");
                    return;
                }

                CalculateMeasures(measuresIdForCalculate?.SmartMeasures);

                Completed = true;
                Result = "";
            }
            catch (Exception e)
            {
                Log.Error(e, "Error. AppGroupCalculateMeasure: in Run()");
            }
            finally
            {
                WsMessageService?.SendMessage(WSMessageService.MessageType.SUCCESS, "Group ESP Calculation is finished.", WSMessageService.CalcMessageStatus.FINISH);

                Log.Debug("Group SM calc finish: Completed: " + Completed);
                if (Callback != null)
                {
                    Log.Debug("_callback is not null. In Execute callback");
                    Callback();
                }
            }
        }

        #region Internal metods

        /// <summary>
        /// Calculate Measure in ESP by portion from Options
        /// </summary>
        /// <param name="smartMeasures">List Measure's id</param>
        private void CalculateMeasures(IReadOnlyCollection<Guid> smartMeasures)
        {
            Log.Debug("in CalculateMeasures");
            if (smartMeasures?.Count == 0)
            {
                Log.Error("SmartMeasure List is empty. No one measure for calculate");
                return;
            }

           
            var processedCount = 0;
            //var tasks = new List<Task>();
            while (processedCount < smartMeasures.Count)
            {
                //get data portion 
                var dataPortion = smartMeasures.Skip(processedCount).Take((int)EspConfig.DataPortion).ToList();
                Log.Debug($"Portion from:{processedCount}");
                ProcessPortion(dataPortion);
                //  var bufTask = Task.Factory.StartNew(() => ProcessPortion(dataPortion));
                processedCount += dataPortion.Count;
                //tasks.Add(bufTask);
            }
            // Task.WaitAll(tasks.ToArray(), -1);
            Log.Debug("End CalculateMeasures");
        }

        private void ProcessPortion(List<Guid> dataPortion)
        {
            var smartMeasureData = CrmHelper.GetMTRefsByMeasure(dataPortion);

            WsMessageService?.SendMessage(WSMessageService.MessageType.INFORMATION, $"Group ESP Processed Measures Count:{smartMeasureData?.Entities.Count}", WSMessageService.CalcMessageStatus.START);
            if (smartMeasureData?.Entities.Count > 0)
            {
                var calcRequest = CrmHelper.BuildCalculationRequest(smartMeasureData, EspConfig.LastToken, CalcOptions.TargetDate);

                var calcResult = _espService.Calculate(calcRequest);
                CrmHelper.UpdateMeasures(calcResult);
            }
            WsMessageService?.SendMessage(WSMessageService.MessageType.SUCCESS, $"Group ESP Processed Measures Count:{smartMeasureData?.Entities.Count}", WSMessageService.CalcMessageStatus.FINISH);
        }
        #endregion
    }
}
