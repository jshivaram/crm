﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreUtils.Utils;
using CoreUtils.Wrap;
using DmnEngineApp.Service;
using EspCalculation.Api;
using EspCalculation.Model;
using EspCalculation.Options;
using EspCalculation.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace EspCalculation.App
{
    public class AppSyncMetadata : BaseApp
    {
        #region internal fields
        private List<ESPWSVariableDefinitionResponse> _uniqueMads = new List<ESPWSVariableDefinitionResponse>();
        private List<ESPWSVariableDefinitionResponse> _uniqueQdds = new List<ESPWSVariableDefinitionResponse>();
        private List<string> _logList = new List<string>();
        private Dictionary<ESPWSCalculatorDefinitionResponse, Diffs> _dataDiff = new Dictionary<ESPWSCalculatorDefinitionResponse, Diffs>();

        struct Diffs
        {
            public List<ESPWSVariableDefinitionResponse> NewMad { get; set; }
            public List<ESPWSVariableDefinitionResponse> NewQdd { get; set; }
        }

        private List<ESPWSCalculatorDefinitionResponse> _addedDiffs;
        private Dictionary<ESPWSCalculatorDefinitionResponse, EspMeasureInfo> _updatedDiffs;

        #endregion

        internal override string StartOperationMsg { get { return "Sync Metadata is started. Target Date: {0}"; } }
        
        public AppSyncMetadata(IOrganizationService orgService, CalculationOptions options, Action callbackAction) : base(orgService, options, callbackAction)
        {
            _addedDiffs = new List<ESPWSCalculatorDefinitionResponse>();
            _updatedDiffs = new Dictionary<ESPWSCalculatorDefinitionResponse, EspMeasureInfo>();
        }

        public override void Run()
        {
            ProcessingLog = new List<string>();

            try
            {
                var measureLibrarysList = EspService.GetAvailableCalculators(CalcOptions.TargetDate);
                WsMessageService?.SendMessage(WSMessageService.MessageType.INFORMATION, "ESP Measure Library Count: " + measureLibrarysList?.CalculatorLibraryResponseList?.Count, WSMessageService.CalcMessageStatus.START);

                var processedEspMeasureLibData = CreateEspMeasureLibrarys(measureLibrarysList);

                var dataDefsResponse = EspService.GetDataDefs(measureLibrarysList, CalcOptions.TargetDate);
                WsMessageService?.SendMessage(WSMessageService.MessageType.INFORMATION, $"Get ESP Data Definitions for {dataDefsResponse?.ESPWSCalculatorDataDefinitionResponseList?.Count} smartmeasures", WSMessageService.CalcMessageStatus.START);

                CollectUniqMaDandQdd(dataDefsResponse.ESPWSCalculatorDataDefinitionResponseList);

                ProcessSmartMeasureData(dataDefsResponse, processedEspMeasureLibData);
            }
            catch (Exception ex)
            {
                WsMessageService?.SendMessage(WSMessageService.MessageType.ERROR, $"Sync Metadata is finished with error. {ex.Message}", WSMessageService.CalcMessageStatus.FINISH);
                _logList.Add(ex.Message);
                Log.Error(ex, "Error");
            }
            finally
            {
                GenerateSyncSummary();
            }
        }

        private void GenerateSyncSummary()
        {
            ExecuteMultipleRequest request = new ExecuteMultipleRequest()
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                }
            };

            var updateLog = new List<string>();
            if (_updatedDiffs.Count > 0)
            {
                updateLog.Add("Updated SM: " + _updatedDiffs.Count);
                foreach (var updMeasure in _updatedDiffs)
                {
                    var data = updMeasure;
                    updateLog.Add(data.Key.Name);
                    var record = _dataDiff.FirstOrDefault(x => x.Key.ID == updMeasure.Key.ID);
                    updateLog.Add(GetAddedFieldMsg(record.Value.NewMad));
                    updateLog.Add(GetAddedFieldMsg(record.Value.NewQdd, false));
                }
            }


            var addLog = new List<string>();
            if (_addedDiffs.Count > 0)
            {
                addLog.Add("Added SM: " + _addedDiffs.Count);

                foreach (var newMeasure in _addedDiffs)
                {
                    addLog.Add(newMeasure.Name);
                }
            }


            // var log = string.Join(Environment.NewLine, addLog) + string.Join(Environment.NewLine, updateLog);

            var logRecord = new Entity("ddsm_espsynclog")
            {
                Attributes =
                {
                    ["ddsm_name"]="Metadata Sync - "+ CalcOptions?.TargetDate,
                    ["ddsm_added"]=_addedDiffs.Count,
                    ["ddsm_updated"]=_updatedDiffs.Count,
                    ["ddsm_addlog"]=string.Join(Environment.NewLine, addLog),
                    ["ddsm_targetdate"]=CalcOptions?.TargetDate,
                    ["ddsm_updatelog"]=string.Join(Environment.NewLine,updateLog)
                }
            };
            if (_logList?.Count > 0)
            {
                logRecord.Attributes.Add("ddsm_witherror", true);
                logRecord.Attributes.Add("ddsm_log", string.Join(Environment.NewLine, _logList));

            }


            var logId = OrganizationService.Create(logRecord);

            if (_addedDiffs.Count > 0)
            {
                var crRequest = new CreateRequest()
                {
                    Target = new Entity("ddsm_esplogitem")
                    {
                        Attributes =
                        {
                           ["regardingobjectid"]= new EntityReference(logRecord.LogicalName, logId),
                           ["subject"]="Added SM: "+ _addedDiffs.Count,
                           ["description"]=string.Join(Environment.NewLine, addLog)
                        }
                    }
                };
                request.Requests.Add(crRequest);
            }

            if (_updatedDiffs.Count > 0)
            {
                var crRequest = new CreateRequest()
                {
                    Target = new Entity("ddsm_esplogitem")
                    {
                        Attributes =
                        {
                            ["regardingobjectid"]= new EntityReference(logRecord.LogicalName, logId),
                            ["subject"]="Updated SM: "+ _updatedDiffs.Count,
                            ["description"]=string.Join(Environment.NewLine,updateLog)
                        }
                    }
                };
                request.Requests.Add(crRequest);
            }

            if (request.Requests.Count > 0)
            {
                var result = (ExecuteMultipleResponse)OrganizationService.Execute(request);
                result.ResultToLog();
            }
            WsMessageService?.SendMessage(WSMessageService.MessageType.SUCCESS, "Sync Metadata is finished. ", WSMessageService.CalcMessageStatus.FINISH);
        }

        #region Internal methods
        private Diffs GetSmMetadataDiff(EntityReference oldSmVersion, ESPWSCalculatorDefinitionResponse currentVersionMeatadata, Entity lastVers)
        {

            var newMad = new List<ESPWSVariableDefinitionResponse>();
            var newQdd = new List<ESPWSVariableDefinitionResponse>();

            if (oldSmVersion == null)
            {
                return new Diffs();
            }
            var madsJson = lastVers.GetAttributeValue<string>("ddsm_madfields");
            var qddsJson = lastVers.GetAttributeValue<string>("ddsm_qddfields");
            if (!string.IsNullOrEmpty(madsJson))
            {
                var oldSmVersionMad = JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(madsJson);
                newMad = currentVersionMeatadata.ESPVariableDefinitionList.Where(i => !oldSmVersionMad.Any(x => x.Name == i.Name && x.DisplayFormat == i.DisplayFormat)).ToList();
            }
            if (!string.IsNullOrEmpty(qddsJson))
            {
                var oldSmVersionQdd = JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(qddsJson);
                newQdd = currentVersionMeatadata.ESPQddDefinitionList.Where(i => !oldSmVersionQdd.Any(x => x.Name == i.Name && x.DisplayFormat == i.DisplayFormat)).ToList();
            }

            return new Diffs() { NewQdd = newQdd, NewMad = newMad };
        }

        private string GetAddedFieldMsg(List<ESPWSVariableDefinitionResponse> dataDiff, bool isMad = true)
        {
            if (dataDiff == null)
            {
                return "";
            }

            List<ESPWSVariableDefinitionResponse> data = null;
            List<string> result = new List<string>();

            var fieldType = "";
            if (isMad)
            {
                fieldType = "MAD";
                data = dataDiff;
            }
            else
            {
                fieldType = "QDD";
                data = dataDiff;
            }

            if (data?.Count > 0)
            {
                //result.Add();
                foreach (var mad in data)
                {
                    result.Add(mad.Name + " - Type: " + mad.DisplayFormat);
                }
                return $" Added new {fieldType} fields: " + Environment.NewLine + string.Join(", ", result);
            }
            else
            {
                return "";
            }

        }

        private Dictionary<Guid, Guid> CreateEspMeasureLibrarys(AvailableCalculatorsResponse measureLibrarysList)
        {
            var crmMeasureLibId = new Guid();

            var result = new Dictionary<Guid, Guid>();
            if (measureLibrarysList != null && measureLibrarysList.CalculatorLibraryResponseList.Count > 0)
            {
                foreach (var lib in measureLibrarysList.CalculatorLibraryResponseList)
                {
                    var expr = new QueryExpression
                    {
                        EntityName = "ddsm_espmeasurelibrary",
                        ColumnSet = new ColumnSet("ddsm_espmeasurelibraryid"),
                        Criteria = new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions = { new ConditionExpression("ddsm_espmeasurelibid", ConditionOperator.Equal, lib.ID.ToString()) }
                        }
                    };

                    // var query = new FetchExpression(expr);
                    var measureLib = new Entity("ddsm_espmeasurelibrary");
                    var crmMt = OrganizationService.RetrieveMultiple(expr);

                    //update if found
                    if (crmMt != null && crmMt.Entities.Count > 0)
                    {
                        crmMeasureLibId = crmMt.Entities[0].Id;
                        measureLib = new Entity("ddsm_espmeasurelibrary", crmMeasureLibId)
                        {
                            ["ddsm_espmeasurelibid"] = lib.ID.ToString(),
                            ["ddsm_name"] = lib.Name,
                            ["ddsm_organizationname"] = lib.OrganizationName
                        };
                        OrganizationService.Update(measureLib);
                    }
                    else
                    {
                        measureLib = new Entity("ddsm_espmeasurelibrary")
                        {
                            ["ddsm_espmeasurelibid"] = lib.ID.ToString(),
                            ["ddsm_name"] = lib.Name,
                            ["ddsm_organizationname"] = lib.OrganizationName
                        };

                        crmMeasureLibId = OrganizationService.Create(measureLib);
                    }
                    foreach (var measure in lib.CalculatorList)
                    {
                        result.Add(measure.ID, crmMeasureLibId);
                    }
                }
            }
            else
            {
                throw new Exception("Can't create measure librarys. Receiver Measure Librarys list is empty.");
            }
            return result;
        }

        private void CollectUniqMaDandQdd(List<ESPWSCalculatorDefinitionResponse> smartMeasureData)
        {
            //collect all uniq MAD & QDD
            foreach (var sm in smartMeasureData)
            {
                _uniqueMads.AddRange(sm.ESPVariableDefinitionList);
                _uniqueQdds.AddRange(sm.ESPQddDefinitionList);
            }

            _uniqueMads = _uniqueMads.GroupBy(p => p.Name.ToUpper()).Select(g => g.First()).OrderBy(x => x.Name).ToList();
            _uniqueQdds = _uniqueQdds.GroupBy(p => p.Name.ToUpper()).Select(g => g.First()).OrderBy(x => x.Name).ToList();
            _uniqueQdds = _uniqueQdds.Except(_uniqueMads, new QDDItemNameComparer()).ToList();
        }

        private void ProcessSmartMeasureData(CalculatorDataDefinitionsResponse smartMeasureData, Dictionary<Guid, Guid> libData)
        {
            if (smartMeasureData == null)
            {
                throw new Exception("Nothing to do! in ProcessSmartMeasureData list is null((");
            }
            var datepattern = "yyyy.MM.dd HH:mm:ss";
            try
            {
                foreach (var measure in smartMeasureData.ESPWSCalculatorDataDefinitionResponseList)
                {
                    var versions = GetSmartMeasureVersions(measure);
                    Entity smRecord = null;
                    DataCollection<Entity> reletedMTs = null;
                    //no SM versions found, create it 
                    if (versions?.Count == 0)
                    {
                        smRecord = FillSmVersion(measure, libData);
                        _addedDiffs.Add(measure);
                        // _added++;
                    }
                    else
                    {
                        var lastVer = versions?.FirstOrDefault(x => x.GetAttributeValue<DateTime>("ddsm_startdatetime").ToString(datepattern).Equals(measure.StartDateTime.ToString(datepattern)));

                        // current version does not exist in CRM
                        if (lastVer == null)
                        {
                            smRecord = FillSmVersion(measure, libData);
                            _updatedDiffs.Add(measure, new EspMeasureInfo { MADs = measure.ESPVariableDefinitionList, QDDs = measure.ESPQddDefinitionList });
                            if (versions?.Count > 0)
                            {
                                //  reletedMTs = GetRelatedMT(versions[0]);
                            }
                            // DeactivateRecords(versions); //Deactivate old SM versions
                            // _updated++;
                        }
                    }

                    if (smRecord != null)
                    {
                        var newVerionId = OrganizationService.Create(smRecord);
                        CreateMapping(measure, versions, newVerionId);
                        //if (reletedMTs != null)
                        //{
                        //    ReAssignMT(reletedMTs, newVerionId);
                        //}
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void CreateMapping(ESPWSCalculatorDefinitionResponse sm, DataCollection<Entity> versions, Guid newSmVeriosnId)
        {
            if (versions?.Count > 0)
            {
                var lastVers = versions[0];

                var madMapping = lastVers.GetAttributeValue<EntityReference>("ddsm_madmappingid");
                var qddMapping = lastVers.GetAttributeValue<EntityReference>("ddsm_qddmappingid");

                var diffs = GetSmMetadataDiff(madMapping, sm, lastVers);
                _dataDiff.Add(sm, diffs);

                if (madMapping != null)
                {
                    Entity mapping = GetActualMapping(madMapping, sm, lastVers, diffs);
                    if (mapping != null)
                    {
                        mapping["ddsm_smartmeasure"] = new EntityReference("ddsm_smartmeasureversion", newSmVeriosnId);
                        var mappingId = OrganizationService.Create(mapping);

                        OrganizationService.Update(new Entity("ddsm_smartmeasureversion", newSmVeriosnId)
                        {
                            Attributes = { new KeyValuePair<string, object>("ddsm_madmappingid", new EntityReference(mapping.LogicalName, mappingId)) }
                        });
                    }
                }
                if (qddMapping != null)
                {
                    Entity mapping = GetActualMapping(qddMapping, sm, lastVers, diffs, false);
                    if (mapping != null)
                    {
                        mapping["ddsm_smartmeasure"] = new EntityReference("ddsm_smartmeasureversion", newSmVeriosnId);
                        var mappingId = OrganizationService.Create(mapping);
                        OrganizationService.Update(new Entity("ddsm_smartmeasureversion", newSmVeriosnId)
                        {
                            Attributes =
                            {
                                new KeyValuePair<string, object>("ddsm_qddmappingid",
                                    new EntityReference(mapping.LogicalName, mappingId))
                            }
                        });
                    }
                }
            }
        }

        private Entity FillSmVersion(ESPWSCalculatorDefinitionResponse measure, Dictionary<Guid, Guid> libData)
        {
            Entity result;
            result = new Entity("ddsm_smartmeasureversion")
            {
                Attributes =
                {
                    ["ddsm_name"] = measure.Name,
                    ["ddsm_madfields"] =JsonConvert.SerializeObject(measure.ESPVariableDefinitionList.OrderBy(v => v.DisplayOrderRank)),
                    ["ddsm_qddfields"] =JsonConvert.SerializeObject(measure.ESPQddDefinitionList.OrderBy(v => v.DisplayOrderRank)),
                    ["ddsm_calculationid"] = measure.ID.ToString(),
                    ["ddsm_startdatetime"] = measure.StartDateTime,
                    ["ddsm_espsmartmeasurelibraryid"] =new EntityReference("ddsm_espmeasurelibrary", libData[measure.ID])
                }
            };
            return result;
        }

        private Entity GetActualMapping(EntityReference oldMappingRef, ESPWSCalculatorDefinitionResponse measure, Entity lastVers, Diffs diffs, bool isMadmapping = true)
        {
            Entity result = null;

            if (oldMappingRef == null)
            {
                return null;
            }

            if (isMadmapping) //MAD Mapping
            {
                if (diffs.NewMad?.Count == 0)
                {
                    Log.Debug("Mad mapping should be clonned");
                    result = CloneRecord(oldMappingRef, measure.Name, measure.StartDateTime, true, "ddsm_mappingid");
                }
            }
            else //qdd Mapping
            {
                if (diffs.NewQdd?.Count == 0)
                {
                    Log.Debug("Qdd mapping should be clonned");
                    result = CloneRecord(oldMappingRef, measure.Name, measure.StartDateTime, false, "ddsm_mappingid");
                }
            }
            return result;
        }

        private Entity CloneRecord(EntityReference oldRecordRef, string smVersionName, DateTime startDate, bool isMad, params string[] excludeFields)
        {
            EntityReference result;
            if (oldRecordRef == null)
            {
                return null;
            }

            var oldRecord = OrganizationService.Retrieve(oldRecordRef.LogicalName, oldRecordRef.Id, new ColumnSet(true));

            Entity newRecord = new Entity(oldRecord.LogicalName);

            foreach (var attr in oldRecord.Attributes)
            {
                if (excludeFields.Contains(attr.Key)) continue;
                newRecord.Attributes.Add(attr);
            }
            if (isMad)
            {
                newRecord["ddsm_name"] = "MAD Mapping - " + smVersionName + " (" + startDate.ToString() + ")";
            }
            else
            {
                newRecord["ddsm_name"] = "QDD Mapping - " + smVersionName + " (" + startDate.ToString() + ")";
            }
            //var clonedMappingId = Service.Create(newRecord);
            //result = new EntityReference(oldRecord.LogicalName, clonedMappingId);
            return newRecord;
        }

        private DataCollection<Entity> GetSmartMeasureVersions(ESPWSCalculatorDefinitionResponse measure)
        {
            var expr = new QueryExpression
            {
                EntityName = "ddsm_smartmeasureversion",
                ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression
                {
                    FilterOperator = LogicalOperator.And,
                    Conditions = { new ConditionExpression("ddsm_calculationid", ConditionOperator.Equal, measure.ID.ToString()) }
                }
            };
            expr.AddOrder("ddsm_startdatetime", OrderType.Descending);

            var crmMt = OrganizationService.RetrieveMultiple(expr);
            return crmMt.Entities;
        }

        #endregion

    }
}
