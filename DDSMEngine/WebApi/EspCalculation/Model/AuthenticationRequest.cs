﻿using System.Runtime.Serialization;

namespace EspCalculation.Model
{
    [DataContract]
    public class AuthenticationRequest
    {
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Password { get; set; }
    }
}
