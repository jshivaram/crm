﻿using System.Runtime.Serialization;


namespace EspCalculation.Model
{
    [DataContract]
    public class AuthenticationResponse
    {
        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }

        public AuthenticationResponse()
        {
            ResponseHeader = new ResponseHeader();
        }
    }
}