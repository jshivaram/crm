﻿using System;
using System.Runtime.Serialization;

namespace EspCalculation.Model
{
    [DataContract]
    public class AvailableCalculatorsRequest
    {
        [DataMember]
        public RequestHeader RequestHeader { get; set; }

        [DataMember]
        public DateTime TargetDateTime { get; set; }

        public AvailableCalculatorsRequest()
        {
            RequestHeader = new RequestHeader();
            TargetDateTime = DateTime.UtcNow;
        }
    }
}