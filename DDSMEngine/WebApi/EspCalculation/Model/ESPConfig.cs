﻿using System;
using CoreUtils.Model;

namespace EspCalculation.Model
{
    public class CustomLookup
    {
        public string Label { get; set; }
        public string LogicalName { get; set; }
    }

    public class ESPConfig : IModuleConfig
    {
        public bool IsTokenNeedUpdate()
        {
            try
            {
                if (LastToken ==null || LastToken?.Length == 0 || RequestReceivedDate ==null || RequestReceivedDate == DateTime.MinValue || (DateTime.UtcNow - (DateTime)RequestReceivedDate).TotalHours >= 24)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public string Url { get; set; }
        public string Login { get; set; }
        public string Pass { get; set; }
        public decimal DataPortion { get; set; }
        public byte[] LastToken { get; set; }
        public CustomLookup DefaultLibrary { get; set; }
        public bool AllMadAreRequired { get; set; }
        public DateTime? RequestReceivedDate { get; set; }
        // mapping data
        public string EspMapingData { get; set; }
        public string EspUniqueQdd { get; set; }
        public string EspQddMappingData { get; set; }
        //remote calculation
        public bool EspRemoteCalculation { get; set; }
        public string EspRemoteCalculationApiUrl { get; set; }
        public Guid ConfigId { get; set; }


        public string ConfigName { get { return "EspConfig"; } }
    }
}
