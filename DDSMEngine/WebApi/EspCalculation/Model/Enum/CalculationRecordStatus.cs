﻿
namespace EspCalculation.Model.Enum
{
    public enum CalculationRecordStatus
    {
        NotReady = 962080000,
        ReadyToCalculation = 962080001
    };
}
