﻿namespace EspCalculation.Model.Enum
{
    public enum ESPWSVariableType
    {
        Double,
        Text,
        DoubleList,
        TextList
    }
}