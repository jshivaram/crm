using System.Collections.Generic;

namespace EspCalculation.Model
{
    public class EspMeasureInfo
    {
        public List<ESPWSVariableDefinitionResponse> QDDs { get; set; }
        public List<ESPWSVariableDefinitionResponse> MADs { get; set; }

        public EspMeasureInfo()
        {
            QDDs = new List<ESPWSVariableDefinitionResponse>();
            MADs = new List<ESPWSVariableDefinitionResponse>();
        }
    }
}