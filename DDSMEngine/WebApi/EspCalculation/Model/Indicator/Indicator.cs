﻿namespace EspCalculation.Model.Indicator
{
    public class Indicator
    {
        public virtual decimal Value { get; set; }

        public decimal Amount { get; set; }
    }
}
