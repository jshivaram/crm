using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;

namespace EspCalculation.Model
{

    public class Version
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Entity Record { get; set; }

        public Version(Entity record)
        {
            if (record != null)
            {
                StartDate = record.GetAttributeValue<DateTime>("ddsm_startdatetime");
                Record = record;
            }
        }
    }


    public class Versions : IList<Version>
    {
        List<Version> _mItems = null;

        public Versions()
        {

            _mItems = new List<Version>();
        }


        #region IEnumerable<T> Members
        public IEnumerator<Version> GetEnumerator()
        {
            foreach (var t in _mItems)
            {
                // Lets check for end of list (its bad code since we used arrays)
                if (t == null) // this wont work is T is not a nullable type
                {
                    break;
                }

                // Return the current element and then on next function call 
                // resume from next element rather than starting all over again;
                yield return t;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(Version item)
        {
            if (_mItems.Count == 0)
            {
                item.EndDate = DateTime.UtcNow.AddSeconds(5);
            }
            else
            {
                var prevItem = _mItems[_mItems.Count - 1];
                prevItem.EndDate = item.StartDate;
                item.EndDate = DateTime.UtcNow.AddSeconds(5);
            }

            _mItems.Add(item);
            // _freeIndex++;
        }

        public void Clear()
        {
            _mItems.Clear();
        }

        public bool Contains(Version item)
        {
            return _mItems.Contains(item);
        }

        public void CopyTo(Version[] array, int arrayIndex)
        {
            _mItems.CopyTo(array, arrayIndex);
        }

        public bool Remove(Version item)
        {
            return _mItems.Remove(item);
        }

        public int Count => _mItems.Count;
        public bool IsReadOnly { get; }
        public int IndexOf(Version item)
        {
            return _mItems.IndexOf(item);
        }

        public void Insert(int index, Version item)
        {
            _mItems.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _mItems.RemoveAt(index);
        }

        public Version this[int index]
        {
            get { return _mItems[index]; }
            set { _mItems[index] = value; }
        }
        #endregion


        public Version GetLatestVersion(DateTime measureCreated)
        {
            return _mItems.FirstOrDefault(x => measureCreated >= x.StartDate && measureCreated < x.EndDate);
        }
    }


}