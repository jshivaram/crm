﻿using System;
using EspCalculation.Model;
using Microsoft.Xrm.Sdk;

namespace EspCalculation.Options
{
    public class CalculationOptions
    {
        public Guid UserId { get; set; }
        public ESPConfig Config { get; set; }
        public EntityReference Target { get; set; }
        public DateTime TargetDate { get; set; }
        public string UserInput { get; set; }
    }
}