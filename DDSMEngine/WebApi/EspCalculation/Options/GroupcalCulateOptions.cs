﻿using Microsoft.Xrm.Sdk;

namespace EspCalculation.Options
{
    public class GroupCalculateOptions : BaseCalculationConfig
    {
        public EntityReference Project { get; set; }
    }
}