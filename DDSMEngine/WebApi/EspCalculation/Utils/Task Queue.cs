﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreUtils.Model;
using CoreUtils.Wrap;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace EspCalculation.Utils
{
    public class TaskQueue
    {
        #region internal fields
        IOrganizationService _service;
        static string MainEntity = "ddsm_taskqueue";
        #endregion


        public TaskQueue(IOrganizationService service)
        {
            _service = service;
        }

        #region Public Methods
        public Guid Create(DDSM_Task task, Model.Enum.TaskQueue.TaskEntity recordType, int startIndex = 0, EntityReference dataUploader= null)
        {
            var newTask = new Entity(MainEntity);
            #region Fill new Task
            newTask["ddsm_name"] = task.GetTaskName();
            newTask["ddsm_taskentity"] = new OptionSetValue((int)task.TaskEntity);
            newTask["ddsm_entityrecordtype"] = new OptionSetValue((int)recordType);

            if (dataUploader != null && !Guid.Empty.Equals(dataUploader?.Id))
                newTask["ddsm_datauploader"] = dataUploader;

            // if count of processed items more than 10 000 will be created two Records
            if (task.ProcessedItems0.Count > 10000)
            {
                newTask["ddsm_processeditems0"] = JsonConvert.SerializeObject(new UserInputObj2 { SmartMeasures = task.ProcessedItems0.GetPortion(startIndex).ToList() });
                Create(task, recordType, startIndex + 1);
            }
            else
            {
                newTask["ddsm_processeditems0"] = JsonConvert.SerializeObject(new UserInputObj2 { SmartMeasures = task.ProcessedItems0 });
            }
            #endregion
            var newTaskGuid = _service.Create(newTask);
            return newTaskGuid;
        }

        public void Update(DDSM_Task task)
        {
            var newTask = new Entity("ddsm_taskqueue", task.Id);
            #region Fill new Task
            newTask["ddsm_name"] = task.Name;
            newTask["ddsm_taskentity"] = new OptionSetValue((int)task.TaskEntity);
            #endregion
            _service.Update(newTask);
        }

        public Entity Get(Guid Id)
        {
            return _service.Retrieve(MainEntity, Id, new ColumnSet(allColumns: true));
        }
        #endregion

    }
    public class DDSM_Task
    {
        public DDSM_Task(Model.Enum.TaskQueue.TaskEntity taskEntity)
        {
            TaskEntity = taskEntity;
            ProcessedItems0 = new List<Guid>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<Guid> ProcessedItems0 { get; set; }
        public Model.Enum.TaskQueue.TaskEntity TaskEntity { get; set; }

        public string GetTaskName(int set = 0)
        {
            return Enum.GetName(typeof(Model.Enum.TaskQueue.TaskEntity), TaskEntity) + " Task Queue - " + "/" + set + DateTime.UtcNow;
        }
    }

    public static class TaskHelper
    {
        public static IList<Guid> GetPortion(this IList<Guid> mylist, int page, int countRecOnPage = 10000)
        {
            return mylist.Skip(page * (page - 1)).Take(countRecOnPage).ToList();
        }
    }

}

