﻿
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CoreUtils.Model;
using CoreUtils.Utils;
using CoreUtils.Wrap;
using EspCalculation.Model;
using EspCalculation.Model.Enum;
using EspCalculation.Options;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;


namespace EspCalculation.Utils
{
    public class CrmHelper
    {
        public ConcurrentBag<Guid> ProcessedProject { get; private set; }
        public ConcurrentBag<Guid> ProcessedProgOff { get; private set; }
        public ConcurrentBag<Guid> ProcessedMasures { get; private set; }
        public List<KeyValuePair<Guid, Guid>> ProcessedMeasures { get; private set; }
        public Dictionary<Guid, EntityCollection> ProcessedMeasuresIndicatorsValues { get; private set; }
        public IDictionary<Guid, IDictionary<string, MappingItem2>> PersonalMads { get; private set; }
        public IDictionary<Guid, IDictionary<string, MappingItem2>> PersonalQdds { get; private set; }

        readonly IOrganizationService _service;

        public CrmHelper(IOrganizationService service)
        {
            _service = service;

            ProcessedProject = new ConcurrentBag<Guid>();
            ProcessedProgOff = new ConcurrentBag<Guid>();
            ProcessedMasures = new ConcurrentBag<Guid>();
            ProcessedMeasuresIndicatorsValues = new Dictionary<Guid, EntityCollection>();
            ProcessedMeasures = new List<KeyValuePair<Guid, Guid>>();
            PersonalMads = new ConcurrentDictionary<Guid, IDictionary<string, MappingItem2>>();
            PersonalQdds = new ConcurrentDictionary<Guid, IDictionary<string, MappingItem2>>();
        }

        private string mainEntity = "ddsm_measure";

        public void SetAttributeValue(MappingItem2 mapItem, ESPWSResponseVariableValue madResult, Entity newMeasure)
        {
            var name = mapItem.AttrLogicalName.ToLower();
            if (string.IsNullOrEmpty(name))
                throw new Exception("in SetAttributeValue() error: mapItem.AttrName is empty");

            var decVal = new decimal();
            if (null != madResult.Value && null != mapItem.FieldType)
                switch (mapItem.FieldType)
                {
                    case "Picklist":
                        var value = GetOptionsSetValueForLabel(mapItem.Entity, mapItem.AttrLogicalName, madResult.Value.ToString());
                        newMeasure.Attributes[name] = new OptionSetValue(value);
                        break;
                    case "Decimal":
                        double dec1;
                        if (double.TryParse(madResult.Value.ToString(), out dec1))
                        {
                            newMeasure.Attributes[name] = Convert.ToDecimal(dec1);
                        }
                        break;
                    case "Double":
                        double dob1;
                        if (double.TryParse(madResult.Value.ToString(), out dob1))
                        {
                            newMeasure.Attributes[name] = dob1;
                        }
                        break;
                    case "Money":
                        decimal dec2;
                        if (decimal.TryParse(madResult.Value.ToString(), out dec2))
                        {
                            newMeasure.Attributes[name] = new Money(dec2);
                        }
                        break;
                    case "DateTime":
                        DateTime date;
                        if (DateTime.TryParse(madResult.Value.ToString(), out date))
                        {
                            newMeasure.Attributes.Add(name, date);
                        }
                        break;
                    case "String":
                    default:
                        newMeasure.Attributes.Add(name, madResult.Value);
                        break;
                }
        }

        public void UpdateMeasures(CalculationResponse calcResult)
        {
            if (calcResult?.ESPWSCalculatorCalculationList?.Count == 0)
            {
                Log.Error("ESP Calc Result is Empty");
                return;
            }
            Log.Debug("In UpdateMeasures. processedMeasures Count: " + ProcessedMasures?.Count);
            var UpdateMeasureResuest = new List<UpdateRequest>();
            var processedCalcs = 0;
            var error = new List<string>();

            Log.Debug("calcResult.ESPWSCalculatorCalculationList.Count = " + calcResult?.ESPWSCalculatorCalculationList?.Count);
            Log.Debug("processedMeasures.Count = " + ProcessedMeasures?.Count);


            for (int i = 0; i < calcResult?.ESPWSCalculatorCalculationList?.Count; i++)
            {
                //Log.Debug("i=" + i);
                var measureCalculation = calcResult.ESPWSCalculatorCalculationList[i];

                if (!measureCalculation.ResponseHeader.StatusOk)
                {
                    var msg = $"Updating measure with error. Error code: {calcResult.ResponseHeader.ErrorCode}. \nError: {calcResult.GetFullESPError()}.";
                    error.Add(msg);
                    Log.Error(msg);
                    continue;
                }
                var rrr = ProcessedMeasures[i];
                var mIndicatorsValues = ProcessedMeasuresIndicatorsValues.FirstOrDefault(x => x.Key.Equals(rrr.Key));

                if (rrr.Value != measureCalculation.ID)
                {
                    Log.Error("Can't get Measure Id for updating");
                    Log.Debug("calcId: " + measureCalculation.ID + " but ProcessedMesure calcId: " + rrr.Value);
                    continue;
                }

                IDictionary<string, MappingItem2> qdDmappingData = PersonalQdds.FirstOrDefault(x => x.Key.Equals(rrr.Key)).Value;
                Entity measureForUpdate = new Entity(mainEntity, rrr.Key);

                processedCalcs++;

                foreach (var result in measureCalculation.ESPWSVariableCalculationList)
                {
                    try
                    {
                        var mapItem = qdDmappingData.FirstOrDefault(x => x.Key.Trim().ToLower().Equals(result.Name?.Trim().ToLower()));

                        if (!string.IsNullOrEmpty(mapItem.Key))
                        {
                            //update measure fields
                            if (!string.IsNullOrEmpty(mapItem.Value?.AttrLogicalName))
                            {
                                if (mapItem.Value != null && mapItem.Key != null && mapItem.Value.Entity.Equals(mainEntity))
                                {
                                    SetAttributeValue(mapItem.Value, result, measureForUpdate);
                                }
                            } //update measure indicators
                            else
                            {
                                var indName = mapItem.Value.AttrDisplayName.Trim().ToLower();
                                
                                var indicator = mIndicatorsValues.Value.Entities.FirstOrDefault(x => x
                                    .GetAttributeValue<string>("ddsm_name")
                                    .Trim()
                                    .ToLower()
                                    .Equals(indName));
                                if (indicator != null)
                                {
                                    var indicatorRecord = new Entity(indicator.LogicalName, indicator.Id);

                                    switch (mapItem.Value.AttrType)
                                    {
                                        case "Money":
                                            indicatorRecord.Attributes["ddsm_amount"] = result.DoubleValue != null ? new Money((decimal)result.DoubleValue) : null;
                                            break;
                                        case "Integer":
                                        case "Decimal":
                                            indicatorRecord.Attributes["ddsm_value"] = result.DoubleValue;
                                            break;
                                    }
                                    var updateIndicatorValueRequest = new UpdateRequest
                                    {
                                        Target =indicatorRecord
                                    };
                                    UpdateMeasureResuest.Add(updateIndicatorValueRequest);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(" error in fill qdd: " + ex.Message + ex.StackTrace);
                        throw;
                    }
                }
                var updateRequest = new UpdateRequest { Target = measureForUpdate };
                UpdateMeasureResuest.Add(updateRequest);
            }

            var processed = 0;
            var updatePortion = 100;

            var tasks = new List<Task>();
            while (processed < UpdateMeasureResuest.Count)
            {
                var portion = UpdateMeasureResuest.Skip(processed).Take(updatePortion);
                processed += updatePortion;

                var bufTask = Task.Factory.StartNew(() => updateMeasurePortion(portion, _service));
                tasks.Add(bufTask);//
            }
            Task.WaitAll(tasks.ToArray(), -1);
        }

        public void SetMSDAttributeValue(MSDField mapItem, string msdValue, Entity newMeasure)
        {
            var name = mapItem?.AttrName?.ToLower();
            if (!string.IsNullOrEmpty(msdValue))
                switch (mapItem.FieldType)
                {
                    case "Picklist":
                        newMeasure.Attributes[name] = new OptionSetValue(int.Parse(msdValue));
                        break;
                    case "Decimal":
                        newMeasure.Attributes[name] = decimal.Parse(msdValue);
                        break;
                    case "Double":
                        newMeasure.Attributes[name] = double.Parse(msdValue);
                        break;
                    case "Money":
                        newMeasure.Attributes[name.ToLower()] = new Money(decimal.Parse(msdValue));
                        break;
                    case "Lookup":
                        var lookup = msdValue.Split('|');
                        if (lookup.Length >= 2 && !string.IsNullOrEmpty(lookup[0]) && !string.IsNullOrEmpty(lookup[1]))
                            newMeasure.Attributes.Add(name, new EntityReference(lookup[0], Guid.Parse(lookup[1])));
                        break;
                    case "DateTime":
                    case "String":
                    default:
                        newMeasure.Attributes.Add(name, msdValue);
                        break;
                }
        }

        public void SetMADAttributeValue(MappingItem2 mapItem, string madValue, Entity newMeasure)
        {
            var name = mapItem.AttrLogicalName.ToLower();
            var decVal = new decimal();
            if (!string.IsNullOrEmpty(madValue))
                switch (mapItem.FieldType)
                {
                    case "Picklist":
                        newMeasure[name] = new OptionSetValue(int.Parse(madValue));
                        break;
                    case "Decimal":
                        newMeasure[name] = decimal.Parse(madValue);
                        break;
                    case "Double":
                        newMeasure[name] = double.Parse(madValue);
                        break;
                    case "Money":
                        newMeasure[name] = new Money(decimal.Parse(madValue));
                        break;
                    case "Lookup":
                        var lookup = madValue.Split('|');
                        if (lookup.Length >= 2 && !string.IsNullOrEmpty(lookup[0]) && !string.IsNullOrEmpty(lookup[1]))
                            newMeasure[name] = new EntityReference(lookup[0], Guid.Parse(lookup[1]));
                        break;
                    case "DateTime":
                    case "String":
                    default:
                        newMeasure[name] = madValue;
                        break;
                }
        }

        //public Entity GetConfig()
        //{
        //    if (service == null)
        //        throw new Exception("Error: Service is null ((");
        //    try
        //    {
        //        var expr = new QueryExpression
        //        {
        //            EntityName = "ddsm_admindata",
        //            ColumnSet = new ColumnSet("ddsm_espurl", "ddsm_esplogin", "ddsm_esppassword", "ddsm_espdataportion", "ddsm_esplasttoken", "ddsm_requestreceiveddate", "ddsm_espmapingdata", "ddsm_espuniqueqdd", "ddsm_espuniquemad", "ddsm_espqddmappingdata", "ddsm_espremotecalculation", "ddsm_espremotecalculationapiurl"),
        //            Criteria = new FilterExpression
        //            {
        //                FilterOperator = LogicalOperator.And,
        //                Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, "Admin Data") }
        //            }
        //        };

        //        var adminData = service.RetrieveMultiple(expr);


        //        if (adminData != null && adminData.Entities?.Count >= 1)
        //        {

        //            return adminData.Entities[0];
        //        }
        //        else return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("error in getting Config: " + ex.Message + ex.Data);
        //    }
        //}

        public Versions GetSMVerions(EntityReference mtRef)
        {
            var Result = new Versions();

            EntityCollection result = null;
            var query = new QueryExpression
            {
                EntityName = "ddsm_smartmeasureversion",
                ColumnSet = new ColumnSet(true),
                Orders = { new OrderExpression("ddsm_startdatetime", OrderType.Ascending) }
            };
            var retrieveRequest = new RetrieveRequest
            {
                Target = new EntityReference(mtRef.LogicalName, mtRef.Id),
                ColumnSet = new ColumnSet("ddsm_name"),
                RelatedEntitiesQuery = new RelationshipQueryCollection()
                {
                    new KeyValuePair<Relationship, QueryBase>(new Relationship("ddsm_ddsm_smartmeasureversion_ddsm_mt"),query)
                }
            };
            var crmData = (RetrieveResponse)_service.Execute(retrieveRequest);
            if (crmData.Entity.RelatedEntities?.Count > 0)
            {
                result = crmData.Entity.RelatedEntities.First().Value;
            }

            foreach (var record in result.Entities)
            {
                Result.Add(new Model.Version(record));
            }
            return Result;
        }

        public EntityReference GetMeasureCalcType(string name = "ESP")
        {
            try
            {
                var expr = new QueryExpression
                {
                    EntityName = "ddsm_measurecalculationtemplate",
                    ColumnSet = new ColumnSet("ddsm_name"),
                    Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, name) }
                    }
                };
                var result = _service.RetrieveMultiple(expr);
                if (result.Entities.Count > 0)
                {
                    return result.Entities[0].ToEntityReference();
                }
                throw new Exception("Calc type record with name " + name + " not found");
            }
            catch (Exception ex)
            {
                return new EntityReference();
            }
        }

        public CalculationRequest BuildCalculationRequest(EntityCollection measures, byte[] lastToken, DateTime targetDate)
        {
            Log.Debug($"In BuildCalculationRequest. Count measures: {measures?.Entities.Count}");
            var aCalculationRequest = new CalculationRequest { RequestHeader = { AuthenticationTokenBytes = lastToken } };

            ProcessedMeasures = new List<KeyValuePair<Guid, Guid>>();

            foreach (var measureItem in measures.Entities)
            {
                var request = GetRequest(measureItem, targetDate);

                if (request != null)
                {
                    aCalculationRequest.CalculatorCalculationRequestList.Add(request);
                }
            }
            Log.Debug("Build calculation request for " + ProcessedMeasures?.Count + " measures.");
            return aCalculationRequest;
        }

        public IDictionary<string, MappingItem2> GetMapping(EntityReference mapping)
        {
            if (mapping == null)
            {
                Log.Error("Can't get mapping!");
                return null;
            }
            IDictionary<string, MappingItem2> result = new Dictionary<string, MappingItem2>();
            var data = _service.Retrieve(mapping.LogicalName, mapping.Id, new ColumnSet(true));

            var mappingJson = data.GetAttributeValue<string>("ddsm_jsondata");

            if (string.IsNullOrEmpty(mappingJson))
            {
                Log.Error($"Mapping: {mapping?.Id} is broken. Check the mapping.");
                return null;
            }
            var mappingData = JsonConvert.DeserializeObject<Dictionary<string, List<MappingItem2>>>(mappingJson);
            foreach (var map in mappingData)
            {
                foreach (var mad in map.Value)
                {
                    // if (!string.IsNullOrEmpty(mad.AttrLogicalName))
                    result[mad.FieldDisplayName] = new MappingItem2 { AttrLogicalName = mad.AttrLogicalName, AttrDisplayName = mad.AttrDisplayName, AttrType = mad.AttrType, Entity = map.Key, FieldType = mad.FieldType }; //FieldDisplayName
                }
            }

            return result;
        }

        public UserInputObj2 GetMeasureForCalculate(CalculationOptions _options)
        {
            if (_options.Target?.LogicalName == "ddsm_taskqueue")
            {
                var taskData = _service.Retrieve(_options.Target.LogicalName, _options.Target.Id, new ColumnSet("ddsm_processeditems0"));
                var userInputJson = taskData?.GetAttributeValue<string>("ddsm_processeditems0");
                return JsonConvert.DeserializeObject<UserInputObj2>(userInputJson);
            }
            else if (_options.Target?.LogicalName == "ddsm_project")
            {
                return GetMeasureByTarget(_options.Target);
            }
            return null;
        }

        public EntityCollection GetMTRefsByMeasure(List<Guid> smartMeasures)
        {
            // clear 
            var measuresId = smartMeasures.Where(x => !x.Equals(Guid.Empty)).ToList();
            if (measuresId?.Count == 0)
            {
                Log.Error("Can't get MT. SmartMeasure List is EMPTY");
                return null;
            }

            var expr = new QueryExpression
            {
                EntityName = mainEntity,
                ColumnSet = new ColumnSet("ddsm_measureselector"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_measureid", ConditionOperator.In, measuresId),
                        new ConditionExpression("statecode", ConditionOperator.Equal, 0)
                    }
                }
            };
            //Add link to MT
            //expr.LinkEntities.Add(new LinkEntity(mainEntity, "ddsm_measuretemplate", "ddsm_measureselector", "ddsm_measuretemplateid", JoinOperator.Inner));
            return _service.RetrieveMultiple(expr);
        }

        public Entity GetSMLatestVersion(Entity measureItem, DateTime targetDate)
        {
            var mainEntity = "ddsm_measure";
            var mtRef = measureItem.GetAttributeValue<EntityReference>("ddsm_measureselector");
            if (mtRef?.Id == Guid.Empty)
            {
                var msg = $"ESP calculation for measure: {measureItem?.Id} skipped. Can't get MT.";
                Log.Error(msg);
            }
            var versions = GetSMVerions(mtRef);
            Log.Debug($"MT: {mtRef?.Id}. Found SM versions: {versions?.Count}");
            var latestVersion = versions?.GetLatestVersion(targetDate);
            Log.Debug($"Latest version: {latestVersion?.Record?.Id}.");
            return latestVersion?.Record;
        }

        public CalculatorCalculationRequest GetRequest(Entity measureItem, DateTime targetDate)
        {
            try
            {
                var latestVerion = GetSMLatestVersion(measureItem, targetDate);

                if (latestVerion != null)
                {
                    var madJson = latestVerion?.GetAttributeValue<string>("ddsm_madfields");
                    var qddJson = latestVerion?.GetAttributeValue<string>("ddsm_qddfields");
                    var calcId = latestVerion?.GetAttributeValue<string>("ddsm_calculationid");

                    List<ESPWSVariableDefinitionResponse> mads = JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(madJson);
                    List<ESPWSVariableDefinitionResponse> qdds = JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(qddJson);

                    var madMapping = GetMapping(latestVerion?.GetAttributeValue<EntityReference>("ddsm_madmappingid"));
                    var qddMapping = GetMapping(latestVerion?.GetAttributeValue<EntityReference>("ddsm_qddmappingid"));

                    if (mads == null || qdds == null || madMapping == null || qddMapping == null)
                    {
                        Log.Error($"Measure: {measureItem?.Id} will be skipped. Check MAD and QDD mapping");
                        return null;
                    }

                    if (string.IsNullOrEmpty(calcId))
                    {
                        Log.Error($"Measure: {measureItem?.Id} will be skipped. Can't get calculation id");
                        return null;
                    }

                    var calculatorCalculationRequest = new CalculatorCalculationRequest
                    {
                        CalculatorID = new Guid(calcId),
                        TargetDateTime = targetDate
                    };

                    var measureData = GetMeasureData(mads, madMapping, measureItem.Id);
                    var mIndicatorsValue = GetMIndicatorsValue(measureData);

                    // fill request from DB values
                    foreach (var mad in mads)
                    {
                        var crmFiled = madMapping.FirstOrDefault(x => x.Key.ToLower().Equals(mad.Name.ToLower()));
                        var variableValueRequest = !string.IsNullOrEmpty(crmFiled.Value.AttrLogicalName) ? GenerateESPWSVariableValueRequest(crmFiled, mad, measureData, mainEntity) : GenerateESPWSVariableValueRequestFromIndicator(crmFiled, mad, mIndicatorsValue);

                        if (calculatorCalculationRequest.InputVariables.FirstOrDefault(x => x.Name.Equals(mad.Name)) == null)
                        {
                            calculatorCalculationRequest.InputVariables.Add(variableValueRequest);
                        }
                    }

                    ProcessedMeasures?.Add(new KeyValuePair<Guid, Guid>(measureItem.Id, calculatorCalculationRequest.CalculatorID));
                    PersonalMads?.Add(measureItem.Id, madMapping);
                    PersonalQdds?.Add(measureItem.Id, qddMapping);

                    // processedCount++;
#if WITH_TASKQ
                collectProcessedData(measureData, processedProject, processedProgOff, processedMasures);
#endif

                    return calculatorCalculationRequest;
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "ERROR");
                throw;
            }
            return null;
        }

        public EntityCollection GetMIndicatorsValue(Entity measure)
        {
            var query = new QueryExpression
            {
                EntityName = "ddsm_indicatorvalue",
                ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = { new ConditionExpression("ddsm_measure", ConditionOperator.Equal, measure?.Id) }
                }
            };

            return _service.RetrieveMultiple(query);
        }

        public Entity GetDataRelationMapping(string targetEntity, string sourceEntity, Guid sourceGuid)
        {
            InitializeFromRequest initialize = new InitializeFromRequest();
            initialize.TargetEntityName = targetEntity;
            initialize.EntityMoniker = new EntityReference(sourceEntity, sourceGuid);
            initialize.TargetFieldType = TargetFieldType.All;
            InitializeFromResponse initialized = (InitializeFromResponse)_service.Execute(initialize);
            return initialized.Entity;
        }

        public ESPWSVariableValueRequest GenerateESPWSVariableValueRequest(KeyValuePair<string, MappingItem2> crmFiled, ESPWSVariableDefinitionResponse mad, Entity data, string mainEntity)
        {
            var variableValueRequest = new ESPWSVariableValueRequest
            {
                ESPWSVariableType = mad.ESPWSVariableType,
                ID = mad.ID,
                Name = mad.Name,
                QDDID = mad.QDDID,
            };

            //main entity
            if (!crmFiled.Equals(new KeyValuePair<string, MappingItem2>()) && crmFiled.Value.Entity.Equals(mainEntity))
            {
                var crmFiledName = crmFiled.Value.AttrLogicalName.ToLower();
                switch (mad.ESPWSVariableType)
                {
                    case ESPWSVariableType.Double:
                        if (data.Attributes.ContainsKey(crmFiledName))
                        {
                            var filedValue = data[crmFiledName];
                            if (filedValue is Money)
                            {
                                variableValueRequest.DoubleValue = double.Parse((filedValue as Money).Value.ToString(CultureInfo.InvariantCulture));
                            }
                            else
                            {
                                variableValueRequest.DoubleValue = double.Parse(filedValue?.ToString());
                            }
                        }
                        break;
                    case ESPWSVariableType.Text:
                        if (data.Attributes.ContainsKey(crmFiledName))
                            variableValueRequest.StringValue = data.GetAttributeValue<string>(crmFiledName);
                        break;
                    case ESPWSVariableType.DoubleList:
                        if (data.Attributes.ContainsKey(crmFiledName))
                            variableValueRequest.DoubleValue = double.Parse(GetOptionsSetTextForValue(data.LogicalName, crmFiledName.ToLower(), data.GetAttributeValue<OptionSetValue>(crmFiledName.ToLower()).Value));
                        break;
                    case ESPWSVariableType.TextList:
                        if (data.Attributes.ContainsKey(crmFiledName))
                            variableValueRequest.StringValue = GetOptionsSetTextForValue(data.LogicalName, crmFiledName.ToLower(), data.GetAttributeValue<OptionSetValue>(crmFiledName.ToLower()).Value);
                        break;
                }
            }
            // fill data from related tables
            else if (!crmFiled.Equals(new KeyValuePair<string, MappingItem2>()))
            {
                var relEnt = data.RelatedEntities.FirstOrDefault(x => x.Key.SchemaName.Contains(crmFiled.Value.Entity));
                if (!relEnt.Equals(new KeyValuePair<Relationship, EntityCollection>()) && relEnt.Value.Entities.Count > 0)
                {
                    var crmFiledName = crmFiled.Value.AttrLogicalName.ToLower();

                    switch (mad.ESPWSVariableType)
                    {
                        case ESPWSVariableType.Double:
                            if (relEnt.Value.Entities[0].Attributes.ContainsKey(crmFiledName))
                            {
                                var filedValue = relEnt.Value.Entities[0][crmFiledName];
                                if (filedValue is Money)
                                {
                                    variableValueRequest.DoubleValue = double.Parse((filedValue as Money).Value.ToString());
                                }
                                else
                                {
                                    variableValueRequest.DoubleValue = double.Parse(filedValue.ToString());
                                }
                            }
                            break;
                        case ESPWSVariableType.Text:
                            if (relEnt.Value.Entities[0].Attributes.ContainsKey(crmFiledName))
                            {
                                variableValueRequest.StringValue = relEnt.Value.Entities[0].GetAttributeValue<string>(crmFiledName);
                            }
                            break;
                        case ESPWSVariableType.DoubleList:
                            if (relEnt.Value.Entities[0].Attributes.ContainsKey(crmFiledName))
                            {
                                var optsetValue = relEnt.Value.Entities[0].GetAttributeValue<OptionSetValue>(crmFiledName);
                                variableValueRequest.DoubleValue = double.Parse(GetOptionsSetTextForValue(relEnt.Value.Entities[0].LogicalName, crmFiledName, optsetValue.Value));
                            }
                            break;
                        case ESPWSVariableType.TextList:
                            if (relEnt.Value.Entities[0].Attributes.ContainsKey(crmFiledName))
                            {
                                var optsetValue1 = relEnt.Value.Entities[0].GetAttributeValue<OptionSetValue>(crmFiledName);
                                variableValueRequest.StringValue = GetOptionsSetTextForValue(relEnt.Value.Entities[0].LogicalName, crmFiledName, optsetValue1.Value);
                            }
                            break;
                    }
                }
            }


            return variableValueRequest;
        }

        public ESPWSVariableValueRequest GenerateESPWSVariableValueRequestForManual(KeyValuePair<string, MappingItem2> crmFiled, ESPWSVariableDefinitionResponse mad, KeyValuePair<string, string> field)
        {
            if (mad == null) return null;

            var variableValueRequest = new ESPWSVariableValueRequest
            {
                ESPWSVariableType = mad.ESPWSVariableType,
                ID = mad.ID,
                Name = mad.Name,
                QDDID = mad.QDDID,
            };

            //main entity
            if (!field.Equals(new KeyValuePair<string, MappingItem2>()))
            {
                double doubleVal;
                var crmFiledName = crmFiled.Value.AttrLogicalName.ToLower();
                switch (mad.ESPWSVariableType)
                {
                    case ESPWSVariableType.Double:
                        if (double.TryParse(field.Value, out doubleVal))
                            variableValueRequest.DoubleValue = doubleVal;
                        break;
                    case ESPWSVariableType.Text:
                        if (!string.IsNullOrEmpty(field.Value))
                            variableValueRequest.StringValue = field.Value;
                        break;
                    case ESPWSVariableType.DoubleList:
                        variableValueRequest.DoubleValue = double.Parse(GetOptionsSetTextForValue(crmFiled.Value.Entity, crmFiledName.ToLower(), int.Parse(field.Value)));
                        break;
                    case ESPWSVariableType.TextList:
                        variableValueRequest.StringValue = GetOptionsSetTextForValue(crmFiled.Value.Entity, crmFiledName.ToLower(), int.Parse(field.Value));
                        break;
                }
            }
            return variableValueRequest;
        }

        public UserInputObj2 GetMeasureByTarget(EntityReference target)
        {
            Log.Debug("in GetMeasureByTarget()");
            Log.Debug("target name: " + target.LogicalName);
            Log.Debug("target Id: " + target.Id);

            var query = new QueryExpression()
            {
                EntityName = "ddsm_measure",
                // ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_projecttomeasureid", ConditionOperator.Equal, target.Id)
                    }
                }
            };
            var result = _service.RetrieveMultiple(query);
            Log.Debug("count of measures: " + result.Entities.Count);

            return new UserInputObj2() { SmartMeasures = result.Entities.Select(x => x.Id).ToList<Guid>() };
        }

        #region OptionSet
        public int GetOptionsSetValueForIndex(string entityName, string attributeName, int index)
        {
            int selectedOptionValue = -1;
            var retrieveAttributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = attributeName.ToLower(),
                RetrieveAsIfPublished = true
            };
            // Execute the request.
            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)_service.Execute(retrieveAttributeRequest);
            // Access the retrieved attribute.
            PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
            // Get the current options list for the retrieved attribute.
            OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();

            if (optionList?.Length > index)
                selectedOptionValue = (int)optionList[index].Value;


            return selectedOptionValue;
        }

        public int GetOptionsSetIndexByValue(string entityName, string attributeName, int value)
        {
            int selectedOptionValue = -1;
            try
            {
                RetrieveAttributeRequest retrieveAttributeRequest = new
                    RetrieveAttributeRequest
                {
                    EntityLogicalName = entityName,
                    LogicalName = attributeName.ToLower(),
                    RetrieveAsIfPublished = true
                };
                // Execute the request.
                var retrieveAttributeResponse = (RetrieveAttributeResponse)_service.Execute(retrieveAttributeRequest);
                // Access the retrieved attribute.
                var retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
                // Get the current options list for the retrieved attribute.
                var optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();

                for (int i = 0; i < optionList.Count(); i++)
                {
                    if (optionList[i].Value == value)
                    {
                        selectedOptionValue = i;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                return selectedOptionValue;
            }

            return selectedOptionValue;
        }

        public int GetOptionsSetValueForLabel(string entityName, string attributeName, string selectedLabel)
        {
            var selectedOptionValue = -1;
            try
            {
                var retrieveAttributeRequest = new RetrieveAttributeRequest
                {
                    EntityLogicalName = entityName,
                    LogicalName = attributeName.ToLower(),
                    RetrieveAsIfPublished = true
                };
                // Execute the request.
                var retrieveAttributeResponse = (RetrieveAttributeResponse)_service.Execute(retrieveAttributeRequest);
                // Access the retrieved attribute.
                var retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
                // Get the current options list for the retrieved attribute.
                var optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();

                foreach (var oMD in optionList)
                {
                    if (oMD.Label.LocalizedLabels[0].Label.Trim().ToLower() == selectedLabel.Trim().ToLower())
                    {
                        selectedOptionValue = oMD.Value.Value;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                return selectedOptionValue;
            }
            return selectedOptionValue;
        }

        public string GetOptionsSetTextForValue(string entityName, string attributeName, int selectedValue)
        {
            var retrieveAttributeRequest = new
                RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = attributeName,
                RetrieveAsIfPublished = true
            };
            // Execute the request.
            var retrieveAttributeResponse = (RetrieveAttributeResponse)_service.Execute(retrieveAttributeRequest);
            // Access the retrieved attribute.
            var retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)
                retrieveAttributeResponse.AttributeMetadata;// Get the current options list for the retrieved attribute.
            var optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
            string selectedOptionLabel = null;
            foreach (var oMD in optionList)
            {
                if (oMD.Value == selectedValue)
                {
                    selectedOptionLabel = oMD.Label.LocalizedLabels[0].Label;
                    break;
                }
            }
            return selectedOptionLabel;
        }
        #endregion
        private ESPWSVariableValueRequest GenerateESPWSVariableValueRequestFromIndicator(KeyValuePair<string, MappingItem2> crmFiled, ESPWSVariableDefinitionResponse mad, EntityCollection mIndicatorsValues)
        {
            var variableValueRequest = new ESPWSVariableValueRequest
            {
                ESPWSVariableType = mad.ESPWSVariableType,
                ID = mad.ID,
                Name = mad.Name,
                QDDID = mad.QDDID,
            };

            var indName = crmFiled.Value.AttrDisplayName.Trim().ToLower();

            var indValue = mIndicatorsValues.Entities.FirstOrDefault(x => x
                .GetAttributeValue<string>("ddsm_name")
                .Trim()
                .ToLower()
                .Equals(indName));
            if (indValue == null) return variableValueRequest;
            switch (mad.ESPWSVariableType)
            {
                case ESPWSVariableType.Double:
                    var fieldValue = indValue.GetAttributeValue<decimal>("ddsm_value");
                    if (!decimal.MinValue.Equals(fieldValue))
                    {
                        variableValueRequest.DoubleValue = double.Parse(fieldValue.ToString());
                    }
                    break;
            }
            return variableValueRequest;
        }

        private Entity GetMeasureData(List<ESPWSVariableDefinitionResponse> curentMad, IDictionary<string, MappingItem2> mappingData, Guid measureId)
        {
            var sortedMadList = (from x in curentMad
                                 join y in mappingData on x.Name.ToLower() equals y.Key.ToLower()
                                 where !string.IsNullOrEmpty(y.Value.AttrLogicalName)
                                 select new KeyValuePair<string, string>(y.Value.AttrLogicalName?.ToLower(), y.Value.Entity));

            var sortedMad = new Dictionary<string, string>();
            foreach (var item in sortedMadList)
            {
                if (!sortedMad.ContainsKey(item.Key))
                {
                    sortedMad.Add(item.Key, item.Value);
                }
            }

            var queryMain = new QueryExpression
            {
                EntityName = mainEntity
            };

            var relatedEntity = new RelationshipQueryCollection();

            foreach (var key in sortedMad.Values.GroupBy(x => x))
            {
                //fill measure columns
                if (key.Key.Equals(mainEntity))
                {
                    foreach (var item in sortedMad)
                    {
                        if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                            queryMain.ColumnSet.AddColumn(item.Key);
                    }
                }//add relates
                else
                {
                    if (key.Key.Equals("ddsm_project"))
                    {
                        var relationship = new Relationship { SchemaName = "ddsm_ddsm_project_ddsm_measure" };

                        var query = new QueryExpression
                        {
                            EntityName = key.Key
                        };

                        foreach (var item in sortedMad)
                        {
                            if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                                query.ColumnSet.AddColumn(item.Key);
                        }

                        relatedEntity.Add(relationship, query);
                    }
                    if (key.Key.Equals("account"))
                    {
                        var relationship = new Relationship { SchemaName = "ddsm_account_ddsm_measure" };

                        var query = new QueryExpression
                        {
                            EntityName = key.Key
                        };

                        foreach (var item in sortedMad)
                        {
                            if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                                query.ColumnSet.AddColumn(item.Key);
                        }
                        relatedEntity.Add(relationship, query);

                    }
                    if (key.Key.Equals("ddsm_site"))
                    {
                        var relationship = new Relationship { SchemaName = "ddsm_ddsm_site_ddsm_measure" };

                        var query = new QueryExpression
                        {
                            EntityName = key.Key
                        };

                        foreach (var item in sortedMad)
                        {
                            if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                                query.ColumnSet.AddColumn(item.Key);
                        }

                        relatedEntity.Add(relationship, query);
                    }
                }
            }

            var request = new RetrieveRequest
            {
                ColumnSet = queryMain.ColumnSet,
                Target = new EntityReference { Id = measureId, LogicalName = mainEntity },
                RelatedEntitiesQuery = relatedEntity
            };

            var response = (RetrieveResponse)_service.Execute(request);

            var result = new Dictionary<string, object>();

            foreach (var relEnt in response.Entity.RelatedEntities)
            {
                result.Add(relEnt.Value.EntityName, relEnt.Value.Entities[0].Attributes);
            }
            return response.Entity;
        }

        private void updateMeasurePortion(IEnumerable<OrganizationRequest> portion, IOrganizationService service)
        {
            var request = new ExecuteMultipleRequest
            {
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };
            request.Requests.AddRange(portion);

            ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)service.Execute(request);
            responseWithResults.ResultToLog();

        }

    }
}
