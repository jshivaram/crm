﻿using System;
using System.Configuration;
using Microsoft.Owin.Hosting;
using OwinWebApi;

namespace KatanaStandalone
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            LogManager.SetupLog();
            var baseAdress = ConfigurationSettings.AppSettings["baseAdress"];
            baseAdress = baseAdress ?? "http://localhost:8080";
            using (WebApp.Start<Startup>(baseAdress))
            {
                Console.WriteLine("Press any key to exit ...");
                Console.ReadKey(true);
            }
        }
    }
}