﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using CoreUtils.Dto;
using CoreUtils.Service.Implementation.DataBase;
using CoreUtils.Service.Interfaces.DataBase;
using MappingImplementation.Service.Implementation;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Tooling.Connector;

namespace MappingImplementation
{
    internal class Program
    {
        private static IOrganizationService _orgService;

        public static void Main(string[] args)
        {
            Console.WriteLine($"***Start Program ");
            try

            {
                var program = new Program();
                Console.WriteLine($"get org service start");
                program.InitCrmClient();
                Console.WriteLine($"get org service finish");


                var sw = Stopwatch.StartNew();

                var impl = new Guid("CB9AF6AA-E896-E711-80D8-663933383038");

                var implementationService =
                    new MappingImplementationService(new MappingImplementationDto
                    {
                        OrgService = _orgService,
                        CreatorRecordType = MappingImplementationDto.RecordCreator.Front
                    });
                var entitiesLN = new List<string> {"ddsm_project"};

                var result = new List<Entity>();
                foreach (var entityLn in entitiesLN)
                {
                    for (var i = 0; i < 5; i++)
                    {
                        var target = new Entity(entityLn, new Guid())
                        {
                            ["ddsm_creatorrecordtype"] = new OptionSetValue(962080001),
                            ["ddsm_accountid"] =
                            new EntityReference("account", new Guid("5F126BCC-55AF-E711-8116-08002724436E")),
                            ["ddsm_parentsite"] =
                            new EntityReference("ddsm_site", new Guid("B1309AE2-55AF-E711-8116-08002724436E")),
                            ["ddsm_name"] = i.ToString()
                        };
                        GetTemplate(target, entityLn);


                        var swLocal = Stopwatch.StartNew();
                        implementationService.ExecuteMapping(target, impl);
                        result.Add(target);
                        //Console.WriteLine($"{i}.mapping time ={swLocal.Elapsed}");
                    }
                    Console.WriteLine($"total mapping time ={sw.Elapsed}");

                    var swDb = Stopwatch.StartNew();

                    Console.WriteLine($"start update to db");
                    program.TestCreate(result);
                    Console.WriteLine($"***End create into db:{swDb.Elapsed}");
                }

                //program.UpdateCrmDbWithTask(result);
                //program.UpdateCrmDbWithOutTask(result);


                Console.WriteLine($"***End Program:{sw.Elapsed}");
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                Console.WriteLine($"{message}");
                throw new FaultException(message);
            }
        }

        private static EntityReference GetTemplate(Entity target, string entityLn)
        {
            switch (entityLn)
            {
                //D73261A3-39A8-E711-80E4-626430656337
                case "ddsm_project":
                    target["ddsm_projecttemplateid"] = new EntityReference("ddsm_projecttemplate",
                        new Guid("D73261A3-39A8-E711-80E4-626430656337"));
                    //new Guid("141B2B2F-6B70-E711-80D1-663933383038"));
                    break;
            }
            return new EntityReference();
        }

        private void UpdateCrmDbWithOutTask(List<Entity> outputEntities)
        {
            var cnt = 0;
            foreach (var entity in outputEntities)
            {
                var swDb = Stopwatch.StartNew();
                UpdateRequestMultipleRequest(entity);
                Console.WriteLine($"{cnt}--- {swDb.Elapsed}");
                cnt++;
            }
        }

        private void TestCreate(List<Entity> outputEntities)
        {
            IDataBaseService dbService = new DataBaseService(_orgService);
            var aaaa = dbService.CreateEntities(outputEntities, 500);


            return;


            var start = 0;
            var offset = 20;
            var counter = 1;
            var tasks = new List<Task>();
            if (outputEntities.Count > offset)
            {
                counter = outputEntities.Count / offset;
                var remainder = outputEntities.Count % offset;
                if (remainder > 0)
                    counter++;
            }
            for (var i = 0; i < counter; i++)
            {
                var btachEntities = outputEntities.Skip(start).Take(offset).ToList();
                var a = dbService.CreateEntities(btachEntities, 500);
                var b = 1;
            }
        }


        private void UpdateCrmDbWithTask(List<Entity> outputEntities)
        {
            var start = 0;
            var offset = 10;
            var counter = 1;
            var tasks = new List<Task>();
            if (outputEntities.Count > offset)
            {
                counter = outputEntities.Count / offset;
                var remainder = outputEntities.Count % offset;
                if (remainder > 0)
                    counter++;
            }
            for (var i = 0; i < counter; i++)
            {
                var btachEntities = outputEntities.Skip(start).Take(offset).ToList();
                var bufTask = Task.Factory.StartNew(() => UpdateRequestTask(btachEntities));
                tasks.Add(bufTask);
                start += offset;
                var swDb = Stopwatch.StartNew();
                var a = Task.WaitAll(tasks.ToArray(), -1);
                Console.WriteLine($"---saved {start * 100 / outputEntities.Count}%");
                Console.WriteLine($"{btachEntities.Count}. time saving to db:{swDb.Elapsed}");
            }
        }

        private void UpdateRequestTask(List<Entity> btachEntities)
        {
            var tasks = new List<Task>();
            foreach (var entity in btachEntities)
            {
                var bufTask = Task.Factory.StartNew(() => UpdateRequestMultipleRequest(entity));
                tasks.Add(bufTask);
            }
            Task.WaitAll(tasks.ToArray(), -1);
        }

        public void UpdateRequestMultipleRequest(Entity entity)
        {
            try
            {
                var a = _orgService.Create(entity);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new Exception(e.Message);
            }
        }


        /// <summary>
        ///     Execute multiple update request
        /// </summary>
        /// <param name="entities"></param>
        public void UpdateRequest(List<Entity> entities)
        {
            try
            {
                var requestWithNoResults = new ExecuteMultipleRequest
                {
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = false,
                        ReturnResponses = true
                    },
                    Requests = new OrganizationRequestCollection()
                };

                foreach (var item in entities)
                {
                    var updateRequest = new CreateRequest {Target = item};
                    requestWithNoResults.Requests.Add(updateRequest);
                }
                var a = _orgService.Execute(requestWithNoResults);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new Exception(e.Message);
            }
        }

        private void InitCrmClient()
        {
            var crmConnectionString = ConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString;


            // Get the CRM connection string and connect to the CRM Organization

            var client = new CrmServiceClient(crmConnectionString);
            _orgService = client.OrganizationServiceProxy;
        }
    }
}