﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;

namespace MappingImplementation.Service.Interfaces
{
   public interface IImplementationService
   {
       Entity ExecuteMapping();

        Dto.ImplementationLookupFieldsDto GetImplementationLookup();
   }
}
