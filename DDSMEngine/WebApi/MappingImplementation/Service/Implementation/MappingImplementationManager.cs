﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using CoreUtils.Dto;
using MappingImplementation.Dto;
using MappingImplementation.Service.Interfaces;
using Microsoft.Xrm.Sdk;

namespace MappingImplementation.Service.Implementation
{
    public class MappingImplementationManager : IMappingImplementationManager
    {
        private readonly BaseDto _baseDto;
        private readonly IDataService _dataService;
        private readonly IMappingImplementationService _mappingImplService;
        private readonly IRelationshipService _relationshipService;


        public MappingImplementationManager(MappingImplementationService mappingImplService)
        {
            var baseDto = mappingImplService.BaseDto;
            _mappingImplService = mappingImplService;
            _baseDto = baseDto;
            _dataService = baseDto.DataService;
            _relationshipService = baseDto.RelationshipService;
        }

        /// <summary>
        ///     Start executing mapping implementation
        /// </summary>
        /// <returns>List of mapped entities</returns>
        public List<Entity> UpdateEntityByMapping()
        {
            try
            {
                //get mapping implementation
                var mappingImplementation = _dataService.GetMappingImplementationById(_baseDto.MappingImplementation);
                if (mappingImplementation == null)
                    throw new Exception($"ImplMapping does not exist for the entity {_baseDto.Target.LogicalName}");
                //get all related mapping and add to cache
                _dataService.GetRelatedMapping(mappingImplementation);

                //Initialize entity by mapping rules
                return InitializeEntityByMappingImplementation(mappingImplementation);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        /// <summary>
        ///     Initialize entity by mapping implementation rules
        /// </summary>
        /// <param name="mappingImplementation"></param>
        /// <returns></returns>
        private List<Entity> InitializeEntityByMappingImplementation(ImplMappingDto mappingImplementation)
        {
            //result
            var mappedEntity = new List<Entity>();

            //get mapping
            var mapping = _dataService.GetMappingById(mappingImplementation.SelectedMapping.Id);

            var entity = InitTargetEntityByMappingImplManyToOne(_baseDto.Target, mappingImplementation, mapping);
            mappedEntity.Add(entity);

            return mappedEntity;
        }


        /// <summary>
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="mappingImplementation"></param>
        /// <param name="mapping"></param>
        /// <returns></returns>
        private Entity InitTargetEntityByMappingImplManyToOne(Entity targetEntity, ImplMappingDto mappingImplementation,
            Dictionary<string, List<MappingDto>> mapping)
        {
            var mappedEntity = targetEntity;
            if (mappingImplementation == null) throw new ArgumentNullException(nameof(mappingImplementation));
            if (mapping.Count == 0)
                throw new Exception($"Mapping does not exist for the entity {targetEntity.LogicalName}");
            //get mapping direction
            var mappingDirection = _dataService.GetMappingDirection(mappingImplementation.SelectedMapping.Id);

            var implementationTabs = _mappingImplService.GetImplementationTabs(mappingImplementation);
            foreach (var tab in implementationTabs)
            {
                if (tab.Lookup.LogicalName == null)
                    throw new Exception($"Please select a certain instance for mapping");

                //Get source entity data
                var sourceEntity = GetSourceEntityData(targetEntity, tab, mappingDirection);

                if (string.IsNullOrEmpty(sourceEntity.LogicalName)) continue;

                //condition for mapping direction
                if (mappingDirection == (int) DataService.MappingDirection.M2O)
                {
                    _mappingImplService.DoMappingEntity(targetEntity, sourceEntity, mapping[tab.LogicalName]);
                }
                else
                {
                    //if mapping one to many need to change target entity to source entity
                    _mappingImplService.DoMappingEntity(sourceEntity, targetEntity, mapping[tab.LogicalName]);
                    mappedEntity = sourceEntity;
                    continue;
                }

                //create childs
                CreateRelationshipByMappingImp(targetEntity, sourceEntity, tab.Relationships);
            }
            return mappedEntity;
        }

        /// <summary>
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="implMappingTab"></param>
        /// <param name="mappingDirection"></param>
        /// <returns></returns>
        private Entity GetSourceEntityData(Entity targetEntity, ImplMappingDto.Tab implMappingTab, int mappingDirection)
        {
            object sourceEntityTmp = null;
            EntityReference sourceEntityReference = null;

            //if target entity does not contain the referencing attr return null
            if (targetEntity.Attributes.TryGetValue(implMappingTab.Lookup.LogicalName, out sourceEntityTmp))
                sourceEntityReference = (EntityReference) sourceEntityTmp;

            //if source entity is null and mapping direction one to many
            if (sourceEntityReference == null && mappingDirection == (int) DataService.MappingDirection.O2M)
            {
                var entity = new Entity(implMappingTab.LogicalName)
                {
                    [implMappingTab.Lookup.LogicalName] = targetEntity.ToEntityReference()
                };
                UpdateEntityIfCreatedFromDataUploader(entity);
                return entity;
            }
            //if source entity is null and mapping direction many to one need return null
            if (sourceEntityReference == null)
                return new Entity();


            //return source entity
            return _dataService.RetreiveEntity(sourceEntityReference.LogicalName, sourceEntityReference.Id);
        }

        /// <summary>
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="sourceEntity"></param>
        /// <param name="relationships"></param>
        public void CreateRelationshipByMappingImp(Entity targetEntity, Entity sourceEntity,
            List<ImplMappingDto.Relationship> relationships)
        {
            //key=mapping implementation guid {Guid}
            //value=list of child entities {EntityCollection}
            var referencingEntites = GetChildNeedCreate(sourceEntity, relationships);

            if (referencingEntites == null) return;
            foreach (var item in referencingEntites)
            {
                //create child by mapping implementation
                var relationshipEntities = CreateChildByMapping(item.Key, item.Value);

                _relationshipService.AddRelationship(targetEntity, relationshipEntities);
            }
        }

        private ConcurrentDictionary<Guid, EntityCollection> GetChildNeedCreate(Entity sourceEntity,
            List<ImplMappingDto.Relationship> relationships)
        {
            var childEntities = new ConcurrentDictionary<Guid, EntityCollection>();
            if (relationships.Count == 0) return childEntities;
            foreach (var relationship in relationships)
            {
                //check if need create this one relationship
                if (!relationship.Implement)
                    continue;
                //check if mapping implementation exist for create relationship
                if (relationship.MappingImplementation == null) continue;
                //get childs
                var referencingEntities = _relationshipService.GetReferencingEntitiesBySchemaName(sourceEntity,
                    relationship.SchemaName);
                childEntities.TryAdd(relationship.MappingImplementation.Id, referencingEntities);
            }
            return childEntities;
        }

        /// <summary>
        /// </summary>
        /// <param name="mappingImplementationId"></param>
        /// <param name="referencingEntites"></param>
        /// <returns></returns>
        private EntityCollection CreateChildByMapping(Guid mappingImplementationId,
            EntityCollection referencingEntites)
        {
            var resultEntityCollection = new EntityCollection();
            var mappingImpl = _dataService.GetMappingImplementationById(mappingImplementationId);
            var mapping = _dataService.GetMappingById(mappingImpl.SelectedMapping.Id);
            foreach (var referencingEntity in referencingEntites.Entities)
            {
                //get prefill entity
                var prefillReferencingEntity = GetPrefillReferencingEntity(mappingImpl, referencingEntity);

                if (prefillReferencingEntity == null) continue;

                InitTargetEntityByMappingImplManyToOne(prefillReferencingEntity, mappingImpl, mapping);
                resultEntityCollection.Entities.Add(prefillReferencingEntity);
            }
            return resultEntityCollection;
        }

        /// <summary>
        /// </summary>
        /// <param name="mappingImpl"></param>
        /// <param name="referencingEntity"></param>
        /// <returns></returns>
        private Entity GetPrefillReferencingEntity(ImplMappingDto mappingImpl,
            Entity referencingEntity)
        {
            //get referencing entity logical name
            var childEntityLogicalName =
                _dataService.GetSourceEntityLogicalNameFromMapping(mappingImpl.SelectedMapping.Id);

            //get referencing attribute
            var referencingAttribute =
                mappingImpl.Tabs.FirstOrDefault(x => x.LogicalName == referencingEntity.LogicalName)
                    .Lookup.LogicalName;
            if (referencingAttribute == null) return null;


            //create entity and fill template attribute 
            var prefillReferencingEntity = new Entity(childEntityLogicalName)
            {
                [referencingAttribute] =
                new EntityReference(referencingEntity.LogicalName, referencingEntity.Id)
            };
            UpdateEntityIfCreatedFromDataUploader(prefillReferencingEntity);
            return prefillReferencingEntity;
        }

        private void UpdateEntityIfCreatedFromDataUploader(Entity entity)
        {
            if (_baseDto.CreatorRecordType == MappingImplementationDto.RecordCreator.DataUploader &&
                _dataService.IsAttributeExists(entity, "ddsm_creatorrecordtype"))
                entity["ddsm_creatorrecordtype"] = new OptionSetValue(962080001);
        }
    }
}