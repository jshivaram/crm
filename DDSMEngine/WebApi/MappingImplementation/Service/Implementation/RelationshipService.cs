﻿using System;
using System.Collections.Generic;
using System.Linq;
using MappingImplementation.Dto;
using MappingImplementation.Service.Interfaces;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;

namespace MappingImplementation.Service.Implementation
{
    public class RelationshipService : IRelationshipService
    {
        private readonly BaseDto _baseDto;
        private readonly DataService _dataService;

        private readonly IOrganizationService _orgService;

        public RelationshipService(BaseDto baseDto, DataService dataService)
        {
            _baseDto = baseDto;
            _orgService = baseDto.OrgService;
            _dataService = dataService;
        }


        public EntityCollection GetReferencingEntitiesBySchemaName(Entity referencedEntity, string relationshipName)
        {
            var metadata = GetOneToManyRelationshipMetadata(referencedEntity.LogicalName, relationshipName);

            if (metadata == null) return null;

            var entityCollectionCache = _baseDto.ReferencingEntities;

            var referencedEntityNavigationPropertyName = metadata.ReferencedEntityNavigationPropertyName;

            var keyHash = (referencedEntityNavigationPropertyName + referencedEntity.Id).GetHashCode();


            var currentCollectionCache = new EntityCollection();

            if (entityCollectionCache.ContainsKey(keyHash))
            {
                var tmp = new EntityCollection();
                if (entityCollectionCache.TryGetValue(keyHash, out tmp))
                    currentCollectionCache = tmp;
            }
            else
            {
                var query = new QueryExpression
                {
                    ColumnSet = new ColumnSet(),
                    EntityName = metadata.ReferencingEntity,
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression(metadata.ReferencingAttribute, ConditionOperator.Equal,
                                referencedEntity.Id)
                        }
                    }
                };
                currentCollectionCache = _dataService.RetrieveMultiple(query);
                _baseDto.ReferencingEntities.TryAdd(keyHash, currentCollectionCache);
            }

            return currentCollectionCache;
        }

        /// <summary>
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="relationshipEntity"></param>
        /// <returns></returns>
        public string GetReferencedPropertyName(string targetEntity, string relationshipEntity)
        {
            var referencingMetadata = GetRelationshipMetadata(targetEntity, relationshipEntity);
            if (referencingMetadata == null)
                throw new Exception(
                    $"Doesn't exist relationships between {targetEntity} and  {relationshipEntity}");

            return referencingMetadata.ReferencedEntityNavigationPropertyName;
        }

        /// <summary>
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="relationshipEntities"></param>
        public void AddRelationship(Entity targetEntity, EntityCollection relationshipEntities)
        {
            if (relationshipEntities.Entities.Count == 0) return;

            var referencedPropertyName = GetReferencedPropertyName(targetEntity.LogicalName,
                relationshipEntities[0].LogicalName);

            var relationship = new Relationship(referencedPropertyName);
            targetEntity.RelatedEntities.Add(relationship, relationshipEntities);
        }


        /// <summary>
        ///     Get Refarencing Attribute from entity metadata
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="referencingEntity"></param>
        /// <returns></returns>
        public OneToManyRelationshipMetadata GetRelationshipMetadata(string targetEntity, string referencingEntity)
        {
            var o2M = GetReferencingAttrOneToMany(targetEntity, referencingEntity);
            if (o2M != null)
                return o2M;
            var m2O = GetReferencingAttrManyToOne(targetEntity, referencingEntity);
            if (m2O != null)
                return m2O;
            return null;
        }

        public OneToManyRelationshipMetadata GetReferencingAttrOneToMany(string targetEntity, string referencingEntity)
        {
            var currentEntityMetadata = new List<OneToManyRelationshipMetadata>();


            var oneToManyRelationshipMetadata = _baseDto.OneToManyRelationshipMetadata;
            if (oneToManyRelationshipMetadata.ContainsKey(targetEntity))
            {
                var tmp = new List<OneToManyRelationshipMetadata>();
                if (oneToManyRelationshipMetadata.TryGetValue(targetEntity, out tmp))
                    currentEntityMetadata = tmp;
            }
            else
            {
                var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
                {
                    EntityFilters = EntityFilters.Relationships,
                    LogicalName = targetEntity
                };

                var retrieveBankEntityResponse =
                    (RetrieveEntityResponse) _orgService.Execute(retrieveBankAccountEntityRequest);

                currentEntityMetadata = retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships.ToList();

                _baseDto.OneToManyRelationshipMetadata.TryAdd(targetEntity, currentEntityMetadata);
            }
            var rs = currentEntityMetadata.Where(r => r.ReferencingEntity == referencingEntity).ToList();
            return rs.Count == 0 ? null : rs[0];
        }

        public OneToManyRelationshipMetadata GetReferencingAttrManyToOne(string targetEntity, string referencedEntity)
        {
            var currentEntityMetadata = new List<OneToManyRelationshipMetadata>();

            var oneToManyRelationshipMetadata = _baseDto.ManyToOneRelationshipMetadata;
            if (oneToManyRelationshipMetadata.ContainsKey(targetEntity))
            {
                var tmp = new List<OneToManyRelationshipMetadata>();
                if (oneToManyRelationshipMetadata.TryGetValue(targetEntity, out tmp))
                    currentEntityMetadata = tmp;
            }
            else
            {
                var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
                {
                    EntityFilters = EntityFilters.Relationships,
                    LogicalName = targetEntity
                };

                var retrieveBankEntityResponse =
                    (RetrieveEntityResponse) _orgService.Execute(retrieveBankAccountEntityRequest);

                currentEntityMetadata = retrieveBankEntityResponse.EntityMetadata.ManyToOneRelationships.ToList();
                _baseDto.OneToManyRelationshipMetadata.TryAdd(targetEntity, currentEntityMetadata);
            }

            var rs =
                currentEntityMetadata.Where(r => r.ReferencedEntity == referencedEntity).ToList();

            return rs.Count == 0 ? null : rs[0];
        }

        /// <summary>
        ///     Rebuild!!!!
        /// </summary>
        /// <param name="referencedEntity"></param>
        /// <param name="relationshipName"></param>
        /// <returns></returns>
        public OneToManyRelationshipMetadata GetOneToManyRelationshipMetadata(string referencedEntity,
            string relationshipName)
        {
            var currentEntityMetadata = new List<OneToManyRelationshipMetadata>();

            var oneToManyRelationshipMetadata = _baseDto.OneToManyRelationshipMetadata;
            if (oneToManyRelationshipMetadata.ContainsKey(referencedEntity))
            {
                var tmp = new List<OneToManyRelationshipMetadata>();
                if (oneToManyRelationshipMetadata.TryGetValue(referencedEntity, out tmp))
                    currentEntityMetadata = tmp;
            }
            else
            {
                var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
                {
                    EntityFilters = EntityFilters.Relationships,
                    LogicalName = referencedEntity
                };

                var retrieveBankEntityResponse =
                    (RetrieveEntityResponse) _orgService.Execute(retrieveBankAccountEntityRequest);

                currentEntityMetadata = retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships.ToList();

                _baseDto.OneToManyRelationshipMetadata.TryAdd(referencedEntity, currentEntityMetadata);
            }
            var rs =
                currentEntityMetadata.Where(r => r.SchemaName == relationshipName).ToList();
            return rs.Count == 0 ? null : rs[0];
        }
    }
}