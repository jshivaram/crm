﻿using System;
using System.Collections.Generic;
using MappingImplementation.Dto;
using Microsoft.Xrm.Sdk;

namespace MappingImplementation.Service.Interfaces
{
    public interface IDataService
    {
        /// <summary>
        ///     Get mapping implementation
        /// </summary>
        /// <param name="mappingImplementation"></param>
        /// <returns></returns>
        ImplMappingDto GetMappingImplementationById(Guid mappingImplementation);

        /// <summary>
        ///     Get mapping by id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        Dictionary<string, List<MappingDto>> GetMappingById(Guid mappingId);

        /// <summary>
        ///     Get all mapping related to source mapping
        /// </summary>
        /// <param name="mappingImplementation"></param>
        void GetRelatedMapping(ImplMappingDto mappingImplementation);

        /// <summary>
        ///     Get mapping Direction
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        int GetMappingDirection(Guid mappingId);

        /// <summary>
        /// </summary>
        /// <param name="sourceEntity"></param>
        /// <param name="targetEntity"></param>
        /// <param name="mapping"></param>
        void InitTargetEntityFromSource(Entity sourceEntity, Entity targetEntity, List<MappingDto> mapping);

        /// <summary>
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        string GetSourceEntityLogicalNameFromMapping(Guid mappingId);

        Entity RetreiveEntity(string logicalName, Guid recordId);


        bool IsAttributeExists(Entity entity, string attributeName);
    }
}