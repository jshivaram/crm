﻿using System;
using System.Collections.Generic;
using MappingImplementation.Dto;
using Microsoft.Xrm.Sdk;

namespace MappingImplementation.Service.Interfaces
{
    public interface IMappingImplementationService
    {
        List<Entity> ExecuteMapping(Entity target, Guid mappingImplementationId);

        ImplementationLookupFieldsDto GetImplementationLookup(Entity target, Guid mappingImplementationId);

        /// <summary>
        ///     Map entity by implementation rules
        /// </summary>
        /// <param name="targetEntity">Target entity need to update</param>
        /// <param name="sourceEntity">The field from target entity which contains reference to source entity</param>
        /// <param name="mapping"></param>
        /// <returns>Mapped entity</returns>
        void DoMappingEntity(Entity targetEntity, Entity sourceEntity, List<MappingDto> mapping);

        /// <summary>
        /// </summary>
        /// <param name="mappingImplementation"></param>
        /// <returns></returns>
        List<ImplMappingDto.Tab> GetImplementationTabs(ImplMappingDto mappingImplementation);
        
    }
}