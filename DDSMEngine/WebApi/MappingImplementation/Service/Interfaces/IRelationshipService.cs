﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;

namespace MappingImplementation.Service.Interfaces
{
    public interface IRelationshipService
    {
        EntityCollection GetReferencingEntitiesBySchemaName(Entity referencedEntity, string relationshipName);

        /// <summary>
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="relationshipEntities"></param>
        /// <param name="referencedPropertyName"></param>
        void AddRelationship(Entity targetEntity, EntityCollection relationshipEntities);

        string GetReferencedPropertyName(string targetEntity, string relationshipEntity);

        /// <summary>
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="referencingEntity"></param>
        /// <returns></returns>
        OneToManyRelationshipMetadata GetRelationshipMetadata(string targetEntity, string referencingEntity);
    }
}