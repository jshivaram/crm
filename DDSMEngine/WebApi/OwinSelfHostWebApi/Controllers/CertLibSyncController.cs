﻿using System.Web.Http;
using BLL.Services.Interfaces;
using CertLibSync.Options;
using Microsoft.Xrm.Sdk;

namespace OwinSelfHostWebApi.Controllers
{
    public class CertLibSyncController : ApiController
    {
        private readonly ICertLibSync _certLibSyncService;

        public CertLibSyncController(ICertLibSync certLibSyncService)
        {
            _certLibSyncService = certLibSyncService;
        }

        [HttpPost]
        public string LoadDlcMetadata([FromBody] CertLibOptions options)
        {
            _certLibSyncService.LoadDlcMetadata(options);
            return "DLC metadata sync is started";
        }
        [HttpPost]
        public string LoadDlcProducts([FromBody] CertLibOptions options)
        {
            _certLibSyncService.LoadDlcProducts(options);
            return "DLC products sync is started";
        }

        [HttpPost]
        public string LoadEsMetadata([FromBody] CertLibOptions options)
        {
            _certLibSyncService.LoadEsMetadata(options);
            return "ES metadata sync is started";
        }
        [HttpPost]
        public string LoadEsProducts([FromBody]CertLibOptions options)
        {
            _certLibSyncService.LoadEsProducts(options);
            return "ES products sync is started";
        }


    }
}
