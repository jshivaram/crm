﻿using System.Web.Http;
using BLL.Services.Interfaces;
using DmnEngineApp.Dto;

namespace OwinSelfHostWebApi.Controllers
{
    public class DmnController : ApiController
    {
        private readonly IDmnService _dmnService;

        public DmnController(IDmnService dmnService)
        {
            _dmnService = dmnService;
        }

        [HttpPost]
        public string RunDmn([FromBody] InputArgsDto dmnInputArgs)
        {            
            _dmnService.RunDmnAsync(dmnInputArgs);
            return "Dmn calculation is started";
        }
    }
}