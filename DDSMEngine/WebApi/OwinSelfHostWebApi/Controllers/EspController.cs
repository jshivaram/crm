﻿using System.Web.Http;
using BLL.Services.Interfaces;
using EspCalculation.Options;

namespace OwinSelfHostWebApi.Controllers
{
    public class EspController : ApiController
    {
        private readonly IEspService _espService;

        public EspController(IEspService espService)
        {
            _espService = espService;
        }

        [HttpPost]
        public string CalculateMeasure([FromBody] CalculateOptions options)
        {
            _espService.CalculateMeasureAsync(options);
            return "ESP calculation is started";
        }

        [HttpPost]
        public string GroupCalculateMeasures([FromBody] GroupCalculateOptions groupcalCulateOptions)
        {
            _espService.GroupCalculateMeasuresAsync(groupcalCulateOptions);
            return "ESP Group calculation is started";
        }
    }
}
