﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using BLL.Services.Interfaces;
using DmnEngineApp.Dto;
using OwinSelfHostWebApi.Exceptions;
using OwinSelfHostWebApi.Models;
using Serilog;
using SocketClients;

namespace OwinSelfHostWebApi.Controllers
{
    //enable cors need for cross-domain requests using filter(attribute)
    /// <summary>
    /// origins - url from which you can send cross-domain queries, * - for all urls
    /// methods - list of methods (post, put, get, delete), * - for all methods
    /// </summary>
    public class TestController : ApiController
    {
        //Service that we can get from ninject
        private readonly ITestService _testService;

        //Ninject inject service dependency to controller
        public TestController(ITestService testService)
        {
            Log.Debug("Created controller");
            _testService = testService;
        }

        //example of using WebSocket Client
        [HttpPost]
        public void SendMessage()
        {
            StaticSocketClient.Send(new Guid(), "qwe");
        }

        [HttpGet]
        [ExceptionWithLoggerFilter] //this filter used for catch all exceeptions
        public int GetValue()
        {
            Log.Debug("Debug");
            Log.Information("Launched Method GetValue");
            return 1;
        }

        [HttpPost]
        public async Task OnPressButton()
        {
            await _testService.CreateRecords();
        }

        [HttpPost]
        public async Task TestSocket()
        {
            await _testService.TestSocket();
        }

        [HttpPost]
        //exemple of using data from post request, FromBody attribute is required
        public async Task Test2([FromBody] GenericModel<InputArgsDto> genericModel)
        {
            await _testService.TestSocket();
        }
    }
}