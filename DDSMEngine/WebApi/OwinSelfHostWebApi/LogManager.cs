﻿using System;
using Serilog;

namespace OwinSelfHostWebApi
{
    public static class LogManager
    {
        public static void SetupLog()
        {
            string dirName = AppDomain.CurrentDomain.BaseDirectory;

            Log.Logger = new LoggerConfiguration()
                .WriteTo.RollingFile(dirName + "/logs/_newlog-{Date}.txt")
                .MinimumLevel.Information()
                .CreateLogger();
#if DEBUG
            Log.Logger = new LoggerConfiguration()
                .WriteTo.RollingFile(dirName + "/logs/_newlog-{Date}.txt")
                .MinimumLevel.Debug()
                .CreateLogger();
#endif

        }
    }
}
