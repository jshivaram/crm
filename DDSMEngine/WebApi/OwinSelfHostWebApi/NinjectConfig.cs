﻿using Ninject;
using BLL.Services.Concrete;
using BLL.Services.Interfaces;

namespace OwinSelfHostWebApi
{
    public static class NinjectConfig
    {
        public static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            //name of the CRM connection string
            const string connectionStringName = "CRMConnectionString";

            //Bind reailization of test to interface with connection string name
            kernel.Bind<ITestService>()
                .To<TestService>()
                .WithConstructorArgument(connectionStringName);

            //Bind reailization of DMN Service to interface with connection string name
            kernel.Bind<IDmnService>()
                .To<DmnService>()
                .WithConstructorArgument(connectionStringName);

            //Bind reailization of Esp Service to interface with connection string name
            kernel.Bind<IEspService>()
                .To<EspService>()
                .WithConstructorArgument(connectionStringName);

            //Bind reailization of CertLib Service to interface with connection string name
            kernel.Bind<ICertLibSync>()
                .To<CertLibService>()
                .WithConstructorArgument(connectionStringName);

            //Bind reailization of Data Uploader Service to interface with connection string name
            kernel.Bind<IDataUploaderService>()
                .To<DataUploaderService>()
                .WithConstructorArgument(connectionStringName);

            return kernel;
        }
    }
}
