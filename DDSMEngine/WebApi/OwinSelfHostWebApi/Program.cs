﻿using System;
using System.Configuration;
using System.Web.Http.SelfHost;

namespace OwinSelfHostWebApi
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                LogManager.SetupLog();

                string baseAdress = ConfigurationManager.AppSettings["baseAdress"];
                baseAdress = baseAdress ?? "http://localhost:8080";

                var config = new HttpSelfHostConfiguration(baseAdress);
                RouteManager.AddHttpRoutes(config);

                config.DependencyResolver = new NinjectResolver(NinjectConfig.CreateKernel());

                var server = new HttpSelfHostServer(config);
                server.OpenAsync().Wait();

                Console.WriteLine($"Server is opened on {baseAdress}");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}