﻿using System.Web.Http;
using System.Web.Http.SelfHost;

namespace OwinSelfHostWebApi
{
    public static class RouteManager
    {
        public static void AddHttpRoutes(HttpSelfHostConfiguration configuration)
        {
            configuration.Routes.MapHttpRoute("defaultWithActuion", "api/{controller}/{action}/{id}", new { id = RouteParameter.Optional });
            configuration.Routes.MapHttpRoute("default", "api/{controller}/{id}", new { id = RouteParameter.Optional });
        }
    }
}
