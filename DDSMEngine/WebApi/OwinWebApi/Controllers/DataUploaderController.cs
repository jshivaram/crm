﻿using System.Configuration;
using System.Web.Http;
using BLL.Services.Concrete;
using BLL.Services.Interfaces;
using DataUploader;
using DataUploaderApp.Hellper;

namespace OwinWebApi.Controllers
{
    public class DataUploaderController : ApiController
    {
        private readonly IDataUploaderService _dataUploaderService;

        public DataUploaderController()
        {
            _dataUploaderService = new DataUploaderService("CRMConnectionString");
        }

        [HttpPost]
        public string RunDataUploaderParserWithSettings([FromBody] DataUploaderSettings settings)
        {
            _dataUploaderService.RunDataUploaderParserAsync(settings);
            return "data uploader parser is started";
        }

        [HttpPost]
        public string RunDataUploaderParserWithAppArgs([FromBody] AppArg appArg)
        {
            _dataUploaderService.RunDataUploaderParserAsync(appArg);
            return "data uploader parser is started";
        }

        [HttpPost]
        public string RunDataUploaderCreatorWithSettings([FromBody] DataUploaderSettings settings)
        {
            _dataUploaderService.RunDataUploaderCreatorAsync(settings);
            return "data uploader creator is started";
        }

        [HttpPost]
        public string RunDataUploaderCreatorWithAppArgs([FromBody] AppArg appArg)
        {
            _dataUploaderService.RunDataUploaderCreatorAsync(appArg);
            return "data uploader creator is started";
        }
    }
}