﻿using System.Web;
using System.Web.Http;
using BLL.Services.Concrete;
using BLL.Services.Interfaces;
using DmnEngineApp.Dto;

namespace OwinWebApi.Controllers
{
    public class DmnController : ApiController
    {
        private readonly IDmnService _dmnService;
        
        public DmnController()
        {
            //_dmnService = new DmnService("CRMConnectionString");
            _dmnService = new DmnService();
        }

        [HttpPost]
        public string RunDmnSync([FromBody] InputArgsDto dmnInputArgs)
        {
            //_dmnService.RunDmn(dmnInputArgs);
            //_dmnService.RunDmnAsync(dmnInputArgs);
            return _dmnService.LaunchDmn(dmnInputArgs);
        }
        [HttpPost]
        public void RunDmnAsync([FromBody] InputArgsDto dmnInputArgs)
        {
            
            _dmnService.RunDmnAsync(dmnInputArgs);
            
        }
    }
}