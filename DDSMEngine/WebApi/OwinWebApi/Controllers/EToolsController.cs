﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using BLL.Services.Concrete;
using BLL.Services.Interfaces;
using OwinWebApi.Models;
using Serilog;

namespace OwinWebApi.Controllers
{
    public class EToolsController : ApiController
    {
        private readonly IEToolsService _eToolsService;

        public EToolsController()
        {
            _eToolsService = new EToolsService("CRMConnectionString");
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetFile(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException(nameof(fileName));
            }

            try
            {
                byte[] bytes = await _eToolsService.GetFileAsync(fileName);

                HttpResponseMessage result = Request.CreateResponse(HttpStatusCode.OK);

                result.Content = new ByteArrayContent(bytes);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = fileName
                };

                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Cant download file.");
            }
            finally
            {
                _eToolsService.Dispose();
            }
        }

        [HttpPost]
        public async Task<HttpResponseMessage> SetFile([FromBody] GenericArgModel<Guid> eToolsIdModel)
        {
            try
            {
                await _eToolsService.SetFileAsync(eToolsIdModel.Arg);
                return Request.CreateResponse(HttpStatusCode.OK, "File uploaded");
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Cant upload file.");
            }
            finally
            {
                _eToolsService.Dispose();
            }
        }
    }
}