﻿using System.Web.Http;
using BLL.Services.Concrete;
using BLL.Services.Interfaces;
using EspCalculation.Options;

namespace OwinWebApi.Controllers
{
    public class EspController : ApiController
    {
        private readonly IEspService _espService;

        public EspController()
        {
            _espService = new EspService();
        }

        [HttpPost]
        public string CalculateMeasure([FromBody] CalculateOptions argModel)
        {
            _espService.CalculateMeasureAsync(argModel);
            return "ESP calculation is started";
        }

        [HttpPost]
        public string GroupCalculateMeasures([FromBody] GroupCalculateOptions groupcalCulateOptions)
        {
            _espService.GroupCalculateMeasuresAsync(groupcalCulateOptions);
            return "ESP Group calculation is started";
        }
    }
}
