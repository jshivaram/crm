﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Serilog;

namespace OwinWebApi.Controllers
{
    public class HomeController : ApiController
    {
        public IEnumerable<int> GetValues()
        {
            Log.Debug("test");
            return Enumerable.Range(0, 10);
        }
    }
}