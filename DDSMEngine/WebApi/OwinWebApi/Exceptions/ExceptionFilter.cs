﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using Serilog;

namespace OwinSelfHostWebApi.Exceptions
{
    public class ExceptionWithLoggerFilter : Attribute, IExceptionFilter
    {
        bool IFilter.AllowMultiple => true;

        Task IExceptionFilter.ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext, 
            CancellationToken cancellationToken)
        {
            var except = actionExecutedContext.Exception;

            if (except != null)
            {
                Log.Fatal(except, "");

                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
                    HttpStatusCode.InternalServerError, "Internal Server Error");
            }

            return Task.FromResult<object>(null);
        }
    }
}