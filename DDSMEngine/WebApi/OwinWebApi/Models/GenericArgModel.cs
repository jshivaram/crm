﻿namespace OwinWebApi.Models
{
    public class GenericArgModel<T>
    {
        public T Arg { get; set; }
    }
}