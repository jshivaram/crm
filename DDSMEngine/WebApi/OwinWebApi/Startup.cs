﻿using System.Threading.Tasks;
using System.Web.Http;
using Owin;
using Owin.WebSocket.Extensions;
using OwinWebApi.Sockets;
using RedisServer;

namespace OwinWebApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            LogManager.SetupLog();

            var config = new HttpConfiguration();

            config.Routes.MapHttpRoute("defaultWithActuion", "api/{controller}/{action}/{id}",
                new {id = RouteParameter.Optional});
            config.Routes.MapHttpRoute("default", "api/{controller}/{id}", new {id = RouteParameter.Optional});

            appBuilder.MapWebSocketRoute<DynamicsCrmSocket>();
            appBuilder.MapWebSocketRoute<DynamicsCrmSocket>("/ws");
            appBuilder.MapWebSocketPattern<DynamicsCrmSocket>("/DynamicsCrm");

            appBuilder.UseWebApi(config);

            Task.Run(() => RedisService.ReadCache());
        }
    }
}