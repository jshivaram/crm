﻿using System;

namespace ProcessFlowBuilder.Dto
{
    public class DefinitionAdtDto
    {
        public DefinitionAdtDto()
        {
            ProcessFlowId = Guid.Empty;
        }

        public string Selector { get; set; }
        public string UseSimpleValueFormat { get; set; }
        public string ButtonsVisibility { get; set; }
        public string SelectedField { get; set; }
        public string SelectedFieldLabel { get; set; }
        public string CustomRequestUrl { get; set; }
        public Guid ProcessFlowId { get; set; }
    }
}