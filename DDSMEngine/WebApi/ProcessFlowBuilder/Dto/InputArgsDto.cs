﻿using System;
using System.Collections.Generic;

namespace ProcessFlowBuilder.Dto
{
    public class InputArgsDto
    {
        public InputArgsDto()
        {
            BusinessRuleIds = new List<string>();
            IsExecuteRule = false;
            RecordIds = new List<string>();
            CallBackRecordId = Guid.Empty;
            ProcessFlow = Guid.Empty;
            ProcessFlowStep = Guid.Empty;
        }

        public string TargetEntityName { get; set; }
        public List<string> RecordIds { get; set; }

        public List<string> BusinessRuleIds { get; set; }

        //for callback
        public Guid CallBackWorkflowId { get; set; }
        public Guid CallBackRecordId { get; set; }
        public Guid UserId { get; set; }
        public bool IsExecuteRule { get; set; }

        public Guid ProcessFlow { get; set; }
        public Guid ProcessFlowStep { get; set; }
    }
}