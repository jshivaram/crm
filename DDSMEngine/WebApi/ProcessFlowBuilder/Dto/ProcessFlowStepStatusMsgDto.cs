﻿using System;
using System.Collections.Generic;

namespace ProcessFlowBuilder.Dto
{
    public class ProcessFlowStepStatusMsgDto
    {
        public List<Guid> ProcessFlowIdList;

        public ProcessFlowStepStatusMsgDto()
        {
            ProcessFlowIdList = new List<Guid>();
            Steps = new List<Step>();
            Entities = new List<Entity>();
        }

        public List<Step> Steps { get; set; }

        public List<Entity> Entities { get; set; }


        public class Step
        {
            public Guid Id;
            public int Status;

            public Step()
            {
                Id = Guid.Empty;
                BusinessRules = new List<BusinessRuleData>();
            }

            public List<BusinessRuleData> BusinessRules { get; set; }
        }

        public class Entity
        {
            public Guid Id;
            public string LogicalName;

            public Entity()
            {
                Id = Guid.Empty;
            }
        }


        public class BusinessRuleData
        {
            public Guid Id;
            public int Status;

            public BusinessRuleData()
            {
                Id = Guid.Empty;
            }
        }
    }
}