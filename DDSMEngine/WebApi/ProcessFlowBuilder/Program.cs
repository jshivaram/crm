﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using Newtonsoft.Json;
using ProcessFlowBuilder.Dto;
using ProcessFlowBuilder.Service;

namespace ProcessFlowBuilder
{
    internal class Program
    {
        private IOrganizationService _orgService;

        public static void Main(string[] args)
        {
            try
            {
               
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                var message = ex.Message;
                throw;
            }
        }

        /// <summary>
        ///     Convert list of ids  to list  of Guid
        /// </summary>
        /// <param name="listIds"></param>
        /// <returns></returns>
        public List<Guid> ConvertListIdToListGuid(List<string> listIds)
        {
            var rs = new List<Guid>();
            foreach (var id in listIds)
                rs.Add(new Guid(id));
            return rs;
        }

        private void InitCrmClient()
        {
            var crmConnectionString = ConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString;

            var client = new CrmServiceClient(crmConnectionString);
            _orgService = client.OrganizationServiceProxy;
        }
    }
}