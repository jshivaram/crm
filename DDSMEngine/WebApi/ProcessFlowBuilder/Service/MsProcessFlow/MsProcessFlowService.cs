﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Serilog;

namespace ProcessFlowBuilder.Service.MsProcessFlow
{
    public class MsProcessFlowService
    {
        private readonly MsDataService _dataService;


        public MsProcessFlowService(MsDataService dataService)
        {
            _dataService = dataService;
        }


        /// <summary>
        ///     Create a new record of the entity "ddsm_historypf"
        /// </summary>
        public Guid AddHistoryMsPfRecord()
        {
            try
            {
                //get existing history by current entity
                var historyPfByCurrentEntity = GetExistingPfHistoryByCurrEntity(_dataService.TargetEntity);


                //add calculated entity only for ws message
                _dataService.CalculatedEntities.Add(new EntityReference(_dataService.TargetEntity.LogicalName,
                    _dataService.TargetEntity.Id)); //target entity


                //update  the list ListOriginalPfToHistoryPf with target history and exid from the method
                if (historyPfByCurrentEntity.Entities.Count != 0)
                    return historyPfByCurrentEntity.Entities[0].Id;


                //create  new  history pf
                return CreateHistoryPfRecord();
            }
            catch (Exception e)
            {
                Log.Error(e, "GetExistingPfHistoryByCurrEntity");
                throw;
            }
        }


        /// <summary>
        ///     Get existing history by current entity
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <returns></returns>
        private EntityCollection GetExistingPfHistoryByCurrEntity(Entity targetEntity)
        {
            try
            {
                //declare query statement
                var query = new QueryExpression("ddsm_historyms")
                {
                    ColumnSet =
                        new ColumnSet("ddsm_historymsid", "ddsm_entitylogicalname", "ddsm_recordid"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("ddsm_entitylogicalname", ConditionOperator.Equal,
                                targetEntity.LogicalName),
                            new ConditionExpression("ddsm_recordid", ConditionOperator.Equal, targetEntity.Id.ToString())
                        }
                    }
                };
                //execute qury
                return _dataService.OrgService.RetrieveMultiple(query);
            }
            catch (Exception e)
            {
                Log.Error(e, "GetExistingPfHistoryByCurrEntity");
                throw;
            }
        }


        /// <summary>
        /// </summary>
        /// <returns></returns>
        private Guid CreateHistoryPfRecord()
        {
            try
            {
                var pf = new Entity("ddsm_historyms")
                {
                    ["ddsm_name"] = _dataService.TargetEntity.LogicalName,
                    ["ddsm_recordid"] = _dataService.TargetEntity.Id.ToString(),
                    ["ddsm_entitylogicalname"] = _dataService.TargetEntity.LogicalName
                };

                return _dataService.OrgService.Create(pf);
            }
            catch (Exception e)
            {
                return Guid.Empty;
            }
        }
    }
}