﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using ProcessFlowBuilder.Dto;
using Serilog;

namespace ProcessFlowBuilder.Service.ProcessFlow
{
    public class ProcessFlowService
    {
        private readonly DataService _dataService;

        //Associate entity logical name with process flow
        private readonly Dictionary<Guid, List<string>> _listEntityLnToPf;

        private List<Guid> _listProcessFlowId;
        //Associate id of the entity "ddsm_processflow"  with the entity "ddsm_historypf"
        public Dictionary<Guid, List<Guid>> ListOriginalPfToHistoryPf;
        //Associate the Process Flow  with  the Step
        public Dictionary<Guid, List<Guid>> ListProcessFlowToStep;
        //Associate the Step  with  the Business Rule
        public Dictionary<Guid, List<Guid>> ListStepToBusinessRule;


        public ProcessFlowService(DataService dataService)
        {
            _dataService = dataService;
            ListOriginalPfToHistoryPf = new Dictionary<Guid, List<Guid>>();
            ListProcessFlowToStep = new Dictionary<Guid, List<Guid>>();
            ListStepToBusinessRule = new Dictionary<Guid, List<Guid>>();
            ListProcessFlowIds = new List<Guid>();
            _listEntityLnToPf = new Dictionary<Guid, List<string>>();
            _listProcessFlowId = new List<Guid>();
        }

        //List of Process Flow Ids
        public List<Guid> ListProcessFlowIds { get; set; }


        /// <summary>
        ///     Get list of process flow  Ids from the ADT  by a list of business rule
        /// </summary>
        /// <param name="businessRuleList"></param>
        /// <returns></returns>
        public List<Guid> GetPfIdListByBusinessRuleIdList(List<Guid> businessRuleList)
        {
            try
            {
                //empty result variable
                var rs = new List<Guid>();

                //get process flow according to entity
                var listAllProcessFlowId = GetProcessFlowAccordingToEntity();
                if (listAllProcessFlowId.Count == 0) return rs;

                //get list of process flow  Ids  by a list of business rule ids which executing right now
                var listProcessFlowId = GetAllPfIdListByBusinessRuleIdList(businessRuleList, listAllProcessFlowId);

                //if process flow does not exist exit from the method
                if (listAllProcessFlowId.Count == 0)
                    return rs;

                _listProcessFlowId = listProcessFlowId;
                return _listProcessFlowId;
            }
            catch (Exception e)
            {
                Log.Error(e, "GetPfIdListByBusinessRuleIdList");
                throw;
            }
        }

        /// <summary>
        ///     Get list of process flow  Ids  by a list of business rule ids which executing right now
        /// </summary>
        /// <param name="businessRuleList"></param>
        /// <returns></returns>
        private List<Guid> GetAllPfIdListByBusinessRuleIdList(List<Guid> businessRuleList, List<Guid> listProcessFlowId)
        {
            const string processFlowEntityLogicalName = "ddsm_processflow";
            const string stepEntityLogicalName = "ddsm_step";
            const string businessruleEntityLogicalName = "ddsm_businessrule";


            const string linkStep2BusinessRule = "ddsm_ddsm_step_ddsm_businessrule";
            const string linkProcessFlow2Step = "ddsm_ddsm_processflow_ddsm_step";

            //declare query statement
            var query = new QueryExpression(stepEntityLogicalName)
            {
                ColumnSet = new ColumnSet("ddsm_stepid")
            };

            //declare relationships
            var linkStep2Br = new LinkEntity(stepEntityLogicalName, linkStep2BusinessRule, "ddsm_stepid", "ddsm_stepid",
                JoinOperator.Inner);

            var linkBr2Step = new LinkEntity(linkStep2BusinessRule, businessruleEntityLogicalName, "ddsm_businessruleid",
                "ddsm_businessruleid", JoinOperator.Inner);


            var linkStep2Pf = new LinkEntity(stepEntityLogicalName, linkProcessFlow2Step, "ddsm_stepid",
                "ddsm_stepid", JoinOperator.Inner);

            var linkPf2Step = new LinkEntity("ddsm_ddsm_processflow_ddsm_step", processFlowEntityLogicalName,
                "ddsm_processflowid", "ddsm_processflowid", JoinOperator.Inner);

            //create a query expression specifying the link entity alias and the columns of the link entity that need to return
            linkStep2Br.LinkEntities.Add(linkBr2Step);
            query.LinkEntities.Add(linkStep2Br);

            linkStep2Pf.LinkEntities.Add(linkPf2Step);
            query.LinkEntities.Add(linkStep2Pf);

            query.LinkEntities[0].LinkEntities[0].EntityAlias = "br";

            query.LinkEntities[0].LinkEntities[0].Columns.AddColumn("ddsm_businessruleid");

            query.LinkEntities[1].LinkEntities[0].EntityAlias = "pf";

            query.LinkEntities[1].LinkEntities[0].Columns.AddColumn("ddsm_processflowid");

            //create the qury condition
            linkBr2Step.LinkCriteria = new FilterExpression();

            linkBr2Step.LinkCriteria.AddCondition(new ConditionExpression("ddsm_businessruleid", ConditionOperator.In,
                businessRuleList));

            linkPf2Step.LinkCriteria.AddCondition(new ConditionExpression("ddsm_processflowid", ConditionOperator.In,
                listProcessFlowId));


            //execute query
            var stepsResultSet = _dataService.OrgService.RetrieveMultiple(query);

            var listProcessFlowIds = new List<Guid>();
            //get data from the "stepsResultSet"
            foreach (var step in stepsResultSet.Entities)
            {
                //all attributes from the entity "ddsm_step"
                var stepAttr = step.Attributes;

                //get  process flow id
                var pf = new Guid(((AliasedValue) stepAttr["pf.ddsm_processflowid"]).Value.ToString());

                //get  business rule id
                var businessruleid = new Guid(((AliasedValue) stepAttr["br.ddsm_businessruleid"]).Value.ToString());

                UpdateListStepToBusinessRule(step.Id, businessruleid);
                UpdateListProcessFlowToStep(pf, step.Id);
                if (listProcessFlowIds.Contains(pf)) continue;
                listProcessFlowIds.Add(pf);
            }

            return listProcessFlowIds;
        }

        /// <summary>
        ///     Get process flow according to entity
        /// </summary>
        /// <param name="processFlowIds"></param>
        /// <returns></returns>
        private List<Guid> GetProcessFlowAccordingToEntity()
        {
            //get all additionaldatatype with type  "Business Process Flows"
            var query = new QueryExpression("ddsm_additionaldatatype")
            {
                ColumnSet = new ColumnSet("ddsm_definition", "ddsm_targetentity"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_datatype", ConditionOperator.Equal, 962080002),
                        new ConditionExpression("ddsm_targetentity", ConditionOperator.Equal,
                            _dataService.TargetEntity.LogicalName)
                    }
                }
            };
            //Execute query
            var adtResultSet = _dataService.OrgService.RetrieveMultiple(query).Entities;

            var listProcessFlowIds = new List<Guid>();

            //get data from the adr record
            foreach (var adt in adtResultSet)
            {
                object ddsmDefinition = null;
                //get adt definition 
                if (adt.Attributes.TryGetValue("ddsm_definition", out ddsmDefinition))
                {
                    //deserialize adt definition to object  
                    var adtObj = JsonConvert.DeserializeObject<DefinitionAdtDto>(ddsmDefinition.ToString());

                    //if (!allProcessFlowByBr.Contains(adtObj.ProcessFlowId)) continue;

                    object ddsmTargetentityLn = null;
                    //get adt target entity
                    if (adt.Attributes.TryGetValue("ddsm_targetentity", out ddsmTargetentityLn))
                        UpdateListEntityLnToPf(adtObj.ProcessFlowId, ddsmTargetentityLn.ToString());

                    if (listProcessFlowIds.Contains(adtObj.ProcessFlowId)) continue;
                    listProcessFlowIds.Add(adtObj.ProcessFlowId);
                }
            }
            return listProcessFlowIds;
        }

        /// <summary>
        ///     Create a new record of the entity "ddsm_historypf"
        /// </summary>
        public void AddHistoryPfRecord()
        {
            try
            {
                var listNeedCreateHistory = new Dictionary<Guid, List<EntityReference>>();

                //get existing history by current entity
                var historyPfByCurrentEntity = GetExistingPfHistoryByCurrEntity(_dataService.TargetEntity);
                //get list of entity reference that relates to the target Entity
                var relatedEntities = GetRelatedEntity(_dataService.TargetEntity);
                //get existing history_pf data that relates to the target entity
                var historyPfByRelatedEntity = GetExistingPfHistoryByRelatedEntity(relatedEntities);

                //update  the list ListOriginalPfToHistoryPf with related history or create new history_pf
                if (historyPfByRelatedEntity.Count != 0)
                    GetOnRelatedHistoryData(historyPfByRelatedEntity, listNeedCreateHistory, relatedEntities);
                else
                    foreach (var pfId in _listProcessFlowId)
                        listNeedCreateHistory.Add(pfId, relatedEntities);


                //add calculated entity only for ws message
                _dataService.CalculatedEntities.Add(new EntityReference(_dataService.TargetEntity.LogicalName,
                    _dataService.TargetEntity.Id)); //target entity
                _dataService.CalculatedEntities.AddRange(relatedEntities); //related entity

                //update  the list ListOriginalPfToHistoryPf with target history and exid from the method
                if (historyPfByCurrentEntity.Count != 0)
                    GetOnTargetEntityHistoryData(historyPfByCurrentEntity, listNeedCreateHistory);
                else
                    foreach (var pfId in _listProcessFlowId)
                        if (listNeedCreateHistory.ContainsKey(pfId))
                        {
                            var item = new EntityReference(_dataService.TargetEntity.LogicalName,
                                _dataService.TargetEntity.Id);
                            var el = listNeedCreateHistory[pfId];
                            if (el.Contains(item)) continue;
                            el.Add(item);
                        }
                        else
                        {
                            listNeedCreateHistory.Add(pfId,
                                new List<EntityReference>
                                {
                                    new EntityReference(_dataService.TargetEntity.LogicalName,
                                        _dataService.TargetEntity.Id)
                                });
                        }

                //create  new  history pf
                CreateHistoryPf(listNeedCreateHistory);
            }
            catch (Exception e)
            {
                Log.Error(e, "GetExistingPfHistoryByCurrEntity");
                throw;
            }
        }

        private void GetOnTargetEntityHistoryData(Dictionary<Guid, EntityReference> historyPfByCurrentEntity,
            Dictionary<Guid, List<EntityReference>> listNeedCreateHistory)
        {
            //loop on list the _listProcessFlowId
            foreach (var pfId in _listProcessFlowId)
                //if history not contains the process flow id
                if (!historyPfByCurrentEntity.ContainsKey(pfId))
                {
                    if (listNeedCreateHistory.ContainsKey(pfId))
                    {
                        var item = new EntityReference(_dataService.TargetEntity.LogicalName,
                            _dataService.TargetEntity.Id);
                        var el = listNeedCreateHistory[pfId];
                        if (el.Contains(item)) continue;
                        el.Add(item);
                    }
                    else
                    {
                        listNeedCreateHistory.Add(pfId, new List<EntityReference>
                        {
                            new EntityReference(_dataService.TargetEntity.LogicalName,
                                _dataService.TargetEntity.Id)
                        });
                    }
                }
                else
                {
                    var item = historyPfByCurrentEntity[pfId];
                    if (!ListOriginalPfToHistoryPf.ContainsKey(pfId))
                        ListOriginalPfToHistoryPf.Add(pfId, new List<Guid> {item.Id});
                    //if history contains the process flow id
                    var el = ListOriginalPfToHistoryPf[pfId];

                    if (el.Contains(item.Id)) continue;
                    el.Add(item.Id);
                }
        }

        private void GetOnRelatedHistoryData(Dictionary<Guid, List<EntityReference>> historyPfByRelatedEntity,
            Dictionary<Guid, List<EntityReference>> listNeedCreateHistory, List<EntityReference> relatedEntities)
        {
            //loop on list the _listProcessFlowId
            foreach (var pfId in _listProcessFlowId)
                //if history contains the process flow id
                if (historyPfByRelatedEntity.ContainsKey(pfId))
                    //explore each related entity and add to the ListOriginalPfToHistoryPf if history on this inctance  exists
                    foreach (var historyEntityRef in historyPfByRelatedEntity[pfId])
                        if (ListOriginalPfToHistoryPf.ContainsKey(pfId))
                        {
                            var el = ListOriginalPfToHistoryPf[pfId];
                            if (el.Contains(historyEntityRef.Id)) continue;
                            el.Add(historyEntityRef.Id);
                        }
                        else
                        {
                            ListOriginalPfToHistoryPf.Add(pfId, new List<Guid> {historyEntityRef.Id});
                        }
                else
                    listNeedCreateHistory.Add(pfId, relatedEntities); //need create a history for this entities
        }

        /// <summary>
        ///     Get existing history by current entity
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <returns></returns>
        private Dictionary<Guid, EntityReference> GetExistingPfHistoryByCurrEntity(Entity targetEntity)
        {
            try
            {
                //ddsm_processflow/ddsm_historypf
                var listPfId2HistoryPfId = new Dictionary<Guid, EntityReference>();

                //if target entity is null exit from the method
                if (targetEntity == null)
                    return listPfId2HistoryPfId;

                //declare query statement
                var query = new QueryExpression("ddsm_historypf")
                {
                    ColumnSet =
                        new ColumnSet("ddsm_historypfid", "ddsm_processflow", "ddsm_entitylogicalname", "ddsm_recordid"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("ddsm_processflow", ConditionOperator.In, _listProcessFlowId),
                            new ConditionExpression("ddsm_entitylogicalname", ConditionOperator.Equal,
                                targetEntity.LogicalName),
                            new ConditionExpression("ddsm_recordid", ConditionOperator.Equal, targetEntity.Id.ToString())
                        }
                    }
                };
                //execute qury
                var historyPfResulSet = _dataService.OrgService.RetrieveMultiple(query);

                //get history data
                foreach (var historyPf in historyPfResulSet.Entities)
                {
                    object processflow = null;
                    if (!historyPf.Attributes.TryGetValue("ddsm_processflow", out processflow)) continue;
                    var pfEntityRef = (EntityReference) processflow;
                    if (!_listProcessFlowId.Contains(pfEntityRef.Id)) continue;
                    listPfId2HistoryPfId.Add(pfEntityRef.Id, historyPf.ToEntityReference());
                }
                return listPfId2HistoryPfId;
            }
            catch (Exception e)
            {
                Log.Error(e, "GetExistingPfHistoryByCurrEntity");
                throw;
            }
        }

        public List<EntityReference> GetRelatedEntity(Entity targetEntity)
        {
            if (_listEntityLnToPf.Count == 0)
                //if list of   "Entity logical name" To "process flow" is empty exit
                if (_listEntityLnToPf.Count == 0)
                    return new List<EntityReference>();

            //create  Entity Request
            var entityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = targetEntity.LogicalName
            };

            //get Entity Metadata
            var entityResponse = (RetrieveEntityResponse) _dataService.OrgService.Execute(entityRequest);

            //get ManyToOneRelationships Metadata from the current entity
            var manyToOneRelationships = entityResponse.EntityMetadata.ManyToOneRelationships;

            //list of entity logical name
            var listEntityLogicalName = new List<string>();

            //get list entity logical name according to process flows
            foreach (var item in _listEntityLnToPf)
            foreach (var entityLogicalName in item.Value)
                if (!listEntityLogicalName.Contains(entityLogicalName))
                    listEntityLogicalName.Add(entityLogicalName);

            var listEntityMetadata = new List<OneToManyRelationshipMetadata>();

            //get related entity according to current entity
            foreach (var entityLogicalName in listEntityLogicalName)
            {
                var entityMetadata = manyToOneRelationships.Where(e => e.ReferencedEntity == entityLogicalName).ToList();
                if (entityMetadata.Count == 0) continue;
                listEntityMetadata.Add(entityMetadata[0]);
            }

            //get list of entity reference that relates to the target Entity
            return GetListEntityByMetadate(targetEntity, listEntityMetadata);
        }

        /// <summary>
        ///     Get list of entity reference that relates to the target Entity
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="relMetadatas"></param>
        /// <returns></returns>
        private List<EntityReference> GetListEntityByMetadate(Entity targetEntity,
            IEnumerable<OneToManyRelationshipMetadata> relMetadatas)
        {
            var columnSet = new ColumnSet();
            var listLinkName = new List<string>();

            var listRelatedEntity = new List<EntityReference>();

            //loop by all related entities metadata
            foreach (var relMetadata in relMetadatas)
            {
                columnSet.AddColumn(relMetadata.ReferencingAttribute);
                listLinkName.Add(relMetadata.ReferencingAttribute);
            }

            //retreive entities
            var rs = _dataService.OrgService.Retrieve(targetEntity.LogicalName, targetEntity.Id, columnSet);

            //get related entity
            foreach (var linkName in listLinkName)
            {
                object referecingEntity = null;
                if (rs.Attributes.TryGetValue(linkName, out referecingEntity))
                {
                    var entityRef = (EntityReference) referecingEntity;
                    listRelatedEntity.Add(entityRef);
                }
            }

            return listRelatedEntity;
        }

        /// <summary>
        ///     Get existing history_pf data that relates to the target entity
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="relatedEntity"></param>
        /// <returns></returns>
        private Dictionary<Guid, List<EntityReference>> GetExistingPfHistoryByRelatedEntity(
            List<EntityReference> relatedEntity)
        {
            try
            {
                var listPfId2HistoryPfId = new Dictionary<Guid, List<EntityReference>>();

                //tmp variables 
                //entity logical name  list
                var entityLnList = new List<string>();
                //entity id  list
                var entityIdList = new List<string>();

                //get related data
                foreach (var entity in relatedEntity)
                {
                    entityIdList.Add(entity.Id.ToString());
                    entityLnList.Add(entity.LogicalName);
                }

                if (entityIdList.Count == 0 || entityLnList.Count == 0)
                    return listPfId2HistoryPfId;

                //declare query statement
                var query = new QueryExpression("ddsm_historypf")
                {
                    ColumnSet =
                        new ColumnSet("ddsm_historypfid", "ddsm_processflow", "ddsm_entitylogicalname", "ddsm_recordid"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("ddsm_processflow", ConditionOperator.In, _listProcessFlowId),
                            new ConditionExpression("ddsm_entitylogicalname", ConditionOperator.In, entityLnList),
                            new ConditionExpression("ddsm_recordid", ConditionOperator.In, entityIdList)
                        }
                    }
                };

                //execute qury
                var historyPfResulSet = _dataService.OrgService.RetrieveMultiple(query);

                //get history Pf  data
                foreach (var item in historyPfResulSet.Entities)
                {
                    object processflow = null;
                    if (!item.Attributes.TryGetValue("ddsm_processflow", out processflow)) continue;
                    var pfEntityRef = (EntityReference) processflow;
                    var listHistoryPf = new List<EntityReference> {item.ToEntityReference()};
                    if (listPfId2HistoryPfId.ContainsKey(pfEntityRef.Id))
                    {
                        listPfId2HistoryPfId[pfEntityRef.Id].Add(item.ToEntityReference());
                        continue;
                    }
                    listPfId2HistoryPfId.Add(pfEntityRef.Id, listHistoryPf);
                }
                return listPfId2HistoryPfId;
            }
            catch (Exception e)
            {
                Log.Error(e, "GetExistingPfHistoryByCurrEntity");
                throw;
            }
        }

        /// <summary>
        ///     Create  new  history pf
        /// </summary>
        private void CreateHistoryPf(Dictionary<Guid, List<EntityReference>> listEntityRef)
        {
            //get executeMultipleRequest
            var multipleRequest = CreateExecuteMultipleRequest();

            //get response
            var response = CreateHistoryPfRecord(multipleRequest, listEntityRef);

            //Explore response
            foreach (var item in response.Responses)
            {
                var request = (Entity) multipleRequest.Requests[item.RequestIndex].Parameters["Target"];
                var originalProcessFlow = (EntityReference) request.Attributes["ddsm_processflow"];
                var historyProcessFlow = new Guid(item.Response.Results["id"].ToString());
                //update the ListOriginalPfToHistoryPf
                UpdateListOriginalPfToHistoryPf(originalProcessFlow.Id, historyProcessFlow);
            }
        }

        /// <summary>
        ///     Update the ListOriginalPfToHistoryPf
        /// </summary>
        /// <param name="originalProcessFlowId"></param>
        /// <param name="historyProcessFlowId"></param>
        private void UpdateListOriginalPfToHistoryPf(Guid originalProcessFlowId, Guid historyProcessFlowId)
        {
            if (ListOriginalPfToHistoryPf.ContainsKey(originalProcessFlowId))
            {
                ListOriginalPfToHistoryPf[originalProcessFlowId].Add(historyProcessFlowId);
                return;
            }

            ListOriginalPfToHistoryPf.Add(originalProcessFlowId, new List<Guid> {historyProcessFlowId});
        }

        /// <summary>
        /// </summary>
        /// <param name="executeMultipleRequest"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        private ExecuteMultipleResponse CreateHistoryPfRecord(ExecuteMultipleRequest executeMultipleRequest,
            Dictionary<Guid, List<EntityReference>> target)
        {
            try
            {
                foreach (var processFlowId in target)
                foreach (var targetEntity in processFlowId.Value)
                {
                    var pf = new Entity("ddsm_historypf")
                    {
                        ["ddsm_processflow"] = new EntityReference("ddsm_processflow", processFlowId.Key),
                        ["ddsm_name"] = _dataService.GetFiledValue("ddsm_processflow", processFlowId.Key, "ddsm_name"),
                        ["ddsm_recordid"] = targetEntity.Id.ToString(),
                        ["ddsm_entitylogicalname"] = targetEntity.LogicalName
                    };

                    var updateRequest = new CreateRequest
                    {
                        Target = pf
                    };
                    executeMultipleRequest.Requests.Add(updateRequest);
                }

                return (ExecuteMultipleResponse) _dataService.OrgService.Execute(executeMultipleRequest);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        ///     Update the ListProcessFlowToStep
        /// </summary>
        /// <param name="processFlowId"></param>
        /// <param name="stepId"></param>
        private void UpdateListProcessFlowToStep(Guid processFlowId, Guid stepId)
        {
            if (ListProcessFlowToStep.ContainsKey(processFlowId))
            {
                var el = ListProcessFlowToStep[processFlowId];
                if (!el.Contains(stepId))
                    el.Add(stepId);
                return;
            }
            ListProcessFlowToStep.Add(processFlowId, new List<Guid> {stepId});
        }

        /// <summary>
        ///     Update the  ListStepToBusinessRule
        /// </summary>
        /// <param name="stepId"></param>
        /// <param name="businessRuleId"></param>
        private void UpdateListStepToBusinessRule(Guid stepId, Guid businessRuleId)
        {
            if (ListStepToBusinessRule.ContainsKey(stepId))
            {
                var el = ListStepToBusinessRule[stepId];
                if (!el.Contains(businessRuleId))
                    el.Add(businessRuleId);
                return;
            }
            ListStepToBusinessRule.Add(stepId, new List<Guid> {businessRuleId});
        }

        /// <summary>
        ///     Update the _listEntityLnToPf
        /// </summary>
        /// <param name="processFlowId"></param>
        /// <param name="targetEntityLogicalName"></param>
        private void UpdateListEntityLnToPf(Guid processFlowId, string targetEntityLogicalName)
        {
            if (_listEntityLnToPf.ContainsKey(processFlowId))
            {
                var el = _listEntityLnToPf[processFlowId];
                if (!el.Contains(targetEntityLogicalName))
                    el.Add(targetEntityLogicalName);
                return;
            }
            _listEntityLnToPf.Add(processFlowId, new List<string> {targetEntityLogicalName});
        }

        /// <summary>
        ///     Create CreateExecuteMultipleRequest
        /// </summary>
        /// <param name="returnResponses"></param>
        /// <returns></returns>
        private static ExecuteMultipleRequest CreateExecuteMultipleRequest(bool returnResponses = true)
        {
            //create a new history
            var executeMultipleRequest = new ExecuteMultipleRequest
            {
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = false,
                    ReturnResponses = returnResponses
                },
                Requests = new OrganizationRequestCollection()
            };
            return executeMultipleRequest;
        }
    }
}