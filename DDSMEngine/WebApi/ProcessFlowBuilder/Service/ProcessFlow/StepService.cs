﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;

namespace ProcessFlowBuilder.Service.ProcessFlow
{
    public class StepService
    {
        private readonly DataService _dataService;
        private readonly Dictionary<Guid, List<Guid>> _originalPfToHistoryPf;
        private readonly Dictionary<Guid, List<Guid>> _processFlowToStep;
        public Dictionary<Guid, Guid> OriginalStepToHistoryStep;

        public StepService(DataService dataService)
        {
            _processFlowToStep = dataService.ListProcessFlowToStep;
            _originalPfToHistoryPf = dataService.ListOriginalPfToHistoryPf;
            _dataService = dataService;
            OriginalStepToHistoryStep = new Dictionary<Guid, Guid>();
        }

        public List<Guid> ProcessFlowList { get; set; }


        /// <summary>
        ///     Create a new ddsm_historystepstatus
        /// </summary>
        /// <param name="processFlowStepId"></param>
        /// <returns></returns>
        public void AddProcessFlowStep()
        {
            try
            {
                if (_processFlowToStep.Count == 0)
                    return;
                var executeMultipleRequest = CreateExecuteMultipleRequest();

                var responseWithResults = CreateHistoryStepStatus(executeMultipleRequest, _processFlowToStep);
                //Explore response
                foreach (var item in responseWithResults.Responses)
                {
                    var request = (Entity) executeMultipleRequest.Requests[item.RequestIndex].Parameters["Target"];
                    var originalStep = (EntityReference) request.Attributes["ddsm_step"];
                    var historyStep = new Guid(item.Response.Results["id"].ToString());
                    //ddsm_step, list of ddsm_historystepstatus
                    UpdateOriginalStepToHistoryStep(originalStep.Id, historyStep);
                }

                var requestWithResults2 = new ExecuteMultipleRequest
                {
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = false,
                        ReturnResponses = false
                    },
                    Requests = new OrganizationRequestCollection()
                };
                foreach (var pf in _processFlowToStep)
                foreach (var step in pf.Value)
                {
                    if (!OriginalStepToHistoryStep.ContainsKey(step)) continue;

                    if (_originalPfToHistoryPf.ContainsKey(pf.Key))
                        foreach (var pfHistoryId in _originalPfToHistoryPf[pf.Key])
                        {
                            var request = new AssociateRequest();
                            var historystepstatus = new EntityReference("ddsm_historystepstatus",
                                OriginalStepToHistoryStep[step]);
                            var pfHistory = new EntityReference("ddsm_historypf", pfHistoryId);
                            request.Target = historystepstatus;
                            request.RelatedEntities = new EntityReferenceCollection {pfHistory};
                            request.Relationship = new Relationship("ddsm_ddsm_historypf_ddsm_historystepstatus");

                            requestWithResults2.Requests.Add(request);
                        }
                }
                _dataService.OrgService.Execute(requestWithResults2);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void UpdateOriginalStepToHistoryStep(Guid originalStepId, Guid historyStep)
        {
            if (OriginalStepToHistoryStep.ContainsKey(originalStepId))
                return;

            OriginalStepToHistoryStep.Add(originalStepId, historyStep);
        }

        /// <summary>
        /// </summary>
        /// <param name="executeMultipleRequest"></param>
        /// <param name="processFlowToStep"></param>
        /// <returns></returns>
        private ExecuteMultipleResponse CreateHistoryStepStatus(ExecuteMultipleRequest executeMultipleRequest,
            Dictionary<Guid, List<Guid>> processFlowToStep)
        {
            try
            {
                //explore all process flow
                foreach (var processFlowId in processFlowToStep)
                {
                    if (!_originalPfToHistoryPf.ContainsKey(processFlowId.Key))
                        continue;
                    //var historyProcessFlowIds = _originalPfToHistoryPf[processFlowId.Key];

                    //foreach (var historyProcessFlowId in historyProcessFlowIds)
                    foreach (var stepId in processFlowId.Value)
                    {
                        var step = new Entity("ddsm_historystepstatus")
                        {
                            //["ddsm_processflowhistory"] =new EntityReference("ddsm_historypf", historyProcessFlowId),
                            ["ddsm_step"] = new EntityReference("ddsm_step", stepId),
                            ["ddsm_name"] = _dataService.GetFiledValue("ddsm_step", stepId, "ddsm_name"),
                            ["ddsm_status"] = new OptionSetValue((int) DataService.RecordStatus.Active)
                        };

                        var createRequest = new CreateRequest
                        {
                            Target = step
                        };
                        executeMultipleRequest.Requests.Add(createRequest);
                    }
                }

                return (ExecuteMultipleResponse) _dataService.OrgService.Execute(executeMultipleRequest);
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public void UpdateStepStatus(DataService.RecordStatus status)
        {
            try
            {
                var executeMultipleRequest = CreateExecuteMultipleRequest();
                foreach (var item in OriginalStepToHistoryStep)
                {
                    var entity = new Entity("ddsm_historystepstatus", item.Value)
                    {
                        ["ddsm_status"] = new OptionSetValue((int) status)
                    };
                    var updateRequest = new UpdateRequest
                    {
                        Target = entity
                    };
                    executeMultipleRequest.Requests.Add(updateRequest);
                }
                _dataService.OrgService.Execute(executeMultipleRequest);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        /// <summary>
        ///     Create CreateExecuteMultipleRequest
        /// </summary>
        /// <param name="returnResponses"></param>
        /// <returns></returns>
        private ExecuteMultipleRequest CreateExecuteMultipleRequest(bool returnResponses = true)
        {
            //create a new history
            var executeMultipleRequest = new ExecuteMultipleRequest
            {
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = false,
                    ReturnResponses = returnResponses
                },
                Requests = new OrganizationRequestCollection()
            };
            return executeMultipleRequest;
        }
    }
}