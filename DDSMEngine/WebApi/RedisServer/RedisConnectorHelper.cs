﻿using System;
using StackExchange.Redis;

namespace RedisServer
{
    internal class RedisConnectorHelper
    {
        private static readonly Lazy<ConnectionMultiplexer> lazyConnection;

        static RedisConnectorHelper()
        {
            lazyConnection =
                new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect("10.30.30.73"));
        }

        public static ConnectionMultiplexer Connection => lazyConnection.Value;
    }
}