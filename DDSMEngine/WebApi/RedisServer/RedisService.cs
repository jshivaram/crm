﻿using System.Collections.Generic;
using System.Threading;
using DmnEngineApp;
using DmnEngineApp.Dto;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;

namespace RedisServer
{
    public class RedisService
    {
        public static IOrganizationService OrgService;

        public static void ReadCache()
        {
            var isMore = true;
            while (isMore)
            {
                Thread.Sleep(5000);
                RunDmnRequest();
                RunDmnBatchRequest();
            }
        }

        private static void RunDmnRequest()
        {
            var groupedRequestList = GetDmnRequest("DMN_REQUEST");

            //call dmn foreach  request
            foreach (var item in groupedRequestList)
                LaunchDmn(item);
        }


        private static void RunDmnBatchRequest()
        {
            var groupedRequestList = GetDmnRequest("DMN_BATCH_REQUEST");
            //call dmn foreach  request
            foreach (var item in groupedRequestList)
                LaunchDmn(item);
        }


        private static List<InputArgsDto> GetDmnRequest(string key)
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();

            var start = 0;
            var end = 100;


            //get data from cache. from 0 to 100
            var data = cache.ListRange(key, start, end);

            //create a list with request
            var requests = new List<string>();
            foreach (var item in data)
            {
                requests.Add(item);
                cache.ListRemove(key, item);
            }
            //grouped requests by entity an business rules
            return CreateGroupRequest(requests);
        }


        /// <summary>
        /// </summary>
        /// <param name="inputArgsDto"></param>
        public static void LaunchDmn(InputArgsDto inputArgsDto)
        {
            var dmnEntryPoint = new DmnEntryPoint(inputArgsDto);
            var isDmnEngineFree = DmnEntryPoint.IsDmnEngineFree;
            while (!isDmnEngineFree)
            {
                Thread.Sleep(2000);
                isDmnEngineFree = DmnEntryPoint.IsDmnEngineFree;
            }

            dmnEntryPoint.LaunchDmn(OrgService);
        }

        public static void SaveRequest(string dmnRequest, string key)
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            cache.ListRightPush(key, dmnRequest);
        }

        private static List<InputArgsDto> CreateGroupRequest(List<string> requestList)
        {
            //result list
            var result = new List<InputArgsDto>();

            //tempopary Dictionary
            var listRsTmp = new Dictionary<string, InputArgsDto>();

            //analyse each request
            foreach (var request in requestList)
            {
                //Deserialize request
                var requestObj = JsonConvert.DeserializeObject<InputArgsDto>(request);

                // create list of business rule
                var businessRuleList = string.Join(",", requestObj.BusinessRuleIds);

                //if in first time add the request to temporary dictionary
                if (listRsTmp.Count == 0)
                {
                    listRsTmp.Add(businessRuleList, requestObj);
                }
                else
                {
                    //found the same requst by business rule
                    if (listRsTmp.ContainsKey(businessRuleList))
                    {
                        //same requst by business rule
                        var sameRequst = listRsTmp[businessRuleList];

                        //Compare logical entity name in the two requests
                        if (requestObj.TargetEntityName == sameRequst.TargetEntityName)
                            if (!sameRequst.RecordIds.Contains(requestObj.RecordIds[0]))
                                sameRequst.RecordIds.Add(requestObj.RecordIds[0]);
                    }
                    else
                    {
                        listRsTmp.Add(businessRuleList, requestObj);
                    }
                }
            }
            //create list of InputArgsDto 
            foreach (var item in listRsTmp)
                result.Add(item.Value);
            return result;
        }
    }
}