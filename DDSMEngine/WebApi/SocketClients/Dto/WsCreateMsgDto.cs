﻿using System;

namespace SocketClients.Dto
{
    public class WsCreateMsgDto
    {
        public string EntityLogicalName;
        public Guid MessageID;
        public Guid UserId;
        
        public WsCreateMsgDto()
        {
            MessageID = Guid.Empty;
            UserId = Guid.Empty;
        }
    }
}