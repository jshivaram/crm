﻿using Newtonsoft.Json;
using SocketClients;
using SocketClients.Dto;

namespace DmnEngineApp.Service
{
    public class WSMessageService
    {
        public enum CalcMessageStatus
        {
            START,
            FINISH
        }

        public enum MessageType
        {
            SUCCESS,
            INFORMATION,
            ERROR
        }


        private readonly WsResultMsgDto _wsMessageDto;

        public WSMessageService(WsCreateMsgDto inParam)
        {
            var wsMessageDto = new WsResultMsgDto
            {
                PrimaryEntityName = inParam.EntityLogicalName,
                MessageID = inParam.MessageID,
                UserId = inParam.UserId
            };
            _wsMessageDto = wsMessageDto;
        }

        /// <summary>
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="message"></param>
        /// <param name="status"></param>
        public void SendMessage(MessageType messageType, string message, CalcMessageStatus status)
        {
            _wsMessageDto.MessageType = messageType.ToString();
            _wsMessageDto.Message = message;
            _wsMessageDto.Status = status.ToString();
            StaticSocketClient.Send(_wsMessageDto.UserId, JsonConvert.SerializeObject(_wsMessageDto));
        }
    }
}