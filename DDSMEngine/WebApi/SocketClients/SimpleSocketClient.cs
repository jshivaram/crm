﻿using System;
using System.Configuration;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SocketClients
{
    public static class SimpleSocketClient
    {
        public static async Task SendMessageAsync(string message)
        {
            using (var client = new ClientWebSocket())
            {
                string uriStr = ConfigurationManager.ConnectionStrings["SocketConnection"].ToString();
                var uri = new Uri(uriStr + "/" + SocketChannelEnum.DynamicsCrm);

                await client.ConnectAsync(uri, CancellationToken.None);

                byte[] bytes = Encoding.UTF8.GetBytes(message);
                var arraySegment = new ArraySegment<byte>(bytes);

                await client.SendAsync(arraySegment, WebSocketMessageType.Text, true, CancellationToken.None);
            }
        }

        public static void SendMessage(string message)
        {
            using (var client = new ClientWebSocket())
            {
                string uriStr = ConfigurationManager.ConnectionStrings["SocketConnection"].ToString();
                var uri = new Uri(uriStr + "/" + SocketChannelEnum.DynamicsCrm);

                client.ConnectAsync(uri, CancellationToken.None).Wait();

                byte[] bytes = Encoding.UTF8.GetBytes(message);
                var arraySegment = new ArraySegment<byte>(bytes);

                client.SendAsync(arraySegment, WebSocketMessageType.Text, true, CancellationToken.None).Wait();
            }
        }
    }
}
