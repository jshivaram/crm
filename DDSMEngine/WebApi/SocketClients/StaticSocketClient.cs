﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Threading.Tasks;
using WebSocketSharp;

namespace SocketClients
{
    public static class StaticSocketClient
    {
        public static void Send(Guid id, string message, SocketChannelEnum socketChannel = SocketChannelEnum.DynamicsCrm)
        {
           SimpleSocketClient.SendMessage(message);
        }

        public static async Task SendAsync(Guid id, string message,
            SocketChannelEnum socketChannel = SocketChannelEnum.DynamicsCrm)
        {
            await SimpleSocketClient.SendMessageAsync(message);
        }
    }
}