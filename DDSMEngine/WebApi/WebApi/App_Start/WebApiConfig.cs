﻿using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            string dirName = AppDomain.CurrentDomain.BaseDirectory;

            Log.Logger = new LoggerConfiguration()
                .WriteTo.Async(a=> a.RollingFile(dirName + "/logs/_newlog-{Date}.txt"))
                .MinimumLevel.Information()
                .CreateLogger();
#if DEBUG
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Async(a=> a.RollingFile(dirName + "/logs/_newlog-{Date}.txt"))
                .MinimumLevel.Debug()
                .CreateLogger();
#endif


            config.MapHttpAttributeRoutes();

            config.EnableCors();

            config.Routes.MapHttpRoute(
              name: "RouteWithActions",
              routeTemplate: "api/{controller}/{action}/{id}",
              defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}