﻿using BLL.Services.Interfaces;
using DataUploader;
using DataUploaderApp.Hellper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DataUploaderController : ApiController
    {
        private IDataUploaderService _dataUploaderService;

        public DataUploaderController(IDataUploaderService dataUploaderService)
        {
            _dataUploaderService = dataUploaderService;
        }

        [HttpPost]
        public string RunDataUploaderParserWithSettings([FromBody] DataUploaderSettings settings)
        {
            _dataUploaderService.RunDataUploaderParserAsync(settings);
            return "data uploader parser is started";
        }

        [HttpPost]
        public string RunDataUploaderParserWithAppArgs([FromBody] AppArg appArg)
        {
            _dataUploaderService.RunDataUploaderParserAsync(appArg);
            return "data uploader parser is started";
        }

        [HttpPost]
        public string RunDataUploaderCreatorWithSettings([FromBody] DataUploaderSettings settings)
        {
            _dataUploaderService.RunDataUploaderCreatorAsync(settings);
            return "data uploader creator is started";
        }

        [HttpPost]
        public string RunDataUploaderCreatorWithAppArgs([FromBody] AppArg appArg)
        {
            _dataUploaderService.RunDataUploaderCreatorAsync(appArg);
            return "data uploader creator is started";
        }
    }
}
