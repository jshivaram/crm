﻿using BLL.Services.Concrete;
using BLL.Services.Interfaces;
using DmnEngineApp.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.Models;

namespace WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DmnController : ApiController
    {
        private IDmnService _dmnService;

        public DmnController(IDmnService dmnService)
        {
            _dmnService = dmnService;
        }

        [HttpPost]
        public string RunDmn([FromBody] InputArgsDto dmnInputArgs)
        {            
            _dmnService.RunDmnAsync(dmnInputArgs);
            return "Dmn calculation is started";
        }
    }
}
