﻿using BLL.Services.Interfaces;
using System.Web.Http;
using System.Web.Http.Cors;
using EspCalculation.Options;

namespace WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EspController : ApiController
    {
        private IEspService _espService;

        public EspController(IEspService espService)
        {
            _espService = espService;
        }

        [HttpPost]
        public string CalculateMeasure([FromBody] CalculateOptions options)
        {
            _espService.CalculateMeasureAsync(options);
            return "ESP calculation is started";
        }

        [HttpPost]
        public string GroupCalculateMeasures([FromBody] GroupCalculateOptions groupcalCulateOptions)
        {
            _espService.GroupCalculateMeasuresAsync(groupcalCulateOptions);
            return "ESP Group calculation is started";
        }
    }
}
