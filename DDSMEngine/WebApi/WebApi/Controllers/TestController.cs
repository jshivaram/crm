﻿using BLL.Services.Concrete;
using BLL.Services.Interfaces;
using DmnEngineApp.Dto;
using Serilog;
using SocketClients;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.Exceptions;
using WebApi.Filters;
using WebApi.Models;

namespace WebApi.Controllers
{
    //enable cors need for cross-domain requests using filter(attribute)
    /// <summary>
    /// origins - url from which you can send cross-domain queries, * - for all urls
    /// methods - list of methods (post, put, get, delete), * - for all methods
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TestController : ApiController
    {
        //Service that we can get from ninject
        private ITestService _testService;

        //Ninject inject service dependency to controller
        public TestController(ITestService testService)
        {
            Log.Debug("Created controller");
            _testService = testService;
        }

        //example of using WebSocket Client
        [HttpPost]
        public void SendMessage()
        {
            StaticSocketClient.Send(new Guid(), "qwe");
        }

        [HttpGet]
        [ExceptionWithLoggerFilter] //this filter used for catch all exceeptions
        public int GetValue()
        {
            Log.Debug("Debug");
            Log.Information("Launched Method GetValue");
            return 1;
        }

        [HttpGet]
        [CustomAuthorize] //only authorized users can get value from this method
        public int GetValueWithAuth()
        {
            return 3;
        }

        [HttpPost]
        public async Task OnPressButton()
        {
            await _testService.CreateRecords();
        }

        [HttpPost]
        public async Task TestSocket()
        {
            await _testService.TestSocket();
        }

        [HttpPost]
        //exemple of using data from post request, FromBody attribute is required
        public async Task Test2([FromBody] GenericModel<InputArgsDto> genericModel)
        {
            await _testService.TestSocket();
        }
    }
}