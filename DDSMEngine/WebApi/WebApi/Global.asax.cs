﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace WebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            // maximum number of concurrent connections allowed by a ServicePoint object
            //System.Net.ServicePointManager.DefaultConnectionLimit = Int16.MaxValue;
        }
    }
}
