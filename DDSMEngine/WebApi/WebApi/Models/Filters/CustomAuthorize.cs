﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace WebApi.Filters
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            IEnumerable<string> userNames = new List<string>();
            actionContext.Request.Headers.TryGetValues("username", out userNames);

            string userName = userNames?.FirstOrDefault();

            if(string.IsNullOrEmpty(userName))
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.ExpectationFailed);
                actionContext.Response.ReasonPhrase = "username cannot be null";
                return;
            }

            if(userName != "abc")
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.ExpectationFailed);
                actionContext.Response.ReasonPhrase = "auth error";
                return;
            }

            HttpContext.Current.Response.AddHeader("username", userName);
            HttpContext.Current.Response.AddHeader("AuthenticationStatus", "Authorized");
        }
    }
}