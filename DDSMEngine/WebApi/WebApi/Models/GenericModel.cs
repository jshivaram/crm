﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class GenericModel<T>
    {
        public Guid UserId { get; set; }
        public T Model { get; set; }
    }
}