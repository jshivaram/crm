﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace SocketServer
{
    public class DynamicsCrm : WebSocketBehavior
    {
        protected override void OnMessage(MessageEventArgs e)
        {
            //Console.WriteLine(e.Data);
            Sessions.Broadcast(e.Data);
        }
    }
}
