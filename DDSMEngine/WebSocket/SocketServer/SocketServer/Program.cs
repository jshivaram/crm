﻿using System;
using System.Security.Cryptography.X509Certificates;
using WebSocketSharp.Server;

namespace SocketServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var wssv = new WebSocketServer(85, true);

            wssv.SslConfiguration.ServerCertificate = new X509Certificate2("C:/ssl/ddsm.online.pfx", "ghjuhfvth");

            wssv.AddWebSocketService<DynamicsCrm>("/DynamicsCrm");

            wssv.AddWebSocketService<Dmn>("/Dmn");
            wssv.AddWebSocketService<Esp>("/Esp");

            wssv.Start();

            Console.ReadKey(true);
            wssv.Stop();
        }
    }
}
