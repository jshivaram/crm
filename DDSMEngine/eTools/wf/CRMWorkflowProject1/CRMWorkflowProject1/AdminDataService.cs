﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMWorkflowProject1
{
    static class AdminDataService
    {
        public static string GetAdminDataUrl(IOrganizationService service, string entityName, string fieldName, string validRecordName)
        {
            QueryExpression query = new QueryExpression
            {
                EntityName = entityName,
                ColumnSet = new ColumnSet(fieldName)
            };

            query.Criteria.AddCondition(new ConditionExpression("ddsm_name", ConditionOperator.Equal, validRecordName));

            List<Entity> response = service.RetrieveMultiple(query).Entities.ToList();

            if (response == null && response.Count < 1)
            {
                throw new Exception("Admin data response is empty");
            }

            string url = response.First().GetAttributeValue<string>(fieldName);

            return url;
        }
    }
}
