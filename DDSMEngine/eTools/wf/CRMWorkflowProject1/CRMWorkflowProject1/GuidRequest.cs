﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace CRMWorkflowProject1
{
    static class GuidRequest
    {
        public static async void SendGuid(string url, Guid guid)
        {
            Dictionary<string, string> body = new Dictionary<string, string>
            {
                {"Arg", guid.ToString() }
            };


            var client = new HttpClient();
            var serializedString = JsonConvert.SerializeObject(body, Formatting.Indented);

            var json = new StringContent(serializedString, Encoding.UTF8, "application/json");
            await client.PostAsync(url, json);
        }
    }
}
