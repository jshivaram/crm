﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;

namespace CRMWorkflowProject1
{
    public class MyActivity : CodeActivity
    {
        [Output("Result")]     
        public OutArgument<String> Result { get; set; }
        
        [RequiredArgument]
        [Input("AdminDataFieldName")]
        public InArgument<String> AdminDataFieldName { get; set; }

        [RequiredArgument]
        [Input("AdminDataEntityName")]
        public InArgument<String> AdminDataEntityName { get; set; }

        [RequiredArgument]
        [Input("AdminDataRecordName")]
        public InArgument<String> AdminDataRecordName { get; set; }


        protected override void Execute(CodeActivityContext executionContext)
        {
            ITracingService tracer = executionContext.GetExtension<ITracingService>();
            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            try
            {
                EntityReference entity = (EntityReference)context.InputParameters["Target"];

                string url = AdminDataService.GetAdminDataUrl(service,
                    AdminDataEntityName.Get(executionContext),
                    AdminDataFieldName.Get(executionContext),
                    AdminDataRecordName.Get(executionContext));

                GuidRequest.SendGuid(url, entity.Id);
                Result.Set(executionContext, url);

                //TODO: Do stuff
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException(e.Message);
            }
        }
    }
}
