﻿using System;
using System.Activities;
using System.Collections.Generic;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;

namespace DDSM.AdditionalDataTypes
{
    public abstract class BaseCodeActivity : CodeActivity
    {
        #region "Parameter Definition"

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        [Output("Result")]
        public OutArgument<string> Result { get; set; }

        [Input("UserInput")]
        public InArgument<string> UserInput { get; set; }
        #endregion


        #region props
        internal IOrganizationService Service;
        internal Common ObjCommon;
        internal Helper _helper;
       // internal CrmHelper CrmHelper;
        internal EntityReference DataUploader;
        internal List<string> _processingLog;
        internal abstract TaskQueue.StatusFileDataUploading CurrentOperationStatus { get; set; }

        #endregion

        /// <summary>
        /// Executes the workflow activity.
        /// </summary>
        /// <param name="executionContext">The execution context.</param>
        protected override void Execute(CodeActivityContext executionContext)
        {
            if (executionContext == null)
                throw new ArgumentNullException("Code Activity Context is null");
            _processingLog = new List<string>();
            ObjCommon = new Common(executionContext);
            Service = ObjCommon.GetOrgService();
            _helper = new Helper(Service);
          // CrmHelper = new CrmHelper(Service);
            DataUploader = GetDataUploaderRef();
            UpdateDACurrentOperationStatus();
            ObjCommon.TracingService.Trace($"Entered custom activity, Correlation Id: {ObjCommon.Context.CorrelationId}, Initiating User: {ObjCommon.Context.InitiatingUserId}");
            try
            {
                ObjCommon.TracingService.Trace($"Entering ExecuteActivity {this.GetType().FullName}.");
                this.ExecuteActivity(executionContext);

                Complete.Set(executionContext, true);
            }
            catch (Exception e)
            {
                SetErrorStatusForDA();
                //   ExceptionOccured.Set(executionContext, true);
                //  ExceptionMessage.Set(executionContext, e.Message);
                //  if (FailOnException.Get<bool>(executionContext))
                {
                    throw new InvalidPluginExecutionException(e.Message, e);
                }
            }
            finally
            {
                SaveOparationLog();
            }
        }

        private void SaveOparationLog()
        {
            var target = Extentions.GetTargetData(ObjCommon);
            if (target.LogicalName == "ddsm_taskqueue")

                if( ObjCommon.TracingService  is MoskTraceService && target.LogicalName== "ddsm_taskqueue")
                {
                    var tracingService = (ObjCommon.TracingService as MoskTraceService);

                    if (tracingService.Logs!= null && tracingService.Logs.Count > 0)
                    {
                        ObjCommon.GetOrgService().Update(new Entity(target.LogicalName, target.Id) { Attributes = new AttributeCollection() { new KeyValuePair<string, object>("ddsm_log", string.Join("\n", tracingService.Logs)) } });
                    }
                }
              
        }

        private void UpdateDACurrentOperationStatus()
        {
            ObjCommon.TracingService.Trace("in UpdateDACurrentOperationStatus ");
            //  if (CurrentOperationStatus != null)
            {
                ObjCommon.TracingService.Trace("Current Operation Status: " + Enum.GetName(typeof(TaskQueue.StatusFileDataUploading), CurrentOperationStatus));

                if (DataUploader?.Id != Guid.Empty)
                {
                    ObjCommon.GetOrgService().Update(new Entity(DataUploader.LogicalName, DataUploader.Id)
                    {
                        Attributes = new AttributeCollection() { new KeyValuePair<string, object>("ddsm_statusfiledatauploading", new OptionSetValue((int)CurrentOperationStatus)) }
                    });
                }
            }
        }

        internal void SetErrorStatusForDA()
        {
            ObjCommon.TracingService.Trace("in SetErrorStatusForDA ");
            //  if (CurrentOperationStatus != null)
            {
                ObjCommon.TracingService.Trace("Current Operation Status: " + Enum.GetName(typeof(TaskQueue.StatusFileDataUploading), CurrentOperationStatus));

                if (DataUploader?.Id != Guid.Empty)
                {
                    ObjCommon.GetOrgService().Update(new Entity(DataUploader.LogicalName, DataUploader.Id)
                    {
                        Attributes = new AttributeCollection() { new KeyValuePair<string, object>("ddsm_statusfiledatauploading", new OptionSetValue((int)TaskQueue.StatusFileDataUploading.importFailed)) }
                    });
                }
            }
        }

        private EntityReference GetDataUploaderRef()
        {
            var result = new EntityReference();
            try
            {
                var target = Extentions.GetTargetData(ObjCommon);
                if (target.LogicalName != "ddsm_taskqueue")
                    return new EntityReference();

                var taskQueue = ObjCommon.GetOrgService().Retrieve(target.LogicalName, target.Id, new ColumnSet("ddsm_datauploader"));

                object tmpObj;
                if (taskQueue.Attributes.TryGetValue("ddsm_datauploader", out tmpObj))
                {
                    result = (EntityReference)tmpObj;
                }
            }
            catch (Exception ex)
            {
                ObjCommon.TracingService.Trace("Error on GetDataUploaderRef() " + ex.Message);
                return result;
            }
            return result;
        }

        protected abstract void ExecuteActivity(CodeActivityContext executionContext);
    }
}
