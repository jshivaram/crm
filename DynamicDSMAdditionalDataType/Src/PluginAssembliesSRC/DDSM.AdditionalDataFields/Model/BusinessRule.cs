﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.AdditionalDataType.Model
{
    public class BusinessRule
    {
        public Guid? Id { get; set; }
        public string DisplayName { get; set; }
    }
}
