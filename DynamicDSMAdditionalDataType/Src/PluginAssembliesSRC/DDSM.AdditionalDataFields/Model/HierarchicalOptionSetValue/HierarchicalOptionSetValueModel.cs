﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.AdditionalDataType.Model.HierarchicalOptionSetValue
{
    public class HierarchicalOptionSetValueModel
    {
        public string IndicatorId { get; set; }
        public string IndicatorName { get; set; }

        public string OptionSetValueId { get; set; }
        public string OptionSetValueName { get; set; }
        public int Code { get; set; }

        public string ParentOptionSetValueId { get; set; }
        public string ParentOptionSetValueName { get; set; }

        public int Id { get; set; }
        public int ParentId { get; set; }
    }
}
