﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.AdditionalDataType.Model
{
    public class HistoryStep
    {
        public Guid? Id { get; set; }
        public int? Status { get; set; }
        public IEnumerable<HistoryBusinessRule> BusinessRules { get; set; }
    }
}
