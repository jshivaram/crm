﻿namespace DDSM.SmartMeasureCalculationPlugin.Model
{
    public class MappingItem
    {
        public string FieldType { get; set; }
        public string Entity { get; set; }
        public string AttrName { get; set; }


    }
    public class MappingItem2
    {
        public string FieldType { get; set; }
        public string Entity { get; set; }
        public string AttrLogicalName { get; set; }
        public string FieldDisplayName { get; set; }
    }
}
