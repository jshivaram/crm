﻿using System;

namespace DDSM.AdditionalDataType.Model
{
    public class ProcessFlowParameter
    {
        public Guid ProcessFlowId { get; set; }
        public string EntityId { get; set; }
        public string EntityLogicalName { get; set; }
        public Guid UserId { get; set; }
    }
}
