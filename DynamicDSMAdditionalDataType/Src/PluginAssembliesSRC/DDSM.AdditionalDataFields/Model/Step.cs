﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.AdditionalDataType.Model
{
    public class Step
    {
        public Guid Id { get; set; }
        public string DisplayName { get; set; }
        public IEnumerable<BusinessRule> BusinessRules { get; set; }
    }
}
