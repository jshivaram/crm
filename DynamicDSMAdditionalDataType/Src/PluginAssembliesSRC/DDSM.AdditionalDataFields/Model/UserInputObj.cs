﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DDSM.SmartMeasureCalculationPlugin.Model
{
    public class UserInputObj
    {
        [JsonConverter(typeof(FakeGuidValueProvider))]
        public Guid AccountID { get; set; }

        [JsonConverter(typeof(FakeGuidValueProvider))]
        public Guid ParentsiteID { get; set; }

        public string Tradeally { get; set; }

        [JsonConverter(typeof(FakeGuidValueProvider))]
        public Guid ProjectID { get; set; }


        public string ProjectName { get; set; }
        public string Projectstatus { get; set; }
        public string Phase { get; set; }
        public int? Phasenumber { get; set; }
        public DateTime Initialphasedate { get; set; }

        [JsonConverter(typeof(FakeGuidValueProvider))]
        public Guid MeasureID { get; set; }

        [JsonConverter(typeof(FakeGuidValueProvider))]
        public Guid MeasureTplID { get; set; }

        [JsonConverter(typeof(FakeGuidValueProvider))]
        public Guid CalculationType { get; set; }

       // [JsonConverter(typeof(FakeGuidValueProvider))]
        public string EspSmartMeasureID { get; set; }

        public Dictionary<string, string> DataFields { get; set; }
    }

    /// <summary>
    /// Object for Recalulate SM from View
    /// </summary>
    public class UserInputObj2
    {      
        public List<Guid> SmartMeasures { get; set; }
        public Dictionary<string, string> DataFields { get; set; }
    }



}