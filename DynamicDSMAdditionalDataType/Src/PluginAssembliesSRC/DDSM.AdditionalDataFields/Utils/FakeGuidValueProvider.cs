﻿using System;
using Newtonsoft.Json;

namespace DDSM.SmartMeasureCalculationPlugin.Model
{
    public class FakeGuidValueProvider : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (!string.IsNullOrEmpty((string)reader.Value))
            {
                return Guid.Parse((string)reader.Value);
            }
            else
            { return Guid.Empty; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
         //   base.WriteJson(writer, value, serializer);
        }
    }
}
