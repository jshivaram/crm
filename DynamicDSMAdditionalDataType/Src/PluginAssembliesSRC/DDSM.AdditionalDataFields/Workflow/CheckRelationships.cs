﻿using DDSM.AdditionalDataTypes;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

public class CheckRelationships : CodeActivity
{
    [Input("FromEntityId")]
    public InArgument<string> FromEntityId { get; set; }

    [Input("FromEntityLogicalName")]
    public InArgument<string> FromEntityLogicalName { get; set; }

    [Input("ToEntityId")]
    public InArgument<string> ToEntityId { get; set; }

    [Input("ToEntityLogicalName")]
    public InArgument<string> ToEntityLogicalName { get; set; }

    [Output("CheckedResult")]
    public OutArgument<bool> CheckedResult { get; set; }

    protected override void Execute(CodeActivityContext executionContext)
    {
        var common = new Common(executionContext);
        var orgService = common.GetOrgService();

        try
        {
            var strFromEntityId = FromEntityId.Get<string>(executionContext);
            var fromEntityId = new Guid(strFromEntityId);
            var fromEntityLogicalName = FromEntityLogicalName.Get<string>(executionContext);

            var strToEntityId = ToEntityId.Get<string>(executionContext);
            var toEntityId = new Guid(strToEntityId);
            var toEntityLogicalName = ToEntityLogicalName.Get<string>(executionContext);

            var retrieveBankEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = fromEntityLogicalName
            };

            var retrieveBankEntityResponse =
                (RetrieveEntityResponse)orgService.Execute(retrieveBankEntityRequest);

            OneToManyRelationshipMetadata[] oneToManyRelationships =
                retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships;
            OneToManyRelationshipMetadata[] manyToOneRelationships =
                retrieveBankEntityResponse.EntityMetadata.ManyToOneRelationships;

            bool oneToManyRelationshipsResult = _CheckOneToManyRelationships(fromEntityId, toEntityLogicalName,
                oneToManyRelationships, orgService);
            bool manyToOneRelationshipsResult = _CheckManyToOneRelationships(fromEntityId, toEntityId, toEntityLogicalName,
                manyToOneRelationships, orgService);

            bool result = oneToManyRelationshipsResult || manyToOneRelationshipsResult;
            CheckedResult.Set(executionContext, result);
        }
        catch (Exception e)
        {
            throw new InvalidPluginExecutionException(e.Message);
        }
    }

    private bool _CheckOneToManyRelationships(Guid fromEntityId, string toEntityLogicalName,
        OneToManyRelationshipMetadata[] oneToManyRelationships, IOrganizationService orgService)
    {
        List<OneToManyRelationshipMetadata> relationships =
            oneToManyRelationships.Where(r => r.ReferencingEntity == toEntityLogicalName).ToList();

        OneToManyRelationshipMetadata relation = relationships.FirstOrDefault();

        if (relation == null)
        {
            return false;
        }

        var query = new QueryExpression
        {
            EntityName = relation.ReferencingEntity,
            Criteria = new FilterExpression
            {
                Conditions =
                    {
                        new ConditionExpression(relation.ReferencingAttribute, ConditionOperator.Equal, fromEntityId)
                    }
            }
        };

        Entity entity = orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

        if (entity == null)
        {
            return false;
        }

        return true;
    }

    private bool _CheckManyToOneRelationships(Guid fromEntityId, Guid toEntityId, string toEntityLogicalName,
        OneToManyRelationshipMetadata[] manyToOneRelationships, IOrganizationService orgService)
    {
        List<OneToManyRelationshipMetadata> relationships =
            manyToOneRelationships.Where(r => r.ReferencedEntity == toEntityLogicalName).ToList();

        if (relationships.Count == 0)
        {
            return false;
        }

        string referencingEntityLogicalName = relationships.FirstOrDefault().ReferencingEntity;

        var query = new QueryExpression(referencingEntityLogicalName);
        query.Criteria = new FilterExpression(LogicalOperator.Or);

        foreach (OneToManyRelationshipMetadata relation in relationships)
        {
            query.Criteria.Conditions.Add(new ConditionExpression(relation.ReferencingAttribute, ConditionOperator.Equal, toEntityId));
        }

        DataCollection<Entity> entities = orgService.RetrieveMultiple(query).Entities;
        Entity entity = entities.SingleOrDefault(e => e.Id == fromEntityId);

        if (entity == null)
        {
            return false;
        }

        return true;
    }
}
