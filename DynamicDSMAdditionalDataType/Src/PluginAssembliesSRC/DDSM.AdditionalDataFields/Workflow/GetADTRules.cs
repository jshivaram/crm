﻿using System.Activities;
using System.Collections.Generic;
using DDSM.AdditionalDataTypes;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;

public class GetADTRules : CodeActivity
{
    #region "Parameter Definition"
    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }
    [Output("Result")]
    public OutArgument<string> Result { get; set; }
    [Input("EntityName")]
    public InArgument<string> EntityName { get; set; }
    #endregion

    public class ADTRule
    {
        public ADTRule(Entity source)
        {
            ddsm_datatype = source.GetAttributeValue<OptionSetValue>("ddsm_datatype")?.Value;
            ddsm_definition = source.GetAttributeValue<string>("ddsm_definition");
            ddsm_sourcetype = source.GetAttributeValue<OptionSetValue>("ddsm_sourcetype")?.Value;
        }

        public int? ddsm_datatype { get; set; }
        public string ddsm_definition { get; set; }
        public int? ddsm_sourcetype { get; set; }
    }

    protected override void Execute(CodeActivityContext context)
    {
        var _common = new Common(context);
        var entName = EntityName.Get(context);
        var result = new List<ADTRule>();
        var rules = GetRules(entName, _common);
        foreach (var entRule in rules.Entities)
        {
            result.Add(new ADTRule(entRule));
        }
        var resultJson = JsonConvert.SerializeObject(result);
        Result.Set(context, resultJson);
        Complete.Set(context,true);
    }

    EntityCollection GetRules(string entityName, Common _objCommon)
    {
        EntityCollection result = null;

        var query = new QueryExpression
        {
            EntityName = "ddsm_additionaldatatype",
            ColumnSet = new ColumnSet("ddsm_targetentity", "ddsm_definition", "ddsm_sourcetype", "ddsm_datatype"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions =
                {
                    new ConditionExpression("ddsm_targetentity",ConditionOperator.Equal,entityName)
                }
            }
        };
        query.Orders.Add(new OrderExpression("ddsm_order", OrderType.Ascending));

        result = _objCommon.GetOrgService(systemCall: true).RetrieveMultiple(query);

        return result;
    }
}
