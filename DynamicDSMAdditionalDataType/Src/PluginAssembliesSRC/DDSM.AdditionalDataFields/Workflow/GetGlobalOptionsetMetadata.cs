﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Workflow;
using DDSM.AdditionalDataTypes;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Newtonsoft.Json;


    public class GetGlobalOptionsetMetadata : CodeActivity
    {
        #region "Parameter Definition"
        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }
        [Output("Result")]
        public OutArgument<string> Result { get; set; }
        [Input("EntityName")]
        public InArgument<string> EntityName { get; set; }
        #endregion

        protected override void Execute(CodeActivityContext context)
        {
            var _common = new Common(context);
            var entName = EntityName.Get(context);
            var result = GetData(entName, _common);
            var resultJson = JsonConvert.SerializeObject(result);
            Result.Set(context, resultJson);
            Complete.Set(context, true);
        }

        private OptionMetadataCollection GetData(string logicalName, Common common)
        {
            var retrieveOptionSetRequest = new RetrieveOptionSetRequest { Name = logicalName };
            // Execute the request.
            var retrieveOptionSetResponse = (RetrieveOptionSetResponse)common.GetOrgService(systemCall: true).Execute(retrieveOptionSetRequest);
            var data = (OptionSetMetadata)retrieveOptionSetResponse.OptionSetMetadata;
            var values = data.Options;
            return values;
        }
    }

