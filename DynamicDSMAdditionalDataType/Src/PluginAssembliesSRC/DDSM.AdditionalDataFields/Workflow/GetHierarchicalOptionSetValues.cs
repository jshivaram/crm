﻿using DDSM.AdditionalDataTypes;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Query;
using DDSM.CommonProvider.Model;
using System.Linq;
using Microsoft.Xrm.Sdk.Messages;
using DDSM.AdditionalDataType.Model.HierarchicalOptionSetValue;
using DDSM.CommonProvider.JSON;

public class GetHierarchicalOptionSetValues : DDSM.CommonProvider.src.BaseCodeActivity
{
    [Input("IndicatorId")]
    public InArgument<string> IndicatorId { get; set; }

    public override StatusFileDataUploading CurrentOperationStatus { get; set; }

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        var indicatorId = Guid.Parse(IndicatorId.Get<string>(executionContext));

        IEnumerable<Guid> optionSetValueIds = GetRootOptionSetValueIds(indicatorId);
        IEnumerable<IEnumerable<Entity>> hierarchicalOptionSetValueGroups = GetHierarchicalOptionSetValuesByRoots(optionSetValueIds);
        IEnumerable<Entity> mergedData = MergeResponses(hierarchicalOptionSetValueGroups);
        IEnumerable<HierarchicalOptionSetValueModel> convertedData = ConvertDataToModels(mergedData);

        string json = JsonConvert.SerializeObject(convertedData);
        Result.Set(executionContext, json);
    }

    private IEnumerable<Guid> GetRootOptionSetValueIds(Guid indicatorId)
    {
        var query = new QueryExpression
        {
            EntityName = "ddsm_optionsetvalue",
            Criteria = new FilterExpression
            {
                Conditions =
                {
                    new ConditionExpression("ddsm_indicator", ConditionOperator.Equal, indicatorId)
                }
            }
        };
        var entities = _objCommon.GetOrgService().RetrieveMultiple(query).Entities;
        var optionSetValueIds = entities.Select(e => e.Id).ToList();

        return optionSetValueIds;
    }

    private IEnumerable<IEnumerable<Entity>> GetHierarchicalOptionSetValuesByRoots(IEnumerable<Guid> optionSetValueIds)
    {
        var executeMultipleRequest = new ExecuteMultipleRequest
        {
            Requests = new OrganizationRequestCollection(),
            Settings = new ExecuteMultipleSettings() { ContinueOnError = true, ReturnResponses = true }
        };

        foreach (var optionSetValueId in optionSetValueIds)
        {
            executeMultipleRequest.Requests.Add(new RetrieveMultipleRequest()
            {
                Query = new QueryExpression
                {
                    EntityName = "ddsm_optionsetvalue",
                    ColumnSet = new ColumnSet("ddsm_parentoptionsetvalue", "ddsm_name", "ddsm_indicator", "ddsm_code"),
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression("ddsm_optionsetvalueid", ConditionOperator.UnderOrEqual, optionSetValueId)
                        }
                    }
                }
            });
        }

        var executeMultipleResponse = (ExecuteMultipleResponse)_objCommon.GetOrgService().Execute(executeMultipleRequest);
        var responses = executeMultipleResponse.Responses.Select(r => ((RetrieveMultipleResponse)r.Response).EntityCollection.Entities).ToList();

        return responses;
    }

    private IEnumerable<Entity> MergeResponses(IEnumerable<IEnumerable<Entity>> hierarchicalOptionSetValueGroups)
    {
        var result = new List<Entity>();
        foreach (IEnumerable<Entity> group in hierarchicalOptionSetValueGroups)
        {
            result.AddRange(group);
        }

        return result;
    }

    private IEnumerable<HierarchicalOptionSetValueModel> ConvertDataToModels(IEnumerable<Entity> mergedData)
    {
        var result = mergedData.Select(e =>
        {
            var model = new HierarchicalOptionSetValueModel
            {
                Id = e.Id.GetHashCode(),
                OptionSetValueId = e.Id.ToString(),
                OptionSetValueName = e.GetAttributeValue<string>("ddsm_name"),
                Code = e.GetAttributeValue<int>("ddsm_code")
            };

            var indicator = e.GetAttributeValue<EntityReference>("ddsm_indicator");
            if (indicator != null)
            {
                model.IndicatorId = indicator.Id.ToString();
                model.IndicatorName = indicator.Name;
            }

            var parentOptionSetValue = e.GetAttributeValue<EntityReference>("ddsm_parentoptionsetvalue");
            if (parentOptionSetValue != null)
            {
                model.ParentId = parentOptionSetValue.Id.GetHashCode();
                model.ParentOptionSetValueId = parentOptionSetValue.Id.ToString();
                model.ParentOptionSetValueName = parentOptionSetValue.Name;
            }

            return model;
        }).ToList();

        return result;
    }
}
