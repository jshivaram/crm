﻿using DDSM.AdditionalDataType.Model;
using DDSM.AdditionalDataTypes;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

public class GetInitialMilestoneFlowData : CodeActivity
{
    [Input("ProcessFlowId")]
    public InArgument<string> ProcessFlowId { get; set; }

    [Output("Json")]
    public OutArgument<string> Json { get; set; }

    private const string _businessRuleAlias = "businessrule";

    protected override void Execute(CodeActivityContext executionContext)
    {
        var common = new Common(executionContext);
        var orgService = common.GetOrgService();

        try
        {
            var strProcessFlowId = ProcessFlowId.Get<string>(executionContext);
            var processFlowId = new Guid(strProcessFlowId);
            DataCollection<Entity> entities = _GetMilestones(processFlowId, orgService);

            IEnumerable<Step> mappedMilestones = _GetMappedMilestones(entities);
            string json = JsonConvert.SerializeObject(mappedMilestones);

            Json.Set(executionContext, json);
        }
        catch (Exception e)
        {
            throw new InvalidPluginExecutionException(e.Message);
        }
    }

    private DataCollection<Entity> _GetMilestones(Guid processFlowId, IOrganizationService orgService)
    {
        var query = new QueryExpression
        {
            EntityName = "ddsm_milestone",
            ColumnSet = new ColumnSet("ddsm_name"),
            LinkEntities =
                {
                    new LinkEntity
                    {
                        LinkFromEntityName = "ddsm_milestone",
                        LinkFromAttributeName = "ddsm_milestoneid",
                        LinkToEntityName = "ddsm_ddsm_processflow_ddsm_milestone",
                        LinkToAttributeName = "ddsm_milestoneid",
                        JoinOperator = JoinOperator.Inner,
                        LinkEntities =
                        {
                            new LinkEntity
                            {
                                LinkFromEntityName = "ddsm_ddsm_processflow_ddsm_milestone",
                                LinkFromAttributeName = "ddsm_processflowid",
                                LinkToEntityName = "ddsm_processflow",
                                LinkToAttributeName = "ddsm_processflowid",
                                JoinOperator = JoinOperator.Inner,
                                LinkCriteria = new FilterExpression
                                {
                                    Conditions =
                                    {
                                        new ConditionExpression("ddsm_processflowid", ConditionOperator.Equal, processFlowId)
                                    }
                                }
                            }
                        }
                    },
                    new LinkEntity
                    {
                        LinkFromEntityName = "ddsm_milestone",
                        LinkFromAttributeName = "ddsm_milestoneid",
                        LinkToEntityName = "ddsm_ddsm_milestone_ddsm_businessrule",
                        LinkToAttributeName = "ddsm_milestoneid",
                        JoinOperator = JoinOperator.Inner,
                        LinkEntities =
                        {
                            new LinkEntity
                            {
                                EntityAlias = _businessRuleAlias,
                                LinkFromEntityName = "ddsm_ddsm_milestone_ddsm_businessrule",
                                LinkFromAttributeName = "ddsm_businessruleid",
                                LinkToEntityName = "ddsm_businessrule",
                                LinkToAttributeName = "ddsm_businessruleid",
                                Columns = new ColumnSet("ddsm_name", "ddsm_businessruleid"),
                                JoinOperator = JoinOperator.Inner
                            }
                        }
                    }
                }
        };
        query.Orders.Add(new OrderExpression("ddsm_order", OrderType.Ascending));

        DataCollection<Entity> entities = orgService.RetrieveMultiple(query).Entities;

        return entities;
    }

    private IEnumerable<Step> _GetMappedMilestones(DataCollection<Entity> entities)
    {
        IEnumerable<IGrouping<Guid, Entity>> groupedValues = entities.GroupBy(step => step.Id, step => step);

        IEnumerable<Step> mappedValues = groupedValues.Select(stepGroup =>
        {
            Entity step = stepGroup.FirstOrDefault();
            var stepDisplayName = step?.GetAttributeValue<string>("ddsm_name");

            var stepModel = new Step
            {
                Id = stepGroup.Key,
                DisplayName = stepDisplayName,
                BusinessRules = _GetMappedBusinessRules(stepGroup)
            };

            return stepModel;
        });

        return mappedValues;
    }

    private IEnumerable<BusinessRule> _GetMappedBusinessRules(IGrouping<Guid, Entity> stepGroup)
    {
        IEnumerable<BusinessRule> businessRules = stepGroup.Select(businessRule =>
        {
            var businessRuleAliasId =
                businessRule.GetAttributeValue<AliasedValue>($"{_businessRuleAlias}.ddsm_businessruleid");
            var businessRuleId = businessRuleAliasId?.Value as Guid?;

            var businessRuleAliasDisplayName =
                businessRule.GetAttributeValue<AliasedValue>($"{_businessRuleAlias}.ddsm_name");
            var businessRuleDisplayName = businessRuleAliasDisplayName?.Value as string;

            var businessRuleModel = new BusinessRule
            {
                Id = businessRuleId,
                DisplayName = businessRuleDisplayName
            };

            return businessRuleModel;
        });

        return businessRules;
    }
}
