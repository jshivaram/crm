﻿using DDSM.AdditionalDataType.Model;
using DDSM.AdditionalDataTypes;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

public class GetProcessFlowMileStoneStatuses : CodeActivity
{
    [Input("ProcessFlowId")]
    public InArgument<string> ProcessFlowId { get; set; }

    [Input("EntityId")]
    public InArgument<string> EntityId { get; set; }

    [Input("EntityLogicalName")]
    public InArgument<string> EntityLogicalName { get; set; }

    [Input("UserId")]
    public InArgument<string> UserId { get; set; }

    [Output("Json")]
    public OutArgument<string> Json { get; set; }

    private const string _historyBusinessRuleAlias = "historybusinessrule";

    protected override void Execute(CodeActivityContext executionContext)
    {
        var common = new Common(executionContext);
        var orgService = common.GetOrgService();

        try
        {
            var processFlowParameter = new ProcessFlowParameter
            {
                ProcessFlowId = new Guid(ProcessFlowId.Get<string>(executionContext)),
                EntityId = EntityId.Get<string>(executionContext),
                EntityLogicalName = EntityLogicalName.Get<string>(executionContext),
                UserId = new Guid(UserId.Get<string>(executionContext))
            };

            Guid historyProcessFlowId = _GetHistoryProcessFlow(processFlowParameter, orgService);
            DataCollection<Entity> historySteps = _GetHistorySteps(historyProcessFlowId, orgService);

            IEnumerable<HistoryStep> mappedHistorySteps = _GetMappedHistorySteps(historySteps);
            string json = JsonConvert.SerializeObject(mappedHistorySteps);

            Json.Set(executionContext, json);
        }
        catch (Exception e)
        {
            throw new InvalidPluginExecutionException(e.Message);
        }
    }

    private Guid _GetHistoryProcessFlow(ProcessFlowParameter processFlowParameter, IOrganizationService orgService)
    {
        var query = new QueryExpression
        {
            EntityName = "ddsm_historyms",
            TopCount = 1,
            Criteria = new FilterExpression
            {
                FilterOperator = LogicalOperator.And,
                Conditions =
                    {
                        new ConditionExpression("ddsm_processflow", ConditionOperator.Equal,
                            processFlowParameter.ProcessFlowId),
                        new ConditionExpression("ddsm_recordid", ConditionOperator.Equal,
                            processFlowParameter.EntityId),
                        new ConditionExpression("ddsm_entitylogicalname", ConditionOperator.Equal,
                            processFlowParameter.EntityLogicalName)
                        //new ConditionExpression("ddsm_user", ConditionOperator.Equal, 
                        //    processFlowParameter.UserId)
                    }
            }
        };
        //query.Orders.Add(new OrderExpression("createdon", OrderType.Descending));

        var entity = orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

        if (entity == null)
        {
            throw new Exception("History process flow is not found by this parameters:"
                + "\n processFlowId = " + processFlowParameter.ProcessFlowId
                + "\n entityId = " + processFlowParameter.EntityId
                + "\n entityLogicalName = " + processFlowParameter.EntityLogicalName
                + "\n userId = " + processFlowParameter.UserId
            );
        }

        return entity.Id;
    }

    private DataCollection<Entity> _GetHistorySteps(Guid historyProcessFlowId, IOrganizationService orgService)
    {
        var query = new QueryExpression
        {
            EntityName = "ddsm_historymsstatus",
            ColumnSet = new ColumnSet("ddsm_status", "ddsm_milestone"),
            LinkEntities =
            {
                new LinkEntity
                {
                    LinkFromEntityName = "ddsm_historymsstatus",
                    LinkFromAttributeName = "ddsm_historymsstatusid",
                    LinkToEntityName = "ddsm_ddsm_historyms_ddsm_historymsstatus",
                    LinkToAttributeName = "ddsm_historymsstatusid",
                    JoinOperator = JoinOperator.Inner,
                    LinkEntities =
                    {
                        new LinkEntity
                        {
                            LinkFromEntityName = "ddsm_ddsm_historyms_ddsm_historymsstatus",
                            LinkFromAttributeName = "ddsm_historymsid",
                            LinkToEntityName = "ddsm_historyms",
                            LinkToAttributeName = "ddsm_historymsid",
                            JoinOperator = JoinOperator.Inner,
                            LinkCriteria = new FilterExpression
                            {
                                Conditions =
                                {
                                    new ConditionExpression("ddsm_historymsid", ConditionOperator.Equal, historyProcessFlowId)
                                }
                            },
                        }
                    }
                },
                new LinkEntity
                {
                    LinkFromEntityName = "ddsm_historymsstatus",
                    LinkFromAttributeName = "ddsm_historymsstatusid",
                    LinkToEntityName = "ddsm_ddsm_historymsstatus_ddsm_historymsbrst",
                    LinkToAttributeName = "ddsm_historymsstatusid",
                    JoinOperator = JoinOperator.Inner,
                    LinkEntities =
                    {
                        new LinkEntity
                        {
                            EntityAlias = _historyBusinessRuleAlias,
                            LinkFromEntityName = "ddsm_ddsm_historymsstatus_ddsm_historymsbrst",
                            LinkFromAttributeName = "ddsm_historymsbrstatusid",
                            LinkToEntityName = "ddsm_historymsbrstatus",
                            LinkToAttributeName = "ddsm_historymsbrstatusid",
                            Columns = new ColumnSet("ddsm_status", "ddsm_businessrule"),
                            JoinOperator = JoinOperator.Inner
                        }
                    }
                }
            }
        };

        DataCollection<Entity> entities = orgService.RetrieveMultiple(query).Entities;

        return entities;
    }

    private IEnumerable<HistoryStep> _GetMappedHistorySteps(DataCollection<Entity> historySteps)
    {
        IEnumerable<IGrouping<Guid, Entity>> groupedValues =
            historySteps.GroupBy(historyStep => historyStep.Id, historyStep => historyStep);

        IEnumerable<HistoryStep> mappedValues = groupedValues.Select(historyStepGroup =>
        {
            Entity historyStep = historyStepGroup.FirstOrDefault();
            var milestoneId = historyStep?.GetAttributeValue<EntityReference>("ddsm_milestone")?.Id;
            var stepStatus = historyStep?.GetAttributeValue<OptionSetValue>("ddsm_status")?.Value;

            var step = new HistoryStep
            {
                Id = milestoneId,
                Status = stepStatus,
                BusinessRules = _GetMappedHistoryBusinessRules(historyStepGroup)
            };

            return step;
        });

        return mappedValues;
    }

    private IEnumerable<HistoryBusinessRule> _GetMappedHistoryBusinessRules(IGrouping<Guid, Entity> historyStepGroup)
    {
        IEnumerable<HistoryBusinessRule> businessRules = historyStepGroup.Select(historyBusinessRule =>
        {
            var businessRuleAlias = historyBusinessRule
                .GetAttributeValue<AliasedValue>($"{_historyBusinessRuleAlias}.ddsm_businessrule");
            var businessRuleReference = businessRuleAlias?.Value as EntityReference;
            var businessRuleId = businessRuleReference?.Id;

            var businessRuleAliasStatus = historyBusinessRule
                .GetAttributeValue<AliasedValue>($"{_historyBusinessRuleAlias}.ddsm_status");
            var businessRuleStatusOptionSet = businessRuleAliasStatus?.Value as OptionSetValue;
            var businessRuleStatus = businessRuleStatusOptionSet?.Value;

            var businessRule = new HistoryBusinessRule
            {
                Id = businessRuleId,
                Status = businessRuleStatus
            };

            return businessRule;
        });

        return businessRules;
    }
}
