function OnSourceTypeChange(_context) {
    var iFrame = Xrm.Page.ui.controls.get('WebResource_adatatype').getObject().contentWindow.window;
    iFrame.OnSourceTypeChange();
}

function OnMetadataTypeChange(_context) {
    var iFrame = Xrm.Page.ui.controls.get('WebResource_adatatype').getObject().contentWindow.window;
    iFrame.OnMetadataTypeChange();
}

function OnValueFormatChange(_context) {
    var iFrame = Xrm.Page.ui.controls.get('WebResource_adatatype').getObject().contentWindow.window;
    iFrame.OnValueFormatChange();
}

function OnDataTypeChange() {
    var iFrame = Xrm.Page.ui.controls.get('WebResource_adatatype').getObject().contentWindow.window;
    iFrame.OnDataTypeChange();
}

function OnSave() {
    console.log("--On save");

    var definition = Xrm.Page.getAttribute("ddsm_definition");
    var json = JSON.stringify(window.top.definition);
    definition.setValue(json);
}

function OnLoad() {
    console.log("--On load");

    var type = Xrm.Page.ui.getFormType();
    if (type == 1) {
        window.top.definition = {};
    }
 
    var json = Xrm.Page.getAttribute("ddsm_definition").getValue();
    if (json && json !== "") {
        window.top.definition = JSON.parse(json);
    }
}