
define(function () {

    /** @namespace Namespace for MYAPP classes and functions. */
    var KendoHelper = KendoHelper || {};

    /**
     * A maths utility
     * @namespace KendoHelper.CRM
     * @class KendoHelper
     */

    KendoHelper.CRM = {
        transport: function (url) {
            var result = {};
            result.read = {
                url: encodeURI(url),
                dataType: "json",
                beforeSend: function (req) {
                    req.setRequestHeader('Accept', 'application/json');
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.setRequestHeader('OData-MaxVersion', "4.0");
                    req.setRequestHeader("OData-Version", "4.0");
                }
            }
            return result;
        },
        defEntityMetadataSchema: function () {
            var result = {};
            result.data = function (response) {
                var value = response.value;
                var model = {};
                if (value) {
                    model = value.map(function (el) {
                        var displayName = "";
                        var id = el.MetadataId;
                        var logicalName = el.SchemaName.toLowerCase();
                        if (el.DisplayName.LocalizedLabels[0] && el.DisplayName.LocalizedLabels[0].Label) {
                            displayName = el.DisplayName.LocalizedLabels[0].Label;
                        } else if (el.DisplayName.UserLocalizedLabel && el.DisplayName.UserLocalizedLabel.Label) {
                            displayName = el.DisplayName.UserLocalizedLabel.Label;
                        } else {
                            displayName = logicalName;
                        }
                        return {
                            Label: displayName,
                            LogicalName: logicalName,
                            Id: id,
                            EntitySetName: el.EntitySetName,
                            PrimaryNameAttribute: el.PrimaryNameAttribute,
                            PrimaryIdAttribute: el.PrimaryIdAttribute,
                        };
                    });
                } else {
                    console.log("---defEntityMetadataSchem->Value ==null  ");
                }

                return model;
            }
            return result;
        },
        defOptionsetMetadataSchema: function () {
            var result = {};
            result.data = function (response) {
                var value = response.value;
                var model = value.map(function (el) {
                    var displayName = "";
                    var id = el.MetadataId;
                    var logicalName = el.Name.toLowerCase();
                    return {
                        LogicalName: logicalName,
                        Id: id
                    };
                });
                return model;
            }
            return result;
        },
        optionSetMetadata: function () {
            var result = {};
            result.data = function (response) {
                var value = response.Options;
                var model = value.map(function (el) {
                    var displayName = el.Label.LocalizedLabels[0].Label;
                    var value = el.Value;

                    return {
                        DisplayName: displayName,
                        Value: value
                    };
                });
                return model;
            }
            return result;
            var logicalName = model.TargetFieldLogicalName;
        },

        /*return kendo datasourse
        optionSetMetadata: function() {
            //debugger;
            var result = {};
            result.data = function(response) {
                var value = response.value;
                var model = value.map(function(el) {
     
                });
            return model;
          }
        }
        */
    };
    /*set default headers for API request*/

    return KendoHelper.CRM;

});