const controlType = {
    Multiselect: 962080000,
    Dropdown: 962080001,
    BusinessProcessFlows: 962080002,
    MileStoneProcessFlows:962080004
};

export default controlType;