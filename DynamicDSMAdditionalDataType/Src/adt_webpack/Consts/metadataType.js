const METADATA_TYPE = {
    forms: 962080000,
    fields: 962080001,
    entities: 962080002,
    views: 962080003
}

export default METADATA_TYPE;