const SOURCE_TYPE = {
    entity: 962080000,
    optionSet: 962080001,
    crmMetadata: 962080002
}

export default SOURCE_TYPE;