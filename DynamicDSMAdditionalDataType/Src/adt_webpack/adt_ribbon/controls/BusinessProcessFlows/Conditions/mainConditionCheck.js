function mainConditionCheck(processFlowId, data) {

    processFlowId = processFlowId.toLowerCase();
    const entityId = Xrm.Page.data.entity.getId()
        .replace('{', '').replace('}', '')
        .toLowerCase();
    const entityLogicalName = Xrm.Page.data.entity.getEntityName();

    let iterationResult = null;
    const isContainsEntity = !!data.Entities.find(entity => {

        iterationResult = entity.Id.toLowerCase() === entityId
            && entity.LogicalName === entityLogicalName;
        
        return iterationResult;
    });

    const isContainsCurrentId = !!data.ProcessFlowIdList
        .find(id => id.toLowerCase() === processFlowId);

    const result = isContainsEntity && isContainsCurrentId;
    return result;
}

export default mainConditionCheck;