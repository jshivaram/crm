import cssClassByBusinessRuleStatus from '../bpfConsts/cssClassByBusinessRuleStatus'
import businessRuleStatus from '../bpfConsts/businessRuleStatus';

function convertBusinessRulesToHtml(stepId, businessRules) {

    const $businessRulesContainer = $('<ul/>', {
        class: 'bpf-br-container'
    });

    const $step = $('<span/>', {
        id: stepId
    });

    $businessRulesContainer.append($step);

    let $businessRuleElement = null;
    let cssClass = null;

    businessRules.forEach(businessRule => {

        cssClass = `bpf-br-element ${cssClassByBusinessRuleStatus[businessRuleStatus.None]}`;

        $businessRuleElement = $('<li/>', {
            id: businessRule.Id,
            html: businessRule.DisplayName,
            class: cssClass
        });

        $businessRulesContainer.append($businessRuleElement);
    });

    let result = $businessRulesContainer[0].outerHTML;

    return result;
}

export default convertBusinessRulesToHtml;