import convertBusinessRulesToHtml from './convertBusinessRulesToHtml';

function getConvertedData(bpfData) {

    let mappedStep = null;
    
    let arrayOfSteps = bpfData.map(step => {

        mappedStep = Object.assign({}, step);
        mappedStep.BusinessRules = convertBusinessRulesToHtml(step.Id, step.BusinessRules);

        return mappedStep;
    });

    return arrayOfSteps;
}

export default getConvertedData;