import showLoader from '../Loader/showLoader';
import hideLoader from '../Loader/hideLoader';
import createBpf from '../createBpf';
import statusRequestHandler from './statusRequestHandler';
// import initializeStepsStatuses from '../bpfTools/initializeStepsStatuses';

function initialRequestHandler(definition, initialRequest, statusRequest, builderResults) {

    showLoader(builderResults.$bpfWrapper);
    initialRequest.done(function (initialResponse) {
        const bpfInitialData = JSON.parse(initialResponse.Json);

        const kendoBpfControl = createBpf(builderResults.$bpfControl, bpfInitialData);

        if (builderResults.showButtons) {
            builderResults.showButtons();
        }

        // initializeStepsStatuses(kendoBpfControl, bpfInitialData);

        statusRequestHandler(statusRequest, kendoBpfControl, builderResults.$bpfWrapper,
            definition.processFlowId);

    }).fail(function (err) {
        console.log(err);
    }).always(function () {
        hideLoader(builderResults.$bpfWrapper);
    });
}

export default initialRequestHandler;