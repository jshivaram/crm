import hideLoader from '../Loader/hideLoader';
import showLoader from '../Loader/showLoader';
import updateStepsStatuses from '../bpfTools/updateStepsStatuses';

import webSocketUrlRequestHandler from './webSocketUrlRequestHandler';

function statusRequestHandler(statusRequest, kendoBpfControl, $bpfWrapper, processFlowId) {

    showLoader($bpfWrapper);
    statusRequest.done(function (statusResponse) {

        const bpfStatusData = JSON.parse(statusResponse.Json);
        updateStepsStatuses(kendoBpfControl, bpfStatusData);

    }).fail(function (err) {
        console.log(err);
    }).always(function() {
        hideLoader($bpfWrapper);
        webSocketUrlRequestHandler(kendoBpfControl, processFlowId);
    });
}

export default statusRequestHandler;