import bpfWebSocketListener from '../Request/bpfWebSocketListener';
import settingsRequest from '../Request/configRequest';
import bpfWebSocketUrlRequest from '../Request/bpfWebSocketUrlRequest';
import launchEnum from '$/launchEnum';
import launchConfig from '$/launchConfig';

function webSocketUrlRequestHandler(kendoBpfControl, processFlowId) {

    switch (launchConfig) {
        case launchEnum.DDSM_365:
            settingsRequest(function (response) {
                const adminData = response.WSConfig;
                const webSocketUrl = adminData.additionalDataTypeWebsocketUrl;

                bpfWebSocketListener(webSocketUrl, kendoBpfControl, processFlowId);
            });
            break;
        case launchEnum.DDSM_2016:
            const request = bpfWebSocketUrlRequest();
            request.done(function (response) {
                const adminData = response.value[0];
                const webSocketUrl = adminData.ddsm_processflowwebsocketurl;

                bpfWebSocketListener(webSocketUrl, kendoBpfControl, processFlowId);
            });
            break;
    }
}

export default webSocketUrlRequestHandler;