function hideLoader($container) {
    kendo.ui.progress($container, false);
}

export default hideLoader;