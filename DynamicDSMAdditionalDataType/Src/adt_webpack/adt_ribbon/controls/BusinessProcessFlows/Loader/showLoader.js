function showLoader($container) {
    kendo.ui.progress($container, true);
}

export default showLoader;