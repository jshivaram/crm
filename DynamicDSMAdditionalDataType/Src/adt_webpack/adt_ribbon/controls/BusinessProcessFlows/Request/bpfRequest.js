import getBpfInitialRequest from './getBpfInitialRequest';
import getBpfStatusRequest from './getBpfStatusRequest';
import showLoader from '../Loader/showLoader';

function bpfRequest(definition, builderResults) {

    showLoader(builderResults.$bpfWrapper);

    const initialRequest = getBpfInitialRequest(definition.processFlowId);
    const statusRequest = getBpfStatusRequest(definition.processFlowId);

    return Promise.all([initialRequest, statusRequest]);
}

export default bpfRequest;