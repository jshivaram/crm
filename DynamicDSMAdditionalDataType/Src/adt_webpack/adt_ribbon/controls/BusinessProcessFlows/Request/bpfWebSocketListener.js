import updateStepsStatuses from '../bpfTools/updateStepsStatuses';
import mainConditionCheck from '../Conditions/mainConditionCheck';

function bpfWebSocketListener(requestUrl, kendoBpfControl, processFlowId) {
    let data = null;

    const ws = new WebSocket(requestUrl);
    ws.onmessage = function (response) {

        if (!response && !response.data) {
            return;
        }

        data = JSON.parse(response.data);

        const conditionResult = mainConditionCheck(processFlowId, data);
        if (!conditionResult) {
            return;
        }

        updateStepsStatuses(kendoBpfControl, data.Steps);
    };
}

export default bpfWebSocketListener;