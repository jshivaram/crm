function bpfWebSocketUrlRequest() {

    const url = Xrm.Page.context.getClientUrl() 
        + "/api/data/v8.1/"
        + "ddsm_admindatas?$select=ddsm_processflowwebsocketurl"
        + "&$filter=ddsm_name eq 'Admin Data'";

    const request = $.ajax({
        type: 'GET',
        url: url,
        headers: {
            'Accept': 'application/json',
            "Content-Type": "application/json; charset=utf-8",
            'OData-MaxVersion': "4.0",
            "OData-Version": "4.0"
        }
    });
    
    return request;
}

export default bpfWebSocketUrlRequest;