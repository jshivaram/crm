function getBpfInitialRequest(processFlowId) {

    const clientUrl = Xrm.Page.context.getClientUrl();
    const baseUrl = '/api/data/v8.1/';

    const data = {
        ProcessFlowId: processFlowId,
    };

    const actionName = 'ddsm_DDSMGetInitialProcessFlowData';

    const request = $.ajax({
        url: clientUrl + baseUrl + actionName,
        type: 'POST',
        data: JSON.stringify(data),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
            'OData-MaxVersion': '4.0',
            'OData-Version': '4.0'
        }
    });

    return request;
}

export default getBpfInitialRequest;