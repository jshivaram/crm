function getBpfRelationshipsRequest(fromEntityId, fromEntityLogicalName, toEntityId, toEntityLogicalName) {

    const clientUrl = Xrm.Page.context.getClientUrl();
    const baseUrl = '/api/data/v8.1/';

    const data = {
        FromEntityId: fromEntityId,
        FromEntityLogicalName: fromEntityLogicalName,
        ToEntityId: toEntityId,
        ToEntityLogicalName: toEntityLogicalName
    };

    const actionName = 'ddsm_DDSMCheckProcessFlowRelationships';

    const request = $.ajax({
        url: clientUrl + baseUrl + actionName,
        type: 'POST',
        data: JSON.stringify(data),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
            'OData-MaxVersion': '4.0',
            'OData-Version': '4.0'
        }
    });

    return request;
}

export default getBpfRelationshipsRequest;