function getBpfStatusRequest(processFlowId) {

    var clientUrl = Xrm.Page.context.getClientUrl();
    var baseUrl = '/api/data/v8.1/';

    var entityId = Xrm.Page.data.entity.getId().replace('{', '').replace('}', '');
    var entityLogicalName = Xrm.Page.data.entity.getEntityName();
    var userId = Xrm.Page.context.getUserId().replace('{', '').replace('}', '');

    var data = {
        ProcessFlowId: processFlowId,
        EntityId: entityId,
        EntityLogicalName: entityLogicalName,
        UserId: userId
    };

    var actionName = 'ddsm_DDSMGetProcessFlowStatuses';

    const request = $.ajax({
        url: clientUrl + baseUrl + actionName,
        type: 'POST',
        data: JSON.stringify(data),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
            'OData-MaxVersion': '4.0',
            'OData-Version': '4.0'
        }
    });

    return request;
}

export default getBpfStatusRequest;