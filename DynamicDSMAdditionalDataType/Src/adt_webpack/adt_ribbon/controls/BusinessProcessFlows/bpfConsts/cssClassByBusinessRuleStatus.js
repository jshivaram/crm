import businessRuleStatus from './businessRuleStatus';

const cssClassByBusinessRuleStatus = {
    [businessRuleStatus.Done]: 'bpf-br-status-done',
    [businessRuleStatus.Error]: 'bpf-br-status-error',
    [businessRuleStatus.InProgress]: 'bpf-br-status-inprogress',
    [businessRuleStatus.None]: 'bpf-br-status-none'
};

export default cssClassByBusinessRuleStatus;