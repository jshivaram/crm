import stepStatus from './stepStatus';

const cssClassByStepStatus = {
    [stepStatus.Done]: 'bpf-step-status-done',
    [stepStatus.Error]: 'bpf-step-status-error',
    [stepStatus.InProgress]: 'bpf-step-status-inprogress',
    [stepStatus.ReadyToCalculate]: '',
    [stepStatus.NotStarted]: ''
};

export default cssClassByStepStatus;