const stepStatus = {
    ReadyToCalculate: 0,
    NotStarted:962080000,
    InProgress: 962080001,
    Done: 962080002,
    Error: 962080003
};

export default stepStatus;