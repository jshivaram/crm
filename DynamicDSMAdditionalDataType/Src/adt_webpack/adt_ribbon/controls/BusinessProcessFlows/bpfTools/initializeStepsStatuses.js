import getHtmlStep from '../htmlHelper/getHtmlStep';
import cssClassByStepStatus from '../bpfConsts/cssClassByStepStatus';
import stepStatus from '../bpfConsts/stepStatus';

function initializeStepsStatuses(kendoBpfControl, stepsData) {

    stepsData.forEach(function (stepData) {

        const $htmlStep = getHtmlStep(stepData.Id, kendoBpfControl);

        if (!$htmlStep.length) {
            console.log('***** ERROR ***** Step with id - '
                + stepData.Id + ' is not found in HTML');
            return;
        }

        const cssClass = cssClassByStepStatus[stepStatus.ReadyToCalculate];
        $htmlStep.addClass(cssClass);
    });
}

export default initializeStepsStatuses;