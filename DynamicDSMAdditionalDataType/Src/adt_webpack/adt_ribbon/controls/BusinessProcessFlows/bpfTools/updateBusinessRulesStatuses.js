import cssClassByBusinessRuleStatus from '../bpfConsts/cssClassByBusinessRuleStatus'
import businessRuleStatus from '../bpfConsts/businessRuleStatus';

const stringOfCssClasses = Object.keys(cssClassByBusinessRuleStatus).map(key => {
    return cssClassByBusinessRuleStatus[key];
}).join(' ');

function updateBusinessRulesStatuses(kendoBpfControl, $htmlStep, businessRulesData) {

    const $bpfControl = kendoBpfControl.wrapper;

    const stepContainerId = $htmlStep.attr('aria-controls');
    const $stepContainer = $bpfControl.find('#' + stepContainerId);
    
    businessRulesData.forEach(function (businessRule) {

        const $businessRule = $stepContainer.find('#' + businessRule.Id);

        if (!$businessRule.length) {
            console.log('***** ERROR ***** Business rule with id - '
                + businessRule.Id + ' is not found in HTML'
                + ' (step - ' + $htmlStep.text() + ')');
            return;
        }

        let cssClass = cssClassByBusinessRuleStatus[businessRule.Status];
        if (!cssClass) {
            cssClass = cssClassByBusinessRuleStatus[businessRuleStatus.None];
        }

        $businessRule.removeClass(stringOfCssClasses);
        $businessRule.addClass(cssClass);
    });
}

export default updateBusinessRulesStatuses;