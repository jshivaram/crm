import getHtmlStep from '../htmlHelper/getHtmlStep';
import updateBusinessRulesStatuses from './updateBusinessRulesStatuses';
import cssClassByStepStatus from '../bpfConsts/cssClassByStepStatus';
import stepStatus from '../bpfConsts/stepStatus';

const stringOfCssClasses = Object.keys(cssClassByStepStatus).map(key => {
    return cssClassByStepStatus[key];
}).join(' ');

function updateStepsStatuses(kendoBpfControl, stepsData) {

    stepsData.forEach(function (stepData) {

        const $htmlStep = getHtmlStep(stepData.Id, kendoBpfControl);

        if (!$htmlStep.length) {
            console.log('***** ERROR ***** Step with id - '
                + stepData.Id + ' is not found in HTML');
            return;
        }

        let cssClass = cssClassByStepStatus[stepData.Status];
        if (!cssClass) {
            cssClass = cssClassByStepStatus[stepStatus.ReadyToCalculate];
        }

        if (stepData.Status === stepStatus.InProgress) {
            kendoBpfControl.activateTab($htmlStep);
        }

        $htmlStep.removeClass(stringOfCssClasses);
        $htmlStep.addClass(cssClass);

        updateBusinessRulesStatuses(kendoBpfControl, $htmlStep, stepData.BusinessRules);
    });
}

export default updateStepsStatuses;