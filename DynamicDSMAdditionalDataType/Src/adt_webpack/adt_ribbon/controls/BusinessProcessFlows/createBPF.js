import getConvertedData from './Convert/getConvertedData';
import updateStepsStatuses from './bpfTools/updateStepsStatuses';

function createBpf($bpfControl, bpfInitialData) {

    let convertedData = getConvertedData(bpfInitialData);

    const kendoBpfControl = $bpfControl.kendoTabStrip({
        animation: false,
        dataTextField: 'DisplayName',
        dataContentField: 'BusinessRules',
        dataSource: convertedData
    }).data('kendoTabStrip');

    return kendoBpfControl;
}

export default createBpf;