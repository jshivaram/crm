function bpfHtmlBuilder(definition) {

    const $bpfContainer = $(definition.selector);
    if (!$bpfContainer.length) {
        throw '********* ERROR ********* BPF container is not found';
    }

    const $bpfWrapper = $('<div/>', {
        class: 'bpf-wrapper'
    });

    const $bpfControl = $('<div/>', {
        class: 'bpf-control'
    });
    $bpfWrapper.append($bpfControl);

    let showButtons = null;

    if (definition.buttonsVisibility) {
        const $bpfBtnsWrapper = $('<div/>', {
            class: 'bpf-btns-wrapper',
            style: 'display: none'
        });

        showButtons = function () {
            $bpfBtnsWrapper[0].style = "";
        };

        const $bpfBtnNext = $('<button/>', {
            class: 'bpf-btn-next k-button',
            html: 'Next'
        });

        const $bpfBtnPrev = $('<button/>', {
            class: 'bpf-btn-prev k-button',
            html: 'Prev'
        });

        $bpfBtnsWrapper.append($bpfBtnNext, $bpfBtnPrev);
        $bpfWrapper.append($bpfBtnsWrapper);
    }

    $bpfContainer.append($bpfWrapper);

    return {
        showButtons: showButtons,
        $bpfWrapper: $bpfWrapper,
        $bpfControl: $bpfControl
    };
}

export default bpfHtmlBuilder;