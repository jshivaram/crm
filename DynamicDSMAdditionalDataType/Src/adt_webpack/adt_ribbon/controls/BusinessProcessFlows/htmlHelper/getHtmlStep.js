function getHtmlStep(stepId, kendoBpfControl) {
    const $bpfControl = kendoBpfControl.wrapper;
    const $content = $bpfControl.find(`#${stepId}`).closest('div.k-content');
    const contentId = $content.attr('id');
    const $title = $(`li[aria-controls="${contentId}"]`);

    return $title;
}

export default getHtmlStep;