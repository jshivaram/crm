function buildHtmlComponents($container) {
    let $dialog = $('<div/>');
    $dialog.appendTo($container);

    let $controlWrapper = $('<div/>', {
        class: 'hos-wrapper'
    });
    $controlWrapper.appendTo($container);

    let $result = $('<div/>', {
        class: 'hos-result'
    });
    $result.appendTo($controlWrapper);

    let $btnPick = $('<button/>', {
        class: 'hos-btn-pick',
        html: 'Pick Value'
    });
    $btnPick.appendTo($controlWrapper);

    let $control = $('<div/>', {
        class: 'hos-control'
    });

    let result = {
        $dialog: $dialog,
        $result: $result,
        $btnPick: $btnPick,
        $control: $control
    };

    return result;
}

export default buildHtmlComponents;