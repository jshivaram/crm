function createDialog(builderResults, config) {
    let crmControl = Xrm.Page.getAttribute(config.definition.selectedField);
    let crmControlCode = Xrm.Page.getAttribute(config.definition.codeField);

    let kendoDialog = builderResults.$dialog.kendoDialog({
        width: '800px',
        height: '490px',
        visible: false,
        closable: true,
        modal: false,
        content: builderResults.$control,
        actions: [
            { text: 'Cancel' },
            { text: 'OK', primary: true, action: actionOK }
        ],
        open: openDialogHandler
    }).data('kendoDialog');

    let $wrapper = kendoDialog.wrapper;
    $wrapper.addClass('hos-dialog');

    function actionOK(e) {
        let treelist = builderResults.$control.data("kendoTreeList");
        let items = treelist.element.find(".k-state-selected");
        updateResult(items, treelist);
    }

    function updateResult(items, treelist) {
        let result = "";
        let selectedCodes = [];
        let joinedHierarchyNames = [];

        if (items.length > 0) {            
            let dataItems = treelist.dataItems();

            for (let i = 0; i < items.length; i++) {
                let dataItem = treelist.dataItem(items[i]);
                selectedCodes.push(dataItem.Code);

                let joinedHierarchyName = getJoinedHierarchyName(dataItems, dataItem);
                joinedHierarchyNames.push(joinedHierarchyName);

                result += "<span class='hos-selected-value'>" + joinedHierarchyName + "</span>";
            }
        } else {
            result = "No items selected";
        }

        let selectedCodesResult = selectedCodes.join(";");
        let joinedHierarchyNamesResult = joinedHierarchyNames.join(";");

        crmControl.setValue(joinedHierarchyNamesResult);
        crmControlCode.setValue(selectedCodesResult);

        builderResults.$result.html(result);
    }

    function getJoinedHierarchyName(items, selectedItem) {
        let currentItem = selectedItem;
        let result = currentItem.OptionSetValueName;

        while (currentItem.ParentOptionSetValueId) {
            currentItem = getItemById(items, currentItem.ParentId);
            result = currentItem.OptionSetValueName + ", " + result;
        }

        return result;
    }

    function getItemById(items, id) {
        let foundItem = items.find(function (item) {
            return item.Id === id;
        });

        return foundItem;
    }

    function openDialogHandler() {
        let kendoTreeList = builderResults.$control.data("kendoTreeList");
        kendoTreeList.dataSource.read();
        kendoTreeList.refresh();
    }
}

export default createDialog;