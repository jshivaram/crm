import getTreeListDataSource from './getTreeListDataSource';

function createTreeList(builderResults, config, indicatorId) {
    let crmControl = Xrm.Page.getAttribute(config.definition.selectedField);
    let crmControlCode = Xrm.Page.getAttribute(config.definition.codeField);

    let savedData = getSavedData(crmControl, crmControlCode);
    if (savedData) {
        let htmlResult = getHtmlResult(savedData.result);
        builderResults.$result.html(htmlResult);
    } else {
        builderResults.$result.html("No items selected");
    }

    function getSavedData(crmControl, crmControlCode) {
        let crmControlValue = crmControl.getValue();
        let crmControlCodeValue = crmControlCode.getValue();

        if (!crmControlValue || !crmControlCodeValue) {
            return null;
        }

        let savedData = {
            result: crmControlValue.split(";"),
            selectedCodes: crmControlCodeValue.split(";")
        };

        return savedData;
    }

    let treeListDataSource = getTreeListDataSource(indicatorId);

    builderResults.$control.kendoTreeList({
        dataSource: treeListDataSource,
        height: 380,
        filterable: true,
        sortable: true,
        columns: [
            {
                // headerTemplate: "<input type='checkbox' class='hos-toggle-all' />",
                template: "<input type='checkbox' class='checkbox' data-bind='checked: checked' />",
                width: 40,
                filterable: false
            },
            {
                field: "OptionSetValueName",
                title: "OptionSet Value",
                expandable: true
            },
            {
                field: "IndicatorName",
                title: "Indicator"
            }
        ],
        dataBound: treeListDataBound,
        autoBind: false
    });

    function treeListDataBound(e) {

        let $rows = builderResults.$control.find(".k-grid-content tr");
        if (!$rows.length) {
            return;
        }

        $rows.each(function (i, row) {
            let $row = $(row);
            let attrValue = $row.attr("aria-expanded");
            if (!attrValue) {
                return;
            }
            $row.find("input[type='checkbox']").attr('disabled', 'disabled');
        });

        let $checkbox = builderResults.$control.find(".checkbox");
        $checkbox.bind("change", function (e) {
            if (!config.definition.isMultipleOptionSet) {
                $rows.removeClass("k-state-selected");
                $checkbox.removeAttr("checked");
                $(e.target).attr("checked", "checked");
            }

            var row = $(e.target).closest("tr");
            this.checked ? row.addClass("k-state-selected") : row.removeClass("k-state-selected");
        });

        let savedData = getSavedData(crmControl, crmControlCode);
        if (!savedData) {
            return;
        }

        builderResults.$control.find(".checkbox").removeAttr("checked");
        builderResults.$control.find(".k-grid-content tr").removeClass("k-state-selected");

        let self = this;
        $rows.each(function (i, htmlRow) {
            let $htmlRow = $(htmlRow);
            let dataItem = self.dataItem($htmlRow);

            let selectedDataItem = savedData.selectedCodes.find(function (selectedCode) {
                return dataItem.Code === +selectedCode;
            });

            if (!selectedDataItem) {
                return;
            }

            $htmlRow.addClass("k-state-selected");
            $htmlRow.find(".checkbox").attr("checked", "checked");
        });
    }

    // let $toggleAll = builderResults.$control.find(".hos-toggle-all");
    // $toggleAll.click(function (e) {
    //     if (e.target.checked) {
    //         builderResults.$control.find("[role='row'] .checkbox").each(function () {
    //             this.checked = "checked";
    //         });
    //     }
    //     else {
    //         builderResults.$control.find("[role='row'] .checkbox").removeAttr("checked");
    //     }

    //     builderResults.$control.find("[role='row'] .checkbox").trigger("change");
    // });

    function getHtmlResult(items) {
        let result = "";
        items.forEach(function (item) {
            result += "<span class='hos-selected-value'>" + item + "</span>";
        });

        return result;
    }
}

export default createTreeList;