function getTreeListDataSource(indicatorId) {

    var dataSource = new kendo.data.TreeListDataSource({
        transport: {
            read: function (options) {
                var data = {
                    IndicatorId: indicatorId
                };
                GlobalJs.WebAPI.ExecuteAction('ddsm_DDSMGetHierarchicalOptionSetValues', data).then(
                    function (response) {
                        options.success(response);
                    },
                    function (error) {
                        options.error(error);
                    }
                );
            }
        },
        batch: true,
        schema: {
            data: function (response) {
                var responsedData = JSON.parse(response.Result);

                return responsedData;
            },
            model: {
                id: "Id",
                parentId: "ParentId",
                fields: {
                    Id: { type: "number" },
                    ParentId: { type: "number" },
                    IndicatorId: { type: "string" },
                    IndicatorName: { type: "string" },
                    OptionSetValueId: { type: "string" },
                    OptionSetValueName: { type: "string" },
                    ParentOptionSetValueId: { type: "string" },
                    ParentOptionSetValueName: { type: "string" },
                    Code: { type: "number" }
                },
                expanded: true
            }
        }
    });

    return dataSource;
}

export default getTreeListDataSource;