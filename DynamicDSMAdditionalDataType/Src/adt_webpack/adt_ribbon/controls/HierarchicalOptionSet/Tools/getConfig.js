function getConfig(definition, adtContainer) {

    var field_name = definition.selectedField;
    if (!field_name) {
        return;
    }

    var nativeControlLabelSelector = "#" + field_name + "_cl_span";
    var nativeControlSelector = "#" + field_name + " .ms-crm-Inline-Value";
    var nativeControlEditSelector = "#" + field_name + " .ms-crm-Inline-Edit";

    var $container = $(adtContainer);
    var $nativeControl = $container.find(nativeControlSelector);

    if (!$nativeControl.length) {
        return;
    }

    var controlInlineStyles = "width: 94%; margin-left: 5%;";
    var $controlContainer = $('<div/>', {
        class: "hos-container",
        style: controlInlineStyles
    });

    $container.find("#" + field_name).append($controlContainer);

    var config = {
        definition: definition,
        nativeControlLabelSelector: nativeControlLabelSelector,
        nativeControlSelector: nativeControlSelector,
        nativeControlEditSelector: nativeControlEditSelector,
        $nativeControl: $nativeControl,
        $controlContainer: $controlContainer
    }

    return config;
}

export default getConfig;