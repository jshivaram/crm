import createDialog from './KendoHelper/createDialog';
import createTreeList from './KendoHelper/createTreeList';

function createHierarchicalOptionSet(config, builderResults, Xrm) {
    let crmIndicatorAttribute = Xrm.Page.getAttribute(config.definition.indicatorLookup);
    let crmControl = Xrm.Page.getControl(config.definition.selectedField);
    let crmAttributeCode = Xrm.Page.getAttribute(config.definition.codeField);
    crmControl.setVisible(false);

    crmIndicatorAttribute.addOnChange(function () {
        clean();
        let selectedIookupValue = crmIndicatorAttribute.getValue();
        if (!selectedIookupValue || !selectedIookupValue.length) {
            return;
        }
        let selectedIndicatorId = selectedIookupValue[0].id.replace("{", "").replace("}", "");
        create(selectedIndicatorId);
    })

    let lookupValue = crmIndicatorAttribute.getValue();
    if (!lookupValue) {
        return;
    }

    let indicatorId = lookupValue[0].id.replace("{", "").replace("}", "");
    create(indicatorId);

    function create(indicatorId) {
        createTreeList(builderResults, config, indicatorId);
        createDialog(builderResults, config);

        builderResults.$btnPick.kendoButton({
            click: function (e) {
                builderResults.$dialog.data("kendoDialog").open();
            }
        });
        crmControl.setVisible(true);
    }

    function clean() {
        let kendoDialog = builderResults.$dialog.data("kendoDialog");
        let kendoTreeList = builderResults.$control.data("kendoTreeList");
        let kendoButton = builderResults.$btnPick.data("kendoButton");

        if (!kendoDialog && !kendoTreeList && !kendoButton) {
            return;
        }

        kendoDialog.destroy();
        builderResults.$dialog.empty();
        kendoTreeList.destroy();
        builderResults.$control.empty();
        kendoButton.destroy();
        // builderResults.$btnPick.empty();
        builderResults.$result.html("No items selected");
        crmControl.setVisible(false);
        crmControl.getAttribute().setValue("");
        crmAttributeCode.setValue("");
    }
}

export default createHierarchicalOptionSet;