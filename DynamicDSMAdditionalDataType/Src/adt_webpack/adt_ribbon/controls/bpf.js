import getBpfInitialRequest from './BusinessProcessFlows/Request/getBpfInitialRequest';
import getBpfStatusRequest from './BusinessProcessFlows/Request/getBpfStatusRequest';
import initialRequestHandler from './BusinessProcessFlows/Handler/initialRequestHandler';
import bpfHtmlBuilder from './BusinessProcessFlows/htmlHelper/bpfHtmlBuilder';

// Business Process Flow

function bpf(element) {
    try {
        const definition = JSON.parse(element.ddsm_definition);

        const builderResults = bpfHtmlBuilder(definition);

        const initialRequest = getBpfInitialRequest(definition.processFlowId);
        const statusRequest = getBpfStatusRequest(definition.processFlowId);

        initialRequestHandler(definition, initialRequest, statusRequest, builderResults);
    }
    catch (err) {
        console.log(err);
    }
}

export default bpf;