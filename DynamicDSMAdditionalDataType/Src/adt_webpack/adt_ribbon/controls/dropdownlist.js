import getControlConfig from './tools/controlConfig';
import commonDataBound from './tools/dataBound/commonDataBound';
import consts from './tools/controlConsts';
import changeEventDictionary from './tools/onChangeEventSettings/changeEventDictionary';

function initDropdown(element) {

    var conf = getControlConfig(consts.DROPDOWNLIST_CONTROL, element);
    if (!conf) {
        return;
    }

    var jsonDataIsRequried = !conf.definition.useSimpleValueFormat;

    var controlConfig = {
        filter: "startswith",
        dataTextField: "Label",
        dataValueField: "LogicalName",
        optionLabel: '--',
        dataSource: conf.dataSource,
        dataBound: commonDataBound(consts.DROPDOWNLIST_CONTROL, conf, jsonDataIsRequried),
        change: changeEventDictionary[consts.DROPDOWNLIST_CONTROL](conf, jsonDataIsRequried)
    };

    if (window.top.ADT.container) {
        var $container = $(window.top.ADT.container);

        controlConfig.popup = {
            appendTo: $container
        };
    }

    conf.$kendoControl.kendoDropDownList(controlConfig);

    window.AlreadyInitCustomControl = false;
}

export default initDropdown;