import getConfig from './HierarchicalOptionSet/Tools/getConfig';
import hideNativeControl from './tools/dataBound/hideNativeControl';
import buildHtmlComponents from './HierarchicalOptionSet/HtmlHelper/buildHtmlComponents';
import createHierarchicalOptionSet from './HierarchicalOptionSet/createHierarchicalOptionSet';

function initHierarchicalOptionSet(element) {

    const definition = JSON.parse(element.ddsm_definition);
    const Xrm = window.top.ADT.Xrm;
    let adtContainer = window.document.body;
    let adtDocument = document;
    if (window.top.ADT.container) {
        adtContainer = window.top.ADT.container;
        adtDocument = window.top.ADT.container.ownerDocument;
    }

    const config = getConfig(definition, adtContainer);
    hideNativeControl(config, Xrm, adtDocument);

    const builderResults = buildHtmlComponents(config.$controlContainer);
    createHierarchicalOptionSet(config, builderResults, Xrm);
}

export default initHierarchicalOptionSet;