import getControlConfig from './tools/controlConfig';
import commonDataBound from './tools/dataBound/commonDataBound';
import consts from './tools/controlConsts';
import changeEventDictionary from './tools/onChangeEventSettings/changeEventDictionary';

function initMultiselect(element) {

    var conf = getControlConfig(consts.MULTISELECT_CONTROL, element);
    if (!conf) {
        return;
    }

    var jsonDataIsRequried = !conf.definition.useSimpleValueFormat;

    var controlConfig = {
        filter: "startswith",
        dataTextField: "Label",
        dataValueField: "Label",
        dataSource: conf.dataSource,
        dataBound: commonDataBound(consts.MULTISELECT_CONTROL, conf, jsonDataIsRequried),
        change: changeEventDictionary[consts.MULTISELECT_CONTROL](conf, jsonDataIsRequried)
    };

    if (window.top.ADT.container) {
        var $container = $(window.top.ADT.container);

        controlConfig.popup = {
            appendTo: $container
        };
    }

    conf.$kendoControl.kendoMultiSelect(controlConfig);

    window.AlreadyInitCustomControl = false;
}

export default initMultiselect;