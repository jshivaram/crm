import getEntityRecordsDatasource from './dataSource/entityRecordsDatasource';
import getOptionSetDatasource from './dataSource/optionSetDatasource';
import getMetadataByType from './metadataByType';

function getControlDatasource(element) {
    var def = JSON.parse(element.ddsm_definition);

    switch (element.ddsm_sourcetype) {
        case 962080000: //entity
            return getEntityRecordsDatasource(def);
        case 962080001: //optionset
            return getOptionSetDatasource(def.selectedFieldValue);
        case 962080002: //crm metadata
            return getMetadataByType(def);
    }
    throw 'Not valid index';
}

export default getControlDatasource;