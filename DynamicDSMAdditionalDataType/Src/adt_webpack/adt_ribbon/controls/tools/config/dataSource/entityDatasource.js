import KendoHelper from '~/kendo_helper/helper';

function getEntityDatasource(def) {
    return new kendo.data.DataSource({
        schema: KendoHelper.defEntityMetadataSchema(),
        sort: {
            field: "Label",
            dir: "asc"
        },
        transport: KendoHelper.transport(encodeURI(clientUrl + baseUrl + 'EntityDefinitions?$select=DisplayName,SchemaName,MetadataId,EntitySetName,PrimaryNameAttribute,PrimaryIdAttribute')),
    });
}

export default getEntityDatasource;