import KendoHelper from '~/kendo_helper/helper';

function getEntityRecordsDatasource(def) {
    var url = clientUrl + baseUrl + def.selectedFieldValueLogicalCollectionName + "?$select=" + def.selectedFieldValuePrimaryNameAttribute;
    return new kendo.data.DataSource({
        schema: {
            data: function (response) {
                var value = response.value;
                var model = {};
                var model = value.map(function (el) {
                    var displayName = el[def.selectedFieldValuePrimaryNameAttribute];
                    var logicalName = el[def.selectedFieldValuePrimaryIdAttribute];
                    return {
                        Label: displayName,
                        LogicalName: logicalName
                    };
                });
                return model;
            }
        },
        sort: {
            field: "Label",
            dir: "asc"
        },
        transport: KendoHelper.transport(url),
    });
}

export default getEntityRecordsDatasource;