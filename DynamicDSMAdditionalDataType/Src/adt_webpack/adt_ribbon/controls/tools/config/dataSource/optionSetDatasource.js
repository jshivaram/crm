function getOptionSetDatasource(logicalName) {
    var query = clientUrl + baseUrl + "ddsm_DDSMGetGlobalOptionSetMetadata"; // Вызов Action new_TestAction и ередача GUID’а записи
    var data = { "LogicalName": logicalName };

    return new kendo.data.DataSource({
        schema: {
            data: function (options) {
                var value = window.parent.JSON.parse(options.Result);
                var model = value.map(function (el) {
                    var displayName = "";
                    if (el.Label.LocalizedLabels[0] && el.Label.LocalizedLabels[0].Label) {
                        displayName = el.Label.LocalizedLabels[0].Label;
                    } else if (el.Label.UserLocalizedLabel && el.Label.UserLocalizedLabel.Label) {
                        displayName = el.Label.UserLocalizedLabel.Label;
                    }
                    return {
                        Label: displayName,
                        LogicalName: el.Value
                    };
                });
                return model;
            }
        },
        sort: {
            field: "Label",
            dir: "asc"
        },
        transport: {
            read: function (options) {
                jQuery.ajax({
                    url: clientUrl + baseUrl + "ddsm_DDSMGetGlobalOptionSetMetadata",
                    type: "POST",
                    data: window.parent.JSON.stringify({ LogicalName: logicalName }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        options.success(data);
                    }
                });
            }
        }
    });
}

export default getOptionSetDatasource;