import KendoHelper from '~/kendo_helper/helper';

function getFormDatasource(def) {
    var url = clientUrl + baseUrl + 'savedqueries' + '?$select=name&$filter=returnedtypecode eq ' + "'" + def.selectedFieldValue + "'";

    return new kendo.data.DataSource({
        schema: {
            data: function (response) {
                var value = response.value;
                var model = {};
                var model = value.map(function (el) {
                    var displayName = el.name;
                    var logicalName = el.formid;
                    return {
                        Label: displayName,
                        LogicalName: logicalName
                    };
                });
                return model;
            }
        },
        sort: {
            field: "Label",
            dir: "asc"
        },
        transport: KendoHelper.transport(url),
    });
}

export default getFormDatasource;