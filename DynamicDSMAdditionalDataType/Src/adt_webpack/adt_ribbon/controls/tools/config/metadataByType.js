import getFormDatasource from './dataSource/formDatasource';
import getEntityFieldsDatasource from './dataSource/entityFieldsDatasource';
import getEntityDatasource from './dataSource/entityDatasource';

function getMetadataByType(def) {
    switch (def.crmMetadataType) {

        case 962080000: //forms
            return getFormDatasource(def);
        case 962080001: //fields
            return getEntityFieldsDatasource(def);
            break;
        case 962080002: //entities
            return getEntityDatasource(def);

    }
}

export default getMetadataByType;