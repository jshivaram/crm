import consts from './controlConsts';
import getControlDatasource from './config/controlDatasource';

function getControlConfig(controlType, element) {
    var definition = JSON.parse(element.ddsm_definition);

    var dataSource = getControlDatasource(element);
    var field_name = definition.selectedField;

    console.log('control field name: ' + field_name);

    if (!field_name) {
        return;
    }

    var controlId = field_name + "_control";
    var nativeControlLabelSelector = "#" + field_name + "_cl_span";
    var nativeControlSelector = "#" + field_name + " .ms-crm-Inline-Value";
    var nativeControlEditSelector = "#" + field_name + " .ms-crm-Inline-Edit";

    var $container = getContainer();

    var $nativeControl = $container.find(nativeControlSelector);

    if (!$nativeControl.length) {
        return;
    }

    var existed = $container.find("#" + controlId).data("kendoMultiSelect");
    if (existed) {
        return;
    }

    var controlInlineStyles = "width: 94%; margin-left: 5%;";
    if (controlType === consts.MULTISELECT_CONTROL) {
        controlInlineStyles += "overflow: auto; max-height: 50px;";
    }

    var $kendoControl = $('<div/>', {
        id: controlId,
        style: controlInlineStyles
    });

    $container.find("#" + field_name).append($kendoControl);

    var config = {
        definition: definition,
        dataSource: dataSource,
        nativeControlLabelSelector: nativeControlLabelSelector,
        nativeControlSelector: nativeControlSelector,
        nativeControlEditSelector: nativeControlEditSelector,
        $nativeControl: $nativeControl,
        $kendoControl: $kendoControl
    }

    return config;
}

function getContainer() {
    if (!window.top.ADT.container) {
        return $(window.document.body);
    }

    return $(window.top.ADT.container);
}

export default getControlConfig;