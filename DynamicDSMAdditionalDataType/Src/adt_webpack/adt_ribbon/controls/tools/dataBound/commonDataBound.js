import hideNativeControl from './hideNativeControl';
import setValueDictionary from './setValueDictionary';

function commonDataBound(controlType, conf, jsonDataIsRequried) {
    return function () {

        var Xrm = window.top.ADT.Xrm;

        var prevValueControl = Xrm.Page.getAttribute(conf.definition.selectedField);
        if (!prevValueControl) {
            console.log("Error in data bound control (" + controlType + ")."
                + " Can't get data from " + conf.definition.selectedField + " field");
            return;
        }

        hideNativeControl(conf);

        var prevValue = prevValueControl.getValue();
        if (!prevValue) {
            return;
        }

        setValueDictionary[controlType](this, jsonDataIsRequried, prevValue);
    }
}

export default commonDataBound;