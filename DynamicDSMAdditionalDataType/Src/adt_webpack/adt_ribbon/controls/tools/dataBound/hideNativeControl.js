function hideNativeControl(conf) {
    var Xrm = window.top.ADT.Xrm;

    var control = Xrm.Page.getControl(conf.definition.selectedField);
    if (control) {
        control.setVisible(true);
    }

    conf.$nativeControl.css("display", "none");

    var source = document;

    if (window.top.ADT.container) {
        source = window.top.ADT.container.ownerDocument;
    }

    // It's like unbind how in jquery
    var nativeControl = source.querySelectorAll(conf.nativeControlSelector)[0];
    var nativeControlClone = nativeControl.cloneNode(true);
    nativeControl.parentNode.replaceChild(nativeControlClone, nativeControl);

    var nativeControlEdit = source.querySelectorAll(conf.nativeControlEditSelector)[0];
    var nativeControlEditClone = nativeControlEdit.cloneNode(true);
    nativeControlEdit.parentNode.replaceChild(nativeControlEditClone, nativeControlEdit);

    var nativeControlLabel = source.querySelectorAll(conf.nativeControlLabelSelector)[0];
    if (nativeControlLabel) {
        var nativeControlLabelClone = nativeControlLabel.cloneNode(true);
        nativeControlLabel.parentNode.replaceChild(nativeControlLabelClone, nativeControlLabel);
    }
}

export default hideNativeControl;