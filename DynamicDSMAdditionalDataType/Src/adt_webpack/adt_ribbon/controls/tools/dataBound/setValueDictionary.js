import consts from '../controlConsts';

var setValueDictionary = {};

setValueDictionary[consts.MULTISELECT_CONTROL] =
    function (adtControl, jsonDataIsRequried, prevValue) {

        var resultValue = null;

        // if (jsonDataIsRequried) {
        //     var objArr = JSON.parse(prevValue);

        //     if (!Array.isArray(objArr)) {
        //         objArr = [objArr];
        //     }

        //     resultValue = objArr.map(function (item) {
        //         return item.Label;
        //     });
        // }
        // else {
        //     resultValue = prevValue.split(',').map(function (item) {
        //         return item.trim();
        //     });
        // }

        resultValue = prevValue.split(',').map(function (item) {
            return item.trim();
        });

        adtControl.value(resultValue);
    };

setValueDictionary[consts.DROPDOWNLIST_CONTROL] =
    function (adtControl, jsonDataIsRequried, prevValue) {

        var resultValue = prevValue.trim();

        if (jsonDataIsRequried) {
            var obj = JSON.parse(resultValue);

            if (Array.isArray(obj)) {
                obj = obj[0];
            }

            resultValue = obj.Label;
        }

        adtControl.text(resultValue);
    };

export default setValueDictionary;