import consts from '../controlConsts';

const changeEventDictionary = {};

changeEventDictionary[consts.MULTISELECT_CONTROL] = function (conf, jsonDataIsRequried) {
    return function (e) {

        var Xrm = window.top.ADT.Xrm;

        var dataItems = this.dataItems();
        var nativeControl = Xrm.Page.getAttribute(conf.definition.selectedField);

        var resultValue = "";

        // if (!jsonDataIsRequried) {
        //     var simpleArr = dataItems.map(function (item) {
        //         return item.Label;
        //     });

        //     resultValue = simpleArr.toString();
        // }
        // else {
        //     var objArr = dataItems.map(function (item) {
        //         return {
        //             Label: item.Label,
        //             LogicalName: item.LogicalName
        //         };
        //     });

        //     resultValue = JSON.stringify(objArr);
        // }

        var simpleArr = dataItems.map(function (item) {
            return item.Label;
        });

        resultValue = simpleArr.toString();

        nativeControl.setValue(resultValue);
    }
}

changeEventDictionary[consts.DROPDOWNLIST_CONTROL] = function (conf, jsonDataIsRequried) {
    return function (e) {

        var Xrm = window.top.ADT.Xrm;

        var dataItem = this.dataItem();
        var nativeControl = Xrm.Page.getAttribute(conf.definition.selectedField);

        if (!jsonDataIsRequried) {
            nativeControl.setValue(dataItem.Label);
            return;
        }

        var settedItem = {
            Label: dataItem.Label,
            LogicalName: dataItem.LogicalName
        };

        var jsonString = JSON.stringify(settedItem);
        nativeControl.setValue(jsonString);
    }
}

export default changeEventDictionary;