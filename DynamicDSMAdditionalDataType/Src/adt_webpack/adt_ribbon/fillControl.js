import initMultiselect from './controls/multiselect';
import initDropdown from './controls/dropdownlist';
import initBPF from './controls/bpf'

function fillControl(element) {
    var ds = {};
    switch (element.ddsm_datatype) {
        case 962080000:
            try {
                initMultiselect(element);
            } catch (err) {
                console.log(err);
            } finally {
                break;
            }

        case 962080001:
            try {
                initDropdown(element);
            } catch (err) {
                console.log(err);
            } finally {
                break;
            }
        case 962080002:
            try {
            initBPF(element);
            } catch (err) {
                console.log(err);
            } finally {
                break;
            }
    }
}

export default fillControl;