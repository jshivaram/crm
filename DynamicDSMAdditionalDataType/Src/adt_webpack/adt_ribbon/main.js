import fillControl from './fillControl';
import launchConfig from './launchConfig';
import launchEnum from './launchEnum';

switch (launchConfig) {
    case launchEnum.DDSM_365:
        Start();
        break;
    case launchEnum.DDSM_2016:
        if (!window.define) {
            console.log('***** ERROR ***** window.define is undefined');
            break;
        }
        window.define(["jquery", "kendo.all.min"], function ($, kendo) {
            Start();
        });
        break;
}

function Start() {
    if (!Xrm.Page.data || window.AlreadyInitCustomControl) {
        return;
    }

    window.AlreadyInitCustomControl = true;

    window.top.ADT.initControls = function (config) {
        if (!window.top.ADT.Xrm) {
            window.top.ADT.Xrm = window.Xrm;
        }

        config.forEach(function (element) {

            fillControl(element);
        }, this);
    }

    const entityLogicalName = Xrm.Page.data.entity.getEntityName();

    window.top.ADT.getControlsMetadata(entityLogicalName, window.top.ADT.initControls);
}