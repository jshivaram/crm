function initControls() {
    let tab = Xrm.Page.ui.tabs.getByName("tab_5");
    tab.addTabStateChange(function (args) {
        let builderResults = buildHtmlComponents();
        Xrm.Page.ui.controls.forEach(function (control) {
            let controlType = control.getControlType();
            if (controlType === "subgrid" && control.getEntityName() === "ddsm_additionalcharacteristic") {
                let grid = control.getGrid();
                grid.addOnRecordSelect(function (args) {
                    let source = args.getEventSource();
                    let attr = source.attributes.getByName("ddsm_value");
                    let attrControl = attr.controls.getAll()[0];
                    let entityId = source.getId();
                    createHierarchicalOptionSet(attr, attrControl, control, builderResults, entityId);
                });
            }
        });
    });
}

function createHierarchicalOptionSet(attr, control, subgrid, builderResults, entityId) {
    clean(builderResults);
    control.setDisabled(true);
    builderResults.$dialog.show();
    createTreeList(builderResults, entityId, function () {
        createDialog(builderResults, entityId, control, subgrid);
    });
}

function clean(builderResults) {
    let kendoDialog = builderResults.$dialog.data("kendoDialog");
    let kendoTreeList = builderResults.$control.data("kendoTreeList");

    if (!kendoDialog && !kendoTreeList) {
        return;
    }

    kendoDialog.destroy();
    builderResults.$dialog.empty();
    kendoTreeList.destroy();
    builderResults.$control.empty();
}

function createTreeList(builderResults, entityId, callback) {
    getRemoteData(entityId, function (data) {
        let treeListDataSource = getTreeListDataSource(data.indicatorId);

        builderResults.$control.kendoTreeList({
            dataSource: treeListDataSource,
            height: 380,
            filterable: true,
            sortable: true,
            columns: [
                {
                    // headerTemplate: "<input type='checkbox' class='hos-toggle-all' />",
                    template: "<input type='checkbox' class='checkbox' data-bind='checked: checked' />",
                    width: 40,
                    filterable: false
                },
                {
                    field: "OptionSetValueName",
                    title: "OptionSet Value",
                    expandable: true
                },
                {
                    field: "IndicatorName",
                    title: "Indicator"
                }
            ],
            dataBound: treeListDataBound,
            autoBind: false
        });

        callback();

        function treeListDataBound(e) {

            let $rows = builderResults.$control.find(".k-grid-content tr");
            if (!$rows.length) {
                return;
            }

            $rows.each(function (i, row) {
                let $row = $(row);
                let attrValue = $row.attr("aria-expanded");
                if (!attrValue) {
                    return;
                }
                $row.find("input[type='checkbox']").attr('disabled', 'disabled');;
            });

            let $checkbox = builderResults.$control.find(".checkbox");
            $checkbox.bind("change", function (e) {
                let isMultipleOptionSet = false; // ******************** HARDCORE ********************
                if (!isMultipleOptionSet) {
                    $rows.removeClass("k-state-selected");
                    $checkbox.removeAttr("checked");
                    $(e.target).attr("checked", "checked");
                }

                var row = $(e.target).closest("tr");
                this.checked ? row.addClass("k-state-selected") : row.removeClass("k-state-selected");
            });

            if (!data.codes) {
                return;
            }

            let selectedCodes = data.codes.split(";");

            builderResults.$control.find(".checkbox").removeAttr("checked");
            builderResults.$control.find(".k-grid-content tr").removeClass("k-state-selected");

            let self = this;
            $rows.each(function (i, htmlRow) {
                let $htmlRow = $(htmlRow);
                let dataItem = self.dataItem($htmlRow);

                let selectedDataItem = selectedCodes.find(function (selectedCode) {
                    return dataItem.Code === +selectedCode;
                });

                if (!selectedDataItem) {
                    return;
                }

                $htmlRow.addClass("k-state-selected");
                $htmlRow.find(".checkbox").attr("checked", "checked");
            });
        }
    });

    function getRemoteData(id, callback) {
        let queryOptions = {
            Select: ['_ddsm_characteristics_value', 'ddsm_optionsetvaluecodes']
            , Filter: 'ddsm_additionalcharacteristicid eq ' + id
        };

        GlobalJs.WebAPI.GetList("ddsm_additionalcharacteristics", queryOptions).then(
            function (response) {
                let record = response.List[0];
                let data = {
                    indicatorId: record["_ddsm_characteristics_value"],
                    codes: record["ddsm_optionsetvaluecodes"]
                };
                callback(data);
            },
            function (error) { });
    }
}

function createDialog(builderResults, entityId, crmAttrControl, subgrid) {

    let kendoDialog = builderResults.$dialog.kendoDialog({
        width: '800px',
        height: '490px',
        visible: false,
        closable: true,
        modal: false,
        content: builderResults.$control,
        actions: [
            { text: 'Cancel', action: actionCancel },
            { text: 'OK', primary: true, action: actionOK }
        ],
        open: openDialogHandler
    }).data('kendoDialog');

    let $wrapper = kendoDialog.wrapper;
    $wrapper.addClass('hos-dialog');

    kendoDialog.open();

    function actionOK(e) {
        let treelist = builderResults.$control.data("kendoTreeList");
        let items = treelist.element.find(".k-state-selected");
        updateResult(items, treelist);
    }

    function actionCancel() {
        subgrid.refresh();
    }

    function updateResult(items, treelist) {
        let result = "";
        let selectedCodes = [];
        let joinedHierarchyNames = [];

        let dataItems = treelist.dataItems();
        for (let i = 0; i < items.length; i++) {
            let dataItem = treelist.dataItem(items[i]);
            selectedCodes.push(dataItem.Code);

            let joinedHierarchyName = getJoinedHierarchyName(dataItems, dataItem);
            joinedHierarchyNames.push(joinedHierarchyName);
        }

        let selectedCodesResult = selectedCodes.join(";");
        let joinedHierarchyNamesResult = joinedHierarchyNames.join(";");

        saveData(entityId, joinedHierarchyNamesResult, selectedCodesResult, function () {
            subgrid.refresh();
            crmAttrControl.setDisabled(false);
        });
    }

    function getJoinedHierarchyName(items, selectedItem) {
        let currentItem = selectedItem;
        let result = currentItem.OptionSetValueName;

        while (currentItem.ParentOptionSetValueId) {
            currentItem = getItemById(items, currentItem.ParentId);
            result = currentItem.OptionSetValueName + ", " + result;
        }

        return result;
    }

    function getItemById(items, id) {
        let foundItem = items.find(function (item) {
            return item.Id === id;
        });

        return foundItem;
    }

    function openDialogHandler() {
        let kendoTreeList = builderResults.$control.data("kendoTreeList");
        kendoTreeList.dataSource.read();
        kendoTreeList.refresh();
    }
}

function saveData(entityId, names, codes, callback) {
    GlobalJs.WebAPI.Update("ddsm_additionalcharacteristics", entityId, {
        ddsm_value: names,
        ddsm_optionsetvaluecodes: codes
    }).then(callback, function (error) { });
}

function buildHtmlComponents() {
    let $container = $(window.document.body);

    let $dialog = $("<div/>");
    $dialog.hide();
    $dialog.appendTo($container);

    let $control = $('<div/>', {
        class: 'hos-control'
    });

    let result = {
        $dialog: $dialog,
        $control: $control
    };

    return result;
}

function getTreeListDataSource(indicatorId) {

    var dataSource = new kendo.data.TreeListDataSource({
        transport: {
            read: function (options) {
                var data = {
                    IndicatorId: indicatorId
                };
                GlobalJs.WebAPI.ExecuteAction('ddsm_DDSMGetHierarchicalOptionSetValues', data).then(
                    function (response) {
                        options.success(response);
                    },
                    function (error) {
                        options.error(error);
                    }
                );
            }
        },
        batch: true,
        schema: {
            data: function (response) {
                var responsedData = JSON.parse(response.Result);

                return responsedData;
            },
            model: {
                id: "Id",
                parentId: "ParentId",
                fields: {
                    Id: { type: "number" },
                    ParentId: { type: "number" },
                    IndicatorId: { type: "string" },
                    IndicatorName: { type: "string" },
                    OptionSetValueId: { type: "string" },
                    OptionSetValueName: { type: "string" },
                    ParentOptionSetValueId: { type: "string" },
                    ParentOptionSetValueName: { type: "string" },
                    Code: { type: "number" }
                },
                expanded: true
            }
        }
    });

    return dataSource;
}

export default initControls;