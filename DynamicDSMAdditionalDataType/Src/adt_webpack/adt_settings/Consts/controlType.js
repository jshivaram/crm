const controlType = {
    Multiselect: 962080000,
    Dropdown: 962080001,
    BusinessProcessFlows: 962080002,
    Button: 962080003
};

export default controlType;