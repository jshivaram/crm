import getValuesForADTDropdown from '../../../button_develop/getValuesForADTDropdown'

export default function (selector, entityName, fieldName) {
    let $container = $(selector);
    let $kendoData = $container.data("kendoDropDownList");
    //remove dropdown if exists
    if ($kendoData) {
        $kendoData.destroy();
        $container.empty();
    }
    //--------------------------

    try {
        let dataArray = getValuesForADTDropdown(entityName, fieldName);

        $container.kendoDropDownList({
            dataSource: dataArray,
            dataTextField: "displayName",
            dataValueField: "displayName",
            optionLabel: '--',
            select: function(e){
                let formControl = parent.Xrm.Page.getAttribute('ddsm_definition');
                let jsonDefinition = formControl.getValue();
                let currentObject = JSON.parse(jsonDefinition);

                //set dropdown value to definition object
                currentObject.methodId = e.sender.dataItem(e.item)[e.sender.options.dataValueField];
                currentObject.entityName = entityName;

                const stringedResult = JSON.stringify(currentObject);
                formControl.setValue(stringedResult);

            }
        });

        $('#buttonContainerDiv').show();
    } catch (error) {
        console.error(error);
        throw error;
    }

}