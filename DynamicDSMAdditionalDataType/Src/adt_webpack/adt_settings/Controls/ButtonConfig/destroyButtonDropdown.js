export default function (parentSelector, componentSelector) {
    let $container = $(componentSelector);
    let $kendoData = $container.data("kendoDropDownList");
    //remove dropdown if exists
    if ($kendoData) {
        $kendoData.destroy();
        $container.empty();
    }
    $(parentSelector).hide();
    //--------------------------
}