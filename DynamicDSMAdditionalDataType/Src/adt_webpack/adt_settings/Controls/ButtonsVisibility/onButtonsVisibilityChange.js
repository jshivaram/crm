import Save from '#/Save';

function onButtonsVisibilityChange() {
    
    window.top.definition.buttonsVisibility = this.buttonsVisibility;

    Save();
}

export default onButtonsVisibilityChange;