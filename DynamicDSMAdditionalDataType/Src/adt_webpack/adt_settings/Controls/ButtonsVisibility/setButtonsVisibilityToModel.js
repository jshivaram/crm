import onButtonsVisibilityChange from './onButtonsVisibilityChange';

function setButtonsVisibilityToModel(model) {
    model.onButtonsVisibilityChange = onButtonsVisibilityChange;

    if (window.top.definition && window.top.definition.buttonsVisibility) {
        model.buttonsVisibility = window.top.definition.buttonsVisibility
    }
}

export default setButtonsVisibilityToModel;