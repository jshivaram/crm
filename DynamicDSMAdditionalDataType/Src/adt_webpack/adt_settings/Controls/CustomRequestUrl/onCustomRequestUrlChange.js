import Save from '#/Save';

function onCustomRequestUrlChange() {
    
    window.top.definition.customRequestUrl = this.customRequestUrl;

    Save();
}

export default onCustomRequestUrlChange;