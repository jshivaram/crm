import onCustomRequestUrlChange from './onCustomRequestUrlChange';

function setCustomRequestUrlToModel(model) {
    model.onCustomRequestUrlChange = onCustomRequestUrlChange;
    model.customRequestUrlVisibility = function() {
        return this.get("isBPFControl") && this.get("isCustomUrl");
    };

    if (window.top.definition && window.top.definition.customRequestUrl) {
        model.customRequestUrl = window.top.definition.customRequestUrl
    }
}

export default setCustomRequestUrlToModel;