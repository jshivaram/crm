import KendoHelper from '~/kendo_helper/helper';

const Xrm = window.parent.Xrm;

function getIndicatorDataSource(entityLogicalName) {
    return new kendo.data.DataSource({
        schema: {
            data: function(response) {
                if (!response.value || !response.value.length) {
                    return [];
                }

                let attrs = response.value[0].Attributes;
                let filteredAttrs = attrs.filter(function (attr) {
                    return attr.Targets[0] === "ddsm_indicator"
                });

                if (!filteredAttrs.length) {
                    return [];
                }

                let result = filteredAttrs.map(function(item) {
                    let newItem = {
                        Label: getLabel(item.DisplayName, item.LogicalName),
                        LogicalName: item.LogicalName
                    };

                    return newItem;
                });

                return result;
            }
        },
        sort: {
            field: "Label",
            dir: "asc"
        },
        transport: KendoHelper.transport(window.parent.GlobalJs.clientUrl + window.parent.GlobalJs.baseUrl
            + "EntityDefinitions?$select=LogicalName&$expand=Attributes($filter=AttributeType eq Microsoft.Dynamics.CRM.AttributeTypeCode'Lookup')"
            + "&$filter=LogicalName eq '" + entityLogicalName + "'"
        )
    });
}

function getLabel(displayName, logicalName) {
    if (displayName.UserLocalizedLabel) {
        return displayName.UserLocalizedLabel.Label;
    }
    else if (displayName.LocalizedLabels && displayName.LocalizedLabels.length) {
        return displayName.LocalizedLabels[0].Label;
    }

    return logicalName;
}

export default getIndicatorDataSource;