function indicatorDataBound() {
    let kendoIndicator = $('#indicator').data('kendoDropDownList');
    if (!window.top.definition || !window.top.definition.indicatorLookup) {
        kendoIndicator.value("");
    }

    kendoIndicator.value(window.top.definition.indicatorLookup);
}

export default indicatorDataBound;