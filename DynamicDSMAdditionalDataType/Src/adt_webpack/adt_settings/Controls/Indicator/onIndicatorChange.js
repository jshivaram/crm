import Save from '#/Save';

function onIndicatorChange() {

    const indicatorControl = $("#indicator").data("kendoDropDownList");
    window.top.definition.indicatorLookup = indicatorControl.value();

    Save();
}

export default onIndicatorChange;