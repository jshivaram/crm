import getIndicatorDataSource from './getIndicatorDataSource';
import Save from '#/Save';

function setIndicatorDependency(viewModel) {

    if (!viewModel.isHierarchicalOptionSet) {
        return;
    }

    let kendoEntity = $('#entity').data('kendoDropDownList');
    let kendoIndicator = $('#indicator').data('kendoDropDownList');

    kendoEntity.bind('change', function (e) {
        let selectedEntityLogicalName = e.sender.value();
        if (!selectedEntityLogicalName) {
            kendoIndicator.setDataSource({});
        }
        else {
            let indicatorDataSource = getIndicatorDataSource(selectedEntityLogicalName);
            kendoIndicator.setDataSource(indicatorDataSource);
        }

        window.top.definition.indicatorLookup = "";
        Save();
    });
    let entityLogicalName = kendoEntity.value();

    if (!entityLogicalName) {
        return;
    }

    let indicatorDataSource = getIndicatorDataSource(entityLogicalName);
    kendoIndicator.setDataSource(indicatorDataSource);
}

export default setIndicatorDependency;