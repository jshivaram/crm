import onIndicatorChange from './onIndicatorChange';
import indicatorDataBound from './indicatorDataBound';

function setIndicatorToModel(model) {
    model.onIndicatorChange = onIndicatorChange;
    model.indicatorDataBound = indicatorDataBound;
}

export default setIndicatorToModel;