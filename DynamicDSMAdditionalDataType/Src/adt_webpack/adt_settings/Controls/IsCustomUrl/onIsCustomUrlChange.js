import Save from '#/Save';

function onIsCustomUrlChange() {
    
    window.top.definition.isCustomUrl = this.isCustomUrl;

    Save();
}

export default onIsCustomUrlChange;