import onIsCustomUrlChange from './onIsCustomUrlChange';

function setIsCustomUrlToModel(model) {
    model.onIsCustomUrlChange = onIsCustomUrlChange;

    if (window.top.definition && window.top.definition.isCustomUrl) {
        model.isCustomUrl = window.top.definition.isCustomUrl
    }
}

export default setIsCustomUrlToModel;