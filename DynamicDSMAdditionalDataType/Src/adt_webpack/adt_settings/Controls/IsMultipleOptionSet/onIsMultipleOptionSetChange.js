import Save from '#/Save';

function onIsMultipleOptionSetChange() {
    window.top.definition.isMultipleOptionSet = this.isMultipleOptionSet;

    Save();
}

export default onIsMultipleOptionSetChange;