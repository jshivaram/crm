import onIsMultipleOptionSetChange from './onIsMultipleOptionSetChange';

function setIsMultipleOptionSetToModel(model) {
    model.onIsMultipleOptionSetChange = onIsMultipleOptionSetChange;

    if (window.top.definition && window.top.definition.isMultipleOptionSet) {
        model.isMultipleOptionSet = window.top.definition.isMultipleOptionSet
    }
}

export default setIsMultipleOptionSetToModel;