import KendoHelper from '~/kendo_helper/helper';

const Xrm = window.parent.Xrm;
const clientUrl = Xrm.Page.context.getClientUrl();
const baseUrl = "/api/data/v8.1/";

function getProcessFlowDataSource() {
    return new kendo.data.DataSource({
        schema: {
            data: 'value'
        },
        sort: {
            field: "DisplayName",
            dir: "asc"
        },
        transport: KendoHelper.transport(
            encodeURI(clientUrl + baseUrl 
                + 'ddsm_processflows?$select=ddsm_name')
        )
    });
}

export default getProcessFlowDataSource;