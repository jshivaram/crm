import Save from '#/Save';

function onProcessFlowChange() {

    const processFlowControl = $("#processflow").data("kendoDropDownList");
    window.top.definition.processFlowId = processFlowControl.value();

    Save();
}

export default onProcessFlowChange;