function selectedProcessFlow() {

    if (!window.top.definition || !window.top.definition.processFlowId) {
        return;
    }

    return window.top.definition.processFlowId;
}

export default selectedProcessFlow;