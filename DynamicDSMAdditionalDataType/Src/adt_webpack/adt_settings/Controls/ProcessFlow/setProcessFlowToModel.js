import getProcessFlowDataSource from './getProcessFlowDataSource';
import onProcessFlowChange from './onProcessFlowChange';
import selectedProcessFlow from './selectedProcessFlow';

function setProcessFlowToModel(model) {
    model.processFlowDataSource = getProcessFlowDataSource();
    model.onProcessFlowChange = onProcessFlowChange;
    model.selectedProcessFlow = selectedProcessFlow;
}

export default setProcessFlowToModel;