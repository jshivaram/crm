function getRelatedEntityDataSource(entityLogicalName) {

    if (!entityLogicalName) {
        return new kendo.data.DataSource({});
    }

    return new kendo.data.DataSource({
        sort: {
            field: "Label",
            dir: "asc"
        },
        schema: {
            data: function (response) {
                if (!response || !response.Result) {
                    return;
                }

                let result = JSON.parse(response.Result);
                return result;
            }
        },
        transport: {
            read: function (options) {
                var data = {
                    EntityLogicalName: entityLogicalName
                };
                parent.GlobalJs.WebAPI.ExecuteAction('ddsm_DDSMGetOneToManyRelationships', data).then(
                    function (response) {
                        options.success(response);
                    },
                    function (error) {
                        options.error(error);
                    }
                );
            }
        }
    });
}

export default getRelatedEntityDataSource;