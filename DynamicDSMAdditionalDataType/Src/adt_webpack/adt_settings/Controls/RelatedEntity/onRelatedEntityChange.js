import Save from '#/Save';

function onRelatedEntityChange() {
    const relatedEntityControl = $("#relatedEntity").data("kendoDropDownList");
    window.top.definition.relatedEntity = relatedEntityControl.value();
    let dataItem = relatedEntityControl.dataItem();
    window.top.definition.relatedEntityCollectionName = dataItem.LogicalCollectionName;

    Save();
}

export default onRelatedEntityChange;