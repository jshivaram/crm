function relatedEntityDataBound() {
    if (this.isRelatedEntityFiltered) {
        return;
    }
    let kendoRelatedEntity = $('#relatedEntity').data('kendoDropDownList');
    if (!window.top.definition || !window.top.definition.relatedEntity) {
        kendoRelatedEntity.value("");
    }

    kendoRelatedEntity.value(window.top.definition.relatedEntity);
}

export default relatedEntityDataBound;