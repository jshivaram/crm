import onRelatedEntityChange from './onRelatedEntityChange';
import relatedEntityDataBound from './relatedEntityDataBound';

function setRelatedEntityToModel(model) {
    model.isRelatedEntityFiltered = false;
    model.relatedEntityFiltering = function () {
        this.isRelatedEntityFiltered = true;
    };
    model.onRelatedEntityChange = onRelatedEntityChange;
    model.relatedEntityDataBound = relatedEntityDataBound;
}

export default setRelatedEntityToModel;