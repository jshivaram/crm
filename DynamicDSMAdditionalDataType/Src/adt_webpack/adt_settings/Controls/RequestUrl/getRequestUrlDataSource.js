function getRequestUrlDatasource() {
    return new kendo.data.DataSource({
        sort: {
            field: "Label",
            dir: "asc"
        },
        data: [
            { Label: "Url1", LogicalName: "url1" },
            { Label: "Url2", LogicalName: "url2" },
            { Label: "Url3", LogicalName: "url3" }
        ]
    });
}

export default getRequestUrlDatasource;