import Save from '#/Save';

function onRequestUrlChange() {

    const requestUrlControl = $("#requestUrl").data("kendoDropDownList");
    window.top.definition.requestUrl = requestUrlControl.value();

    Save();
}

export default onRequestUrlChange;