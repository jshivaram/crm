function requestUrlDataBound() {
    
    if (!window.top.definition || !window.top.definition.requestUrl) {
        return;
    }

    const RUControl = $("#requestUrl").data("kendoDropDownList");
    RUControl.value(window.top.definition.requestUrl);
}

export default requestUrlDataBound;