import getRequestUrlDataSource from './getRequestUrlDataSource';
import onRequestUrlChange from './onRequestUrlChange';
import requestUrlDataBound from './requestUrlDataBound';

function setRequestUrlToModel(model) {
    model.requestUrlsListDatasource = getRequestUrlDataSource();
    model.onRequestUrlsListChange = onRequestUrlChange;
    model.requestUrlsListDataBound = requestUrlDataBound;
    model.requestUrlVisibility = function() {
        return this.get("isBPFControl") && !this.get("isCustomUrl");
    };
}

export default setRequestUrlToModel;