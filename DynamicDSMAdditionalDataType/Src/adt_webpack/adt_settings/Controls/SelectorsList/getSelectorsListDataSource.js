function getSelectorsListDatasource() {
    return new kendo.data.DataSource({
        sort: {
            field: "Label",
            dir: "asc"
        },
        data: [
            { Label: "Header data container", LogicalName: ".headerDataContainerTable" }
        ]
    });
}

export default getSelectorsListDatasource;