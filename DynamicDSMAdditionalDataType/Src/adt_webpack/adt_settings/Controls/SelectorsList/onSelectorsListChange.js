import Save from '#/Save';

function onSelectorsListChange() {

    const selectorsListControl = $("#selectorsList").data("kendoDropDownList");
    window.top.definition.selector = selectorsListControl.value();

    Save();
}

export default onSelectorsListChange;