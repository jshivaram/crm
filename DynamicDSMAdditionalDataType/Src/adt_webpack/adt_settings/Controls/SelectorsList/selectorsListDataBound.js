function selectorsListDataBound() {
    
    if (!window.top.definition || !window.top.definition.selector) {
        return;
    }

    const SLControl = $("#selectorsList").data("kendoDropDownList");
    SLControl.value(window.top.definition.selector);
}

export default selectorsListDataBound;