import getSelectorsListDataSource from './getSelectorsListDataSource';
import onSelectorsListChange from './onSelectorsListChange';
import selectorsListDataBound from './selectorsListDataBound';

function setSelectorsListToModel(model) {
    model.selectorsListDatasource = getSelectorsListDataSource();
    model.onSelectorsListChange = onSelectorsListChange;
    model.selectorsListDataBound = selectorsListDataBound;
}

export default setSelectorsListToModel;