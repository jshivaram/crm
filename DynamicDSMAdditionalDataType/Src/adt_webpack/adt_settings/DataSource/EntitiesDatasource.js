import KendoHelper from '~/kendo_helper/helper';

const Xrm = window.parent.Xrm;
const clientUrl = Xrm.Page.context.getClientUrl();
const baseUrl = "/api/data/v8.1/";

function EntitiesDatasource() {
    return new kendo.data.DataSource({
        schema: KendoHelper.defEntityMetadataSchema(),
        sort: {
            field: "Label",
            dir: "asc"
        },
        transport: KendoHelper.transport(encodeURI(clientUrl + baseUrl + 'EntityDefinitions?$select=DisplayName,SchemaName,MetadataId,EntitySetName,PrimaryNameAttribute,PrimaryIdAttribute')),
    });
}

export default EntitiesDatasource;