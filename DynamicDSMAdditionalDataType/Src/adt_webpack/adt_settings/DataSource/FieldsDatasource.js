import KendoHelper from '~/kendo_helper/helper';

const Xrm = window.parent.Xrm;
const clientUrl = Xrm.Page.context.getClientUrl();
const baseUrl = "/api/data/v8.1/";

function FieldsDatasource(entityname) {
    if (entityname === null || entityname === "") {
        console.log("exit from FieldsDatasource. Entity Name is empty");
        return;
    }
    return new kendo.data.DataSource({
        schema: {
            data: function (response) {
                if (!response.value || response.value.length == 0) {
                    console.log("cant get data for entity: " + entityname);
                    return;
                }
                var value = response.value[0].Attributes;
                var model = value.filter(function (el) {
                    //debugger;
                    var displayName = "";
                    // var id = el.MetadataId;
                    var type = el.AttributeType;
                    //console.log("type:" + type);
                    var logicalName = el.SchemaName; //.toLowerCase();
                    // var attributeOf = el.AttributeOf;
                    if (el.DisplayName.LocalizedLabels[0] && el.DisplayName.LocalizedLabels[0].Label) {
                        displayName = el.DisplayName.LocalizedLabels[0].Label;
                    } else if (el.DisplayName.UserLocalizedLabel && el.DisplayName.UserLocalizedLabel.Label) {
                        displayName = el.DisplayName.UserLocalizedLabel.Label;
                    } else {
                        displayName = logicalName;
                    }

                    if (type == "Memo") {
                        el["Label"] = displayName;
                        //el["PrimaryNameAttribute"] = el.PrimaryNameAttribute;
                        return el;
                    }

                });
                return model;
            }
        },
        transport: KendoHelper.transport(clientUrl + baseUrl + "EntityDefinitions?$select=LogicalName&$expand=Attributes($select=DisplayName,LogicalName,AttributeType,SchemaName,AttributeOf)&$filter=LogicalName eq '" + entityname + "'"),
    });
}

export default FieldsDatasource;