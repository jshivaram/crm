import KendoHelper from '~/kendo_helper/helper';

const Xrm = window.parent.Xrm;
const clientUrl = Xrm.Page.context.getClientUrl();
const baseUrl = "/api/data/v8.1/";

function OptionSetDatasource() {
    return new kendo.data.DataSource({
        schema: {
            data: function (response) {
                var value = response.value;
                var model = value.filter(function (el) {
                    var displayName = "";
                    var logicalName = el.Name;

                    if (el.DisplayName.LocalizedLabels[0] && el.DisplayName.LocalizedLabels[0].Label) {
                        displayName = el.DisplayName.LocalizedLabels[0].Label;
                    } else if (el.DisplayName.UserLocalizedLabel && el.DisplayName.UserLocalizedLabel.Label) {
                        displayName = el.DisplayName.UserLocalizedLabel.Label;
                    } else {
                        displayName = logicalName;
                    }
                    el["Label"] = displayName;
                    el["LogicalName"] = logicalName;
                    return el;
                });
                return model;
            }
        }, //KendoHelper.defEntityMetadataSchema(),
        sort: {
            field: "Label",
            dir: "asc"
        },
        transport: KendoHelper.transport(encodeURI(clientUrl + baseUrl + 'GlobalOptionSetDefinitions?$select=DisplayName,Name')),
    });
}

export default OptionSetDatasource;