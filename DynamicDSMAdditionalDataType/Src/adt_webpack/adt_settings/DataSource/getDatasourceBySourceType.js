import EntitiesDatasource from '../DataSource/EntitiesDatasource';
import OptionSetDatasource from '../DataSource/OptionSetDatasource';

const Xrm = window.parent.Xrm;

function getDatasourceBySourceType() {
    var attribute = Xrm.Page.getAttribute("ddsm_sourcetype");

    if (!attribute) return;

    switch (attribute.getValue()) {
        case 962080000:
        case 962080002:
            return EntitiesDatasource();
            break;
        case 962080001:
            return OptionSetDatasource();
            break;
    }

}

export default getDatasourceBySourceType;