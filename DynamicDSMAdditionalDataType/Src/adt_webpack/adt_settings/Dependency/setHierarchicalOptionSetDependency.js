import getIndicatorDataSource from '../Controls/Indicator/getIndicatorDataSource';
import Save from '#/Save';
import FieldsDatasource from '../DataSource/FieldsDatasource';

function setHierarchicalOptionSetDependency(viewModel) {
    if (!viewModel.isHierarchicalOptionSet || viewModel.isHierarchicalOptionSetinSubgrid) {
        return;
    }

    let kendoEntity = $('#entity').data('kendoDropDownList');
    let kendoIndicator = $('#indicator').data('kendoDropDownList');
    var codes = $("#codesField").data("kendoDropDownList");
    kendoIndicator.setDataSource({});
    codes.setDataSource({});

    kendoEntity.bind('change', function (e) {
        let selectedEntityLogicalName = e.sender.value();
        if (!selectedEntityLogicalName) {
            kendoIndicator.setDataSource({});
            codes.setDataSource({});
        }
        else {
            codes.setDataSource(FieldsDatasource(selectedEntityLogicalName));
            let indicatorDataSource = getIndicatorDataSource(selectedEntityLogicalName);
            kendoIndicator.setDataSource(indicatorDataSource);
        }

        Save();
    });
    kendoEntity.bind('dataBound', function (e) {
        let entityLogicalName = kendoEntity.value();

        if (!entityLogicalName) {
            return;
        }

        codes.setDataSource(FieldsDatasource(entityLogicalName));
        let indicatorDataSource = getIndicatorDataSource(entityLogicalName);
        kendoIndicator.setDataSource(indicatorDataSource);
    });
}

export default setHierarchicalOptionSetDependency;