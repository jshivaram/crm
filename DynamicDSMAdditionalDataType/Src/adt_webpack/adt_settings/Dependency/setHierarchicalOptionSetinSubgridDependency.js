import getIndicatorDataSource from '../Controls/Indicator/getIndicatorDataSource';
import Save from '#/Save';
import FieldsDatasource from '../DataSource/FieldsDatasource';

function setHierarchicalOptionSetinSubgridDependency(viewModel) {
    if (!viewModel.isHierarchicalOptionSetinSubgrid) {
        return;
    }

    var relatedEntity = $("#relatedEntity").data("kendoDropDownList");
    var fields = $("#field").data("kendoDropDownList");
    var codes = $("#codesField").data("kendoDropDownList");
    let kendoIndicator = $('#indicator').data('kendoDropDownList');
    clean();

    relatedEntity.bind('change', function (e) {
        let selectedEntityLogicalName = e.sender.value();
        if (!selectedEntityLogicalName) {
            clean();
        }
        else {
            fields.setDataSource(FieldsDatasource(selectedEntityLogicalName));
            codes.setDataSource(FieldsDatasource(selectedEntityLogicalName));
            let indicatorDataSource = getIndicatorDataSource(selectedEntityLogicalName);
            kendoIndicator.setDataSource(indicatorDataSource);
        }

        Save();
    });
    relatedEntity.bind('dataBound', function (e) {
        let entityLogicalName = relatedEntity.value();

        if (!entityLogicalName) {
            return;
        }

        fields.setDataSource(FieldsDatasource(entityLogicalName));
        codes.setDataSource(FieldsDatasource(entityLogicalName));
        let indicatorDataSource = getIndicatorDataSource(entityLogicalName);
        kendoIndicator.setDataSource(indicatorDataSource);
    });

    function clean() {
        fields.setDataSource({});
        kendoIndicator.setDataSource({});
        codes.setDataSource({});
    }
}

export default setHierarchicalOptionSetinSubgridDependency;