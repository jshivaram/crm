import setControlsVisibility from '../setControlsVisibility';

const Xrm = window.parent.Xrm;

function OnDataTypeChange(model) {
    return function () {
        const ddsm_datatype = Xrm.Page.getAttribute('ddsm_datatype').getValue();
        setControlsVisibility(ddsm_datatype, model);
    }
}

export default OnDataTypeChange;