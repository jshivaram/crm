import Save from '#/Save';

const Xrm = window.parent.Xrm;

function OnMetadataTypeChange(_context) {
    window.top.definition.crmMetadataType = Xrm.Page.getAttribute('ddsm_metadatatype').getValue();
    Save();
}

export default OnMetadataTypeChange;