import EntitiesDatasource from '../DataSource/EntitiesDatasource';
import OptionSetDatasource from '../DataSource/OptionSetDatasource';

const Xrm = window.parent.Xrm;

function OnSourceTypeChange(_context) {
    
    var attribute = Xrm.Page.getAttribute("ddsm_sourcetype");
    if (!attribute) return;
    var fieldValue = $("#fieldValue").data("kendoDropDownList");
    
    switch (attribute.getValue()) {
        case 962080000:
        case 962080002:
            fieldValue.setDataSource(EntitiesDatasource());
            fieldValue.dataSource.read();
            break;
        case 962080001:
            fieldValue.setDataSource(OptionSetDatasource());
            fieldValue.dataSource.read();
            break;
    }
}

export default OnSourceTypeChange;