import Save from'#/Save';

const Xrm = window.parent.Xrm;

function OnValueFormatChange(_context) {
    window.top.definition.useSimpleValueFormat = Xrm.Page.getAttribute('ddsm_usesimplevalueformat').getValue();
    Save();
}

export default OnValueFormatChange;