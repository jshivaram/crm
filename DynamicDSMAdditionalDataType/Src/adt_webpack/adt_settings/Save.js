
const Xrm = window.parent.Xrm;

function Save() {
    var definition = Xrm.Page.getAttribute("ddsm_definition");
    var ddsm_usesimplevalueformat = Xrm.Page.getAttribute("ddsm_usesimplevalueformat");

    if (!window.top.definition['useSimpleValueFormat'] && ddsm_usesimplevalueformat) {
        window.top.definition['useSimpleValueFormat'] = ddsm_usesimplevalueformat.getValue();
    }

    var json = JSON.stringify(window.top.definition);
    definition.setValue(json);
}

export default Save;