import FieldsDatasource from '../DataSource/FieldsDatasource';
import EntitiesDatasource from '../DataSource/EntitiesDatasource';
import getDatasourceBySourceType from '../DataSource/getDatasourceBySourceType';

import Save from '#/Save';

import setSelectorsListToModel from '../Controls/SelectorsList/setSelectorsListToModel';
import setButtonsVisibilityToModel from '../Controls/ButtonsVisibility/setButtonsVisibilityToModel';

// import setRequestUrlsListToModel from '../Controls/RequestUrl/setRequestUrlToModel';
// import setIsCustomUrlToModel from '../Controls/IsCustomUrl/setIsCustomUrlToModel';
// import setCustomRequestUrlToModel from '../Controls/CustomRequestUrl/setCustomRequestUrlToModel'
import setProcessFlowToModel from '../Controls/ProcessFlow/setProcessFlowToModel';

import setControlsVisibility from '../setControlsVisibility';
import createButtonDropdown from '../Controls/ButtonConfig/createButtonDropdown'
import destroyButtonDropdown from '../Controls/ButtonConfig/destroyButtonDropdown'
const Xrm = window.parent.Xrm;

//todo main functional file

function getViewModelObserver(kendo) {

    const viewModel = kendo.observable({
        selectedEntity: function () {
            console.log("--in selectedEntity");
            return Xrm.Page.getAttribute("ddsm_targetentity").getValue();
            // if (window.top["definition"]) {
            // return window.top.definition.selectedEntity;
            //}
        },
        entityOnDataBound: function () {
            console.log("--in entityOnDataBound");
            var entity = $("#entity").data("kendoDropDownList");
            var fields = $("#field").data("kendoDropDownList");
            fields.setDataSource(FieldsDatasource(entity.value()));
        },
        selectedField: function () {
            console.log("--in selectedField");
            if (window.top["definition"]) {
                var fields = $("#field").data("kendoDropDownList");
                fields.setDataSource(FieldsDatasource(window.top.definition.selectedField));
                return window.top.definition.selectedField;
            }
        },
        selectedFieldValue: function () {            
            if (window.top.definition && window.top.definition.selectedFieldValue) {
                var fieldValueControl = $("#fieldValue").data("kendoDropDownList");
                fieldValueControl.value(window.top.definition.selectedFieldValue); //getLogicalName(fieldValueControl, e);
            }
        },
        onEntitySelect: function (e) {
            var fields = $("#field").data("kendoDropDownList");
            var entity = $("#entity").data("kendoDropDownList");

            function getLogicalName(s, d) {
                var dataItem = s.dataItem(d.item);
                return dataItem.LogicalName;
            }
            var entityname = getLogicalName(entity, e);

            //save to top
            Xrm.Page.getAttribute("ddsm_targetentity").setValue(entityname);
            //window.top.definition.selectedEntity = entityname;

            // var fields = $("#field").data("kendoDropDownList");
            fields.setDataSource(FieldsDatasource(entityname));

            Save();
        },
        onFieldSelect: function (e) {
            console.log("--in onFieldSelect");
            var fields = $("#field").data("kendoDropDownList");

            function getLogicalName(s, d) {
                var dataItem = s.dataItem(d.item);
                return dataItem.LogicalName;
            }

            function getLabel(s, d) {
                var dataItem = s.dataItem(d.item);
                return dataItem.Label;
            }
            var fieldSchemaName = getLogicalName(fields, e);
            var label = getLabel(fields, e);
            //save to top
            window.top.definition.selectedField = fieldSchemaName;
            window.top.definition.selectedFieldLabel = label;

            const currentOptionSet = parent.Xrm.Page.getAttribute('ddsm_datatype').getValue();
            let entityName = parent.window.Xrm.Page.getAttribute("ddsm_targetentity").getValue();

            if (currentOptionSet === 962080003 && entityName && fieldSchemaName) {
                try {
                    createButtonDropdown('#buttonDropdown', entityName, fieldSchemaName);
                } catch (error) {
                    destroyButtonDropdown('#buttonContainerDiv','#buttonDropdown' );
                }
            } else {
                destroyButtonDropdown('#buttonContainerDiv','#buttonDropdown' );
            }
            Save();


        },
        fieldDataBound: function (e) {
            if (window.top.definition && window.top.definition.selectedField) {
                var fieldValueControl = $("#field").data("kendoDropDownList");
                fieldValueControl.value(window.top.definition.selectedField); //getLogicalName(fieldValueControl, e);
            }
        },
        onFieldValueChange: function (e) {
            var fields = $("#fieldValue").data("kendoDropDownList");
            window.top.definition.selectedFieldValue = fields.value();
            var data = fields.dataItem();
            window.top.definition.selectedFieldValueId = data.MetadataId;
            if (data["EntitySetName"]) {
                window.top.definition.selectedFieldValueLogicalCollectionName = data["EntitySetName"];
            }
            
            window.top.definition.selectedFieldValueLabel = data["Label"];
            window.top.definition.selectedFieldValuePrimaryNameAttribute = data["PrimaryNameAttribute"];
            window.top.definition.selectedFieldValuePrimaryIdAttribute = data["PrimaryIdAttribute"];
            //todo 

            Save();
            /*
                    var definition = Xrm.Page.getAttribute("ddsm_definition");
                    var json = JSON.stringify(window.top.definition);
                    definition.setValue(json);
            */
        },
        fieldValueDataBound: function (e) {
            console.log("--in fieldValueDataBound");
            if (window.top.definition && window.top.definition.selectedFieldValue) {
                var fieldValueControl = $("#fieldValue").data("kendoDropDownList");
                fieldValueControl.value(window.top.definition.selectedFieldValue); //getLogicalName(fieldValueControl, e);
            }
        },
        entities: EntitiesDatasource(),
        fieldValueDatasource: getDatasourceBySourceType()
        //simpleDataIsUsed: OnValueFormatChange()
    });

    setSelectorsListToModel(viewModel);
    setButtonsVisibilityToModel(viewModel);

    // setRequestUrlsListToModel(viewModel);
    // setIsCustomUrlToModel(viewModel);
    // setCustomRequestUrlToModel(viewModel);
    setProcessFlowToModel(viewModel);
    // setButtonConfigToModel(viewModel);

    const ddsm_datatype = Xrm.Page.getAttribute('ddsm_datatype').getValue();
    setControlsVisibility(ddsm_datatype, viewModel);

    return viewModel;
}

export default getViewModelObserver;