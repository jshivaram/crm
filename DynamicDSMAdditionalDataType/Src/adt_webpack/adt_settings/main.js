import OnSourceTypeChange from './GlobalFuncsForFormHandlers/OnSourceTypeChange';
import OnMetadataTypeChange from './GlobalFuncsForFormHandlers/OnMetadataTypeChange';
import OnValueFormatChange from './GlobalFuncsForFormHandlers/OnValueFormatChange';
import OnDataTypeChange from './GlobalFuncsForFormHandlers/OnDataTypeChange';
import getViewModel from './ViewModel/viewModelObserver';

const Xrm = window.parent.Xrm;
const clientUrl = Xrm.Page.context.getClientUrl();

window.requirejs.config({
    baseUrl: "../WebResources",
    waitSeconds: 60,
    paths: {
        "jquery": clientUrl + "/WebResources/kendoui_/jquery2.2.4.min",
        "kendo.all.min": clientUrl + "/WebResources/kendoui_/kendo.all.min"
    }
});

window.requirejs(["jquery", "kendo.all.min"], function ($, kendo) {

    if (!window.top.definition) {
        window.top.definition = {};
    }

    let viewModel = getViewModel(kendo);

    kendo.bind($("#adt-container"), viewModel);

    const $adt_container = $('#adt-container');
    $adt_container.show();

    window.OnSourceTypeChange = OnSourceTypeChange;
    window.OnMetadataTypeChange = OnMetadataTypeChange;
    window.OnValueFormatChange = OnValueFormatChange;
    window.OnDataTypeChange = OnDataTypeChange(viewModel);
});