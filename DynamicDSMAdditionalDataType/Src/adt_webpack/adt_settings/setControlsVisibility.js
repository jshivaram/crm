import CONTROL_TYPE from './Consts/controlType';

function setControlsVisibility(controlType, model) {

    switch (controlType) {
        case CONTROL_TYPE.Multiselect:
        case CONTROL_TYPE.Dropdown: {
            model.set("isSimpleControl", true);
            model.set("isDataSourceControl", true);
            model.set("isBPFControl", false);
            model.set("isButtonControl", false);
            break;
        }
        case CONTROL_TYPE.BusinessProcessFlows: {
            model.set("isSimpleControl", false);
            model.set("isDataSourceControl", false);
            model.set("isBPFControl", true);
            model.set("isButtonControl", false);
            break;
        }
        case CONTROL_TYPE.Button: {
            model.set("isSimpleControl", true);
            model.set("isDataSourceControl", false);
            model.set("isBPFControl", false);
            model.set("isButtonControl", true);
            break;
        }
        default: {
            model.set("isSimpleControl", false);
            model.set("isDataSourceControl", false);
            model.set("isBPFControl", false);
            model.set("isButtonControl", false);
        }
    }
}

export default setControlsVisibility;