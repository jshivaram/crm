import ButtonException from './buttonException'
import config from './buttonConfig'

export default function (entityName, fieldName) {
    let currentEntityObject = config[entityName];

    /* Exceptions block */
    if (!currentEntityObject) {
        throw new ButtonException(`Current entity "${entityName}" is missing in config`);
    }

    let fieldAvailableMethods = currentEntityObject[fieldName];

    if (!fieldAvailableMethods || fieldAvailableMethods.length === 0) {
        throw new ButtonException(`Current entity "${entityName}" and ${fieldName} is missing in config.`);
    }
    /* -------------- */

    let availableMethodsModel = fieldAvailableMethods.map(function(item){
        return {
            displayName: item.label
        }
    });

    return availableMethodsModel;
}