const path = require('path');

module.exports = {
    devtool: 'source-map',
    entry: {
        ribbon: './adt_ribbon/main.js',
        main: './adt_settings/main.js'
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            }
        ]
    }
};