const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: {
        ribbon: './adt_ribbon/main.js',
        main: './adt_settings/main.js'
    },
    output: {
        path: path.resolve(__dirname, '../Solution/WebResources/adatatypes_/js'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            }
        ]
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        }),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            mangle: {
                screw_ie8: true,
                keep_fnames: true
            },
            compress: {
                screw_ie8: true
            },
            comments: false
        })
    ]
};