﻿using System;
using System.Activities;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Moq;

namespace CRMWorkflowUnitTestProject.Tests1
{
    [TestClass]
    public class UnitTest1
    {
        #region Class Constructor
        private  string _namespaceClassAssembly;

        public UnitTest1()
        {
            //[Namespace.class name, assembly name] for the class/assembly being tested
            //Namespace and class name can be found on the class file being tested
            //Assembly name can be found under the project properties on the Application tab
            //_namespaceClassAssembly = "RecalculateMeasure" + ", " + "DDSM.SmartMeasureCalculationPlugin"; //DDSM.Helper.Class.
            // _namespaceClassAssembly = "GroupCalculateMeasure" + ", " + "DDSM.SmartMeasureCalculationPlugin"; //DDSM.Helper.Class.
             _namespaceClassAssembly = "GetAllMadInfo" + ", " + "DDSM.SmartMeasureCalculationPlugin"; //DDSM.Helper.Class.
            
            //"DDSM.Helper.Class.CreateProject"
        }
        #endregion
        #region Test Initialization and Cleanup
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext) { }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void ClassCleanup() { }

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void TestMethodInitialize() { }

        // Use TestCleanup to run code after each test has run
        [TestCleanup]
        public void TestMethodCleanup() { }
        #endregion


        [TestMethod]
        public void TestMethodGenerateEntityRecord()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "GenerateEntityRecord" + ", " + "DDSM.Aplication";

            //Target            
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_application", Id = Guid.Parse("E58673E9-1B9D-E611-80E0-366637386334") };
            //Input parameters
            var inputs = new Dictionary<string, object>
            {
             //  { "ForceUpdate", false},
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        /// <summary>
        /// Modify to mock CRM Organization Service actions
        /// </summary>
        /// <param name="serviceMock">The Organization Service to mock</param>
        /// <returns>Configured Organization Service</returns>
        private static Mock<IOrganizationService> TestMethod1Setup(Mock<IOrganizationService> serviceMock)
        {
            EntityCollection queryResult = new EntityCollection();
            ////Add created items to EntityCollection

            serviceMock.Setup(t =>
                t.RetrieveMultiple(It.IsAny<QueryExpression>()))
                .ReturnsInOrder(queryResult);

            return serviceMock;
        }

        /// <summary>
        /// Invokes the workflow.
        /// </summary>
        /// <param name="name">Namespace.Class, Assembly</param>
        /// <param name="target">The target entity</param>
        /// <param name="inputs">The workflow input parameters</param>
        /// <param name="configuredServiceMock">The function to configure the Organization Service</param>
        /// <returns>The workflow output parameters</returns>
        private static IDictionary<string, object> InvokeWorkflow(string name, ref EntityReference target, Dictionary<string, object> inputs,
            Func<Mock<IOrganizationService>, Mock<IOrganizationService>> configuredServiceMock)
        {
            var testClass = Activator.CreateInstance(Type.GetType(name)) as CodeActivity;

            var serviceMock = new Mock<IOrganizationService>();
            var factoryMock = new Mock<IOrganizationServiceFactory>();
            var tracingServiceMock = new Mock<ITracingService>();
            var workflowContextMock = new Mock<IWorkflowContext>();

            //Apply configured Organization Service Mock
            if (configuredServiceMock != null)
                serviceMock = configuredServiceMock(serviceMock);

            CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS090916; Username=Administrator; Password=c#102612;");
            //CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS090916; Username=Administrator; Password=Rjnecz121969!;");
            //  CrmConnection connection = CrmConnection.Parse("Url=https://egddemo1.crm.dynamics.com/; ; Username=admin@EGDdemo1.onmicrosoft.com; Password=P@ssword1; Timeout=00:10:00;");
            // CrmConnection connection = CrmConnection.Parse("Url=http://e1-dev1.dynamicdsm.com/RLS/; Domain=e1-dev1.dynamicdsm.com; Username=dev2; Password=Development2pass!; Timeout=00:10:00;");
            IOrganizationService service = new OrganizationService(connection);
          //  IOrganizationService service = serviceMock.Object;


            //Mock workflow Context
            var workflowUserId = Guid.NewGuid();
            var workflowCorrelationId = Guid.NewGuid();
            var workflowInitiatingUserId = Guid.NewGuid();

            //Workflow Context Mock
            workflowContextMock.Setup(t => t.InitiatingUserId).Returns(workflowInitiatingUserId);
            workflowContextMock.Setup(t => t.CorrelationId).Returns(workflowCorrelationId);
            workflowContextMock.Setup(t => t.UserId).Returns(workflowUserId);
            var workflowContext = workflowContextMock.Object;

            //Organization Service Factory Mock
            factoryMock.Setup(t => t.CreateOrganizationService(It.IsAny<Guid>())).Returns(service);
            var factory = factoryMock.Object;

            //Tracing Service - Content written appears in output
            tracingServiceMock.Setup(t => t.Trace(It.IsAny<string>(), It.IsAny<object[]>())).Callback<string, object[]>(MoqExtensions.WriteTrace);
            var tracingService = tracingServiceMock.Object;

            //Parameter Collection
            ParameterCollection inputParameters = new ParameterCollection { { "Target", target } };
            workflowContextMock.Setup(t => t.InputParameters).Returns(inputParameters);

            //Workflow Invoker
            var invoker = new WorkflowInvoker(testClass);
            invoker.Extensions.Add(() => tracingService);
            invoker.Extensions.Add(() => workflowContext);
            invoker.Extensions.Add(() => factory);

            return invoker.Invoke(inputs);
        }
    }
}
