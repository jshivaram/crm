﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.Aplication.Model
{
    public class MetadataModel
    {
        public string LogicalName { get; set; }
        public IEnumerable<string> Attributes { get; set; }
    }
}
