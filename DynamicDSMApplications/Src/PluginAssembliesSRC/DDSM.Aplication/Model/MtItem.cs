﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.Aplication.Model
{
   public class MtItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Qty { get; set; }
        public bool Deleted { get; set; }
    }
}
