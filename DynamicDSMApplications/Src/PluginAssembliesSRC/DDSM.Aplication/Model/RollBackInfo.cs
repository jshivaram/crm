﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.Aplication.Model
{
    public class RollBackInfo
    {
        public string LogicalName { get; set; }
        public Guid Guid { get; set; }
    }
}
