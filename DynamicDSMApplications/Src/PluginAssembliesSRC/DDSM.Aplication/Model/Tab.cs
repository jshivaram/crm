﻿namespace DDSM.Aplication.Model
{
    public class Tab
    {
        public string Label { get; set; }
        public string LogicalName { get; set; }
    }
}
