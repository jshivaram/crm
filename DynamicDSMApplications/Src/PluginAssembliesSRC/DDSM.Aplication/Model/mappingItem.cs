﻿namespace DDSM.Aplication.Model
{
    public class MappingItem
    {
        public string SourceField { get; set; }
        public string SourceFieldType { get; set; }
        public string SourceFieldLogicalName { get; set; }

        public string TargetField { get; set; }
        public string TargetFieldType { get; set; }
        public string TargetFieldLogicalName { get; set; }

    }
}
