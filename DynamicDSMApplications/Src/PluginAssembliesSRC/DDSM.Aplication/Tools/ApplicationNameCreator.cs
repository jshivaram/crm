﻿using System;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDSM.Aplication.Model;
using System.Globalization;

namespace DDSM.Aplication
{
    public class ApplicationNameCreator
    {
        public string CreateEntityShortName(IEnumerable<Tab> tabs)
        {
            var nowDate = DateTime.Now;
            string applicationName = nowDate.ToString("MMM/dd/yyyy", CultureInfo.InvariantCulture) + " ";

            foreach(Tab tab in tabs)
            {
                string entityLogicalName = tab.Label.Replace("ddsm_", "");
                applicationName += entityLogicalName
                            .Substring(0, 1)
                            .ToUpper();
                applicationName += entityLogicalName
                            .Substring(1, 2);   
            }

            return applicationName;
        }
    }
}
