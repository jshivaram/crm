﻿using DDSM.Aplication.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.Aplication.Tools
{
    public class EntityRollBack
    {
        private IOrganizationService _service;
        private ITracingService _tracer;
        public EntityRollBack(IOrganizationService service, ITracingService tracer)
        {
            _tracer = tracer;
            _service = service;
        }

        public IEnumerable<RollBackInfo> GetSuccessfullyCreatedRecords(ExecuteMultipleRequest executeRequest, ExecuteMultipleResponse response)
        {

            _tracer.Trace("Was found error when creating");

            List<RollBackInfo> rollBackInfoList = new List<RollBackInfo>();

            var requests = executeRequest.Requests;
            var responses = ((ExecuteMultipleResponseItemCollection)response.Results["Responses"]);

            for (int i = 0, length = requests.Count(), responseCount = responses.Count(); i < length; i++)
            {
                var currentRequest = requests[i] as CreateRequest;
                if (responseCount <= i)
                {
                    break;
                }
                var currentResponse = responses[i];
                if (currentResponse.Response == null)
                {
                    _tracer.Trace($"Error:{currentResponse.Fault.Message}");
                    continue;
                }
                string responseName = currentResponse.Response.ResponseName;

                if (currentRequest != null && responseName == "Create")
                {
                    string entityLogicalName = currentRequest.Target.LogicalName;
                    var createdGuid = (Guid?)currentResponse.Response["id"];
                    if (createdGuid != null)
                    {
                        rollBackInfoList.Add(new RollBackInfo()
                        {
                            LogicalName = entityLogicalName,
                            Guid = (Guid)createdGuid
                        });
                    }

                }
                else if (currentRequest == null && responseName != "Create")
                {
                    continue;
                }
                else
                {
                    throw new Exception("There are messed array in response and array");
                }

            }
            return rollBackInfoList;

        }

        public bool CheckAllRequestIsValid(ExecuteMultipleResponse response)
        {
            var requestIsFaulted = (bool)response.Results["IsFaulted"];
            if (requestIsFaulted)
            {
                _tracer.Trace("Was found error when creating");
            }
            return requestIsFaulted;

        }

        public void DeleteCreatedEntityRecords(IEnumerable<RollBackInfo> rollBacks)
        {
            if (rollBacks == null || rollBacks.Count() == 0)
            {
                _tracer.Trace("Create entity records with mapping was failed, but there are no successfully created entities");
            }
            else
            {
                _tracer.Trace("Beggining deleting records");

                var emRequest = new ExecuteMultipleRequest()
                {
                    // Assign settings that define execution behavior: continue on error, return responses. 
                    Settings = new ExecuteMultipleSettings()
                    {
                        ContinueOnError = false,
                        ReturnResponses = true
                    },
                    // Create an empty organization request collection.
                    Requests = new OrganizationRequestCollection()
                };

                foreach (RollBackInfo r in rollBacks)
                {
                    var entityReference = new EntityReference(r.LogicalName, r.Guid);
                    var deleteRequest = new DeleteRequest();
                    deleteRequest.Target = entityReference;

                    emRequest.Requests.Add(deleteRequest);
                }

                var response = (ExecuteMultipleResponse)_service.Execute(emRequest);

                if (response.IsFaulted)
                {
                    _tracer.Trace("Deleting was ended with fail.");
                }
                else
                {
                    _tracer.Trace("Deleting was successfully ended.");
                }

            }


        }
    }

}
