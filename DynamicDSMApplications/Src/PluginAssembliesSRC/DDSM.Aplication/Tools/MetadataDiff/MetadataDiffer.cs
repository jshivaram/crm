﻿using DDSM.Aplication.Model;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.Aplication.Tools.MetadataDiff
{
    class MetadataDiffer
    {
        public bool CheckValidFields(IEnumerable<MappingItem> mapData, MetadataModel metadataModel, ITracingService tracer)
        {
            var filteredFromEmptyFields  = mapData?
                    .Where(x =>
                            !string.IsNullOrEmpty(x.TargetField) &&
                            !string.IsNullOrEmpty(x.TargetFieldLogicalName) &&
                            !string.IsNullOrEmpty(x.TargetFieldType))
                    .ToList();

            bool isValid = true;

            foreach(MappingItem item in filteredFromEmptyFields)
            {
                var element = metadataModel.Attributes.FirstOrDefault(a => a == item.TargetFieldLogicalName.ToLower());

                if(element == null)
                {
                    string message = $"Entity {metadataModel.LogicalName} does not contain {item.TargetFieldLogicalName.ToLower()}";
                    tracer.Trace(message);
                    isValid = false;
                }
            }

            return isValid;
        }
    }
}
