﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.Aplication.Tools.MetadataDiff
{
    public class MetadataGetter
    {
        private IOrganizationService _service;
        private ExecuteMultipleRequest _executeRequest;
        private ITracingService _tracer;

        public MetadataGetter(IOrganizationService service, ITracingService tracer)
        {
            _service = service;
            _executeRequest = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };
        }

        public OrganizationResponse GetMetadata(IEnumerable<string> entityLogicalNamesList)
        {
            var requests = entityLogicalNamesList.Select(e => _CreateEntityMetadataRequest(e));
            _executeRequest.Requests = new OrganizationRequestCollection();
            _executeRequest.Requests.AddRange(requests);

            var data = _service.Execute(_executeRequest);
            return data;
        }

        private RetrieveEntityRequest _CreateEntityMetadataRequest(string entityLogicalName)
        {
            RetrieveEntityRequest metaDataRequest = new RetrieveEntityRequest();
            metaDataRequest.EntityFilters = EntityFilters.Attributes;
            metaDataRequest.LogicalName = entityLogicalName.ToLower();

            return metaDataRequest;
        }




    }
}
