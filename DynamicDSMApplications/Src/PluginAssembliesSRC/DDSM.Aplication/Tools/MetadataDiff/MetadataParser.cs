﻿using DDSM.Aplication.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.Aplication.Tools.MetadataDiff
{
    public class MetadataParser
    {
        public IEnumerable<MetadataModel> ConvertResponseToModel(ExecuteMultipleResponse response)
        {
            List<MetadataModel> metadataModelList = new List<MetadataModel>();

            foreach (var item in response.Responses)
            {
                var retrieveResponse = (RetrieveEntityResponse)item.Response;
                var metadata = _GetAttributesLogicalNameList(retrieveResponse);

                metadataModelList.Add(metadata);
            }

            return metadataModelList;
        }

        private MetadataModel _GetAttributesLogicalNameList(RetrieveEntityResponse response)
        {
            string entityLogicalName = response.EntityMetadata.LogicalName;
            IEnumerable<string> attributes = response.EntityMetadata.Attributes.Select(a => a.LogicalName)
                                                                            .ToList();

            var metadataModel = new MetadataModel()
            {
                LogicalName = entityLogicalName,
                Attributes = attributes
            };

            return metadataModel;
        }
    }
}
