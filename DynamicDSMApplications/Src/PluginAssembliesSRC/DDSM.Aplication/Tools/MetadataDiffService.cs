﻿using DDSM.Aplication.Model;
using DDSM.Aplication.Tools.MetadataDiff;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.Aplication.Tools
{
    public class MetadataDiffService
    {
        public void CheckTrueMappingData(IOrganizationService service, ITracingService tracer, 
                                        IEnumerable<Tab> entitiesForCreate,
                                        Dictionary<string, List<MappingItem>>mapData)
        {
            var metadataResponse = (ExecuteMultipleResponse)(new MetadataGetter(service, tracer).GetMetadata(entitiesForCreate.Select(t => t.LogicalName.ToLower())));
            var parsedData = new MetadataParser().ConvertResponseToModel(metadataResponse);
            bool isValid = true;
            foreach (var element in mapData)
            {
                string mappedEntity = element.Key;
                MetadataModel model = parsedData.First(m => m.LogicalName == mappedEntity);
                var metadataDiffer = new MetadataDiffer();
                bool currentMappingIsValid = metadataDiffer.CheckValidFields(element.Value, model, tracer);
                isValid &= currentMappingIsValid;
            }

            if (!isValid)
            {
                string message = "Mapping didn't create entities, because mapping data has fields that are absent";
                tracer.Trace(message);
                throw new Exception(message);
            }
        }
    }
}
