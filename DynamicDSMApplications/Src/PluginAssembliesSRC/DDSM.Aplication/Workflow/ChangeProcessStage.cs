﻿using System;
using System.Activities;
using System.Linq;
using CrmEarlyBound;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Workflow;


public class ChangeProcessStage : CodeActivity
{
    [RequiredArgument]
    [Input("Process Name")]
    public InArgument<string> Process { get; set; }

    [RequiredArgument]
    [Input("Process Stage Name")]
    public InArgument<string> ProcessStage { get; set; }

    private IOrganizationService _service;

    protected override void Execute(CodeActivityContext executionContext)
    {
        var tracer = executionContext.GetExtension<ITracingService>();
        var context = executionContext.GetExtension<IWorkflowContext>();
        var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        _service = serviceFactory.CreateOrganizationService(context.UserId);

        try
        {
            using (var ctx = new OrganizationServiceContext(_service))
            {
                // Get the processid using the name provided
                var process = (from p in ctx.CreateQuery<CrmEarlyBound.Workflow>()
                               where
                               p.Name == Process.Get<string>(executionContext)
                               &&
                               p.StateCode == WorkflowState.Activated
                               select new CrmEarlyBound.Workflow
                               {
                                   WorkflowId = p.WorkflowId

                               }).FirstOrDefault();
                if (process == null)
                    throw new InvalidPluginExecutionException(
                        $"Process '{Process.Get<string>(executionContext)}' not found");

                // Get the stage id using the name provided
                var stage = (from s in ctx.CreateQuery<ProcessStage>()
                             where
                             s.StageName == ProcessStage.Get<string>(executionContext)
                             &&
                             s.ProcessId.Id == process.WorkflowId
                             select new ProcessStage
                             {
                                 ProcessStageId = s.ProcessStageId

                             }).FirstOrDefault();
                if (stage == null)
                    throw new InvalidPluginExecutionException(
                        $"Stage '{ProcessStage.Get<string>(executionContext)}' not found");

                // Change the stage
                var updatedStage = new Entity(context.PrimaryEntityName)
                {
                    Id = context.PrimaryEntityId,
                    ["stageid"] = stage.ProcessStageId,
                    ["processid"] = process.WorkflowId
                };
                _service.Update(updatedStage);
            }
        }
        catch (Exception ex)
        {
            tracer.Trace("Exception: {0}", ex.Message);

            // Handle the exception.
            throw;
        }

    }
}

