﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using DDSM.Aplication.Model;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;

public class GenerateEntityRecord : CodeActivity
{
    [Output("Result")]
    public OutArgument<string> Result { get; set; }

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }


    private IOrganizationService _service;
    private ITracingService tracer;

    protected override void Execute(CodeActivityContext executionContext)
    {
        executionContext.GetExtension<ITracingService>();
        var context = executionContext.GetExtension<IWorkflowContext>();
        var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        _service = serviceFactory.CreateOrganizationService(context.UserId);
        tracer = executionContext.GetExtension<ITracingService>();
        try
        {
            var entityTargetRef = GetTarget(context.InputParameters["Target"]);
            //get target record data
            var targetRecordData = _service.Retrieve(entityTargetRef.LogicalName, entityTargetRef.Id,
                new ColumnSet(true));

            var emRequest = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            object value;

            //get link for Template
            if (targetRecordData.Attributes.TryGetValue("ddsm_applicationfieldsmappingconfigid", out value))
            {
                var appConfRef = (EntityReference)value;

                var appTemplate = _service.Retrieve(appConfRef.LogicalName, appConfRef.Id,
                    new ColumnSet("ddsm_entities", "ddsm_mappingdata"));
                //get list of entities
                if (appTemplate.Attributes.TryGetValue("ddsm_entities", out value))
                {
                    var entitiesJson = (string)value;

                    //get mapping data
                    if (appTemplate.Attributes.TryGetValue("ddsm_mappingdata", out value))
                    {
                        var mapDataJson = (string)value;

                        var entitiesForCreate = JsonConvert.DeserializeObject<List<Tab>>(entitiesJson);
                        var mapData = JsonConvert.DeserializeObject<Dictionary<string, List<MappingItem>>>(mapDataJson);
                        tracer.Trace("before FillRequest");
                        FillRequest(emRequest, entitiesForCreate, mapData, targetRecordData);
                        tracer.Trace("after FillRequest/ Request count: " + emRequest.Requests.Count);
                        try
                        {
                            foreach (var entInstance in emRequest.Requests)
                            {
                                _service.Create(((CreateRequest)entInstance).Target);
                            }
                            tracer.Trace("before FillRequest");
                        }
                        catch (Exception ex)
                        {
                            tracer.Trace("in exception on creating :" + ex.Message + ex.Source);
                            throw;
                        }
                        //if (emRequest.Requests.Count > 0)
                        //{
                        //    var responseWithResults = (ExecuteMultipleResponse)_service.Execute(emRequest);
                        //    if (!responseWithResults.IsFaulted)
                        //    {

                        //        Result.Set(executionContext, "{Result: 'Done!'}");
                        //        Complete.Set(executionContext, true);
                        //    }
                        //    else
                        //    {
                        //        tracer.Trace("in responseWithResults is Fault. count of response:  " +responseWithResults.Responses.Count);
                        //        var errors = new List<string>();

                        //        foreach (var responseItem in responseWithResults.Responses)
                        //        {
                        //            if (responseItem.Fault != null)
                        //                errors.Add("Error on record creation: " + responseItem.Fault.TraceText + responseItem.Fault.ErrorCode +
                        //                           responseItem.Fault.Message);
                        //        }
                        //        throw new Exception(string.Join(Environment.NewLine, errors));
                        //    }
                        //}

                        Result.Set(executionContext, "{Result: 'Done!'}");
                        Complete.Set(executionContext, true);

                    }
                    else
                    {
                        throw new Exception("Fatal ERROR: Mapping data is Empty. Check Application Config.");
                    }
                }
                else
                {
                    throw new Exception("Fatal ERROR: No one entity to Creation. Check Application Config.");
                }
            }
            else
            {
                throw new Exception("Fatal ERROR: Application Record does'n contains link to Parent Application config");
            }
        }
        catch (Exception e)
        {
            tracer.Trace("in catch");
            Complete.Set(executionContext, false);
            Result.Set(executionContext, e.Message);
        }
    }

    private EntityReference GetTarget(object contextInputParameter)
    {
        var entity = contextInputParameter as Entity;
        if (entity != null)
            return entity.ToEntityReference();
        else
        {
            return (EntityReference)contextInputParameter;
        }
    }

    private void FillRequest(ExecuteMultipleRequest emRequest, List<Tab> entitiesForCreate,
        IReadOnlyDictionary<string, List<MappingItem>> mapData, Entity targetRecordData)
    {
        var instances = new List<Entity>();
        foreach (var tab in entitiesForCreate)
        {
            List<MappingItem> entityMappingData;
            if (mapData.TryGetValue(tab.LogicalName, out entityMappingData))
            {
                instances.AddRange(GenerateNewEntity(entityMappingData, tab, targetRecordData));
            }
        }
        tracer.Trace("instances count: " + instances.Count);
        foreach (var inst in instances)
        {
            if (inst.LogicalName == "ddsm_project" &&
                entitiesForCreate.FirstOrDefault(x => x.LogicalName == "ddsm_measure") != null)
            {
                var measRelationship = new Relationship("ddsm_ddsm_project_ddsm_measure");
                var measureList = instances.Where(x => x.LogicalName == "ddsm_measure");
                tracer.Trace("measures count: " + measureList.ToList().Count);
                var dd = new EntityCollection();
                dd.Entities.AddRange(measureList);
                inst.RelatedEntities.Add(measRelationship, dd);
                emRequest.Requests.Add(new CreateRequest
                {
                    Target = inst
                });
            }
            else if (inst.LogicalName != "ddsm_measure")
            {
                emRequest.Requests.Add(new CreateRequest
                {
                    Target = inst
                });
            }
        }
    }

    IEnumerable<Entity> GenerateNewEntity(List<MappingItem> fields, Tab tab, Entity targetRecordData)
    {
        var newInstances = new List<Entity>();
        var newInstance = new Entity(tab.LogicalName.ToLower());
        switch (tab.LogicalName)
        {
            case "ddsm_measure":
                object mtDataJson;
                if (targetRecordData.Attributes.TryGetValue("ddsm_projecttemplateid", out mtDataJson))
                {
                    var mtDataRef = (EntityReference)mtDataJson;

                    var measureTplList = GetMTList(mtDataRef,_service);
                    foreach (var mt in measureTplList.Entities)
                    {
                       // if (!mt.Deleted)
                        object mtQtyValue;
                        object mtRef;
                        if (mt.Attributes.TryGetValue("ddsm_units", out mtQtyValue) &&
                            mt.Attributes.TryGetValue("ddsm_measuretemplateid", out mtRef))
                        {
                            for (var i = 0; i < (decimal)mtQtyValue; i++)
                            {
                                //init from tpl
                                newInstance = GetDataRelationMapping(tab.LogicalName, "ddsm_measuretemplate", (mtRef as EntityReference).Id,
                                    _service);
                                try
                                {
                                    FillEntity(fields, targetRecordData, newInstance);
                                    // set parent record lookup 
                                    newInstance["ddsm_applicationid"] = new EntityReference(targetRecordData.LogicalName,
                                        targetRecordData.Id);
                                }
                                catch (Exception ex)
                                {
                                    // ignored
                                }
                                newInstances.Add(newInstance);
                            }
                        }
                           
                    }
                }
                #region oldImpl
                //object mtDataJson;
                //if (targetRecordData.Attributes.TryGetValue("ddsm_measuretemplatedata", out mtDataJson))
                //{
                //    // Dict<Guid of MT,qty>
                //    var mtData = JsonConvert.DeserializeObject<List<MtItem>>((string)mtDataJson);

                //    foreach (var mt in mtData)
                //    {
                //        if (!mt.Deleted)
                //            for (var i = 0; i < mt.Qty; i++)
                //            {
                //                //init from tpl
                //                newInstance = GetDataRelationMapping(tab.LogicalName, "ddsm_measuretemplate", mt.Id,
                //                    _service);
                //                try
                //                {
                //                    FillEntity(fields, targetRecordData, newInstance);
                //                    // set parent record lookup 
                //                    newInstance["ddsm_applicationid"] = new EntityReference(targetRecordData.LogicalName,
                //                        targetRecordData.Id);
                //                }
                //                catch (Exception ex)
                //                {
                //                    // ignored
                //                }
                //                newInstances.Add(newInstance);
                //            }
                //    }
                //}
                #endregion
                break;
            case "ddsm_project":
                object value;
                if (targetRecordData.Attributes.TryGetValue("ddsm_projecttemplateid", out value))
                {
                    newInstance = GetDataRelationMapping(tab.LogicalName, "ddsm_projecttemplate", ((EntityReference)value).Id, _service);
                }

                if (targetRecordData.Attributes.TryGetValue("ddsm_parentacocuntid", out value))
                {
                    var fff = GetDataRelationMapping(tab.LogicalName, "account", ((EntityReference)value).Id, _service);
                    foreach (var attr in fff.Attributes)
                    {
                        newInstance[attr.Key] = attr.Value;
                    }
                }

                if (targetRecordData.Attributes.TryGetValue("ddsm_parentsiteid", out value))
                {
                    var fff = GetDataRelationMapping(tab.LogicalName, "ddsm_site", ((EntityReference)value).Id, _service);
                    foreach (var attr in fff.Attributes)
                    {
                        newInstance[attr.Key] = attr.Value;
                    }
                }

                FillEntity(fields, targetRecordData, newInstance);
                // set parent record lookup 
                newInstance["ddsm_applicationid"] = new EntityReference(targetRecordData.LogicalName, targetRecordData.Id);
                newInstances.Add(newInstance);
                break;
            default:
                FillEntity(fields, targetRecordData, newInstance);
                // set parent record lookup 
                newInstance["ddsm_applicationid"] = new EntityReference(targetRecordData.LogicalName, targetRecordData.Id);
                newInstances.Add(newInstance);
                break;
        }


        return newInstances;
    }

    private EntityCollection GetMTList(EntityReference mtDataRef, IOrganizationService service)
    {
        var result = new EntityCollection();
        var query = new QueryExpression
        {
            EntityName = "ddsm_measuretemplaterelation",
            ColumnSet = new ColumnSet("ddsm_units", "ddsm_measuretemplateid"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions =
                {
                    new ConditionExpression("ddsm_projecttemplateid", ConditionOperator.Equal, mtDataRef.Id)
                }
            }
        };

        result = service.RetrieveMultiple(query);
        return result;
    }

    private void FillEntity(IEnumerable<MappingItem> fields, Entity targetRecordData, Entity newInstance)
    {
        foreach (var mapInfo in fields)
        {
            object fieldValue;
            if (!string.IsNullOrEmpty(mapInfo.TargetFieldLogicalName) && !string.IsNullOrEmpty(mapInfo.SourceFieldLogicalName) && targetRecordData.Attributes.TryGetValue(mapInfo.SourceFieldLogicalName, out fieldValue))
            {
                newInstance[mapInfo.TargetFieldLogicalName] = fieldValue;
            }
        }
    }

    public Entity GetDataRelationMapping(string targetEntity, string sourceEntity, Guid sourceGuid, IOrganizationService service)
    {
        var initialize = new InitializeFromRequest
        {
            TargetEntityName = targetEntity,
            EntityMoniker = new EntityReference(sourceEntity, sourceGuid),
            TargetFieldType = TargetFieldType.All
        };
        var initialized = (InitializeFromResponse)service.Execute(initialize);
        return initialized.Entity;
    }

}
