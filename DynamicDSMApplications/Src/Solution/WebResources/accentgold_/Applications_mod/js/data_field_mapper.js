dataRowsArrayMapper = (function () {

    var setRowData = function (row, target) {
        var swapData = function (row, target) {
            row.TargetField = target.TargetField;
            row.TargetFieldLogicalName = target.TargetFieldLogicalName;
            row.TargetFieldType = target.TargetFieldType;
        }

        var sourceFieldName = row.SourceField;
        var targetFieldName = target.TargetField;

        if (sourceFieldName === targetFieldName) {
            swapData(row, target);
        }
    }

    var forEachTargets = function (row, targetArray) {
        targetArray.forEach(target => {
            setRowData(row, target);
        });
    }

    var mapAllRows = function (rowsArray, targetsArray) {
        rowsArray.forEach(row => {
            forEachTargets(row, targetsArray);
        });
    }

    return {
        mapAllRows
    }
    
})();