dataConverter = (function () {
    let filterApplicationappItem = function (appItem) {
        let field = {
            SourceField: appItem.AttributeType === "Lookup" ? appItem.DisplayName.LocalizedLabels[0].Label + " (" + appItem.Targets[0] + ")" : appItem.DisplayName.LocalizedLabels[0].Label,
            SourceFieldType: appItem.AttributeType,
            SourceFieldLogicalName: appItem.LogicalName,
            TargetField: null,
            TargetFieldType: null,
            TargetFieldLogicalName: null
        };

        return field;
    };
    let getApplicationDataArray = function (array) {
    //debugger;
        let resultArray = [];

        array.forEach(item => {
            let field = filterApplicationappItem(item);
            resultArray.push(field);
        });

        return resultArray;
    };

    let filterTargetItem = function (targetItem) {
        let field = {
            TargetField: targetItem.AttributeType === "Lookup" ? targetItem.DisplayName.LocalizedLabels[0].Label + " (" + targetItem.Targets[0] + ")" : targetItem.DisplayName.LocalizedLabels[0].Label,
            TargetFieldType: targetItem.AttributeType,
            TargetFieldLogicalName: targetItem.LogicalName
        };
        return field;
    };
    let filterTargetDataArray = function (array) {
   // debugger;
        let resultArray = [];

        array.forEach(item => {
            let field = filterTargetItem(item);
            resultArray.push(field);
        });

        return resultArray;
    };



    return {
        getApplicationDataArray,
        filterTargetDataArray
    }
}
)();