﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Moq;
using System;
using System.Activities;
using System.Collections.Generic;


namespace CRMWorkflowUnitTestProject.Tests1
{
    [TestClass]
    public class UnitTest1
    {
        #region Class Constructor
        private string _namespaceClassAssembly;
        public UnitTest1()
        {
            //[Namespace.class name, assembly name] for the class/assembly being tested
            //Namespace and class name can be found on the class file being tested
            //Assembly name can be found under the project properties on the Application tab
            _namespaceClassAssembly = "CertifiedRefBook.EnergyStar" + ", " + "CertifiedRefBook";
        }
        #endregion
        #region Test Initialization and Cleanup
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void ClassInitialize(TestContext testContext) { }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup()]
        public static void ClassCleanup() { }

        // Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void TestMethodInitialize() { }

        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void TestMethodCleanup() { }
        #endregion

        [TestMethod]
        public void TestGetDLCMetadata()
        {
            _namespaceClassAssembly = "GetDLCMetadata" + ", " + "CertifiedRefBook";
            //Target
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_certifyingorganization", Id = new Guid("{EADBCB2D-1AF5-E611-80FE-366637386334}") };

            //Input parameters
            var inputs = new Dictionary<string, object>();
            

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethodAssociateCertLibToModelNumber()
        {
            _namespaceClassAssembly = "CertifiedRefBook.AssociateCertLibToModelNumber" + ", " + "CertifiedRefBook";
            //Target
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_certlib", Id = new Guid("{F011B55B-A6ED-E611-811F-666665393634}") };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                  { "ModelName", "20 L 100 A-GCL" },
                  { "MappingRef", new EntityReference("ddsm_mapping",Guid.Parse("14F5CD6D-8FED-E611-811E-666665393634"))},
                // { "LastUpdateDateLoad", "2016-03-04" },
                //  { "TypeCategory", "[{\"Audio Video\":true},{\"Boilers\":true}]" }
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethod1()
        {
            //Target
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_certlibsync", Id = new Guid("A44E33EC-F3E3-E611-80FB-366637386334") };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                //  { "TypeLoad", "" },
                // { "LastUpdateDateLoad", "2016-03-04" },
                //  { "TypeCategory", "[{\"Audio Video\":true},{\"Boilers\":true}]" }
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestGetMetadata()
        {
            _namespaceClassAssembly = "CertifiedRefBook.GetMetadata" + ", " + "CertifiedRefBook";
            //Target
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_certlibsync", Id = new Guid("{628E02B8-E1B7-E611-80DC-3A3232303937}") };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                //  { "TypeLoad", "" },
                // { "LastUpdateDateLoad", "2016-03-04" },
                //  { "TypeCategory", "[{\"Audio Video\":true},{\"Boilers\":true}]" }
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        /// <summary>
        /// Modify to mock CRM Organization Service actions
        /// </summary>
        /// <param name="serviceMock">The Organization Service to mock</param>
        /// <returns>Configured Organization Service</returns>
        private static Mock<IOrganizationService> TestMethod1Setup(Mock<IOrganizationService> serviceMock)
        {
            //EntityCollection queryResult = new EntityCollection();
            ////Add created items to EntityCollection

            //serviceMock.Setup(t =>
            //    t.RetrieveMultiple(It.IsAny<QueryExpression>()))
            //    .ReturnsInOrder(queryResult);

            return serviceMock;
        }

        /// <summary>
        /// Invokes the workflow.
        /// </summary>
        /// <param name="name">Namespace.Class, Assembly</param>
        /// <param name="target">The target entity</param>
        /// <param name="inputs">The workflow input parameters</param>
        /// <param name="configuredServiceMock">The function to configure the Organization Service</param>
        /// <returns>The workflow output parameters</returns>
        private static IDictionary<string, object> InvokeWorkflow(string name, ref EntityReference target, Dictionary<string, object> inputs,
            Func<Mock<IOrganizationService>, Mock<IOrganizationService>> configuredServiceMock)
        {
            var testClass = Activator.CreateInstance(Type.GetType(name)) as CodeActivity;

            var serviceMock = new Mock<IOrganizationService>();
            var factoryMock = new Mock<IOrganizationServiceFactory>();
            var tracingServiceMock = new Mock<ITracingService>();
            var workflowContextMock = new Mock<IWorkflowContext>();

            //Apply configured Organization Service Mock
            if (configuredServiceMock != null)
                serviceMock = configuredServiceMock(serviceMock);
            // CrmConnection connection = CrmConnection.Parse("Url=http://ddsm-build-e1.dynamicdsm.com/RLS; Username=yuriyn; Password=AgAJfH6T4p");

            // CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.180/CRMDEV/; Username=Administrator; Password=RLSRecruitment2016;");
            CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS/; Username=Administrator; Password=Dz1026121;");
            connection.Timeout = new TimeSpan(0, 600, 0);

            IOrganizationService service = new OrganizationService(connection);

            //Mock workflow Context
            var workflowUserId = Guid.NewGuid();
            var workflowCorrelationId = Guid.NewGuid();
            var workflowInitiatingUserId = Guid.NewGuid();

            //Workflow Context Mock
            workflowContextMock.Setup(t => t.InitiatingUserId).Returns(workflowInitiatingUserId);
            workflowContextMock.Setup(t => t.CorrelationId).Returns(workflowCorrelationId);
            workflowContextMock.Setup(t => t.UserId).Returns(workflowUserId);
            var workflowContext = workflowContextMock.Object;

            //Organization Service Factory Mock
            factoryMock.Setup(t => t.CreateOrganizationService(It.IsAny<Guid>())).Returns(service);
            var factory = factoryMock.Object;

            //Tracing Service - Content written appears in output
            tracingServiceMock.Setup(t => t.Trace(It.IsAny<string>(), It.IsAny<object[]>())).Callback<string, object[]>(MoqExtensions.WriteTrace);
            var tracingService = tracingServiceMock.Object;

            //Parameter Collection
            ParameterCollection inputParameters = new ParameterCollection { { "Target", target } };
            workflowContextMock.Setup(t => t.InputParameters).Returns(inputParameters);

            //Workflow Invoker
            var invoker = new WorkflowInvoker(testClass);
            invoker.Extensions.Add(() => tracingService);
            invoker.Extensions.Add(() => workflowContext);
            invoker.Extensions.Add(() => factory);

            return invoker.Invoke(inputs);
        }
    }
}
