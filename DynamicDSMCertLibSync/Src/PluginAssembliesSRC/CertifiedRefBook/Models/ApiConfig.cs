﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CertifiedRefBook.Models
{
   public class ApiConfig
    {
        public Guid Id { get; set; }
        //remote calculation
        public bool RemoteCalculation { get; set; }
        public string RemoteCalculationApiUrl { get; set; }
        public string Url { get; set; }
    }
}
