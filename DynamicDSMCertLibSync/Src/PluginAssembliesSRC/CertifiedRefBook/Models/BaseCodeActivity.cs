﻿using System;
using System.Activities;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;

namespace CertifiedRefBook.Models
{
    public class BaseCodeActivity : CodeActivity
    {

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }
        [Output("Result")]
        public OutArgument<string> Result { get; set; }

        protected virtual string RemoteMethod { get; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            ExecuteAction(executionContext);
        }

        protected virtual void ExecuteAction(CodeActivityContext executionContext)
        {
            throw new NotImplementedException();
        }

        internal void RunRemoteCalc(ApiConfig espConfig, EntityReference target, IOrganizationService service)
        {
            var userRequest = new WhoAmIRequest();
            var whoAmI = (WhoAmIResponse)service.Execute(userRequest);
            var dlcRemoteCalcURL = espConfig.RemoteCalculationApiUrl;
            dlcRemoteCalcURL += dlcRemoteCalcURL.EndsWith("/") ? "" : "/";


            using (var client = new HttpClient {BaseAddress = new Uri(dlcRemoteCalcURL)})
            {
                // specify to use TLS 1.2 as default connection
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls; //use https
                // ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; //ignore invalid cert
                var config = new
                {
                    UserId = whoAmI.UserId,
                    Target = target
                };

                var data = JsonConvert.SerializeObject(config);
                var content = new StringContent(data, Encoding.UTF8, "application/json");
                try
                {
                var ddd=    client.PostAsync(RemoteMethod, content).Result.Content.ReadAsStringAsync();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
          
        }

    }
}
