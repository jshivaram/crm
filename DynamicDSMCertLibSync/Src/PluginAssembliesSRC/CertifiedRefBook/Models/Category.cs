﻿using System;
using System.Collections.Generic;

namespace CertifiedRefBook.Models
{
    public class EsCategory
    {
        private string namePart = "ENERGY STAR Certified ";
        private string namePart2 = "ENERGY STAR ";

        public string Id { get; set; }
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value.Contains(namePart) ? value.Replace(namePart, "") : value;
                _name = _name.Contains(namePart2) ? _name.Replace(namePart2, "") : _name;
            }
        }
        public string Description { get; set; }
        public string Category { get; set; }
    }

    public class Tab
    {
        public string MetadataId { get; set; }
        public string Label { get; set; }
        public Guid LogicalName { get; set; }
    }
    public class Tab2
    {
        public Guid Id { get; set; }
        public string Label { get; set; }
        public string LogicalName { get; set; }
    }


    public class EsCategoryMeatadata
    {
        public Meta Meta { get; set; }
    }

    public class Meta
    {
        public View View { get; set; }
    }

    public class View : EsCategory
    {
        public List<EsField> Columns { get; set; }
    }

    public class EsField
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string DataTypeName { get; set; }
        public string FieldName { get; set; }
        public string RenderTypeName { get; set; }
        // public string Description { get; set; }
    }

}
