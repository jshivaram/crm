﻿using System.Collections.Generic;

namespace CertifiedRefBook.Models.DLC
{
    public class DlcCategoryResponse: DlcResponse
    {
        public List<DlcCategory> pageRecords { get; set; }
    }
}
