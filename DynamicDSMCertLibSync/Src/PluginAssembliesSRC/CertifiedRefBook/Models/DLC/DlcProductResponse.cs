﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace CertifiedRefBook.Models.DLC
{
    public class DlcProductResponse: DlcResponse
    {
        public List<JObject> pageRecords { get; set; }
    }
}
