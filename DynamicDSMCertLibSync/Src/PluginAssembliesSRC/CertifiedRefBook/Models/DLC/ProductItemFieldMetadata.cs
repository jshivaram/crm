﻿namespace CertifiedRefBook.Models.DLC
{
    public class ProductItemFieldMetadata
    {
        public string Type { get; set; }
        public string Label { get; set; }

    }
}