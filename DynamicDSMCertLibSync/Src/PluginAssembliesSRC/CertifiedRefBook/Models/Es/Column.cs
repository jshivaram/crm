using System.Collections.Generic;

namespace CertifiedRefBook.Models.Es
{
    public class Column
    {
        public Column()
        {
            data = new List<int>();
        }

        public string label { get; set; }
        public string name { get; set; }
        public string backgroundColor { get; set; }
        public List<int> data { get; set; }
    }
}