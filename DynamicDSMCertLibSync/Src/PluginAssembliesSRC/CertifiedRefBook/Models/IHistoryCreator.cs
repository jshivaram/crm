using System;
using System.Collections.Generic;
using System.Linq;
using CertifiedRefBook.Models.Es;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;

namespace CertifiedRefBook.Models
{
    public interface IHistoryCreator
    {
        string CreateHistory(ITracingService tracer, IOrganizationService service, EntityReference mainEntityRef,
            DateTime syncDate, IEnumerable<Tab> categoriesList, out int newCount, out int updatedCount,
            out int deletedCount, List<EntityReference> proseccedRefs);

        Helper.History GetNew(IOrganizationService service, DateTime syncDate, EntityReference mainEntityRef, Tab cat, List<EntityReference> proseccedRefs);

        void UpdateStatus(IEnumerable<Entity> newRecords, IEnumerable<EntityReference> updatedRecords,
            IEnumerable<Entity> disabledRecords, IOrganizationService service);
    }

    public class BaseHistoryCreator : IHistoryCreator
    {
        public virtual string CreateHistory(ITracingService tracer, IOrganizationService service, EntityReference mainEntityRef,
            DateTime syncDate, IEnumerable<Tab> categoriesList, out int newCount, out int updatedCount, out int deletedCount, List<EntityReference> proseccedRefs)
        {
            throw new NotImplementedException();
        }

        public virtual Helper.History GetNew(IOrganizationService service, DateTime syncDate, EntityReference mainEntityRef, Tab cat, List<EntityReference> proseccedRefs)
        {
            throw new NotImplementedException();
        }

        public enum StateCode
        {
            Active = 0, Inactive = 1
        }

        public List<UpdateRequest> UpdateMNStatus(IEnumerable<Entity> disabledRecords, IOrganizationService service)
        {
            var result = new List<UpdateRequest>();

            foreach (var recordForDisabling in disabledRecords)
            {
                var record = recordForDisabling;
                string entity1 = "ddsm_modelnumber";
                string entity2 = "ddsm_certlib";

                string relationshipEntityName = "ddsm_ddsm_modelnumber_ddsm_certlib";
                var query = new QueryExpression(entity1) { ColumnSet = new ColumnSet(true) };
                var linkEntity1 = new LinkEntity(entity1, relationshipEntityName, "ddsm_modelnumberid", "ddsm_modelnumberid", JoinOperator.Inner);

                var linkEntity2 = new LinkEntity(relationshipEntityName, entity2, "ddsm_certlibid", "ddsm_certlibid", JoinOperator.Inner);

                linkEntity1.LinkEntities.Add(linkEntity2);
                query.LinkEntities.Add(linkEntity1);

                linkEntity2.LinkCriteria = new FilterExpression()
                {
                    Conditions =
                   {
                       new ConditionExpression("ddsm_certlibid", ConditionOperator.Equal, record.Id),
                       new ConditionExpression("statecode", ConditionOperator.Equal, (int)StateCode.Active)
                   }
                };

                EntityCollection collRecords = service.RetrieveMultiple(query);

                foreach (var mn in collRecords.Entities)
                {
                    var request = new UpdateRequest
                    {
                        Target = new Entity(mn.LogicalName, mn.Id)
                        {
                            Attributes = {
                                new KeyValuePair<string, object>("ddsm_processstatus", new OptionSetValue((int)Helper.ProcessStatus.Disabled))
                            }
                        }
                    };

                    if (record.GetAttributeValue<DateTime>("ddsm_datedelisted") > DateTime.MinValue)
                    {
                        request.Target["ddsm_datedelisted"] = record.GetAttributeValue<DateTime>("ddsm_datedelisted");
                    }
                    result.Add(request);
                }
            }
            return result;
        }

        /// <summary>
        ///Def impl for ES 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="syncDate"></param>
        /// <param name="cat"></param>
        /// <param name="proseccedRefs"></param>
        /// <returns></returns>
        public virtual List<Entity> GetDisabledRecords(IOrganizationService service, DateTime syncDate, Tab cat, List<EntityReference> proseccedRefs)
        {
            int page = 1;
            int recordPerPage = 5000;
            var ids = proseccedRefs.Select(x => x.Id).ToList().Where(x => !x.Equals(Guid.Empty)).ToList();
            List<Entity> all = new List<Entity>();//allResp.Entities;

            if (ids?.Count > 0)
            {
                var query = new QueryExpression
                {
                    EntityName = "ddsm_certlib",
                    ColumnSet = new ColumnSet("createdon", "modifiedon", "ddsm_processstatus", "ddsm_datedelisted"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("ddsm_certlibid",ConditionOperator.In,ids ),
                            new ConditionExpression("ddsm_processstatus",ConditionOperator.NotEqual, (int)Helper.ProcessStatus.Disabled ),
                            new ConditionExpression("ddsm_type_product",ConditionOperator.Equal,cat.MetadataId.ToLower()),
                            new ConditionExpression("statecode", ConditionOperator.Equal, (int)StateCode.Active)
                        }
                    },
                    PageInfo = new PagingInfo
                    {
                        Count = recordPerPage,
                        PageNumber = page
                    }
                };
                // var allResp = service.RetrieveMultiple(query);
                var entityList = new EntityCollection();

                do
                {
                    query.PageInfo.PagingCookie = (page == 1) ? null : entityList.PagingCookie;
                    query.PageInfo.PageNumber = page++;

                    entityList = service.RetrieveMultiple(query);
                    all.AddRange(entityList?.Entities);
                    // Do something with the results here
                }
                while (entityList.MoreRecords);

            }
            return all;
        }

        public List<Entity> GetAllProducByCategory(IOrganizationService service, DateTime syncDate, Tab cat)
        {
            int page = 1;
            int recordPerPage = 5000;
            var query = new QueryExpression
            {
                EntityName = "ddsm_certlib",
                ColumnSet = new ColumnSet("createdon", "modifiedon", "ddsm_processstatus", "ddsm_datedelisted"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("statecode",ConditionOperator.Equal,(int)StateCode.Active),
                        //  new ConditionExpression("ddsm_certlibid",ConditionOperator.In,ids ),
                        //  new ConditionExpression("ddsm_certlibsyncid",ConditionOperator.Equal,mainEntityRef.Id ),
                        new ConditionExpression("ddsm_type_product",ConditionOperator.Equal,cat.MetadataId.ToLower() )
                    }
                },
                PageInfo = new PagingInfo
                {
                    Count = recordPerPage,
                    PageNumber = page
                }
            };
            // var allResp = service.RetrieveMultiple(query);
            var entityList = new EntityCollection();
            List<Entity> all = new List<Entity>();//allResp.Entities;
            do
            {
                query.PageInfo.PagingCookie = (page == 1) ? null : entityList.PagingCookie;
                query.PageInfo.PageNumber = page++;

                entityList = service.RetrieveMultiple(query);
                all.AddRange(entityList?.Entities);
                // Do something with the results here
            }
            while (entityList.MoreRecords);

            return all;

        }


        public void UpdateStatus(IEnumerable<Entity> newRecords, IEnumerable<EntityReference> updatedRecords, IEnumerable<Entity> disabledRecords, IOrganizationService service)
        {
            var listRequests = new List<UpdateRequest>();


            foreach (var product in disabledRecords)
            {
                listRequests.Add(new UpdateRequest
                {
                    Target = new Entity(product.LogicalName, product.Id)
                    {
                        Attributes = {
                                         new KeyValuePair<string, object>("ddsm_processstatus", new OptionSetValue((int)Helper.ProcessStatus.Disabled))
                                    }
                    }
                });
            }



            var disableMnRequest = UpdateMNStatus(disabledRecords, service);
            listRequests.AddRange(disableMnRequest);

            var portion = 250;
            var processed = 0;

            while (processed < listRequests.Count)
            {
                var dataPortion = listRequests.Skip(processed).Take(portion);
                processed += dataPortion.Count();
                var requestWithResults = new ExecuteMultipleRequest
                {
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = true,
                        ReturnResponses = true
                    },
                    Requests = new OrganizationRequestCollection()
                };

                requestWithResults.Requests.AddRange(dataPortion);

                var result = (ExecuteMultipleResponse)service.Execute(requestWithResults);
                requestWithResults.Requests.Clear();
            }
        }

    }

    public class DlcHistoryCreator : BaseHistoryCreator
    {
        public override List<Entity> GetDisabledRecords(IOrganizationService service, DateTime syncDate, Tab cat, List<EntityReference> proseccedRefs)
        {
            int page = 1;
            int recordPerPage = 5000;
            var ids = proseccedRefs.Select(x => x.Id).ToList().Where(x => !x.Equals(Guid.Empty)).ToList();
            List<Entity> all = new List<Entity>();//allResp.Entities;// var allResp = service.RetrieveMultiple(query);

            if (ids?.Count > 0)
            {
                var query = new QueryExpression
                {
                    EntityName = "ddsm_certlib",
                    ColumnSet = new ColumnSet("createdon", "modifiedon", "ddsm_processstatus", "ddsm_datedelisted"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("statecode",ConditionOperator.Equal,(int)StateCode.Active),
                            new ConditionExpression("ddsm_datedelisted",ConditionOperator.NotNull),
                            new ConditionExpression("ddsm_certlibid",ConditionOperator.In,ids ),
                            new ConditionExpression("ddsm_processstatus",ConditionOperator.NotEqual, (int)Helper.ProcessStatus.Disabled ),
                            new ConditionExpression("ddsm_type_product",ConditionOperator.Equal,cat.MetadataId.ToLower())
                        }
                    },
                    PageInfo = new PagingInfo
                    {
                        Count = recordPerPage,
                        PageNumber = page
                    }
                };

                var entityList = new EntityCollection();
                do
                {
                    query.PageInfo.PagingCookie = (page == 1) ? null : entityList.PagingCookie;
                    query.PageInfo.PageNumber = page++;

                    entityList = service.RetrieveMultiple(query);
                    all.AddRange(entityList?.Entities);
                    // Do something with the results here
                }
                while (entityList.MoreRecords);
            }
            return all;
        }

        public override string CreateHistory(ITracingService tracer, IOrganizationService service, EntityReference mainEntityRef,
            DateTime syncDate, IEnumerable<Tab> categoriesList, out int newCount, out int updatedCount, out int deletedCount, List<EntityReference> proseccedRefs)
        {
            newCount = 0;
            updatedCount = 0;
            deletedCount = 0;

            var result = new
            {
                labels = categoriesList.Select(x => x.Label).ToList(),
                datasets = new List<Column>()
            };

            var newItems = new List<int>();
            var updatesItems = new List<int>();
            var disabledItems = new List<int>();

            var ds = new List<Column>();
            foreach (var cat in categoriesList)
            {
                var historyData = GetNew(service, syncDate, mainEntityRef, cat, proseccedRefs);
                newItems.Add(historyData.New);
                updatesItems.Add(historyData.Updated);
                disabledItems.Add(historyData.Disabled);

                newCount += historyData.New;
                updatedCount += historyData.Updated;
                deletedCount += historyData.Disabled;
            }//TODO: remove label and backgroundColor. 
            ds.Add(new Column { data = newItems, backgroundColor = "#5B97D5", label = "New", name = "New" });
            ds.Add(new Column { data = updatesItems, backgroundColor = "#70AD47", label = "Updated", name = "Updated" });
            ds.Add(new Column { data = disabledItems, backgroundColor = "#A7ADBA", label = "Disabled", name = "Disabled" });
            result.datasets.AddRange(ds);
            var history = JsonConvert.SerializeObject(result);
            return history;
        }

        public override Helper.History GetNew(IOrganizationService service, DateTime syncDate, EntityReference mainEntityRef, Tab cat, List<EntityReference> proseccedRefs)
        {
            int page = 1;
            int recordPerPage = 5000;
            var ids = proseccedRefs.Select(x => x.Id).ToList().Where(x => !x.Equals(Guid.Empty)).ToList();
            var all = new List<Entity>();//allResp.Entities;

            if (ids?.Count > 0)
            {
                var query = new QueryExpression
                {
                    EntityName = "ddsm_certlib",
                    ColumnSet = new ColumnSet("createdon", "modifiedon", "ddsm_processstatus", "ddsm_datedelisted"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("statecode",ConditionOperator.Equal,(int)StateCode.Active),
                            new ConditionExpression("ddsm_certlibid",ConditionOperator.In,ids ),
                            new ConditionExpression("ddsm_certlibsyncid",ConditionOperator.Equal,mainEntityRef.Id ),
                            new ConditionExpression("ddsm_type_product",ConditionOperator.Equal,cat.MetadataId.ToLower() )
                        }
                    },
                    PageInfo = new PagingInfo
                    {
                        Count = recordPerPage,
                        PageNumber = page
                    }
                };
                // var allResp = service.RetrieveMultiple(query);
                var entityList = new EntityCollection();

                do
                {
                    query.PageInfo.PagingCookie = (page == 1) ? null : entityList.PagingCookie;
                    query.PageInfo.PageNumber = page++;

                    entityList = service.RetrieveMultiple(query);
                    all.AddRange(entityList?.Entities);
                    // Do something with the results here
                }
                while (entityList.MoreRecords);
            }

            var newRecords = all.Where(x => x.GetAttributeValue<DateTime>("createdon").Equals(x.GetAttributeValue<DateTime>("modifiedon"))
                                            && x.GetAttributeValue<OptionSetValue>("ddsm_processstatus")?.Value == (int)Helper.ProcessStatus.ToBeReviewed);

            var updatedRecordsRefs = all.Select(x => x.ToEntityReference()).Where(i => !newRecords.Select(x => x.ToEntityReference()).Contains(i)); //proseccedRefs.Where(i => !newRecords.Select(x => x.ToEntityReference()).Contains(i)).ToList();

            var unChangedRecords = updatedRecordsRefs;

            var recordsNotInCurrentSync = GetAllProducByCategory(service, syncDate, cat).Select(x => x.ToEntityReference()).Where(i => !proseccedRefs.Contains(i));

            //from DB
            var disabledRecords = GetDisabledRecords(service, syncDate, cat, recordsNotInCurrentSync.ToList()); //.Select(x => x.ToEntityReference())
            var disabledRecords2 = all.Where(x => x.Attributes.Contains("ddsm_datedelisted") && !x.GetAttributeValue<DateTime>("ddsm_datedelisted").Equals(DateTime.MinValue) && x.GetAttributeValue<OptionSetValue>("ddsm_processstatus")?.Value != (int)Helper.ProcessStatus.Disabled).ToList();

            disabledRecords.AddRange(disabledRecords2);

            UpdateStatus(newRecords, updatedRecordsRefs, disabledRecords, service);

            return new Helper.History()
            {
                New = newRecords.ToList().Count,
                Updated = updatedRecordsRefs.ToList().Count,
                Unchanged = unChangedRecords.ToList().Count,
                Disabled = disabledRecords.ToList().Count
            };
        }
    }

    public class EsHistoryCreator : BaseHistoryCreator
    {
        public override string CreateHistory(ITracingService tracer, IOrganizationService service, EntityReference mainEntityRef,
            DateTime syncDate, IEnumerable<Tab> categoriesList, out int newCount, out int updatedCount, out int deletedCount, List<EntityReference> proseccedRefs)
        {
            newCount = 0;
            updatedCount = 0;
            deletedCount = 0;

            var result = new
            {
                labels = categoriesList.Select(x => x.Label).ToList(),
                datasets = new List<Column>()
            };

            var newItems = new List<int>();
            var updatesItems = new List<int>();
            var disabledItems = new List<int>();

            var ds = new List<Column>();
            foreach (var cat in categoriesList)
            {
                var historyData = GetNew(service, syncDate, mainEntityRef, cat, proseccedRefs);
                newItems.Add(historyData.New);
                updatesItems.Add(historyData.Updated);
                disabledItems.Add(historyData.Disabled);

                newCount += historyData.New;
                updatedCount += historyData.Updated;
                deletedCount += historyData.Disabled;
            }//TODO: remove label and backgroundColor. 
            ds.Add(new Column { data = newItems, backgroundColor = "#5B97D5", label = "New", name = "New" });
            ds.Add(new Column { data = updatesItems, backgroundColor = "#70AD47", label = "Updated", name = "Updated" });
            ds.Add(new Column { data = disabledItems, backgroundColor = "#A7ADBA", label = "Disabled", name = "Disabled" });
            result.datasets.AddRange(ds);
            var history = JsonConvert.SerializeObject(result);
            return history;
        }


        public override Helper.History GetNew(IOrganizationService service, DateTime syncDate, EntityReference mainEntityRef, Tab cat, List<EntityReference> proseccedRefs)
        {
            int page = 1;
            int recordPerPage = 5000;
            var ids = proseccedRefs.Select(x => x.Id).ToList().Where(x => !x.Equals(Guid.Empty)).ToList();
            List<Entity> all = new List<Entity>();

            if (ids?.Count > 0)
            {
                var query = new QueryExpression
                {
                    EntityName = "ddsm_certlib",
                    ColumnSet = new ColumnSet("createdon", "modifiedon", "ddsm_processstatus", "ddsm_datedelisted"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("statecode",ConditionOperator.Equal,(int)StateCode.Active),
                            new ConditionExpression("ddsm_certlibid",ConditionOperator.In,ids ),
                            new ConditionExpression("ddsm_certlibsyncid",ConditionOperator.Equal,mainEntityRef.Id ),
                            new ConditionExpression("ddsm_type_product",ConditionOperator.Equal,cat.MetadataId.ToLower() )
                        }
                    },
                    PageInfo = new PagingInfo
                    {
                        Count = recordPerPage,
                        PageNumber = page
                    }
                };

                var entityList = new EntityCollection();

                do
                {
                    query.PageInfo.PagingCookie = (page == 1) ? null : entityList.PagingCookie;
                    query.PageInfo.PageNumber = page++;

                    entityList = service.RetrieveMultiple(query);
                    all.AddRange(entityList?.Entities);
                }
                while (entityList.MoreRecords);
            }


            var newRecords = all.Where(x => x.GetAttributeValue<DateTime>("createdon").Equals(x.GetAttributeValue<DateTime>("modifiedon"))
            && x.GetAttributeValue<OptionSetValue>("ddsm_processstatus")?.Value == (int)Helper.ProcessStatus.ToBeReviewed).ToList();

            var updatedRecords = all.Select(x => x.ToEntityReference()).Where(i => !newRecords.Select(x => x.ToEntityReference()).Contains(i)).ToList(); //proseccedRefs.Where(i => !newRecords.Select(x => x.ToEntityReference()).Contains(i)).ToList();

            // var updatedRecords = proseccedRefs.Where(i => !newRecords.Select(x => x.ToEntityReference()).Contains(i)).ToList();

            var unChangedRecords = updatedRecords;

            var recordsNotInCurrentSync = GetAllProducByCategory(service, syncDate, cat).Select(x => x.ToEntityReference()).Where(i => !proseccedRefs.Contains(i));

            var disabledRecords = GetDisabledRecords(service, syncDate, cat, recordsNotInCurrentSync.ToList());

            UpdateStatus(newRecords, updatedRecords, disabledRecords, service);

            return new Helper.History()
            {
                New = newRecords.ToList().Count,
                Updated = updatedRecords.ToList().Count,
                Unchanged = unChangedRecords.ToList().Count,
                Disabled = disabledRecords.ToList().Count
            };
        }
    }
}




