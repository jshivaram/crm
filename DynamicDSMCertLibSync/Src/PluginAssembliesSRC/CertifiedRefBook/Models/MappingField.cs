﻿namespace CertifiedRefBook.Models
{
    public class MappingField
    {
        public string FieldName { get; set; }
        public string FieldDisplayName { get; set; }
        public string FieldType { get; set; }
        public string AttrLogicalName { get; set; }
        public string AttrType { get; set; }
    }
}