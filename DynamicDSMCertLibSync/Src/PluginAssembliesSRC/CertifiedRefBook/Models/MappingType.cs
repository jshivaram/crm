﻿namespace CertifiedRefBook.Models
{
    public enum MappingType
    {
        Entity = 962080000,
        Mad = 962080001,
        Qdd = 962080002,
        Msd = 962080003,
        ByCertified = 962080004,
    }
}