﻿using System;
using CertifiedRefBook.Models.DLC;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace CertifiedRefBook.Utils
{
    public class DlcHelper
    {
        public DlcConfig Config { get; set; }
        private readonly IOrganizationService _service;
        private readonly string[] _columns = new string[] { "ddsm_dlcurl", "ddsm_dlcaccesskey", "ddsm_dlcaccesskeysecret","ddsm_dlcportion", "ddsm_dlcproductmodel" };
        public DlcHelper(IOrganizationService service)
        {
            _service = service;
            GetConfig();
        }

        public void GetConfig()
        {
            var defConfName = "Admin Data";
            try
            {
                var expr = new QueryExpression
                {
                    EntityName = "ddsm_admindata",
                    ColumnSet = new ColumnSet(_columns),
                    Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, defConfName) }
                    }
                };
                var adminData = _service.RetrieveMultiple(expr);
                if (adminData != null && adminData.Entities?.Count >= 1)
                {
                    Config = new DlcConfig(adminData.Entities[0]);
                }
                else
                {
                    throw new Exception("Can't get Energy Star configuration. Check Admin data record.");
                }
            }
            catch (Exception ex)
            {
                // return null;
                throw new Exception("error in getting Config: " + ex.Message + ex.Data);
            }
        }
    }

}
