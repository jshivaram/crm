﻿using CertifiedRefBook.Models;
using CertifiedRefBook.Models.Es;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CertifiedRefBook.Utils
{
    public class EnergyStarHelper
    {
        private string _sqlLimit = "?$limit=50000&$offset=0&$$exclude_system_fields=false";
        private string _sql = "";
        //selected categories from mapping record 
        public List<Tab> Tabs { get; set; }
        private readonly IOrganizationService _service;
        public EsConfig Config { get; set; }
        // public string MainURL = "http://data.energystar.gov/";
        string CategoriesPart = "api/views/";
        string ResourcesPart = "resource/";
        private readonly string[] _columns = new string[] { "ddsm_esurl", "ddsm_eslogin", "ddsm_espass", "ddsm_estoken" };

        public string GetCategorieURL(string metadataId)
        {
            return metadataId.ToString() + ".json" + _sqlLimit + _sql;
        }

        public Dictionary<Guid, List<MappingField>> GetCurrentMapping(EntityReference mainEntityRef)
        {
            var result = new Dictionary<Guid, List<MappingField>>();
            var query = new QueryExpression
            {
                EntityName = mainEntityRef.LogicalName,

                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = { new ConditionExpression("ddsm_certlibsyncid", ConditionOperator.Equal, mainEntityRef.Id) }
                },
                LinkEntities =
                {
                    new LinkEntity(mainEntityRef.LogicalName,"ddsm_certlibsyncconf","ddsm_certlibsyncconfid","ddsm_certlibsyncconfid", JoinOperator.Inner)
                    {
                        LinkEntities =
                        {
                            new LinkEntity("ddsm_certlibsyncconf","ddsm_mapping", "ddsm_mappingid", "ddsm_mappingid", JoinOperator.Inner)
                            {
                                Columns = new ColumnSet("ddsm_jsondata","ddsm_entities")
                            }
                        }
                    }
                }
            };
            var crmData = _service.RetrieveMultiple(query);
            if (crmData.Entities.Count > 0)
            {
                var admindata = crmData.Entities[0];

                object value;
                if (admindata.Attributes.TryGetValue("ddsm_mapping2.ddsm_jsondata", out value))
                {
                    var json = (string)((AliasedValue)value).Value;
                    result = JsonConvert.DeserializeObject<Dictionary<Guid, List<MappingField>>>(json);
                }
                if (admindata.Attributes.TryGetValue("ddsm_mapping2.ddsm_entities", out value))
                {
                    var json = (string)((AliasedValue)value).Value;
                    Tabs = JsonConvert.DeserializeObject<List<Tab>>(json);
                }

            }
            else
            {
                throw new Exception("Can't receive mapping data.");
            }
            return result;
        }

        public EnergyStarHelper(IOrganizationService service)
        {
            _service = service;
            GetConfig();
        }

        public string GetResourcesUrl(string catId)
        {
            return Config.Url + ResourcesPart + catId + ".json";
        }

        // string MetadataPart = "/rows.json?accessType=DOWNLOAD";

        public string GetMetadataUrlPart(string catId)
        {
            return Config.Url + CategoriesPart + catId; //+ MetadataPart;
        }

        public string GetCategories()
        {
            return Config.Url + CategoriesPart; //+ MetadataPart;
        }

        public void GetConfig()
        {
            var defConfName = "Admin Data";
            try
            {
                var expr = new QueryExpression
                {
                    EntityName = "ddsm_admindata",
                    ColumnSet = new ColumnSet(_columns),
                    Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, defConfName) }
                    }
                };
                var adminData = _service.RetrieveMultiple(expr);
                if (adminData != null && adminData.Entities?.Count >= 1)
                {
                    Config = new EsConfig(adminData.Entities[0]);
                }
                else
                {
                    throw new Exception("Can't get Energy Star configuration. Check Admin data record.");
                }
            }
            catch (Exception ex)
            {
                // return null;
                throw new Exception("error in getting Config: " + ex.Message + ex.Data);
            }
        }
    }
}
