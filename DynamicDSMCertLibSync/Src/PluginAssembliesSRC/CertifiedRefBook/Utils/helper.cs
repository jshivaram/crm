﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Activities;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Messages;
using System.Linq;

namespace CertifiedRefBook
{


    public class Helper
    {
       

        public static Entity GetCertOrg(IOrganizationService service, string orgName)
        {
            //  var orgName = ;

            var query = new QueryExpression()
            {
                EntityName = "ddsm_certifyingorganization",
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, orgName) }
                }
            };

            var data = service.RetrieveMultiple(query);
            if (data.Entities.Count > 0)
            {
                return data[0];
            }
            else
            {
                throw new Exception(string.Format("Certified Organization with name {0} doesn't exist.", orgName));
            }

        }

        public static void DeleteCategories(Guid orgId, IOrganizationService service)
        {
            var query = new QueryExpression()
            {
                EntityName = "ddsm_certifiedcategory",
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = { new ConditionExpression("ddsm_certifyingorganizationid", ConditionOperator.Equal, orgId) }
                }
            };

            var data = service.RetrieveMultiple(query);
            if (data.Entities.Count > 0)
            {
                var delRequest = new ExecuteMultipleRequest()
                {
                    Settings = new ExecuteMultipleSettings()
                    {
                        ContinueOnError = false,
                        ReturnResponses = true
                    },
                    Requests = new OrganizationRequestCollection()
                };
                var ddd = from ent in data.Entities
                          select new DeleteRequest() { Target = ent.ToEntityReference() };

                delRequest.Requests.AddRange(ddd);

                service.Execute(delRequest);
            }
        }

        public static EntityReference GetTargetData(CodeActivityContext objDdsm)
        {
            IWorkflowContext context = objDdsm.GetExtension<IWorkflowContext>();

            //objDdsm.TracingService.Trace("in GetTargetData:");
            EntityReference target = new EntityReference();
            if (context.InputParameters.ContainsKey("Target"))
            {
                // objDdsm.Trace("objDdsm.Context.InputParameters.ContainsKey(Target)");
                // objDdsm.TracingService.Trace("in GetTargetData:");
                if (context.InputParameters["Target"] is Entity)
                {
                    target = ((Entity)context.InputParameters["Target"]).ToEntityReference();
                }
                else if (context.InputParameters["Target"] is EntityReference)
                {
                    target = (EntityReference)context.InputParameters["Target"];
                }
            }


            //objDdsm.TracingService.Trace("Target LogicalName: " + target.LogicalName + " Id: " + target.Id.ToString());
            // throw new Exception("FAKE Exception::::: " + string.Join(",", ((MoskTraceService)objDdsm.TracingService).Logs));
            return target;
        }
    }

}
