﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using CertifiedRefBook.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using CertifiedRefBook.Utils;

namespace CertifiedRefBook
{
    public class AssociateCertLibToModelNumber : CodeActivity
    {
        [RequiredArgument]
        [Input("Mapping")]
        [ArgumentEntity("ddsm_mapping")]
        [ReferenceTarget("ddsm_mapping")]
        public InArgument<EntityReference> MappingRef { get; set; }

        [RequiredArgument]
        [Input("ModelName")]
        public InArgument<string> ModelName { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            var tracer = executionContext.GetExtension<ITracingService>();
            var context = executionContext.GetExtension<IWorkflowContext>();
            var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            var service = serviceFactory.CreateOrganizationService(context.UserId);
            var helper = new EnergyStarHelper(service);

            var modelName = ModelName.Get(executionContext);

            var target = Helper.GetTargetData(executionContext);

            var mappingRef = MappingRef.Get(executionContext);
            List<Tab2> tabs;
            var mappingData = GetMappingData(mappingRef, service, out tabs);


            var modelNumber = FillEntity(mappingData, tabs, target, service);

            var respounse = (UpsertResponse)service.Execute(new UpsertRequest() { Target = new Entity("ddsm_modelnumber", "ddsm_modelname", modelName) { Attributes = modelNumber.Attributes } });
            var modelNumberRef = respounse.Target;

            // Creating EntityReferenceCollection for the Contact
            var relatedEntities = new EntityReferenceCollection { target };

            // Add the related entity contact
            //relatedEntities.Add(contact);

            // Add the Account Contact relationship schema name
            var relationship = new Relationship("ddsm_ddsm_modelnumber_ddsm_certlib");

            // Associate the contact record to Account
            service.Associate(modelNumberRef.LogicalName, modelNumberRef.Id, relationship, relatedEntities);


        }
        // GetMappingData(EntityReference mappingRef, IOrganizationService service
        private Entity FillEntity(Dictionary<string, List<EntityMappingField>> mappingDict, List<Tab2> entities, EntityReference targetRef, IOrganizationService service)
        {
            var mapping = mappingDict[entities[0].LogicalName];
            var columns = mapping.Where(x => !string.IsNullOrEmpty(x.TargetFieldLogicalName)).Select(y => y.SourceFieldLogicalName.ToLower()).ToArray();

            var result = new Entity();

            var certLibData = service.Retrieve(targetRef.LogicalName, targetRef.Id, new ColumnSet(columns));

            foreach (var attr in certLibData.Attributes)
            {
                var mappingData = mapping.FirstOrDefault(x => !string.IsNullOrEmpty(x.TargetFieldLogicalName) && x.SourceFieldLogicalName.ToLower().Equals(attr.Key.ToLower()));

                if (mappingData?.TargetFieldLogicalName.ToLower() != "ddsm_modelname" && !string.IsNullOrEmpty(mappingData?.TargetFieldType))
                {
                    if (mappingData.TargetFieldType.Equals("String"))
                    {
                        result[mappingData.TargetFieldLogicalName.ToLower()] = attr.Value;
                    }
                    if (mappingData.TargetFieldType.Equals("Decimal"))
                    {
                        result[mappingData.TargetFieldLogicalName.ToLower()] = Decimal.Parse((string)attr.Value);
                    }
                    if (mappingData.TargetFieldType.Equals("Integer"))
                    {
                        result[mappingData.TargetFieldLogicalName.ToLower()] = Int32.Parse((string)attr.Value);
                    }
                    if (mappingData.TargetFieldType.Equals("DateTime"))
                    {
                        result[mappingData.TargetFieldLogicalName.ToLower()] = DateTime.Parse((string)attr.Value);
                    }
                }
            }
            return result;
        }
        private Dictionary<string, List<EntityMappingField>> GetMappingData(EntityReference mappingRef, IOrganizationService service, out List<Tab2> tabs)
        {
            tabs = new List<Tab2>();
            var mappingRecord = service.Retrieve(mappingRef.LogicalName, mappingRef.Id,
                new ColumnSet("ddsm_jsondata", "ddsm_entities"));

            var result = new Dictionary<string, List<EntityMappingField>>();
            if (mappingRecord != null && mappingRecord.Attributes.ContainsKey("ddsm_jsondata"))
            {
                var json = (string)mappingRecord["ddsm_jsondata"];
                result = JsonConvert.DeserializeObject<Dictionary<string, List<EntityMappingField>>>(json);
            }
            if (mappingRecord != null && mappingRecord.Attributes.ContainsKey("ddsm_entities"))
            {
                var json = (string)mappingRecord["ddsm_entities"];
                tabs = JsonConvert.DeserializeObject<List<Tab2>>(json);
            }
            return result;
        }
    }
}
