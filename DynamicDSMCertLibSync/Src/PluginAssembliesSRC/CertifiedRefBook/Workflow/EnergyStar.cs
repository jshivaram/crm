﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json.Linq;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using CertifiedRefBook.Models;
using Newtonsoft.Json;
using CertifiedRefBook.Models.Es;
using CertifiedRefBook.Utils;
using System.Net.Http;
using System.Net.Http.Headers;

namespace CertifiedRefBook
{
    public enum StatusCertLib
    {
        New = 962080000,
        Update = 962080001,
        Unchange = 962080002,
        Delete = 962080003
    }

    public class EnergyStar : CodeActivity
    {
        #region Const

        public const string MainEntity = "ddsm_certlib";
        public const int LimitMultiRequst = 250;

        #endregion

        #region Params

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        #endregion

        #region internal fields 

        // Counters for summary of Sync
        private int NewCount; //count added items
        private int UpdatedCount; //count updated items
        private int DeletedCount; // count deleted items
        private int UnchangedCount; //count unchanged items

        //  private Guid PreviousCertLibSyncId;
        private JArray JArrayMetaDataFilter;
        private bool IsKeepUncheckedCategories;
        private EnergyStarHelper _helper;
        private EsConfig _config;
        #endregion

        protected override void Execute(CodeActivityContext executionContext)
        {
            NewCount = 0;
            UpdatedCount = 0;
            DeletedCount = 0;
            UnchangedCount = 0;
            JArrayMetaDataFilter = null;
            IsKeepUncheckedCategories = false;

            var tracer = executionContext.GetExtension<ITracingService>();
            var context = executionContext.GetExtension<IWorkflowContext>();
            var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            var service = serviceFactory.CreateOrganizationService(context.UserId);

            _helper = new EnergyStarHelper(service);
            _config = _helper.Config;
            tracer.Trace("---------start------------");

            tracer.Trace("Target1 = " + context.InputParameters["Target"]);

            var isNativeTarget = true;
            EntityReference mainEntityRef;

            if (context.InputParameters["Target"] is Entity)
            {
                var entity = context.InputParameters["Target"] as Entity;
                mainEntityRef = entity.ToEntityReference();
            }
            else
            {
                var entity = context.InputParameters["Target"] as EntityReference;

                if (entity.LogicalName == "ddsm_schedule")
                {
                    var entit = new Entity("ddsm_certlibsync");
                    service.Create(entit);

                    var query = new QueryExpression
                    {
                        EntityName = "ddsm_certlibsync",
                        ColumnSet = new ColumnSet("ddsm_categoriesjson"),
                        TopCount = 1,
                        Orders =
                        {
                            new OrderExpression("createdon", OrderType.Descending)
                        }
                    };

                    var listCertLibSync = service.RetrieveMultiple(query);
                    var certLibSync = listCertLibSync.Entities[0];
                    mainEntityRef = certLibSync.ToEntityReference();
                    isNativeTarget = false;
                }
                else
                {
                    mainEntityRef = (EntityReference)context.InputParameters["Target"];
                }
            }


            RunAction(tracer, service, mainEntityRef, isNativeTarget);

            Complete.Set(executionContext, true);

            tracer.Trace("---------end------------");
        }

        private JArray GetDataFromAPI(EntityReference mainEntityRef)
        {
            JArray arrCertAllData = new JArray();
            var selectedCategories = _helper.Tabs;
            var config = _helper.Config;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(config.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-App-Token", config.Token);

                foreach (var categorie in selectedCategories)
                {
                    var metadataId = categorie.MetadataId;
                    var url = _helper.GetCategorieURL(metadataId);

                    //var response = client.DownloadString(url);
                    HttpResponseMessage response = client.GetAsync(_helper.GetCategories()).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var arrCertData = response.Content.ReadAsAsync<JArray>(); //new JArray(JsonConvert.DeserializeObject<JArray>(response));

                        foreach (var jObj in arrCertData.Result.Children<JObject>())
                        {
                            jObj.Add("type_product", metadataId);
                            jObj.Add("certified", "Energy Star");
                            jObj.Add("is_delete", "false");
                            jObj.Add("status", "");
                            jObj.Add("certlibsyncid", mainEntityRef.Id.ToString());
                        }

                        arrCertAllData.Merge(arrCertData, new JsonMergeSettings
                        {
                            MergeArrayHandling = MergeArrayHandling.Concat
                        });
                    }
                }
            }
            return arrCertAllData;
        }

        private void RunAction(ITracingService tracer, IOrganizationService service, EntityReference mainEntityRef,
            bool isNativeTarget)
        {
            var mappingDict = _helper.GetCurrentMapping(mainEntityRef);
            var syncDate = DateTime.UtcNow;

            #region init vars

            // var begTime = DateTime.Now;

            //main array from API for processing
            JArray arrCertAllData = new JArray();

            var jaMetaDataLastSync = new JArray();

            var multipleRequestDel = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };
            var listEnergyStarDel = new EntityCollection();
            var listEnergyStarUp = new EntityCollection();
            var listEnergyStarNew = new EntityCollection();

            var listEnergyStarUnchange = new EntityCollection();

            #endregion

            // get data from API
            arrCertAllData = GetDataFromAPI(mainEntityRef);

            #region Create new Items

            var multipleRequestRow = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };

            if (arrCertAllData.Count > 0)
            {
                var countObjNew = arrCertAllData.Count;
                var jsonDataItems = arrCertAllData.Children<JObject>();
                tracer.Trace("Count certified Items for creting in crm: " + countObjNew);
                List<UpsertRequest> entitiesForProcessing = new List<UpsertRequest>();
                var allProcessedCount = 0;

                //prepare entities for processing
                foreach (var itemJson in jsonDataItems)
                {
                    var crmItem = FillItemEntity(itemJson, mainEntityRef, mappingDict);
                    entitiesForProcessing.Add(new UpsertRequest() { Target = crmItem });
                }
                //process entities by portion
                while (allProcessedCount < countObjNew)
                {
                    var range = entitiesForProcessing.Skip(allProcessedCount).Take(LimitMultiRequst).ToList();
                    allProcessedCount += range.Count;
                    multipleRequestRow.Requests.AddRange(range);
                    var multipleResponseRow = (ExecuteMultipleResponse)service.Execute(multipleRequestRow);
                    if (multipleResponseRow.IsFaulted)
                    {
                        throw new Exception("Creating items failed: ");
                    }
                    multipleRequestRow.Requests.Clear();
                }

                EntityCollection oldRecords = GetOldRecords(syncDate, service);
                DeletedCount = oldRecords.Entities.Count;
                if (!IsKeepUncheckedCategories)
                {
                }
                NewCount = UpdatedCount = allProcessedCount;
            }

            #endregion

            UpdateParentEntity(tracer, service, mainEntityRef, syncDate, isNativeTarget);
        }

        private EntityCollection GetOldRecords(DateTime syncDate, IOrganizationService service)
        {
            var result = new EntityCollection();
            try
            {
                var query = new QueryExpression
                {
                    EntityName = MainEntity,
                    ColumnSet = new ColumnSet("ddsm_externaluniqueid"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("modifiedon",ConditionOperator.LessThan, syncDate),
                            new ConditionExpression("ddsm_type_product",ConditionOperator.In, _helper.Tabs.Select(x=>x.MetadataId).ToList()),
                        }
                    }
                };
                result = service.RetrieveMultiple(query);

            }
            catch (Exception ex)
            {
                // throw;
            }
            return result;
        }

        private Entity FillItemEntity(JObject item, EntityReference mainEntityRef, Dictionary<Guid, List<MappingField>> mappingDict)
        {
            var catMetadataId = item.Value<string>("type_product");
            List<MappingField> mapping = GetMappingForCurrentCategorie(catMetadataId, mappingDict);

            var energystarcertified = new Entity(MainEntity, "ddsm_externaluniqueid", Int32.Parse(item.SelectToken("pd_id").ToString()))
            {
                Attributes =
                        {
                            ["ddsm_certlibsyncid"] = mainEntityRef
                        }
            };

            foreach (var prop in item.Properties())
            {
                var propName = prop.Name;

                var propVal = string.Empty;

                if (prop.HasValues)
                {
                    propVal = prop.Value.ToString().Length > 300 ? prop.Value.ToString().Substring(0, 300) : prop.Value.ToString();
                }
                else
                {
                    propVal = string.Empty;
                }
                var mappingData = mapping.FirstOrDefault(x => !string.IsNullOrEmpty(x.FieldName) && x.FieldName.Equals(propName));
                if (!string.IsNullOrEmpty(mappingData?.AttrLogicalName))
                {
                    if (mappingData.AttrType.Equals("String"))
                    {
                        energystarcertified[mappingData.AttrLogicalName.ToLower()] = propVal;
                    }
                    if (mappingData.AttrType.Equals("Decimal"))
                    {
                        energystarcertified[mappingData.AttrLogicalName.ToLower()] = Decimal.Parse(propVal);
                    }
                    if (mappingData.AttrType.Equals("Integer"))
                    {
                        energystarcertified[mappingData.AttrLogicalName.ToLower()] = Int32.Parse(propVal);
                    }
                    if (mappingData.AttrType.Equals("DateTime"))
                    {
                        energystarcertified[mappingData.AttrLogicalName.ToLower()] = DateTime.Parse(propVal);
                    }
                }
            }
            // var brandName = energystarcertified.Contains("ddsm_brand_name") ? energystarcertified.Attributes["ddsm_brand_name"].ToString() : string.Empty;
            var modName = energystarcertified.Contains("ddsm_model_name") ? energystarcertified.Attributes["ddsm_model_name"].ToString() : string.Empty;
            //var fullName = brandName + " - " + modName;

            energystarcertified.Attributes["ddsm_name"] = modName;
            energystarcertified["ddsm_type_product"] = catMetadataId;
            energystarcertified["ddsm_jsondata"] = item.ToString(Newtonsoft.Json.Formatting.Indented);
            return energystarcertified;
        }

        private List<MappingField> GetMappingForCurrentCategorie(string catMetadataId, Dictionary<Guid, List<MappingField>> mappingDict)
        {
            var cat = _helper.Tabs.FirstOrDefault(x => x.MetadataId.Equals(catMetadataId));
            var catId = cat?.LogicalName ?? Guid.Empty;
            return mappingDict[catId].Where(x => !string.IsNullOrEmpty(x.AttrLogicalName)).ToList();
        }

        private void UpdateParentEntity(ITracingService tracer, IOrganizationService service, EntityReference mainEntityRef, DateTime begTime, bool isNativeTarget)
        {
            var jaFilter = JArrayMetaDataFilter;

            try
            {
                tracer.Trace("---------Update1------------");

                var energyStarUpdate = new Entity(mainEntityRef.LogicalName, mainEntityRef.Id)
                {
                    ["ddsm_enddate"] = DateTime.UtcNow,
                    ["ddsm_newcount"] = NewCount,
                    ["ddsm_updatecount"] = UpdatedCount,
                    ["ddsm_unchangedcount"] = UnchangedCount,
                    ["ddsm_deletecount"] = DeletedCount,
                    ["ddsm_status"] = new OptionSetValue(962080002),
                    ["ddsm_historyjson"] = CreateHistory(tracer, service, mainEntityRef)
                };

                tracer.Trace("---------Update2------------");

                if (IsKeepUncheckedCategories)
                {
                    foreach (var objFilter in jaFilter.Children<JObject>())
                    {
                        foreach (var propFilter in objFilter.Properties())
                        {
                            energyStarUpdate[propFilter.Value.ToString()] = true;
                        }
                    }
                }

                if (!isNativeTarget)
                {
                    energyStarUpdate["ddsm_startdate"] = begTime;
                    energyStarUpdate["ddsm_isdraft"] = false;
                }

                service.Update(energyStarUpdate);
            }
            catch (Exception e)
            {
                tracer.Trace($"Update.{Environment.NewLine}{e.Message} ");
            }
        }

        private string CreateHistory(ITracingService tracer, IOrganizationService service, EntityReference mainEntityRef)
        {
            var arrNew = new JArray();
            var arrUpdate = new JArray();
            var arrUnchange = new JArray();
            var arrDelete = new JArray();
            var arrCategory = new JArray();
            var barChartData = new JObject();




            //var listNew = new EntityCollection();

            //var pageNumberNew = 1;

            //var multiResponseNew = new RetrieveMultipleResponse();

            //do
            //{
            //    queryNew.PageInfo.Count = LimitMultiRequst;
            //    queryNew.PageInfo.PagingCookie = (pageNumberNew == 1) ? null : multiResponseNew.EntityCollection.PagingCookie;
            //    queryNew.PageInfo.PageNumber = pageNumberNew++;

            //    var multiRequestNew = new RetrieveMultipleRequest { Query = queryNew };
            //    multiResponseNew = (RetrieveMultipleResponse)service.Execute(multiRequestNew);

            //    listNew.Entities.AddRange(multiResponseNew.EntityCollection.Entities);
            //}
            //while (multiResponseNew.EntityCollection.MoreRecords);

            //if (listNew.Entities.Count > 0)
            //{
            //    arrNew.Add(listNew.Entities.Count);
            //}
            //else
            //{
            //    arrNew.Add(0);
            //}

            //// update
            //var queryUp = new QueryExpression
            //{
            //    EntityName = MainEntity,
            //    ColumnSet = new ColumnSet(true),
            //    Criteria = new FilterExpression(LogicalOperator.And)
            //    {
            //        Conditions =
            //        {
            //            new ConditionExpression("ddsm_status", ConditionOperator.Equal, (int)StatusCertLib.Update),
            //            new ConditionExpression("ddsm_type_product", ConditionOperator.Equal, typeCat),
            //            new ConditionExpression("ddsm_certlibsyncid", ConditionOperator.Equal, mainEntityRef.Id),
            //        }
            //    }

            //};
            //queryUp.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, "update");

            //var listUp = new EntityCollection();

            //var pageNumberUp = 1;

            //var multiResponseUp = new RetrieveMultipleResponse();

            //do
            //{
            //    queryUp.PageInfo.Count = LimitMultiRequst;
            //    queryUp.PageInfo.PagingCookie = (pageNumberUp == 1) ? null : multiResponseUp.EntityCollection.PagingCookie;
            //    queryUp.PageInfo.PageNumber = pageNumberUp++;

            //    var multiRequestUp = new RetrieveMultipleRequest { Query = queryUp };
            //    multiResponseUp = (RetrieveMultipleResponse)service.Execute(multiRequestUp);

            //    listUp.Entities.AddRange(multiResponseUp.EntityCollection.Entities);
            //}
            //while (multiResponseUp.EntityCollection.MoreRecords);

            //if (listUp.Entities.Count > 0)
            //{
            //    arrUpdate.Add(listUp.Entities.Count);
            //}
            //else
            //{
            //    arrUpdate.Add(0);
            //}

            //// unchange
            //var queryUnchange = new QueryExpression
            //{
            //    EntityName = MainEntity,
            //    ColumnSet = new ColumnSet(true),
            //    Criteria = new FilterExpression(LogicalOperator.And)
            //    {
            //        Conditions =
            //        {
            //            new ConditionExpression("ddsm_status", ConditionOperator.Equal, (int)StatusCertLib.Unchange),
            //            new ConditionExpression("ddsm_type_product", ConditionOperator.Equal, typeCat),
            //            new ConditionExpression("ddsm_certlibsyncid", ConditionOperator.Equal, mainEntityRef.Id),
            //        }
            //    }

            //};
            //queryUp.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, "update");


            //var listUnchange = new EntityCollection();

            //var pageNumberUnchange = 1;

            //var multiResponseUnchange = new RetrieveMultipleResponse();

            //do
            //{
            //    queryUnchange.PageInfo.Count = LimitMultiRequst;
            //    queryUnchange.PageInfo.PagingCookie = (pageNumberUnchange == 1) ? null : multiResponseUnchange.EntityCollection.PagingCookie;
            //    queryUnchange.PageInfo.PageNumber = pageNumberUnchange++;

            //    var multiRequestUnchange = new RetrieveMultipleRequest { Query = queryUnchange };
            //    multiResponseUnchange = (RetrieveMultipleResponse)service.Execute(multiRequestUnchange);

            //    listUnchange.Entities.AddRange(multiResponseUnchange.EntityCollection.Entities);
            //}
            //while (multiResponseUnchange.EntityCollection.MoreRecords);

            //arrUnchange.Add(listUnchange.Entities.Count > 0 ? listUnchange.Entities.Count : 0);

            //// delete
            //var queryDel = new QueryExpression { EntityName = MainEntity, ColumnSet = new ColumnSet(true) };
            ////queryDel.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, "delete");
            //queryDel.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, (int)StatusCertLib.Delete);
            //queryDel.Criteria.AddCondition("ddsm_type_product", ConditionOperator.Equal, typeCat);
            //queryDel.Criteria.AddCondition("ddsm_certlibsyncid", ConditionOperator.Equal, mainEntityRef.Id);

            //var listDel = new EntityCollection();

            //var pageNumberDel = 1;

            //var multiResponseDel = new RetrieveMultipleResponse();

            //    do
            //    {
            //        queryDel.PageInfo.Count = LimitMultiRequst;
            //        queryDel.PageInfo.PagingCookie = (pageNumberDel == 1) ? null : multiResponseDel.EntityCollection.PagingCookie;
            //        queryDel.PageInfo.PageNumber = pageNumberDel++;

            //        var multiRequestDel = new RetrieveMultipleRequest();
            //        multiRequestDel.Query = queryDel;
            //        multiResponseDel = (RetrieveMultipleResponse)service.Execute(multiRequestDel);

            //        listDel.Entities.AddRange(multiResponseDel.EntityCollection.Entities);
            //    }
            //    while (multiResponseDel.EntityCollection.MoreRecords);

            //    arrDelete.Add(listDel.Entities.Count > 0 ? listDel.Entities.Count : 0);

            //    arrCategory.Add(typeCat);

            //});

            var joNew = new JObject { { "label", "New" }, { "backgroundColor", "#5B97D5" }, { "data", arrNew } };
            var joUp = new JObject { { "label", "Updated" }, { "backgroundColor", "#70AD47" }, { "data", arrUpdate } };
            var joUnchange = new JObject { { "label", "Unchanged" }, { "backgroundColor", "#9E73A2" }, { "data", arrUnchange } };
            var joDel = new JObject { { "label", "Disabled" }, { "backgroundColor", "#ED7D31" }, { "data", arrDelete } };
            var jaDataSet = new JArray { joNew, joUp, joUnchange, joDel };

            barChartData.Add("labels", arrCategory);
            barChartData.Add("datasets", jaDataSet);

            var history = barChartData.ToString(Formatting.None);

            return history;
        }

        //private void CreateAtributes(ITracingService tracer, IOrganizationService service, JArray jaMetaData)
        //{

        //    var multipleRequestFields = new ExecuteMultipleRequest()
        //    {
        //        Settings = new ExecuteMultipleSettings()
        //        {
        //            ContinueOnError = false,
        //            ReturnResponses = true
        //        },
        //        Requests = new OrganizationRequestCollection()
        //    };

        //    foreach (JValue item in jaMetaData)
        //    {
        //        string propName;

        //        propName = "ddsm_" + item.Value.ToString();

        //        var stringAttribute = new StringAttributeMetadata
        //        {
        //            SchemaName = propName,
        //            DisplayName = new Label(item.Value.ToString(), 1033),
        //            RequiredLevel = new AttributeRequiredLevelManagedProperty(AttributeRequiredLevel.None),
        //            Description = new Label(item.Value.ToString(), 1033),
        //            MaxLength = 300
        //        };

        //        var createAttributeRequest = new CreateAttributeRequest
        //        {
        //            EntityName = EntityNameString,
        //            Attribute = stringAttribute
        //        };

        //        multipleRequestFields.Requests.Add(createAttributeRequest);
        //    }

        //    try
        //    {
        //        var multipleResponse = (ExecuteMultipleResponse)service.Execute(multipleRequestFields);
        //    }
        //    catch (Exception e)
        //    {
        //        tracer.Trace($"Exception in Multi.{Environment.NewLine}{e.Message} ");
        //    }
        //}

        //private JArray GetDataLastSync(IOrganizationService service, Dictionary<string, Dictionary<string, string>> dictionaryCertFilter, Dictionary<string, Dictionary<string, string>> dictionarySertifiedES)
        //{
        //    var arrCertTypeCategoryLast = new JArray();
        //    var jaMetaDataFilter1 = new JArray();
        //    var jaMetaDataFilter2 = new JArray();

        //    var columnSet = new ColumnSet(
        //                "ddsm_audio_video",
        //                "ddsm_boilers",
        //                "ddsm_ceiling_fans",
        //                "ddsm_commercial_clothes_washers",
        //                "ddsm_commercial_dishwashers",
        //                "ddsm_commercial_fryers",
        //                "ddsm_commercial_griddles",
        //                "ddsm_commercial_hot_food_holding_cabinet",
        //                "ddsm_commercial_mce_machines",
        //                "ddsm_commercial_ovens",
        //                "ddsm_commercial_refrigerators_and_freezers",
        //                "ddsm_commercial_steam_cookers",
        //                "ddsm_commercial_water_heaters",
        //                "ddsm_computers",
        //                "ddsm_data_center_storage",
        //                "ddsm_decorative_light_strings",
        //                "ddsm_dehumidifiers",
        //                "ddsm_displays",
        //                "ddsm_enterprise_servers",
        //                "ddsm_furnaces",
        //                "ddsm_geothermal_heat_pumps",
        //                "ddsm_imaging_equipment",
        //                "ddsm_large_network_equipment",
        //                "ddsm_light_bulbs",
        //                "ddsm_light_commercial_hvac",
        //                "ddsm_light_fixtures",
        //                "ddsm_non_ahri_cent_air_con_heat_pum",
        //                "ddsm_pool_pumps",
        //                "ddsm_products_lighting",
        //                "ddsm_products_non_lighting",
        //                "ddsm_residential_clothes_dryers",
        //                "ddsm_residential_clothes_washers",
        //                "ddsm_residential_dishwashers",
        //                "ddsm_residential_freezers",
        //                "ddsm_residential_refrigerators",
        //                "ddsm_roof_products",
        //                "ddsm_room_air_conditioners",
        //                "ddsm_set_top_boxes",
        //                "ddsm_small_network_equipment",
        //                "ddsm_telephones",
        //                "ddsm_televisions",
        //                "ddsm_uninterruptible_power_supplies",
        //                "ddsm_vending_machines",
        //                "ddsm_ventilating_fans",
        //                "ddsm_water_coolers",
        //                "ddsm_water_heaters",
        //                "ddsm_historic_room_air_cleaners",
        //                "ddsm_certlibsyncid");

        //    var query = new QueryExpression
        //    {
        //        EntityName = "ddsm_certlibsync",
        //        ColumnSet = columnSet,
        //        TopCount = 1,
        //        Criteria = new FilterExpression(LogicalOperator.And)
        //        {
        //            Conditions =
        //            {
        //                new ConditionExpression("ddsm_status", ConditionOperator.Equal, 962080002)
        //            }
        //        },
        //        Orders = { new OrderExpression("createdon", OrderType.Descending) }
        //    };

        //    var listCertLibSync = service.RetrieveMultiple(query);

        //    var certLibSync = listCertLibSync.Entities[0];
        //    if (certLibSync == null) return jaMetaDataFilter2;

        //    foreach (var attribute in certLibSync.Attributes)
        //    {
        //        if (attribute.Key == "ddsm_certlibsyncid")
        //        {
        //            PreviousCertLibSyncId = new Guid(attribute.Value.ToString());
        //        }
        //        else
        //        {
        //            if ((bool)attribute.Value)
        //            {
        //                if (dictionaryCertFilter.Count > 0)
        //                {
        //                    foreach (var pair in dictionaryCertFilter)
        //                    {
        //                        foreach (var pairValue in pair.Value)
        //                        {
        //                            if (pairValue.Value != attribute.Key)
        //                            {
        //                                jaMetaDataFilter1.Add(new JObject(new JProperty("Name", attribute.Key)));
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    jaMetaDataFilter1.Add(new JObject(new JProperty("Name", attribute.Key)));

        //                }
        //            }
        //        }
        //    }

        //    JArrayMetaDataFilter = jaMetaDataFilter1;

        //    foreach (var jObjFilter1 in jaMetaDataFilter1.Children<JObject>())
        //    {
        //        foreach (var propFilter1 in jObjFilter1.Properties())
        //        {
        //            foreach (var pairFilter1 in dictionarySertifiedES)
        //            {
        //                foreach (var pairValue1 in pairFilter1.Value)
        //                {
        //                    if (pairValue1.Value == propFilter1.Value.ToString())
        //                    {
        //                        jaMetaDataFilter2.Add(new JObject(new JProperty("Name", pairFilter1.Key)));
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return jaMetaDataFilter2;
        //}

    }
}

