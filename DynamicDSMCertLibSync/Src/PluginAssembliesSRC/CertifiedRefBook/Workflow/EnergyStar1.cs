﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json.Linq;
using System;
using System.Activities;
using CertifiedRefBook.Models.Es;
using CertifiedRefBook.Utils;

namespace CertifiedRefBook
{


    public class EnergyStar1 : CodeActivity
    {
        public const string EntityNameString = "ddsm_certlib";
        public const int LimitMultiRequst = 250;

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        public int NewCount { get; set; }

        public int UpdatedCount { get; set; }

        public int DeletedCount { get; set; }

        public int UnchangedCount { get; set; }

        public Guid PreviousCertLibSyncId { get; set; }

        private EnergyStarHelper _helper;

        private EsConfig _config;

        public JArray JArrayMetaDataFilter { get; set; }

        public bool IsKeepUncheckedCategories { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            NewCount = 0;
            UpdatedCount = 0;
            DeletedCount = 0;
            UnchangedCount = 0;
            PreviousCertLibSyncId = Guid.Empty;
            JArrayMetaDataFilter = null;
            IsKeepUncheckedCategories = false;

            var tracer = executionContext.GetExtension<ITracingService>();
            var context = executionContext.GetExtension<IWorkflowContext>();
            var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            var service = serviceFactory.CreateOrganizationService(context.UserId);


            _helper = new EnergyStarHelper(service);
            _config = _helper.Config;
            tracer.Trace("---------start------------");

            tracer.Trace("Target1 = " + context.InputParameters["Target"]);

            var isNativeTarget = true;
            EntityReference mainEntityRef;

            if (context.InputParameters["Target"] is Entity)
            {
                var entity = context.InputParameters["Target"] as Entity;
                mainEntityRef = entity.ToEntityReference();
            }
            else
            {
                var entity = context.InputParameters["Target"] as EntityReference;

                if (entity.LogicalName == "ddsm_schedule")
                {
                    var entit = new Entity("ddsm_certlibsync");
                    service.Create(entit);

                    var query = new QueryExpression { EntityName = "ddsm_certlibsync", ColumnSet = new ColumnSet(true) };
                    query.TopCount = 1;
                    query.AddOrder("createdon", OrderType.Descending);

                    var listCertLibSync = service.RetrieveMultiple(query);

                    var certLibSync = listCertLibSync.Entities[0];

                    mainEntityRef = certLibSync.ToEntityReference();

                    isNativeTarget = false;
                }
                else
                {
                    mainEntityRef = (EntityReference)context.InputParameters["Target"];
                }
            }

            var jaMetaData = new JArray("pd_id",
                                    "brand_name",
                                    "model_name",
                                    "model_number",
                                    "energy_star_partner",
                                    "date_available_on_market",
                                    "date_qualified",
                                    "certified",
                                    "type_product",
                                    "is_delete",
                                    "status");

           // RunAction(tracer, service, jaMetaData, mainEntityRef, isNativeTarget);

            Complete.Set(executionContext, true);

            tracer.Trace("---------end------------");
        }

        //private void RunAction(ITracingService tracer, IOrganizationService service, JArray jaMetaData, EntityReference mainEntityRef, bool isNativeTarget)
        //{
        //    var begTime = DateTime.Now;
        //    // string generateName = "SyncOn_" + begTime.ToLongDateString(); 

        //    JArray arrCertAllData = null;
        //    JArray arrCertData = null;
        //    var arrCertTypeCategoryLast = new JArray();

        //    //    JArray jaMetaData1 = new JArray();
        //    var jaMetaDataLastSync = new JArray();

        //    var sqlLimit = "?$limit=50000&$offset=0&$$exclude_system_fields=false";
        //    var sql = string.Empty;
        //    //            string createSql = "?$limit=50000&$offset=0&$$exclude_system_fields=false&$where= :created_at between '2011-03-04' and '2016-03-04'";

        //    var dictionarySertifiedES = new Dictionary<string, Dictionary<string, string>>()
        //    {
        //       { "Audio Video", new Dictionary<string, string>() { { "crdd-rykg.json" + sqlLimit + sql, "ddsm_audio_video" } } },
        //       { "Boilers", new Dictionary<string, string>() { { "y465-hf45.json" + sqlLimit  +  sql, "ddsm_boilers" } } },
        //       { "Ceiling Fans", new Dictionary<string, string>() { { "5dsh-4p9f.json" + sqlLimit + sql, "ddsm_ceiling_fans" } } },
        //       { "Commercial Clothes Washers", new Dictionary<string, string>() { { "rtgq-se74.json" + sqlLimit  +  sql, "ddsm_commercial_clothes_washers" } } },
        //       { "Commercial Dishwashers", new Dictionary<string, string>() { { "rmp2-53xx.json" + sqlLimit  +  sql, "ddsm_commercial_dishwashers" } } },
        //       { "Commercial Fryers", new Dictionary<string, string>() { { "aqwf-f28t.json" + sqlLimit  +  sql, "ddsm_commercial_fryers" } } },
        //       { "Commercial Griddles", new Dictionary<string, string>() { { "3r8r-x4sy.json" + sqlLimit  +  sql, "ddsm_commercial_griddles" } } },
        //       { "Commercial Hot Food Holding Cabinet", new Dictionary<string, string>() { { "7ben-3gbg.json" + sqlLimit  +  sql, "ddsm_commercial_hot_food_holding_cabinet" } } },
        //       { "Commercial Ice Machines", new Dictionary<string, string>() { { "ds2x-qrj6.json" + sqlLimit  +  sql, "ddsm_commercial_mce_machines" } } },
        //       { "Commercial Ovens", new Dictionary<string, string>() { { "r4d8-qn8t.json" + sqlLimit + sql, "ddsm_commercial_ovens" } } },
        //       { "Commercial Refrigerators and Freezers", new Dictionary<string, string>() { { "ebvx-pb7r.json" + sqlLimit  +  sql, "ddsm_commercial_refrigerators_and_freezers" } } },
        //       { "Commercial Steam Cookers", new Dictionary<string, string>() { { "2nsb-9kvh.json" + sqlLimit + sql, "ddsm_commercial_steam_cookers" } } },
        //       { "Commercial Water Heaters", new Dictionary<string, string>() { { "cjfa-92ur.json" + sqlLimit + sql, "ddsm_commercial_water_heaters" } } },
        //       { "Computers", new Dictionary<string, string>() { { "snk9-ygek.json" + sqlLimit + sql, "ddsm_computers" } } },
        //       { "Data Center Storage", new Dictionary<string, string>() { { "aptq-envv.json" + sqlLimit + sql, "ddsm_data_center_storage" } } },
        //       { "Decorative Light Strings", new Dictionary<string, string>() { { "743m-e894.json" + sqlLimit + sql, "ddsm_decorative_light_strings" } } },
        //       { "Dehumidifiers", new Dictionary<string, string>() { { "nnzm-ye2i.json" + sqlLimit + sql, "ddsm_dehumidifiers" } } },
        //       { "Displays", new Dictionary<string, string>() { { "xpzk-vasr.json" + sqlLimit + sql, "ddsm_displays" } } },
        //       { "Enterprise Servers", new Dictionary<string, string>() { { "ikxg-vjvt.json" + sqlLimit + sql, "ddsm_enterprise_servers" } } },
        //       { "Furnaces", new Dictionary<string, string>() { { "w8xr-y37j.json" + sqlLimit + sql, "ddsm_furnaces" } } },
        //       { "Geothermal Heat Pumps", new Dictionary<string, string>() { { "64ea-ksra.json" + sqlLimit + sql, "ddsm_geothermal_heat_pumps" } } },
        //       { "Imaging Equipment", new Dictionary<string, string>() { { "dw2k-vpei.json" + sqlLimit + sql, "ddsm_imaging_equipment" } } },
        //       { "Large Network Equipment", new Dictionary<string, string>() { { "yhae-jcp4.json" + sqlLimit + sql, "ddsm_large_network_equipment" } } },
        //       { "Light Bulbs", new Dictionary<string, string>() { { "7k7b-bh22.json" + sqlLimit + sql, "ddsm_light_bulbs" } } },
        //       { "Light Commercial HVAC", new Dictionary<string, string>() { { "3w3i-37ae.json" + sqlLimit + sql, "ddsm_light_commercial_hvac" } } },
        //       { "Light Fixtures", new Dictionary<string, string>() { { "3pic-tayf.json" + sqlLimit + sql, "ddsm_light_fixtures" } } },
        //       { "Non-AHRI Central Air Conditioner Equipment and Air Source Heat Pump", new Dictionary<string, string>() { { "sy8m-4bug.json" + sqlLimit + sql, "ddsm_non_ahri_cent_air_con_heat_pum" } } },
        //       { "Pool Pumps", new Dictionary<string, string>() { { "kp2q-4r2a.json" + sqlLimit + sql, "ddsm_pool_pumps" } } },
        //       { "Products-Lighting", new Dictionary<string, string>() { { "8bjc-dg2y.json" + sqlLimit + sql, "ddsm_products_lighting" } } },
        //       { "Products-Non-lighting", new Dictionary<string, string>() { { "4482-3mef.json" + sqlLimit + sql, "ddsm_products_non_lighting" } } },
        //       { "Residential Clothes Dryers", new Dictionary<string, string>() { { "cv4u-mmnf.json" + sqlLimit + sql, "ddsm_residential_clothes_dryers" } } },
        //       { "Residential Clothes Washers", new Dictionary<string, string>() { { "k5sb-ibyp.json" + sqlLimit + sql, "ddsm_residential_clothes_washers" } } },
        //       { "Residential Dishwashers", new Dictionary<string, string>() { { "v32c-ywkg.json" + sqlLimit + sql, "ddsm_residential_dishwashers" } } },
        //       { "Residential Freezers", new Dictionary<string, string>() { { "37hj-nayy.json" + sqlLimit + sql, "ddsm_residential_freezers" } } },
        //       { "Residential Refrigerators", new Dictionary<string, string>() { { "ymjh-yrse.json" + sqlLimit + sql, "ddsm_residential_refrigerators" } } },
        //       { "Roof Products", new Dictionary<string, string>() { { "32iu-u43z.json" + sqlLimit + sql, "ddsm_roof_products" } } },
        //       { "Room Air Conditioners", new Dictionary<string, string>() { { "gwgp-353b.json" + sqlLimit + sql, "ddsm_room_air_conditioners" } } },
        //       { "Set Top Boxes", new Dictionary<string, string>() { { "c2ej-f2sh.json" + sqlLimit + sql, "ddsm_set_top_boxes" } } },
        //       { "Small Network Equipment", new Dictionary<string, string>() { { "pi2x-8jju.json" + sqlLimit + sql, "ddsm_small_network_equipment" } } },
        //       { "Telephones", new Dictionary<string, string>() { { "8vp9-ypaw.json" + sqlLimit + sql, "ddsm_telephones" } } },
        //       { "Televisions", new Dictionary<string, string>() { { "wrfq-4shy.json" + sqlLimit + sql, "ddsm_televisions" } } },
        //       { "Uninterruptible Power Supplies", new Dictionary<string, string>() { { "yq5h-4utf.json" + sqlLimit + sql, "ddsm_uninterruptible_power_supplies" } } },
        //       { "Vending Machines", new Dictionary<string, string>() { { "iacv-es29.json" + sqlLimit + sql, "ddsm_vending_machines" } } },
        //       { "Ventilating Fans", new Dictionary<string, string>() { { "fjr3-adps.json" + sqlLimit + sql, "ddsm_ventilating_fans" } } },
        //       { "Water Coolers", new Dictionary<string, string>() { { "7si9-xk4v.json" + sqlLimit + sql, "ddsm_water_coolers" } } },
        //       { "Water Heaters", new Dictionary<string, string>() { { "6ruy-6ykc.json" + sqlLimit + sql, "ddsm_water_heaters" } } },
        //       { "HISTORIC Room Air Cleaners", new Dictionary<string, string>() { { "2skb-py7k.json" + sqlLimit + sql, "ddsm_historic_room_air_cleaners" } } }
        //    };

        //    var dictionaryCertFilter = GetFilteringTemplete(service, dictionarySertifiedES);

        //    /*
        //    Dictionary<string, Dictionary<string, string>> dictionaryCertFilter = new Dictionary<string, Dictionary<string, string>>();

        //    foreach (JObject jObj in arrTypeCategory.Children<JObject>())
        //    {
        //        foreach (JProperty prop in jObj.Properties())
        //        {
        //            string urlCategory = string.Empty;

        //            if (prop.Name != "NotSelected")
        //            {
        //                foreach (KeyValuePair<string, Dictionary<string, string>> pair in dictionarySertifiedES)
        //                {
        //                    if (pair.Key == prop.Name)
        //                    {
        //                        foreach (KeyValuePair<string, string> pair1 in pair.Value)
        //                        {
        //                            dictionaryCertFilter.Add(prop.Name, new Dictionary<string, string>() { { pair1.Key, pair1.Value } });
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    */

        //    if (IsKeepUncheckedCategories)
        //    {
        //        jaMetaDataLastSync = GetDataLastSync(service, dictionaryCertFilter, dictionarySertifiedES);
        //        /*
        //        foreach (var child in objTypeCategoryLast)
        //        {
        //            if (child.Key == "ddsm_certlibsyncid")
        //            {
        //                PreviousCertLibSyncId = new Guid(child.Value.SelectToken("value").ToString());
        //            }

        //            if (child.Value.SelectToken("type").ToString() == "boolean")
        //            {
        //                string name = child.Key;
        //                string val = child.Value.SelectToken("value").ToString().ToLower();

        //                if (val == "true")
        //                {
        //                    arrCertTypeCategoryLast.Add(new JObject(new JProperty(name, val)));
        //                }
        //            }
        //        }

        //        foreach (JObject jObj2 in arrCertTypeCategoryLast.Children<JObject>())
        //        {
        //            foreach (JProperty prop3 in jObj2.Properties())
        //            {
        //                foreach (KeyValuePair<string, Dictionary<string, string>> pair2 in dictionarySertifiedES)
        //                {
        //                    foreach (KeyValuePair<string, string> pair3 in pair2.Value)
        //                    {
        //                        if (prop3.Name == pair3.Value)
        //                        {
        //                            jaMetaData1.Add(new JObject(new JProperty("Name", pair2.Key)));
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        foreach (JObject jObj21 in jaMetaData1.Children<JObject>())
        //        {
        //            foreach (JProperty prop31 in jObj21.Properties())
        //            {
        //                foreach (KeyValuePair<string, Dictionary<string, string>> pair21 in dictionaryCertFilter)
        //                {
        //                        if (pair21.Key != prop31.Value.ToString())
        //                        {
        //                            jaMetaData11.Add(new JObject(new JProperty("Name", prop31.Value.ToString())));
        //                        }
        //                }
        //            }
        //        }
        //        */
        //    }

        //    CreateAtributes(tracer, service, jaMetaData);

        //    using (var client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } })
        //    {

        //        client.Headers.Add("X-App-Token", _config.Token);
        //        client.BaseAddress = _config.Url + "resource/";

        //        foreach (var keyVal in dictionaryCertFilter)
        //        {
        //            var prodType = keyVal.Key;
        //            var url = string.Empty;

        //            foreach (var keyVal1 in keyVal.Value)
        //            {
        //                url = keyVal1.Key;

        //                var response = client.DownloadString(url);

        //                // if (response.IsSuccessStatusCode)
        //                //{
        //                if (arrCertAllData != null)
        //                {
        //                    arrCertData = new JArray(JsonConvert.DeserializeObject<JArray>(response));

        //                    foreach (var jObj in arrCertData.Children<JObject>())
        //                    {
        //                        jObj.Add("type_product", prodType);
        //                        jObj.Add("certified", "Energy Star");
        //                        jObj.Add("is_delete", "false");
        //                        jObj.Add("status", "");
        //                        jObj.Add("certlibsyncid", mainEntityRef.Id.ToString());
        //                    }

        //                    arrCertAllData.Merge(arrCertData, new JsonMergeSettings
        //                    {
        //                        MergeArrayHandling = MergeArrayHandling.Concat
        //                    });
        //                }
        //                else
        //                {
        //                    arrCertAllData = JsonConvert.DeserializeObject<JArray>(response);

        //                    foreach (var jObj in arrCertAllData.Children<JObject>())
        //                    {
        //                        jObj.Add("type_product", prodType);
        //                        jObj.Add("certified", "Energy Star");
        //                        jObj.Add("is_delete", "false");
        //                        jObj.Add("status", "");
        //                        jObj.Add("certlibsyncid", mainEntityRef.Id.ToString());
        //                    }
        //                }
        //                //}
        //            }
        //        }
        //    }

        //    var multipleRequestDel = new ExecuteMultipleRequest()
        //    {
        //        Settings = new ExecuteMultipleSettings()
        //        {
        //            ContinueOnError = false,
        //            ReturnResponses = true
        //        },
        //        Requests = new OrganizationRequestCollection()
        //    };

        //    var multipleRequestUp = new ExecuteMultipleRequest()
        //    {
        //        Settings = new ExecuteMultipleSettings()
        //        {
        //            ContinueOnError = false,
        //            ReturnResponses = true
        //        },
        //        Requests = new OrganizationRequestCollection()
        //    };

        //    var multipleRequestUnchange = new ExecuteMultipleRequest()
        //    {
        //        Settings = new ExecuteMultipleSettings()
        //        {
        //            ContinueOnError = false,
        //            ReturnResponses = true
        //        },
        //        Requests = new OrganizationRequestCollection()
        //    };

        //    var multipleRequestNew = new ExecuteMultipleRequest()
        //    {
        //        Settings = new ExecuteMultipleSettings()
        //        {
        //            ContinueOnError = false,
        //            ReturnResponses = true
        //        },
        //        Requests = new OrganizationRequestCollection()
        //    };

        //    var multipleRequestNewUp = new ExecuteMultipleRequest()
        //    {
        //        Settings = new ExecuteMultipleSettings()
        //        {
        //            ContinueOnError = false,
        //            ReturnResponses = true
        //        },
        //        Requests = new OrganizationRequestCollection()
        //    };

        //    //   int deleteCountObj = 0;
        //    //   int updateCountObj = 0;
        //    //   int newCountObj = 0;

        //    var listEnergyStarDel = new EntityCollection();
        //    var listEnergyStarUp = new EntityCollection();
        //    var listEnergyStarNew = new EntityCollection();
        //    var listEnergyStarUpLast = new EntityCollection();
        //    var listEnergyStarUnchange = new EntityCollection();

        //    if (IsKeepUncheckedCategories)
        //    {
        //        foreach (var jObjLastSync in jaMetaDataLastSync.Children<JObject>())
        //        {
        //            foreach (var propLastSync in jObjLastSync.Properties())
        //            {
        //                var queryAllLast = new QueryExpression { EntityName = EntityNameString, ColumnSet = new ColumnSet(true) };
        //                queryAllLast.Criteria.AddCondition("ddsm_certlibsyncid", ConditionOperator.Equal, PreviousCertLibSyncId);
        //                queryAllLast.Criteria.AddCondition("ddsm_type_product", ConditionOperator.Equal, propLastSync.Value.ToString());
        //                queryAllLast.Criteria.AddCondition("ddsm_is_delete", ConditionOperator.Equal, "false");

        //                var listEnergyStarAllLast = new EntityCollection();

        //                var pageNumberLast = 1;
        //                RetrieveMultipleRequest multiRequestLast;

        //                var multiResponseLast = new RetrieveMultipleResponse();

        //                do
        //                {
        //                    queryAllLast.PageInfo.Count = LimitMultiRequst;
        //                    queryAllLast.PageInfo.PagingCookie = (pageNumberLast == 1) ? null : multiResponseLast.EntityCollection.PagingCookie;
        //                    queryAllLast.PageInfo.PageNumber = pageNumberLast++;

        //                    multiRequestLast = new RetrieveMultipleRequest();
        //                    multiRequestLast.Query = queryAllLast;
        //                    multiResponseLast = (RetrieveMultipleResponse)service.Execute(multiRequestLast);

        //                    listEnergyStarUnchange.Entities.AddRange(multiResponseLast.EntityCollection.Entities);
        //                }
        //                while (multiResponseLast.EntityCollection.MoreRecords);

        //            }
        //        }

        //        listEnergyStarUnchange.Entities.ToList().ForEach(ent =>
        //        {
        //            ent["ddsm_certlibsyncid"] = mainEntityRef;
        //            ent["ddsm_status"] = new OptionSetValue((int)StatusCertLib.Unchange);
        //            //ent["ddsm_status"] = "update";

        //        });
        //    }

        //    var queryAll = new QueryExpression { EntityName = EntityNameString, ColumnSet = new ColumnSet(true) };
        //    queryAll.Criteria.AddCondition("ddsm_is_delete", ConditionOperator.Equal, "false");
        //    // EntityCollection listEnergyStarRange = new EntityCollection(); //service.RetrieveMultiple(queryDel);
        //    var listEnergyStarAll = new EntityCollection();

        //    var pageNumber = 1;
        //    RetrieveMultipleRequest multiRequest;

        //    var multiResponse = new RetrieveMultipleResponse();

        //    do
        //    {
        //        queryAll.PageInfo.Count = LimitMultiRequst;
        //        queryAll.PageInfo.PagingCookie = (pageNumber == 1) ? null : multiResponse.EntityCollection.PagingCookie;
        //        queryAll.PageInfo.PageNumber = pageNumber++;

        //        multiRequest = new RetrieveMultipleRequest();
        //        multiRequest.Query = queryAll;
        //        multiResponse = (RetrieveMultipleResponse)service.Execute(multiRequest);

        //        listEnergyStarAll.Entities.AddRange(multiResponse.EntityCollection.Entities);
        //    }
        //    while (multiResponse.EntityCollection.MoreRecords);

        //    if (listEnergyStarAll.Entities.Count > 0)
        //    {
        //        //                EntityCollection listEnergyStarDel = new EntityCollection();
        //        //              EntityCollection listEnergyStarUp = new EntityCollection();
        //        //            EntityCollection listEnergyStarNew = new EntityCollection();

        //        foreach (var entityDb in listEnergyStarAll.Entities)
        //        {
        //            var idDb = entityDb.Attributes["ddsm_pd_id"].ToString();

        //            var entityES = arrCertAllData.Where(p => p.SelectToken("pd_id").ToString() == idDb).FirstOrDefault();

        //            if (entityES == null)
        //            {
        //                if (!IsKeepUncheckedCategories)
        //                {
        //                    entityDb["ddsm_is_delete"] = "true";
        //                    // entityDb["ddsm_status"] = "delete";
        //                    entityDb["ddsm_status"] = new OptionSetValue((int)StatusCertLib.Delete);
        //                    entityDb["ddsm_certlibsyncid"] = mainEntityRef;

        //                    entityDb["statecode"] = new OptionSetValue(1);
        //                    entityDb["statuscode"] = new OptionSetValue(2);

        //                    listEnergyStarDel.Entities.Add(entityDb);
        //                }
        //            }
        //            else
        //            {
        //                var brand_nameES = entityES.Value<string>("brand_name") ?? string.Empty;
        //                var model_nameES = entityES.Value<string>("model_name") ?? string.Empty;
        //                var model_numberES = entityES.Value<string>("model_number") ?? string.Empty;
        //                var energy_star_partnerES = entityES.Value<string>("energy_star_partner") ?? string.Empty;
        //                var date_available_on_marketES = entityES.Value<string>("date_available_on_market") ?? string.Empty;
        //                var date_qualifiedES = entityES.Value<string>("date_qualified") ?? string.Empty;

        //                object brand_nameDb1 = null;
        //                entityDb.Attributes.TryGetValue("ddsm_brand_name", out brand_nameDb1);
        //                var brand_nameDb = brand_nameDb1 != null ? brand_nameDb1.ToString() : string.Empty;
        //                //string brand_nameDb = entityDb["ddsm_brand_name"].ToString() != null ? entityDb["ddsm_brand_name"].ToString() : string.Empty;

        //                var model_nameDb = entityDb.Contains("ddsm_model_name") ? entityDb["ddsm_model_name"].ToString() : string.Empty;

        //                object model_numberDb1 = null;
        //                entityDb.Attributes.TryGetValue("ddsm_model_number", out model_numberDb1);
        //                var model_numberDb = model_numberDb1 != null ? model_numberDb1.ToString() : string.Empty;
        //                //string model_numberDb = entityDb["ddsm_model_number"].ToString() != null ? entityDb["ddsm_model_number"].ToString() : string.Empty;

        //                var energy_star_partnerDb = entityDb.Contains("ddsm_energy_star_partner") ? entityDb["ddsm_energy_star_partner"].ToString() : string.Empty;
        //                var date_available_on_marketDb = entityDb.Contains("ddsm_date_available_on_market") ? entityDb["ddsm_date_available_on_market"].ToString() : string.Empty;
        //                var date_qualifiedDb = entityDb.Contains("ddsm_date_qualified") ? entityDb["ddsm_date_qualified"].ToString() : string.Empty;

        //                var date_available_on_marketESConvert = Convert.ToDateTime(date_available_on_marketES);
        //                var date_qualifiedESConvert = Convert.ToDateTime(date_qualifiedES);
        //                var date_available_on_marketStringES = date_available_on_marketESConvert.ToString("dd.MM.yyyy");
        //                var date_qualifiedStringES = date_qualifiedESConvert.ToString("dd.MM.yyyy");

        //                var date_available_on_marketDbConvert = Convert.ToDateTime(entityDb["ddsm_date_available_on_market"].ToString());
        //                var date_qualifiedDbConvert = Convert.ToDateTime(entityDb["ddsm_date_qualified"].ToString());
        //                var date_available_on_marketStringDb = date_available_on_marketDbConvert.ToString("dd.MM.yyyy");
        //                var date_qualifiedStringDb = date_qualifiedDbConvert.ToString("dd.MM.yyyy");

        //                if (brand_nameDb != brand_nameES ||
        //                    model_nameDb != model_nameES ||
        //                    model_numberDb != model_numberES ||
        //                    energy_star_partnerDb != energy_star_partnerES ||
        //                    date_available_on_marketStringDb != date_available_on_marketStringES ||
        //                    date_qualifiedStringDb != date_qualifiedStringES)

        //                {
        //                    entityDb["ddsm_brand_name"] = brand_nameES;
        //                    entityDb["ddsm_model_name"] = model_nameES;
        //                    entityDb["ddsm_model_number"] = model_numberES;
        //                    entityDb["ddsm_energy_star_partner"] = energy_star_partnerES;
        //                    entityDb["ddsm_date_available_on_market"] = date_available_on_marketES;
        //                    entityDb["ddsm_date_qualified"] = date_qualifiedES;
        //                    entityDb["ddsm_certlibsyncid"] = mainEntityRef;
        //                    entityDb["ddsm_status"] = new OptionSetValue((int)StatusCertLib.Update);
        //                    //entityDb["ddsm_status"] = "update";

        //                    listEnergyStarUp.Entities.Add(entityDb);
        //                }
        //                else
        //                {
        //                    entityDb["ddsm_brand_name"] = entityES.Value<string>("brand_name") ?? string.Empty;
        //                    entityDb["ddsm_model_name"] = entityES.Value<string>("model_name") ?? string.Empty;
        //                    entityDb["ddsm_model_number"] = entityES.Value<string>("model_number") ?? string.Empty;
        //                    entityDb["ddsm_energy_star_partner"] = entityES.Value<string>("energy_star_partner") ?? string.Empty;
        //                    entityDb["ddsm_date_available_on_market"] = entityES.Value<string>("date_available_on_market") ?? string.Empty;
        //                    entityDb["ddsm_date_qualified"] = entityES.Value<string>("date_qualified") ?? string.Empty;
        //                    entityDb["ddsm_certlibsyncid"] = mainEntityRef;
        //                    entityDb["ddsm_status"] = new OptionSetValue((int)StatusCertLib.Unchange);

        //                    listEnergyStarUnchange.Entities.Add(entityDb);
        //                }
        //            }
        //        }

        //        foreach (var itemNew in arrCertAllData.Children<JObject>())
        //        {
        //            var pd_id = itemNew.SelectToken("pd_id").ToString();

        //            var entityES = listEnergyStarAll.Entities.ToList().Where(p => p.Attributes["ddsm_pd_id"].ToString() == pd_id).FirstOrDefault();

        //            if (entityES == null)
        //            {
        //                var energystarcertified1 = new Entity(EntityNameString);

        //                foreach (var prop in itemNew.Properties())
        //                {
        //                    var propName = prop.Name;

        //                    foreach (JValue jVal in jaMetaData)
        //                    {
        //                        if (propName == jVal.ToString())
        //                        {
        //                            var propVal = string.Empty;

        //                            if (prop.HasValues)
        //                            {
        //                                propVal = prop.Value.ToString().Length > 300 ? prop.Value.ToString().Substring(0, 300) : prop.Value.ToString();
        //                            }

        //                            energystarcertified1.Attributes.Add("ddsm_" + propName.ToString(), propVal);
        //                        }
        //                    }

        //                    if (propName == "certlibsyncid")
        //                    {
        //                        energystarcertified1.Attributes.Add("ddsm_" + propName.ToString(), mainEntityRef);
        //                    }

        //                    //    if (propName == "status")
        //                    //   {
        //                    //        energystarcertified1.Attributes.Add("ddsm_" + propName.ToString(), "new");
        //                    //   }
        //                }

        //                var brandName = energystarcertified1.Contains("ddsm_brand_name") ? energystarcertified1.Attributes["ddsm_brand_name"].ToString() : string.Empty;
        //                var modName = energystarcertified1.Contains("ddsm_model_name") ? energystarcertified1.Attributes["ddsm_model_name"].ToString() : string.Empty;
        //                var fullName = brandName + " - " + modName;

        //                energystarcertified1.Attributes.Add("ddsm_name", fullName);
        //                //energystarcertified1.Attributes["ddsm_status"] = "new";
        //                energystarcertified1.Attributes["ddsm_status"] = new OptionSetValue((int)StatusCertLib.New);
        //                energystarcertified1.Attributes["ddsm_certlibsyncid"] = mainEntityRef;

        //                listEnergyStarNew.Entities.Add(energystarcertified1);
        //            }
        //        }

        //        if (listEnergyStarDel.Entities.Count > 0)
        //        {
        //            var countObjDel = listEnergyStarDel.Entities.Count;

        //            var countStepMultiDel = (int)Math.Floor((decimal)(listEnergyStarDel.Entities.Count / LimitMultiRequst));
        //            var countMultiDel = countStepMultiDel * LimitMultiRequst;
        //            var countSingleDel = listEnergyStarDel.Entities.Count - countMultiDel;

        //            foreach (var entityDbDel in listEnergyStarDel.Entities)
        //            {
        //                var delRequest = new UpdateRequest { Target = entityDbDel };
        //                multipleRequestDel.Requests.Add(delRequest);

        //                if (multipleRequestDel.Requests.Count == LimitMultiRequst)
        //                {
        //                    try
        //                    {
        //                        var multipleResponseRow = (ExecuteMultipleResponse)service.Execute(multipleRequestDel);
        //                        multipleRequestDel.Requests.Clear();

        //                        countObjDel -= LimitMultiRequst;

        //                        DeletedCount += LimitMultiRequst;
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        tracer.Trace(string.Format("Exception in Multi.{0}{1} ", System.Environment.NewLine, e.Message));
        //                    }
        //                }

        //                else if (countObjDel <= countSingleDel)
        //                {
        //                    try
        //                    {
        //                        service.Update(entityDbDel);
        //                        multipleRequestDel.Requests.Clear();

        //                        DeletedCount++;
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        tracer.Trace(string.Format("Exception in Single.{0}{1} ", System.Environment.NewLine, e.Message));
        //                    }
        //                }
        //            }
        //        }

        //        if (listEnergyStarUnchange.Entities.Count > 0)
        //        {
        //            var countObjUnchange = listEnergyStarUnchange.Entities.Count;

        //            var countStepMultiUnchange = (int)Math.Floor((decimal)(listEnergyStarUnchange.Entities.Count / LimitMultiRequst));
        //            var countMultiUnchange = countStepMultiUnchange * LimitMultiRequst;
        //            var countSingleUnchange = listEnergyStarUnchange.Entities.Count - countMultiUnchange;

        //            foreach (var entityDbUnchange in listEnergyStarUnchange.Entities)
        //            {
        //                var unchangeRequest = new UpdateRequest { Target = entityDbUnchange };
        //                multipleRequestUnchange.Requests.Add(unchangeRequest);

        //                if (multipleRequestUnchange.Requests.Count == LimitMultiRequst)
        //                {
        //                    try
        //                    {
        //                        var multipleResponseRow = (ExecuteMultipleResponse)service.Execute(multipleRequestUnchange);
        //                        multipleRequestUnchange.Requests.Clear();

        //                        countObjUnchange -= LimitMultiRequst;

        //                        UnchangedCount += LimitMultiRequst;
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        tracer.Trace(string.Format("Exception in Multi.{0}{1} ", System.Environment.NewLine, e.Message));
        //                    }
        //                }

        //                else if (countObjUnchange <= countSingleUnchange)
        //                {
        //                    try
        //                    {
        //                        service.Update(entityDbUnchange);
        //                        multipleRequestUp.Requests.Clear();

        //                        UnchangedCount++;
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        tracer.Trace(string.Format("Exception in Single.{0}{1} ", System.Environment.NewLine, e.Message));
        //                    }
        //                }
        //            }
        //        }

        //        if (listEnergyStarUp.Entities.Count > 0)
        //        {
        //            var countObjUp = listEnergyStarUp.Entities.Count;

        //            var countStepMultiUp = (int)Math.Floor((decimal)(listEnergyStarUp.Entities.Count / LimitMultiRequst));
        //            var countMultiUp = countStepMultiUp * LimitMultiRequst;
        //            var countSingleUp = listEnergyStarUp.Entities.Count - countMultiUp;

        //            foreach (var entityDbUp in listEnergyStarUp.Entities)
        //            {
        //                var updateRequest = new UpdateRequest { Target = entityDbUp };
        //                multipleRequestUp.Requests.Add(updateRequest);

        //                if (multipleRequestUp.Requests.Count == LimitMultiRequst)
        //                {
        //                    try
        //                    {
        //                        var multipleResponseRow = (ExecuteMultipleResponse)service.Execute(multipleRequestUp);
        //                        multipleRequestUp.Requests.Clear();

        //                        countObjUp -= LimitMultiRequst;
        //                        UpdatedCount += LimitMultiRequst;
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        tracer.Trace(string.Format("Exception in Multi.{0}{1} ", System.Environment.NewLine, e.Message));
        //                    }
        //                }

        //                else if (countObjUp <= countSingleUp)
        //                {
        //                    try
        //                    {
        //                        service.Update(entityDbUp);
        //                        multipleRequestUp.Requests.Clear();

        //                        UpdatedCount++;
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        tracer.Trace(string.Format("Exception in Single.{0}{1} ", System.Environment.NewLine, e.Message));
        //                    }
        //                }
        //            }
        //        }

        //        if (listEnergyStarNew.Entities.Count > 0)
        //        {
        //            var countObjNewUp = listEnergyStarNew.Entities.Count;

        //            var countStepMultiNewUp = (int)Math.Floor((decimal)(listEnergyStarNew.Entities.Count / LimitMultiRequst));
        //            var countMultiNewUp = countStepMultiNewUp * LimitMultiRequst;
        //            var countSingleNewUp = listEnergyStarNew.Entities.Count - countMultiNewUp;

        //            foreach (var entityDbNewUp in listEnergyStarNew.Entities)
        //            {
        //                var newUpRequest = new CreateRequest { Target = entityDbNewUp };
        //                multipleRequestNewUp.Requests.Add(newUpRequest);

        //                if (multipleRequestNewUp.Requests.Count == LimitMultiRequst)
        //                {
        //                    try
        //                    {
        //                        var multipleResponseRow = (ExecuteMultipleResponse)service.Execute(multipleRequestNewUp);
        //                        multipleRequestNewUp.Requests.Clear();

        //                        countObjNewUp -= LimitMultiRequst;

        //                        // newCountObj += LimitMultiRequst;
        //                        NewCount += LimitMultiRequst;

        //                    }
        //                    catch (Exception e)
        //                    {
        //                        tracer.Trace(string.Format("Exception in Multi.{0}{1} ", System.Environment.NewLine, e.Message));
        //                    }
        //                }

        //                else if (countObjNewUp <= countSingleNewUp)
        //                {
        //                    try
        //                    {
        //                        service.Create(entityDbNewUp);
        //                        multipleRequestNewUp.Requests.Clear();

        //                        // newCountObj++;
        //                        NewCount++;
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        tracer.Trace(string.Format("Exception in Single.{0}{1} ", System.Environment.NewLine, e.Message));
        //                    }
        //                }
        //            }

        //            //   NewCount.Set(executionContext, newCountObj);
        //        }
        //    }
        //    else
        //    {
        //        var multipleRequestRow = new ExecuteMultipleRequest()
        //        {
        //            Settings = new ExecuteMultipleSettings()
        //            {
        //                ContinueOnError = false,
        //                ReturnResponses = true
        //            },
        //            Requests = new OrganizationRequestCollection()
        //        };

        //        if (arrCertAllData.Count > 0)
        //        {
        //            var countObjNew = arrCertAllData.Count;
        //            //int limitMulti = 100;

        //            var countStepMultiNew = (int)Math.Floor((decimal)(arrCertAllData.Count / LimitMultiRequst));
        //            var countMultiNew = countStepMultiNew * LimitMultiRequst;
        //            var countSingleNew = arrCertAllData.Count - countMultiNew;

        //            foreach (var item in arrCertAllData.Children<JObject>())
        //            {
        //                var energystarcertified = new Entity(EntityNameString);

        //                foreach (var prop in item.Properties())
        //                {
        //                    var propName = prop.Name;

        //                    foreach (JValue jVal in jaMetaData)
        //                    {
        //                        if (propName == jVal.ToString())
        //                        {
        //                            var propVal = string.Empty;

        //                            if (prop.HasValues)
        //                            {
        //                                propVal = prop.Value.ToString().Length > 300 ? prop.Value.ToString().Substring(0, 300) : prop.Value.ToString();
        //                            }
        //                            else
        //                            {
        //                                propVal = string.Empty;
        //                            }

        //                            energystarcertified.Attributes.Add("ddsm_" + propName.ToString(), propVal);
        //                        }
        //                    }

        //                    if (propName == "certlibsyncid")
        //                    {
        //                        energystarcertified.Attributes.Add("ddsm_" + propName.ToString(), mainEntityRef);
        //                    }
        //                }

        //                var brandName = energystarcertified.Contains("ddsm_brand_name") ? energystarcertified.Attributes["ddsm_brand_name"].ToString() : string.Empty;
        //                var modName = energystarcertified.Contains("ddsm_model_name") ? energystarcertified.Attributes["ddsm_model_name"].ToString() : string.Empty;
        //                var fullName = brandName + " - " + modName;

        //                energystarcertified.Attributes.Add("ddsm_name", fullName);
        //                //energystarcertified.Attributes["ddsm_status"] = "new";
        //                energystarcertified.Attributes["ddsm_status"] = new OptionSetValue((int)StatusCertLib.New);
        //                energystarcertified.Attributes["ddsm_certlibsyncid"] = mainEntityRef;
        //                //    energystarcertified.Attributes.Add("ddsm_status", "new");

        //                var createRequest = new CreateRequest { Target = energystarcertified };
        //                multipleRequestRow.Requests.Add(createRequest);

        //                if (multipleRequestRow.Requests.Count == LimitMultiRequst)
        //                {
        //                    try
        //                    {
        //                        var multipleResponseRow = (ExecuteMultipleResponse)service.Execute(multipleRequestRow);
        //                        multipleRequestRow.Requests.Clear();

        //                        countObjNew -= LimitMultiRequst;

        //                        //newCountObj += LimitMultiRequst;
        //                        NewCount += LimitMultiRequst;
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        tracer.Trace(string.Format("Exception in Multi.{0}{1} ", System.Environment.NewLine, e.Message));
        //                    }
        //                }
        //                else if (countObjNew <= countSingleNew)
        //                {
        //                    try
        //                    {
        //                        service.Create(energystarcertified);
        //                        multipleRequestRow.Requests.Clear();

        //                        //newCountObj++;
        //                        NewCount++;
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        tracer.Trace(string.Format("Exception in Single.{0}{1} ", System.Environment.NewLine, e.Message));
        //                    }
        //                }

        //                //   NewCount.Set(executionContext, newCountObj);
        //            }
        //        }
        //    }

        //    UpdateParentEntity(tracer, service, mainEntityRef, begTime, isNativeTarget);
        //}

        //private void UpdateParentEntity(ITracingService tracer, IOrganizationService service, EntityReference mainEntityRef, DateTime begTime, bool isNativeTarget)
        //{
        //    var jaFilter = JArrayMetaDataFilter;

        //    try
        //    {
        //        tracer.Trace("---------Update1------------");

        //        var energyStarUpdate = new Entity(mainEntityRef.LogicalName, mainEntityRef.Id);

        //        tracer.Trace("---------Update2------------");

        //        if (IsKeepUncheckedCategories)
        //        {
        //            foreach (var objFilter in jaFilter.Children<JObject>())
        //            {
        //                foreach (var propFilter in objFilter.Properties())
        //                {
        //                    //  if (energyStarUpdate.Attributes.ContainsKey(propFilter.Value.ToString()))
        //                    //   {
        //                    energyStarUpdate[propFilter.Value.ToString()] = true;
        //                    // }
        //                }
        //            }
        //        }

        //        energyStarUpdate["ddsm_enddate"] = DateTime.Now;
        //        energyStarUpdate["ddsm_newcount"] = NewCount;
        //        energyStarUpdate["ddsm_updatecount"] = UpdatedCount;
        //        energyStarUpdate["ddsm_unchangedcount"] = UnchangedCount;
        //        energyStarUpdate["ddsm_deletecount"] = DeletedCount;
        //        energyStarUpdate["ddsm_status"] = new OptionSetValue(962080002);
        //        energyStarUpdate["ddsm_historyjson"] = CreateHistory(tracer, service, mainEntityRef);

        //        if (!isNativeTarget)
        //        {
        //            energyStarUpdate["ddsm_startdate"] = begTime;
        //            energyStarUpdate["ddsm_isdraft"] = false;

        //            //   energyStarUpdate["ddsm_name"] = generateName;
        //        }

        //        service.Update(energyStarUpdate);
        //    }
        //    catch (Exception e)
        //    {
        //        tracer.Trace(string.Format("Update.{0}{1} ", System.Environment.NewLine, e.Message));
        //    }
        //}

        //private string CreateHistory(ITracingService tracer, IOrganizationService service, EntityReference mainEntityRef)
        //{
        //    var arrNew = new JArray();
        //    var arrUpdate = new JArray();
        //    var arrUnchange = new JArray();
        //    var arrDelete = new JArray();
        //    var arrCategory = new JArray();
        //    var barChartData = new JObject();

        //    var columnSet = new ColumnSet("ddsm_type_product",
        //                                  "ddsm_certlibsyncid");

        //    var queryCat = new QueryExpression { EntityName = EntityNameString, ColumnSet = columnSet };
        //    queryCat.Distinct = true;
        //    queryCat.Criteria.AddCondition("ddsm_certlibsyncid", ConditionOperator.Equal, mainEntityRef.Id);

        //    var listCat = service.RetrieveMultiple(queryCat);

        //    listCat.Entities.ToList().ForEach(entity =>
        //    {
        //        var typeCat = entity["ddsm_type_product"].ToString();

        //        // new
        //        var queryNew = new QueryExpression { EntityName = EntityNameString, ColumnSet = new ColumnSet(true) };
        //        //queryNew.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, "new");
        //        queryNew.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, (int)StatusCertLib.New);
        //        queryNew.Criteria.AddCondition("ddsm_type_product", ConditionOperator.Equal, typeCat);
        //        queryNew.Criteria.AddCondition("ddsm_certlibsyncid", ConditionOperator.Equal, mainEntityRef.Id);

        //        var listNew = new EntityCollection();

        //        var pageNumberNew = 1;
        //        RetrieveMultipleRequest multiRequestNew;

        //        var multiResponseNew = new RetrieveMultipleResponse();

        //        do
        //        {
        //            queryNew.PageInfo.Count = LimitMultiRequst;
        //            queryNew.PageInfo.PagingCookie = (pageNumberNew == 1) ? null : multiResponseNew.EntityCollection.PagingCookie;
        //            queryNew.PageInfo.PageNumber = pageNumberNew++;

        //            multiRequestNew = new RetrieveMultipleRequest();
        //            multiRequestNew.Query = queryNew;
        //            multiResponseNew = (RetrieveMultipleResponse)service.Execute(multiRequestNew);

        //            listNew.Entities.AddRange(multiResponseNew.EntityCollection.Entities);
        //        }
        //        while (multiResponseNew.EntityCollection.MoreRecords);

        //        if (listNew.Entities.Count > 0)
        //        {
        //            arrNew.Add(listNew.Entities.Count);
        //        }
        //        else
        //        {
        //            arrNew.Add(0);
        //        }

        //        // update
        //        var queryUp = new QueryExpression { EntityName = EntityNameString, ColumnSet = new ColumnSet(true) };
        //        //queryUp.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, "update");
        //        queryUp.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, (int)StatusCertLib.Update);
        //        queryUp.Criteria.AddCondition("ddsm_type_product", ConditionOperator.Equal, typeCat);
        //        queryUp.Criteria.AddCondition("ddsm_certlibsyncid", ConditionOperator.Equal, mainEntityRef.Id);

        //        var listUp = new EntityCollection();

        //        var pageNumberUp = 1;
        //        RetrieveMultipleRequest multiRequestUp;

        //        var multiResponseUp = new RetrieveMultipleResponse();

        //        do
        //        {
        //            queryUp.PageInfo.Count = LimitMultiRequst;
        //            queryUp.PageInfo.PagingCookie = (pageNumberUp == 1) ? null : multiResponseUp.EntityCollection.PagingCookie;
        //            queryUp.PageInfo.PageNumber = pageNumberUp++;

        //            multiRequestUp = new RetrieveMultipleRequest();
        //            multiRequestUp.Query = queryUp;
        //            multiResponseUp = (RetrieveMultipleResponse)service.Execute(multiRequestUp);

        //            listUp.Entities.AddRange(multiResponseUp.EntityCollection.Entities);
        //        }
        //        while (multiResponseUp.EntityCollection.MoreRecords);

        //        if (listUp.Entities.Count > 0)
        //        {
        //            arrUpdate.Add(listUp.Entities.Count);
        //        }
        //        else
        //        {
        //            arrUpdate.Add(0);
        //        }

        //        // unchange
        //        var queryUnchange = new QueryExpression { EntityName = EntityNameString, ColumnSet = new ColumnSet(true) };
        //        //queryUp.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, "update");
        //        queryUnchange.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, (int)StatusCertLib.Unchange);
        //        queryUnchange.Criteria.AddCondition("ddsm_type_product", ConditionOperator.Equal, typeCat);
        //        queryUnchange.Criteria.AddCondition("ddsm_certlibsyncid", ConditionOperator.Equal, mainEntityRef.Id);

        //        var listUnchange = new EntityCollection();

        //        var pageNumberUnchange = 1;
        //        RetrieveMultipleRequest multiRequestUnchange;

        //        var multiResponseUnchange = new RetrieveMultipleResponse();

        //        do
        //        {
        //            queryUnchange.PageInfo.Count = LimitMultiRequst;
        //            queryUnchange.PageInfo.PagingCookie = (pageNumberUnchange == 1) ? null : multiResponseUnchange.EntityCollection.PagingCookie;
        //            queryUnchange.PageInfo.PageNumber = pageNumberUnchange++;

        //            multiRequestUnchange = new RetrieveMultipleRequest();
        //            multiRequestUnchange.Query = queryUnchange;
        //            multiResponseUnchange = (RetrieveMultipleResponse)service.Execute(multiRequestUnchange);

        //            listUnchange.Entities.AddRange(multiResponseUnchange.EntityCollection.Entities);
        //        }
        //        while (multiResponseUnchange.EntityCollection.MoreRecords);

        //        if (listUnchange.Entities.Count > 0)
        //        {
        //            arrUnchange.Add(listUnchange.Entities.Count);
        //        }
        //        else
        //        {
        //            arrUnchange.Add(0);
        //        }

        //        // delete
        //        var queryDel = new QueryExpression { EntityName = EntityNameString, ColumnSet = new ColumnSet(true) };
        //        //queryDel.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, "delete");
        //        queryDel.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, (int)StatusCertLib.Delete);
        //        queryDel.Criteria.AddCondition("ddsm_type_product", ConditionOperator.Equal, typeCat);
        //        queryDel.Criteria.AddCondition("ddsm_certlibsyncid", ConditionOperator.Equal, mainEntityRef.Id);

        //        var listDel = new EntityCollection();

        //        var pageNumberDel = 1;
        //        RetrieveMultipleRequest multiRequestDel;

        //        var multiResponseDel = new RetrieveMultipleResponse();

        //        do
        //        {
        //            queryDel.PageInfo.Count = LimitMultiRequst;
        //            queryDel.PageInfo.PagingCookie = (pageNumberDel == 1) ? null : multiResponseDel.EntityCollection.PagingCookie;
        //            queryDel.PageInfo.PageNumber = pageNumberDel++;

        //            multiRequestDel = new RetrieveMultipleRequest();
        //            multiRequestDel.Query = queryDel;
        //            multiResponseDel = (RetrieveMultipleResponse)service.Execute(multiRequestDel);

        //            listDel.Entities.AddRange(multiResponseDel.EntityCollection.Entities);
        //        }
        //        while (multiResponseDel.EntityCollection.MoreRecords);

        //        if (listDel.Entities.Count > 0)
        //        {
        //            arrDelete.Add(listDel.Entities.Count);
        //        }
        //        else
        //        {
        //            arrDelete.Add(0);
        //        }

        //        arrCategory.Add(typeCat);

        //    });

        //    var joNew = new JObject();
        //    joNew.Add("label", "New");
        //    joNew.Add("backgroundColor", "#5B97D5");
        //    joNew.Add("data", arrNew);

        //    var joUp = new JObject();
        //    joUp.Add("label", "Updated");
        //    joUp.Add("backgroundColor", "#70AD47");
        //    joUp.Add("data", arrUpdate);

        //    var joUnchange = new JObject();
        //    joUnchange.Add("label", "Unchanged");
        //    joUnchange.Add("backgroundColor", "#9E73A2");
        //    joUnchange.Add("data", arrUnchange);

        //    var joDel = new JObject();
        //    joDel.Add("label", "Disabled");
        //    joDel.Add("backgroundColor", "#ED7D31");
        //    joDel.Add("data", arrDelete);

        //    var jaDataSet = new JArray();
        //    jaDataSet.Add(joNew);
        //    jaDataSet.Add(joUp);
        //    jaDataSet.Add(joUnchange);
        //    jaDataSet.Add(joDel);


        //    barChartData.Add("labels", arrCategory);
        //    barChartData.Add("datasets", jaDataSet);

        //    var history = barChartData.ToString(Newtonsoft.Json.Formatting.None);

        //    return history;
        //}

        //private void CreateAtributes(ITracingService tracer, IOrganizationService service, JArray jaMetaData)
        //{

        //    var multipleRequestFields = new ExecuteMultipleRequest()
        //    {
        //        Settings = new ExecuteMultipleSettings()
        //        {
        //            ContinueOnError = false,
        //            ReturnResponses = true
        //        },
        //        Requests = new OrganizationRequestCollection()
        //    };

        //    foreach (JValue item in jaMetaData)
        //    {
        //        var propName = string.Empty;

        //        propName = "ddsm_" + item.Value.ToString();

        //        var stringAttribute = new StringAttributeMetadata
        //        {
        //            SchemaName = propName,
        //            DisplayName = new Label(item.Value.ToString(), 1033),
        //            RequiredLevel = new AttributeRequiredLevelManagedProperty(AttributeRequiredLevel.None),
        //            Description = new Label(item.Value.ToString(), 1033),
        //            MaxLength = 300
        //        };

        //        var createAttributeRequest = new CreateAttributeRequest
        //        {
        //            EntityName = EntityNameString,
        //            Attribute = stringAttribute
        //        };

        //        multipleRequestFields.Requests.Add(createAttributeRequest);
        //    }

        //    try
        //    {
        //        var multipleResponse = (ExecuteMultipleResponse)service.Execute(multipleRequestFields);
        //    }
        //    catch (Exception e)
        //    {
        //        tracer.Trace(string.Format("Exception in Multi.{0}{1} ", System.Environment.NewLine, e.Message));
        //    }
        //}

//        private Dictionary<string, Dictionary<string, string>> GetFilteringTemplete(IOrganizationService service, Dictionary<string, Dictionary<string, string>> dictionarySertifiedES)
//        {
//            var dictionaryCertFilter = new Dictionary<string, Dictionary<string, string>>();

//            var columnSet = new ColumnSet("ddsm_audio_video",
//"ddsm_boilers",
//"ddsm_ceiling_fans",
//"ddsm_commercial_clothes_washers",
//"ddsm_commercial_dishwashers",
//"ddsm_commercial_fryers",
//"ddsm_commercial_griddles",
//"ddsm_commercial_hot_food_holding_cabinet",
//"ddsm_commercial_mce_machines",
//"ddsm_commercial_ovens",
//"ddsm_commercial_refrigerators_and_freezers",
//"ddsm_commercial_steam_cookers",
//"ddsm_commercial_water_heaters",
//"ddsm_computers",
//"ddsm_data_center_storage",
//"ddsm_decorative_light_strings",
//"ddsm_dehumidifiers",
//"ddsm_displays",
//"ddsm_enterprise_servers",
//"ddsm_furnaces",
//"ddsm_geothermal_heat_pumps",
//"ddsm_imaging_equipment",
//"ddsm_large_network_equipment",
//"ddsm_light_bulbs",
//"ddsm_light_commercial_hvac",
//"ddsm_light_fixtures",
//"ddsm_non_ahri_cent_air_con_heat_pum",
//"ddsm_pool_pumps",
//"ddsm_products_lighting",
//"ddsm_products_non_lighting",
//"ddsm_residential_clothes_dryers",
//"ddsm_residential_clothes_washers",
//"ddsm_residential_dishwashers",
//"ddsm_residential_freezers",
//"ddsm_residential_refrigerators",
//"ddsm_roof_products",
//"ddsm_room_air_conditioners",
//"ddsm_set_top_boxes",
//"ddsm_small_network_equipment",
//"ddsm_telephones",
//"ddsm_televisions",
//"ddsm_uninterruptible_power_supplies",
//"ddsm_vending_machines",
//"ddsm_ventilating_fans",
//"ddsm_water_coolers",
//"ddsm_water_heaters",
//"ddsm_historic_room_air_cleaners",
//"ddsm_keep_unchecked_categories");

//            var query = new QueryExpression { EntityName = "ddsm_certlibsyncconf", ColumnSet = columnSet };
//            query.Criteria.AddCondition("statuscode", ConditionOperator.Equal, 1);
//            var listCertLibSyncConf = service.RetrieveMultiple(query);

//            var certLibSyncConf = listCertLibSyncConf.Entities[0];

//            if (certLibSyncConf != null)
//            {
//                IsKeepUncheckedCategories = (bool)certLibSyncConf.Attributes["ddsm_keep_unchecked_categories"];

//                foreach (var attribute in certLibSyncConf.Attributes)
//                {
//                    foreach (var pair in dictionarySertifiedES)
//                    {
//                        foreach (var pairValue in pair.Value)
//                        {
//                            if (pairValue.Value == attribute.Key && (bool)attribute.Value)
//                            {
//                                dictionaryCertFilter.Add(pair.Key, new Dictionary<string, string>() { { pairValue.Key, pairValue.Value } });
//                            }
//                        }
//                    }
//                }
//            }

//            return dictionaryCertFilter;
//        }

//        private JArray GetDataLastSync(IOrganizationService service, Dictionary<string, Dictionary<string, string>> dictionaryCertFilter, Dictionary<string, Dictionary<string, string>> dictionarySertifiedES)
//        {
//            var arrCertTypeCategoryLast = new JArray();
//            var jaMetaDataFilter1 = new JArray();
//            var jaMetaDataFilter2 = new JArray();

//            var columnSet = new ColumnSet("ddsm_audio_video",
//"ddsm_boilers",
//"ddsm_ceiling_fans",
//"ddsm_commercial_clothes_washers",
//"ddsm_commercial_dishwashers",
//"ddsm_commercial_fryers",
//"ddsm_commercial_griddles",
//"ddsm_commercial_hot_food_holding_cabinet",
//"ddsm_commercial_mce_machines",
//"ddsm_commercial_ovens",
//"ddsm_commercial_refrigerators_and_freezers",
//"ddsm_commercial_steam_cookers",
//"ddsm_commercial_water_heaters",
//"ddsm_computers",
//"ddsm_data_center_storage",
//"ddsm_decorative_light_strings",
//"ddsm_dehumidifiers",
//"ddsm_displays",
//"ddsm_enterprise_servers",
//"ddsm_furnaces",
//"ddsm_geothermal_heat_pumps",
//"ddsm_imaging_equipment",
//"ddsm_large_network_equipment",
//"ddsm_light_bulbs",
//"ddsm_light_commercial_hvac",
//"ddsm_light_fixtures",
//"ddsm_non_ahri_cent_air_con_heat_pum",
//"ddsm_pool_pumps",
//"ddsm_products_lighting",
//"ddsm_products_non_lighting",
//"ddsm_residential_clothes_dryers",
//"ddsm_residential_clothes_washers",
//"ddsm_residential_dishwashers",
//"ddsm_residential_freezers",
//"ddsm_residential_refrigerators",
//"ddsm_roof_products",
//"ddsm_room_air_conditioners",
//"ddsm_set_top_boxes",
//"ddsm_small_network_equipment",
//"ddsm_telephones",
//"ddsm_televisions",
//"ddsm_uninterruptible_power_supplies",
//"ddsm_vending_machines",
//"ddsm_ventilating_fans",
//"ddsm_water_coolers",
//"ddsm_water_heaters",
//"ddsm_historic_room_air_cleaners",
//"ddsm_certlibsyncid");

//            var query = new QueryExpression { EntityName = "ddsm_certlibsync", ColumnSet = columnSet };
//            query.TopCount = 1;
//            query.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, 962080002);
//            query.AddOrder("createdon", OrderType.Descending);

//            var listCertLibSync = service.RetrieveMultiple(query);

//            var certLibSync = listCertLibSync.Entities[0];

//            if (certLibSync != null)
//            {
//                foreach (var attribute in certLibSync.Attributes)
//                {
//                    if (attribute.Key == "ddsm_certlibsyncid")
//                    {
//                        PreviousCertLibSyncId = new Guid(attribute.Value.ToString());
//                    }
//                    else
//                    {
//                        if ((bool)attribute.Value)
//                        {
//                            if (dictionaryCertFilter.Count > 0 && dictionaryCertFilter != null)
//                            {
//                                foreach (var pair in dictionaryCertFilter)
//                                {
//                                    foreach (var pairValue in pair.Value)
//                                    {
//                                        if (pairValue.Value != attribute.Key)
//                                        {
//                                            jaMetaDataFilter1.Add(new JObject(new JProperty("Name", attribute.Key)));
//                                        }
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                jaMetaDataFilter1.Add(new JObject(new JProperty("Name", attribute.Key)));

//                            }
//                        }
//                    }
//                }

//                JArrayMetaDataFilter = jaMetaDataFilter1;

//                foreach (var jObjFilter1 in jaMetaDataFilter1.Children<JObject>())
//                {
//                    foreach (var propFilter1 in jObjFilter1.Properties())
//                    {
//                        foreach (var pairFilter1 in dictionarySertifiedES)
//                        {
//                            foreach (var pairValue1 in pairFilter1.Value)
//                            {
//                                if (pairValue1.Value == propFilter1.Value.ToString())
//                                {
//                                    jaMetaDataFilter2.Add(new JObject(new JProperty("Name", pairFilter1.Key)));
//                                }
//                            }
//                        }
//                    }
//                }
//            }

//            return jaMetaDataFilter2;
//        }
    }
}

