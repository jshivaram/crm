﻿using System;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk.Messages;
using CertifiedRefBook.Models.DLC;
using CertifiedRefBook;
using CertifiedRefBook.Utils;

public class GetDLCMetadata : CodeActivity
{
    //public Logger ConfigLogger()
    //{
    //    // Step 1. Create configuration object 
    //    var config = new LoggingConfiguration();


    //    // Step 2. Create targets and add them to the configuration 
    //    //var consoleTarget = new ColoredConsoleTarget();
    //    //config.AddTarget("console", consoleTarget);

    //    //var nlogViewerTarget = new FileTarget("file_txt") { FileName = @"e:\log_____.txt" };
    //    //config.AddTarget("file", nlogViewerTarget);

    //    var nlogViewerTarget = new NLogViewerTarget { Address = "tcp4://localhost:4505" };
    //    config.AddTarget("nlog", nlogViewerTarget);

    //    // Step 3. Set target properties 
    //    //consoleTarget.Layout = @"${date:format=HH\:mm\:ss} ${logger} ${message}";
    //    //fileTarget.FileName = "${basedir}/file.txt";
    //    //fileTarget.Layout = "${message}";

    //    // Step 4. Define rules for ALL
    //    foreach (var logLevel in LogLevel.AllLoggingLevels)
    //    {
    //        var rule1 = new LoggingRule("*", logLevel, nlogViewerTarget);
    //        config.LoggingRules.Add(rule1);
    //    }

    //    //var rule1 = new LoggingRule("file_txt", LogLevel.Debug, fileTarget);
    //    //config.LoggingRules.Add(rule1);
    //    //rule1 = new LoggingRule("file_txt", LogLevel.Error, fileTarget);
    //    //config.LoggingRules.Add(rule1);
    //    //rule1 = new LoggingRule("file_txt", LogLevel.Trace, fileTarget);
    //    //config.LoggingRules.Add(rule1);





    //    //var rule2 = new LoggingRule("*", LogLevel.Debug, nlogViewerTarget);
    //    //config.LoggingRules.Add(rule2);

    //    //rule2 = new LoggingRule("*", LogLevel.Error, nlogViewerTarget);
    //    //config.LoggingRules.Add(rule2);

    //    //rule2 = new LoggingRule("*", LogLevel.Info, nlogViewerTarget);
    //    //config.LoggingRules.Add(rule2);

    //    //rule2 = new LoggingRule("*", LogLevel.Trace, nlogViewerTarget);
    //    //config.LoggingRules.Add(rule2);

    //    //rule2 = new LoggingRule("*", LogLevel.Fatal, nlogViewerTarget);
    //    //config.LoggingRules.Add(rule2);

    //    //rule2 = new LoggingRule("*", LogLevel.Warn, nlogViewerTarget);
    //    //config.LoggingRules.Add(rule2);

    //    // Step 5. Activate the configuration
    //    LogManager.Configuration = config;

    //    // Example usage
    //    Logger logger = LogManager.GetLogger("Example");
    //    logger.Trace("trace log message");
    //    logger.Debug("debug log message");
    //    logger.Info("info log message");
    //    logger.Warn("warn log message");
    //    logger.Error("error log message");
    //    logger.Fatal("fatal log message");

    //    return logger;
    //}

    //Logger ConfigLogger()
    //{
    //    // Step 1. Create configuration object 
    //    var config = new LoggingConfiguration();

    //    // Step 2. Create targets and add them to the configuration 
    //    //var consoleTarget = new ColoredConsoleTarget();
    //    //config.AddTarget("console", consoleTarget);

    //    var fileTarget = new FileTarget();
    //    config.AddTarget("file", fileTarget);

    //    // Step 3. Set target properties 
    //   // consoleTarget.Layout = @"${date:format=HH\:mm\:ss} ${logger} ${message}";
    //    fileTarget.FileName = "${basedir}/file.txt";
    //    fileTarget.Layout = "${message}";

    //    // Step 4. Define rules
    //    //var rule1 = new LoggingRule("*", LogLevel.Debug, consoleTarget);
    //    //config.LoggingRules.Add(rule1);

    //    var rule2 = new LoggingRule("*", LogLevel.Debug, fileTarget);
    //    config.LoggingRules.Add(rule2);

    //    // Step 5. Activate the configuration
    //    LogManager.Configuration = config;



    //    return logger;
    //}

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }

    [Output("Result")]
    public OutArgument<string> Result { get; set; }

    protected override void Execute(CodeActivityContext executionContext)
    {

        var logger = executionContext.GetExtension<ITracingService>();
        var context = executionContext.GetExtension<IWorkflowContext>();
        var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        var service = serviceFactory.CreateOrganizationService(context.UserId);

        var certOrg = Helper.GetTargetData(executionContext);
        logger.Trace("after get target cert org");

        #region delete Existing metadata
        Helper.DeleteCategories(certOrg.Id, service);

        #endregion
        var helper = new DlcHelper(service);
        var categories = new List<DlcCategory>();
        var dataPortion = helper.Config.Portion > 0 ? helper.Config.Portion : 100;
        try
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(helper.Config.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Access-Key", helper.Config.AccessKey);
                client.DefaultRequestHeaders.Add("Access-Key-Secret", helper.Config.AccessKeySecret);
                HttpResponseMessage response = client.GetAsync("category/?P:Show=" + dataPortion).Result;
                if (response.IsSuccessStatusCode)
                {
                    var dataTask = response.Content.ReadAsAsync<DlcRespounse>();
                    var data = dataTask.Result;
                    //first portion
                    categories.AddRange(data.pageRecords);
                    //all pages 
                    var currentPage = data.currentPage;
                    var pageCount = data.totalPages;
                    while (data.currentPage < pageCount)
                    {
                        currentPage++;
                        HttpResponseMessage catPortion = client.GetAsync("category/?P:Show=" + dataPortion + "&P:Current=" + currentPage).Result;
                        if (catPortion.IsSuccessStatusCode)
                        {
                            var dataTask1 = catPortion.Content.ReadAsAsync<DlcRespounse>();
                            data = dataTask1.Result;

                            categories.AddRange(data.pageRecords);
                        }
                    }
                }
            }

            logger.Trace("count of categories: " + categories.Count);
            ProcessCategories(certOrg, categories, service, logger, helper.Config);

            Result.Set(executionContext,"");
            Complete.Set(executionContext,true);
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    private void ProcessCategories(EntityReference certOrg, List<DlcCategory> categories, IOrganizationService service, ITracingService logger, DlcConfig config)
    {
        logger.Trace("in ProcessCategories");
        var emRequest = new ExecuteMultipleRequest()
        {
            Settings = new ExecuteMultipleSettings()
            {
                ContinueOnError = false,
                ReturnResponses = true
            },
            Requests = new OrganizationRequestCollection()
        };

        var columns1 = from column in config.ProductModel
                       select new Entity("ddsm_certifieditemfield")
                       {
                           Attributes =
                                    {
                                        new KeyValuePair<string, object>("ddsm_logicalname", column.Key),
                                        new KeyValuePair<string, object>("ddsm_name", column.Value.Label),
                                        new KeyValuePair<string, object>("ddsm_datatype", column.Value.Type),
                                    }
                       };
        var columns = columns1.ToList();

        var processed = 0;
        var portion = 50;
        while (processed < categories.Count)
        {
            var cats = categories.Skip(processed).Take(portion);
            foreach (var cat in categories)
            {
                EntityCollection relatedColumns = new EntityCollection();
                Relationship rel = new Relationship("ddsm_ddsm_certifiedcategory_ddsm_certifieditem");

                var category = new Entity("ddsm_certifiedcategory")
                {
                    Attributes =
                        {
                            new KeyValuePair<string, object>("ddsm_name" ,cat.categoryName),
                            new KeyValuePair<string, object>("ddsm_category" ,cat.urlTitle),
                            new KeyValuePair<string, object>("ddsm_metadataid" ,cat.categoryID),
                            new KeyValuePair<string, object>("ddsm_description" ,cat.categoryDescription),
                            new KeyValuePair<string, object>("ddsm_certifyingorganizationid", certOrg)

                        }
                };

                relatedColumns.Entities.AddRange(columns);
                category.RelatedEntities.Add(rel, relatedColumns);

                emRequest.Requests.Add(new CreateRequest() { Target = category });
            }

            if (emRequest.Requests.Count > 0)
            {
                var result = (ExecuteMultipleResponse)service.Execute(emRequest);
                processed += emRequest.Requests.Count;
                foreach (ExecuteMultipleResponseItem responseItem in result.Responses)
                {
                    // An error has occurred.
                    if (responseItem.Fault != null)
                    {
                        var msg = "Error: Name: " + emRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message;
                        logger.Trace(msg);
                        //   _projectGroup.ObjCommon.TracingService.Trace(msg);
                        //  Result.Set(executionContext, msg);
                    }
                }


            }
        }





    }
}


