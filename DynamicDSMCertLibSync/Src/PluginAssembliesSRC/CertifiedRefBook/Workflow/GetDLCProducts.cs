﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CertifiedRefBook.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

namespace CertifiedRefBook.Workflow
{
    public class GetDLCProducts : CodeActivity
    {
        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        [Output("Result")]
        public OutArgument<string> Result { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            var logger = executionContext.GetExtension<ITracingService>();
            var context = executionContext.GetExtension<IWorkflowContext>();
            var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            var service = serviceFactory.CreateOrganizationService(context.UserId);

            var target = Helper.GetTargetData(executionContext);

            logger.Trace("after get target cert org");
            var helper = new DlcHelper(service);
            var dataPortion = helper.Config.Portion > 0 ? helper.Config.Portion : 100;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(helper.Config.Url);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Access-Key", helper.Config.AccessKey);
                    client.DefaultRequestHeaders.Add("Access-Key-Secret", helper.Config.AccessKeySecret);
                    HttpResponseMessage response = client.GetAsync("category/?P:Show=" + dataPortion).Result;
                    if (response.IsSuccessStatusCode)
                    {



                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}
