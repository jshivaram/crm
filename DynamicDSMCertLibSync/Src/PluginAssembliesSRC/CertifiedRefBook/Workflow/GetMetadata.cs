﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using CertifiedRefBook.Models;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using Microsoft.Xrm.Sdk;
using CertifiedRefBook.Utils;

namespace CertifiedRefBook
{
    public class GetMetadata : CodeActivity
    {
        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        [Output("Result")]
        public OutArgument<string> Result { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            var tracer = executionContext.GetExtension<ITracingService>();
            var context = executionContext.GetExtension<IWorkflowContext>();
            var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            var service = serviceFactory.CreateOrganizationService(context.UserId);

            var certOrg = Helper.GetCertOrg(service, "Energy Star");
            var _helper = new EnergyStarHelper(service);

            //var logger = Helper.ConfigLogger();

            //logger.Debug("debug log message");
            //logger.Info("info log message");
            //logger.Warn("warn log message");
            //logger.Error("error log message");
            //logger.Fatal("fatal log message");


            List<EsCategory> categories = new List<EsCategory>();
            #region delete Existing metadata
            Helper.DeleteCategories(certOrg.Id, service);
            #endregion


            var config = _helper.Config;
            //get categories
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(config.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("X-App-Token", config.Token);

                HttpResponseMessage response = client.GetAsync(_helper.GetCategories()).Result;
                if (response.IsSuccessStatusCode)
                {
                    var categoriesTask = response.Content.ReadAsAsync<List<EsCategory>>();
                    categories = categoriesTask.Result;

                    //get metadata for categories
                    foreach (var cat in categories)
                    {
                        try
                        {
                            response = client.GetAsync(_helper.GetMetadataUrlPart(cat.Id)).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                var dataTask = response.Content.ReadAsAsync<View>();
                                var metadata = dataTask.Result;
                                var columns = from column in metadata.Columns
                                              select new Entity("ddsm_certifieditemfield")
                                              {
                                                  Attributes =
                                    {
                                        new KeyValuePair<string, object>("ddsm_logicalname", column.FieldName),
                                        new KeyValuePair<string, object>("ddsm_name", column.Name),
                                        new KeyValuePair<string, object>("ddsm_datatype", column.DataTypeName),
                                        new KeyValuePair<string, object>("ddsm_metadataid", column.Id),
                                    }
                                              };

                                EntityCollection relatedColumns = new EntityCollection();
                                Relationship rel = new Relationship("ddsm_ddsm_certifiedcategory_ddsm_certifieditem");

                                var category = new Entity("ddsm_certifiedcategory")
                                {
                                    Attributes =
                                {
                                    new KeyValuePair<string, object>("ddsm_name", cat.Name),
                                    new KeyValuePair<string, object>("ddsm_category", cat.Category),
                                    new KeyValuePair<string, object>("ddsm_metadataid", cat.Id),
                                    new KeyValuePair<string, object>("ddsm_description", cat.Description),
                                    new KeyValuePair<string, object>("ddsm_certifyingorganizationid",
                                        certOrg.ToEntityReference())

                                }
                                };
                                relatedColumns.Entities.AddRange(columns);
                                category.RelatedEntities.Add(rel, relatedColumns);
                                var id = service.Create(category);

                               // counter++;
                            }

                        }
                        catch (Exception ex)
                        {
                            tracer.Trace(ex.Message + " error URL: " + _helper.GetMetadataUrlPart(cat.Id));
                        }
                    }
                }
            }
            Complete.Set(executionContext, true);
            Result.Set(executionContext, JsonConvert.SerializeObject(new { ErrorMsg = "Unique Ceterories: " + categories.Count }));
        }

    }
}
