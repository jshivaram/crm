﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using CertifiedRefBook;
using CertifiedRefBook.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;


public class GetSyncProgressInfo : BaseCodeActivity
{

    [Input("Cert Lib Sync")]
    [ArgumentEntity("ddsm_certlibsync")]
    [ReferenceTarget("ddsm_certlibsync")]
    public InArgument<EntityReference> CertLibSync { get; set; }

    protected override void ExecuteAction(CodeActivityContext executionContext)
    {
        var tracer = executionContext.GetExtension<ITracingService>();
        var context = executionContext.GetExtension<IWorkflowContext>();
        var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        var service = serviceFactory.CreateOrganizationService(context.UserId);

        var CommonHelper = new Helper();

        var syncDate = new DateTime(2017, 03, 31);

        var mainEntityRef = CertLibSync.Get(executionContext);
        var Tabs = new List<Tab>();
        var mappingDict = Helper.GetCurrentMapping(mainEntityRef, service, out Tabs);
        var NewCount = 0;
        var UpdatedCount = 0;
        var DisabledCount = 0;


        var allData = getCertlibRecords(mainEntityRef, Tabs, service);

        IHistoryCreator historyCreator = new EsHistoryCreator();

        var historyjson = historyCreator.CreateHistory(tracer, service, mainEntityRef, syncDate, Tabs, out NewCount, out UpdatedCount, out DisabledCount, allData?.Select(x=>x.ToEntityReference()).ToList());
        Result.Set(executionContext, historyjson);
        Complete.Set(executionContext, true);
        // CommonHelper.UpdateParentEntity(tracer, service, mainEntityRef, syncDate, Tabs);

    }


    List<Entity> getCertlibRecords(EntityReference mainEntityRef, List<Tab> tabs, IOrganizationService service)
    {
        int recordPerPage = 5000;
        int page = 1;

        var all = new List<Entity>();

        foreach (var _cat in tabs)
        {
            var cat = _cat;
            var query = new QueryExpression
            {
                EntityName = "ddsm_certlib",
                ColumnSet = new ColumnSet("createdon", "modifiedon", "ddsm_processstatus", "ddsm_datedelisted"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("statecode",ConditionOperator.Equal,(int)BaseHistoryCreator.StateCode.Active),
                        //new ConditionExpression("ddsm_certlibid",ConditionOperator.In,ids ),
                        new ConditionExpression("ddsm_certlibsyncid",ConditionOperator.Equal,mainEntityRef.Id ),
                      //  new ConditionExpression("ddsm_type_product",ConditionOperator.Equal,cat.MetadataId.ToLower() )
                    }
                },
                PageInfo = new PagingInfo
                {
                    Count = recordPerPage,
                    PageNumber = page
                }
            };
            // var allResp = service.RetrieveMultiple(query);
            var entityList = new EntityCollection();

            do
            {
                query.PageInfo.PagingCookie = (page == 1) ? null : entityList.PagingCookie;
                query.PageInfo.PageNumber = page++;

                entityList = service.RetrieveMultiple(query);
                all.AddRange(entityList?.Entities);
                // Do something with the results here
            }
            while (entityList.MoreRecords);
        }
        return all;
    }
}

