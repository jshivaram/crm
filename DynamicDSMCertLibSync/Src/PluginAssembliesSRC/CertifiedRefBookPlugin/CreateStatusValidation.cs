﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Linq;

namespace CertifiedRefBookPlugin
{
    public class CreateStatusValidation : IPlugin
    {
        #region Secure/Unsecure Configuration Setup
        private string _secureConfig = null;
        private string _unsecureConfig = null;
        ITracingService tracer;

        public CreateStatusValidation(string unsecureConfig, string secureConfig)
        {
            _secureConfig = secureConfig;
            _unsecureConfig = unsecureConfig;
        }
        #endregion
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            tracer.Trace("----------start1------------");

            Entity certLibSyncConfTarget = (Entity)context.InputParameters["Target"];

            var columnSet = new ColumnSet("statuscode");

            var multipleRequestUp = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };

            QueryExpression query = new QueryExpression { EntityName = "ddsm_certlibsyncconf", ColumnSet = columnSet };
            EntityCollection listCertLibSyncConf = service.RetrieveMultiple(query);

            listCertLibSyncConf.Entities.ToList().ForEach(ent =>
            {

                SetStateRequest setStateRequest = new SetStateRequest();

                setStateRequest.EntityMoniker = new EntityReference("ddsm_certlibsyncconf", ent.Id);
                setStateRequest.State = new OptionSetValue(1);
                setStateRequest.Status = new OptionSetValue(2);
                service.Execute(setStateRequest);
            });
        }
    }
}
