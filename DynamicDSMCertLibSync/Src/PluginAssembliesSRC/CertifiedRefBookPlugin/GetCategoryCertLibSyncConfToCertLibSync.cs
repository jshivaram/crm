﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;

namespace CertifiedRefBookPlugin
{
    public class GetCategoryCertLibSyncConfToCertLibSync : IPlugin
    {
        #region Secure/Unsecure Configuration Setup
        private string _secureConfig = null;
        private string _unsecureConfig = null;
        ITracingService tracer;

        public GetCategoryCertLibSyncConfToCertLibSync(string unsecureConfig, string secureConfig)
        {
            _secureConfig = secureConfig;
            _unsecureConfig = unsecureConfig;
        }
        #endregion
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            tracer.Trace("----------start1------------");

            //EntityReference certLibSyncRef = (EntityReference)context.InputParameters["Target"];
            Entity certLibSync = (Entity)context.InputParameters["Target"];

            /*
            if (certLibSyncRef != null)
            {
               // ColumnSet colSet = new ColumnSet(true);

                var colSet = new Microsoft.Xrm.Sdk.Query.ColumnSet("ddsm_boilers");



                certLibSync = service.Retrieve(certLibSyncRef.LogicalName, certLibSyncRef.Id, colSet);
            }
            */

            //            ColumnSet columnSet = new ColumnSet(true);

//            var columnSet = new Microsoft.Xrm.Sdk.Query.ColumnSet("ddsm_boilers");

            var columnSet = new Microsoft.Xrm.Sdk.Query.ColumnSet("ddsm_audio_video",
"ddsm_boilers",
"ddsm_ceiling_fans",
"ddsm_commercial_clothes_washers",
"ddsm_commercial_dishwashers",
"ddsm_commercial_fryers",
"ddsm_commercial_griddles",
"ddsm_commercial_hot_food_holding_cabinet",
"ddsm_commercial_mce_machines",
"ddsm_commercial_ovens",
"ddsm_commercial_refrigerators_and_freezers",
"ddsm_commercial_steam_cookers",
"ddsm_commercial_water_heaters",
"ddsm_computers",
"ddsm_data_center_storage",
"ddsm_decorative_light_strings",
"ddsm_dehumidifiers",
"ddsm_displays",
"ddsm_enterprise_servers",
"ddsm_furnaces",
"ddsm_geothermal_heat_pumps",
"ddsm_imaging_equipment",
"ddsm_large_network_equipment",
"ddsm_light_bulbs",
"ddsm_light_commercial_hvac",
"ddsm_light_fixtures",
"ddsm_non_ahri_cent_air_con_heat_pum",
"ddsm_pool_pumps",
"ddsm_products_lighting",
"ddsm_products_non_lighting",
"ddsm_residential_clothes_dryers",
"ddsm_residential_clothes_washers",
"ddsm_residential_dishwashers",
"ddsm_residential_freezers",
"ddsm_residential_refrigerators",
"ddsm_roof_products",
"ddsm_room_air_conditioners",
"ddsm_set_top_boxes",
"ddsm_small_network_equipment",
"ddsm_telephones",
"ddsm_televisions",
"ddsm_uninterruptible_power_supplies",
"ddsm_vending_machines",
"ddsm_ventilating_fans",
"ddsm_water_coolers",
"ddsm_water_heaters",
"ddsm_historic_room_air_cleaners",
"ddsm_keep_unchecked_categories",
"ddsm_certlibsyncconfid");

            QueryExpression query = new QueryExpression { EntityName = "ddsm_certlibsyncconf", ColumnSet = columnSet };
            query.Criteria.AddCondition("statuscode", ConditionOperator.Equal, 1);
            EntityCollection listCertLibSyncConf = service.RetrieveMultiple(query);

            Entity certLibSyncConf = listCertLibSyncConf.Entities[0];

            foreach (var col in columnSet.Columns)
            {
                object fieldValue = null;
                if (certLibSyncConf.Attributes.TryGetValue(col, out fieldValue))
                {
                    if (col == "ddsm_certlibsyncconfid")
                    {
                        EntityReference refCertLibSyncConfId = new EntityReference("ddsm_certlibsyncconf", certLibSyncConf.Id);

                        certLibSync["ddsm_certlibsyncconfid"] = refCertLibSyncConfId;
                    }
                    else
                    {
                        certLibSync[col] = fieldValue;
                    }
                }
                else
                {
                    tracer.Trace("can't get value for column: " + col);
                }
            }

            certLibSync["ddsm_name"] = "SyncOn_" + DateTime.Now.ToString();
        }
    }
}
