$("body").attr({"id": "bodyTop1"});
var spinnerForm;
function onLoad() {
    MakeButton("ddsm_esloadmetadata", "Load Energy Star Metadata", UpdateESMetadata);
}

function UpdateESMetadata() {
    if (Xrm.Page.getAttribute("ddsm_esurl").getValue() &&
        Xrm.Page.getAttribute("ddsm_eslogin").getValue() &&
        Xrm.Page.getAttribute("ddsm_espass").getValue()) {


        Alert.show("Are you sure you want to update Meta Data to a recent target date?", null, [{
                label: "Yes",
                callback: function() {
                    showPreloader(true);
                    window.EspMadMappingJson = [];
                    Process.callAction("ddsm_DDSMLoadESMetadata", [{
                                key: "Target",
                                type: Process.Type.EntityReference,
                                value: { id: Xrm.Page.data.entity.getId(), entityType: Xrm.Page.data.entity.getEntityName()}
                            }                           
                        ],
                        function(params) {
                            // Success
                            for (var i = 0; i < params.length; i++) {
                                console.log(params[i].key + "=" + params[i].value);
                            }
                            showPreloader(false);
                            var msg = JSON.parse(params[1].value);

                            Alert.show("ES Data was downloaded successfully", msg.ErrorMsg + ". Please Check the mapping.", [{
                                label: "Ok",
                                callback: function() {
                                    Xrm.Page.data.refresh();
                                }
                            }], "SUCCESS", 500, 200);
                            //   Xrm.Page.data.refresh();
                        },
                        function(e) {
                            // Error
                            console.log(e);
                            showPreloader(false);
                            Alert.show("During data download, the following error occurred!", e, null, "ERROR", 500, 200);
                        }
                    );
                }
            },
            {
                label: "No"
            }
        ], "QUESTION", 500, 200);

    } else {
        Alert.show("Please fill Energy Star connection details before continue!", null, null, "ERROR", 500, 200);
    }

}


function showPreloader(show) {
    if (show) {
// show overlay
        spinnerForm = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Loading data");
    }
    else {
        spinnerForm.stop();
    }
}

function MakeButton(atrname, caption, action) {
    if (document.getElementById(atrname) !== null) {
        var fieldId = "field" + atrname;
        if (document.getElementById(fieldId) === null) {
            var elementId = document.getElementById(atrname + "_d");
            var div = document.createElement("div");
            div.style.width = "175px";
            div.style.textAlign = "right";
            div.style.display = "inline";
            //  elementId.appendChild(div, elementId);

            //old style
            //margin-left: 3px; width: 80; display: none;
            div.innerHTML = '<button id="' + fieldId + '"  type="button" >' + caption + '</button>';
            document.getElementById(atrname).style.width = "0%";
            elementId.innerHTML = div.innerHTML;
            //  $(atrname+"_cl").hide();
            document.getElementById(fieldId).onclick = action;
            if (atrname == 'ddsm_espupdatemad' || atrname == 'ddsm_espupdatemad1') {
                document.getElementById(fieldId).style.width = "175px"
            }
            if (atrname == 'ddsm_espforceupdatemad') {
                document.getElementById(fieldId).style.width = "175px"
            }
            if (atrname == 'ddsm_assignsmartmeasurelibrary_btn' || atrname =='ddsm_esloadmetadata') {
                document.getElementById(fieldId).style.width = "175px"
            }

        }
        // hide main button
        //Xrm.Page.ui.controls.get(atrname).setVisible(false);
    }
}