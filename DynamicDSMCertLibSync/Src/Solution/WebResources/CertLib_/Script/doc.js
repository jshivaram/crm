var baseUrl = "/api/data/v8.1/";
var Xrm = window.parent.Xrm;
var clientUrl = Xrm.Page.context.getClientUrl();
var isFiltered = false;
var cats=[];

function initCategories(mapId){
    // multiselect of Target entities
    $("#esCategories").empty().kendoMultiSelect({
        filter: "startswith",
        ignoreCase: true,
        filtering: function() {
            isFiltered = true;
        },
        dataSource: {
            schema: {
                data: function(response) {
                    var data = response;
                    var json = data["ddsm_entities"];
                    var categories = window.parent.JSON.parse(json);
                    cats = categories;
                    return categories;
                } 
            },
            transport: {
                read: {
                    url: encodeURI(clientUrl + baseUrl + "ddsm_mappings("+ mapId +")?$select=ddsm_entities"),
                    dataType: "json",
                    beforeSend: function(req) {
                        req.setRequestHeader('Accept', 'application/json');
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader('OData-MaxVersion', "4.0");
                        req.setRequestHeader("OData-Version", "4.0");
                    }
                }
            }
        },
        dataTextField: "Label",
        dataValueField: "LogicalName",
        change: function(e) {
            var dropdownlist = $("#esCategories").data("kendoMultiSelect");
            var selectedEntity = Xrm.Page.getAttribute("ddsm_categoriesjson").setValue(
                window.parent.JSON.stringify(dropdownlist.value())
            );

            if (isFiltered) {
                e.preventDefault();
                return;
            }
        },
        dataBound: function(e) {
            if (isFiltered) {
                return;
            }
            var dropdownlist = $("#esCategories").data("kendoMultiSelect");         
                dropdownlist.value(cats);
                dropdownlist.enable(false);       
        },
    });
 }


window.top.showCategories = function showCategories(){
  //debugger;
    if (Xrm.Page.data.entity.getEntityName() === "ddsm_certlibsync" && 
                Xrm.Page.getAttribute("ddsm_certlibsyncconfid") && 
                Xrm.Page.getAttribute("ddsm_certlibsyncconfid").getValue() ) {
                var confId = Xrm.Page.getAttribute("ddsm_certlibsyncconfid").getValue()[0].id;
                var req = new XMLHttpRequest();
                req.open("GET", encodeURI(clientUrl + baseUrl +"ddsm_certlibsyncconfs("+confId.replace(/[{}]/g,"") + ")?$select=_ddsm_mappingid_value"), true);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("OData-MaxVersion", "4.0");
                req.setRequestHeader("OData-Version", "4.0");
                req.onreadystatechange = function() {
                    if (this.readyState == 4 /* complete */ ) {
                        req.onreadystatechange = null;
                        if (this.status == 200) {
                            var data = JSON.parse(this.response);  
                              initCategories(data._ddsm_mappingid_value.replace(/[{}]/g,""));            
                        //dropdownlist.value(window.parent.JSON.parse());
                        //dropdownlist.enable(false);
                        } else {
                            var error = JSON.parse(this.response).error;
                            alert("Error retrieving contact – " + error.message);
                        }
                    }
                };
                req.send();

        } else {
            var mappingControl = Xrm.Page.getAttribute("ddsm_mappingid");
            if(mappingControl){
                var mappingValue = mappingControl.getValue();
                if(mappingValue && mappingValue.length >0)
                initCategories(mappingValue[0].id.replace(/[{}]/g,""));
            }
        }
    }
window.top.showCategories();
