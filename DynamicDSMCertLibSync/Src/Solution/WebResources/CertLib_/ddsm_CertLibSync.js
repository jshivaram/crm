﻿/**
 * Created by tkach on 8/25/2016.
 */
$("body").attr({ "id": "bodyTop1" });
var spinnerForm;
window.EspMadMappingJson = [];
window.QddMappingJson = [];


function CustomCreate() {
    CustomCreateCallBack();
}

function CustomCreateCallBack() {
    var idLib = "";
    var organizationUrl = Xrm.Page.context.getClientUrl();
    var query = "ddsm_DDSMCreateCertLibSyncRecord";
    var req = new XMLHttpRequest();
    req.open("POST", organizationUrl + "/api/data/v8.0/" + query, true);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.setRequestHeader("OData-MaxVersion", "4.0");
    req.setRequestHeader("OData-Version", "4.0");
    req.onreadystatechange = function () {
        if (this.readyState == 4) {
            req.onreadystatechange = null;
            if (this.status == 200) {
                var data = JSON.parse(this.response);
                if (data.Complete) {
                    idLib = data.Result;
                    Xrm.Utility.openEntityForm("ddsm_certlibsync", idLib);
                }

            } else {
                var error = JSON.parse(this.response).error;
                console.log(error.message);
                Alert.show("Error Record Creation", error.message, null, "ERROR", 450, 250);
            }
        }
    };
    req.send();
}

function OnLoadPage() {
    Xrm.Page.getAttribute("ddsm_status").setSubmitMode("always");
    Xrm.Page.getAttribute("ddsm_startdate").setSubmitMode("always");
    Xrm.Page.getAttribute("ddsm_name").setSubmitMode("always");

    if (Xrm.Page.getAttribute("ddsm_status").getValue() == "962080002") {
        Xrm.Page.ui.controls.get("ddsm_isdraft").setDisabled(true);
    }

    setTimeout(function () {
        //TODO: uncomment for prod
        $('.processStep').hide();
        $(".processStep").css("display", "none");
        $("#processActionsContainer").remove();
        $("#leftScrollButton").remove();
        $("#rightScrollButton").remove();

        $(".processStepsContent").width(450);
        $(".processStepsContent .processStepColumn").width(450);
        $(".processStepsContent .processStepColumn .processStep").width(450);
        $(".processStepsContent .processStepColumn .processStepValue").width(450 - 31 - $(".processStepsContent .processStepColumn .processStepLabel").width());
        var tmpWidth = $(".processStagesContainer").width();
        tmpWidth = tmpWidth + 91;
        $(".processStagesContainer").width(tmpWidth);

    }, 100);
}

function ChangeIsDraft() {
    var valIsDraft = Xrm.Page.getAttribute("ddsm_isdraft").getValue();
    if (valIsDraft) {
        Xrm.Page.data.save();
        return true;
    } else {
        Xrm.Page.data.save();
        return false;
    }
}

function ChangeStatus() {
    Alert.show("Are you sure you want to start data syncronization?", "Don't close this window while synchronization is work.", [{
        label: "Yes",
        callback: function () {
            Xrm.Page.getAttribute("ddsm_status").setValue(962080001);
            Xrm.Page.getAttribute("ddsm_startdate").setValue(new Date());
            Xrm.Page.data.save().then(function () {
                setTimeout(CertLoadData, 500);
            }, errorCallback);
        }
    },
    {
        label: "No"
    }
    ], "QUESTION", 500, 200);
}

errorCallback = function (errorCode, errorLocalized) {
    alert(errorLocalized);
}

function CertLoadData() {
    //showPreloader(true);
    setTimeout(function () {
        $('.processStep').hide();
        $(".processStep").css("display", "none");

        $("#processActionsContainer").remove();
        $("#leftScrollButton").remove();
        $("#rightScrollButton").remove();

        $("#processStagesScrollRegion div.processStageContainer.selectedStage.completedStage").addClass("unselectedStage");
        $("#processStagesScrollRegion div.processStageContainer.selectedStage.completedStage").removeClass("selectedStage");
        $("#processStagesScrollRegion div.processStageContainer.activeStage").removeClass("unselectedStage");
        $("#processStagesScrollRegion div.processStageContainer.activeStage").addClass("selectedStage");

        $(".processStepsContent").width(450);
        $(".processStepsContent .processStepColumn").width(450);
        $(".processStepsContent .processStepColumn .processStep").width(450);
        $(".processStepsContent .processStepColumn .processStepValue").width(450 - 31 - $(".processStepsContent .processStepColumn .processStepLabel").width());

        var tmpWidth = $(".processStagesContainer").width();
        tmpWidth = tmpWidth + 91;
        $(".processStagesContainer").width(tmpWidth);

    }, 100);
    var params = [{ key: "Target", type: Process.Type.EntityReference, value: { id: Xrm.Page.data.entity.getId(), entityType: Xrm.Page.data.entity.getEntityName() } }];

    Process.callAction("ddsm_CertLoadDataAction", params,
        function (params) {
            showPreloader(false);
            Alert.show("Sync successfully completed.", null, [{
                label: "Ok",
                callback: function () {
                    Xrm.Utility.openEntityForm("ddsm_certlibsync", Xrm.Page.data.entity.getId());
                    // Xrm.Page.data.refresh();
                }
            }], "SUCCESS", 500, 200);
            for (var i = 0; i < params.length; i++) {
                if (params[i].key == "Complete") {
                    if (params[i].value == "true") {
                        Xrm.Page.ui.controls.get("ddsm_isdraft").setDisabled(true);

                        setTimeout(function () {
                            document.getElementById('WebResource_ChartCertLibSync').contentWindow.ShowChart();
                        }, 1000);
                        setTimeout(function () {
                            $('.processStep').hide();
                            $(".processStep").css("display", "none");
                            $("#processActionsContainer").remove();
                            $("#leftScrollButton").remove();
                            $("#rightScrollButton").remove();

                            $("#processStagesScrollRegion div.processStageContainer.selectedStage.completedStage").addClass("unselectedStage");
                            $("#processStagesScrollRegion div.processStageContainer.selectedStage.completedStage").removeClass("selectedStage");
                            $("#processStagesScrollRegion div.processStageContainer.activeStage").removeClass("unselectedStage");
                            $("#processStagesScrollRegion div.processStageContainer.activeStage").addClass("selectedStage");

                            $(".processStepsContent").width(450);
                            $(".processStepsContent .processStepColumn").width(450);
                            $(".processStepsContent .processStepColumn .processStep").width(450);
                            $(".processStepsContent .processStepColumn .processStepValue").width(450 - 31 - $(".processStepsContent .processStepColumn .processStepLabel").width());

                            var tmpWidth = $(".processStagesContainer").width();
                            tmpWidth = tmpWidth + 91;
                            $(".processStagesContainer").width(tmpWidth);

                        }, 100);
                    }
                }
            }
        },
        function (e) {
            console.log("error: " + e);
            showPreloader(false);
            if(e || e !=""){
                    Alert.show("Syncronization error", e, null, "ERROR", 500, 200);
            }    
        }
    );
    Alert.show("Models Number sync has started. You can check sync status refreshing the record.", null, null, "WARNING", 500, 200);
}

function showPreloader(show) {
    var _targetSpinner = document.getElementById("bodyTop1");
    if (show) {
        $("body").attr({ "id": "bodyTop1" });
        var popupHTML = '<div id="overlay" style="display:none; position:fixed; z-index:998; opacity:0.5; filter:alpha(opacity=50); top:0;    right:0; left:0; bottom:0;background:#000000;"></div>';
        $('body').append(popupHTML);
        $('#overlay').show();
        var spinnerForm = CreaLab.Spinner.spin(_targetSpinner, "Syncronization is in progress. This should take a while, you can see the progress and info in this record.");
    } else {
        $('#overlay').hide();
        var spinnerForm = CreaLab.Spinner.spin(_targetSpinner);
        spinnerForm.stop();
    }
}

function onSave(context) { }
