function SyncMetadata() {  
    var orgTypeText = Xrm.Page.getAttribute("ddsm_certifyingorganizationtype").getText();
    var msg = "Are you sure you want to start "+ orgTypeText+" Metadata syncronization?";
    Alert.show(msg, "This action will remove all existing categories and their metadata.", [{
            label: "Yes",
            callback: function() {
            var params = [{ key: "Target", type: Process.Type.EntityReference, value: { id: Xrm.Page.data.entity.getId(), entityType: Xrm.Page.data.entity.getEntityName() } }];
            
                Process.callAction("ddsm_DDSMCertLoadMetaDataAction", params,
                    function(params) {
                       // showPreloader(false);
                        Alert.show("Metadata loaded successfully!", null, [{
                            label: "Ok",
                            callback: function() {
                                Xrm.Page.data.refresh();
                            }
                        }], "SUCCESS", 500, 200);
                        for (var i = 0; i < params.length; i++) {
                            if (params[i].key == "Complete") {
                                if (params[i].value == "true") {
	
                                }
                            }
                        }
                    },
                    function(e) {
                        console.log("error: " + e);                      
                        Alert.show("Syncronization error", e, null, "ERROR", 500, 200);
                    }
                );
                Alert.show("Sync was started! Check status in 15 minutes", null, null, "WARNING", 500, 200);
                }
            },
            {
                label: "No"
            }
        ], "QUESTION", 500, 200);
    }