﻿<?xml version="1.0" ?><xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"><xsl:output method="text" indent="no"/><xsl:template match="/data"><![CDATA[<p class=MsoNormal>Dear &#160;]]><xsl:choose><xsl:when test="account/ddsm_firstname"><xsl:value-of select="account/ddsm_firstname" /></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose><![CDATA[ ]]><xsl:choose><xsl:when test="account/ddsm_lastname"><xsl:value-of select="account/ddsm_lastname" /></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose><![CDATA[ :&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>Thank you for applying to Efficiency Nova Scotia’s Green Heat Program. We are pleased to inform you that you have been pre-approved for a rebate of $300 for a new cold climate mini-split heat pump. If more than one single zone system is installed, each subsequent system will receive a $150 rebate. For multi-zone systems, every additional indoor head after the first will receive a $150 rebate. Your confirmation number is: [Project Number]&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&quot;Your Next Steps</p>

<p class=MsoNormal>1.&#160;&#160;&#160;&#160;&#160;&#160;&#160; Install your system. Be sure to follow the equipment and installation criteria listed below.</p>

<p class=MsoNormal>2.&#160;&#160;&#160;&#160;&#160;&#160;&#160; Obtain copies of installation receipts/invoices including the Refrigeration and Air Conditioning Mechanic (RACM) certificate #.</p>

<p class=MsoNormal>3.&#160;&#160;&#160;&#160;&#160;&#160;&#160; Take photos of the indoor and outdoor components of the system.</p>

<p class=MsoNormal>4.&#160;&#160;&#160;&#160;&#160;&#160;&#160; Your rebate cheque will be mailed to you when complete. If you wish to receive your rebate via direct deposit (optional), please complete the Electronic Funds Transfer (EFT) form.</p>

<p class=MsoNormal>5.&#160;&#160;&#160;&#160;&#160;&#160;&#160; Submit all of the above documentation via mail, email or fax to the Green Heat program.</p>

<p class=MsoNormal>&quot;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>These steps must be completed within 6 months from the date of this email ([GH-Pre-Approval Email Template Sent Date]).&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&quot;Mail:&#160;&#160; Green Heat</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 230 Brownlow Ave, Suite 300</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; Dartmouth NS, B3B 0G5</p>

<p class=MsoNormal>Email: greenheat@efficiencyns.ca</p>

<p class=MsoNormal>Fax:&#160;&#160;&#160;&#160; 902 470 3599&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&quot;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&quot;Equipment Eligibility Criteria</p>

<p class=MsoNormal>To be eligible for this program, ductless mini-split heat pumps must meet the following criteria:</p>

<p class=MsoNormal>•&#160;&#160;&#160;&#160;&#160;&#160;&#160; Unit must be on Efficiency Nova Scotia's Cold Climate Heat Pump List;</p>

<p class=MsoNormal>•&#160;&#160;&#160;&#160;&#160;&#160;&#160; ENERGY STAR (version 5.0 or higher) certified and appear on Natural Resource Canada’s (NRCan’s) list of eligible products;</p>

<p class=MsoNormal>•&#160;&#160;&#160;&#160;&#160;&#160;&#160; Single-zone systems must have a Heating Seasonal Performance Factor (HSPF) (Region 5 - Canada) greater or equal to 9.0;</p>

<p class=MsoNormal>•&#160;&#160;&#160;&#160;&#160;&#160;&#160; Multi-zone systems must have a HSPF (Region 5 - Canada) greater or equal to 8.0;</p>

<p class=MsoNormal>•&#160;&#160;&#160;&#160;&#160;&#160;&#160; Coefficient of Performance (COP) at -15 °C greater or equal to 1.75.</p>

<p class=MsoNormal>•&#160;&#160;&#160;&#160;&#160;&#160;&#160; System must be installed by a certified Refrigeration and Air Conditioning Mechanic (RACM).</p>

<p class=MsoNormal>&#160;</p>

<p class=MsoNormal>&quot;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>For questions on equipment eligibility, please contact us, using the contact information below, to confirm prior to purchasing.&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>Please note that systems purchased or installed prior to the receipt of this pre-approval confirmation letter are not eligible for incentives.&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>You can find more information on all our residential programs at efficiencyns.ca/residential&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>Kind regards,&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal><b><span style="font-size:11.5pt;line-height:107%;font-family:Arial, sans-serif;color:rgb(0, 176, 240);background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-attachment:initial;background-origin:initial;background-clip:initial;">Green Heat Team</span></b>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal><span style="font-size:8.5pt;line-height:107%;font-family:Arial, sans-serif;color:rgb(0, 176, 240);background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-attachment:initial;background-origin:initial;background-clip:initial;">Email </span><a>greenheat@efficiencyns.ca</a>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal><span style="font-size:8.5pt;line-height:107%;font-family:Arial, sans-serif;color:rgb(0, 176, 240);background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-attachment:initial;background-origin:initial;background-clip:initial;">Main</span>&#160; 877 999 6035&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal><span style="font-size:8.5pt;line-height:107%;font-family:Arial, sans-serif;color:rgb(0, 176, 240);background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-attachment:initial;background-origin:initial;background-clip:initial;">Fax</span>&#160; 902 470 3599&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal><b><span style="font-size:14.5pt;line-height:107%;font-family:Arial, sans-serif;color:rgb(146, 208, 80);background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-attachment:initial;background-origin:initial;background-clip:initial;">Efficiency Nova Scotia</span></b><span style="font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;">&#160;&#160;&#160;&#160;&#160;&#160; </span></p>

<p class=MsoNormal><span style="font-size:8.5pt;line-height:107%;font-family:Arial, sans-serif;color:rgb(88, 89, 91);background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-attachment:initial;background-origin:initial;background-clip:initial;">230 Brownlow Avenue, Suite 300 | Dartmouth, NS | B3B 0G5</span>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal><a href="http://efficiencyns.ca/">efficiencyns.ca</a> | <a href="http://www.twitter.com/efficiencyns/">@efficiencyns</a> | <a href="http://www.facebook.com/efficiencyns">facebook.com/efficiencyns</a>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>This email is intended for {ddsm_accountid.emailaddress1}.&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal><u><span style="font-size:11.5pt;line-height:107%;color:darkblue;background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-attachment:initial;background-origin:initial;background-clip:initial;">EfficiencyOne Privacy Policy</span></u>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</p>

<p class=MsoNormal>&#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;&#160;</p><font face="Tahoma, Verdana, Arial" size=2 style="display:inline;"></font>]]></xsl:template></xsl:stylesheet>