﻿<?xml version="1.0" ?><xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"><xsl:output method="text" indent="no"/><xsl:template match="/data"><![CDATA[<p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><span lang=EN-US>Dear&#160;{ddsm_accountid.ddsm_firstname}&#160;{ddsm_accountid.ddsm_lastname}:</span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><span lang=EN-US>Thank you for submitting your Green Heat Completion Form.</span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><span lang=EN-US>Our records indicate that you were pre-approved on {ddsm_projectpreapprovaldate},&#160;however&#160;your invoice indicates that the system was purchased and/or installed prior to this date. Unfortunately, we are not able to grant incentives for equipment purchased or installed prior to pre-approval. As a result, your project is not eligible for an incentive.</span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><span lang=EN-US>As a publically-funded organization, we are required to demonstrate that our incentives encourage energy efficiency; and, pre-approvals confirm that Efficiency Nova Scotia’s incentives affected your purchasing decision.</span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><span lang=EN-US>We understand this may be disappointing; and, while we are unable to make exceptions, we hope you enjoy energy savings as a result of installing your new equipment. And, when you’re ready to start your next project, we welcome you to contact us for information on how we can support your energy efficiency efforts. You can find more information on all our residential programs at efficiencyns.ca/residential</span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><span lang=EN-US>&#160;</span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><span lang=EN-US>Kindest regards,</span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><span lang=EN-US>&#160;</span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><b><span lang=EN-US style="color:#00B0F0;">Green Heat Team</span></b><span lang=EN-US></span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><span lang=EN-US style="color:#00B0F0;">Email&#160;</span><a href="mailto:greenheat@efficiencyns.ca"><span lang=EN-US style="border:1pt none windowtext;padding:0in;">greenheat@efficiencyns.ca</span></a><span lang=EN-US></span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><span lang=EN-US style="color:#00B0F0;">Main&#160;</span><span lang=EN-US>877 999 6035</span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><span lang=EN-US style="color:#00B0F0;">Fax&#160;</span><span lang=EN-US>902 470 3599</span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><span lang=EN-US style="color:#00B0F0;">&#160;</span><span lang=EN-US></span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;line-height:normal;"><b><span lang=EN-US style="color:#92D050;">Efficiency Nova Scotia</span></b><span lang=EN-US></span></p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;line-height:normal;"><span lang=EN-US style="color:#58595B;">230 Brownlow Avenue, Suite 300 | Dartmouth, NS | B3B 0G5</span><span lang=EN-US></span></p><p class=MsoNormal>





























</p><p class=MsoNormal style="margin-top:9.0pt;margin-right:0in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;line-height:normal;"><a href="http://efficiencyns.ca/"><span lang=EN-US style="border:1pt none windowtext;padding:0in;">efficiencyns.ca</span></a><span lang=EN-US>&#160;|&#160;</span><a href="http://www.twitter.com/efficiencyns/"><span lang=EN-US style="border:1pt none windowtext;padding:0in;">@efficiencyns</span></a><span lang=EN-US>&#160;|&#160;</span><a href="http://www.facebook.com/efficiencyns"><span lang=EN-US style="border:1pt none windowtext;padding:0in;">facebook.com/efficiencyns</span></a><span lang=EN-US>&#160;&#160;</span></p><font face="Tahoma, Verdana, Arial" size=2 style="display:inline;"></font>]]></xsl:template></xsl:stylesheet>