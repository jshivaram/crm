﻿<?xml version="1.0" ?><xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"><xsl:output method="text" indent="no"/><xsl:template match="/data"><![CDATA[<div><span style="font-size:110%;font-family:Calibri;" data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Hello {ddsm_accountid.name},&quot;}" data-sheets-userformat="{&quot;2&quot;:13057,&quot;3&quot;:{&quot;1&quot;:0},&quot;11&quot;:4,&quot;12&quot;:0,&quot;15&quot;:&quot;Calibri&quot;,&quot;16&quot;:11}"><span style="font-size:110%;" data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Congratulations on completing your energy efficiency project. We have received your Completion Form and will begin processing your incentive. We will reach out to you if we have any questions while completing this process. You can expect to receive your cheque by mail within 4-6 weeks. &quot;}" data-sheets-userformat="{&quot;2&quot;:13057,&quot;3&quot;:{&quot;1&quot;:0},&quot;11&quot;:4,&quot;12&quot;:0,&quot;15&quot;:&quot;Calibri&quot;,&quot;16&quot;:11}"><span style="font-size:110%;" data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Efficiency Nova Scotia helps families, businesses, non-profit organizations, and institutions save energy and money by connecting you to the expertise, guidance and resources you need to take action. Since 2010, we\u2019ve helped Nova Scotians reduce the province's annual electricity load by over 7%, making Nova Scotia a North American leader in energy conservation.&quot;}" data-sheets-userformat="{&quot;2&quot;:15105,&quot;3&quot;:{&quot;1&quot;:0},&quot;11&quot;:4,&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:0},&quot;15&quot;:&quot;Calibri&quot;,&quot;16&quot;:11}"><span style="font-size:110%;" data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;When you're ready to start your next project, remember to contact us to see how we can help. You can find out about all of Efficiency Nova Scotia's programs and services for homes here.&quot;}" data-sheets-userformat="{&quot;2&quot;:12801,&quot;3&quot;:{&quot;1&quot;:0},&quot;12&quot;:0,&quot;15&quot;:&quot;Calibri&quot;,&quot;16&quot;:11}"><span style="font-size:110%;font-family:Arial;font-weight:bold;color:#00b0f0;" data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Green Heat Team&quot;}" data-sheets-userformat="{&quot;2&quot;:31233,&quot;3&quot;:{&quot;1&quot;:0},&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:45296},&quot;15&quot;:&quot;\&quot;Arial\&quot;,sans-serif&quot;,&quot;16&quot;:11,&quot;17&quot;:1}"><p class=MsoNormal style="margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;"><span style="color:rgb(0, 0, 0);font-family:Calibri;font-size:15px;font-weight:normal;white-space:pre-wrap;">Hello {ddsm_accountid.name},</span><br></p>

<p class=MsoNormal style="line-height:normal;"><span style="color:rgb(0, 0, 0);font-family:Calibri;font-size:15px;font-weight:normal;white-space:pre-wrap;">Congratulations on completing your energy efficiency project. We have received your Completion Form and will begin processing your incentive. We will reach out to you if we have any questions while completing this process. You can expect to receive your cheque by mail within 4-6 weeks. </span></p><p class=MsoNormal style="line-height:normal;"><span style="color:rgb(0, 0, 0);font-family:Calibri;font-size:15px;font-weight:normal;white-space:pre-wrap;">Efficiency Nova Scotia helps families, businesses, non-profit organizations, and institutions save energy and money by connecting you to the expertise, guidance and resources you need to take action. Since 2010, we’ve helped Nova Scotians reduce the province's annual electricity load by over 7%, making Nova Scotia a North American leader in energy conservation.</span></p><p class=MsoNormal style="line-height:normal;"><span style="font-family:&quot;Tahoma&quot;,sans-serif;color:black;"></span></p>

<p class=MsoNormal style="line-height:normal;"><span style="color:rgb(0, 0, 0);font-family:Calibri;font-size:15px;font-weight:normal;white-space:pre-wrap;">When you're ready to start your next project, remember to contact us to see how we can help. You can find out about all of Efficiency Nova Scotia's programs and services for homes here.</span><span style="font-size:12.0pt;color:black;"><br>
<br>
</span><span style="font-size:9.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:black;"></span></p>

<p class=MsoNormal style="line-height:normal;"><b><span style="font-size:11.5pt;font-family:Arial, sans-serif;">Green Heat Team</span></b><span style="font-size:13.0pt;color:black;"><br>
</span><b><span style="font-size:8.5pt;font-family:Arial, sans-serif;">Email</span></b><b><span style="font-size:11.5pt;font-family:Arial, sans-serif;">&#160;</span></b><b><u><span style="font-size:8.5pt;font-family:&quot;Arial&quot;,sans-serif;color:blue;">greenheat@efficiencyns.ca</span></u></b><b><span style="font-size:14.5pt;font-family:Arial, sans-serif;"><br>
</span></b><span style="font-size:8.5pt;font-family:Arial, sans-serif;">Main </span><span style="font-size:8.5pt;font-family:&quot;Arial&quot;,sans-serif;color:black;">877 999 6035</span></p>

<p class=MsoNormal style="margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;"><span style="font-size:8.5pt;font-family:Arial, sans-serif;">Fax </span><span style="font-size:8.5pt;font-family:&quot;Arial&quot;,sans-serif;color:black;"><span style="white-space:pre-wrap;">902 470 3599</span></span></p><p class=MsoNormal style="margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;"><span style="font-size:8.5pt;font-family:&quot;Arial&quot;,sans-serif;color:black;"><span style="white-space:pre-wrap;"><br></span></span></p>

<p class=MsoNormal style="margin-bottom:0in;margin-bottom:.0001pt;line-height:normal;"><b><span style="font-size:14.5pt;font-family:Arial, sans-serif;color:rgb(146, 208, 80);background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-attachment:initial;background-origin:initial;background-clip:initial;">Efficiency Nova Scotia</span></b><span style="font-size:8.5pt;font-family:&quot;Arial&quot;,sans-serif;color:black;"></span></p>

<p class=MsoNormal style="margin-bottom:0in;margin-bottom:.0001pt;"><span style="font-size:8.5pt;line-height:107%;font-family:Arial, sans-serif;color:rgb(88, 89, 91);background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-attachment:initial;background-origin:initial;background-clip:initial;">230 Brownlow Avenue, Suite 300 | Dartmouth, NS | B3B 0G5</span></p>

<p class=MsoNormal style="margin-bottom:0in;margin-bottom:.0001pt;"><span style="font-size:8.5pt;line-height:107%;font-family:Arial, sans-serif;color:rgb(88, 89, 91);background-image:initial;background-position:initial;background-size:initial;background-repeat:initial;background-attachment:initial;background-origin:initial;background-clip:initial;">efficiencyns.ca | @efficiencyns | facebook.com/efficiencyns</span></p></span></span></span></span></span></div><style>
td {
border:1px solid #ccc;
}

br {
}
</style><style>
td {
border:1px solid #ccc;
}

br {
}
</style><style>
td {
border:1px solid #ccc;
}

br {
}
</style>]]></xsl:template></xsl:stylesheet>