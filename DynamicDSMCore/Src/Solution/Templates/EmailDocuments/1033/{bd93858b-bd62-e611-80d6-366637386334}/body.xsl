﻿<?xml version="1.0" ?><xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"><xsl:output method="text" indent="no"/><xsl:template match="/data"><![CDATA[<p style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;margin-left:0in;"><span style="font-size:11pt;font-family:Calibri, sans-serif;">Hi&#160;<strong><span style="font-weight:normal;">{ddsm_primarycontactid.firstname}</span></strong>,</span></p><p style="margin:9pt 0in;"><span style="font-size:11pt;font-family:Calibri, sans-serif;">
Thank you for your participation in<b>&#160;</b><strong><span style="font-weight:normal;">Efficiency
Nova Scotia’s Efficient Product Installation Service</span></strong><b>.</b> We hope your residents are enjoying their new
products which could save up to $<strong><span style="font-weight:normal;">{ddsm_kwhsavingsatmeterperyear}</span></strong>&#160;per
year.&#160;</span></p><p style="margin:9pt 0in;"><span style="font-size:11pt;font-family:Calibri, sans-serif;"><br>
Efficiency Nova Scotia helps families, businesses, non-profits and institutions
save energy and money by connecting you to the expertise, guidance and
resources you need to take action. Since 2010, we’ve helped Nova Scotians
reduce our annual electricity load by over 7%, making us North American leaders
in energy conservation. Find more ways to save&#160;<a href="https://efficiencyns.ca/business/business-tools-resources/create-a-business-profile/" data-mce-href="https://efficiencyns.ca/business/business-tools-resources/create-a-business-profile/"><span style="border:1pt none windowtext;padding:0in;">here</span></a>.</span></p><p style="margin:9pt 0in 12pt;"><span style="font-size:11pt;font-family:Calibri, sans-serif;"><br>
Should you encounter any issues or concerns with the installed product, please
contact our Efficiency Partner,<b>&#160;</b><strong><span style="font-weight:normal;">{ddsm_deliveryagent.ddsm_companyname}</span></strong>&#160;at<b>&#160;</b><strong><span style="font-weight:normal;">{ddsm_deliveryagent.telephone1}</span></strong><b>.</b><br>
<br>
Your friends at Efficiency Nova Scotia&#160;<br>
230 Brownlow Ave, Suite 300&#160;<br>
Dartmouth Nova Scotia B3B 0G5&#160;<br>
Canada&#160;<br>
<br>
<br>
This email is intended for&#160;<strong><span style="font-weight:normal;">{ddsm_accountid.emailaddress1}</span></strong><b>.</b></span></p><p style="margin:9pt 0in;"><span style="font-size:11pt;font-family:Calibri, sans-serif;"><a href="http://www.efficiencyone.ca/privacy-policy/" title="EfficiencyOne Privacy Policy" data-mce-href="http://www.efficiencyone.ca/privacy-policy/"><span style="border:1pt none windowtext;padding:0in;">EfficiencyOne Privacy Policy</span></a></span></p><p style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:11px;">







</p><p style="margin:9pt 0in;"><span style="font-size:8.5pt;font-family:Verdana, sans-serif;">&#160;</span></p><font face="Tahoma, Verdana, Arial" size=2 style="display:inline;"></font>]]></xsl:template></xsl:stylesheet>