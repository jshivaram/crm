﻿using DDSM.SmartMeasureCalculationPlugin.Model;
using DDSM_CalculationProgramInterval;
using DDSM_CalculationProgramInterval.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

public class CalculateDSMPalnTotalsField : CodeActivity
{
    #region "Parameter Definition"
    [Input("DSM Plan"), ArgumentEntity("ddsm_dsmplan"), ReferenceTarget("ddsm_dsmplan")]
    public InArgument<EntityReference> DSMPlan { get; set; }

    [Input("User Input")]
    public InArgument<string> UserInput { get; set; }

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }

    #endregion


    #region Internal fields
    readonly string MainEntity = "ddsm_dsmplan";
    Common objCommon;
    IOrganizationService service;
    ExecuteMultipleRequest requestWithResults;
    #endregion

    protected override void Execute(CodeActivityContext context)
    {
        #region "Load CRM Service from context"
        objCommon = new Common(context);
        service = objCommon.Service;
        objCommon.TracingService.Trace("Load CRM Service from context --- OK");
        #endregion

        var dsmPl = DSMPlan.Get(context);
        //Target Entity
        EntityReference Target = (EntityReference)objCommon.Context.InputParameters["Target"];

        string inputP = null;
        string inputTargetId = null;

        objCommon.TracingService.Trace("Iput Parameters:");
        //Target ID
        if (Target != null)
        {
            inputTargetId = Target.Id.ToString();
            objCommon.TracingService.Trace("Iput Parameters. Target ID: " + inputTargetId);
        }
        else { objCommon.TracingService.Trace("Target ID is null"); }
        //Program
        if (dsmPl != null)
        {
            inputP = dsmPl.Id.ToString();
            objCommon.TracingService.Trace("Portfolio: " + inputP);
        }
        else { objCommon.TracingService.Trace("Portfolio is null"); }

        try
        {
            requestWithResults = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            var userInput = UserInput.Get(context);
            var listId = new List<Guid>();
            var planRef = DSMPlan.Get(context);

            if (!string.IsNullOrEmpty(userInput) && planRef == null)
            {
                UserInputObj2 userInpObj = null;

                userInpObj = JsonConvert.DeserializeObject<UserInputObj2>(userInput);
                if (userInpObj?.SmartMeasures != null)
                {
                    listId = userInpObj.SmartMeasures.Where(x => !x.Equals(Guid.Empty)).Distinct().ToList();
                }
            }
            else if (planRef != null)
            {
                /*var programIntervalsTotals = GetProgramIntervalsTotals(planRef.Id);
                UpdateTotals(planRef, programIntervalsTotals);*/
                listId.Add(planRef.Id);
            }
            else if (planRef == null)
            {
                if (Target != null)
                {
                    listId.Add(Target.Id);
                }
            }
            else
            {
                throw new Exception("DSM Plan parameter is null. Please check it.");
            }


            foreach (var plan in listId)
            {
                var programIntervalsTotals = GetProgramIntervalsTotals(plan);
                UpdateTotals(plan, programIntervalsTotals);
            }

            if (requestWithResults.Requests.Count > 0)
            {
                ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)service.Execute(requestWithResults);
                if (responseWithResults.IsFaulted)
                {
                    objCommon.TracingService.Trace("Update of Portfolio failed! Check correct names fileds or fetch. ");
                }
                else
                {
                    objCommon.TracingService.Trace(requestWithResults.Requests.Count + " updates was done. DSM Plans was updated.");
                }
            }
            else
            {
                objCommon.TracingService.Trace("DSM Plans not updated. No items to update.");
            }
            //if (requestWithResults.Requests.Count > 0)
            //{
            //    ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)service.Execute(requestWithResults);
            //}          
            //If need create Task
            Complete.Set(context, true);
        }
        catch (Exception exception)
        {
            objCommon.TracingService.Trace("Error of DSM Plan totals calculation. Error:" + exception.Message);
            Complete.Set(context, false);
        }
    }

    private Entity GetDSMPlan(Guid planId)
    {
        string[] columns = { "ddsm_dsmplanid" };
        return service.Retrieve(MainEntity, planId, new ColumnSet(columns));
    }

    private EntityCollection GetProgramIntervalsTotals(Guid planId)
    {
        EntityCollection entitys = new EntityCollection();
        string str = (Guid.Empty != planId) ? string.Format("<condition attribute='ddsm_dsmplanid' operator='eq' value='{0}' />", planId) : "";
        string format = @"<fetch aggregate='true'>
                        <entity name='ddsm_portfolio'>    
                        <attribute name='ddsm_forecastedkwnetsavingsatgenerator' alias='ddsm_ForecastedkWNetSavingsatGenerator' aggregate='sum' />
                        <attribute name='ddsm_forecastedkwhnetsavingsatgenerator' alias='ddsm_ForecastedkWhNetSavingsatGenerator' aggregate='sum' />
                        <attribute name='ddsm_forecastedspending' alias='ddsm_ForecastedSpending' aggregate='sum' />
                        <attribute name='ddsm_forecastedgjsavingsatmeter' alias='ddsm_ForecastedGJSavingsatMeter' aggregate='sum' />
                        <attribute name='ddsm_pnsforecastedspending' alias='ddsm_PNSForecastedSpending' aggregate='sum' />
                        <attribute name='ddsm_actualallkwhnetsavingsatgenerator' alias='ddsm_ActualAllkWhNetSavingsatGenerator' aggregate='sum' />
                        <attribute name='ddsm_actualallkwnetsavingsatgenerator' alias='ddsm_ActualAllkWNetSavingsatGenerator' aggregate='sum' />
                        <attribute name='ddsm_actualspending' alias='ddsm_ActualSpending' aggregate='sum' />
                        <attribute name='ddsm_actualallgjsavingsatmeter' alias='ddsm_ActualAllGJSavingsatMeter' aggregate='sum' />
                        <attribute name='ddsm_pnsactualspending' alias='ddsm_PNSActualSpending' aggregate='sum' />
                        <attribute name='ddsm_targetkwhnetsavingsatgenerator' alias='ddsm_TargetkWhNetSavingsatGenerator' aggregate='sum' />
                        <attribute name='ddsm_targetkwnetsavingsatgenerator' alias='ddsm_TargetkWNetSavingsatGenerator' aggregate='sum' />
                        <attribute name='ddsm_budget' alias='ddsm_Budget' aggregate='sum' />
                        <attribute name='ddsm_numberofparticipants' alias='ddsm_NumberofParticipants' aggregate='sum' />
                        <attribute name='ddsm_estimatedkwhnetsavingsatgenerator' alias='ddsm_EstimatedkWhNetSavingsatGenerator' aggregate='sum' />
                        <attribute name='ddsm_estimatedkwnetsavingsatgenerator' alias='ddsm_EstimatedkWNetSavingsatGenerator' aggregate='sum' />
                        <attribute name='ddsm_estimatedspending' alias='ddsm_EstimatedSpending' aggregate='sum' />
                        <attribute name='ddsm_directprogramunitcost' alias='ddsm_DirectProgramUnitCost' aggregate='sum' />
                        <attribute name='ddsm_targetgjsavingsatmeter' alias='ddsm_TargetGJSavingsatMeter' aggregate='sum' />
                        <attribute name='ddsm_pnsbudget' alias='ddsm_PNSBudget' aggregate='sum' />
                        <attribute name='ddsm_pnsnumberofparticipants' alias='ddsm_PNSNumberofParticipants' aggregate='sum' />
                        <attribute name='ddsm_pnsestimatedspending' alias='ddsm_PNSEstimatedSpending' aggregate='sum' />
                        <attribute name='ddsm_estimatedgjsavingsatmeter' alias='ddsm_EstimatedGJSavingsatMeter' aggregate='sum' />
                        <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        {0}                                                   
                        </filter>    
                        </entity>
                        </fetch>";
        FetchExpression query = new FetchExpression(string.Format(format, str));
        EntityCollection entitys2 = service.RetrieveMultiple(query);
        entitys.Entities.Add(entitys2.Entities[0]);
        return entitys;
    }

    private void UpdateTotals(Guid planRef, EntityCollection totals)
    {
        objCommon.TracingService.Trace("Totals count: " + totals.Entities.Count);
        var dSMPlan = GetDSMPlan(planRef);
        try
        {
            //if (totals == null)
            //{
            //    throw new Exception("Program Interval Totals are null.");
            //}
            foreach (Entity entity2 in totals.Entities)
            {
                foreach (KeyValuePair<string, object> pair in entity2.Attributes)
                {
                    var introduced7 = pair.Key.ToLower();
                    dSMPlan[introduced7] = pair.Value.GetRecordValue();
                }
            }

            var updateRequest = new UpdateRequest { Target = dSMPlan };
            requestWithResults.Requests.Add(updateRequest);
            //  service.Update(dSMPlan);
        }
        catch (Exception exception)
        {
            objCommon.TracingService.Trace("Error On DSM Plan totals update. Exeption:" + exception.Message);
        }
    }


}

