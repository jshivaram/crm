﻿using DDSM_CalculationProgramInterval;
using DDSM_CalculationProgramInterval.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;

public class CalculateDSMPalnTotalsField : CodeActivity
{

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }

    [Input("DSM Plan"), ArgumentEntity("ddsm_dsmplan"), ReferenceTarget("ddsm_dsmplan")]
    public InArgument<EntityReference> DSMPlan { get; set; }
    private readonly string DDSM_PROGRAMOFFERING = "ddsm_dsmplan";
    private Common objCommon;
    private IOrganizationService service;

    protected override void Execute(CodeActivityContext context)
    {
        try
        {
            this.objCommon = new Common(context);
            this.service = this.objCommon.Service;
            this.objCommon.TracingService.Trace("Load CRM Service from context --- OK", new object[0]);
            EntityReference planRef = this.DSMPlan.Get(context);
            if (planRef != null)
            {
                EntityCollection programIntervalsTotals = this.GetProgramIntervalsTotals(planRef);
                this.UpdateTotals(planRef, programIntervalsTotals);
            }
            this.Complete.Set(context, true);
        }
        catch (Exception exception)
        {
            this.objCommon.TracingService.Trace("Error of ProgramInterval totals calculation. Error:" + exception.Message, new object[0]);
            this.Complete.Set(context, false);
        }
    }

    private Entity GetDSMPlan(Guid Id)
    {
        string[] columns = new string[] { "ddsm_dsmplanid" };
        return this.service.Retrieve("ddsm_dsmplan", Id, new ColumnSet(columns));
    }

    private EntityCollection GetProgramIntervalsTotals(EntityReference planRef)
    {
        EntityCollection entitys = new EntityCollection();
        string str = ((planRef != null) && !string.IsNullOrEmpty(planRef.Id.ToString())) ? string.Format("<condition attribute='ddsm_dsmplancustomerid' operator='eq' value='{0}' />", planRef.Id.ToString()) : "";
        string format = "<fetch aggregate='true'>  \r\n            <entity name='ddsm_kpiinterval'>           \r\n            <attribute name='ddsm_targetsavingsthm' alias='ddsm_targetsavingsthm' aggregate='sum' />\r\n            <attribute name='ddsm_targetsavingskw' alias='ddsm_targetsavingskw' aggregate='sum' />\r\n            <attribute name='ddsm_targetsavingskwh' alias='ddsm_targetsavingskwh' aggregate='sum' />\r\n            <attribute name='ddsm_actualallsavingsthm' alias='ddsm_actualallsavingsthm' aggregate='sum' />\r\n            <attribute name='ddsm_actualallsavingskw' alias='ddsm_actualallsavingskw' aggregate='sum' />\r\n            <attribute name='ddsm_actualallsavingskwh' alias='ddsm_actualallsavingskwh' aggregate='sum' />\r\n                    <filter>\r\n                    <condition attribute='statecode' operator='eq' value='0' />\r\n                    {0}                                                   \r\n                </filter>\r\n              </entity>\r\n            </fetch>";
        FetchExpression query = new FetchExpression(string.Format(format, str));
        EntityCollection entitys2 = this.service.RetrieveMultiple(query);
        entitys.Entities.Add(entitys2.Entities[0]);
        return entitys;
    }

    private void UpdateTotals(EntityReference planRef, EntityCollection totals)
    {
        Entity dSMPlan = this.GetDSMPlan(planRef.Id);
        try
        {
            if (totals == null)
            {
                throw new Exception("Program Interval Totals are null.");
            }
            foreach (Entity entity2 in totals.Entities)
            {
                foreach (KeyValuePair<string, object> pair in entity2.Attributes)
                {
                    string introduced7 = pair.Key.ToLower();
                    dSMPlan[introduced7] = pair.Value.GetRecordValue();
                }
            }
            this.service.Update(dSMPlan);
        }
        catch (Exception exception)
        {
            this.objCommon.TracingService.Trace("Error On DSM Plan totals update. Exeption:" + exception.Message, new object[0]);
        }
    }


}

