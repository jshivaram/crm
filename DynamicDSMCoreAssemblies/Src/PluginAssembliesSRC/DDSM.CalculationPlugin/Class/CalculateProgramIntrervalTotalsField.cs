﻿using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using DDSM_CalculationProgramInterval;
using DDSM_CalculationProgramInterval.Utils;
using System.Collections.Generic;
using System;
using Microsoft.Xrm.Sdk.Query;
using System.Diagnostics.Contracts;
using Newtonsoft.Json;
using DDSM.CalculationPlugin.Utils;
using System.Linq;
using DDSM.SmartMeasureCalculationPlugin.Model;
using Microsoft.Xrm.Sdk.Messages;

/// <summary>
/// Plugin for calculation ProgramInterval totals field from workflow that runing from Measure entity
/// </summary>
public class CalculateProgramIntrervalTotalsField : CodeActivity
{
	#region "Parameter Definition"

	[Input("Program Offerings")]
	[ArgumentEntity("ddsm_programoffering")]
	[ReferenceTarget("ddsm_programoffering")]
	public InArgument<EntityReference> ProgramOffering { get; set; }


	[Input("Program")]
	[ArgumentEntity("ddsm_program")]
	[ReferenceTarget("ddsm_program")]
	public InArgument<EntityReference> Program { get; set; }

	[Input("User Input")]
	public InArgument<string> UserInput { get; set; }

	[Output("Complete")]
	public OutArgument<bool> Complete { get; set; }

	Common objCommon;
	IOrganizationService service;

	ExecuteMultipleRequest requestWithResults;
	#endregion


	protected override void Execute(CodeActivityContext context)
	{

		#region "Load CRM Service from context"
		objCommon = new Common(context);
		service = objCommon.Service;
		objCommon.TracingService.Trace("01 Load CRM Service from context --- OK");
		#endregion


		try
		{
			requestWithResults = new ExecuteMultipleRequest()
			{
				// Assign settings that define execution behavior: continue on error, return responses. 
				Settings = new ExecuteMultipleSettings()
				{
					ContinueOnError = false,
					ReturnResponses = true
				},
				// Create an empty organization request collection.
				Requests = new OrganizationRequestCollection()
			};

			var measureIDsJson = UserInput.Get(context);

			var config = GetConfig(service); //AdminData conditions
			var measureDateFiledName = "";
			if (config.Attributes.ContainsKey("ddsm_pimeasuredatefieldname"))
			{

				measureDateFiledName = config["ddsm_pimeasuredatefieldname"].ToString();
				measureDateFiledName = !string.IsNullOrEmpty(measureDateFiledName) ? measureDateFiledName.ToLower() : "ddsm_initialphasedate";

			}

			//List of All Intervals
			List<Entity> IntervalsList = new List<Entity>();
			Dictionary<int, string> phasesSet = GetAvailablePhaseSet(config);
			EntityCollection conditionIntervals = new EntityCollection();


			var progrOf = ProgramOffering.Get(context);
			var progr = Program.Get(context);
			//Check "User Input" (colection of measures) if it have some data - do some logic with it
			if (!String.IsNullOrEmpty(measureIDsJson))
			{
				var measureIDs = JsonConvert.DeserializeObject<UserInputObj2>(measureIDsJson);
				//measureIDs?.SmartMeasures != null

				//Get Distinct of Program and Program Offerings of Measures
				conditionIntervals = GetConditionFromMeasures(measureIDs);


				/*foreach (var meas in measureIDs.SmartMeasures)
				{
					var measData = GetMeasData(meas); //data condidtions for meas
					var programMeas = measData.GetAttributeValue<EntityReference>("ddsm_programid");
					var programOfferingMeas = measData.GetAttributeValue<EntityReference>("ddsm_programofferingsid");

					var programIntervalsForUpdate = GetProgramIntervals(programOfferingMeas.Id, programMeas.Id, config);
					IntervalsList.AddRange(programIntervalsForUpdate.Entities);
				}*/

				foreach (var cond in conditionIntervals.Entities)
				{
					var programMeas = cond.GetAttributeValue<EntityReference>("ddsm_programid");
					var programOfferingMeas = cond.GetAttributeValue<EntityReference>("ddsm_programofferingsid");
					var programIntervalsForUpdate = GetProgramIntervals(programOfferingMeas.Id, programMeas.Id, config);
					IntervalsList.AddRange(programIntervalsForUpdate.Entities);
				}



			}
			//Else If it null check Parameters ProgramOffering and Program and do logic - OnChange Measure
			else if (progrOf != null && progr != null)
			{
				var programIntervalsForUpdate = GetProgramIntervals(progrOf.Id, progr.Id, config);
				IntervalsList.AddRange(programIntervalsForUpdate.Entities);
			}
			else
			{
				throw new Exception("No valid input parameters. Please check it.");
			}
			//ProcessedItems0 = processedProject.Select(x => x).Distinct().ToList()
			IntervalsList = IntervalsList.Select(x => x).Distinct(new EntityComparer()).ToList();
			//   var intIDs = IntervalsList.Select(x => x.Id).Distinct().ToList();

			objCommon.TracingService.Trace("Get GetProgramIntervals. Kol: " + IntervalsList.Count);

			if (IntervalsList.Count > 0)
			{
				foreach (var interval in IntervalsList)
				{
					var programMeas = interval.GetAttributeValue<EntityReference>("ddsm_programid");
					var programOfferingMeas = interval.GetAttributeValue<EntityReference>("ddsm_programofferingsid");

					var totals = GetMeasureTotals(interval, service, programOfferingMeas, programMeas, phasesSet, measureDateFiledName);
					UpdatePiTotals(interval, totals);
				}
			}



			if (requestWithResults.Requests.Count > 0)
			{
				ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)service.Execute(requestWithResults);
				objCommon.TracingService.Trace(requestWithResults.Requests.Count + " Program Interval was updated.");
			}
			else
			{
				objCommon.TracingService.Trace("Program Interval not updated.");
			}

			processCollectedData(conditionIntervals);
			Complete.Set(context, true);

		}
		catch (Exception ex)
		{
			objCommon.TracingService.Trace("Error of ProgramInterval totals calculation. Error:" + ex.Message);
			Complete.Set(context, false);
		}
	}

	private EntityCollection GetConditionFromMeasures(UserInputObj2 measureIDs)
	{
		var result = new EntityCollection();
		try
		{
			Contract.Ensures(Contract.Result<EntityCollection>() != null);

				

		   var queryMeasureConditions = @"<fetch distinct='true' >
											  <entity name='ddsm_measure' >
												<attribute name='ddsm_programid' />
												<attribute name='ddsm_programofferingsid' />
												<filter type='and' >
												  <condition attribute='ddsm_measureid' operator='in' >
													{0}
												  </condition>
												</filter>
											  </entity>
											</fetch>";
			List<string> measIds = new List<string>();

			foreach (var meas in measureIDs.SmartMeasures)
			{
				measIds.Add(string.Format("<value>{0}</value>", meas));
			}
			queryMeasureConditions = String.Format(queryMeasureConditions, string.Join(System.Environment.NewLine, measIds));
				//objCommon.TracingService.Trace("getProjectCounts totals OK. " + queryMeasureTotals);
				var queryMeasureExpression = new FetchExpression(queryMeasureConditions);
				return service.RetrieveMultiple(queryMeasureExpression);
		}
		catch (Exception)
		{
			return result;
		}

	}

	private Entity GetMeasData(Guid meas)
	{
		try
		{
			var query = new QueryExpression
			{
				EntityName = "ddsm_measure",
				ColumnSet = new ColumnSet("ddsm_programofferingsid", "ddsm_programid", "ddsm_uptodatephase", "statecode"),
				Criteria = new FilterExpression(LogicalOperator.And)
				{
					Conditions = {
						 new ConditionExpression("ddsm_measureid", ConditionOperator.Equal, meas),
				},
				}

			};
			//query..Add(new LinkEntity("ddsm_milestone", ProjectEntity, "ddsm_projecttomilestoneid", "ddsm_projectid");
			var result = service.RetrieveMultiple(query);
			if (result.Entities.Count > 0)
				return result[0];
			else
				return null;
		}
		catch (Exception ex)
		{
			return null;
		}
	}

#if WITH_TASKQ
	private void processCollectedData(EntityCollection intervals)
	{
		var listIds = new List<Guid>();
		foreach (var interv in intervals.Entities)
		{
			listIds.Add(interv.GetAttributeValue<EntityReference>("ddsm_programofferingsid").Id);// ToString();
		}

		var taskQueue = new TaskQueue(objCommon.Service);
		taskQueue.Create(new DDSM_Task(TaskEntity.ProgramOffering)
		{
			ProcessedItems0 = listIds.Select(x => x).Distinct().ToList()
		}, TaskEntity.ProgramOffering
		);
	}
#endif

	#region Internal methods
	//Phase set important for calculate(we need same fields in pahase sets for entity and same count in Admin Data (Example: if we have 3 phasa sets we need set countOfPhaseSet = 2 
	//and use in Admin data 0,1,2 index for populate phase sets; If we have 5 phasa set like Savings for Program Intervals we must countOfPhaseSet = 4))
	private Dictionary<int, string> GetAvailablePhaseSet(Entity config)
	{
		var result = new Dictionary<int, string>();
		var fieldname = "ddsm_phaseset";
		var countOfPhaseSet = 3;

		for (int i = 0; i <= countOfPhaseSet; i++)
		{
			if (config.Attributes.ContainsKey(fieldname + i))
			{
				var value = config[fieldname + i].ToString();
				if (!string.IsNullOrEmpty(value))
				{
					result.Add(i, value);
				}
			}
		}
		return result;
	}

	private Entity GetConfig(IOrganizationService service)
	{
		string adminDataId = "";

		var expr = new QueryExpression
		{
			EntityName = "ddsm_admindata",
			ColumnSet = new ColumnSet(true),
			Criteria = new FilterExpression
			{
				FilterOperator = LogicalOperator.And,
				Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, "Admin Data") }
			}
		};

		var adminData = service.RetrieveMultiple(expr);
		if (adminData != null && adminData.Entities?.Count >= 1)
		{
			adminDataId = adminData.Entities[0].Attributes["ddsm_admindataid"].ToString();
		}

		return service.Retrieve("ddsm_admindata", Guid.Parse(adminDataId),
				new ColumnSet("ddsm_phaseset1", "ddsm_phaseset2", "ddsm_phaseset3",
				"ddsm_useyearcorrection", "ddsm_programintervalyearorrection", "ddsm_pimeasuredatefieldname"));
	}

	private EntityCollection GetMeasureTotals(Entity interval, IOrganizationService service, EntityReference programOff, EntityReference program, Dictionary<int, string> phases, string dateFiledName)
	{
		var result = new EntityCollection();
		try
		{
			Contract.Ensures(Contract.Result<EntityCollection>() != null);


			var programOffCond = programOff != null ?
				$"<condition attribute = 'ddsm_programofferingsid' operator= 'eq' value = '{programOff.Id.ToString()}'/>"
				: "";


			var programCond = program != null ?
				$"<condition attribute = 'ddsm_programid' operator= 'eq' value = '{program.Id.ToString()}'/>"
				: "";

			var startDate = new DateTime();
			var endDate = new DateTime();


			foreach (var phase in phases)
			{

				var phaseCondition =
					$"<condition attribute='ddsm_uptodatephase' operator='in'>{phase.Value.GetFirterInValues()} </condition>";

				if (interval.Attributes.ContainsKey("ddsm_startdate"))
				{
					startDate = (DateTime)interval.Attributes["ddsm_startdate"];
				}
				if (interval.Attributes.ContainsKey("ddsm_enddate"))
				{
					endDate = (DateTime)interval.Attributes["ddsm_enddate"];
				}
				/*
				 <attribute name='ddsm_oemrecoverycost' alias='ddsm_{3}PhaseOEMRecoveryCost' aggregate='sum' />
				*/

				//11 Inc fields: add ddsm_totalincentive, ddsm_oemrecoverycost from Meas
				//7 Savings fields: add kW Gross Savings at Meter, kWh Gross Savings at Meter, GJ Savings at Meter, kW Gross Savings at Generator, kWh Gross Savings at Generator, kW Net Savings at Generator, kWh Net Savings at Generator, 
				/*		<attribute name='ddsm_kwgrosssavingsatmeter' alias='ddsm_{3}phasekwgrosssavingsatmeter' aggregate='sum' />
		<attribute name='ddsm_kwhgrosssavingsatmeter' alias='ddsm_{3}phasekwhgrosssavingsatmeter' aggregate='sum' />
		<attribute name='ddsm_gjsavingsatmeter' alias='ddsm_{3}phasegjsavingsatmeter' aggregate='sum' />
		<attribute name='ddsm_kwgrosssavingsatgenerator' alias='ddsm_{3}phasekwgrosssavingsatgenerator' aggregate='sum' />
		<attribute name='ddsm_kwhgrosssavingsatgenerator' alias='ddsm_{3}phasekwhgrosssavingsatgenerator' aggregate='sum' />
		<attribute name='ddsm_kwnetsavingsatgenerator' alias='ddsm_{3}phasekwnetsavingsatgenerator' aggregate='sum' />
		<attribute name='ddsm_kwhnetsavingsatgenerator' alias='ddsm_{3}phasekwhnetsavingsatgenerator' aggregate='sum' />*/

				/*5 savings fields*/
				/*<attribute name='ddsm_savingskwh' alias='ddsm_{3}phasesavingskwh' aggregate='sum' />
				<attribute name='ddsm_savingskw' alias='ddsm_{3}phasesavingskw' aggregate='sum' />
				<attribute name='ddsm_savingsthm' alias='ddsm_{3}phasesavingsthm' aggregate='sum' />
				<attribute name='ddsm_savingswaterccf' alias='ddsm_{3}phasesavingswccf' aggregate='sum' />
				<attribute name='ddsm_gjsavingsatmeter' alias='ddsm_{3}phasesavingsgj' aggregate='sum' />*/

				var queryMeasureTotals =
					@"<fetch aggregate='true'>
					<entity name='ddsm_measure'>               
					<attribute name='ddsm_kwhgrosssavingsatmeter' alias='ddsm_{3}phasekwhgrosssavingsatmeter' aggregate='sum' />
					<attribute name='ddsm_kwhgrosssavingsatgenerator' alias='ddsm_{3}phasekwhgrosssavingsatgenerator' aggregate='sum' />
					<attribute name='ddsm_kwhnetsavingsatgenerator' alias='ddsm_{3}phasekwhnetsavingsatgenerator' aggregate='sum' />
					<attribute name='ddsm_kwgrosssavingsatmeter' alias='ddsm_{3}phasekwgrosssavingsatmeter' aggregate='sum' />
					<attribute name='ddsm_kwgrosssavingsatgenerator' alias='ddsm_{3}phasekwgrosssavingsatgenerator' aggregate='sum' />
					<attribute name='ddsm_kwnetsavingsatgenerator' alias='ddsm_{3}phasekwnetsavingsatgenerator' aggregate='sum' />
					<attribute name='ddsm_gjsavingsatmeter' alias='ddsm_{3}phasegjsavingsatmeter' aggregate='sum' />
					<attribute name='ddsm_m3savings' alias='ddsm_{3}phasem3savings' aggregate='sum' />
					<attribute name='ddsm_watersavings' alias='ddsm_{3}phasewatersavings' aggregate='sum' />
					<attribute name='ddsm_incentivepaymentnet' alias='ddsm_{3}phaseincentivepaymentnet' aggregate='sum' />
					<attribute name='ddsm_incentivecosttotal' alias='ddsm_{3}phaseincentivecosttotal' aggregate='sum' />
					<attribute name='ddsm_totalincentive' alias='ddsm_{3}phasetotalincentive' aggregate='sum' />
					<attribute name='ddsm_avoidedgascost' alias='ddsm_{3}phaseavoidedgascost' aggregate='sum' />
					<attribute name='ddsm_avoidedelectriccost' alias='ddsm_{3}phaseavoidedelectriccost' aggregate='sum' />
					<attribute name='ddsm_avoidedwatercost' alias='ddsm_{3}phaseavoidedwatercost' aggregate='sum' />
					<attribute name='ddsm_totaltrcbenefit' alias='ddsm_{3}phasetotaltrcbenefit' aggregate='sum' />
					<attribute name='ddsm_totaltrccost' alias='ddsm_{3}phasetotaltrccost' aggregate='sum' />
					<attribute name='ddsm_nettrcbenefit' alias='ddsm_{3}phasenettrcbenefit' aggregate='sum' />
					<filter>
						<condition attribute='statecode' operator='eq' value='0' />
						{0}
						{1} 
						{2}
						{4}                 
					</filter>
			  </entity>
			</fetch>";
				//  < attribute name = 'ddsm_projecttomeasureid' alias = 'ddsm_{3}phasecountprojects' aggregate = 'count' />



				var dateFilter = !startDate.Equals(DateTime.MinValue) && !endDate.Equals(DateTime.MinValue) ?
				  string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>",
				  startDate.Date.ToString("MM/dd/yyyy"), endDate.Date.ToString("MM/dd/yyyy"), dateFiledName) : "";

				queryMeasureTotals = string.Format(queryMeasureTotals, programOffCond, phaseCondition, dateFilter, phase.Key, programCond);
				//objCommon.TracingService.Trace("queryMeasureTotals totals OK. " + queryMeasureTotals);
				var queryMeasureExpression = new FetchExpression(queryMeasureTotals);



				var totals = service.RetrieveMultiple(queryMeasureExpression);

				if (totals.Entities.Count > 0)
					result.Entities.Add(totals.Entities[0]);

				#region getProjectCounts

				queryMeasureTotals = @"<fetch aggregate='true' >
									  <entity name='ddsm_measure'>
										<link-entity name='ddsm_project' from='ddsm_projectid' to='ddsm_projecttomeasureid' alias='project' >
										  <attribute name='ddsm_recordtype' alias='type' groupby='true' />
										  <attribute name='ddsm_recordtype' alias='countType' aggregate='count' />
										  <filter type='and' >
											<condition attribute='ddsm_recordtype' operator='not-null' />
										   
										  </filter>
										</link-entity>
									  <filter type='and' >
											{0}
											{1}
											{2}
											{3}
									  </filter>
									  </entity>
									</fetch>";
				queryMeasureTotals = string.Format(queryMeasureTotals, programOffCond, phaseCondition, dateFilter, programCond);
				//objCommon.TracingService.Trace("getProjectCounts totals OK. " + queryMeasureTotals);
				queryMeasureExpression = new FetchExpression(queryMeasureTotals);
				totals = GetProjectCountByRecordType(service.RetrieveMultiple(queryMeasureExpression), phase.Key);


				if (totals.Entities.Count > 0)
					result.Entities.Add(totals.Entities[0]);
				#endregion

			}
			return result;
		}
		catch (Exception)
		{
			return result;
		}
	}

	private EntityCollection GetProjectCountByRecordType(EntityCollection countProjectTypes, int phase)
	{
		var result = new EntityCollection();
		result.Entities.Add(new Entity());
		var key = "";

		foreach (var item in countProjectTypes.Entities)
		{
			var aliasedValue = item.Attributes["type"] as AliasedValue;
			if (aliasedValue != null)
				switch (((OptionSetValue)aliasedValue.Value).Value)
				{
					case 962080000:
						key = $"ddsm_{phase}" + "phasecountopportunities";
						break;
					case 962080001:
						key = $"ddsm_{phase}" + "phasecountprojects";
						break;

				}

			result[0].Attributes[key] = (item.Attributes["countType"] as AliasedValue).Value;
		}

		return result;
	}

	private EntityCollection GetProgramIntervals(Guid programOfferingId, Guid program, Entity config)
	{
		var dataFilter = "";

		#region "Get Program Intervals"

		var programOffCond = Guid.Empty != programOfferingId ?
			$"<condition attribute='ddsm_programofferingsid' operator='eq' value='{programOfferingId}' />"
			: "";

		var programCond = Guid.Empty != program ?
			$"<condition attribute='ddsm_programid' operator='eq' value='{program}' />"
			: "";


		var queryProgIntervals = @"<fetch version='1.0'>
							  <entity name='ddsm_kpiinterval'>
									<attribute name='ddsm_programid'/>
									<attribute name='ddsm_programofferingsid'/>
									<attribute name='ddsm_startdate'/>
									<attribute name = 'ddsm_enddate'/>
								 <filter>
								{0}
								  <condition attribute='statecode' operator='eq' value='0'/>
								{1}  
								{2}                            
								</filter>
							  </entity>
							</fetch>";

		// DateTime dateWithCorrection;
		if (config.Attributes.ContainsKey("ddsm_useyearcorrection") &&
			(bool)config.Attributes["ddsm_useyearcorrection"] &&
			config.Attributes.ContainsKey("ddsm_programintervalyearorrection")) //use year correction
		{
			int dateCorrection = int.Parse(config.Attributes["ddsm_programintervalyearorrection"].ToString());
			dataFilter =
				$"<condition attribute='createdon' operator='between'><value>{DateTime.Now.Date.AddYears(-dateCorrection).ToString("MM/dd/yyyy")}</value><value>{DateTime.Now.Date.AddDays(1).ToString("MM/dd/yyyy")}</value></condition>";
		}

		queryProgIntervals = string.Format(queryProgIntervals, programOffCond, dataFilter, programCond);
		//objCommon.TracingService.Trace("queryProgIntervals totals OK. " + queryProgIntervals);

		var queryProgIntervalsExpression = new FetchExpression(queryProgIntervals);
		#endregion

		return service.RetrieveMultiple(queryProgIntervalsExpression);

	}

	//Microsoft.Xrm.Sdk.Entity
	private void UpdatePiTotals(Entity programInterval, EntityCollection totals)
	{
		try
		{
			if (totals == null) throw new Exception("Measure Totals are null.");
			//objCommon.TracingService.Trace("6.2 in UpdatePiTotals: " + totals.Entities.Count);
			foreach (var total in totals.Entities)
			{
				foreach (var column in total.Attributes)
				{
					programInterval[column.Key.ToLower()] = column.Value.GetRecordValue();
				}

			}


			#region Temp
			// if we need hardcoded mapping
			/*
			programInterval["ddsm_0phasecostequipment"] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostEquipment");
			programInterval["ddsm_0phasecostoemrecovery".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostOEMRecovery");
			programInterval["ddsm_0phasecostfinancing".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostFinancing");
			programInterval["ddsm_0phasesavingswccf".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseSavingsWCCF");
			programInterval["ddsm_0phasecostmeasureperunit".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostMeasurePerUnit");
			programInterval["ddsm_0phasesavingsthm".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseSavingsthm");
			programInterval["ddsm_0phasecostpartner".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostPartner");
			programInterval["ddsm_0phasecostsannualom".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostsAnnualOM");
			programInterval["ddsm_0phasecostlabor".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostLabor");
			programInterval["ddsm_0phasesavingskw".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseSavingskW");
			programInterval["ddsm_0phasecostsincrementalinstalled".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostsIncrementalInstalled");
			programInterval["ddsm_0phasesavingskwh".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseSavingskWh");
			programInterval["ddsm_0phasecostproject".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostProject");
			programInterval["ddsm_0phasecostmeasure".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostMeasure");
			programInterval["ddsm_0phasecostother".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostOther");
			*/
			#endregion

			//objCommon.TracingService.Trace("Pi Attrs: " + JsonConvert.SerializeObject(programInterval.Attributes) + "Pi guid: " + programInterval.Id.ToString());

			//objCommon.Service.Update(programInterval);
			UpdateRequest createRequest = new UpdateRequest { Target = programInterval };
			requestWithResults.Requests.Add(createRequest);
		}
		catch (Exception ex)
		{
			objCommon.TracingService.Trace("Error On Program Interval totals update. Exeption:" + ex.Message);
		}
	}
	#endregion Internal methods

}

