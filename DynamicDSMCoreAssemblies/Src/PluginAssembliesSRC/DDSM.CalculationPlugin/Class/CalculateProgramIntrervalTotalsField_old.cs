﻿using DDSM_CalculationProgramInterval;
using DDSM_CalculationProgramInterval.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

public class CalculateProgramIntrervalTotalsField : CodeActivity
{
    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }

    [Input("Program"), ArgumentEntity("ddsm_program"), ReferenceTarget("ddsm_program")]
    public InArgument<EntityReference> Program { get; set; }

    [Input("Program Offerings"), ArgumentEntity("ddsm_programcycle"), ReferenceTarget("ddsm_programcycle")]
    public InArgument<EntityReference> ProgramOffering { get; set; }

    protected override void Execute(CodeActivityContext context)
    {
        Common objCommon = new Common(context);
        IOrganizationService service = objCommon.Service;
        objCommon.TracingService.Trace("Load CRM Service from context --- OK", new object[0]);
        try
        {
            EntityReference programOff = ProgramOffering.Get(context);
            EntityReference program = Program.Get(context);
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            EntityCollection entitys = this.GetProgramIntervals((programOff != null) ? programOff.Id.ToString() : "", service, program);
            if ((entitys != null) && (entitys.Entities.Count > 0))
            {
                foreach (Entity entity in entitys.Entities)
                {
                    this.updatePITotals(entity, this.GetMeasureTotals(entity, service, programOff, program), objCommon);
                }
            }
            this.Complete.Set(context, true);
        }
        catch (Exception exception)
        {
            objCommon.TracingService.Trace("Error of ProgramInterval totals calculation. Error:" + exception.Message, new object[0]);
            this.Complete.Set(context, false);
        }
    }

    private Dictionary<int, string> getAvailablePhaseSet(Entity config)
    {
        Dictionary<int, string> dictionary = new Dictionary<int, string>();
        string str = "ddsm_phaseset";
        int num = 4;
        for (int i = 0; i <= num; i++)
        {
            if (config.Attributes.ContainsKey(str + i.ToString()))
            {
                string str2 = config[str + i.ToString()].ToString();
                if (!string.IsNullOrEmpty(str2))
                {
                    dictionary.Add(i, str2);
                }
            }
        }
        return dictionary;
    }

    private Entity GetConfig(IOrganizationService service)
    {
        var query = new QueryExpression()
        {
            EntityName = "ddsm_admindata",
            ColumnSet = new ColumnSet("ddsm_phaseset0", "ddsm_phaseset1", "ddsm_phaseset2", "ddsm_phaseset3", "ddsm_phaseset4", "ddsm_useyearcorrection", "ddsm_programintervalyearorrection", "ddsm_pimeasuredatefieldname"),
            Criteria = new FilterExpression
            {
                FilterOperator = LogicalOperator.And,
                Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, "Admin Data") }
            }
        };

        var result = service.RetrieveMultiple(query);

        if (result != null && result.Entities.Count > 0)
            return result[0];
        else
            return null;
        //F56C3086-9AF2-E411-80EC-FC15B4284D68}
        //string input = "F56C3086-9AF2-E411-80EC-FC15B4284D68";
        //string[] columns = new string[] ;
        //return service.Retrieve("ddsm_admindata", Guid.Parse(input), new ColumnSet(columns));
    }

    private EntityCollection GetMeasureTotals(Entity interval, IOrganizationService service, EntityReference programOff, EntityReference program)
    {
        EntityCollection entitys = new EntityCollection();
        string str = (programOff != null) ? string.Format("<condition attribute = 'ddsm_programcycleid' operator= 'eq' value = '{0}'/>", programOff.Id.ToString()) : "";
        string str2 = (program != null) ? string.Format("<condition attribute = 'ddsm_programid' operator= 'eq' value = '{0}'/>", program.Id.ToString()) : "";
        DateTime time = new DateTime();
        DateTime time2 = new DateTime();
        if (interval.Attributes.ContainsKey("ddsm_startdate"))
        {
            time = (DateTime)interval.Attributes["ddsm_startdate"];
        }
        if (interval.Attributes.ContainsKey("ddsm_enddate"))
        {
            time2 = (DateTime)interval.Attributes["ddsm_enddate"];
        }
        string format = "<fetch aggregate='true'>\r\n                <entity name='ddsm_measure' >\r\n                <attribute name='ddsm_phase1savingskwh' alias='ddsm_0phasesavingskwh' aggregate='sum' />\r\n                <attribute name='ddsm_phase1savingskw' alias='ddsm_0phasesavingskw' aggregate='sum' />\r\n                <attribute name='ddsm_phase1savingsccf' alias='ddsm_0phasesavingsthm' aggregate='sum' />\r\n                <attribute name='ddsm_phase1incentivetotal' alias='ddsm_0PhaseIncentiveCostTotal' aggregate='sum' />    \r\n                \r\n\r\n                <filter>\r\n                    <condition attribute='statecode' operator='eq' value='0' />\r\n                    {0}\r\n                    {1} \r\n                    {2}                                   \r\n                </filter>\r\n              </entity>\r\n            </fetch>";
        string str4 = (!time.Equals(DateTime.MinValue) && !time2.Equals(DateTime.MinValue)) ? string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>", time.Date.ToString("MM/dd/yyyy"), time2.Date.ToString("MM/dd/yyyy"), "ddsm_precommitdate") : "";
        FetchExpression query = new FetchExpression(string.Format(format, str, str4, str2));
        EntityCollection entitys2 = service.RetrieveMultiple(query);
        entitys.Entities.Add(entitys2.Entities[0]);
        format = "<fetch count='50' aggregate='true' >\r\n          <entity name='ddsm_measure'>            \r\n                <filter>\r\n                    <condition attribute='statecode' operator='eq' value='0' />\r\n                    {0}\r\n                    {1} \r\n                    {2}                                   \r\n                </filter>\r\n            <link-entity name='ddsm_project' from='ddsm_projectid' to='ddsm_projecttomeasureid' link-type='inner' alias='proj' >\r\n              <attribute name='ddsm_projecttype' alias='ddsm_0phasecountprojects' aggregate='count' />\r\n              <filter type='and'>\r\n                <condition attribute='ddsm_projecttype' operator='eq' value='962080000' />\r\n              </filter>\r\n            </link-entity>\r\n          </entity>\r\n        </fetch>";
        str4 = (!time.Equals(DateTime.MinValue) && !time2.Equals(DateTime.MinValue)) ? string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>", time.Date.ToString("MM/dd/yyyy"), time2.Date.ToString("MM/dd/yyyy"), "ddsm_precommitdate") : "";
        query = new FetchExpression(string.Format(format, str, str4, str2));
        entitys2 = service.RetrieveMultiple(query);
        entitys.Entities.Add(entitys2.Entities[0]);
        format = "<fetch count='50' aggregate='true' >\r\n          <entity name='ddsm_measure'>            \r\n                <filter>\r\n                    <condition attribute='statecode' operator='eq' value='0' />\r\n                    {0}\r\n                    {1} \r\n                    {2}                                   \r\n                </filter>\r\n            <link-entity name='ddsm_project' from='ddsm_projectid' to='ddsm_projecttomeasureid' link-type='inner' alias='proj' >\r\n              <attribute name='ddsm_projecttype' alias='ddsm_0phasecountopportunities' aggregate='count' />\r\n              <filter type='and'>\r\n                <condition attribute='ddsm_projecttype' operator='eq' value='962080002' />\r\n              </filter>\r\n            </link-entity>\r\n          </entity>\r\n        </fetch>";
        str4 = (!time.Equals(DateTime.MinValue) && !time2.Equals(DateTime.MinValue)) ? string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>", time.Date.ToString("MM/dd/yyyy"), time2.Date.ToString("MM/dd/yyyy"), "ddsm_precommitdate") : "";
        query = new FetchExpression(string.Format(format, str, str4, str2));
        entitys2 = service.RetrieveMultiple(query);
        entitys.Entities.Add(entitys2.Entities[0]);
        format = "<fetch aggregate='true'>\r\n                <entity name='ddsm_measure' >\r\n                  <attribute name='ddsm_phase2savingskwh' alias='ddsm_1PhaseSavingskWh' aggregate='sum' />\r\n                  <attribute name='ddsm_phase2savingskw' alias='ddsm_1PhaseSavingskW' aggregate='sum' />\r\n                  <attribute name='ddsm_phase2savingsccf' alias='ddsm_1phasesavingsthm' aggregate='sum' />\r\n                  <attribute name='ddsm_phase2incentivetotal' alias='ddsm_1PhaseIncentiveCostTotal' aggregate='sum' />       \r\n                <filter>\r\n                    <condition attribute='statecode' operator='eq' value='0' />\r\n                    {0}\r\n                    {1} \r\n                    {2}                                   \r\n                </filter>\r\n              </entity>\r\n            </fetch>";
        str4 = (!time.Equals(DateTime.MinValue) && !time2.Equals(DateTime.MinValue)) ? string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>", time.Date.ToString("MM/dd/yyyy"), time2.Date.ToString("MM/dd/yyyy"), "ddsm_committeddate") : "";
        query = new FetchExpression(string.Format(format, str, str4, str2));
        entitys2 = service.RetrieveMultiple(query);
        entitys.Entities.Add(entitys2.Entities[0]);
        format = "<fetch count='50' aggregate='true' >\r\n          <entity name='ddsm_measure'>            \r\n                <filter>\r\n                    <condition attribute='statecode' operator='eq' value='0' />\r\n                    {0}\r\n                    {1} \r\n                    {2}                                   \r\n                </filter>\r\n            <link-entity name='ddsm_project' from='ddsm_projectid' to='ddsm_projecttomeasureid' link-type='inner' alias='proj' >\r\n              <attribute name='ddsm_projecttype' alias='ddsm_1phasecountprojects' aggregate='count' />\r\n              <filter type='and'>\r\n                <condition attribute='ddsm_projecttype' operator='eq' value='962080000' />\r\n              </filter>\r\n            </link-entity>\r\n          </entity>\r\n        </fetch>";
        str4 = (!time.Equals(DateTime.MinValue) && !time2.Equals(DateTime.MinValue)) ? string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>", time.Date.ToString("MM/dd/yyyy"), time2.Date.ToString("MM/dd/yyyy"), "ddsm_precommitdate") : "";
        query = new FetchExpression(string.Format(format, str, str4, str2));
        entitys2 = service.RetrieveMultiple(query);
        entitys.Entities.Add(entitys2.Entities[0]);
        format = "<fetch aggregate='true' >\r\n          <entity name='ddsm_measure'>            \r\n                <filter>\r\n                    <condition attribute='statecode' operator='eq' value='0' />\r\n                    {0}\r\n                    {1} \r\n                    {2}                                   \r\n                </filter>\r\n            <link-entity name='ddsm_project' from='ddsm_projectid' to='ddsm_projecttomeasureid' link-type='inner' alias='proj' >\r\n              <attribute name='ddsm_projecttype' alias='ddsm_1phasecountopportunities' aggregate='count' />\r\n              <filter type='and'>\r\n                <condition attribute='ddsm_projecttype' operator='eq' value='962080002' />\r\n              </filter>\r\n            </link-entity>\r\n          </entity>\r\n        </fetch>";
        str4 = (!time.Equals(DateTime.MinValue) && !time2.Equals(DateTime.MinValue)) ? string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>", time.Date.ToString("MM/dd/yyyy"), time2.Date.ToString("MM/dd/yyyy"), "ddsm_precommitdate") : "";
        query = new FetchExpression(string.Format(format, str, str4, str2));
        entitys2 = service.RetrieveMultiple(query);
        entitys.Entities.Add(entitys2.Entities[0]);
        format = "<fetch aggregate='true'>\r\n                <entity name='ddsm_measure'>                   \r\n               <attribute name='ddsm_phase3savingskwh' alias='ddsm_2PhaseSavingskWh' aggregate='sum' />\r\n               <attribute name='ddsm_phase3savingskw' alias='ddsm_2PhaseSavingskW' aggregate='sum' />\r\n               <attribute name='ddsm_phase3savingsccf' alias='ddsm_2phasesavingsthm' aggregate='sum' />\r\n               <attribute name='ddsm_phase3incentivetotal' alias='ddsm_2PhaseIncentiveCostTotal' aggregate='sum' />\r\n               \r\n              \r\n                <filter>\r\n                    <condition attribute='statecode' operator='eq' value='0' />\r\n                    {0}\r\n                    {1} \r\n                    {2}                                   \r\n                </filter>\r\n              </entity>\r\n            </fetch>";
        str4 = (!time.Equals(DateTime.MinValue) && !time2.Equals(DateTime.MinValue)) ? string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>", time.Date.ToString("MM/dd/yyyy"), time2.Date.ToString("MM/dd/yyyy"), "ddsm_installeddate") : "";
        query = new FetchExpression(string.Format(format, str, str4, str2));
        entitys2 = service.RetrieveMultiple(query);
        entitys.Entities.Add(entitys2.Entities[0]);
        format = "<fetch count='50' aggregate='true' >\r\n          <entity name='ddsm_measure'>            \r\n                <filter>\r\n                    <condition attribute='statecode' operator='eq' value='0' />\r\n                    {0}\r\n                    {1} \r\n                    {2}                                   \r\n                </filter>\r\n            <link-entity name='ddsm_project' from='ddsm_projectid' to='ddsm_projecttomeasureid' link-type='inner' alias='proj' >\r\n              <attribute name='ddsm_projecttype' alias='ddsm_2phasecountprojects' aggregate='count' />\r\n              <filter type='and'>\r\n                <condition attribute='ddsm_projecttype' operator='eq' value='962080000' />\r\n              </filter>\r\n            </link-entity>\r\n          </entity>\r\n        </fetch>";
        str4 = (!time.Equals(DateTime.MinValue) && !time2.Equals(DateTime.MinValue)) ? string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>", time.Date.ToString("MM/dd/yyyy"), time2.Date.ToString("MM/dd/yyyy"), "ddsm_precommitdate") : "";
        query = new FetchExpression(string.Format(format, str, str4, str2));
        entitys2 = service.RetrieveMultiple(query);
        entitys.Entities.Add(entitys2.Entities[0]);
        format = "<fetch count='50' aggregate='true' >\r\n          <entity name='ddsm_measure'>            \r\n                <filter>\r\n                    <condition attribute='statecode' operator='eq' value='0' />\r\n                    {0}\r\n                    {1} \r\n                    {2}                                   \r\n                </filter>\r\n            <link-entity name='ddsm_project' from='ddsm_projectid' to='ddsm_projecttomeasureid' link-type='inner' alias='proj' >\r\n              <attribute name='ddsm_projecttype' alias='ddsm_2phasecountopportunities' aggregate='count' />\r\n              <filter type='and'>\r\n                <condition attribute='ddsm_projecttype' operator='eq' value='962080002' />\r\n              </filter>\r\n            </link-entity>\r\n          </entity>\r\n        </fetch>";
        str4 = (!time.Equals(DateTime.MinValue) && !time2.Equals(DateTime.MinValue)) ? string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>", time.Date.ToString("MM/dd/yyyy"), time2.Date.ToString("MM/dd/yyyy"), "ddsm_precommitdate") : "";
        query = new FetchExpression(string.Format(format, str, str4, str2));
        entitys2 = service.RetrieveMultiple(query);
        entitys.Entities.Add(entitys2.Entities[0]);
        return entitys;
    }

    private EntityCollection GetProgramIntervals(string programOfferingId, IOrganizationService service, EntityReference program)
    {
        // string str = "";
        string str2 = !string.IsNullOrEmpty(programOfferingId) ? string.Format("<condition attribute='ddsm_program' operator='eq' value='{0}' />", programOfferingId) : "";
        string str3 = (program != null) ? string.Format("<condition attribute='ddsm_programid' operator='eq' value='{0}' />", program.Id.ToString()) : "";
        string format = @"<fetch version='1.0' >
            <entity name='ddsm_kpiinterval' >
            <all-attributes/>         
                       <filter>            
                        {0}                        
                        <condition attribute='statecode' operator='eq' value='0' />
                        {1}    
                                     
                        </filter>
                        </entity>
                        </fetch>";
        FetchExpression query = new FetchExpression(string.Format(format, str2, str3));
        return service.RetrieveMultiple(query);
    }

    private void updatePITotals(Entity programInterval, EntityCollection totals, Common objCommon)
    {
        try
        {
            if (totals == null)
            {
                throw new Exception("Measure Totals are null.");
            }
            foreach (Entity entity in totals.Entities)
            {
                foreach (KeyValuePair<string, object> pair in entity.Attributes)
                {
                    string introduced6 = pair.Key.ToLower();
                    programInterval[introduced6] = pair.Value.GetRecordValue();
                }
            }
            objCommon.Service.Update(programInterval);
        }
        catch (Exception exception)
        {
            objCommon.TracingService.Trace("Error On Program Interval totals update. Exeption:" + exception.Message, new object[0]);
        }
    }


}

