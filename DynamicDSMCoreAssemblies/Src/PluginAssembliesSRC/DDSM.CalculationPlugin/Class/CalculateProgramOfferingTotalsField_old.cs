﻿using DDSM_CalculationProgramInterval;
using DDSM_CalculationProgramInterval.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

public class CalculateProgramOfferingTotalsField : CodeActivity
{
    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }

    [Input("Program"), ArgumentEntity("ddsm_program"), ReferenceTarget("ddsm_program")]
    public InArgument<EntityReference> Program { get; set; }

    [RequiredArgument, Input("Program Offerings"), ArgumentEntity("ddsm_programcycle"), ReferenceTarget("ddsm_programcycle")]
    public InArgument<EntityReference> ProgramOffering { get; set; }

    private readonly string DDSM_PROGRAMOFFERING = "ddsm_programcycle";
    private Common objCommon;
    private IOrganizationService service;

    protected override void Execute(CodeActivityContext context)
    {
        try
        {
            this.objCommon = new Common(context);
            this.service = this.objCommon.Service;
            this.objCommon.TracingService.Trace("Load CRM Service from context --- OK", new object[0]);
            EntityReference programOffering = this.ProgramOffering.Get(context);
            EntityReference program = this.Program.Get(context);
            Entity offering = this.GetProgramOffering(programOffering.Id);
            EntityCollection programIntervalsTotals = this.GetProgramIntervalsTotals(programOffering, program);
            if (offering != null)
            {
                this.UpdateTotals(offering, programIntervalsTotals);
            }
            this.Complete.Set(context, true);
        }
        catch (Exception exception)
        {
            this.objCommon.TracingService.Trace("Error of ProgramInterval totals calculation. Error:" + exception.Message, new object[0]);
            this.Complete.Set(context, false);
        }
    }

    private EntityCollection GetProgramIntervalsTotals(EntityReference programOffering, EntityReference program)
    {
        EntityCollection entitys = new EntityCollection();
        string str = ((program != null) && !string.IsNullOrEmpty(program.Id.ToString())) ? string.Format("<condition attribute='ddsm_programid' operator='eq' value='{0}'/>", program.Id.ToString()) : "";
        string str2 = ((programOffering != null) && !string.IsNullOrEmpty(programOffering.Id.ToString())) ? string.Format("<condition attribute='ddsm_program' operator='eq' value='{0}'/>", programOffering.Id.ToString()) : "";
        string format = @"<fetch count='50' aggregate='true' >
  <entity name='ddsm_kpiinterval'>
    <attribute name='ddsm_budgettotalincentive' alias='ddsm_BudgetTotalIncentive' aggregate='sum'/>
    <attribute name='ddsm_targettotalincentive' alias='ddsm_TargetTotalIncentive' aggregate='sum'/>
    <attribute name='ddsm_actualalltotalincentive' alias='ddsm_ActualAllTotalIncentive' aggregate='sum'/>
    <attribute name='ddsm_budgetincentivecosttotal' alias='ddsm_BudgetIncentiveCostTotal' aggregate='sum'/>
    <attribute name='ddsm_targetincentivecosttotal' alias='ddsm_TargetIncentiveCostTotal' aggregate='sum'/>
    <attribute name='ddsm_actualallincentivecosttotal' alias='ddsm_ActualAllIncentiveCostTotal' aggregate='sum'/>
    <attribute name='ddsm_budgetincentivepaymentnet' alias='ddsm_BudgetIncentivePaymentNet' aggregate='sum'/>
    <attribute name='ddsm_targetincentivepaymentnet' alias='ddsm_TargetIncentivePaymentNet' aggregate='sum'/>
    <attribute name='ddsm_actualallincentivepaymentnet' alias='ddsm_ActualAllIncentivePaymentNet' aggregate='sum'/>
    <attribute name='ddsm_budgetsavingskwh' alias='ddsm_BudgetSavingskWh' aggregate='sum'/>
    <attribute name='ddsm_targetsavingskwh' alias='ddsm_TargetSavingskWh' aggregate='sum'/>
    <attribute name='ddsm_actualallsavingskwh' alias='ddsm_ActualAllSavingskWh' aggregate='sum'/>
    <attribute name='ddsm_budgetsavingskw' alias='ddsm_BudgetSavingskW' aggregate='sum'/>
    <attribute name='ddsm_targetsavingskw' alias='ddsm_TargetSavingskW' aggregate='sum'/>
    <attribute name='ddsm_actualallsavingskw' alias='ddsm_ActualAllSavingskW' aggregate='sum'/>
    <attribute name='ddsm_budgetsavingsthm' alias='ddsm_BudgetSavingsthm' aggregate='sum'/>
    <attribute name='ddsm_targetsavingsthm' alias='ddsm_TargetSavingsthm' aggregate='sum'/>
    <attribute name='ddsm_actualallsavingsthm' alias='ddsm_ActualAllSavingsthm' aggregate='sum'/>
    <attribute name='ddsm_budgetadministrationcosts' alias='ddsm_BudgetAdministrationCosts' aggregate='sum'/>
    <attribute name='ddsm_budgetmarketingcosts' alias='ddsm_BudgetMarketingCosts' aggregate='sum'/>
    <attribute name='ddsm_budgetprogramdesigncosts' alias='ddsm_BudgetProgramDesignCosts' aggregate='sum'/>
    <attribute name='ddsm_budgetcustomereducationcosts' alias='ddsm_BudgetCustomerEducationCosts' aggregate='sum'/>
    <attribute name='ddsm_budgetpartnertrainingcosts' alias='ddsm_BudgetPartnerTrainingCosts' aggregate='sum'/>
    <attribute name='ddsm_budgetevaluationcosts' alias='ddsm_BudgetEvaluationCosts' aggregate='sum'/>
    <attribute name='ddsm_targetadministrationcosts' alias='ddsm_TargetAdministrationCosts' aggregate='sum'/>
    <attribute name='ddsm_targetmarketingcosts' alias='ddsm_TargetMarketingCosts' aggregate='sum'/>
    <attribute name='ddsm_targetprogramdesigncosts' alias='ddsm_TargetProgramDesignCosts' aggregate='sum'/>
    <attribute name='ddsm_targetcustomereducationcosts' alias='ddsm_TargetCustomerEducationCosts' aggregate='sum'/>
    <attribute name='ddsm_targetpartnertrainingcosts' alias='ddsm_TargetPartnerTrainingCosts' aggregate='sum'/>
    <attribute name='ddsm_targetevaluationcosts' alias='ddsm_TargetEvaluationCosts' aggregate='sum'/>
    <attribute name='ddsm_0phasetotalincentive' alias='ddsm_0PhaseTotalIncentive' aggregate='sum'/>
    <attribute name='ddsm_1phasetotalincentive' alias='ddsm_1PhaseTotalIncentive' aggregate='sum'/>
    <attribute name='ddsm_2phasetotalincentive' alias='ddsm_2PhaseTotalIncentive' aggregate='sum'/>
    <attribute name='ddsm_0phaseincentivecosttotal' alias='ddsm_0PhaseIncentiveCostTotal' aggregate='sum'/>
    <attribute name='ddsm_1phaseincentivecosttotal' alias='ddsm_1PhaseIncentiveCostTotal' aggregate='sum'/>
    <attribute name='ddsm_2phaseincentivecosttotal' alias='ddsm_2PhaseIncentiveCostTotal' aggregate='sum'/>
    <attribute name='ddsm_0phaseincentivepaymentnet' alias='ddsm_0PhaseIncentivePaymentNet' aggregate='sum'/>
    <attribute name='ddsm_1phaseincentivepaymentnet' alias='ddsm_1PhaseIncentivePaymentNet' aggregate='sum'/>
    <attribute name='ddsm_2phaseincentivepaymentnet' alias='ddsm_2PhaseIncentivePaymentNet' aggregate='sum'/>
    <attribute name='ddsm_0phasesavingskwh' alias='ddsm_0PhaseSavingskWh' aggregate='sum'/>
    <attribute name='ddsm_1phasesavingskwh' alias='ddsm_1PhaseSavingskWh' aggregate='sum'/>
    <attribute name='ddsm_2phasesavingskwh' alias='ddsm_2PhaseSavingskWh' aggregate='sum'/>
    <attribute name='ddsm_0phasesavingskw' alias='ddsm_0PhaseSavingskW' aggregate='sum'/>
    <attribute name='ddsm_1phasesavingskw' alias='ddsm_1PhaseSavingskW' aggregate='sum'/>
    <attribute name='ddsm_2phasesavingskw' alias='ddsm_2PhaseSavingskW' aggregate='sum'/>
    <attribute name='ddsm_0phasesavingsthm' alias='ddsm_0PhaseSavingsthm' aggregate='sum'/>
    <attribute name='ddsm_1phasesavingsthm' alias='ddsm_1PhaseSavingsthm' aggregate='sum'/>
    <attribute name='ddsm_2phasesavingsthm' alias='ddsm_2PhaseSavingsthm' aggregate='sum'/>
    <filter>
     <condition attribute='statecode' operator='eq' value='0'/>
     {0}
     {1}                               
      </filter>
  </entity>
</fetch>";
            FetchExpression query = new FetchExpression(string.Format(format, str2, str));
        EntityCollection entitys2 = this.service.RetrieveMultiple(query);
        entitys.Entities.Add(entitys2.Entities[0]);
        return entitys;
    }

    private Entity GetProgramOffering(Guid Id)
    {
        string[] columns = new string[] { "ddsm_programcycleid" };
        return this.service.Retrieve(this.DDSM_PROGRAMOFFERING, Id, new ColumnSet(columns));
    }

    private void UpdateTotals(Entity offering, EntityCollection totals)
    {
        try
        {
            if (totals == null)
            {
                throw new Exception("Program Interval Totals are null.");
            }
            foreach (Entity entity in totals.Entities)
            {
                foreach (KeyValuePair<string, object> pair in entity.Attributes)
                {
                    string introduced6 = pair.Key.ToLower();
                    offering[introduced6] = pair.Value.GetRecordValue();
                }
            }
            this.service.Update(offering);
        }
        catch (Exception exception)
        {
            this.objCommon.TracingService.Trace("Error On Program Offering totals update. Exeption:" + exception.Message, new object[0]);
        }
    }

   
}

