﻿using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using DDSM_CalculationProgramInterval;
using DDSM_CalculationProgramInterval.Utils;
using System;
using Microsoft.Xrm.Sdk.Query;
using DDSM.SmartMeasureCalculationPlugin.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk.Messages;
using DDSM.CalculationPlugin.Utils;

/// <summary>
///  Plugin for calculation Program totals field from workflow that runing from ProgramOffering entity
/// </summary>
public class CalculateProgramTotalsField : CodeActivity
{
    #region "Parameter Definition"

    [Input("Program")]
    [ArgumentEntity("ddsm_program")]
    [ReferenceTarget("ddsm_program")]
    public InArgument<EntityReference> Program { get; set; }

    [Input("User Input")]
    public InArgument<string> UserInput { get; set; }

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }

    #endregion

    #region Internal fields
    Common _objCommon;
    IOrganizationService _service;
    const string _ddsmProgram = "ddsm_program";
    ExecuteMultipleRequest requestWithResults;
    private List<Guid> processedPrograms = new List<Guid>();
    #endregion

    protected override void Execute(CodeActivityContext context)
    {
        #region "Load CRM Service from context"

        _objCommon = new Common(context);
        _service = _objCommon.Service;
        _objCommon.TracingService.Trace("Load CRM Service from context --- OK");
        #endregion

        var progr = Program.Get(context);
        //Target Entity
        EntityReference Target = (EntityReference)_objCommon.Context.InputParameters["Target"];

        string inputP = null;
        string inputTargetId = null;

        _objCommon.TracingService.Trace("Iput Parameters:");
        //Target ID
        if (Target != null)
        {
            inputTargetId = Target.Id.ToString();
            _objCommon.TracingService.Trace("Iput Parameters. Target ID: " + inputTargetId);
        }
        else { _objCommon.TracingService.Trace("Target ID is null"); }
        //Program
        if (progr != null)
        {
            inputP = progr.Id.ToString();
            _objCommon.TracingService.Trace("Program: " + inputP);
        }
        else { _objCommon.TracingService.Trace("Program is null"); }

        try
        {
            requestWithResults = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            // resolve parameters        
            var programRef = Program.Get(context);
            var userInput = UserInput.Get(context);



            // get all prog offerings that must be totals updated


            //get totals of program Int
            //var totals = GetProgramOfferingsTotals(programRef);
            var listId = new List<Guid>();

            if (!string.IsNullOrEmpty(userInput) && programRef == null)
            {
                UserInputObj2 userInpObj = null;

                userInpObj = JsonConvert.DeserializeObject<UserInputObj2>(userInput);
                if (userInpObj?.SmartMeasures != null)
                {
                    listId = userInpObj.SmartMeasures.Where(x => !x.Equals(Guid.Empty)).Distinct().ToList();
                }
            }
            else if (programRef != null)
            {
                /*var totals = GetProgramOfferingsTotals(programRef.Id);
                var program = GetProgram(programRef.Id);
                UpdateTotals(program, totals);*/
                listId.Add(programRef.Id);
            }
            else if (programRef == null)
            {
                if (Target != null)
                {
                    listId.Add(Target.Id);
                }
            }
            else
            {
                throw new Exception("Program parameter is null. Please check it.");
            }

            foreach (var prog in listId)
            {
                //get totals of program Int
                var totals = GetProgramOfferingsTotals(prog);
                // get all prog offerings that must be totals updated
                //var programOffering = GetProgramOffering(programOfferingRef.Id);
                var program = GetProgram(prog);
                UpdateTotals(program, totals);
            }

            if (requestWithResults.Requests.Count > 0)
            {
                ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_service.Execute(requestWithResults);
                if (responseWithResults.IsFaulted)
                {
                    _objCommon.TracingService.Trace("Update of Program failed! Check correct names fileds or fetch. ");
                }
                else
                {
                    _objCommon.TracingService.Trace(requestWithResults.Requests.Count + " updates was done. Program was updated.");
                }
            }
            else
            {
                _objCommon.TracingService.Trace("Program not updated. No items to update.");
            }
            //ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_service.Execute(requestWithResults);
            //_objCommon.TracingService.Trace(requestWithResults.Requests.Count + " Program updated.");

            //processCollectedData();
            Complete.Set(context, true);
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("Error of Program totals calculation. Error:" + ex.Message);
            Complete.Set(context, false);
        }
    }

#if WITH_TASKQ
    private void processCollectedData()
    {
        var taskQueue = new TaskQueue(_objCommon.Service);
        taskQueue.Create(new DDSM_Task(TaskEntity.Portfolio)
        {
            ProcessedItems0 = processedPrograms.Select(x => x).Distinct().ToList()
        }, TaskEntity.Portfolio
        );
    }
#endif

    #region Internal methods

    private void UpdateTotals(Entity program, EntityCollection totals)
    {
        try
        {
            if (totals == null) throw new Exception("Program Interval Totals are null.");
            foreach (var total in totals.Entities)
            {
                foreach (var column in total.Attributes)
                {
                    program[column.Key.ToLower()] = column.Value.GetRecordValue();
                }
            }

            object portf = null;
            if (program.Attributes.TryGetValue("ddsm_portfolioid", out portf))
                processedPrograms.Add((portf as EntityReference).Id);
            //_service.Update(program);
            var updateRequest = new UpdateRequest { Target = program };
            requestWithResults.Requests.Add(updateRequest);
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("Error On Program totals update. Exeption:" + ex.Message);
        }
    }

    private EntityCollection GetProgramOfferingsTotals(Guid program)
    {
        var result = new EntityCollection();
        var programCond = (!string.IsNullOrEmpty(program.ToString())) ?
            $"<condition attribute='ddsm_programid' operator='eq' value='{program.ToString()}' />"
            : "";

        var queryMeasureTotals =
            @"<fetch aggregate='true' >
                  <entity name='ddsm_programoffering' >
                    <attribute name='ddsm_forecastedkwnetsavingsatgenerator' alias='ddsm_ForecastedkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_forecastedkwhnetsavingsatgenerator' alias='ddsm_ForecastedkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_forecastedspending' alias='ddsm_ForecastedSpending' aggregate='sum' />
                    <attribute name='ddsm_forecastedgjsavingsatmeter' alias='ddsm_ForecastedGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_pnsforecastedspending' alias='ddsm_PNSForecastedSpending' aggregate='sum' />
                    <attribute name='ddsm_actualallkwhnetsavingsatgenerator' alias='ddsm_ActualAllkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_actualallkwnetsavingsatgenerator' alias='ddsm_ActualAllkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_actualspending' alias='ddsm_ActualSpending' aggregate='sum' />
                    <attribute name='ddsm_actualallgjsavingsatmeter' alias='ddsm_ActualAllGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_pnsactualspending' alias='ddsm_PNSActualSpending' aggregate='sum' />
                    <attribute name='ddsm_targetkwhnetsavingsatgenerator' alias='ddsm_TargetkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_targetkwnetsavingsatgenerator' alias='ddsm_TargetkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_budget' alias='ddsm_Budget' aggregate='sum' />
                    <attribute name='ddsm_numberofparticipants' alias='ddsm_NumberofParticipants' aggregate='sum' />
                    <attribute name='ddsm_estimatedkwhnetsavingsatgenerator' alias='ddsm_EstimatedkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_estimatedkwnetsavingsatgenerator' alias='ddsm_EstimatedkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_estimatedspending' alias='ddsm_EstimatedSpending' aggregate='sum' />
                    <attribute name='ddsm_directprogramunitcost' alias='ddsm_DirectProgramUnitCost' aggregate='sum' />
                    <attribute name='ddsm_targetgjsavingsatmeter' alias='ddsm_TargetGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_pnsbudget' alias='ddsm_PNSBudget' aggregate='sum' />
                    <attribute name='ddsm_pnsnumberofparticipants' alias='ddsm_PNSNumberofParticipants' aggregate='sum' />
                    <attribute name='ddsm_pnsestimatedspending' alias='ddsm_PNSEstimatedSpending' aggregate='sum' />
                    <attribute name='ddsm_estimatedgjsavingsatmeter' alias='ddsm_EstimatedGJSavingsatMeter' aggregate='sum' />
                    <filter>
                    <condition attribute='statecode' operator='eq' value='0' />
                    {0}                                                   
                </filter>
              </entity>
            </fetch>";

        queryMeasureTotals = string.Format(queryMeasureTotals, programCond);
        var queryMeasureExpression = new FetchExpression(queryMeasureTotals);

        var totals = _service.RetrieveMultiple(queryMeasureExpression);

        result.Entities.Add(totals.Entities[0]);

        return result;
    }

    private Entity GetProgram(Guid id)
    {
        return _service.Retrieve(_ddsmProgram, id, new ColumnSet("ddsm_programid", "ddsm_portfolioid"));
    }
    #endregion
}

