﻿
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.src;
using DDSM.CommonProvider.Utils;
using DDSM_CalculationProgramInterval.Utils;

/// <summary>
/// This plugin will be calculate PG and PGF for every PG 
/// </summary>
public class CalculateProjectGroupFinancialTotalsField : BaseCodeActivity
{
    #region "Parameter Definition"
    [Input("Project")]
    [ArgumentEntity("ddsm_project")]
    [ReferenceTarget("ddsm_project")]
    public InArgument<EntityReference> ProjectRef { get; set; }

    [Input("Disable Processing PG (only PGF)")]
    public InArgument<bool> DisablePGRecalculation { get; set; }

    public override StatusFileDataUploading CurrentOperationStatus
    {
        get
        {
            return StatusFileDataUploading.recalculationStarted;
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    #endregion
    public string TargetEntity = "ddsm_projectgroupfinancials";
    public enum MilestonStatus
    {
        NotStarted = 962080000,
        Active = 962080001,
        Completed = 962080002,
        Skipped = 962080003
    }
    private UserInputObj2 procesedPGs;
    public enum FinancialStatus
    {
        NotStarted = 962080000,
        Active = 962080001,
        Completed = 962080002
    }
    //Use only 9
    public enum FinancialCalculationType
    {
        AdjustmentEvaluation = 962080007,
        AdjustmentCommitUpdate = 962080008,
        AllProjectIncentiveTotals = 962080009
    }
    private enum Program
    {
        _2001 = 962080000,
        _2003 = 962080001,
        _2004 = 962080002,
        _2005 = 962080003,
        _2010 = 962080004,
        _2013 = 962080005,
        _2014 = 962080006,
        _2507 = 962080007,
        _2510 = 962080008,
        _2515 = 962080009,
        _4001 = 962080010,
        _4002 = 962080011,
        _4003 = 962080012,
        _4005 = 962080013,
        _4007 = 962080014,
        _4010 = 962080015
    };

    

    
    protected override void ExecuteActivity(CodeActivityContext context)
    {
        #region Internal fields
        ExecuteMultipleRequest requestWithResults;

#if WITH_TASKQ
        List<Guid> processedProject; //list of project
#endif
        #endregion

        var _objCommon = new Common(context);
        var DataUploader = _objCommon.GetDataUploaderRef();
        procesedPGs = new UserInputObj2();
        try
        {
            requestWithResults = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

#if WITH_TASKQ
            processedProject = new List<Guid>(); //list of project

#endif

            var projectIDsJson = UserInput.Get(context);
            var projectRef = ProjectRef.Get(context);
            UserInputObj2 projectIDs;

            var disablePGREcalculation = DisablePGRecalculation.Get(context);

            if (!string.IsNullOrEmpty(projectIDsJson) && projectRef == null)
            {
                projectIDs = JsonConvert.DeserializeObject<UserInputObj2>(projectIDsJson);
            }
            else
            {
                projectIDs = new UserInputObj2() { SmartMeasures = { projectRef.Id } };
            }

            _objCommon.TracingService.Trace("Input Projects ID count :" + projectIDs.SmartMeasures.Count);

            var preparedProject = GetPreparedProjects(projectIDs.SmartMeasures, _objCommon);
            _objCommon.TracingService.Trace("Prepared Projects ID count :" + preparedProject.Count);
            var projGroupList = GetProjGroups(preparedProject, _objCommon); //GetProjGroups(projectIDs.SmartMeasures);


            if (projGroupList != null)
            {
                _objCommon.TracingService.Trace("Project Group Not null. Count: " + projGroupList.Entities.Count);


                foreach (var projGroupItem in projGroupList.Entities)
                {

                    //get ProjectGroup Financial by Proj group
                    object projGroupId = null;
                    if (projGroupItem.Contains("ddsm_projectgroupid"))
                    {
                        projGroupId = projGroupItem.GetAttributeValue<AliasedValue>("ddsm_projectgroupid").Value;
                        _objCommon.TracingService.Trace("Selected PG: " + (projGroupId as EntityReference).Id);
                    }
                    else
                    {
                        _objCommon.TracingService.Trace("Can't get ddsm_projectgroupid from PG");
                        return;
                    }

                    var PGFinancials = GetPGFinancials((projGroupId as EntityReference).Id, _objCommon);
                    var alreadyProcessedPGF = new List<Guid>();

                    _objCommon.TracingService.Trace("PG Financials Count: " + PGFinancials.Entities.Count);

                    //set Calculation Status for PGF as "Recalculation in progress"
                    foreach (var pgf in PGFinancials.Entities)
                    {

                        if (alreadyProcessedPGF.Contains(pgf.Id))
                        {
                            _objCommon.TracingService.Trace("PGF: " + pgf.Id + " already have status " + Enum.GetName(typeof(CalculationStatus), CalculationStatus.RecalculationInProgress));
                            continue;
                        }

                        var _pgf = new Entity(pgf.LogicalName, pgf.Id)
                        {
                            Attributes =  {
                                new KeyValuePair<string, object>("ddsm_calculationstatus",new OptionSetValue((int)CalculationStatus.RecalculationInProgress))
                                }
                        };
                        _objCommon.TracingService.Trace("Update calculation status of PGF: " + _pgf.Id + " status = " + (int)CalculationStatus.RecalculationInProgress);
                        _objCommon.GetOrgService(systemCall: true).Update(_pgf);
                        alreadyProcessedPGF.Add(_pgf.Id);
                    }

                    var measures = new EntityCollection(); //get all measures

                    var projects = getAllProjectsByPG((projGroupId as EntityReference).Id, _objCommon);
                    _objCommon.TracingService.Trace("Projects Count: " + projects.Entities.Count);

                    foreach (var item in projects.Entities)
                    {
                        _objCommon.TracingService.Trace("-Selected project: " + item.Id);
                        if (item.Contains("ddsm_phasenumber"))
                        {
                            var upToDatePhase = (int)item.GetAttributeValue<int>("ddsm_phasenumber");//TODO: fix
                            _objCommon.TracingService.Trace("-Project Phase Number: " + upToDatePhase);
                            var measTotal = GetMeasureData(item.Id, upToDatePhase, _objCommon);
                            measures.Entities.Add(measTotal[0]);
                        }
                        else
                        {
                            _objCommon.TracingService.Trace("-Selected project: " + item.Id+ " ddsm_phasenumber is null");
                        }
                       
                    }

                    _objCommon.TracingService.Trace("Before update PG. disablePGREcalculation = " + disablePGREcalculation);
                    if (!disablePGREcalculation)
                    {
                        UpdatePG((projGroupId as EntityReference).Id, measures, _objCommon);
                    }


                    foreach (var fin in PGFinancials.Entities)
                    {
                        _objCommon.TracingService.Trace("-selected PGF: " + fin.Id + " has Current Status: " + Enum.GetName(typeof(FinancialStatus), (FinancialStatus)fin.GetAttributeValue<OptionSetValue>("ddsm_status").Value));
                        if (fin.GetAttributeValue<OptionSetValue>("ddsm_status").Value == (int)FinancialStatus.NotStarted)
                        {
                            var index = fin.GetAttributeValue<int>("ddsm_initiatingmilestoneindex");
                            _objCommon.TracingService.Trace("-PGF initiating milestone index: " + index);

                            var milestone = GetProjMilestoneData((projGroupId as EntityReference).Id, (int)index, _objCommon);
                            if (milestone != null)
                            {
                                object milestoneStatus = null;
                                if (milestone.Attributes.TryGetValue("ddsm_status", out milestoneStatus))
                                {
                                    _objCommon.TracingService.Trace("-Milestone status: " + Enum.GetName(typeof(MilestonStatus), (MilestonStatus)(milestoneStatus as OptionSetValue).Value));
                                    if ((milestoneStatus as OptionSetValue).Value == (int)MilestonStatus.Active || (milestoneStatus as OptionSetValue).Value == (int)MilestonStatus.Completed)
                                    {
                                        UpdatePGFinancial(fin, milestone, measures, requestWithResults, _objCommon, disablePGREcalculation, true);
                                    }
                                    else
                                    {
                                        UpdatePGFinancial(fin, milestone, measures, requestWithResults, _objCommon, disablePGREcalculation, false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (requestWithResults.Requests.Count > 0)
            {
                //  ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)localObjCommon.Service.Execute(requestWithResults);
                foreach (var req in requestWithResults.Requests)
                {
                    _objCommon.GetOrgService().Update(((UpdateRequest)req).Target);
                }
            }
            processCollectedData(_objCommon);
            _objCommon.TracingService.Trace("End of work");
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("Error:" + ex.Message + " sta");
            SetErrorStatusForDA();
            throw;
        }
    }



    #region Internal methods

    private void processCollectedData(Common localObjCommon)
    {
        if (procesedPGs.SmartMeasures.Count > 0)
        {
            var pgfIds = procesedPGs.SmartMeasures.Select(x => x).Distinct().ToList();
            if (pgfIds.Count > 0)
            {
                var taskQueue = new TaskQueue(localObjCommon.GetOrgService());
                taskQueue.Create(new DDSM_Task(TaskEntity.ProjectGroupNotification)
                {
                    ProcessedItems0 = pgfIds
                }, TaskEntity.ProjectGroupNotification
                );
            }
        }
    }

    private List<Guid> GetPreparedProjects(List<Guid> projectsId, Common localObjCommon)
    {
        var result = new EntityCollection();
        var listPrepared = new List<Guid>();
        var query = new QueryExpression()
        {
            EntityName = "ddsm_project",
            ColumnSet = new ColumnSet("ddsm_projectid", "ddsm_calculationrecordstatus"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions =
                {
                    new ConditionExpression("ddsm_projectid", ConditionOperator.In, projectsId),
                    new ConditionExpression("statecode",ConditionOperator.Equal,(int)State.Active),
                    new ConditionExpression("statuscode",ConditionOperator.Equal,(int)Status.Active)
                }
            }
        };

        //var enitities = localObjCommon.Service.RetrieveMultiple(query);
        result = localObjCommon.GetOrgService(systemCall: true).RetrieveMultiple(query);

        foreach (var project in result.Entities)
        {
            if (project.Attributes.ContainsKey("ddsm_calculationrecordstatus") &&
                project.GetAttributeValue<OptionSetValue>("ddsm_calculationrecordstatus").Value == (int)CalculationRecordStatus.ReadyToCalculation)
            {
                listPrepared.Add(project.Id);
            }
        }
        return listPrepared;
    }

    private void UpdatePG(Guid id, EntityCollection measures, Common localObjCommon)
    {
        localObjCommon.TracingService.Trace("-Update PG: " + id);
        var pg = new Entity("ddsm_projectgroup", id);

        if (measures != null)
        {
            foreach (var measure in measures.Entities)
            {
                foreach (var finfiled in measure.Attributes)
                {
                    if (finfiled.Key != "ddsm_actualamount")
                    {
                        var val = measure.GetAttributeValue<AliasedValue>(finfiled.Key.ToLower()).Value;
                        if (val != null)
                        {
                            if (val is Money && pg.Contains(finfiled.Key.ToLower()))
                                pg[finfiled.Key.ToLower()] = new Money((pg[finfiled.Key.ToLower()] as Money).Value + (val as Money).Value);
                            else
                                pg[finfiled.Key.ToLower()] = val as Money;
                        }
                    }

                }
            }
        }


        //pg["ddsm_incentivedaservicesdsmprogram"] = new OptionSetValue((int)Program._2014); //TODO: is this needed??
        localObjCommon.GetOrgService(systemCall: true).Update(pg);

        procesedPGs.SmartMeasures.Add(pg.Id);
        //UpdateRequest createRequest = new UpdateRequest { Target = pg };
        //requestWithResults.Requests.Add(createRequest);

    }

    private void UpdatePGFinancial(Entity fin, Entity milestone, EntityCollection measures, ExecuteMultipleRequest requestWithResults, Common localObjCommon, bool disablePGREcalculation, bool activatePGF = true)
    {
        localObjCommon.TracingService.Trace("-UpdatePGFinancial: " + fin.Id);
        var todayDate = DateTime.UtcNow;
        var PGfinance = new Entity(TargetEntity, fin.Id);

        // var calcTypeFin = fin.GetAttributeValue<OptionSetValue>("ddsm_calculationtype").Value;

        //restore status of pgf record as RecalculationCompleted, that disabling to showing blocking message
        localObjCommon.TracingService.Trace("Before update PGF. disablePGREcalculation = " + disablePGREcalculation);
        if (!disablePGREcalculation)
        {
            localObjCommon.TracingService.Trace("In updating PGF summ fields. disablePGREcalculation=" + disablePGREcalculation);
            // var measures = GetMeasures(projId, calcTypeFin);
            if (measures != null)
            {
                foreach (var measure in measures.Entities)
                {
                    foreach (var finfiled in measure.Attributes)
                    {
                        if (finfiled.Key.ToLower().Equals("ddsm_incentivedaservicesdsmamount"))
                        {
                            var val = measure.GetAttributeValue<AliasedValue>(finfiled.Key.ToLower()).Value;
                            if (val != null)
                            {
                                if (val is Money && PGfinance.Contains("ddsm_cc1_amount"))
                                    PGfinance["ddsm_cc1_amount"] = new Money((PGfinance["ddsm_cc1_amount"] as Money).Value + (val as Money).Value);
                                else
                                    PGfinance["ddsm_cc1_amount"] = val as Money;
                            }
                        }

                        if (finfiled.Key.ToLower().Equals("ddsm_incentivedaservicespnsamount"))
                        {
                            var val = measure.GetAttributeValue<AliasedValue>(finfiled.Key.ToLower()).Value;
                            if (val != null)
                            {
                                if (val is Money && PGfinance.Contains("ddsm_cc3_amount"))
                                    PGfinance["ddsm_cc3_amount"] = new Money((PGfinance["ddsm_cc3_amount"] as Money).Value + (val as Money).Value);
                                else
                                    PGfinance["ddsm_cc3_amount"] = val as Money;
                            }
                        }

                        if (!finfiled.Key.ToLower().Equals("ddsm_incentivedaservicesdsmamount")
                            && !finfiled.Key.ToLower().Equals("ddsm_incentivedaservicespnsamount"))
                        {
                            var val = measure.GetAttributeValue<AliasedValue>(finfiled.Key.ToLower()).Value;
                            if (val != null)
                            {
                                if (val is Money && PGfinance.Contains(finfiled.Key.ToLower()))
                                    PGfinance[finfiled.Key.ToLower()] = new Money((PGfinance[finfiled.Key.ToLower()] as Money).Value + (val as Money).Value);
                                else
                                    PGfinance[finfiled.Key.ToLower()] = val as Money;
                            }
                        }



                        //if (finfiled.Key != "ddsm_actualamount")
                        //{
                        //    var val = measure.GetAttributeValue<AliasedValue>(finfiled.Key.ToLower()).Value;
                        //    if (val != null)
                        //    {
                        //        if (val is Money && PGfinance.Contains("ddsm_cc1_amount"))
                        //            PGfinance["ddsm_cc1_amount"] = new Money((PGfinance["ddsm_cc1_amount"] as Money).Value + (val as Money).Value);
                        //        else
                        //            PGfinance["ddsm_cc1_amount"] = val as Money;
                        //    }
                        //}
                    }
                }
                if (PGfinance.Attributes.Contains("ddsm_actualamount"))
                    PGfinance["ddsm_calculatedamount"] = PGfinance.GetAttributeValue<Money>("ddsm_actualamount");
                else
                {
                    localObjCommon.TracingService.Trace("PGfinance with id: " + PGfinance.Id + " doesn't contains attribute ddsm_actualamount. Field ddsm_calculatedamount not updated.");
                }

            }

            //     PGfinance.Attributes.Remove("ddsm_incentivedaservicesdsmamount");
            //   PGfinance.Attributes.Remove("ddsm_incentivedaservicespnsamount");
        }
        

        if (activatePGF)
        {
            localObjCommon.TracingService.Trace("-PGF will be activated: " + PGfinance.Id);
            //Add  duration!
            var thisStartDate = todayDate;
            #region Add duration

            PGfinance["ddsm_status"] = new OptionSetValue((int)FinancialStatus.Active);

            if (milestone.Attributes.Contains("ddsm_actualend"))
                PGfinance["ddsm_actualstart1"] = milestone.GetAttributeValue<DateTime>("ddsm_actualend");
            else
            {
                localObjCommon.TracingService.Trace("Milestone with id: " + milestone.Id + " doesn't contains attribute ddsm_actualend. Field ddsm_actualstart1 not updated on PGfinance: " + PGfinance.Id);
            }

            //PGfinance["ddsm_actualstart1"] = milestone.GetAttributeValue<DateTime>("ddsm_actualstart");
            //---aguk 09/02/2016 start
            //moved to the Plug-in DDSM.Utils.GenFinancialsActualTargetDate
            /*PGfinance["ddsm_targetstart2"] = thisStartDate;

            object duration2 = null;
            if (fin.Attributes.TryGetValue("ddsm_duration2", out duration2))
                thisStartDate = thisStartDate.AddDays((int)duration2);

            PGfinance["ddsm_targetend2"] = thisStartDate;
            PGfinance["ddsm_targetstart3"] = thisStartDate;

            object duration3 = null;
            if (fin.Attributes.TryGetValue("ddsm_duration3", out duration3))
                thisStartDate = thisStartDate.AddDays((int)duration3);

            PGfinance["ddsm_targetend3"] = thisStartDate;
            PGfinance["ddsm_targetstart4"] = thisStartDate;

            object duration4 = null;
            if (fin.Attributes.TryGetValue("ddsm_duration4", out duration4))
                thisStartDate = thisStartDate.AddDays((int)duration4);

            PGfinance["ddsm_targetend4"] = thisStartDate;
            PGfinance["ddsm_targetstart5"] = thisStartDate;

            object duration5 = null;
            if (fin.Attributes.TryGetValue("ddsm_duration5", out duration5))
                thisStartDate = thisStartDate.AddDays((int)duration5);

            PGfinance["ddsm_targetend5"] = thisStartDate;*/
            //---aguk 09/02/2016 end
            #endregion Add duration       
        }


        // PGfinance["ddsm_calculationstatus"] = new OptionSetValue((int)CalculationStatus.RecalculationCompleted);
        localObjCommon.GetOrgService(systemCall: true).Update(PGfinance);
        // TracingService.Trace("in UpdatePGFinancial. Set ddsm_calculationstatus: " + ((OptionSetValue)PGfinance["ddsm_calculationstatus"]).Value.ToString() )  ;
        //   UpdateRequest createRequest = new UpdateRequest { Target = PGfinance };
        //   requestWithResults.Requests.Add(createRequest);
    }

    private EntityCollection getAllProjectsByPG(Guid projGroupItem, Common localObjCommon)
    {
        localObjCommon.TracingService.Trace("Get Projects for PG:" + projGroupItem);
        var query = new QueryExpression
        {
            EntityName = "ddsm_project",
            ColumnSet = new ColumnSet(allColumns: true),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions = {
                         new ConditionExpression("ddsm_projectgroupid", ConditionOperator.Equal, projGroupItem),
                         new ConditionExpression("statecode",ConditionOperator.Equal,(int)State.Active),
                         new ConditionExpression("statuscode",ConditionOperator.Equal,(int)Status.Active)
                },
            }
        };
        return localObjCommon.GetOrgService().RetrieveMultiple(query);
    }

    private Entity GetProjMilestoneData(Guid projId, int ind, Common localObjCommon)
    {
        localObjCommon.TracingService.Trace("-Get Proj Milestone Data:");
        var query = new QueryExpression
        {
            EntityName = "ddsm_projectgroupmilestone",
            ColumnSet = new ColumnSet("ddsm_index", "ddsm_actualstart", "ddsm_actualend", "ddsm_status"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions = {
                         new ConditionExpression("ddsm_projectgrouptoprojectgroupapproid", ConditionOperator.Equal, projId),
                         new ConditionExpression("ddsm_index", ConditionOperator.Equal, ind),
                         new ConditionExpression("statecode",ConditionOperator.Equal,(int)State.Active),
                         new ConditionExpression("statuscode",ConditionOperator.Equal,(int)Status.Active)
                },
            }
        };
        var result = localObjCommon.GetOrgService().RetrieveMultiple(query);
        localObjCommon.TracingService.Trace("--Found: " + result.Entities.Count);
        if (result.Entities.Count > 0)
            return result[0];
        else
            return null;

    }

    private EntityCollection GetPGFinancials(Guid projGroupItem, Common localObjCommon)
    {
        localObjCommon.TracingService.Trace("Get PG Financials for : " + projGroupItem);
        var expr = new QueryExpression
        {
            EntityName = "ddsm_projectgroupfinancials",
            ColumnSet = new ColumnSet("ddsm_duration1", "ddsm_duration2", "ddsm_duration3", "ddsm_duration4", "ddsm_duration5", "ddsm_status", "ddsm_calculationtype", "ddsm_initiatingmilestoneindex"), //TODO: set columns 
            Criteria = new FilterExpression()
            {
                Conditions = {
                    new ConditionExpression("ddsm_projectgrouptoprojectgroupfinanid", ConditionOperator.Equal, projGroupItem),
                    new ConditionExpression("statecode",ConditionOperator.Equal,(int)State.Active),
                    new ConditionExpression("statuscode",ConditionOperator.Equal,(int)Status.Active)

                }
            }
        };
        return localObjCommon.GetOrgService().RetrieveMultiple(expr);
    }

    private EntityCollection GetMeasureData(Guid projItem, int phase, Common localObjCommon)
    {
        localObjCommon.TracingService.Trace("-Get Measure Data");
        //TODO: set proj group ID from param
        var queryXml = @"<fetch aggregate='true'>
                    <entity name = 'ddsm_measure'>
                         <attribute name='ddsm_incentivecostmeasure' alias='ddsm_actualamount' aggregate='sum'/>
                         <attribute name='ddsm_incentivecostmeasuredsm' alias='ddsm_incentivedaservicesdsmamount' aggregate='sum'/>
                         <attribute name='ddsm_incentivecostmeasurepns' alias='ddsm_incentivedaservicespnsamount' aggregate='sum'/>
                    <filter type = 'and'>
                     <condition attribute = 'ddsm_projecttomeasureid' operator= 'eq' value = '{0}'/>
                     <condition attribute='ddsm_uptodatephase' operator='eq' value='{1}' />';
					 <condition attribute='ddsm_selectableprogramoffering' operator='neq' value='962080012' />
					 <condition attribute='ddsm_selectableprogramoffering' operator='neq' value='962080005' />
                     <condition attribute='statecode' operator='eq' value='0' />
                     <condition attribute='statuscode' operator='eq' value='1' /> 
                    </filter>
                 </entity>
                 </fetch>";
        //     <attribute name='ddsm_incentivecostmeasurepns' alias='ddsm_incentivedaservicespnsamount' aggregate='sum'/>
        queryXml = string.Format(queryXml, projItem, phase);

        return localObjCommon.GetOrgService().RetrieveMultiple(new FetchExpression(queryXml));
    }

    private EntityCollection GetProjGroups(List<Guid> projIdList, Common localObjCommon)
    {
        if (projIdList.Count == 0)
        {
            return null;
        }
        // TracingService.Trace("In GetProjGroups: projIdList count =" + projIdList.Count);
        var queryXml = @"<fetch aggregate='true'> 
                         <entity name='ddsm_project'>
                            <attribute name='ddsm_projectgroupid' alias='ddsm_projectgroupid' groupby='true'/>
                            <filter type='and'>
							  <condition attribute='statecode' operator='eq' value='0' />
                              <condition attribute='statuscode' operator='eq' value='1' /> 
                              <condition attribute='ddsm_projectid' operator='in'>
                              {0}
                              </condition>
                            </filter>
                          </entity>
                        </fetch>";
        var projIdsCond = "";
        var listCond = new List<string>();

        foreach (var projId in projIdList)
        {
            listCond.Add(string.Format("<value>{0}</value>", projId));
        }
        projIdsCond = string.Join(Environment.NewLine, listCond);

        queryXml = string.Format(queryXml, projIdsCond);
        //TracingService.Trace("In GetProjGroups: fetch =" + queryXml);
        return localObjCommon.GetOrgService().RetrieveMultiple(new FetchExpression(queryXml));
    }

    #endregion
}

