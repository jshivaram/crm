﻿using DDSM_CalculationProgramInterval;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;

namespace DDSM.CalculationPlugin.Class
{
    /// <summary>
    /// Update Child Measure records from Project
    /// </summary>
    public class CalculateProjectToMeasure : CodeActivity
    {
        #region Parameters

        //[Input("Program Offering")]
        //[ArgumentEntity("ddsm_programoffering")]
        //[ReferenceTarget("ddsm_programoffering")]
        //public InArgument<EntityReference> ProgramOffering { get; set; }

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }


        [Output("Result")]
        public OutArgument<string> Result { get; set; }
        #endregion

        #region Internal fields
        Common _objCommon;
        IOrganizationService _service;
        readonly string DDSM_PROJECT = "ddsm_project";
        readonly string DDSM_MEASURE = "ddsm_measure";
        #endregion


        protected override void Execute(CodeActivityContext context)
        {
            // var programOffering = ProgramOffering.Get(context);
            #region "Load CRM Service from context"
            _objCommon = new Common(context);
            _service = _objCommon.Service;
            _objCommon.TracingService.Trace("Load CRM Service from context --- OK");
            #endregion

            //get curent project ref
            var targetProject = _objCommon.Context.InputParameters["Target"] as Entity;// _objCommon.Context.InputParameters.Contains("Target");
            // get all data of project records
            var currentProject = _service.Retrieve(DDSM_PROJECT, targetProject.Id, new ColumnSet(allColumns: true));
            // get related measures of project
            var projectMeasures = GetProjectMeasures(targetProject.Id);

            var result = UpdateMeasures(currentProject, projectMeasures);


            Complete.Set(context, true);
        }

        private bool UpdateMeasures(Entity currentProject, EntityCollection projectMeasures)
        {
            bool result;
            //1 GJ - 277.778 kWh
            var constGJ = 277.778;

            #region Prepare Data
            //project
            // ddsm_totalprojectcos
            var totalProjectCost = (decimal) currentProject.Attributes["ddsm_totalprojectcos"];
            var measureCount = projectMeasures.Entities.Count;



            #endregion

            foreach (var measure in projectMeasures.Entities)
            {
                _service.Update(new Entity(DDSM_MEASURE,measure.Id)
                {
                    Attributes = {
                        new KeyValuePair<string, object>("",""),
                        new KeyValuePair<string, object>("",""),
                        new KeyValuePair<string, object>("","")
                    }
                });

            }
            result = true;


            return result;
        }

        private EntityCollection GetProjectMeasures(Guid idProj)
        {
            var query = new QueryExpression
            {
                EntityName = DDSM_MEASURE,
                ColumnSet = new ColumnSet(allColumns: true),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = {
                        new ConditionExpression("ddsm_projecttomeasureid", ConditionOperator.Equal, idProj)
                    }
                }
            };
            var result = _service.RetrieveMultiple(query);

            return result;
        }
    }
}
