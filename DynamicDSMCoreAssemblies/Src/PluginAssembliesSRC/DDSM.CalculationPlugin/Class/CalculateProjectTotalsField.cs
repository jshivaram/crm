﻿using System.Activities;
using Microsoft.Xrm.Sdk;
using DDSM_CalculationProgramInterval.Utils;
using System;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.src;
using DDSM.CommonProvider.Utils;

/// <summary>
///  Plugin for calculation Program totals field from workflow that runing from ProgramOffering entity
/// </summary>
public class CalculateProjectTotalsField : BaseCodeActivity
{
    #region "Parameter Definition"

    [Input("Project")]
    [ArgumentEntity("ddsm_project")]
    [ReferenceTarget("ddsm_project")]
    public InArgument<EntityReference> Project { get; set; }

    public override StatusFileDataUploading CurrentOperationStatus
    {
        get
        {
            return StatusFileDataUploading.recalculationStarted;
        }

        set
        {
            throw new NotImplementedException();
        }
    }
    #endregion

    #region Internal fields
    readonly string _ddsmProject = "ddsm_project";
    // private Entity DataUploader;

    enum CalculationRecordStatus
    {
        NotReady = 962080000,
        ReadyToCalculation = 962080001
    };



    #endregion

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {

        var _objCommon = new Common(executionContext);
        var DataUploader = _objCommon.GetDataUploaderRef();




#if WITH_TASKQ
        List<Guid> processedProject = new List<Guid>();

#endif
        ExecuteMultipleRequest requestWithResults;
        UserInputObj2 userInpObj = new DDSM.CommonProvider.Model.UserInputObj2();

        try
        {
            requestWithResults = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            var projectRef = Project.Get(executionContext);
            var userInput = UserInput.Get(executionContext);

            if (!string.IsNullOrEmpty(userInput))
            {
                userInpObj = JsonConvert.DeserializeObject<DDSM.CommonProvider.Model.UserInputObj2>(userInput);
            }
            var listId = new List<Guid>();
            if (projectRef == null && userInpObj?.SmartMeasures != null)
            {
                listId = userInpObj.SmartMeasures.Where(x => !x.Equals(Guid.Empty)).Distinct().ToList();
            }
            else if (projectRef != null && !Guid.Empty.Equals(projectRef.Id))
            {
                listId.Add(projectRef.Id);
            }
            else
            {
                return;
            }
            _objCommon.TracingService.Trace("count of project: " + listId.Count);

            _objCommon.TracingService.Trace("before GetPreparedProjects ");
            var preparedPeojects = listId;//GetPreparedProjects(listId, service);
            _objCommon.TracingService.Trace("Count Prepared projects:" + preparedPeojects.Count);

            foreach (var item in preparedPeojects)
            {
                var totals = GetProgramTotals(item, _objCommon);
                UpdateTotals(item, totals, requestWithResults, _objCommon);
#if WITH_TASKQ
                processedProject.Add(item);
#endif
            }
            Complete.Set(executionContext, true);
        }
        catch (Exception ex)
        {
            SetErrorStatusForDA();
            _objCommon.TracingService.Trace("Error of Project totals calculation. Error:" + ex.Message);
            Complete.Set(executionContext, false);
        }
        finally
        {
            //TODO: run task for Financial
#if WITH_TASKQ
            processCollectedData(processedProject, _objCommon.GetOrgService(), DataUploader);
#endif

        }
    }


#if WITH_TASKQ
    private void processCollectedData(List<Guid> processedProject, IOrganizationService _service, Entity DataUploader)
    {
        if (processedProject.Count > 0)
        {
            var taskQueue = new TaskQueue(_service);


            var finIDs = processedProject.Select(x => x).Distinct().ToList();
            if (finIDs.Count > 0)
            {
                //create task for finances
                taskQueue.Create(new DDSM_Task(TaskEntity.Financial)
                {
                    ProcessedItems0 = finIDs
                }, TaskEntity.Project, dataUploader: DataUploader.ToEntityReference()
                );
            }
        }
    }
#endif

    private List<Guid> GetPreparedProjects(List<Guid> listId, Common _objCommon)
    {
        var preparedPeojects = new List<Guid>();
        var calcRecStatus = "ddsm_calculationrecordstatus";
        _objCommon.TracingService.Trace("in GetPreparedProjects");
        foreach (var itemProj in listId)
        {
            try
            {
                var measures = GetRelatedMeasures(itemProj, _objCommon);
                _objCommon.TracingService.Trace("measures count " + measures.Count + " for: " + itemProj);
                foreach (var meas in measures)
                {
                    foreach (var attr in meas.Attributes)
                    {
                        _objCommon.TracingService.Trace(attr.Key);
                    }

                    object status;
                    if (meas.Attributes.TryGetValue(calcRecStatus, out status))
                    {

                        _objCommon.TracingService.Trace("Meas: " + meas.Id + "measures attrs : " + calcRecStatus + " = " + ((OptionSetValue)status).Value);
                        status = null;
                    }
                    else
                    {
                        _objCommon.TracingService.Trace("Can't get attr " + calcRecStatus + "for measure: " + meas.Id);
                    }

                }
                if (measures.Count == measures.Count(x => x.Attributes.ContainsKey(calcRecStatus) &&
                x.GetAttributeValue<OptionSetValue>(calcRecStatus).Value == (int)CalculationRecordStatus.ReadyToCalculation))
                    preparedPeojects.Add(itemProj);
            }
            catch (Exception ex)
            {
                _objCommon.TracingService.Trace("in ex GetPreparedProjects: " + ex.Message + ex.StackTrace);
                //  throw;
            }
        }
        return preparedPeojects;
    }
    #region Internal methods
    private void UpdateTotals(Guid projId, EntityCollection totals, ExecuteMultipleRequest requestWithResults, Common _objCommon)
    {
        var listRolups = new List<string>()
        {
            "ddsm_incentivecostproject",
            "ddsm_incentivecostpartner",
            "ddsm_incentivecostmeasure",
            "ddsm_incentivepaymentgross",
            "ddsm_waterccf",
            "ddsm_propanegallons",
            "ddsm_oemrecoverycost",
            "ddsm_incentivecostfinancing",
            "ddsm_incentivepaymentnet",
            "ddsm_oilgallons"
        };

        Entity project = new Entity(_ddsmProject, projId);
        try
        {
            if (totals == null) throw new Exception("Measures Sun of Inc fields are null.");
            foreach (var total in totals.Entities)
            {
                foreach (var column in total.Attributes)
                {
                    project[column.Key.ToLower()] = column.Value.GetRecordValue();
                }
            }
            project["ddsm_calculationrecordstatus"] = new OptionSetValue((int)CalculationRecordStatus.ReadyToCalculation);
            _objCommon.GetOrgService().Update(project);
            //Sytem Rolluped Update!
            foreach (var rollup in listRolups)
            {
                var rollupRequest =
                new CalculateRollupFieldRequest { Target = new EntityReference("ddsm_project", projId), FieldName = rollup };
                requestWithResults.Requests.Add(rollupRequest);
            }

            if (requestWithResults.Requests.Count > 0)
            {
                ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_objCommon.GetOrgService().Execute(requestWithResults);
                _objCommon.TracingService.Trace(requestWithResults.Requests.Count + " Project rollups was started for recalc.");
                requestWithResults.Requests.Clear();
            }



            // rollupRequest. = 

            //CalculateRollupFieldResponse response = (CalculateRollupFieldResponse)_service.Execute(rollupRequest);

        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("Error On Project Inc fields update. Exeption:" + ex.Message);
        }
    }

    private EntityCollection GetProgramTotals(Guid project, Common _objCommon)
    {
        var result = new EntityCollection();

        var programCond = $"<condition attribute='ddsm_projecttomeasureid' operator='eq' value='{project.ToString()}' />";
        var queryMeasureTotals =
            @"<fetch aggregate='true'>
                  <entity name='ddsm_measure'>
                    <attribute name='ddsm_incentivecosttotal' alias='ddsm_incentivecosttotal' aggregate='sum' />
                    <attribute name='ddsm_totalincentive' alias='ddsm_totalincentive' aggregate='sum' />
                    <attribute name='ddsm_totalincentivepns' alias='ddsm_totalincentivepns' aggregate='sum' />
                    <attribute name='ddsm_totalincentivedsm' alias='ddsm_totalincentivedsm' aggregate='sum' />
                    <attribute name='ddsm_kwgrosssavingsatmeter' alias='ddsm_kwgrosssavingsatmeter' aggregate='sum' />
                    <attribute name='ddsm_kwhgrosssavingsatmeter' alias='ddsm_kwhgrosssavingsatmeter' aggregate='sum' />
                    <attribute name='ddsm_gjsavingsatmeter' alias='ddsm_gjsavingsatmeter' aggregate='sum' />
                    <attribute name='ddsm_kwgrosssavingsatgenerator' alias='ddsm_kwgrosssavingsatgenerator' aggregate='sum' />
                    <attribute name='ddsm_kwhgrosssavingsatgenerator' alias='ddsm_kwhgrosssavingsatgenerator' aggregate='sum' />    
                    <attribute name='ddsm_kwnetsavingsatgenerator' alias='ddsm_kwnetsavingsatgenerator' aggregate='sum' />
                    <attribute name='ddsm_kwhnetsavingsatgenerator' alias='ddsm_kwhnetsavingsatgenerator' aggregate='sum' />
                    <attribute name='ddsm_evaluatedkwnetsavingsatgenerator' alias='ddsm_evaluatedkwnetsavingsatgenerator' aggregate='sum' />
                    <attribute name='ddsm_evaluatedkwhnetsavingsatgenerator' alias='ddsm_evaluatedkwhnetsavingsatgenerator' aggregate='sum' />
                    <attribute name='ddsm_m3savings' alias='ddsm_m3savings' aggregate='sum' />
                    <attribute name='ddsm_watersavings' alias='ddsm_watersavings' aggregate='sum' />
                <filter>
                    <condition attribute='statecode' operator='eq' value='0' />
                    <condition attribute='statuscode' operator='eq' value='1' /> 
                    <condition attribute='ddsm_recalculationgroup' operator='null' />
                    {0}                                                   
                </filter>
              </entity>
            </fetch>";

        queryMeasureTotals = string.Format(queryMeasureTotals, programCond);
        var queryMeasureExpression = new FetchExpression(queryMeasureTotals);
        //< attribute name = 'ddsm_incentivecostpartner' alias = 'ddsm_incentivecostpartner' aggregate = 'sum' />
        //< attribute name = 'ddsm_incentivecostpartnertest' alias = 'ddsm_incentivecostpartnertest' aggregate = 'sum' />
        var totals = _objCommon.GetOrgService().RetrieveMultiple(queryMeasureExpression);

        result.Entities.Add(totals.Entities[0]);
        //result.Entities.Add(rollupRequest);
        return result;
    }


    private List<Entity> GetRelatedMeasures(Guid projId, Common _objCommon)
    {
        var query = new QueryExpression
        {
            EntityName = "ddsm_measure",
            ColumnSet = new ColumnSet("ddsm_name", "ddsm_calculationrecordstatus"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions =
                {
                    new ConditionExpression("statecode", ConditionOperator.Equal, "Active")
                }
            }
        };
        var relationship = new Relationship { SchemaName = "ddsm_ddsm_project_ddsm_measure" };

        var relatedEntity = new RelationshipQueryCollection { { relationship, query } };

        var request = new RetrieveRequest
        {
            RelatedEntitiesQuery = relatedEntity,
            ColumnSet = new ColumnSet("ddsm_projectid"),
            Target = new EntityReference { Id = projId, LogicalName = "ddsm_project" }
        };

        var response = (RetrieveResponse)_objCommon.GetOrgService().Execute(request);

        return response.Entity.RelatedEntities.Values.ToList()[0].Entities.ToList();
    }

    //private Entity GetProject(Guid id, Common _objCommon)
    //{
    //    return _objCommon.GetOrgService().Retrieve(_ddsmProject, id, new ColumnSet("ddsm_projectid"));
    //}
    #endregion
}

