﻿using Microsoft.Xrm.Sdk;
using System.Activities;
using System;
using Microsoft.Xrm.Sdk.Query;
using DDSM_CalculationProgramInterval.Utils;
using Microsoft.Xrm.Sdk.Messages;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.Utils;
using DDSM.CommonProvider.src;

public class ClaculateFinancialTotalField : BaseCodeActivity
{
    #region "Parameter Definition"
    [Input("Project")]
    [ArgumentEntity("ddsm_project")]
    [ReferenceTarget("ddsm_project")]
    public InArgument<EntityReference> ProjectRef { get; set; }

    public override StatusFileDataUploading CurrentOperationStatus
    {
        get
        {
            return StatusFileDataUploading.recalculationStarted;
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    #endregion

    #region Internal fields
    readonly string MainEntity = "ddsm_measure"; //Entity to get data from Measures sum(Incentive-Payment Net), sum(Incentive Peyment Net-DSM)
    readonly string TargetEntity = "ddsm_financial"; //Update entity Financial
    readonly string ProjectEntity = "ddsm_project"; //Update entity Financial




#if WITH_TASKQ
    List<Guid> processedProject; //list of project
#endif


    public enum MilestonStatus
    {
        NotStarted = 962080000,
        Active = 962080001,
        Completed = 962080002,
        Skipped = 962080003
    }

    public enum FinancialStatus
    {
        NotStarted = 962080000,
        Active = 962080001,
        Completed = 962080002
    }

    public enum FinancialType
    {
        Percent50 = 962080002,
    }

    //Use only 9
    public enum FinancialCalculationType
    {
        AdjustmentEvaluation = 962080007,
        AdjustmentCommitUpdate = 962080008,
        AllProjectIncentiveTotals = 962080009
    }

    public enum IncentivesCustomerRebatesDSMProgram
    {
        _2014 = 962080006
    }

    public enum CC1_ProgramCode
    {
        _2014 = 962080006
    }

    public enum CC1_ProgramCodeDescription
    {
        _2014DSMApplianceRetirement = 962080006
    }

    public enum ProjectType
    {
        Rebate = 962080000,
        Financing = 962080001,
        BothBNIonly = 962080002
    }
    #endregion
    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        var _objCommon = new Common(executionContext);
        var DataUploader = _objCommon.GetDataUploaderRef();

        DateTime TodayDate = DateTime.UtcNow;
        var requestWithResults = new ExecuteMultipleRequest()
        {
            // Assign settings that define execution behavior: continue on error, return responses. 
            Settings = new ExecuteMultipleSettings()
            {
                ContinueOnError = false,
                ReturnResponses = true
            },
            // Create an empty organization request collection.
            Requests = new OrganizationRequestCollection()
        };
        List<Guid> processedId = new List<Guid>();

        var projectRef = ProjectRef.Get(executionContext);

        UserInputObj2 projectIDs;

        var projectIDsJson = UserInput.Get(executionContext);
        if (!string.IsNullOrEmpty(projectIDsJson) && projectRef == null)
        {
            projectIDs = JsonConvert.DeserializeObject<UserInputObj2>(projectIDsJson);
        }
        else
        {
            projectIDs = new UserInputObj2() { SmartMeasures = { projectRef.Id } };
        }

        try
        {
            foreach (var projId in projectIDs.SmartMeasures)
            {
                var Financials = GetFinancial(projId, _objCommon);
                if (Financials != null)
                {
                    foreach (var fin in Financials.Entities)
                    {
                        var ind = fin.GetAttributeValue<int>("ddsm_initiatingmilestoneindex");
                        var milestone = GetMilestoneData(projId, ind, _objCommon);

                        if (milestone != null)
                        {
                            object milestoneStatus = null;
                            if (milestone.Attributes.TryGetValue("ddsm_status", out milestoneStatus))
                            {
                                if ((milestoneStatus as OptionSetValue).Value == (int)MilestonStatus.Active || (milestoneStatus as OptionSetValue).Value == (int)MilestonStatus.Completed)
                                {
                                    var StatusFin = fin.GetAttributeValue<OptionSetValue>("ddsm_status").Value;
                                    if (StatusFin == (int)FinancialStatus.NotStarted)
                                    {
                                        UpdateFinancial(fin, projId, milestone, requestWithResults, TodayDate, _objCommon);
                                    }
                                }
                            }
                        }
                    }
                    processedId.Add(projId);
                }
                else { _objCommon.TracingService.Trace("Can't get Financials by project id:" + projId); }
            }
            if (requestWithResults.Requests.Count > 0)
            {
                ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_objCommon.GetOrgService().Execute(requestWithResults);
                _objCommon.TracingService.Trace(requestWithResults.Requests.Count + " Financial was updated.");
            }
            else
            {
                _objCommon.TracingService.Trace("Financial not updated.");
            }
        }
        catch (Exception ex)
        {
            SetErrorStatusForDA();
            _objCommon.TracingService.Trace(requestWithResults.Requests.Count + "Error: " + ex.StackTrace);
            throw;
        }
        finally
        {
#if WITH_TASKQ
            processCollectedData(processedId, projectIDs.SmartMeasures, _objCommon, DataUploader);
#endif
        }
    }


#if WITH_TASKQ
    private void processCollectedData(List<Guid> processedFin, List<Guid> processedProj, Common _objCommon, Entity DataUploader)
    {
        if (processedFin.Count > 0)
        {
            var pgfIds = processedFin.Select(x => x).Distinct().ToList();
            if (pgfIds.Count > 0)
            {
                var taskQueue = new TaskQueue(_objCommon.GetOrgService());
                taskQueue.Create(new DDSM_Task(TaskEntity.ProjectGroupFinancial)
                {
                    ProcessedItems0 = pgfIds
                }, TaskEntity.Project, dataUploader: DataUploader.ToEntityReference()
                );
            }
        }
        else
        {
            var pgfIds = processedProj.Select(x => x).Distinct().ToList();
            if (pgfIds.Count > 0)
            {
                var taskQueue = new TaskQueue(_objCommon.GetOrgService());
                taskQueue.Create(new DDSM_Task(TaskEntity.ProjectGroupFinancial)
                {
                    ProcessedItems0 = pgfIds
                }, TaskEntity.Project, dataUploader: DataUploader.ToEntityReference()
                );
            }

        }

    }
#endif

    private Entity GetMilestoneData(Guid projId, int ind, Common _objCommon)
    {
        var query = new QueryExpression
        {
            EntityName = "ddsm_milestone",
            ColumnSet = new ColumnSet("ddsm_index", "ddsm_actualstart", "ddsm_actualend", "ddsm_status"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions = {
                         new ConditionExpression("ddsm_projecttomilestoneid", ConditionOperator.Equal, projId),
                         new ConditionExpression("ddsm_index", ConditionOperator.Equal, ind),
                         new ConditionExpression("statecode",ConditionOperator.Equal,(int)State.Active),
                         new ConditionExpression("statuscode",ConditionOperator.Equal,(int)Status.Active)
                },
            }

        };
        var result = _objCommon.GetOrgService().RetrieveMultiple(query);
        if (result.Entities.Count > 0)
            return result[0];
        else
            return null;

    }

    private void UpdateFinancial(Entity fin, Guid projId, Entity milestone, ExecuteMultipleRequest requestWithResults, DateTime TodayDate, Common _objCommon)
    {
        var finance = new Entity(TargetEntity, fin.Id);
        var linkedfields = CrmHelper.getDataRelationMapping(_objCommon.GetOrgService(), TargetEntity, "ddsm_project", projId);

        var currentFinRecord = _objCommon.GetOrgService().Retrieve("ddsm_financial", fin.Id, new ColumnSet("ddsm_stagename1", "ddsm_stagename2", "ddsm_stagename3"));

        object adr1 = null;
        linkedfields.Attributes.TryGetValue("ddsm_payeeaddress1", out adr1);
        object adr2 = null;
        linkedfields.Attributes.TryGetValue("ddsm_payeeaddress2", out adr2);

        _objCommon.TracingService.Trace("Address 1,2: " + adr1 + ", " + adr2);

        #region Implementation #8550
        object finName = null;
        if (fin.Attributes.TryGetValue("ddsm_name", out finName) && (finName as string).Contains("ARET"))
        {
            var parentProj = _objCommon.GetOrgService().Retrieve("ddsm_project", projId, new ColumnSet("ddsm_accountid", "ddsm_projecttype", "ddsm_financingcost"));

            object parentAccProj = null;
            if (parentProj.Attributes.TryGetValue("ddsm_accountid", out parentAccProj))
            {
                fin["ddsm_cc1_vendor"] = new EntityReference("account", (parentAccProj as EntityReference).Id);
            }
            //TODO
            //Project.Project Type = Rebate : - CCDI - Cost Date = TODAY's date.
            //Project.Project Type = Financing, Project.Financing cost = 0$ : - CCDI - Cost Date = TODAY's date.
            //Project.Project Type = Financing, Project.Financing cost > 0$ : - CCDF - Cost Date = TODAY's date.
        }
        #endregion

        finance.Attributes.AddRange(linkedfields.Attributes);
        var calcTypeFin = fin.GetAttributeValue<OptionSetValue>("ddsm_calculationtype").Value;
        var measures = GetMeasures(projId, calcTypeFin, _objCommon);
        /*
         <attribute name='ddsm_incentivepaymentnet' alias='ddsm_actualamount' aggregate='sum' />
         <attribute name='ddsm_incentivepaymentnet' alias='ddsm_calculatedamount' aggregate='sum' />
         */
        if (fin.Attributes.TryGetValue("ddsm_name", out finName) && (finName as string).Contains("GH"))
        {
            if (measures.Entities[0].Attributes.ContainsKey("ddsm_actualamount"))
            {
                measures.Entities[0].Attributes.Remove("ddsm_actualamount");
            }
            if (measures.Entities[0].Attributes.ContainsKey("ddsm_calculatedamount"))
            {
                measures.Entities[0].Attributes.Remove("ddsm_calculatedamount");
            }
        }

        if (measures != null)
        {
            foreach (var finfiled in measures.Entities[0].Attributes)
            {
                finance[finfiled.Key.ToLower()] = finfiled.Value.GetRecordValue();
            }
        }
        //Add  duration!
        var thisStartDate = TodayDate;
        #region Add duration       

        object curentStage = null;

        finance["ddsm_status"] = new OptionSetValue((int)FinancialStatus.Active); //{ Value: 962080001};
        //OLD SET
        //finance["ddsm_actualstart1"] = milestone.GetAttributeValue<DateTime>("ddsm_actualstart");
        //New set as TODAY!
        finance["ddsm_actualstart1"] = thisStartDate;
        if (fin.Attributes.TryGetValue("ddsm_name", out finName) && (finName as string).Contains("ARET"))
        {

            finance["ddsm_actualend1"] = thisStartDate;//Not set if GH
            finance["ddsm_actualstart2"] = thisStartDate;//Not set if GH
            finance["ddsm_targetstart2"] = thisStartDate;//Not set if GH

            //set pending stage // from stage2
            if (currentFinRecord.Attributes.TryGetValue("ddsm_stagename2", out curentStage))
                finance["ddsm_pendingstage"] = curentStage as string;

        }
        else
        {
            //set pending stage // from stage1
            if (currentFinRecord.Attributes.TryGetValue("ddsm_stagename1", out curentStage))
                finance["ddsm_pendingstage"] = curentStage as string;
        }


        //fin["ddsm_cc1_vendor"] = new EntityReference("account", (parentAccProj as EntityReference).Id);


        //Set ddsm_actualstart1, ddsm_targetend1, ddsm_targetstart2
        /*Add new*/
        thisStartDate = thisStartDate.AddDays(fin.GetAttributeValue<int>("ddsm_duration1"));
        finance["ddsm_targetend1"] = thisStartDate;

        finance["ddsm_targetstart2"] = thisStartDate;
        /*Add new*/
        thisStartDate = thisStartDate.AddDays(fin.GetAttributeValue<int>("ddsm_duration2"));
        finance["ddsm_targetend2"] = thisStartDate;

        finance["ddsm_targetstart3"] = thisStartDate;
        thisStartDate = thisStartDate.AddDays(fin.GetAttributeValue<int>("ddsm_duration3"));
        finance["ddsm_targetend3"] = thisStartDate;

        finance["ddsm_targetstart4"] = thisStartDate;
        thisStartDate = thisStartDate.AddDays(fin.GetAttributeValue<int>("ddsm_duration4"));
        finance["ddsm_targetend4"] = thisStartDate;

        finance["ddsm_targetstart5"] = thisStartDate;
        thisStartDate = thisStartDate.AddDays(fin.GetAttributeValue<int>("ddsm_duration5"));
        finance["ddsm_targetend5"] = thisStartDate;





        #endregion Add duration
        //UpdateRequest createRequest = new UpdateRequest { Target = finance };
        //requestWithResults.Requests.Add(createRequest);
        _objCommon.GetOrgService().Update(finance);
    }

    private EntityCollection GetFinancial(Guid projId, Common _objCommon)
    {
        var query = new QueryExpression
        {
            EntityName = TargetEntity,
            ColumnSet = new ColumnSet("ddsm_duration1", "ddsm_duration2", "ddsm_duration3", "ddsm_duration4", "ddsm_duration5", "ddsm_status", "ddsm_calculationtype", "ddsm_initiatingmilestoneindex", "ddsm_financialtype", "ddsm_name"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions = {
                    new ConditionExpression("ddsm_projecttofinancialid", ConditionOperator.Equal, projId),
                    new ConditionExpression("statecode",ConditionOperator.Equal,(int)State.Active),
                    new ConditionExpression("statuscode",ConditionOperator.Equal,(int)Status.Active)
                }
            }
        };

        var result = _objCommon.GetOrgService().RetrieveMultiple(query);

        //If Index of Finance <> Index of Projects Milestones we haven't records  to update and LOG this info
        if (result.Entities.Count > 0)
            return result;
        else
            _objCommon.TracingService.Trace("Financial ProjectId: " + projId);
        return null;
    }

    private EntityCollection GetMeasures(Guid projId, int calcType, Common _objCommon)
    {
        var upToDatePhase = GetProjectPhase(projId, _objCommon);

        var fetchMeas = "";
        switch (calcType)
        {
            case (int)FinancialCalculationType.AllProjectIncentiveTotals:
                fetchMeas = @"<fetch aggregate='true'>
                    <entity name = 'ddsm_measure'>
                        <attribute name='ddsm_incentivepaymentnetdsm' alias='ddsm_cc1_amount' aggregate='sum' />
                        <attribute name='ddsm_incentivepaymentnetdsm' alias='ddsm_incentivescustomerrebatesdsmamount' aggregate='sum' />
                        <attribute name='ddsm_incentivepaymentnet' alias='ddsm_actualamount' aggregate='sum' />
                        <attribute name='ddsm_incentivepaymentnet' alias='ddsm_calculatedamount' aggregate='sum' />
                    <filter type = 'and'>
                     <condition attribute = 'ddsm_projecttomeasureid' operator= 'eq' value = '{0}'/>
                     <condition attribute='ddsm_uptodatephase' operator='eq' value='{1}' />
                     <condition attribute='statecode' operator='eq' value='0' />
                     <condition attribute='statuscode' operator='eq' value='1' /> 
                    </filter>
                 </entity>
                 </fetch>";
                fetchMeas = String.Format(fetchMeas, projId.ToString(), upToDatePhase);
                break;
            case (int)FinancialCalculationType.AdjustmentEvaluation:
            case (int)FinancialCalculationType.AdjustmentCommitUpdate:
            default:
                _objCommon.TracingService.Trace("This Financial Calculation Type have no logic. Value of Financial.ddsm_calculationtype: " + calcType);
                break;
        }
        if (String.IsNullOrEmpty(fetchMeas))
        {
            return null;
        }
        else
        {
            var queryMeasureExpression = new FetchExpression(fetchMeas);
            var Measures = _objCommon.GetOrgService().RetrieveMultiple(queryMeasureExpression);
            return Measures;
        }

    }

    private int GetProjectPhase(Guid projId, Common _objCommon)
    {
        var query = new QueryExpression
        {
            EntityName = ProjectEntity,
            ColumnSet = new ColumnSet("ddsm_phasenumber"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions = {
                    new ConditionExpression("ddsm_projectid", ConditionOperator.Equal, projId),
                    new ConditionExpression("statecode",ConditionOperator.Equal,(int)State.Active),
                    new ConditionExpression("statuscode",ConditionOperator.Equal,(int)Status.Active)

                }
            }
        };

        var result = _objCommon.GetOrgService().RetrieveMultiple(query);
        if (result.Entities.Count > 0)
        {
            return result.Entities[0].GetAttributeValue<int>("ddsm_phasenumber");
        }
        else
        {
            return 0;
        }
    }

}

