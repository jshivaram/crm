﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider;
using Newtonsoft.Json;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.src;
using DDSM_CalculationProgramInterval.Utils;


public class MeasureCalculation : BaseCodeActivity
{
    #region Internal Fields
    List<string> _processingLog;
    readonly string MainEntity = "ddsm_measure";
    Dictionary<Guid, Guid> ProcessedMeasures; // key crm ID, value esp ID

#if WITH_TASKQ
    IList<Guid> processedProject; //list of project                                                     
    IList<Guid> processedMeasure;

 
#endif
    public override StatusFileDataUploading CurrentOperationStatus
    {
        get
        {
            return StatusFileDataUploading.recalculationStarted;
        }

        set
        {
            throw new NotImplementedException();
        }
    }


    #endregion

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        var _objCommon = new Common(executionContext);
        Entity DataUploader = _objCommon.GetDataUploaderRef();


        _processingLog = new List<string>();
        ProcessedMeasures = new Dictionary<Guid, Guid>();
#if WITH_TASKQ
        processedProject = new List<Guid>();
        processedMeasure = new List<Guid>();
#endif
        try
        {
            var measureIDsJson = UserInput.Get(executionContext);
            if (!string.IsNullOrEmpty(measureIDsJson))
            {
                var measureIDs = JsonConvert.DeserializeObject<DDSM.CommonProvider.Model.UserInputObj2>(measureIDsJson);


                if (measureIDs != null && measureIDs.SmartMeasures != null)
                {
                    var measureData = GetMeasuresInfo(measureIDs.SmartMeasures, _objCommon);
                    if (measureData?.Entities.Count > 0)
                    {
                        foreach (var meas in measureData.Entities)
                        {
                            //   _objCommon.Service.Update(new Entity("ddsm_measure", meas.Id) { Attributes = { new KeyValuePair<string, object>("ddsm_fundingcalculation", Guid.NewGuid().ToString()) } });
                            //measureForUpdate["ddsm_fundingcalculation"] = Guid.NewGuid().ToString();// new Random(6000000).Next();
#if WITH_TASKQ
                            collectData(meas);
#endif
                        }
                    }
                }
            }
            else
            {
                throw new Exception("UserInput parameter  is empty. Nothing to deserialize.");
            }
        }
        catch (Exception ex)
        {
            SetErrorStatusForDA();
            _objCommon.TracingService.Trace("DDSM Erorr: " + ex.Message, ex.StackTrace);
            throw;
        }


#if WITH_TASKQ
        processCollectedData(_objCommon.GetOrgService(), _objCommon,DataUploader);
#endif

    }

#if WITH_TASKQ
    private void processCollectedData(IOrganizationService _service,Common _objCommon, Entity DataUploader)
    {
        if (processedProject.Count > 0)
        {
            var taskQueue = new TaskQueue(_service);


            taskQueue.Create(new DDSM_Task(TaskEntity.Project)
            {
                ProcessedItems0 = processedProject.Select(x => x).Distinct().ToList()
            }, TaskEntity.Project, dataUploader: DataUploader.ToEntityReference()
            );
        }
        else
        {
            _objCommon.TracingService.Trace("No one project was collected. Task wasn't created");
        }

    }
#endif
#if WITH_TASKQ
    private void collectData(Entity meas)
    {
        object projRef = null;
        if (meas.Attributes.TryGetValue("ddsm_projecttomeasureid", out projRef))
            processedProject.Add((projRef as EntityReference).Id);
        processedMeasure.Add(meas.Id);
    }
#endif
    private EntityCollection GetMeasuresInfo(List<Guid> IDs, Common _objCommon)
    {
        // clear 
        var measureIds = IDs.Where(x => !x.Equals(Guid.Empty)).ToList();
        Guid calcTypeId = GetMeasureCalcType(_objCommon);
        var expr = new QueryExpression
        {
            EntityName = MainEntity,
            ColumnSet = new ColumnSet("ddsm_projecttomeasureid"),
            Criteria = new FilterExpression
            {
                Conditions = {
                    new ConditionExpression("ddsm_measureid", ConditionOperator.In, measureIds),
                    new ConditionExpression("ddsm_calculationtype", ConditionOperator.NotEqual, calcTypeId),
                    new ConditionExpression("statecode",ConditionOperator.Equal,(int)State.Active),
                    new ConditionExpression("statuscode",ConditionOperator.Equal,(int)Status.Active)
                },
                FilterOperator = LogicalOperator.And
            }
        };
        return _objCommon.GetOrgService().RetrieveMultiple(expr);
    }

    public Guid GetMeasureCalcType(Common _objCommon, string name = "ESP")
    {
        try
        {
            var expr = new QueryExpression
            {
                EntityName = "ddsm_measurecalculationtemplate",
                ColumnSet = new ColumnSet("ddsm_name"),
                Criteria = new FilterExpression
                {
                    FilterOperator = LogicalOperator.And,
                    Conditions = {
                        new ConditionExpression("ddsm_name", ConditionOperator.Equal, name),
                        new ConditionExpression("statecode",ConditionOperator.Equal,(int)State.Active),
                        new ConditionExpression("statuscode",ConditionOperator.Equal,(int)Status.Active)
                    }
                }
            };
            var result = _objCommon.GetOrgService().RetrieveMultiple(expr);
            if (result.Entities.Count > 0)
            {
                return result.Entities[0].Id;
            }
            throw new Exception("Calc type record with name " + name + " not found");
        }
        catch (Exception ex)
        {
            //_processingLog.Add("WARN: " + ex.Message);
            return Guid.Empty;
        }
    }

}

