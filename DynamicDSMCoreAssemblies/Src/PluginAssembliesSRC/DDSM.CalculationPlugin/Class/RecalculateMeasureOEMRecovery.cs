﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Activities;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk.Workflow;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.src;

/// <summary>
/// This plugin should be recalculate "OEM Recovery Cost" field on Child Measures of Project
/// </summary>
public class RecalculateMeasureOEMRecovery : BaseCodeActivity
{

    [Input("Recovery Percentage")]
    public InArgument<Double> RecoveryPercentage { get; set; }

    public override StatusFileDataUploading CurrentOperationStatus
    {
        get
        {
            return StatusFileDataUploading.rollupsRecalculation;
        }

        set
        {
            throw new NotImplementedException();
        }
    }

    #region Internal fields
    readonly string _ddsmPortfolio = "ddsm_portfolio";

    ExecuteMultipleRequest requestWithResults;

    #endregion

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        var _objCommon = new Common(executionContext);
        requestWithResults = new ExecuteMultipleRequest()
        {
            // Assign settings that define execution behavior: continue on error, return responses. 
            Settings = new ExecuteMultipleSettings()
            {
                ContinueOnError = false,
                ReturnResponses = true
            },
            // Create an empty organization request collection.
            Requests = new OrganizationRequestCollection()
        };


        try
        {
            Entity entityTargetProject = (Entity)_objCommon.Context.InputParameters["Target"];
            if (entityTargetProject != null)
            {
                var reciveryPercentage = RecoveryPercentage.Get(executionContext);
                var measures = GetMeasuresByProjectId(entityTargetProject.Id,_objCommon.GetOrgService());
                foreach (var meas in measures.Entities)
                {
                    UpdateMeasure(meas, reciveryPercentage);
                }
                if (requestWithResults.Requests.Count > 0)
                {
                    ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_objCommon.GetOrgService().Execute(requestWithResults);
                }
                else
                {
                    _objCommon.TracingService.Trace("Project has no measures.");
                }
            }

            var rollupRequest = new CalculateRollupFieldRequest { Target = new EntityReference("ddsm_project", entityTargetProject.Id), FieldName = "ddsm_oemrecoverycost" };
            _objCommon.GetOrgService().Execute(rollupRequest);
        }
        catch (Exception ex)
        {
            SetErrorStatusForDA();
            _objCommon.TracingService.Trace(ex.StackTrace);
        }
    }

    private void UpdateMeasure(Entity meas, double recoveryPercentage)
    {

        var ddsm_incentivepaymentgross = meas.GetAttributeValue<Money>("ddsm_incentivepaymentgross");


        var ddsm_oemrecoverycost = new Money(ddsm_incentivepaymentgross.Value * (decimal)recoveryPercentage);
        var ddsm_incentivepaymentnet = new Money(ddsm_incentivepaymentgross.Value - ddsm_oemrecoverycost.Value);


        meas["ddsm_oemrecoverycost"] = ddsm_oemrecoverycost;
        meas["ddsm_incentivepaymentnet"] = ddsm_incentivepaymentnet;


        UpdateRequest createRequest = new UpdateRequest { Target = meas };
        requestWithResults.Requests.Add(createRequest);

    }

    private EntityCollection GetMeasuresByProjectId(Guid id, IOrganizationService _service)
    {
        var expr = new QueryExpression
        {
            EntityName = "ddsm_measure",
            ColumnSet = new ColumnSet("ddsm_incentivepaymentgross"),
            Criteria = new FilterExpression
            {
                Conditions = {
                        new ConditionExpression("ddsm_projecttomeasureid", ConditionOperator.Equal, id),
                        new ConditionExpression("statecode",ConditionOperator.Equal,(int)State.Active),
                        new ConditionExpression("statuscode",ConditionOperator.Equal,(int)Status.Active)
                    }
            }
        };

        return _service.RetrieveMultiple(expr);
    }
}

