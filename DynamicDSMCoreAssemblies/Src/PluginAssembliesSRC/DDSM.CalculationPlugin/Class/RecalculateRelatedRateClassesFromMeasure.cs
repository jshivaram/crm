﻿using Microsoft.Xrm.Sdk;
using System.Activities;
using System;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.Utils;
using DDSM.CommonProvider.src;
using DDSM.CommonProvider;

public class RecalculateRelatedRateClassesFromMeasure : BaseCodeActivity
{
    #region "Parameter Definition"

    [Input("Measure")]
    [ArgumentEntity("ddsm_measure")]
    [ReferenceTarget("ddsm_measure")]
    public InArgument<EntityReference> Measure { get; set; }

    public override StatusFileDataUploading CurrentOperationStatus
    {
        get
        {
            return StatusFileDataUploading.rollupsRecalculation;
        }

        set
        {
            throw new NotImplementedException();
        }
    }
    #endregion
    #region Internal fields

    readonly string MainEntity = "ddsm_measure";
    readonly string _ddsmRateClass = "ddsm_rateclass";
    #endregion


    protected override void ExecuteActivity(CodeActivityContext context)
    {
        var _objCommon = new Common(context);


        var targetMeasure = Measure.Get(context);
        if (targetMeasure != null)
        {
            var measureData = GetMeasureData(targetMeasure.Id, _objCommon.GetOrgService(systemCall: true));
            UpdateRelatedRecords(measureData , _objCommon.GetOrgService(systemCall:true));
        }
    }

    private void UpdateRelatedRecords(Entity measureData, IOrganizationService service)
    {
        if (measureData != null && measureData.Attributes != null)
        {
            var query = new QueryExpression
            {
                EntityName = _ddsmRateClass,
                ColumnSet = new ColumnSet(allColumns: true),
                Criteria = new FilterExpression(LogicalOperator.And) {
                    Conditions = {
                        new ConditionExpression("ddsm_measureid", ConditionOperator.Equal, measureData.Id),
                        new ConditionExpression("statecode",ConditionOperator.Equal,(int)State.Active),
                        new ConditionExpression("statuscode",ConditionOperator.Equal,(int)Status.Active)
                    }
                }
            };

            var rateClasses = service.RetrieveMultiple(query);
            foreach (var item in rateClasses.Entities)
            {
                var updatedRateClass = CrmHelper.getDataRelationMapping(service, _ddsmRateClass, MainEntity, measureData.Id);
                updatedRateClass.Id = item.Id;
                service.Update(updatedRateClass);
            }
        }
    }

    private Entity GetMeasureData(Guid id, IOrganizationService service)
    {
        return service.Retrieve(MainEntity, id, new ColumnSet(allColumns: true));
    }
}

