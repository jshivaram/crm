﻿using System;
using System.Linq;
using DDSM.CommonProvider;
using DDSM.CommonProvider.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;

namespace DDSM_CalculationProgramInterval.Utils
{
    public static class Extentions
    {      
        public static string GetFirterInValues(this object recordValue)
        {
            var result = "";
            var s = recordValue as string;
            if (s != null)
                result = s.Split(',').Where(item => !string.IsNullOrEmpty(item)).Aggregate(result, (current, item) => current + $"<value>{item}</value>");
            return result;
        }

        public static Entity GetDataUploaderRef(this Common _objCommon)
        {
            var result = new Entity();
            try
            {
                var target = CrmHelper.GetTargetData(_objCommon);

                if (target.LogicalName != "ddsm_taskqueue")
                    return new Entity();

                var taskQueue = _objCommon.GetOrgService(systemCall: true).Retrieve(target.LogicalName, target.Id, new ColumnSet("ddsm_datauploader"));

                object tmpObj;
                if (taskQueue.Attributes.TryGetValue("ddsm_datauploader", out tmpObj))
                {
                    var dataUploaderRef = (EntityReference)tmpObj;
                    result = _objCommon.GetOrgService(systemCall: true).Retrieve(dataUploaderRef.LogicalName, dataUploaderRef.Id, new ColumnSet("ddsm_statusfiledatauploading"));
                }
            }
            catch (Exception ex)
            {
                _objCommon.TracingService.Trace("Error on GetDataUploaderRef() " + ex.Message);
                return result;
            }
            return result;
        }

        public static object GetRecordValue(this AttributeCollection records, string key)
        {
            if (records.Keys.Contains(key))
            {
                var record = records[key];

                return (record as AliasedValue)?.Value;
            }
            else return null;
        }

        public static object GetRecordValue(this object record)
        {
            if ((record as AliasedValue)?.Value != null)
            {
                return ((AliasedValue)record).Value;
            }
            else if (record != null && !(record is AliasedValue))
                return record;
            else return null;
        }

        /// <summary>
        /// This function is used to retrieve the optionset labek using the optionset value
        /// </summary>
        /// <param name="service"></param>
        /// <param name="entityName"></param>
        /// <param name="attributeName"></param>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        public static string GetOptionsSetTextForValue(IOrganizationService service, string entityName, string attributeName, int selectedValue)
        {
            var retrieveAttributeRequest = new
            RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = attributeName,
                RetrieveAsIfPublished = true
            };
            // Execute the request.
            var retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
            // Access the retrieved attribute.
            var retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)
            retrieveAttributeResponse.AttributeMetadata;// Get the current options list for the retrieved attribute.
            var optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
            string selectedOptionLabel = null;
            foreach (var oMD in optionList)
            {
                if (oMD.Value == selectedValue)
                {
                    selectedOptionLabel = oMD.Label.LocalizedLabels[0].Label;
                    break;
                }
            }
            return selectedOptionLabel;
        }
    }
}
