﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using DDSM.SmartMeasureCalculationPlugin.Model;
using Newtonsoft.Json;


namespace DDSM.CalculationPlugin.Utils
{

    public enum StatusFileDataUploading
    {
        notUploaded = 962080000,
        fileUploadCompleted = 962080001,
        fileUploadFailed = 962080002,
        parsingFile = 962080003,
        parsingFileCompleted = 962080004,
        parsingFileFailed = 962080005,
        contactsProcessing = 962080006,
        accountsProcessing = 962080007,
        sitesProcessing = 962080008,
        projectGroupsProcessing = 962080009,
        projectsProcessing = 962080010,
        measuresProcessing = 962080011,
        projectReportingsProcessing = 962080012,
        measureReportingsProcessing = 962080013,
        relationshipsProcessing = 962080014,
        recordsUploadCompleted = 962080015,
        recalculationStarted = 962080016,
        ESPRecalculation = 962080017,
        recalculationBusinessRules = 962080018,
        rollupsRecalculation = 962080019,
        intervalsRecalculation = 962080020,
        importSuccessfully = 962080021,
        importFailed = 962080022,
        recordsUploadFailed = 962080023
    }

    public enum TaskEntity
    {
        Measure = 962080000,
        Project = 962080001,
        ProgramInterval = 962080002,
        ProgramOffering = 962080003,
        Portfolio = 962080004,
        Financial = 962080005,
        ProjectGroupFinancial = 962080006,
        Program = 962080007,
        DsmPlan = 962080008,
        ProjectGroupNotification = 962080010,
    }

    public class TaskQueue
    {

        enum State { Active = 0, Inactive = 1 }
        enum Status { Active = 1, Inactive = 2 }

        #region internal fields
        IOrganizationService _service;
        static string MainEntity = "ddsm_taskqueue";
        #endregion


        public TaskQueue(IOrganizationService service)
        {
            _service = service;
        }

        #region Public Methods
        public Guid Create(DDSM_Task task, TaskEntity recordType, int startIndex = 0,EntityReference dataUploader =null)
        {
            var newTask = new Entity(MainEntity);
            var json = "";
            #region Fill new Task
            newTask["ddsm_name"] = task.GetTaskName();
            newTask["ddsm_taskentity"] = new OptionSetValue((int)task.TaskEntity);
            newTask["ddsm_entityrecordtype"] = new OptionSetValue((int)recordType);
            if (dataUploader != null && !Guid.Empty.Equals(dataUploader?.Id))
                newTask["ddsm_datauploader"] = dataUploader;
            // if count of processed items more than 10 000 will be created two Records
            if (task.ProcessedItems0.Count > 10000)
            {
                json = JsonConvert.SerializeObject(new UserInputObj2 { SmartMeasures = task.ProcessedItems0.GetPortion(startIndex).ToList() });
                newTask["ddsm_processeditems0"] = json;
                Create(task, recordType, startIndex + 1);
            }
            else
            {
                json = JsonConvert.SerializeObject(new UserInputObj2 { SmartMeasures = task.ProcessedItems0 });
                newTask["ddsm_processeditems0"] = json;
            }

            #endregion

            if (CheckIsDuplicateTask(task, recordType, json))
            {
                return Guid.Empty;
            }
            else
            {
                var newTaskGuid = _service.Create(newTask);
                return newTaskGuid;
            }
        }

        private bool CheckIsDuplicateTask(DDSM_Task task, TaskEntity recordType, string userObjJson)
        {
            var query = new QueryExpression()
            {
                EntityName = MainEntity,
                ColumnSet = new ColumnSet("ddsm_taskentity", "ddsm_entityrecordtype", "ddsm_processeditems0"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("statecode",ConditionOperator.Equal,(int)State.Active ),
                        new ConditionExpression("statuscode",ConditionOperator.Equal,(int)Status.Active ),
                        new ConditionExpression("ddsm_taskentity",ConditionOperator.Equal,(int)task.TaskEntity),
                        new ConditionExpression("ddsm_entityrecordtype",ConditionOperator.Equal,(int)recordType),
                        new ConditionExpression("ddsm_processeditems0",ConditionOperator.Equal,userObjJson)

                    }
                }
            };

            var result = _service.RetrieveMultiple(query);

            if (result.Entities.Count > 0)
            {
                return true;
            }
            else
            { return false; }
        }

        public void Update(DDSM_Task task)
        {
            var newTask = new Entity("ddsm_taskqueue", task.Id);
            #region Fill new Task
            newTask["ddsm_name"] = task.Name;
            newTask["ddsm_taskentity"] = new OptionSetValue((int)task.TaskEntity);
            #endregion
            _service.Update(newTask);
        }

        public Entity Get(Guid Id)
        {
            return _service.Retrieve(MainEntity, Id, new ColumnSet(allColumns: true));
        }

        internal void Create(DDSM_Task dDSM_Task, object programOffering)
        {
            throw new NotImplementedException();
        }


        #endregion

    }

    public class DDSM_Task
    {
        public DDSM_Task(TaskEntity taskEntity)
        {
            TaskEntity = taskEntity;
            ProcessedItems0 = new List<Guid>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<Guid> ProcessedItems0 { get; set; }
        public TaskEntity TaskEntity { get; set; }

        public string GetTaskName(int set = 0)
        {
            return Enum.GetName(typeof(TaskEntity), TaskEntity) + " Task Queue - " + "/" + set + DateTime.UtcNow;
        }
    }

    public static class TaskHelper
    {
        public static IList<Guid> GetPortion(this IList<Guid> mylist, int page, int countRecOnPage = 10000)
        {
            return mylist.Skip(page * (page - 1)).Take(countRecOnPage).ToList();
        }
    }

}

