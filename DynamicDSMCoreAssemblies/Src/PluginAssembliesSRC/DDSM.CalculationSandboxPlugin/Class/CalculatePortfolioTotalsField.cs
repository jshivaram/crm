﻿using System.Activities;
using Microsoft.Xrm.Sdk;
using System;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using System.Collections.Generic;
using System.Linq;
using DDSM.CalculationSandbox.Utils;
using DDSM.CalculationSandbox.Model;
using DDSM.CalculationSandbox;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;

/// <summary>
///  Plugin for calculation Program totals field from workflow that runing from ProgramOffering entity
/// </summary>
public class CalculatePortfolioTotalsField : CodeActivity
{
    #region "Parameter Definition"

    //[RequiredArgument]
    //[Input("Program Offerings")]
    //[ArgumentEntity("ddsm_programoffering")]
    //[ReferenceTarget("ddsm_programoffering")]
    //public InArgument<EntityReference> ProgramOffering { get; set; }

    [Input("Portfolio")]
    [ArgumentEntity("ddsm_portfolio")]
    [ReferenceTarget("ddsm_portfolio")]
    public InArgument<EntityReference> Portfolio { get; set; }

    [Input("User Input")]
    public InArgument<string> UserInput { get; set; }

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }


    #endregion

    #region Internal fields
    Common _objCommon;
    IOrganizationService _service;
    readonly string _ddsmPortfolio = "ddsm_portfolio";
    ExecuteMultipleRequest requestWithResults;
    private List<Guid> processedPrograms = new List<Guid>();
    #endregion

    protected override void Execute(CodeActivityContext context)
    {
        #region "Load CRM Service from context"

        _objCommon = new Common(context);
        _service = _objCommon.GetOrgService();
        _objCommon.TracingService.Trace("Load CRM Service from context --- OK");
        #endregion

        var portf = Portfolio.Get(context);
        //Target Entity
        EntityReference Target = (EntityReference)_objCommon.Context.InputParameters["Target"];

        string inputP = null;
        string inputTargetId = null;

        _objCommon.TracingService.Trace("Iput Parameters:");
        //Target ID
        if (Target != null)
        {
            inputTargetId = Target.Id.ToString();
            _objCommon.TracingService.Trace("Iput Parameters. Target ID: " + inputTargetId);
        }
        else { _objCommon.TracingService.Trace("Target ID is null"); }
        //Program
        if (portf != null)
        {
            inputP = portf.Id.ToString();
            _objCommon.TracingService.Trace("Portfolio: " + inputP);
        }
        else { _objCommon.TracingService.Trace("Portfolio is null"); }

        requestWithResults = new ExecuteMultipleRequest()
        {
            // Assign settings that define execution behavior: continue on error, return responses. 
            Settings = new ExecuteMultipleSettings()
            {
                ContinueOnError = false,
                ReturnResponses = true
            },
            // Create an empty organization request collection.
            Requests = new OrganizationRequestCollection()
        };
        try
        {
            var listId = new List<Guid>();
            // resolve parameters
            // var programOfferingRef = ProgramOffering.Get(context);
            var portfolioRef = Portfolio.Get(context);
            // get all prog offerings that must be totals updated

            //get totals of program Int
            //var totals = GetProgramTotals(portfolioRef);
            var userInput = UserInput.Get(context);

            if (!string.IsNullOrEmpty(userInput) && portfolioRef == null)
            {
                UserInputObj2 userInpObj = null;

                userInpObj = JsonConvert.DeserializeObject<UserInputObj2>(userInput);
                if (userInpObj?.SmartMeasures != null)
                {
                    listId = userInpObj.SmartMeasures.Where(x => !x.Equals(Guid.Empty)).Distinct().ToList();
                }
            }
            else if (portfolioRef != null)
            {
                /*var portfolio = GetPortfolio(portfolioRef.Id);
                UpdateTotals(portfolio, totals);*/
                listId.Add(portfolioRef.Id);
            }
            else if (portfolioRef == null)
            {
                if (Target != null)
                {
                    listId.Add(Target.Id);
                }
            }
            else
            {
                throw new Exception("Portfolio parameter is null. Please check it.");
            }

            foreach (var portfilio in listId)
            {
                var totals = GetProgramTotals(portfilio);
                var portfolio = GetPortfolio(portfilio);
                UpdateTotals(portfolio, totals);
            }

            if (requestWithResults.Requests.Count > 0)
            {
                ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_service.Execute(requestWithResults);
                if (responseWithResults.IsFaulted)
                {
                    _objCommon.TracingService.Trace("Update of Portfolio failed! Check correct names fileds or fetch. ");
                }
                else
                {
                    _objCommon.TracingService.Trace(requestWithResults.Requests.Count + " updates was done. Portfolios was updated.");
                }
            }
            else
            {
                _objCommon.TracingService.Trace("Portfolios not updated. No items to update.");
            }
            //if (requestWithResults.Requests.Count > 0)
            //{
            //    ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_service.Execute(requestWithResults);
            //}

            //processCollectedData();
            Complete.Set(context, true);
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("Error of Portfolio totals calculation. Error:" + ex.Message);
            Complete.Set(context, false);
        }
    }

    #region Internal methods
    private void UpdateTotals(Entity portfolio, EntityCollection totals)
    {
        try
        {
            if (totals == null) throw new Exception("Program Interval Totals are null.");
            foreach (var total in totals.Entities)
            {
                foreach (var column in total.Attributes)
                {
                    portfolio[column.Key.ToLower()] = column.Value.GetRecordValue();
                }
            }

            /*Set directprogramunitcost after population of all fields Actual Spending and Actual All kWh Net Savings at Generator*/
            portfolio["ddsm_directprogramunitcost"] = new Money(0);
            decimal actualSpend;
            decimal kwhSavingatGen;
            if (portfolio["ddsm_actualspending"] != null)
            {
                if (portfolio["ddsm_actualspending"].GetType().Name == "Money")
                {
                    if (((Money)portfolio["ddsm_actualspending"]).Value != null && portfolio["ddsm_actualallkwhnetsavingsatgenerator"] != null)
                    {
                        actualSpend = ((Money)portfolio["ddsm_actualspending"]).Value;
                        kwhSavingatGen = (Decimal)portfolio["ddsm_actualallkwhnetsavingsatgenerator"];
                        portfolio["ddsm_directprogramunitcost"] = new Money(actualSpend / kwhSavingatGen);
                    }
                }
            }
            /*End Set directprogramunitcost after population of all fields Actual Spending and Actual All kWh Net Savings at Generator*/
            object dsmplan = null;
            if (portfolio.Attributes.TryGetValue("ddsm_dsmplanid", out dsmplan))
                processedPrograms.Add((dsmplan as EntityReference).Id);

            var updateRequest = new UpdateRequest { Target = portfolio };
            requestWithResults.Requests.Add(updateRequest);
            //  _service.Update(offering);
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("Error On Portfolio totals update. Exeption:" + ex.Message);
        }

    }

    private EntityCollection GetProgramTotals(Guid portfolio)
    {
        var result = new EntityCollection();

        var programCond = (portfolio != null && !string.IsNullOrEmpty(portfolio.ToString())) ?
            $"<condition attribute='ddsm_portfolioid' operator='eq' value='{portfolio.ToString()}' />"
            : "";

        var queryMeasureTotals =
            @"<fetch aggregate='true' >
                  <entity name='ddsm_program' >
                    <attribute name='ddsm_forecastedkwnetsavingsatgenerator' alias='ddsm_ForecastedkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_forecastedkwhnetsavingsatgenerator' alias='ddsm_ForecastedkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_forecastedspending' alias='ddsm_ForecastedSpending' aggregate='sum' />
                    <attribute name='ddsm_forecastedgjsavingsatmeter' alias='ddsm_ForecastedGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_pnsforecastedspending' alias='ddsm_PNSForecastedSpending' aggregate='sum' />
                    <attribute name='ddsm_actualallkwhnetsavingsatgenerator' alias='ddsm_ActualAllkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_actualallkwnetsavingsatgenerator' alias='ddsm_ActualAllkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_actualspending' alias='ddsm_ActualSpending' aggregate='sum' />
                    <attribute name='ddsm_actualallgjsavingsatmeter' alias='ddsm_ActualAllGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_pnsactualspending' alias='ddsm_PNSActualSpending' aggregate='sum' />
                    <attribute name='ddsm_targetkwhnetsavingsatgenerator' alias='ddsm_TargetkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_targetkwnetsavingsatgenerator' alias='ddsm_TargetkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_budget' alias='ddsm_Budget' aggregate='sum' />
                    <attribute name='ddsm_numberofparticipants' alias='ddsm_NumberofParticipants' aggregate='sum' />
                    <attribute name='ddsm_estimatedkwhnetsavingsatgenerator' alias='ddsm_EstimatedkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_estimatedkwnetsavingsatgenerator' alias='ddsm_EstimatedkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_estimatedspending' alias='ddsm_EstimatedSpending' aggregate='sum' />
                    <attribute name='ddsm_directprogramunitcost' alias='ddsm_DirectProgramUnitCost' aggregate='sum' />
                    <attribute name='ddsm_targetgjsavingsatmeter' alias='ddsm_TargetGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_pnsbudget' alias='ddsm_PNSBudget' aggregate='sum' />
                    <attribute name='ddsm_pnsnumberofparticipants' alias='ddsm_PNSNumberofParticipants' aggregate='sum' />
                    <attribute name='ddsm_pnsestimatedspending' alias='ddsm_PNSEstimatedSpending' aggregate='sum' />
                    <attribute name='ddsm_estimatedgjsavingsatmeter' alias='ddsm_EstimatedGJSavingsatMeter' aggregate='sum' />
                    <filter>
                    <condition attribute='statecode' operator='eq' value='0' />
                    <condition attribute='statuscode' operator='eq' value='1' />
                    {0}                                                   
                </filter>
              </entity>
            </fetch>";

        queryMeasureTotals = string.Format(queryMeasureTotals, programCond);
        var queryMeasureExpression = new FetchExpression(queryMeasureTotals);

        var totals = _service.RetrieveMultiple(queryMeasureExpression);

        result.Entities.Add(totals.Entities[0]);

        return result;
    }

    private Entity GetPortfolio(Guid id)
    {
        return _service.Retrieve(_ddsmPortfolio, id, new ColumnSet("ddsm_portfolioid", "ddsm_dsmplanid"));
    }
    #endregion
#if WITH_TASKQ
    private void processCollectedData()
    {
        var taskQueue = new TaskQueue(_objCommon.GetOrgService(systemCall:true));
        taskQueue.Create(new DDSM_Task(TaskEntity.DsmPlan)
        {
            ProcessedItems0 = processedPrograms.Select(x => x).Distinct().ToList()
        }, TaskEntity.DsmPlan
        );
    }
#endif
}

