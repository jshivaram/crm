﻿using DDSM.CalculationSandbox;
using DDSM.CalculationSandbox.Model;
using DDSM.CalculationSandbox.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using DDSM.CommonProvider;

/// <summary>
/// Plugin for calculation ProgramInterval totals field for Forecast and Actual fileds. from workflow that runing from Task Queue entity
/// </summary>
public class CalculateProgramIntrervalTotalsFieldForecastActual : CodeActivity
{
	#region "Parameter Definition"

	[Input("Program Offerings")]
	[ArgumentEntity("ddsm_programoffering")]
	[ReferenceTarget("ddsm_programoffering")]
	public InArgument<EntityReference> ProgramOffering { get; set; }

	[Input("Program")]
	[ArgumentEntity("ddsm_program")]
	[ReferenceTarget("ddsm_program")]
	public InArgument<EntityReference> Program { get; set; }

	[Input("User Input")]
	public InArgument<string> UserInput { get; set; }

	[Output("Complete")]
	public OutArgument<bool> Complete { get; set; }

	Common objCommon;
	IOrganizationService service;

	ExecuteMultipleRequest requestWithResults;
    #endregion
    protected override void Execute(CodeActivityContext context)
    {
        #region "Load CRM Service from context"
        objCommon = new Common(context);
        service = objCommon.GetOrgService();
        objCommon.TracingService.Trace("01 Load CRM Service from context --- OK");
        #endregion

        //Entity ProgramInterval = (Entity)objCommon.Context.InputParameters["Target"];
        //EntityReference ProgramInterval = (EntityReference)objCommon.Context.InputParameters["Target"];
        var progrOf = ProgramOffering.Get(context);
        var progr = Program.Get(context);
        //Target Entity
        EntityReference Target = (EntityReference)objCommon.Context.InputParameters["Target"];

        string inputPO = null;
        string inputP = null;
        string inputTargetId = null;

        objCommon.TracingService.Trace("Iput Parameters:");
        //Target ID
        if (Target != null)
        {
            inputTargetId = Target.Id.ToString();
            objCommon.TracingService.Trace("Iput Parameters. Target ID: " + inputTargetId);
        }
        else { objCommon.TracingService.Trace("Target ID is null"); }
        //Program Offering
        if (progrOf != null)
        {
            inputPO = progrOf.Id.ToString();
            objCommon.TracingService.Trace("Program Offering: " + inputPO);
        }
        else { objCommon.TracingService.Trace("Program Offering is null"); }
        //Program
        if (progr != null)
        {
            inputP = progr.Id.ToString();
            objCommon.TracingService.Trace("Program: " + inputP);
        }
        else { objCommon.TracingService.Trace("Program is null"); }


        try
        {
            requestWithResults = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };
            var measureIDsJson = UserInput.Get(context);
            //Config for Actual
            var actualConfig = GetConfig(service, "actual"); //AdminData conditions Actual
            var forecastConfig = GetConfig(service, "forecast"); //AdminData conditions Forecat
                                                                 //forecastConfig["conditiondate"] = GetConditionDateForecast();
            var measureDateFiledName = "";

            //For Actual Config
            #region Actual Calc Program Intervals
            if (actualConfig.Attributes.ContainsKey("ddsm_pimeasuredatefieldname"))
            {

                measureDateFiledName = actualConfig["ddsm_pimeasuredatefieldname"].ToString();
                measureDateFiledName = !string.IsNullOrEmpty(measureDateFiledName) ? measureDateFiledName.ToLower() : "ddsm_initialphasedate";

            }
            //List of All Intervals for update Actual Field
            List<Entity> IntervalsListActual = new List<Entity>();
            List<Entity> IntervalsListForecast = new List<Entity>();
            //List<Entity> IntervalsListForecast = new List<Entity>();
            Dictionary<int, string> actualPhasesSet = GetAvailableActualPhaseSet(actualConfig);
            Dictionary<int, string> forecastPhasesSet = GetAvailableForecastPhaseSet(forecastConfig);
            EntityCollection conditionIntervals = new EntityCollection();//Common condition of Program and Program Offerings for BOTH!!!!!!

            //var progrOf = ProgramOffering.Get(context);
            //var progr = Program.Get(context);
            //Target Entity
            //EntityReference Target = (EntityReference)objCommon.Context.InputParameters["Target"];

            //Check "User Input" (colection of measures) if it have some data - do some logic with it
            if (!String.IsNullOrEmpty(measureIDsJson))
            {
                var measureIDs = JsonConvert.DeserializeObject<UserInputObj2>(measureIDsJson);
                //measureIDs?.SmartMeasures != null

                //Get Distinct of Program and Program Offerings of Measures
                conditionIntervals = GetConditionFromMeasures(measureIDs);

                foreach (var cond in conditionIntervals.Entities)
                {
                    var programMeas = cond.GetAttributeValue<EntityReference>("ddsm_programid");
                    var programOfferingMeas = cond.GetAttributeValue<EntityReference>("ddsm_programofferingsid");
                    var programIntervalsForUpdateActual = GetProgramIntervals(programOfferingMeas.Id, programMeas.Id, actualConfig, Guid.Empty);
                    //var programIntervalsForUpdateForecast = GetProgramIntervals(programOfferingMeas.Id, programMeas.Id, forecastConfig);
                    IntervalsListActual.AddRange(programIntervalsForUpdateActual.Entities);
                    //IntervalsListForecast..AddRange(programIntervalsForUpdateActual.Entities);
                    //IntervalsListForecast add
                }



            }
            #endregion

            //Else If it null check Parameters ProgramOffering and Program and do logic - OnChange Measure
            else if (progrOf != null && progr != null)
            {
                var programIntervalsForUpdate = GetProgramIntervals(progrOf.Id, progr.Id, actualConfig, Guid.Empty);
                IntervalsListActual.AddRange(programIntervalsForUpdate.Entities);
            }
            //Else If if we need calculate only 1 Target ID - Interval.
            else if (progrOf == null || progr == null)
            {
                if (Target != null)
                {
                    var programIntervalsForUpdate = GetProgramIntervals(Guid.Empty, Guid.Empty, actualConfig, Target.Id);
                    IntervalsListActual.AddRange(programIntervalsForUpdate.Entities);
                }
            }
            else
            {
                throw new Exception("No valid input parameters. Please check it.");
            }
            //Actual Intervals
            IntervalsListActual = IntervalsListActual.Select(x => x).Distinct(new EntityComparer()).ToList();

            //ForecastIntervals
            //IntervalsListForecast = IntervalsListForecast.Select(x => x).Distinct(new EntityComparer()).ToList();

            objCommon.TracingService.Trace("Get GetProgramIntervalsActual. Kol: " + IntervalsListActual.Count);

            //Actual Updates 	//Forecast Updates
            if (IntervalsListActual.Count > 0)
            {
                foreach (var interval in IntervalsListActual)
                {
                    var programMeas = interval.GetAttributeValue<EntityReference>("ddsm_programid");
                    var programOfferingMeas = interval.GetAttributeValue<EntityReference>("ddsm_programofferingsid");

                    //Actual Update
                    //Totals from Measures
                    var totals = GetMeasureTotals(interval, service, programOfferingMeas, programMeas, actualPhasesSet, measureDateFiledName, 0);
                    //From PGF
                    var pgfTotals = GetPGFinancialTotal(interval, service, programOfferingMeas, Target, "ddsm_cc1_fundcode", "962080000");
                    var pgfTotalsPNS = GetPGFinancialTotal(interval, service, programOfferingMeas, Target, "ddsm_cc3_fundcode", "962080001");
                    //From CostLineItem
                    var costLineItemTotals = GetCostLineItemTotal(interval, service, programOfferingMeas, Target, "ddsm_fundcode", "962080000");
                    var costLineItemTotalsPNS = GetCostLineItemTotal(interval, service, programOfferingMeas, Target, "ddsm_fundcode", "962080001");
                    //Actual Updates
                    UpdatePiTotals(interval, totals, 0, pgfTotals, costLineItemTotals, pgfTotalsPNS, costLineItemTotalsPNS);

                    //Forecast Update
                    var totalsforecast = GetMeasureTotals(interval, service, programOfferingMeas, programMeas, forecastPhasesSet, measureDateFiledName, 1);
                    UpdatePiTotals(interval, totalsforecast, 1, pgfTotals, costLineItemTotals, pgfTotalsPNS, costLineItemTotalsPNS);
                }
            }

            //Common Update
            if (requestWithResults.Requests.Count > 0)
            {
                ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)service.Execute(requestWithResults);
                if (responseWithResults.IsFaulted)
                {
                    foreach (var resp in responseWithResults.Responses)
                    {
                        if (resp.Fault != null)
                        {
                            objCommon.TracingService.Trace("Update is falted in ExecuteMultipleResponse: " + resp.Fault.Message);
                        }
                    }
                    //objCommon.TracingService.Trace("Update of Interval failed! Check correct names fileds or fetch. ");
                }
                else
                {
                    objCommon.TracingService.Trace(requestWithResults.Requests.Count + " updates was done(Actual and Forecast fileds). Program Intervals was updated.");
                }
            }
            else
            {
                objCommon.TracingService.Trace("Program Interval not updated.");
            }

            //processCollectedData(conditionIntervals);
            Complete.Set(context, true);
        }
        catch (Exception ex)
        {
            objCommon.TracingService.Trace("Error of ProgramInterval totals calculation for Forecast and Actual fields. Error:" + ex.Message + ". Input ProgramOffering: " + inputPO + ", Input Program: " + inputP + " Targer: " + inputTargetId);
            Complete.Set(context, false);
        }

        //throw new NotImplementedException();
    }

	private EntityCollection GetCostLineItemTotal(Entity interval, IOrganizationService service, EntityReference programOff, EntityReference progrIntId, string fieldName, string fundCode)
	{

		var result = new EntityCollection();


		try
		{
			Contract.Ensures(Contract.Result<EntityCollection>() != null);


			var programOffCond = programOff != null ?
				$"<condition attribute = 'ddsm_programofferingid' operator= 'eq' value = '{programOff.Id.ToString()}'/>"
				: "";
            var progrIntCond = progrIntId != null ?
                $"<condition attribute = 'ddsm_programintervalid' operator= 'eq' value = '{progrIntId.Id.ToString()}'/>"
                : "";

            var startDate = new DateTime();
			var endDate = new DateTime();

			List<string> cond = new List<string>();


			if (interval.Attributes.ContainsKey("ddsm_startdate"))
			{
				startDate = (DateTime)interval.Attributes["ddsm_startdate"];
                startDate = startDate.AddSeconds(-1);
			}
			if (interval.Attributes.ContainsKey("ddsm_enddate"))
			{
				endDate = (DateTime)interval.Attributes["ddsm_enddate"];
                endDate = endDate.AddDays(1);
            }

			var fundCodeCond = $"<condition attribute='{fieldName}' operator='eq' value='{fundCode}'/>";

			var queryMeasureTotals =
                @"<fetch aggregate='true'>
					<entity name='ddsm_costlineitem'>               
						<attribute name='ddsm_amount' alias='ddsm_amount' aggregate='sum' />
					<filter type='and'>
						<condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='1' />
						<condition attribute='ddsm_date' operator='not-null' />
						{0}
						{1} 
						{2}   
                        {3}            
					</filter>
			  </entity>
			</fetch>";

			var dateFilter = !startDate.Equals(DateTime.MinValue) && !endDate.Equals(DateTime.MinValue) ?
			string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>",
			startDate.ToString("MM/dd/yyyy HH:mm:ss"), endDate.ToString("MM/dd/yyyy HH:mm:ss"), "ddsm_date") : "";

			queryMeasureTotals = string.Format(queryMeasureTotals, programOffCond, dateFilter, fundCodeCond, progrIntCond);


			//old field ddsm_estimatedprojectcomplete
			//objCommon.TracingService.Trace("queryMeasureTotals totals OK. " + queryMeasureTotals);
			var queryMeasureExpression = new FetchExpression(queryMeasureTotals);

			var totals = service.RetrieveMultiple(queryMeasureExpression);

			if (totals.Entities.Count > 0)
				result.Entities.Add(totals.Entities[0]);

			return result;
		}
		catch (Exception ex)
		{
			return result;
		}
	}

	private EntityCollection GetPGFinancialTotal(Entity interval, IOrganizationService service, EntityReference programOff, EntityReference progrIntId, string fieldName, string fundCode)
	{
		var result = new EntityCollection();
		try
		{
			Contract.Ensures(Contract.Result<EntityCollection>() != null);


			var programOffCond = programOff != null ?
				$"<condition attribute = 'ddsm_programofferingid' operator= 'eq' value = '{programOff.Id.ToString()}'/>"
				: "";
            /*var programIntCond = progrIntId != null ?
                $"<condition attribute = 'ddsm_programintervalid' operator= 'eq' value = '{progrIntId.Id.ToString()}'/>"
                : "";*/

            var startDate = new DateTime();
			var endDate = new DateTime();

			List<string> cond = new List<string>();


			if (interval.Attributes.ContainsKey("ddsm_startdate"))
			{
				startDate = (DateTime)interval.Attributes["ddsm_startdate"];
                startDate = startDate.AddSeconds(-1);
			}
			if (interval.Attributes.ContainsKey("ddsm_enddate"))
			{
				endDate = (DateTime)interval.Attributes["ddsm_enddate"];
                endDate = endDate.AddDays(1);
            }

			var fundCodeCond = $"<condition attribute='{fieldName}' operator='eq' value='{fundCode}'/>";
			var fundCodeCond2 = "";
			if (fieldName == "ddsm_cc1_fundcode")
			{
				fundCodeCond2 = $"<condition attribute='ddsm_cc2_fundcode' operator='eq' value='{fundCode}'/>";
			}
            var queryMeasureTotals =
                        @"<fetch aggregate='true' >
                              <entity name='ddsm_projectgroupfinancials' >
                                <attribute name='ddsm_cc1_amount' alias='ddsm_cc1_amount' aggregate='sum' />
                                <attribute name='ddsm_cc2_amount' alias='ddsm_cc2_amount' aggregate='sum' />
                                <attribute name='ddsm_cc3_amount' alias='ddsm_cc3_amount' aggregate='sum' />
                                <filter type='and' >
                                  <condition attribute='statecode' operator='eq' value='0' />
                                  <condition attribute='statuscode' operator='eq' value='1' />
                                  {0}
                                  {2}
                                  {3}                                  
                                </filter>
                                <link-entity name='ddsm_projectgroup' from='ddsm_projectgroupid' to='ddsm_projectgrouptoprojectgroupfinanid' alias='PG' >                                  
                                  <filter type='and'>
                                   {1}                                                                        
                                  </filter>
                                </link-entity>
                              </entity>
                    </fetch>";
            //<condition attribute='ddsm_actualend2' operator='not-null' />
            var dateFilter = !startDate.Equals(DateTime.MinValue) && !endDate.Equals(DateTime.MinValue) ?
				  string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>",
				  startDate.ToString("MM/dd/yyyy HH:mm:ss"), endDate.ToString("MM/dd/yyyy HH:mm:ss"), "ddsm_claimdate") : "";

				 queryMeasureTotals = string.Format(queryMeasureTotals, programOffCond, dateFilter, fundCodeCond, fundCodeCond2);

			
			//old field ddsm_estimatedprojectcomplete
			//objCommon.TracingService.Trace("queryMeasureTotals totals OK. " + queryMeasureTotals);
			var queryMeasureExpression = new FetchExpression(queryMeasureTotals);

			var totals = service.RetrieveMultiple(queryMeasureExpression);

			if (totals.Entities.Count > 0)
				result.Entities.Add(totals.Entities[0]);

			return result;
		}
		catch (Exception ex)
		{
			return result;
		}
	}

#if WITH_TASKQ
	private void processCollectedData(EntityCollection intervals)
	{
		var listIds = new List<Guid>();
		foreach (var interv in intervals.Entities)
		{
			listIds.Add(interv.GetAttributeValue<EntityReference>("ddsm_programofferingsid").Id);// ToString();
		}

		var taskQueue = new TaskQueue(objCommon.GetOrgService(systemCall:true));
		taskQueue.Create(new DDSM_Task(TaskEntity.ProgramOffering)
		{
			ProcessedItems0 = listIds.Select(x => x).Distinct().ToList()
		}, TaskEntity.ProgramOffering
		);
	}
#endif

    public enum State
    {
        Active = 0,
        Inactive = 1
    };

    public enum Status
    {
        Active = 1,
        Inactive = 2
    };

    private Entity GetConfig(IOrganizationService service, string typeOfFields)
	{
        string adminDataId = "";

        var expr = new QueryExpression
		{
			EntityName = "ddsm_admindata",
			ColumnSet = new ColumnSet(true),
			Criteria = new FilterExpression
			{
				FilterOperator = LogicalOperator.And,
				Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, "Admin Data"),
                new ConditionExpression("statecode", ConditionOperator.Equal, (int)State.Active),
                new ConditionExpression("statuscode", ConditionOperator.Equal, (int)Status.Active)
                }
			}
		};

		var adminData = service.RetrieveMultiple(expr);
		if (adminData != null && adminData.Entities?.Count >= 1)
		{
			adminDataId = adminData.Entities[0].Attributes["ddsm_admindataid"].ToString();
		}

		if (typeOfFields == "actual")
		{
			return service.Retrieve("ddsm_admindata", Guid.Parse(adminDataId),
					new ColumnSet("ddsm_phaseset1", "ddsm_phaseset2", "ddsm_phaseset3",
					"ddsm_useyearcorrection", "ddsm_programintervalyearorrection", "ddsm_pimeasuredatefieldname"));
		}
		else if (typeOfFields == "forecast")
		{
			return service.Retrieve("ddsm_admindata", Guid.Parse(adminDataId),
					new ColumnSet("ddsm_phaseset4", "ddsm_phaseset5",
					"ddsm_useyearcorrection", "ddsm_programintervalyearorrection", "ddsm_pimeasuredatefieldname"));
		}
		else { return null; }
	}

	private Dictionary<int, string> GetAvailableActualPhaseSet(Entity config)
	{
		var result = new Dictionary<int, string>();
		var fieldname = "ddsm_phaseset";
		var countOfPhaseSet = 3;

		for (int i = 0; i <= countOfPhaseSet; i++)
		{
			if (config.Attributes.ContainsKey(fieldname + i))
			{
				var value = config[fieldname + i].ToString();
				if (!string.IsNullOrEmpty(value))
				{
					result.Add(i, value);
				}
			}
		}
		return result;
	}

	private Dictionary<int, string> GetAvailableForecastPhaseSet(Entity config)
	{
		var result = new Dictionary<int, string>();
		var fieldname = "ddsm_phaseset";
		var countOfPhaseSet = 5;

		for (int i = 3; i <= countOfPhaseSet; i++)
		{
			if (config.Attributes.ContainsKey(fieldname + i))
			{
				var value = config[fieldname + i].ToString();
				if (!string.IsNullOrEmpty(value))
				{
					result.Add(i, value);
				}
			}
		}
		return result;
	}

	//Common
	private EntityCollection GetConditionFromMeasures(UserInputObj2 measureIDs)
	{
		var result = new EntityCollection();
		try
		{
			Contract.Ensures(Contract.Result<EntityCollection>() != null);



			var queryMeasureConditions = @"<fetch distinct='true' >
											  <entity name='ddsm_measure' >
												<attribute name='ddsm_programid' />
												<attribute name='ddsm_programofferingsid' />
												<filter type='and' >
                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                  <condition attribute='statuscode' operator='eq' value='1' />
												  <condition attribute='ddsm_measureid' operator='in' >
													{0}
												  </condition>
												</filter>
											  </entity>
											</fetch>";
			List<string> measIds = new List<string>();

			foreach (var meas in measureIDs.SmartMeasures)
			{
				measIds.Add(string.Format("<value>{0}</value>", meas));
			}
			queryMeasureConditions = String.Format(queryMeasureConditions, string.Join(System.Environment.NewLine, measIds));
			//objCommon.TracingService.Trace("getProjectCounts totals OK. " + queryMeasureTotals);
			var queryMeasureExpression = new FetchExpression(queryMeasureConditions);
			return service.RetrieveMultiple(queryMeasureExpression);
		}
		catch (Exception)
		{
			return result;
		}

	}

	private EntityCollection GetProgramIntervals(Guid programOfferingId, Guid program, Entity config, Guid programIntervalId)
	{
		var dataFilter = "";

		#region "Get Program Intervals"

		var programOffCond = Guid.Empty != programOfferingId ?
			$"<condition attribute='ddsm_programofferingsid' operator='eq' value='{programOfferingId}' />"
			: "";

		var programCond = Guid.Empty != program ?
			$"<condition attribute='ddsm_programid' operator='eq' value='{program}' />"
			: "";

		var programIntervalCond = Guid.Empty != programIntervalId ?
			$"<condition attribute='ddsm_kpiintervalid' operator='eq' value='{programIntervalId}' />"
			: "";

		var queryProgIntervals = @"<fetch version='1.0'>
							  <entity name='ddsm_kpiinterval'>
									<attribute name='ddsm_programid'/>
									<attribute name='ddsm_programofferingsid'/>
									<attribute name='ddsm_startdate' />
									<attribute name = 'ddsm_enddate' />
									<attribute name = 'ddsm_calculatefrommeasure' />
									<attribute name = 'ddsm_calculatefromcostlineitem' />
									<attribute name = 'ddsm_calculatefrompgfinancial' />
								<filter>
								{0}
								  <condition attribute='statecode' operator='eq' value='0'/>
                                  <condition attribute='statuscode' operator='eq' value='1' />
								{1}  
								{2}   
								{3}                         
								</filter>
							  </entity>
							</fetch>";

		// DateTime dateWithCorrection;
		if (config.Attributes.ContainsKey("ddsm_useyearcorrection") &&
			(bool)config.Attributes["ddsm_useyearcorrection"] &&
			config.Attributes.ContainsKey("ddsm_programintervalyearorrection")) //use year correction
		{
			int dateCorrection = int.Parse(config.Attributes["ddsm_programintervalyearorrection"].ToString());
			dataFilter =
				$"<condition attribute='createdon' operator='between'><value>{DateTime.Now.Date.AddYears(-dateCorrection).ToString("MM/dd/yyyy HH:mm:ss")}</value><value>{DateTime.Now.Date.AddDays(1).ToString("MM/dd/yyyy HH:mm:ss")}</value></condition>";
		}

		queryProgIntervals = string.Format(queryProgIntervals, programOffCond, dataFilter, programCond, programIntervalCond);
		//objCommon.TracingService.Trace("queryProgIntervals totals OK. " + queryProgIntervals);

		var queryProgIntervalsExpression = new FetchExpression(queryProgIntervals);
		#endregion

		return service.RetrieveMultiple(queryProgIntervalsExpression);

	}

	private EntityCollection GetMeasureTotals(Entity interval, IOrganizationService service, EntityReference programOff, EntityReference program, Dictionary<int, string> phases, string dateFiledName, int forecast)
	{
		var result = new EntityCollection();
		try
		{
			Contract.Ensures(Contract.Result<EntityCollection>() != null);


			var programOffCond = programOff != null ?
				$"<condition attribute = 'ddsm_programofferingsid' operator= 'eq' value = '{programOff.Id.ToString()}'/>"
				: "";


			var programCond = program != null ?
				$"<condition attribute = 'ddsm_programid' operator= 'eq' value = '{program.Id.ToString()}'/>"
				: "";

			var startDate = new DateTime();
			var endDate = new DateTime();



			List<string> cond = new List<string>();
			foreach (var phase in phases.Values)
			{
				var splitedValue = phase.Split(',');
				foreach (var item in splitedValue)
				{
					cond.Add(string.Format("<value>{0}</value>", item.Trim()));
				}
				//
			}
            string phaseCondition = "";
            string phaseConditionCurrentProject = "";
            if (cond.Count > 0)
            {
                phaseCondition = "<condition attribute='ddsm_projectphasename' operator='in'>{0}</condition>";
                phaseConditionCurrentProject = "<condition entityname='Proj' attribute='ddsm_phase' operator='in'>{0}</condition>";
                phaseCondition = String.Format(phaseCondition, string.Join(System.Environment.NewLine, cond));
                phaseConditionCurrentProject = String.Format(phaseConditionCurrentProject, string.Join(System.Environment.NewLine, cond));
            }
            else
            {
                phaseCondition = "<condition attribute='ddsm_projectphasename' operator='null' />";
                phaseConditionCurrentProject = "<condition entityname='Proj' attribute='ddsm_phase' operator='null' />";
            }




			string linkEntity = @"<link-entity name='ddsm_project' from='ddsm_projectid' to='ddsm_projecttomeasureid' alias='Proj'/>";

            if (interval.Attributes.ContainsKey("ddsm_startdate"))
            {
                startDate = (DateTime)interval.Attributes["ddsm_startdate"];
                startDate = startDate.AddSeconds(-1);
            }
            if (interval.Attributes.ContainsKey("ddsm_enddate"))
            {
                endDate = (DateTime)interval.Attributes["ddsm_enddate"];
                endDate = endDate.AddDays(1);

            }

				var queryMeasureTotals =
                    @"<fetch aggregate='true'>
					<entity name='ddsm_measure'>               
						<attribute name='ddsm_kwhnetsavingsatgenerator' alias='ddsm_kwhnetsavingsatgenerator' aggregate='sum' />
						<attribute name='ddsm_incentivepaymentnetpns' alias='ddsm_incentivepaymentnetpns' aggregate='sum' />
						<attribute name='ddsm_kwnetsavingsatgenerator' alias='ddsm_kwnetsavingsatgenerator' aggregate='sum' />
						<attribute name='ddsm_gjsavingsatmeter' alias='ddsm_gjsavingsatmeter' aggregate='sum' />
						<attribute name='ddsm_incentivecostfinancing' alias='ddsm_incentivecostfinancing' aggregate='sum' />
						<attribute name='ddsm_incentivepaymentnetdsm' alias='ddsm_incentivepaymentnetdsm' aggregate='sum' />
					<filter type='and'>
						<condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='1' />
						{0}
						{1} 
						{2}
						{3}
						{5}                 
					</filter>
						{4}
			  </entity>
			</fetch>";

			if (forecast == 0)
			{
				var dateFilter = !startDate.Equals(DateTime.MinValue) && !endDate.Equals(DateTime.MinValue) ?
				  string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>",
				  startDate.ToString("MM/dd/yyyy HH:mm:ss"), endDate.ToString("MM/dd/yyyy HH:mm:ss"), dateFiledName) : "";

				queryMeasureTotals = string.Format(queryMeasureTotals, programOffCond, phaseCondition, dateFilter, programCond, linkEntity, phaseConditionCurrentProject);
			}
			else if (forecast == 1)
			{
				//string conditionDataField = GetConditionField();

				var dateFilterForecast = string.Format(@"<link-entity name='ddsm_project' from='ddsm_projectid' to='ddsm_projecttomeasureid' alias='Proj' >
											  <filter type='and'>
						                        <condition attribute='statecode' operator='eq' value='0' />
                                                <condition attribute='statuscode' operator='eq' value='1' />
												<condition attribute='ddsm_projectcompletiondateupdatedestimate' operator='on-or-after' value='{0}'/>
												<condition attribute='ddsm_projectcompletiondateupdatedestimate' operator='on-or-before' value='{1}'/>
											  </filter>
											</link-entity>", startDate.ToString("MM/dd/yyyy HH:mm:ss"), endDate.ToString("MM/dd/yyyy HH:mm:ss")) ;
				queryMeasureTotals = string.Format(queryMeasureTotals, programOffCond, phaseCondition, programCond, "", dateFilterForecast, "");
			}
			//old field ddsm_estimatedprojectcomplete
			//objCommon.TracingService.Trace("queryMeasureTotals totals OK. " + queryMeasureTotals);
			var queryMeasureExpression = new FetchExpression(queryMeasureTotals);

				var totals = service.RetrieveMultiple(queryMeasureExpression);
			
				if (totals.Entities.Count > 0)
					result.Entities.Add(totals.Entities[0]);
				/*
				#region getProjectCounts

				queryMeasureTotals = @"<fetch aggregate='true' >
									  <entity name='ddsm_measure'>
										<link-entity name='ddsm_project' from='ddsm_projectid' to='ddsm_projecttomeasureid' alias='project' >
										  <attribute name='ddsm_recordtype' alias='type' groupby='true' />
										  <attribute name='ddsm_recordtype' alias='countType' aggregate='count' />
										  <filter type='and' >
											<condition attribute='ddsm_recordtype' operator='not-null' />
										   
										  </filter>
										</link-entity>
									  <filter type='and' >
											{0}
											{1}
											{2}
											{3}
									  </filter>
									  </entity>
									</fetch>";
				queryMeasureTotals = string.Format(queryMeasureTotals, programOffCond, phaseCondition, dateFilter, programCond);
				//objCommon.TracingService.Trace("getProjectCounts totals OK. " + queryMeasureTotals);
				queryMeasureExpression = new FetchExpression(queryMeasureTotals);
				totals = GetProjectCountByRecordType(service.RetrieveMultiple(queryMeasureExpression), phase.Key);


				if (totals.Entities.Count > 0)
					result.Entities.Add(totals.Entities[0]);
				#endregion
				*/
			
			return result;
		}
		catch (Exception ex)
		{
			return result;
		}
	}

	private void UpdatePiTotals(Entity programInterval, EntityCollection totals, int forecast, EntityCollection projectGroupFinancials, EntityCollection costLineItem, EntityCollection projectGroupFinancialsPNS, EntityCollection costLineItemPNS) //Entity projectGroupFinancials, //Entity costLineItem
	{
		try
		{
            //objCommon.TracingService.Trace("Start Update PI");

            if (totals == null) throw new Exception("Measure Totals are null.");


			//Checkbox
			var calcMeas = programInterval.GetAttributeValue<bool>("ddsm_calculatefrommeasure");
			var calcPGF = programInterval.GetAttributeValue<bool>("ddsm_calculatefrompgfinancial");
			var calcCostLineItem = programInterval.GetAttributeValue<bool>("ddsm_calculatefromcostlineitem");
			#region Temp
			// if we need hardcoded mapping
			if (forecast == 0)
			{
                objCommon.TracingService.Trace("Start Actual (0) Update PI");
                programInterval["ddsm_actualallkwhnetsavingsatgenerator"] = totals.Entities[0].Attributes.GetRecordValue("ddsm_kwhnetsavingsatgenerator");
				programInterval["ddsm_actualallkwnetsavingsatgenerator"] = totals.Entities[0].Attributes.GetRecordValue("ddsm_kwnetsavingsatgenerator");
				programInterval["ddsm_actualallgjsavingsatmeter"] = totals.Entities[0].Attributes.GetRecordValue("ddsm_gjsavingsatmeter");
				//ddsm_pnsactualspending = Measure + PGF + CoatLineItem
				//projectGroupFinancialsPNS, costLineItemPNS

				#region Add ActualSpending and ActualSpendingPNS
				decimal actualSpendingPNS;
				decimal firstMoneyPNS = 0;
				decimal pgfMoneyPNS = 0;
				decimal cliMoneyPNS = 0;

				//From Meas
				if (calcMeas == true)
				{
					if (totals.Entities.Count > 0)
					{
						firstMoneyPNS = ((Money)totals.Entities[0].Attributes.GetRecordValue("ddsm_incentivepaymentnetpns")) != null ? ((Money)totals.Entities[0].Attributes.GetRecordValue("ddsm_incentivepaymentnetpns")).Value : 0;

					}
				}
				//From PGF
				if (calcPGF == true)
				{
					if (projectGroupFinancialsPNS.Entities.Count > 0)
					{
						pgfMoneyPNS = ((Money)projectGroupFinancialsPNS.Entities[0].Attributes.GetRecordValue("ddsm_cc3_amount")) != null ? ((Money)projectGroupFinancialsPNS.Entities[0].Attributes.GetRecordValue("ddsm_cc3_amount")).Value : 0;
					}
				}
				//From CostLineItem
				if (calcCostLineItem == true)
				{
					if (costLineItemPNS.Entities.Count > 0)
					{
						cliMoneyPNS = ((Money)costLineItemPNS.Entities[0].Attributes.GetRecordValue("ddsm_amount")) != null ? ((Money)costLineItemPNS.Entities[0].Attributes.GetRecordValue("ddsm_amount")).Value : 0;
					}
				}
				actualSpendingPNS = firstMoneyPNS + pgfMoneyPNS + cliMoneyPNS;
				var resActualSpendingPNS = new Money(actualSpendingPNS);
				programInterval["ddsm_pnsactualspending"] = resActualSpendingPNS;//totals.Entities[0].Attributes.GetRecordValue("ddsm_incentivepaymentnetpns");

                //objCommon.TracingService.Trace("Actual (0.1) ActualSpending" + resActualSpendingPNS.Value.ToString());

                decimal actualSpending;
				decimal firstMoney = 0;
				decimal secondMoney = 0;
				decimal pgfMoney1 = 0;
				decimal pgfMoney2 = 0;
				decimal cliMoney = 0;
				//From Meas
				if (calcMeas == true)
				{
					if (totals.Entities.Count > 0)
					{
						firstMoney = ((Money)totals.Entities[0].Attributes.GetRecordValue("ddsm_incentivecostfinancing")) != null ? ((Money)totals.Entities[0].Attributes.GetRecordValue("ddsm_incentivecostfinancing")).Value : 0;
						secondMoney = ((Money)totals.Entities[0].Attributes.GetRecordValue("ddsm_incentivepaymentnetdsm")) != null ? ((Money)totals.Entities[0].Attributes.GetRecordValue("ddsm_incentivepaymentnetdsm")).Value : 0;
					}
				}
				//From PGF
				if (calcPGF == true)
				{
					if (projectGroupFinancials.Entities.Count > 0)
					{
						pgfMoney1 = ((Money)projectGroupFinancials.Entities[0].Attributes.GetRecordValue("ddsm_cc1_amount")) != null ? ((Money)projectGroupFinancials.Entities[0].Attributes.GetRecordValue("ddsm_cc1_amount")).Value : 0;
						pgfMoney2 = ((Money)projectGroupFinancials.Entities[0].Attributes.GetRecordValue("ddsm_cc2_amount")) != null ? ((Money)projectGroupFinancials.Entities[0].Attributes.GetRecordValue("ddsm_cc2_amount")).Value : 0;
					}
				}
				//From CostLineItem
				if (calcCostLineItem == true)
				{
					if (costLineItem.Entities.Count > 0)
					{
						cliMoney = ((Money)costLineItem.Entities[0].Attributes.GetRecordValue("ddsm_amount")) != null ? ((Money)costLineItem.Entities[0].Attributes.GetRecordValue("ddsm_amount")).Value : 0;
					}
				}
				actualSpending = firstMoney + secondMoney + pgfMoney1 + pgfMoney2 + cliMoney; // add PGF = (pgfMoney1 + pgfMoney2) add CostLineItem = cliMoney
				//actualSpending = ((Money)totals.Entities[0].Attributes.GetRecordValue("ddsm_incentivecostfinancing")).Value + ((Money)totals.Entities[0].Attributes.GetRecordValue("ddsm_incentivepaymentnetdsm")).Value;
				var resActualSpending = new Money(actualSpending);
				//ddsm_actualspending = Measure + PGF + CoatLineItem
				//projectGroupFinancials, costLineItem

				programInterval["ddsm_actualspending"] = resActualSpending;
                //objCommon.TracingService.Trace("Actual (0.2) ActualSpending" + resActualSpending.Value.ToString());
                #endregion
            }
			if (forecast == 1)
			{
                objCommon.TracingService.Trace("Start Actual (1) Update PI");
                programInterval["ddsm_forecastedkwhnetsavingsatgenerator"] = totals.Entities[0].Attributes.GetRecordValue("ddsm_kwhnetsavingsatgenerator");
				programInterval["ddsm_forecastedkwnetsavingsatgenerator"] = totals.Entities[0].Attributes.GetRecordValue("ddsm_kwnetsavingsatgenerator");
				programInterval["ddsm_forecastedgjsavingsatmeter"] = totals.Entities[0].Attributes.GetRecordValue("ddsm_gjsavingsatmeter");
				programInterval["ddsm_pnsforecastedspending"] = totals.Entities[0].Attributes.GetRecordValue("ddsm_incentivepaymentnetpns");

				decimal actualSpending = 0;
				object ddsm_incentivecostfinancingO;
				object ddsm_incentivepaymentnetdsmO;

				decimal ddsm_incentivecostfinancing =0;
				decimal ddsm_incentivepaymentnetdsm =0;


				if (totals.Entities[0].Attributes.TryGetValue("ddsm_incentivecostfinancing", out ddsm_incentivecostfinancingO))
					ddsm_incentivecostfinancing = (ddsm_incentivecostfinancingO as AliasedValue).Value != null ? ((Money)(ddsm_incentivecostfinancingO as AliasedValue).Value).Value : 0;


				if (totals.Entities[0].Attributes.TryGetValue("ddsm_incentivepaymentnetdsm", out ddsm_incentivepaymentnetdsmO))
					ddsm_incentivepaymentnetdsm = (ddsm_incentivepaymentnetdsmO as AliasedValue).Value != null ? ((Money)(ddsm_incentivepaymentnetdsmO as AliasedValue).Value).Value : 0;


				actualSpending = ddsm_incentivecostfinancing + ddsm_incentivepaymentnetdsm;

				var resActualSpending = new Money(actualSpending);

				if (resActualSpending.Value != 0)
				{
					programInterval["ddsm_forecastedspending"] = resActualSpending;
				}
				else { programInterval["ddsm_forecastedspending"] = null; }
                //objCommon.TracingService.Trace("Actual (1.1) ActualSpending" + resActualSpending.Value.ToString());
            }

            //Calc Direct Program Unit Cost =  Actual Spending / Actual kWh Net Savings at Generator

            programInterval["ddsm_directprogramunitcost"] = new Money(0);
            decimal actualSpend;
            decimal kwhSavingatGen;
            if (programInterval["ddsm_actualspending"]!=null)
            {
                if (programInterval["ddsm_actualspending"].GetType().Name == "Money")
                {
                    if (((Money)programInterval["ddsm_actualspending"]).Value != null && programInterval["ddsm_actualallkwhnetsavingsatgenerator"] != null)
                    {
                        actualSpend = ((Money)programInterval["ddsm_actualspending"]).Value;
                        kwhSavingatGen = (Decimal)programInterval["ddsm_actualallkwhnetsavingsatgenerator"];
                        programInterval["ddsm_directprogramunitcost"] = new Money(actualSpend / kwhSavingatGen);
                    }
                }
            }

            

            #endregion

            //objCommon.TracingService.Trace("Pi Attrs: " + JsonConvert.SerializeObject(programInterval.Attributes) + "Pi guid: " + programInterval.Id.ToString());

            //objCommon.Service.Update(programInterval);
            UpdateRequest createRequest = new UpdateRequest { Target = programInterval };
			requestWithResults.Requests.Add(createRequest);
		}
		catch (Exception ex)
		{
			objCommon.TracingService.Trace("Error On Program Interval totals update. Exeption:" + ex.Message);
		}
	}
}

