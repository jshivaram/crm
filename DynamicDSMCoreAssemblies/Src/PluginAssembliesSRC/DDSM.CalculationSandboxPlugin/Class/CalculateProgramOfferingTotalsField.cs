﻿using System.Activities;
using Microsoft.Xrm.Sdk;
using System;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk.Messages;
using DDSM.CalculationSandbox.Model;
using DDSM.CalculationSandbox;
using DDSM.CalculationSandbox.Utils;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;

/// <summary>
///  Plugin for calculation ProgramOffering totals field from workflow that runing from ProgramInterval entity
/// </summary>
public class CalculateProgramOfferingTotalsField : CodeActivity
{
    #region "Parameter Definition"

    //[RequiredArgument]
    [Input("Program Offerings")]
    [ArgumentEntity("ddsm_programoffering")]
    [ReferenceTarget("ddsm_programoffering")]
    public InArgument<EntityReference> ProgramOffering { get; set; }

    [Input("Program")]
    [ArgumentEntity("ddsm_program")]
    [ReferenceTarget("ddsm_program")]
    public InArgument<EntityReference> Program { get; set; }

    [Input("User Input")]
    public InArgument<string> UserInput { get; set; }

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }



    #endregion

    #region Internal fields
    Common _objCommon;
    IOrganizationService _service;
    readonly string _ddsmProgramoffering = "ddsm_programoffering";
    ExecuteMultipleRequest requestWithResults;
    private List<Guid> processedPrograms = new List<Guid>();
    #endregion

    protected override void Execute(CodeActivityContext context)
    {
        #region "Load CRM Service from context"

        _objCommon = new Common(context);
        _service = _objCommon.GetOrgService();
        _objCommon.TracingService.Trace("Load CRM Service from context --- OK");
        #endregion

        var progrOf = ProgramOffering.Get(context);
        var progr = Program.Get(context);
        //Target Entity
        EntityReference Target = (EntityReference)_objCommon.Context.InputParameters["Target"];

        string inputPO = null;
        string inputP = null;
        string inputTargetId = null;

        _objCommon.TracingService.Trace("Iput Parameters:");
        //Target ID
        if (Target != null)
        {
            inputTargetId = Target.Id.ToString();
            _objCommon.TracingService.Trace("Iput Parameters. Target ID: " + inputTargetId);
        }
        else { _objCommon.TracingService.Trace("Target ID is null"); }
        //Program Offering
        if (progrOf != null)
        {
            inputPO = progrOf.Id.ToString();
            _objCommon.TracingService.Trace("Program Offering: " + inputPO);
        }
        else { _objCommon.TracingService.Trace("Program Offering is null"); }
        //Program
        if (progr != null)
        {
            inputP = progr.Id.ToString();
            _objCommon.TracingService.Trace("Program: " + inputP);
        }
        else { _objCommon.TracingService.Trace("Program is null"); }

        try
        {
            requestWithResults = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };


            var userInput = UserInput.Get(context);
            // resolve parameters
            var programOfferingRef = ProgramOffering.Get(context);
            var program = Program.Get(context);

            var listId = new List<Guid>();

            if (!string.IsNullOrEmpty(userInput) && programOfferingRef == null && program == null)
            {
                UserInputObj2 userInpObj = null;

                userInpObj = JsonConvert.DeserializeObject<UserInputObj2>(userInput);
                if (userInpObj?.SmartMeasures != null)
                {
                    listId = userInpObj.SmartMeasures.Where(x => !x.Equals(Guid.Empty)).Distinct().ToList();
                }
            }
            else if (programOfferingRef != null && program != null)
            {
                listId.Add(programOfferingRef.Id);
            }
            else if (programOfferingRef == null || program == null)
            {
                if (Target != null)
                {
                    listId.Add(Target.Id);
                }
            }
            else
            {
                throw new Exception("No valid input parameters for Program Offering. Please check it.");
            }

            foreach (var progOff in listId)
            {
                //get totals of program Int
                var totals = GetProgramIntervalsTotals(progOff, program);
                // get all prog offerings that must be totals updated
                //var programOffering = GetProgramOffering(programOfferingRef.Id);
                var programOffering = GetProgramOffering(progOff);
                UpdateTotals(programOffering, totals);
            }
            if (requestWithResults.Requests.Count > 0)
            {
                ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_service.Execute(requestWithResults);
                if (responseWithResults.IsFaulted)
                {
                    _objCommon.TracingService.Trace("Update of Program Offering failed! Check correct names fileds or fetch. ");
                }
                else
                {
                    _objCommon.TracingService.Trace(requestWithResults.Requests.Count + " updates was done. Program Offerings was updated.");
                }
            }
            else
            {
                _objCommon.TracingService.Trace("Program Offerings not updated. No items to update.");
            }
            //ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_service.Execute(requestWithResults);
            //_objCommon.TracingService.Trace(requestWithResults.Requests.Count + " Program Offerings updated.");

            //processCollectedData();
            Complete.Set(context, true);
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("Error of ProgramOffering totals calculation. Error:" + ex.Message);
            Complete.Set(context, false);
        }
    }

    #region Internal methods
    private void UpdateTotals(Entity offering, EntityCollection totals)
    {
        try
        {
            if (totals == null) throw new Exception("Program Interval Totals are null.");
            foreach (var total in totals.Entities)
            {
                foreach (var column in total.Attributes)
                {
                    offering[column.Key.ToLower()] = column.Value.GetRecordValue();
                }
            }

            /*Set directprogramunitcost after population of all fields Actual Spending and Actual All kWh Net Savings at Generator*/
            offering["ddsm_directprogramunitcost"] = new Money(0);
            /*
            _objCommon.TracingService.Trace("01. /ddsm_directprogramunitcost:");
            _objCommon.TracingService.Trace("01. /ddsm_directprogramunitcost:" + ((Money)offering["ddsm_directprogramunitcost"]).Value);
            //+ "/ ddsm_actualspending:" + ((Money)offering["ddsm_actualspending"]).Value.ToString()+ "/ ddsm_actualallkwhnetsavingsatgenerator" + ((Money)offering["ddsm_actualallkwhnetsavingsatgenerator"]).Value.ToString());
            _objCommon.TracingService.Trace("01. /ddsm_actualspending:");
            _objCommon.TracingService.Trace("01. /ddsm_actualspending:" + ((Money)offering["ddsm_actualspending"]).Value);
            _objCommon.TracingService.Trace("01. /ddsm_actualallkwhnetsavingsatgenerator:");
            _objCommon.TracingService.Trace("01. /ddsm_actualallkwhnetsavingsatgenerator:" + ((Decimal)offering["ddsm_actualallkwhnetsavingsatgenerator"]).ToString());*/
            decimal actualSpend;
            decimal kwhSavingatGen;
            if (offering["ddsm_actualspending"] != null)
            {
                if (offering["ddsm_actualspending"].GetType().Name == "Money")
                {
                    if (((Money)offering["ddsm_actualspending"]).Value != null && offering["ddsm_actualallkwhnetsavingsatgenerator"] != null)
                    {
                        actualSpend = ((Money)offering["ddsm_actualspending"]).Value;
                        kwhSavingatGen = (Decimal)offering["ddsm_actualallkwhnetsavingsatgenerator"];
                        offering["ddsm_directprogramunitcost"] = new Money(actualSpend / kwhSavingatGen);
                    }
                }
            }

            //_objCommon.TracingService.Trace("01. /ddsm_directprogramunitcost:" + offering["ddsm_directprogramunitcost"]);
            /*End Set directprogramunitcost after population of all fields Actual Spending and Actual All kWh Net Savings at Generator*/

            object program = null;
            if (offering.Attributes.TryGetValue("ddsm_programid", out program))
                processedPrograms.Add((program as EntityReference).Id);
            //offering.Attributes.Remove("ddsm_actualspending");

            //collect offerings for multiply creation request
            var updateRequest = new UpdateRequest { Target = offering };
            requestWithResults.Requests.Add(updateRequest);
            //_service.Update(offering);
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("Error On Program Offering totals update. Exeption:" + ex.Message);
        }
    }

    private EntityCollection GetProgramIntervalsTotals(Guid programOffering, EntityReference program)
    {
        var result = new EntityCollection();

        var programCond = (!string.IsNullOrEmpty(program?.Id.ToString())) ?
            $"<condition attribute='ddsm_programid' operator='eq' value='{program.Id.ToString()}' />"
            : "";
        /*For DEMO*/
        /*var programOffCond = (!string.IsNullOrEmpty(programOffering.ToString())) ?
            $"<condition attribute='ddsm_parentprogramofferingsid' operator='eq' value='{programOffering.ToString()}' />"
            : "";*/
        /*Normal Version*/
        var programOffCond = (!string.IsNullOrEmpty(programOffering.ToString())) ?
        $"<condition attribute='ddsm_programofferingsid' operator='eq' value='{programOffering.ToString()}' />"
        : "";

        var queryMeasureTotals =
            @"<fetch aggregate='true'>
                  <entity name='ddsm_kpiinterval'>
                    <attribute name='ddsm_forecastedkwnetsavingsatgenerator' alias='ddsm_ForecastedkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_forecastedkwhnetsavingsatgenerator' alias='ddsm_ForecastedkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_forecastedspending' alias='ddsm_ForecastedSpending' aggregate='sum' />
                    <attribute name='ddsm_forecastedgjsavingsatmeter' alias='ddsm_ForecastedGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_pnsforecastedspending' alias='ddsm_PNSForecastedSpending' aggregate='sum' />
                    <attribute name='ddsm_actualallkwhnetsavingsatgenerator' alias='ddsm_ActualAllkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_actualallkwnetsavingsatgenerator' alias='ddsm_ActualAllkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_actualspending' alias='ddsm_ActualSpending' aggregate='sum' />
                    <attribute name='ddsm_actualallgjsavingsatmeter' alias='ddsm_ActualAllGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_pnsactualspending' alias='ddsm_PNSActualSpending' aggregate='sum' />
                    <attribute name='ddsm_targetkwhnetsavingsatgenerator' alias='ddsm_TargetkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_targetkwnetsavingsatgenerator' alias='ddsm_TargetkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_budget' alias='ddsm_Budget' aggregate='sum' />
                    <attribute name='ddsm_numberofparticipants' alias='ddsm_NumberofParticipants' aggregate='sum' />
                    <attribute name='ddsm_estimatedkwhnetsavingsatgenerator' alias='ddsm_EstimatedkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_estimatedkwnetsavingsatgenerator' alias='ddsm_EstimatedkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_estimatedspending' alias='ddsm_EstimatedSpending' aggregate='sum' />
                    <attribute name='ddsm_directprogramunitcost' alias='ddsm_DirectProgramUnitCost' aggregate='sum' />
                    <attribute name='ddsm_targetgjsavingsatmeter' alias='ddsm_TargetGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_pnsbudget' alias='ddsm_PNSBudget' aggregate='sum' />
                    <attribute name='ddsm_pnsnumberofparticipants' alias='ddsm_PNSNumberofParticipants' aggregate='sum' />
                    <attribute name='ddsm_pnsestimatedspending' alias='ddsm_PNSEstimatedSpending' aggregate='sum' />
                    <attribute name='ddsm_estimatedgjsavingsatmeter' alias='ddsm_EstimatedGJSavingsatMeter' aggregate='sum' />
                    <filter>
                    <condition attribute='statecode' operator='eq' value='0' />
                    <condition attribute='statuscode' operator='eq' value='1' />
                    {0}
                    {1}                                 
                </filter>
              </entity>
            </fetch>";
        queryMeasureTotals = string.Format(queryMeasureTotals, programOffCond, programCond);
        var queryMeasureExpression = new FetchExpression(queryMeasureTotals);
        var totals = _service.RetrieveMultiple(queryMeasureExpression);

        result.Entities.Add(totals.Entities[0]);

        return result;

    }

    private Entity GetProgramOffering(Guid id)
    {
        return _service.Retrieve(_ddsmProgramoffering, id, new ColumnSet("ddsm_programofferingid", "ddsm_programid"));
    }

#if WITH_TASKQ
    private void processCollectedData()
    {
        var taskQueue = new TaskQueue(_objCommon.GetOrgService(systemCall:true));
        taskQueue.Create(new DDSM_Task(TaskEntity.Program)
        {
            ProcessedItems0 = processedPrograms.Select(x => x).Distinct().ToList()
        }, TaskEntity.Program
        );
    }
#endif

    #endregion Internal methods
}

