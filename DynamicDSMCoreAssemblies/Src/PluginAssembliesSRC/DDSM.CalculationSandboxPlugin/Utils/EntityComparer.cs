﻿using Microsoft.Xrm.Sdk;
using System.Collections.Generic;

namespace DDSM.CalculationSandbox.Utils
{
    public class EntityComparer : IEqualityComparer<Entity>
    {
        public bool Equals(Entity x, Entity y)
        {
            return x.Id.Equals(y.Id);
        }

        public int GetHashCode(Entity obj)
        {
            unchecked  // overflow is fine
            {
                int hash = 17;
                hash = hash * 23 + (obj.Id.ToString() ?? "").GetHashCode();
               // hash = hash * 23 + (obj.empPL ?? "").GetHashCode();
               // hash = hash * 23 + (obj.empShift ?? "").GetHashCode();
                return hash;
            }
        }
    }
}
