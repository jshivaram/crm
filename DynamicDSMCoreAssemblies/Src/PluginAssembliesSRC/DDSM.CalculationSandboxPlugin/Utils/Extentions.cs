﻿using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

namespace DDSM.CalculationSandbox.Utils
{
    public static class Extentions
    {      
        public static string GetFirterInValues(this object recordValue)
        {
            var result = "";
            var s = recordValue as string;
            if (s != null)
                result = s.Split(',').Where(item => !string.IsNullOrEmpty(item)).Aggregate(result, (current, item) => current + $"<value>{item}</value>");
            return result;
        }

        public static object GetRecordValue(this AttributeCollection records, string key)
        {
            if (records.Keys.Contains(key))
            {
                var record = records[key];

                return (record as AliasedValue)?.Value;
            }
            else return null;
        }

        public static object GetRecordValue(this object record)
        {
            if ((record as AliasedValue)?.Value != null)
            {
                return ((AliasedValue)record).Value;
            }
            else if (record != null && !(record is AliasedValue))
                return record;
            else return null;
        }

        /// <summary>
        /// This function is used to retrieve the optionset labek using the optionset value
        /// </summary>
        /// <param name="service"></param>
        /// <param name="entityName"></param>
        /// <param name="attributeName"></param>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        public static string GetOptionsSetTextForValue(IOrganizationService service, string entityName, string attributeName, int selectedValue)
        {
            var retrieveAttributeRequest = new
            RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = attributeName,
                RetrieveAsIfPublished = true
            };
            // Execute the request.
            var retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
            // Access the retrieved attribute.
            var retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)
            retrieveAttributeResponse.AttributeMetadata;// Get the current options list for the retrieved attribute.
            var optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
            string selectedOptionLabel = null;
            foreach (var oMD in optionList)
            {
                if (oMD.Value == selectedValue)
                {
                    selectedOptionLabel = oMD.Label.LocalizedLabels[0].Label;
                    break;
                }
            }
            return selectedOptionLabel;
        }
        
    }
}
