﻿using System;
using System.Collections.Generic;

namespace DDSM.CalculationSandbox.Model
{
    public class UserInputObj
    {
        public string AccountID { get; set; }
        public string ParentsiteID { get; set; }
        public string Tradeally { get; set; }
        public string ProjectID { get; set; }
        public string ProjectName { get; set; }
        public string Projectstatus { get; set; }
        public string Phase { get; set; }
        public int? Phasenumber { get; set; }
        public DateTime Initialphasedate { get; set; }
        public string MeasureID { get; set; }
        public string MeasureTplID { get; set; }
        public string CalculationType { get; set; }
        public string EspSmartMeasureID { get; set; }
        public Dictionary<string, string> DataFields { get; set; }
    }

    /// <summary>
    /// Object for Recalulate SM from View
    /// </summary>
    public class UserInputObj2
    {      
        public List<Guid> SmartMeasures { get; set; }
        public Dictionary<string, string> DataFields { get; set; }
    }
}