﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Metadata.Query;
using Microsoft.Xrm.Sdk.Query;
using System.Globalization;
using Microsoft.Xrm.Sdk.Workflow;

namespace DDSM.CommonProvider
{
    public class MoskTraceService : ITracingService
    {
        public MoskTraceService()
        {
            Logs = new List<string>();
        }

        public List<string> Logs { get; set; }
        public void Trace(string format, params object[] args)
        {
            Logs.Add(string.Format((IFormatProvider)CultureInfo.InvariantCulture, format, args));
        }
    }
    public class Common
    {
        public ITracingService TracingService;

        public IWorkflowContext Context;
        public IOrganizationServiceFactory ServiceFactory;
        private IOrganizationService Service;
        IOrganizationService SystemService;
        public CodeActivityContext ExecutionContext; 

        /// <summary>
        ///  Return OrgService
        /// </summary>
        /// <param name="systemCall"></param>
        /// <returns></returns>
        public IOrganizationService GetOrgService(bool systemCall = false)
        {
            if (systemCall != true)
            {
                return Service ?? (Service = ServiceFactory.CreateOrganizationService(Context.UserId));
            }
            else
            {
                return SystemService ??
                       (SystemService = ServiceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", GetOrgService())));
            }
        }

        public Common(CodeActivityContext executionContext)
        {
            TracingService = new MoskTraceService(); //executionContext.GetExtension<ITracingService>();
            Context = executionContext.GetExtension<IWorkflowContext>();
            ServiceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            ExecutionContext = executionContext;

        }

        /// <summary>
        /// Query the Metadata to get the Entity Schema Name from the Object Type Code
        /// </summary>
        /// <param name="objectTypeCode"></param>
        /// <param name="service"></param>
        /// <returns>Entity Schema Name</returns>
        public string GetEntityNameFromCode(string objectTypeCode, IOrganizationService service)
        {
            var entityFilter = new MetadataFilterExpression(LogicalOperator.And);
            entityFilter.Conditions.Add(new MetadataConditionExpression("ObjectTypeCode", MetadataConditionOperator.Equals, Convert.ToInt32(objectTypeCode)));
            var entityQueryExpression = new EntityQueryExpression
            {
                Criteria = entityFilter
            };
            var retrieveMetadataChangesRequest = new RetrieveMetadataChangesRequest
            {
                Query = entityQueryExpression,
                ClientVersionStamp = null
            };
            var response = (RetrieveMetadataChangesResponse)service.Execute(retrieveMetadataChangesRequest);

            var entityMetadata = response.EntityMetadata[0];
            return entityMetadata.SchemaName.ToLower();
        }

        public List<string> GetEntityAttributesToClone(string entityName, IOrganizationService service)
        {
            var req = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Attributes,
                LogicalName = entityName
            };

            var res = (RetrieveEntityResponse)service.Execute(req);
            var atts = (from attMetadata in res.EntityMetadata.Attributes where attMetadata.IsPrimaryId != null && (attMetadata.IsValidForCreate != null && (attMetadata.IsValidForCreate.Value && !attMetadata.IsPrimaryId.Value)) select attMetadata.LogicalName).ToList();

            return (atts);
        }


        public Guid GetSystemUserId(string name, IOrganizationService service)
        {
            QueryByAttribute queryUsers = new QueryByAttribute
            {
                EntityName = "systemuser",
                ColumnSet = new ColumnSet("systemuserid")
            };

            queryUsers.AddAttributeValue("fullname", name);
            EntityCollection retrievedUsers = service.RetrieveMultiple(queryUsers);
            Guid systemUserId = ((Entity)retrievedUsers.Entities[0]).Id;

            return systemUserId;
        }
    }
}
