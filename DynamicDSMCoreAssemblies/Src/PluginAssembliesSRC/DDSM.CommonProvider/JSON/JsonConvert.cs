﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DDSM.CommonProvider.JSON
{
    public static class JsonConvert
    {
        public static string SerializeObject(Object target)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(target);
        }

        public static string SerializeObject(Object target, Formatting formatting)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(target, formatting);
        }

        public static T DeserializeObject<T>(string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }
    }
}
