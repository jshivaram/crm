﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.CommonProvider.Model
{
    public class EditResponse
    {
        public int MNCount { get; set; }
        public int SKUCount { get; set; }
        public int MNACount { get; set; }
        public int SKUACount { get; set; }
        public bool IsSuccessfull { get; set; }
    }

    public class EditRequest
    {
        public bool MakeInactiveMN { get; set; }

        public bool MakeInactiveSKU { get; set; }

        public bool MakeInactiveMNA { get; set; }

        public bool MakeInactiveSKUA { get; set; }

        public DateTime? EndDateMN { get; set; }

        public DateTime? EndDateSKU { get; set; }

        public DateTime? EndDateMNA { get; set; }

        public DateTime? EndDateSKUA { get; set; }

        [JsonConverter(typeof(FakeGuidValueProvider))]
        public Guid? MNAonlyForProgramOfferingId { get; set; }

        [JsonConverter(typeof(FakeGuidValueProvider))]
        public Guid? SKUAonlyForProgramOfferingId { get; set; }

        public List<Guid> ModelIds { get; set; }
    }
}
