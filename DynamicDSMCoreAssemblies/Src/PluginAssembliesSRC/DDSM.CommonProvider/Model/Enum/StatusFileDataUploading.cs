namespace DDSM.CommonProvider.Model.Enum
{
    public enum StatusFileDataUploading
    {
        notUploaded = 962080000,
        fileUploadCompleted = 962080001,
        fileUploadFailed = 962080002,
        parsingFile = 962080003,
        parsingFileCompleted = 962080004,
        parsingFileFailed = 962080005,
        contactsProcessing = 962080006,
        accountsProcessing = 962080007,
        sitesProcessing = 962080008,
        projectGroupsProcessing = 962080009,
        projectsProcessing = 962080010,
        measuresProcessing = 962080011,
        projectReportingsProcessing = 962080012,
        measureReportingsProcessing = 962080013,
        relationshipsProcessing = 962080014,
        recordsUploadCompleted = 962080015,
        recalculationStarted = 962080016,
        ESPRecalculation = 962080017,
        recalculationBusinessRules = 962080018,
        rollupsRecalculation = 962080019,
        intervalsRecalculation = 962080020,
        importSuccessfully = 962080021,
        importFailed = 962080022,
        recordsUploadFailed = 962080023
    }
}