namespace DDSM.CommonProvider.Model.Enum
{
    public enum TaskEntity
    {
        Measure = 962080000,
        Project = 962080001,
        ProgramInterval = 962080002,
        ProgramOffering = 962080003,
        Portfolio = 962080004,
        Financial = 962080005,
        ProjectGroupFinancial = 962080006,
        Program = 962080007,
        DsmPlan = 962080008,
        ProjectGroupNotification = 962080010,
    }
}