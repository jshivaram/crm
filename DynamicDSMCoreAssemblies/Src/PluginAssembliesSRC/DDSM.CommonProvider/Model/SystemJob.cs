//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
// Created via this command line: "C:\Users\Administrator\Desktop\XrmToolBoxv.1.2016.6.29_with_conn\XrmToolBoxv.1.2016.6.29\Plugins\CrmSvcUtil Ref\crmsvcutil.exe" /namespace:"CrmEarlyBound" /out:"C:\Users\Administrator\Desktop\XrmToolBoxv.1.2016.6.29_with_conn\XrmToolBoxv.1.2016.6.29\Plugins\CrmSvcUtil Ref\SystemJob.cs" /servicecontextname:"CrmServiceContext" /codecustomization:"DLaB.CrmSvcUtilExtensions.Entity.CustomizeCodeDomService,DLaB.CrmSvcUtilExtensions" /codegenerationservice:"DLaB.CrmSvcUtilExtensions.Entity.CustomCodeGenerationService,DLaB.CrmSvcUtilExtensions" /codewriterfilter:"DLaB.CrmSvcUtilExtensions.Entity.CodeWriterFilterService,DLaB.CrmSvcUtilExtensions" /namingservice:"DLaB.CrmSvcUtilExtensions.NamingService,DLaB.CrmSvcUtilExtensions" /metadataproviderservice:"DLaB.CrmSvcUtilExtensions.Entity.MetadataProviderService,DLaB.CrmSvcUtilExtensions" /connectionstring:"Url=http://crm-server/RLS090916;Domain=crm.srv;UserName=Administrator;Password=************" 
//------------------------------------------------------------------------------

[assembly: Microsoft.Xrm.Sdk.Client.ProxyTypesAssemblyAttribute()]

namespace DDSM.CommonProvider.Model
{
	
	
	[System.Runtime.Serialization.DataContractAttribute()]
	[System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "8.0.1.7297")]
	public enum AsyncOperationState
	{
		
		[System.Runtime.Serialization.EnumMemberAttribute()]
		Ready = 0,
		
		[System.Runtime.Serialization.EnumMemberAttribute()]
		Suspended = 1,
		
		[System.Runtime.Serialization.EnumMemberAttribute()]
		Locked = 2,
		
		[System.Runtime.Serialization.EnumMemberAttribute()]
		Completed = 3,
	}

    [System.Runtime.Serialization.DataContractAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "8.0.1.7297")]
    public enum AsyncOperationStatus
    {
        Canceled =32,
        Canceling = 22,
        Failed= 31,
        InProgress =20,
        Pausing = 21,
        Succeeded= 30,
        Waiting= 10,
        WaitingForResources =0,
    }
	
	/// <summary>
	/// Process whose execution can proceed independently or in the background.
	/// </summary>
	[System.Runtime.Serialization.DataContractAttribute()]
	[Microsoft.Xrm.Sdk.Client.EntityLogicalNameAttribute("asyncoperation")]
	[System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "8.0.1.7297")]
	public partial class AsyncOperation : Microsoft.Xrm.Sdk.Entity, System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		public struct Fields
		{
			public const string AsyncOperationId = "asyncoperationid";
			public const string Id = "asyncoperationid";
			public const string CompletedOn = "completedon";
			public const string CorrelationId = "correlationid";
			public const string CorrelationUpdatedTime = "correlationupdatedtime";
			public const string CreatedBy = "createdby";
			public const string CreatedOn = "createdon";
			public const string CreatedOnBehalfBy = "createdonbehalfby";
			public const string Data = "data";
			public const string DependencyToken = "dependencytoken";
			public const string Depth = "depth";
			public const string ErrorCode = "errorcode";
			public const string ExecutionTimeSpan = "executiontimespan";
			public const string FriendlyMessage = "friendlymessage";
			public const string HostId = "hostid";
			public const string IsWaitingForEvent = "iswaitingforevent";
			public const string Message = "message";
			public const string MessageName = "messagename";
			public const string ModifiedBy = "modifiedby";
			public const string ModifiedOn = "modifiedon";
			public const string ModifiedOnBehalfBy = "modifiedonbehalfby";
			public const string Name = "name";
			public const string OperationType = "operationtype";
			public const string OwnerId = "ownerid";
			public const string OwningBusinessUnit = "owningbusinessunit";
			public const string OwningExtensionId = "owningextensionid";
			public const string OwningTeam = "owningteam";
			public const string OwningUser = "owninguser";
			public const string ParentPluginExecutionId = "parentpluginexecutionid";
			public const string PostponeUntil = "postponeuntil";
			public const string PrimaryEntityType = "primaryentitytype";
			public const string RecurrencePattern = "recurrencepattern";
			public const string RecurrenceStartTime = "recurrencestarttime";
			public const string RegardingObjectId = "regardingobjectid";
			public const string RequestId = "requestid";
			public const string RetryCount = "retrycount";
			public const string Sequence = "sequence";
			public const string StartedOn = "startedon";
			public const string StateCode = "statecode";
			public const string StatusCode = "statuscode";
			public const string Subtype = "subtype";
			public const string TimeZoneRuleVersionNumber = "timezoneruleversionnumber";
			public const string UTCConversionTimeZoneCode = "utcconversiontimezonecode";
			public const string WorkflowActivationId = "workflowactivationid";
			public const string WorkflowStageName = "workflowstagename";
		}

		
		/// <summary>
		/// Default Constructor.
		/// </summary>
		public AsyncOperation() : 
				base(EntityLogicalName)
		{
		}
		
		public const string EntityLogicalName = "asyncoperation";
		
		public const int EntityTypeCode = 4700;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		private void OnPropertyChanged(string propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void OnPropertyChanging(string propertyName)
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, new System.ComponentModel.PropertyChangingEventArgs(propertyName));
			}
		}
		
		/// <summary>
		/// Unique identifier of the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("asyncoperationid")]
		public System.Nullable<System.Guid> AsyncOperationId
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<System.Guid>>("asyncoperationid");
			}
			set
			{
				this.OnPropertyChanging("AsyncOperationId");
				this.SetAttributeValue("asyncoperationid", value);
				if (value.HasValue)
				{
					base.Id = value.Value;
				}
				else
				{
					base.Id = System.Guid.Empty;
				}
				this.OnPropertyChanged("AsyncOperationId");
			}
		}
		
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("asyncoperationid")]
		public override System.Guid Id
		{
			get
			{
				return base.Id;
			}
			set
			{
				this.AsyncOperationId = value;
			}
		}
		
		/// <summary>
		/// Date and time when the system job was completed.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("completedon")]
		public System.Nullable<System.DateTime> CompletedOn
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<System.DateTime>>("completedon");
			}
		}
		
		/// <summary>
		/// Unique identifier used to correlate between multiple SDK requests and system jobs.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("correlationid")]
		public System.Nullable<System.Guid> CorrelationId
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<System.Guid>>("correlationid");
			}
			set
			{
				this.OnPropertyChanging("CorrelationId");
				this.SetAttributeValue("correlationid", value);
				this.OnPropertyChanged("CorrelationId");
			}
		}
		
		/// <summary>
		/// Last time the correlation depth was updated.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("correlationupdatedtime")]
		public System.Nullable<System.DateTime> CorrelationUpdatedTime
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<System.DateTime>>("correlationupdatedtime");
			}
			set
			{
				this.OnPropertyChanging("CorrelationUpdatedTime");
				this.SetAttributeValue("correlationupdatedtime", value);
				this.OnPropertyChanged("CorrelationUpdatedTime");
			}
		}
		
		/// <summary>
		/// Unique identifier of the user who created the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdby")]
		public Microsoft.Xrm.Sdk.EntityReference CreatedBy
		{
			get
			{
				return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("createdby");
			}
		}
		
		/// <summary>
		/// Date and time when the system job was created.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdon")]
		public System.Nullable<System.DateTime> CreatedOn
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<System.DateTime>>("createdon");
			}
		}
		
		/// <summary>
		/// Unique identifier of the delegate user who created the asyncoperation.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdonbehalfby")]
		public Microsoft.Xrm.Sdk.EntityReference CreatedOnBehalfBy
		{
			get
			{
				return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("createdonbehalfby");
			}
			set
			{
				this.OnPropertyChanging("CreatedOnBehalfBy");
				this.SetAttributeValue("createdonbehalfby", value);
				this.OnPropertyChanged("CreatedOnBehalfBy");
			}
		}
		
		/// <summary>
		/// Unstructured data associated with the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("data")]
		public string Data
		{
			get
			{
				return this.GetAttributeValue<string>("data");
			}
			set
			{
				this.OnPropertyChanging("Data");
				this.SetAttributeValue("data", value);
				this.OnPropertyChanged("Data");
			}
		}
		
		/// <summary>
		/// Execution of all operations with the same dependency token is serialized.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("dependencytoken")]
		public string DependencyToken
		{
			get
			{
				return this.GetAttributeValue<string>("dependencytoken");
			}
			set
			{
				this.OnPropertyChanging("DependencyToken");
				this.SetAttributeValue("dependencytoken", value);
				this.OnPropertyChanged("DependencyToken");
			}
		}
		
		/// <summary>
		/// Number of SDK calls made since the first call.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("depth")]
		public System.Nullable<int> Depth
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<int>>("depth");
			}
			set
			{
				this.OnPropertyChanging("Depth");
				this.SetAttributeValue("depth", value);
				this.OnPropertyChanged("Depth");
			}
		}
		
		/// <summary>
		/// Error code returned from a canceled system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("errorcode")]
		public System.Nullable<int> ErrorCode
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<int>>("errorcode");
			}
		}
		
		/// <summary>
		/// Time that the system job has taken to execute.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("executiontimespan")]
		public System.Nullable<double> ExecutionTimeSpan
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<double>>("executiontimespan");
			}
		}
		
		/// <summary>
		/// Message provided by the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("friendlymessage")]
		public string FriendlyMessage
		{
			get
			{
				return this.GetAttributeValue<string>("friendlymessage");
			}
			set
			{
				this.OnPropertyChanging("FriendlyMessage");
				this.SetAttributeValue("friendlymessage", value);
				this.OnPropertyChanged("FriendlyMessage");
			}
		}
		
		/// <summary>
		/// Unique identifier of the host that owns this system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("hostid")]
		public string HostId
		{
			get
			{
				return this.GetAttributeValue<string>("hostid");
			}
			set
			{
				this.OnPropertyChanging("HostId");
				this.SetAttributeValue("hostid", value);
				this.OnPropertyChanged("HostId");
			}
		}
		
		/// <summary>
		/// Indicates that the system job is waiting for an event.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("iswaitingforevent")]
		public System.Nullable<bool> IsWaitingForEvent
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<bool>>("iswaitingforevent");
			}
		}
		
		/// <summary>
		/// Message related to the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("message")]
		public string Message
		{
			get
			{
				return this.GetAttributeValue<string>("message");
			}
		}
		
		/// <summary>
		/// Name of the message that started this system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("messagename")]
		public string MessageName
		{
			get
			{
				return this.GetAttributeValue<string>("messagename");
			}
			set
			{
				this.OnPropertyChanging("MessageName");
				this.SetAttributeValue("messagename", value);
				this.OnPropertyChanged("MessageName");
			}
		}
		
		/// <summary>
		/// Unique identifier of the user who last modified the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedby")]
		public Microsoft.Xrm.Sdk.EntityReference ModifiedBy
		{
			get
			{
				return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("modifiedby");
			}
		}
		
		/// <summary>
		/// Date and time when the system job was last modified.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedon")]
		public System.Nullable<System.DateTime> ModifiedOn
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<System.DateTime>>("modifiedon");
			}
		}
		
		/// <summary>
		/// Unique identifier of the delegate user who last modified the asyncoperation.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedonbehalfby")]
		public Microsoft.Xrm.Sdk.EntityReference ModifiedOnBehalfBy
		{
			get
			{
				return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("modifiedonbehalfby");
			}
			set
			{
				this.OnPropertyChanging("ModifiedOnBehalfBy");
				this.SetAttributeValue("modifiedonbehalfby", value);
				this.OnPropertyChanged("ModifiedOnBehalfBy");
			}
		}
		
		/// <summary>
		/// Name of the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("name")]
		public string Name
		{
			get
			{
				return this.GetAttributeValue<string>("name");
			}
			set
			{
				this.OnPropertyChanging("Name");
				this.SetAttributeValue("name", value);
				this.OnPropertyChanged("Name");
			}
		}
		
		/// <summary>
		/// Type of the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("operationtype")]
		public Microsoft.Xrm.Sdk.OptionSetValue OperationType
		{
			get
			{
				return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("operationtype");
			}
			set
			{
				this.OnPropertyChanging("OperationType");
				this.SetAttributeValue("operationtype", value);
				this.OnPropertyChanged("OperationType");
			}
		}
		
		/// <summary>
		/// Unique identifier of the user or team who owns the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ownerid")]
		public Microsoft.Xrm.Sdk.EntityReference OwnerId
		{
			get
			{
				return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ownerid");
			}
			set
			{
				this.OnPropertyChanging("OwnerId");
				this.SetAttributeValue("ownerid", value);
				this.OnPropertyChanged("OwnerId");
			}
		}
		
		/// <summary>
		/// Unique identifier of the business unit that owns the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owningbusinessunit")]
		public Microsoft.Xrm.Sdk.EntityReference OwningBusinessUnit
		{
			get
			{
				return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("owningbusinessunit");
			}
		}
		
		/// <summary>
		/// Unique identifier of the owning extension with which the system job is associated.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owningextensionid")]
		public Microsoft.Xrm.Sdk.EntityReference OwningExtensionId
		{
			get
			{
				return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("owningextensionid");
			}
			set
			{
				this.OnPropertyChanging("OwningExtensionId");
				this.SetAttributeValue("owningextensionid", value);
				this.OnPropertyChanged("OwningExtensionId");
			}
		}
		
		/// <summary>
		/// Unique identifier of the team who owns the record.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owningteam")]
		public Microsoft.Xrm.Sdk.EntityReference OwningTeam
		{
			get
			{
				return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("owningteam");
			}
		}
		
		/// <summary>
		/// Unique identifier of the user who owns the record.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owninguser")]
		public Microsoft.Xrm.Sdk.EntityReference OwningUser
		{
			get
			{
				return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("owninguser");
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("parentpluginexecutionid")]
		public System.Nullable<System.Guid> ParentPluginExecutionId
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<System.Guid>>("parentpluginexecutionid");
			}
			set
			{
				this.OnPropertyChanging("ParentPluginExecutionId");
				this.SetAttributeValue("parentpluginexecutionid", value);
				this.OnPropertyChanged("ParentPluginExecutionId");
			}
		}
		
		/// <summary>
		/// Indicates whether the system job should run only after the specified date and time.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("postponeuntil")]
		public System.Nullable<System.DateTime> PostponeUntil
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<System.DateTime>>("postponeuntil");
			}
			set
			{
				this.OnPropertyChanging("PostponeUntil");
				this.SetAttributeValue("postponeuntil", value);
				this.OnPropertyChanged("PostponeUntil");
			}
		}
		
		/// <summary>
		/// Type of entity with which the system job is primarily associated.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("primaryentitytype")]
		public string PrimaryEntityType
		{
			get
			{
				return this.GetAttributeValue<string>("primaryentitytype");
			}
			set
			{
				this.OnPropertyChanging("PrimaryEntityType");
				this.SetAttributeValue("primaryentitytype", value);
				this.OnPropertyChanged("PrimaryEntityType");
			}
		}
		
		/// <summary>
		/// Pattern of the system job's recurrence.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("recurrencepattern")]
		public string RecurrencePattern
		{
			get
			{
				return this.GetAttributeValue<string>("recurrencepattern");
			}
			set
			{
				this.OnPropertyChanging("RecurrencePattern");
				this.SetAttributeValue("recurrencepattern", value);
				this.OnPropertyChanged("RecurrencePattern");
			}
		}
		
		/// <summary>
		/// Starting time in UTC for the recurrence pattern.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("recurrencestarttime")]
		public System.Nullable<System.DateTime> RecurrenceStartTime
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<System.DateTime>>("recurrencestarttime");
			}
			set
			{
				this.OnPropertyChanging("RecurrenceStartTime");
				this.SetAttributeValue("recurrencestarttime", value);
				this.OnPropertyChanged("RecurrenceStartTime");
			}
		}
		
		/// <summary>
		/// Unique identifier of the object with which the system job is associated.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("regardingobjectid")]
		public Microsoft.Xrm.Sdk.EntityReference RegardingObjectId
		{
			get
			{
				return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("regardingobjectid");
			}
			set
			{
				this.OnPropertyChanging("RegardingObjectId");
				this.SetAttributeValue("regardingobjectid", value);
				this.OnPropertyChanged("RegardingObjectId");
			}
		}
		
		/// <summary>
		/// Unique identifier of the request that generated the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("requestid")]
		public System.Nullable<System.Guid> RequestId
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<System.Guid>>("requestid");
			}
			set
			{
				this.OnPropertyChanging("RequestId");
				this.SetAttributeValue("requestid", value);
				this.OnPropertyChanged("RequestId");
			}
		}
		
		/// <summary>
		/// Number of times to retry the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("retrycount")]
		public System.Nullable<int> RetryCount
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<int>>("retrycount");
			}
		}
		
		/// <summary>
		/// Order in which operations were submitted.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("sequence")]
		public System.Nullable<long> Sequence
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<long>>("sequence");
			}
		}
		
		/// <summary>
		/// Date and time when the system job was started.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("startedon")]
		public System.Nullable<System.DateTime> StartedOn
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<System.DateTime>>("startedon");
			}
		}
		
		/// <summary>
		/// Status of the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("statecode")]
		public System.Nullable<DDSM.CommonProvider.Model.AsyncOperationState> StateCode
		{
			get
			{
				Microsoft.Xrm.Sdk.OptionSetValue optionSet = this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("statecode");
				if ((optionSet != null))
				{
					return ((DDSM.CommonProvider.Model.AsyncOperationState)(System.Enum.ToObject(typeof(DDSM.CommonProvider.Model.AsyncOperationState), optionSet.Value)));
				}
				else
				{
					return null;
				}
			}
			set
			{
				this.OnPropertyChanging("StateCode");
				if ((value == null))
				{
					this.SetAttributeValue("statecode", null);
				}
				else
				{
					this.SetAttributeValue("statecode", new Microsoft.Xrm.Sdk.OptionSetValue(((int)(value))));
				}
				this.OnPropertyChanged("StateCode");
			}
		}
		
		/// <summary>
		/// Reason for the status of the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("statuscode")]
		public Microsoft.Xrm.Sdk.OptionSetValue StatusCode
		{
			get
			{
				return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("statuscode");
			}
			set
			{
				this.OnPropertyChanging("StatusCode");
				this.SetAttributeValue("statuscode", value);
				this.OnPropertyChanged("StatusCode");
			}
		}
		
		/// <summary>
		/// The Subtype of the Async Job
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("subtype")]
		public System.Nullable<int> Subtype
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<int>>("subtype");
			}
		}
		
		/// <summary>
		/// For internal use only.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("timezoneruleversionnumber")]
		public System.Nullable<int> TimeZoneRuleVersionNumber
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<int>>("timezoneruleversionnumber");
			}
			set
			{
				this.OnPropertyChanging("TimeZoneRuleVersionNumber");
				this.SetAttributeValue("timezoneruleversionnumber", value);
				this.OnPropertyChanged("TimeZoneRuleVersionNumber");
			}
		}
		
		/// <summary>
		/// Time zone code that was in use when the record was created.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("utcconversiontimezonecode")]
		public System.Nullable<int> UTCConversionTimeZoneCode
		{
			get
			{
				return this.GetAttributeValue<System.Nullable<int>>("utcconversiontimezonecode");
			}
			set
			{
				this.OnPropertyChanging("UTCConversionTimeZoneCode");
				this.SetAttributeValue("utcconversiontimezonecode", value);
				this.OnPropertyChanged("UTCConversionTimeZoneCode");
			}
		}
		
		/// <summary>
		/// Unique identifier of the workflow activation related to the system job.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("workflowactivationid")]
		public Microsoft.Xrm.Sdk.EntityReference WorkflowActivationId
		{
			get
			{
				return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("workflowactivationid");
			}
			set
			{
				this.OnPropertyChanging("WorkflowActivationId");
				this.SetAttributeValue("workflowactivationid", value);
				this.OnPropertyChanged("WorkflowActivationId");
			}
		}
		
		/// <summary>
		/// Name of a workflow stage.
		/// </summary>
		[Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("workflowstagename")]
		public string WorkflowStageName
		{
			get
			{
				return this.GetAttributeValue<string>("workflowstagename");
			}
		}
	}
	
	/// <summary>
	/// Represents a source of entities bound to a CRM service. It tracks and manages changes made to the retrieved entities.
	/// </summary>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "8.0.1.7297")]
	public partial class CrmServiceContext : Microsoft.Xrm.Sdk.Client.OrganizationServiceContext
	{
		
		/// <summary>
		/// Constructor.
		/// </summary>
		public CrmServiceContext(Microsoft.Xrm.Sdk.IOrganizationService service) : 
				base(service)
		{
		}
		
		/// <summary>
		/// Gets a binding to the set of all <see cref="DDSM.CommonProvider.Model.AsyncOperation"/> entities.
		/// </summary>
		public System.Linq.IQueryable<DDSM.CommonProvider.Model.AsyncOperation> AsyncOperationSet
		{
			get
			{
				return this.CreateQuery<DDSM.CommonProvider.Model.AsyncOperation>();
			}
		}
	}
}
