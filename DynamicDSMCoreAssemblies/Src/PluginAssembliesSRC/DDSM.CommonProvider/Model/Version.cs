﻿using System;
using Microsoft.Xrm.Sdk;

namespace DDSM.CommonProvider.Model
{
    public class Version
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Entity Record { get; set; }

        public Version(Entity record)
        {
            if (record != null)
            {
                StartDate = record.GetAttributeValue<DateTime>("ddsm_startdatetime");
                Record = record;
            }
        }
    }

}
