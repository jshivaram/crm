﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.CommonProvider.Utils
{
    // new field in PGF that provide status of parent PG recalculating
    public enum CalculationStatus
    {
        RecalculationInProgress = 962080000,
        RecalculationCompleted = 962080001
    }

    public enum CalculationRecordStatus
    {
        NotReady = 962080000,
        ReadyToCalculation = 962080001
    };
}
