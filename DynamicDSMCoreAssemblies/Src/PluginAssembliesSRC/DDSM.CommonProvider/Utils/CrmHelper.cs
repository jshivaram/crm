﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text;
using DDSM.CommonProvider.Model;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DDSM.CommonProvider.Utils
{
    public static class CrmHelper
    {
        public static EntityReference GetTargetData(Common objDdsm)
        {
            EntityReference target = null;
            try
            {
                //  Contract.Ensures(Contract.Result<EntityReference>() != null);


                if (objDdsm.Context.InputParameters["Target"] is EntityReference)
                {
                    target = new EntityReference(((EntityReference)objDdsm.Context.InputParameters["Target"]).LogicalName, ((EntityReference)objDdsm.Context.InputParameters["Target"]).Id);
                }
                else if (objDdsm.Context.InputParameters["Target"] is Entity)
                {
                    target = new EntityReference(((Entity)objDdsm.Context.InputParameters["Target"]).LogicalName, ((Entity)objDdsm.Context.InputParameters["Target"]).Id);
                }
            }
            catch (Exception)
            {

                objDdsm.TracingService.Trace("Can't get target from Context");
            }


            return target;
        }

        public static Entity getDataRelationMapping(IOrganizationService _orgService, string targetEntity, string sourceEntity, Guid sourceGuid)
        {
            InitializeFromRequest initialize = new InitializeFromRequest();
            initialize.TargetEntityName = targetEntity;
            initialize.EntityMoniker = new EntityReference(sourceEntity, sourceGuid);
            initialize.TargetFieldType = TargetFieldType.All;
            InitializeFromResponse initialized = (InitializeFromResponse)_orgService.Execute(initialize);
            return initialized.Entity;
        }


        //public static EntityReference GetTargetData(Common objDdsm)
        //{
        //    EntityReference target = null;
        //    try
        //    {
        //        Contract.Ensures(Contract.Result<EntityReference>() != null);


        //        if (objDdsm.Context.InputParameters["Target"] is EntityReference)
        //        {
        //            target = new EntityReference(((EntityReference)objDdsm.Context.InputParameters["Target"]).LogicalName, ((EntityReference)objDdsm.Context.InputParameters["Target"]).Id);
        //        }
        //        else if (objDdsm.Context.InputParameters["Target"] is Entity)
        //        {
        //            target = new EntityReference(((Entity)objDdsm.Context.InputParameters["Target"]).LogicalName, ((Entity)objDdsm.Context.InputParameters["Target"]).Id);
        //        }
        //    }
        //    catch (Exception)
        //    {

        //        objDdsm.TracingService.Trace("Can't get target from Context");
        //    }


        //    return target;
        //}
    }
}
