﻿using System;
using Microsoft.Xrm.Sdk;

namespace DDSM.CommonProvider.Utils
{
    public static class Extentions
    {
        public static Object GetRecordValue(this Object record)
        {

            if (record is AliasedValue && ((AliasedValue)record).Value != null)
            {
                return ((AliasedValue)record).Value;
            }
            if (record != null && record is Money)
            {
                return ((Money)record).Value;
            }
            if (record != null && !(record is AliasedValue))
                return record;
            return null;
        }

        public static EntityReference GetTargetData(Common objDdsm)
        {
            objDdsm.TracingService.Trace("in GetTargetData:");
            EntityReference target = new EntityReference();
            if (objDdsm.Context.InputParameters.ContainsKey("Target"))
            {
                objDdsm.TracingService.Trace("objDdsm.Context.InputParameters.ContainsKey(Target)");
                objDdsm.TracingService.Trace("in GetTargetData:");
                if (objDdsm.Context.InputParameters["Target"] is Entity)
                {
                    target = ((Entity)objDdsm.Context.InputParameters["Target"]).ToEntityReference();
                }
                else if (objDdsm.Context.InputParameters["Target"] is EntityReference)
                {
                    target = (EntityReference)objDdsm.Context.InputParameters["Target"];
                }
            }
            //else if (objDdsm.Context.ParentContext.InputParameters.ContainsKey("Target"))
            //{
            //    objDdsm.TracingService.Trace("NOT objDdsm.Context.InputParameters.ContainsKey(Target)");

            //    if (objDdsm.Context.ParentContext.InputParameters["Target"] is Entity)
            //    {
            //        target = ((Entity)objDdsm.Context.ParentContext.InputParameters["Target"]).ToEntityReference();
            //    }
            //    else if (objDdsm.Context.ParentContext.InputParameters["Target"] is EntityReference)
            //    {
            //        target = (EntityReference)objDdsm.Context.ParentContext.InputParameters["Target"];
            //    }
            //}

            objDdsm.TracingService.Trace("Target LogicalName: " + target.LogicalName + " Id: " + target.Id.ToString());
            // throw new Exception("FAKE Exception::::: " + string.Join(",", ((MoskTraceService)objDdsm.TracingService).Logs));
            return target;
        }


    }
}
