﻿using System;
using System.Collections.Generic;
using System.Text;
using DDSM.CommonProvider.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DDSM.CommonProvider.Utils
{
    public class SettingsProvider
    {
        private SettingsProvider()
        {
            _config = new JObject();
        }

        static readonly string confFileName = "accentgold_/settings/config.js";
        static readonly string mainEntity = "webresource";
        static readonly string jsonStart = "var configJSON = '";
        static readonly string endJson = "';";

        private static JObject _config;
        private static Guid ConfId;

        public static T GetConfig<T>(IOrganizationService orgService, Type confType) where T : IModuleConfig
        {
            //var result = new Dictionary<string, object>();
          

            var query = new QueryExpression
            {
                EntityName = mainEntity,
                ColumnSet = new ColumnSet("name", "content"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = { new ConditionExpression("name", ConditionOperator.Equal, confFileName) }
                }
            };

            var crmData = orgService.RetrieveMultiple(query);

            if (crmData.Entities.Count > 0)
            {
                var record = crmData.Entities[0];
                ConfId = record.Id;

                var configJsonB64 = record.GetAttributeValue<string>("content");
                var configJson = Encoding.UTF8.GetString(Convert.FromBase64String(configJsonB64));

                if (!string.IsNullOrEmpty(configJson) && configJson.StartsWith(jsonStart))
                {
                    configJson = configJson.Replace(jsonStart, "");
                    if (configJson.EndsWith(endJson))
                    {
                        configJson = configJson.Replace(endJson, "");
                    }
                    _config = JsonConvert.DeserializeObject<JObject>(configJson);
                }
                else
                {
                    throw new Exception("Can't get main configuration from CRM. Resource with name " + confFileName + " not existing");
                }
            }
            else
            {
                throw new Exception("Can't get main configuration from CRM. Resource with name " + confFileName + " not existing");
            }
            var data = _config.SelectToken(confType.Name);
            if (data != null)
            {
                return data.ToObject<T>();
            }
            else
            {
                throw new Exception("Config not found");
            }
        }

        public static void SaveConfig(IOrganizationService orgService, IModuleConfig moduleConfig)
        {
           // var newConf = _config.ToObject<Dictionary<string, Object>>();
            var key = moduleConfig.GetType();

            _config[key.Name] = JObject.FromObject(moduleConfig);
            var json = JsonConvert.SerializeObject(_config,Formatting.None);

            json = jsonStart + json + endJson;

            //newConf[key] = moduleConfig;
           var data= Convert.ToBase64String(Encoding.UTF8.GetBytes(json));

            var newSetting = new Entity(mainEntity, ConfId)
            {
                Attributes ={
                            new KeyValuePair<string, object>("content",data)
                            }
            };
            orgService.Update(newSetting);
        }

    }
}
