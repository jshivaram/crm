﻿using Microsoft.Xrm.Sdk;
using System;
using System.Activities;
using System.Collections.Generic;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Messages;
using DDSM.CommonProvider.Model;
using System.Linq;
using DDSM.CommonProvider.Utils;
using Microsoft.Crm.Sdk.Messages;

namespace DDSM.CommonProvider.src
{
    public abstract class BaseCodeActivity : CodeActivity
    {

        public enum State { Active = 0, Inactive = 1 }
        public enum Status { Active = 1, Inactive = 2 }

        #region "Parameter Definition"

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        [Output("Result")]
        public OutArgument<string> Result { get; set; }

        [Input("UserInput")]
        public InArgument<string> UserInput { get; set; }
        #endregion


        #region props
        private IOrganizationService _service;
        private Common _objCommon;
        //internal Helper _helper;
        //internal CrmHelper CrmHelper;
        private Entity DataUploader;
        public abstract StatusFileDataUploading CurrentOperationStatus { get; set; }
        private ITracingService TracingService;
        #endregion

        /// <summary>
        /// Executes the workflow activity.
        /// </summary>
        /// <param name="executionContext">The execution context.</param>
        protected override void Execute(CodeActivityContext executionContext)
        {
            if (executionContext == null)
                throw new ArgumentNullException("Code Activity Context is null");

            _objCommon = new Common(executionContext);
            _service = _objCommon.GetOrgService();

            var target = CrmHelper.GetTargetData(_objCommon);

            if (target.LogicalName != "ddsm_taskqueue")
                _objCommon.TracingService = executionContext.GetExtension<ITracingService>();



            TracingService = _objCommon.TracingService;
            _objCommon.TracingService.Trace("Current Depth: " + _objCommon.Context.Depth);

            DataUploader = GetDataUploaderRef();
            UpdateDACurrentOperationStatus();
            _objCommon.TracingService.Trace($"Entered custom activity, Initiating User: {_objCommon.Context.InitiatingUserId}");
            try
            {
                //TODO: comment for prod
               //  ClearAsyncJob();

                _objCommon.TracingService.Trace(string.Format("Entering ExecuteActivity {0}. Correlation Id: {1}",
                    this.GetType().FullName, _objCommon.Context.CorrelationId));

                this.ExecuteActivity(executionContext);
                Complete.Set(executionContext, true);
            }
            catch (Exception e)
            {
                SetErrorStatusForDA();
                ////   ExceptionOccured.Set(executionContext, true);
                ////  ExceptionMessage.Set(executionContext, e.Message);
                ////  if (FailOnException.Get<bool>(executionContext))
                //{
                _objCommon.TracingService.Trace("In catch: " + e.Message + e.Source + e.StackTrace);
                throw new InvalidPluginExecutionException(e.Message, e);
                //}
            }

            finally
            {
                UpdateTQLog();
            }
        }

        /// <summary>
        /// Set status Completed, Canceled  for System Jobs with status  Waiting or WaitingForResources 
        /// </summary>
        private void ClearAsyncJob()
        {
            TracingService.Trace("in ClearAsyncJob()");
            var portionCount = 100;
            var jobs = GetSystemJobs();
            
            //Set Canceled status for jobs
            var asyncJobForCancel = jobs.Select(x => new AsyncOperation { Id = x.Id, StateCode = AsyncOperationState.Completed, StatusCode = new OptionSetValue((int)AsyncOperationStatus.Canceled) }).ToList(); //

            var allProcessedCount = 0;
            while (allProcessedCount < asyncJobForCancel.Count)
            {
                var portion = asyncJobForCancel.Skip(allProcessedCount).Take(portionCount).ToList();
                allProcessedCount += portion.Count;

                foreach (var sysJob in portion)
                {
                    try
                    {
                        _objCommon.GetOrgService(systemCall: true).Update(sysJob);
                    }
                    catch (Exception ex)
                    {
                        TracingService.Trace("ERROR of Job canceling: " + ex.Message + ex.Source + ex.Message);
                    }
                }
            }           
        }

        /// <summary>
        /// Return Top 250 system jobs records with status Waiting or WaitingForResources, when StartedOn less then (Current Date -30 mins)
        /// </summary>
        /// <returns></returns>
        private List<AsyncOperation> GetSystemJobs()
        {
            var statusCodes = new List<int>
                {
                   (int)AsyncOperationStatus.Waiting,
                   (int)AsyncOperationStatus.WaitingForResources,
                };

            var dateNow = DateTime.UtcNow;
            var startedDate = dateNow.AddMinutes(-30);

            var query = new QueryExpression()
            {
                EntityName = AsyncOperation.EntityLogicalName,
                TopCount = 250,
                ColumnSet = new ColumnSet("name", "createdon", "completedon", "regardingobjectid", "asyncoperationid", "startedon", "messagename", "statuscode", "statecode"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression(AsyncOperation.Fields.StatusCode,ConditionOperator.In,statusCodes),
                        new ConditionExpression(AsyncOperation.Fields.StartedOn,ConditionOperator.LessThan, startedDate),
                    }
                }
            };

            var jobs = _objCommon.GetOrgService(systemCall:true).RetrieveMultiple(query).Entities.Select(e => e.ToEntity<AsyncOperation>()).ToList();
            TracingService.Trace("Found System Jobs: " + jobs.Count);

            return jobs;
        }

        private void UpdateTQLog()
        {
            EntityReference target = new EntityReference();
            if (_objCommon.Context.InputParameters.Contains("Target") && _objCommon.Context.InputParameters["Target"] is Entity)
            {
                // Obtain the target entity from the input parameters.
                target = ((Entity)_objCommon.Context.InputParameters["Target"]).ToEntityReference();
            }
            else if (_objCommon.Context.InputParameters.Contains("Target") && _objCommon.Context.InputParameters["Target"] is EntityReference)
            {
                // Obtain the target entity from the input parameters.
                target = (EntityReference)_objCommon.Context.InputParameters["Target"];
            }

            if (_objCommon.TracingService is MoskTraceService && target.LogicalName == "ddsm_taskqueue")
            {
                _objCommon.GetOrgService(systemCall: true).Update(new Entity(target.LogicalName, target.Id)
                {
                    Attributes =
                    {
                        new KeyValuePair<string, object>("ddsm_log", string.Join(System.Environment.NewLine,  (_objCommon.TracingService as MoskTraceService).Logs ) )
                    }
                }
                );
            }


        }

        private void UpdateDACurrentOperationStatus()
        {
            _objCommon.TracingService.Trace("in UpdateDACurrentOperationStatus ");
            //  if (CurrentOperationStatus != null)
            {
                _objCommon.TracingService.Trace("Current Operation Status: " + Enum.GetName(typeof(StatusFileDataUploading), CurrentOperationStatus));

                object curStatusFileDataUploading;
                DataUploader.Attributes.TryGetValue("ddsm_statusfiledatauploading", out curStatusFileDataUploading);

                if (DataUploader?.Id != Guid.Empty && ((OptionSetValue)curStatusFileDataUploading).Value != (int)CurrentOperationStatus)
                {
                    _objCommon.GetOrgService(systemCall: true).Update(new Entity(DataUploader.LogicalName, DataUploader.Id)
                    {
                        Attributes = new AttributeCollection() { new KeyValuePair<string, object>("ddsm_statusfiledatauploading", new OptionSetValue((int)CurrentOperationStatus)) }
                    });
                }
            }
        }

        protected void SetErrorStatusForDA()
        {
            _objCommon.TracingService.Trace("in UpdateDACurrentOperationStatus ");
            //  if (CurrentOperationStatus != null)
            {
                _objCommon.TracingService.Trace("Current Operation Status: " + Enum.GetName(typeof(StatusFileDataUploading), CurrentOperationStatus));

                object curStatusFileDataUploading;
                DataUploader.Attributes.TryGetValue("ddsm_statusfiledatauploading", out curStatusFileDataUploading);

                if (DataUploader?.Id != Guid.Empty && ((OptionSetValue)curStatusFileDataUploading).Value != (int)CurrentOperationStatus)
                {
                    _objCommon.GetOrgService(systemCall: true).Update(new Entity(DataUploader.LogicalName, DataUploader.Id)
                    {
                        Attributes = new AttributeCollection() { new KeyValuePair<string, object>("ddsm_statusfiledatauploading", new OptionSetValue((int)StatusFileDataUploading.importFailed)) }
                    });
                }
            }
        }

        private Entity GetDataUploaderRef()
        {
            var result = new Entity();
            try
            {
                var target = CrmHelper.GetTargetData(_objCommon);

                if (target.LogicalName != "ddsm_taskqueue")
                    return new Entity();

                var taskQueue = _objCommon.GetOrgService(systemCall: true).Retrieve(target.LogicalName, target.Id, new ColumnSet("ddsm_datauploader"));

                object tmpObj;
                if (taskQueue.Attributes.TryGetValue("ddsm_datauploader", out tmpObj))
                {
                    var dataUploaderRef = (EntityReference)tmpObj;
                    result = _objCommon.GetOrgService(systemCall: true).Retrieve(dataUploaderRef.LogicalName, dataUploaderRef.Id, new ColumnSet("ddsm_statusfiledatauploading"));
                }
            }
            catch (Exception ex)
            {
                _objCommon.TracingService.Trace("Error on GetDataUploaderRef() " + ex.Message);
                return result;
            }
            return result;
        }

        protected abstract void ExecuteActivity(CodeActivityContext executionContext);
    }
}
