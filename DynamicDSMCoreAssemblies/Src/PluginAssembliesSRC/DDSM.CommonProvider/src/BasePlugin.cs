﻿using System;
using Microsoft.Xrm.Sdk;
using System.Diagnostics;

namespace DDSM.CommonProvider.src
{
    public abstract class BasePlugin : IPlugin
    {
        //Execution Context
        public IPluginExecutionContext Context { get; private set; }

        //Oganization Service
        public IOrganizationService Service { get; private set; }

        //Target Entity
        public object Target { get; set; }

        //Tracing Service
        public ITracingService Tracer { get; private set; }

        /// <summary>
        ///     A plug-in that provide common implementation
        /// </summary>
        public void Execute(IServiceProvider serviceProvider)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                // The InputParameters collection contains all the data passed in the message request.
                if (!IsContextFull(serviceProvider))
                {
                    return;
                }

                //Execute plug-in
                Main();
                Tracer.Trace("--- Performance info ---");
                Tracer.Trace("Execution time: " + sw.Elapsed);
            }
            catch (Exception ex)
            {
                Tracer.Trace(ex.Message);
            }
        }      

        /// <summary>
        ///     Start Executing a plug-in
        /// </summary>
        private void Main()
        {
            Tracer.Trace("*** Plug-in " + GetType().FullName + " is started ***");

            //Terminate executing plugin, because record is creating via DataUploader
            if (Context.MessageName.ToUpper() == "CREATE" && GetEntryPoint() == CreatorRecordType.DataUploader)
            {
                Tracer.Trace("*** Stop plugin! Record is creating via DataUploader.***");
                Tracer.Trace("*** Plug-in " + GetType().FullName +
                             " is terminated because record is creating via DataUploader.***");
                return;
            }
          

            //Run the specific plugin
            Run();

            Tracer.Trace("*** Plug-in " + GetType().FullName + " is successfully executed ***");
        }

        /// <summary>
        ///     Abstract method should be implemented in a child class
        /// </summary>
        protected abstract void Run();

        /// <summary>
        ///     Get Entry point
        /// </summary>
        /// <returns></returns>
        protected virtual CreatorRecordType GetEntryPoint()
        {
            //default entry point
            var entryPoint = CreatorRecordType.UnassignedValue;

            //get entity
            var entity = (Entity)Context.InputParameters["Target"];

            if (!entity.Attributes.Contains("ddsm_creatorrecordtype"))
            {
                return entryPoint;
            }
            object creatorRecordType = null;
            if (entity.Attributes.TryGetValue("ddsm_creatorrecordtype", out creatorRecordType))
            {
                entryPoint = (CreatorRecordType)(creatorRecordType as OptionSetValue).Value;
            }
            //retrun entry point
            return entryPoint;
        }

        /// <summary>
        ///     Obtain the execution context from the service provider.
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        private bool IsContextFull(IServiceProvider serviceProvider)
        {
            var rs = false;
            try
            {
                // Obtain the execution context from the service provider.
                Context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                //Extract the tracing service for use in debugging sandboxed plug-ins.
                Tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

                // Obtain the organization service reference.
                var factory =
                    (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                Service = factory.CreateOrganizationService(Context.UserId);


                if (string.IsNullOrEmpty(Context.MessageName))
                {
                    rs = false;
                }
                // Obtain the target entity from the input parameters.
                if (Context.InputParameters.Contains("Target") &&
                    Context.InputParameters["Target"] != null
                    )
                {
                    Target = Context.InputParameters["Target"];
                    rs = true;
                }
            }
            catch (Exception ex)
            {
                Tracer.Trace(ex.Message);
            }
            return rs;
        }


        /// <summary>
        ///     Creator record type
        /// </summary>
        protected enum CreatorRecordType
        {
            UnassignedValue = -2147483648,
            Front = 962080000,
            DataUploader = 962080001,
            Portal = 962080002
        }

        public enum EntityTypeCode
        {
            ddsm_admindata = 10007,
        }
    }
}