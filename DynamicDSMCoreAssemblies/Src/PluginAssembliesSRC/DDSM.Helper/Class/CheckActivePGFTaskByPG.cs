﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Activities;
using DDSM.CommonProvider;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk.Workflow;


public class CheckActivePGFTaskByPG : BaseCodeActivity
{
    public override StatusFileDataUploading CurrentOperationStatus
    {
        get
        {
            return StatusFileDataUploading.projectGroupsProcessing;
        }

        set
        {
            throw new NotImplementedException();
        }
    }

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        var context = executionContext.GetExtension<IWorkflowContext>();
        var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        var _service = serviceFactory.CreateOrganizationService(context.UserId);
        var tracer = executionContext.GetExtension<ITracingService>();
        var _objCommon = new Common(executionContext);

        var pgId = Guid.Parse(UserInput.Get(executionContext));
        var projects = GetProjectsByPG(pgId,_objCommon);

        var activeTaskCount = GetActiveTaskQueue(projects, _objCommon);
        _objCommon.TracingService.Trace("activeTaskCount: " + activeTaskCount);
        Result.Set(executionContext, activeTaskCount.ToString());
        Complete.Set(executionContext, true);

    }

    private int GetActiveTaskQueue(EntityCollection projects, Common _objCommon)
    {
        _objCommon.TracingService.Trace("Found Projects count: " + projects.Entities.Count);
        var result = 0;
        try
        {
            var query = new QueryExpression
            {
                EntityName = "ddsm_taskqueue",
                ColumnSet = new ColumnSet("ddsm_taskentity", "ddsm_processeditems0"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                   {
                       new ConditionExpression("ddsm_processeditems0",ConditionOperator.NotNull ),
                       new ConditionExpression("ddsm_taskentity",ConditionOperator.Equal, 962080006),    // TQ type ddsm_projectgroupfinancial
                       new ConditionExpression("statuscode",ConditionOperator.Equal, 1), //active
                   }
                }
            };

            var resultEntities = _objCommon.GetOrgService(systemCall: true).RetrieveMultiple(query).Entities;
            _objCommon.TracingService.Trace("Task resultEntities: " + resultEntities.Count.ToString());

            foreach (var task in resultEntities)
            {
                foreach (var prj in projects.Entities)
                {
                    _objCommon.TracingService.Trace("ddsm_processeditems0: "+ (task.Attributes["ddsm_processeditems0"] as string));
                    _objCommon.TracingService.Trace("Project ID: " + prj.Id.ToString().ToLower());                  
                    if ((task.Attributes["ddsm_processeditems0"] as string).Contains(prj.Id.ToString().ToLower()))
                    {
                        _objCommon.TracingService.Trace("task contains Proj id: " + prj.Id.ToString().ToLower());
                        return ++result;
                    }
                }
            }
            return result;
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace(ex.Message);
            return 0;
        }
    }

    private EntityCollection GetProjectsByPG(Guid pgId, Common _objCommon)
    {
        var query = new QueryExpression
        {
            EntityName = "ddsm_project",
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions = {
                    new ConditionExpression("ddsm_projectgroupid", ConditionOperator.Equal, pgId),
                    new ConditionExpression("statuscode",ConditionOperator.Equal, 1), //active
                }
            }
        };

        return _objCommon.GetOrgService(systemCall: true).RetrieveMultiple(query);
    }

}
