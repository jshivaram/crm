﻿using DDSM.Helper.Model;
using System;
using System.Activities;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;


public class CreateProject : CodeActivity
    {
        #region Parameters
        [RequiredArgument]
        [Input("Parent Account")]
        [ArgumentEntity("account")]
        [ReferenceTarget("account")]
        public InArgument<EntityReference> ParentAccount { get; set; }

        [RequiredArgument]
        [Input("Parent Site")]
        [ArgumentEntity("ddsm_site")]
        [ReferenceTarget("ddsm_site")]
        public InArgument<EntityReference> ParentSite { get; set; }

        [RequiredArgument]
        [Input("Start Date")]
        public InArgument<DateTime> StartDate { get; set; }

        [RequiredArgument]
        [Input("Program Offering")]
        [ArgumentEntity("ddsm_programoffering")]
        [ReferenceTarget("ddsm_programoffering")]
        public InArgument<EntityReference> ProgramOffering { get; set; }

        [RequiredArgument]
        [Input("Workflow")]
        [ArgumentEntity("ddsm_projecttemplate")]
        [ReferenceTarget("ddsm_projecttemplate")]
        public InArgument<EntityReference> Workflow { get; set; }

        [RequiredArgument]
        [Input("Site Measure")]
        [ArgumentEntity("ddsm_sitemeasure")]
        [ReferenceTarget("ddsm_sitemeasure")]
        public InArgument<EntityReference> SiteMeasure { get; set; }

        #endregion

        #region Internal Fields

        private Common _common;
        private Logger _logger;

        private int startRow;
        private int _startProjNumber;

        private EntityReference _parentAccount;
        private EntityReference _parentSite;
        private DateTime _startDate;
        private EntityReference _programOffering;
        private EntityReference _workflow;
        private EntityReference _siteMeasure;




        #endregion

        protected override void Execute(CodeActivityContext executionContext)
        {
            _common = new Common(executionContext);
            _logger = new Logger(_common.TracingService);


            _logger.Add("After Common Initialized");

            #region initialize params
            _parentAccount = ParentAccount.Get(executionContext);
            _parentSite = ParentSite.Get(executionContext);
            _startDate = StartDate.Get(executionContext);
            _programOffering = ProgramOffering.Get(executionContext);
            _workflow = Workflow.Get(executionContext);
            _siteMeasure = SiteMeasure.Get(executionContext);
            #endregion

            _logger.Add("After getting input Agsuments");
            _logger.Add("Parent Acc: {0}, Parent Site: {1}, Start Date: {2}, Program Off: {3}, Workflow: {4}, Site Measure: {5}",
                _parentAccount.Id.ToString(), _parentSite.Id.ToString(), _startDate, _programOffering.Id.ToString(), _workflow.Id.ToString(), _siteMeasure.Id.ToString());

            try
            {
                var entityTagret = (Entity)_common.Context.InputParameters["Target"];

                //Get Guid of Currency
                //TODO: uncomment on Prod 
                // DDSM.Helper.Utils.CrmHelper.GetGuidofCurrency(_common.Service, _common.TracingService);


                //Project && milestone && BisProcess
                CreateNewProjectRecord();


                /// Finanse
                /// Measure

            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException(e.Message);
            }
        }

        /// <summary>
        /// Create Project Record
        /// </summary>
        /// <param name="orgService"></param>
        /// <param name="AccountUniqueID"></param>
        /// <param name="AccountTableName"></param>
        /// <param name="SiteUniqueID"></param>
        /// <param name="SiteTableName"></param>
        /// <param name="objects"></param>
        /// <param name="table"></param>
        public void CreateNewProjectRecord()
        {
            GetProjectIdCounter();

            var newProject = new Entity("ddsm_project")
            {
                ["ddsm_phasenumber"] = 1,
                ["ddsm_name"] = GenerateProjectName(),
                ["ddsm_startdate"] = DDSM.Helper.Utils.Utils.ConvertAttributToDateTimeUtc("today"),
                ["ddsm_accountid"] = new EntityReference("account", _parentAccount.Id),
                ["ddsm_parentsiteid"] = new EntityReference("ddsm_site", _parentSite.Id)
            };




            //Get relate fields ProjectTemplate
            var rmEntityProjTemplate = DDSM.Helper.Utils.CrmHelper.GetDataRelationMapping(_common.GetOrgService(), "ddsm_project", "ddsm_projecttemplate", _workflow.Id);
            foreach (var attribute in rmEntityProjTemplate.Attributes)
            {
                newProject[attribute.Key] = attribute.Value;
            }

            //Get relate fields Account
            var rmEntityAcc = DDSM.Helper.Utils.CrmHelper.GetDataRelationMapping(_common.GetOrgService(), "ddsm_project", "account", _parentAccount.Id);
            foreach (var attribute in rmEntityAcc.Attributes)
            {
                newProject[attribute.Key] = attribute.Value;
            }

            //Get relate fields Site
            var rmEntitySite = DDSM.Helper.Utils.CrmHelper.GetDataRelationMapping(_common.GetOrgService(), "ddsm_project", "ddsm_site", _parentSite.Id);
            foreach (var attribute in rmEntitySite.Attributes)
            {
                newProject[attribute.Key] = attribute.Value;
            }

            //Create & ReCalc Milestone Collection
            var milestoneCollection = DDSM.Helper.Utils.CrmHelper.CreateMilestones(_common.GetOrgService(), _workflow.Id, Convert.ToDateTime(newProject["ddsm_startdate"]));

            //Get Business Process project
            var projBP = new ProjectBusinessProcess();
            // if (!string.IsNullOrEmpty(projTplName))
            {
                var workflow = _common.GetOrgService().Retrieve(_workflow.LogicalName, _workflow.Id, new ColumnSet("ddsm_name"));
                projBP = DDSM.Helper.Utils.CrmHelper.GetBProcess(_common.GetOrgService(), workflow["ddsm_name"] as string, _common.TracingService);
            }
            int activeIdx = 2000000000;
            string traversedpath;
            string stageid;
            string processid;
            foreach (Entity ms in milestoneCollection.Entities)
            {
                if (ms["ddsm_projectphasename"].ToString() == newProject["ddsm_phase"].ToString() && ms["ddsm_projectstatus"].ToString() == newProject["ddsm_projectstatus"].ToString())
                {
                    activeIdx = Convert.ToInt32(ms["ddsm_index"]);
                    break;
                }
            }


            _common.GetOrgService().Create(newProject);
        }



        void GetProjectIdCounter()
        {
            var ConfigProjCounter = new QueryExpression
            {
                EntityName = "ddsm_admindata",
                ColumnSet = new ColumnSet("ddsm_projectidcounter", "ddsm_admindataid", "ddsm_startdatarowdu", "ddsm_duplicatedetection", "ddsm_datavalidation", "ddsm_deduplicationrules")
            };

            ConfigProjCounter.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, "Admin Data");

            var ConfigProjCounterRetrieve = _common.GetOrgService().RetrieveMultiple(ConfigProjCounter);

            if (ConfigProjCounterRetrieve == null || ConfigProjCounterRetrieve.Entities.Count != 1) return;
            startRow = Convert.ToInt32(ConfigProjCounterRetrieve.Entities[0].Attributes["ddsm_startdatarowdu"].ToString());
            _startProjNumber = Convert.ToInt32(ConfigProjCounterRetrieve.Entities[0].Attributes["ddsm_projectidcounter"].ToString());
            var adminData = ConfigProjCounterRetrieve.Entities[0] as Entity;
            adminData["ddsm_projectidcounter"] = _startProjNumber++;
            _common.GetOrgService().Update(adminData);
        }

        private string GenerateProjectName()
        {
            var site = _common.GetOrgService().Retrieve(_parentSite.LogicalName, _parentSite.Id, new ColumnSet("ddsm_name"));
            var o = "";
            var ln = 20;
            var s = Convert.ToString(_startProjNumber);
            if (s.Length < 6)
            {
                s = ("000000" + s).Substring(("000000" + s).Length - 6, 6);
            }
            _startProjNumber++;


            o = site["ddsm_name"] as string;
            if (o?.Length < 20)
                ln = o.Length;

            return s + "- - " + o.Substring(0, ln);
        }

    }

