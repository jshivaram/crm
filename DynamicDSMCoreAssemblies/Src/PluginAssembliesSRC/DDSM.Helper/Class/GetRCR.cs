﻿using DDSM.Helper.Model;
using System;
using System.Activities;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;

public class GetRCR : CodeActivity
{
    #region Parameters
    [RequiredArgument]
    [Input("Start Project Date")]
    public InArgument<DateTime> StartProjectDate { get; set; }

    //[RequiredArgument]
    //[Input("Rate Code")]
    //public InArgument<string> RateCode { get; set; }

    [RequiredArgument]
    [Input("Rate Code")]
    [AttributeTarget("ddsm_measure", "ddsm_ratecode")]
    public InArgument<OptionSetValue> RateCode { get; set; }

    [Output("Rate Class Name")]
    [ArgumentEntity("ddsm_rateclassreference")]
    [ReferenceTarget("ddsm_rateclassreference")]
    public OutArgument<EntityReference> RateCodeName { get; set; }

    [Output("Complete")]
   
    public OutArgument<bool> Complete { get; set; }

    #endregion

    #region Internal Fields

    private Common _common;
    private Logger _logger;
    #endregion

    protected override void Execute(CodeActivityContext executionContext)
    {
        _common = new Common(executionContext);
        _logger = new Logger(_common.TracingService);

       // var targetRef = _common.Context.InputParameters["Target"] as EntityReference;
        var rateCode = RateCode.Get(executionContext);
        var startDate = StartProjectDate.Get(executionContext);


        var rateClassRefs = GetRateClassReference(startDate.Year.ToString(),rateCode.Value);

        if (rateClassRefs.Entities.Count > 0)
        {
            Complete.Set(executionContext, true);
            RateCodeName.Set(executionContext, new EntityReference("ddsm_rateclassreference", rateClassRefs.Entities[0].Id));
           
        }
        else
        {
            Complete.Set(executionContext, false);
            RateCodeName.Set(executionContext, new EntityReference("ddsm_rateclassreference",Guid.Empty));
        }    

        _logger.Add("After Common Initialized");
    }

    EntityCollection GetRateClassReference(string rateCodeYear, int rateCode)
    {
        var query = new QueryExpression()
        {
             EntityName = "ddsm_rateclassreference",
             Criteria = new FilterExpression(LogicalOperator.And) { Conditions = { new ConditionExpression("ddsm_ratecodeyear", ConditionOperator.Equal, rateCodeYear ),
                     new ConditionExpression("ddsm_ratecode",ConditionOperator.Equal, rateCode) } } 
        };
        return _common.GetOrgService().RetrieveMultiple(query);
    }

}

