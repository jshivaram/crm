﻿using DDSM.Helper.Model;
using System;
using System.Activities;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;


public class GetAdminDataRecordByName : CodeActivity
{
    #region Parameters
    [RequiredArgument]
    [Input("Entity Name")]
    [Default("ddsm_admindata")]
    public InArgument<String> EntityName { get; set; }

    [RequiredArgument]
    [Input("RecordName")]
    [Default("Admin Data")]
    public InArgument<string> RecordName { get; set; }

    [Output("Record Result")]
    [ArgumentEntity("ddsm_admindata")]
    [ReferenceTarget("ddsm_admindata")]
    public OutArgument<EntityReference> RecordResult{ get; set; }

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }

    #endregion

    private Common _common;
    private Logger _logger;

    protected override void Execute(CodeActivityContext context)
    {
        _common = new Common(context);
        _logger = new Logger(_common.TracingService);

        var entityName = EntityName.Get(context);
        var name = RecordName.Get(context);

        var adminDataRef = GetAdminDataRecord(entityName,name);
        if (adminDataRef != null)
        {
            Complete.Set(context, true);
        }
        else
        {
            Complete.Set(context, false);
        }

        RecordResult.Set(context, adminDataRef);
    }

    private EntityReference GetAdminDataRecord(string entityName, string recordName)
    {
        var query = new QueryExpression()
        {
           EntityName= entityName,
           Criteria = new FilterExpression(LogicalOperator.And) { Conditions = { new ConditionExpression("ddsm_name",ConditionOperator.Equal,recordName )} }
        };

        var result = _common.GetOrgService(systemCall:true).RetrieveMultiple(query);
        if (result != null && result.Entities?.Count > 0)
        {
            return result.Entities[0].ToEntityReference();
        }
        else
        {
            return null;
        }
    }
}

  

