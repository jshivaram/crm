﻿using System;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using DDSM.CommonProvider.Model;
using System.Collections.Generic;
using DDSM.CommonProvider;
using DDSM.CommonProvider.src;
using DDSM.CommonProvider.Utils;

public class UpdatePGCalculationStatus : BaseCodeActivity
{
    #region Parameters 

    public override StatusFileDataUploading CurrentOperationStatus
    {
        get
        {
            return StatusFileDataUploading.recalculationBusinessRules;
        }

        set
        {
            throw new NotImplementedException();
        }
    }

    #endregion  

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        var _objCommon = new Common(executionContext);

        UserInputObj2 projectIDs;
        var projectIDsJson = UserInput.Get(executionContext);
        var alreadyProcessedPGF = new List<Guid>();

        if (!string.IsNullOrEmpty(projectIDsJson))
        {
            projectIDs = JsonConvert.DeserializeObject<UserInputObj2>(projectIDsJson);
            if (projectIDs == null)
            {
                _objCommon.TracingService.Trace("projectIDs is null");
            }

            var preparedProject = GetPreparedProjects(projectIDs?.SmartMeasures, _objCommon);

            _objCommon.TracingService.Trace("Prepared projects Count: " + preparedProject?.Count);
            if (preparedProject != null)
                foreach (var proj in preparedProject)
                {
                    var PGFinancials = GetPGFinancials(proj.Value, _objCommon);

                    if (PGFinancials != null)
                    {
                        _objCommon.TracingService.Trace("PG Financials Count: " + PGFinancials?.Entities?.Count);

                        //set Calculation Status for PGF as "Recalculation in progress"
                        foreach (var pgf in PGFinancials.Entities)
                        {
                            if (alreadyProcessedPGF.Contains(pgf.Id))
                            {
                                _objCommon.TracingService.Trace("PGF: " + pgf.Id + " already have status " + Enum.GetName(typeof(CalculationStatus), CalculationStatus.RecalculationCompleted));
                                continue;
                            }
                            var _pgf = new Entity(pgf.LogicalName, pgf.Id)
                            {
                                Attributes =  {
                                new KeyValuePair<string, object>("ddsm_calculationstatus",new OptionSetValue((int)CalculationStatus.RecalculationCompleted)),
                            }
                            };
                            _objCommon.TracingService.Trace("Update calculation status of PGF: " + _pgf.Id + " status = " + Enum.GetName(typeof(CalculationStatus), CalculationStatus.RecalculationCompleted));
                            _objCommon.GetOrgService(systemCall: true).Update(_pgf);
                            alreadyProcessedPGF.Add(_pgf.Id);
                        }
                    }
                }
        }
        else
        {
            _objCommon.TracingService.Trace("UserInput is empty. No Project for processing.");
        }
    }


    private EntityCollection GetPGFinancials(Guid? projGroupItem, Common localObjCommon)
    {
        localObjCommon.TracingService.Trace("Get PG Financials for : " + projGroupItem);
        if (projGroupItem != null)
        {
            var expr = new QueryExpression
            {
                EntityName = "ddsm_projectgroupfinancials",
                ColumnSet = new ColumnSet("ddsm_duration1", "ddsm_duration2", "ddsm_duration3", "ddsm_duration4", "ddsm_duration5", "ddsm_status", "ddsm_calculationtype", "ddsm_initiatingmilestoneindex"), //TODO: set columns 
                Criteria = new FilterExpression()
                {
                    Conditions = {
                    new ConditionExpression("ddsm_projectgrouptoprojectgroupfinanid", ConditionOperator.Equal, projGroupItem),
                    new ConditionExpression("statuscode",ConditionOperator.Equal, 1)

                }
                }
            };
            return localObjCommon.GetOrgService().RetrieveMultiple(expr);
        }
        return null;
    }

    private Dictionary<Guid, Guid?> GetPreparedProjects(List<Guid> projectsId, Common localObjCommon)
    {
        var listPrepared = new Dictionary<Guid, Guid?>();
        if(projectsId !=null && projectsId.Count >0){
        
        var query = new QueryExpression()
        {
            EntityName = "ddsm_project",
            ColumnSet = new ColumnSet("ddsm_projectid", "ddsm_calculationrecordstatus", "ddsm_projectgroupid"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions =
                {
                    new ConditionExpression("ddsm_projectid", ConditionOperator.In, projectsId),
                    new ConditionExpression("statuscode",ConditionOperator.Equal, 1),
                    new ConditionExpression("ddsm_calculationrecordstatus",ConditionOperator.Equal, (int)CalculationRecordStatus.ReadyToCalculation)
                }
            }
        };

        //var enitities = localObjCommon.Service.RetrieveMultiple(query);
        var result = localObjCommon.GetOrgService(systemCall: true).RetrieveMultiple(query);

        foreach (var project in result.Entities)
        {
            if (project.Attributes.ContainsKey("ddsm_calculationrecordstatus") &&
                project.GetAttributeValue<OptionSetValue>("ddsm_calculationrecordstatus").Value == (int)CalculationRecordStatus.ReadyToCalculation)
            {
                listPrepared.Add(project.Id, project.GetAttributeValue<EntityReference>("ddsm_projectgroupid")?.Id);
            }
        }
        }
        return listPrepared;
    }
}



