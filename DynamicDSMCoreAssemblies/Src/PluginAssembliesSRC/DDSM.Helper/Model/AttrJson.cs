﻿namespace DDSM.Helper.Model
{
    public class AttrJson
    {
        public string Header { get; set; }       
        public string Name { get; set; }
        public string AttrType { get; set; }
    }
}
