﻿using System;
using Microsoft.Xrm.Sdk;

namespace DDSM.Helper.Model
{
    public class CreatedEntity
    {
        public string Name { get; set; }
        public string addInfo { get; set; }
        public Guid EntityGuid { get; set; }
        public Entity NewEntity { get; set; }
        public string UniqueParentGUID { get; set; }
    }
}
