﻿using System.Collections.Generic;

namespace DDSM.Helper.Model
{
    public class EntityJson
    {
       
        public string Name { get; set; }
       
        public Dictionary<string, AttrJson> Attrs { get; set; }
    }
}
