﻿using Microsoft.Xrm.Sdk;

namespace DDSM.Helper.Model
{
    public class Logger
    {
        ITracingService _service;
        public Logger(ITracingService service)
        {
            _service = service;

        }

        public void Add(string text, params object[] args)
        {
            if (_service != null)
                _service.Trace(text, args);
        }

    }
}
