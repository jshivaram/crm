﻿using System.Collections.Generic;

namespace DDSM.Helper.Model
{
    public class ProjectBusinessProcess
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public List<ProjectBusinessProcessStage> Stages { get; set; }
    }

    public class ProjectBusinessProcessStage
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }
}
