﻿using DDSM.Helper.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using DDSM.CommonProvider;

namespace DDSM.Helper.Utils
{
    public static class CrmHelper
    {
        private static int insertRecords;

        public static string AccountTableName { get; private set; }
        public static string SiteTableName { get; private set; }
        public static string ContactTableName { get; private set; }
        public static string ProjectTableName { get; private set; }
        public static string MeasureTableName { get; private set; }
        public static string FinancialTableName { get; private set; }
        public static string ProjectGroupTableName { get; private set; }
        public static string ProjectGroupFinancialTableName { get; private set; }

        public static Guid CurrencyGuid { get; private set; }

        public static bool ExistAccountTable { get; private set; }
        public static bool ExistSiteTable { get; private set; }
        public static bool ExistContactTable { get; private set; }
        public static bool ExistProjectTable { get; private set; }
        public static bool ExistMeasureTable { get; private set; }
        public static bool ExistFinancialTable { get; private set; }
        public static bool ExistProjectGroupTable { get; private set; }
        public static bool ExistProjectGroupFinancialTable { get; private set; }


        public static DataSet result { get; private set; }


        public static Entity GetDataRelationMapping(IOrganizationService _orgService, string targetEntity, string sourceEntity, Guid sourceGuid)
        {
            var initialize = new InitializeFromRequest();
            initialize.TargetEntityName = targetEntity;
            initialize.EntityMoniker = new EntityReference(sourceEntity, sourceGuid);
            initialize.TargetFieldType = TargetFieldType.All;
            var initialized = (InitializeFromResponse)_orgService.Execute(initialize);
            return initialized.Entity;
        }


        public static EntityReference GetTargetData(Common objDdsm)
        {
            EntityReference target = null;
            try
            {
              //  Contract.Ensures(Contract.Result<EntityReference>() != null);


                if (objDdsm.Context.InputParameters["Target"] is EntityReference)
                {
                    target = new EntityReference(((EntityReference)objDdsm.Context.InputParameters["Target"]).LogicalName, ((EntityReference)objDdsm.Context.InputParameters["Target"]).Id);
                }
                else if (objDdsm.Context.InputParameters["Target"] is Entity)
                {
                    target = new EntityReference(((Entity)objDdsm.Context.InputParameters["Target"]).LogicalName, ((Entity)objDdsm.Context.InputParameters["Target"]).Id);
                }
            }
            catch (Exception)
            {

                objDdsm.TracingService.Trace("Can't get target from Context");
            }


            return target;
        }



        //Generation Entity Object
        public static Entity generationEntity(IOrganizationService _orgService, Dictionary<string, EntityJson> objects, string TableName, DataRow dr)
        {

            var EntityObj = new Entity(objects[TableName].Name.ToLower());
            var EntityUniqueID = string.Empty;
            try
            {
                foreach (var attr in objects[TableName].Attrs)
                {
                    if (attr.Value.Header.ToLower() == (TableName + " GUID").ToLower())
                    {
                        EntityUniqueID = dr[attr.Key].ToString();
                    }
                    if ((attr.Value.Header.ToLower()).IndexOf("guid") != -1)
                    {
                    }
                    else if ((attr.Value.Header.ToLower()).IndexOf("template") != -1)
                    {
                    }
                    else if ((attr.Value.Header.ToLower()).IndexOf("milestone") != -1)
                    {
                    }
                    else {
                        if (dr[attr.Key].ToString() != "")
                        {
                            if (attr.Value.AttrType.ToString() != "Lookup")
                            {
                                dynamic tmpEntityObj = DDSM.Helper.Utils.CrmHelper.AttributeValue(_orgService, objects[TableName].Name.ToString().ToLower(), attr.Value.Name.ToString().ToLower(), attr.Value.AttrType.ToString(), dr[attr.Key], string.Empty);
                                if (tmpEntityObj != null)
                                {
                                    EntityObj[attr.Value.Name.ToLower()] = tmpEntityObj;
                                }
                            }
                            else {
                                dynamic tmpEntityObj = DDSM.Helper.Utils.CrmHelper.AttributeValue(_orgService, objects[TableName].Name.ToString().ToLower(), (attr.Value.Name.ToString().ToLower()).Split(':')[0], attr.Value.AttrType.ToString(), dr[attr.Key], (attr.Value.Name.ToLower()).Split(':')[1]);
                                if (tmpEntityObj != null)
                                {
                                    EntityObj[(attr.Value.Name.ToLower()).Split(':')[0]] = tmpEntityObj;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //Logger.Add("Data of the '" + TableName + "'table doesn't correspond to a configuration template.");
                EntityObj = new Entity(objects[TableName].Name.ToLower());
                return EntityObj;
            }
            return EntityObj;

        }


        //Create value attribute entity
        public static dynamic AttributeValue(IOrganizationService _orgService, string entityName, string AttrName, string AttrType, dynamic AttrValue, string LookupEntity)
        {
            try
            {
                switch (AttrType)
                {
                    case "String":
                        return AttrValue.ToString();

                    case "Memo":
                        return AttrValue.ToString();

                    case "Picklist":

                        var retrieveAttributeRequest = new RetrieveAttributeRequest
                        {
                            EntityLogicalName = entityName,
                            LogicalName = AttrName,
                            RetrieveAsIfPublished = true
                        };
                        var retrieveAttributeResponse = (RetrieveAttributeResponse)_orgService.Execute(retrieveAttributeRequest);
                        var retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)
                        retrieveAttributeResponse.AttributeMetadata;
                        var optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
                        var selectedOptionValue = 0;
                        foreach (var oMD in optionList)
                        {
                            if (oMD.Label.LocalizedLabels[0].Label.ToString().ToLower() == AttrValue.ToString().ToLower())
                            {
                                selectedOptionValue = oMD.Value.Value;
                                break;
                            }
                        }
                        if (selectedOptionValue != 0)
                        {
                            return new OptionSetValue(selectedOptionValue);
                        }
                        else {
                            return new OptionSetValue(retrievedPicklistAttributeMetadata.DefaultFormValue.Value);
                        }

                    case "Lookup":
                        QueryExpression LookupQuery;
                        if (LookupEntity != string.Empty)
                        {
                            if (LookupEntity.IndexOf("ddsm_") != -1)
                            {
                                if (LookupEntity == "ddsm_projecttask")
                                {
                                    LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("subject", "activityid") };
                                    LookupQuery.Criteria.AddCondition("subject", ConditionOperator.Equal, AttrValue.ToString());
                                }
                                else {
                                    LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("ddsm_name", LookupEntity + "id") };
                                    LookupQuery.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, AttrValue.ToString());
                                }
                            }
                            else if (LookupEntity == "contact" || LookupEntity == "systemuser")
                            {
                                LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("fullname", LookupEntity + "id") };
                                LookupQuery.Criteria.AddCondition("fullname", ConditionOperator.Equal, AttrValue.ToString());
                            }
                            else if (LookupEntity == "task")
                            {
                                LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("subject", "activityid") };
                                LookupQuery.Criteria.AddCondition("subject", ConditionOperator.Equal, AttrValue.ToString());
                            }
                            else if (LookupEntity == "annotation")
                            {
                                LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("subject", "annotationid") };
                                LookupQuery.Criteria.AddCondition("subject", ConditionOperator.Equal, AttrValue.ToString());
                            }
                            else {
                                LookupQuery = new QueryExpression { EntityName = LookupEntity, ColumnSet = new ColumnSet("name", LookupEntity + "id") };
                                LookupQuery.Criteria.AddCondition("name", ConditionOperator.Equal, AttrValue.ToString());
                            }

                            var LookupRetrieve = _orgService.RetrieveMultiple(LookupQuery);
                            if (LookupRetrieve != null && LookupRetrieve.Entities.Count == 1)
                            {
                                return new EntityReference(LookupEntity, new Guid(LookupRetrieve.Entities[0].Attributes[LookupEntity + "id"].ToString()));
                            }
                            else {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    case "Decimal":
                        if (IsNumeric(AttrValue))
                            return Convert.ToDecimal(AttrValue.ToString());
                        else return null;

                    case "Double":
                        if (IsNumeric(AttrValue))
                            return Convert.ToDouble(AttrValue.ToString());
                        else return null;

                    case "Money":
                        if (IsNumeric(AttrValue))
                            return new Money(decimal.Parse(AttrValue.ToString()));
                        else return null;

                    case "Boolean":

                        if (AttrValue.ToString().ToLower() == "true") return true;
                        if (AttrValue.ToString().ToLower() == "false") return false;

                        var retrieveAttributeBoolRequest = new RetrieveAttributeRequest
                        {
                            EntityLogicalName = entityName,
                            LogicalName = AttrName,
                            RetrieveAsIfPublished = true
                        };
                        var retrieveAttributeBoolResponse = (RetrieveAttributeResponse)_orgService.Execute(retrieveAttributeBoolRequest);
                        var retrievedBooleanAttributeMetadata = (BooleanAttributeMetadata)
                        retrieveAttributeBoolResponse.AttributeMetadata;
                        if (retrievedBooleanAttributeMetadata.OptionSet.FalseOption.Label.LocalizedLabels[0].Label.ToString().ToLower() == AttrValue.ToString().ToLower())
                            return false;
                        else if (retrievedBooleanAttributeMetadata.OptionSet.TrueOption.Label.LocalizedLabels[0].Label.ToString().ToLower() == AttrValue.ToString().ToLower())
                            return true;
                        return retrievedBooleanAttributeMetadata.DefaultValue.Value;
                    case "DateTime":
                        return DDSM.Helper.Utils.Utils.ConvertAttributToDateTimeUtc(AttrValue);

                    case "Integer":
                        if (IsNumeric(AttrValue))
                            return Convert.ToInt32(AttrValue.ToString());
                        else return null;

                    case "BigInt":
                        if (IsNumeric(AttrValue))
                            return Convert.ToInt64(AttrValue.ToString());
                        else return null;
                }
                return null;
            }
            catch (Exception e)
            {
                // Logger.Add(entityName + " : " + AttrName + " : " + e.Message);
                return null;
            }
        }


        //Is Numeric Format
        public static Boolean IsNumeric(object AttrValue)
        {
            try
            {
                if (AttrValue == null || AttrValue is DateTime)
                    throw new Exception("Value '" + AttrValue.ToString() + "' is not a number");

                if (AttrValue is Int16 || AttrValue is Int32 || AttrValue is Int64 || AttrValue is Decimal || AttrValue is Single || AttrValue is Double || AttrValue is Boolean)
                    return true;
                double number;
                if (Double.TryParse(Convert.ToString(AttrValue, CultureInfo.InvariantCulture), NumberStyles.Any, NumberFormatInfo.InvariantInfo, out number))
                    return true;
                else
                    throw new Exception("Value '" + AttrValue.ToString() + "' is not a number");
            }
            catch (Exception e)
            {
                //   Logger.Add(e.Message);
                return false;
            }
        }



        //Generation Entity Object
        public static Entity GenerationEntity(IOrganizationService _orgService, Dictionary<string, EntityJson> objects, string TableName, DataRow dr)
        {

            var EntityObj = new Entity(objects[TableName].Name.ToLower());
            var EntityUniqueID = string.Empty;
            try
            {
                foreach (var attr in objects[TableName].Attrs)
                {
                    if (attr.Value.Header.ToLower() == (TableName + " GUID").ToLower())
                    {
                        EntityUniqueID = dr[attr.Key].ToString();
                    }
                    if ((attr.Value.Header.ToLower()).IndexOf("guid") != -1)
                    {
                    }
                    else if ((attr.Value.Header.ToLower()).IndexOf("template") != -1)
                    {
                    }
                    else if ((attr.Value.Header.ToLower()).IndexOf("milestone") != -1)
                    {
                    }
                    else {
                        if (dr[attr.Key].ToString() != "")
                        {
                            if (attr.Value.AttrType.ToString() != "Lookup")
                            {
                                dynamic tmpEntityObj = DDSM.Helper.Utils.CrmHelper.AttributeValue(_orgService, objects[TableName].Name.ToString().ToLower(), attr.Value.Name.ToString().ToLower(), attr.Value.AttrType.ToString(), dr[attr.Key], string.Empty);
                                if (tmpEntityObj != null)
                                {
                                    EntityObj[attr.Value.Name.ToLower()] = tmpEntityObj;
                                }
                            }
                            else {
                                dynamic tmpEntityObj = DDSM.Helper.Utils.CrmHelper.AttributeValue(_orgService, objects[TableName].Name.ToString().ToLower(), (attr.Value.Name.ToString().ToLower()).Split(':')[0], attr.Value.AttrType.ToString(), dr[attr.Key], (attr.Value.Name.ToLower()).Split(':')[1]);
                                if (tmpEntityObj != null)
                                {
                                    EntityObj[(attr.Value.Name.ToLower()).Split(':')[0]] = tmpEntityObj;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //   Logger.Add("Data of the '" + TableName + "'table doesn't correspond to a configuration template.");
                EntityObj = new Entity(objects[TableName].Name.ToLower());
                return EntityObj;
            }
            return EntityObj;

        }




        /// <summary>
        ///  Create Financials Collection
        /// </summary>
        /// <param name="_orgService"></param>
        /// <param name="ProjEntityUniqueID"></param>
        /// <param name="ProjTableName"></param>
        /// <param name="projTplID"></param>
        /// <param name="account"></param>
        /// <param name="site"></param>
        /// <param name="projStartDate"></param>
        /// <param name="objects"></param>
        /// <returns></returns>
        public static EntityCollection CreateFinancials(IOrganizationService _orgService, string ProjEntityUniqueID, string ProjTableName, string projTplID, Guid account, Guid site, DateTime projStartDate, Dictionary<string, EntityJson> objects)
        {
            var fnclCollection = new EntityCollection();

            var table = getDataTableByName(result, "ddsm_financial", objects);
            //if (table.Rows.Count == 0) return fnclCollection;

            var newEntiy = new Entity("ddsm_financial");
            //string uniqueIdColumnKey = objects[table.TableName].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (table.TableName + " GUID").ToLower()).Key;
            string projColumnKey = null;
            if (table.Rows.Count != 0)
                projColumnKey = objects[table.TableName].Attrs.FirstOrDefault(x => x.Value.Header.ToLower() == (ProjTableName + " GUID").ToLower()).Key;

            if (!string.IsNullOrEmpty(projTplID))
            {
                var relationship = new Relationship();
                relationship.SchemaName = "ddsm_ddsm_projecttemplate_ddsm_financialtemplate";

                var query = new QueryExpression();
                query.EntityName = "ddsm_financialtemplate";
                query.ColumnSet = new ColumnSet("ddsm_financialtemplateid");
                query.Criteria = new FilterExpression();
                query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

                var relatedEntity = new RelationshipQueryCollection();
                relatedEntity.Add(relationship, query);

                var request = new RetrieveRequest();
                request.ColumnSet = new ColumnSet("ddsm_projecttemplateid", "ddsm_name");
                request.Target = new EntityReference { Id = new Guid(projTplID), LogicalName = "ddsm_projecttemplate" };
                request.RelatedEntitiesQuery = relatedEntity;

                var response = (RetrieveResponse)_orgService.Execute(request);

                var RelatedEntitis = response.Entity.RelatedEntities[new Relationship("ddsm_ddsm_projecttemplate_ddsm_financialtemplate")].Entities;

                foreach (var RelatedEntity in RelatedEntitis)
                {
                    var rmEntity = GetDataRelationMapping(_orgService, "ddsm_financial", "ddsm_financialtemplate", new Guid(RelatedEntity["ddsm_financialtemplateid"].ToString()));
                    if (rmEntity != null)
                    {
                        foreach (var Attribute in rmEntity.Attributes)
                        {
                            newEntiy[Attribute.Key] = Attribute.Value;
                        }
                    }
                    //default 1 financial record
                    break;
                }
            }

            newEntiy["transactioncurrencyid"] = new EntityReference("transactioncurrency", CurrencyGuid);

            //Get relate fields Account
            var rmEntityAcc = GetDataRelationMapping(_orgService, "ddsm_financial", "account", account);
            foreach (var Attribute in rmEntityAcc.Attributes)
            {
                newEntiy[Attribute.Key] = Attribute.Value;
            }
            //Get relate fields Site
            var rmEntitySite = GetDataRelationMapping(_orgService, "ddsm_financial", "ddsm_site", site);
            foreach (var Attribute in rmEntitySite.Attributes)
            {
                newEntiy[Attribute.Key] = Attribute.Value;
            }

            if (!string.IsNullOrEmpty(projColumnKey))
            {
                var drs = table.Select(projColumnKey + " = '" + ProjEntityUniqueID + "'");

                foreach (var dr in drs)
                {
                    insertRecords++;
                    //default 1 financial record
                    var newEntiyXLSX = generationEntity(_orgService, objects, table.TableName, dr);
                    foreach (var Attribute in newEntiyXLSX.Attributes)
                    {
                        newEntiy[Attribute.Key] = Attribute.Value;
                    }

                    break;
                }
            }
            if (!newEntiy.Attributes.Contains("ddsm_status"))
                newEntiy["ddsm_status"] = new OptionSetValue(962080000);

            fnclCollection.Entities.Add(newEntiy);
            fnclCollection.EntityName = newEntiy.LogicalName;

            return fnclCollection;

        }


        /// <summary>
        /// Get excel DataTable by name
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="TableName"></param>
        /// <param name="objects"></param>
        /// <returns></returns>
        public static DataTable getDataTableByName(DataSet ds, string TableName, Dictionary<string, EntityJson> objects)
        {
            var dt = new DataTable();
            foreach (DataTable table in ds.Tables)
            {
                if (objects[table.TableName].Name.ToString().ToLower() == TableName)
                {
                    switch (TableName)
                    {
                        case "account":
                            AccountTableName = table.TableName;
                            ExistAccountTable = true;
                            break;
                        case "ddsm_site":
                            SiteTableName = table.TableName;
                            ExistSiteTable = true;
                            break;
                        case "contact":
                            ContactTableName = table.TableName;
                            ExistContactTable = true;
                            break;
                        case "ddsm_project":
                            ProjectTableName = table.TableName;
                            ExistProjectTable = true;
                            break;
                        case "ddsm_measure":
                            MeasureTableName = table.TableName;
                            ExistMeasureTable = true;
                            break;
                        case "ddsm_financial":
                            FinancialTableName = table.TableName;
                            ExistFinancialTable = true;
                            break;
                        case "ddsm_projectgroup":
                            ProjectGroupTableName = table.TableName;
                            ExistProjectGroupTable = true;
                            break;
                        case "ddsm_projectgroupfinancials":
                            ProjectGroupFinancialTableName = table.TableName;
                            ExistProjectGroupFinancialTable = true;
                            break;
                    }
                    dt = table;
                    break;
                }
            }
            return dt;
        }

        /// <summary>
        /// Create Milestones Collection
        /// </summary>
        /// <param name="_orgService"></param>
        /// <param name="projTplID"></param>
        /// <param name="projStartDate"></param>
        /// <returns></returns>
        public static EntityCollection CreateMilestones(IOrganizationService _orgService, Guid projTplID, DateTime projStartDate)
        {
            var msCollection = new EntityCollection();

            try
            {
                if (Guid.Empty != projTplID)
                {
                    //
                    var relationship = new Relationship
                    {
                        SchemaName = "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate"
                    };

                    var query = new QueryExpression
                    {
                        EntityName = "ddsm_milestonetemplate",
                        ColumnSet = new ColumnSet("ddsm_milestonetemplateid", "ddsm_index"),
                        Criteria = new FilterExpression()
                    };
                    query.AddOrder("ddsm_index", OrderType.Ascending);
                    query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

                    var relatedEntity = new RelationshipQueryCollection {{relationship, query}};

                    var request = new RetrieveRequest
                    {
                        ColumnSet = new ColumnSet("ddsm_projecttemplateid", "ddsm_name"),
                        Target = new EntityReference {Id = projTplID, LogicalName = "ddsm_projecttemplate"},
                        RelatedEntitiesQuery = relatedEntity
                    };

                    var response = (RetrieveResponse)_orgService.Execute(request);

                    var relatedEntities = response.Entity.RelatedEntities[new Relationship("ddsm_ddsm_projecttemplate_ddsm_milestonetemplate")].Entities;

                    foreach (var related in relatedEntities)
                    {
                        var ms = new Entity("ddsm_milestone");
                        var rmEntity = GetDataRelationMapping(_orgService, "ddsm_milestone", "ddsm_milestonetemplate", new Guid(related["ddsm_milestonetemplateid"].ToString()));
                        if (rmEntity == null) continue;
                        insertRecords++;
                        foreach (var attribute in rmEntity.Attributes)
                        {
                            ms[attribute.Key] = attribute.Value;
                        }
                        if (Convert.ToInt32(ms["ddsm_index"]) == 1)
                        {
                            ms["ddsm_actualstart"] = projStartDate;
                            ms["ddsm_status"] = new OptionSetValue(962080001);
                        }
                        else {
                            ms["ddsm_status"] = new OptionSetValue(962080000);
                        }
                        ms["ddsm_targetstart"] = projStartDate;
                        ms["ddsm_actualstart"] = projStartDate;
                        var addMilliseconds = projStartDate.AddMilliseconds(Convert.ToInt32(ms["ddsm_duration"]) * 86400000);
                        ms["ddsm_targetend"] = addMilliseconds;

                        ms["transactioncurrencyid"] = new EntityReference("transactioncurrency", CurrencyGuid);

                        msCollection.Entities.Add(ms);
                        msCollection.EntityName = ms.LogicalName;
                    }
                }
            }
            catch (Exception e)
            {
                msCollection.Entities.Add(new Entity("ddsm_milestone"));
                //  Logger.Add(e.Message);
            }
            return msCollection;
        }

        /// <summary>
        /// Get Business Process
        /// </summary>
        /// <param name="_orgService"></param>
        /// <param name="projTplName"></param>
        /// <param name="tracer"></param>
        /// <returns></returns>
        public static ProjectBusinessProcess GetBProcess(IOrganizationService _orgService, string projTplName, ITracingService tracer)
        {
            //tracer.Trace("getBProcess: projTplName: " + projTplName);
            var projBP = new ProjectBusinessProcess();
            //Get Project Template ID
            try
            {
                var processQuery = new QueryExpression("workflow");
                processQuery.ColumnSet = new ColumnSet("name", "workflowid");
                processQuery.ColumnSet = new ColumnSet(true);
                processQuery.Criteria.AddCondition("name", ConditionOperator.Equal, projTplName);
                processQuery.Criteria.AddCondition("primaryentity", ConditionOperator.Equal, "ddsm_project");
                //processQuery.Criteria.AddCondition("statecode", ConditionOperator.Equal, 1);
                var processRetrieve = _orgService.RetrieveMultiple(processQuery);
                if (processRetrieve != null && processRetrieve.Entities.Count == 1)
                {
                    projBP.Id = processRetrieve.Entities[0].Attributes["workflowid"].ToString();
                    projBP.Name = processRetrieve.Entities[0].Attributes["name"].ToString();
                    projBP.Stages = new List<ProjectBusinessProcessStage>();
                    var stageQuery = new QueryExpression("processstage");
                    stageQuery.ColumnSet = new ColumnSet("stagename", "processstageid");
                    //stageQuery.ColumnSet = new ColumnSet(true);
                    stageQuery.Criteria.AddCondition("processid", ConditionOperator.Equal, new Guid(projBP.Id));
                    var stageRetrieve = _orgService.RetrieveMultiple(stageQuery);
                    if (stageRetrieve != null && stageRetrieve.Entities.Count > 0)
                    {
                        foreach (var stage in stageRetrieve.Entities)
                        {
                            var stageObj = new ProjectBusinessProcessStage();
                            stageObj.Id = stage.Attributes["processstageid"].ToString();
                            stageObj.Name = stage.Attributes["stagename"].ToString();
                            projBP.Stages.Add(stageObj);
                        }
                    }
                }
                else {
                    throw new Exception(" not found. ");
                }
            }
            catch (Exception e)
            {
                //Logger.Add("Project Business Process '" + projTplName + "' " + e.Message);
            }
            return projBP;
        }



        //Get Currency
        public static void GetGuidofCurrency(IOrganizationService _orgService, ITracingService tracer)
        {
            tracer.Trace("GetGuidofCurrency");
         
            var queryCurrency = new QueryExpression("currency")
            {
                EntityName = "transactioncurrency",
                //                ColumnSet = new ColumnSet(new string[] { "transactioncurrencyid" }),
                ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression()
            };

            try
            {
                var entityData = _orgService.RetrieveMultiple(queryCurrency).Entities;
                var count = entityData.Count;

                if (count > 0)
                {
                    CurrencyGuid = entityData[0].Id;
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        #region SystemUser Operations
        //TODO: May be moved to dedicated class

        /// <summary>
        /// Get the GUID of the current system user
        /// </summary>
        /// <param name="_orgService"></param>
        /// <returns></returns>
        public static Guid GetCurretUserGuid(IOrganizationService _orgService)
        {
            WhoAmIRequest who = new WhoAmIRequest();
            WhoAmIResponse whoResp = (WhoAmIResponse)_orgService.Execute(who);
            return whoResp.UserId;
        }

        /// <summary>
        /// Check if user has a given role
        /// </summary>
        /// <param name="GivenRole"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static bool checkUserRole(IOrganizationService _orgService, string GivenRole, Guid? UserId)
        {
            if (UserId == null || UserId == Guid.Empty)
            {
                //Get Guid of Current User
                UserId = GetCurretUserGuid(_orgService);
            }

            // Find a role.
            QueryExpression query = new QueryExpression
            {
                EntityName = "role",
                ColumnSet = new ColumnSet("roleid"),
                Criteria = new FilterExpression
                {
                    Conditions =
                            {

                                new ConditionExpression
                                {
                                    AttributeName = "name",
                                    Operator = ConditionOperator.Equal,
                                    Values = {GivenRole}
                                }
                            }
                }
            };

            // Get the role.
            EntityCollection givenRoles = _orgService.RetrieveMultiple(query);

            if (givenRoles.Entities.Count > 0)
            {
                Entity givenRole = givenRoles.Entities[0];

                // Establish a SystemUser link for a query.
                LinkEntity systemUserLink = new LinkEntity()
                {
                    LinkFromEntityName = "systemuserroles",
                    LinkFromAttributeName = "systemuserid",
                    LinkToEntityName = "systemuser",
                    LinkToAttributeName = "systemuserid",
                    LinkCriteria =
                                {
                                    Conditions =
                                    {
                                        new ConditionExpression(
                                            "systemuserid", ConditionOperator.Equal, UserId)
                                    }
                                }
                };

                // Build the query.
                QueryExpression linkQuery = new QueryExpression()
                {
                    EntityName = "role",
                    ColumnSet = new ColumnSet("roleid"),
                    LinkEntities =
                                {
                                    new LinkEntity()
                                    {
                                        LinkFromEntityName = "role",
                                        LinkFromAttributeName = "roleid",
                                        LinkToEntityName = "systemuserroles",
                                        LinkToAttributeName = "roleid",
                                        LinkEntities = {systemUserLink}
                                    }
                                },
                    Criteria =
                                {
                                    Conditions =
                                    {
                                        new ConditionExpression("roleid", ConditionOperator.Equal, givenRole.Id)
                                    }
                                }
                };

                // Retrieve matching roles.
                EntityCollection matchEntities = _orgService.RetrieveMultiple(linkQuery);

                // if an entity is returned then the user is a member
                // of the role
                return (matchEntities.Entities.Count > 0);
            }
            return false;
        }
    }

    #endregion
}
