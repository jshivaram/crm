﻿using System;

namespace DDSM.Helper.Utils
{
  public static class Utils
    {
        public static dynamic ConvertAttributToDateTimeUtc(object xlsxDate)
        {
            try
            {
                if (xlsxDate.GetType().Name == "DateTime")
                {
                    return xlsxDate;
                }
                else if (xlsxDate.GetType().Name == "Double")
                {
                    return DateTime.FromOADate(Convert.ToDouble(xlsxDate)).ToUniversalTime();
                }
                else if (xlsxDate.GetType().Name == "String")
                {
                    if (xlsxDate.ToString() == "")
                    {
                        return null;
                    }
                    else if (xlsxDate.ToString() == "today")
                    {
                        return DateTime.UtcNow;
                    }
                    else {
                        return Convert.ToDateTime(xlsxDate.ToString()).ToUniversalTime();
                    }
                }
                else {
                  //  Logger.Add("Error converting the values of '" + xlsxDate.ToString() + "' in the type Date Time.");
                    return null;
                }
            }
            catch (Exception e)
            {
             //   Logger.Add("Error converting the values of '" + xlsxDate.ToString() + "' in the type Date Time." + e.Message);
                return null;
            }
        }

    }
}
