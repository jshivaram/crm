﻿using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using DDSM_CalculationProgramInterval;
using DDSM_CalculationProgramInterval.Utils;
using System;
using System.Collections.Generic;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk.Query;

/// <summary>
///  Plugin for calculation ProgramOffering totals field from workflow that runing from ProgramInterval entity
/// </summary>
public class CalculateParentProgramOfferingTotalsField : CodeActivity
{
    #region "Parameter Definition"

    //[RequiredArgument] //Commented to make available to change
    [Input("Parent Program Offerings")]
    [ArgumentEntity("ddsm_programoffering")]
    [ReferenceTarget("ddsm_programoffering")]
    public InArgument<EntityReference> ProgramOffering { get; set; }


    [Input("Program")]
    [ArgumentEntity("ddsm_program")]
    [ReferenceTarget("ddsm_program")]
    public InArgument<EntityReference> Program { get; set; }
    
    [Input("Portfolio")]
    [ArgumentEntity("ddsm_portfolio")]
    [ReferenceTarget("ddsm_portfolio")]
    public InArgument<EntityReference> Portfolio { get; set; }

    [Input("User Input")]
    public InArgument<string> UserInput { get; set; }

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }

    #endregion

    #region Internal fields
    Common _objCommon;
    IOrganizationService _service;
    readonly string _ddsmProgramoffering = "ddsm_programoffering";
    #endregion

    protected override void Execute(CodeActivityContext context)
    {
        try
        {
            #region "Load CRM Service from context"

            _objCommon = new Common(context);
            _service = _objCommon.GetOrgService();
            _objCommon.TracingService.Trace("Load CRM Service from context --- OK");
            #endregion         

            var updateLookups = new List<EntityReference>
                {
                    {ProgramOffering.Get(context)},
                    {Program.Get(context)},
                    {Portfolio.Get(context)},
                };

            foreach (var item in updateLookups)
            {
                if (item == null)
                {
                    continue;
                }
                Entity itemEnt = GetEntityFromRef(item);
                //get totals of program Int
                EntityCollection totals = GetProgramIntervalsTotals(item);

                UpdateTotals(itemEnt, totals);

            }          

            Complete.Set(context, true);
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("Error of ProgramInterval totals calculation. Error:" + ex.Message);
            Complete.Set(context, false);
        }
    }

    #region Internal methods
    private void UpdateTotals(Entity updLookup, EntityCollection totals)
    {
        try
        {
            if (totals == null) throw new Exception("Program Interval Totals are null.");

            foreach (var total in totals.Entities)
            {
                foreach (var column in total.Attributes)
                {
                    updLookup[column.Key.ToLower()] = column.Value.GetRecordValue();
                }
            }
            _service.Update(updLookup);
        }
        catch (Exception ex)
        {
            //_objCommon.TracingService.Trace("Error On Program Offering totals update. Exeption:" + ex.Message);
            string msgStart;
            if (updLookup.LogicalName == "ddsm_programoffering")
            {
                msgStart = "Error On Program Offering totals update. Exeption:";
            }
            else if (updLookup.LogicalName == "ddsm_program")
            {
                msgStart = "Error On Program totals update. Exeption:";
            }
            else 
            {
                msgStart = "Error On Portfolio totals update. Exeption:";
            }
            _objCommon.TracingService.Trace( msgStart + ex.Message);
        }
    }

    //EntityCollection GetProgramIntervalsTotals(EntityReference programOffering, EntityReference program, EntityReference portfolio)
    private EntityCollection GetProgramIntervalsTotals(EntityReference updateEntity)
    {
        if(updateEntity == null)
        {
            return null;
        }

        //var condPattern = "<condition attribute='ddsm_programid' operator='eq' value='{program.Id.ToString()}' />";
        var condPattern = "<condition attribute='{0}' operator='eq' value='{1}' />";

        string rollupTarget = updateEntity.LogicalName;
        string condStr;

        //TODO: must be rebuilt if the uniq key may not equal to logicalName + 'id';
        if (rollupTarget == "ddsm_programoffering")
        {
            condStr = string.Format(condPattern, "ddsm_parentprogramofferingsid", updateEntity.Id.ToString());
            //condStr = string.Format(condPattern, "ddsm_programofferingsid", updateEntity.Id.ToString());
            condStr += string.Format(condPattern, "ddsm_programmofferingrollup", 1);
        }
        else
        {
            condStr = string.Format(condPattern, rollupTarget + "id", updateEntity.Id.ToString());
            condStr += string.Format(condPattern, rollupTarget + "rollup", 1);
        }


        var result = new EntityCollection();

        var queryMeasureTotals =
            @"<fetch aggregate='true' >
                  <entity name='ddsm_kpiinterval' >
                    <attribute name='ddsm_targetkwhgrosssavingsatmeter' alias='ddsm_TargetkWhGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_targetkwhgrosssavingsatgenerator' alias='ddsm_TargetkWhGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_targetkwhnetsavingsatgenerator' alias='ddsm_TargetkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_targetkwgrosssavingsatmeter' alias='ddsm_TargetkWGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_targetkwgrosssavingsatgenerator' alias='ddsm_TargetkWGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_targetkwnetsavingsatgenerator' alias='ddsm_TargetkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_targetgjsavingsatmeter' alias='ddsm_TargetGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_targetm3savings' alias='ddsm_TargetM3Savings' aggregate='sum' />
                    <attribute name='ddsm_targetwatersavings' alias='ddsm_TargetWaterSavings' aggregate='sum' />
                    <attribute name='ddsm_1phasekwhgrosssavingsatmeter' alias='ddsm_1PhasekWhGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_1phasekwhgrosssavingsatgenerator' alias='ddsm_1PhasekWhGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_1phasekwhnetsavingsatgenerator' alias='ddsm_1PhasekWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_1phasekwgrosssavingsatmeter' alias='ddsm_1PhasekWGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_1phasekwgrosssavingsatgenerator' alias='ddsm_1PhasekWGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_1phasekwnetsavingsatgenerator' alias='ddsm_1PhasekWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_1phasegjsavingsatmeter' alias='ddsm_1PhaseGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_1phasem3savings' alias='ddsm_1PhaseM3Savings' aggregate='sum' />
                    <attribute name='ddsm_1phasewatersavings' alias='ddsm_1PhaseWaterSavings' aggregate='sum' />
                    <attribute name='ddsm_2phasekwhgrosssavingsatmeter' alias='ddsm_2PhasekWhGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_2phasekwhgrosssavingsatgenerator' alias='ddsm_2PhasekWhGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_2phasekwhnetsavingsatgenerator' alias='ddsm_2PhasekWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_2phasekwgrosssavingsatmeter' alias='ddsm_2PhasekWGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_2phasekwgrosssavingsatgenerator' alias='ddsm_2PhasekWGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_2phasekwnetsavingsatgenerator' alias='ddsm_2PhasekWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_2phasegjsavingsatmeter' alias='ddsm_2PhaseGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_2phasem3savings' alias='ddsm_2PhaseM3Savings' aggregate='sum' />
                    <attribute name='ddsm_2phasewatersavings' alias='ddsm_2PhaseWaterSavings' aggregate='sum' />
                    <attribute name='ddsm_3phasekwhgrosssavingsatmeter' alias='ddsm_3PhasekWhGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_3phasekwhgrosssavingsatgenerator' alias='ddsm_3PhasekWhGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_3phasekwhnetsavingsatgenerator' alias='ddsm_3PhasekWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_3phasekwgrosssavingsatmeter' alias='ddsm_3PhasekWGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_3phasekwgrosssavingsatgenerator' alias='ddsm_3PhasekWGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_3phasekwnetsavingsatgenerator' alias='ddsm_3PhasekWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_3phasegjsavingsatmeter' alias='ddsm_3PhaseGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_3phasem3savings' alias='ddsm_3PhaseM3Savings' aggregate='sum' />
                    <attribute name='ddsm_3phasewatersavings' alias='ddsm_3PhaseWaterSavings' aggregate='sum' />
                    <attribute name='ddsm_actualallkwhgrosssavingsatmeter' alias='ddsm_ActualAllkWhGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_actualallkwhgrosssavingsatgenerator' alias='ddsm_ActualAllkWhGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_actualallkwhnetsavingsatgenerator' alias='ddsm_ActualAllkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_actualallkwgrosssavingsatmeter' alias='ddsm_ActualAllkWGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_actualallkwgrosssavingsatgenerator' alias='ddsm_ActualAllkWGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_actualallkwnetsavingsatgenerator' alias='ddsm_ActualAllkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_actualallm3savings' alias='ddsm_ActualAllM3Savings' aggregate='sum' />
                    <attribute name='ddsm_actualallwatersavings' alias='ddsm_ActualAllWaterSavings' aggregate='sum' />
                    <attribute name='ddsm_budgetincentivepaymentnet' alias='ddsm_BudgetIncentivePaymentNet' aggregate='sum' />
                    <attribute name='ddsm_budgetincentivecosttotal' alias='ddsm_BudgetIncentiveCostTotal' aggregate='sum' />
                    <attribute name='ddsm_budgettotalincentive' alias='ddsm_BudgetTotalIncentive' aggregate='sum' />
                    <attribute name='ddsm_1phaseincentivepaymentnet' alias='ddsm_1PhaseIncentivePaymentNet' aggregate='sum' />
                    <attribute name='ddsm_1phaseincentivecosttotal' alias='ddsm_1PhaseIncentiveCostTotal' aggregate='sum' />
                    <attribute name='ddsm_1phasetotalincentive' alias='ddsm_1PhaseTotalIncentive' aggregate='sum' />
                    <attribute name='ddsm_2phaseincentivepaymentnet' alias='ddsm_2PhaseIncentivePaymentNet' aggregate='sum' />
                    <attribute name='ddsm_2phaseincentivecosttotal' alias='ddsm_2PhaseIncentiveCostTotal' aggregate='sum' />
                    <attribute name='ddsm_2phasetotalincentive' alias='ddsm_2PhaseTotalIncentive' aggregate='sum' />
                    <attribute name='ddsm_3phaseincentivepaymentnet' alias='ddsm_3PhaseIncentivePaymentNet' aggregate='sum' />
                    <attribute name='ddsm_3phaseincentivecosttotal' alias='ddsm_3PhaseIncentiveCostTotal' aggregate='sum' />
                    <attribute name='ddsm_3phasetotalincentive' alias='ddsm_3PhaseTotalIncentive' aggregate='sum' />
                    <attribute name='ddsm_actualallincentivepaymentnet' alias='ddsm_ActualAllIncentivePaymentNet' aggregate='sum' />
                    <attribute name='ddsm_actualallincentivecosttotal' alias='ddsm_ActualAllIncentiveCostTotal' aggregate='sum' />
                    <attribute name='ddsm_actualalltotalincentive' alias='ddsm_ActualAllTotalIncentive' aggregate='sum' />
                    <attribute name='ddsm_targetadministrationcosts' alias='ddsm_TargetAdministrationCosts' aggregate='sum' />
                    <attribute name='ddsm_targetcustomereducationcosts' alias='ddsm_TargetCustomerEducationCosts' aggregate='sum' />
                    <attribute name='ddsm_targetevaluationcosts' alias='ddsm_TargetEvaluationCosts' aggregate='sum' />
                    <attribute name='ddsm_targetmarketingcosts' alias='ddsm_TargetMarketingCosts' aggregate='sum' />
                    <attribute name='ddsm_targetpartnertrainingcosts' alias='ddsm_TargetPartnerTrainingCosts' aggregate='sum' />
                    <attribute name='ddsm_targetprogramdesigncosts' alias='ddsm_TargetProgramDesignCosts' aggregate='sum' />
                    <attribute name='ddsm_budgetadministrationcosts' alias='ddsm_BudgetAdministrationCosts' aggregate='sum' />
                    <attribute name='ddsm_budgetcustomereducationcosts' alias='ddsm_BudgetCustomerEducationCosts' aggregate='sum' />
                    <attribute name='ddsm_budgetevaluationcosts' alias='ddsm_BudgetEvaluationCosts' aggregate='sum' />
                    <attribute name='ddsm_budgetmarketingcosts' alias='ddsm_BudgetMarketingCosts' aggregate='sum' />
                    <attribute name='ddsm_budgetpartnertrainingcosts' alias='ddsm_BudgetPartnerTrainingCosts' aggregate='sum' />
                    <attribute name='ddsm_budgetprogramdesigncosts' alias='ddsm_BudgetProgramDesignCosts' aggregate='sum' />
                    <attribute name='ddsm_actualalladministrationcosts' alias='ddsm_ActualAllAdministrationCosts' aggregate='sum' />
                    <attribute name='ddsm_actualallcustomereducationcosts' alias='ddsm_ActualAllCustomerEducationCosts' aggregate='sum' />
                    <attribute name='ddsm_actualallevaluationcosts' alias='ddsm_ActualAllEvaluationCosts' aggregate='sum' />
                    <attribute name='ddsm_actualallmarketingcosts' alias='ddsm_ActualAllMarketingCosts' aggregate='sum' />
                    <attribute name='ddsm_actualallpartnertrainingcosts' alias='ddsm_ActualAllPartnerTrainingCosts' aggregate='sum' />
                    <attribute name='ddsm_actualallprogramdesigncosts' alias='ddsm_ActualAllProgramDesignCosts' aggregate='sum' />
                    <attribute name='ddsm_actualallcountparticipants' alias='ddsm_ActualAllCountParticipants' aggregate='sum' />
                    <attribute name='ddsm_actualallcountnewparticipants' alias='ddsm_ActualAllCountNewParticipants' aggregate='sum' />
                    <attribute name='ddsm_actualallcountopportunities' alias='ddsm_ActualAllCountOpportunities' aggregate='sum' />
                    <attribute name='ddsm_actualallcountprojects' alias='ddsm_ActualAllCountProjects' aggregate='sum' />
                    <attribute name='ddsm_targetcountparticipants' alias='ddsm_TargetCountParticipants' aggregate='sum' />
                    <attribute name='ddsm_targetcountnewparticipants' alias='ddsm_TargetCountNewParticipants' aggregate='sum' />
                    <attribute name='ddsm_targetcountopportunities' alias='ddsm_TargetCountOpportunities' aggregate='sum' />
                    <attribute name='ddsm_targetcountprojects' alias='ddsm_TargetCountProjects' aggregate='sum' />
                    <attribute name='ddsm_1phasecountparticipants' alias='ddsm_1PhaseCountParticipants' aggregate='sum' />
                    <attribute name='ddsm_1phasecountnewparticipants' alias='ddsm_1PhaseCountNewParticipants' aggregate='sum' />
                    <attribute name='ddsm_1phasecountopportunities' alias='ddsm_1PhaseCountOpportunities' aggregate='sum' />
                    <attribute name='ddsm_1phasecountprojects' alias='ddsm_1PhaseCountProjects' aggregate='sum' />
                    <attribute name='ddsm_2phasecountparticipants' alias='ddsm_2PhaseCountParticipants' aggregate='sum' />
                    <attribute name='ddsm_2phasecountnewparticipants' alias='ddsm_2PhaseCountNewParticipants' aggregate='sum' />
                    <attribute name='ddsm_2phasecountopportunities' alias='ddsm_2PhaseCountOpportunities' aggregate='sum' />
                    <attribute name='ddsm_2phasecountprojects' alias='ddsm_2PhaseCountProjects' aggregate='sum' />
                    <attribute name='ddsm_3phasecountparticipants' alias='ddsm_3PhaseCountParticipants' aggregate='sum' />
                    <attribute name='ddsm_3phasecountnewparticipants' alias='ddsm_3PhaseCountNewParticipants' aggregate='sum' />
                    <attribute name='ddsm_3phasecountopportunities' alias='ddsm_3PhaseCountOpportunities' aggregate='sum' />
                    <attribute name='ddsm_3phasecountprojects' alias='ddsm_3PhaseCountProjects' aggregate='sum' />
                    <attribute name='ddsm_targettotalindirectcost' alias='ddsm_TargetTotalIndireCtcost' aggregate='sum' />
                    <attribute name='ddsm_budgettotalindirectcost' alias='ddsm_BudgetTotalIndireCtcost' aggregate='sum' />
                    <attribute name='ddsm_actualalltotalindirectcost' alias='ddsm_ActualAllTotalIndirectCost' aggregate='sum' />
                    <attribute name='ddsm_1phaseavoidedgascost' alias='ddsm_1PhaseAvoidedGasCost' aggregate='sum' />
                    <attribute name='ddsm_1phaseavoidedelectriccost' alias='ddsm_1PhaseAvoidedElectricCost' aggregate='sum' />
                    <attribute name='ddsm_1phaseavoidedwatercost' alias='ddsm_1PhaseAvoidedWaterCost' aggregate='sum' />
                    <attribute name='ddsm_1phasetotaltrcbenefit' alias='ddsm_1PhaseTotalTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_1phasetotaltrccost' alias='ddsm_1PhaseTotalTRCCost' aggregate='sum' />
                    <attribute name='ddsm_1phasenettrcbenefit' alias='ddsm_1PhaseNetTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_2phaseavoidedgascost' alias='ddsm_2PhaseAvoidedGasCost' aggregate='sum' />
                    <attribute name='ddsm_2phaseavoidedelectriccost' alias='ddsm_2PhaseAvoidedElectricCost' aggregate='sum' />
                    <attribute name='ddsm_2phaseavoidedwatercost' alias='ddsm_2PhaseAvoidedWaterCost' aggregate='sum' />
                    <attribute name='ddsm_2phasetotaltrcbenefit' alias='ddsm_2PhaseTotalTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_2phasetotaltrccost' alias='ddsm_2PhaseTotalTRCCost' aggregate='sum' />
                    <attribute name='ddsm_2phasenettrcbenefit' alias='ddsm_2PhaseNetTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_3phaseavoidedgascost' alias='ddsm_3PhaseAvoidedGasCost' aggregate='sum' />
                    <attribute name='ddsm_3phaseavoidedelectriccost' alias='ddsm_3PhaseAvoidedElectricCost' aggregate='sum' />
                    <attribute name='ddsm_3phaseavoidedwatercost' alias='ddsm_3PhaseAvoidedWaterCost' aggregate='sum' />
                    <attribute name='ddsm_3phasetotaltrcbenefit' alias='ddsm_3PhaseTotalTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_3phasetotaltrccost' alias='ddsm_3PhaseTotalTRCCost' aggregate='sum' />
                    <attribute name='ddsm_3phasenettrcbenefit' alias='ddsm_3PhaseNetTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_actualallavoidedgascost' alias='ddsm_ActualAllAvoidedGasCost' aggregate='sum' />
                    <attribute name='ddsm_actualallavoidedelectriccost' alias='ddsm_ActualAllAvoidedElectricCost' aggregate='sum' />
                    <attribute name='ddsm_actualallavoidedwatercost' alias='ddsm_ActualAllAvoidedWaterCost' aggregate='sum' />
                    <attribute name='ddsm_actualalltotaltrcbenefit' alias='ddsm_ActualAllTotalTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_actualalltotaltrccost' alias='ddsm_ActualAllTotalTRCCost' aggregate='sum' />
                    <attribute name='ddsm_actualallnettrcbenefit' alias='ddsm_ActualAllNetTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_actualallgjsavingsatmeter' alias='ddsm_ActualAllGJSavingsatMeter' aggregate='sum' />
                    {0}
                    <filter>
                    <condition attribute='statecode' operator='eq' value='0' />
                    <condition attribute='statuscode' operator='eq' value='1' /> 
                    {1}
                </filter>
              </entity>
            </fetch>";


        var dsm_pnsFields = GetDsmPnsFields(rollupTarget);

        //queryMeasureTotals = string.Format(queryMeasureTotals, programOffCond);
        queryMeasureTotals = string.Format(queryMeasureTotals, dsm_pnsFields, condStr);
        var queryMeasureExpression = new FetchExpression(queryMeasureTotals);

        var totals = _service.RetrieveMultiple(queryMeasureExpression);

        result.Entities.Add(totals.Entities[0]);

        return result;

    }

    /*----OBSOLETE----*/
    private Entity GetProgramOffering(Guid id)
    {
        return _service.Retrieve(_ddsmProgramoffering, id, new ColumnSet("ddsm_programofferingid"));
    }

    private Entity GetEntityFromRef(EntityReference item)
    {
        if (item == null)
        {
            return null;
        }
        var idField = item.LogicalName + "id";

        return _service.Retrieve(item.LogicalName, item.Id, new ColumnSet(idField));
    }


    private string GetDsmPnsFields(string entName)
    {
        var result = "";
        var attrTpl = "<attribute name='{0}' alias='{1}' aggregate='sum' />";
        var isProgOff = (entName == "ddsm_programoffering");


        var fields = new Dictionary<string, string>{
            {"ddsm_budget","ddsm_Budget" },
            {"ddsm_numberofparticipants","ddsm_NumberofParticipants" },
            {"ddsm_estimatedkwhnetsavingsatgenerator","ddsm_EstimatedkWhNetSavingsatGenerator" },
            {"ddsm_estimatedkwnetsavingsatgenerator","ddsm_EstimatedkWNetSavingsatGenerator" },
            {"ddsm_estimatedspending","ddsm_EstimatedSpending" },
            {"ddsm_directprogramunitcost","ddsm_DirectProgramUnitCost" },
            {"ddsm_forecastedkwhnetsavingsatgenerator","ddsm_ForecastedkWhNetSavingsatGenerator" },
            {"ddsm_forecastedkwnetsavingsatgenerator","ddsm_ForecastedkWNetSavingsatGenerator" },
            {"ddsm_forecastedspending","ddsm_ForecastedSpending" },
            {"ddsm_actualspending","ddsm_ActualSpending" },
            {"ddsm_pnsbudget","ddsm_PNSBudget" },
            {"ddsm_pnsnumberofparticipants","ddsm_PNSNumberofParticipants" },
            {"ddsm_estimatedgjsavingsatmeter","ddsm_EstimatedGJSavingsatMeter" },
            {"ddsm_pnsestimatedspending","ddsm_PNSEstimatedSpending" },
            {"ddsm_forecastedgjsavingsatmeter","ddsm_ForecastedGJSavingsatMeter" },
            {"ddsm_pnsforecastedspending","ddsm_PNSForecastedSpending" },
            {"ddsm_pnsactualspending","ddsm_PNSActualSpending" },

        };


        if (isProgOff)
        {
            fields.Add("ddsm_targetkwhnetsavingsatgenerator", "ddsm_TargetkWhNetSavingsatGeneratorDSM");
            fields.Add("ddsm_targetkwnetsavingsatgenerator", "ddsm_TargetkWNetSavingsatGeneratorDSM");
            fields.Add("ddsm_targetgjsavingsatmeter", "ddsm_TargetGJSavingsatMeterPNS" );
            fields.Add("ddsm_actualallkwhnetsavingsatgenerator", "ddsm_ActualAllkWhNetSavingsatGeneratorDSM" );
            fields.Add("ddsm_actualallkwnetsavingsatgenerator", "ddsm_ActualAllkWNetSavingsatGeneratorDSM" );
        }

        foreach (var item in fields)
        {
            result += string.Format(attrTpl, item.Key, item.Value);
        }


        return result;
    }

    #endregion Internal methods
}

