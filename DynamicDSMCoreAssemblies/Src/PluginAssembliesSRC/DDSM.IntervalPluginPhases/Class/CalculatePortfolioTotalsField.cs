﻿using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using DDSM_CalculationProgramInterval;
using DDSM_CalculationProgramInterval.Utils;
using System;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using DDSM.SmartMeasureCalculationPlugin.Model;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using DDSM.CalculationPlugin.Utils;
using DDSM.CommonProvider;

/// <summary>
///  Plugin for calculation Program totals field from workflow that runing from ProgramOffering entity
/// </summary>
public class CalculatePortfolioTotalsField : CodeActivity
{
    #region "Parameter Definition"

    //[RequiredArgument]
    //[Input("Program Offerings")]
    //[ArgumentEntity("ddsm_programoffering")]
    //[ReferenceTarget("ddsm_programoffering")]
    //public InArgument<EntityReference> ProgramOffering { get; set; }

    [Input("Portfolio")]
    [ArgumentEntity("ddsm_portfolio")]
    [ReferenceTarget("ddsm_portfolio")]
    public InArgument<EntityReference> Portfolio { get; set; }

    [Input("User Input")]
    public InArgument<string> UserInput { get; set; }

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }


    #endregion

    #region Internal fields
    Common _objCommon;
    IOrganizationService _service;
    readonly string _ddsmPortfolio = "ddsm_portfolio";
    ExecuteMultipleRequest requestWithResults;
    private List<Guid> processedPrograms = new List<Guid>();
    #endregion

    protected override void Execute(CodeActivityContext context)
    {
        #region "Load CRM Service from context"

        _objCommon = new Common(context);
        _service = _objCommon.GetOrgService();
        _objCommon.TracingService.Trace("Load CRM Service from context --- OK");
        #endregion

        var portf = Portfolio.Get(context);
        //Target Entity
        EntityReference Target = (EntityReference)_objCommon.Context.InputParameters["Target"];

        string inputP = null;
        string inputTargetId = null;

        _objCommon.TracingService.Trace("Iput Parameters:");
        //Target ID
        if (Target != null)
        {
            inputTargetId = Target.Id.ToString();
            _objCommon.TracingService.Trace("Iput Parameters. Target ID: " + inputTargetId);
        }
        else { _objCommon.TracingService.Trace("Target ID is null"); }
        //Program
        if (portf != null)
        {
            inputP = portf.Id.ToString();
            _objCommon.TracingService.Trace("Portfolio: " + inputP);
        }
        else { _objCommon.TracingService.Trace("Portfolio is null"); }

        requestWithResults = new ExecuteMultipleRequest()
        {
            // Assign settings that define execution behavior: continue on error, return responses. 
            Settings = new ExecuteMultipleSettings()
            {
                ContinueOnError = false,
                ReturnResponses = true
            },
            // Create an empty organization request collection.
            Requests = new OrganizationRequestCollection()
        };
        try
        {
            var listId = new List<Guid>();
            // resolve parameters
            // var programOfferingRef = ProgramOffering.Get(context);
            var portfolioRef = Portfolio.Get(context);
            // get all prog offerings that must be totals updated

            //get totals of program Int
            var totals = GetProgramTotals(portfolioRef);
            var userInput = UserInput.Get(context);

            if (!string.IsNullOrEmpty(userInput) && portfolioRef == null)
            {
                UserInputObj2 userInpObj = null;

                userInpObj = JsonConvert.DeserializeObject<UserInputObj2>(userInput);
                if (userInpObj?.SmartMeasures != null)
                {
                    listId = userInpObj.SmartMeasures.Where(x => !x.Equals(Guid.Empty)).Distinct().ToList();
                }
            }
            else if (portfolioRef != null)
            {
                /*var portfolio = GetPortfolio(portfolioRef.Id);
                UpdateTotals(portfolio, totals);*/
                listId.Add(portfolioRef.Id);
            }
            else if (portfolioRef == null)
            {
                if (Target != null)
                {
                    listId.Add(Target.Id);
                }
            }
            else
            {
                throw new Exception("Portfolio parameter is null. Please check it.");
            }

            foreach (var portfilio in listId)
            {
                var portfolio = GetPortfolio(portfilio);
                UpdateTotals(portfolio, totals);
            }

            if (requestWithResults.Requests.Count > 0)
            {
                ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_service.Execute(requestWithResults);
                if (responseWithResults.IsFaulted)
                {
                    _objCommon.TracingService.Trace("Update of Portfolio failed! Check correct names fileds or fetch. ");
                }
                else
                {
                    _objCommon.TracingService.Trace(requestWithResults.Requests.Count + " updates was done. Portfolios was updated.");
                }
            }
            else
            {
                _objCommon.TracingService.Trace("Portfolios not updated. No items to update.");
            }
            //if (requestWithResults.Requests.Count > 0)
            //{
            //    ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_service.Execute(requestWithResults);
            //}

            //processCollectedData();
            Complete.Set(context, true);
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("Error of Portfolio totals calculation. Error:" + ex.Message);
            Complete.Set(context, false);
        }
    }

    #region Internal methods
    private void UpdateTotals(Entity portfolio, EntityCollection totals)
    {
        try
        {
            if (totals == null) throw new Exception("Program Interval Totals are null.");
            foreach (var total in totals.Entities)
            {
                foreach (var column in total.Attributes)
                {
                    portfolio[column.Key.ToLower()] = column.Value.GetRecordValue();
                }
            }
            object dsmplan = null;
            if (portfolio.Attributes.TryGetValue("ddsm_dsmplanid", out dsmplan))
                processedPrograms.Add((dsmplan as EntityReference).Id);

            var updateRequest = new UpdateRequest { Target = portfolio };
            requestWithResults.Requests.Add(updateRequest);
            //  _service.Update(offering);
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("Error On Portfolio totals update. Exeption:" + ex.Message);
        }

    }

    private EntityCollection GetProgramTotals(EntityReference portfolio)
    {
        var result = new EntityCollection();

        var programCond = (portfolio != null && !string.IsNullOrEmpty(portfolio.Id.ToString())) ?
            $"<condition attribute='ddsm_portfolioid' operator='eq' value='{portfolio.Id.ToString()}' />"
            : "";

        var queryMeasureTotals =
            @"<fetch aggregate='true' >
                  <entity name='ddsm_program' >
                    <attribute name='ddsm_targetkwhgrosssavingsatmeter' alias='ddsm_TargetkWhGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_targetkwhgrosssavingsatgenerator' alias='ddsm_TargetkWhGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_targetkwhnetsavingsatgenerator' alias='ddsm_TargetkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_targetkwgrosssavingsatmeter' alias='ddsm_TargetkWGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_targetkwgrosssavingsatgenerator' alias='ddsm_TargetkWGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_targetkwnetsavingsatgenerator' alias='ddsm_TargetkWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_targetgjsavingsatmeter' alias='ddsm_TargetGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_targetm3savings' alias='ddsm_TargetM3Savings' aggregate='sum' />
                    <attribute name='ddsm_targetwatersavings' alias='ddsm_TargetWaterSavings' aggregate='sum' />
                    <attribute name='ddsm_1phasekwhgrosssavingsatmeter' alias='ddsm_1PhasekWhGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_1phasekwhgrosssavingsatgenerator' alias='ddsm_1PhasekWhGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_1phasekwhnetsavingsatgenerator' alias='ddsm_1PhasekWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_1phasekwgrosssavingsatmeter' alias='ddsm_1PhasekWGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_1phasekwgrosssavingsatgenerator' alias='ddsm_1PhasekWGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_1phasekwnetsavingsatgenerator' alias='ddsm_1PhasekWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_1phasegjsavingsatmeter' alias='ddsm_1PhaseGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_1phasem3savings' alias='ddsm_1PhaseM3Savings' aggregate='sum' />
                    <attribute name='ddsm_1phasewatersavings' alias='ddsm_1PhaseWaterSavings' aggregate='sum' />
                    <attribute name='ddsm_2phasekwhgrosssavingsatmeter' alias='ddsm_2PhasekWhGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_2phasekwhgrosssavingsatgenerator' alias='ddsm_2PhasekWhGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_2phasekwhnetsavingsatgenerator' alias='ddsm_2PhasekWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_2phasekwgrosssavingsatmeter' alias='ddsm_2PhasekWGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_2phasekwgrosssavingsatgenerator' alias='ddsm_2PhasekWGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_2phasekwnetsavingsatgenerator' alias='ddsm_2PhasekWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_2phasegjsavingsatmeter' alias='ddsm_2PhaseGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_2phasem3savings' alias='ddsm_2PhaseM3Savings' aggregate='sum' />
                    <attribute name='ddsm_2phasewatersavings' alias='ddsm_2PhaseWaterSavings' aggregate='sum' />
                    <attribute name='ddsm_3phasekwhgrosssavingsatmeter' alias='ddsm_3PhasekWhGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_3phasekwhgrosssavingsatgenerator' alias='ddsm_3PhasekWhGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_3phasekwhnetsavingsatgenerator' alias='ddsm_3PhasekWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_3phasekwgrosssavingsatmeter' alias='ddsm_3PhasekWGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_3phasekwgrosssavingsatgenerator' alias='ddsm_3PhasekWGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_3phasekwnetsavingsatgenerator' alias='ddsm_3PhasekWNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_3phasegjsavingsatmeter' alias='ddsm_3PhaseGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_3phasem3savings' alias='ddsm_3PhaseM3Savings' aggregate='sum' />
                    <attribute name='ddsm_3phasewatersavings' alias='ddsm_3PhaseWaterSavings' aggregate='sum' />
                    <attribute name='ddsm_actualallkwhgrosssavingsatmeter' alias='ddsm_ActualAllkWhGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_actualallkwhgrosssavingsatgenerator' alias='ddsm_ActualAllkWhGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_actualallkwhnetsavingsatgenerator' alias='ddsm_ActualAllkWhNetSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_actualallkwgrosssavingsatmeter' alias='ddsm_ActualAllkWGrossSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_actualallkwgrosssavingsatgenerator' alias='ddsm_ActualAllkWGrossSavingsatGenerator' aggregate='sum' />
                    <attribute name='ddsm_actualallkwnetsavingsatgenerator' alias='ddsm_ActualAllkWNetSavingsatGenerator' aggregate='sum' />
		            <attribute name='ddsm_actualallgjsavingsatmeter' alias='ddsm_ActualAllGJSavingsatMeter' aggregate='sum' />
                    <attribute name='ddsm_actualallm3savings' alias='ddsm_ActualAllM3Savings' aggregate='sum' />
                    <attribute name='ddsm_actualallwatersavings' alias='ddsm_ActualAllWaterSavings' aggregate='sum' />
                    <attribute name='ddsm_budgetincentivepaymentnet' alias='ddsm_BudgetIncentivePaymentNet' aggregate='sum' />
                    <attribute name='ddsm_budgetincentivecosttotal' alias='ddsm_BudgetIncentiveCostTotal' aggregate='sum' />
                    <attribute name='ddsm_budgettotalincentive' alias='ddsm_BudgetTotalIncentive' aggregate='sum' />
                    <attribute name='ddsm_1phaseincentivepaymentnet' alias='ddsm_1PhaseIncentivePaymentNet' aggregate='sum' />
                    <attribute name='ddsm_1phaseincentivecosttotal' alias='ddsm_1PhaseIncentiveCostTotal' aggregate='sum' />
                    <attribute name='ddsm_1phasetotalincentive' alias='ddsm_1PhaseTotalIncentive' aggregate='sum' />
                    <attribute name='ddsm_2phaseincentivepaymentnet' alias='ddsm_2PhaseIncentivePaymentNet' aggregate='sum' />
                    <attribute name='ddsm_2phaseincentivecosttotal' alias='ddsm_2PhaseIncentiveCostTotal' aggregate='sum' />
                    <attribute name='ddsm_2phasetotalincentive' alias='ddsm_2PhaseTotalIncentive' aggregate='sum' />
                    <attribute name='ddsm_3phaseincentivepaymentnet' alias='ddsm_3PhaseIncentivePaymentNet' aggregate='sum' />
                    <attribute name='ddsm_3phaseincentivecosttotal' alias='ddsm_3PhaseIncentiveCostTotal' aggregate='sum' />
                    <attribute name='ddsm_3phasetotalincentive' alias='ddsm_3PhaseTotalIncentive' aggregate='sum' />
                    <attribute name='ddsm_actualallincentivepaymentnet' alias='ddsm_ActualAllIncentivePaymentNet' aggregate='sum' />
                    <attribute name='ddsm_actualallincentivecosttotal' alias='ddsm_ActualAllIncentiveCostTotal' aggregate='sum' />
                    <attribute name='ddsm_actualalltotalincentive' alias='ddsm_ActualAllTotalIncentive' aggregate='sum' />

                    <attribute name='ddsm_targetcountparticipants' alias='ddsm_TargetCountParticipants' aggregate='sum' />
                    <attribute name='ddsm_targetcountnewparticipants' alias='ddsm_TargetCountNewParticipants' aggregate='sum' />
                    <attribute name='ddsm_targetcountopportunities' alias='ddsm_TargetCountOpportunities' aggregate='sum' />
                    <attribute name='ddsm_targetcountprojects' alias='ddsm_TargetCountProjects' aggregate='sum' />

                    <attribute name='ddsm_1phasecountparticipants' alias='ddsm_1PhaseCountParticipants' aggregate='sum' />
                    <attribute name='ddsm_1phasecountnewparticipants' alias='ddsm_1PhaseCountNewParticipants' aggregate='sum' />
                    <attribute name='ddsm_1phasecountopportunities' alias='ddsm_1PhaseCountOpportunities' aggregate='sum' />
                    <attribute name='ddsm_1phasecountprojects' alias='ddsm_1PhaseCountProjects' aggregate='sum' />

                    <attribute name='ddsm_2phasecountparticipants' alias='ddsm_2PhaseCountParticipants' aggregate='sum' />
                    <attribute name='ddsm_2phasecountnewparticipants' alias='ddsm_2PhaseCountNewParticipants' aggregate='sum' />
                    <attribute name='ddsm_2phasecountopportunities' alias='ddsm_2PhaseCountOpportunities' aggregate='sum' />
                    <attribute name='ddsm_2phasecountprojects' alias='ddsm_2PhaseCountProjects' aggregate='sum' />

                    <attribute name='ddsm_3phasecountparticipants' alias='ddsm_3PhaseCountParticipants' aggregate='sum' />
                    <attribute name='ddsm_3phasecountnewparticipants' alias='ddsm_3PhaseCountNewParticipants' aggregate='sum' />
                    <attribute name='ddsm_3phasecountopportunities' alias='ddsm_3PhaseCountOpportunities' aggregate='sum' />
                    <attribute name='ddsm_3phasecountprojects' alias='ddsm_3PhaseCountProjects' aggregate='sum' />

                    <attribute name='ddsm_actualallcountparticipants' alias='ddsm_ActualAllCountParticipants' aggregate='sum' />
                    <attribute name='ddsm_actualallcountnewparticipants' alias='ddsm_ActualAllCountNewParticipants' aggregate='sum' />
                    <attribute name='ddsm_actualallcountopportunities' alias='ddsm_ActualAllCountOpportunities' aggregate='sum' />
                    <attribute name='ddsm_actualallcountprojects' alias='ddsm_ActualAllCountProjects' aggregate='sum' />
                    
                    
                    
                    <attribute name='ddsm_1phaseavoidedgascost' alias='ddsm_1PhaseAvoidedGasCost' aggregate='sum' />
                    <attribute name='ddsm_1phaseavoidedelectriccost' alias='ddsm_1PhaseAvoidedElectricCost' aggregate='sum' />
                    <attribute name='ddsm_1phaseavoidedwatercost' alias='ddsm_1PhaseAvoidedWaterCost' aggregate='sum' />
                    <attribute name='ddsm_1phasetotaltrcbenefit' alias='ddsm_1PhaseTotalTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_1phasetotaltrccost' alias='ddsm_1PhaseTotalTRCCost' aggregate='sum' />
                    <attribute name='ddsm_1phasenettrcbenefit' alias='ddsm_1PhaseNetTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_2phaseavoidedgascost' alias='ddsm_2PhaseAvoidedGasCost' aggregate='sum' />
                    <attribute name='ddsm_2phaseavoidedelectriccost' alias='ddsm_2PhaseAvoidedElectricCost' aggregate='sum' />
                    <attribute name='ddsm_2phaseavoidedwatercost' alias='ddsm_2PhaseAvoidedWaterCost' aggregate='sum' />
                    <attribute name='ddsm_2phasetotaltrcbenefit' alias='ddsm_2PhaseTotalTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_2phasetotaltrccost' alias='ddsm_2PhaseTotalTRCCost' aggregate='sum' />
                    <attribute name='ddsm_2phasenettrcbenefit' alias='ddsm_2PhaseNetTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_3phaseavoidedgascost' alias='ddsm_3PhaseAvoidedGasCost' aggregate='sum' />
                    <attribute name='ddsm_3phaseavoidedelectriccost' alias='ddsm_3PhaseAvoidedElectricCost' aggregate='sum' />
                    <attribute name='ddsm_3phaseavoidedwatercost' alias='ddsm_3PhaseAvoidedWaterCost' aggregate='sum' />
                    <attribute name='ddsm_3phasetotaltrcbenefit' alias='ddsm_3PhaseTotalTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_3phasetotaltrccost' alias='ddsm_3PhaseTotalTRCCost' aggregate='sum' />
                    <attribute name='ddsm_3phasenettrcbenefit' alias='ddsm_3PhaseNetTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_actualallavoidedgascost' alias='ddsm_ActualAllAvoidedGasCost' aggregate='sum' />
                    <attribute name='ddsm_actualallavoidedelectriccost' alias='ddsm_ActualAllAvoidedElectricCost' aggregate='sum' />
                    <attribute name='ddsm_actualallavoidedwatercost' alias='ddsm_ActualAllAvoidedWaterCost' aggregate='sum' />
                    <attribute name='ddsm_actualalltotaltrcbenefit' alias='ddsm_ActualAllTotalTRCBenefit' aggregate='sum' />
                    <attribute name='ddsm_actualalltotaltrccost' alias='ddsm_ActualAllTotalTRCCost' aggregate='sum' />
                    <attribute name='ddsm_actualallnettrcbenefit' alias='ddsm_ActualAllNetTRCBenefit' aggregate='sum' />
                    <filter>
                    <condition attribute='statecode' operator='eq' value='0' />
                    <condition attribute='statuscode' operator='eq' value='1' /> 
                    {0}                                                   
                </filter>
              </entity>
            </fetch>";

        queryMeasureTotals = string.Format(queryMeasureTotals, programCond);
        var queryMeasureExpression = new FetchExpression(queryMeasureTotals);

        var totals = _service.RetrieveMultiple(queryMeasureExpression);

        result.Entities.Add(totals.Entities[0]);

        return result;
    }

    private Entity GetPortfolio(Guid id)
    {
        return _service.Retrieve(_ddsmPortfolio, id, new ColumnSet("ddsm_portfolioid", "ddsm_dsmplanid"));
    }
    #endregion
#if WITH_TASKQ
    private void processCollectedData()
    {
        var taskQueue = new TaskQueue(_objCommon.GetOrgService(systemCall:true));
        taskQueue.Create(new DDSM_Task(TaskEntity.DsmPlan)
        {
            ProcessedItems0 = processedPrograms.Select(x => x).Distinct().ToList()
        }, TaskEntity.DsmPlan
        );
    }
#endif
}

