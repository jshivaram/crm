﻿using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using DDSM_CalculationProgramInterval;
using DDSM_CalculationProgramInterval.Utils;
using System.Collections.Generic;
using System;
using Microsoft.Xrm.Sdk.Query;
using System.Diagnostics.Contracts;
using Newtonsoft.Json;
using DDSM.CalculationPlugin.Utils;
using System.Linq;
using DDSM.CommonProvider;
using DDSM.SmartMeasureCalculationPlugin.Model;
using Microsoft.Xrm.Sdk.Messages;

/// <summary>
/// Plugin for calculation ProgramInterval totals field from workflow that runing from Measure entity
/// </summary>
public class CalculateProgramIntrervalTotalsField : CodeActivity
{
    #region "Parameter Definition"

    [Input("Program Offerings")]
    [ArgumentEntity("ddsm_programoffering")]
    [ReferenceTarget("ddsm_programoffering")]
    public InArgument<EntityReference> ProgramOffering { get; set; }


    [Input("Program")]
    [ArgumentEntity("ddsm_program")]
    [ReferenceTarget("ddsm_program")]
    public InArgument<EntityReference> Program { get; set; }

    [Input("User Input")]
    public InArgument<string> UserInput { get; set; }

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }

    Common objCommon;
    IOrganizationService service;

    ExecuteMultipleRequest requestWithResults;
    #endregion


    protected override void Execute(CodeActivityContext context)
    {
        #region "Load CRM Service from context"
        objCommon = new Common(context);
        service = objCommon.GetOrgService();
        objCommon.TracingService.Trace("01 Load CRM Service from context --- OK");
        #endregion

        var progrOf = ProgramOffering.Get(context);
        var progr = Program.Get(context);
        //Target Entity
        EntityReference Target = (EntityReference)objCommon.Context.InputParameters["Target"];

        string inputPO = null;
        string inputP = null;
        string inputTargetId = null;

        objCommon.TracingService.Trace("Iput Parameters:");
        //Target ID
        if (Target != null)
        {
            inputTargetId = Target.Id.ToString();
            objCommon.TracingService.Trace("Target ID: " + inputTargetId);
        }
        else { objCommon.TracingService.Trace("Target ID is null"); }
        //Program Offering
        if (progrOf != null)
        {
            inputPO = progrOf.Id.ToString();
            objCommon.TracingService.Trace("Program Offering: " + inputPO);
        }
        else { objCommon.TracingService.Trace("Program Offering is null"); }
        //Program
        if (progr != null)
        {
            inputP = progr.Id.ToString();
            objCommon.TracingService.Trace("Program: " + inputP);
        }
        else { objCommon.TracingService.Trace("Program is null"); }

        try
        {
            requestWithResults = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            var measureIDsJson = UserInput.Get(context);

            var config = GetConfig(service); //AdminData conditions
            var measureDateFiledName = "";
            /*ddsm_phaseset1, ddsm_phaseset2, ddsm_phaseset3*/

            if (config.Attributes.ContainsKey("ddsm_pimeasuredatefieldname"))
            {

                measureDateFiledName = config["ddsm_pimeasuredatefieldname"].ToString();
                measureDateFiledName = !string.IsNullOrEmpty(measureDateFiledName) ? measureDateFiledName.ToLower() : "ddsm_initialphasedate";

            }

            //List of All Intervals
            List<Entity> IntervalsList = new List<Entity>();
            Dictionary<int, string> phasesSet = GetAvailablePhaseSet(config);
            EntityCollection conditionIntervals = new EntityCollection();


            //var progrOf = ProgramOffering.Get(context);
            //var progr = Program.Get(context);
            //Check "User Input" (colection of measures) if it have some data - do some logic with it
            if (!String.IsNullOrEmpty(measureIDsJson))
            {
                var measureIDs = JsonConvert.DeserializeObject<UserInputObj2>(measureIDsJson);
                //measureIDs?.SmartMeasures != null

                //Get Distinct of Program and Program Offerings of Measures
                conditionIntervals = GetConditionFromMeasures(measureIDs);

                foreach (var cond in conditionIntervals.Entities)
                {
                    var programMeas = cond.GetAttributeValue<EntityReference>("ddsm_programid");
                    var programOfferingMeas = cond.GetAttributeValue<EntityReference>("ddsm_programofferingsid");
                    var programIntervalsForUpdate = GetProgramIntervals(programOfferingMeas.Id, programMeas.Id, config, Guid.Empty);
                    IntervalsList.AddRange(programIntervalsForUpdate.Entities);
                }



            }
            //Else If it null check Parameters ProgramOffering and Program and do logic - OnChange Measure
            else if (progrOf != null && progr != null)
            {
                var programIntervalsForUpdate = GetProgramIntervals(progrOf.Id, progr.Id, config, Guid.Empty);
                IntervalsList.AddRange(programIntervalsForUpdate.Entities);
            }
            //Else If if we need calculate only 1 Target ID - Interval.
            else if (progrOf == null || progr == null)
            {
                if (Target != null)
                {
                    var programIntervalsForUpdate = GetProgramIntervals(Guid.Empty, Guid.Empty, config, Target.Id);
                    IntervalsList.AddRange(programIntervalsForUpdate.Entities);
                }
            }
            else
            {
                throw new Exception("No valid input parameters. Please check it.");
            }
            //ProcessedItems0 = processedProject.Select(x => x).Distinct().ToList()
            IntervalsList = IntervalsList.Select(x => x).Distinct(new EntityComparer()).ToList();
            //   var intIDs = IntervalsList.Select(x => x.Id).Distinct().ToList();

            objCommon.TracingService.Trace("Get GetProgramIntervals. Kol: " + IntervalsList.Count);

            if (IntervalsList.Count > 0)
            {
                foreach (var interval in IntervalsList)
                {
                    var programMeas = interval.GetAttributeValue<EntityReference>("ddsm_programid");
                    var programOfferingMeas = interval.GetAttributeValue<EntityReference>("ddsm_programofferingsid");


                    var totals = GetMeasureTotals(interval, service, programOfferingMeas, programMeas, phasesSet, measureDateFiledName);
                    UpdatePiTotals(interval, totals);
                }
            }



            if (requestWithResults.Requests.Count > 0)
            {
                ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)service.Execute(requestWithResults);
                if (!responseWithResults.IsFaulted)
                {
                    //Calculate Actual Sum after update phases
                    foreach (var interval in IntervalsList)
                    {
                        var actualSumInterval = GetActualSumInterval(interval, service);
                    }
                    //UpdateRequest createRequest = new UpdateRequest { Target = programInterval };
                    //requestWithResults.Requests.Add(createRequest);
                    objCommon.TracingService.Trace(IntervalsList.Count + " Program Interval was updated.");
                }
                else
                {
                    //Log Error on Update.
                    foreach (var resp in responseWithResults.Responses)
                    {
                        if (resp.Fault != null)
                        {
                            objCommon.TracingService.Trace("Update is falted in ExecuteMultipleResponse: " + resp.Fault.Message);
                        }
                    }
                }
            }
            else
            {
                objCommon.TracingService.Trace("Program Interval not updated.");
            }

            //processCollectedData(conditionIntervals);
            Complete.Set(context, true);

        }
        catch (Exception ex)
        {
            objCommon.TracingService.Trace("Error of ProgramInterval totals calculation. Error:" + ex.Message);
            Complete.Set(context, true);
        }
        finally
        {
            Complete.Set(context, true);
        }
    }

    private object GetActualSumInterval(Entity interval, IOrganizationService service)
    {

        var programIntervalCond = Guid.Empty != (Guid)interval.Attributes["ddsm_kpiintervalid"] ?
            $"<condition attribute='ddsm_kpiintervalid' operator='eq' value='{interval.Attributes["ddsm_kpiintervalid"]}' />"
            : "";

        var queryProgIntervals = @"<fetch version='1.0'>
							  <entity name='ddsm_kpiinterval'>
									<attribute name='ddsm_programid'/>
									<attribute name='ddsm_programofferingsid'/>
									<attribute name='ddsm_startdate'/>
									<attribute name = 'ddsm_enddate'/>
                                    <attribute name='ddsm_probabilityphase1' />
                                    <attribute name='ddsm_probabilityphase2' />
                                    <attribute name='ddsm_probabilityphase3' />
                                 <filter>
								  	<condition attribute='statecode' operator='eq' value='0' />
                                    <condition attribute='statuscode' operator='eq' value='1' /> 
                                    {0}                   
								</filter>
							  </entity>
							</fetch>";

        // DateTime dateWithCorrection;

        queryProgIntervals = string.Format(queryProgIntervals, programIntervalCond);
        //objCommon.TracingService.Trace("queryProgIntervals totals OK. " + queryProgIntervals);

        var queryProgIntervalsExpression = new FetchExpression(queryProgIntervals);

        return service.RetrieveMultiple(queryProgIntervalsExpression);
    }

    private EntityCollection GetConditionFromMeasures(UserInputObj2 measureIDs)
    {
        var result = new EntityCollection();
        try
        {
            Contract.Ensures(Contract.Result<EntityCollection>() != null);



            var queryMeasureConditions = @"<fetch distinct='true' >
											  <entity name='ddsm_measure' >
												<attribute name='ddsm_programid' />
												<attribute name='ddsm_programofferingsid' />
												<filter type='and' >
												  <condition attribute='ddsm_measureid' operator='in' >
						                          <condition attribute='statecode' operator='eq' value='0' />
                                                  <condition attribute='statuscode' operator='eq' value='1' />
													{0}
												  </condition>
												</filter>
											  </entity>
											</fetch>";
            List<string> measIds = new List<string>();

            foreach (var meas in measureIDs.SmartMeasures)
            {
                measIds.Add(string.Format("<value>{0}</value>", meas));
            }
            queryMeasureConditions = String.Format(queryMeasureConditions, string.Join(System.Environment.NewLine, measIds));
            //objCommon.TracingService.Trace("getProjectCounts totals OK. " + queryMeasureTotals);
            var queryMeasureExpression = new FetchExpression(queryMeasureConditions);
            return service.RetrieveMultiple(queryMeasureExpression);
        }
        catch (Exception)
        {
            return result;
        }

    }

    private Entity GetMeasData(Guid meas)
    {
        try
        {
            var query = new QueryExpression
            {
                EntityName = "ddsm_measure",
                ColumnSet = new ColumnSet("ddsm_programofferingsid", "ddsm_programid", "ddsm_uptodatephase", "statecode"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = {
                         new ConditionExpression("ddsm_measureid", ConditionOperator.Equal, meas),
                            new ConditionExpression("statecode", ConditionOperator.Equal, (int)State.Active),
                            new ConditionExpression("statuscode", ConditionOperator.Equal, (int)Status.Active)
                },
                }

            };
            //query..Add(new LinkEntity("ddsm_milestone", ProjectEntity, "ddsm_projecttomilestoneid", "ddsm_projectid");
            var result = service.RetrieveMultiple(query);
            if (result.Entities.Count > 0)
                return result[0];
            else
                return null;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public enum State
    {
        Active = 0,
        Inactive = 1
    };

    public enum Status
    {
        Active = 1,
        Inactive = 2
    };

#if WITH_TASKQ
    private void processCollectedData(EntityCollection intervals)
    {
        var listIds = new List<Guid>();
        foreach (var interv in intervals.Entities)
        {
            listIds.Add(interv.GetAttributeValue<EntityReference>("ddsm_programofferingsid").Id);// ToString();
        }

        var taskQueue = new TaskQueue(objCommon.GetOrgService());
        taskQueue.Create(new DDSM_Task(TaskEntity.ProgramOffering)
        {
            ProcessedItems0 = listIds.Select(x => x).Distinct().ToList()
        }, TaskEntity.ProgramOffering
        );
    }
#endif

    #region Internal methods
    //Phase set important for calculate(we need same fields in pahase sets for entity and same count in Admin Data (Example: if we have 3 phasa sets we need set countOfPhaseSet = 2 
    //and use in Admin data 0,1,2 index for populate phase sets; If we have 5 phasa set like Savings for Program Intervals we must countOfPhaseSet = 4))
    private Dictionary<int, string> GetAvailablePhaseSet(Entity config)
    {
        var result = new Dictionary<int, string>();
        var fieldname = "ddsm_phaseset";
        var countOfPhaseSet = 3;

        for (int i = 0; i <= countOfPhaseSet; i++)
        {
            if (config.Attributes.ContainsKey(fieldname + i))
            {
                string value = null;
                if (config[fieldname + i] != null) { value = config[fieldname + i].ToString(); }
                if (!string.IsNullOrEmpty(value))
                {
                    result.Add(i, value);
                }
                else { result.Add(i, null); }
            }
        }
        return result;
    }

    private Entity GetConfig(IOrganizationService service)
    {
        var result = new Entity();
        string adminDataId = "";

        var expr = new QueryExpression
        {
            EntityName = "ddsm_admindata",
            ColumnSet = new ColumnSet(true),
            Criteria = new FilterExpression
            {
                FilterOperator = LogicalOperator.And,
                Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, "Admin Data"),
                                new ConditionExpression("statecode", ConditionOperator.Equal, (int)State.Active),
                                new ConditionExpression("statuscode", ConditionOperator.Equal, (int)Status.Active) }
            }
        };

        var adminData = service.RetrieveMultiple(expr);
        if (adminData != null && adminData.Entities?.Count >= 1)
        {
            adminDataId = adminData.Entities[0].Attributes["ddsm_admindataid"].ToString();
        }


        result = service.Retrieve("ddsm_admindata", Guid.Parse(adminDataId), new ColumnSet("ddsm_phaseset1", "ddsm_phaseset2", "ddsm_phaseset3", "ddsm_useyearcorrection", "ddsm_programintervalyearorrection", "ddsm_pimeasuredatefieldname"));
        //return service.Retrieve("ddsm_admindata", Guid.Parse(adminDataId), new ColumnSet("ddsm_phaseset1", "ddsm_phaseset2", "ddsm_phaseset3", "ddsm_useyearcorrection", "ddsm_programintervalyearorrection", "ddsm_pimeasuredatefieldname"));
        if (!result.Attributes.ContainsKey("ddsm_phaseset1"))
        {
            result.Attributes.Add("ddsm_phaseset1", null);
        }
        if (!result.Attributes.ContainsKey("ddsm_phaseset2"))
        {
            result.Attributes.Add("ddsm_phaseset2", null);
        }
        if (!result.Attributes.ContainsKey("ddsm_phaseset3"))
        {
            result.Attributes.Add("ddsm_phaseset3", null);
        }
        return result;
    }

    private EntityCollection GetMeasureTotals(Entity interval, IOrganizationService service, EntityReference programOff, EntityReference program, Dictionary<int, string> phases, string dateFiledName)
    {


        if (phases.Count == 0)
        {
            phases.Add(1, null);
            phases.Add(2, null);
            phases.Add(3, null);
        }

        /*if (phases.Count != 3)
        {
            phases
        }*/

        var result = new EntityCollection();
        var resultSum = new EntityCollection();
        try
        {
            Contract.Ensures(Contract.Result<EntityCollection>() != null);

            /*****************************************/
            //1 Phases 
            /*****************************************/

            var programOffCond = programOff != null ?
                $"<condition attribute = 'ddsm_programofferingsid' operator= 'eq' value = '{programOff.Id.ToString()}'/>"
                : "";

            var programCond = program != null ?
                $"<condition attribute = 'ddsm_programid' operator= 'eq' value = '{program.Id.ToString()}'/>"
                : "";

            var startDate = new DateTime();
            var endDate = new DateTime();

            if (interval.Attributes.ContainsKey("ddsm_startdate"))
            {
                startDate = (DateTime)interval.Attributes["ddsm_startdate"];
                startDate = startDate.AddSeconds(-1);
            }
            if (interval.Attributes.ContainsKey("ddsm_enddate"))
            {
                endDate = (DateTime)interval.Attributes["ddsm_enddate"];
                endDate = endDate.AddDays(1);
            }
            var dateFilter = "";
            if (!string.IsNullOrEmpty(dateFiledName))
            {
                dateFilter = !startDate.Equals(DateTime.MinValue) && !endDate.Equals(DateTime.MinValue) ?
                      string.Format("<condition attribute='{2}' operator='between'><value>{0}</value><value>{1}</value></condition>",
                      startDate.ToString("MM/dd/yyyy HH:mm:ss"), endDate.ToString("MM/dd/yyyy HH:mm:ss"), dateFiledName) : "";
            }


            foreach (var phase in phases)
            {
                //OLD Condition when we select by number of phase ddsm_uptodatephase!!!
                //$"<condition attribute='ddsm_uptodatephase' operator='in'>{phase.Value.GetFirterInValues()} </condition>";
                var phaseCondition = $"<condition attribute='ddsm_projectphasename' operator='null' />";
                if (phase.Value != null)
                {
                    phaseCondition =
                        $"<condition attribute='ddsm_projectphasename' operator='in'>{phase.Value.GetFirterInValues()} </condition>";
                }


                var queryMeasureTotals =
                    @"<fetch aggregate='true'>
					<entity name='ddsm_measure'>               
					<attribute name='ddsm_kwhgrosssavingsatmeter' alias='ddsm_{3}phasekwhgrosssavingsatmeter' aggregate='sum' />
					<attribute name='ddsm_kwhgrosssavingsatgenerator' alias='ddsm_{3}phasekwhgrosssavingsatgenerator' aggregate='sum' />
					<attribute name='ddsm_kwhnetsavingsatgenerator' alias='ddsm_{3}phasekwhnetsavingsatgenerator' aggregate='sum' />
					<attribute name='ddsm_kwgrosssavingsatmeter' alias='ddsm_{3}phasekwgrosssavingsatmeter' aggregate='sum' />
					<attribute name='ddsm_kwgrosssavingsatgenerator' alias='ddsm_{3}phasekwgrosssavingsatgenerator' aggregate='sum' />
					<attribute name='ddsm_kwnetsavingsatgenerator' alias='ddsm_{3}phasekwnetsavingsatgenerator' aggregate='sum' />
					<attribute name='ddsm_gjsavingsatmeter' alias='ddsm_{3}phasegjsavingsatmeter' aggregate='sum' />
					<attribute name='ddsm_m3savings' alias='ddsm_{3}phasem3savings' aggregate='sum' />
					<attribute name='ddsm_watersavings' alias='ddsm_{3}phasewatersavings' aggregate='sum' />
					<attribute name='ddsm_incentivepaymentnet' alias='ddsm_{3}phaseincentivepaymentnet' aggregate='sum' />
					<attribute name='ddsm_incentivecosttotal' alias='ddsm_{3}phaseincentivecosttotal' aggregate='sum' />
					<attribute name='ddsm_totalincentive' alias='ddsm_{3}phasetotalincentive' aggregate='sum' />
					<attribute name='ddsm_avoidedgascost' alias='ddsm_{3}phaseavoidedgascost' aggregate='sum' />
					<attribute name='ddsm_avoidedelectriccost' alias='ddsm_{3}phaseavoidedelectriccost' aggregate='sum' />
					<attribute name='ddsm_avoidedwatercost' alias='ddsm_{3}phaseavoidedwatercost' aggregate='sum' />
					<attribute name='ddsm_totaltrcbenefit' alias='ddsm_{3}phasetotaltrcbenefit' aggregate='sum' />
					<attribute name='ddsm_totaltrccost' alias='ddsm_{3}phasetotaltrccost' aggregate='sum' />
					<attribute name='ddsm_nettrcbenefit' alias='ddsm_{3}phasenettrcbenefit' aggregate='sum' />
					<filter>
						<condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='1' />
						{0}
						{1} 
						{2}
						{4}                 
					</filter>
			  </entity>
			</fetch>";
                //  < attribute name = 'ddsm_projecttomeasureid' alias = 'ddsm_{3}phasecountprojects' aggregate = 'count' />

                queryMeasureTotals = string.Format(queryMeasureTotals, programOffCond, phaseCondition, dateFilter, phase.Key, programCond);
                //objCommon.TracingService.Trace("queryMeasureTotals totals OK. " + queryMeasureTotals);
                var queryMeasureExpression = new FetchExpression(queryMeasureTotals);



                var totals = service.RetrieveMultiple(queryMeasureExpression);

                var totalWeight = new Entity("ddsm_kpiinterval", (Guid)interval["ddsm_kpiintervalid"]);


                if (totals.Entities.Count > 0)
                {
                    //Update total Curent phase with Weight != 100
                    if (interval.Attributes.ContainsKey("ddsm_probabilityphase" + phase.Key))
                    {
                        object prob = null;
                        prob = interval.Attributes["ddsm_probabilityphase" + phase.Key];
                        if (prob != null && (Decimal)prob != 100)
                        {
                            var weight = (Decimal)prob / 100;
                            foreach (string attrname in totals.Entities[0].Attributes.Keys)
                            {
                                if (totals.Entities[0].Attributes.GetRecordValue(attrname) != null)
                                {
                                    //totals.Entities[0][attrname] = (Decimal)totals.Entities[0].Attributes.GetRecordValue(attrname) * weight;
                                    if (totals.Entities[0].Attributes.GetRecordValue(attrname).GetType().Name == "Money")
                                    {
                                        totalWeight[attrname] = new Money(((Money)totals.Entities[0].Attributes.GetRecordValue(attrname)).Value * weight);
                                    }
                                    else
                                    {
                                        totalWeight[attrname] = (Decimal)totals.Entities[0].Attributes.GetRecordValue(attrname) * weight;
                                    }
                                }
                                else
                                {
                                    totalWeight[attrname] = null;
                                }

                            }

                        }


                    }
                    if (totalWeight.Attributes.Count > 0)
                    {
                        result.Entities.Add(totalWeight);
                    }
                    else
                    {
                        result.Entities.Add(totals.Entities[0]);
                    }

                    if (totalWeight.Attributes.Count > 0)
                    {
                        resultSum.Entities.Add(totalWeight);
                    }
                    else
                    {
                        resultSum.Entities.Add(totals.Entities[0]);
                    }
                    //result.Entities.Add(totals.Entities[0]);
                }

                #region getProjectCounts

                queryMeasureTotals = @"<fetch aggregate='true' >
									  <entity name='ddsm_measure'>
										<link-entity name='ddsm_project' from='ddsm_projectid' to='ddsm_projecttomeasureid' alias='project' >
										  <attribute name='ddsm_recordtype' alias='type' groupby='true' />
										  <attribute name='ddsm_recordtype' alias='countType' aggregate='count' />
										  <filter type='and' >
						                    <condition attribute='statecode' operator='eq' value='0' />
                                            <condition attribute='statuscode' operator='eq' value='1' />
											<condition attribute='ddsm_recordtype' operator='not-null' />
										  </filter>
										</link-entity>
									  <filter type='and' >
											{0}
											{1}
											{2}
											{3}
									  </filter>
									  </entity>
									</fetch>";
                queryMeasureTotals = string.Format(queryMeasureTotals, programOffCond, phaseCondition, dateFilter, programCond);
                //objCommon.TracingService.Trace("getProjectCounts totals OK. " + queryMeasureTotals);
                queryMeasureExpression = new FetchExpression(queryMeasureTotals);
                totals = GetProjectCountByRecordType(service.RetrieveMultiple(queryMeasureExpression), phase.Key);


                if (totals.Entities.Count > 0)
                    result.Entities.Add(totals.Entities[0]);
                #endregion
            }



            /*****************************************/
            //Actual Sum of
            /*****************************************/
            //Actual Set = sum (PhaseSet1+PhaseSet2+PhaseSet3)
            //Condition with all phases
            List<string> cond = new List<string>();
            //Condition for Actual Sum of Counts: Counts PhaseSet1+Counts  PhaseSet2+Counts  PhaseSet3
            //List<string> condNumber = new List<string>();
            #region Old ActualSum
            foreach (var phase in phases.Values)
            {
                /*var splitedValue = phase.Split(',');
                foreach (var item in splitedValue)
                {
                    cond.Add(string.Format("<value>{0}</value>", item.Trim()));
                }*/
                if (phase != null)
                {
                    cond.Add(string.Format("<value>{0}</value>", phase));
                }
            }
            string phaseConditionSum = "";
            if (cond.Count > 0)
            {
                phaseConditionSum = "<condition attribute='ddsm_projectphasename' operator='in'>{0}</condition>";
                phaseConditionSum = String.Format(phaseConditionSum, string.Join(System.Environment.NewLine, cond));
            }
            else
            {
                phaseConditionSum = "<condition attribute='ddsm_projectphasename' operator='null' />";
            }



            //         var queryMeasureTotalsSum =
            //                 @"<fetch aggregate='true'>
            //		<entity name='ddsm_measure'>               
            //		<attribute name='ddsm_kwhgrosssavingsatmeter' alias='ddsm_{3}kwhgrosssavingsatmeter' aggregate='sum' />
            //		<attribute name='ddsm_kwhgrosssavingsatgenerator' alias='ddsm_{3}kwhgrosssavingsatgenerator' aggregate='sum' />
            //		<attribute name='ddsm_kwhnetsavingsatgenerator' alias='ddsm_{3}kwhnetsavingsatgenerator' aggregate='sum' />
            //		<attribute name='ddsm_kwgrosssavingsatmeter' alias='ddsm_{3}kwgrosssavingsatmeter' aggregate='sum' />
            //		<attribute name='ddsm_kwgrosssavingsatgenerator' alias='ddsm_{3}kwgrosssavingsatgenerator' aggregate='sum' />
            //		<attribute name='ddsm_kwnetsavingsatgenerator' alias='ddsm_{3}kwnetsavingsatgenerator' aggregate='sum' />
            //		<attribute name='ddsm_gjsavingsatmeter' alias='ddsm_{3}gjsavingsatmeter' aggregate='sum' />
            //		<attribute name='ddsm_m3savings' alias='ddsm_{3}m3savings' aggregate='sum' />
            //		<attribute name='ddsm_watersavings' alias='ddsm_{3}watersavings' aggregate='sum' />
            //		<attribute name='ddsm_incentivepaymentnet' alias='ddsm_{3}incentivepaymentnet' aggregate='sum' />
            //		<attribute name='ddsm_incentivecosttotal' alias='ddsm_{3}incentivecosttotal' aggregate='sum' />
            //		<attribute name='ddsm_totalincentive' alias='ddsm_{3}totalincentive' aggregate='sum' />
            //		<attribute name='ddsm_avoidedgascost' alias='ddsm_{3}avoidedgascost' aggregate='sum' />
            //		<attribute name='ddsm_avoidedelectriccost' alias='ddsm_{3}avoidedelectriccost' aggregate='sum' />
            //		<attribute name='ddsm_avoidedwatercost' alias='ddsm_{3}avoidedwatercost' aggregate='sum' />
            //		<attribute name='ddsm_totaltrcbenefit' alias='ddsm_{3}totaltrcbenefit' aggregate='sum' />
            //		<attribute name='ddsm_totaltrccost' alias='ddsm_{3}totaltrccost' aggregate='sum' />
            //		<attribute name='ddsm_nettrcbenefit' alias='ddsm_{3}nettrcbenefit' aggregate='sum' />
            //		<filter>
            //			<condition attribute='statecode' operator='eq' value='0' />
            //			{0}
            //			{1} 
            //			{2}
            //			{4}                 
            //		</filter>
            //  </entity>
            //</fetch>";

            //         //Add Sum of all savings and costs phases
            //         queryMeasureTotalsSum = string.Format(queryMeasureTotalsSum, programOffCond, phaseConditionSum, dateFilter, "actualall", programCond);
            //         //objCommon.TracingService.Trace("queryMeasureTotals totals OK. " + queryMeasureTotals);
            //         var queryMeasureExpressionSum = new FetchExpression(queryMeasureTotalsSum);

            //         var totalsSum = service.RetrieveMultiple(queryMeasureExpressionSum);

            //         if (totalsSum.Entities.Count > 0)
            //             result.Entities.Add(totalsSum.Entities[0]);
            #endregion
            var programIntervalId = $"<condition attribute='ddsm_kpiintervalid' operator='eq' value='{interval.Attributes["ddsm_kpiintervalid"]}' />";

            var queryForTotalsSum =
                    @"<fetch aggregate='true'>
            		<entity name='ddsm_kpiinterval'>               
            		<attribute name='ddsm_{1}kwhgrosssavingsatmeter' alias='ddsm_{1}kwhgrosssavingsatmeter' aggregate='sum' />
            		<attribute name='ddsm_{1}kwhgrosssavingsatgenerator' alias='ddsm_{1}kwhgrosssavingsatgenerator' aggregate='sum' />
            		<attribute name='ddsm_{1}kwhnetsavingsatgenerator' alias='ddsm_{1}kwhnetsavingsatgenerator' aggregate='sum '/>
            		<attribute name='ddsm_{1}kwgrosssavingsatmeter' alias='ddsm_{1}kwgrosssavingsatmeter' aggregate='sum' />
            		<attribute name='ddsm_{1}kwgrosssavingsatgenerator' alias='ddsm_{1}kwgrosssavingsatgenerator' aggregate='sum' />
            		<attribute name='ddsm_{1}kwnetsavingsatgenerator' alias='ddsm_{1}kwnetsavingsatgenerator' aggregate='sum' />
            		<attribute name='ddsm_{1}gjsavingsatmeter' alias='ddsm_{1}gjsavingsatmeter' aggregate='sum' />
            		<attribute name='ddsm_{1}m3savings' alias='ddsm_{1}m3savings' aggregate='sum' />
            		<attribute name='ddsm_{1}watersavings' alias='ddsm_{1}watersavings' aggregate='sum'/>
            		<attribute name='ddsm_{1}incentivepaymentnet' alias='ddsm_{1}incentivepaymentnet' aggregate='sum' />
            		<attribute name='ddsm_{1}incentivecosttotal' alias='ddsm_{1}incentivecosttotal' aggregate='sum' />
            		<attribute name='ddsm_{1}totalincentive' alias='ddsm_{1}totalincentive' aggregate='sum' />
            		<attribute name='ddsm_{1}avoidedgascost' alias='ddsm_{1}avoidedgascost' aggregate='sum' />
            		<attribute name='ddsm_{1}avoidedelectriccost' alias='ddsm_{1}avoidedelectriccost' aggregate='sum' />
            		<attribute name='ddsm_{1}avoidedwatercost' alias='ddsm_{1}avoidedwatercost' aggregate='sum' />
            		<attribute name='ddsm_{1}totaltrcbenefit' alias='ddsm_{1}totaltrcbenefit' aggregate='sum' />
            		<attribute name='ddsm_{1}totaltrccost' alias='ddsm_{1}totaltrccost' aggregate='sum' />
            		<attribute name='ddsm_{1}nettrcbenefit' alias='ddsm_{1}nettrcbenefit' aggregate='sum' />
            		<filter>
            			<condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='1' />
            			{0}
            		</filter>
              </entity>
            </fetch>";

            //Add Sum of all savings and costs phases
            queryForTotalsSum = string.Format(queryForTotalsSum, programIntervalId, "actualall");
            //objCommon.TracingService.Trace("queryMeasureTotals totals OK. " + queryMeasureTotals);
            var queryMeasureExpressionSum = new FetchExpression(queryForTotalsSum);

            var sumTempl = service.RetrieveMultiple(queryMeasureExpressionSum);

            var ActualSumAllPhases = new Entity("ddsm_kpiinterval", (Guid)interval.Attributes["ddsm_kpiintervalid"]);

            foreach (var phase in phases)
            {
                string namePhases = phase.Key.ToString() + "phase";
                string nameActual = "actualall";

                if (resultSum.Entities[(Int32)phase.Key - 1].Attributes.Count > 0) //If we have some result in Phases
                {

                    foreach (string item in sumTempl.Entities[0].Attributes.Keys)
                    {
                        string currentName = "";
                        currentName = item.Replace(nameActual, namePhases);
                        if (phase.Key == 1)
                        {
                            ActualSumAllPhases.Attributes[item] = null;
                        }
                        if (resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName] is AliasedValue)
                        {
                            if (((AliasedValue)resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName]).Value != null)
                            {
                                if (((AliasedValue)resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName]).Value is Money)
                                {
                                    if (ActualSumAllPhases.Attributes[item] == null)
                                    {
                                        ActualSumAllPhases.Attributes[item] = new Money(((Money)((AliasedValue)resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName]).Value).Value);
                                    }
                                    else
                                    {
                                        ActualSumAllPhases.Attributes[item] = new Money(((Money)ActualSumAllPhases.Attributes[item]).Value + ((Money)((AliasedValue)resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName]).Value).Value);
                                    }
                                }
                                else
                                {
                                    if (ActualSumAllPhases.Attributes[item] == null)
                                    {
                                        ActualSumAllPhases.Attributes[item] = (Decimal)((AliasedValue)resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName]).Value;
                                    }
                                    else
                                    {
                                        ActualSumAllPhases.Attributes[item] = Decimal.Parse(ActualSumAllPhases.Attributes[item].ToString()) + (Decimal)((AliasedValue)resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName]).Value;
                                    }
                                }
                            }
                        }
                        else //
                        {
                            if (resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName] != null)
                            {
                                if (ActualSumAllPhases.Attributes[item] == null)
                                {
                                    if (resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName].GetType().Name == "Money")
                                    {
                                        ActualSumAllPhases.Attributes[item] = (Money)resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName];
                                    }
                                    else
                                    {
                                        ActualSumAllPhases.Attributes[item] = (Decimal)resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName];
                                    }
                                }
                                else
                                {
                                    if (resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName].GetType().Name == "Money")
                                    {
                                        ActualSumAllPhases.Attributes[item] = new Money(((Money)ActualSumAllPhases.Attributes[item]).Value + ((Money)resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName]).Value);
                                    }
                                    else
                                    {
                                        ActualSumAllPhases.Attributes[item] = Decimal.Parse(ActualSumAllPhases.Attributes[item].ToString()) + (Decimal)resultSum.Entities[(Int32)phase.Key - 1].Attributes[currentName];
                                    }
                                }
                            }
                        }
                    }
                }


            }

            if (ActualSumAllPhases.Attributes.Count > 0)
                result.Entities.Add(ActualSumAllPhases);

            /*****************************************/
            //Add Sum of all counts by phases
            /*****************************************/
            /*string phaseCountCondSum = "";
            if (condNumber.Count > 1)
            {
                phaseCountCondSum = "<condition attribute='ddsm_projectphasename' operator='in'>{0}</condition>";
                phaseCountCondSum = String.Format(phaseConditionSum, string.Join(System.Environment.NewLine, condNumber));
            }
            else
            {
                phaseCountCondSum = "<condition attribute='ddsm_projectphasename' operator='null' />";
            }*/


            var queryMeasureTotalsCountsSum = @"<fetch aggregate='true' >
									  <entity name='ddsm_measure'>
										<link-entity name='ddsm_project' from='ddsm_projectid' to='ddsm_projecttomeasureid' alias='project' >
										  <attribute name='ddsm_recordtype' alias='type' groupby='true' />
										  <attribute name='ddsm_recordtype' alias='countType' aggregate='count' />
										  <filter type='and' >
						                    <condition attribute='statecode' operator='eq' value='0' />
                                            <condition attribute='statuscode' operator='eq' value='1' />
											<condition attribute='ddsm_recordtype' operator='not-null' />
										  </filter>
										</link-entity>
									  <filter type='and' >
											{0}
											{1}
											{2}
											{3}
									  </filter>
									  </entity>
									</fetch>";
            queryMeasureTotalsCountsSum = string.Format(queryMeasureTotalsCountsSum, programOffCond, phaseConditionSum, dateFilter, programCond);
            //objCommon.TracingService.Trace("getProjectCounts totals OK. " + queryMeasureTotals);
            var queryMeasureExpressionCountsSum = new FetchExpression(queryMeasureTotalsCountsSum);
            var totalsCountsSum = GetProjectCountByRecordType(service.RetrieveMultiple(queryMeasureExpressionCountsSum));

            if (totalsCountsSum.Entities.Count > 0)
                result.Entities.Add(totalsCountsSum.Entities[0]);

            return result;
        }
        catch (Exception ex)
        {
            objCommon.TracingService.Trace("Error on select GetMeasureTotals: " + ex.Message);
            return result;
        }
    }

    private EntityCollection GetProjectCountByRecordType(EntityCollection countProjectTypes, int phase = 0)
    {
        var result = new EntityCollection();
        result.Entities.Add(new Entity());
        var key = "";


        if (phase == 0)
        {
            //Set 0 for all SumCounts
            result[0].Attributes[$"ddsm_actualallcountopportunities"] = 0;
            result[0].Attributes[$"ddsm_actualallcountprojects"] = 0;

            foreach (var item in countProjectTypes.Entities)
            {
                var aliasedValue = item.Attributes["type"] as AliasedValue;
                if (aliasedValue != null)
                    switch (((OptionSetValue)aliasedValue.Value).Value)
                    {
                        case 962080000:
                            key = $"ddsm_actualallcountopportunities";
                            break;
                        case 962080001:
                            key = $"ddsm_actualallcountprojects";
                            break;

                    }

                result[0].Attributes[key] = (item.Attributes["countType"] as AliasedValue).Value;
            }
            /*if (countProjectTypes.Entities.Count == 0)
            {
                result[0].Attributes[$"ddsm_actualallcountopportunities"] = 0;
                result[0].Attributes[$"ddsm_actualallcountprojects"] = 0;
            }*/
        }
        else
        {
            //Set 0 for all counts
            result[0].Attributes[$"ddsm_{phase}" + "phasecountopportunities"] = 0;
            result[0].Attributes[$"ddsm_{phase}" + "phasecountprojects"] = 0;

            foreach (var item in countProjectTypes.Entities)
            {
                var aliasedValue = item.Attributes["type"] as AliasedValue;
                if (aliasedValue != null)
                    switch (((OptionSetValue)aliasedValue.Value).Value)
                    {
                        case 962080000:
                            key = $"ddsm_{phase}" + "phasecountopportunities";
                            break;
                        case 962080001:
                            key = $"ddsm_{phase}" + "phasecountprojects";
                            break;

                    }

                result[0].Attributes[key] = (item.Attributes["countType"] as AliasedValue).Value;
            }

            //Set Counts = 0 if we have countProjectTypes == null
            /*if (countProjectTypes.Entities.Count == 0)
            {
                result[0].Attributes[$"ddsm_{phase}" + "phasecountopportunities"] = 0;
                result[0].Attributes[$"ddsm_{phase}" + "phasecountprojects"] = 0;
            }*/
        }
        return result;
    }

    private EntityCollection GetProgramIntervals(Guid programOfferingId, Guid program, Entity config, Guid programIntervalId)
    {
        var dataFilter = "";

        #region "Get Program Intervals"

        var programOffCond = Guid.Empty != programOfferingId ?
            $"<condition attribute='ddsm_programofferingsid' operator='eq' value='{programOfferingId}' />"
            : "";

        var programCond = Guid.Empty != program ?
            $"<condition attribute='ddsm_programid' operator='eq' value='{program}' />"
            : "";
        var programIntervalCond = Guid.Empty != programIntervalId ?
            $"<condition attribute='ddsm_kpiintervalid' operator='eq' value='{programIntervalId}' />"
            : "";

        var queryProgIntervals = @"<fetch version='1.0'>
							  <entity name='ddsm_kpiinterval'>
									<attribute name='ddsm_programid'/>
									<attribute name='ddsm_programofferingsid'/>
									<attribute name='ddsm_startdate'/>
									<attribute name = 'ddsm_enddate'/>
                                    <attribute name='ddsm_probabilityphase1' />
                                    <attribute name='ddsm_probabilityphase2' />
                                    <attribute name='ddsm_probabilityphase3' />
                                 <filter>
								{0}
								  	<condition attribute='statecode' operator='eq' value='0' />
                                    <condition attribute='statuscode' operator='eq' value='1' />
								{1}  
								{2}    
                                {3}                        
								</filter>
							  </entity>
							</fetch>";

        // DateTime dateWithCorrection;
        if (config.Attributes.ContainsKey("ddsm_useyearcorrection") &&
            (bool)config.Attributes["ddsm_useyearcorrection"] &&
            config.Attributes.ContainsKey("ddsm_programintervalyearorrection")) //use year correction
        {
            int dateCorrection = int.Parse(config.Attributes["ddsm_programintervalyearorrection"].ToString());
            dataFilter =
                $"<condition attribute='createdon' operator='between'><value>{DateTime.Now.Date.AddYears(-dateCorrection).ToString("MM/dd/yyyy HH:mm:ss")}</value><value>{DateTime.Now.Date.AddDays(1).ToString("MM/dd/yyyy HH:mm:ss")}</value></condition>";
        }

        queryProgIntervals = string.Format(queryProgIntervals, programOffCond, dataFilter, programCond, programIntervalCond);
        //objCommon.TracingService.Trace("queryProgIntervals totals OK. " + queryProgIntervals);

        var queryProgIntervalsExpression = new FetchExpression(queryProgIntervals);
        #endregion

        return service.RetrieveMultiple(queryProgIntervalsExpression);

    }

    //Microsoft.Xrm.Sdk.Entity
    private void UpdatePiTotals(Entity programInterval, EntityCollection totals)
    {
        try
        {
            if (totals == null) throw new Exception("Measure Totals are null.");
            //objCommon.TracingService.Trace("6.2 in UpdatePiTotals: " + totals.Entities.Count);
            //var i = 8;
            //var i = 1;
            foreach (var total in totals.Entities)
            {
                //if (i < 3)
                //{
                foreach (var column in total.Attributes)
                {
                    programInterval[column.Key.ToLower()] = column.Value.GetRecordValue();
                }
                //}
                //i = i-1;
                //i = i+1;
            }
            #region Temp
            // if we need hardcoded mapping
            /*
			programInterval["ddsm_0phasecostequipment"] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostEquipment");
			programInterval["ddsm_0phasecostoemrecovery".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostOEMRecovery");
			programInterval["ddsm_0phasecostfinancing".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostFinancing");
			programInterval["ddsm_0phasesavingswccf".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseSavingsWCCF");
			programInterval["ddsm_0phasecostmeasureperunit".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostMeasurePerUnit");
			programInterval["ddsm_0phasesavingsthm".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseSavingsthm");
			programInterval["ddsm_0phasecostpartner".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostPartner");
			programInterval["ddsm_0phasecostsannualom".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostsAnnualOM");
			programInterval["ddsm_0phasecostlabor".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostLabor");
			programInterval["ddsm_0phasesavingskw".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseSavingskW");
			programInterval["ddsm_0phasecostsincrementalinstalled".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostsIncrementalInstalled");
			programInterval["ddsm_0phasesavingskwh".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseSavingskWh");
			programInterval["ddsm_0phasecostproject".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostProject");
			programInterval["ddsm_0phasecostmeasure".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostMeasure");
			programInterval["ddsm_0phasecostother".ToLower()] = totals.Entities[0].Attributes.GetRecordValue("ddsm_0PhaseCostOther");
			*/
            #endregion
            //objCommon.TracingService.Trace("Pi Attrs: " + JsonConvert.SerializeObject(programInterval.Attributes) + "Pi guid: " + programInterval.Id.ToString());
            //objCommon.Service.Update(programInterval);
            UpdateRequest createRequest = new UpdateRequest { Target = programInterval };
            requestWithResults.Requests.Add(createRequest);
        }
        catch (Exception ex)
        {
            objCommon.TracingService.Trace("Error On Program Interval totals update. Exeption:" + ex.Message);
        }
    }
    #endregion Internal methods

}

