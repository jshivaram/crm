﻿using DDSM.SmartMeasureCalculationPlugin.Model;
using DDSM_CalculationProgramInterval;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// This plugin will be calculate PG and PGF for every PG 
/// </summary>
public class CalculateProjectGroupFinancialTotalsField : CodeActivity
{
    #region "Parameter Definition"

    [Input("User Input")]
    public InArgument<string> UserInput { get; set; }

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }
    public string TargetEntity = "ddsm_projectgroupfinancials";

    [Input("Project")]
    [ArgumentEntity("ddsm_project")]
    [ReferenceTarget("ddsm_project")]
    public InArgument<EntityReference> ProjectRef { get; set; }

    #endregion

    #region Internal fields
    Common _objCommon;
    IOrganizationService _service;
    ExecuteMultipleRequest requestWithResults;

#if WITH_TASKQ
    List<Guid> processedProject; //list of project    

#endif

    public enum MilestonStatus
    {
        NotStarted = 962080000,
        Active = 962080001,
        Completed = 962080002,
        Skipped = 962080003
    }

    public enum FinancialStatus
    {
        NotStarted = 962080000,
        Active = 962080001,
        Completed = 962080002
    }

    //Use only 9
    public enum FinancialCalculationType
    {
        AdjustmentEvaluation = 962080007,
        AdjustmentCommitUpdate = 962080008,
        AllProjectIncentiveTotals = 962080009
    }

    enum CalculationRecordStatus
    {
        NotReady = 962080000,
        ReadyToCalculation = 962080001
    };
    enum Program
    {
        _2001 = 962080000,
        _2003 = 962080001,
        _2004 = 962080002,
        _2005 = 962080003,
        _2010 = 962080004,
        _2013 = 962080005,
        _2014 = 962080006,
        _2507 = 962080007,
        _2510 = 962080008,
        _2515 = 962080009,
        _4001 = 962080010,
        _4002 = 962080011,
        _4003 = 962080012,
        _4005 = 962080013,
        _4007 = 962080014,
        _4010 = 962080015
    };

    #endregion

    protected override void Execute(CodeActivityContext context)
    {
        try
        {
            #region "Load CRM Service from context"

            _objCommon = new Common(context);
            _service = _objCommon.Service;
            _objCommon.TracingService.Trace("Load CRM Service from context --- OK");

            requestWithResults = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            #endregion

#if WITH_TASKQ
            processedProject = new List<Guid>(); //list of project   

#endif

            var projectIDsJson = UserInput.Get(context);
            var projectRef = ProjectRef.Get(context);
            UserInputObj2 projectIDs;

            if (!string.IsNullOrEmpty(projectIDsJson) && projectRef == null)
            {
                projectIDs = JsonConvert.DeserializeObject<UserInputObj2>(projectIDsJson);
            }
            else
            {
                projectIDs = new UserInputObj2() { SmartMeasures = { projectRef.Id } };
            }

            _objCommon.TracingService.Trace("projectIDs count :" + projectIDs.SmartMeasures.Count);

            var preparedProject = GetPreparedProjects(projectIDs.SmartMeasures);



            var projGroupList = GetProjGroups(preparedProject); //GetProjGroups(projectIDs.SmartMeasures);
            if (projGroupList != null)
            {

                foreach (var projGroupItem in projGroupList.Entities)
                {
                    //get ProjectGroup Financial by Proj group
                    object projGroupId = null;
                    if (projGroupItem.Contains("ddsm_projectgroupid"))
                        projGroupId = projGroupItem.GetAttributeValue<AliasedValue>("ddsm_projectgroupid").Value;
                    else
                    {
                        return;
                    }


                    var PGFinancials = GetPGFinancials((projGroupId as EntityReference).Id);
                    var measures = new EntityCollection(); //get all measures
                    var projects = getAllProjectsByPG((projGroupId as EntityReference).Id);

                    foreach (var item in projects.Entities)
                    {
                        var upToDatePhase = (int)item.GetAttributeValue<int>("ddsm_phasenumber");//TODO: fix
                        var measTotal = GetMeasureData(item.Id, upToDatePhase);
                        measures.Entities.Add(measTotal[0]);
                    }


                    UpdatePG((projGroupId as EntityReference).Id, measures);

                    foreach (var fin in PGFinancials.Entities)
                    {
                        if (fin.GetAttributeValue<OptionSetValue>("ddsm_status").Value == (int)FinancialStatus.NotStarted)
                        {
                            var index = fin.GetAttributeValue<int>("ddsm_initiatingmilestoneindex");
                            var milestone = GetProjMilestoneData((projGroupId as EntityReference).Id, (int)index);
                            if (milestone != null)
                            {
                                object milestoneStatus = null;
                                if (milestone.Attributes.TryGetValue("ddsm_status", out milestoneStatus))
                                {
                                    if ((milestoneStatus as OptionSetValue).Value == (int)MilestonStatus.Active || (milestoneStatus as OptionSetValue).Value == (int)MilestonStatus.Completed)
                                    {
                                        UpdatePGFinancial(fin, milestone, measures);
                                    }
                                    else
                                    {
                                        UpdatePGFinancial(fin, milestone, measures, false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (requestWithResults.Requests.Count > 0)
            {
                ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_service.Execute(requestWithResults);
                if (responseWithResults.IsFaulted)
                { _objCommon.TracingService.Trace("Error on Update PGF."); }
            }
        }
        catch (Exception ex)
        {
            throw;
        }
        finally
        {

        }
    }

    private List<Guid> GetPreparedProjects(List<Guid> projectsId)
    {
        var result = new EntityCollection();
        var listPrepared = new List<Guid>();
        var query = new QueryExpression()
        {
            EntityName = "ddsm_project",
            ColumnSet = new ColumnSet("ddsm_projectid", "ddsm_calculationrecordstatus"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions =
                {
                    new ConditionExpression("ddsm_projectid", ConditionOperator.In, projectsId)
                  //new ConditionExpression("ddsm_calculationrecordstatus",ConditionOperator.Equal, new OptionSetValue((int)CalculationRecordStatus.ReadyToCalculation))
                }
            }
        };

        //var enitities = _service.RetrieveMultiple(query);
        result = _service.RetrieveMultiple(query);

        foreach (var project in result.Entities)
        {
            if (project.Attributes.ContainsKey("ddsm_calculationrecordstatus") &&
                project.GetAttributeValue<OptionSetValue>("ddsm_calculationrecordstatus").Value == (int)CalculationRecordStatus.ReadyToCalculation)
            {
                listPrepared.Add(project.Id);
            }
        }
        return listPrepared;
    }

    #region Internal methods
    private void UpdatePG(Guid id, EntityCollection measures)
    {
        _objCommon.TracingService.Trace("In UpdatePG:");
        var pg = new Entity("ddsm_projectgroup", id);

        if (measures != null)
        {
            foreach (var measure in measures.Entities)
            {
                foreach (var finfiled in measure.Attributes)
                {
                    if (finfiled.Key != "ddsm_actualamount")
                    {
                        var val = measure.GetAttributeValue<AliasedValue>(finfiled.Key.ToLower()).Value;
                        if (val != null)
                        {
                            if (val is Money && pg.Contains(finfiled.Key.ToLower()))
                                pg[finfiled.Key.ToLower()] = new Money((pg[finfiled.Key.ToLower()] as Money).Value + (val as Money).Value);
                            else
                                pg[finfiled.Key.ToLower()] = val as Money;
                        }
                    }

                }
            }
        }
        //pg["ddsm_incentivedaservicesdsmprogram"] = new OptionSetValue((int)Program._2014); //TODO: is this needed??
        _service.Update(pg);
        //UpdateRequest createRequest = new UpdateRequest { Target = pg };
        //requestWithResults.Requests.Add(createRequest);

    }

    private void UpdatePGFinancial(Entity fin, Entity milestone, EntityCollection measures, bool activatePGF = true)
    {
        _objCommon.TracingService.Trace("In UpdatePGFinancial:");

        if (activatePGF) { _objCommon.TracingService.Trace("Need Activete PGF."); }
        else { _objCommon.TracingService.Trace("NOT NEED Activete PGF."); }

        DateTime TodayDate = DateTime.UtcNow;
        var PGfinance = new Entity(TargetEntity, fin.Id);
        var calcTypeFin = fin.GetAttributeValue<OptionSetValue>("ddsm_calculationtype").Value;

        // var measures = GetMeasures(projId, calcTypeFin);
        if (measures != null)
        {
            foreach (var measure in measures.Entities)
            {
                foreach (var finfiled in measure.Attributes)
                {


                    if (finfiled.Key.ToLower().Equals("ddsm_incentivedaservicesdsmamount"))
                    {
                        var val = measure.GetAttributeValue<AliasedValue>(finfiled.Key.ToLower()).Value;
                        if (val != null)
                        {
                            if (val is Money && PGfinance.Contains("ddsm_cc1_amount"))
                                PGfinance["ddsm_cc1_amount"] = new Money((PGfinance["ddsm_cc1_amount"] as Money).Value + (val as Money).Value);
                            else
                                PGfinance["ddsm_cc1_amount"] = val as Money;
                        }
                    }

                    if (finfiled.Key.ToLower().Equals("ddsm_incentivedaservicespnsamount"))
                    {
                        var val = measure.GetAttributeValue<AliasedValue>(finfiled.Key.ToLower()).Value;
                        if (val != null)
                        {
                            if (val is Money && PGfinance.Contains("ddsm_cc3_amount"))
                                PGfinance["ddsm_cc3_amount"] = new Money((PGfinance["ddsm_cc3_amount"] as Money).Value + (val as Money).Value);
                            else
                                PGfinance["ddsm_cc3_amount"] = val as Money;
                        }
                    }



                    if (!finfiled.Key.ToLower().Equals("ddsm_incentivedaservicesdsmamount")
                        && !finfiled.Key.ToLower().Equals("ddsm_incentivedaservicespnsamount"))
                    {
                        var val = measure.GetAttributeValue<AliasedValue>(finfiled.Key.ToLower()).Value;
                        if (val != null)
                        {
                            if (val is Money && PGfinance.Contains(finfiled.Key.ToLower()))
                                PGfinance[finfiled.Key.ToLower()] = new Money((PGfinance[finfiled.Key.ToLower()] as Money).Value + (val as Money).Value);
                            else
                                PGfinance[finfiled.Key.ToLower()] = val as Money;
                        }
                    }



                    //if (finfiled.Key != "ddsm_actualamount")
                    //{
                    //    var val = measure.GetAttributeValue<AliasedValue>(finfiled.Key.ToLower()).Value;
                    //    if (val != null)
                    //    {
                    //        if (val is Money && PGfinance.Contains("ddsm_cc1_amount"))
                    //            PGfinance["ddsm_cc1_amount"] = new Money((PGfinance["ddsm_cc1_amount"] as Money).Value + (val as Money).Value);
                    //        else
                    //            PGfinance["ddsm_cc1_amount"] = val as Money;
                    //    }
                    //}


                }
            }
            PGfinance["ddsm_calculatedamount"] = PGfinance.GetAttributeValue<Money>("ddsm_actualamount");

            //     PGfinance.Attributes.Remove("ddsm_incentivedaservicesdsmamount");
            //   PGfinance.Attributes.Remove("ddsm_incentivedaservicespnsamount");

        }

        if (activatePGF)
        {
            //Add  duration!
            var thisStartDate = TodayDate;
            #region Add duration

            PGfinance["ddsm_status"] = new OptionSetValue((int)FinancialStatus.Active);

            PGfinance["ddsm_actualstart1"] = milestone.GetAttributeValue<DateTime>("ddsm_actualstart");
            PGfinance["ddsm_targetstart2"] = thisStartDate;

            object duration2 = null;
            if (fin.Attributes.TryGetValue("duration2", out duration2))
                thisStartDate = thisStartDate.AddDays((int)duration2);

            PGfinance["ddsm_targetend2"] = thisStartDate;
            PGfinance["ddsm_targetstart3"] = thisStartDate;

            object duration3 = null;
            if (fin.Attributes.TryGetValue("duration3", out duration3))
                thisStartDate = thisStartDate.AddDays((int)duration3);

            PGfinance["ddsm_targetend3"] = thisStartDate;
            PGfinance["ddsm_targetstart4"] = thisStartDate;

            object duration4 = null;
            if (fin.Attributes.TryGetValue("duration4", out duration4))
                thisStartDate = thisStartDate.AddDays((int)duration4);

            PGfinance["ddsm_targetend4"] = thisStartDate;
            PGfinance["ddsm_targetstart5"] = thisStartDate;

            object duration5 = null;
            if (fin.Attributes.TryGetValue("duration5", out duration5))
                thisStartDate = thisStartDate.AddDays((int)duration5);

            PGfinance["ddsm_targetend5"] = thisStartDate;

            #endregion Add duration       
        }

        UpdateRequest createRequest = new UpdateRequest { Target = PGfinance };
        requestWithResults.Requests.Add(createRequest);
    }

    private EntityCollection getAllProjectsByPG(Guid projGroupItem)
    {
        _objCommon.TracingService.Trace("In getAllProjectsByPG:");
        var query = new QueryExpression
        {
            EntityName = "ddsm_project",
            ColumnSet = new ColumnSet(allColumns: true),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions = {
                         new ConditionExpression("ddsm_projectgroupid", ConditionOperator.Equal, projGroupItem),
                },
            }
        };
        return _service.RetrieveMultiple(query);
    }

    private Entity GetProjMilestoneData(Guid projId, int ind)
    {
        _objCommon.TracingService.Trace("In GetProjMilestoneData:");
        var query = new QueryExpression
        {
            EntityName = "ddsm_projectgroupmilestone",
            ColumnSet = new ColumnSet("ddsm_index", "ddsm_actualstart", "ddsm_actualend", "ddsm_status"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions = {
                         new ConditionExpression("ddsm_projectgrouptoprojectgroupapproid", ConditionOperator.Equal, projId),
                         new ConditionExpression("ddsm_index", ConditionOperator.Equal, ind)
                },
            }
        };
        var result = _service.RetrieveMultiple(query);
        if (result.Entities.Count > 0)
            return result[0];
        else
            return null;

    }

    private EntityCollection GetPGFinancials(Guid projGroupItem)
    {
        _objCommon.TracingService.Trace("In GetPGFinancials:");
        var expr = new QueryExpression
        {
            EntityName = "ddsm_projectgroupfinancials",
            ColumnSet = new ColumnSet("ddsm_duration1", "ddsm_duration2", "ddsm_duration3", "ddsm_duration4", "ddsm_duration5", "ddsm_status", "ddsm_calculationtype", "ddsm_initiatingmilestoneindex"), //TODO: set columns 
            Criteria = new FilterExpression()
            {
                Conditions = {
                    new ConditionExpression("ddsm_projectgrouptoprojectgroupfinanid", ConditionOperator.Equal, projGroupItem),
                }
            }
        };
        return _service.RetrieveMultiple(expr);
    }

    private EntityCollection GetMeasureData(Guid projItem, int phase)
    {
        _objCommon.TracingService.Trace("In GetMeasureData:");
        //TODO: set proj group ID from param
        var queryXml = @"<fetch aggregate='true'>
                    <entity name = 'ddsm_measure'>
                         <attribute name='ddsm_incentivecostmeasure' alias='ddsm_actualamount' aggregate='sum'/>
                         <attribute name='ddsm_incentivecostmeasuredsm' alias='ddsm_incentivedaservicesdsmamount' aggregate='sum'/>
                         <attribute name='ddsm_incentivecostmeasurepns' alias='ddsm_incentivedaservicespnsamount' aggregate='sum'/>
                    <filter type = 'and'>
                     <condition attribute = 'ddsm_projecttomeasureid' operator= 'eq' value = '{0}'/>
                     <condition attribute='ddsm_uptodatephase' operator='eq' value='{1}' />';
                    </filter>
                 </entity>
                 </fetch>";

        //     <attribute name='ddsm_incentivecostmeasurepns' alias='ddsm_incentivedaservicespnsamount' aggregate='sum'/>


        queryXml = string.Format(queryXml, projItem, phase);

        return _service.RetrieveMultiple(new FetchExpression(queryXml));
    }

    private EntityCollection GetProjGroups(List<Guid> projIdList)
    {
        if (projIdList.Count == 0)
        {
            return null;
        }

        _objCommon.TracingService.Trace("In GetProjGroups: projIdList count =" + projIdList.Count);
        var queryXml = @"<fetch aggregate='true'> 
                         <entity name='ddsm_project'>
                            <attribute name='ddsm_projectgroupid' alias='ddsm_projectgroupid' groupby='true'/>
                            <filter type='and'>
                              <condition attribute='ddsm_projectid' operator='in'>
                              {0}
                              </condition>
                            </filter>
                          </entity>
                        </fetch>";
        var projIdsCond = "";
        var listCond = new List<string>();



        foreach (var projId in projIdList)
        {
            listCond.Add(string.Format("<value>{0}</value>", projId));
        }
        projIdsCond = string.Join(Environment.NewLine, listCond);

        queryXml = string.Format(queryXml, projIdsCond);
        _objCommon.TracingService.Trace("In GetProjGroups: fetch =" + queryXml);
        return _service.RetrieveMultiple(new FetchExpression(queryXml));
    }

    #endregion
}

