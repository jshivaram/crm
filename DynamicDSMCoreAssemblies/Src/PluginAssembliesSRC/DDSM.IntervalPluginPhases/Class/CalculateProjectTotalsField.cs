﻿using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using DDSM_CalculationProgramInterval;
using DDSM_CalculationProgramInterval.Utils;
using System;
using Microsoft.Xrm.Sdk.Query;
using DDSM.SmartMeasureCalculationPlugin.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using DDSM.CalculationPlugin.Utils;

/// <summary>
///  Plugin for calculation Program totals field from workflow that runing from ProgramOffering entity
/// </summary>
public class CalculateProjectTotalsField : CodeActivity
{
    #region "Parameter Definition"

    [Input("Project")]
    [ArgumentEntity("ddsm_project")]
    [ReferenceTarget("ddsm_project")]
    public InArgument<EntityReference> Project { get; set; }

    [Input("User Input")]
    public InArgument<string> UserInput { get; set; }

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }


    #endregion

    #region Internal fields
    Common _objCommon;
    IOrganizationService _service;
    readonly string _ddsmProject = "ddsm_project";
    ExecuteMultipleRequest requestWithResults;
    UserInputObj2 userInpObj;

    enum CalculationRecordStatus
    {
        NotReady = 962080000,
        ReadyToCalculation = 962080001
    };

#if WITH_TASKQ
    List<Guid> processedProject; //list of project    

#endif

    #endregion

    protected override void Execute(CodeActivityContext context)
    {

#if WITH_TASKQ
        processedProject = new List<Guid>();
#endif

        #region "Load CRM Service from context"

        _objCommon = new Common(context);
        _service = _objCommon.Service;
        _objCommon.TracingService.Trace("Load CRM Service from context --- OK");
        #endregion
        try
        {
            requestWithResults = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            var projectRef = Project.Get(context);
            var userInput = UserInput.Get(context);

            if (!string.IsNullOrEmpty(userInput))
            {
                userInpObj = JsonConvert.DeserializeObject<UserInputObj2>(userInput);
            }
            var listId = new List<Guid>();
            if (projectRef == null && userInpObj?.SmartMeasures != null)
            {
                listId = userInpObj.SmartMeasures.Where(x => !x.Equals(Guid.Empty)).Distinct().ToList();
            }
            else if (projectRef != null && !Guid.Empty.Equals(projectRef.Id))
            {
                listId.Add(projectRef.Id);
            }
            else
            {
                return;
            }


            var preparedPeojects = GetPreparedProjects(listId);
            _objCommon.TracingService.Trace("Count Prepared projects:" + preparedPeojects.Count);

            foreach (var item in preparedPeojects)
            {
                var totals = GetProgramTotals(item);
                UpdateTotals(item, totals);
                processedProject.Add(item);
            }
            Complete.Set(context, true);
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("Error of Project totals calculation. Error:" + ex.Message);
            Complete.Set(context, false);
        }
        finally
        {
            //TODO: run task for Financial
#if WITH_TASKQ
            processCollectedData();
#endif

        }
    }


#if WITH_TASKQ
    private void processCollectedData()
    {
        if (processedProject.Count > 0)
        {
            var taskQueue = new TaskQueue(_service);


            var finIDs = processedProject.Select(x => x).Distinct().ToList();
            if (finIDs.Count > 0)
            {
                //create task for finances
                taskQueue.Create(new DDSM_Task(TaskEntity.Financial)
                {
                    ProcessedItems0 = finIDs
                }, TaskEntity.Project
                );
            }



            //var uprocessed = userInpObj.SmartMeasures.Except(processedProject).ToList();
            //if (uprocessed.Count > 0)
            //{
            //    // create task for project than is not prepared
            //    taskQueue.Create(new DDSM_Task(TaskEntity.Project)
            //    {
            //        ProcessedItems0 = uprocessed
            //    }, TaskEntity.Project
            // );
            //}

        }
    }
#endif

    private List<Guid> GetPreparedProjects(List<Guid> listId)
    {
        var preparedPeojects = new List<Guid>();
        var calcRecStatus = "ddsm_calculationrecordstatus";

        foreach (var itemProj in listId)
        {
            var measures = GetRelatedMeasures(itemProj);
            if (measures.Count == measures.Count(x => x.Attributes.ContainsKey(calcRecStatus) &&
            x.GetAttributeValue<OptionSetValue>(calcRecStatus).Value == (int)CalculationRecordStatus.ReadyToCalculation))
                preparedPeojects.Add(itemProj);
        }

        return preparedPeojects;
    }
    #region Internal methods
    private void UpdateTotals(Guid projId, EntityCollection totals)
    {
        var listRolups = new List<string>()
        {
            "ddsm_incentivecostproject",
            "ddsm_incentivecostpartner",
            "ddsm_incentivecostmeasure",
            "ddsm_incentivepaymentgross",
            "ddsm_waterccf",
            "ddsm_propanegallons",
            "ddsm_oemrecoverycost",
            "ddsm_incentivecostfinancing",
            "ddsm_incentivepaymentnet",
            "ddsm_oilgallons"
        };



        Entity project = new Entity(_ddsmProject, projId);
        try
        {
            if (totals == null) throw new Exception("Measures Sun of Inc fields are null.");
            foreach (var total in totals.Entities)
            {
                foreach (var column in total.Attributes)
                {
                    project[column.Key.ToLower()] = column.Value.GetRecordValue();
                }
            }
            project["ddsm_calculationrecordstatus"] = new OptionSetValue((int)CalculationRecordStatus.ReadyToCalculation);
            _service.Update(project);
            //Sytem Rolluped Update!
            foreach (var rollup in listRolups)
            {
                var rollupRequest =
                new CalculateRollupFieldRequest { Target = new EntityReference("ddsm_project", projId), FieldName = rollup };
                requestWithResults.Requests.Add(rollupRequest);
            }

            if (requestWithResults.Requests.Count > 0)
            {
                ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_service.Execute(requestWithResults);
                _objCommon.TracingService.Trace(requestWithResults.Requests.Count + " Project rollups was started for recalc.");
                requestWithResults.Requests.Clear();
            }



            // rollupRequest. = 

            //CalculateRollupFieldResponse response = (CalculateRollupFieldResponse)_service.Execute(rollupRequest);

        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("Error On Project Inc fields update. Exeption:" + ex.Message);
        }
    }

    private EntityCollection GetProgramTotals(Guid project)
    {
        var result = new EntityCollection();

        var programCond = $"<condition attribute='ddsm_projecttomeasureid' operator='eq' value='{project.ToString()}' />";
        var queryMeasureTotals =
            @"<fetch aggregate='true'>
                  <entity name='ddsm_measure'>
                    <attribute name='ddsm_incentivecosttotal' alias='ddsm_incentivecosttotal' aggregate='sum' />
                    <attribute name='ddsm_totalincentive' alias='ddsm_totalincentive' aggregate='sum' />
                    <attribute name='ddsm_totalincentivepns' alias='ddsm_totalincentivepns' aggregate='sum' />
                    <attribute name='ddsm_totalincentivedsm' alias='ddsm_totalincentivedsm' aggregate='sum' />
                    <attribute name='ddsm_kwgrosssavingsatmeter' alias='ddsm_kwgrosssavingsatmeter' aggregate='sum' />
                    <attribute name='ddsm_kwhgrosssavingsatmeter' alias='ddsm_kwhgrosssavingsatmeter' aggregate='sum' />
                    <attribute name='ddsm_gjsavingsatmeter' alias='ddsm_gjsavingsatmeter' aggregate='sum' />
                    <attribute name='ddsm_kwgrosssavingsatgenerator' alias='ddsm_kwgrosssavingsatgenerator' aggregate='sum' />
                    <attribute name='ddsm_kwhgrosssavingsatgenerator' alias='ddsm_kwhgrosssavingsatgenerator' aggregate='sum' />    
                    <attribute name='ddsm_kwnetsavingsatgenerator' alias='ddsm_kwnetsavingsatgenerator' aggregate='sum' />
                    <attribute name='ddsm_kwhnetsavingsatgenerator' alias='ddsm_kwhnetsavingsatgenerator' aggregate='sum' />
                    <attribute name='ddsm_evaluatedkwnetsavingsatgenerator' alias='ddsm_evaluatedkwnetsavingsatgenerator' aggregate='sum' />
                    <attribute name='ddsm_evaluatedkwhnetsavingsatgenerator' alias='ddsm_evaluatedkwhnetsavingsatgenerator' aggregate='sum' />
                    <attribute name='ddsm_m3savings' alias='ddsm_m3savings' aggregate='sum' />
                    <attribute name='ddsm_watersavings' alias='ddsm_watersavings' aggregate='sum' />
                <filter>
                    <condition attribute='statecode' operator='eq' value='0'/>
                    <condition attribute='ddsm_recalculationgroup' operator='null' />
                    {0}                                                   
                </filter>
              </entity>
            </fetch>";

        queryMeasureTotals = string.Format(queryMeasureTotals, programCond);
        var queryMeasureExpression = new FetchExpression(queryMeasureTotals);
        //< attribute name = 'ddsm_incentivecostpartner' alias = 'ddsm_incentivecostpartner' aggregate = 'sum' />
        //< attribute name = 'ddsm_incentivecostpartnertest' alias = 'ddsm_incentivecostpartnertest' aggregate = 'sum' />
        var totals = _service.RetrieveMultiple(queryMeasureExpression);

        result.Entities.Add(totals.Entities[0]);
        //result.Entities.Add(rollupRequest);
        return result;
    }


    private List<Entity> GetRelatedMeasures(Guid projId)
    {
        //create the query expression object
        QueryExpression query = new QueryExpression();

        //Query on reated entity records
        query.EntityName = "ddsm_measure";

        //Retrieve the all attributes of the related record
        query.ColumnSet = new ColumnSet(true);

        //create the relationship object
        Relationship relationship = new Relationship();

        //add the condition where you can retrieve only the account related active contacts
        query.Criteria = new FilterExpression();
        query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

        // name of relationship between account & contact
        relationship.SchemaName = "ddsm_ddsm_project_ddsm_measure";

        //create relationshipQueryCollection Object
        RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();

        //Add the your relation and query to the RelationshipQueryCollection
        relatedEntity.Add(relationship, query);

        //create the retrieve request object
        RetrieveRequest request = new RetrieveRequest();

        //add the relatedentities query
        request.RelatedEntitiesQuery = relatedEntity;

        //set column to  and the condition for the account
        request.ColumnSet = new ColumnSet("ddsm_projectid");
        request.Target = new EntityReference { Id = projId, LogicalName = "ddsm_project" };

        //execute the request
        RetrieveResponse response = (RetrieveResponse)_service.Execute(request);

        return response.Entity.RelatedEntities.Values.ToList()[0].Entities.ToList();
    }

    private Entity GetProject(Guid id)
    {
        return _service.Retrieve(_ddsmProject, id, new ColumnSet("ddsm_projectid"));
    }
    #endregion
}

