﻿using DDSM_CalculationProgramInterval;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Activities;
using System;
using Microsoft.Xrm.Sdk.Query;
using DDSM.CalculationPlugin.Utils;
using Newtonsoft.Json;

using DDSM.SmartMeasureCalculationPlugin.Model;
using DDSM_CalculationProgramInterval.Utils;
using Microsoft.Xrm.Sdk.Messages;
using System.Collections.Generic;
using System.Linq;

public class ClaculateFinancialTotalField : CodeActivity
{
    #region "Parameter Definition"

    [Input("Project")]
    [ArgumentEntity("ddsm_project")]
    [ReferenceTarget("ddsm_project")]
    public InArgument<EntityReference> ProjectRef { get; set; }


    [Input("Selected Project ID's")]
    public InArgument<string> UserInput { get; set; }
    #endregion

    #region Internal fields
    Common _objCommon;
    IOrganizationService _service;
    readonly string MainEntity = "ddsm_measure"; //Entity to get data from Measures sum(Incentive-Payment Net), sum(Incentive Peyment Net-DSM)
    readonly string TargetEntity = "ddsm_financial"; //Update entity Financial
    readonly string ProjectEntity = "ddsm_project"; //Update entity Financial

    ExecuteMultipleRequest requestWithResults;


#if WITH_TASKQ
    List<Guid> processedProject; //list of project

#endif


    public enum MilestonStatus
    {
        NotStarted = 962080000,
        Active = 962080001,
        Completed = 962080002,
        Skipped = 962080003
    }

    public enum FinancialStatus
    {
        NotStarted = 962080000,
        Active = 962080001,
        Completed = 962080002
    }

    public enum FinancialType
    {
        Percent50 = 962080002,
    }

    //Use only 9
    public enum FinancialCalculationType
    {
        AdjustmentEvaluation = 962080007,
        AdjustmentCommitUpdate = 962080008,
        AllProjectIncentiveTotals = 962080009
    }

    public enum IncentivesCustomerRebatesDSMProgram
    {
        _2014 = 962080006
    }

    public enum CC1_ProgramCode
    {
        _2014 = 962080006
    }

    public enum CC1_ProgramCodeDescription
    {
        _2014DSMApplianceRetirement = 962080006
    }

    public DateTime TodayDate = DateTime.UtcNow;

    List<Guid> processedId;
    #endregion



    protected override void Execute(CodeActivityContext context)
    {
        #region "Load CRM Service from context"
        _objCommon = new Common(context);
        _service = _objCommon.Service;
        _objCommon.TracingService.Trace("Load CRM Service from context --- OK");
        #endregion


        processedId = new List<Guid>();

        var projectRef = ProjectRef.Get(context);

        UserInputObj2 projectIDs;

        var projectIDsJson = UserInput.Get(context);
        if (!string.IsNullOrEmpty(projectIDsJson) && projectRef == null)
        {
            projectIDs = JsonConvert.DeserializeObject<UserInputObj2>(projectIDsJson);
        }
        else
        {
            projectIDs = new UserInputObj2() { SmartMeasures = { projectRef.Id } };
        }

        try
        {
            requestWithResults = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };
           
            foreach (var projId in projectIDs.SmartMeasures)
            {
                var Financials = GetFinancial(projId);
                if (Financials != null)
                {
                    foreach (var fin in Financials.Entities)
                    {
                        var ind = fin.GetAttributeValue<int>("ddsm_initiatingmilestoneindex");
                        var milestone = GetMilestoneData(projId, ind);

                        if (milestone != null)
                        {
                            object milestoneStatus = null;
                            if (milestone.Attributes.TryGetValue("ddsm_status", out milestoneStatus))
                            {
                                if ((milestoneStatus as OptionSetValue).Value == (int)MilestonStatus.Active || (milestoneStatus as OptionSetValue).Value == (int)MilestonStatus.Completed)
                                {
                                    var StatusFin = fin.GetAttributeValue<OptionSetValue>("ddsm_status").Value;
                                    if (StatusFin == (int)FinancialStatus.NotStarted)
                                    {
                                        UpdateFinancial(fin, projId, milestone);
                                    }
                                }
                            }
                        }
                    }
                    processedId.Add(projId);
                }
                else { _objCommon.TracingService.Trace("Can't get Financials by project id:" + projId); }

            }
            if (requestWithResults.Requests.Count > 0)
            {
                ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_service.Execute(requestWithResults);
                _objCommon.TracingService.Trace(requestWithResults.Requests.Count + " Financial was updated.");
            }
            else
            {
                _objCommon.TracingService.Trace("Financial not updated.");
            }
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace(requestWithResults.Requests.Count + "Error: " + ex.StackTrace);
            throw;
        }
        finally
        {
            processCollectedData(processedId, projectIDs.SmartMeasures);
        }
    }


#if WITH_TASKQ
    private void processCollectedData(List<Guid> processedFin, List<Guid> processedProj)
    {

        if (processedFin.Count > 0)
        {
            var pgfIds = processedFin.Select(x => x).Distinct().ToList();
            if (pgfIds.Count > 0)
            {
                var taskQueue = new TaskQueue(_service);
                taskQueue.Create(new DDSM_Task(TaskEntity.ProjectGroupFinancial)
                {
                    ProcessedItems0 = pgfIds
                }, TaskEntity.Project
                );
            }
        }
        else
        {
            var pgfIds = processedProj.Select(x => x).Distinct().ToList();
            if (pgfIds.Count > 0)
            {
                var taskQueue = new TaskQueue(_service);
                taskQueue.Create(new DDSM_Task(TaskEntity.ProjectGroupFinancial)
                {
                    ProcessedItems0 = pgfIds
                }, TaskEntity.Project
                );
            }

        }     

    }
#endif

    private Entity GetMilestoneData(Guid projId, int ind)
    {
        var query = new QueryExpression
        {
            EntityName = "ddsm_milestone",
            ColumnSet = new ColumnSet("ddsm_index", "ddsm_actualstart", "ddsm_actualend", "ddsm_status"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions = {
                         new ConditionExpression("ddsm_projecttomilestoneid", ConditionOperator.Equal, projId),
                         new ConditionExpression("ddsm_index", ConditionOperator.Equal, ind)
                },
            }

        };
        var result = _service.RetrieveMultiple(query);
        if (result.Entities.Count > 0)
            return result[0];
        else
            return null;

    }

    private void UpdateFinancial(Entity fin, Guid projId, Entity milestone)
    {
        var finance = new Entity(TargetEntity, fin.Id);
        var linkedfields = CrmHelper.getDataRelationMapping(_service, TargetEntity, "ddsm_project", projId);

        var currentFinRecord = _service.Retrieve("ddsm_financial", fin.Id, new ColumnSet("ddsm_stagename1", "ddsm_stagename2", "ddsm_stagename3"));

        object adr1 = null;
        linkedfields.Attributes.TryGetValue("ddsm_payeeaddress1", out adr1);
        object adr2 = null;
        linkedfields.Attributes.TryGetValue("ddsm_payeeaddress2", out adr2);

        _objCommon.TracingService.Trace("Address 1,2: " + adr1 + ", " + adr2);

        #region Implementation #8550
        object finName = null;
        if (fin.Attributes.TryGetValue("ddsm_name", out finName) && (finName as string).Contains("ARET"))
        {
            var parentProj = _service.Retrieve("ddsm_project", projId, new ColumnSet("ddsm_accountid"));

            object parentAccProj = null;
            if (parentProj.Attributes.TryGetValue("ddsm_accountid", out parentAccProj))
            {
                fin["ddsm_cc1_vendor"] = new EntityReference("account", (parentAccProj as EntityReference).Id);
            }
        }
        #endregion

        finance.Attributes.AddRange(linkedfields.Attributes);
        var calcTypeFin = fin.GetAttributeValue<OptionSetValue>("ddsm_calculationtype").Value;

        var measures = GetMeasures(projId, calcTypeFin);
        if (measures != null)
        {
            foreach (var finfiled in measures.Entities[0].Attributes)
            {
                finance[finfiled.Key.ToLower()] = finfiled.Value.GetRecordValue();
            }
        }

        // for Demo for Southern California Edison
#if DEMO
        var FinType = fin.GetAttributeValue<OptionSetValue>("ddsm_financialtype").Value;

        //amount 50 percent
        if (FinType.Equals((int)FinancialType.Percent50))
        {
            object acualAmount = null;
            if (finance.Attributes.TryGetValue("ddsm_actualamount", out acualAmount))
            {
                if (acualAmount != null)
                {
                    finance.Attributes["ddsm_actualamount"] = new Money((acualAmount as Money).Value / 2);
                }
            }
            object CalcAcualAmount = null;
            if (finance.Attributes.TryGetValue("ddsm_calculatedamount", out CalcAcualAmount))
            {
                if (CalcAcualAmount != null)
                {
                    finance.Attributes["ddsm_calculatedamount"] = new Money((CalcAcualAmount as Money).Value / 2);
                }

            }

        }
#endif

        //Add  duration!
        var thisStartDate = TodayDate;
        #region Add duration       

        finance["ddsm_status"] = new OptionSetValue((int)FinancialStatus.Active); //{ Value: 962080001};

        finance["ddsm_actualstart1"] = milestone.GetAttributeValue<DateTime>("ddsm_actualstart");
        finance["ddsm_actualend1"] = thisStartDate;

        finance["ddsm_actualstart2"] = thisStartDate;
        finance["ddsm_targetstart2"] = thisStartDate;

        //set pending stage // from stage2
        object curentStage = null;
        if (currentFinRecord.Attributes.TryGetValue("ddsm_stagename2", out curentStage))
            finance["ddsm_pendingstage"] = curentStage as string;


        thisStartDate = thisStartDate.AddDays(fin.GetAttributeValue<int>("duration2"));
        finance["ddsm_targetend2"] = thisStartDate;

        finance["ddsm_targetstart3"] = thisStartDate;
        thisStartDate = thisStartDate.AddDays(fin.GetAttributeValue<int>("duration3"));
        finance["ddsm_targetend3"] = thisStartDate;

        finance["ddsm_targetstart4"] = thisStartDate;
        thisStartDate = thisStartDate.AddDays(fin.GetAttributeValue<int>("duration4"));
        finance["ddsm_targetend4"] = thisStartDate;

        finance["ddsm_targetstart5"] = thisStartDate;
        thisStartDate = thisStartDate.AddDays(fin.GetAttributeValue<int>("duration5"));
        finance["ddsm_targetend5"] = thisStartDate;





        #endregion Add duration

        //Add constant values...
        #region Add constant values...
        // var actualStart = milestone.GetAttributeValue<DateTime>("ddsm_actualstart");
        //update for
        finance["ddsm_incentivescustomerrebatesdsmprogram"] = new OptionSetValue((int)IncentivesCustomerRebatesDSMProgram._2014);
        finance["ddsm_cc1_costdate"] = TodayDate; //milestone.GetAttributeValue<DateTime>("ddsm_actualend");
        finance["ddsm_cc1_programcode"] = new OptionSetValue((int)CC1_ProgramCode._2014);
        finance["ddsm_cc1_programcodedescription"] = new OptionSetValue((int)CC1_ProgramCodeDescription._2014DSMApplianceRetirement);


        //Need add Payee
        /*var projPayee = GetProjectsPayee(projId);
        finance["ddsm_payeecompany"] = projPayee.GetAttributeValue<string>("ddsm_payeecompany");
        //finance["ddsm_payeestreet1"] = projPayee.GetAttributeValue<string>("ddsm_payeeaddress1");
        //finance["ddsm_payeestreet2"] = projPayee.GetAttributeValue<string>("ddsm_payeeaddress2");
        finance["ddsm_payeeattnto"] = projPayee.GetAttributeValue<string>("ddsm_payeeattnto");
        finance["ddsm_payeecity"] = projPayee.GetAttributeValue<string>("ddsm_payeecity");

        finance["ddsm_payeestate"] = projPayee.GetAttributeValue<string>("ddsm_payeestate");
        finance["ddsm_payeezipcode"] = projPayee.GetAttributeValue<string>("ddsm_payeezipcode");
        finance["ddsm_payeephone"] = projPayee.GetAttributeValue<string>("ddsm_payeephone");
        //finance["ddsm_taxentity"] = projPayee.GetAttributeValue<string>("ddsm_taxentity");
        finance["ddsm_payeeemail"] = projPayee.GetAttributeValue<string>("ddsm_payeeemail");
        finance["ddsm_payeename"] = projPayee.GetAttributeValue<string>("ddsm_payeename");


        if (finance["ddsm_payeecompany"] != null
        && finance["ddsm_payeeattnto"] != null 
        && finance["ddsm_payeecity"] != null
        && finance["ddsm_payeestate"] != null 
        && finance["ddsm_payeezipcode"] != null 
        && finance["ddsm_payeephone"] != null
        //&& finance["ddsm_taxentity"] != null 
        && finance["ddsm_payeeemail"] != null
        && finance["ddsm_payeename"] != null)
        {
            finance["ddsm_payeeaddress1"] = projPayee.GetAttributeValue<string>("ddsm_payeeaddress1");
            finance["ddsm_payeeaddress2"] = projPayee.GetAttributeValue<string>("ddsm_payeeaddress2");
        }*/


        #endregion Add constant values...
        //thisFinancialData["ddsm_IncentivesCustomerRebatesDSMProgram"] = { };
        //thisFinancialData["ddsm_IncentivesCustomerRebatesDSMProgram"].Value = 962080006;
        //thisFinancialData["ddsm_CC1_CostDate"] = ActualEnd;
        //thisFinancialData["ddsm_CC1_ProgramCode"] = { };
        //thisFinancialData["ddsm_CC1_ProgramCode"].Value = 962080006;
        //thisFinancialData["ddsm_CC1_ProgramCodeDescription"] = { };
        //thisFinancialData["ddsm_CC1_ProgramCodeDescription"].Value = 962080006;



        UpdateRequest createRequest = new UpdateRequest { Target = finance };
        requestWithResults.Requests.Add(createRequest);
    }

    private Entity GetProjectsPayee(Guid projId)
    {
        var query = new QueryExpression
        {
            EntityName = ProjectEntity,
            ColumnSet = new ColumnSet("ddsm_payeecompany", "ddsm_payeeaddress1", "ddsm_payeeaddress2", "ddsm_payeeattnto", "ddsm_payeeattnto", "ddsm_payeecity", "ddsm_payeestate", "ddsm_payeezipcode", "ddsm_payeephone", "ddsm_taxentity", "ddsm_payeeemail", "ddsm_payeename"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions = {
                    new ConditionExpression("ddsm_projectid", ConditionOperator.Equal, projId)
                }
            }
        };

        var result = _service.RetrieveMultiple(query);
        //If Index of Finance <> Index of Projects Milestones we haven't records  to update and LOG this info
        if (result.Entities.Count > 0)
            return result[0];
        else
            _objCommon.TracingService.Trace("Project not found ProjectId: " + projId);
        return null;
    }

    private EntityCollection GetFinancial(Guid projId)
    {
        var query = new QueryExpression
        {
            EntityName = TargetEntity,
            ColumnSet = new ColumnSet("ddsm_duration1", "ddsm_duration2", "ddsm_duration3", "ddsm_duration4", "ddsm_duration5", "ddsm_status", "ddsm_calculationtype", "ddsm_initiatingmilestoneindex", "ddsm_financialtype", "ddsm_name"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions = {
                    new ConditionExpression("ddsm_projecttofinancialid", ConditionOperator.Equal, projId)//,
                }
            }
        };

        var result = _service.RetrieveMultiple(query);

        //If Index of Finance <> Index of Projects Milestones we haven't records  to update and LOG this info
        if (result.Entities.Count > 0)
            return result;
        else
            _objCommon.TracingService.Trace("Financial ProjectId: " + projId);
        return null;
    }

    private EntityCollection GetMeasures(Guid projId, int calcType)
    {
        var upToDatePhase = GetProjectPhase(projId);

        var fetchMeas = "";
        switch (calcType)
        {
            case (int)FinancialCalculationType.AllProjectIncentiveTotals:
                fetchMeas = @"<fetch aggregate='true'>
                    <entity name = 'ddsm_measure'>
                        <attribute name='ddsm_incentivepaymentnetdsm' alias='ddsm_cc1_amount' aggregate='sum' />
                        <attribute name='ddsm_incentivepaymentnetdsm' alias='ddsm_incentivescustomerrebatesdsmamount' aggregate='sum' />
                        <attribute name='ddsm_incentivepaymentnet' alias='ddsm_actualamount' aggregate='sum' />
                        <attribute name='ddsm_incentivepaymentnet' alias='ddsm_calculatedamount' aggregate='sum' />
                    <filter type = 'and'>
                     <condition attribute = 'ddsm_projecttomeasureid' operator= 'eq' value = '{0}'/>
                     <condition attribute='ddsm_uptodatephase' operator='eq' value='{1}' />';
                    </filter>
                 </entity>
                 </fetch>";
                fetchMeas = String.Format(fetchMeas, projId.ToString(), upToDatePhase);
                break;
            case (int)FinancialCalculationType.AdjustmentEvaluation:
            case (int)FinancialCalculationType.AdjustmentCommitUpdate:
            default:
                _objCommon.TracingService.Trace("This Financial Calculation Type have no logic. Value of Financial.ddsm_calculationtype: " + calcType);
                break;
        }
        if (String.IsNullOrEmpty(fetchMeas))
        {
            return null;
        }
        else
        {
            var queryMeasureExpression = new FetchExpression(fetchMeas);
            var Measures = _service.RetrieveMultiple(queryMeasureExpression);
            return Measures;
        }

    }

    private int GetProjectPhase(Guid projId)
    {
        var query = new QueryExpression
        {
            EntityName = ProjectEntity,
            ColumnSet = new ColumnSet("ddsm_phasenumber"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions = {
                    new ConditionExpression("ddsm_projectid", ConditionOperator.Equal, projId)
                }
            }
        };

        var result = _service.RetrieveMultiple(query);
        if (result.Entities.Count > 0)
        {
            return result.Entities[0].GetAttributeValue<int>("ddsm_phasenumber");
        }
        else
        {
            return 0;
        }
    }

    private void UpdateRelatedRecords(Entity measureData)
    {
        if (measureData != null && measureData.Attributes != null)
        {
            var query = new QueryExpression
            {
                EntityName = TargetEntity,
                ColumnSet = new ColumnSet(allColumns: true),
                Criteria = new FilterExpression(LogicalOperator.And) { Conditions = { new ConditionExpression("ddsm_measureid", ConditionOperator.Equal, measureData.Id) } }
            };

            var rateClasses = _service.RetrieveMultiple(query);


            foreach (var item in rateClasses.Entities)
            {
                var updatedRateClass = CrmHelper.getDataRelationMapping(_service, TargetEntity, MainEntity, measureData.Id);
                updatedRateClass.Id = item.Id;

                _service.Update(updatedRateClass);
            }
        }
    }

    private Entity GetMeasureData(Guid id)
    {
        return _service.Retrieve(MainEntity, id, new ColumnSet(allColumns: true));
    }
}

