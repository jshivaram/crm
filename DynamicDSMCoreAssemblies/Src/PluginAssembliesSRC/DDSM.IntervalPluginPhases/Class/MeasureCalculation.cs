﻿using DDSM.CalculationPlugin.Utils;
using DDSM.SmartMeasureCalculationPlugin.Model;
using DDSM_CalculationProgramInterval;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

public class MeasureCalculation : CodeActivity
{
    #region "Parameter Definition"

    //[Input("Project")]
    //[ArgumentEntity("ddsm_project")]
    //[ReferenceTarget("ddsm_project")]
    //public InArgument<EntityReference> Measure { get; set; }


    [Input("Selected Project ID's")]
    public InArgument<string> UserInput { get; set; }
    #endregion


    #region Internal Fields
    IOrganizationService _service;
    private Common _objCommon;
    //private Helper _helper;
    //CrmHelper crmHelper;
    //private string url = "";
    List<string> _processingLog;
    readonly string MainEntity = "ddsm_measure";
    Dictionary<Guid, Guid> ProcessedMeasures; // key crm ID, value esp ID

#if WITH_TASKQ
    IList<Guid> processedProject; //list of project                                                     
    IList<Guid> processedMeasure;
#endif

    #endregion

    protected override void Execute(CodeActivityContext context)
    {
        #region "Load CRM Service from context"

        _objCommon = new Common(context);
        _service = _objCommon.Service;
        _objCommon.TracingService.Trace("Load CRM Service from context --- OK");

        #endregion


        _processingLog = new List<string>();
        ProcessedMeasures = new Dictionary<Guid, Guid>();
        processedProject = new List<Guid>();
        processedMeasure = new List<Guid>();

        try
        {
            var measureIDsJson = UserInput.Get(context);
            if (!string.IsNullOrEmpty(measureIDsJson))
            {
                var measureIDs = JsonConvert.DeserializeObject<UserInputObj2>(measureIDsJson);


                if (measureIDs != null && measureIDs.SmartMeasures != null)
                {
                    var measureData = GetMeasuresInfo(measureIDs.SmartMeasures);
                    if (measureData?.Entities.Count > 0)
                    {
                        foreach (var meas in measureData.Entities)
                        {
                            _objCommon.Service.Update(new Entity("ddsm_measure", meas.Id) { Attributes = { new KeyValuePair<string, object>("ddsm_fundingcalculation", Guid.NewGuid().ToString()) } });
                            //measureForUpdate["ddsm_fundingcalculation"] = Guid.NewGuid().ToString();// new Random(6000000).Next();
                            collectData(meas);
                        }
                    }
                }
            }
            else {
                throw new Exception("UserInput parameter  is empty. Nothing to deserialize.");
            }


        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace("DDSM Erorr: " + ex.Message, ex.StackTrace);
            throw;
        }


#if WITH_TASKQ
        processCollectedData();
#endif

    }


#if WITH_TASKQ
    private void processCollectedData()
    {
        if (processedProject.Count > 0)
        {
            var taskQueue = new TaskQueue(_service);


            taskQueue.Create(new DDSM_Task(TaskEntity.Project)
            {
                ProcessedItems0 = processedProject.Select(x => x).Distinct().ToList()
            }, TaskEntity.Project
            );

            //taskQueue.Create(new DDSM_Task(TaskEntity.Financial)
            //{
            //    ProcessedItems0 = processedProject.Select(x => x).Distinct().ToList()
            //}, TaskEntity.Project
            //);

            //taskQueue.Create(new DDSM_Task(TaskEntity.ProgramInterval)
            //{
            //    ProcessedItems0 = processedMeasure.Select(x => x).Distinct().ToList()
            //}, TaskEntity.Measure
            //);


            //taskQueue.Create(new DDSM_Task(TaskEntity.ProjectGroupFinancial)
            //{
            //    ProcessedItems0 = processedProject.Select(x => x).Distinct().ToList()
            //}, TaskEntity.Project
            //);
        }
        else
        {
            _objCommon.TracingService.Trace("No one project was collected. Task wasn't created");
        }

    }
#endif

    private void collectData(Entity meas)
    {
        object projRef = null;
        if (meas.Attributes.TryGetValue("ddsm_projecttomeasureid", out projRef))
            processedProject.Add((projRef as EntityReference).Id);
        processedMeasure.Add(meas.Id);
    }

    private EntityCollection GetMeasuresInfo(List<Guid> IDs)
    {
        // clear 
        var measureIds = IDs.Where(x => !x.Equals(Guid.Empty)).ToList();
        Guid calcTypeId = GetMeasureCalcType();
        var expr = new QueryExpression
        {
            EntityName = MainEntity,
            ColumnSet = new ColumnSet("ddsm_projecttomeasureid"),
            Criteria = new FilterExpression
            {
                Conditions = {
                    new ConditionExpression("ddsm_measureid", ConditionOperator.In, measureIds),
                    new ConditionExpression("ddsm_calculationtype", ConditionOperator.NotEqual, calcTypeId)
                },
                FilterOperator = LogicalOperator.And
            }
        };


        return _service.RetrieveMultiple(expr);
    }

    public Guid GetMeasureCalcType(string name = "ESP")
    {
        try
        {
            var expr = new QueryExpression
            {
                EntityName = "ddsm_measurecalculationtemplate",
                ColumnSet = new ColumnSet("ddsm_name"),
                Criteria = new FilterExpression
                {
                    FilterOperator = LogicalOperator.And,
                    Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, name) }
                }
            };
            var result = _service.RetrieveMultiple(expr);
            if (result.Entities.Count > 0)
            {
                return result.Entities[0].Id;
            }
            throw new Exception("Calc type record with name " + name + " not found");
        }
        catch (Exception ex)
        {
            //_processingLog.Add("WARN: " + ex.Message);
            return Guid.Empty;
        }
    }

}

