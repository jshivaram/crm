﻿using DDSM_CalculationProgramInterval;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.CalculationPlugin.Class
{
    public class ProjectGroupNameOverride : CodeActivity
    {
        #region Parameters

        [Input("Program Offering")]
        [ArgumentEntity("ddsm_programoffering")]
        [ReferenceTarget("ddsm_programoffering")]
        public InArgument<EntityReference> ProgramOffering { get; set; }

        [Output("Result")]
        public OutArgument<string> Result { get; set; }
        #endregion


        #region Internal fields
        Common _objCommon;
        IOrganizationService _service;
        readonly string _ddsmPortfolio = "ddsm_portfolio";
        #endregion

        protected override void Execute(CodeActivityContext context)
        {
            var programOffering = ProgramOffering.Get(context);
            #region "Load CRM Service from context"
            _objCommon = new Common(context);
            _service = _objCommon.Service;
            _objCommon.TracingService.Trace("Load CRM Service from context --- OK");
            #endregion

            //formula Date(YYYYMMDD) – Program Offering Shortname – Delivery Agent Name (on related sub-projects)
            var date = DateTime.UtcNow.ToString("YYYYMMDD");
            //Program Offering ShortName
            var poSn = GetProgramOfferingsShortName(programOffering.Id);


            var result = $"{date}-{poSn}-";
            Result.Set(context, result);

           // 0A7462D6-1CFB-E511-80C0-323166616637
        }


        string GetProgramOfferingsShortName(Guid id)
        {
            var needColumn = "ddsm_shortname";
            var entName = "ddsm_programoffering";
            var result = _service.Retrieve(entName, id, new Microsoft.Xrm.Sdk.Query.ColumnSet(needColumn));

            if (result.Attributes.ContainsKey(needColumn))
            {
                var valOptionSet = result.Attributes[needColumn].ToString();
                int val;
                if (int.TryParse(valOptionSet, out val))
                {
                    return DDSM_CalculationProgramInterval.Utils.Extentions.GetOptionsSetTextForValue(_service, entName, needColumn, val);
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }      


    }
}
