﻿using DDSM_CalculationProgramInterval;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;


/// <summary>
/// This plugin should be recalculate "OEM Recovery Cost" field on Child Measures of Project
/// </summary>
public class RecalculateMeasureOEMRecovery : CodeActivity
{

    [Input("Recovery Percentage")]
    // [ArgumentEntity("ddsm_programoffering")]
    // [ReferenceTarget("ddsm_programoffering")]
    public InArgument<Double> RecoveryPercentage { get; set; }

    #region Internal fields
    Common _objCommon;
    IOrganizationService _service;
    readonly string _ddsmPortfolio = "ddsm_portfolio";

    ExecuteMultipleRequest requestWithResults;

    #endregion

    protected override void Execute(CodeActivityContext context)
    {
        #region "Load CRM Service from context"
        _objCommon = new Common(context);
        _service = _objCommon.Service;
        _objCommon.TracingService.Trace("Load CRM Service from context --- OK");
        #endregion

        requestWithResults = new ExecuteMultipleRequest()
        {
            // Assign settings that define execution behavior: continue on error, return responses. 
            Settings = new ExecuteMultipleSettings()
            {
                ContinueOnError = false,
                ReturnResponses = true
            },
            // Create an empty organization request collection.
            Requests = new OrganizationRequestCollection()
        };


        try
        {
            Entity entityTargetProject = (Entity)_objCommon.Context.InputParameters["Target"];
            if (entityTargetProject != null)
            {
                var reciveryPercentage = RecoveryPercentage.Get(context); 
                var measures = GetMeasuresByProjectId(entityTargetProject.Id);
                foreach (var meas in measures.Entities)
                {
                    UpdateMeasure(meas, reciveryPercentage);
                }
                if (requestWithResults.Requests.Count > 0)
                {
                    ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_service.Execute(requestWithResults);
                }
                else
                {
                    _objCommon.TracingService.Trace("Project has no measures.");
                }
            }

            var rollupRequest =  new CalculateRollupFieldRequest { Target = new EntityReference("ddsm_project", entityTargetProject.Id), FieldName = "ddsm_oemrecoverycost" };
            _service.Execute(rollupRequest);
        }
        catch (Exception ex)
        {
            _objCommon.TracingService.Trace(ex.StackTrace);
        }
    }

    private void UpdateMeasure(Entity meas, double recoveryPercentage)
    {

        var ddsm_incentivepaymentgross = meas.GetAttributeValue<Money>("ddsm_incentivepaymentgross");


        var ddsm_oemrecoverycost = new Money(ddsm_incentivepaymentgross.Value * (decimal)recoveryPercentage);
        var ddsm_incentivepaymentnet = new Money(ddsm_incentivepaymentgross.Value - ddsm_oemrecoverycost.Value);


        meas["ddsm_oemrecoverycost"] = ddsm_oemrecoverycost;
        meas["ddsm_incentivepaymentnet"] = ddsm_incentivepaymentnet;


        UpdateRequest createRequest = new UpdateRequest { Target = meas };
        requestWithResults.Requests.Add(createRequest);   

    }

    private EntityCollection GetMeasuresByProjectId(Guid id)
    {
        var expr = new QueryExpression
        {
            EntityName = "ddsm_measure",
            ColumnSet = new ColumnSet("ddsm_incentivepaymentgross"),
            Criteria = new FilterExpression
            {
                Conditions = {
                        new ConditionExpression("ddsm_projecttomeasureid", ConditionOperator.Equal, id)
                    }
            }
        };

        return _service.RetrieveMultiple(expr);
    }
}

