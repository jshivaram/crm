﻿using DDSM_CalculationProgramInterval;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Activities;
using System;
using Microsoft.Xrm.Sdk.Query;
using DDSM.CalculationPlugin.Utils;

public class RecalculateRelatedRateClassesFromMeasure : CodeActivity
{
    #region "Parameter Definition"

    [Input("Measure")]
    [ArgumentEntity("ddsm_measure")]
    [ReferenceTarget("ddsm_measure")]
    public InArgument<EntityReference> Measure { get; set; }
    #endregion
    #region Internal fields
    Common _objCommon;
    IOrganizationService _service;
    readonly string MainEntity = "ddsm_measure";
    readonly string _ddsmRateClass = "ddsm_rateclass";
    #endregion


    protected override void Execute(CodeActivityContext context)
    {
        #region "Load CRM Service from context"
        _objCommon = new Common(context);
        _service = _objCommon.Service;
        _objCommon.TracingService.Trace("Load CRM Service from context --- OK");
        #endregion

        var targetMeasure = Measure.Get(context);
        if (targetMeasure != null)
        {
            var measureData = GetMeasureData(targetMeasure.Id);
            UpdateRelatedRecords(measureData);
        }
    }

    private void UpdateRelatedRecords(Entity measureData)
    {
        if (measureData != null && measureData.Attributes != null)
        {
            var query = new QueryExpression
            {
                EntityName = _ddsmRateClass,
                ColumnSet = new ColumnSet(allColumns: true),
                Criteria = new FilterExpression(LogicalOperator.And) { Conditions = { new ConditionExpression("ddsm_measureid", ConditionOperator.Equal, measureData.Id) } }
            };

            var rateClasses = _service.RetrieveMultiple(query);
            foreach (var item in rateClasses.Entities)
            {
                var updatedRateClass = CrmHelper.getDataRelationMapping(_service, _ddsmRateClass, MainEntity, measureData.Id);
                updatedRateClass.Id = item.Id;
                _service.Update(updatedRateClass);
            }
        }
    }

    private Entity GetMeasureData(Guid id)
    {
        return _service.Retrieve(MainEntity, id, new ColumnSet(allColumns: true));
    }
}

