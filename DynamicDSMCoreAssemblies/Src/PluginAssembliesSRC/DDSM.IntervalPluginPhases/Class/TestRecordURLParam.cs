﻿using DDSM_CalculationProgramInterval;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;

public class TestRecordURLParam : CodeActivity
{
    #region "Parameter Definition"

    //[Input("Project")]
    //[ArgumentEntity("ddsm_project")]
    //[ReferenceTarget("ddsm_project")]
    //public InArgument<EntityReference> Measure { get; set; }


    [Input("Selected Project ID's")]
    public InArgument<string> UserInput { get; set; }
    #endregion


    #region Internal Fields
    IOrganizationService _service;
    private Common _objCommon;
    //private Helper _helper;
    //CrmHelper crmHelper;
    private string url = "";
    readonly List<string> _processingLog = new List<string>();
    readonly string MainEntity = "ddsm_measure";
    Dictionary<Guid, Guid> ProcessedMeasures = new Dictionary<Guid, Guid>(); // key crm ID, value esp ID

#if WITH_TASKQ
    IList<Guid> processedProject = new List<Guid>(); //list of project 
                                                     // IList<Guid> processedProgOff = new List<Guid>(); //list of project 
    IList<Guid> processedMeasure = new List<Guid>();                                                // IList<Guid> processedMasures = new List<Guid>();
#endif

    #endregion

    protected override void Execute(CodeActivityContext context)
    {
        #region "Load CRM Service from context"

        _objCommon = new Common(context);
        _service = _objCommon.Service;
        _objCommon.TracingService.Trace("Load CRM Service from context --- OK");

        #endregion

        var measureIDsJson = UserInput.Get(context);

        _objCommon.TracingService.Trace("RecordURL: " + measureIDsJson);

    }


}

