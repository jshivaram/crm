﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Metadata.Query;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;

namespace DDSM_CalculationProgramInterval
{
    class Common
    {
        public ITracingService TracingService;
        public IWorkflowContext Context;
        public IOrganizationServiceFactory ServiceFactory;
        public IOrganizationService Service;

        public  Common(CodeActivityContext executionContext)
        {
            TracingService = executionContext.GetExtension<ITracingService>();
            Context = executionContext.GetExtension<IWorkflowContext>();
            ServiceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            Service = ServiceFactory.CreateOrganizationService(Context.UserId);
        }

        /// <summary>
        /// Query the Metadata to get the Entity Schema Name from the Object Type Code
        /// </summary>
        /// <param name="objectTypeCode"></param>
        /// <param name="service"></param>
        /// <returns>Entity Schema Name</returns>
        public string GetEntityNameFromCode(string objectTypeCode, IOrganizationService service)
        {
            var entityFilter = new MetadataFilterExpression(LogicalOperator.And);
            entityFilter.Conditions.Add(new MetadataConditionExpression("ObjectTypeCode", MetadataConditionOperator.Equals, Convert.ToInt32(objectTypeCode)));
            var entityQueryExpression = new EntityQueryExpression
            {
                Criteria = entityFilter
            };
            var retrieveMetadataChangesRequest = new RetrieveMetadataChangesRequest
            {
                Query = entityQueryExpression,
                ClientVersionStamp = null
            };
            var response = (RetrieveMetadataChangesResponse)service.Execute(retrieveMetadataChangesRequest);

            var entityMetadata = response.EntityMetadata[0];
            return entityMetadata.SchemaName.ToLower();
        }

        public List<string> GetEntityAttributesToClone(string entityName, IOrganizationService service) {
            var req = new RetrieveEntityRequest {
                EntityFilters=EntityFilters.Attributes, 
                LogicalName= entityName
            };

            var res = (RetrieveEntityResponse)service.Execute(req);
            var atts = (from attMetadata in res.EntityMetadata.Attributes where attMetadata.IsPrimaryId != null && (attMetadata.IsValidForCreate != null && (attMetadata.IsValidForCreate.Value && !attMetadata.IsPrimaryId.Value)) select attMetadata.LogicalName).ToList();

            return (atts);
        }
    }
}
