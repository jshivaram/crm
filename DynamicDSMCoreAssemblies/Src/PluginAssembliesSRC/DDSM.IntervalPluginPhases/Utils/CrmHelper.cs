﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System;

namespace DDSM.CalculationPlugin.Utils
{
    public static class CrmHelper
    {
        public static Entity getDataRelationMapping(IOrganizationService _orgService, string targetEntity, string sourceEntity, Guid sourceGuid)
        {
            InitializeFromRequest initialize = new InitializeFromRequest();
            initialize.TargetEntityName = targetEntity;
            initialize.EntityMoniker = new EntityReference(sourceEntity, sourceGuid);
            initialize.TargetFieldType = TargetFieldType.All;
            InitializeFromResponse initialized = (InitializeFromResponse)_orgService.Execute(initialize);
            return initialized.Entity;
        }
    }
}
