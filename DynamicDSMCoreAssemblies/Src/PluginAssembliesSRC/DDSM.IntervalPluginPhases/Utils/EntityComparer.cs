﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.CalculationPlugin.Utils
{
    public class EntityComparer : IEqualityComparer<Entity>
    {
        public bool Equals(Entity x, Entity y)
        {
            return x.Id.Equals(y.Id);
        }

        public int GetHashCode(Entity obj)
        {
            unchecked  // overflow is fine
            {
                int hash = 17;
                hash = hash * 23 + (obj.Id.ToString() ?? "").GetHashCode();
               // hash = hash * 23 + (obj.empPL ?? "").GetHashCode();
               // hash = hash * 23 + (obj.empShift ?? "").GetHashCode();
                return hash;
            }
        }
    }
}
