﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Text;
using DDSM.SmartMeasureCalculationPlugin.Model;
using System.Web;
using System.Web.Script.Serialization;

//using DDSM.ESPSmartMeasurePlugin.Model;
//using Newtonsoft.Json;

namespace DDSM.SmartMeasureCalculationPlugin.API
{
    public static class CalculatorWebServiceConsumer
    {
        static CalculatorWebServiceConsumer()
        {
            //// If a specific certificate thumbprint has been defined in the app.config file, use it to validate the server certificate.
            //// NOTE: This is an override -- if the certificate is otherwise valid, e.g. from a recognized authority or added to the
            //// trusted list, then the thumbprint isn't checked.
            //var validThumbprint = "B098638EC17B960CC48CA5BBC0CA12F96F7E4DB8";//ConfigurationManager.AppSettings["CertThumbprint"];

            //if (!string.IsNullOrWhiteSpace(validThumbprint))
            //{
            //    ServicePointManager.ServerCertificateValidationCallback = ((object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) =>
            //    {
            //        // Only check the thumbprint if the certificate is not otherwise valid
            //        var isValid = !chain.ChainStatus.Any(cs => cs.Status != X509ChainStatusFlags.NoError);

            //        if (!isValid)
            //        {
            //            var cert = certificate as X509Certificate2;
            //            if (cert != null)
            //            {
            //                if (cert.Thumbprint.Equals(validThumbprint, StringComparison.OrdinalIgnoreCase))
            //                {
            //                    isValid = true;
            //                }
            //            }
            //        }
            //        return isValid;
            //    });
            //}


        }

        public static AuthenticationResponse Authenticate(string aSBaseUri, string aSEmail, string aSPassword)
        {
            var result = new AuthenticationResponse();

            var url = string.Format(aSBaseUri
                + "/API/Authentication"
                + "/" + aSEmail
                + "/" + aSPassword);

            using (var serviceRequest = new WebClient())
            {

                byte[] responseBytes = serviceRequest.DownloadData(url);

                var jsoNresponse = Encoding.UTF8.GetString(responseBytes);//serviceRequest.DownloadString(new Uri(url));

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                result = serializer.Deserialize<AuthenticationResponse>(jsoNresponse);
            }


            return result;
        }

        public static AvailableCalculatorsResponse GetAvailableCalculators(string aSBaseUri, AvailableCalculatorsRequest aAvailableCalculatorsRequest)
        {
            AvailableCalculatorsResponse result = null;

            var url = string.Format(aSBaseUri + "/API/AvailableCalculators/");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var request = serializer.Serialize(aAvailableCalculatorsRequest);

            var client = new WebClient();
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            var response = client.UploadString(url, "POST", request);

            result = serializer.Deserialize<AvailableCalculatorsResponse>(response.ToString());

            return result;
        }

        public static CalculatorDataDefinitionsResponse GetCalculatorDataDefinitions(string aSBaseUri, CalculatorDataDefinitionsRequest aMadDefinitionRequest)
        {
            CalculatorDataDefinitionsResponse result = null;

            var url = string.Format(aSBaseUri + "/API/CalculatorDataDefinitions/");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var request = serializer.Serialize(aMadDefinitionRequest);

            var client = new WebClient();
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            var response = client.UploadString(url, "POST", request);

            result = serializer.Deserialize<CalculatorDataDefinitionsResponse>(response.ToString());

            return result;
        }

        public static CalculationResponse GetCalculationResponse(string aSBaseUri, CalculationRequest aCalculationRequest)
        {
            CalculationResponse result = null;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var url = string.Format(aSBaseUri + "/API/Calculation/");

            var request = serializer.Serialize(aCalculationRequest);

            var client = new WebClient();
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            var response = client.UploadString(url, "POST", request);

            result = serializer.Deserialize<CalculationResponse>(response.ToString());

            return result;
        }

       
    }
}
