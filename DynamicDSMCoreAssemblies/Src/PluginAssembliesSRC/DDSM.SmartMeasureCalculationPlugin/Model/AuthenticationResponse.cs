﻿using System.Runtime.Serialization;

namespace ESPWebServices.Models
{
    [DataContract]
    public class AuthenticationResponse
    {
        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }

        public AuthenticationResponse()
        {
            ResponseHeader = new ResponseHeader();
        }
    }
}