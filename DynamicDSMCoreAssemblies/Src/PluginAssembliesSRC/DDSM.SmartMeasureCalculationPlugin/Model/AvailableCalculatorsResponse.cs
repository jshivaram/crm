﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ESPWebServices.Models
{
    [DataContract]
    public class AvailableCalculatorsResponse
    {
        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }

        [DataMember]
        public List<ESPWSCalculatorLibraryResponse> CalculatorLibraryResponseList { get; set; }

        public AvailableCalculatorsResponse()
        {
            ResponseHeader = new ResponseHeader();
            CalculatorLibraryResponseList = new List<ESPWSCalculatorLibraryResponse>();
        }
    }

    [DataContract]
    public class ESPWSCalculatorLibraryResponse
    {
        public ResponseHeader ResponseHeader { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string OrganizationName { get; set; }

        [DataMember]
        public Guid ID { get; set; }

        [DataMember]
        public List<ESPWSCalculatorAvailableResponse> CalculatorList { get; set; }

        public ESPWSCalculatorLibraryResponse()
        {
            ResponseHeader = new ResponseHeader();
            CalculatorList = new List<ESPWSCalculatorAvailableResponse>();
        }

        public override string ToString()
        {
            return Name;
        }
    }

    [DataContract]
    public class ESPWSCalculatorAvailableResponse
    {
        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }

        [DataMember]
        public Guid ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public DateTime VersionStartDateTime { get; set; }

        public ESPWSCalculatorAvailableResponse()
        {
            ResponseHeader = new ResponseHeader();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}