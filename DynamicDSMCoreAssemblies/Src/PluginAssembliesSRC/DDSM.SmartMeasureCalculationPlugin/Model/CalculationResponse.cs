﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ESPWebServices.Models
{
    [DataContract]
    public class CalculationResponse
    {
        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }
        [DataMember]
        public List<ESPWSCalculatorCalculation> ESPWSCalculatorCalculationList { get; set; }

        public CalculationResponse()
        {
            ResponseHeader = new ResponseHeader();
            ESPWSCalculatorCalculationList = new List<ESPWSCalculatorCalculation>();
        }
    }

    [DataContract]
    public class ESPWSCalculatorCalculation
    {
        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public Guid ID { get; set; }

        [DataMember]
        public DateTime StartDateTime { get; set; }

        [DataMember]
        public List<ESPWSResponseVariableValue> ESPWSVariableCalculationList { get; set; }

        public ESPWSCalculatorCalculation()
        {
            ResponseHeader = new ResponseHeader();
            ESPWSVariableCalculationList = new List<ESPWSResponseVariableValue>();
        }
    }

    [DataContract]
    public partial class ESPWSResponseVariableValue
    {
        public ESPWSResponseVariableValue()
        {
        }

        public ESPWSResponseVariableValue(ESPWSVariableValueRequest a_espWSRequestVariableValue)
        {
            ResponseHeader = new ResponseHeader();
            Name = a_espWSRequestVariableValue.Name;
            ID = a_espWSRequestVariableValue.ID;
            QDDID = a_espWSRequestVariableValue.QDDID;
            ESPWSVariableType = a_espWSRequestVariableValue.ESPWSVariableType;
            DoubleValue = a_espWSRequestVariableValue.DoubleValue;
            StringValue = a_espWSRequestVariableValue.StringValue;
        }

        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }

        [DataMember]
        public ESPWSVariableType ESPWSVariableType { get; set; }

        [DataMember]
        public Guid ID { get; set; }

        [DataMember]
        public Guid? QDDID { get; set; }

        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Must be null if type is Double or DoubleList.
        /// </summary>
        [DataMember]
        public string StringValue { get; set; }

        /// <summary>
        /// Must be null if type is StringValue or StringList.
        /// </summary>
        [DataMember]
        public double? DoubleValue { get; set; }

        public object Value
        {
            get
            {
                switch (ESPWSVariableType)
                {
                    case ESPWSVariableType.Double:
                        return DoubleValue;

                    case ESPWSVariableType.Text:
                    default:
                        return StringValue;
                }
            }
        }

    }
}