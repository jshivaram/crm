﻿namespace ESPWebServices.Models
{
    public partial class ESPWSResponseVariableValue
    {

        public string DoubleValueString
        {
            get
            {
                var returnValue = "";
                if (DoubleValue.HasValue)
                {
                    returnValue = DoubleValue.Value.ToString();
                }
                return returnValue;
            }
            set
            {
                double doubleValue;
                if (double.TryParse(value, out doubleValue))
                {
                    DoubleValue = doubleValue;
                }
                else
                {
                    DoubleValue = null;
                }
            }
        }
    }
}
