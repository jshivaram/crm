﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ESPWebServices.Models
{
    [DataContract]
    public class CalculatorDataDefinitionsRequest
    {
        [DataMember]
        public RequestHeader RequestHeader { get; set; }

        [DataMember]
        public List<CalculatorDefinitionRequest> CalculatorDefinitionRequestList { get; set; }

        public CalculatorDataDefinitionsRequest()
        {
            RequestHeader = new RequestHeader();
            CalculatorDefinitionRequestList = new List<CalculatorDefinitionRequest>();
        }
    }

    [DataContract]
    public class CalculatorDefinitionRequest
    {
        [DataMember]
        public Guid CalculatorID { get; set; }

        [DataMember]
        public DateTime TargetDateTime { get; set; }
    }
}