﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ESPWebServices.Models
{
    public partial class ESPWSVariableDefinitionResponse : INotifyPropertyChanged
    {
        public delegate void ComboSelectionChangedHandler(int a_iSelectedIndex);
        public event ComboSelectionChangedHandler ComboSelectionChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        public void RaiseDependentSelectionChanged(int a_iSelectedIndex)
        {
            if (ComboSelectionChanged != null)
            {
                ComboSelectionChanged(a_iSelectedIndex);
            }
        }

        public void RaiseValueChangedEvent()
        {
            switch (ESPWSVariableType)
            {
                case ESPWSVariableType.Double:
                    RaisePropertyChanged("DoubleValueString");
                    break;

                case ESPWSVariableType.Text:
                    RaisePropertyChanged("StringValue");
                    break;
            }
        }

        public void RaisePropertyChanged(string a_sPropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(a_sPropertyName));
            }
        }

        public void WireUpDependencies(List<ESPWSVariableDefinitionResponse> a_sourceList)
        {
            // Pull the dependency selections
            if (ComboBoxSelectionDependencyVariableID.HasValue)
            {
                var comboBoxDependencyVariable = a_sourceList.FirstOrDefault(vd => vd.ID == ComboBoxSelectionDependencyVariableID.Value);
                if (comboBoxDependencyVariable != null)
                {
                    SetDependentComboBoxValues(comboBoxDependencyVariable.GetDefaultSelectedIndex());
                    comboBoxDependencyVariable.ComboSelectionChanged += ComboBoxDependencyVariable_ComboSelectionChanged;
                }
            }
        }

        private void ComboBoxDependencyVariable_ComboSelectionChanged(int a_iSelectedIndex)
        {
            SetDependentComboBoxValues(a_iSelectedIndex);

            switch (ESPWSVariableType)
            {
                case ESPWSVariableType.DoubleList:
                    // The event handler for the available values list may change the selected value,
                    // so we need to save that value and reset it after the event handling
                    var fValue = DoubleValue;
                    RaisePropertyChanged("AvailableComboBoxDoubleValues");
                    DoubleValue = fValue;
                    RaisePropertyChanged("DoubleValueString");
                    break;

                case ESPWSVariableType.TextList:
                    // The event handler for the available values list may change the selected value,
                    // so we need to save that value and reset it after the event handling
                    var sValue = StringValue;
                    RaisePropertyChanged("AvailableComboBoxStringValues");
                    StringValue = sValue;
                    RaisePropertyChanged("StringValue");
                    break;
            }
        }

        public string DoubleValueString
        {
            get
            {
                var returnValue = "";
                if (DoubleValue.HasValue)
                {
                    returnValue = DoubleValue.Value.ToString(CultureInfo.InvariantCulture);
                }
                return returnValue;
            }
            set
            {
                double doubleValue;
                if (double.TryParse(value, out doubleValue))
                {
                    DoubleValue = doubleValue;
                }
                else
                {
                    DoubleValue = null;
                }
            }
        }

        public string ComboValues
        {
            get
            {
                var builder = new StringBuilder();
                var separator = string.Empty;
                if (ComboBoxStringValues != null)
                {
                    foreach (var value in ComboBoxStringValues)
                    {
                        builder.Append(separator);
                        builder.Append(value);
                        separator = ",";
                    }
                }
                return builder.ToString();
            }
        }
    }
}
