﻿using System;
using DDSM.CommonProvider.Model;
using DDSM.SmartMeasureCalculationPlugin.Model;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;

public class ESPConfig : IModuleConfig
{

    public ESPConfig(Entity config)
    {
        if (config != null)
        {
            Url = config.GetAttributeValue<string>("ddsm_espurl");
            Login = config.GetAttributeValue<string>("ddsm_esplogin");
            Pass = config.GetAttributeValue<string>("ddsm_esppassword");
            DataPortion = config.GetAttributeValue<decimal>("ddsm_espdataportion");
            LastToken = Convert.FromBase64String(config.GetAttributeValue<string>("ddsm_esplasttoken"));
            RequestReceivedDate = config.GetAttributeValue<DateTime>("ddsm_requestreceiveddate");
            EspMapingData = config.GetAttributeValue<string>("ddsm_espmapingdata");
            EspUniqueQdd = config.GetAttributeValue<string>("ddsm_espuniqueqdd");
            EspQddMappingData = config.GetAttributeValue<string>("ddsm_espqddmappingdata");
            EspRemoteCalculation = config.GetAttributeValue<bool>("ddsm_espremotecalculation");
            EspRemoteCalculationApiUrl = config.GetAttributeValue<string>("ddsm_espremotecalculationapiurl");
            ConfigId = config.Id;
        }
    }
   
    public string ConfigName => nameof(ESPConfig);
    public string Url { get; set; }
    public string Login { get; set; }
    public string Pass { get; set; }
    public decimal DataPortion { get; set; }
    public byte[] LastToken { get; set; }
    public string DefaultLibrary { get; set; }
    public bool AllMadAreRequired { get; set; }
    [JsonConverter(typeof(JsonNetDateConverter))]
    public DateTime RequestReceivedDate { get; set; }
    // mapping data
    public string EspMapingData { get; set; }
    public string EspUniqueQdd { get; set; }
    public string EspQddMappingData { get; set; }
    //remote calculation
    public bool EspRemoteCalculation { get; set; }
    public string EspRemoteCalculationApiUrl { get; set; }
    [JsonConverter(typeof(FakeGuidValueProvider))]
    public Guid ConfigId { get; set; }
}

