﻿namespace ESPWebServices.Models
{
    /// <summary>
    /// Standard error codes returned by ESP Web Services.
    /// </summary>
    public enum ESPErrorCode
    {
        // NOTE: This enum is included primarily for documentation. It may be useful if
        // different handling is desired for different types of errors.

        NoError = 0,
        UndefinedError = -1,

        // Smart measure errors
        FailedToFindCalculatorVersionForThisTargetDate = 10,
        CalculatorNotAvailableToUser = 11,

        // Variable errors
        IncorrectNumberOfInputVariables = 20,
        ESPVariableStringValueMustBeNull = 21,
        ESPVariableDoubleMustBeNull = 22,
        ESPVariableNotFoundInCalculator = 23,
        AttemptToSetESPVariableToInvalIDValue = 24,
        ESPVariableIsNotApplicationData = 25,
        OneOrMoreCalculatorRequestsHasAProblem = 26,
        DualLookupVariable1IsNotApplicationData = 27,
        DualLookupVariable2IsNotApplicationData = 27,

        // Authentication errors
        CannotAccessDatabaseForAuthentication = 40,
        UserNotFound = 41,
        InvalIDPassword = 42,
        UserLockedOut = 43,
        AuthenticationTokenTimedOut = 44,
        UnknownAuthenticationProblem = 49
    }
}