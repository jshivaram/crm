﻿namespace ESPWebServices.Models
{
    public enum ESPWSVariableType
    {
        Double,
        Text,
        DoubleList,
        TextList
    }
}