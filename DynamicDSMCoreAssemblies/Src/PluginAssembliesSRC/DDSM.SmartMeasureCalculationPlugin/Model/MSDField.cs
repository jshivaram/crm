﻿namespace DDSM.SmartMeasureCalculationPlugin.Model
{
    public class MSDField
    {
        public string AttrName { get; set; }
        public string Entity { get; set; }
        public string FieldType { get; set; }
        public string IsReadOnly { get; set; }
        public bool IsRequired { get; set; }
        public string DispName { get; set; }
        public string DispNameOver { get; set; }
    }
}
