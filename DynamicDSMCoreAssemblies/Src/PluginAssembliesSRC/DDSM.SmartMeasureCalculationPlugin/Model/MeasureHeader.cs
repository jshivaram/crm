﻿using System.Runtime.Serialization;

//using Newtonsoft.Json;

namespace DDSM.SmartMeasureCalculationPlugin.Model
{
   [DataContract]
    public class MeasureHeader
    {

       // [JsonRequired, JsonProperty("sss1")]
        public string ID { get; set; }
       // [JsonRequired, JsonProperty("sss2")]
        public string Name { get; set; }

    }
}
