﻿using System.Runtime.Serialization;

namespace ESPWebServices.Models
{
    [DataContract]
    public class RequestHeader
    {
        [DataMember]
        public byte[] AuthenticationTokenBytes { get; set; }
    }
}