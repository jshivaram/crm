﻿using System;
using System.Runtime.Serialization;

namespace ESPWebServices.Models
{
    [DataContract]
    public class ResponseHeader
    {
        [DataMember]
        public byte[] AuthenticationTokenBytes { get; set; }

        [DataMember]
        public DateTime RequestReceivedDateTime { get; set; }

        [DataMember]
        public DateTime ResponseDateTime { get; set; }

        [DataMember]
        public long ProcessingTimeMilliseconds { get; set; }

        [DataMember]
        public bool StatusOk { get; set; }

        [DataMember]
        public bool IsDataFromCache { get; set; }

        [DataMember]
        public bool NeedsAuthentication { get; set; }

        [DataMember]
        public int? ErrorCode { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public string ESPException { get; set; }

        public ResponseHeader()
        {
            ResponseDateTime = DateTime.UtcNow;
            StatusOk = true;
            NeedsAuthentication = false;
            ErrorCode = null;
            ErrorMessage = null;
            ESPException = null;
        }

        public ResponseHeader(bool _statusOK, int a_errorCode, string a_errorMessage, string a_espException)
        {
            ResponseDateTime = DateTime.UtcNow;
            StatusOk = _statusOK;
            ErrorCode = a_errorCode;
            ErrorMessage = a_errorMessage;
            ESPException = a_espException;
        }
    }
}