﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

// ***********************************************************************
//                              CONFIDENTIAL
//                  Copyright 2015, Energy Platforms LLC
//                          All rights reserved.
// ***********************************************************************

namespace DDSM.SmartMeasureCalculationPlugin.Models
{
    // Classes are "flattened" from the proper inheritance structure to make things easier on the JSON web service consumer.

    #region Common Models

    public enum EspwsVariableType
    {
        Double,
        Text,
        DoubleList,
        TextList
    }

    [DataContract]
    public class RequestHeader
    {
        [DataMember]
        public byte[] AuthenticationTokenBytes { get; set; }
    }

    [DataContract]
    public class ResponseHeader
    {
        [DataMember]
        public byte[] AuthenticationTokenBytes { get; set; }

        [DataMember]
        public DateTime RequestReceivedDateTime { get; set; }

        [DataMember]
        public DateTime ResponseDateTime { get; set; }

        [DataMember]
        public long ProcessingTimeMilliseconds { get; set; }

        [DataMember]
        public bool StatusOk { get; set; }

        [DataMember]
        public bool IsDataFromCache { get; set; }

        [DataMember]
        public bool NeedsAuthentication { get; set; }

        [DataMember]
        public int? ErrorCode { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public string EspException { get; set; }

        public ResponseHeader()
        {
            ResponseDateTime = DateTime.UtcNow;
            StatusOk = true;
            NeedsAuthentication = false;
            ErrorCode = null;
            ErrorMessage = null;
            EspException = null;
        }

        public ResponseHeader(bool statusOk, int aErrorCode, string aErrorMessage, string aEspException)
        {
            ResponseDateTime = DateTime.UtcNow;
            StatusOk = statusOk;
            ErrorCode = aErrorCode;
            ErrorMessage = aErrorMessage;
            EspException = aEspException;
        }
    }

    #endregion

    #region Authentication Response Models

    [DataContract]
    public class AuthenticationResponse
    {
        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }

        public AuthenticationResponse()
        {
            ResponseHeader = new ResponseHeader();
        }
    }

    #endregion

    #region Available Smart Measure Request Models

    [DataContract]
    public class AvailableSmartMeasuresRequest
    {
        [DataMember]
        public RequestHeader RequestHeader { get; set; }

        [DataMember]
        public DateTime TargetDateTime { get; set; }

        public AvailableSmartMeasuresRequest()
        {
            RequestHeader = new RequestHeader();
            TargetDateTime = DateTime.UtcNow;
        }
    }
    
    #endregion

    #region Available Smart Measure Response Models

    [DataContract]
    public class AvailableSmartMeasuresResponse
    {
        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }

        [DataMember]
        public List<EspwsSmartMeasureLibraryResponse> SmartMeasureLibraryResponseList { get; set; }

        public AvailableSmartMeasuresResponse()
        {
            ResponseHeader = new ResponseHeader();
            SmartMeasureLibraryResponseList = new List<EspwsSmartMeasureLibraryResponse>();
        }
    }

    [DataContract]
    public class EspwsSmartMeasureAvailableResponse
    {
        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public DateTime VersionStartDateTime { get; set; }

        public EspwsSmartMeasureAvailableResponse()
        {
            ResponseHeader = new ResponseHeader();
        }

        public override string ToString()
        {
            return Name;
        }
    }

    [DataContract]
    public class EspwsSmartMeasureLibraryResponse
    {
        public ResponseHeader ResponseHeader { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string OrganizationName { get; set; }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public List<EspwsSmartMeasureAvailableResponse> SmartMeasureList { get; set; }

        public EspwsSmartMeasureLibraryResponse()
        {
            ResponseHeader = new ResponseHeader();
            SmartMeasureList = new List<EspwsSmartMeasureAvailableResponse>();
        }

        public override string ToString()
        {
            return Name;
        }
    }

    #endregion

    #region MAD Definition Request Models

    [DataContract]
    public class MadDefinitionsRequest
    {
        [DataMember]
        public RequestHeader RequestHeader { get; set; }

        [DataMember]
        public List<SmartMeasureDefinitionRequest> SmartMeasureDefinitionRequestList { get; set; }

        public MadDefinitionsRequest()
        {
            RequestHeader = new RequestHeader();
            SmartMeasureDefinitionRequestList = new List<SmartMeasureDefinitionRequest>();
        }
    }

    [DataContract]
    public class SmartMeasureDefinitionRequest
    {
        [DataMember]
        public Guid SmartMeasureId { get; set; }

        [DataMember]
        public DateTime TargetDateTime { get; set; }
    }

    #endregion

    #region MAD Definition Response Models

    [DataContract]
    public class MadDefinitionsResponse
    {
        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }

        [DataMember]
        public List<EspwsSmartMeasureDefinitionResponse> EspwsSmartMeasureDefinitionResponseList { get; set; }

        public MadDefinitionsResponse()
        {
            ResponseHeader = new ResponseHeader();
            EspwsSmartMeasureDefinitionResponseList = new List<EspwsSmartMeasureDefinitionResponse>();
        }
    }

    [DataContract]
    public class EspwsSmartMeasureDefinitionResponse
    {
        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public DateTime StartDateTime { get; set; }

        [DataMember]
        public List<EspwsVariableDefinitionResponse> EspVariableDefinitionList { get; set; }

        public EspwsSmartMeasureDefinitionResponse()
        {
            ResponseHeader = new ResponseHeader();
            EspVariableDefinitionList = new List<EspwsVariableDefinitionResponse>();
        }
    }

    [DataContract]
    public class EspwsVariableDefinitionResponse
    {
        [DataMember]
        public EspwsVariableType EspwsVariableType { get; set; }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public Guid? Qddid { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool IsReadOnly { get; set; }

        [DataMember]
        public string DisplayFormat { get; set; }

        /// <summary>
        /// Always 0 in current implementation. Kept for possible future use.
        /// </summary>
        [DataMember]
        public int DisplayLevel { get; set; }

        [DataMember]
        public int DisplayOrderRank { get; set; }

        /// <summary>
        /// Must be null if type is DoubleValue or DoubleList.
        /// </summary>
        [DataMember]
        public string DefaultStringValue { get; set; }

        /// <summary>
        /// Must be null if type is StringValue or StringList.
        /// </summary>
        [DataMember]
        public double? DefaultDoubleValue { get; set; }

        /// <summary>
        /// Must be null if type is not a StringList.
        /// </summary>
        [DataMember]
        public List<string> ComboBoxStringValues { get; set; }

        /// <summary>
        /// Must be null if type is not a DoubleList.
        /// </summary>
        [DataMember]
        public List<double?> ComboBoxDoubleValues { get; set; }
    }
    #endregion

    #region Calculation Request Models

    [DataContract]
    public class CalculationRequest
    {
        [DataMember]
        public RequestHeader RequestHeader { get; set; }

        [DataMember]
        public List<SmartMeasureCalculationRequest> SmartMeasureCalculationRequestList { get; set; }

        public CalculationRequest()
        {
            RequestHeader = new RequestHeader();
            SmartMeasureCalculationRequestList = new List<SmartMeasureCalculationRequest>();
        }
    }

    [DataContract]
    public class SmartMeasureCalculationRequest
    {
        [DataMember]
        public Guid SmartMeasureId { get; set; }

        [DataMember]
        public DateTime TargetDateTime { get; set; }

        [DataMember]
        public List<EspwsVariableValueRequest> MadVariables { get; set; }

        public SmartMeasureCalculationRequest()
        {
            MadVariables = new List<EspwsVariableValueRequest>();
        }
    }

    [DataContract]
    public class EspwsVariableValueRequest
    {
        [DataMember]
        public EspwsVariableType EspwsVariableType { get; set; }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public Guid? Qddid { get; set; }

        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Must be null if type is Double or DoubleList.
        /// </summary>
        [DataMember]
        public string StringValue { get; set; }

        /// <summary>
        /// Must be null if type is StringValue or StringList.
        /// </summary>
        [DataMember]
        public double? DoubleValue { get; set; }
    }

    #endregion

    #region Calculation Response Models

    [DataContract]
    public class CalculationResponse
    {
        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }
        [DataMember]
        public List<EspwsSmartMeasureCalculation> EspwsSmartMeasureCalculationList { get; set; }

        public CalculationResponse()
        {
            ResponseHeader = new ResponseHeader();
            EspwsSmartMeasureCalculationList = new List<EspwsSmartMeasureCalculation>();
        }
    }

    [DataContract]
    public class EspwsSmartMeasureCalculation
    {
        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public DateTime StartDateTime { get; set; }

        [DataMember]
        public List<EspwsResponseVariableValue> EspwsVariableCalculationList { get; set; }

        public EspwsSmartMeasureCalculation()
        {
            ResponseHeader = new ResponseHeader();
            EspwsVariableCalculationList = new List<EspwsResponseVariableValue>();
        }
    }

    [DataContract]
    public class EspwsResponseVariableValue
    {
        public EspwsResponseVariableValue()
        {
        }

        public EspwsResponseVariableValue(EspwsVariableValueRequest aEspWsRequestVariableValue)
        {
            ResponseHeader = new ResponseHeader();
            Name = aEspWsRequestVariableValue.Name;
            Id = aEspWsRequestVariableValue.Id;
            Qddid = aEspWsRequestVariableValue.Qddid;
            EspwsVariableType = aEspWsRequestVariableValue.EspwsVariableType;
            DoubleValue = aEspWsRequestVariableValue.DoubleValue;
            StringValue = aEspWsRequestVariableValue.StringValue;
        }

        [DataMember]
        public ResponseHeader ResponseHeader { get; set; }

        [DataMember]
        public EspwsVariableType EspwsVariableType { get; set; }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public Guid? Qddid { get; set; }

        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Must be null if type is Double or DoubleList.
        /// </summary>
        [DataMember]
        public string StringValue { get; set; }

        /// <summary>
        /// Must be null if type is StringValue or StringList.
        /// </summary>
        [DataMember]
        public double? DoubleValue { get; set; }
    }

    #endregion


}
