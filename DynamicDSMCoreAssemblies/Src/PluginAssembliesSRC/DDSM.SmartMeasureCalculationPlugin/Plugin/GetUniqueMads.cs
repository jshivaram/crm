﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Script.Serialization;
using DDSM.SmartMeasureCalculationPlugin.Model;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;

namespace DDSM.SmartMeasureCalculationPlugin.Plugin
{
    public class GetUniqueMads : IPlugin
    {



        private string _webAddress;
        private IOrganizationService _service;
        private IOrganizationServiceFactory _serviceFactory;
        private Entity _config;
        public GetUniqueMads(string url, string user, string pass)
        {
            //try
            //{
            //    if (String.IsNullOrEmpty(url))
            //    {
            //        config = GetConfig();
            //        webAddress= config["ddsm_espurl"].ToString();
            //    }
            //}
            //catch (Exception)
            //{
            //    throw;
            //}


        }
        public GetUniqueMads()
        {
            //try
            //{
            //    config = GetConfig();
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
        }

        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService =
                (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            //<snippetFollowupPlugin1>
            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)
                serviceProvider.GetService(typeof(IPluginExecutionContext));
            //</snippetFollowupPlugin1>

            // Obtain the organization service reference.
            _serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            _service = _serviceFactory.CreateOrganizationService(context.UserId);
            _config = GetConfig();
            //if (context.InputParameters.Contains("URL") && context.InputParameters["URL"] is string)
            //{
            //    webAddress = context.InputParameters["URL"].ToString();

            //}

            try
            {


                if (_config != null)
                {
                    if ( // config.Attributes.ContainsKey("ddsm_espurl") 
                        _config.Attributes.ContainsKey("ddsm_esplogin") &&
                        _config.Attributes.ContainsKey("ddsm_esppassword")
                        )
                    {
                        var credential = new Dictionary<string, object>
                        {
                            {"URI",  _config["ddsm_espurl"].ToString()},
                            {"UserName", _config["ddsm_esplogin"].ToString()},
                            {"Password", _config["ddsm_esppassword"].ToString()}
                          //  {"Portion", config["ddsm_espdataportion"]}

                        };

                        #region Get currentAuthenticationResponse

                        // ApiHelper helper = new ApiHelper(credential);
                        var currentAuthenticationResponse = new AuthenticationResponse();

                        var url = string.Format(credential["URI"].ToString()
                            + "/API/Authentication"
                            + "/" + credential["UserName"].ToString()
                            + "/" + credential["Password"].ToString());


                        using (var serviceRequest = new WebClient())
                        {

                            string responseBytes = serviceRequest.DownloadString(url);
                            currentAuthenticationResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(responseBytes);
                        }
                        #endregion


                      //  currentAuthenticationResponse.ResponseHeader.ResponseDateTime.ToString();


                        if (currentAuthenticationResponse == null)
                        {
                            throw new Exception("Error with getting MAD definitions");
                        }
                        //else
                        //{
                        //    throw new Exception("Complete!!!" );
                        //}

                    }

                }
                else
                {
                    throw new Exception("Config is null((");

                }
            }
            catch (Exception ex)
            {
                Entity account = new Entity("ddsm_admindata");
                account["ddsm_esplog"] = ex.Message + ex.Source + ex.StackTrace;
                account["ddsm_name"] = "Plugin";
                // Create an account record named Fourth Coffee.
                var accountId = _service.Create(account);


                tracingService.Trace(ex.Message);


                throw;
            }
        }




        Entity GetConfig()
        {
            var adminDataId = "F56C3086-9AF2-E411-80EC-FC15B4284D68"; //main admin data record with current config
            var result = new Entity();
            try
            {

                result = _service.Retrieve("ddsm_admindata", Guid.Parse(adminDataId),
                        new ColumnSet("ddsm_espurl", "ddsm_esplogin", "ddsm_esppassword", "ddsm_espdataportion"));
            }
            catch (Exception)
            {

                throw;
            }
            // F56C3086-9AF2-E411-80EC-FC15B4284D68
            return result;
        }
    }

}
