﻿using System.Collections.Generic;
using ESPWebServices.Models;

namespace DDSM.SmartMeasureCalculationPlugin.Utils
{
    public class Comparer: IEqualityComparer<ESPWSVariableDefinitionResponse>
    {
        public bool Equals(ESPWSVariableDefinitionResponse x, ESPWSVariableDefinitionResponse y)
        {
            if (x?.Name == y?.Name)
                return true;
            return false;
        }

        public int GetHashCode(ESPWSVariableDefinitionResponse obj)
        {
            return obj.GetHashCode();
        }
    }
}
