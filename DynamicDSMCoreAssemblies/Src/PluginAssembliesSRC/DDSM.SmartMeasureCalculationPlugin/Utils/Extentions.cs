﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using ESPWebServices.Models;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using DDSM.CommonProvider;

namespace DDSM.SmartMeasureCalculationPlugin.Utils
{
    public static class Extentions
    {
        public static Object GetRecordValue(this Object record)
        {

            if (record is AliasedValue && ((AliasedValue)record).Value != null)
            {
                return ((AliasedValue)record).Value;
            }
            if (record != null && record is Money)
            {
                return ((Money)record).Value;
            }
            if (record != null && !(record is AliasedValue))
                return record;
            return null;
        }


        public static string GetFullESPError(this CalculationResponse response)
        {
            var withError = new List<string>();
            if (!response.ResponseHeader.StatusOk)
            {
                withError.Add(response.ResponseHeader.ErrorMessage);
                foreach (var calc in response.ESPWSCalculatorCalculationList.Where(x => x.ResponseHeader.StatusOk == false))
                {
                    withError.Add(response.ESPWSCalculatorCalculationList[0].ResponseHeader.ErrorMessage);
                
                    withError.Add("Calc Id:" + calc.ID);
                    foreach (var madVariable in calc.ESPWSVariableCalculationList.Where(x => x.ResponseHeader.StatusOk == false))
                    {
                        withError.Add($"Measure name: {calc.Name}, MAD field: {madVariable.Name}. Error: {madVariable.ResponseHeader.ErrorMessage}, MAD Json:{JsonConvert.SerializeObject(madVariable)}");
                    }
                }
            }
            return string.Join("\n", withError);
        }

        public static EntityReference GetTargetData(Common objDdsm)
        {
            objDdsm.TracingService.Trace("in GetTargetData:");
            EntityReference target = new EntityReference();
            if (objDdsm.Context.InputParameters.ContainsKey("Target"))
            {
                objDdsm.TracingService.Trace("objDdsm.Context.InputParameters.ContainsKey(Target)");
                objDdsm.TracingService.Trace("in GetTargetData:");
                if (objDdsm.Context.InputParameters["Target"] is Entity)
                {
                    target = ((Entity)objDdsm.Context.InputParameters["Target"]).ToEntityReference();
                }
                else if (objDdsm.Context.InputParameters["Target"] is EntityReference)
                {
                    target = (EntityReference)objDdsm.Context.InputParameters["Target"];
                }
            }
            //else if (objDdsm.Context.ParentContext.InputParameters.ContainsKey("Target"))
            //{
            //    objDdsm.TracingService.Trace("NOT objDdsm.Context.InputParameters.ContainsKey(Target)");

            //    if (objDdsm.Context.ParentContext.InputParameters["Target"] is Entity)
            //    {
            //        target = ((Entity)objDdsm.Context.ParentContext.InputParameters["Target"]).ToEntityReference();
            //    }
            //    else if (objDdsm.Context.ParentContext.InputParameters["Target"] is EntityReference)
            //    {
            //        target = (EntityReference)objDdsm.Context.ParentContext.InputParameters["Target"];
            //    }
            //}

            objDdsm.TracingService.Trace("Target LogicalName: " + target.LogicalName + " Id: " + target.Id.ToString());
            // throw new Exception("FAKE Exception::::: " + string.Join(",", ((MoskTraceService)objDdsm.TracingService).Logs));
            return target;
        }


    }
}
