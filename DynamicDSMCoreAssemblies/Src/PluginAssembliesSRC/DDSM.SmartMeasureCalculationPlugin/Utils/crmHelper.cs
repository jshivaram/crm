﻿using DDSM.SmartMeasureCalculationPlugin.Model;
using ESPWebServices.Models;
using Microsoft.Crm.Sdk.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using DDSM.CommonProvider;
using Newtonsoft.Json;

namespace DDSM.SmartMeasureCalculationPlugin.Utils
{
    public class CrmHelper
    {
        IOrganizationService service;
        public CrmHelper(IOrganizationService _service)
        {
            service = _service;
        }

        /// <summary>
        /// This function is used to retrieve the optionset value using the optionset text label
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="attributeName"></param>
        /// <param name="selectedLabel"></param>
        /// <returns></returns>
        public int GetOptionsSetValueForLabel(string entityName, string attributeName, string selectedLabel)
        {
            var selectedOptionValue = -1;
            try
            {
                var retrieveAttributeRequest = new RetrieveAttributeRequest
                {
                    EntityLogicalName = entityName,
                    LogicalName = attributeName.ToLower(),
                    RetrieveAsIfPublished = true
                };
                // Execute the request.
                var retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
                // Access the retrieved attribute.
                var retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
                // Get the current options list for the retrieved attribute.
                var optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();

                foreach (var oMD in optionList)
                {
                    if (!String.Equals(oMD.Label.LocalizedLabels[0].Label.Trim(), selectedLabel.Trim(),
                        StringComparison.CurrentCultureIgnoreCase)) continue;
                    if (oMD.Value != null) selectedOptionValue = oMD.Value.Value;
                    break;
                }
            }
            catch (Exception ex)
            {
                return selectedOptionValue;
            }
            return selectedOptionValue;
        }

        /// <summary>
        /// This function is used to retrieve the optionset labek using the optionset value
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="attributeName"></param>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        public string GetOptionsSetTextForValue(string entityName, string attributeName, int selectedValue)
        {
            var retrieveAttributeRequest = new
            RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = attributeName,
                RetrieveAsIfPublished = true
            };
            // Execute the request.
            var retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
            // Access the retrieved attribute.
            var retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)
            retrieveAttributeResponse.AttributeMetadata;// Get the current options list for the retrieved attribute.
            var optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
            string selectedOptionLabel = null;
            foreach (var oMD in optionList)
            {
                if (oMD.Value == selectedValue)
                {
                    selectedOptionLabel = oMD.Label.LocalizedLabels[0].Label;
                    break;
                }
            }
            return selectedOptionLabel;
        }

        /// <summary>
        /// Set fileds by his type
        /// </summary>
        /// <param name="mapItem"></param>
        /// <param name="madResult"></param>
        /// <param name="newMeasure"></param>
        public void SetAttributeValue(MappingItem mapItem, ESPWSResponseVariableValue madResult, Entity newMeasure)
        {
            try
            {
                var name = mapItem.AttrName.ToLower();
                if (string.IsNullOrEmpty(name))
                    throw new Exception("in SetAttributeValue() error: mapItem.AttrName is empty");

                decimal decVal;
                if (null != madResult.Value && null != mapItem.FieldType)
                    switch (mapItem.FieldType)
                    {
                        case "Picklist":
                            //  int index;
                            //  int.TryParse(madResult.Value.ToString(), out index);
                            var value = GetOptionsSetValueForLabel(mapItem.Entity, mapItem.AttrName, madResult.Value.ToString());
                            // if (value != 0)
                            newMeasure.Attributes[name] = new OptionSetValue(value);
                            break;
                        case "Decimal":
                            if (decimal.TryParse(madResult.Value.ToString(), out decVal))
                            {
                                newMeasure.Attributes[name.ToLower()] = decVal;
                            }
                            break;
                        case "Double":
                            double doubleVal;
                            if (double.TryParse(madResult.Value.ToString(), out doubleVal))
                            {
                                newMeasure.Attributes[name.ToLower()] = doubleVal;
                            }
                            break;
                        case "Money":
                            if (decimal.TryParse(madResult.Value.ToString(), out decVal))
                            {
                                newMeasure.Attributes[name.ToLower()] = new Money(decVal);
                            }
                            break;
                        case "DateTime":
                        case "String":
                        default:
                            newMeasure.Attributes.Add(name, madResult.Value);
                            break;
                    }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Set fileds by his type
        /// </summary>
        /// <param name="mapItem"></param>
        /// <param name="newMeasure"></param>
        public void SetMSDAttributeValue(MSDField mapItem, string msdValue, Entity newMeasure)
        {
            var name = mapItem.AttrName.ToLower();
            var decVal = new decimal();
            if (null != msdValue)   // && !newMeasure.Attributes.ContainsKey(name)
                switch (mapItem.FieldType)
                {
                    case "Picklist":
                        if (!string.IsNullOrEmpty(msdValue))
                        {
                            newMeasure.Attributes[name] = new OptionSetValue(int.Parse(msdValue));
                        }
                        break;
                    case "Decimal":

                        if (decimal.TryParse(msdValue, out decVal))
                        {
                            newMeasure.Attributes[name.ToLower()] = decVal;
                        }

                        break;
                    case "Double":
                        var doubleVal = new double();
                        if (double.TryParse(msdValue, out doubleVal))
                        {
                            newMeasure.Attributes[name.ToLower()] = doubleVal;
                        }

                        break;
                    case "Money":
                        if (decimal.TryParse(msdValue, out decVal))
                        {
                            newMeasure.Attributes[name.ToLower()] = new Money(decVal);
                        }
                        break;
                    case "Lookup":
                        var lookup = msdValue.Split('|');
                        if (lookup.Length >= 2 && !string.IsNullOrEmpty(lookup[0]) && !string.IsNullOrEmpty(lookup[1]))
                            newMeasure.Attributes.Add(name, new EntityReference(lookup[0], Guid.Parse(lookup[1])));
                        break;
                    case "DateTime":
                    case "String":
                    default:
                        newMeasure.Attributes.Add(name, msdValue);
                        break;
                }

        }
        /// <summary>
        /// Set fileds by his type
        /// </summary>
        /// <param name="mapItem"></param>
        /// <param name="newMeasure"></param>
        public void SetMADAttributeValue(MappingItem mapItem, string madValue, Entity newMeasure)
        {
            var name = mapItem.AttrName.ToLower();
            var decVal = new decimal();
            if (null != madValue)   // && !newMeasure.Attributes.ContainsKey(name)
                switch (mapItem.FieldType)
                {
                    case "Picklist":
                        if (!string.IsNullOrEmpty(madValue))
                        {
                            newMeasure[name] = new OptionSetValue(int.Parse(madValue));
                        }
                        break;
                    case "Decimal":

                        if (decimal.TryParse(madValue, out decVal))
                        {
                            newMeasure[name] = decVal;
                        }

                        break;
                    case "Double":
                        var doubleVal = new double();
                        if (double.TryParse(madValue, out doubleVal))
                        {
                            newMeasure[name] = doubleVal;
                        }

                        break;
                    case "Money":
                        if (decimal.TryParse(madValue, out decVal))
                        {
                            newMeasure[name] = new Money(decVal);
                        }
                        break;
                    case "Lookup":
                        var lookup = madValue.Split('|');
                        if (lookup.Length >= 2 && !string.IsNullOrEmpty(lookup[0]) && !string.IsNullOrEmpty(lookup[1]))
                            newMeasure[name] = new EntityReference(lookup[0], Guid.Parse(lookup[1]));
                        break;
                    case "DateTime":
                    case "String":
                    default:
                        newMeasure[name] = madValue;
                        break;
                }

        }

        public int GetOptionsSetValueForIndex(string entityName, string attributeName, int index)
        {
            int selectedOptionValue = -1;

            try
            {
                var retrieveAttributeRequest = new RetrieveAttributeRequest
                {
                    EntityLogicalName = entityName,
                    LogicalName = attributeName.ToLower(),
                    RetrieveAsIfPublished = true
                };
                // Execute the request.
                RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
                // Access the retrieved attribute.
                PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
                // Get the current options list for the retrieved attribute.
                OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();

                if (optionList.Count() > index)
                    selectedOptionValue = (int)optionList[index].Value;
            }
            catch (Exception ex)
            {
                return selectedOptionValue;
            }

            return selectedOptionValue;
        }

        public int GetOptionsSetIndexByValue(string entityName, string attributeName, int value)
        {
            int selectedOptionValue = -1;
            try
            {
                RetrieveAttributeRequest retrieveAttributeRequest = new
                    RetrieveAttributeRequest
                {
                    EntityLogicalName = entityName,
                    LogicalName = attributeName.ToLower(),
                    RetrieveAsIfPublished = true
                };
                // Execute the request.
                var retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
                // Access the retrieved attribute.
                var retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
                // Get the current options list for the retrieved attribute.
                var optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();

                for (int i = 0; i < optionList.Count(); i++)
                {
                    if (optionList[i].Value == value)
                    {
                        selectedOptionValue = i;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                return selectedOptionValue;
            }

            return selectedOptionValue;
        }

        public EntityReference GetMeasureCalcType(string name = "ESP")
        {
            try
            {
                var expr = new QueryExpression
                {
                    EntityName = "ddsm_measurecalculationtemplate",
                    ColumnSet = new ColumnSet("ddsm_name"),
                    Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_name", ConditionOperator.Equal, name) }
                    }
                };
                var result = service.RetrieveMultiple(expr);
                if (result.Entities.Count > 0)
                {
                    return result.Entities[0].ToEntityReference();
                }
                throw new Exception("Calc type record with name " + name + " not found");
            }
            catch (Exception ex)
            {
                return new EntityReference();
            }
        }

        public Entity GetMeasureTmplById(Guid measureTmplId)
        {
            try
            {
                var measureTmpl = service.Retrieve("ddsm_measuretemplate", measureTmplId, new ColumnSet("ddsm_measurecalculationtype", "ddsm_msdfields"));
                if (measureTmpl == null)
                    return null;
                return measureTmpl;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Guid GetMeasureTmplCalcTypeId(Entity measureTmpl)
        {
            try
            {
                if (measureTmpl == null)
                    return Guid.Empty;
                return ((EntityReference)measureTmpl.Attributes["ddsm_measurecalculationtype"]).Id;
            }
            catch (Exception)
            {
                return Guid.Empty;
            }
        }

        public string GetMeasureCalcTypeNameById(Guid id)
        {
            try
            {
                var result = service.Retrieve("ddsm_measurecalculationtemplate", id, new ColumnSet("ddsm_name"));
                if (result == null)
                    return string.Empty;
                return result.Attributes["ddsm_name"].ToString();
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public Entity GetDataRelationMapping(string targetEntity, string sourceEntity, Guid sourceGuid)
        {
            InitializeFromRequest initialize = new InitializeFromRequest();
            initialize.TargetEntityName = targetEntity;
            initialize.EntityMoniker = new EntityReference(sourceEntity, sourceGuid);
            initialize.TargetFieldType = TargetFieldType.All;
            InitializeFromResponse initialized = (InitializeFromResponse)service.Execute(initialize);
            return initialized.Entity;
        }

        #region Clone MT Part
        public Guid CloneMTRecord(Guid idForClone, Common common)
        {
            string entityName = "ddsm_measuretemplate";

            common.TracingService.Trace("Record ID: " + idForClone + " LogicalName: " + entityName);

            Entity retrievedObject = common.GetOrgService().Retrieve(entityName, idForClone, new ColumnSet(allColumns: true));
            common.TracingService.Trace("retrieved object OK");

            Entity newEntity = new Entity(entityName);
            string PrimaryIdAttribute = "";
            List<string> atts = getEntityAttributesToClone(entityName, ref PrimaryIdAttribute);

            foreach (string att in atts)
            {

                if (retrievedObject.Attributes.Contains(att) && att != "statuscode" && att != "statecode"
                    || att.StartsWith("partylist-", StringComparison.Ordinal))
                {
                    if (att.StartsWith("partylist-", StringComparison.Ordinal))
                    {
                        string att2 = att.Replace("partylist-", "");

                        string fetchParty = @"<fetch version='1.0' output-format='xml - platform' mapping='logical' distinct='true'>
                                                <entity name='activityparty'>
                                                    <attribute name = 'partyid'/>
                                                        <filter type = 'and' >
                                                            <condition attribute = 'activityid' operator= 'eq' value = '" + idForClone + @"' />
                                                            <condition attribute = 'participationtypemask' operator= 'eq' value = '" + getParticipation(att2) + @"' />
                                                         </filter>
                                                </entity>
                                            </fetch> ";

                        RetrieveMultipleRequest fetchRequest1 = new RetrieveMultipleRequest
                        {
                            Query = new FetchExpression(fetchParty)
                        };
                        common.TracingService.Trace(fetchParty);
                        EntityCollection returnCollection = ((RetrieveMultipleResponse)common.GetOrgService().Execute(fetchRequest1)).EntityCollection;


                        EntityCollection arrPartiesNew = new EntityCollection();
                        common.TracingService.Trace("attribute:{0}", att2);

                        foreach (Entity ent in returnCollection.Entities)
                        {
                            Entity party = new Entity("activityparty");
                            EntityReference partyid = (EntityReference)ent.Attributes["partyid"];


                            party.Attributes.Add("partyid", new EntityReference(partyid.LogicalName, partyid.Id));
                            common.TracingService.Trace("attribute:{0}:{1}:{2}", att2, partyid.LogicalName, partyid.Id.ToString());
                            arrPartiesNew.Entities.Add(party);
                        }

                        newEntity.Attributes.Add(att2, arrPartiesNew);
                        continue;

                    }

                    common.TracingService.Trace("attribute:{0}", att);
                    newEntity.Attributes.Add(att, retrievedObject.Attributes[att]);
                }
            }

            common.TracingService.Trace("generation new Measure Number...");
            if (newEntity.Attributes.Contains("ddsm_measureid"))
            {
                string measureNumber = string.Empty;
                string measureID = newEntity.GetAttributeValue<string>("ddsm_measureid");
                if (!string.IsNullOrEmpty(measureID))
                {
                    QueryExpression query = new QueryExpression();
                    query.EntityName = "ddsm_measuretemplate";
                    query.ColumnSet = new ColumnSet();
                    query.ColumnSet.Columns.Add("ddsm_measurenumber");
                    query.ColumnSet.Columns.Add("ddsm_measureid");

                    FilterExpression filter = new FilterExpression(LogicalOperator.And);

                    ConditionExpression condition = new ConditionExpression();
                    condition.AttributeName = "ddsm_measureid";
                    condition.Values.Add(measureID);
                    condition.Operator = ConditionOperator.Equal;
                    filter.Conditions.Add(condition);
                    query.Criteria = filter;

                    query.AddOrder("ddsm_measurenumber", OrderType.Descending);

                    EntityCollection results = common.GetOrgService().RetrieveMultiple(query);

                    if (results.Entities.Count > 0)
                    {
                        measureNumber = results.Entities[0]["ddsm_measurenumber"].ToString();

                        common.TracingService.Trace("measureNumber: {0} measureID:{1}", measureNumber, measureID);

                        int numValue = 1;
                        if (!string.IsNullOrEmpty(measureNumber))
                        {
                            measureNumber = measureNumber.Replace(measureID, "");
                            if (Int32.TryParse(measureNumber, out numValue))
                            {
                                numValue++;
                            }
                        }
                        common.TracingService.Trace("numValue: {0} ", numValue);

                        measureNumber = measureID + (numValue.ToString()).PadLeft(3, '0');


                        newEntity["ddsm_measurenumber"] = measureNumber;
                    }
                }
            }

            //// set name with (current)
            //if (newEntity.Contains("ddsm_name") && newEntity["ddsm_name"].ToString().Contains(" (current)"))
            //{
            //    //common.Service.Update(new Entity("ddsm_measuretemplate", idForClone) {
            //    //    Attributes = new AttributeCollection {
            //    //        new KeyValuePair<string, object>("ddsm_name", newEntity["ddsm_name"].ToString().Replace(" (current)", ""))
            //    //    }
            //    //});

            //}
            //else
            {
                newEntity["ddsm_name"] = newEntity["ddsm_name"] + " (current)";
            }

            Guid createdGUID = common.GetOrgService().Create(newEntity);

            common.TracingService.Trace("created cloned object OK");

            return createdGUID;
            /*
            Entity record = objDdsm.orgService.Retrieve(entityName, createdGUID, new ColumnSet("statuscode", "statecode"));

            if (retrievedObject.Attributes["statuscode"] != record.Attributes["statuscode"] ||
                retrievedObject.Attributes["statecode"] != record.Attributes["statecode"])
            {
                Entity setStatusEnt = new Entity(entityName, createdGUID);
                setStatusEnt.Attributes.Add("statuscode", retrievedObject.Attributes["statuscode"]);
                setStatusEnt.Attributes.Add("statecode", retrievedObject.Attributes["statecode"]);

                objDdsm.orgService.Update(setStatusEnt);
            }

            objDdsm.tracingService.Trace("cloned object OK");
            */

            // common.TracingService.Trace("cloned all objects --- OK");
            //this.CloneGuids.Set(executionContext, string.Join("|", objectCloneIds.ToArray()));
            //this.Complete.Set(executionContext, true);
        }

        static string getParticipation(string attributeName)
        {
            string sReturn = "";
            switch (attributeName)
            {
                case "from":
                    sReturn = "1";
                    break;
                case "to":
                    sReturn = "2";
                    break;
                case "cc":
                    sReturn = "3";
                    break;
                case "bcc":
                    sReturn = "4";
                    break;

                case "organizer":
                    sReturn = "7";
                    break;
                case "requiredattendees":
                    sReturn = "5";
                    break;
                case "optionalattendees":
                    sReturn = "6";
                    break;
                case "customer":
                    sReturn = "11";
                    break;
                case "resources":
                    sReturn = "10";
                    break;
            }
            return sReturn;
        }

        List<string> getEntityAttributesToClone(string entityName, ref string PrimaryIdAttribute)
        {

            List<string> atts = new List<string>();
            RetrieveEntityRequest req = new RetrieveEntityRequest()
            {
                EntityFilters = EntityFilters.Attributes,
                LogicalName = entityName
            };

            RetrieveEntityResponse res = (RetrieveEntityResponse)service.Execute(req);
            PrimaryIdAttribute = res.EntityMetadata.PrimaryIdAttribute;

            foreach (AttributeMetadata attMetadata in res.EntityMetadata.Attributes)
            {
                if ((attMetadata.IsValidForCreate.Value || attMetadata.IsValidForUpdate.Value)
                    && !attMetadata.IsPrimaryId.Value)
                {
                    //tracingService.Trace("Tipo:{0}", attMetadata.AttributeTypeName.Value.ToLower());
                    if (attMetadata.AttributeTypeName.Value.ToLower() == "partylisttype")
                    {
                        atts.Add("partylist-" + attMetadata.LogicalName);
                        //atts.Add(attMetadata.LogicalName);
                    }
                    else
                    {
                        atts.Add(attMetadata.LogicalName);
                    }
                }
            }

            return (atts);
        }
        #endregion

        double? GetDoubleValue(Entity data, string crmFiledName)
        {
            object value;
            double doubleVal;
            if (data.Attributes.TryGetValue(crmFiledName.ToLower(), out value))
            {
                var valueEntity = value.GetRecordValue();
                if (valueEntity != null)
                {
                    if (double.TryParse(valueEntity.ToString(), out doubleVal))
                    {
                        return doubleVal;
                    }
                    else return null;
                }
                else return null;
            }
            else
            {
                return null;
            }
        }

        string GetStringValue(Entity data, string crmFiledName, ESPWSVariableDefinitionResponse mad)
        {
            object value;
            if (data.Attributes.TryGetValue(crmFiledName.ToLower(), out value))
            {
                if (value is OptionSetValue && mad.ComboBoxStringValues.Count > 0)
                {
                    var tmpIndex = GetOptionsSetIndexByValue("ddsm_measure", crmFiledName.ToLower(), (value as OptionSetValue).Value);
                    if (tmpIndex < mad.ComboBoxStringValues.Count && tmpIndex >= 0)
                    {
                        return mad.ComboBoxStringValues[tmpIndex];
                    }
                    else
                    {
                        return mad.ComboBoxStringValues[0];
                    }
                }
                else
                {
                    return value.ToString();
                }
            }
            else return "";
        }

        public ESPWSVariableValueRequest GenerateESPWSVariableValueRequest(KeyValuePair<string, MappingItem> crmFiled, ESPWSVariableDefinitionResponse mad, Entity data, string MainEntity)
        {
            var variableValueRequest = new ESPWSVariableValueRequest
            {
                ESPWSVariableType = mad.ESPWSVariableType
            };

            try
            {
                //main entity
                if (!crmFiled.Equals(new KeyValuePair<string, MappingItem>()) && crmFiled.Value.Entity.Equals(MainEntity))
                {
                    var crmFiledName = crmFiled.Value.AttrName.ToLower();
                    switch (mad.ESPWSVariableType)
                    {
                        case ESPWSVariableType.Double:
                            variableValueRequest.DoubleValue = GetDoubleValue(data, crmFiledName);
                            break;
                        case ESPWSVariableType.Text:
                            variableValueRequest.StringValue = GetStringValue(data, crmFiledName, mad);
                            break;
                        case ESPWSVariableType.DoubleList:
                            if (data.Attributes.ContainsKey(crmFiledName))
                                variableValueRequest.DoubleValue = Double.Parse(GetOptionsSetTextForValue(data.LogicalName, crmFiledName, data.GetAttributeValue<OptionSetValue>(crmFiledName).Value));
                            break;
                        case ESPWSVariableType.TextList:
                            if (data.Attributes.ContainsKey(crmFiledName))
                                variableValueRequest.StringValue = GetOptionsSetTextForValue(data.LogicalName, crmFiledName, data.GetAttributeValue<OptionSetValue>(crmFiledName).Value);
                            break;
                        default:
                            break;
                    }
                }
                // fill data from related tables
                else if (!crmFiled.Equals(new KeyValuePair<string, MappingItem>()))
                {
                    var relEnt = data.RelatedEntities.FirstOrDefault(x => x.Key.SchemaName.Contains(crmFiled.Value.Entity));
                    if (!relEnt.Equals(new KeyValuePair<Relationship, EntityCollection>()) && relEnt.Value.Entities.Count > 0)
                    {
                        var crmFiledName = crmFiled.Value.AttrName.ToLower();

                        switch (mad.ESPWSVariableType)
                        {
                            case ESPWSVariableType.Double:
                                variableValueRequest.DoubleValue = GetDoubleValue(relEnt.Value.Entities[0], crmFiledName);
                                break;
                            case ESPWSVariableType.Text:
                                variableValueRequest.StringValue = GetStringValue(relEnt.Value.Entities[0], crmFiledName, mad);
                                break;
                            case ESPWSVariableType.DoubleList:
                                var optsetValue = relEnt.Value.Entities[0].GetAttributeValue<OptionSetValue>(crmFiledName);
                                if (optsetValue != null)
                                {
                                    variableValueRequest.DoubleValue = Double.Parse(GetOptionsSetTextForValue(relEnt.Value.Entities[0].LogicalName, crmFiledName, optsetValue.Value));
                                }
                                break;
                            case ESPWSVariableType.TextList:
                                var optsetValue1 = relEnt.Value.Entities[0].GetAttributeValue<OptionSetValue>(crmFiledName);
                                if (optsetValue1 != null)
                                {
                                    variableValueRequest.StringValue = GetOptionsSetTextForValue(relEnt.Value.Entities[0].LogicalName, crmFiledName, optsetValue1.Value);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }

                variableValueRequest.ESPWSVariableType = mad.ESPWSVariableType;
                variableValueRequest.ID = mad.ID;
                variableValueRequest.Name = mad.Name;
                variableValueRequest.QDDID = mad.QDDID;
            }
            catch (Exception ex)
            {

            }
            return variableValueRequest;
        }

        public ESPWSVariableValueRequest GenerateESPWSVariableValueRequestForManual(KeyValuePair<string, MappingItem> crmFiled, ESPWSVariableDefinitionResponse mad, KeyValuePair<string, string> field)
        {
            var variableValueRequest = new ESPWSVariableValueRequest
            {
                ESPWSVariableType = mad.ESPWSVariableType
            };
            try
            {
                //main entity
                if (!field.Equals(new KeyValuePair<string, MappingItem>()) && !crmFiled.Equals(new KeyValuePair<string, MappingItem>()))
                {
                    double doubleVal;
                    var crmFiledName = crmFiled.Value?.AttrName;

                    switch (mad.ESPWSVariableType)
                    {
                        case ESPWSVariableType.Double:
                            if (double.TryParse(field.Value, out doubleVal))
                                variableValueRequest.DoubleValue = doubleVal;
                            break;
                        case ESPWSVariableType.Text:
                            if (!string.IsNullOrEmpty(field.Value))
                                variableValueRequest.StringValue = field.Value;
                            break;
                        case ESPWSVariableType.DoubleList:
                            variableValueRequest.DoubleValue = Double.Parse(GetOptionsSetTextForValue(crmFiled.Value.Entity, crmFiledName.ToLower(), Int32.Parse(field.Value)));
                            break;
                        case ESPWSVariableType.TextList:
                            variableValueRequest.StringValue = GetOptionsSetTextForValue(crmFiled.Value.Entity, crmFiledName.ToLower(), Int32.Parse(field.Value));
                            break;
                        default:
                            break;
                    }
                }
                variableValueRequest.ESPWSVariableType = mad.ESPWSVariableType;
                variableValueRequest.ID = mad.ID;
                variableValueRequest.Name = mad.Name;
                variableValueRequest.QDDID = mad.QDDID;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return variableValueRequest;
        }

        /// <summary>
        /// Return Current mapping for MT, if link ddsm_mapping exist, this method will return MT mapping, else will be returned old mapping form Admin Data
        /// </summary>
        public ConcurrentDictionary<string, MappingItem> GetMadMapping(Entity targetEntity, bool groupRecalc = false)
        {
            ConcurrentDictionary<string, MappingItem> result = new ConcurrentDictionary<string, MappingItem>();
            var jsonField = groupRecalc ? "ddsm_mapping2.ddsm_jsondata" : "ddsm_mapping1.ddsm_jsondata";
            try
            {
                var json = targetEntity.GetAttributeValue<AliasedValue>(jsonField);
                if (json?.Value == null)
                {
                    throw new Exception("No MAD mapping data in Mesaure Template. Please select mapping and try again.");
                }
                var mappingJson = (string)json.Value;
                var mapping = JsonConvert.DeserializeObject<Dictionary<string, List<MappingItem2>>>(mappingJson);
                foreach (var map in mapping)
                {
                    foreach (var mad in map.Value)
                    {
                        if (!string.IsNullOrEmpty(mad.AttrLogicalName))
                            result[mad.FieldDisplayName] = new MappingItem { AttrName = mad.AttrLogicalName, Entity = map.Key, FieldType = mad.FieldType };
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        /// <summary>
        /// Return Current mapping for MT, if link ddsm_mapping exist, this method will return MT mapping, else will be returned old mapping form Admin Data
        /// </summary>
        public ConcurrentDictionary<string, MappingItem> GetQddMapping(Entity targetEntity, bool groupRecalc = false)
        {
            ConcurrentDictionary<string, MappingItem> result = new ConcurrentDictionary<string, MappingItem>();
            var jsonField = groupRecalc ? "ddsm_mapping3.ddsm_jsondata" : "ddsm_mapping2.ddsm_jsondata";
            try
            {
                var _value = targetEntity.GetAttributeValue<AliasedValue>(jsonField);
                if (_value?.Value == null)
                {
                    throw new Exception("No QDD mapping data in Mesaure Template. Please select mapping and try again.");
                }
                var mappingJson = (string)_value.Value;
                var mapping = JsonConvert.DeserializeObject<Dictionary<string, List<MappingItem2>>>(mappingJson);
                foreach (var map in mapping)
                {
                    foreach (var mad in map.Value)
                    {
                        result[mad.FieldDisplayName] = new MappingItem { AttrName = mad.AttrLogicalName, Entity = map.Key, FieldType = mad.FieldType };
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public ConcurrentDictionary<string, MSDField> GetMSDMapping(Entity measureTempl)
        {
            string msdValue = measureTempl.GetAttributeValue<string>("ddsm_msdfields");
            if (!string.IsNullOrEmpty(msdValue))
            {
                return JsonConvert.DeserializeObject<ConcurrentDictionary<string, MSDField>>(msdValue);
            }
            else
            {
                return null;
            }
        }
    }
}
