﻿using DDSM.CommonProvider;
using DDSM.SmartMeasureCalculationPlugin.Model;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using DDSM.SmartMeasureCalculationPlugin.Workflow;
using ESPWebServices.Models;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Activities;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Microsoft.Xrm.Sdk.Workflow;

/// <summary>
/// The Plugin provide opportunity to Create SmartMeasure from Grid in Project
/// </summary>
public class CalculateMeasure : BaseCodeActivity
{
    private Helper _helper;
    private Common ObjCommon;
    private CrmHelper CrmHelper;
    private IOrganizationService Service;
    private List<string> _processingLog;
    private ConcurrentDictionary<string, MappingItem> currentMadMapping;
    private ConcurrentDictionary<string, MSDField> currentMSDData;

    #region Internal fields
    private string _url = "";
    ESPConfig espConfig;
    private readonly string _mainEntity = "ddsm_measure";
    //  readonly List<String> _processingLog = new List<string>();


    [Output("Error Message")]
    public OutArgument<string> ErrorMsg { get; set; }

    internal override TaskQueue.StatusFileDataUploading CurrentOperationStatus
    {
        get { return TaskQueue.StatusFileDataUploading.ESPRecalculation; }
    }

    public enum CalculationRecordStatus
    {
        NotReady = 962080000,
        ReadyToCalculation = 962080001
    };

    enum CreatorRecordType
    {
        Front = 962080000,
        DataUploader = 962080001,
        Portal = 962080002
    };


    #endregion

    protected override void ExecuteActivity(CodeActivityContext context)
    {
        ObjCommon = new Common(context);
        Service = ObjCommon.GetOrgService();
        CrmHelper = new CrmHelper(Service);
        _helper = new Helper(ObjCommon.GetOrgService(systemCall: true));
        _processingLog = new List<string>();

        try
        {
            var userInput = GetUserObj(context);

            _helper.GetConfig();
            espConfig = new ESPConfig(_helper.Config);
            var measureTemplate = GetMeasureTemplateMAD(userInput.MeasureTplID, ObjCommon);
            var measureId = CreateMeasure(userInput, measureTemplate, ObjCommon);
            userInput.MeasureID = measureId;
            if (espConfig.EspRemoteCalculation)
            {
                RunRemoteEspCalc(espConfig, userInput, context);
                Complete.Set(context, true);
                Result.Set(context, JsonConvert.SerializeObject(new { MeasureName = "", MeasureID = measureId }));
                return;
            }

            RecalculateMeasure(userInput, measureTemplate, ObjCommon);
            Complete.Set(context, true);
            Result.Set(context, JsonConvert.SerializeObject(new { MeasureName = "", MeasureID = measureId }));

        }
        catch (Exception ex)
        {
            Complete.Set(context, false);
            ErrorMsg.Set(context, ex.Message);
            Result.Set(context, JsonConvert.SerializeObject(new { ErrorMsg = ex.Message }));
        }
        finally
        {
            _helper = null;
            ObjCommon = null;
            Service = null;
            CrmHelper = null;
            _processingLog = null;
            DataUploader = null;
            currentMSDData = null;
            currentMadMapping = null;
        }
    }
    #region Internal methods

    private void RecalculateMeasure(UserInputObj userInput, Entity measureTemplate, Common objCommon)
    {
        _helper = new Helper(objCommon.GetOrgService(systemCall: true));
        
        _helper.GetConfig();
        if (_helper.IsConfigValID())
        {
            #region Get currentAuthenticationResponse

            if (_helper.IsTokenNeedUpdate())
            {
                AuthenticationResponse currentAuthenticationResponse;
                _url = _helper.GetAuthUrl();
                using (var serviceRequest = new WebClient())
                {
                    var responseBytes = serviceRequest.DownloadString(_url);
                    currentAuthenticationResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(responseBytes);
                    if (currentAuthenticationResponse.ResponseHeader.StatusOk)
                    {
                        _helper.Token = currentAuthenticationResponse.ResponseHeader.AuthenticationTokenBytes;
                        _helper.LastRequestReceivedData = currentAuthenticationResponse.ResponseHeader.RequestReceivedDateTime;
                        _helper.UpdateConfig();
                    }
                    else
                    {
                        throw new AccessViolationException("Can't get authorization token using esp login details");
                    }
                }
            }
            else
            {
                _helper.LastRequestReceivedData = (DateTime)_helper.Config.Attributes["ddsm_requestreceiveddate"];
                _helper.Token = Convert.FromBase64String(_helper.Config.Attributes["ddsm_esplasttoken"].ToString());
            }
            #endregion

            //Calculation Request
            var aCalculationRequest = CreateCalculationRequest(measureTemplate, _helper.Token, userInput);

            #region Calculate Response

            CalculationResponse resultCalculationResponse = null;

            _url = _helper.GetCalculatioUrl();
            var request = JsonConvert.SerializeObject(aCalculationRequest);

            var client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } };
            var response = client.UploadString(_url, "POST", request);

            resultCalculationResponse = JsonConvert.DeserializeObject<CalculationResponse>(response);
            #endregion

            //update Measure using calculation result
            UpdateMeasure(resultCalculationResponse, userInput, measureTemplate, objCommon, _helper);

        }
    }

    private UserInputObj GetUserObj(CodeActivityContext context)
    {
        UserInputObj result;

        var userInputJsonB64 = UserInput.Get(context);

        if (!string.IsNullOrEmpty(userInputJsonB64))
        {
            var userInputJsonBytes = Convert.FromBase64String(userInputJsonB64);
            var userInputJson = Encoding.UTF8.GetString(userInputJsonBytes);
            result = JsonConvert.DeserializeObject<UserInputObj>(userInputJson);
        }
        else
        {
            throw new Exception("User Input is Empty. Nothing to processing.");
        }
        return result;
    }

    private void RunRemoteEspCalc(ESPConfig espConfig, UserInputObj userInput, CodeActivityContext context)
    {
        var espRemoteCalcURL = espConfig.EspRemoteCalculationApiUrl;
        var client = new HttpClient { BaseAddress = new Uri(espRemoteCalcURL) };
        var config = new
        {
            UserId = ObjCommon?.Context?.InitiatingUserId,
            MeasureId = userInput.MeasureID,
            UserInput = UserInput.Get(context)
        };

        var data = JsonConvert.SerializeObject(config);
        var content = new StringContent(data, Encoding.UTF8, "application/json");
        client.PostAsync("CalculateMeasure", content);
    }



    private Entity GetMeasureTemplateMAD(Guid id, Common _objCommon)
    {
        Entity result = new Entity();
        var query = new QueryExpression()
        {
            EntityName = "ddsm_measuretemplate",
            ColumnSet = new ColumnSet("ddsm_madfields", "ddsm_smartmeasureversionid", "ddsm_espsmartmeasureid", "ddsm_programoffering", "ddsm_msdfields", "ddsm_mappingid", "ddsm_qddmappingid", "ddsm_measurecalculationtype"),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions = {
                    new ConditionExpression("ddsm_measuretemplateid",ConditionOperator.Equal,id)
                }
            },
            LinkEntities = {
                new LinkEntity("ddsm_measuretemplate", "ddsm_mapping", "ddsm_mappingid", "ddsm_mappingid", JoinOperator.Inner) { Columns = new ColumnSet("ddsm_jsondata") },
                new LinkEntity("ddsm_measuretemplate", "ddsm_mapping", "ddsm_qddmappingid", "ddsm_mappingid", JoinOperator.Inner) { Columns = new ColumnSet("ddsm_jsondata") },
            }
        };
        var resultCollection = _objCommon.GetOrgService(systemCall: true).RetrieveMultiple(query);
        if (resultCollection.Entities.Count > 0)
        {
            result = resultCollection.Entities[0];
        }

        //var result = Service.Retrieve("ddsm_measuretemplate", id,
        //    new ColumnSet("ddsm_madfields", "ddsm_smartmeasureversionid", "ddsm_espsmartmeasureid", "ddsm_programoffering", "ddsm_msdfields")
        //    );

        return result;

    }

    /// <summary>
    /// Update Measure Without ESP Recalculation
    /// added aguk 04/04/2017
    /// </summary>
    /// <param name="userInput"></param>
    /// <param name="measureTempl"></param>
    /// <param name="_objCommon"></param>
    /// <param name="_helper"></param>
    private Guid CreateMeasure(UserInputObj userInput, Entity measureTempl, Common _objCommon)
    {
        //try
        //{
        currentMSDData = CrmHelper.GetMSDMapping(measureTempl);
        //var calcTypeId = CrmHelper.GetMeasureCalcType();
        var newMeasure = new Entity(_mainEntity)
        {
            Attributes =
                    {
                        ["ddsm_creatorrecordtype"] = new OptionSetValue((int) CreatorRecordType.Front),  //from CRM form
                        ["ddsm_calculationrecordstatus"] = new OptionSetValue((int) CalculationRecordStatus.ReadyToCalculation),       // ready for calculation of parent
                    }
        };
        FillDataFromRelates(userInput, newMeasure);

        currentMadMapping = CrmHelper.GetMadMapping(measureTempl);

        //process mad
        foreach (var mad in userInput.DataFields)
        {
            var crmName = currentMadMapping.FirstOrDefault(x => x.Key.Equals(mad.Key) && !string.IsNullOrEmpty(x.Value.AttrName)).Value;
            if (crmName != null)
            {
                if (crmName.Entity.Equals(_mainEntity))
                {
                    CrmHelper.SetMADAttributeValue(crmName, mad.Value, newMeasure);
                }
            }
        }

        if (currentMSDData != null)
        {
            //process msd
            foreach (var msd in userInput.DataFields)
            {
                var crmName = currentMSDData.FirstOrDefault(x => x.Key.Equals(msd.Key) && !string.IsNullOrEmpty(x.Value.AttrName)).Value;
                if (crmName != null)
                {
                    if (crmName.Entity.Equals(_mainEntity))
                    {
                        CrmHelper.SetMSDAttributeValue(crmName, msd.Value, newMeasure);
                    }
                }
            }
        }
        return _objCommon.GetOrgService(systemCall: true).Create(newMeasure);
        //}
        //catch (Exception ex)
        //{
        //   Result.Set(_objCommon.ExecutionContext, ex.Message);
        //   // throw new Exception("Error, Can not save the Measuse."+ ex.Message);
        //}
        //return Guid.Empty;
    }

    private void UpdateMeasure(CalculationResponse resultCalculationResponse, UserInputObj userInput, Entity measureTempl, Common _objCommon, Helper _helper)
    {
        //try
        //{
        if (currentMSDData == null)
        {
            currentMSDData = CrmHelper.GetMSDMapping(measureTempl);
        }


        var mappingQddData = CrmHelper.GetQddMapping(measureTempl);
        Entity newMeasure;

        //var calcTypeId = CrmHelper.GetMeasureCalcType();

        if (resultCalculationResponse.ResponseHeader.StatusOk)
        {
            newMeasure = new Entity(_mainEntity, userInput.MeasureID)
            {
                Attributes =
                    {
                        ["ddsm_smartmeasureid"] = userInput.EspSmartMeasureID,
                        ["ddsm_esptargetdate"] = resultCalculationResponse.ResponseHeader.ResponseDateTime,
                    }
            };

            //process qdd
            foreach (
                var qdd in
                resultCalculationResponse.ESPWSCalculatorCalculationList[0].ESPWSVariableCalculationList)
            {
                var crmName = mappingQddData.FirstOrDefault(x => x.Key.Equals(qdd.Name) && !string.IsNullOrEmpty(x.Value.AttrName)).Value;
                if (crmName != null)
                {
                    if (crmName.Entity.Equals(_mainEntity))
                    {
                        CrmHelper.SetAttributeValue(crmName, qdd, newMeasure);
                    }
                }
            }
            if (currentMSDData != null)
            {
                //process msd
                foreach (var msd in userInput.DataFields)
                {
                    var crmName = currentMSDData
                        .FirstOrDefault(x => x.Key.Equals(msd.Key) && !string.IsNullOrEmpty(x.Value.AttrName))
                        .Value;
                    if (crmName != null)
                    {
                        if (crmName.Entity.Equals(_mainEntity))
                        {
                            CrmHelper.SetMSDAttributeValue(crmName, msd.Value, newMeasure);
                        }
                    }
                }
            }
            _objCommon.GetOrgService(systemCall: true).Update(newMeasure);
        }
        else
        {
            throw new Exception(resultCalculationResponse.GetFullESPError());
        }
        //}
        //catch (Exception ex)
        //{
        //    throw;
        //}
    }

    private CalculationRequest CreateCalculationRequest(Entity measureTempl, byte[] token, UserInputObj userInput)
    {
        var aCalculationRequest = new CalculationRequest { RequestHeader = { AuthenticationTokenBytes = token } };
        if (currentMadMapping == null)
        {
            currentMadMapping = CrmHelper.GetMadMapping(measureTempl);
        }

        if (measureTempl != null)
        {
            if (Guid.Empty.Equals(measureTempl.Id))
            {
                throw new Exception("Can't get actual Measure Template. Please check MAD and QDD mapping and try again.");
            }

            if (!string.IsNullOrEmpty(measureTempl.Attributes["ddsm_madfields"].ToString()))
            {
                var calculatorCalculationRequest = new CalculatorCalculationRequest
                {
                    CalculatorID = new Guid(measureTempl.Attributes["ddsm_espsmartmeasureid"].ToString()),
                    TargetDateTime = DateTime.UtcNow
                };
                var curentMad = JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(measureTempl.Attributes["ddsm_madfields"].ToString());

                foreach (var field in userInput.DataFields)
                {
                    var mad = curentMad.FirstOrDefault(x => x.Name.Equals(field.Key));

                    var mappingInfo = currentMadMapping.FirstOrDefault(x => x.Key.Equals(field.Key));

                    if (mad != null)
                    {
                        var variableValueRequest = CrmHelper.GenerateESPWSVariableValueRequestForManual(mappingInfo, mad, field); //field, mad, mappingInfo?.Value

                        var sss = calculatorCalculationRequest.InputVariables.FirstOrDefault(x => x.Name.Equals(mad.Name));
                        if (sss == null)
                            calculatorCalculationRequest.InputVariables.Add(variableValueRequest);
                        else
                        {
                            calculatorCalculationRequest.InputVariables.Remove(sss);
                            calculatorCalculationRequest.InputVariables.Add(variableValueRequest);
                        }
                    }
                }
                if (curentMad.Count != calculatorCalculationRequest.InputVariables.Count)
                    throw new Exception("User input is damaged. The number of fields in the calculator request is not equal to the current measure template mad.\n Please contact to your administrator");

                aCalculationRequest.CalculatorCalculationRequestList.Add(calculatorCalculationRequest);
            }
        }
        else
        { throw new Exception("Can't get actual Measure Template MAD fields. Check mapping between ESP SmartMeasure and Measure Template."); }

        return aCalculationRequest;
    }

    private void FillDataFromRelates(UserInputObj userInput, Entity newEntity)
    {
        Entity siteAttrs = new Entity();
        Entity projAttr = new Entity();
        Entity rateclassAttr = new Entity();
        Entity accAttrs = new Entity();
        Entity mtAttrs = new Entity();

        //mt
        if (!Guid.Empty.Equals(userInput.MeasureTplID))
        {
            mtAttrs = CrmHelper.GetDataRelationMapping("ddsm_measure", "ddsm_measuretemplate", userInput.MeasureTplID);
        }

        //account
        if (!Guid.Empty.Equals(userInput.AccountID))
        {
            var accAttrsTmp = CrmHelper.GetDataRelationMapping("ddsm_measure", "account", userInput.AccountID);
            //try
            //{
            if (accAttrsTmp.Attributes.ContainsKey("ddsm_accountid"))
            {
                var parentAccount = accAttrsTmp.GetAttributeValue<EntityReference>("ddsm_accountid");

                foreach (KeyValuePair<String, Object> Attribute in accAttrsTmp.Attributes)
                {
                    if (!(parentAccount.GetType() == Attribute.Value.GetType() && parentAccount.Id == (Attribute.Value as EntityReference).Id))
                    {
                        accAttrs[Attribute.Key] = Attribute.Value;
                    }
                }
                accAttrs["ddsm_accountid"] = parentAccount;
            }
            //}
            //catch (Exception e)
            //{
            //    //Console.WriteLine(e);
            //    throw;
            //}
        }

        //site
        if (!Guid.Empty.Equals(userInput.ParentsiteID))
        {
            siteAttrs = CrmHelper.GetDataRelationMapping("ddsm_measure", "ddsm_site", userInput.ParentsiteID);
        }

        //project
        if (!Guid.Empty.Equals(userInput.ProjectID))
        {
            projAttr = CrmHelper.GetDataRelationMapping("ddsm_measure", "ddsm_project", userInput.ProjectID);
        }

        //rateclass
        if (siteAttrs.Attributes.ContainsKey("ddsm_ratecodenameid"))
        {
            rateclassAttr = CrmHelper.GetDataRelationMapping("ddsm_measure", "ddsm_rateclassreference", ((EntityReference)siteAttrs.Attributes["ddsm_ratecodenameid"]).Id);
        }
        addToEntity(newEntity, accAttrs);
        addToEntity(newEntity, siteAttrs);
        addToEntity(newEntity, projAttr);
        addToEntity(newEntity, rateclassAttr);
        addToEntity(newEntity, mtAttrs);
    }

    private void addToEntity(Entity destEntity, Entity sourceEntity)
    {
        foreach (var attr in sourceEntity.Attributes)
        {
            destEntity[attr.Key] = attr.Value;
        }
    }

    #endregion
}
