﻿//using System;
//using System.Activities;
//using DDSM.SmartMeasureCalculationPlugin.Utils;
//using Microsoft.Xrm.Sdk;

//using Microsoft.Xrm.Sdk.Workflow;
//using System.Net;
//using Newtonsoft.Json;
//using DDSM.SmartMeasureCalculationPlugin.Model;
//using Microsoft.Xrm.Sdk.Query;
//using System.Collections.Generic;

//public class CalculateMeasure : CodeActivity
//{
//    #region "Parameter Definition"

//    [Output("Complete")]
//    public OutArgument<bool> Complete { get; set; }


    

//    [Input("UserInput")]
//    public InArgument<string> UserInput { get; set; }


//    [Output("Result")]
//    public OutArgument<string> Result { get; set; }
//    #endregion

//    IOrganizationService _service;

//    Common _objCommon;

//    private Helper _helper;
//    private string url = "";
//    protected override void Execute(CodeActivityContext context)
//    {
//        try
//        {
           
//           #region "Load CRM Service from context"

//           _objCommon = new Common(context);
//           _service = _objCommon.Service;
//           _objCommon.TracingService.Trace("Load CRM Service from context --- OK");

//           //Entity measure = null;
//           #endregion
//         //  _helper = new Helper(_service);

//           //var measureTemplateId = MeasureTemplateId.Get(context);

//           var userInputJson = UserInput.Get(context);

//           var userInput = JsonConvert.DeserializeObject<UserInputObj>(userInputJson);

//           if (userInput == null)
//           {
//               throw new Exception("userInput Json is not valid");
//           }
//           var measureTemplate = GetMeasureTemplate(new Guid(userInput.measureTplId));
//           // var measure = GetMeasureTemplate(new Guid(userInput.measureId));



           

         

//           _helper.GetConfig();



//           if (_helper.IsConfigValid())
//           {
//               #region Get currentAuthenticationResponse
//               if (_helper.IsTokenNeedUpdate())
//               {
//                   AuthenticationResponse currentAuthenticationResponse;

//                   url = _helper.GetAuthUrl();

//                   using (var serviceRequest = new WebClient())
//                   {
//                       var responseBytes = serviceRequest.DownloadString(url);
//                       currentAuthenticationResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(responseBytes);
//                       if (currentAuthenticationResponse.ResponseHeader.StatusOk)
//                       {
//                           _helper.token = currentAuthenticationResponse.ResponseHeader.AuthenticationTokenBytes;
//                           _helper.LastRequestReceivedData = currentAuthenticationResponse.ResponseHeader.RequestReceivedDateTime;
//                           //   jsonResult = string.Format("{0};{1}", Convert.ToBase64String(token), LastRequestReceivedData.ToString());
//                           _helper.UpdateConfig();
//                       }
//                       else
//                       {
//                           throw new AccessViolationException("Can't get authorization token using esp login details");
//                       }
//                   }
//               }
//               else
//               {
//                   _helper.LastRequestReceivedData = (DateTime)_helper.Config.Attributes["ddsm_requestreceiveddate"];
//                   _helper.token = Convert.FromBase64String(_helper.Config.Attributes["ddsm_esplasttoken"].ToString());
//               }

//               #endregion

//               //Calculation Request
//               var aCalculationRequest = CreateCalculationRequest(measureTemplate, _helper.token, userInput);

//               #region Calculate Response

//               CalculationResponse resultCalculationResponse = null;

//               url = _helper.GetCalculatioUrl();
//               var request = JsonConvert.SerializeObject(aCalculationRequest);

//               var client = new WebClient();
//               client.Headers[HttpRequestHeader.ContentType] = "application/json";
//               var response = client.UploadString(url, "POST", request);

//               resultCalculationResponse = JsonConvert.DeserializeObject<CalculationResponse>(response);
//               #endregion

//               //update Measure using calculation result
//               var measureid = UpdateMeasure(resultCalculationResponse, userInput, measureTemplate);

//               var jsonResult = "dfdd";
//               Complete.Set(context, true);

//               // Result.Set(context, JsonConvert.SerializeObject(new { MeasureId = measureid, }));
//           }
         
//        }
//        catch (Exception ex)
//        {
//            //Entity errorEntity = new Entity("ddsm_admindata") { ["ddsm_esplog"] = ex.Message + ex.StackTrace + ex.Data };

//            //// Create an record.
//            //_service.Create(errorEntity);
//            //_objCommon?.TracingService.Trace(ex.Message);
//            Complete.Set(context, false);
//            Result.Set(context, ex.Message + ex.StackTrace + ex.Data);
//        }
//    }

//    private string UpdateMeasure(CalculationResponse resultCalculationResponse, UserInputObj userInput, Entity measureTempl)
//    {
//        var mappingData = JsonConvert.DeserializeObject<Dictionary<string, MappingItem>>(_helper.Config.Attributes["ddsm_espmapingdata"].ToString());
//        var curentMad = JsonConvert.DeserializeObject<List<EspwsVariableDefinitionResponse>>(measureTempl.Attributes["ddsm_madfields"].ToString());
//        var measureID = "";
//        Microsoft.Xrm.Sdk.Entity newMeasure;
//        if (resultCalculationResponse?.ResponseHeader?.StatusOk == true)
//        {
//            if (userInput.measureId.Equals("00000000-0000-0000-0000-000000000000"))
//            {
//                newMeasure = new Microsoft.Xrm.Sdk.Entity("ddsm_measure");
//                newMeasure.Attributes["ddsm_smartmeasureid"] = userInput.espSmartMeasureId;
//                newMeasure.Attributes["ddsm_esptargetdate"] = resultCalculationResponse.ResponseHeader.ResponseDateTime;
//                newMeasure.Attributes["ddsm_name"] = "-" +
//                      measureTempl.Attributes["ddsm_name"].ToString() + "_"
//                      //+ DateTime.Now.Year 
//                      //+ DateTime.Now.Month 
//                      + DateTime.UtcNow + "-" + DateTime.UtcNow.Hour + DateTime.UtcNow.Minute + DateTime.UtcNow.Second;

//                newMeasure.Attributes["ddsm_madfields"] = measureTempl.Attributes["ddsm_madfields"].ToString();
//                newMeasure.Attributes["ddsm_calculationtype"] = "ESP";
//                newMeasure.Attributes["ddsm_projecttomeasureid"] = new EntityReference("ddsm_project", new Guid(userInput.projectId));
//                newMeasure.Attributes["ddsm_initialphasedate"] = userInput.initialphasedate;
//                newMeasure.Attributes["ddsm_uptodatephase"] = userInput.phasenumber;
//                newMeasure.Attributes["ddsm_parentsite"] = new EntityReference("ddsm_site", new Guid(userInput.parentsiteid));
//                newMeasure.Attributes["ddsm_projectphasename"] = userInput.phase;
//                newMeasure.Attributes["ddsm_projectstatus"] = userInput.projectstatus;
//                newMeasure.Attributes["ddsm_accountid"] = new EntityReference("account", new Guid(userInput.accountid));


//                //process mad
//                foreach (var mad in resultCalculationResponse.EspwsCalculatorCalculationList[0].EspwsVariableCalculationList)
//                {
//                    var crmName = mappingData.FirstOrDefault(x => x.Key.Equals(mad.Name)).Value;
//                    if (crmName != null)
//                    {
//                        var name = crmName.AttrdName;
//                        newMeasure.Attributes[name.ToLower()] = mad.Value;
//                    }
//                }

//                measureID = _service.Create(newMeasure).ToString();
//            }
//            else
//            {
//                newMeasure = new Microsoft.Xrm.Sdk.Entity("ddsm_measure", new Guid(userInput.measureId));

//                newMeasure.Attributes["ddsm_smartmeasureid"] = userInput.espSmartMeasureId;
//                newMeasure.Attributes["ddsm_esptargetdate"] = resultCalculationResponse.ResponseHeader.ResponseDateTime;
//                newMeasure.Attributes["ddsm_madfields"] = measureTempl.Attributes["ddsm_madfields"].ToString();

//                newMeasure.Attributes["ddsm_madfields"] = measureTempl.Attributes["ddsm_madfields"].ToString();
//                newMeasure.Attributes["ddsm_calculationtype"] = "ESP";
//                //   newMeasure.Attributes["ddsm_projecttomeasureid"] = new EntityReference("ddsm_project", new Guid(userInput.projectId));
//                newMeasure.Attributes["ddsm_initialphasedate"] = userInput.initialphasedate;
//                newMeasure.Attributes["ddsm_uptodatephase"] = userInput.phasenumber;
//                newMeasure.Attributes["ddsm_parentsite"] = new EntityReference("ddsm_site", new Guid(userInput.parentsiteid));
//                newMeasure.Attributes["ddsm_projectphasename"] = userInput.phase;
//                newMeasure.Attributes["ddsm_projectstatus"] = userInput.projectstatus;
//                // newMeasure.Attributes["ddsm_accountid"] = new EntityReference("account", new Guid(userInput.accountid));

//                //process mad
//                foreach (var mad in resultCalculationResponse.EspwsCalculatorCalculationList[0].EspwsVariableCalculationList)
//                {
//                    var crmName = mappingData.FirstOrDefault(x => x.Key.Equals(mad.Name)).Value;
//                    if (crmName != null)
//                    {
//                        var name = crmName.AttrdName;
//                        newMeasure.Attributes[name.ToLower()] = mad.Value;
//                    }
//                }
//                _service.Update(newMeasure);

//                measureID = userInput.measureId;
//            }

//        }
//        return measureID;
//    }

//    private CalculationRequest CreateCalculationRequest(Entity measureTempl, byte[] token, UserInputObj userInput)
//    {
//        var aCalculationRequest = new CalculationRequest { RequestHeader = { AuthenticationTokenBytes = token } };
//        if (measureTempl != null)
//        {
//            if (!string.IsNullOrEmpty(measureTempl.Attributes["ddsm_madfields"].ToString()))
//            {
//                var calculatorCalculationRequest = new CalculatorCalculationRequest
//                {
//                    CalculatorId = new Guid(measureTempl.Attributes["ddsm_espsmartmeasureid"].ToString()),
//                    TargetDateTime = DateTime.UtcNow
//                };
//                //mapping data as dict <key,value> where key = espname , value = crm field schema name
//                //   var mappingData = JsonConvert.DeserializeObject<Dictionary<string, MappingItem>>(_helper.Config.Attributes["ddsm_espmapingdata"].ToString());
//                var curentMad = JsonConvert.DeserializeObject<List<EspwsVariableDefinitionResponse>>(measureTempl.Attributes["ddsm_madfields"].ToString());

//                //var userInput = JsonConvert.DeserializeObject<List<>>(userInputJson);               

//                foreach (var field in userInput.dataFields)
//                {

//                    var mad = curentMad.FirstOrDefault(x => x.Name.Equals(field.Key));
//                    if (mad != null)
//                    {
//                        EspwsVariableValueRequest variableValueRequest = new EspwsVariableValueRequest();
//                        variableValueRequest.DoubleValue = mad.DoubleValue;
//                        variableValueRequest.StringValue = mad.StringValue;
//                        variableValueRequest.EspwsVariableType = mad.EspwsVariableType;
//                        variableValueRequest.Id = mad.Id;
//                        variableValueRequest.Name = mad.Name;
//                        variableValueRequest.Qddid = mad.Qddid;

//                        calculatorCalculationRequest.InputVariables.Add(variableValueRequest);
//                    }


//                }
//                aCalculationRequest.CalculatorCalculationRequestList.Add(calculatorCalculationRequest);
//            }
//        }
//        return aCalculationRequest;
//    }
   
//    private Entity GetMeasure(Guid id)
//    {
//        var expr = new QueryExpression
//        {
//            EntityName = "ddsm_measure",
//            ColumnSet = new ColumnSet(allColumns: true),
//            Criteria = new FilterExpression
//            {
//                FilterOperator = LogicalOperator.And,
//                Conditions = { new ConditionExpression("ddsm_measureid", ConditionOperator.Equal, id.ToString()) }
//            }
//        };

//        // var query = new FetchExpression(expr);
//        var measure = new Entity("ddsm_measure");
//        var measureList = _service.RetrieveMultiple(expr);
//        if (measureList != null && measureList.Entities.Count > 0)
//        {
//            measure = measureList.Entities[0];
//        }


//        return measure;
//    }
   
//    private Entity GetMeasureTemplate(Guid id)
//    {
//        return _service.Retrieve("ddsm_measuretemplate", id, new ColumnSet("ddsm_madfields", "ddsm_espsmartmeasureid"));
//    }
 
//}
