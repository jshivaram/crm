﻿using DDSM.SmartMeasureCalculationPlugin.Model;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDSM.SmartMeasureCalculationPlugin.Workflow
{
    //public class GenerateSMForm : CodeActivity
    //{
    //    [Output("Form JSON")]
    //    public OutArgument<string> FormJson { get; set; }

    //    #region Internal fields
    //    IOrganizationService _service;
    //    private Common _objCommon;
    //    private Helper _helper;
    //    private string _url = "";
    //    private readonly string _mainEntity = "ddsm_measure";
    //    readonly List<String> _processingLog = new List<string>();
    //    #endregion

    //    protected override void Execute(CodeActivityContext context)
    //    {
    //        #region "Load CRM Service from context"

    //        _objCommon = new Common(context);
    //        _service = _objCommon.Service;
    //        _objCommon.TracingService.Trace("Load CRM Service from context --- OK");


    //        #endregion

    //        var result = "";/// form JSON

    //        var espData = GetMeasureTemplateESPData(Guid.Empty);//TODO: set guid from Target or Parameters

    //        if (espData != null)
    //        {
    //            var formObj = GenerateFormObj(espData);

    //            if (formObj != null)
    //                result = JsonConvert.SerializeObject(formObj);

    //        }

    //        FormJson.Set(context, result);
    //    }

    //    private ExtJSFormObj GenerateFormObj(Entity espData)
    //    {
    //        var result = new ExtJSFormObj();
    //        var madJson = (espData.Attributes["ddsm_measuretemplate1.ddsm_madfields"] as AliasedValue).Value.ToString();
    //        var msdJson = (espData.Attributes["ddsm_measuretemplate1.ddsm_msdfields"] as AliasedValue).Value.ToString();

    //        var mad = JsonConvert.DeserializeObject<List<MappingItem>>(madJson);
    //        var msd = JsonConvert.DeserializeObject<List<MSDField>>(msdJson);


    //        return result;
    //    }

    //    private Entity GetMeasureTemplateESPData(Guid mtId)
    //    {
    //        //var expr = new QueryExpression
    //        //{
    //        //    EntityName = "ddsm_measuretemplate",
    //        //    ColumnSet = new ColumnSet("ddsm_msdfields", "ddsm_madfields"),
    //        //    Criteria = new FilterExpression
    //        //    {
    //        //        FilterOperator = LogicalOperator.And,
    //        //        Conditions = { new ConditionExpression("ddsm_measuretemplateid", ConditionOperator.Equal, mtId) }
    //        //    },
    //        //  //  LinkEntities = { new LinkEntity(_mainEntity, "ddsm_measuretemplate", "ddsm_measureselector", "ddsm_measuretemplateid", JoinOperator.Inner) { Columns = new ColumnSet("ddsm_msdfields", "ddsm_madfields") } }

    //        //};

    //        //var mtData = _service.RetrieveMultiple(expr);
    //        var mtData = _service.Retrieve("ddsm_measuretemplate", mtId, new ColumnSet("ddsm_msdfields", "ddsm_madfields"));

    //        return mtData;
    //        //if (mtData.Entities.Count > 0)
    //        //    return mtData[0];
    //        //else
    //        //{
    //        //    return null;
    //        //}


    //    }
    //}
}
