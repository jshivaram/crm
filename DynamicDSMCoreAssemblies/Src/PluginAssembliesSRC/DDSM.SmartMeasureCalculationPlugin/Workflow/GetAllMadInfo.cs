﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using DDSM.CommonProvider;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using DDSM.SmartMeasureCalculationPlugin.Workflow;
using ESPWebServices.Models;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;


/// <summary>
/// Provide an opportunity to Load all Mrtadata From ESP (SM Library,SM,MAD,QDD) 
/// </summary>
public class GetAllMadInfo : BaseCodeActivity
{
    private Helper _helper;
    private Common ObjCommon;
    private CrmHelper CrmHelper;
    private IOrganizationService Service;
    private List<string> _processingLog;

    #region "Parameter Definition"

    [Input("ForceUpdate")]
    public InArgument<bool> ForceUpdate { get; set; }

    internal override TaskQueue.StatusFileDataUploading CurrentOperationStatus
    {
        get
        {
            return TaskQueue.StatusFileDataUploading.ESPRecalculation;
        }
    }

    #endregion

    #region internal fields

    private string url = "";
    private List<ESPWSVariableDefinitionResponse> UniqueMads = new List<ESPWSVariableDefinitionResponse>();
    private List<ESPWSVariableDefinitionResponse> UniqueQdds = new List<ESPWSVariableDefinitionResponse>();

    List<ESPWSVariableDefinitionResponse> newMad = new List<ESPWSVariableDefinitionResponse>();
    List<ESPWSVariableDefinitionResponse> newQdd = new List<ESPWSVariableDefinitionResponse>();
    // private readonly List<String> _processingLog = new List<string>();
    private int updated = 0;
    private int added = 0;
    #endregion


    protected override void ExecuteActivity(CodeActivityContext context)
    {
        ObjCommon = new Common(context);
        Service = ObjCommon.GetOrgService();
        CrmHelper = new CrmHelper(Service);
        _helper = new Helper(ObjCommon.GetOrgService(systemCall: true));
        _processingLog = new List<string>();

        try
        {
            _helper.GetConfig();
            if (_helper.IsConfigValID())
            {

                #region Get currentAuthenticationResponse

                if (_helper.IsTokenNeedUpdate() || ForceUpdate.Get(context))
                {
                    url = _helper.GetAuthUrl();

                    using (var serviceRequest = new WebClient())
                    {
                        var responseBytes = serviceRequest.DownloadString(url);
                        var currentAuthenticationResponse =
                            JsonConvert.DeserializeObject<AuthenticationResponse>(responseBytes);
                        if (currentAuthenticationResponse.ResponseHeader.StatusOk)
                        {
                            _helper.Token = currentAuthenticationResponse.ResponseHeader.AuthenticationTokenBytes;
                            _helper.LastRequestReceivedData = currentAuthenticationResponse.ResponseHeader
                                .RequestReceivedDateTime;
                            _helper.UpdateConfig();
                        }
                        else
                        {
                            throw new AccessViolationException(
                                "Can't get authorization token using esp login details. Please contact to your administrator." +
                                Environment.NewLine + "ESP Error: " +
                                currentAuthenticationResponse.ResponseHeader.ErrorMessage);
                        }
                    }
                }
                else
                {
                    _helper.LastRequestReceivedData = (DateTime)_helper.Config.Attributes["ddsm_requestreceiveddate"];
                    _helper.Token = Convert.FromBase64String(_helper.Config.Attributes["ddsm_esplasttoken"].ToString());
                }

                #endregion


                #region get available calculators

                var aAvailableCalculatorsRequest = new AvailableCalculatorsRequest
                {
                    RequestHeader = { AuthenticationTokenBytes = _helper.Token },
                    TargetDateTime = _helper.LastRequestReceivedData
                };
                AvailableCalculatorsResponse measureLibrarysList = null;

                url = _helper.GetAvailableCalculatorsUrl();

                var requestCalculators = JsonConvert.SerializeObject(aAvailableCalculatorsRequest);

                #endregion


                #region get all mad defs by measure

                var client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } };
                var response = client.UploadString(url, "POST", requestCalculators);

                measureLibrarysList = JsonConvert.DeserializeObject<AvailableCalculatorsResponse>(response);

                var processedESPMEasureLibData = CreateEspMeasureLibrarys(measureLibrarysList);

                var dataDefsRequest = GetDataDefsRequest(measureLibrarysList, _helper.Token);

                //call calculator datadefinitions
                CalculatorDataDefinitionsResponse result = null;

                url = _helper.GetCalculatiorDataDefinitionsUrl();
                var request = JsonConvert.SerializeObject(dataDefsRequest);

                client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } };
                response = client.UploadString(url, "POST", request);

                result = JsonConvert.DeserializeObject<CalculatorDataDefinitionsResponse>(response);

                #endregion

                ProcessSmartMeasureData(result, processedESPMEasureLibData);

                //Generate unique MAD and QDD
                UpdateUniqueDatas();

                Complete.Set(context, true);

                var msg = $"Updated: {updated}, Created: {added}. ";
                msg = msg + getAddedFieldMsg();
                msg = msg + getAddedFieldMsg(false);

                Result.Set(context,
                    JsonConvert.SerializeObject(new
                    {
                        MeasureName = "",
                        MeasureID = "",
                        ErrorMsg = msg
                    }));
            }
            else
            {
                throw new Exception("ESP connection details is not configured. Please fill it before continue.");
            }
        }
        catch (Exception ex)
        {
            SetErrorStatusForDA();
            Complete.Set(context, false);
            Result.Set(context,
                JsonConvert.SerializeObject(new
                {
                    MeasureName = "",
                    MeasureID = "",
                    ErrorMsg = ex.Message
                }));
        }
        finally
        {
            _helper = null;
            ObjCommon = null;
            Service = null;
            CrmHelper = null;
            _processingLog = null;
            DataUploader = null;
        }
    }

    private void GetUpdatedMAD()
    {
        var oldMadJson = _helper.Config.GetAttributeValue<string>("ddsm_espuniquemad");
        var oldQddJson = _helper.Config.GetAttributeValue<string>("ddsm_espuniqueqdd");

        var oldMad = !string.IsNullOrEmpty(oldMadJson)
            ? JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(oldMadJson)
            : null;
        var oldQdd = !string.IsNullOrEmpty(oldMadJson)
            ? JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(oldQddJson)
            : null;
        if (oldMad != null)
        {
            newMad = UniqueMads.Where(i => !oldMad.Any(x => x.Name == i.Name && x.DisplayFormat == i.DisplayFormat)).ToList();
        }
        if (oldQdd != null)
        {
            newQdd = UniqueQdds.Where(i => !oldQdd.Any(x => x.Name == i.Name && x.DisplayFormat == i.DisplayFormat)).ToList();
        }
    }

    #region Internal methods

    private void UpdateUniqueDatas()
    {
        UniqueMads = UniqueMads.GroupBy(p => p.Name.ToUpper()).Select(g => g.First()).OrderBy(x => x.Name).ToList();
        UniqueQdds = UniqueQdds.GroupBy(p => p.Name.ToUpper()).Select(g => g.First()).OrderBy(x => x.Name).ToList();
        UniqueQdds = UniqueQdds.Except(UniqueMads, new QDDItemNameComparer()).ToList();

        GetUpdatedMAD();

        if (!string.IsNullOrEmpty(_helper.adminDataID))
        {
            var newEntity = new Entity("ddsm_admindata", new Guid(_helper.adminDataID))
            {
                Attributes =
                {
                    ["ddsm_espuniquemad"] = JsonConvert.SerializeObject(UniqueMads),
                    ["ddsm_espuniqueqdd"] = JsonConvert.SerializeObject(UniqueQdds)
                }
            };
            Service.Update(newEntity);
        }
    }

    string getAddedFieldMsg(bool isMad = true)
    {
        List<ESPWSVariableDefinitionResponse> data = null;
        List<string> result = new List<string>();

        var fieldType = "";
        if (isMad)
        {
            fieldType = "MAD";
            data = newMad;
        }
        else
        {
            fieldType = "QDD";
            data = newQdd;
        }

        if (data?.Count > 0)
        {
            //result.Add();
            foreach (var mad in data)
            {
                result.Add(mad.Name + " - Type: " + mad.DisplayFormat);
            }
            return $" Added new {fieldType} fields: " + string.Join(",", result);
        }
        else
        {
            return "";
        }

    }

    private Dictionary<Guid, Guid> CreateEspMeasureLibrarys(AvailableCalculatorsResponse measureLibrarysList)
    {
        var crmMeasureLibID = new Guid();

        var result = new Dictionary<Guid, Guid>();
        if (measureLibrarysList != null && measureLibrarysList.CalculatorLibraryResponseList.Count > 0)
        {
            foreach (var lib in measureLibrarysList.CalculatorLibraryResponseList)
            {
                var expr = new QueryExpression
                {
                    EntityName = "ddsm_espmeasurelibrary",
                    ColumnSet = new ColumnSet("ddsm_espmeasurelibraryid"),
                    Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_espmeasurelibid", ConditionOperator.Equal, lib.ID.ToString()) }
                    }
                };

                // var query = new FetchExpression(expr);
                var measureLib = new Entity("ddsm_espmeasurelibrary");
                var crmMt = Service.RetrieveMultiple(expr);

                //update if found
                if (crmMt != null && crmMt.Entities.Count > 0)
                {
                    crmMeasureLibID = crmMt.Entities[0].Id;
                    measureLib = new Entity("ddsm_espmeasurelibrary", crmMeasureLibID)
                    {
                        ["ddsm_espmeasurelibid"] = lib.ID.ToString(),
                        ["ddsm_name"] = lib.Name,
                        ["ddsm_organizationname"] = lib.OrganizationName
                    };
                    Service.Update(measureLib);
                }
                else
                {
                    measureLib = new Entity("ddsm_espmeasurelibrary")
                    {
                        ["ddsm_espmeasurelibid"] = lib.ID.ToString(),
                        ["ddsm_name"] = lib.Name,
                        ["ddsm_organizationname"] = lib.OrganizationName
                    };

                    crmMeasureLibID = Service.Create(measureLib);
                }
                foreach (var measure in lib.CalculatorList)
                {
                    result.Add(measure.ID, crmMeasureLibID);
                }
            }
        }
        else
        {
            throw new Exception("Can't create measure librarys. Receiver Measure Librarys list is empty.");
        }
        return result;
    }

    /// <summary>
    /// Return JSON Dictionary, when key = Measure Name, Value = list of MAD fields
    /// </summary>
    /// <returns></returns>
    private CalculatorDataDefinitionsRequest GetDataDefsRequest(AvailableCalculatorsResponse response, byte[] token)
    {
        var measureLibrarysList = response.CalculatorLibraryResponseList; //measureLibrarys
        var request = new CalculatorDataDefinitionsRequest
        {
            RequestHeader = { AuthenticationTokenBytes = token }
        };

        foreach (var measureLib in measureLibrarysList)
        {
            foreach (var measure in measureLib.CalculatorList)
            {
                request.CalculatorDefinitionRequestList.Add(new CalculatorDefinitionRequest { CalculatorID = measure.ID, TargetDateTime = _helper.LastRequestReceivedData });
            }
        }
        return request;
    }

    private List<ESPWSCalculatorDefinitionResponse> GetDataMad(CalculatorDataDefinitionsResponse madDefinitionResponse)
    {
        var result = new List<ESPWSCalculatorDefinitionResponse>();

        if (madDefinitionResponse != null && madDefinitionResponse.ResponseHeader.StatusOk)
        {
            foreach (var measureMad in madDefinitionResponse.ESPWSCalculatorDataDefinitionResponseList)
            {
                result.Add(measureMad);
            }
        }
        return result;
    }

    private void ProcessSmartMeasureData(CalculatorDataDefinitionsResponse smartMeasureData, Dictionary<Guid, Guid> libData)
    {
        if (smartMeasureData == null)
        {
            throw new Exception("Nothing to do! in ProcessSmartMeasureData list is null((");
        }
        var datepattern = "yyyy.MM.dd HH:mm:ss";
        try
        {
            foreach (var measure in smartMeasureData.ESPWSCalculatorDataDefinitionResponseList)
            {
                var versions = GetSmartMeasureVersions(measure);
                Entity smRecord = null;
                DataCollection<Entity> reletedMTs = null;
                //no SM versions found, create it 
                if (versions?.Count == 0)
                {
                    smRecord = FillSMVersion(measure, libData);
                    added++;
                }
                else
                {
                    var lastVer = versions.FirstOrDefault(x => x.GetAttributeValue<DateTime>("ddsm_startdatetime").ToString(datepattern).Equals(measure.StartDateTime.ToString(datepattern)));

                    // current version does not exist in CRM
                    if (lastVer == null)
                    {
                        smRecord = FillSMVersion(measure, libData);

                        if (versions?.Count > 0)
                        {
                            reletedMTs = GetRelatedMT(versions[0]);
                        }
                        // DeactivateRecords(versions); //Deactivate old SM versions
                        updated++;
                    }
                    // clone MT from old version and assign the new copyes to new SM version

                }

                if (smRecord != null)
                {
                    var newVerionId = Service.Create(smRecord);
                    //if (reletedMTs != null)
                    //{
                    //    ReAssignMT(reletedMTs, newVerionId);
                    //}
                }
                UniqueMads.AddRange(measure.ESPVariableDefinitionList);
                UniqueQdds.AddRange(measure.ESPQddDefinitionList);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    private void ReAssignMT(DataCollection<Entity> reletedMTs, Guid newSMVerionId)
    {
        List<String> Log = new List<string>();

        var emRequest = new ExecuteMultipleRequest()
        {
            Settings = new ExecuteMultipleSettings()
            {
                ContinueOnError = false,
                ReturnResponses = true
            },
            Requests = new OrganizationRequestCollection()
        };

        foreach (var mt in reletedMTs)
        {

            var updResuqet = new UpdateRequest
            {
                Target = new Entity(mt.LogicalName, mt.Id)
                {
                    Attributes = { new KeyValuePair<string, object>("ddsm_smartmeasureversionid", new EntityReference("ddsm_smartmeasureversion", newSMVerionId)) }
                }
            };
            emRequest.Requests.Add(updResuqet);
        }

        if (emRequest.Requests.Count > 0)
        {
            var response = (ExecuteMultipleResponse)Service.Execute(emRequest);

            if (response.IsFaulted)
            {
                foreach (var resp in response.Responses)
                {
                    if (resp.Fault != null)
                    {
                        Log.Add("in ExecuteMultipleResponse: " + resp.Fault.Message);
                    }
                }
                throw new Exception(string.Join(Environment.NewLine, Log));
            }
        }

    }

    private Entity FillSMVersion(ESPWSCalculatorDefinitionResponse measure, Dictionary<Guid, Guid> libData)
    {
        return new Entity("ddsm_smartmeasureversion")
        {
            Attributes =
            {
                ["ddsm_name"] = measure.Name,
                ["ddsm_madfields"] =JsonConvert.SerializeObject(measure.ESPVariableDefinitionList.OrderBy(v => v.DisplayOrderRank)),
                ["ddsm_calculationid"] = measure.ID.ToString(),
                ["ddsm_startdatetime"] = measure.StartDateTime,
                ["ddsm_espsmartmeasurelibraryid"] =
                new EntityReference("ddsm_espmeasurelibrary", libData[measure.ID])
            }
        };
    }

    private void DeactivateRecords(DataCollection<Entity> versions)
    {
        if (versions?.Count == 0) return;

        try
        {
            foreach (var vers in versions)
            {

                SetStateRequest setStateRequest = new SetStateRequest()
                {
                    EntityMoniker = new EntityReference
                    {
                        Id = vers.Id,
                        LogicalName = vers.LogicalName,
                    },
                    State = new OptionSetValue(1),
                    Status = new OptionSetValue(2)
                };
                Service.Execute(setStateRequest);


            }


        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }





    }

    //Entity GetMTCopy(Entity mt)
    //{
    //    if (mt == null)
    //    {
    //        return null;
    //    }

    //    Entity result = new Entity(mt.LogicalName);

    //    foreach (var attr in mt.Attributes)
    //    {
    //        if (attr.Key == "ddsm_measuretemplateid") continue;
    //        result.Attributes.Add(attr);
    //    }
    //    return result;
    //}

    DataCollection<Entity> GetRelatedMT(Entity smVersion)
    {
        if (smVersion == null)
        {
            return null;
        }
        DataCollection<Entity> result = null;

        var query = new QueryExpression
        {
            EntityName = "ddsm_measuretemplate",
            ColumnSet = new ColumnSet(true),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions =
                {
                    new ConditionExpression("ddsm_smartmeasureversionid", ConditionOperator.Equal, smVersion.Id),
                    new ConditionExpression("statecode", ConditionOperator.Equal, 0)
                }
            }
        };
        result = Service.RetrieveMultiple(query).Entities;
        return result;
    }

    DataCollection<Entity> GetSmartMeasureVersions(ESPWSCalculatorDefinitionResponse measure)
    {
        var expr = new QueryExpression
        {
            EntityName = "ddsm_smartmeasureversion",
            ColumnSet = new ColumnSet("ddsm_startdatetime"),
            Criteria = new FilterExpression
            {
                FilterOperator = LogicalOperator.And,
                Conditions = { new ConditionExpression("ddsm_calculationid", ConditionOperator.Equal, measure.ID.ToString()) }
            }
        };
        expr.AddOrder("ddsm_startdatetime", OrderType.Descending);

        var crmMt = Service.RetrieveMultiple(expr);
        return crmMt.Entities;
    }

    #endregion
}
