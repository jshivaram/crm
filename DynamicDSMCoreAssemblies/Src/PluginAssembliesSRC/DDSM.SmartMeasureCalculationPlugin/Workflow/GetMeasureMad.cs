﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Net;
using DDSM.SmartMeasureCalculationPlugin.Model;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using System.Linq;


public class GetMeasureMad : CodeActivity
{
    #region "Parameter Definition"

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }
  

    [RequiredArgument]
    [Input("SmartMesure ID")]
    public InArgument<string> SmartMeasureId { get; set; }


    //[Output("Error msg")]
    //public OutArgument<string> ErrorMsg { get; set; }

    //[RequiredArgument]
    //[Input("Program")]
    //[ArgumentEntity("ddsm_admindata")]
    //[ReferenceTarget("ddsm_admindata")]
    //public InArgument<EntityReference> Admindata { get; set; }

    [Output("Result")]
    public OutArgument<string> Result { get; set; }
    #endregion

    IOrganizationService _service;
    private Common _objCommon;
    private Dictionary<string, object> _credential;//= new Dictionary<string, object>
    protected override void Execute(CodeActivityContext context)
    {

        var smartMeasureId = SmartMeasureId.Get(context);

        #region "Load CRM Service from context"

        _objCommon = new Common(context);
        _service = _objCommon.Service;
        _objCommon.TracingService.Trace("Load CRM Service from context --- OK");


        #endregion

        try
        {
            var config = GetConfig();

            if (config.Attributes.ContainsKey("ddsm_espurl") &&
                config.Attributes.ContainsKey("ddsm_esplogin") &&
                config.Attributes.ContainsKey("ddsm_esppassword")
                )
            {

                _credential = new Dictionary<string, object>
                        {
                            {"URI", config["ddsm_espurl"].ToString()},
                            {"UserName", config["ddsm_esplogin"].ToString()},
                            {"Password", config["ddsm_esppassword"].ToString()},
                            {"Portion", config["ddsm_espdataportion"]}

                        };

                #region Get currentAuthenticationResponse


                AuthenticationResponse currentAuthenticationResponse;

                var url = string.Format(_credential["URI"].ToString()
                                        + "/API/Authentication"
                                        + "/" + _credential["UserName"].ToString()
                                        + "/" + _credential["Password"].ToString());


                using (var serviceRequest = new WebClient())
                {
                    var responseBytes = serviceRequest.DownloadString(url);
                    currentAuthenticationResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(responseBytes);
                }

                #endregion



                #region get mad defs by measure

                var dataDefsRequest = GetDataDefsRequest(smartMeasureId, currentAuthenticationResponse.ResponseHeader.AuthenticationTokenBytes);

                ///call calculator datadefinitions
                CalculatorDataDefinitionsResponse calcRespons = null;

                url = string.Format(_credential["URI"] + "/API/CalculatorDataDefinitions/");
                var request = JsonConvert.SerializeObject(dataDefsRequest);

                var client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } };
                var response = client.UploadString(url, "POST", request);

                calcRespons = JsonConvert.DeserializeObject<CalculatorDataDefinitionsResponse>(response);

                #endregion

                Complete.Set(context, true);
                Result.Set(context, JsonConvert.SerializeObject(calcRespons.EspwsCalculatorDataDefinitionResponseList));
            }
        }
        catch (Exception ex)
        {
            Entity account = new Entity("ddsm_admindata") { ["ddsm_esplog"] = ex.Message + ex.Source + ex.StackTrace };

            // Create an account record named Fourth Coffee.
            _service.Create(account);

            _objCommon?.TracingService.Trace(ex.Message);
            Complete.Set(context, false);
            Result.Set(context, ex.Message);
        }
    }

    Entity GetConfig()
    {
        var adminDataId = "F56C3086-9AF2-E411-80EC-FC15B4284D68"; //main admin data record with current config

        return _service.Retrieve("ddsm_admindata", Guid.Parse(adminDataId),
                new ColumnSet("ddsm_espurl", "ddsm_esplogin", "ddsm_esppassword", "ddsm_espdataportion"));
    }

    /// <summary>
    /// Return JSON Dictionary, when key = Measure Name, Value = list of MAD fields
    /// </summary>
    /// <returns></returns>
    CalculatorDataDefinitionsRequest GetDataDefsRequest(string smartMeasureId, byte[] token)
    {

        var request = new CalculatorDataDefinitionsRequest
        {
            RequestHeader = { AuthenticationTokenBytes = token }
        };
        request.CalculatorDefinitionRequestList.Add(new CalculatorDefinitionRequest { CalculatorId = Guid.Parse(smartMeasureId), TargetDateTime = DateTime.UtcNow });

        return request;
    }

    string GetUniqueDataMadJson(CalculatorDataDefinitionsResponse madDefinitionResponse)
    {
        var result = new List<EspwsVariableDefinitionResponse>();

        if (madDefinitionResponse != null && madDefinitionResponse.ResponseHeader.StatusOk)
        {
            foreach (var measureMad in madDefinitionResponse.EspwsCalculatorDataDefinitionResponseList)
            {

                if (result.Count > 0)
                {
                    // result.Distinct((IEqualityComparer<DDSM.ESPSmartMeasurePlugin.Model.ESPWSVariableDefinitionResponse>)measureMad.ESPVariableDefinitionList);
                    result = result.Except(measureMad.EspVariableDefinitionList, new Comparer()).ToList();
                    //  result.Union(measureMad.ESPVariableDefinitionList);
                }
                else
                {
                    result.AddRange(measureMad.EspVariableDefinitionList);
                }
                //  result.Add(measureMad.Name, measureMad.ESPVariableDefinitionList);
            }
        }

        return JsonConvert.SerializeObject(result);
    }

}
