﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using ESPWebServices.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;

public class GetUniqueMad : CodeActivity
{
    #region "Parameter Definition"

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }

    //[Output("Error msg")]
    //public OutArgument<string> ErrorMsg { get; set; }

    //[RequiredArgument]
    //[Input("Program")]
    //[ArgumentEntity("ddsm_admindata")]
    //[ReferenceTarget("ddsm_admindata")]
    //public InArgument<EntityReference> Admindata { get; set; }

    [Output("Result")]
    public OutArgument<string> Result { get; set; }
    #endregion

    IOrganizationService _service;
    private Common _objCommon;
    private Helper _helper;
    private string url = "";
    protected override void Execute(CodeActivityContext context)
    {
        #region "Load CRM Service from context"

        _objCommon = new Common(context);
        _service = _objCommon.Service;
        _objCommon.TracingService.Trace("Load CRM Service from context --- OK");


        #endregion
        _helper = new Helper(_service);

        _helper.GetConfig();

        if (_helper.IsConfigValID())
        {

            #region Get currentAuthenticationResponse


            if (_helper.IsTokenNeedUpdate())
            {
                url = _helper.GetAuthUrl();

                using (var serviceRequest = new WebClient())
                {
                    var responseBytes = serviceRequest.DownloadString(url);
                    var currentAuthenticationResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(responseBytes);
                    if (currentAuthenticationResponse.ResponseHeader.StatusOk)
                    {
                        _helper.Token = currentAuthenticationResponse.ResponseHeader.AuthenticationTokenBytes;
                        _helper.LastRequestReceivedData = currentAuthenticationResponse.ResponseHeader.RequestReceivedDateTime;

                        _helper.UpdateConfig();
                    }
                    else
                    {
                        throw new AccessViolationException("Can't get authorization token using esp login details");
                    }
                }
            }
            else
            {
                _helper.LastRequestReceivedData = (DateTime)_helper.Config.Attributes["ddsm_requestreceiveddate"];
                _helper.Token = Convert.FromBase64String(_helper.Config.Attributes["ddsm_esplasttoken"].ToString());
            }

            #endregion


            #region get available calculators

            var aAvailableCalculatorsRequest = new AvailableCalculatorsRequest
            {
                RequestHeader = { AuthenticationTokenBytes = _helper.Token },
                TargetDateTime = _helper.LastRequestReceivedData
            };
            AvailableCalculatorsResponse measureLibrarysList = null;

            url = _helper.GetAvailableCalculatorsUrl();

            var requestCalculators = JsonConvert.SerializeObject(aAvailableCalculatorsRequest);


            #endregion


            #region get all mad defs by measure

            var client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } };
            var response = client.UploadString(url, "POST", requestCalculators);

            measureLibrarysList = JsonConvert.DeserializeObject<AvailableCalculatorsResponse>(response);
            //throw new Exception("MeasureLib: " + response);

            var dataDefsRequest = GetDataDefsRequest(measureLibrarysList, _helper.Token);

            ///call calculator datadefinitions
            CalculatorDataDefinitionsResponse result = null;

            url = _helper.GetCalculatiorDataDefinitionsUrl();
            var request = JsonConvert.SerializeObject(dataDefsRequest);

            client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } };
            response = client.UploadString(url, "POST", request);

            result = JsonConvert.DeserializeObject<CalculatorDataDefinitionsResponse>(response);

            var jsonResult = GetUniqueDataMadJson(result);
            #endregion

            Complete.Set(context, true);
            Result.Set(context, jsonResult);
        }


    }


    /// <summary>
    /// Return JSON Dictionary, when key = Measure Name, Value = list of MAD fields
    /// </summary>
    /// <returns></returns>
    CalculatorDataDefinitionsRequest GetDataDefsRequest(AvailableCalculatorsResponse response, byte[] token)
    {
        var measureLibrarysList = response.CalculatorLibraryResponseList; //measureLibrarys
        var request = new CalculatorDataDefinitionsRequest
        {
            RequestHeader = { AuthenticationTokenBytes = token }
        };

        foreach (var measureLib in measureLibrarysList)
        {
            foreach (var measure in measureLib.CalculatorList)
            {
                request.CalculatorDefinitionRequestList.Add(new CalculatorDefinitionRequest { CalculatorID = measure.ID, TargetDateTime = _helper.LastRequestReceivedData });
            }
        }
        return request;
    }

    string GetUniqueDataMadJson(CalculatorDataDefinitionsResponse madDefinitionResponse)
    {
        var result = new List<ESPWSVariableDefinitionResponse>();

        if (madDefinitionResponse != null && madDefinitionResponse.ResponseHeader.StatusOk)
        {
            foreach (var measureMad in madDefinitionResponse.ESPWSCalculatorDataDefinitionResponseList)
            {
                result.AddRange(measureMad.ESPVariableDefinitionList);
            }
            result = result.GroupBy(p => p.Name.ToUpper()).Select(g => g.First()).OrderBy(x => x.Name).ToList();
        }
        return JsonConvert.SerializeObject(result);
    }

}
