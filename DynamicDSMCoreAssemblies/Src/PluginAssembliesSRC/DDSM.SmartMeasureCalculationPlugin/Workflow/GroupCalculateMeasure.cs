using System;
using System.Activities;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using DDSM.SmartMeasureCalculationPlugin.Model;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using DDSM.SmartMeasureCalculationPlugin.Workflow;
using ESPWebServices.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using DDSM.CommonProvider;
using Microsoft.Crm.Sdk.Messages;

public class GroupCalculateMeasure : BaseCodeActivity
{

    private Helper _helper;
    private Common ObjCommon;
    private CrmHelper CrmHelper;
    private IOrganizationService Service;
    private List<string> _processingLog;
    #region "Parameter Definition"

    [Output("Processed Projects")]
    public OutArgument<string> ProcessedProject { get; set; }

    [Input("Project")]
    //[RequiredArgument]
    [ArgumentEntity("ddsm_project")]
    [ReferenceTarget("ddsm_project")]
    public InArgument<EntityReference> Project { get; set; }

    [Input("From Measure Grid")]
    public InArgument<bool> FromMeasureGrid { get; set; }



    #endregion

    #region Internal Fields

    private string url = "";

    ConcurrentDictionary<string, MappingItem> MappingData;
    ConcurrentDictionary<string, MappingItem> QddMappingData;
    ESPConfig espConfig;

    readonly string MainEntity = "ddsm_measure";





#if WITH_TASKQ
    //private IList<Guid> _processedProject;
    //private IList<Guid> _processedProgOff;
    //private IList<Guid> _processedMasures;
    UserInputObj2 measureIDs;
#endif


    bool _fromMeasuregrid;

    #endregion

    internal override TaskQueue.StatusFileDataUploading CurrentOperationStatus
    {
        get { return TaskQueue.StatusFileDataUploading.ESPRecalculation; }
    }

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        ObjCommon = new Common(executionContext);
        Service = ObjCommon.GetOrgService();
        CrmHelper = new CrmHelper(Service);
        _helper = new Helper(ObjCommon.GetOrgService(systemCall: true));
        _processingLog = new List<string>();

        ObjCommon = new Common(executionContext);



        _helper.GetConfig();
        espConfig = new ESPConfig(_helper.Config);

        var measureIDsJson = UserInput.Get(executionContext);
        ObjCommon.TracingService.Trace("before get target");
        EntityReference target = Project.Get(executionContext);


        if (espConfig.EspRemoteCalculation)
        {
            RunRemoteEspCalc(espConfig, target);
            return;
        }
        ObjCommon.TracingService.Trace("in ExecuteActivity");
        Entity log = new Entity("ddsm_taskqueue");
        var logList = new List<String>();
        ObjCommon.TracingService.Trace("GroupCalculateMeasure: in ExecuteActivity()");
        IList<Guid> processedProject = new List<Guid>();
        IList<Guid> processedProgOff = new List<Guid>();
        IList<Guid> processedMasures = new List<Guid>();
        ConcurrentDictionary<Guid, ConcurrentDictionary<string, MappingItem>> personalMads = new ConcurrentDictionary<Guid, ConcurrentDictionary<string, MappingItem>>();
        ConcurrentDictionary<Guid, ConcurrentDictionary<string, MappingItem>> personalQdds = new ConcurrentDictionary<Guid, ConcurrentDictionary<string, MappingItem>>();


        if (!string.IsNullOrEmpty(measureIDsJson))
        {
            measureIDs = JsonConvert.DeserializeObject<UserInputObj2>(measureIDsJson);
            //       ObjCommon.TracingService.Trace("measureIDsJson != null: " + measureIDsJson);
        }



        //if UserInput is empty and target entity is ddsm_project try get measure list by target id 
        if (target?.LogicalName == "ddsm_project" && measureIDs == null)
        {
            measureIDs = GetMeasureByTarget(target, ObjCommon.TracingService);
        }
        else
        {
            ObjCommon.TracingService.Trace(" measures list is empty ");//TODO: fix msg
        }

        if (FromMeasureGrid != null && FromMeasureGrid.Get(executionContext))
        {
            _fromMeasuregrid = true;
        }
        try
        {

            var successMeasCount = 0;
            if (_helper.IsConfigValID())
            {
                #region Get currentAuthenticationResponse

                if (_helper.IsTokenNeedUpdate())
                {
                    AuthenticationResponse currentAuthenticationResponse;

                    url = _helper.GetAuthUrl();

                    using (var serviceRequest = new WebClient())
                    {
                        var responseBytes = serviceRequest.DownloadString(url);
                        currentAuthenticationResponse =
                            JsonConvert.DeserializeObject<AuthenticationResponse>(responseBytes);
                        if (currentAuthenticationResponse.ResponseHeader.StatusOk)
                        {
                            _helper.Token = currentAuthenticationResponse.ResponseHeader.AuthenticationTokenBytes;
                            _helper.LastRequestReceivedData =
                                currentAuthenticationResponse.ResponseHeader.RequestReceivedDateTime;

                            //jsonResult = string.Format("{0};{1}", Convert.ToBase64String(token), LastRequestReceivedData.ToString());
                            _helper.UpdateConfig();
                        }
                        else
                        {
                            throw new AccessViolationException("Can't get authorization token using esp login details");
                        }
                    }
                }
                else
                {
                    _helper.LastRequestReceivedData = (DateTime)_helper.Config.Attributes["ddsm_requestreceiveddate"];
                    _helper.Token = Convert.FromBase64String(_helper.Config.Attributes["ddsm_esplasttoken"].ToString());
                }

                #endregion

                if (measureIDs?.SmartMeasures != null)
                {
                    ObjCommon.TracingService.Trace("MEaure count " + measureIDs.SmartMeasures.Count);
                    var allProcessedCount = 0;
                    //#if DEBUG
                    //                    var GroupCount = 1;// _helper.Config.GetAttributeValue<decimal>("ddsm_espdataportion");
                    //#else
                    var GroupCount = _helper.Config.GetAttributeValue<decimal>("ddsm_espdataportion");
                    //#endif
                    while (allProcessedCount < measureIDs.SmartMeasures.Count)
                    {
                        var packGroup = new UserInputObj2();
                        packGroup.DataFields = measureIDs.DataFields;
                        packGroup.SmartMeasures = measureIDs.SmartMeasures.Skip(allProcessedCount).Take((int)GroupCount).ToList();
                        allProcessedCount += packGroup.SmartMeasures.Count;

                        var smartMeasureData = GetMeasuresInfo(packGroup.SmartMeasures);
                        var processedMeasures = new List<KeyValuePair<Guid, Guid>>();
                        if (smartMeasureData?.Entities.Count > 0)
                        {
                            var calcRequest = BuildCalculationRequest(smartMeasureData, packGroup, processedMeasures,
                                processedProject, processedProgOff, processedMasures, personalMads, personalQdds);

                            //Call calc request
                            CalculationResponse resultCalculationResponse = null;
                            url = _helper.GetCalculatioUrl();
                            var request = JsonConvert.SerializeObject(calcRequest);

                            var client = new WebClient();
                            client.Headers[HttpRequestHeader.ContentType] = "application/json";
                            var response = client.UploadString(url, "POST", request);

                            resultCalculationResponse = JsonConvert.DeserializeObject<CalculationResponse>(response);
                            UpdateMeasures(resultCalculationResponse, packGroup, processedMeasures, personalMads,
                                personalQdds, logList, ObjCommon.GetOrgService());
                            successMeasCount++;
                        }
                        else
                        {
                            ObjCommon.TracingService.Trace(
                                "The project does not have any smart measure for calculation. Collect data for Parent calculation task!");
                        }
                    }
                    // now we getting all measure (not only smart) 
                    GetProjectsByMeasure(measureIDs.SmartMeasures, processedProject, processedProgOff, processedMasures);
                }
                else
                {
                    ObjCommon.TracingService.Trace("You have no project for measure calculation");
                    throw new Exception("You have no project for measure calculation");
                }
            }
            else
            {
                throw new AccessViolationException(
                    "Can't get authorization token using esp login details. Please contact to your administrator.");
            }

            Result.Set(executionContext,
                JsonConvert.SerializeObject(
                    new
                    {
                        MeasureName = "",
                        MeasureID = "",
                        ErrorMsg = string.Join("\n", _processingLog.OrderBy(x => x).ToArray()),
                        completecount = successMeasCount
                    }));
            Complete.Set(executionContext, true);
            ObjCommon.TracingService.Trace(string.Join("\n", _processingLog.ToArray()));
        }
        catch (Exception ex)
        {

            SetErrorStatusForDA();
            ObjCommon.TracingService.Trace("FATAL ERROR: " + ex.Message);
            Result.Set(executionContext,
                JsonConvert.SerializeObject(
                    new
                    {
                        MeasureName = "",
                        MeasureID = "",
                        ErrorMsg = string.Join("\n", _processingLog.OrderBy(x => x).ToArray()),
                        completecount = 0
                    }));
            Complete.Set(executionContext, false);
        }
        finally
        {
#if WITH_TASKQ
            processCollectedData(processedProject);
            var processed = JsonConvert.SerializeObject(new UserInputObj2 { SmartMeasures = processedProject.ToList() });
            ProcessedProject.Set(executionContext, processed);

            _helper = null;
            ObjCommon = null;
            Service = null;
            CrmHelper = null;
            _processingLog = null;
            DataUploader = null;

#endif
        }
    }

    private void RunRemoteEspCalc(ESPConfig espConfig, EntityReference projectRef)
    {
        //var userRequest = new WhoAmIRequest();
        //var whoAmI = (WhoAmIResponse)Service.Execute(userRequest);
        var espRemoteCalcURL = espConfig.EspRemoteCalculationApiUrl;
        var client = new HttpClient { BaseAddress = new Uri(espRemoteCalcURL) };
        var target = Extentions.GetTargetData(ObjCommon);
        var config = new
        {
            UserId = ObjCommon?.Context?.InitiatingUserId,
            Project = projectRef,
            FromMeasureGrid = true,
            //  Config = espConfig,
            Target = target
        };

        var data = JsonConvert.SerializeObject(config);
        var content = new StringContent(data, Encoding.UTF8, "application/json");
        client.PostAsync("GroupCalculateMeasures", content);
        //var rrr = ddd.Result.Content.ReadAsStringAsync();
    }

    private UserInputObj2 GetMeasureByTarget(EntityReference target, ITracingService trace)
    {
        trace.Trace("in GetMeasureByTarget()");
        trace.Trace("target name: " + target.LogicalName);
        trace.Trace("target Id: " + target.Id);

        var query = new QueryExpression()
        {
            EntityName = "ddsm_measure",
            //ColumnSet = new ColumnSet(true),
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions =
               {
                   new ConditionExpression("ddsm_projecttomeasureid", ConditionOperator.Equal, target.Id)
               }
            }
        };
        var result = ObjCommon.GetOrgService(systemCall: true).RetrieveMultiple(query);
        trace.Trace("count of measures: " + result.Entities.Count);

        return new UserInputObj2() { SmartMeasures = result.Entities.Select(x => x.Id).ToList<Guid>() };
    }

    private void GetProjectsByMeasure(List<Guid> IDs, IList<Guid> processedProject, IList<Guid> processedProgOff, IList<Guid> processedMasures)
    {
        // clear 
        var measureIds = IDs.Where(x => !x.Equals(Guid.Empty)).ToList();
        var expr = new QueryExpression
        {
            EntityName = MainEntity,
            ColumnSet = new ColumnSet("ddsm_projecttomeasureid", "ddsm_programofferingsid"),
            // ColumnSet = new ColumnSet("ddsm_measureid", "ddsm_smartmeasureid", "ddsm_madfields", "ddsm_name", "ddsm_esptargetdate"),
        };


        // Guid calcTypeId = CrmHelper.GetMeasureCalcType();

        expr.Criteria = new FilterExpression
        {
            FilterOperator = LogicalOperator.And,
            Conditions =
            {
                new ConditionExpression("ddsm_measureid", ConditionOperator.In, measureIds),
            }
        };


        var measures = Service.RetrieveMultiple(expr);

        foreach (var meas in measures.Entities)
        {
            collectProcessedData(meas, processedProject, processedProgOff, processedMasures);
        }
    }

#if WITH_TASKQ
    private void processCollectedData(IList<Guid> processedProject)
    {
        var taskQueue = new TaskQueue(Service);

        if (processedProject.Count > 0)
        {
            taskQueue.Create(new DDSM_Task(TaskQueue.TaskEntity.Project)
            {
                ProcessedItems0 = processedProject.Select(x => x).Distinct().ToList()
            }, TaskQueue.TaskEntity.Project, dataUploader: DataUploader
            );
        }
    }
#endif

    #region Internal methods 1111

    private CalculationRequest BuildCalculationRequest(EntityCollection measureData, UserInputObj2 userInput,
        List<KeyValuePair<Guid, Guid>> processedMeasures,
        IList<Guid> processedProject, IList<Guid> processedProgOff,
        IList<Guid> processedMasures,
        ConcurrentDictionary<Guid, ConcurrentDictionary<string, MappingItem>> personalMads,
        ConcurrentDictionary<Guid, ConcurrentDictionary<string, MappingItem>> personalQdds)
    {
        var aCalculationRequest = new CalculationRequest { RequestHeader = { AuthenticationTokenBytes = _helper.Token } };

        JsonConvert.DeserializeObject<Dictionary<string, MappingItem>>(
            _helper.Config.Attributes["ddsm_espmapingdata"].ToString());
        var processedCount = 0;
        foreach (var measureItem in measureData.Entities)
        {
            var calculatorCalculationRequest = new CalculatorCalculationRequest
            {
                CalculatorID =
                    new Guid(
                        (measureItem.Attributes["ddsm_measuretemplate1.ddsm_espsmartmeasureid"] as AliasedValue).Value
                            .ToString()),
                TargetDateTime = DateTime.UtcNow
            };
            var curentMad =
                JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(
                    measureItem.Attributes["ddsm_madfields"].ToString());
            MappingData = CrmHelper.GetMadMapping(measureItem, groupRecalc: true);
            QddMappingData = CrmHelper.GetQddMapping(measureItem, groupRecalc: true);

            personalMads.TryAdd(measureItem.Id, MappingData); //store personal mads to list
            personalQdds.TryAdd(measureItem.Id, QddMappingData);
            // get data
            var data = GetMeasureData(curentMad, MappingData, measureItem.Attributes["ddsm_measureid"].ToString());


            // fill request from DB values
            foreach (var mad in curentMad)
            {
                ObjCommon.TracingService.Trace("--" + mad.Name.ToLower());
                var crmFiled = MappingData.FirstOrDefault(x => x.Key.ToLower().Equals(mad.Name.ToLower()));
                var variableValueRequest = CrmHelper.GenerateESPWSVariableValueRequest(crmFiled, mad, data, MainEntity);

                if (calculatorCalculationRequest.InputVariables.FirstOrDefault(x => x.Name.Equals(mad.Name)) == null)
                {
                    calculatorCalculationRequest.InputVariables.Add(variableValueRequest);
                }
            }
            processedCount++;

#if WITH_TASKQ
            collectProcessedData(data, processedProject, processedProgOff, processedMasures);
#endif
            if (curentMad.Count == calculatorCalculationRequest.InputVariables.Count)
            {
                aCalculationRequest.CalculatorCalculationRequestList.Add(calculatorCalculationRequest);
                processedMeasures.Add(new KeyValuePair<Guid, Guid>(
                    new Guid(measureItem.Attributes["ddsm_measureid"].ToString()),
                    calculatorCalculationRequest.CalculatorID)
                    );
            }
            else
            {
                ObjCommon.TracingService.Trace("WARN: " + $"Mapping MAD are not successfully. Measure: {measureItem.Attributes["ddsm_name"]}. Current MAD count: {curentMad.Count}, mapped only: {calculatorCalculationRequest.InputVariables.Count}. Please check MAD mapping for current Smart Measure Template Or contact your administrator.");
            }
        }

        if (aCalculationRequest.CalculatorCalculationRequestList.Count == 0)
        {
            throw new Exception("Can't build Calculation request. No one mapped Smart Measure");
        }
        return aCalculationRequest;
    }

#if WITH_TASKQ
    private void collectProcessedData(Entity data, IList<Guid> processedProject, IList<Guid> processedProgOff, IList<Guid> processedMasures)
    {
        object attrValue;
        if (data.Attributes.TryGetValue("ddsm_projecttomeasureid", out attrValue))
        {
            var projRef = attrValue as EntityReference;
            processedProject.Add(projRef.Id);
        }

        if (data.Attributes.TryGetValue("ddsm_programofferingsid", out attrValue))
        {
            var progOffRef = attrValue as EntityReference;
            processedProgOff.Add(progOffRef.Id);
        }

        //collect measure ids  
        processedMasures.Add(data.Id);

    }
#endif

    private Entity GetMeasureData(List<ESPWSVariableDefinitionResponse> curentMad, ConcurrentDictionary<string, MappingItem> _mappingData, string ID)
    {
        var sortedMadList = (from x in curentMad
                             join y in _mappingData on x.Name.ToLower() equals y.Key.ToLower()
                             select new KeyValuePair<string, string>(y.Value.AttrName.ToLower(), y.Value.Entity));//.ToDictionary(x => x.Key, x => x.Value);

        var sortedMad = new Dictionary<string, string>();
        foreach (var item in sortedMadList)
        {
            if (!sortedMad.ContainsKey(item.Key))
            {
                sortedMad.Add(item.Key, item.Value);
            }
        }

        var queryMain = new QueryExpression
        {
            EntityName = MainEntity
        };

        var relatedEntity = new RelationshipQueryCollection();

        foreach (var key in sortedMad.Values.GroupBy(x => x))
        {
            //fill measure columns
            if (key.Key.Equals(MainEntity))
            {
                foreach (var item in sortedMad)
                {
                    if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                        queryMain.ColumnSet.AddColumn(item.Key);
                }
            }//add relates
            else
            {
                if (key.Key.Equals("ddsm_project"))
                {
                    var relationship = new Relationship { SchemaName = "ddsm_ddsm_project_ddsm_measure" };

                    var query = new QueryExpression
                    {
                        EntityName = key.Key
                    };

                    foreach (var item in sortedMad)
                    {
                        if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                            query.ColumnSet.AddColumn(item.Key);
                    }

                    relatedEntity.Add(relationship, query);
                }
                if (key.Key.Equals("account"))
                {
                    var relationship = new Relationship { SchemaName = "ddsm_account_ddsm_measure" };

                    var query = new QueryExpression
                    {
                        EntityName = key.Key
                    };

                    foreach (var item in sortedMad)
                    {
                        if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                            query.ColumnSet.AddColumn(item.Key);
                    }
                    relatedEntity.Add(relationship, query);

                }
                if (key.Key.Equals("ddsm_site"))
                {
                    var relationship = new Relationship { SchemaName = "ddsm_ddsm_site_ddsm_measure" };

                    var query = new QueryExpression
                    {
                        EntityName = key.Key
                    };

                    foreach (var item in sortedMad)
                    {
                        if (!string.IsNullOrEmpty(item.Key) && item.Value == key.Key)
                            query.ColumnSet.AddColumn(item.Key);
                    }

                    relatedEntity.Add(relationship, query);
                }
            }
#if WITH_TASKQ
            //get ProjId,POId,
            if (!queryMain.ColumnSet.Columns.Contains("ddsm_projecttomeasureid"))
                queryMain.ColumnSet.AddColumn("ddsm_projecttomeasureid");

            if (!queryMain.ColumnSet.Columns.Contains("ddsm_programofferingsid"))
                queryMain.ColumnSet.AddColumn("ddsm_programofferingsid");

#endif

        }

        var request = new RetrieveRequest
        {
            ColumnSet = queryMain.ColumnSet,
            Target = new EntityReference { Id = new Guid(ID), LogicalName = MainEntity },
            RelatedEntitiesQuery = relatedEntity
        };

        var response = (RetrieveResponse)Service.Execute(request);

        var result = new Dictionary<string, object>();

        foreach (var relEnt in response.Entity.RelatedEntities)
        {
            result.Add(relEnt.Value.EntityName, relEnt.Value.Entities[0].Attributes);
        }
        return response.Entity;
    }

    private EntityCollection GetMeasuresInfo(List<Guid> IDs, bool isSmartMeasure = true)
    {
        // clear 
        var measureIds = IDs.Where(x => !x.Equals(Guid.Empty)).ToList();
        var expr = new QueryExpression
        {
            EntityName = MainEntity,
            ColumnSet = new ColumnSet("ddsm_madfields"),
            // ColumnSet = new ColumnSet("ddsm_measureid", "ddsm_smartmeasureid", "ddsm_madfields", "ddsm_name", "ddsm_esptargetdate"),
        };
        expr.LinkEntities.Add(new LinkEntity(MainEntity, "ddsm_measuretemplate", "ddsm_measureselector", "ddsm_measuretemplateid", JoinOperator.Inner)
        {
            LinkEntities = {
                new LinkEntity("ddsm_measuretemplate","ddsm_mapping","ddsm_mappingid","ddsm_mappingid",JoinOperator.Inner)  { Columns = new ColumnSet("ddsm_jsondata")  },
                new LinkEntity("ddsm_measuretemplate","ddsm_mapping","ddsm_qddmappingid","ddsm_mappingid",JoinOperator.Inner)  { Columns = new ColumnSet("ddsm_jsondata")  },
            }
        });
        expr.LinkEntities[0].Columns.AddColumns("ddsm_madfields", "ddsm_espsmartmeasureid", "ddsm_mappingid");

        var calcTypeId = CrmHelper.GetMeasureCalcType();

        if (_fromMeasuregrid)
        {
            if (isSmartMeasure)
            {
                expr.Criteria = new FilterExpression
                {
                    FilterOperator = LogicalOperator.And,
                    Conditions = { new ConditionExpression("ddsm_measureid", ConditionOperator.In, measureIds),
                        new ConditionExpression("ddsm_calculationtype", ConditionOperator.Equal, calcTypeId.Id),
                        new ConditionExpression("ddsm_disablerecalculation", ConditionOperator.NotEqual, true),
                     //   new ConditionExpression("ddsm_recalculatemeasure", ConditionOperator.Equal, true),
                }
                };
            }
            else
            {
                ObjCommon.TracingService.Trace("get data for non smart measure");
                expr.Criteria = new FilterExpression
                {
                    FilterOperator = LogicalOperator.And,
                    Conditions = { new ConditionExpression("ddsm_measureid", ConditionOperator.In, measureIds),
                        new ConditionExpression("ddsm_calculationtype", ConditionOperator.NotEqual, calcTypeId),
                        new ConditionExpression("ddsm_disablerecalculation", ConditionOperator.NotEqual, true),
                        new ConditionExpression("ddsm_recalculatemeasure", ConditionOperator.Equal, true),
                }
                };
            }
        }
        else
        {
            expr.Criteria = new FilterExpression
            {
                FilterOperator = LogicalOperator.And,
                Conditions = { new ConditionExpression("ddsm_projecttomeasureid", ConditionOperator.In, measureIds),
                        new ConditionExpression("ddsm_calculationtype", ConditionOperator.Equal, calcTypeId),
                        new ConditionExpression("ddsm_disablerecalculation", ConditionOperator.NotEqual, true)
                }
            };
        }

        return Service.RetrieveMultiple(expr);
    }

    private void UpdateMeasures(CalculationResponse calcResult, UserInputObj2 userInput, List<KeyValuePair<Guid, Guid>> processedMeasures, ConcurrentDictionary<Guid, ConcurrentDictionary<string, MappingItem>> personalMads, ConcurrentDictionary<Guid, ConcurrentDictionary<string, MappingItem>> personalQdds, List<string> log, IOrganizationService service)
    {
        ConcurrentDictionary<string, MappingItem> QDDmappingData;//JsonConvert.DeserializeObject<Dictionary<string, MappingItem>>(_helper.Config.Attributes["ddsm_espqddmappingdata"].ToString());
        ConcurrentDictionary<string, MappingItem> MADmappingData;//JsonConvert.DeserializeObject<Dictionary<string, MappingItem>>(_helper.Config.Attributes["ddsm_espmapingdata"].ToString());

        var requestWithResults = new ExecuteMultipleRequest()
        {
            // Assign settings that define execution behavior: continue on error, return responses. 
            Settings = new ExecuteMultipleSettings()
            {
                ContinueOnError = false,
                ReturnResponses = true
            },
            // Create an empty organization request collection.
            Requests = new OrganizationRequestCollection()
        };

        var currentMSDData = new Dictionary<string, MSDField>();

        for (int i = 0; i < calcResult?.ESPWSCalculatorCalculationList?.Count; i++)
        {
            var measureCalculation = calcResult.ESPWSCalculatorCalculationList[i];
            if (!measureCalculation.ResponseHeader.StatusOk)
            {
                ObjCommon.TracingService.Trace($"Updating measure with error. Error code: {calcResult.ResponseHeader.ErrorCode}. \nError: {calcResult.GetFullESPError()}.");
                continue;
            }
            Entity measureForUpdateOrig;
            Entity measureForUpdate;
            var rrr = processedMeasures[i];

            if (rrr.Value != measureCalculation.ID)
            {
                ObjCommon.TracingService.Trace("Can't get Measure Id for updating");
                ObjCommon.TracingService.Trace("calcId: " + measureCalculation.ID + " but ProcessedMesure calcId: " + rrr.Value);
                continue;
            }

            if (userInput.DataFields != null && userInput.DataFields.ContainsKey("ddsm_recalculationgroup") && !string.IsNullOrEmpty(userInput.DataFields["ddsm_recalculationgroup"]))
            {
                MADmappingData = personalMads.FirstOrDefault(x => x.Key.Equals(rrr.Key)).Value;
                QDDmappingData = personalQdds.FirstOrDefault(x => x.Key.Equals(rrr.Key)).Value;

                measureForUpdateOrig = ObjCommon.GetOrgService().Retrieve(MainEntity, rrr.Key, new ColumnSet(allColumns: true));
                // processedMeasures.Remove(rrr.Key);
                measureForUpdate = new Entity(MainEntity);

                foreach (var attr in measureForUpdateOrig.Attributes)
                {
                    if (attr.Key != "ddsm_measureid")
                    {
                        measureForUpdate.Attributes.Add(attr);
                    }
                }
            }
            else
            {
                //var rrr = processedMeasures.FirstOrDefault(x => x.Value.Equals(measureCalculation.ID));
                MADmappingData = personalMads.FirstOrDefault(x => x.Key.Equals(rrr.Key)).Value;
                QDDmappingData = personalQdds.FirstOrDefault(x => x.Key.Equals(rrr.Key)).Value;
                measureForUpdate = new Entity(MainEntity, rrr.Key);
                // processedMeasures.Remove(rrr.Key);
            }

            // set mads
            ObjCommon.TracingService.Trace("fill mads");
            foreach (var result in measureCalculation.ESPWSVariableCalculationList)
            {
                var mapItem = MADmappingData.FirstOrDefault(x => x.Key.ToLower().Equals(result.Name.ToLower()) && !string.IsNullOrEmpty(x.Value.AttrName));

                if (mapItem.Value != null && mapItem.Key != null && mapItem.Value.Entity.Equals(MainEntity))
                {
                    //  ObjCommon.TracingService.Trace("set Attr value for mapping Item: "+ JsonConvert.SerializeObject(mapItem) );
                    CrmHelper.SetAttributeValue(mapItem.Value, result, measureForUpdate);

                }
            }
            ObjCommon.TracingService.Trace("fill qdds");
            //set qdds
            foreach (var result in measureCalculation.ESPWSVariableCalculationList)
            {
                try
                {
                    var mapItem = QDDmappingData.FirstOrDefault(x => x.Key.ToLower().Equals(result.Name.ToLower()) && !string.IsNullOrEmpty(x.Value.AttrName));

                    if (mapItem.Value != null && mapItem.Key != null && mapItem.Value.Entity.Equals(MainEntity))
                    {
                        CrmHelper.SetAttributeValue(mapItem.Value, result, measureForUpdate);
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }

            }
            var valueOfrecalcGroup = "";

            if (userInput.DataFields != null && userInput.DataFields.TryGetValue("ddsm_recalculationgroup", out valueOfrecalcGroup))
                measureForUpdate["ddsm_recalculationgroup"] = valueOfrecalcGroup;

            measureForUpdate["ddsm_recalculationdate"] = measureForUpdate["ddsm_esptargetdate"] = DateTime.UtcNow;
            measureForUpdate["ddsm_espcalculationsuccessful"] = true;
            // measureForUpdate["ddsm_fundingcalculation"] = Guid.NewGuid().ToString();
            measureForUpdate["ddsm_calculationrecordstatus"] =
                new OptionSetValue((int)CalculateMeasure.CalculationRecordStatus.NotReady); // ready for calculation of parent

            if (userInput.DataFields != null && !string.IsNullOrEmpty(valueOfrecalcGroup))
            {
                var createRequest = new CreateRequest { Target = measureForUpdate };
                requestWithResults.Requests.Add(createRequest);
                // Service.Create(measureForUpdate);
            }
            else
            {
                // log.Add("MEASURE To UPDATE: " + JsonConvert.SerializeObject(measureForUpdate));
                var updateRequest = new UpdateRequest { Target = measureForUpdate };
                requestWithResults.Requests.Add(updateRequest);
                //  Service.Update(measureForUpdate);
            }
        }
        // Execute all the requests in the request collection using a single web method call.
        ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)service.Execute(requestWithResults);

        //foreach (UpdateRequest req in requestWithResults.Requests)
        //{
        //    service.Update(req.Target);
        //}

        if (responseWithResults.IsFaulted)
        {
            foreach (var resp in responseWithResults.Responses)
            {
                if (resp.Fault != null)
                {
                    ObjCommon.TracingService.Trace("in ExecuteMultipleResponse: " + resp.Fault.Message);
                }
            }
        }
        //}
        //else
        //{
        //    //foreach (var measureCalculation in calcResult.ESPWSCalculatorCalculationList)
        //    //{
        //    //    var rrr = ProcessedMeasures.FirstOrDefault(x => x.Value.Equals(measureCalculation.ID));
        //    //    var measureForUpdate = new Entity(MainEntity, rrr.Key)
        //    //    {
        //    //        ["ddsm_fundingcalculation"] = Guid.NewGuid().ToString()
        //    //    };
        //    //    ProcessedMeasures.Remove(rrr.Key);
        //    //    Service.Update(measureForUpdate);
        //    //}


        //    // throw new Exception($"Upgrading measure impossible. Error code: {calcResult.ResponseHeader.ErrorCode}. \nError: {calcResult.GetFullESPError()}.");
        //}

    }

    #endregion
}

