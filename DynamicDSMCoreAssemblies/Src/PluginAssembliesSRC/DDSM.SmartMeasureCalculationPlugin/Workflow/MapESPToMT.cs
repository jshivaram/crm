﻿using DDSM.SmartMeasureCalculationPlugin.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using System;
using System.Activities;
using System.Collections.Generic;

//namespace DDSM.SmartMeasureCalculationPlugin.Workflow
//{
    public class MapESPToMT : CodeActivity
    {
        #region "Parameter Definition"

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        [Output("Result")]
        public OutArgument<string> Result { get; set; }

        [Input("User Input")]
        public InArgument<string> UserInput { get; set; }


        #endregion

        #region Internal Fields
        IOrganizationService _service;
        private Common _objCommon;
        private Helper _helper;
        private string url = "";
        readonly List<String> _processingLog = new List<string>();        
        readonly string MainEntity = "ddsm_measure";
        Dictionary<Guid, Guid> ProcessedMeasures = new Dictionary<Guid, Guid>(); // key crm ID, value esp ID
        bool _fromMeasuregrid;
        #endregion

        protected override void Execute(CodeActivityContext context)
        {
            #region "Load CRM Service from context"
            _objCommon = new Common(context);
            _service = _objCommon.Service;
            _objCommon.TracingService.Trace("Load CRM Service from context --- OK");
            #endregion

            _helper = new Helper(_service);

            var mappingDataJson = UserInput.Get(context);


            //dict where Key = MTid,value= Array{0=SMid,1=SMLid}
            var mappingData =  JsonConvert.DeserializeObject<Dictionary<Guid,Guid[]>>(mappingDataJson);


            // Update MT (add sml_id, sm_id)
            foreach (var itemMT in mappingData)
            {
                var Mt = new Entity("ddsm_measuretemplate", itemMT.Key);
                //sm lib ref
                Mt.Attributes["ddsm_espcalculationid"] = new EntityReference("", itemMT.Value[0]);
                //sm calc id
                Mt.Attributes["ddsm_espcalculationid"] = itemMT.Value[0].ToString();

                _service.Update(Mt);
            }



            // Update MT (add sml_id, sm_id)
            foreach (var itemMT in mappingData)
            {
                var Mt = new Entity("ddsm_measuretemplate", itemMT.Key);

                //sm calc id
                Mt.Attributes["ddsm_espsmartmeasureid"] = itemMT.Value[0].ToString();

                //sm lib ref
                Mt.Attributes["ddsm_espmeasurelibraryid"] = new EntityReference("ddsm_espmeasurelibrary", itemMT.Value[1]);
                

                _service.Update(Mt);




                //Update All vers of SM

                var expr = new QueryExpression
                {
                    EntityName = "ddsm_smartmeasureversion",
                    ColumnSet = new ColumnSet("ddsm_calculationid"),
                    Criteria = new FilterExpression
                    {
                        FilterOperator = LogicalOperator.And,
                        Conditions = { new ConditionExpression("ddsm_calculationid", ConditionOperator.Equal, itemMT.Value[0]) }
                    }

                };

                var smVersionList= _service.RetrieveMultiple(expr);


                foreach (var version in smVersionList.Entities)
                {
                    version.Attributes["ddsm_measuretemplateid"] = new EntityReference("ddsm_measuretemplate", itemMT.Key);
                    _service.Update(version);

                }


            }
            



        }
    }
//}
