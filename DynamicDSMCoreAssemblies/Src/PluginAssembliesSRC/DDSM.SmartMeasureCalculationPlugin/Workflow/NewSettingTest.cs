﻿using System;
using System.Activities;
using DDSM.CommonProvider;
using DDSM.CommonProvider.Utils;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using DDSM.SmartMeasureCalculationPlugin.Workflow;


class NewSettingTest : BaseCodeActivity
{
    internal override TaskQueue.StatusFileDataUploading CurrentOperationStatus { get; }
    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        var ObjCommon = new Common(executionContext);
        var conf = SettingsProvider.GetConfig<ESPConfig>(ObjCommon.GetOrgService(), typeof(ESPConfig));

        conf.RequestReceivedDate = DateTime.UtcNow;
        SettingsProvider.SaveConfig(ObjCommon.GetOrgService(systemCall: true), conf);
    }
}

