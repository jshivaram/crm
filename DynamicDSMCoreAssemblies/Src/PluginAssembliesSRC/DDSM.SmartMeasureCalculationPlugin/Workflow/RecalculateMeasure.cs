﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using DDSM.CommonProvider;
using DDSM.SmartMeasureCalculationPlugin.Model;
using DDSM.SmartMeasureCalculationPlugin.Utils;
using ESPWebServices.Models;
using DDSM.SmartMeasureCalculationPlugin.Workflow;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;

public class RecalculateMeasure : BaseCodeActivity
{
    private Helper _helper;
    private Common ObjCommon;
    private CrmHelper CrmHelper;
    private IOrganizationService Service;
    private List<string> _processingLog;

    #region "Parameter Definition"

    [RequiredArgument]
    [Input("Measure")]
    [ReferenceTarget("ddsm_measure")]
    public InArgument<EntityReference> Measure { get; set; }

    [RequiredArgument]
    [Input("Measure Type")]
    public InArgument<string> MeasureType { get; set; }

    [RequiredArgument]
    [Input("DisableRecalculation")]
    public InArgument<bool> DisableRecalculation { get; set; }
    #endregion

    #region Internal Fileds

    private string url = "";
   // readonly List<String> _processingLog = new List<string>();
    Dictionary<string, MappingItem> MappingData;
    readonly string MainEntity = "ddsm_measure";
    Dictionary<Guid, Guid> ProcessedMeasures = new Dictionary<Guid, Guid>(); // key crm ID, value esp ID
    Guid projectId;
    #endregion

    internal override TaskQueue.StatusFileDataUploading CurrentOperationStatus
    {
        get { return TaskQueue.StatusFileDataUploading.ESPRecalculation; }
    }

    protected override void ExecuteActivity(CodeActivityContext context)
    {
        Guid measureID;
        var measure = Measure.Get(context);
        var measureType = MeasureType.Get(context);


        ObjCommon = new Common(context);
        Service = ObjCommon.GetOrgService();
        _helper = new Helper(ObjCommon.GetOrgService(systemCall: true));


        if (string.IsNullOrEmpty(measureType) || !measureType.Equals("ESP") || Guid.Empty.Equals(measure.Id))
            return;

        if (measure != null)
        {
            measureID = measure.Id;
        }
        else { throw new Exception("You have no measure for recalculation"); }

        try
        {
            _helper.GetConfig();
            if (_helper.IsConfigValID())
            {
                #region Get currentAuthenticationResponse
                if (_helper.IsTokenNeedUpdate())
                {
                    AuthenticationResponse currentAuthenticationResponse;

                    url = _helper.GetAuthUrl();

                    using (var serviceRequest = new WebClient())
                    {
                        var responseBytes = serviceRequest.DownloadString(url);
                        currentAuthenticationResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(responseBytes);
                        if (currentAuthenticationResponse.ResponseHeader.StatusOk)
                        {
                            _helper.Token = currentAuthenticationResponse.ResponseHeader.AuthenticationTokenBytes;
                            _helper.LastRequestReceivedData = currentAuthenticationResponse.ResponseHeader.RequestReceivedDateTime;
                            _helper.UpdateConfig();
                        }
                        else { throw new AccessViolationException("Can't get authorization token using esp login details"); }
                    }
                }
                else
                {
                    _helper.LastRequestReceivedData = (DateTime)_helper.Config.Attributes["ddsm_requestreceiveddate"];
                    _helper.Token = Convert.FromBase64String(_helper.Config.Attributes["ddsm_esplasttoken"].ToString());
                }

                #endregion
                var measureData = GetMeasuresInfo(measureID);
                projectId = measureData.Entities[0].GetAttributeValue<EntityReference>("ddsm_projecttomeasureid").Id;

                if (measureData?.Entities.Count > 0)
                {
                    var calcRequest = BuildCalculationRequest(measureData);

                    CalculationResponse resultCalculationResponse = null;

                    url = _helper.GetCalculatioUrl();
                    var request = JsonConvert.SerializeObject(calcRequest);

                    var client = new WebClient { Headers = { [HttpRequestHeader.ContentType] = "application/json" } };
                    var response = client.UploadString(url, "POST", request);

                    resultCalculationResponse = JsonConvert.DeserializeObject<CalculationResponse>(response);

                    UpdateMeasures(resultCalculationResponse);
                }
                else { throw new Exception("The project does not have any smart measure for calculation"); }
            }
            else
            {
                throw new AccessViolationException("Can't get authorization token using esp login details. Please contact to your administrator.");
            }

            ObjCommon.TracingService.Trace(string.Join("\n", _processingLog.OrderBy(x => x).ToArray()));
            Result.Set(context, JsonConvert.SerializeObject(new { MeasureName = "", MeasureID = "", ErrorMsg = string.Join("\n", _processingLog.OrderBy(x => x).ToArray()) }));
            Complete.Set(context, true);
        }
        catch (Exception ex)
        {
            SetErrorStatusForDA();
            ObjCommon.TracingService.Trace("FATAL ERROR: " + ex.Message + string.Join("\n", _processingLog.OrderBy(x => x).ToArray()));
            Result.Set(context, JsonConvert.SerializeObject(new { MeasureName = "", MeasureID = "", ErrorMsg = string.Join("\n", _processingLog.OrderBy(x => x).ToArray()) }));
            Complete.Set(context, false);
        }
    }


#if WITH_TASKQ
    private void processCollectedData(Guid measureID)
    {
        var taskQueue = new TaskQueue(Service);

        taskQueue.Create(
           new DDSM_Task(TaskQueue.TaskEntity.ProgramInterval)
           {
               ProcessedItems0 = { measureID }
           },
       TaskQueue.TaskEntity.Measure, dataUploader: DataUploader
       );

        taskQueue.Create(
            new DDSM_Task(TaskQueue.TaskEntity.Project)
            {
                ProcessedItems0 = { projectId }
            },
        TaskQueue.TaskEntity.Project, dataUploader: DataUploader
        );
    }
#endif

    private CalculationRequest BuildCalculationRequest(EntityCollection measureData)
    {
        var aCalculationRequest = new CalculationRequest { RequestHeader = { AuthenticationTokenBytes = _helper.Token } };

        MappingData = JsonConvert.DeserializeObject<Dictionary<string, MappingItem>>(_helper.Config.Attributes["ddsm_espmapingdata"].ToString());

        foreach (var measureItem in measureData.Entities)
        {
            var calculatorCalculationRequest = new CalculatorCalculationRequest
            {
                CalculatorID = new Guid(measureItem.Attributes["ddsm_smartmeasureid"].ToString()),
                TargetDateTime = DateTime.UtcNow
            };

            //get mads from MT not from measure
            if (!measureItem.Attributes.ContainsKey("ddsm_measuretemplate1.ddsm_madfields") &&
                (measureItem.Attributes["ddsm_measuretemplate1.ddsm_madfields"] as AliasedValue).Value != null)
                throw new Exception("Can't get ddsm_measuretemplate1.ddsm_madfields. Are you sure that measure has Measure Template or MAD Fields of Measure Template is not null ?? ");


            var madsJSON = (measureItem.Attributes["ddsm_measuretemplate1.ddsm_madfields"] as AliasedValue).Value;
            var curentMad = JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(madsJSON.ToString());
            var data = GetMeasureData(curentMad, MappingData, measureItem.Attributes["ddsm_measureid"].ToString());


            // fill request from DB values
            foreach (var mad in curentMad)
            {
                var crmFiled = MappingData.FirstOrDefault(x => x.Key.ToLower().Equals(mad.Name.ToLower()));
                var variableValueRequest = CrmHelper.GenerateESPWSVariableValueRequest(crmFiled, mad, data, MainEntity);

                if (calculatorCalculationRequest.InputVariables.FirstOrDefault(x => x.Name.Equals(mad.Name)) == null)
                {
                    calculatorCalculationRequest.InputVariables.Add(variableValueRequest);
                }
            }
            if (curentMad.Count == calculatorCalculationRequest.InputVariables.Count)
            {
                aCalculationRequest.CalculatorCalculationRequestList.Add(calculatorCalculationRequest);
                ProcessedMeasures.Add(new Guid(measureItem.Attributes["ddsm_measureid"].ToString()),
                    new Guid(measureItem.Attributes["ddsm_smartmeasureid"].ToString()));
            }
            else
            {
                ObjCommon.TracingService.Trace("WARN: " + $"Mapping MAD are not successfully. Measure: {measureItem.Attributes["ddsm_name"]}. Current MAD count: {curentMad.Count}, mapped only: {calculatorCalculationRequest.InputVariables.Count}. Please check MAD mapping for current Smart Measure Template Or contact your administrator.");
            }
        }

        if (aCalculationRequest.CalculatorCalculationRequestList.Count == 0)
        {
            throw new Exception("Can't build Calculation request. No one mapped Smart Measure");
        }

        return aCalculationRequest;
    }

    private Entity GetMeasureData(List<ESPWSVariableDefinitionResponse> curentMad, Dictionary<string, MappingItem> _mappingData, string ID)
    {
        var sortedMadList = (from x in curentMad
                             join y in _mappingData on x.Name.ToLower() equals y.Key.ToLower()
                             select new KeyValuePair<string, string>(y.Value.AttrName.ToLower(), y.Value.Entity));

        var sortedMad = new Dictionary<string, string>();
        foreach (var item in sortedMadList)
        {
            if (!sortedMad.ContainsKey(item.Key))
            {
                sortedMad.Add(item.Key, item.Value);
            }
        }

        var queryMain = new QueryExpression
        {
            EntityName = MainEntity
        };

        var relatedEntity = new RelationshipQueryCollection();

        foreach (var key in sortedMad.Values.GroupBy(x => x))
        {
            //fill measure columns
            if (key.Key.Equals(MainEntity))
            {
                foreach (var item in sortedMad)
                {
                    if (item.Value == key.Key)
                        queryMain.ColumnSet.AddColumn(item.Key);
                }
            }//add relates
            else
            {
                if (key.Key.Equals("ddsm_project"))
                {
                    var relationship = new Relationship { SchemaName = "ddsm_ddsm_project_ddsm_measure" };

                    var query = new QueryExpression
                    {
                        EntityName = key.Key
                    };

                    foreach (var item in sortedMad)
                    {
                        if (item.Value == key.Key)
                            query.ColumnSet.AddColumn(item.Key);
                    }

                    relatedEntity.Add(relationship, query);
                }
                if (key.Key.Equals("account"))
                {
                    var relationship = new Relationship { SchemaName = "ddsm_account_ddsm_measure" };

                    var query = new QueryExpression
                    {
                        EntityName = key.Key
                    };

                    foreach (var item in sortedMad)
                    {
                        if (item.Value == key.Key)
                            query.ColumnSet.AddColumn(item.Key);
                    }
                    relatedEntity.Add(relationship, query);

                }
                if (key.Key.Equals("ddsm_site"))
                {
                    var relationship = new Relationship { SchemaName = "ddsm_ddsm_site_ddsm_measure" };

                    var query = new QueryExpression
                    {
                        EntityName = key.Key
                    };

                    foreach (var item in sortedMad)
                    {
                        if (item.Value == key.Key)
                            query.ColumnSet.AddColumn(item.Key);
                    }

                    relatedEntity.Add(relationship, query);
                }
            }
        }

        var request = new RetrieveRequest
        {
            ColumnSet = queryMain.ColumnSet,
            Target = new EntityReference { Id = new Guid(ID), LogicalName = MainEntity },
            RelatedEntitiesQuery = relatedEntity
        };

        var response = (RetrieveResponse)Service.Execute(request);

        var result = new Dictionary<string, object>();

        foreach (var relEnt in response.Entity.RelatedEntities)
        {
            result.Add(relEnt.Value.EntityName, relEnt.Value.Entities[0].Attributes);
        }
        return response.Entity;
    }

    private EntityCollection GetMeasuresInfo(Guid ID)
    {
        var calcTypeId = CrmHelper.GetMeasureCalcType();
        var expr = new QueryExpression
        {
            EntityName = MainEntity,
            ColumnSet = new ColumnSet("ddsm_measureid", "ddsm_smartmeasureid", "ddsm_madfields", "ddsm_name", "ddsm_projecttomeasureid"),
            Criteria = new FilterExpression
            {
                FilterOperator = LogicalOperator.And,
                Conditions = { new ConditionExpression("ddsm_measureid", ConditionOperator.Equal, ID),
                        new ConditionExpression("ddsm_calculationtype", ConditionOperator.Equal, calcTypeId) }
            }
        };

        expr.LinkEntities.Add(new LinkEntity(MainEntity, "ddsm_measuretemplate", "ddsm_measureselector", "ddsm_measuretemplateid", JoinOperator.Inner));
        expr.LinkEntities[0].Columns.AddColumns("ddsm_madfields");

        return Service.RetrieveMultiple(expr);
    }

    private void UpdateMeasures(CalculationResponse calcResult)
    {
        #region newImpl
        var QDDmappingData = JsonConvert.DeserializeObject<Dictionary<string, MappingItem>>(_helper.Config.Attributes["ddsm_espqddmappingdata"].ToString());
        var MADmappingData = JsonConvert.DeserializeObject<Dictionary<string, MappingItem>>(_helper.Config.Attributes["ddsm_espmapingdata"].ToString());

        var currentMSDData = new Dictionary<string, MSDField>();

        if (calcResult.ResponseHeader.StatusOk)
        {
            foreach (var measureCalculation in calcResult.ESPWSCalculatorCalculationList)
            {
                Entity measureForUpdate;

                var rrr = ProcessedMeasures.FirstOrDefault(x => x.Value.Equals(measureCalculation.ID));
                measureForUpdate = new Entity(MainEntity, rrr.Key);
                ProcessedMeasures.Remove(rrr.Key);

                // set mads
                foreach (var result in measureCalculation.ESPWSVariableCalculationList)
                {
                    var mapItem = MADmappingData.FirstOrDefault(x => x.Key.ToLower().Equals(result.Name.ToLower()));

                    if (mapItem.Value != null && mapItem.Key != null && mapItem.Value.Entity.Equals(MainEntity))
                    {
                        CrmHelper.SetAttributeValue(mapItem.Value, result, measureForUpdate);
                    }
                }

                //set qdds
                foreach (var result in measureCalculation.ESPWSVariableCalculationList)
                {
                    var mapItem = QDDmappingData.FirstOrDefault(x => x.Key.ToLower().Equals(result.Name.ToLower()));

                    if (mapItem.Value != null && mapItem.Key != null && mapItem.Value.Entity.Equals(MainEntity))
                    {
                        CrmHelper.SetAttributeValue(mapItem.Value, result, measureForUpdate);
                    }
                }

                measureForUpdate["ddsm_recalculationdate"] = measureForUpdate["ddsm_esptargetdate"] = DateTime.UtcNow;
                measureForUpdate["ddsm_espcalculationsuccessful"] = true;
                //measureForUpdate["ddsm_fundingcalculation"] = Guid.NewGuid().ToString();// new Random(6000000).Next();
                Service.Update(measureForUpdate);

                processCollectedData(measureForUpdate.Id);
            }
        }
        else
        {
            throw new Exception($"Upgrading measure impossible. Error code: {calcResult.ResponseHeader.ErrorCode}. \nError: {calcResult.GetFullESPError()}.");
        }
        #endregion
    }

}

