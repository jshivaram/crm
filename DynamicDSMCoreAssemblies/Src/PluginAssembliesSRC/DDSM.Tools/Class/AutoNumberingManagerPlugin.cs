﻿using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


public class AutoNumberingManagerPlugin : BasePlugin
{
    private readonly List<string> _listOfEntitiesForAutonumbering = new List<string>
    {
        "ddsm_project",
        "ddsm_financial",
        "ddsm_site",
        "ddsm_projectgroup",
        "account"
    };

    private Entity _targetEntity;
    private string _unsecure, _secure;
    private Entity _targetEntityPreImage;

    public AutoNumberingManagerPlugin(string unsecure, string secure)
    {
        _unsecure = unsecure;
        _secure = secure;
    }

    protected override void Run()
    {
        try
        {
            _targetEntity = (Entity)Target;
            if (Context.PreEntityImages != null && Context.PreEntityImages.Contains("Pre"))
            {
                _targetEntityPreImage = Context.PreEntityImages["Pre"];
            }
            var updatedTargetEntity = new Entity(_targetEntity.LogicalName, _targetEntity.Id);
            //Create autonumber only on create and only for entities from list
            if (Context?.MessageName?.ToUpper() == "CREATE" && _listOfEntitiesForAutonumbering.Contains(_targetEntity.LogicalName))
            {
                //Getting of autonumber entity record
                QueryExpression getAutonumberEntities = new QueryExpression("ddsm_autonumbering");
                getAutonumberEntities.ColumnSet = new ColumnSet("ddsm_prefix", "ddsm_counter", "ddsm_length", "ddsm_increment", "ddsm_lookupforanalyze");
                getAutonumberEntities.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, _targetEntity.LogicalName);
                var autonumberArray = Service.RetrieveMultiple(getAutonumberEntities).Entities;
                List<string> allLookups = autonumberArray.Select(el => el.GetAttributeValue<string>("ddsm_lookupforanalyze")).ToList();

                Entity autonumberEntity = null;
                if (autonumberArray.Count == 0)
                {
                    Tracer.Trace("Can't find autonumber entity with name " + _targetEntity.LogicalName);
                    return;
                }
                else if (autonumberArray.Count == 1)
                {
                    autonumberEntity = autonumberArray[0];
                }
                else if (autonumberArray.Count > 1)
                {
                    foreach (string lookup in allLookups)
                    {
                        if (string.IsNullOrEmpty(lookup))
                        {
                            continue;
                        }
                        var lookupValue = _targetEntity.GetAttributeValue<EntityReference>(lookup);
                        if (lookupValue != null)
                        {
                            autonumberEntity = autonumberArray.Where(el => el.GetAttributeValue<string>("ddsm_lookupforanalyze") == lookup).FirstOrDefault();
                            break;
                        }
                    }
                }
                if (autonumberEntity == null)
                {
                    Tracer.Trace("Can't find autonumber entity with needed lookup for autonumbering, check configuration of autonumber entity " 
                        + _targetEntity.LogicalName + " or lookups in entity! " + _targetEntity.Id.ToString());
                    return;
                }

                //Creation of number
                var prefix = autonumberEntity.GetAttributeValue<string>("ddsm_prefix");
                var counter = autonumberEntity.GetAttributeValue<decimal>("ddsm_counter");
                if (counter == 0)
                {
                    Tracer.Trace("Counter of " + _targetEntity.LogicalName + "cannot be 0!");
                    return;
                }
                var length = autonumberEntity.GetAttributeValue<int>("ddsm_length");
                if (length == 0)
                {
                    Tracer.Trace("Length of " + _targetEntity.LogicalName + "cannot be 0!");
                    return;
                }
                var increment = autonumberEntity.GetAttributeValue<int>("ddsm_increment");
                if (increment == 0)
                {
                    Tracer.Trace("Increment of " + _targetEntity.LogicalName + "cannot be 0!");
                    return;
                }
                var roundedCounter = decimal.Round(counter).ToString();

                if (roundedCounter.Length < length)
                {
                    roundedCounter = roundedCounter.PadLeft(length, '0');
                }
                var newAutonumber = prefix + roundedCounter;
                newAutonumber = Regex.Replace(newAutonumber, "[^a-zA-Z0-9]", "");

                //Set autonumber field in target not for updates
                _targetEntity["ddsm_autonumbering"] = newAutonumber;
                updatedTargetEntity["ddsm_autonumbering"] = newAutonumber;
                //Update counter
                autonumberEntity["ddsm_counter"] = counter + increment;
                Service.Update(autonumberEntity);
            }
            //Generation of names of entities
            string nameOfEntity = "";
            switch(_targetEntity.LogicalName)
            {
                case "ddsm_project":
                    nameOfEntity = GenerateNameForProject();
                    break;
                case "ddsm_projectgroup":
                    nameOfEntity = GenerateNameForProjectGroup();
                    break;
                case "ddsm_financial":
                    nameOfEntity = GenerateNameForFinancial();
                    break;
                case "ddsm_premise":
                    nameOfEntity = GenerateNameForPremise();
                    break;
                case "ddsm_site":
                    nameOfEntity = GenerateNameForSite();
                    break;
                case "account":
                    Tracer.Trace("Name of account generated in Primary Key Manager Plug-in!");
                    Service.Update(updatedTargetEntity);
                    return;
                default:
                    throw new Exception("Generation of names for this entity type is not assumed! " + _targetEntity.LogicalName);
            }
            updatedTargetEntity["ddsm_name"] = nameOfEntity;
            Service.Update(updatedTargetEntity);
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            Tracer.Trace("Timestamp: " + ex.Detail.Timestamp);
            Tracer.Trace("Code: " + ex.Detail.ErrorCode);
            Tracer.Trace("Message: " + ex.Detail.Message);
        }
        catch (Exception e)
        {
            Tracer.Trace("Error:" + e.Message);
        }
    }

    #region Name generators
    private string GenerateNameForProject()
    {
        List<string> arrayOfAttrValues = new List<string>();
        arrayOfAttrValues.Add(GetValueFromStringAttribute("ddsm_autonumbering"));
        arrayOfAttrValues.Add(GetValueFromStringAttribute("ddsm_programcodetype"));
        arrayOfAttrValues.Add(RetrieveStringValueFromReference("ddsm_parentsite", "ddsm_address1"));

        var arrayOfSeparators = new string[] { "-", "-" };
        return getNameOfEntity(arrayOfAttrValues.ToArray(), arrayOfSeparators);
    }

    private string GenerateNameForSite()
    {
        List<string> arrayOfAttrValues = new List<string>();
        arrayOfAttrValues.Add(GetValueFromStringAttribute("ddsm_address1"));
        arrayOfAttrValues.Add(GetValueFromStringAttribute("ddsm_address2"));
        arrayOfAttrValues.Add(GetValueFromStringAttribute("ddsm_city"));
        arrayOfAttrValues.Add(RetrieveStringValueFromReference("ddsm_parentaccount", "name"));

        var arrayOfSeparators = new string[] { " ", " ", "-" };
        return getNameOfEntity(arrayOfAttrValues.ToArray(), arrayOfSeparators);
    }

    private string GenerateNameForPremise()
    {
        List<string> arrayOfAttrValues = new List<string>();
        arrayOfAttrValues.Add(GetValueFromStringAttribute("ddsm_addressline1"));
        arrayOfAttrValues.Add(GetValueFromStringAttribute("ddsm_addressline2"));
        arrayOfAttrValues.Add(GetValueFromStringAttribute("ddsm_city"));
        arrayOfAttrValues.Add(GetValueFromStringAttribute("ddsm_stateorprovince"));
        arrayOfAttrValues.Add(GetValueFromStringAttribute("ddsm_postalcode"));

        var arrayOfSeparators = new string[] { " ", " ", " ", " " };
        return getNameOfEntity(arrayOfAttrValues.ToArray(), arrayOfSeparators);
    }

    private string GenerateNameForFinancial()
    {
        List<string> arrayOfAttrValues = new List<string>();
        var autonumbering = GetValueFromStringAttribute("ddsm_autonumbering");
        arrayOfAttrValues.Add(autonumbering);
        arrayOfAttrValues.Add(RetrieveStringValueFromReference("ddsm_financialtemplate", "ddsm_name"));

        if (autonumbering.StartsWith("PF"))
        {
            arrayOfAttrValues.Add(RetrieveStringValueFromReference("ddsm_parentproject", "ddsm_autonumbering"));
        }

        var arrayOfSeparators = new string[] { "-", "-"};
        return getNameOfEntity(arrayOfAttrValues.ToArray(), arrayOfSeparators);
    }

    private string GenerateNameForProjectGroup()
    {
        List<string> arrayOfAttrValues = new List<string>();
        arrayOfAttrValues.Add(GetValueFromStringAttribute("ddsm_autonumbering"));
        arrayOfAttrValues.Add(RetrieveStringValueFromReference("ddsm_projectgrouptemplates", "ddsm_name"));
        
        var arrayOfSeparators = new string[] { "-" };
        return getNameOfEntity(arrayOfAttrValues.ToArray(), arrayOfSeparators);
    }
    #endregion

    #region Helpers
    private string GetValueFromStringAttribute(string nameOfAttribute)
    {
        string fieldValue = "";
        if (_targetEntity.Attributes.Keys.Contains(nameOfAttribute))
        {
            fieldValue = _targetEntity.GetAttributeValue<string>(nameOfAttribute);
        }
        else if (_targetEntityPreImage != null && _targetEntityPreImage.Attributes.Keys.Contains(nameOfAttribute))
        {
            fieldValue = _targetEntityPreImage.GetAttributeValue<string>(nameOfAttribute);
        }
        return fieldValue ?? "";
    }

    private EntityReference GetValueFromEntityRefAttr(string nameOfAttribute)
    {
        EntityReference fieldValue = null;
        if (_targetEntity.Attributes.Keys.Contains(nameOfAttribute))
        {
            fieldValue = _targetEntity.GetAttributeValue<EntityReference>(nameOfAttribute);
        }
        else if (_targetEntityPreImage != null && _targetEntityPreImage.Attributes.Keys.Contains(nameOfAttribute))
        {
            fieldValue = _targetEntityPreImage.GetAttributeValue<EntityReference>(nameOfAttribute);
        }
        return fieldValue;
    }

    private string getNameOfEntity(string[] attributes, string[] separators)
    {
        string fieldValue = attributes[0];
        for (int i = 1; i < attributes.Length; i++)
        {
            if (!string.IsNullOrEmpty(attributes[i]))
            {
                attributes[i] = attributes[i].Trim(' ');
                fieldValue = string.Concat(fieldValue, separators[i-1], attributes[i]);
            }
        }
        return fieldValue ?? "";
    }

    private string RetrieveStringValueFromReference(string entityRefName, string entityRefAttr)
    {
        string fieldValue = null;
        var entityReference = GetValueFromEntityRefAttr(entityRefName);
        if (entityReference != null)
        {
            var entity = Service.Retrieve(entityReference.LogicalName, entityReference.Id, new ColumnSet(entityRefAttr));
            fieldValue = entity.GetAttributeValue<string>(entityRefAttr);
        }
        return fieldValue ?? "";
    }
    #endregion
}