﻿using DDSM.CommonProvider.JSON;
using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

public class BulkMoveNextStage : BaseCodeActivity
{
    private readonly Dictionary<string, string> _milestoneLookupDictionary = new Dictionary<string, string>
    {
        {"ddsm_project", "ddsm_projectid" },
        {"ddsm_financial", "ddsm_financialtomilestone" },
        {"ddsm_measure", "ddsm_parentmeasure" },
        {"ddsm_projectgroup", "ddsm_parentprojectgroup" }
    };

    private readonly Dictionary<string, string> _templateDictionary = new Dictionary<string, string>
    {
        {"ddsm_project", "ddsm_projecttemplate" }, 
        {"ddsm_financial", "ddsm_financialtemplate" },
        {"ddsm_measure", "ddsm_measuretemplate" },
        {"ddsm_projectgroup", "ddsm_projectgrouptemplate" }
    };

    private readonly Dictionary<string, string> _templateFieldDictionary = new Dictionary<string, string>
    {
        {"ddsm_project", "ddsm_projecttemplateid" },
        {"ddsm_financial", "ddsm_financialtemplate" },
        {"ddsm_measure", "ddsm_measureselector" },
        {"ddsm_projectgroup", "ddsm_projectgrouptemplates" }
    };

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        try
        {
            if (string.IsNullOrEmpty(this.UserInput.Get(executionContext)))
            {
                throw new Exception("User Input is null!");
            }
            var InputJson = this.UserInput.Get<string>(executionContext);
            var listofEntitiesModel = JsonConvert.DeserializeObject<OutputBulkMoveNextStageModel>(InputJson);
            if (!_milestoneLookupDictionary.ContainsKey(listofEntitiesModel.LogicalEntityName))
            {
                _objCommon.TracingService.Trace("Cannot find selected entity in lookup dictionary!");
                Result.Set(executionContext, "Cannot find selected entity in lookup dictionary!");
                Complete.Set(executionContext, false);
            }

            var entitiesQuery = new QueryExpression(listofEntitiesModel.LogicalEntityName);
            entitiesQuery.ColumnSet = new ColumnSet("ddsm_pendingmilestoneindex", _templateFieldDictionary[entitiesQuery.EntityName]);
            entitiesQuery.Criteria.AddCondition(entitiesQuery.EntityName + "id", ConditionOperator.In, listofEntitiesModel.ListOfIds);
            DataCollection<Entity> entities = _objCommon.GetOrgService().RetrieveMultiple(entitiesQuery).Entities;
            foreach (var entity in entities)
            {
                var template = entity.GetAttributeValue<EntityReference>(_templateFieldDictionary[entitiesQuery.EntityName]);
                if (template == null)
                {
                    _objCommon.TracingService.Trace("Entity with id " + entity.Id + " has no template!");
                    Result.Set(executionContext, "Entity with id " + entity.Id + " has no template!");
                    Complete.Set(executionContext, false);
                    return;
                }
            }

            //Validate the same milestone index and his value
            List<int> listOfMilesoneIndex = entities.Select(el => el.GetAttributeValue<int>("ddsm_pendingmilestoneindex")).Distinct().ToList();
            if (listOfMilesoneIndex.Count > 1)
            {
                _objCommon.TracingService.Trace("Selected entities don't have the same pending milestone index!");
                Result.Set(executionContext, "Selected entities don't have the same pending milestone index!");
                Complete.Set(executionContext, false);
                return;
            }
            if (listOfMilesoneIndex.First() < 1)
            {
                _objCommon.TracingService.Trace("Selected entities have 0 milestone index!");
                Result.Set(executionContext, "Selected entities have 0 milestone index!");
                Complete.Set(executionContext, false);
                return;
            }

            //Validate last milestone index
            List<EntityReference> listOfTemplateReferences = entities
                .Where(el => el.GetAttributeValue<EntityReference>(_templateFieldDictionary[entitiesQuery.EntityName]) != null)
                .Select(el => el.GetAttributeValue<EntityReference>(_templateFieldDictionary[entitiesQuery.EntityName]))
                .ToList();
            if (listOfTemplateReferences.Count < 1)
            {
                _objCommon.TracingService.Trace("Selected entities have no template!");
                Result.Set(executionContext, "Selected entities have no template!");
                Complete.Set(executionContext, false);
                return;
            }
            string[] listOfTemplateIds = listOfTemplateReferences.Select(el => el.Id.ToString()).Distinct().ToArray();
            var templateQuery = new QueryExpression(_templateDictionary[entitiesQuery.EntityName]);
            templateQuery.ColumnSet = new ColumnSet("ddsm_lastmilestoneindex", "ddsm_name");
            templateQuery.Criteria.AddCondition(_templateDictionary[entitiesQuery.EntityName] + "id", ConditionOperator.In, listOfTemplateIds);
            DataCollection<Entity> templates = _objCommon.GetOrgService().RetrieveMultiple(templateQuery).Entities;

            foreach (var template in templates)
            {
                var lastMilestoneIndex = template.GetAttributeValue<int>("ddsm_lastmilestoneindex");
                if (lastMilestoneIndex == listOfMilesoneIndex.First())
                {
                    var templateName = template.GetAttributeValue<string>("ddsm_name");
                    _objCommon.TracingService.Trace("Selected projects of " + templateName + " project template has last milestone index(" + lastMilestoneIndex + ")");
                    Result.Set(executionContext, "Selected projects of " + templateName + " project template has last milestone index(" + lastMilestoneIndex + ")");
                    Complete.Set(executionContext, false);
                    return;
                }
            }

            //Get milestones for validation status
            var milestonesQuery = new QueryExpression("ddsm_milestone");
            milestonesQuery.Criteria.AddCondition("ddsm_index", ConditionOperator.Equal, listOfMilesoneIndex.First());
            milestonesQuery.Criteria.AddCondition(_milestoneLookupDictionary[entitiesQuery.EntityName], ConditionOperator.In, listofEntitiesModel.ListOfIds);
            milestonesQuery.ColumnSet = new ColumnSet("ddsm_status", "ddsm_name", _milestoneLookupDictionary[entitiesQuery.EntityName]);
            DataCollection<Entity> milestones = _objCommon.GetOrgService().RetrieveMultiple(milestonesQuery).Entities;

            foreach (var milestone in milestones)
            {
                var milestoneStatus = milestone.GetAttributeValue<OptionSetValue>("ddsm_status");
                if (milestoneStatus.Value != 962080001)
                {
                    var entityName = milestone.FormattedValues[_milestoneLookupDictionary[entitiesQuery.EntityName]].ToString();
                    var milestoneName = milestone.GetAttributeValue<string>("ddsm_name");
                    _objCommon.TracingService.Trace("Milestone index of milestone " + milestoneName + " is not active!. Entity: " + entityName);
                    Result.Set(executionContext, "Milestone index of milestone " + milestoneName + " is not active!. Entity: " + entityName);
                    Complete.Set(executionContext, false);
                    return;
                }
            }

            //Get milestones for validation required fields
            var milestonesQuery2 = new QueryExpression("ddsm_milestone");
            milestonesQuery2.Criteria.AddCondition("ddsm_index", ConditionOperator.Equal, listOfMilesoneIndex.First() + 1);
            milestonesQuery2.Criteria.AddCondition(_milestoneLookupDictionary[entitiesQuery.EntityName], ConditionOperator.In, listofEntitiesModel.ListOfIds);
            milestonesQuery2.ColumnSet = new ColumnSet("ddsm_name", _milestoneLookupDictionary[entitiesQuery.EntityName]);
            DataCollection<Entity> milestones2 = _objCommon.GetOrgService().RetrieveMultiple(milestonesQuery2).Entities;
            
            var multipleRequest = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };
            foreach (var milestone in milestones2)
            {
                var milestoneRequest = new OrganizationRequest("ddsm_DDSMGetRequiredFields")
                {
                    Parameters = new ParameterCollection
                    {
                        {"MilestoneId", milestone.Id.ToString()}
                    }
                };
                multipleRequest.Requests.Add(milestoneRequest);
            }
            ExecuteMultipleResponse multipleResponse = (ExecuteMultipleResponse)_objCommon.GetOrgService().Execute(multipleRequest);
            var responses = multipleResponse.Responses;
            string logOfProjectMilestones = "";
            foreach( var resp in responses)
            {
                if (resp.Response.Results["Result"].ToString() != "[]")
                {
                    var milestoneArray = milestones2.ToArray();
                    var milestoneWithoutRequiredFields = milestoneArray[resp.RequestIndex];
                    var entityName = milestoneWithoutRequiredFields.FormattedValues[_milestoneLookupDictionary[entitiesQuery.EntityName]].ToString();
                    var milestoneName = milestoneWithoutRequiredFields.GetAttributeValue<string>("ddsm_name");
                    logOfProjectMilestones += "Project " + entityName + " has active milestone " + milestoneName + " with empty required fields \n";
                    _objCommon.TracingService.Trace("Project " + entityName + "has active milestone " + milestoneName + "with empty required fields");
                }
            }
            if (!string.IsNullOrEmpty(logOfProjectMilestones))
            {
                Result.Set(executionContext, logOfProjectMilestones);
                Complete.Set(executionContext, false);
                return;
            }

            //THERE WILL BE CODE FOR BULK MOVE AFTER ALL VALIDATION

            _objCommon.TracingService.Trace("Projects were successful moved to next stage");
            Result.Set(executionContext, "Projects were successful moved to next stage");
            Complete.Set(executionContext, true);
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            _objCommon.TracingService.Trace("Timestamp: " + ex.Detail.Timestamp);
            _objCommon.TracingService.Trace("Code: " + ex.Detail.ErrorCode);
            _objCommon.TracingService.Trace("Message: " + ex.Detail.Message);
        }
        catch (Exception e)
        {
            _objCommon.TracingService.Trace("Error:" + e.Message);
        }
    }
}

public class OutputBulkMoveNextStageModel
{
    public string[] ListOfIds { get; set; }
    public string LogicalEntityName { get; set; }
}