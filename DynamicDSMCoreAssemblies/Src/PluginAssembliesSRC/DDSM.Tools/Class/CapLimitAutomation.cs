﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.JSON;
using Microsoft.Xrm.Sdk.Query;

namespace DdsmTools.Class
{

    public sealed class CapLimitAutomation : CodeActivity
    {
        [Input("Target Entity")]
        public InArgument<string> TargetEntity { get; set; }
        [Output("Reply")]
        public OutArgument<string> Reply { get; set; }

        ITracingService _tracer;
        IWorkflowContext _context;
        IOrganizationServiceFactory _serviceFactory;
        IOrganizationService _service;

        protected override void Execute(CodeActivityContext executionContext)
        {
            _tracer = executionContext.GetExtension<ITracingService>();
            _context = executionContext.GetExtension<IWorkflowContext>();
            _serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            _service = _serviceFactory.CreateOrganizationService(_context.UserId);

            try
            {
                string requestJson = TargetEntity.Get<string>(executionContext);
                CapLimitInput input = JsonConvert.DeserializeObject<CapLimitInput>(requestJson);

                CapLimitReply reply = new CapLimitReply { IsExecuted = true };

                if (input.Name == "ddsm_project")
                    reply = CheckProject(input);
                if (input.Name == "ddsm_financial")
                    reply = CheckFinancial(input);


                string responseJson = JsonConvert.SerializeObject(reply);
                Reply.Set(executionContext, responseJson);
                return;
            }
            catch (Exception e)
            {
                string responseJson = JsonConvert.SerializeObject(new CapLimitReply { IsExecuted = false, IsLimitReached = false });

                Reply.Set(executionContext, responseJson);
                throw new InvalidPluginExecutionException(e.Message);
            }
            
        }

        private CapLimitReply CheckFinancial(CapLimitInput input)
        {
            string entityInfo = @"<fetch>
                  <entity name='ddsm_project' >
                    <attribute name='ddsm_startdate' />
                    <attribute name='ddsm_programoffering' />
                    <attribute name='ddsm_parentsiteid' />
                    <attribute name='ddsm_accountid' />
                    <filter>
                      <condition attribute='ddsm_projectid' operator='eq' value='" + input.ProjectId + @"' />
                    </filter>
                  </entity>
                </fetch>";

            EntityCollection infoResult = _service.RetrieveMultiple(new FetchExpression(entityInfo));
            Entity project = infoResult[0];
            DateTime startDate = (DateTime)project.Attributes["ddsm_startdate"];
            Guid programOfferingId = ((EntityReference)project.Attributes["ddsm_programoffering"]).Id;
            Guid parentSiteId = ((EntityReference)project.Attributes["ddsm_parentsiteid"]).Id;
            Guid parentAccountId = ((EntityReference)project.Attributes["ddsm_accountid"]).Id;

            string displayName = input.Name.Substring(input.Name.IndexOf("_") + 1);

            //------------ Check customer limit -------------------------

            string tesrgetEntityJson = "{&quot;Label&quot;:&quot;account&quot;,&quot;LogicalName&quot;:&quot;account&quot;}";

            string checkAccount = @"<fetch>
                      <entity name='ddsm_capcalculation' >
                        <attribute name='ddsm_year' />
                        <attribute name='ddsm_capsum' />
                        <attribute name='ddsm_targetentity' />
                        <filter>
                          <condition attribute='ddsm_targetentity' operator='eq' value='" + tesrgetEntityJson + @"' />
                          <condition attribute='ddsm_year' operator='eq' value='" + (startDate.Year + 962077990) + @"' />
                          <condition entityname='ddsm_programoffering' attribute='ddsm_programofferingid' operator='eq' value='" + programOfferingId + @"' />
                        </filter>
                        <link-entity name='ddsm_programoffering' from='ddsm_programofferingid' to='ddsm_programoffering' >
                          <attribute name='ddsm_programofferingid' />
                        </link-entity>
                      </entity>
                    </fetch>";

            EntityCollection result = _service.RetrieveMultiple(new FetchExpression(checkAccount));

            CapLimitReply reply = new CapLimitReply();
            reply.IsExecuted = true;

            if (result.Entities.Count == 1)
            {
                decimal capSum = ((Money)result.Entities[0].Attributes["ddsm_capsum"]).Value;

                string entitiesFetch = @"<fetch aggregate='true' >
                      <entity name='account' >
                        <filter type='and' >
                          <condition attribute='accountid' operator='eq' value='" + parentAccountId + @"' />
                        </filter>
                        <link-entity name='ddsm_project' from='ddsm_accountid' to='accountid' >
                          <attribute name='ddsm_totalincentive' alias='totalincentive' aggregate='sum' />
                          <filter>
                            <condition attribute='ddsm_programoffering' operator='eq' value='" + programOfferingId + @"' />
                            <condition attribute='ddsm_startdate' operator='this-year' />
                          </filter>
                        </link-entity>
                      </entity>
                    </fetch>";

                EntityCollection entitiesList = _service.RetrieveMultiple(new FetchExpression(entitiesFetch));
                Entity aggregateResult = entitiesList[0];
                decimal totalIncentiveSum = ((Money)((AliasedValue)aggregateResult.Attributes["totalincentive"]).Value).Value;

                if (totalIncentiveSum > capSum)
                    reply.IsCustomerLimitReached = true;
            }
            else if (result.Entities.Count > 1)
            {
                reply.HasConflict = true;
            }

            //------------ Check site limit -------------------------
            tesrgetEntityJson = "{&quot;Label&quot;:&quot;site&quot;,&quot;LogicalName&quot;:&quot;ddsm_site&quot;}";

            string checkSite = @"<fetch>
                      <entity name='ddsm_capcalculation' >
                        <attribute name='ddsm_year' />
                        <attribute name='ddsm_capsum' />
                        <attribute name='ddsm_targetentity' />
                        <filter>
                          <condition attribute='ddsm_targetentity' operator='eq' value='" + tesrgetEntityJson + @"' />
                          <condition attribute='ddsm_year' operator='eq' value='" + (startDate.Year + 962077990) + @"' />
                          <condition entityname='ddsm_programoffering' attribute='ddsm_programofferingid' operator='eq' value='" + programOfferingId + @"' />
                        </filter>
                        <link-entity name='ddsm_programoffering' from='ddsm_programofferingid' to='ddsm_programoffering' >
                          <attribute name='ddsm_programofferingid' />
                        </link-entity>
                      </entity>
                    </fetch>";

            result = _service.RetrieveMultiple(new FetchExpression(checkSite));

            if (result.Entities.Count == 1)
            {
                decimal capSum = ((Money)result.Entities[0].Attributes["ddsm_capsum"]).Value;

                string entitiesFetch = @"<fetch aggregate='true' >
                      <entity name='account' >
                        <filter type='and' >
                          <condition attribute='accountid' operator='eq' value='" + parentAccountId + @"' />
                        </filter>
                        <link-entity name='ddsm_project' from='ddsm_accountid' to='accountid' >
                          <attribute name='ddsm_totalincentive' alias='totalincentive' aggregate='sum' />
                          <filter>
                            <condition attribute='ddsm_programoffering' operator='eq' value='" + programOfferingId + @"' />
                            <condition attribute='ddsm_parentsiteid' operator='eq' value='" + parentSiteId + @"' />
                            <condition attribute='ddsm_startdate' operator='this-year' />
                          </filter>
                        </link-entity>
                      </entity>
                    </fetch>";

                EntityCollection entitiesList = _service.RetrieveMultiple(new FetchExpression(entitiesFetch));
                Entity aggregateResult = entitiesList[0];
                decimal totalIncentiveSum = ((Money)((AliasedValue)aggregateResult.Attributes["totalincentive"]).Value).Value;

                if (totalIncentiveSum > capSum)
                    reply.IsSiteLimitReached = true;
            }
            else if (result.Entities.Count > 1)
            {
                reply.HasConflict = true;
            }

            return reply;
        }

        private CapLimitReply CheckProject(CapLimitInput input)
        {
            string entityInfo = @"<fetch>
                  <entity name='ddsm_project' >
                    <attribute name='ddsm_startdate' />
                    <attribute name='ddsm_programoffering' />
                    <attribute name='ddsm_parentsiteid' />
                    <attribute name='ddsm_accountid' />
                    <filter>
                      <condition attribute='ddsm_projectid' operator='eq' value='" + input.ProjectId + @"' />
                    </filter>
                  </entity>
                </fetch>";

            EntityCollection infoResult = _service.RetrieveMultiple(new FetchExpression(entityInfo));
            Entity project = infoResult[0];
            DateTime startDate = (DateTime)project.Attributes["ddsm_startdate"];
            Guid programOfferingId = ((EntityReference)project.Attributes["ddsm_programoffering"]).Id;
            Guid parentSiteId = ((EntityReference)project.Attributes["ddsm_parentsiteid"]).Id;
            Guid parentAccountId = ((EntityReference)project.Attributes["ddsm_accountid"]).Id;

            string displayName = input.Name.Substring(input.Name.IndexOf("_") + 1);
            string tesrgetEntityJson = "{&quot;Label&quot;:&quot;" + displayName + "&quot;,&quot;LogicalName&quot;:&quot;" + input.Name + "&quot;}";

            string checkFetch = @"<fetch>
                      <entity name='ddsm_capcalculation' >
                        <attribute name='ddsm_year' />
                        <attribute name='ddsm_capsum' />
                        <attribute name='ddsm_targetentity' />
                        <filter>
                          <condition attribute='ddsm_targetentity' operator='eq' value='" + tesrgetEntityJson + @"' />
                          <condition attribute='ddsm_year' operator='eq' value='" + (startDate.Year + 962077990) + @"' />
                          <condition entityname='ddsm_programoffering' attribute='ddsm_programofferingid' operator='eq' value='" + programOfferingId + @"' />
                        </filter>
                        <link-entity name='ddsm_programoffering' from='ddsm_programofferingid' to='ddsm_programoffering' >
                          <attribute name='ddsm_programofferingid' />
                        </link-entity>
                      </entity>
                    </fetch>";

            EntityCollection result = _service.RetrieveMultiple(new FetchExpression(checkFetch));

            CapLimitReply reply = new CapLimitReply();
            reply.IsExecuted = true;

            if (result.Entities.Count == 1)
            {
                decimal capSum = ((Money)result.Entities[0].Attributes["ddsm_capsum"]).Value;

                string entitiesFetch = @"<fetch aggregate='true' >
                      <entity name='account' >
                        <filter type='and' >
                          <condition attribute='accountid' operator='eq' value='" + parentAccountId + @"' />
                        </filter>
                        <link-entity name='ddsm_project' from='ddsm_accountid' to='accountid' >
                          <attribute name='ddsm_totalincentive' alias='totalincentive' aggregate='sum' />
                          <filter>
                            <condition attribute='ddsm_programoffering' operator='eq' value='" + programOfferingId + @"' />
                            <condition attribute='ddsm_startdate' operator='this-year' />
                          </filter>
                        </link-entity>
                      </entity>
                    </fetch>";

                EntityCollection entitiesList = _service.RetrieveMultiple(new FetchExpression(entitiesFetch));
                Entity aggregateResult = entitiesList[0];
                decimal totalIncentiveSum = ((Money)((AliasedValue)aggregateResult.Attributes["totalincentive"]).Value).Value;

                if (totalIncentiveSum > capSum)
                    reply.IsLimitReached = true;
            }
            else if (result.Entities.Count > 1)
            {
                reply.HasConflict = true;
            }

            return reply;
        }
    }

    class CapLimitReply
    {
        public bool IsLimitReached { get; set; }
        public bool IsCustomerLimitReached { get; set; }
        public bool IsSiteLimitReached { get; set; }
        public bool IsExecuted { get; set; }
        public bool HasConflict { get; set; }
    }

}
