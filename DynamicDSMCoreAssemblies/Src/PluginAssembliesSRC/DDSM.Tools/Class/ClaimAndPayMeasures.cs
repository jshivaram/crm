﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.Activities;
using DDSM.CommonProvider.JSON;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider.src;


public class ClaimAndPayMeasures : BaseCodeActivity
{

    

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        try
        {
            var data = JsonConvert.DeserializeObject<ClaimMeasureModel>(UserInput.Get<string>(executionContext));

            #region Validation
            if (data.measuresId == null || data.measuresId.Length < 1)
            {
                _objCommon.TracingService.Trace("No one measure id");
                Result.Set(executionContext, "No one measure selected. Please select at least one");
                return;
            }

            _objCommon.TracingService.Trace("Data is OK");
            #endregion

            #region Claim Measure Only
            if (data.message.Equals(Message.CLAIM))
            {
                var measures = RetriveMeasures(new ColumnSet("ddsm_phase", "ddsm_parentfinancialid"), data);
                if (measures == null)
                {
                    throw new Exception("Measures with selected identifiers does not exist!");
                }
            #region Validation
               var measureWithParentFinancial = measures.Where(measure => {
                   var parentFinancial = measure.GetAttributeValue<EntityReference>("ddsm_parentfinancialid");
                   return parentFinancial != null;

                }).ToList();
                if (measureWithParentFinancial.Count > 0)
                {
                    _objCommon.TracingService.Trace("Finded measure with parent financial");
                    Result.Set(executionContext, "You chose measure(s) with financial. Please choose another one");
                    return;
                }

                var measureWithBadStatus = measures.Where(measure => {

                    var phase = measure.GetAttributeValue<string>("ddsm_phase");
                    if (!string.IsNullOrEmpty(phase))
                    {
                        return phase.Contains("Completed") || phase.Contains("Evaluated") || phase.Contains("Claimed");
                    }
                    return false;
                }).ToList();
                if (measureWithBadStatus.Count > 0)
                {
                    _objCommon.TracingService.Trace("Finded measure with not allowed phase");
                    Result.Set(executionContext, "You chose measure(s) with phase \"Completed\" or \"Evaluated\" or \"Claimed\". Please choose another one");
                    return;
                }
                #endregion

                var measuresForUpdate = ClaimMeasures(measures);
                UpdateCollection(measuresForUpdate);
            }
            #endregion

            #region Claim Measure and create Financial
            else if (data.message.Equals(Message.CLAIM_AND_CREATE_FINANCIAL))
            {
                var measures = RetriveMeasures(new ColumnSet("ddsm_parentfinancialid", "ddsm_phase", "ddsm_parentproject"), data);
                if (measures == null)
                {
                    throw new Exception("Measures with selected identifiers does not exist!");
                }

                #region Validation

                var measureWithParentFinancial = measures.Where(measure => 
                {
                    var parentFinancial = measure.GetAttributeValue<EntityReference>("ddsm_parentfinancialid");
                    return parentFinancial != null;
                }
                ).ToList();
                if (measureWithParentFinancial.Count > 0)
                {
                    _objCommon.TracingService.Trace("Finded measure with parent financial");
                    Result.Set(executionContext, "You chose measure(s) with financial. Please choose another one");
                    return;
                }

                var measureWithBadStatus = measures.Where(measure => 
                {
                    var phase = measure.GetAttributeValue<string>("ddsm_phase");
                    if(!string.IsNullOrEmpty(phase))
                    {
                        return phase.Contains("Completed") || phase.Contains("Evaluated");
                    }
                    return false;
                }
                ).ToList();
                if (measureWithBadStatus.Count > 0)
                {
                    _objCommon.TracingService.Trace("Finded measure with not allowed phase");
                    Result.Set(executionContext, "You chose measure(s) with phase \"Completed\" or \"Evaluated\". Please choose another one");
                    return;
                }

                if (data.financialtemplate == string.Empty)
                {
                    _objCommon.TracingService.Trace("No default financial template!");
                    Result.Set(executionContext, "You did not chose financial template. Please choose it");
                    return;
                }
                var projectReference = measures[0].GetAttributeValue<EntityReference>("ddsm_parentproject");
                if (projectReference == null)
                {
                    _objCommon.TracingService.Trace("Measure with id " + measures[0].Id + " does not contains parent project!");
                    Result.Set(executionContext, "Inner error. Parent project was not received");
                    return;
                }
                #endregion

                var measuresForUpdate = CreateFinancialAndUpdateMeasures(measures.ToList(), projectReference, Guid.Parse(data.financialtemplate), executionContext);

                if (measuresForUpdate.Count == 1)
                {
                    _objCommon.GetOrgService().Update(measuresForUpdate[0]);
                }
                else
                {
                    UpdateCollection(measuresForUpdate);
                }
            }
            #endregion

            Result.Set(executionContext, "Success");
        }
        catch (Exception e)
        {
            _objCommon.TracingService.Trace("Error in method execute: " + e);
            Result.Set(executionContext, e.Message + e.StackTrace);
        }
    }

    private DataCollection<Entity> RetriveMeasures(ColumnSet columns, ClaimMeasureModel data)
    {
        try
        {
            QueryExpression query = new QueryExpression
                {
                    ColumnSet = columns,
                    EntityName = "ddsm_measure",
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression("ddsm_measureid", ConditionOperator.In, data.measuresId)
                        }
                    }
                };
            var measures = _objCommon.GetOrgService().RetrieveMultiple(query).Entities;
            return measures;
        }
        catch (Exception e)
        {
            _objCommon.TracingService.Trace("Error when retrive measures: " + e.Message);
            throw e;
        }
    }

    private List<Entity> ClaimMeasures(DataCollection<Entity> measures)
    {
        var measuresForUpdate = new List<Entity>();
        try
        {
            foreach (var measure in measures)
            {
                measure["ddsm_phase"] = "Claimed";
            }
            return measures.ToList();
        }
        catch (Exception e)
        {
            _objCommon.TracingService.Trace("Error when claim measures: " + e.Message);
            throw e;
        }
    }

    private List<Entity> CreateFinancialAndUpdateMeasures(List<Entity> measures, EntityReference projectReference, Guid defaultFinancialTemplateId, CodeActivityContext context)
    {
        var result = new List<Entity>();
        try
        {
            var financial = new Entity("ddsm_financial");
            financial["ddsm_name"] = projectReference.Name;
            financial["ddsm_parentproject"] = projectReference;
            var financialTemplateEntityReference = new EntityReference("ddsm_financialtemplate", defaultFinancialTemplateId);
            financial["ddsm_financialtemplate"] = financialTemplateEntityReference;

            var financialId = _objCommon.GetOrgService().Create(financial);

            _objCommon.TracingService.Trace("Created financial with id: " + financialId.ToString());

            var phase = "Claimed";

            foreach (var measure in measures)
            {
                var financialRecord = _objCommon.GetOrgService().Retrieve(financial.LogicalName, financialId, new ColumnSet(true));
                if (financialRecord.Id == Guid.Empty)
                {
                    throw new Exception("Can't get Financial by id");
                }

                var measureForUpdate = new Entity(measure.LogicalName, measure.Id)
                    {
                        Attributes =
                        {
                            { "ddsm_parentfinancialid", financialRecord.ToEntityReference()},
                            { "ddsm_phase", phase }
                        }
                    };

                result.Add(measureForUpdate);
            }

            return result;
        }
        catch (Exception e)
        {
            _objCommon.TracingService.Trace("Error when create financial: " + e.Message);
            throw e;
        }
    }

    private void UpdateCollection(List<Entity> entitiesToUpdate)
    {
        try
        {
            var multipleRequest = new ExecuteMultipleRequest()
                {
                    Settings = new ExecuteMultipleSettings()
                    {
                        ContinueOnError = true,
                        ReturnResponses = true
                    },
                    Requests = new OrganizationRequestCollection()
                };

            multipleRequest.Requests.AddRange(
                entitiesToUpdate.Select(entity => new UpdateRequest { Target = entity })
            );

            ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)_objCommon.GetOrgService().Execute(multipleRequest);
            if (responseWithResults.IsFaulted)
            {
                _objCommon.TracingService.Trace("Update of Entity failed! Check it.");
                _objCommon.TracingService.Trace("Log: " + responseWithResults.Responses[0].Fault.Message);
                throw new Exception("Update of Entity failed. " + responseWithResults.Responses[0].Fault.Message);
            }
            else
            {
                _objCommon.TracingService.Trace(multipleRequest.Requests.Count + " updates was done. Entity was updated.");
            }
        }
        catch (Exception e)
        {
            _objCommon.TracingService.Trace("Error when update collection. Error: " + e.Message);
            throw e;
        }
    }
}

public struct ClaimMeasureModel
{
    public string[] measuresId { get; set; }
    public Message message { get; set; }
    public string financialtemplate { get; set; }

}

public enum Message
{
    CLAIM = 0,
    CLAIM_AND_CREATE_FINANCIAL = 1
}