﻿using DDSM.CommonProvider.JSON;
using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Xml.Linq;

public class GetDataForMeasureClaim : BaseCodeActivity
{
    [RequiredArgument]
    [Input("Parent Project ID")]
    public InArgument<string> ParentProjectId { get; set; }

    [RequiredArgument]
    [Input("View Name")]
    public InArgument<string> ViewName { get; set; }

    [Output("MeasuresList JSON")]
    public OutArgument<string> MeasuresList { get; set; }

    [Output("View columns")]
    public OutArgument<string> ViewColumns { get; set; }

    [Output("View column titles")]
    public OutArgument<string> ColumnDisplayNames { get; set; }

    [Output("Financial Template JSON")]
    public OutArgument<string> FinancialTemplatesList { get; set; }

    [Output("Default Financial Template Id")]
    public OutArgument<string> DefaultFinancialTemplateId { get; set; }


   

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        try
        {
            if (string.IsNullOrEmpty(this.ParentProjectId.Get(executionContext)))
            {
                throw new Exception("Parent Project Id is null!");
            }
            var parentProjectId = new Guid(this.ParentProjectId.Get(executionContext));

            if (string.IsNullOrEmpty(this.ViewName.Get(executionContext)))
            {
                throw new Exception("View Name is null!");
            }
            var viewName = this.ViewName.Get(executionContext);

            var viewQuery = new QueryExpression("savedquery")
            {
                ColumnSet = new ColumnSet("fetchxml")
            };
            viewQuery.Criteria.AddCondition(new ConditionExpression("name", ConditionOperator.Equal, viewName));

            var views = _objCommon.GetOrgService().RetrieveMultiple(viewQuery).Entities;
            if (views.Count == 0)
            {
                throw new Exception("View " + viewName + " does not exist!");
            }
            if (views.Count > 1)
            {
                throw new Exception("There are " + views.Count + " views with this name!");
            }
            var fetchXml = views[0].GetAttributeValue<string>("fetchxml");
            if (fetchXml == null)
            {
                throw new Exception("There is no fetch Xml");
            }
            List<string> propsNames;
            propsNames = GetPropNamesFromFetchXml(fetchXml);

            var measuresQuery = new QueryExpression("ddsm_measure")
            {
                ColumnSet = new ColumnSet(propsNames.ToArray())
            };
            measuresQuery.Criteria.AddCondition("ddsm_parentproject", ConditionOperator.Equal, new Guid(parentProjectId.ToString()));
            measuresQuery.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);

            var measures = _objCommon.GetOrgService().RetrieveMultiple(measuresQuery).Entities;
            if (measures.Count == 0)
            {
                Complete.Set(executionContext, false);
                MeasuresList.Set(executionContext, "");
                ViewColumns.Set(executionContext, "");
                ColumnDisplayNames.Set(executionContext, "");
                FinancialTemplatesList.Set(executionContext, "");
                Result.Set(executionContext, "");
                DefaultFinancialTemplateId.Set(executionContext, "");
                return;
            }
            
            var columnDisplayNames = new List<string>();
            foreach (string logicalName in propsNames)
            {
                var attributeRequest = new RetrieveAttributeRequest
                    {
                        EntityLogicalName = "ddsm_measure",
                        LogicalName = logicalName,
                        RetrieveAsIfPublished = true
                    };

                var attributeResponse = (RetrieveAttributeResponse)_objCommon.GetOrgService(systemCall: true).Execute(attributeRequest);
                if (attributeResponse != null)
                {
                    string label = attributeResponse.AttributeMetadata.DisplayName?.LocalizedLabels?.FirstOrDefault()?.Label;
                    if (!string.IsNullOrEmpty(label))
                    {
                        columnDisplayNames.Add(label);
                    }
                    else
                    {
                        throw new Exception("Attribute " + logicalName + " has no Display name!");
                    }
                }
                else
                {
                    throw new Exception("Attribute " + logicalName + " has no Attribute Metadata!");
                }
            }

            var financialTemplatesQuery = new QueryExpression("ddsm_financialtemplate")
            {
                ColumnSet = new ColumnSet("ddsm_name")
            };
            financialTemplatesQuery.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);

            var dateTimeNow = DateTime.Now;
            string dateTimeNowYear = dateTimeNow.Year.ToString();
            var financialTemplates = _objCommon.GetOrgService().RetrieveMultiple(financialTemplatesQuery).Entities;
            string financialTemplatesJSON;
            if (financialTemplates.Count != 0)
            {
                financialTemplatesJSON = JsonConvert.SerializeObject(financialTemplates);
            }
            else
            {
                throw new Exception("There is no financial templates in the system in" + dateTimeNowYear);
            }

            var measuresJSON = JsonConvert.SerializeObject(measures);
            string columnList = string.Join(",", propsNames.Select(p => p));
            string titleList = string.Join(",,", columnDisplayNames.Select(p => p));

            var project = _objCommon.GetOrgService().Retrieve("ddsm_project", parentProjectId, new ColumnSet("ddsm_projecttemplateid"));
            if (project == null)
            {
                throw new Exception("Project with id " + parentProjectId + " does not exist");
            }
            var projectTemplateReference = project.GetAttributeValue<EntityReference>("ddsm_projecttemplateid");
            if (projectTemplateReference == null)
            {
                throw new Exception("Project with id " + parentProjectId + " does not have Project Template!");
            }

            var projectTemplate = _objCommon.GetOrgService().Retrieve("ddsm_projecttemplate", projectTemplateReference.Id, new ColumnSet("ddsm_defaultfinancialtemplateid"));
            if (projectTemplate == null)
            {
                throw new Exception("Project template with id " + projectTemplateReference.Id + " does not exist!");
            }

            var defaultFinancialTemplateId = projectTemplate.GetAttributeValue<EntityReference>("ddsm_defaultfinancialtemplateid");
            if (defaultFinancialTemplateId != null)
            {
                DefaultFinancialTemplateId.Set(executionContext, defaultFinancialTemplateId.Id.ToString());
            }
            else
            {
                DefaultFinancialTemplateId.Set(executionContext, "");
            }
            Complete.Set(executionContext, true);
            MeasuresList.Set(executionContext, measuresJSON);
            ViewColumns.Set(executionContext, columnList);
            ColumnDisplayNames.Set(executionContext, titleList);
            FinancialTemplatesList.Set(executionContext, financialTemplatesJSON);
            Result.Set(executionContext, "");
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            _objCommon.TracingService.Trace("Timestamp: " + ex.Detail.Timestamp);
            _objCommon.TracingService.Trace("Code: " + ex.Detail.ErrorCode);
            _objCommon.TracingService.Trace("Message: " + ex.Detail.Message);
        }
        catch (Exception e)
        {
            _objCommon.TracingService.Trace("Error:" + e.Message);
        }
    }

    private List<string> GetPropNamesFromFetchXml(string fetchXml)
    {
        XElement xelem = XElement.Parse(fetchXml);
        XElement firstEntity = xelem.Elements("entity").FirstOrDefault();
        if (firstEntity == null)
        {
            return new List<string>();
        }
        IEnumerable<XElement> attributes = firstEntity.Elements("attribute");
        List<string> propsNames = new List<string>();
        XElement order = firstEntity.Element("order");

        string orderEntityFieldName = order?.Attribute("attribute")?.Value;
        propsNames.Add(orderEntityFieldName);
        propsNames.AddRange(attributes.Select(a => a.Attribute("name")?.Value)
            .Distinct()
            .ToList());
        propsNames = propsNames.Distinct().ToList();
        return propsNames.ToList();
    }
}
