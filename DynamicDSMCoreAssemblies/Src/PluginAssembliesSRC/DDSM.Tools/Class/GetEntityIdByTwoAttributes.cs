﻿using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using DDSM.CommonProvider.Model.Enum;


public class GetEntityIdByTwoAttributes : BaseCodeActivity
{
    [RequiredArgument]
    [Input("Entity Logical Name")]
    public InArgument<string> EntityLogicalName { get; set; }

    [RequiredArgument]
    [Input("First Attribute Name")]
    public InArgument<string> FirstAttributeName { get; set; }
    
    [Input("First Attribute Value")]
    public InArgument<string> FirstAttributeValue { get; set; }

    [Input("Second Attribute Name")]
    public InArgument<string> SecondAttributeName { get; set; }

    [Input("Second Attribute Value")]
    public InArgument<string> SecondAttributeValue { get; set; }


   


    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        try
        {
            if (string.IsNullOrEmpty(EntityLogicalName.Get(executionContext)))
            {
                throw new Exception("Entity Logical Name is null!");
            }
            string entityLogicalName = EntityLogicalName.Get(executionContext);

            if (string.IsNullOrEmpty(FirstAttributeName.Get(executionContext)))
            {
                throw new Exception("First Attribute Name is null!");
            }
            string firstAttributeName = FirstAttributeName.Get(executionContext);
            string firstAttributeValue = FirstAttributeValue.Get(executionContext);

            var firstAttributeMetadata = GetAttributeMetadata(entityLogicalName, firstAttributeName);
            var firstAttributeType = firstAttributeMetadata.AttributeType;
            var firstCondition = GetQueryCondition(firstAttributeName, firstAttributeValue, firstAttributeType);
            ConditionExpression secondCondition = new ConditionExpression();
            if (!string.IsNullOrEmpty(SecondAttributeName.Get(executionContext)))
            {
                string secondAttributeName = SecondAttributeName.Get(executionContext); 
                string secondAttributeValue = null;
                var secondAttributeMetadata = GetAttributeMetadata(entityLogicalName, secondAttributeName);
                var secondAttributeType = secondAttributeMetadata.AttributeType;
                if (!string.IsNullOrEmpty(SecondAttributeValue.Get(executionContext)))
                {
                    secondAttributeValue = SecondAttributeValue.Get(executionContext);
                }
                secondCondition = GetQueryCondition(secondAttributeName, secondAttributeValue, secondAttributeType);
            }

            QueryExpression query = new QueryExpression(entityLogicalName);
            query.Criteria.AddCondition(firstCondition);
            if (secondCondition.AttributeName != null)
            {
                query.Criteria.AddCondition(secondCondition);
            }
            var entities = _objCommon.GetOrgService().RetrieveMultiple(query).Entities;
            if (entities.Count > 0)
            {
                _objCommon.TracingService.Trace("Successfully get id " + entities[0].Id.ToString() + " of entity" + entityLogicalName);
                Result.Set(executionContext, entities[0].Id.ToString());
            }
            else
            {
                throw new Exception("There are no entities with selected conditions!");
            }
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            _objCommon.TracingService.Trace("Timestamp: " + ex.Detail.Timestamp);
            _objCommon.TracingService.Trace("Code: " + ex.Detail.ErrorCode);
            _objCommon.TracingService.Trace("Message: " + ex.Detail.Message);
        }
        catch (Exception e)
        {
            _objCommon.TracingService.Trace("Error:" + e.Message);
        }
    }

    public AttributeMetadata GetAttributeMetadata(string entityName, string fieldName)
    {
        var attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = fieldName,
                RetrieveAsIfPublished = true
            };

        var attributeResponse = (RetrieveAttributeResponse)_objCommon.GetOrgService(systemCall: true).Execute(attributeRequest);
        if (attributeResponse != null)
        {
            return attributeResponse.AttributeMetadata;
        }
        else
        {
            throw new Exception("Attribute " + fieldName + " of entity " + entityName + " has no Attribute Metadata!");
        }
    }

    public ConditionExpression GetQueryCondition(string fieldName, string fieldValue, AttributeTypeCode? attrType)
    {
        ConditionExpression condition = new ConditionExpression();
        condition.AttributeName = fieldName;
        var lookupId = new Guid();
        decimal tempDecimal;
        int tempInt;
        switch (attrType)
        {
            case AttributeTypeCode.Integer:
            case AttributeTypeCode.Picklist:
            case AttributeTypeCode.State:
            case AttributeTypeCode.Status:
                if (!string.IsNullOrEmpty(fieldValue))
                {
                    if (int.TryParse(fieldValue, out tempInt))
                    {
                        condition.Operator = ConditionOperator.Equal;
                        condition.Values.Add(tempInt);
                    }
                    else
                    {
                        throw new Exception("Can't parse integer value for attribute " + fieldName + ". Value is " + fieldValue);
                    }
                }
                else
                {
                    condition.Operator = ConditionOperator.Null;
                }
                break;
            case AttributeTypeCode.BigInt:
                long tempLong;
                if (!string.IsNullOrEmpty(fieldValue))
                {
                    if (long.TryParse(fieldValue, out tempLong))
                    {
                        condition.Operator = ConditionOperator.Equal;
                        condition.Values.Add(tempLong);
                    }
                    else
                    {
                        throw new Exception("Can't parse long value for attribute " + fieldName + ". Value is " + fieldValue);
                    }
                }
                else
                {
                    condition.Operator = ConditionOperator.Null;
                }
                break;
            case AttributeTypeCode.Decimal:
            case AttributeTypeCode.Money:
                if (!string.IsNullOrEmpty(fieldValue))
                {
                    if (decimal.TryParse(fieldValue, out tempDecimal))
                    {
                        condition.Operator = ConditionOperator.Equal;
                        condition.Values.Add(tempDecimal);
                    }
                    else
                    {
                        throw new Exception("Can't parse decimal value for attribute " + fieldName + ". Value is " + fieldValue);
                    }
                }
                else
                {
                    condition.Operator = ConditionOperator.Null;
                }
                break;
            case AttributeTypeCode.Double:
                double tempDouble;
                if (!string.IsNullOrEmpty(fieldValue))
                {
                    if (double.TryParse(fieldValue, out tempDouble))
                    {
                        condition.Operator = ConditionOperator.Equal;
                        condition.Values.Add(tempDouble);
                    }
                    else
                    {
                        throw new Exception("Can't parse double value for attribute " + fieldName + ". Value is " + fieldValue);
                    }
                }
                else
                {
                    condition.Operator = ConditionOperator.Null;
                }
                break;
            case AttributeTypeCode.Boolean:
                bool tempBool;
                if (!string.IsNullOrEmpty(fieldValue))
                {
                    if (bool.TryParse(fieldValue, out tempBool))
                    {
                        condition.Operator = ConditionOperator.Equal;
                        condition.Values.Add(tempBool);
                    }
                    else
                    {
                        throw new Exception("Can't parse double value for attribute " + fieldName + ". Value is " + fieldValue);
                    }
                }
                else
                {
                    condition.Operator = ConditionOperator.Null;
                }
                break;
            case AttributeTypeCode.String:
            case AttributeTypeCode.Memo:
                if (!string.IsNullOrEmpty(fieldValue))
                {
                    condition.Operator = ConditionOperator.Equal;
                    condition.Values.Add(fieldValue);
                }
                else
                {
                    condition.Operator = ConditionOperator.Null;
                }
                break;
            case AttributeTypeCode.Lookup:
            case AttributeTypeCode.Owner:
            case AttributeTypeCode.Customer:
                if (!string.IsNullOrEmpty(fieldValue))
                {
                    if (Guid.TryParse(fieldValue, out lookupId))
                    {
                        condition.Operator = ConditionOperator.Equal;
                        condition.Values.Add(lookupId);
                    }
                    else
                    {
                        throw new Exception("Can't parse GUID value for attribute " + fieldName + ". Value is " + fieldValue);
                    }
                }
                else
                {
                    condition.Operator = ConditionOperator.Null;
                }
                break;
            case AttributeTypeCode.DateTime:
                if (!string.IsNullOrEmpty(fieldValue))
                {
                    var date = new DateTime();
                    if (DateTime.TryParse(fieldValue, out date))
                    {
                        condition.Operator = ConditionOperator.Equal;
                        condition.Values.Add(date);
                    }
                    else
                    {
                        throw new Exception("Can't parse date value for attribute " + fieldName + ". Value is " + fieldValue);
                    }
                }
                else
                {
                    condition.Operator = ConditionOperator.Null;
                }
                break;
            case AttributeTypeCode.Uniqueidentifier:
                throw new Exception("You try to get record id of entity by record id of entity!");
            default:
                throw new Exception("Unknown type of attribute!");
        }
        return condition;
    }
}