﻿using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

public class PrimaryKeyManagerPlugin : BasePlugin
{
    private Entity _targetEntity;
    private Entity _targetEntityPreImage;
    private string _unsecure, _secure;

    public PrimaryKeyManagerPlugin(string unsecure, string secure)
    {
        _unsecure = unsecure;
        _secure = secure;
    }

    protected override void Run()
    {
        try
        {
            _targetEntity = (Entity)Target;
            if (Context.PreEntityImages != null && Context.PreEntityImages.Contains("Pre"))
            {
                _targetEntityPreImage = Context.PreEntityImages["Pre"];
            }
            string primaryKey = null;

            switch (_targetEntity.LogicalName)
            {
                case "account":
                    primaryKey = GetPrimaryKeyForAccount();
                    break;
                case "contact":
                    primaryKey = GetPrimaryKeyForContact();
                    break;
                case "ddsm_site":
                    primaryKey = GetPrimaryKeyForSite();
                    break;
                default:
                    throw new Exception("unexpected entity in primary key generation!" + _targetEntity.LogicalName);
            }
            var entityForUpdate = new Entity(_targetEntity.LogicalName, _targetEntity.Id);
            entityForUpdate["ddsm_primarykey"] = primaryKey;
            
            if (_targetEntity.LogicalName == "account")
            { 
                entityForUpdate["name"] = _targetEntity.GetAttributeValue<string>("name");
            }
            Service.Update(entityForUpdate);
        }
        catch (FaultException<OrganizationServiceFault> ex)
        {
            Tracer.Trace("Timestamp: " + ex.Detail.Timestamp);
            Tracer.Trace("Code: " + ex.Detail.ErrorCode);
            Tracer.Trace("Message: " + ex.Detail.Message);
        }
        catch (Exception e)
        {
            Tracer.Trace("Error:" + e.Message);
        }
    }

    private string GetPrimaryKeyForSite()
    {
        string serviceAddress1 = GetValueFromStringAttribute("ddsm_address1");
        string substringServiceAddress1 = GetSubstringOfField(serviceAddress1, 10, 0);
        string serviceAddress2 = GetValueFromStringAttribute("ddsm_address2");
        string substringServiceAddress2 = GetSubstringOfField(serviceAddress2, 10, 0);
        string serviceCity = GetValueFromStringAttribute("ddsm_city");
        string substringServiceCity = GetSubstringOfField(serviceCity, 10, 0);
        string serviceProvince = GetValueFromStringAttribute("ddsm_province");
        string substringServiceProvince = GetSubstringOfField(serviceProvince, 10, 0);
        string servicePostalCode = GetValueFromStringAttribute("ddsm_zip");
        string substringServicePostalCode = GetSubstringOfField(servicePostalCode, 10, 0);
        
        string parentAccountName = "";
        if (_targetEntity.Attributes.Keys.Contains("ddsm_parentaccount"))
        {
            var parentAccRef = _targetEntity.GetAttributeValue<EntityReference>("ddsm_parentaccount");
            if (parentAccRef != null) { 
                var parentAcc = Service.Retrieve("account", parentAccRef.Id, new ColumnSet("name"));
                parentAccountName = parentAcc.GetAttributeValue<string>("name");
            }
        }
        else if (_targetEntityPreImage != null && _targetEntityPreImage.FormattedValues.Keys.Contains("ddsm_parentaccount"))
        {
            parentAccountName = _targetEntityPreImage.FormattedValues["ddsm_parentaccount"].ToString();
        }
        string substringParentAccountName = GetSubstringOfField(parentAccountName, 15, 0);

        string sitePrimaryKey = substringServiceAddress1 + substringServiceAddress2 + substringServiceCity 
            + substringServiceProvince + substringServicePostalCode + substringParentAccountName;
        return GetFormattedPrimaryKey(sitePrimaryKey);
    }

    private string GetPrimaryKeyForContact()
    {
        string lastName = GetValueFromStringAttribute("lastname");
        string substringLastName = GetSubstringOfField(lastName, 4, 0);
        if(string.IsNullOrEmpty(substringLastName))
        {
            substringLastName = "LLLL";
        }
        string firstName = GetValueFromStringAttribute("firstname");
        string substringFirstName = GetSubstringOfField(firstName, 3, 0);
        if (string.IsNullOrEmpty(substringFirstName))
        {
            substringFirstName = "FFF";
        }
        string address1Telephone1 = GetValueFromStringAttribute("address1_telephone1");
        string substringAddress1Telephone1 = GetSubstringOfField(address1Telephone1, 4, 6);
        substringAddress1Telephone1 = substringAddress1Telephone1 ?? "0000";

        string contactPrimaryKey = substringLastName + substringFirstName + substringAddress1Telephone1;
        return GetFormattedPrimaryKey(contactPrimaryKey);
    }

    private string GetPrimaryKeyForAccount()
    {
        //Create account name here, because it take a part in primary key generation of account
        var accountName = "";
        var accountType = _targetEntity.GetAttributeValue<OptionSetValue>("ddsm_accounttype");
        if (accountType == null && _targetEntityPreImage != null)
        {
            accountType = _targetEntityPreImage.GetAttributeValue<OptionSetValue>("ddsm_accounttype");
            Tracer.Trace("get value from pre image!" + accountType.Value);
        }
        if (accountType?.Value == (int) AccountType.Residential)
        {
            accountName = GetValueFromStringAttribute("ddsm_firstname")
                + GetValueFromStringAttribute("ddsm_lastname");
        }
        else if (accountType?.Value == (int)AccountType.Business)
        {
            accountName = GetValueFromStringAttribute("ddsm_companyname");
        }
        _targetEntity["name"] = accountName;

        var address1Line1 = GetValueFromStringAttribute("address1_line1");
        var address1Line2 = GetValueFromStringAttribute("address1_line2");
        var address1City = GetValueFromStringAttribute("address1_city");
        string substringAccountName = GetSubstringOfField(accountName, 15, 0);

        string accountPrimaryKey = substringAccountName + address1Line1 + address1Line2 + address1City;
        return GetFormattedPrimaryKey(accountPrimaryKey);
    }

    private string GetFormattedPrimaryKey(string primaryKey)
    {
        primaryKey = Regex.Replace(primaryKey, "[^a-zA-Z0-9]", "");
        return primaryKey.ToLower();
    }

    private string GetSubstringOfField(string fieldValue, int lengthOfSubstr, int startIndex)
    {
        if (fieldValue.Length  >= lengthOfSubstr + startIndex)
        {
            return fieldValue.Substring(startIndex, lengthOfSubstr);
        }
        else if (fieldValue.Length < lengthOfSubstr + startIndex && startIndex == 0)
        {
            return fieldValue;
        }
        else
        {
            return null;
        }
    }

    private string GetValueFromStringAttribute(string nameOfAttribute)
    {
        string fieldValue = "";
        if (_targetEntity.Attributes.Keys.Contains(nameOfAttribute))
        {
            fieldValue = _targetEntity.GetAttributeValue<string>(nameOfAttribute);
        }
        else if (_targetEntityPreImage != null && _targetEntityPreImage.Attributes.Keys.Contains(nameOfAttribute))
        {
            fieldValue = _targetEntityPreImage.GetAttributeValue<string>(nameOfAttribute);
        }
        return fieldValue;
    }
}

public enum AccountType
{
    Residential = 962080002,
    Business = 962080000
}