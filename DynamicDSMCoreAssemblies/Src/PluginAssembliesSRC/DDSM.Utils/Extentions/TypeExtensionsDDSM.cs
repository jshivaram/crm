﻿using System;
using System.Text.RegularExpressions;

namespace DDSM.Extentions
{
    public static class TypeExtensionsDDSM
    {
        public static string prepareKey(this string key, int keyLength, char additionalChar, bool isMainPhone = false)
        {
            var result = key.GetStringByPattern();
            if (!string.IsNullOrEmpty(key) && (result.Length >= keyLength))
                result = result.Substring(0, keyLength);
            if (string.IsNullOrEmpty(key))
                result = result.PadRight(keyLength, additionalChar);

            return result;
        }

        public static string prepareMainPhoneKey(this string key, int keyLength = 4, char additionalChar = '0')
        {
            var result = key.GetStringByPattern();
            if (!string.IsNullOrEmpty(key) && (result.Length >= keyLength))
            {
                result = result.Reverse();
                result = result.Substring(0, keyLength);
                result = result.Reverse();
            }
            if (string.IsNullOrEmpty(key))
                result = result.PadRight(keyLength, additionalChar);

            return result;
        }

        public static string Reverse(this string s)
        {
            var charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        /// <summary>
        ///     Return string by Pattern
        /// </summary>
        /// <param name="value">input string</param>
        /// <param name="pattern"> default pattern is [^a-zA-Z0-9]</param>
        /// <returns></returns>
        public static string GetStringByPattern(this string value, string pattern = "[^a-zA-Z0-9-_]")
        {
            var replacedString = Regex.Replace(value, pattern, "");
            return replacedString;
        }
    }
}