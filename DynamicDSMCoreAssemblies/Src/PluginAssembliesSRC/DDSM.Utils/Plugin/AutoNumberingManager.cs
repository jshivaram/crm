﻿using System;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Text.RegularExpressions;

namespace DDSM.Utils
{
    /// <summary>
    ///     Plugin that auto generate an unique number for entity
    /// </summary>
    /// <remarks>
    ///     Register this plug-in on the Create message for all entities
    ///     and pre-operation stage.
    /// </remarks>
    public class AutoNumberingManager : BasePlugin
    {
        private readonly string _autoNumberingEntityName = "ddsm_autonumbering";
        private readonly string _autoNumberingSearcAttrName = "ddsm_name";

        private readonly ICollection<KeyValuePair<string, string>> _numberFieldMapping =
            new Dictionary<string, string>();

        private Entity _autoNumberingEntity;

        private Entity _targetEntity;
        private Entity _targetEntityPre;


        protected override void Run()
        {
            // Obtain the target entity from the input parameters.
            _targetEntity = (Entity) Target;

            //Terminate execution if entity is AutoNumbering
            if (_targetEntity.LogicalName == _autoNumberingEntityName)
                return;

            initNumberFieldMapping();
            Tracer.Trace("Entity: " + _targetEntity.LogicalName);
            Tracer.Trace("EntityId: " + _targetEntity.Id);

            //Get number field name
            var numberingPair = _numberFieldMapping.SingleOrDefault(x => x.Key == _targetEntity.LogicalName);
            var numberingFiledName = numberingPair.Value;

            //Check if filed exists
            if (string.IsNullOrEmpty(numberingFiledName))
                return;

            //Check if value exists
            _targetEntityPre = GetPreImage();
            string _numberingFiledValue = GetNumberValue(_targetEntityPre, numberingFiledName);
            if (!string.IsNullOrEmpty(_numberingFiledValue))
                return;

            //Update Entity number
            _targetEntity[numberingFiledName] = GetNumber();

            //Update Auto Numbering Entity
            UpdateCounter();
        }

        /// <summary>
        ///     Create numbering field name and entity name
        /// </summary>
        private void initNumberFieldMapping()
        {
            _numberFieldMapping.Add(new KeyValuePair<string, string>("ddsm_site", "ddsm_sitenumber"));
            _numberFieldMapping.Add(new KeyValuePair<string, string>("account", "accountnumber"));
            _numberFieldMapping.Add(new KeyValuePair<string, string>("ddsm_project", "ddsm_projectnumber"));
            _numberFieldMapping.Add(new KeyValuePair<string, string>("ddsm_projectgroupfinancials",
                "ddsm_pgfinancialnumber"));
            _numberFieldMapping.Add(new KeyValuePair<string, string>("ddsm_financial", "ddsm_financialnumber"));
        }

        /// <summary>
        ///     Prepare and return unique number of entity
        /// </summary>
        /// <returns></returns>
        private string GetNumber()
        {
            //Get auto numbering rules
            var autoNumberingMetaData = GetAutoNumberingRules();
            if (autoNumberingMetaData.Entities.Count == 0)
                throw new InvalidPluginExecutionException("Auto Numbering Metadata does not exist.");
            //Extract first record
            _autoNumberingEntity = autoNumberingMetaData.Entities[0];

            return GenerateNumber();
        }

        /// <summary>
        ///     Generate unique number for entity by specific rules for each entity
        /// </summary>
        /// <param name="metaData"></param>
        /// <returns></returns>
        private string GenerateNumber()
        {
            var number = "";
            try
            {
                //Get counter
                var counter = GetDataByField("ddsm_counter");
                //if counter is empty set 1
                counter = !string.IsNullOrEmpty(counter) ? counter : "1";
                //Get prefix
                var prefix = GetDataByField("ddsm_prefix");
                //Get length of number
                var length = GetDataByField("ddsm_length");
                //if length is empty set 0
                var lengthAmt = !string.IsNullOrEmpty(length) ? Convert.ToInt32(length) : 0;

                number = counter;

                if (counter.Length < lengthAmt)
                    number = counter.PadLeft(lengthAmt, '0');
                //Join prefix and counter
                number = prefix + number;

                //Replace specific character
                number = GetStringByPattern(number);
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.Message);
            }
            return number;
        }


        /// <summary>
        ///     Get value by specific field
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private string GetDataByField(string fieldName)
        {
            object value = null;
            try
            {
                if (_autoNumberingEntity.Attributes.TryGetValue(fieldName, out value))
                    return value.ToString();
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.Message);
            }
            return string.Empty;
        }

        /// <summary>
        ///     Get rules for generate number
        /// </summary>
        /// <returns></returns>
        private EntityCollection GetAutoNumberingRules()
        {
            // Construct query            
            var condition = new ConditionExpression
            {
                AttributeName = _autoNumberingSearcAttrName,
                Operator = ConditionOperator.Equal
            };
            condition.Values.Add(_targetEntity.LogicalName);

            //Create a column set.
            var columns = new ColumnSet("ddsm_counter", "ddsm_length", "ddsm_prefix", "ddsm_increment");

            // Create query expression.
            var query = new QueryExpression
            {
                ColumnSet = columns,
                EntityName = _autoNumberingEntityName
            };
            query.Criteria.AddCondition(condition);

            var result = Service.RetrieveMultiple(query);
            return result;
        }

        /// <summary>
        ///     Update Auto Numbering Entity to next value
        /// </summary>
        private void UpdateCounter()
        {
            var counter = GetDataByField("ddsm_counter");
            //if counter is empty set 1
            counter = !string.IsNullOrEmpty(counter) ? counter : "1";

            //convert string to int
            var cnt = Convert.ToInt32(counter);

            var increment = Convert.ToInt32(GetDataByField("ddsm_increment"));
            if (increment == int.MinValue)
                increment = 1;
            _autoNumberingEntity["ddsm_counter"] = cnt + increment;
            ;
            Service.Update(_autoNumberingEntity);
        }

        /// <summary>
        ///     Return string by Pattern
        /// </summary>
        /// <param name="value">input string</param>
        /// <param name="pattern"> default pattern is [^a-zA-Z0-9]</param>
        /// <returns></returns>
        private string GetStringByPattern(string value, string pattern = "[^a-zA-Z0-9]")
        {
            var replacedString = Regex.Replace(value, pattern, "");
            return replacedString;
        }

        private Entity GetPreImage()
        {
            if (!Context.PreEntityImages.Contains("Pre"))
                return null;
            _targetEntityPre = Context.PreEntityImages["Pre"];
            return _targetEntityPre;
        }

        private string GetNumberValue(Entity preImage, string numberField)
        {
            object val = null;
            if (_targetEntity.Attributes.TryGetValue(numberField, out val))
                return Convert.ToString(val);

            if ((preImage != null) && preImage.Attributes.TryGetValue(numberField, out val))
                return Convert.ToString(val);

            return string.Empty;
        }

    }
}