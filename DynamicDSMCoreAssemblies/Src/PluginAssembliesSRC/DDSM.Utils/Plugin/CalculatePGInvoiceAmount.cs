﻿using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace DDSM.Utils
{
    public class CalculatePGInvoiceAmount : BasePlugin
    {
        protected override void Run()
        {
            var pgTarget = (Entity) Target;

            var columnSet = new Microsoft.Xrm.Sdk.Query.ColumnSet("ddsm_incentivedaservicesdsmamount",
                "ddsm_incentivedaservicespnsamount", "ddsm_incentiverebatespaidtodadsmamount",
                "ddsm_projectmanagementamount", "ddsm_storevisitsamount", "ddsm_eventsamount", "ddsm_hst");

            var pgData = Service.Retrieve(pgTarget.LogicalName, pgTarget.Id, columnSet);

            decimal invoiceAmount = 0;
            foreach (var col in columnSet.Columns)
            {
                object fieldValue = null;
                if (pgTarget.Attributes.TryGetValue(col, out fieldValue))
                {
                    if (fieldValue != null)
                        invoiceAmount += (fieldValue as Money).Value;
                }
                else
                {
                    if (pgData.Attributes.TryGetValue(col, out fieldValue))
                        if (fieldValue != null)
                            invoiceAmount += (fieldValue as Money).Value;
                }
            }
            pgTarget["ddsm_invoiceamount"] = new Money(invoiceAmount);

            //Update Project Group Financial->ddsm_invoiceamount
            UpdatePgFinancial(Service, pgTarget.Id.ToString(), pgTarget["ddsm_invoiceamount"] as Money);
        }


        //Update Project Group Financial->ddsm_invoiceamount
        public void UpdatePgFinancial(IOrganizationService service, string projectGroupId, Money pgInvoiceAmount)
        {
            EntityCollection pGFinancial = GetPgFinancials(service, projectGroupId);
            if (pGFinancial == null)
                return;
            var emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings {ContinueOnError = false, ReturnResponses = true}
            };

            foreach (var record in pGFinancial.Entities)
            {
                record["ddsm_invoiceamount"] = pgInvoiceAmount;
                var updateRequest = new UpdateRequest();
                updateRequest.Target = record;
                emRequest.Requests.Add(updateRequest);
            }
            var result = service.Execute(emRequest);
        }

        public EntityCollection GetPgFinancials(IOrganizationService service, string projectGroupId)
        {
            // Construct query            
            ConditionExpression condition = new ConditionExpression();
            condition.AttributeName = "ddsm_projectgrouptoprojectgroupfinanid";
            condition.Operator = ConditionOperator.Equal;
            condition.Values.Add(projectGroupId.ToString());

            //Create a column set.
            ColumnSet columns = new ColumnSet("ddsm_projectgroupfinancialsid");

            // Create query expression.
            QueryExpression query = new QueryExpression();
            query.ColumnSet = columns;
            query.EntityName = "ddsm_projectgroupfinancials";
            query.Criteria.AddCondition(condition);

            EntityCollection result = service.RetrieveMultiple(query);

            return result;
        }
    }
}