﻿using System;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Text.RegularExpressions;
using Microsoft.Xrm.Sdk.Messages;

namespace DDSM.Utils
{
    /// <summary>
    ///     Plugin that auto generate an unique number for entity
    /// </summary>
    /// <remarks>
    ///     Register this plug-in on the Create message for all entities
    ///     and pre-operation stage.
    /// </remarks>
    public class CreateRequiredDocsToProject : BasePlugin
    {
        protected override CreatorRecordType GetEntryPoint()
        {
            return CreatorRecordType.Front;
        }

        private readonly string _requiredDocEntityName = "ddsm_documentconvention";
        private readonly ColumnSet _columnSet = new ColumnSet("ddsm_requiredbystatus", "ddsm_name");

        private readonly ICollection<KeyValuePair<string, string>> _fieldMapping =
            new Dictionary<string, string>();
        private readonly ICollection<KeyValuePair<string, string>> _guidFieldMapping =
            new Dictionary<string, string>();

        private Entity _targetEntity;

        private Entity _preContactImages;
        private Entity _postContactImages;

        protected override void Run()
        {
            // Obtain the target entity from the input parameters.
            _targetEntity = (Entity)Target;

            //Terminate execution if entity is Required (Convention) Document
            if (_targetEntity.LogicalName == _requiredDocEntityName)
                return;

            initFieldMapping();
            initGuidFieldMapping();
            Tracer.Trace("Entity: " + _targetEntity.LogicalName);

            //Get required doc field name
            var requiredDocPair = _fieldMapping.SingleOrDefault(x => x.Key == _targetEntity.LogicalName);
            var requiredDocFiledName = requiredDocPair.Value;

            var requiredGuidPair = _guidFieldMapping.SingleOrDefault(x => x.Key == _targetEntity.LogicalName);
            var requiredGuidFiledName = requiredGuidPair.Value;

            var _parentProj = Guid.Empty;

            //Check if filed exists
            if (string.IsNullOrEmpty(requiredDocFiledName))
                return;

            //Check if guid field exists
            var _guid = GetGuid(requiredGuidFiledName);

            if (!(_guid != null && _guid != Guid.Empty))
            {
                return;
            }

            EntityCollection reqDocs = GetRequiredDocCollection(requiredDocFiledName, _guid);

            if (!(reqDocs != null && reqDocs.Entities.Count > 0))
            {
                return;
            }

            ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = false
                }
            }; 

            ExecuteMultipleResponse emResponse;

            switch (_targetEntity.LogicalName)
            {
                case "ddsm_project":

                    _parentProj = _targetEntity.Id;

                    break;

                case "ddsm_measure":

                    _parentProj = GetGuid("ddsm_projecttomeasureid");

                    break;
            }

            if (!(_parentProj != null && _parentProj != Guid.Empty))
            {
                return;
            }

            foreach (var en in reqDocs.Entities)
            {
//                Entity reqDocEntity = new Entity(_requiredDocEntityName);
                Entity reqDocEntity = new Entity("ddsm_requireddocument");

                for (var i = 0; i < _columnSet.Columns.Count; i++)
                {
                    reqDocEntity[_columnSet.Columns[i]] = en[_columnSet.Columns[i]];
                }

                reqDocEntity["ddsm_project"] = new EntityReference("ddsm_project", _parentProj);

                CreateRequest createRequest = new CreateRequest();
                createRequest.Target = reqDocEntity;
                emRequest.Requests.Add(createRequest);
            }

            if (emRequest.Requests.Count > 0)
            {
                emResponse = (ExecuteMultipleResponse)Service.Execute(emRequest);

                foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                {
                    // An error has occurred.
                    if (responseItem.Fault != null)
                    {
                        Tracer.Trace("RequestName: " + emRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message);

                    }
                }

            }

        }

        private void initFieldMapping()
        {
            _fieldMapping.Add(new KeyValuePair<string, string>("ddsm_measure", "ddsm_measuretemplateid"));
            _fieldMapping.Add(new KeyValuePair<string, string>("ddsm_project", "ddsm_projecttemplateid"));
        }

        private void initGuidFieldMapping()
        {
            _guidFieldMapping.Add(new KeyValuePair<string, string>("ddsm_measure", "ddsm_measureselector"));
            _guidFieldMapping.Add(new KeyValuePair<string, string>("ddsm_project", "ddsm_projecttemplateid"));
        }

        private EntityCollection GetRequiredDocCollection(string fieldName, Guid parentGuid)
        {
            EntityCollection reqDocs = new EntityCollection();

            QueryExpression query = new QueryExpression { EntityName = _requiredDocEntityName, ColumnSet = _columnSet };
            query.Criteria = new FilterExpression(LogicalOperator.And);
            query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));
            query.Criteria.AddCondition(new ConditionExpression(fieldName, ConditionOperator.Equal, new object[] { parentGuid }));
            reqDocs = Service.RetrieveMultiple(query);
            return reqDocs;
        }

        /// <summary>
        ///     Get Guid by field
        /// </summary>
        /// <returns></returns>
        private Guid GetGuid(string field)
        {
            var preImages = GetPreImage();
            var postImages = GetPostImage();

            if (_targetEntity.Attributes.ContainsKey(field) && _targetEntity.GetAttributeValue<EntityReference>(field) != null && _targetEntity.GetAttributeValue<EntityReference>(field).Id != null)
            {
                return _targetEntity.GetAttributeValue<EntityReference>(field).Id;
            }

            if (preImages != null && preImages.Attributes.ContainsKey(field) && preImages.GetAttributeValue<EntityReference>(field) != null && preImages.GetAttributeValue<EntityReference>(field).Id != null)
            {
                return preImages.GetAttributeValue<EntityReference>(field).Id;
            }

            if (postImages != null && postImages.Attributes.ContainsKey(field) && postImages.GetAttributeValue<EntityReference>(field) != null && postImages.GetAttributeValue<EntityReference>(field).Id != null)
            {
                return postImages.GetAttributeValue<EntityReference>(field).Id;
            }

            return Guid.Empty;
        }

        /// <summary>
        ///     Get Pre Image
        /// </summary>
        /// <returns></returns>
        private Entity GetPreImage()
        {
            if (!Context.PreEntityImages.Contains("Pre"))
                return null;
            _preContactImages = Context.PreEntityImages["Pre"];
            return _preContactImages;
        }

        /// <summary>
        ///     Get Post Image
        /// </summary>
        /// <returns></returns>
        private Entity GetPostImage()
        {
            if (!Context.PostEntityImages.Contains("Post"))
                return null;
            _postContactImages = Context.PostEntityImages["Post"];
            return _postContactImages;
        }

    }
}