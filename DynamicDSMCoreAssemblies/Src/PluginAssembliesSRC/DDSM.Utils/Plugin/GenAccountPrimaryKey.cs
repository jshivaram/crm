﻿using System;
using DDSM.CommonProvider.src;
using DDSM.Extentions;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace DDSM.Utils
{
    public class GenAccountPrimaryKey : BasePlugin
    {
        //public enum CreatorRecordType
        //{
        //    UnassignedValue = -2147483648,
        //    Front = 962080000,
        //    DataUploader = 962080001,
        //    Portal = 962080002
        //}

        private Entity _account;
        private string _accountPrimaryKey = "";
        private Entity _preAccount;

        protected override void Run()
        {
            // Obtain the target entity from the input parameters.
            var entity = (Entity) Target;

            if (entity.LogicalName != "account")
                return;

            var ddsmAccountprimarykey = entity.Attributes.Contains("ddsm_accountprimarykey")
                ? entity.GetAttributeValue<string>("ddsm_accountprimarykey")
                : "";
            if (
                (Context.MessageName.ToUpper() == "CREATE") &&
                !string.IsNullOrEmpty(ddsmAccountprimarykey)
            )
                return;

            _account = entity;

            var accountPrimaryKey = GetAccountPrimaryKey();

            _account["ddsm_accountprimarykey"] = accountPrimaryKey.ToLower();

            if (Context.MessageName.ToUpper() == "UPDATE")
            {
                _accountPrimaryKey = (_account["ddsm_accountprimarykey"].ToString()).ToLower();

                UpdateSitePrimaryKey();
            }
        }


        private void UpdateSitePrimaryKey()
        {
            var ddsmSites = GetAccount2Site();
            if (ddsmSites == null)
                return;
            var emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings {ContinueOnError = false, ReturnResponses = true}
            };

            foreach (var ddsmSite in ddsmSites.Entities)
            {
                ddsmSite["ddsm_siteprimarykey"] = (CreateSitePrimaryKey(ddsmSite)).ToLower();
                ddsmSite["ddsm_name"] = CreateSiteName(ddsmSite);
                var updateRequest = new UpdateRequest {Target = ddsmSite};
                emRequest.Requests.Add(updateRequest);
            }
            var result = Service.Execute(emRequest);
        }

        private EntityCollection GetAccount2Site()
        {
            try
            {
                // Construct query            
                var condition = new ConditionExpression
                {
                    AttributeName = "ddsm_parentaccount",
                    Operator = ConditionOperator.Equal
                };
                condition.Values.Add(_account.Id.ToString());

                // Create query expression.
                var query = new QueryExpression
                {
                    ColumnSet = new ColumnSet("ddsm_address1", "ddsm_address2", "ddsm_city", "ddsm_parentunitid"),
                    EntityName = "ddsm_site"
                };
                query.Criteria.AddCondition(condition);

                var result = Service.RetrieveMultiple(query);

                return result;
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.Message);
            }
        }

        private string CreateSitePrimaryKey(Entity ddsmSite)
        {
            var sitePrimaryKey = "";
            var ddsmSiteId = ddsmSite.GetAttributeValue<Guid>("ddsm_siteid");
            object ddsmParentunitid = null;
            var isParentSite = false;
            if (ddsmSite.Attributes.TryGetValue("ddsm_parentunitid", out ddsmParentunitid))
                isParentSite = true;


            foreach (var column in ddsmSite.Attributes.Keys)
            {
                if ((column == "ddsm_siteid") || (column == "ddsm_parentunitid") || (column == "ddsm_parentaccount"))
                    continue;
                if (isParentSite && (column == "ddsm_address2"))
                    continue;

                object tmpSitePrimaryKey = null;
                if (ddsmSite.Attributes.TryGetValue(column, out tmpSitePrimaryKey))
                    sitePrimaryKey += tmpSitePrimaryKey;
            }
            sitePrimaryKey += _accountPrimaryKey;
            Tracer.Trace("Site ID=" + ddsmSiteId + ". Site Primary Key=" + sitePrimaryKey);
            return sitePrimaryKey;
        }

        private string CreateSiteName(Entity ddsmSite)
        {
            var sitePrimaryKey = "";
            var ddsmSiteId = ddsmSite.GetAttributeValue<Guid>("ddsm_siteid");
            foreach (var column in ddsmSite.Attributes.Keys)
            {
                if ((column == "ddsm_siteid") || (column == "ddsm_siteprimarykey") || (column == "ddsm_parentunitid") ||
                    (column == "ddsm_parentaccount"))
                    continue;

                object tmpSitePrimaryKey = null;
                if (ddsmSite.Attributes.TryGetValue(column, out tmpSitePrimaryKey))
                    sitePrimaryKey += tmpSitePrimaryKey + " ";
            }
            sitePrimaryKey = sitePrimaryKey.TrimEnd();
            if (string.IsNullOrEmpty(sitePrimaryKey))
                sitePrimaryKey += _account.GetAttributeValue<string>("name");
            else
                sitePrimaryKey += "-" + _account.GetAttributeValue<string>("name");

            Tracer.Trace("Site ID=" + ddsmSiteId + ". Site Name=" + sitePrimaryKey);
            return sitePrimaryKey;
        }

        private string GetAccountPrimaryKey()
        {
            var accountPrimaryKey = "";
            var preImage = GetAccountPreImage();
            var accountType = GetAccountType(preImage);

            accountPrimaryKey = CreateAccountPrimaryKey(accountType);

            return accountPrimaryKey;
        }

        /// <summary>
        ///     Create Account Primary key by account type
        /// </summary>
        /// <param name="accountType"></param>
        /// <returns></returns>
        private string CreateAccountPrimaryKey(AccountTypes accountType)
        {
            var accountPrimaryKey = "";

            switch (accountType)
            {
                case AccountTypes.Business:
                    accountPrimaryKey = GetBusinessAccPrimaryKey();
                    break;
                case AccountTypes.Residential:
                    accountPrimaryKey = GetResidentialAccPrimaryKey();
                    break;
                case AccountTypes.UnassignedValue:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(accountType), accountType, null);
            }

            return accountPrimaryKey;
        }

        /// <summary>
        ///     create account primary key if account type is Business
        /// </summary>
        /// <returns></returns>
        private string GetBusinessAccPrimaryKey(int companyNameLenght = 15)
        {
            var accountPrimaryKey = "";

            var companyName = "";

            //Address 1
            var address1Composite = "";
            //address1Composite = Street1, Street2, City
            var street1 = "";
            var street2 = "";
            var city = "";

            if (Context.MessageName.ToUpper() == "CREATE")
            {
                companyName = GetEntityValue("ddsm_companyname", _account);
                //address1Composite = GetEntityValue("address1_composite", _account);

                street1 = GetEntityValue("address1_line1", _account);
                street2 = GetEntityValue("address1_line2", _account);
                city = GetEntityValue("address1_city", _account);

                address1Composite = street1 + street2 + city;
            }
            else if (Context.MessageName.ToUpper() == "UPDATE")
            {
                companyName = GetEntityValue("ddsm_companyname", _account, _preAccount);
                //address1Composite = GetEntityValue("address1_composite", _account, _preAccount);

                //address1Composite = Street1+Street2+City
                street1 = GetEntityValue("address1_line1", _account, _preAccount);
                street2 = GetEntityValue("address1_line2", _account, _preAccount);
                city = GetEntityValue("address1_city", _account, _preAccount);
                address1Composite = street1 + street2 + city;
                //GetEntityValue("address1_line1", _account, _preAccount) + GetEntityValue("address1_line2", _account, _preAccount) + GetEntityValue("address1_city", _account, _preAccount);
            }


            //parse company name
            companyName = companyName.GetStringByPattern("[^a-zA-Z0-9-_]+");

            //First 15 characters of Company Name
            if (companyName.Length > companyNameLenght)
                companyName = companyName.Substring(0, companyNameLenght);

            //parse address1
            address1Composite = address1Composite.GetStringByPattern("[^a-zA-Z0-9-_]+");

            //Account Primary Key
            accountPrimaryKey = companyName + address1Composite;
            return accountPrimaryKey;
        }

        /// <summary>
        ///     Create account primary key if account type is Residential
        /// </summary>
        /// <returns></returns>
        private string GetResidentialAccPrimaryKey()
        {
            var lastName = "";
            var firstName = "";
            var mainPhone = "";
            if (Context.MessageName.ToUpper() == "CREATE")
            {
                lastName = GetEntityValue("ddsm_lastname", _account);
                firstName = GetEntityValue("ddsm_firstname", _account);
                mainPhone = GetEntityValue("telephone1", _account);
            }
            else if (Context.MessageName.ToUpper() == "UPDATE")
            {
                lastName = GetEntityValue("ddsm_lastname", _account, _preAccount);
                firstName = GetEntityValue("ddsm_firstname", _account, _preAccount);
                mainPhone = GetEntityValue("telephone1", _account, _preAccount);
            }


            return PrepareResidentialAccPrimaryKey(lastName, firstName, mainPhone);
        }


        private string PrepareResidentialAccPrimaryKey(string lastName, string firstName, string mainPhone)
        {
            var result = "";

            var lastNameKey = lastName.prepareKey(4, 'L');
            var firstNameKey = firstName.prepareKey(3, 'F');
            var mainPhoneKey = mainPhone.prepareMainPhoneKey();

            result = lastNameKey + firstNameKey + mainPhoneKey;

            return result.ToUpper();
        }

        /// <summary>
        ///     Get Account type
        /// </summary>
        /// <param name="preImage"></param>
        /// <returns></returns>
        private AccountTypes GetAccountType(Entity preImage)
        {
            object accountType = null;
            if (_account.Attributes.TryGetValue("ddsm_accounttype", out accountType))
                return (AccountTypes) (accountType as OptionSetValue).Value;
            if ((preImage != null) && preImage.Attributes.TryGetValue("ddsm_accounttype", out accountType))
                return (AccountTypes) (accountType as OptionSetValue).Value;
            return AccountTypes.UnassignedValue;
        }

        private string GetEntityValue(string field, Entity entity, Entity preEntity = null)
        {
            //Tracer.Trace("GetEntityValue: " + field);
            var rs = "";
            if (entity.Attributes.Contains(field))
                rs = GetValueByKey(field, entity);
            else if (preEntity != null)
                if (string.IsNullOrEmpty(rs))
                    rs = GetValueByKey(field, preEntity);
            //Tracer.Trace(field + " Retutned rs- " + rs);
            return rs;
        }

        private string GetValueByKey(string column, Entity entity)
        {
            var rs = "";
            object value = null;
            if (entity.Attributes.TryGetValue(column, out value))
                rs = value.ToString();
            return rs;
        }


        private Entity GetAccountPreImage()
        {
            if (!Context.PreEntityImages.Contains("Pre"))
                return null;
            _preAccount = Context.PreEntityImages["Pre"];
            return _preAccount;
        }

        private enum AccountTypes
        {
            Business = 962080000,
            Residential = 962080002,
            UnassignedValue = -2147483648
        }
    }
}