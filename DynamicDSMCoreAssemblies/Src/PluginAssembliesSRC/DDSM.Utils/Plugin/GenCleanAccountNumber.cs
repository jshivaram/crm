﻿using DDSM.CommonProvider.src;
using DDSM.Extentions;
using Microsoft.Xrm.Sdk;

namespace DDSM.Utils
{
    public class GenCleanAccountNumber : BasePlugin
    {
        protected override void Run()
        {
            var account = (Entity) Target;

            object accountNumberObj = null;
            if (!account.Attributes.TryGetValue("accountnumber", out accountNumberObj))
                return;
            var cleanAccountNumber = (accountNumberObj as string).GetStringByPattern();
            account["ddsm_accountnumberclean"] = cleanAccountNumber;
        }
    }
}