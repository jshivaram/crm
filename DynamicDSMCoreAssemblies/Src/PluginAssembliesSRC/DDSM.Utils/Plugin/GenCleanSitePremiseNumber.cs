﻿using DDSM.CommonProvider.src;
using DDSM.Extentions;
using Microsoft.Xrm.Sdk;

namespace DDSM.Utils
{
    public class GenCleanSitePremiseNumber : BasePlugin
    {
        protected override void Run()
        {
            var site = (Entity) Target;
            object tmpObj = null;
            if (!site.Attributes.TryGetValue("ddsm_premisenumber", out tmpObj))
                return;

            var cleanAccountNumber = (tmpObj as string).GetStringByPattern();
            site["ddsm_premisenumberclean"] = cleanAccountNumber;
        }
    }
}