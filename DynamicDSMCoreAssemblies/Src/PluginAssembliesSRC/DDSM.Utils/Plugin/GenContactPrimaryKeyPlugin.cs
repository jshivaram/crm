﻿using DDSM.CommonProvider.src;
using DDSM.Extentions;
using Microsoft.Xrm.Sdk;

namespace DDSM.Utils
{
    public class GenContactPrimaryKeyPlugin : BasePlugin
    {
        private Entity _contact;

        private Entity _preContactImages;


        /// <summary>
        ///     Plugin that auto generate a Contact Primary Key
        /// </summary>
        /// <remarks>
        ///     Register this plug-in on the Create/Update message, contact entity,
        ///     and pre-operation stage.
        /// </remarks>
        protected override void Run()
        {
            // Obtain the target entity from the input parameters.
            _contact = (Entity) Target;

            Tracer.Trace("Entity: " + _contact.LogicalName);
            Tracer.Trace("EntityId: " + _contact.Id);

            //get the values from target
            var ddsmContactPrimaryKey = _contact.Attributes.Contains("ddsm_contactprimarykey")
                ? _contact.GetAttributeValue<string>("ddsm_contactprimarykey")
                : "";

            //get the values from target
            var lastName = GetData("lastname");


            //string firstName = contact.GetAttributeValue<string>("firstname");
            var firstName = GetData("firstname");

            //string mainPhone = contact.GetAttributeValue<string>("address1_telephone1");
            var mainPhone = GetData("address1_telephone1");

            //get Contact Primary Key 
            var contactPrimaryKey = GetContactPrimaryKey(lastName, firstName, mainPhone);

            //set Contact Primary Key
            _contact["ddsm_contactprimarykey"] = contactPrimaryKey.ToLower();

            //show log
            Tracer.Trace("ContactPrimaryKey:" + _contact["ddsm_contactprimarykey"]);
        }

        /// <summary>
        ///     Get data by field
        /// </summary>
        /// <returns></returns>
        private string GetData(string field)
        {
            var preContactImages = GetContactPreImage();
            object data = null;
            if (_contact.Attributes.TryGetValue(field, out data))
                return data.ToString();
            if ((preContactImages != null) && preContactImages.Attributes.TryGetValue(field, out data))
                return data.ToString();
            return "";
        }

        /// <summary>
        ///     Get Pre Image
        /// </summary>
        /// <returns></returns>
        private Entity GetContactPreImage()
        {
            if (!Context.PreEntityImages.Contains("Pre"))
                return null;
            _preContactImages = Context.PreEntityImages["Pre"];
            return _preContactImages;
        }

        /// <summary>
        ///     Get Contact Primary Key by params
        /// </summary>
        /// <param name="lastName"></param>
        /// <param name="firstName"></param>
        /// <param name="mainPhone"></param>
        /// <returns></returns>
        private string GetContactPrimaryKey(string lastName, string firstName, string mainPhone)
        {
            var result = "";
            var lastNameKey = lastName.prepareKey(4, 'L');
            var firstNameKey = firstName.prepareKey(3, 'F');
            var mainPhoneKey = mainPhone.prepareMainPhoneKey();
            result = (lastNameKey + firstNameKey + mainPhoneKey).ToUpper();

            return result;
        }
    }
}