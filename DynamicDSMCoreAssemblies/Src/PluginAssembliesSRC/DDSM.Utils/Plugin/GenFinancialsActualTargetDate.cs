﻿using System;
using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace DDSM.Utils
{
    public class GenFinancialsActualTargetDate : BasePlugin
    {
        protected override void Run()
        {
            var financials = (Entity) Target;
            Tracer.Trace("Entity:" + financials.LogicalName);
            Tracer.Trace("EntityId:" + financials.Id);

            var columnSet = new ColumnSet("ddsm_actualstart1", "ddsm_duration1", "ddsm_stagename1", "ddsm_actualstart2",
                "ddsm_duration2", "ddsm_stagename2", "ddsm_actualstart3", "ddsm_duration3", "ddsm_stagename3",
                "ddsm_targetend2", "ddsm_targetend1");

            var fin = Service.Retrieve(financials.LogicalName, financials.Id, columnSet);

            //to think how do it better
            //E1 - Bug #8791
            object actualend3 = null;
            if (financials.Attributes.TryGetValue("ddsm_actualend3", out actualend3))
                if (!DateTime.MinValue.Equals(Convert.ToDateTime(actualend3)))
                {
                    //TO DO +++ aguk 09/09/2016
                    // Change type of the field  "ddsm_pendingstage" to OptionSet
                    financials["ddsm_pendingstage"] = "Completed";
                    return;
                }
            try
            {
                object dateLine1 = null;
                object dateLine2 = null;
                object actualstart1 = null;
                if (financials.Attributes.TryGetValue("ddsm_actualend1", out dateLine1))
                {
                    if (DateTime.MinValue.Equals(Convert.ToDateTime(dateLine1)))
                    {
                        financials["ddsm_targetstart2"] = fin.GetAttributeValue<DateTime>("ddsm_targetend1");
                        financials["ddsm_actualstart2"] = null;
                        financials["ddsm_targetend2"] =
                            Convert.ToDateTime(financials["ddsm_targetstart2"])
                                .AddDays(fin.GetAttributeValue<int>("ddsm_duration2"));
                        financials["ddsm_targetstart3"] = financials["ddsm_targetend2"];
                        financials["ddsm_targetend3"] =
                            Convert.ToDateTime(financials["ddsm_targetstart3"])
                                .AddDays(fin.GetAttributeValue<int>("ddsm_duration3"));
                        financials["ddsm_pendingstage"] = fin.GetAttributeValue<string>("ddsm_stagename1");
                        return;
                    }

                    financials["ddsm_targetstart2"] = Convert.ToDateTime(dateLine1);
                    financials["ddsm_actualstart2"] = financials["ddsm_targetstart2"];
                    financials["ddsm_targetend2"] =
                        Convert.ToDateTime(dateLine1).AddDays(fin.GetAttributeValue<int>("ddsm_duration2"));
                    financials["ddsm_targetstart3"] = financials["ddsm_targetend2"];
                    financials["ddsm_targetend3"] =
                        Convert.ToDateTime(financials["ddsm_targetstart3"])
                            .AddDays(fin.GetAttributeValue<int>("ddsm_duration3"));
                }
                else if (financials.Attributes.TryGetValue("ddsm_actualend2", out dateLine2) && (dateLine2 != null))
                {
                    financials["ddsm_targetstart3"] = Convert.ToDateTime(dateLine2);
                    financials["ddsm_actualstart3"] = financials["ddsm_targetstart3"];
                    financials["ddsm_targetend3"] =
                        Convert.ToDateTime(dateLine2).AddDays(fin.GetAttributeValue<int>("ddsm_duration3"));
                    financials["ddsm_pendingstage"] = fin.GetAttributeValue<string>("ddsm_stagename3");
                }
                else if (financials.Attributes.TryGetValue("ddsm_actualend2", out dateLine2) && (dateLine2 == null) &&
                         fin.Attributes.TryGetValue("ddsm_targetend2", out dateLine2))
                {
                    financials["ddsm_targetstart3"] = Convert.ToDateTime(dateLine2);
                    financials["ddsm_actualstart3"] = null;
                    financials["ddsm_targetend3"] =
                        Convert.ToDateTime(dateLine2).AddDays(fin.GetAttributeValue<int>("ddsm_duration3"));
                    financials["ddsm_pendingstage"] = fin.GetAttributeValue<string>("ddsm_stagename2");
                }
                else if (financials.Attributes.TryGetValue("ddsm_actualstart1", out actualstart1))
                {
                    financials["ddsm_targetend1"] =
                        Convert.ToDateTime(actualstart1).AddDays(fin.GetAttributeValue<int>("ddsm_duration1"));
                    financials["ddsm_targetstart2"] = financials["ddsm_targetend1"];
                    financials["ddsm_actualstart2"] = null;
                    financials["ddsm_targetend2"] =
                        Convert.ToDateTime(financials["ddsm_targetstart2"])
                            .AddDays(fin.GetAttributeValue<int>("ddsm_duration2"));
                    financials["ddsm_targetstart3"] = financials["ddsm_targetend2"];
                    financials["ddsm_targetend3"] =
                        Convert.ToDateTime(financials["ddsm_targetstart3"])
                            .AddDays(fin.GetAttributeValue<int>("ddsm_duration3"));
                    financials["ddsm_pendingstage"] = fin.GetAttributeValue<string>("ddsm_stagename1");
                }
            }
            catch (Exception ex)
            {
                Tracer.Trace(ex.Message);
            }
        }
    }
}