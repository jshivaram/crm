﻿using DDSM.CommonProvider.src;
using DDSM.Extentions;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace DDSM.Utils
{
    public class GenSitePrimaryKey : BasePlugin
    {
        private string _address1 = "";
        private string _address1_name = "";
        private string _address2_name = "";
        private string _address2 = "";
        private string _city = "";
        private string _city_name = "";
        private Entity _entity;
        private string _parentAccountName = "";


        protected override void Run()
        {
            // Obtain the target entity from the input parameters.
            var entity = (Entity) Target;
            _entity = entity;

            Tracer.Trace("entity id=" + entity.Id);

            if (entity.LogicalName != "ddsm_site")
                return;

            var ddsm_siteprimarykey = entity.Attributes.Contains("ddsm_siteprimarykey")
                ? entity.GetAttributeValue<string>("ddsm_siteprimarykey")
                : "";
            if (
                (Context.MessageName.ToUpper() == "CREATE") &&
                !string.IsNullOrEmpty(ddsm_siteprimarykey)
            )
                return;

            var sitePrimaryKey = "";
            var siteName = "";

            if (Context.MessageName.ToUpper() == "CREATE")
            {
                var accountPrimaryKey = GetAccountPrimaryKey(entity, null);
                _address1 = entity.Attributes.Contains("ddsm_address1")
                    ? entity.GetAttributeValue<string>("ddsm_address1")
                    : "";
                _address2 = entity.Attributes.Contains("ddsm_address2")
                    ? entity.GetAttributeValue<string>("ddsm_address2")
                    : "";
                _city = entity.Attributes.Contains("ddsm_city") ? entity.GetAttributeValue<string>("ddsm_city") : "";
                sitePrimaryKey = GetSitePrimaryKey();
                sitePrimaryKey += accountPrimaryKey;
                sitePrimaryKey = sitePrimaryKey.ToLower();
                siteName = CreateSiteName();
            }
            else
            {
                var columnSet = new ColumnSet("ddsm_address1", "ddsm_address2", "ddsm_city", "ddsm_parentaccount");
                var site = Service.Retrieve(entity.LogicalName, entity.Id, columnSet);

                string[] field = {"ddsm_address1", "ddsm_address2", "ddsm_city"};

                for (var i = 0; i < field.Length; i++)
                {
                    var value = "";

                    if (entity.Attributes.Contains(field[i]))
                        value = GetValueByKey(field[i], entity);
                    else if (site != null)
                        value = GetValueByKey(field[i], site);

                    InetClassPropeties(field[i], value);
                }

                var accountPrimaryKey = GetAccountPrimaryKey(entity, site);
                sitePrimaryKey = GetSitePrimaryKey();
                sitePrimaryKey += accountPrimaryKey;
                siteName = CreateSiteName();


                //if entry from plugin UpdateSitePrimaryKey
                object tmpSitePrimaryKey = null;
                if (entity.Attributes.TryGetValue("ddsm_siteprimarykey", out tmpSitePrimaryKey) &&
                    (tmpSitePrimaryKey.ToString() != sitePrimaryKey))
                    sitePrimaryKey = tmpSitePrimaryKey.ToString();

                object tmpSiteName = null;
                if (entity.Attributes.TryGetValue("ddsm_name", out tmpSiteName) && (tmpSiteName.ToString() != siteName))
                    siteName = tmpSiteName.ToString();
            }

            siteName = string.IsNullOrEmpty(siteName) && entity.Attributes.Contains("ddsm_name")
                ? entity.GetAttributeValue<string>("ddsm_name")
                : siteName;


            var target = Service.Retrieve(entity.LogicalName, entity.Id,
                new ColumnSet("ddsm_siteprimarykey", "ddsm_name"));
            target["ddsm_siteprimarykey"] = sitePrimaryKey.GetStringByPattern().ToLower();
            Tracer.Trace($"ddsm_siteprimarykey={target.Attributes["ddsm_siteprimarykey"]}");
            target["ddsm_name"] = siteName;

            Service.Update(target);
        }

        private void InetClassPropeties(string field, string value)
        {
            switch (field)
            {
                case "ddsm_address1":
                    _address1 = value;
                    break;
                case "ddsm_address2":
                    _address2 = value;
                    break;
                case "ddsm_city":
                    _city = value;
                    break;
            }
        }

        private string GetSitePrimaryKey()
        {
            _address1_name = _address1; //for correct Site Name;
            _city_name = _city;
            _address2_name = _address2;
            _address1 = string.IsNullOrEmpty(_address1) ? _address1 : _address1.GetStringByPattern();
            _address2 = string.IsNullOrEmpty(_address2) ? _address2 : _address2.GetStringByPattern();
            _city = string.IsNullOrEmpty(_city) ? _city : _city.GetStringByPattern();

            /*var target = Service.Retrieve(_entity.LogicalName, _entity.Id, new ColumnSet("ddsm_parentunitid"));
            object ddsmParentunitid = null;

            if (target.Attributes.TryGetValue("ddsm_parentunitid", out ddsmParentunitid))
                return _address1 + _city;*/


            return _address1 + _address2 + _city;
        }

        private string GetAccountPrimaryKey(Entity entity, Entity site = null)
        {
            var accountPrimaryKey = "";
            object ddsm_parentaccount = null;
            if (!entity.Attributes.TryGetValue("ddsm_parentaccount", out ddsm_parentaccount) && (site != null))
                site.Attributes.TryGetValue("ddsm_parentaccount", out ddsm_parentaccount);
            if (ddsm_parentaccount == null)
                return accountPrimaryKey;
            var parentaccount = (EntityReference) ddsm_parentaccount;
            var account = Service.Retrieve(parentaccount.LogicalName, parentaccount.Id,
                new ColumnSet("ddsm_accountprimarykey", "name"));
            object tmpAccountPrimaryKey = null;
            if (account.Attributes.TryGetValue("ddsm_accountprimarykey", out tmpAccountPrimaryKey))
                accountPrimaryKey = tmpAccountPrimaryKey.ToString();
            object accountName = null;
            if (account.Attributes.TryGetValue("name", out accountName))
                _parentAccountName = accountName.ToString();

            return accountPrimaryKey;
        }

        private string GetValueByKey(string column, Entity entity)
        {
            var rs = "";
            object value = null;
            if (entity.Attributes.TryGetValue(column, out value))
                rs = value.ToString();
            return rs;
        }

        private string CreateSiteName()
        {
            var delimer = "";
            var siteName = "";
            var parentAccountName = _parentAccountName;
            if (_parentAccountName.Length > 15)
                parentAccountName = _parentAccountName.Substring(0, 15);
            if (!string.IsNullOrEmpty(_address1_name) || !string.IsNullOrEmpty(_address2) ||
                !string.IsNullOrEmpty(_city))
                delimer = "-";
            siteName = _address1_name + " " + _address2_name + " " + _city_name + delimer + parentAccountName;
            siteName = siteName.Trim();

            return siteName;
        }
    }
}