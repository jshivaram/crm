﻿using System;
using Microsoft.Xrm.Sdk;

namespace DDSM.Utils
{
    //WhoGetsPaid = PayeePreference OptionSet
    public enum WhoGetsPaid
    {
        Account = 962080000,
        Parthner = 962080001,
        Other = 962080002
    }

    public class GetAccountFieldsForFinancial : IPlugin
    {
        private ITracingService tracer;

        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context =
                (IPluginExecutionContext) serviceProvider.GetService(typeof(IPluginExecutionContext));
            tracer = (ITracingService) serviceProvider.GetService(typeof(ITracingService));
            IOrganizationServiceFactory factory =
                (IOrganizationServiceFactory) serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            tracer.Trace("----------start1------------");

            Entity finance = (Entity) context.InputParameters["Target"];

            if (finance.Attributes.Contains("ddsm_whogetspaid"))
                tracer.Trace("Contains ddsm_whogetspaid: " + finance.Attributes.Contains("ddsm_whogetspaid"));

            tracer.Trace("----------start2------------");

            object accountProj = null;
            EntityReference accref = null;

            OptionSetValue payeePref = (OptionSetValue) finance.Attributes["ddsm_whogetspaid"];

            if ((payeePref != null) && (payeePref.Value == (decimal) WhoGetsPaid.Account))
            {
                if (finance.Attributes.TryGetValue("ddsm_payeeaccount", out accountProj))
                    accref = (EntityReference) accountProj;
            }
            else if ((payeePref != null) && (payeePref.Value == (decimal) WhoGetsPaid.Parthner))
            {
                if (finance.Attributes.TryGetValue("ddsm_payeeid", out accountProj))
                    accref = (EntityReference) accountProj;
            }


            /* if (!finance.Attributes.TryGetValue("ddsm_payeeid", out accountProj) && !finance.Attributes.TryGetValue("ddsm_payeeaccount", out accountProj))
             {
                 return;
             }*/


            tracer.Trace("----------start3------------");

            var columnSet = new Microsoft.Xrm.Sdk.Query.ColumnSet("ddsm_payeeaddress1",
                "ddsm_payeeaddress2",
                "ddsm_payeecity",
                "ddsm_payeestate",
                "ddsm_payeezipcode",
                "ddsm_payeeattnto",
                "ddsm_payeenumber",
                "ddsm_payeecompany"
            );

            if (accref != null)
            {
                var accData = service.Retrieve(accref.LogicalName, accref.Id, columnSet);

                tracer.Trace("----------start4------------");

                foreach (var col in columnSet.Columns)
                {
                    object fieldValue = null;
                    if (accData.Attributes.TryGetValue(col, out fieldValue))
                        if (col == "ddsm_payeecompany")
                        {
                            finance["ddsm_payeecompany"] = fieldValue;
                            finance["ddsm_payeename"] = fieldValue;

                            tracer.Trace("name: " + fieldValue);
                        }
                        else
                        {
                            tracer.Trace("set value to column:" + col);

                            finance[col] = fieldValue;
                        }
                    else
                        tracer.Trace("can't get value for column: " + col);
                }
            }

            tracer.Trace("----------start5------------");
        }
    }
}