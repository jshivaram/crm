﻿using System;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;

namespace DDSM.Utils
{
    public class OnDeleteDDSMSite : BasePlugin
    {
        private readonly string Prefix = "ddsm_";
        private EntityReference TargetEntity = null;

        protected override void Run()
        {
            string resultMsg = "********Plug -in OnDeleteDDSMSite is successfully finished ********";
            try
            {
                // Obtain the target entity from the input parameters.
                TargetEntity = (EntityReference) Target;

                Tracer.Trace("LogicalName: " + TargetEntity.LogicalName);
                Tracer.Trace("Entity Id=" + TargetEntity.Id);
                Tracer.Trace("---------------------------------------");

                if (string.IsNullOrEmpty(TargetEntity.LogicalName))
                    return;

                deleteReference();
            }
            catch (Exception ex)
            {
                resultMsg = ex.Message;
            }


            Tracer.Trace(resultMsg);
        }


        private void deleteReference()
        {
            var referencingEntityMetadata = getReferencingEntityMetadata();
            foreach (var item in referencingEntityMetadata)
            {
                string entityName = item.Key;
                List<string> referencingEntityPropertyName = new List<string>();
                foreach (var rel in item.Value)
                    referencingEntityPropertyName.Add(rel.ReferencingAttribute);
                EntityCollection referencingEntity = getReferencingEntity(entityName, referencingEntityPropertyName);
                var i = 1;
                foreach (var record in referencingEntity.Entities)
                {
                    foreach (var attributeName in referencingEntityPropertyName)
                        record[attributeName] = null;
                    Tracer.Trace(i + ". " + "Unlink " + record.LogicalName + " start");
                    Tracer.Trace(record.LogicalName + " ID=" + record.Id);
                    Service.Update(record);
                    Tracer.Trace(i + ". " + "Unlink " + record.LogicalName + " finish");
                    Tracer.Trace("---------------------------------");
                    i++;
                }
            }
        }

        public EntityCollection getReferencingEntity(string entityName, List<string> referencingEntityPropertyName)
        {
            // Create query expression.

            QueryExpression query = new QueryExpression();
            query.EntityName = entityName;
            ColumnSet columns = new ColumnSet();
            var filter = new FilterExpression();
            filter.FilterOperator = LogicalOperator.Or;
            // Construct query
            foreach (var attributeName in referencingEntityPropertyName)
            {
                ConditionExpression condition = new ConditionExpression(attributeName, ConditionOperator.Equal,
                    TargetEntity.Id);
                columns.AddColumn(attributeName);

                filter.AddCondition(condition);
            }
            ;
            query.ColumnSet = columns;
            query.Criteria.Filters.Add(filter);

            EntityCollection result = Service.RetrieveMultiple(query);

            return result;
        }

        private Dictionary<string, List<OneToManyRelationshipMetadata>> getReferencingEntityMetadata()
        {
            var referencingEntity = getOneToManyRelationships();
            var grupped = referencingEntity.Where(b => b.ReferencingEntity.Contains(Prefix))
                .GroupBy(x => x.ReferencingEntity)
                .Where(g => g.Count() > 1).ToDictionary(z => z.Key, x => x.ToList());
            return grupped;
        }

        /// <summary>
        ///     Retrieve the one-to-many relationship using the Name.
        /// </summary>
        /// <returns></returns>
        private OneToManyRelationshipMetadata[] getOneToManyRelationships()
        {
            RetrieveEntityRequest retrieveBankAccountEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = TargetEntity.LogicalName
            };
            RetrieveEntityResponse retrieveBankAccountEntityResponse =
                (RetrieveEntityResponse) Service.Execute(retrieveBankAccountEntityRequest);

            return retrieveBankAccountEntityResponse.EntityMetadata.OneToManyRelationships;
        }
    }
}