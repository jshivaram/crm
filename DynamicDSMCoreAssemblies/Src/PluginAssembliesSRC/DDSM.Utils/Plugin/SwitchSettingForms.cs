﻿using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace DDSM.Utils
{
    public class SwitchSettingForms : BasePlugin
    {
        protected override void Run()
        {
            if (Context.Stage == 20) // Стадия Пре
            {
                var columns = (ColumnSet) Context.InputParameters["ColumnSet"];
                if (!columns.Columns.Contains("ddsm_solution"))
                    columns.AddColumn("ddsm_solution");
            }
            else if (Context.Stage == 40) // Стадия Пост
            {
                var target = (Entity) Context.OutputParameters["BusinessEntity"];

                var businesstypecode = new OptionSetValue();
                object value;
                target.Attributes.TryGetValue("ddsm_solution", out value);
                if (value != null)
                    businesstypecode = (OptionSetValue) value;

                var forms = GetEntityForms("ddsm_admindata");
                string formId;
                if ((forms != null) && (forms.Count > 0))
                {
                    formId = forms[0].Id.ToString(); // Дефолтная форма
                }
                else
                {
                    Tracer.Trace("List forms is null");
                    return;
                }
                // Определяем необходиму форму по значению пиклиста
                var firstOrDefault =
                    forms.FirstOrDefault(
                        x => ((string) x.Attributes["name"]).EndsWith(businesstypecode.Value.ToString()));
                if (firstOrDefault != null) formId = firstOrDefault.Id.ToString();

                var query = new QueryExpression("userentityuisettings");
                query.Criteria.AddCondition("ownerid", ConditionOperator.Equal, Context.UserId); //
                query.Criteria.AddCondition("objecttypecode", ConditionOperator.Equal,
                    (int) EntityTypeCode.ddsm_admindata); // ddsm_admindata
                var uiSettingsCollection = Service.RetrieveMultiple(query);

                if (uiSettingsCollection.Entities?.Count <= 0) return;
                // Обновляем GUID последней просмотренной формы
                var settings = uiSettingsCollection[0];
                settings["lastviewedformxml"] = "<MRUForm><Form Type=\"Main\" Id=\"" + formId + "\" /></MRUForm>";
                Service.Update(settings);
            }
        }

        private IList<Entity> GetEntityForms(string entityName)
        {
            var query = new QueryExpression
            {
                EntityName = "systemform",
                ColumnSet = new ColumnSet("name"),
                Criteria =
                    new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("objecttypecode", ConditionOperator.Equal, entityName)
                        }
                    }
            };


            var result = Service.RetrieveMultiple(query);
            return result.Entities;
        }
    }
}