﻿using DDSM.Extentions;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDSM.CommonProvider.src;

namespace DDSM.Utils
{
    public class UpdateNumberOfImplSites : BasePlugin
    {
        protected override void Run()
        {
            //Tracer.Trace("S0");

            Entity preSiteImage = GetPreSiteImage(Context);
            Entity Site = (Entity)Target;
            //object old_CountImplSites = null;
            //int oldCount = 0;
            //int CountSiteAttribute = 0;
            //string SiteId = "";
            //CountSiteAttribute = Site.Attributes.Count;
            //SiteId = Site.Id.ToString();
            //if (Site.Attributes.TryGetValue("ddsm_numberofimplementationsites", out old_CountImplSites))
            //{
            //    oldCount = (int)old_CountImplSites;
            //}

            object MainParenSite;
            EntityReference MainParSite = null;
            if (Site.Attributes.TryGetValue("ddsm_parentunitid", out MainParenSite))
            {
                if (MainParenSite != null)
                {
                    MainParSite = (EntityReference)MainParenSite;
                    Tracer.Trace("Main Parent Site: " + ((EntityReference)MainParenSite).Id.ToString());
                }
                else
                {

                    Tracer.Trace("Main Parent Site: NULL ");
                }
            }
            else
            {
                Tracer.Trace("Main Parent Site: NULL ");
            }

            ////Select MAin Site ID
            //var querry1 = new QueryExpression();
            //querry1.ColumnSet = new ColumnSet("ddsm_parentunitid");

            //querry1.EntityName = Site.LogicalName;

            //var condition1 = new ConditionExpression();
            //condition1.AttributeName = "ddsm_siteid";
            //condition1.Operator = ConditionOperator.Equal;
            //condition1.Values.Add(Site.Id);
            //querry1.Criteria.AddCondition(condition1);

            //EntityCollection result1 = Service.RetrieveMultiple(querry1);

            //Tracer.Trace("Main ID: ");


            //Tracer.Trace("old_CountImplSites: " + oldCount+" AttrCount: "+ CountSiteAttribute+ " SiteId: "+SiteId);
            //For DELTE STEP
            if (Context.MessageName == "Delete")
            {


                object parentSiteDelLokup = null;
                if (preSiteImage.Attributes.TryGetValue("ddsm_parentunitid", out parentSiteDelLokup))
                {
                    Tracer.Trace("Delete record: " + ((EntityReference)preSiteImage.Attributes["ddsm_parentunitid"]).Id);
                    var parentSite = (EntityReference)parentSiteDelLokup;
                    Tracer.Trace("S3_NEW //");

                    //Select all sites which have Parent Site (all child Sites!)
                    var querry = new QueryExpression();
                    querry.ColumnSet = new ColumnSet("ddsm_name");

                    querry.EntityName = parentSite.LogicalName;

                    var condition = new ConditionExpression();
                    condition.AttributeName = "ddsm_parentunitid";
                    condition.Operator = ConditionOperator.Equal;
                    condition.Values.Add(parentSite.Id);
                    querry.Criteria.AddCondition(condition);

                    EntityCollection result = Service.RetrieveMultiple(querry);

                    if (result.Entities.Count >= 0)
                    {
                        var emRequest = new ExecuteMultipleRequest { Requests = new OrganizationRequestCollection(), Settings = new ExecuteMultipleSettings { ContinueOnError = false, ReturnResponses = true } };

                        var siteData = Service.Retrieve(parentSite.LogicalName, parentSite.Id, new ColumnSet("ddsm_name"));

                        var countImplSites = result.Entities.Count;
                        if (countImplSites >= 0) { countImplSites = countImplSites - 1; }
                        siteData["ddsm_numberofimplementationsites"] = countImplSites;

                        UpdateRequest updateRequest = new UpdateRequest { Target = siteData };
                        emRequest.Requests.Add(updateRequest);
                        Service.Execute(emRequest);
                        Tracer.Trace("Update of Site: " + siteData.Attributes["ddsm_name"] + "Number of Impl Sites: " + countImplSites);
                    }

                }
                object parentSiteDelName = null;
                if (preSiteImage.Attributes.TryGetValue("ddsm_name", out parentSiteDelName))
                {
                    Tracer.Trace("Deleted record: " + (string)parentSiteDelName);
                }
                else
                {
                    Tracer.Trace("Deleted recoerd name of Site wasn't find.");
                }


            }
            //For UPDATE STEP or ADD STEP
            else
            {
                Tracer.Trace("Site: " + Site.Id);

                Tracer.Trace("S1");
                try
                {
                    //Entity preSiteImage = GetPreSiteImage(Context);
                    object parentSiteLokup = null;

                    if (preSiteImage != null)
                    {
                        object oldParentSite = null;
                        //if old ParentSite != null
                        if (preSiteImage.Attributes.TryGetValue("ddsm_parentunitid", out oldParentSite))
                        {
                            //Update Old Parent Site!
                            var oldSite = (EntityReference)oldParentSite;
                            //Select all sites which have Parent Site (all child Sites!)
                            QueryExpression querry = new QueryExpression();
                            querry.ColumnSet = new ColumnSet("ddsm_name");

                            querry.EntityName = oldSite.LogicalName;
                            Tracer.Trace("preImage ParentSite Id: " + oldSite.Id.ToString());
                            Tracer.Trace("Site Main Id: " + Site.Id.ToString());
                            ConditionExpression condition = new ConditionExpression();
                            condition.AttributeName = "ddsm_parentunitid";
                            condition.Operator = ConditionOperator.Equal;
                            condition.Values.Add(oldSite.Id);
                            querry.Criteria.AddCondition(condition);

                            EntityCollection result = Service.RetrieveMultiple(querry);

                            if (result.Entities.Count >= 0)
                            {

                                var emRequest = new ExecuteMultipleRequest { Requests = new OrganizationRequestCollection(), Settings = new ExecuteMultipleSettings { ContinueOnError = false, ReturnResponses = true } };

                                var siteData = Service.Retrieve(oldSite.LogicalName, oldSite.Id, new ColumnSet("ddsm_name"));

                                var countImplSites = result.Entities.Count;
                                //var old_countImplSites = result.Entities.Count;
                                if (countImplSites > 0) { countImplSites = countImplSites - 1; }
                                siteData["ddsm_numberofimplementationsites"] = countImplSites;

                                UpdateRequest updateRequest = new UpdateRequest { Target = siteData };

                                emRequest.Requests.Add(updateRequest);

                                ////update rollup field
                                //var rollupRequest =
                                //new CalculateRollupFieldRequest { Target = new EntityReference("ddsm_site", oldSite.Id), FieldName = "ddsm_numberofimplementationsites" };
                                //emRequest.Requests.Add(rollupRequest);


                                Service.Execute(emRequest);
                                Tracer.Trace("Update of Site: " + siteData.Attributes["ddsm_name"] + "Number of Impl Sites: " + countImplSites);
                            }

                            //Update New Parent Site
                            //if New ParentSite == null
                            if (Site.Attributes.TryGetValue("ddsm_parentunitid", out parentSiteLokup))
                            {
                                Tracer.Trace("S2_NEW");
                                if (parentSiteLokup != null)
                                {
                                    var parentSite = (EntityReference)parentSiteLokup;
                                    Tracer.Trace("S3_NEW ...");

                                    //Select all sites which have Parent Site (all child Sites!)
                                    querry = new QueryExpression();
                                    querry.ColumnSet = new ColumnSet("ddsm_name");

                                    querry.EntityName = parentSite.LogicalName;

                                    condition = new ConditionExpression();
                                    condition.AttributeName = "ddsm_parentunitid";
                                    condition.Operator = ConditionOperator.Equal;
                                    condition.Values.Add(parentSite.Id);
                                    querry.Criteria.AddCondition(condition);

                                    result = Service.RetrieveMultiple(querry);

                                    if (result.Entities.Count >= 0)
                                    {
                                        var emRequest = new ExecuteMultipleRequest { Requests = new OrganizationRequestCollection(), Settings = new ExecuteMultipleSettings { ContinueOnError = false, ReturnResponses = true } };

                                        var siteData = Service.Retrieve(parentSite.LogicalName, parentSite.Id, new ColumnSet("ddsm_name"));

                                        var countImplSites = result.Entities.Count;

                                        //Tracer.Trace("---------------------oldSite: " + oldSite.Id.ToString());
                                        //Tracer.Trace("---------------------MainParenSite: " + MainParSite.Id.ToString());
                                        string s1 = oldSite.Id.ToString();
                                        string s2 = MainParSite.Id.ToString();
                                        //Tracer.Trace("---------------------");
                                        //Tracer.Trace("---------------------s1: " + oldSite.Id.ToString());
                                        //Tracer.Trace("---------------------s2: " + MainParSite.Id.ToString());
                                        if (countImplSites >= 0 && s1 != s2/*oldCount != countImplSites*/) { countImplSites = countImplSites + 1; }
                                        siteData["ddsm_numberofimplementationsites"] = countImplSites;

                                        UpdateRequest updateRequest = new UpdateRequest { Target = siteData };

                                        emRequest.Requests.Add(updateRequest);

                                        ////update rollup field
                                        //var rollupRequest =
                                        //new CalculateRollupFieldRequest { Target = new EntityReference("ddsm_site", parentSite.Id), FieldName = "ddsm_numberofimplementationsites" };
                                        //emRequest.Requests.Add(rollupRequest);

                                        Service.Execute(emRequest);
                                        Tracer.Trace("Update of Site: " + siteData.Attributes["ddsm_name"] + "Number of Impl Sites: " + countImplSites);
                                    }
                                }
                                //return;
                            }

                        }
                        else //Update New parent Site if old Parent Site == null
                        {
                            if (!Site.Attributes.TryGetValue("ddsm_parentunitid", out parentSiteLokup))
                            {
                                return;
                            }
                            Tracer.Trace("S2_NEW_OLD=0");

                            Tracer.Trace(((EntityReference)parentSiteLokup).Id + " " + ((EntityReference)parentSiteLokup).LogicalName);
                            var parentSite = (EntityReference)parentSiteLokup;
                            Tracer.Trace("S3_NEW_OLD=0");
                            //Select all sites which have Parent Site (all child Sites!)
                            QueryExpression querry = new QueryExpression();
                            querry.ColumnSet = new ColumnSet("ddsm_name");

                            querry.EntityName = parentSite.LogicalName;

                            ConditionExpression condition = new ConditionExpression();
                            condition.AttributeName = "ddsm_parentunitid";
                            condition.Operator = ConditionOperator.Equal;
                            condition.Values.Add(parentSite.Id);
                            querry.Criteria.AddCondition(condition);

                            EntityCollection result = Service.RetrieveMultiple(querry);

                            if (result.Entities.Count >= 0)
                            {
                                var emRequest = new ExecuteMultipleRequest { Requests = new OrganizationRequestCollection(), Settings = new ExecuteMultipleSettings { ContinueOnError = false, ReturnResponses = true } };

                                var siteData = Service.Retrieve(parentSite.LogicalName, parentSite.Id, new ColumnSet("ddsm_name"));

                                var countImplSites = result.Entities.Count;
                                if (countImplSites >= 0) { countImplSites = countImplSites + 1; }
                                siteData["ddsm_numberofimplementationsites"] = countImplSites;

                                UpdateRequest updateRequest = new UpdateRequest { Target = siteData };

                                emRequest.Requests.Add(updateRequest);

                                ////update rollup field
                                //var rollupRequest =
                                //new CalculateRollupFieldRequest { Target = new EntityReference("ddsm_site", parentSite.Id), FieldName = "ddsm_numberofimplementationsites" };
                                //emRequest.Requests.Add(rollupRequest);

                                Service.Execute(emRequest);
                                Tracer.Trace("Update of Site: " + siteData.Attributes["ddsm_name"] + "Number of Impl Sites: " + countImplSites);
                            }
                            Tracer.Trace("Update done.");
                        }


                    }




                }

                catch (Exception ex)
                {
                    Tracer.Trace("Error: " + ex);
                }
            }



            /*
            var cleanAccountNumber = (accountNumberObj as string).GetStringByPattern();

            account["ddsm_accountnumberclean"] = cleanAccountNumber;*/
        }


        private Entity GetPreSiteImage(IPluginExecutionContext Context)
        {
            Entity preSite;
            if (Context.PreEntityImages.Contains("Pre"))
            {
                Tracer.Trace("Preimage exist.");
                preSite = Context.PreEntityImages["Pre"];
                return preSite;
            }
            else { return null; }
        }
    }
}

