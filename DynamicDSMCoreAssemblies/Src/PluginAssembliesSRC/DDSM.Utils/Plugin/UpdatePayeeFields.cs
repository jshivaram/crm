﻿using System;
using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace DDSM.Utils.Plugin
{
    internal class UpdatePayeeFields : BasePlugin
    {
        private ExecuteMultipleRequest requestWithResults;

        protected override void Run()
        {
            try
            {
                requestWithResults = new ExecuteMultipleRequest
                {
                    // Assign settings that define execution behavior: continue on error, return responses. 
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = false,
                        ReturnResponses = true
                    },
                    // Create an empty organization request collection.
                    Requests = new OrganizationRequestCollection()
                };
                Entity pTarget = (Entity) Target;

                var colSet = new Microsoft.Xrm.Sdk.Query.ColumnSet("ddsm_accountid", "ddsm_payeenumber");

                Entity projectData = Service.Retrieve(pTarget.LogicalName, pTarget.Id, colSet);
                //Get Acc Number from Account
                Entity account = Service.Retrieve("account", (Guid) projectData["ddsm_accountid"],
                    new Microsoft.Xrm.Sdk.Query.ColumnSet("ddsm_payeenumber"));

                //1. Set Project.Peyee Number
                pTarget["ddsm_payeenumber"] = account["ddsm_payeenumber"];
                UpdateRequest createRequest = new UpdateRequest {Target = pTarget};
                requestWithResults.Requests.Add(createRequest);

                //GetHashCode All Financials records
                ConditionExpression condition = new ConditionExpression();
                condition.AttributeName = "ddsm_projecttofinancialid";
                condition.Operator = ConditionOperator.Equal;
                condition.Values.Add(pTarget.Id.ToString());

                ColumnSet columns = new ColumnSet("ddsm_payeenumber");

                QueryExpression querryFin = new QueryExpression();
                querryFin.ColumnSet = columns;
                querryFin.EntityName = "ddsm_financial";
                querryFin.Criteria.AddCondition(condition);

                EntityCollection financials = Service.RetrieveMultiple(querryFin);

                //2. Update all Financial.Payee Number
                foreach (Entity fin in financials.Entities)
                {
                    fin["ddsm_payeenumber"] = account["ddsm_payeenumber"];
                    UpdateRequest createRequestFin = new UpdateRequest {Target = fin};
                    requestWithResults.Requests.Add(createRequestFin);
                }

                ExecuteMultipleResponse responseWithResults =
                    (ExecuteMultipleResponse) Service.Execute(requestWithResults);
                if (responseWithResults.IsFaulted == true)
                    foreach (var resp in responseWithResults.Responses)
                        Tracer.Trace("Update Error: " + resp.Fault.Message);
                else
                    Tracer.Trace("Update Payee Number done.");
            }
            catch (Exception ex)
            {
                Tracer.Trace("Error: " + ex.Message);
            }
        }
    }
}