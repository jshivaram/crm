﻿using System;
using DDSM.CommonProvider.src;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace DDSM.Utils
{
    public class UpdateRateClassStatusAccordingToMeasures : BasePlugin
    {
        protected override void Run()
        {
            try
            {
                Entity measure = (Entity) Target;
                object state = null;
                object status = null;
                if (measure.Attributes.TryGetValue("statecode", out state) &&
                    measure.Attributes.TryGetValue("statuscode", out status))
                    UpdateRelatedRecord(Service, measure.Id, (status as OptionSetValue).Value,
                        (state as OptionSetValue).Value, measure.LogicalName);
            }
            catch (Exception ex)
            {
                Tracer.Trace("In exception: " + ex.Message + ex.StackTrace);
                throw;
            }
        }

        private void UpdateRelatedRecord(IOrganizationService service, Guid parenMeasureId, int statusCode,
            int stateCode, string logicalName)
        {
            // Construct query            
            ConditionExpression condition = new ConditionExpression();
            condition.AttributeName = "ddsm_measureid";
            condition.Operator = ConditionOperator.Equal;
            condition.Values.Add(parenMeasureId.ToString());

            //Create a column set.
            ColumnSet columns = new ColumnSet("ddsm_rateclassid");

            // Create query expression.
            QueryExpression query1 = new QueryExpression();
            query1.ColumnSet = columns;
            query1.EntityName = "ddsm_rateclass";
            query1.Criteria.AddCondition(condition);

            EntityCollection result1 = service.RetrieveMultiple(query1);
            /*ExecuteMultipleRequest req = new ExecuteMultipleRequest();
            req.Requests = new OrganizationRequestCollection();
            req.Settings = new ExecuteMultipleSettings();
            req.Settings.ContinueOnError = true;
            */
            foreach (var r in result1.Entities)
            {
                SetStateRequest setStateRequest = new SetStateRequest
                {
                    EntityMoniker = new EntityReference("ddsm_rateclass", r.Id),
                    State = new OptionSetValue(stateCode),
                    Status = new OptionSetValue(statusCode)
                };
                SetStateResponse setStateResponse = (SetStateResponse) service.Execute(setStateRequest);
            }
            ;
        }
    }
}