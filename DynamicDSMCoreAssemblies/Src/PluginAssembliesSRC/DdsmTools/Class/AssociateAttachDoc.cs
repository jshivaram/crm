﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.ServiceModel;
using Newtonsoft.Json;

namespace DdsmTools
{
    public class AssociateAttachDoc : CodeActivity
    {
        [RequiredArgument]
        [ReferenceTarget("ddsm_attachmentdocument")]
        [Input("Target Version")]
        public InArgument<EntityReference> TargetVersion { get; set; }

        [RequiredArgument]
        [ReferenceTarget("ddsm_attachmentdocument")]
        [Input("Parent Version")]
        public InArgument<EntityReference> ParentVersion { get; set; }

        private Ddsm objCommon;

        protected override void Execute(CodeActivityContext executionContext)
        {

            if (!IsContextFull(executionContext))
                return;

            try
            {
                objCommon = new Ddsm(executionContext);

                objCommon.TracingService.Trace(">>> AssociateAttachDoc");
                /*
                object target = null;

                if (objCommon.Context.InputParameters.Contains("Target") && (objCommon.Context.InputParameters["Target"] != null))
                {
                    target = objCommon.Context.InputParameters["Target"];
                }
                var targetEntityRef = GetTargetData(target);
                */
                EntityReference targetEntityRef = this.TargetVersion.Get(executionContext);
                EntityReference parentVersion = this.ParentVersion.Get(executionContext);

                objCommon.TracingService.Trace("Target: " + targetEntityRef.Id);
                objCommon.TracingService.Trace("ParentVersion: " + parentVersion.Id);


                string associateEntityName = GetSettingsAssociateEntity(objCommon.OrgService, targetEntityRef);

                if (string.IsNullOrEmpty(associateEntityName)) {
                    return;
                }

                objCommon.TracingService.Trace("AssociateEntityName: " + associateEntityName);

                string[] associateEntities = associateEntityName.Split(',');

                foreach (string enName in associateEntities) {
                    EntityCollection entityCollection = GetEntityCollection(objCommon.OrgService, targetEntityRef, enName);
                    if (entityCollection.Entities.Count > 0) {
                        foreach (Entity en in entityCollection.Entities) {
                            EntityReferenceCollection enRefColl = new EntityReferenceCollection();
                            enRefColl.Add(new EntityReference(en.LogicalName, en.Id));
                            Relationship relationShip = new Relationship("ddsm_attachdoc_" + enName);
                            objCommon.OrgService.Associate("ddsm_attachmentdocument", parentVersion.Id, relationShip, enRefColl);
                        }
                    }
                }


            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException(e.Message);
            }
        }

        private EntityCollection GetEntityCollection(IOrganizationService _orgService, EntityReference enRef, string entityName)
        {
            EntityCollection enCollection = new EntityCollection();

            try
            {
                string fetchCollection = @"<fetch version='1.0' output-format='xml-platform' mapping='logical'>
                <entity name='" + entityName + @"' >
                    <attribute name='" + entityName + @"id' />
                    <filter type='and' >
                        <condition entityname='ddsm_attachdoc_" + entityName + @"' attribute='ddsm_attachmentdocumentid' operator='eq' value='" + enRef.Id + @"' />
                    </filter>
                    <link-entity name='ddsm_attachdoc_" + entityName + @"' from='" + entityName + @"id' to='" + entityName + @"id' intersect='true' />
                </entity>
            </fetch>";

            var fetchRequest = new RetrieveMultipleRequest
            {
                Query = new FetchExpression(fetchCollection)
            };

                enCollection = ((RetrieveMultipleResponse)_orgService.Execute(fetchRequest)).EntityCollection;
            }
            catch (Exception e)
            {
                //throw new InvalidPluginExecutionException(e.Message);
            }

            return enCollection;
        }

        private string GetSettingsAssociateEntity(IOrganizationService _orgService, EntityReference enRef)
        {
            string strEntityName = null;

            try { 
            OrganizationRequest req = new OrganizationRequest("ddsm_AttachmentDocumentSettings");
            //req["Target"] = enRef;
            var paramsSettings = _orgService.Execute(req);

            strEntityName = Convert.ToString( paramsSettings.Results["AttachedEntitiesList"] );

            }
            catch (Exception e)
            {
                //throw new InvalidPluginExecutionException(e.Message);
            }

            return strEntityName;

        }

        private bool IsContextFull(CodeActivityContext executionContext)
        {
            if (executionContext == null) throw new ArgumentNullException(nameof(executionContext));
            var rs = true;
            try
            {
                if (executionContext.GetExtension<IWorkflowContext>() == null)
                    rs = false;
            }
            catch (Exception ex)
            {
                return false;
            }
            return rs;
        }

        private EntityReference GetTargetData(object target)
        {
            try
            {
                if (target is EntityReference)
                    return (EntityReference)target;

                if (target is Entity)
                {
                    var sourceEntity = (Entity)target;

                    return new EntityReference(sourceEntity.LogicalName, sourceEntity.Id);
                }
            }
            catch (Exception)
            {
                throw new Exception($"Can't get target from Context!");
            }


            return null;
        }

    }
}
