﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.ServiceModel;
using Newtonsoft.Json;

namespace DdsmTools
{

    public class BusinessProcessNextStage : CodeActivity
    {
        /*
        [Input("Completed Ms Date")]
        public InArgument<string> CompletedMsDate { get; set; }
        */
        [Input("Docs No Check")]
        public InArgument<bool> DocsNotCheck { get; set; }

        [Input("Payee No Check")]
        public InArgument<bool> PayeeNotCheck { get; set; }

        [Output("OutputMessage")]
        public OutArgument<string> OutputMessage { get; set; }

        private Ddsm objCommon;

        private TimeZoneInfo timeZoneInfo;

        protected override void Execute(CodeActivityContext executionContext)
        {
            var rs = new List<KeyValuePair<string, string>>();

            // The InputParameters collection contains all the data passed in the message request.
            if (!IsContextFull(executionContext))
                return;

            bool docsCheck = true, payeeCheck = true, phaseChenged = false;
            int activeIdx = 2000000000, activeNewIdx = 2000000000, ForecastCompleteLastRecalcIndex = -1, phaseNumber = 0; 
            string traversedpath = string.Empty, stageid = string.Empty, processid = string.Empty;
            Guid completedMsTplGuid = Guid.Empty;
            BusinessProcess projBP = new BusinessProcess();
           
            objCommon = new Ddsm(executionContext);

            string windowsTimeZoneName = objCommon.GetTimeZoneByCRMConnection(objCommon.OrgService, objCommon.Context.UserId);
            timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZoneName);

            DateTime completedMsDate, leftDate, rigthDate, initialPhaseDate;
            completedMsDate = leftDate = rigthDate = initialPhaseDate = objCommon.ConverAttributToDateTimeUtc("today", timeZoneInfo);

            try
            {
                object target = null;

                if (objCommon.Context.InputParameters.Contains("Target") && (objCommon.Context.InputParameters["Target"] != null))
                {
                    target = objCommon.Context.InputParameters["Target"];
                }
                var targetEntityRef = GetTargetData(target);

                /*
                                if (!string.IsNullOrEmpty(this.CompletedMsDate.Get(executionContext)))
                                {
                                    var _completedMsDate = objCommon.ConverAttributToDateTimeUtc(this.CompletedMsDate.Get(executionContext));
                                    if (_completedMsDate != null)
                                    {
                                        completedMsDate = _completedMsDate;
                                    }
                                }
                */
                //Retrive Completed Milestone Date (ddsm_milestoneactualend)
                QueryExpression recordQuery = new QueryExpression { EntityName = "ddsm_project", ColumnSet = new ColumnSet("ddsm_milestoneactualend") };
                recordQuery.Criteria = new FilterExpression(LogicalOperator.And);
                recordQuery.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));
                recordQuery.Criteria.AddCondition("ddsm_projectid", ConditionOperator.Equal, targetEntityRef.Id);
                EntityCollection recordRetrieve = objCommon.OrgService.RetrieveMultiple(recordQuery);
                if (recordRetrieve != null && recordRetrieve.Entities.Count > 0) {
                    if (recordRetrieve[0].Attributes.ContainsKey("ddsm_milestoneactualend") && recordRetrieve[0].GetAttributeValue<DateTime>("ddsm_milestoneactualend") != null) {
                        completedMsDate = recordRetrieve[0].GetAttributeValue<DateTime>("ddsm_milestoneactualend");
                    }
                }
                objCommon.TracingService.Trace("completedMsDate: " + completedMsDate);

                docsCheck = !this.DocsNotCheck.Get(executionContext);
                payeeCheck = !this.PayeeNotCheck.Get(executionContext);

                //completedMsDate = leftDate = objCommon.ResetTimeToStartOfDay(completedMsDate);
                rigthDate = objCommon.ResetTimeToEndOfDay(rigthDate);
                objCommon.TracingService.Trace("rigthDate: " + rigthDate);

                //Get Milestone Collection
                EntityCollection milestoneCollection = GetMilestones(objCommon.OrgService, targetEntityRef.Id);
                if (milestoneCollection.Entities.Count == 0)
                {
                    throw new Exception($"Can't get Project Milestones!");
                }
                foreach (Entity ms in milestoneCollection.Entities)
                {
                    if ((int)ms.GetAttributeValue<OptionSetValue>("ddsm_status").Value == 962080001)
                    {
                        activeIdx = ms.GetAttributeValue<int>("ddsm_index");
                        activeNewIdx = activeIdx + 1;
                        leftDate = ms.GetAttributeValue<DateTime>("ddsm_actualstart");
                        leftDate = objCommon.ResetTimeToStartOfDay(leftDate);
                        objCommon.TracingService.Trace("leftDate: " + leftDate);
                        break;
                    }
                }
                if (activeIdx == 2000000000)
                {
                    throw new Exception($"Can't get Active Milestone!");
                }
                // Verifed Completed Milestone Date
                if (completedMsDate > rigthDate)
                {
                    throw new Exception($"Completed Milestone Date can not be later than today's date.");
                }
                if (completedMsDate < leftDate)
                {
                    throw new Exception($"Completed Milestone Date must be later than Actual Start's date.");
                }

                //Verifed Uploaded Document
                if (!VerifedUploadedDocument(objCommon.OrgService, targetEntityRef.Id, activeIdx, docsCheck))
                {
                    throw new Exception($"Required documents must be attached to this Project before completing this Milestone");
                }

                //Get Project Attributes
                Entity projectAttributes = GetProjectAttributes(objCommon.OrgService, targetEntityRef.Id);
                
                //Payee Info Check
                if (!PayeeInfoCheck(projectAttributes, activeIdx, payeeCheck))
                {
                    throw new Exception($"Payee information incomplete. Please fill in missing fields to continue.");
                }

                //Current phase number
                if (projectAttributes.Attributes.ContainsKey("ddsm_phasenumber"))
                {
                    phaseNumber = projectAttributes.GetAttributeValue<int>("ddsm_phasenumber");
                }

                if (projectAttributes.Attributes.ContainsKey("processid"))
                {
                    processid = Convert.ToString(projectAttributes["processid"]);
                }
                
                if (projectAttributes.Attributes.ContainsKey("stageid"))
                {
                    stageid = Convert.ToString(projectAttributes["stageid"]);
                }
                if (projectAttributes.Attributes.ContainsKey("traversedpath"))
                {
                    traversedpath = Convert.ToString(projectAttributes["traversedpath"]);
                }

                if (!string.IsNullOrEmpty(processid))
                {
                    projBP = GetBusinessProcess(objCommon.OrgService, new Guid(processid));
                }
                else
                { projBP.Id = null; }

                if (projectAttributes.Attributes.Contains("ddsm_forecastcompletelastrecalcindex"))
                    ForecastCompleteLastRecalcIndex = projectAttributes.GetAttributeValue<int>("ddsm_forecastcompletelastrecalcindex");

                DateTime msPrevEndtDay = completedMsDate, ForecastComplete = DateTime.UtcNow;

                Entity updateProj = new Entity("ddsm_project", targetEntityRef.Id);

                foreach (Entity ms in milestoneCollection.Entities)
                {
                    if (ms.Contains("ddsm_duration") && ms["ddsm_duration"] != null)
                    {
                    }
                    else { ms["ddsm_duration"] = 0; }

                    if ((int)ms.GetAttributeValue<OptionSetValue>("ddsm_status").Value == 962080001 && activeIdx == ms.GetAttributeValue<int>("ddsm_index"))
                    {
                        ms["ddsm_status"] = new OptionSetValue(962080002);
                        //ms["ddsm_targetend"] = msPrevEndtDay;
                        //ms["ddsm_actualend"] = ms["ddsm_targetend"];

                        ms["ddsm_actualend"] = msPrevEndtDay;

                        //Actual Duration
                        ms["ddsm_actualduration"] = (ms.GetAttributeValue<DateTime>("ddsm_actualend")).Day - (ms.GetAttributeValue<DateTime>("ddsm_actualstart")).Day;

                        if (ms.Attributes.ContainsKey("ddsm_mstpltoms"))
                        {
                            completedMsTplGuid = ms.GetAttributeValue<EntityReference>("ddsm_mstpltoms").Id;
                        }

                        updateProj["ddsm_completedmilestone"] = ms["ddsm_name"].ToString();
                        updateProj["ddsm_completedstatus"] = ms["ddsm_projectstatus"].ToString();
                        updateProj["ddsm_completephase"] = ms["ddsm_projectphasename"].ToString();

                        if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0 && ForecastCompleteLastRecalcIndex == Convert.ToInt32(ms["ddsm_index"]))
                            ForecastComplete = msPrevEndtDay;


                    }
                    else if ((int)ms.GetAttributeValue<OptionSetValue>("ddsm_status").Value == 962080000 && activeNewIdx == ms.GetAttributeValue<int>("ddsm_index"))
                    {
                        if (projectAttributes["ddsm_phase"].ToString() != ms["ddsm_projectphasename"].ToString())
                        {
                            //initialPhaseDate = msPrevEndtDay;
                            phaseNumber += 1;
                            phaseChenged = true;
                            updateProj["ddsm_phasenumber"] = phaseNumber;
                        }

                        ms["ddsm_status"] = new OptionSetValue(962080001);
                        ms["ddsm_actualstart"] = msPrevEndtDay;
                        ms["ddsm_targetstart"] = msPrevEndtDay;
                        msPrevEndtDay = msPrevEndtDay.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                        ms["ddsm_targetend"] = msPrevEndtDay;

                        updateProj["ddsm_pendingmilestone"] = ms["ddsm_name"].ToString();
                        updateProj["ddsm_pendingmilestoneindex"] = Convert.ToInt32(ms["ddsm_index"]);
                        updateProj["ddsm_pendingactualstart"] = ms["ddsm_actualstart"];
                        updateProj["ddsm_pendingtargetend"] = ms["ddsm_targetend"];
                        updateProj["ddsm_responsible"] = ms["ddsm_responsible"];

                        updateProj["ddsm_projectstatus"] = ms["ddsm_projectstatus"].ToString();
                        updateProj["ddsm_phase"] = ms["ddsm_projectphasename"].ToString();



                        if (projBP.Id != null)
                        {
                            if ((projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())) != null))
                            {
                                stageid = projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())).Id;
                            }

                        }
                        if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0 && ForecastCompleteLastRecalcIndex == Convert.ToInt32(ms["ddsm_index"]))
                            ForecastComplete = ms.GetAttributeValue<DateTime>("ddsm_targetend");
                    }
                    else
                    {
                        ms["ddsm_actualstart"] = msPrevEndtDay;
                        ms["ddsm_targetstart"] = msPrevEndtDay;
                        msPrevEndtDay = msPrevEndtDay.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                        ms["ddsm_targetend"] = msPrevEndtDay;

                        ms["ddsm_status"] = new OptionSetValue(962080000);

                        if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0 && ForecastCompleteLastRecalcIndex == Convert.ToInt32(ms["ddsm_index"]))
                            ForecastComplete = ms.GetAttributeValue<DateTime>("ddsm_targetend");
                    }

                    if (Convert.ToBoolean(projectAttributes["ddsm_completeonlastmilestone"]) && Convert.ToInt32(ms["ddsm_index"]) == Convert.ToInt32(projectAttributes["ddsm_lastmilestoneindex"]) && activeNewIdx == Convert.ToInt32(projectAttributes["ddsm_lastmilestoneindex"]))
                    {
                        updateProj["ddsm_responsible"] = null;

                        updateProj["ddsm_pendingmilestone"] = null;
                        updateProj["ddsm_pendingmilestoneindex"] = null;
                        updateProj["ddsm_pendingactualstart"] = null;
                        updateProj["ddsm_pendingtargetend"] = null;

                        updateProj["ddsm_completedmilestone"] = null;
                        updateProj["ddsm_completedstatus"] = null;
                        updateProj["ddsm_completephase"] = null;

                        updateProj["ddsm_projectstatus"] = ms["ddsm_projectstatus"].ToString();
                        updateProj["ddsm_phase"] = ms["ddsm_projectphasename"].ToString();
                        updateProj["ddsm_enddate"] = msPrevEndtDay;

                        break;
                    }
                }
                //Clear field "Completed Milestone Date"
                updateProj["ddsm_milestoneactualend"] = null;

                if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0)
                {
                    updateProj["ddsm_estimatedprojectcomplete"] = ForecastComplete;
                    updateProj["ddsm_projectcompletiondateupdatedestimate"] = ForecastComplete;
                }
                else
                {
                    updateProj["ddsm_estimatedprojectcomplete"] = msPrevEndtDay;
                    updateProj["ddsm_projectcompletiondateupdatedestimate"] = msPrevEndtDay;
                }

                updateProj["ddsm_targetprojectcompletion"] = msPrevEndtDay;

                //Update Business Process
                if (projBP.Id != null)
                {
                    if (!string.IsNullOrEmpty(stageid))
                    {
                        traversedpath = traversedpath + "," + stageid;
                        updateProj["stageid"] = new Guid(stageid);
                        if (!string.IsNullOrEmpty(traversedpath))
                        {
                            updateProj["traversedpath"] = traversedpath;
                        }
                    }
                }
                //is project form flag loaded components
                updateProj["ddsm_loadedcomponents"] = false;

                //Update Ms records
                var msRequest = new ExecuteMultipleRequest
                {
                    Requests = new OrganizationRequestCollection(),
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = true,
                        ReturnResponses = false
                    }
                };
                foreach (Entity ms in milestoneCollection.Entities)
                {
                    var updateRequest = new UpdateRequest();
                    updateRequest.Target = ms;
                    msRequest.Requests.Add(updateRequest);
                }
                var msResponse = (ExecuteMultipleResponse)objCommon.OrgService.Execute(msRequest);
                foreach (ExecuteMultipleResponseItem responseItem in msResponse.Responses)
                {
                    if (responseItem.Response != null)
                    {
                        if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                        {
                        }
                        else
                        {
                        }
                    }

                    // An error has occurred.
                    else if (responseItem.Fault != null)
                    {
                        objCommon.TracingService.Trace("Error: " + msRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message);
                    }
                }

                //Update project records
                if (updateProj.Attributes.Count > 1)
                {
                    objCommon.OrgService.Update(updateProj);
                }

                //Calling Action clone project measures
                if (phaseChenged)
                {
                    IsChangedProjectPhase(objCommon.OrgServiceUser, targetEntityRef, updateProj["ddsm_phase"].ToString(), updateProj["ddsm_projectstatus"].ToString(), initialPhaseDate, phaseNumber);
                }

                //Recalculate Financial
                FinancialKickoff(objCommon.OrgService, targetEntityRef.Id);

                //Recalculate Milestone Template Average Duration
                RecalcMsTplAverageDuration(objCommon.OrgService, completedMsTplGuid);

                //Success
                rs.Add(new KeyValuePair<string, string>("result", "true"));
                rs.Add(new KeyValuePair<string, string>("msg", "success next stage"));
                var json = JsonConvert.SerializeObject(rs);
                OutputMessage.Set(executionContext, json);

            }
            catch (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex)
            {
                objCommon.TracingService.Trace("Timestamp: " + ex.Detail.Timestamp);
                objCommon.TracingService.Trace("Code: " + ex.Detail.ErrorCode);
                objCommon.TracingService.Trace("Message: " + ex.Detail.Message);
                rs.Add(new KeyValuePair<string, string>("result", "false"));
                rs.Add(new KeyValuePair<string, string>("msg", ex.Detail.Message));
                var json = JsonConvert.SerializeObject(rs);
                OutputMessage.Set(executionContext, json);
            }
            catch (Exception ex)
            {
                objCommon.TracingService.Trace("Error:" + ex.Message);
                rs.Add(new KeyValuePair<string, string>("result", "false"));
                rs.Add(new KeyValuePair<string, string>("msg", ex.Message));
                var json = JsonConvert.SerializeObject(rs);
                OutputMessage.Set(executionContext, json);
            }

        }

        private void FinancialKickoff(IOrganizationService _orgService, Guid id)
        {
            Entity taskQueue = new Entity("ddsm_taskqueue");
            taskQueue["ddsm_processeditems0"] = "{\"SmartMeasures\":[\"" + id.ToString() +"\"],\"DataFields\":null}";
            taskQueue["ddsm_taskentity"] = new OptionSetValue(962080005);
            taskQueue["ddsm_entityrecordtype"] = new OptionSetValue(962080001);
            taskQueue["ddsm_name"] = "Financial Task Queue - " + (objCommon.ConverAttributToDateTimeUtc("today", timeZoneInfo)).ToShortDateString();
            _orgService.Create(taskQueue);
        }

        private void IsChangedProjectPhase(IOrganizationService _orgService, EntityReference eRef, string projPhase, string projStatus, DateTime initialPhaseDate, int phaseNumber)
        {
            
            OrganizationRequest req = new OrganizationRequest("ddsm_CloneMeasProjectNextStage");
            req["Project"] = eRef;
            req["ActualEnd"] = initialPhaseDate;
            req["ProjectPhaseName"] = projPhase;
            req["ProjectStatus"] = projStatus;
            req["ProjectPhaseNumber"] = phaseNumber;
            _orgService.Execute(req);
            
            //objCommon.CloneMeasuresProject(objCommon, eRef.Id, projPhase, phaseNumber, projStatus, initialPhaseDate);

        }

        private void RecalcMsTplAverageDuration(IOrganizationService orgService, Guid completedMsTplGuid)
        {
            if (completedMsTplGuid != Guid.Empty)
            {
                //Get summ and count

                //Update Ms Tpl (ddsm_AverageActualDuration, ddsm_AverageDurationDifference)
            }
        }

        private BusinessProcess GetBusinessProcess(IOrganizationService orgService, Guid id)
        {
            BusinessProcess projBP = new BusinessProcess();
            projBP.Id = null;
            //Get Project Template ID
            try
            {
                QueryExpression processQuery = new QueryExpression("workflow");
                processQuery.ColumnSet = new ColumnSet("name", "workflowid");
                processQuery.ColumnSet = new ColumnSet(true);
                processQuery.Criteria.AddCondition("workflowid", ConditionOperator.Equal, id);
                processQuery.Criteria.AddCondition("primaryentity", ConditionOperator.Equal, "ddsm_project");
                EntityCollection processRetrieve = orgService.RetrieveMultiple(processQuery);
                if (processRetrieve != null && processRetrieve.Entities.Count == 1)
                {
                    projBP.Id = processRetrieve.Entities[0].Attributes["workflowid"].ToString();
                    projBP.Name = processRetrieve.Entities[0].Attributes["name"].ToString();
                    projBP.Stages = new List<BusinessProcessStage>();
                    QueryExpression stageQuery = new QueryExpression("processstage");
                    stageQuery.ColumnSet = new ColumnSet("stagename", "processstageid");
                    stageQuery.Criteria.AddCondition("processid", ConditionOperator.Equal, new Guid(projBP.Id));
                    EntityCollection stageRetrieve = orgService.RetrieveMultiple(stageQuery);
                    if (stageRetrieve != null && stageRetrieve.Entities.Count > 0)
                    {
                        foreach (var stage in stageRetrieve.Entities)
                        {
                            BusinessProcessStage stageObj = new BusinessProcessStage();
                            stageObj.Id = stage.Attributes["processstageid"].ToString();
                            stageObj.Name = stage.Attributes["stagename"].ToString();
                            projBP.Stages.Add(stageObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                projBP = new BusinessProcess();
                projBP.Id = null;
            }
            return projBP;

        }

        private bool PayeeInfoCheck(Entity proj, int activeIdx, bool check)
        {
            if (!check)
            { return true; }

            if (!proj.Attributes.ContainsKey("ddsm_requirepayeeinfoindex"))
            { return true; }
            else if (proj.GetAttributeValue<int>("ddsm_actualstart") != activeIdx)
            { return true; }

            if (!proj.Attributes.ContainsKey("ddsm_payeeaddress1") 
                || !proj.Attributes.ContainsKey("ddsm_payeeaddress2") 
                || !proj.Attributes.ContainsKey("ddsm_payeecity")
                || !proj.Attributes.ContainsKey("ddsm_payeestate")
                || !proj.Attributes.ContainsKey("ddsm_payeezipcode")
                || !proj.Attributes.ContainsKey("ddsm_payeecompany")
                || !proj.Attributes.ContainsKey("ddsm_payeeattnto")
                || !proj.Attributes.ContainsKey("ddsm_payeenumber")
                || !proj.Attributes.ContainsKey("ddsm_payeephone")
                || !proj.Attributes.ContainsKey("ddsm_payeeemail")
                || !proj.Attributes.ContainsKey("ddsm_taxentity")
                || !proj.Attributes.ContainsKey("ddsm_taxid")
                )
            {
                return false;
            }
            else { return true; }


        }

        private bool VerifedUploadedDocument(IOrganizationService orgService, Guid id, int activeIdx, bool check)
        {
            if (!check)
            { return true; }

            QueryExpression query = new QueryExpression();
            query.EntityName = "ddsm_requireddocument";
            query.ColumnSet = new ColumnSet(
                "ddsm_name"
                );
            query.Criteria = new FilterExpression(LogicalOperator.And);
            query.Criteria.AddCondition(new ConditionExpression("ddsm_project", ConditionOperator.Equal, id));
            query.Criteria.AddCondition(new ConditionExpression("ddsm_uploaded", ConditionOperator.NotEqual, true));
            query.Criteria.AddCondition(new ConditionExpression("ddsm_canceled", ConditionOperator.NotEqual, true));
            query.Criteria.AddCondition(new ConditionExpression("ddsm_requiredbystatus", ConditionOperator.Equal, activeIdx));
            query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

            EntityCollection retrive = orgService.RetrieveMultiple(query);

            if (retrive.Entities.Count > 0)
            {
                return false;
            }
            else { return true; }
        }

        private Entity GetProjectAttributes(IOrganizationService orgService, Guid id)
        {
            QueryExpression query = new QueryExpression();
            query.EntityName = "ddsm_project";
            query.ColumnSet = new ColumnSet(
                "ddsm_name"
                , "ddsm_requirepayeeinfoindex"
                , "ddsm_payeeaddress1"
                , "ddsm_payeeaddress2"
                , "ddsm_payeecity"
                , "ddsm_payeestate"
                , "ddsm_payeezipcode"
                , "ddsm_payeecompany"
                , "ddsm_payeeattnto"
                , "ddsm_payeenumber"
                , "ddsm_payeephone"
                , "ddsm_payeeemail"
                , "ddsm_taxentity"
                , "ddsm_taxid"
                , "stageid"
                , "processid"
                , "traversedpath"
                , "ddsm_completeonlastmilestone"
                , "ddsm_lastmilestoneindex"
                , "traversedpath"
                , "ddsm_phasenumber"
                , "ddsm_phase"
                , "ddsm_projectstatus"
                , "ddsm_forecastcompletelastrecalcindex"
                );
            query.Criteria = new FilterExpression(LogicalOperator.And);
            query.Criteria.AddCondition(new ConditionExpression("ddsm_projectid", ConditionOperator.Equal, id));
            query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

            EntityCollection retrive = orgService.RetrieveMultiple(query);

            if (retrive.Entities.Count > 0)
            {
                return retrive.Entities[0];
            }
            else { return new Entity(); }
        }

        private EntityCollection GetMilestones(IOrganizationService orgService, Guid id)
        {
            QueryExpression query = new QueryExpression();
            query.EntityName = "ddsm_milestone";
            query.ColumnSet = new ColumnSet(
                "ddsm_milestoneid"
                , "ddsm_status"
                , "ddsm_name"
                , "ddsm_index"
                , "ddsm_targetstart"
                , "ddsm_actualstart"
                , "ddsm_targetend"
                , "ddsm_actualend"
                , "ddsm_projectstatus"
                , "ddsm_projectphasename"
                , "ddsm_duration"
                , "ddsm_responsible"
                , "ddsm_mstpltoms"
                );
            query.Criteria = new FilterExpression(LogicalOperator.And);
            query.AddOrder("ddsm_index", OrderType.Ascending);
            query.Criteria.AddCondition(new ConditionExpression("ddsm_projecttomilestoneid", ConditionOperator.Equal, id));
            query.Criteria.AddCondition(new ConditionExpression("ddsm_status", ConditionOperator.NotEqual, 962080002)); //Compled Status
            query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

            EntityCollection retrive = orgService.RetrieveMultiple(query);

            if (retrive.Entities.Count > 0)
            {
                return retrive;
            }
            else { return new EntityCollection(); }
        }

        private bool IsContextFull(CodeActivityContext executionContext)
        {
            if (executionContext == null) throw new ArgumentNullException(nameof(executionContext));
            var rs = true;
            try
            {
                if (executionContext.GetExtension<IWorkflowContext>() == null)
                    rs = false;
            }
            catch (Exception ex)
            {
                return false;
            }
            return rs;
        }

        private EntityReference GetTargetData(object target)
        {
            try
            {
                if (target is EntityReference)
                    return (EntityReference)target;

                if (target is Entity)
                {
                    var sourceEntity = (Entity)target;

                    return new EntityReference(sourceEntity.LogicalName, sourceEntity.Id);
                }
            }
            catch (Exception)
            {
                throw new Exception($"Can't get target from Context!");
            }


            return null;
        }

    }
}

