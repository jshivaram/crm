﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;

namespace DdsmTools
{
    public class CalculateDaysCase : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracing = null;
            try
            {
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
                tracing = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

                object target = null;
                context.InputParameters.TryGetValue("Target", out target);
                if (!(target is Entity) || 
                    (target as Entity).GetAttributeValue<OptionSetValue>("statuscode").Value == 3)
                {
                    return;
                }

                var entity = service.Retrieve(
                    (target as Entity).LogicalName,
                    (target as Entity).Id,
                    new Microsoft.Xrm.Sdk.Query.ColumnSet("createdon", "ddsm_dateresolved", "ddsm_assignedresolvedintervals"));

                var assignedDate = entity.GetAttributeValue<DateTime>("createdon");

                tracing.Trace(assignedDate.ToString());

                entity["ddsm_dateresolved"] = DateTime.UtcNow;

                var resolvedDate = entity.GetAttributeValue<DateTime>("ddsm_dateresolved");

                var interval = resolvedDate - assignedDate;

                entity["ddsm_assignedresolvedintervals"] = interval.Days;

                service.Update(entity);
            }
            catch (Exception e)
            {
                tracing?.Trace(e.ToString());
            }
        }
    }
}
