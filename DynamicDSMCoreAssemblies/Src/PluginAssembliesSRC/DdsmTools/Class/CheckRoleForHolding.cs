﻿using System;
using System.Linq;
using System.Activities;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Query;

namespace DdsmTools
{
    public class CheckRoleForHolding : CodeActivity
    {
        [Input("Current security role")]
        public InArgument<string> CurrentRoleArgument { get; set; }
        [Output("Reply")]
        public OutArgument<bool> Reply { get; set; }

        public Guid GetSystemUserId(string name, IOrganizationService service)
        {
            QueryByAttribute queryUsers = new QueryByAttribute
            {
                EntityName = "systemuser",
                ColumnSet = new ColumnSet("systemuserid")
            };

            queryUsers.AddAttributeValue("fullname", name);
            EntityCollection retrievedUsers = service.RetrieveMultiple(queryUsers);
            Guid systemUserId = ((Entity)retrievedUsers.Entities[0]).Id;

            return systemUserId;
        }

        protected override void Execute(CodeActivityContext context)
        {
            string[] allowedRolsNames = null;
            IOrganizationService service = null;
            string currentRolesLine = null;
            string[] currentRoles = null;
            try
            {

                IWorkflowContext workflowContext = context?.GetExtension<IWorkflowContext>();
                IOrganizationServiceFactory serviceFactory = context?.GetExtension<IOrganizationServiceFactory>();
                IOrganizationService OrgServiceUser = serviceFactory.CreateOrganizationService(workflowContext.UserId);
                service = serviceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", OrgServiceUser));

                if (service == null) throw new Exception("Organization service is null");

                currentRolesLine = CurrentRoleArgument?.Get<string>(context);

                if (currentRolesLine == null)
                {
                    Reply.Set(context, false);
                    return;
                }
                currentRoles = currentRolesLine.Split(',', '[', ']', '\"');
                var columns = new ColumnSet(true);
                // Create query expression.
                var query = new QueryExpression
                {
                    ColumnSet = new ColumnSet("ddsm_rolesforonhold"),
                    EntityName = "ddsm_admindata",
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression("ddsm_name", ConditionOperator.Equal, "Admin Data")
                        }
                    }
                };

                var adminData = service.RetrieveMultiple(query).Entities[0];
                
                if (adminData.Attributes.ContainsKey("ddsm_rolesforonhold")) { 

                    string allowedRolsLine = adminData.GetAttributeValue<string>("ddsm_rolesforonhold");

                    if (string.IsNullOrEmpty(allowedRolsLine))
                    {
                        Reply.Set(context, false);
                        return;
                    }

                    allowedRolsNames = allowedRolsLine.Split(',');
                }
                else
                {
                    Reply.Set(context, false);
                    return;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error in C# code. When try to get data. Class: CheckRoleForHolding, Method: Execute.\nError message: " + e.Message + "\nStackTrace:" + e.StackTrace);
            }

            try
            {
                List<string> allowedRolsdID = new List<string>();

                foreach (var name in allowedRolsNames)
                {
                    allowedRolsdID.AddRange(GetRoleId(name, service));
                }

                foreach (var currentRole in currentRoles)
                {
                    foreach (var allowedRole in allowedRolsdID)
                    {
                        if (currentRole == allowedRole)
                        {
                            Reply.Set(context, true);
                            return;
                        }
                    }
                }
                Reply.Set(context, false);
            }
            catch (Exception e)
            {
                throw new Exception("Error in C# code. When try to handle data. Class: CheckRole, Method: Execute.\nError message: " + e.Message);
            }
        }

        private string[] GetRoleId(string name, IOrganizationService service)
        {
            try
            {
                if (name == null) return null;
                if (service == null) throw new Exception("Organization service is null");

                QueryByAttribute querybyattribute = new QueryByAttribute("role");
                querybyattribute.ColumnSet = new ColumnSet("roleid");
                querybyattribute.Attributes.AddRange("name");
                querybyattribute.Values.AddRange(name);
                EntityCollection retrieved = service.RetrieveMultiple(querybyattribute);

                if (retrieved?.Entities?.Count == 0) return null;

                List<string> idList = new List<string>();

                idList.AddRange(retrieved.Entities.Select(entity => entity.Id.ToString()));

                return idList.ToArray();
            }
            catch (Exception e)
            {
                throw new Exception("Error in c# code. Class: CheckRoleForHolding, Method: GetRoleId\nError message: " + e.Message);
            }
        }
    }
}