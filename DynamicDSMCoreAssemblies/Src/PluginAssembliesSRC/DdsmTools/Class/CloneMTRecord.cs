﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

namespace DdsmTools
{
    public class CloneMTRecord : CodeActivity
    {
        [Input("Guids")]
        [Default("")]
        public InArgument<string> Guids { get; set; }

        [Output("CloneGuids")]
        public OutArgument<string> CloneGuids { get; set; }

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        protected string GetParticipation(string attributeName)
        {
            string sReturn = "";
            switch (attributeName)
            {
                case "from":
                    sReturn = "1";
                    break;
                case "to":
                    sReturn = "2";
                    break;
                case "cc":
                    sReturn = "3";
                    break;
                case "bcc":
                    sReturn = "4";
                    break;

                case "organizer":
                    sReturn = "7";
                    break;
                case "requiredattendees":
                    sReturn = "5";
                    break;
                case "optionalattendees":
                    sReturn = "6";
                    break;
                case "customer":
                    sReturn = "11";
                    break;
                case "resources":
                    sReturn = "10";
                    break;
            }
            return sReturn;
        }

        protected List<string> GetEntityAttributesToClone(string entityName, IOrganizationService service, ref string primaryIdAttribute)
        {

            var atts = new List<string>();
            var req = new RetrieveEntityRequest()
            {
                EntityFilters = EntityFilters.Attributes,
                LogicalName = entityName
            };

            var res = (RetrieveEntityResponse)service.Execute(req);
            primaryIdAttribute = res.EntityMetadata.PrimaryIdAttribute;

            foreach (AttributeMetadata attMetadata in res.EntityMetadata.Attributes)
            {
                if ((attMetadata.IsValidForCreate.Value || attMetadata.IsValidForUpdate.Value)
                    && !attMetadata.IsPrimaryId.Value)
                {
                    //tracingService.Trace("Tipo:{0}", attMetadata.AttributeTypeName.Value.ToLower());
                    if (attMetadata.AttributeTypeName.Value.ToLower() == "partylisttype")
                    {
                        atts.Add("partylist-" + attMetadata.LogicalName);
                        //atts.Add(attMetadata.LogicalName);
                    }
                    else
                    {
                        atts.Add(attMetadata.LogicalName);
                    }
                }
            }

            return (atts);
        }

        protected override void Execute(CodeActivityContext executionContext)
        {
            var objDdsm = new Ddsm(executionContext);
            objDdsm.TracingService.Trace("Load CRM Service from context --- OK");
            var objIds = (string)objDdsm.Context.InputParameters["Guids"];
            objDdsm.TracingService.Trace("Input Guids: {0}", objIds);

            string entityName = "ddsm_measuretemplate";
            var objectIds = new List<string>();
            var objectCloneIds = new List<string>();

            if (string.IsNullOrEmpty(objIds))
            {
                //Entity target = (Entity)objDdsm.context.InputParameters["Target"];
                //objectIds.Add(target.Id.ToString());
            }
            else {
                
                objectIds = objIds.Split('|').ToList(); ;
            }

            foreach (string objectId in objectIds)
            {
                objDdsm.TracingService.Trace("Rcord ID: " + objectId + " LogicalName: " + entityName);

                var retrievedObject = objDdsm.OrgService.Retrieve(entityName, new Guid(objectId), new ColumnSet(allColumns: true));
                objDdsm.TracingService.Trace("retrieved object OK");

                var newEntity = new Entity(entityName);
                string primaryIdAttribute = "";
                var atts = GetEntityAttributesToClone(entityName, objDdsm.OrgService, ref primaryIdAttribute);

                foreach (string att in atts)
                {

                    if (retrievedObject.Attributes.Contains(att) && att != "statuscode" && att != "statecode"
                        || att.StartsWith("partylist-"))
                    {
                        if (att.StartsWith("partylist-"))
                        {
                            var att2 = att.Replace("partylist-", "");

                            string fetchParty = @"<fetch version='1.0' output-format='xml - platform' mapping='logical' distinct='true'>
                                                <entity name='activityparty'>
                                                    <attribute name = 'partyid'/>
                                                        <filter type = 'and' >
                                                            <condition attribute = 'activityid' operator= 'eq' value = '" + objectId + @"' />
                                                            <condition attribute = 'participationtypemask' operator= 'eq' value = '" + GetParticipation(att2) + @"' />
                                                         </filter>
                                                </entity>
                                            </fetch> ";

                            var fetchRequest1 = new RetrieveMultipleRequest
                            {
                                Query = new FetchExpression(fetchParty)
                            };
                            objDdsm.TracingService.Trace(fetchParty);
                            EntityCollection returnCollection = ((RetrieveMultipleResponse)objDdsm.OrgService.Execute(fetchRequest1)).EntityCollection;


                            var arrPartiesNew = new EntityCollection();
                            objDdsm.TracingService.Trace("attribute:{0}", att2);

                            foreach (Entity ent in returnCollection.Entities)
                            {
                                var party = new Entity("activityparty");
                                var partyid = (EntityReference)ent.Attributes["partyid"];


                                party.Attributes.Add("partyid", new EntityReference(partyid.LogicalName, partyid.Id));
                                objDdsm.TracingService.Trace("attribute:{0}:{1}:{2}", att2, partyid.LogicalName, partyid.Id.ToString());
                                arrPartiesNew.Entities.Add(party);
                            }

                            newEntity.Attributes.Add(att2, arrPartiesNew);
                            continue;

                        }

                        objDdsm.TracingService.Trace("attribute:{0}", att);
                        newEntity.Attributes.Add(att, retrievedObject.Attributes[att]);
                    }
                }

                objDdsm.TracingService.Trace("generation new Measure Number...");
                if (newEntity.Attributes.Contains("ddsm_measureid"))
                {
                    string measureNumber = string.Empty;
                    var measureId = newEntity.GetAttributeValue<string>("ddsm_measureid");
                    if (!string.IsNullOrEmpty(measureId)) {
                        var query = new QueryExpression();
                        query.EntityName = "ddsm_measuretemplate";
                        query.ColumnSet = new ColumnSet();
                        query.ColumnSet.Columns.Add("ddsm_measurenumber");
                        query.ColumnSet.Columns.Add("ddsm_measureid");

                        var filter = new FilterExpression(LogicalOperator.And);

                        var condition = new ConditionExpression();
                        condition.AttributeName = "ddsm_measureid";
                        condition.Values.Add(measureId);
                        condition.Operator = ConditionOperator.Equal;
                        filter.Conditions.Add(condition);
                        query.Criteria = filter;

                        query.AddOrder("ddsm_measurenumber", OrderType.Descending);

                        var results = objDdsm.OrgService.RetrieveMultiple(query);

                        if (results.Entities.Count > 0)
                        {
                            measureNumber = results.Entities[0]["ddsm_measurenumber"].ToString();

                            objDdsm.TracingService.Trace("measureNumber: {0} measureID:{1}", measureNumber, measureId);

                            int numValue = 1;
                            if (!string.IsNullOrEmpty(measureNumber))
                            {
                                measureNumber = measureNumber.Replace(measureId, "");
                                if (Int32.TryParse(measureNumber, out numValue)) {
                                    numValue++;
                                }
                            }
                            objDdsm.TracingService.Trace("numValue: {0} ", numValue);

                            measureNumber = measureId + (numValue.ToString()).PadLeft(3, '0');
                            
                            newEntity["ddsm_measurenumber"] = measureNumber;
                        }
                    }
                }

                objDdsm.TracingService.Trace("creting cloned object...");
                var createdGuid = objDdsm.OrgService.Create(newEntity);
                objectCloneIds.Add(createdGuid.ToString());
                objDdsm.TracingService.Trace("created cloned object OK");
                /*
                Entity record = objDdsm.orgService.Retrieve(entityName, createdGUID, new ColumnSet("statuscode", "statecode"));

                if (retrievedObject.Attributes["statuscode"] != record.Attributes["statuscode"] ||
                    retrievedObject.Attributes["statecode"] != record.Attributes["statecode"])
                {
                    Entity setStatusEnt = new Entity(entityName, createdGUID);
                    setStatusEnt.Attributes.Add("statuscode", retrievedObject.Attributes["statuscode"]);
                    setStatusEnt.Attributes.Add("statecode", retrievedObject.Attributes["statecode"]);

                    objDdsm.orgService.Update(setStatusEnt);
                }

                objDdsm.tracingService.Trace("cloned object OK");
                */
            }
            objDdsm.TracingService.Trace("cloned all objects --- OK");
            this.CloneGuids.Set(executionContext, string.Join("|", objectCloneIds.ToArray()));
            this.Complete.Set(executionContext, true);
        }

    }
}
