﻿using System;
using System.Collections.Generic;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;


namespace DdsmTools.Class
{
    public class CloneMeasProjectNextStage : CodeActivity
    {
        [Input("Project")]
        [ArgumentEntity("ddsm_project")]
        [ReferenceTarget("ddsm_project")]
        public InArgument<EntityReference> Project { get; set; }

        [Input("Actual End")]
        public InArgument<DateTime> ActualEnd { get; set; }

        [Input("Project Phase Name")]
        public InArgument<string> ProjectPhaseName { get; set; }

        [Input("Project Status")]
        public InArgument<string> ProjectStatus { get; set; }

        [Input("Project Phase Number")]
        public InArgument<int> ProjectPhaseNumber { get; set; }

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        private TimeZoneInfo timeZoneInfo;

        //Common _common;

        protected override void Execute(CodeActivityContext executionContext)
        {
            // The InputParameters collection contains all the data passed in the message request.
            if (!IsContextFull(executionContext))
                return;

            var objCommon = new Ddsm(executionContext);
            //_common = new Common(executionContext);
            objCommon.TracingService.Trace("Load CRM Service from context --->>> OK");
            objCommon.TracingService.Trace("User ID: " + objCommon.Context.UserId);

            string windowsTimeZoneName = objCommon.GetTimeZoneByCRMConnection(objCommon.OrgService, objCommon.Context.UserId);
            timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZoneName);

            try
            {
                var project = this.Project.Get(executionContext);
                var actualEnd = this.ActualEnd.Get(executionContext);
                var projectPhaseName = this.ProjectPhaseName.Get(executionContext);
                var projectStatus = this.ProjectStatus.Get(executionContext);
                var projectPhaseNumber = this.ProjectPhaseNumber.Get(executionContext);

                objCommon.TracingService.Trace("Parent Project: {0}", project.Id.ToString());
                objCommon.TracingService.Trace("Fncl ActualEnd: {0}", actualEnd.ToString());

                objCommon.TracingService.Trace("projectPhaseName: {0}", projectPhaseName);
                objCommon.TracingService.Trace("projectStatus: {0}", projectStatus);
                objCommon.TracingService.Trace("projectPhaseNumber: {0}", projectPhaseNumber.ToString());

                if (actualEnd == null)
                    actualEnd = objCommon.ConverAttributToDateTimeUtc("today", timeZoneInfo);

                if (projectPhaseName == null)
                    projectPhaseName = string.Empty;

                if (projectStatus == null)
                    projectStatus = string.Empty;

                if (projectPhaseNumber == null)
                    projectPhaseNumber = 0;

                // Change Projcet Phase // Clone project Measure
                objCommon.CloneMeasuresProject(objCommon, project.Id, projectPhaseName, projectPhaseNumber, projectStatus, actualEnd);

                this.Complete.Set(executionContext, true);

            }
            catch (Exception ex)
            {
                objCommon.TracingService.Trace("Error:" + ex.Message);
                this.Complete.Set(executionContext, false);
            }

        }

        private bool IsContextFull(CodeActivityContext executionContext)
        {
            if (executionContext == null) throw new ArgumentNullException(nameof(executionContext));
            var rs = true;
            try
            {
                if (executionContext.GetExtension<IWorkflowContext>() == null)
                    rs = false;
            }
            catch (Exception ex)
            {
                return false;
            }
            return rs;
        }

    }
}
