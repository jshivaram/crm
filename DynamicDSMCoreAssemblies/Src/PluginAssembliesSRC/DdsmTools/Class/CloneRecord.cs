﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

namespace DdsmTools
{
    public class CloneRecord : CodeActivity
    {
        [Input("Guids")]
        [Default("")]
        public InArgument<String> Guids { get; set; }

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        protected string GetParticipation(string attributeName)
        {
            string sReturn = "";
            switch (attributeName)
            {
                case "from":
                    sReturn = "1";
                    break;
                case "to":
                    sReturn = "2";
                    break;
                case "cc":
                    sReturn = "3";
                    break;
                case "bcc":
                    sReturn = "4";
                    break;

                case "organizer":
                    sReturn = "7";
                    break;
                case "requiredattendees":
                    sReturn = "5";
                    break;
                case "optionalattendees":
                    sReturn = "6";
                    break;
                case "customer":
                    sReturn = "11";
                    break;
                case "resources":
                    sReturn = "10";
                    break;
            }
            return sReturn;
        }

        protected List<string> GetEntityAttributesToClone(string entityName, IOrganizationService service, ref string primaryIdAttribute)
        {

            var atts = new List<string>();
            var req = new RetrieveEntityRequest()
            {
                EntityFilters = EntityFilters.Attributes,
                LogicalName = entityName
            };

            var res = (RetrieveEntityResponse)service.Execute(req);
            primaryIdAttribute = res.EntityMetadata.PrimaryIdAttribute;

            foreach (AttributeMetadata attMetadata in res.EntityMetadata.Attributes)
            {
                if ((attMetadata.IsValidForCreate.Value || attMetadata.IsValidForUpdate.Value)
                    && !attMetadata.IsPrimaryId.Value)
                {
                    //tracingService.Trace("Tipo:{0}", attMetadata.AttributeTypeName.Value.ToLower());
                    if (attMetadata.AttributeTypeName.Value.ToLower() == "partylisttype")
                    {
                        atts.Add("partylist-" + attMetadata.LogicalName);
                        //atts.Add(attMetadata.LogicalName);
                    }
                    else
                    {
                        atts.Add(attMetadata.LogicalName);
                    }
                }
            }

            return (atts);
        }

        protected override void Execute(CodeActivityContext executionContext)
        {
            var objDdsm = new Ddsm(executionContext);
            objDdsm.TracingService.Trace("Load CRM Service from context --- OK");

            var target = (Entity)objDdsm.Context.InputParameters["Target"];

            string entityName = "ddsm_measuretemplate";
            var objectIds = new List<string>();

            //objectId = target.Id.ToString();
            if (string.IsNullOrEmpty(Guids.Get(executionContext)))
            {
                objectIds.Add(target.Id.ToString());
            }
            else {
                objectIds = (Guids.Get(executionContext)).Split('|').ToList(); ;
            }

            foreach (string objectId in objectIds)
            {
                objDdsm.TracingService.Trace("Rcord ID: " + objectId + " LogicalName: " + entityName);

                var retrievedObject = objDdsm.OrgService.Retrieve(entityName, new Guid(objectId), new ColumnSet(allColumns: true));
                objDdsm.TracingService.Trace("retrieved object OK");

                var newEntity = new Entity(entityName);
                string primaryIdAttribute = "";
                var atts = GetEntityAttributesToClone(entityName, objDdsm.OrgService, ref primaryIdAttribute);

                foreach (string att in atts)
                {

                    if (retrievedObject.Attributes.Contains(att) && att != "statuscode" && att != "statecode"
                        || att.StartsWith("partylist-"))
                    {
                        if (att.StartsWith("partylist-"))
                        {
                            var att2 = att.Replace("partylist-", "");

                            string fetchParty = @"<fetch version='1.0' output-format='xml - platform' mapping='logical' distinct='true'>
                                                <entity name='activityparty'>
                                                    <attribute name = 'partyid'/>
                                                        <filter type = 'and' >
                                                            <condition attribute = 'activityid' operator= 'eq' value = '" + objectId + @"' />
                                                            <condition attribute = 'participationtypemask' operator= 'eq' value = '" + GetParticipation(att2) + @"' />
                                                         </filter>
                                                </entity>
                                            </fetch> ";

                            var fetchRequest1 = new RetrieveMultipleRequest
                            {
                                Query = new FetchExpression(fetchParty)
                            };
                            objDdsm.TracingService.Trace(fetchParty);
                            EntityCollection returnCollection = ((RetrieveMultipleResponse)objDdsm.OrgService.Execute(fetchRequest1)).EntityCollection;


                            var arrPartiesNew = new EntityCollection();
                            objDdsm.TracingService.Trace("attribute:{0}", att2);

                            foreach (Entity ent in returnCollection.Entities)
                            {
                                var party = new Entity("activityparty");
                                var partyid = (EntityReference)ent.Attributes["partyid"];


                                party.Attributes.Add("partyid", new EntityReference(partyid.LogicalName, partyid.Id));
                                objDdsm.TracingService.Trace("attribute:{0}:{1}:{2}", att2, partyid.LogicalName, partyid.Id.ToString());
                                arrPartiesNew.Entities.Add(party);
                            }

                            newEntity.Attributes.Add(att2, arrPartiesNew);
                            continue;

                        }

                        objDdsm.TracingService.Trace("attribute:{0}", att);
                        newEntity.Attributes.Add(att, retrievedObject.Attributes[att]);
                    }
                }

                objDdsm.TracingService.Trace("creting cloned object...");
                var createdGuid = objDdsm.OrgService.Create(newEntity);
                objDdsm.TracingService.Trace("created cloned object OK");

                var record = objDdsm.OrgService.Retrieve(entityName, createdGuid, new ColumnSet("statuscode", "statecode"));

                if (retrievedObject.Attributes["statuscode"] != record.Attributes["statuscode"] ||
                    retrievedObject.Attributes["statecode"] != record.Attributes["statecode"])
                {
                    var setStatusEnt = new Entity(entityName, createdGuid);
                    setStatusEnt.Attributes.Add("statuscode", retrievedObject.Attributes["statuscode"]);
                    setStatusEnt.Attributes.Add("statecode", retrievedObject.Attributes["statecode"]);

                    objDdsm.OrgService.Update(setStatusEnt);
                }

                objDdsm.TracingService.Trace("cloned object OK");

            }

            Complete.Set(executionContext, true);
        }

    }

}