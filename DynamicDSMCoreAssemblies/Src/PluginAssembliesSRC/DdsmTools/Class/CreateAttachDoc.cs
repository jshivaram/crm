﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.ServiceModel;
using Newtonsoft.Json;

namespace DdsmTools
{
    public class CreateAttachDoc : CodeActivity
    {
        [RequiredArgument]
        [Input("Associate Objects")]
        public InArgument<string> AssociateObjects { get; set; }

        [RequiredArgument]
        [Input("File Name")]
        public InArgument<string> FileName { get; set; }

        [Input("Description")]
        public InArgument<string> Description { get; set; }

        [RequiredArgument]
        [Input("File")]
        public InArgument<string> File { get; set; }

        private Ddsm objCommon;

        protected override void Execute(CodeActivityContext executionContext)
        {

            if (!IsContextFull(executionContext))
                return;

            try
            {
                objCommon = new Ddsm(executionContext);

                object target = null;

                if (objCommon.Context.InputParameters.Contains("Target") && (objCommon.Context.InputParameters["Target"] != null))
                {
                    target = objCommon.Context.InputParameters["Target"];
                }
                var targetEntityRef = GetTargetData(target);

            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException(e.Message);
            }
        }

        private bool IsContextFull(CodeActivityContext executionContext)
        {
            if (executionContext == null) throw new ArgumentNullException(nameof(executionContext));
            var rs = true;
            try
            {
                if (executionContext.GetExtension<IWorkflowContext>() == null)
                    rs = false;
            }
            catch (Exception ex)
            {
                return false;
            }
            return rs;
        }

        private EntityReference GetTargetData(object target)
        {
            try
            {
                if (target is EntityReference)
                    return (EntityReference)target;

                if (target is Entity)
                {
                    var sourceEntity = (Entity)target;

                    return new EntityReference(sourceEntity.LogicalName, sourceEntity.Id);
                }
            }
            catch (Exception)
            {
                throw new Exception($"Can't get target from Context!");
            }

            return null;
        }
    }
}
