﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.ServiceModel;
using Newtonsoft.Json;
using DDSM.CommonProvider;

namespace DdsmTools
{

    public class CreateMNAmultipleByMeasureID : CodeActivity
    {
        /*[RequiredArgument]*/
        [Input("Model Number Approval")]
        [ArgumentEntity("ddsm_modelnumberapproval")]
        [ReferenceTarget("ddsm_modelnumberapproval")]
        public InArgument<EntityReference> ModelNumberApproval { get; set; }

        /*[RequiredArgument]*/
        [Input("Model Number")]
        [ArgumentEntity("ddsm_modelnumber")]
        [ReferenceTarget("ddsm_modelnumber")]
        public InArgument<EntityReference> ModelNumber { get; set; }

        /*[RequiredArgument]
        [Input("LookupEntity")]
        public InArgument<String> LookupEntity { get; set; }

        [RequiredArgument]
        [Input("LookupField")]
        public InArgument<String> LookupField { get; set; }

        [RequiredArgument]
        [Input("LookupName")]
        public InArgument<String> LookupName { get; set; }

        [RequiredArgument]
        [Input("TargetEntity")]
        public InArgument<String> TargetEntity { get; set; }

        [RequiredArgument]
        [Input("TargetField")]
        public InArgument<String> TargetField { get; set; }*/

        //Common objCommon;
        protected override void Execute(CodeActivityContext executionContext)
        {

            /*IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);*/


            //var _objCommon = new Ddsm(executionContext);
            var objCommon = new Common(executionContext);
            //throw new Exception("IN Execute");
            //objCommon.TracingService.Trace("--0000001. Execute");
            //Entity Target = (Entity)objCommon.Context.InputParameters["Target"];
            var Target = ModelNumber.Get(executionContext);
            var modelNumberApprovalRef = ModelNumberApproval.Get(executionContext);
            //throw new Exception("After exec context get Execute");



            /*EntityReference entityTarget = new EntityReference(context.PrimaryEntityName, context.PrimaryEntityId);
            if (entityTarget.Id == Guid.Empty)
            {
                //tracer.Trace("Cannot get target");
                return;
            }

            var lookupEntity = LookupEntity.Get(executionContext).Trim();
            var lookupField = LookupField.Get(executionContext).Trim();
            var lookupName = LookupName.Get(executionContext).Trim();
            var targetEntity = TargetEntity.Get(executionContext).Trim();
            var targetField = TargetField.Get(executionContext).Trim();

            var lookupRef = GetLookupRef(service, lookupEntity, lookupField, lookupName);
            if (entityTarget != null && !String.IsNullOrEmpty(targetField))
            {
                var ent = new Entity(entityTarget.LogicalName, entityTarget.Id);
                ent.Attributes[targetField] = lookupRef;
                service.Update(ent);
            }*/


            string measureId = GetMeasureID(Target, modelNumberApprovalRef, objCommon);

            //EntityCollection measureTemplateList = GetMTList(modelNumberRef);

        }


        /*private EntityReference GetLookupRef(IOrganizationService service, string lookupEntity, string lookupField, string lookupName)
        {
            var query = new QueryExpression
            {
                EntityName = lookupEntity,
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = { new ConditionExpression(lookupField, ConditionOperator.Equal, lookupName) }
                }

            };
            var crmData = service.RetrieveMultiple(query);
            if (crmData.Entities.Count > 0)
            {
                return crmData.Entities[0].ToEntityReference();
            }
            else { return null; }

        }*/

        private string GetMeasureID(EntityReference modelNumberRef, EntityReference ModelNumberApprovalRef, Common objCommon)
        {
            //objCommon.TracingService.Trace("--0000001. GetMeasureID");
            //Get MN (mt and mn fields fill 1 or both)
            string measID = "";
            var ddsm_modelnumber = new Entity();

            //objCommon.TracingService.Trace("--0000002. GetMeasureID modelNumberRef.LogicalName: "+modelNumberRef.LogicalName);
            ddsm_modelnumber = objCommon.GetOrgService(systemCall: true).Retrieve(modelNumberRef.LogicalName, modelNumberRef.Id, new ColumnSet("ddsm_name", "ddsm_measuretemplateselectorforapproval", "ddsm_measurenumber"));
            //objCommon.TracingService.Trace("--0000003. After");

            string measureId = null;
            var ddsm_measuretemplate = new Entity();

            // object ddsm_measurenumber;
            //ddsm_modelnumber.Attributes.TryGetValue("ddsm_measurenumber", out ddsm_measurenumber);\
            string ddsm_measurenumber = ddsm_modelnumber.GetAttributeValue<string>("ddsm_measurenumber");

            object ddsm_measTmpl;
            ddsm_modelnumber.Attributes.TryGetValue("ddsm_measuretemplateselectorforapproval", out ddsm_measTmpl);
            //objCommon.TracingService.Trace("--0000003. GetMeasureID");
            //1. IF MeasureTemplate in ModelNumber <> null
            if (ddsm_measTmpl != null)
            {
                var measTmplRef = (EntityReference)ddsm_measTmpl;
                ddsm_measuretemplate = objCommon.GetOrgService(systemCall: true).Retrieve(measTmplRef.LogicalName, measTmplRef.Id, new ColumnSet("ddsm_measureid"));
                //objCommon.TracingService.Trace("--0000004. GetMeasureID");
            }
            else
            {
                //objCommon.TracingService.Trace("--0000005. GetMeasureID");
                //2. IF MeasureNumber in ModelNumber <> null
                if (ddsm_modelnumber != null)
                {
                    //objCommon.TracingService.Trace("--0000006. GetMeasureID");
                    var queryMTNumber = new QueryExpression()
                        {
                            EntityName = "ddsm_measuretemplate",
                            ColumnSet = new ColumnSet("ddsm_measuretemplateid","ddsm_measureid"),
                            Criteria = new FilterExpression(LogicalOperator.And)
                            {
                                Conditions =
                                {
                                    new ConditionExpression("ddsm_measurenumber", ConditionOperator.Equal, ddsm_measurenumber)
                                }
                            }
                        };
                        var mtFinded = objCommon.GetOrgService(systemCall: true).RetrieveMultiple(queryMTNumber);
                    //objCommon.TracingService.Trace("--0000007. GetMeasureID");

                    if (mtFinded.Entities.Count > 0)
                    {
                        ddsm_measuretemplate = mtFinded.Entities[0];
                    }
                    else
                    {
                        objCommon.TracingService.Trace("No Measures Tempaltes with MeasureNumber: " + ddsm_measurenumber);
                        //throw new Exception("No Measures Tempaltes with MeasureNumber: " + ddsm_measurenumber);
                    }
                    //objCommon.TracingService.Trace("--0000008. GetMeasureID");
                }

            }

            measureId = ddsm_measuretemplate.Attributes["ddsm_measureid"].ToString();
            //objCommon.TracingService.Trace("--0000009. GetMeasureID");
            //Get list of MT with same Measure ID
            var queryMT = new QueryExpression()
            {
                EntityName = "ddsm_measuretemplate",
                ColumnSet = new ColumnSet("ddsm_name", "ddsm_measuretemplateid"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_measureid", ConditionOperator.Equal, measureId)
                    }
                }
            };
            //objCommon.TracingService.Trace("--0000008. GetMeasureID");
            var mtList = objCommon.GetOrgService().RetrieveMultiple(queryMT);
            //objCommon.TracingService.Trace("--0000009. GetMeasureID");
            CreateMNA(mtList, modelNumberRef, ModelNumberApprovalRef, objCommon);

            return measID;
        }

        private void CreateMNA(EntityCollection mtList, EntityReference modelNumberRef, EntityReference modelNumberApprovalRef, Common objCommon)
        {
            if (mtList.Entities.Count > 1)
            {
                //Get MNA first created record in workflow
                var createdMNA = new Entity();
                createdMNA = objCommon.GetOrgService(systemCall: true).Retrieve("ddsm_modelnumberapproval", modelNumberApprovalRef.Id, new ColumnSet(true));
                object createdMeasTemplId;
                createdMNA.Attributes.TryGetValue("ddsm_measuretemplateid", out createdMeasTemplId);

                //TODO
                //Update 2 date fields of MNA
                object approvalStartDate;
                object approvalEndDate;
                createdMNA.Attributes.TryGetValue("ddsm_approvalstartdate", out approvalStartDate);
                createdMNA.Attributes.TryGetValue("ddsm_approvalenddate", out approvalEndDate);

                if (approvalStartDate==null)
                {
                    createdMNA["ddsm_approvalstartdate"] = DateTime.UtcNow;
                }

                if (approvalEndDate == null)
                {
                    createdMNA["ddsm_approvalenddate"] = DateTime.MaxValue.ToUniversalTime();
                }

                //createdMNA["ddsm_approvalstartdate"] = ;
                //createdMNA["ddsm_approvalenddate"] = ;

                //create empty MNA
                //objCommon.TracingService.Trace("--01.");
                int index = 1;//for update created MNA
                foreach (var mt in mtList.Entities)
                {

                    EntityReference mtLookup = new EntityReference();
                    mtLookup.Id = (Guid)mt.Attributes["ddsm_measuretemplateid"];
                    mtLookup.LogicalName = "ddsm_measuretemplate";
                    //TO DO need fix update MNA record if it critical update already filled with Measure Template and then create others!!!!
                    //objCommon.TracingService.Trace("--02."); 
                    if (index == 1)
                    {
                            createdMNA["ddsm_measuretemplateid"] = mtLookup;
                            objCommon.GetOrgService(systemCall: true).Update(createdMNA);
                    }
                    else
                    {
                        //objCommon.TracingService.Trace("--03.");
                        //Create(Clone) MNA from createdMNA with current ddsm_measuretemplateid
                        Entity mnaRecord = new Entity(createdMNA.LogicalName);
                        foreach (var attr in createdMNA.Attributes)
                        {
                            if (attr.Key != "ddsm_modelnumberapprovalid")
                            {
                                mnaRecord.Attributes.Add(attr);
                            }
                        }
                        // mnaRecord.Attributes.Remove("ddsm_modelnumberapprovalid");
                        mnaRecord["ddsm_measuretemplateid"] = mtLookup;
                        objCommon.GetOrgService(systemCall: true).Create(mnaRecord);
                    }
                    //objCommon.TracingService.Trace("--04.");
                    index++;
                    //objCommon.TracingService.Trace("--05.");
                }
            }
        }

    }
}
