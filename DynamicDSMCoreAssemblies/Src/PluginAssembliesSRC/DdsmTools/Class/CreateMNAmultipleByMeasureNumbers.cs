﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Crm.Sdk.Messages;

namespace DdsmTools
{

    public class CreateMNAmultipleByMeasureNumbers : CodeActivity
    {
        // Define an activity input argument of type string
        [Input("Model Number")]
        [ArgumentEntity("ddsm_modelnumber")]
        [ReferenceTarget("ddsm_modelnumber")]
        public InArgument<EntityReference> ModelNumber { get; set; }

        public enum CreateApproval
        {
            MeasureID = 962080000,
            MeasureNumber = 962080001,
            DoNotCreate = 962080002
        }
        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        //ddsm_createmnapproval
        protected override void Execute(CodeActivityContext executionContext)
        {
           
            var objCommon = new Common(executionContext);
            var Target = ModelNumber.Get(executionContext);
            objCommon.TracingService.Trace("--0000001. Start");

            var ddsm_modelnumber = new Entity();
            ddsm_modelnumber = objCommon.GetOrgService(systemCall: true).Retrieve(Target.LogicalName, Target.Id, new ColumnSet(true));
            //EntityReference newMNLookup = new EntityReference();
            objCommon.TracingService.Trace("--0000002.");

            object CreateMNApproval = null;  //OptionSet Create Approval
            if (!ddsm_modelnumber.Attributes.TryGetValue("ddsm_createmnapproval", out CreateMNApproval))
                return;
            objCommon.TracingService.Trace("--0000003.");


            //If there are any MN Approvals with such Measure Template and this Model Number which "Start Date Approval" = "Selectable End Date", should not create MN Approval.(FOR CONSULTATION)
            //ddsm_approvalstartdate
            object appStartDateMNobj;
            DateTime appStartDateMN = DateTime.UtcNow;
            if (ddsm_modelnumber.Attributes.TryGetValue("ddsm_approvalstartdate", out appStartDateMNobj))
            {
                appStartDateMN = (DateTime)appStartDateMNobj;
            }
            appStartDateMN = appStartDateMN.ToLocalTime();




            //Get Measure Number Text field with MeasureNumbers!!!...
            object MeasureNumberText;
            if (!ddsm_modelnumber.Attributes.TryGetValue("ddsm_measurenumber", out MeasureNumberText))
            {
                //If MeasureNumber is null check MeasureTemplate and use it for creating MNA!!!
                string condMTid = string.Empty;
                //Get MTlookup
                object MTLookup;
                if (!ddsm_modelnumber.Attributes.TryGetValue("ddsm_measuretemplateselectorforapproval", out MTLookup))
                    return;

                
                //Set condition with MTid
                condMTid = @"<condition attribute='ddsm_measuretemplateid' operator='eq' value='"+ ((EntityReference)MTLookup).Id.ToString() + "' />";
                /* foreach (var item in measureNumberList)
                 {
                     condMNumbers += $"<value>{item.Trim()}</value>";
                 }*/

                //MT Lookup not null + Create Approval = MeasureID + MeasureNumber=null
                if (((OptionSetValue)CreateMNApproval).Value == (int)CreateApproval.MeasureID)
                {
                    //Check Created MNA
                    string mtID = $"<value>" + Guid.Empty.ToString() + "</value>";// string.Empty;
                    EntityCollection MTList = GetMeasTemplatesByID(condMTid, objCommon, appStartDateMN);

                    string condMesureID = String.Empty;
                    object measID;
                    if (MTList.Entities.Count > 0)
                        if (MTList.Entities[0].Attributes.TryGetValue("ddsm_measureid", out measID))
                            condMesureID = measID.ToString();

                    EntityCollection MTListByMeasureID = GetMeasTemplatesByMeasureIDField(condMesureID, objCommon, appStartDateMN);

                    foreach (var mt in MTListByMeasureID.Entities)
                    {
                        object condVal;
                        if (mt.Attributes.TryGetValue("ddsm_measureid", out condVal))
                        {
                            mtID += $"<value>{condVal}</value>";
                        }
                    }
                    EntityCollection createMTListByMID = GetMeasTemplatesByMID(mtID, objCommon, appStartDateMN); //List of MT to create MNA

                    EntityCollection createdMNAList = GetCreatedMNA(ddsm_modelnumber, objCommon); //List of created MNA to compare with MT List;
                    if (createMTListByMID.Entities.Count != 0 && createdMNAList.Entities.Count != 0)
                    {
                        //List of createMTListByMID                   
                        Dictionary<Guid, Entity> list1 = new Dictionary<Guid, Entity>();

                        //List of MNA 
                        Dictionary<Guid, Entity> list2 = new Dictionary<Guid, Entity>();

                        foreach (var item in createMTListByMID.Entities)
                        {
                            list1.Add(item.Id, item);
                        }

                        foreach (var item in createdMNAList.Entities)
                        {
                            object mTLookup;
                            item.Attributes.TryGetValue("ddsm_measuretemplateid", out mTLookup);
                            if (mTLookup != null)
                            {
                                var id = ((EntityReference)item.Attributes["ddsm_measuretemplateid"]).Id;
                                list2.Add(id, item);
                            }
                        }

                        foreach (var item in list1)
                        {
                            if (list2.ContainsKey(item.Key))
                            {
                                //Update MNA
                                //TODO crete List with item that will be updated!
                                createMTListByMID.Entities.Remove(item.Value); //Remove from List to create MNA!
                            }
                            else
                            {
                                //Create MNA

                            }
                        }


                    }





                    //Create list of MNA
                    if (createMTListByMID.Entities.Count != 0)
                        CreateMNA(createMTListByMID, objCommon, ddsm_modelnumber, appStartDateMN);
                }
                //MT Lookup not null + Create Approval = MeasureNumber + MeasureNumber=null
                if (((OptionSetValue)CreateMNApproval).Value == (int)CreateApproval.MeasureNumber)
                {
                    //MNA FOR CREATE
                    EntityCollection createMTListByMLookup = GetMeasTemplatesByID(condMTid, objCommon, appStartDateMN);

                    //CREATED MNA
                    EntityCollection createdMNAList = GetCreatedMNA(ddsm_modelnumber, objCommon); //List of created MNA to compare with MT List;

                    if (createMTListByMLookup.Entities.Count != 0 && createdMNAList.Entities.Count != 0)
                    {
                        //List of createMTListByMID                   
                        Dictionary<Guid, Entity> list1 = new Dictionary<Guid, Entity>();

                        //List of MNA 
                        Dictionary<Guid, Entity> list2 = new Dictionary<Guid, Entity>();

                        foreach (var item in createMTListByMLookup.Entities)
                        {
                            list1.Add(item.Id, item);
                        }

                        foreach (var item in createdMNAList.Entities)
                        {
                            object mTLookup;
                            item.Attributes.TryGetValue("ddsm_measuretemplateid", out mTLookup);
                            if (mTLookup != null)
                            {
                                var id = ((EntityReference)item.Attributes["ddsm_measuretemplateid"]).Id;
                                list2.Add(id, item);
                            }
                        }

                        foreach (var item in list1)
                        {
                            if (list2.ContainsKey(item.Key))
                            {
                                //Update MNA
                                //TODO crete List with item that will be updated!
                                createMTListByMLookup.Entities.Remove(item.Value); //Remove from List to create MNA!
                            }
                            else
                            {
                                //Create MNA

                            }
                        }


                    }



                    //Create 1 MNA
                    if (createMTListByMLookup.Entities.Count != 0)
                        CreateMNA(createMTListByMLookup, objCommon, ddsm_modelnumber, appStartDateMN);
                }
                //return;
            }
            else
            {
                //If MeasureNumber not null use it or list of MeasureNumbers to create MNAs!!!




                objCommon.TracingService.Trace("--0000004.");
                //Get List of Measure Number from text field
                string[] measureNumberList = new string[] { };

                objCommon.TracingService.Trace("--0000005.");
                /*if (MeasureNumberText == null)
                    return;
                */


                objCommon.TracingService.Trace("--0000006.");
                measureNumberList = MeasureNumberText.ToString().Split(',');
                //Get String with conditions for fetch;
                string condMNumbers = string.Empty;
                foreach (var item in measureNumberList)
                {
                    condMNumbers += $"<value>{item.Trim()}</value>";
                }

                objCommon.TracingService.Trace("--0000007. Start");


                if (((OptionSetValue)CreateMNApproval).Value == (int)CreateApproval.MeasureID)
                {
                    string mtID = $"<value>"+Guid.Empty.ToString()+"</value>";// string.Empty;
                    EntityCollection MTList = GetMeasTemplatesByMeasureNum(condMNumbers, objCommon, appStartDateMN);

                    foreach (var mt in MTList.Entities)
                    {
                        object condVal;
                        if (mt.Attributes.TryGetValue("ddsm_measureid", out condVal))
                        {
                            mtID += $"<value>{condVal}</value>";
                        }
                    }
                    EntityCollection createMTListByMID = GetMeasTemplatesByMID(mtID, objCommon, appStartDateMN); //List of MT to create MNA

                    //----------------------------------------------------------------------------------------
                    //Remove existed(created)MT from List - if removed then update MNA
                    EntityCollection createdMNAList = GetCreatedMNA(ddsm_modelnumber, objCommon); //List of created MNA to compare with MT List;
                    if (createMTListByMID.Entities.Count != 0 && createdMNAList.Entities.Count != 0)
                    {
                        //List of createMTListByMID                   
                        Dictionary<Guid, Entity> list1 = new Dictionary<Guid, Entity>();

                        //List of MNA 
                        Dictionary<Guid, Entity> list2 = new Dictionary<Guid, Entity>();

                        foreach (var item in createMTListByMID.Entities)
                        {
                            list1.Add(item.Id, item);
                        }

                        foreach (var item in createdMNAList.Entities)
                        {
                            object mTLookup;
                            item.Attributes.TryGetValue("ddsm_measuretemplateid", out mTLookup);
                            if (mTLookup != null)
                            {
                                var id = ((EntityReference)item.Attributes["ddsm_measuretemplateid"]).Id;
                                list2.Add(id, item);
                            }
                        }

                        foreach (var item in list1)
                        {
                            if (list2.ContainsKey(item.Key))
                            {
                                //Update MNA
                                //TODO crete List with item that will be updated!
                                createMTListByMID.Entities.Remove(item.Value); //Remove from List to create MNA!
                            }
                            else
                            {
                                //Create MNA

                            }
                        }


                    }
                    //----------------------------------------------------------------------------------------
                    if (createMTListByMID.Entities.Count != 0)
                        CreateMNA(createMTListByMID, objCommon, ddsm_modelnumber, appStartDateMN);

                }
                if (((OptionSetValue)CreateMNApproval).Value == (int)CreateApproval.MeasureNumber)
                {
                    //Get List of MT for creating MNA

                    EntityCollection createMTListByMN = GetMeasTemplatesByMeasureNum(condMNumbers, objCommon, appStartDateMN);
                    //----------------------------------------------------------------------------------------
                    //Remove existed(created)MT from List - if removed then update MNA
                    EntityCollection createdMNAList = GetCreatedMNA(ddsm_modelnumber, objCommon); //List of created MNA to compare with MT List;
                    if (createMTListByMN.Entities.Count != 0 && createdMNAList.Entities.Count != 0)
                    {
                        //List of createMTListByMID                   
                        Dictionary<Guid, Entity> list1 = new Dictionary<Guid, Entity>();

                        //List of MNA 
                        Dictionary<Guid, Entity> list2 = new Dictionary<Guid, Entity>();

                        foreach (var item in createMTListByMN.Entities)
                        {
                            list1.Add(item.Id, item);
                        }

                        foreach (var item in createdMNAList.Entities)
                        {
                            object mTLookup;
                            item.Attributes.TryGetValue("ddsm_measuretemplateid", out mTLookup);
                            if (mTLookup != null)
                            {
                                var id = ((EntityReference)item.Attributes["ddsm_measuretemplateid"]).Id;
                                list2.Add(id, item);
                            }
                        }

                        foreach (var item in list1)
                        {
                            if (list2.ContainsKey(item.Key))
                            {
                                //Update MNA
                                //TODO crete List with item that will be updated!
                                createMTListByMN.Entities.Remove(item.Value); //Remove from List to create MNA!
                            }
                            else
                            {
                                //Create MNA

                            }
                        }


                    }
                    //----------------------------------------------------------------------------------------





                    if (createMTListByMN.Entities.Count != 0)
                        CreateMNA(createMTListByMN, objCommon, ddsm_modelnumber, appStartDateMN);


                }
            }
            if (((OptionSetValue)CreateMNApproval).Value == (int)CreateApproval.DoNotCreate)
            {
                return;
            }

        }

        private void CreateMNA(EntityCollection createMTList, Common objCommon, Entity ddsm_modelnumber, DateTime appStartDateMN)
        {
            Entity MNA = new Entity("ddsm_modelnumberapproval"); //Entity with common attributes for all SKUA
            
            MNA["ddsm_name"] = ddsm_modelnumber["ddsm_name"];
            MNA["ddsm_modelnumberid"] = new EntityReference("ddsm_modelnumber", new Guid(ddsm_modelnumber["ddsm_modelnumberid"].ToString())); 
            MNA["ddsm_approvalstartdate"] = appStartDateMN;
            

            //Set fileds for MNA and create records.
            foreach (var item in createMTList.Entities)
            {
                MNA.Id = new Guid();
                EntityReference lookupMT = new EntityReference()
                {
                    LogicalName = "ddsm_measuretemplate",
                    Id = new Guid(item.Attributes["ddsm_measuretemplateid"].ToString())
                };
                MNA["ddsm_measuretemplateid"] = lookupMT;
                //Set MNA.endApprovalDate
                object endApprovalDateMT; // End Approval Date from Measure template to set to MNA
                if (item.Attributes.TryGetValue("ddsm_selectableenddate", out endApprovalDateMT))
                {
                    object endApprovalDateMN;
                    if (ddsm_modelnumber.Attributes.TryGetValue("ddsm_approvalenddate", out endApprovalDateMN))
                    {
                        if ((DateTime)endApprovalDateMN < (DateTime)endApprovalDateMT)
                        {
                            MNA["ddsm_approvalenddate"] = endApprovalDateMN;
                        }
                        else
                        {
                            MNA["ddsm_approvalenddate"] = endApprovalDateMT;
                        }
                    }
                    else
                    {
                        MNA["ddsm_approvalenddate"] = endApprovalDateMT;
                    }
                }


                var createdRecord = objCommon.GetOrgService().Create(MNA);

                var relationship = new Relationship();
                relationship.SchemaName = "ddsm_ddsm_modelnumber_ddsm_modelnumberapproval";

                var query = new QueryExpression();
                query.EntityName = "ddsm_modelnumber";
                query.ColumnSet = new ColumnSet("ddsm_modelnumberid");
                query.Criteria = new FilterExpression();
                query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

                var relatedEntity = new RelationshipQueryCollection();
                relatedEntity.Add(relationship, query);

                var request = new RetrieveRequest();
                request.ColumnSet = new ColumnSet("ddsm_modelnumberapprovalid", "ddsm_name");
                request.Target = new EntityReference { Id = new Guid(createdRecord.ToString()), LogicalName = "ddsm_modelnumberapproval" };
                request.RelatedEntitiesQuery = relatedEntity;

                var response = (RetrieveResponse)objCommon.GetOrgService().Execute(request);

                var RelatedEntitis = response.Entity.RelatedEntities[new Relationship("ddsm_ddsm_modelnumber_ddsm_modelnumberapproval")].Entities;
                foreach (var RelatedEntity in RelatedEntitis)
                {
                    var rmEntity = GetDataRelationMapping(objCommon.GetOrgService(), "ddsm_modelnumberapproval","ddsm_modelnumber", new Guid(RelatedEntity.Id.ToString()));
                    if (rmEntity != null)
                    {
                        foreach (var Attribute in rmEntity.Attributes)
                        {
                            MNA[Attribute.Key] = Attribute.Value;
                        }
                    }
                    MNA.Id = createdRecord;
                    MNA.LogicalName = "ddsm_modelnumberapproval";
                    objCommon.GetOrgService().Update(MNA);
                }


                


                //Retrieve the One-to-many relationship using the MetadataId.
                /*RetrieveRelationshipRequest retrieveOneToManyRequest =
                    new RetrieveRelationshipRequest { Name = "ddsm_ddsm_modelnumber_ddsm_modelnumberapproval" };
                RetrieveRelationshipResponse retrieveOneToManyResponse =
                    (RetrieveRelationshipResponse)objCommon.GetOrgService().Execute(retrieveOneToManyRequest);*/

            }
        }

        public static Entity GetDataRelationMapping(IOrganizationService _orgService, string targetEntity, string sourceEntity, Guid sourceGuid)
        {
            var initialize = new InitializeFromRequest();
            initialize.TargetEntityName = targetEntity;
            initialize.EntityMoniker = new EntityReference(sourceEntity, sourceGuid);
            initialize.TargetFieldType = TargetFieldType.All;
            var initialized = (InitializeFromResponse)_orgService.Execute(initialize);
            return initialized.Entity;
        }

        private EntityCollection GetMeasTemplatesByMeasureNum(string condMNumbers, Common objCommon, DateTime appStartDateMN)
        {
            
            string dateCondStart = String.Format(@"<condition attribute='ddsm_selectablestartdate' operator='le' value='{0}' />", appStartDateMN.AddDays(1).ToString("MM/dd/yyyy"));
            string dateCondEnd = String.Format(@"<condition attribute='ddsm_selectableenddate' operator='ge' value='{0}' />", appStartDateMN.ToString("MM/dd/yyyy"));

            var fetchQueryMeasNumb = @"<fetch>
                                      <entity name='ddsm_measuretemplate' >
                                        <filter type='and' >
                                          <condition attribute='ddsm_measurenumber' operator='in' >
                                           {0}
                                          </condition>
                                          {1}
                                          {2}
                                          <condition attribute='statecode' operator='eq' value='0' />
                                          <condition attribute='statuscode' operator='eq' value='1' />
                                        </filter>
                                      </entity>
                                    </fetch>";
            fetchQueryMeasNumb = String.Format(fetchQueryMeasNumb, condMNumbers, dateCondStart, dateCondEnd);

            var query = new FetchExpression(fetchQueryMeasNumb);
            return objCommon.GetOrgService().RetrieveMultiple(query);
        }

        private EntityCollection GetMeasTemplatesByMID(string condMID, Common objCommon, DateTime appStartDateMN)
        {
            string dateCondStart = String.Format(@"<condition attribute='ddsm_selectablestartdate' operator='le' value='{0}' />", appStartDateMN.AddDays(1).ToString("MM/dd/yyyy"));
            string dateCondEnd = String.Format(@"<condition attribute='ddsm_selectableenddate' operator='ge' value='{0}' />", appStartDateMN.ToString("MM/dd/yyyy"));
            var fetchQueryMeasNumb = @"<fetch>
                                      <entity name='ddsm_measuretemplate' >
                                        <filter type='and' >
                                          <condition attribute='ddsm_measureid' operator='in' >
                                           {0}
                                          </condition>
                                          {1}
                                          {2}
                                          <condition attribute='statecode' operator='eq' value='0' />
                                          <condition attribute='statuscode' operator='eq' value='1' />
                                        </filter>
                                      </entity>
                                    </fetch>";
            fetchQueryMeasNumb = String.Format(fetchQueryMeasNumb, condMID, dateCondStart, dateCondEnd);

            var query = new FetchExpression(fetchQueryMeasNumb);
            return objCommon.GetOrgService().RetrieveMultiple(query);
        }

        private EntityCollection GetCreatedMNA(Entity ddsm_modelnumber, Common objCommon)
        {
            string mnId = ddsm_modelnumber.Id.ToString();
            var fetchQueryMeasNumb = @"<fetch>
                                          <entity name='ddsm_modelnumberapproval' >
                                            <filter type='and' >
                                              <condition attribute='ddsm_modelnumberid' operator='eq' value='{0}' />
                                              <condition attribute='statecode' operator='eq' value='0' />
                                              <condition attribute='statuscode' operator='eq' value='1' />
                                            </filter>
                                          </entity>
                                        </fetch>";
            fetchQueryMeasNumb = String.Format(fetchQueryMeasNumb, mnId);

            var query = new FetchExpression(fetchQueryMeasNumb);
            return objCommon.GetOrgService().RetrieveMultiple(query);
        }

        private EntityCollection GetMeasTemplatesByID(string condMTid, Common objCommon, DateTime appStartDateMN)
        {

            string dateCondStart = String.Format(@"<condition attribute='ddsm_selectablestartdate' operator='le' value='{0}' />", appStartDateMN.AddDays(1).ToString("MM/dd/yyyy"));
            string dateCondEnd = String.Format(@"<condition attribute='ddsm_selectableenddate' operator='ge' value='{0}' />", appStartDateMN.ToString("MM/dd/yyyy"));

            var fetchQueryMeasNumb = @"<fetch>
                                      <entity name='ddsm_measuretemplate' >
                                        <filter type='and' >
                                          {0}
                                          {1}
                                          {2}
                                          <condition attribute='statecode' operator='eq' value='0' />
                                          <condition attribute='statuscode' operator='eq' value='1' />
                                        </filter>
                                      </entity>
                                    </fetch>";
            fetchQueryMeasNumb = String.Format(fetchQueryMeasNumb, condMTid, dateCondStart, dateCondEnd);

            var query = new FetchExpression(fetchQueryMeasNumb);
            return objCommon.GetOrgService().RetrieveMultiple(query);
        }

        private EntityCollection GetMeasTemplatesByMeasureIDField(string condMesureID, Common objCommon, DateTime appStartDateMN)
        {
            if (!String.IsNullOrEmpty(condMesureID))
            {
                condMesureID = @"<condition attribute='ddsm_measureid' operator='eq' value='"+condMesureID+"' />";
            }

            var fetchQueryMeasT = @"<fetch>
                                      <entity name='ddsm_measuretemplate' >
                                        <filter type='and' >
                                          {0}
                                          <condition attribute='statecode' operator='eq' value='0' />
                                          <condition attribute='statuscode' operator='eq' value='1' />
                                        </filter>
                                      </entity>
                                    </fetch>";
            fetchQueryMeasT = String.Format(fetchQueryMeasT, condMesureID);

            var query = new FetchExpression(fetchQueryMeasT);
            return objCommon.GetOrgService().RetrieveMultiple(query);
        }
    }
}

