﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.ServiceModel;
using Newtonsoft.Json;
using DDSM.CommonProvider;

namespace DdsmTools
{

    public class CreateSKUAmultipleByMeasureID : CodeActivity
    {
        /*[RequiredArgument]*/
        [Input("SKU Approval")]
        [ArgumentEntity("ddsm_skuapproval")]
        [ReferenceTarget("ddsm_skuapproval")]
        public InArgument<EntityReference> SKUApproval { get; set; }

        /*[RequiredArgument]*/
        [Input("SKU")]
        [ArgumentEntity("ddsm_sku")]
        [ReferenceTarget("ddsm_sku")]
        public InArgument<EntityReference> SKU { get; set; }

        //Common objCommon;
        protected override void Execute(CodeActivityContext executionContext)
        {
            var objCommon = new Common(executionContext);

            var Target = SKU.Get(executionContext);
            var skuApprovalRef = SKUApproval.Get(executionContext);
            string measureId = GetMeasureID(Target, skuApprovalRef, objCommon);
        }

        private string GetMeasureID(EntityReference skuRef, EntityReference skuApprovalRef, Common objCommon)
        {
            string measID = "";
            var ddsm_sku = new Entity();
            ddsm_sku = objCommon.GetOrgService(systemCall: true).Retrieve(skuRef.LogicalName, skuRef.Id, new ColumnSet("ddsm_name", "ddsm_measuretemplateselectorforapproval", "ddsm_measurenumber"));

            string measureId = null;
            var ddsm_measuretemplate = new Entity();

            string ddsm_measurenumber = ddsm_sku.GetAttributeValue<string>("ddsm_measurenumber");

            object ddsm_measTmpl;
            ddsm_sku.Attributes.TryGetValue("ddsm_measuretemplateselectorforapproval", out ddsm_measTmpl);

            //1. IF MeasureTemplate in ModelNumber <> null
            if (ddsm_measTmpl != null)
            {
                var measTmplRef = (EntityReference)ddsm_measTmpl;
                ddsm_measuretemplate = objCommon.GetOrgService(systemCall: true).Retrieve(measTmplRef.LogicalName, measTmplRef.Id, new ColumnSet("ddsm_measureid"));
            }
            else
            {
                //2. IF MeasureNumber in ModelNumber <> null
                if (ddsm_sku != null)
                {
                    var queryMTNumber = new QueryExpression()
                        {
                            EntityName = "ddsm_measuretemplate",
                            ColumnSet = new ColumnSet("ddsm_measuretemplateid","ddsm_measureid"),
                            Criteria = new FilterExpression(LogicalOperator.And)
                            {
                                Conditions =
                                {
                                    new ConditionExpression("ddsm_measurenumber", ConditionOperator.Equal, ddsm_measurenumber)
                                }
                            }
                        };
                        var mtFinded = objCommon.GetOrgService(systemCall: true).RetrieveMultiple(queryMTNumber);
                    if (mtFinded.Entities.Count > 0)
                    {
                        ddsm_measuretemplate = mtFinded.Entities[0];
                    }
                    else
                    {
                        throw new Exception("No Measures Tempaltes with MeasureNumber: " + ddsm_measurenumber);
                    }
                }

            }

            measureId = ddsm_measuretemplate.Attributes["ddsm_measureid"].ToString();
            //Get list of MT with same Measure ID
            var queryMT = new QueryExpression()
            {
                EntityName = "ddsm_measuretemplate",
                ColumnSet = new ColumnSet("ddsm_name", "ddsm_measuretemplateid"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_measureid", ConditionOperator.Equal, measureId)
                    }
                }
            };
            var mtList = objCommon.GetOrgService().RetrieveMultiple(queryMT);
            CreateMNA(mtList, skuRef, skuApprovalRef, objCommon);

            return measID;
        }

        private void CreateMNA(EntityCollection mtList, EntityReference skuRef, EntityReference skuApprovalRef, Common objCommon)
        {
            if (mtList.Entities.Count > 1)
            {
                //Get MNA first created record in workflow
                var createdSKUA = new Entity();
                createdSKUA = objCommon.GetOrgService(systemCall: true).Retrieve("ddsm_skuapproval", skuApprovalRef.Id, new ColumnSet(true));
                object createdMeasTemplId;
                createdSKUA.Attributes.TryGetValue("ddsm_measuretemplateid", out createdMeasTemplId);

                //create empty MNA
                int index = 1;//for update created MNA
                foreach (var mt in mtList.Entities)
                {

                    EntityReference mtLookup = new EntityReference();
                    mtLookup.Id = (Guid)mt.Attributes["ddsm_measuretemplateid"];
                    mtLookup.LogicalName = "ddsm_measuretemplate";
                    //TO DO need fix update MNA record if it critical update already filled with Measure Template and then create others!!!!
                    if (index == 1)
                    {
                        createdSKUA["ddsm_measuretemplateid"] = mtLookup;
                            objCommon.GetOrgService(systemCall: true).Update(createdSKUA);
                    }
                    else
                    {
                        //Create(Clone) MNA from createdMNA with current ddsm_measuretemplateid
                        Entity skuaRecord = new Entity(createdSKUA.LogicalName);
                        foreach (var attr in createdSKUA.Attributes)
                        {
                            if (attr.Key != "ddsm_skuapprovalid")
                            {
                                skuaRecord.Attributes.Add(attr);
                            }
                        }
                        // mnaRecord.Attributes.Remove("ddsm_modelnumberapprovalid");
                        skuaRecord["ddsm_measuretemplateid"] = mtLookup;
                        objCommon.GetOrgService(systemCall: true).Create(skuaRecord);
                    }
                    index++;
                }
            }
        }

    }
}
