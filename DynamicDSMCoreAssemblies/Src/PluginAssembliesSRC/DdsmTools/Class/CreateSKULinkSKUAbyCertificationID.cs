﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.ServiceModel;
using Newtonsoft.Json;
using DDSM.CommonProvider;

namespace DdsmTools
{

    public class CreateSKULinkSKUAbyCertificationID : CodeActivity
    {
        /*[RequiredArgument]*/
        [Input("SKU")]
        [ArgumentEntity("ddsm_sku")]
        [ReferenceTarget("ddsm_sku")]
        public InArgument<EntityReference> SKU { get; set; }

        public enum CreateLinktoMN
        {
            CreateNotOverride = 962080000,
            CreateAndOverride = 962080001,
            DoNotCreate = 962080002
        }
        public enum CreateApprovalSKU
        {
            Create = 962080000,
            DoNotCreate = 962080001
        }
        protected override void Execute(CodeActivityContext executionContext)
        {
            var objCommon = new Common(executionContext);
            var Target = SKU.Get(executionContext);

            var ddsm_sku = new Entity();
            ddsm_sku = objCommon.GetOrgService(systemCall: true).Retrieve(Target.LogicalName, Target.Id, new ColumnSet(true));
            EntityReference newMNLookup = new EntityReference();


            object linkToMNOptionSet = null;  //OptionSet CreateLinktoModelNumber
            object mnLookup;
            if (ddsm_sku.Attributes.TryGetValue("ddsm_modelnumber", out mnLookup))
            {
                newMNLookup = (EntityReference)mnLookup;
            }
            //If Create Link to MN = Create and Override - update SKU
            if (ddsm_sku.Attributes.TryGetValue("ddsm_createlinktomodelnumber", out linkToMNOptionSet))
            {
                //////////////////////////////////////
                //CreateNotOverride
                if (((OptionSetValue)linkToMNOptionSet).Value == (int)CreateLinktoMN.CreateNotOverride)
                {
                    //NOT Update SKU
                    //Check ddsm_modelnumberid 
                    //if null set new from from ddsm_modelnumbertext
                    object oldMNLookupID;
                    if (ddsm_sku.Attributes.TryGetValue("ddsm_modelnumber", out oldMNLookupID))
                    {

                        newMNLookup.Id = ((EntityReference)oldMNLookupID).Id;//(Guid)oldMNLookupID;
                        newMNLookup.LogicalName = "ddsm_modelnumber";
                    }
                    else
                    {
                        object mnTextNew;
                        object CertIDNew;
                        EntityCollection mnListNew = new EntityCollection();
                        if (ddsm_sku.Attributes.TryGetValue("ddsm_certificationid", out CertIDNew))
                        {
                            //Find MN(EntityReference) queryexpr by ddsm_certificationid
                            var queryMN = new QueryExpression()
                            {
                                EntityName = "ddsm_modelnumber",
                                ColumnSet = new ColumnSet("ddsm_name", "ddsm_modelnumberid"),
                                Criteria = new FilterExpression(LogicalOperator.And)
                                {
                                    Conditions =
                            {
                                new ConditionExpression("ddsm_certificationid", ConditionOperator.Equal, CertIDNew)
                            }
                                }
                            };
                            mnListNew = objCommon.GetOrgService().RetrieveMultiple(queryMN);
                        }
                        if (mnListNew.Entities.Count == 0)
                        {
                            if (ddsm_sku.Attributes.TryGetValue("ddsm_modelnumbertext", out mnTextNew))
                            {
                                //Update SKU

                                //Find MN(EntityReference) queryexpr by ddsm_modelnumbertext
                                var queryMN = new QueryExpression()
                                {
                                    EntityName = "ddsm_modelnumber",
                                    ColumnSet = new ColumnSet("ddsm_name", "ddsm_modelnumberid"),
                                    Criteria = new FilterExpression(LogicalOperator.And)
                                    {
                                        Conditions =
                                    {
                                        new ConditionExpression("ddsm_name", ConditionOperator.Equal, mnTextNew)
                                    }
                                    }
                                };
                                mnListNew = objCommon.GetOrgService().RetrieveMultiple(queryMN);
                            }
                        }
                        //MN List
                        if (mnListNew.Entities.Count == 1)
                        {
                            object newMNLookupID;
                            if (mnListNew.Entities[0].Attributes.TryGetValue("ddsm_modelnumberid", out newMNLookupID))
                            {

                                newMNLookup.Id = (Guid)newMNLookupID;
                                newMNLookup.LogicalName = "ddsm_modelnumber";


                                ddsm_sku["ddsm_modelnumber"] = newMNLookup;
                                objCommon.GetOrgService(systemCall: true).Update(ddsm_sku);
                                objCommon.TracingService.Trace("01-1 SKU recoerd updated: " + mnListNew.Entities[0].Attributes["ddsm_name"]);
                            }

                        }

                        //newMNLookup.Id = (Guid)oldMNLookupID;
                        //newMNLookup.LogicalName = "ddsm_modelnumber";
                    }

                    objCommon.TracingService.Trace("01 SKU recoerd not updated.");
                }
                //////////////////////////////////////
                //CreateAndOverride
                if (((OptionSetValue)linkToMNOptionSet).Value == (int)CreateLinktoMN.CreateAndOverride)
                {
                    object mnText;
                    object CertID;
                    EntityCollection mnList = new EntityCollection();
                    if (ddsm_sku.Attributes.TryGetValue("ddsm_certificationid", out CertID))
                    {
                        //Find MN(EntityReference) queryexpr by ddsm_certificationid
                        var queryMN = new QueryExpression()
                        {
                            EntityName = "ddsm_modelnumber",
                            ColumnSet = new ColumnSet("ddsm_name", "ddsm_modelnumberid"),
                            Criteria = new FilterExpression(LogicalOperator.And)
                            {
                                Conditions =
                            {
                                new ConditionExpression("ddsm_certificationid", ConditionOperator.Equal, CertID)
                            }
                            }
                        };
                        mnList = objCommon.GetOrgService().RetrieveMultiple(queryMN);
                    }
                    if (mnList.Entities.Count==0)
                    {
                        if (ddsm_sku.Attributes.TryGetValue("ddsm_modelnumbertext", out mnText))
                        {
                            //Update SKU

                            //Find MN(EntityReference) queryexpr by ddsm_modelnumbertext
                            var queryMN = new QueryExpression()
                            {
                                EntityName = "ddsm_modelnumber",
                                ColumnSet = new ColumnSet("ddsm_name", "ddsm_modelnumberid"),
                                Criteria = new FilterExpression(LogicalOperator.And)
                                {
                                    Conditions =
                                    {
                                        new ConditionExpression("ddsm_name", ConditionOperator.Equal, mnText)
                                    }
                                }
                            };
                            mnList = objCommon.GetOrgService().RetrieveMultiple(queryMN);
                        }
                    }
                        //MN List
                        if (mnList.Entities.Count==1)
                        {
                            object newMNLookupID;
                            if (mnList.Entities[0].Attributes.TryGetValue("ddsm_modelnumberid", out newMNLookupID))
                            {
                                
                                newMNLookup.Id = (Guid)newMNLookupID;
                                newMNLookup.LogicalName = "ddsm_modelnumber";


                                ddsm_sku["ddsm_modelnumber"] = newMNLookup;
                                objCommon.GetOrgService(systemCall: true).Update(ddsm_sku);
                                objCommon.TracingService.Trace("02 SKU recoerd updated: "+ mnList.Entities[0].Attributes["ddsm_name"]);
                            }
                            
                        }
                        else
                        {
                            objCommon.TracingService.Trace("03 SKU recoerd not updated, found more than 1 Model Number or any Model Number not found.");
                        }
                        objCommon.TracingService.Trace("04 SKU recoerd was Processed.");

                }
                //////////////////////////////////////
                //DonotCreate
                if (((OptionSetValue)linkToMNOptionSet).Value == (int)CreateLinktoMN.DoNotCreate)
                {

                    objCommon.TracingService.Trace("05 SKU recoerd was Processed. SKU not updated.");
                    //Nothing to do
                }
            }
            //UpdateSKU(ddsm_sku);
            CreateSKUA(ddsm_sku, newMNLookup, objCommon);

        }

        private void CreateSKUA(Entity ddsm_sku, EntityReference newMNLookup, Common objCommon)
        {
            //If Create Approval = Create - create SKUA records
            

            object CreateApprovalSKUOptionSet = null; //OptionSet CreateApprovalSKU
            if (ddsm_sku.Attributes.TryGetValue("ddsm_createapprovalsku", out CreateApprovalSKUOptionSet))
            {
                //Create
                if (((OptionSetValue)CreateApprovalSKUOptionSet).Value == (int)CreateApprovalSKU.Create)
                {
                    //Create list of SKUA records for creating
                    EntityCollection skuAList = new EntityCollection(); //List of all SKUA for create
                    EntityCollection skuAListUpdate = new EntityCollection(); //List of all SKUA fro update
                    Entity SKUA = new Entity("ddsm_skuapproval"); //Entity with common attributes for all SKUA
                    EntityReference newSKULookup = new EntityReference("ddsm_sku", (Guid)ddsm_sku["ddsm_skuid"]); //new SKU reference

                    //Common attributes
                    SKUA["ddsm_name"] = ddsm_sku["ddsm_name"];
                    SKUA["ddsm_skuid"] = newSKULookup;
                    object appStatus;
                    if (ddsm_sku.Attributes.TryGetValue("ddsm_approvalstatus", out appStatus))
                    {
                        SKUA["ddsm_approvalstatus"] = appStatus;
                    }



                    //TODO
                    //Update 2 date fields of MNA
                    object approvalStartDate;
                    object approvalEndDate;
                    ddsm_sku.Attributes.TryGetValue("ddsm_startdate", out approvalStartDate);
                    ddsm_sku.Attributes.TryGetValue("ddsm_enddate", out approvalEndDate);

                    if (approvalStartDate == null)
                    {
                        SKUA["ddsm_approvalstartdate"] = DateTime.UtcNow;
                    }
                    else
                    {
                        SKUA["ddsm_approvalstartdate"] = approvalStartDate;
                    }

                    if (approvalEndDate == null)
                    {
                        SKUA["ddsm_approvalenddate"] = DateTime.MaxValue.ToUniversalTime();
                    }
                    else
                    {
                        SKUA["ddsm_approvalenddate"] = approvalEndDate;
                    }

                    //TODO
                    //Update 2 fields ddsm_maxapprovedrebateamount, ddsm_partner
                    object partner;
                    object maxapprovedrebateamount;
                    ddsm_sku.Attributes.TryGetValue("ddsm_partner", out partner);
                    ddsm_sku.Attributes.TryGetValue("ddsm_maxapprovedrebateamount", out maxapprovedrebateamount);

                    if (partner != null)
                    {
                        SKUA["ddsm_partner"] = partner;
                    }
                    if (maxapprovedrebateamount != null)
                    {
                        SKUA["ddsm_maxapprovedrebateamount"] = maxapprovedrebateamount;
                    }

                    //TODO
                    //Update fields 
                    /*
                    ddsm_skudescription
                    ddsm_approvedby
                    ddsm_forecastsalesvolume
                    ddsm_quantityperpackage
                    ddsm_packagetype
                    ddsm_retailprice
                    ddsm_saleprice
                    ddsm_rebatesaleprice
                    ddsm_partnernotes
                    ddsm_deliveryagentnotes
                    ddsm_internalnotes
                    */
                    object skudescription;
                    object approvedby;
                    object forecastsalesvolume;
                    object quantityperpackage;
                    object packagetype;
                    object retailprice;
                    object saleprice;
                    object rebateprice;
                    object rebatesaleprice;
                    object partnernotes;
                    object deliveryagentnotes;
                    object internalnotes;
                    ddsm_sku.Attributes.TryGetValue("ddsm_skudescription", out skudescription);
                    ddsm_sku.Attributes.TryGetValue("ddsm_approvedby", out approvedby);
                    ddsm_sku.Attributes.TryGetValue("ddsm_forecastsalesvolume", out forecastsalesvolume);
                    ddsm_sku.Attributes.TryGetValue("ddsm_quantityperpackage", out quantityperpackage);
                    ddsm_sku.Attributes.TryGetValue("ddsm_packagetype", out packagetype);
                    ddsm_sku.Attributes.TryGetValue("ddsm_retailprice", out retailprice);
                    ddsm_sku.Attributes.TryGetValue("ddsm_saleprice", out saleprice);
                    ddsm_sku.Attributes.TryGetValue("ddsm_rebateprice", out rebateprice);
                    ddsm_sku.Attributes.TryGetValue("ddsm_rebatesaleprice", out rebatesaleprice);
                    ddsm_sku.Attributes.TryGetValue("ddsm_partnernotes", out partnernotes);
                    ddsm_sku.Attributes.TryGetValue("ddsm_deliveryagentnotes", out deliveryagentnotes);
                    ddsm_sku.Attributes.TryGetValue("ddsm_internalnotes", out internalnotes);
                    if (skudescription != null)
                    {
                        SKUA["ddsm_skudescription"] = skudescription;
                    }
                    if (approvedby != null)
                    {
                        SKUA["ddsm_approvedby"] = approvedby;
                    }
                    if (forecastsalesvolume != null)
                    {
                        SKUA["ddsm_forecastsalesvolume"] = forecastsalesvolume;
                    }
                    if (quantityperpackage != null)
                    {
                        SKUA["ddsm_quantityperpackage"] = quantityperpackage;
                    }
                    if (packagetype != null)
                    {
                        SKUA["ddsm_packagetype"] = packagetype;
                    }
                    if (retailprice != null)
                    {
                        SKUA["ddsm_retailprice"] = retailprice;
                    }
                    if (saleprice != null)
                    {
                        SKUA["ddsm_saleprice"] = saleprice;
                    }
                    if (rebateprice != null)
                    {
                        SKUA["ddsm_rebateprice"] = rebateprice;
                    }
                    if (rebatesaleprice != null)
                    {
                        SKUA["ddsm_rebatesaleprice"] = rebatesaleprice;
                    }
                    if (partnernotes != null)
                    {
                        SKUA["ddsm_partnernotes"] = partnernotes;
                    }
                    if (deliveryagentnotes != null)
                    {
                        SKUA["ddsm_deliveryagentnotes"] = deliveryagentnotes;
                    }
                    if (internalnotes != null)
                    {
                        SKUA["ddsm_internalnotes"] = internalnotes;
                    }

                    //Get List of MNA with MN = newMNLookup
                    EntityCollection mnaRecords = new EntityCollection();
                    
                    var queryMNA = new QueryExpression()
                    {
                        EntityName = "ddsm_modelnumberapproval",
                        ColumnSet = new ColumnSet("ddsm_name", "ddsm_modelnumberapprovalid", "ddsm_measuretemplateid"),
                        Criteria = new FilterExpression(LogicalOperator.And)
                        {
                            Conditions =
                                    {
                                        new ConditionExpression("ddsm_modelnumberid", ConditionOperator.Equal, newMNLookup.Id)
                                    }
                        }
                    };
                    mnaRecords = objCommon.GetOrgService().RetrieveMultiple(queryMNA);

                    //Get List of SKUA if exists
                    EntityCollection skuaExistedRecords = new EntityCollection();
                    var querySKUA = new QueryExpression()
                    {
                      EntityName = "ddsm_skuapproval",
                      ColumnSet = new ColumnSet("ddsm_name", "ddsm_skuid", "ddsm_modelnumberapprovalid", "ddsm_approvalstatus"),
                      Criteria = new FilterExpression(LogicalOperator.And)
                      {
                          Conditions =
                          {
                              new ConditionExpression("ddsm_skuid", ConditionOperator.Equal, newSKULookup.Id)
                          }
                      }
                    };
                    skuaExistedRecords = objCommon.GetOrgService().RetrieveMultiple(querySKUA);

                    Dictionary<Guid, Entity> list1 = new Dictionary<Guid, Entity>();

                    Dictionary<Guid,Entity> list2 = new Dictionary<Guid, Entity>();

                    foreach (var item in mnaRecords.Entities) {
                        list1.Add(item.Id,item);
                    }

                    foreach (var item in skuaExistedRecords.Entities)
                    {
                        var id = ((EntityReference)item.Attributes["ddsm_modelnumberapprovalid"]).Id;
                        list2.Add(id,item);
                    }

                    //List<Guid> resultCrete = new List<Guid>();

                    //List<Guid> resultUpdate= new List<Guid>();

                    //foreach (var item in list1)
                    //{
                    //    if (!list2.ContainsKey(item.Key)) {
                    //        resultCrete.Add(item.Value);
                    //    }
                    //    resultUpdate.Add(item.Value);
                    //}

                    //List<Guid> ListTMP = new List<Guid>();
                    EntityReference newMNALookup = new EntityReference();
                    object newMT = new EntityReference();
                    foreach (var item in list1)
                    {
                        if (list2.ContainsKey(item.Key)) {

                            //var a = ()list2[item.Key];
                            newMNALookup.Id = item.Key;//(Guid)mnaR["ddsm_modelnumberapprovalid"];
                            newMNALookup.LogicalName = "ddsm_modelnumberapproval";
                            SKUA["ddsm_modelnumberapprovalid"] = newMNALookup;
                            SKUA.Id = ((Entity)list2[item.Key]).Id;
                            if ( item.Value.Attributes.TryGetValue("ddsm_measuretemplateid", out newMT) )
                            {
                                SKUA["ddsm_measuretemplateid"] = newMT;
                            }
                            //skuAListUpdate.Entities.Add(SKUA);
                            UpdateSKUA(SKUA, objCommon);
                            //ListTMP.Add((Guid)mnaR["ddsm_modelnumberapprovalid"]);
                        }
                        else
                        {
                            newMNALookup.Id = item.Key;//(Guid)mnaR["ddsm_modelnumberapprovalid"];
                            newMNALookup.LogicalName = "ddsm_modelnumberapproval";
                            SKUA.Id = Guid.Empty;
                            SKUA["ddsm_modelnumberapprovalid"] = newMNALookup;
                            if (item.Value.Attributes.TryGetValue("ddsm_measuretemplateid", out newMT))
                            {
                                SKUA["ddsm_measuretemplateid"] = newMT;
                            }
                            //skuAList.Entities.Add(SKUA);
                            CreateSKUA(SKUA, objCommon);
                        }
                    }

                    /*
                    List<Guid> ListTMP = new List<Guid>();
                    EntityReference newMNALookup = new EntityReference();
                    foreach (var mnaR in mnaRecords.Entities)
                    {
                        for (int i = 0; i < skuaExistedRecords.Entities.Count; i++)
                        {
                            if ( ((EntityReference)skuaExistedRecords.Entities[i].Attributes["ddsm_modelnumberapprovalid"]).Id == (Guid)mnaR["ddsm_modelnumberapprovalid"])
                            {
                                newMNALookup.Id = (Guid)mnaR["ddsm_modelnumberapprovalid"];
                                newMNALookup.LogicalName = "ddsm_modelnumberapproval";
                                SKUA["ddsm_modelnumberapprovalid"] = newMNALookup;
                                SKUA.Id = skuaExistedRecords.Entities[i].Id;
                                //skuAListUpdate.Entities.Add(SKUA);
                                UpdateSKUA(SKUA, objCommon);
                                ListTMP.Add((Guid)mnaR["ddsm_modelnumberapprovalid"]);
                                //mnaRecords.Entities.Remove(mnaR);
                            }
                        }
                    }


                    



                    foreach (var mnaR in mnaRecords.Entities)
                    {
                        for (int i = 0; i < ListTMP.Count; i++)
                        {
                            if ((Guid)mnaR["ddsm_modelnumberapprovalid"] != ListTMP[i] )
                            {
                                newMNALookup.Id = (Guid)mnaR["ddsm_modelnumberapprovalid"];
                                newMNALookup.LogicalName = "ddsm_modelnumberapproval";
                                SKUA["ddsm_modelnumberapprovalid"] = newMNALookup;
                                skuAList.Entities.Add(SKUA);
                                CreateSKUA(SKUA, objCommon);
                            }
                        }
                    }
                    
                    */


                        /*old
                        EntityReference newMNALookup = new EntityReference();
                        foreach (var mnaR in mnaRecords.Entities)
                        {
                            newMNALookup.Id = (Guid)mnaR["ddsm_modelnumberapprovalid"];
                            newMNALookup.LogicalName = "ddsm_modelnumberapproval";
                            SKUA["ddsm_modelnumberapprovalid"] = newMNALookup;

                            skuAList.Entities.Add(SKUA);
                        }*/


                        //Create new SKUA
                    /*foreach (var skuCreate in skuAList.Entities)
                    { 
                        try
                        {
                            objCommon.GetOrgService(systemCall: true).Create(skuCreate);
                        }
                        catch(Exception e)
                        {
                            objCommon.TracingService.Trace("Exeption: " + e.Message.ToString());
                        }
                    }*/

                    //Update existed SKUA
                    

                    /*
                    //Get List of existed SKU where SKU = ddsm_skuid
                    EntityCollection skuARecords = new EntityCollection();
                    var querySKUA = new QueryExpression()
                    {
                        EntityName = "ddsm_skuapproval",
                        ColumnSet = new ColumnSet("ddsm_name", "ddsm_skuapprovalid", "ddsm_skuid", "ddsm_modelnumberapprovalid"),
                        Criteria = new FilterExpression(LogicalOperator.And)
                        {
                            Conditions =
                                    {
                                        new ConditionExpression("ddsm_skuid", ConditionOperator.Equal, ddsm_sku.Attributes["ddsm_skuid"])
                                    }
                        }
                    };
                    skuARecords = objCommon.GetOrgService().RetrieveMultiple(querySKUA);


                    EntityCollection skuARecordForCreate = new EntityCollection();


                    foreach (var skuA in skuARecords.Entities)
                    {
                        if (skuA.Attributes["ddsm_skuid"] == ddsm_sku.Attributes["ddsm_skuid"] )
                        {

                        }


                    }
                    */



                    //Create all SKUA

                    //Created some SKUA 
                }
                //DoNotCreate
                if (((OptionSetValue)CreateApprovalSKUOptionSet).Value == (int)CreateApprovalSKU.DoNotCreate)
                {
                    objCommon.TracingService.Trace("07 SKU recoerd was Processed. SKUA not created - OtionSet = DoNotCreate");
                }
            }
            objCommon.TracingService.Trace("08 SKU recoerd was Processed.");
            //throw new NotImplementedException();
        }

        private void CreateSKUA(Entity sKUA, Common objCommon)
        {
            try
            {
                objCommon.GetOrgService(systemCall: true).Create(sKUA);
            }
            catch (Exception e)
            {
                objCommon.TracingService.Trace("Exeption: " + e.Message.ToString());
            }
        }

        private void UpdateSKUA(Entity sKUA, Common objCommon)
        {
            try
            {
                objCommon.GetOrgService(systemCall: true).Update(sKUA);
            }
            catch (Exception e)
            {
                objCommon.TracingService.Trace("Exeption: " + e.Message.ToString());
            }
        }

    }
}

