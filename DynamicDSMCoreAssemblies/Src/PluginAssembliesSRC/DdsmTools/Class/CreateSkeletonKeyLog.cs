﻿using System;
using System.Collections.Generic;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using DdsmTools.Model;
using DDSM.CommonProvider;

namespace DdsmTools.Class
{
    public class CreateSkeletonKeyLog : CodeActivity
    {
        [Input("Record Id")]
        public InArgument<string> RecordId { get; set; }

        [Input("Record Type Name")]
        public InArgument<string> RecordTypeName { get; set; }

        [Input("Record Name")]
        public InArgument<string> RecordName { get; set; }

        [Input("Reason")]
        public InArgument<string> Reason { get; set; }

        [Input("Disable Recalculation")]
        public InArgument<bool> DisableRecalculation { get; set; }

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        Common _common;

        protected override void Execute(CodeActivityContext executionContext)
        {
            var objCommon = new Ddsm(executionContext);
            _common = new Common(executionContext);
            objCommon.TracingService.Trace("Load CRM Service from context --->>> OK");

            try
            {
                var recordId = this.RecordId.Get(executionContext);
                var recordTypeName = this.RecordTypeName.Get(executionContext);
                var recordName = this.RecordName.Get(executionContext);
                var reason = this.Reason.Get(executionContext);
                var disableRecalculation = this.DisableRecalculation.Get(executionContext);

                objCommon.TracingService.Trace("RecordId: {0}", recordId);
                objCommon.TracingService.Trace("recordTypeName: {0}", recordTypeName);
                objCommon.TracingService.Trace("recordName: {0}", recordName);
                objCommon.TracingService.Trace("reason: {0}", reason);
                objCommon.TracingService.Trace("disableRecalculation: {0}", disableRecalculation);


                if (RecordId == null)
                {
                    this.Complete.Set(executionContext, false);
                    return;
                }

                if (recordTypeName == null)
                {
                    this.Complete.Set(executionContext, false);
                    return;
                }

                if (recordName == null)
                    recordName = "SkeletonKey Reason - " + recordTypeName;

                if (reason == null)
                    reason = string.Empty;

                if (disableRecalculation == null)
                    disableRecalculation = false;

                Entity newReasonLog = new Entity("ddsm_skeletonkeyreason");
                newReasonLog["ddsm_name"] = recordName;
                newReasonLog["ddsm_reason"] = reason;
                newReasonLog["ddsm_disablerecalculation"] = disableRecalculation;
                newReasonLog["ddsm_user"] = new EntityReference("systemuser", objCommon.Context.UserId);

                switch (recordTypeName)
                {
                    case "ddsm_measure":
                        newReasonLog["ddsm_measure"] = new EntityReference(recordTypeName, new Guid(recordId));
                        break;
                    case "ddsm_project":
                        newReasonLog["ddsm_project"] = new EntityReference(recordTypeName, new Guid(recordId));
                        break;
                    case "ddsm_projectgroup":
                        newReasonLog["ddsm_projectgroup"] = new EntityReference(recordTypeName, new Guid(recordId));
                        break;
                    case "ddsm_projectgroupfinancials":
                        newReasonLog["ddsm_projectgroupfinancial"] = new EntityReference(recordTypeName, new Guid(recordId));
                        break;
                }

                newReasonLog["ddsm_name"] = recordName;

                objCommon.OrgService.Create(newReasonLog);

                this.Complete.Set(executionContext, true);

            }
            catch (Exception ex)
            {
                objCommon.TracingService.Trace("Error:" + ex.Message);
                this.Complete.Set(executionContext, false);
            }

        }

    }
}
