﻿using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;

namespace DdsmTools
{
    public class GenMeasureTplID_Number : CodeActivity
    {
        [RequiredArgument]
        [Input("SelectableProgramOffering")]
        [ReferenceTarget("")]
        public InArgument<String> SelectableProgramOffering { get; set; }

        [Output("MeasureID")]
        public OutArgument<String> MeasureId { get; set; }

        [Output("MeasureNumber")]
        public OutArgument<String> MeasureNumber { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {

            var objDdsm = new Ddsm(executionContext);
            objDdsm.TracingService.Trace("Load CRM Service from context --- OK");

            var selectableProgramOffering = this.SelectableProgramOffering.Get(executionContext);

            objDdsm.TracingService.Trace(String.Format("SelectableProgramOffering: {0}", selectableProgramOffering));

            var query = new QueryExpression();
            query.EntityName = "ddsm_measuretemplate";
            query.ColumnSet = new ColumnSet();
            query.ColumnSet.Columns.Add("ddsm_measurenumber");
            query.ColumnSet.Columns.Add("ddsm_measureid");

            var filter = new FilterExpression(LogicalOperator.And);
            if (!string.IsNullOrEmpty(selectableProgramOffering))
            {

                var retrieveAttributeRequest = new RetrieveAttributeRequest
                {
                    EntityLogicalName = "ddsm_measuretemplate",
                    LogicalName = "ddsm_selectableprogramoffering",
                    RetrieveAsIfPublished = true
                };
                var retrieveAttributeResponse = (RetrieveAttributeResponse)objDdsm.OrgService.Execute(retrieveAttributeRequest);
                var retrievedPicklistAttributeMetadata = (Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata)
                retrieveAttributeResponse.AttributeMetadata;
                var optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
                int selectedOptionValue = 0;
                foreach (OptionMetadata oMd in optionList)
                {
                    if (oMd.Label.LocalizedLabels[0].Label.ToString().ToLower() == selectableProgramOffering.ToString().ToLower())
                    {
                        selectedOptionValue = oMd.Value.Value;
                        break;
                    }
                }
                if (selectedOptionValue != 0)
                {
                    var condition = new ConditionExpression();
                    condition.AttributeName = "ddsm_selectableprogramoffering";
                    condition.Values.Add(selectedOptionValue);
                    condition.Operator = ConditionOperator.Equal;
                    filter.Conditions.Add(condition);
                    query.Criteria = filter;

                    query.AddOrder("createdon", OrderType.Descending);

                    var results = objDdsm.OrgService.RetrieveMultiple(query);

                    if (results.Entities.Count > 0)
                    {
                        this.MeasureNumber.Set(executionContext, results.Entities[0]["ddsm_measurenumber"]);
                        this.MeasureId.Set(executionContext, results.Entities[0]["ddsm_measureid"]);
                    }
                    else {
                        this.MeasureNumber.Set(executionContext, null);
                        this.MeasureId.Set(executionContext, null);
                    }
                }
                else {
                    this.MeasureNumber.Set(executionContext, null);
                    this.MeasureId.Set(executionContext, null);
                }

            }
            else {
                this.MeasureNumber.Set(executionContext, null);
                this.MeasureId.Set(executionContext, null);
            }
        }
    }
}
