﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;

/// <summary>
///     Executes the workflow activity.
/// </summary>
/// <param name="executionContext">The execution context.</param>
public class GetAsyncOperation : CodeActivity
{
    public Dictionary<string, EntityMetadata> EntityMetadata = new Dictionary<string, EntityMetadata>();

    [Input("Is check related entities")]
    public InArgument<bool> IsCheckRelatedEntities { get; set; }

    [Input("Is DMN jobs")]
    public InArgument<bool> IsCheckDMNJobs { get; set; }

    [Input("List of related entities")]
    public InArgument<string> ListRelatedEntities { get; set; }

    [Output("OutputMessage")]
    public OutArgument<string> OutputMessage { get; set; }

    /// <summary>
    ///     Executes the workflow activity.
    /// </summary>
    /// <param name="executionContext">The execution context.</param>
    protected override void Execute(CodeActivityContext executionContext)
    {
        //Extract the tracing service for use in debugging sandboxed plug-ins.
        var tracingService = executionContext.GetExtension<ITracingService>();
        var output = new OutputObj();
        try
        {
            // The InputParameters collection contains all the data passed in the message request.
            if (!IsContextFull(executionContext))
                return;

            // Obtain the execution context from the service provider.
            var workflowContext = executionContext.GetExtension<IWorkflowContext>();

            // Obtain the organization service reference.
            var factory = executionContext.GetExtension<IOrganizationServiceFactory>();
            var orgService = factory.CreateOrganizationService(workflowContext.UserId);

            // Obtain the target entity from the input parameters.
            object target = null;
            if (workflowContext.InputParameters.Contains("Target") &&
                (workflowContext.InputParameters["Target"] != null)
            )
                target = workflowContext.InputParameters["Target"];

            var relEntities = ListRelatedEntities.Get(executionContext);
            var relEntitiesList = JsonConvert.DeserializeObject<RelatedEntity>(relEntities);

            var isCheckRelatedEntities = IsCheckRelatedEntities.Get(executionContext);
            var isCheckDMNJobs = IsCheckDMNJobs.Get(executionContext);

            var targetEntityRef = GetTargetData(target);
            var isAsyncOperationsExist = false;

            var asyncOperationDTO = new AsyncOperationDTO(executionContext, workflowContext, orgService, tracingService,
                targetEntityRef, relEntitiesList, isCheckRelatedEntities, isCheckDMNJobs);

            var systemJobs = GetSystemJobs(asyncOperationDTO);
            if (systemJobs.Count != 0)
            {
                output.Result = true;
                output.Message = systemJobs;
            }
            else
            {
                output.Result = false;
            }

            var json = JsonConvert.SerializeObject(output);
            OutputMessage.Set(executionContext, json);
        }
        catch (Exception e)
        {
            var json = JsonConvert.SerializeObject(output);
            OutputMessage.Set(executionContext, json);
        }
    }

    private List<RsPerEntity> GetSystemJobs(AsyncOperationDTO asyncOperationDTO)
    {
        return GetAsyncOperationRelatedToTargetEntity(asyncOperationDTO);
    }

    /// <summary>
    ///     GetOneToManyRelationshipsMetadata
    /// </summary>
    /// <returns></returns>
    public List<OneToManyRelationshipMetadata> GetManyToOneRelationshipsMetadata(EntityReference targetEntityRef,
        AsyncOperationDTO asyncOperationDto)
    {
        var orgService = asyncOperationDto.Service;
        var relatedEntities = asyncOperationDto.RelatedEntities;

        var retrieveEntityRequest = new RetrieveEntityRequest
        {
            EntityFilters = EntityFilters.Relationships,
            LogicalName = targetEntityRef.LogicalName
        };

        var retrieveEntityResponse = (RetrieveEntityResponse) orgService.Execute(retrieveEntityRequest);

        var manyToOneRelationships = retrieveEntityResponse.EntityMetadata.OneToManyRelationships;

        var oneToManyRelationshipMetadata = new List<OneToManyRelationshipMetadata>();

        var relatedEntitiesName = new List<string>();

        foreach (var item in relatedEntities.EntityNames)
        {
            relatedEntitiesName.Add(item.EntityName);
            var rs = manyToOneRelationships.Where(r => r.ReferencingEntity == item.EntityName).ToList();
            if (rs.Count != 0)
                oneToManyRelationshipMetadata.Add(rs.FirstOrDefault());
        }
        return oneToManyRelationshipMetadata;
    }

    /// <summary>
    /// </summary>
    /// <param name="targetEntityRef"></param>
    /// <param name="entityMetadata"></param>
    /// <param name="orgService"></param>
    /// <returns></returns>
    private List<Guid> GetIds(EntityReference targetEntityRef, OneToManyRelationshipMetadata entityMetadata,
        IOrganizationService orgService)
    {
        var query = new QueryExpression(entityMetadata.ReferencingEntity)
        {
            ColumnSet = new ColumnSet()
        };

        AddRelationCondition(query, entityMetadata.ReferencingAttribute, targetEntityRef.Id);
        IEnumerable<Entity> entities = orgService.RetrieveMultiple(query).Entities;
        var ids = new List<Guid>();
        foreach (var item in entities)
            ids.Add(item.Id);
        return ids;
    }

    /// <summary>
    /// </summary>
    /// <param name="asyncOperationDTO"></param>
    /// <returns></returns>
    private List<RsPerEntity> GetAsyncOperationRelatedToTargetEntity(AsyncOperationDTO asyncOperationDTO)
    {
        var rsPerEntity = new List<RsPerEntity>();
        var targetEntityRef = asyncOperationDTO.TargetEntityReference;
        var targetEntityId = targetEntityRef.Id;
        var searchIds = new List<Guid> {targetEntityId};
        var isCheckRelatedEntities = asyncOperationDTO.IsCheckRelatedEntities;
        var isCheckDMNJobs = asyncOperationDTO.IsCheckDMNJobs;

        var output = new List<KeyValuePair<string, bool>>();

        var relatedEntities = new Dictionary<string, List<Guid>>();
        if (isCheckRelatedEntities || isCheckDMNJobs)
        {
            var manyToOneRelationshipsMetadata = GetManyToOneRelationshipsMetadata(targetEntityRef, asyncOperationDTO);
            foreach (var entityMetadata in manyToOneRelationshipsMetadata)
            {
                var entitiesId = GetIds(targetEntityRef, entityMetadata, asyncOperationDTO.Service);
                relatedEntities.Add(entityMetadata.ReferencingEntity, entitiesId);
            }
        }

        if (isCheckRelatedEntities)
            foreach (var entity in relatedEntities)
            {
                var asyncOperation = GetAsyncOperations(entity.Value, asyncOperationDTO);
                if (asyncOperation.Entities.Count == 0) continue;
                AddRsToOutput(rsPerEntity, entity.Key, true, asyncOperationDTO);
            }
        if (isCheckDMNJobs)
            foreach (var entity in relatedEntities)
            {
                var asyncOperation = GetDmnJobs(entity.Value, new List<string> {entity.Key}, asyncOperationDTO);
                if (asyncOperation.Entities.Count == 0) continue;
                AddRsToOutput(rsPerEntity, entity.Key, true, asyncOperationDTO);
            }

        var asyncOperationRelatedToTargetEntity = GetAsyncOperations(searchIds, asyncOperationDTO);
        if (asyncOperationRelatedToTargetEntity.Entities.Count == 0) return rsPerEntity;
        AddRsToOutput(rsPerEntity, targetEntityRef.LogicalName, true, asyncOperationDTO);


        return rsPerEntity;
    }

    private void AddRsToOutput(List<RsPerEntity> output, string entityLogicalName, bool rs,
        AsyncOperationDTO asyncOperationDTO)
    {
        var entityDisplayName = GetEntityDisplayName(entityLogicalName, asyncOperationDTO.Service);
        foreach (var item in output)
            if (item.EntityLogicalName == entityLogicalName)
            {
                item.IsExistAsyncOperation = rs;
                break;
            }
        output.Add(new RsPerEntity(entityLogicalName, entityDisplayName, rs));
    }

    /// <summary>
    /// </summary>
    /// <param name="searchIds"></param>
    /// <param name="asyncOperationDTO"></param>
    /// <returns></returns>
    public EntityCollection GetAsyncOperations(List<Guid> searchIds, AsyncOperationDTO asyncOperationDTO)
    {
        if ((searchIds == null) || (searchIds.Count == 0))
            return new EntityCollection();
        var systemJobsQuery = new QueryExpression
        {
            ColumnSet = new ColumnSet(),
            EntityName = "asyncoperation",
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions =
                {
                    new ConditionExpression("regardingobjectid", ConditionOperator.In, searchIds)
                },
                Filters =
                {
                    new FilterExpression(LogicalOperator.Or)
                    {
                        Conditions =
                        {
                            new ConditionExpression("statuscode", ConditionOperator.Equal, 0),
                            new ConditionExpression("statuscode", ConditionOperator.Equal, 20)
                        }
                    }
                }
            }
        };
        var systemJobs = asyncOperationDTO.Service.RetrieveMultiple(systemJobsQuery);
        return systemJobs.Entities.Count != 0 ? systemJobs : new EntityCollection();
    }

    /// <summary>
    /// </summary>
    /// <param name="searchIds"></param>
    /// <param name="searchEntitiesName"></param>
    /// <param name="asyncOperationDTO"></param>
    /// <returns></returns>
    public EntityCollection GetDmnJobs(List<Guid> searchIds, List<string> searchEntitiesName,
        AsyncOperationDTO asyncOperationDTO)
    {
        if ((searchIds == null) || (searchIds.Count == 0))
            return new EntityCollection();

        var ids = new List<string>();

        foreach (var id in searchIds)
            ids.Add(id.ToString());

        var dmnJobsQuery = new QueryExpression
        {
            ColumnSet = new ColumnSet(),
            EntityName = "ddsm_dmnjobs",
            Criteria = new FilterExpression(LogicalOperator.And)
            {
                Conditions =
                {
                    new ConditionExpression("ddsm_targetentity", ConditionOperator.In, searchEntitiesName),
                    new ConditionExpression("ddsm_recordid", ConditionOperator.In, ids)
                },
                Filters =
                {
                    new FilterExpression(LogicalOperator.Or)
                    {
                        Conditions =
                        {
                            new ConditionExpression("ddsm_dmnjobstatus", ConditionOperator.Equal, 962080001),
                            //In progress
                            new ConditionExpression("ddsm_dmnjobstatus", ConditionOperator.Equal, 962080000)
                            //Not Started
                        }
                    }
                }
            }
        };
        var dmnJobs = asyncOperationDTO.Service.RetrieveMultiple(dmnJobsQuery);
        return dmnJobs.Entities.Count != 0 ? dmnJobs : new EntityCollection();
    }

    /// <summary>
    /// </summary>
    /// <param name="query"></param>
    /// <param name="relationAttribute"></param>
    /// <param name="targetEntityId"></param>
    private static void AddRelationCondition(QueryExpression query, string relationAttribute, Guid targetEntityId)
    {
        query.Criteria.AddCondition(
            relationAttribute,
            ConditionOperator.Equal,
            targetEntityId);
    }

    private string GetEntityDisplayName(string entityLogicalName, IOrganizationService orgService)
    {
        if (EntityMetadata.ContainsKey(entityLogicalName))
        {
            var metadata = EntityMetadata[entityLogicalName];
            return metadata.DisplayName.LocalizedLabels[0].Label;
        }
        var entityMetadata = GetEntityMetadata(entityLogicalName, orgService);
        EntityMetadata.Add(entityLogicalName, entityMetadata);
        return entityMetadata.DisplayName.LocalizedLabels[0].Label;
    }

    private EntityMetadata GetEntityMetadata(string entityName, IOrganizationService orgService)
    {
        var retrieveEntityRequest = new RetrieveEntityRequest
        {
            EntityFilters = EntityFilters.Entity,
            LogicalName = entityName
        };
        var rs = (RetrieveEntityResponse) orgService.Execute(retrieveEntityRequest);
        return rs.EntityMetadata;
    }

    /// <summary>
    ///     Get target entity
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    private EntityReference GetTargetData(object target)
    {
        try
        {
            if (target is EntityReference)
                return (EntityReference) target;

            if (target is Entity)
            {
                var sourceEntity = (Entity) target;

                return new EntityReference(sourceEntity.LogicalName, sourceEntity.Id);
            }
        }
        catch (Exception)
        {
            throw new Exception($"Can't get target from Context!");
        }


        return null;
    }

    /// <summary>
    ///     Obtain the execution context from the service provider.
    /// </summary>
    /// <param name="executionContext"></param>
    /// <returns></returns>
    private bool IsContextFull(CodeActivityContext executionContext)
    {
        if (executionContext == null) throw new ArgumentNullException(nameof(executionContext));
        var rs = true;
        try
        {
            if (executionContext.GetExtension<IWorkflowContext>() == null)
                rs = false;
        }
        catch (Exception ex)
        {
            return false;
        }
        return rs;
    }
}