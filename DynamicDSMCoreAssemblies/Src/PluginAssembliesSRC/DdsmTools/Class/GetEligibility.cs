﻿using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

namespace DdsmTools
{
    public class GetEligibility : CodeActivity
    {
        #region "Parameter Definition"

        [Input("Program Offering Guid")]
        [RequiredArgument]
        public InArgument<string> ObjectGuid { get; set; }

        [Output("Eligibilities")]
        public OutArgument<string> Eligibilities { get; set; }

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }
        #endregion

        protected override void Execute(CodeActivityContext executionContext)
        {
            var result = new List<string>();
            var objDdsm = new Ddsm(executionContext);
            objDdsm.TracingService.Trace("Load CRM Service from context --- OK");
            var objectId = ObjectGuid.Get(executionContext);
            objDdsm.TracingService.Trace("Input Guid: {0}", objectId);

            string entityName = "ddsm_eligibility";
            var objectIds = new List<string>();
            var objectCloneIds = new List<string>();

            if (!string.IsNullOrEmpty(objectId))
            {
                objDdsm.TracingService.Trace("Record ID: " + objectId + " LogicalName: " + entityName);

                var xmlStr = @"<fetch aggregate='true'> <entity name = 'ddsm_eligibility'> <attribute name = 'ddsm_name' alias = 'ddsm_name' groupby = 'true'/> <filter type = 'and'> <condition attribute = 'ddsm_programoffering' operator= 'eq' value = '{0}'/> </filter> </entity > </fetch >";
                var xmlQuery = new FetchExpression(string.Format(xmlStr, objectId));

                var entResults = objDdsm.OrgService.RetrieveMultiple(xmlQuery);
                if (entResults?.Entities == null)
                {
                    objDdsm.TracingService.Trace("Fetch Failed");
                    this.Eligibilities.Set(executionContext, "");
                    this.Complete.Set(executionContext, true);
                    return;
                }
                // Retrieve the only record from the result set.
                result.AddRange(from ent in entResults.Entities let entCol = "ddsm_name" select ((Microsoft.Xrm.Sdk.AliasedValue) ent.Attributes[entCol]).Value into entValue select entValue.ToString().ToLower());
            }

            this.Eligibilities.Set(executionContext, string.Join("|", result.ToArray()));
            this.Complete.Set(executionContext, true);
        }

    }
}
