﻿using System.Activities;
using Microsoft.Xrm.Sdk;
using System;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;

namespace DdsmTools
{

    public sealed class GetEntityReferenceByID : CodeActivity
    {
        // Define an activity input argument of type string
        [Input("EntityID")]
        public InArgument<string> EntityID { get; set; }

        [Input("EntityName")]
        public InArgument<string> EntityName { get; set; }

        [Output("EntityReference")]
        [ArgumentEntity("ddsm_measuretemplate")]
        [ReferenceTarget("ddsm_measuretemplate")]
        public OutArgument<EntityReference> EntityRef { get; set; }

        Common _objCommon;
       // IOrganizationService _service;
        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            // Obtain the runtime value of the Text input argument
            _objCommon = new Common(context);
           
            string ERef = EntityID.Get(context);
            _objCommon.TracingService.Trace("ERef: " + ERef);
            string EName = EntityName.Get(context);
            _objCommon.TracingService.Trace("EName: " + EName);
            var result = GetEntityRefByID(ERef, EName);
            if (result != null)
            {
                EntityRef.Set(context, result.ToEntityReference());
                _objCommon.TracingService.Trace("entRef: " + result.Id);
            }
            else {
                _objCommon.TracingService.Trace("entRef not found.");
                EntityRef.Set(context, null);
            }
            
        }

        private Entity GetEntityRefByID(string entityId, string eName)
        {
            /*
            var query = 
            @"<fetch>
              <entity name='{0}'>
                    <attribute name='ddsm_measuretemplateid' />
                    <filter>
                    <condition attribute='ddsm_measurenumber' operator='eq' value='{1}' />                                               
                    </filter>
              </entity>
            </fetch>";
            query = string.Format(query, EName, EntityGUID);
            */

            var query = new QueryExpression()
            {
                EntityName = "ddsm_measuretemplate",
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_measurenumber", ConditionOperator.Equal, entityId)
                    }
                }
            };



            //  var queryExpression = new FetchExpression(query);

            var totals = _objCommon.GetOrgService().RetrieveMultiple(query);

            if (totals.Entities.Count > 0)
            {
                return totals.Entities[0];
            }
            else { return null; }
        }
    }
}
