﻿using DdsmTools.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk.Messages;
using Newtonsoft.Json;

namespace DdsmTools.Class
{
    public class GroupSendEmail : CodeActivity
    {
        [Input("Processed Project Group Ids")]
        public InArgument<string> PGIds { get; set; }

        Common _objCommon;
        //    Entity _target;
        ProjectGroup _projectGroup;

        protected override void Execute(CodeActivityContext context)
        {
            try
            {
                _objCommon = new Common(context);
                //  _target = (Entity)_objCommon.Context.InputParameters["Target"];

                //current milestone index
                var pgIdsJson = PGIds.Get(context);
                var PGids = JsonConvert.DeserializeObject<UserInputObj2>(pgIdsJson);
                var PGs = getPG(PGids.SmartMeasures);
                var emRequest = new ExecuteMultipleRequest
                {
                    Requests = new OrganizationRequestCollection(),
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = false,
                        ReturnResponses = true
                    }
                };
                foreach (var user in PGs.Select(pG => GetUserForNotification(pG)))
                {
                    emRequest.Requests.Add(sendEmail(user)); // 
                }

                if (emRequest.Requests.Count > 0)
                {
                    var result = _objCommon.GetOrgService().Execute(emRequest);
                }
            }
            catch (Exception ex)
            {
                //  throw;
            }
        }

        private List<ProjectGroup> getPG(List<Guid> idsList)
        {
            return idsList.Select(pg => new ProjectGroup(new EntityReference("ddsm_projectgroup", pg), _objCommon)).ToList();

        }

        #region Internal Methods

        private CreateRequest sendEmail(Entity user)
        {
            if (user == null)
            {
                _objCommon.TracingService.Trace("Can't find user for Sending Email ((");
                return null;
            }
              
            //  throw new Exception("Can't find user for Sending Email ((");

            Ddsm.SetApprovalAuditRecord(_objCommon.GetOrgService(systemCall:true), _projectGroup.Milestones.CurrentStage.Name, new EntityReference("ddsm_projectgroup", _projectGroup.Id), new EntityReference("systemuser", user.GetAttributeValue<EntityReference>("ddsm_user").Id), _projectGroup.AppprovalAmount, false);

            var emailTask = Ddsm.CreateEmailTask(user, _projectGroup.Milestones.CurrentStage.Name, _projectGroup.Id);
            if (!Ddsm.EmailTaskExist(_objCommon.GetOrgService(systemCall: true), emailTask.Target))
                return emailTask;
            else return null;
            // _objCommon.OrgService.Create(emailTask.Target);
        }

        private Entity GetUserForNotification(ProjectGroup _pg)
        {
            _projectGroup = _pg;
            EntityCollection users;
            var randomUser = new Entity();
            //  if (_projectGroup.ApprovalsAmmountsRules == Ddsm.ApprovalsAmmountsRules.SearchRange)
            // {
            users = Ddsm.GetUserForSendEmail(_projectGroup.Milestones.PrevStage.Amount, _projectGroup.Team.Id, _projectGroup.Milestones.CurrentStage.Amount, _objCommon.GetOrgService(systemCall: true));
            randomUser = users.Entities.Random();
            // }
            //else
            //{
            //    users = Ddsm.GetUserForSendEmail(_projectGroup.AppprovalAmount, _projectGroup.Team.Id, _projectGroup.Milestones.CurrentStage.Amount, _objCommon.OrgService);
            //    randomUser = users.Entities[0];
            //}
            return randomUser;
        }

        private EntityReference GetPg(Entity targetData)
        {
            object obj = null;
            if (targetData.Attributes.TryGetValue("ddsm_projectgrouptoprojectgroupapproid", out obj))
                obj = (EntityReference)obj;

            return (EntityReference)obj;
        }

        private Entity GetMsData(Entity record)
        {
            return _objCommon.GetOrgService(systemCall: true).Retrieve(record.LogicalName, record.Id, new ColumnSet("ddsm_projectgroupmilestoneid", "ddsm_projectgrouptoprojectgroupapproid", "ddsm_name", "ddsm_index", "ddsm_status", "ddsm_targetstart", "ddsm_targetend", "ddsm_actualstart", "ddsm_actualend", "ddsm_duration", "ddsm_approvalthresholdcust"));
        }
        #endregion
    }
}
