﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.ServiceModel;

namespace DdsmTools
{

    public class ManuallyCreatingProject : CodeActivity
    {
        [RequiredArgument]
        [Input("Parent Account ID")]
        public InArgument<string> ParentAccount { get; set; }

        [RequiredArgument]
        [Input("Parent Site ID")]
        public InArgument<string> ParentSite { get; set; }

        [Input("Start Date")]
        public InArgument<string> StartDate { get; set; }

        [RequiredArgument]
        [Input("Program Offering ID")]
        public InArgument<string> ProgramOffering { get; set; }

        [RequiredArgument]
        [Input("Project Template ID")]
        public InArgument<string> ProjectTemplate { get; set; }

        [Input("Partner Contact ID")]
        public InArgument<string> PartnerContact { get; set; }

        [Input("Partner Company ID")]
        public InArgument<string> PartnerCompany { get; set; }

        [Output("Project ID")]
        public OutArgument<string> ProjectId { get; set; }

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        //public IOrganizationService OrgService;
        //public IOrganizationService SystemService;

        private Guid Currency { get; set; }

        private Ddsm objCommon;
        private TimeZoneInfo timeZoneInfo;

        protected override void Execute(CodeActivityContext executionContext)
        {
            objCommon = new Ddsm(executionContext);
            //OrgService = objCommon.OrgServiceUser;
            //SystemService = objCommon.OrgService;

            string windowsTimeZoneName = objCommon.GetTimeZoneByCRMConnection(objCommon.OrgService, objCommon.Context.UserId);
            timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZoneName);

            Guid parentAccount = Guid.Empty
                , parentSite = Guid.Empty
                , programOffering = Guid.Empty
                , projectTemplate = Guid.Empty
                , partnerContact = Guid.Empty
                , partnerCompany = Guid.Empty;
            DateTime startDate = objCommon.ConverAttributToDateTimeUtc("today", timeZoneInfo);

            Currency = GetGuidofCurrency(objCommon.OrgService);

            try
            {
                //Entity entity = (Entity)objCommon.Context.InputParameters["Target"];
                if (!string.IsNullOrEmpty(this.ParentAccount.Get(executionContext)))
                {
                    parentAccount = new Guid(this.ParentAccount.Get(executionContext));
                }
                if (!string.IsNullOrEmpty(this.ParentSite.Get(executionContext)))
                {
                    parentSite = new Guid(this.ParentSite.Get(executionContext));
                }
                if (!string.IsNullOrEmpty(this.ProgramOffering.Get(executionContext)))
                {
                    programOffering = new Guid(this.ProgramOffering.Get(executionContext));
                }
                if (!string.IsNullOrEmpty(this.ProjectTemplate.Get(executionContext)))
                {
                    projectTemplate = new Guid(this.ProjectTemplate.Get(executionContext));
                }

                if (!string.IsNullOrEmpty(this.StartDate.Get(executionContext)))
                {
                    string strStartDate = Convert.ToString(this.StartDate.Get(executionContext));
                    startDate = objCommon.ConverAttributToDateTimeUtc(strStartDate, timeZoneInfo);
                }

                //Additional parameters
                if (!string.IsNullOrEmpty(this.PartnerContact.Get(executionContext)))
                {
                    partnerContact = new Guid(this.PartnerContact.Get(executionContext));
                }
                if (!string.IsNullOrEmpty(this.PartnerCompany.Get(executionContext)))
                {
                    partnerCompany = new Guid(this.PartnerCompany.Get(executionContext));
                }


                if (parentAccount == Guid.Empty)
                {
                    throw new Exception("Parent Account is empty.");
                }
                if (parentSite == Guid.Empty)
                {
                    throw new Exception("Parent Site is empty.");
                }
                if (programOffering == Guid.Empty)
                {
                    throw new Exception("Program Offering is empty.");
                }
                if (projectTemplate == Guid.Empty)
                {
                    throw new Exception("Project Template is empty.");
                }

                Guid newProject = CreateProject(parentAccount, parentSite, programOffering, projectTemplate, startDate, partnerContact, partnerCompany, objCommon.TracingService);

                if (newProject != Guid.Empty)
                {
                    Complete.Set(executionContext, true);
                    ProjectId.Set(executionContext, newProject.ToString());
                }
                else {
                    Complete.Set(executionContext, false);
                    ProjectId.Set(executionContext, (Guid.Empty).ToString());
                }

            }
            catch (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex)
            {
                objCommon.TracingService.Trace("Timestamp: " + ex.Detail.Timestamp);
                objCommon.TracingService.Trace("Code: " + ex.Detail.ErrorCode);
                objCommon.TracingService.Trace("Message: " + ex.Detail.Message);
                Complete.Set(executionContext, false);
                ProjectId.Set(executionContext, (Guid.Empty).ToString());
            }
            catch (Exception e)
            {
                objCommon.TracingService.Trace("Error:" + e.Message);
                Complete.Set(executionContext, false);
                ProjectId.Set(executionContext, (Guid.Empty).ToString());
            }
        }

        private Guid CreateProject(Guid parentAccount, Guid parentSite, Guid programOffering, Guid projectTemplate, DateTime startDate, Guid partnerContact, Guid partnerCompany, ITracingService TracingService)
        {
            Guid projectGuid = Guid.Empty;

            try
            {
                Entity rmEntity;
                string accountName = string.Empty;
                QueryExpression query;
                EntityCollection retrieveQuery;

                Entity newProject = new Entity("ddsm_project");
                newProject["ddsm_phasenumber"] = 1;
                //Project Template
                rmEntity = getDataRelationMapping("ddsm_project", "ddsm_projecttemplate", projectTemplate);
                if (rmEntity != null)
                {
                    foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                    {
                        newProject[Attribute.Key] = Attribute.Value;
                    }

                }

                //Get relate fields Parent Account
                query = new QueryExpression { EntityName = "account", ColumnSet = new ColumnSet("name") };
                query.Criteria.AddCondition("accountid", ConditionOperator.Equal, parentAccount);
                query.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
                retrieveQuery = objCommon.OrgService.RetrieveMultiple(query);
                if (retrieveQuery != null && retrieveQuery.Entities.Count == 1)
                {
                    accountName = retrieveQuery.Entities[0].Attributes["name"].ToString();
                }

                rmEntity = getDataRelationMapping("ddsm_project", "account", parentAccount);

                if (rmEntity.Attributes.ContainsKey("ddsm_accountid"))
                {
                    var accountId = rmEntity.GetAttributeValue<EntityReference>("ddsm_accountid");

                    foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                    {
                        if (!(accountId.GetType() == Attribute.Value.GetType() && accountId.Id == (Attribute.Value as EntityReference).Id))
                        {
                            newProject[Attribute.Key] = Attribute.Value;
                        }
                    }
                    newProject["ddsm_accountid"] = accountId;
                }

                //You need to add a Partner Contact & Partner Company
                if (partnerContact != Guid.Empty)
                {
                    newProject["ddsm_tradeallycontact"] = new EntityReference("contact", partnerContact);
                }
                if (partnerCompany != Guid.Empty)
                {
                    newProject["ddsm_tradeally"] = new EntityReference("account", partnerCompany);
                }

                //Get relate fields Parent Site
                rmEntity = getDataRelationMapping("ddsm_project", "ddsm_site", parentSite);
                if (rmEntity.Attributes.ContainsKey("ddsm_parentsiteid"))
                {
                    var parentSiteId = rmEntity.GetAttributeValue<EntityReference>("ddsm_parentsiteid");

                    foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                    {
                        if (!(parentSiteId.GetType() == Attribute.Value.GetType() && parentSiteId.Id == (Attribute.Value as EntityReference).Id))
                        {
                            newProject[Attribute.Key] = Attribute.Value;
                        }
                    }
                    newProject["ddsm_parentsiteid"] = parentSiteId;
                }

                //Get relate fields Program Offering
                rmEntity = getDataRelationMapping("ddsm_project", "ddsm_programoffering", programOffering);
                if (rmEntity != null)
                {
                    foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                    {
                        newProject[Attribute.Key] = Attribute.Value;
                    }
                }

                //AutoNumbering
                newProject["ddsm_projectnumber"] = GetAutoNumber("ddsm_project", objCommon.OrgService);

                //Gen Project Name
                newProject["ddsm_name"] = newProject.GetAttributeValue<string>("ddsm_projectnumber") + "-" + newProject.GetAttributeValue<string>("ddsm_name") + "-" + accountName;

                newProject["ddsm_startdate"] = startDate;
                newProject["ddsm_accountid"] = new EntityReference("account", parentAccount);
                newProject["ddsm_parentsiteid"] = new EntityReference("ddsm_site", parentSite);

                //Create & ReCalc Milestone Collection
                EntityCollection milestoneCollection = createMilestones(objCommon.OrgService, projectTemplate, startDate);

                //Get Business Process project
                string projTplName = string.Empty, bpfName = string.Empty;
                BusinessProcess projBP = new BusinessProcess();
                projBP.Id = null;
                query = new QueryExpression { EntityName = "ddsm_projecttemplate", ColumnSet = new ColumnSet("ddsm_name", "ddsm_bpfname") };
                query.Criteria.AddCondition("ddsm_projecttemplateid", ConditionOperator.Equal, projectTemplate);
                query.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
                retrieveQuery = objCommon.OrgService.RetrieveMultiple(query);
                if (retrieveQuery != null && retrieveQuery.Entities.Count == 1)
                {
                    projTplName = retrieveQuery.Entities[0].Attributes["ddsm_name"].ToString();
                    if (retrieveQuery.Entities[0].Attributes.ContainsKey("ddsm_bpfname")) {
                        bpfName = retrieveQuery.Entities[0].Attributes["ddsm_bpfname"].ToString();
                    }
                }
                if (!string.IsNullOrEmpty(bpfName))
                {
                    projBP = getBProcessProj(objCommon.OrgService, bpfName);
                }
                if (string.IsNullOrEmpty(projBP.Id))
                {
                    objCommon.TracingService.Trace("Business Process Flow (Project Template: '" + projTplName + "') is empty or not found. An attempt is made to find the BPF by the name of the current project template.");

                    if (!string.IsNullOrEmpty(projTplName))
                    {
                        projBP = getBProcessProj(objCommon.OrgService, projTplName);
                    }
                    else
                    { projBP.Id = null; }
                }

                int activeIdx = 2000000000;
                string traversedpath = string.Empty, stageid = string.Empty, processid = string.Empty;
                foreach (Entity ms in milestoneCollection.Entities)
                {
                    if (ms["ddsm_projectphasename"].ToString() == newProject["ddsm_phase"].ToString() && ms["ddsm_projectstatus"].ToString() == newProject["ddsm_projectstatus"].ToString() && Convert.ToInt32(newProject["ddsm_pendingmilestoneindex"]) == Convert.ToInt32(ms["ddsm_index"]))
                    {
                        activeIdx = Convert.ToInt32(ms["ddsm_index"]);
                        break;
                    }
                }
                string tmpPhaseName = null;
                int ForecastCompleteLastRecalcIndex = -1;
                if (newProject.Attributes.Contains("ddsm_forecastcompletelastrecalcindex"))
                    ForecastCompleteLastRecalcIndex = newProject.GetAttributeValue<int>("ddsm_forecastcompletelastrecalcindex");

                DateTime msPrevEndtDay = DateTime.UtcNow, ForecastComplete = DateTime.UtcNow;

                if (milestoneCollection.Entities.Count > 0)
                {
                    newProject["ddsm_projectstatus"] = milestoneCollection.Entities[0].GetAttributeValue<string>("ddsm_projectstatus").ToString();
                    newProject["ddsm_phase"] = tmpPhaseName = milestoneCollection.Entities[0].GetAttributeValue<string>("ddsm_projectphasename").ToString();
                }
                foreach (Entity ms in milestoneCollection.Entities)
                {
                    if (ms.Contains("ddsm_duration") && ms["ddsm_duration"] != null)
                    {
                    }
                    else ms["ddsm_duration"] = 0;

                    if (Convert.ToInt32(ms["ddsm_index"]) != 1 && Convert.ToInt32(ms["ddsm_index"]) <= activeIdx && tmpPhaseName.ToLower() != ms["ddsm_projectphasename"].ToString().ToLower())
                    {
                        newProject["ddsm_phasenumber"] = Convert.ToInt32(newProject["ddsm_phasenumber"].ToString()) + 1;
                        newProject["ddsm_phase"] = tmpPhaseName = ms["ddsm_projectphasename"].ToString();
                    }
                    if (Convert.ToInt32(ms["ddsm_index"]) == 1)
                        msPrevEndtDay = Convert.ToDateTime(ms["ddsm_actualstart"]);

                    if (Convert.ToInt32(ms["ddsm_index"]) < activeIdx)
                    {

                        ms["ddsm_actualstart"] = msPrevEndtDay;
                        ms["ddsm_targetstart"] = msPrevEndtDay;
                        msPrevEndtDay = msPrevEndtDay.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                        ms["ddsm_targetend"] = msPrevEndtDay;

                        ms["ddsm_status"] = new OptionSetValue(962080002);

                        ms["ddsm_actualend"] = ms["ddsm_targetend"];
                        newProject["ddsm_completedmilestone"] = ms["ddsm_name"].ToString();
                        newProject["ddsm_completedstatus"] = ms["ddsm_projectstatus"].ToString();
                        newProject["ddsm_completephase"] = ms["ddsm_projectphasename"].ToString();

                        //newProject["ddsm_milestoneactualend"] = ms["ddsm_targetend"];
                        newProject["ddsm_responsible"] = ms["ddsm_responsible"];

                        newProject["ddsm_projectstatus"] = ms["ddsm_projectstatus"].ToString();
                        newProject["ddsm_phase"] = ms["ddsm_projectphasename"].ToString();

                        msPrevEndtDay = Convert.ToDateTime(ms["ddsm_actualend"]);

                        if (projBP.Id != null)
                        {
                            if ((projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())) != null))
                            {
                                traversedpath = traversedpath + "," + projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())).Id;
                            }
                        }

                        if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0 && ForecastCompleteLastRecalcIndex == Convert.ToInt32(ms["ddsm_index"]))
                            ForecastComplete = ms.GetAttributeValue<DateTime>("ddsm_actualend");

                    }
                    else if (Convert.ToInt32(ms["ddsm_index"]) == activeIdx)
                    {
                        ms["ddsm_actualstart"] = msPrevEndtDay;
                        ms["ddsm_targetstart"] = msPrevEndtDay;
                        msPrevEndtDay = msPrevEndtDay.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                        ms["ddsm_targetend"] = msPrevEndtDay;

                        ms["ddsm_status"] = new OptionSetValue(962080001);
                        newProject["ddsm_pendingmilestone"] = ms["ddsm_name"].ToString();
                        newProject["ddsm_pendingmilestoneindex"] = Convert.ToInt32(ms["ddsm_index"]);
                        newProject["ddsm_pendingactualstart"] = ms["ddsm_actualstart"];
                        newProject["ddsm_pendingtargetend"] = ms["ddsm_targetend"];
                        newProject["ddsm_responsible"] = ms["ddsm_responsible"];

                        newProject["ddsm_projectstatus"] = ms["ddsm_projectstatus"].ToString();
                        newProject["ddsm_phase"] = ms["ddsm_projectphasename"].ToString();

                        if (projBP.Id != null)
                        {
                            if ((projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())) != null))
                            {
                                stageid = projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())).Id;
                            }

                        }
                        if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0 && ForecastCompleteLastRecalcIndex == Convert.ToInt32(ms["ddsm_index"]))
                            ForecastComplete = ms.GetAttributeValue<DateTime>("ddsm_targetend");
                    }
                    else
                    {
                        ms["ddsm_actualstart"] = msPrevEndtDay;
                        ms["ddsm_targetstart"] = msPrevEndtDay;
                        msPrevEndtDay = msPrevEndtDay.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                        ms["ddsm_targetend"] = msPrevEndtDay;

                        ms["ddsm_status"] = new OptionSetValue(962080000);

                        if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0 && ForecastCompleteLastRecalcIndex == Convert.ToInt32(ms["ddsm_index"]))
                            ForecastComplete = ms.GetAttributeValue<DateTime>("ddsm_targetend");
                    }

                    if (Convert.ToBoolean(newProject["ddsm_completeonlastmilestone"]) && Convert.ToInt32(ms["ddsm_index"]) == Convert.ToInt32(newProject["ddsm_lastmilestoneindex"]) && activeIdx == Convert.ToInt32(newProject["ddsm_lastmilestoneindex"]))
                    {
                        newProject["ddsm_responsible"] = null;

                        newProject["ddsm_pendingmilestone"] = null;
                        newProject["ddsm_pendingmilestoneindex"] = null;
                        newProject["ddsm_pendingactualstart"] = null;
                        newProject["ddsm_pendingtargetend"] = null;

                        newProject["ddsm_completedmilestone"] = null;
                        newProject["ddsm_completedstatus"] = null;
                        newProject["ddsm_completephase"] = null;

                        newProject["ddsm_projectstatus"] = ms["ddsm_projectstatus"].ToString();
                        newProject["ddsm_phase"] = ms["ddsm_projectphasename"].ToString();
                        newProject["ddsm_enddate"] = msPrevEndtDay;

                        break;
                    }
                }
                if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0)
                {
                    newProject["ddsm_estimatedprojectcomplete"] = ForecastComplete;
                    newProject["ddsm_projectcompletiondateupdatedestimate"] = ForecastComplete;
                }
                else
                {
                    newProject["ddsm_estimatedprojectcomplete"] = msPrevEndtDay;
                    newProject["ddsm_projectcompletiondateupdatedestimate"] = msPrevEndtDay;
                }

                newProject["ddsm_targetprojectcompletion"] = msPrevEndtDay;

                if (!string.IsNullOrEmpty(stageid))
                {
                    traversedpath = traversedpath + "," + stageid;
                }
                traversedpath = traversedpath.TrimStart(',');

                //Add Project Owner to Milestone
                foreach (Entity ms in milestoneCollection.Entities)
                {
                    if (newProject.Attributes.ContainsKey("ownerid"))
                        ms["ownerid"] = newProject["ownerid"];
                }

                //Add Milestone Collection
                if (milestoneCollection.Entities.Count > 0)
                {

                    Relationship msRelationship = new Relationship("ddsm_ddsm_project_ddsm_milestone");
                    newProject.RelatedEntities.Add(msRelationship, milestoneCollection);
                }

                //Add Business Process
                if (projBP.Id != null)
                {
                    newProject["processid"] = new Guid(projBP.Id);
                    if (!string.IsNullOrEmpty(stageid))
                        newProject["stageid"] = new Guid(stageid);
                    if (!string.IsNullOrEmpty(traversedpath))
                        newProject["traversedpath"] = traversedpath;
                }

                newProject["transactioncurrencyid"] = new EntityReference("transactioncurrency", Currency);

                //Create & Add Financial Collection
                EntityCollection financilCollection = createFinancials(objCommon.OrgService, projectTemplate, newProject, parentAccount, parentSite, milestoneCollection);


                //Add Project Owner to Financial
                foreach (Entity fncl in financilCollection.Entities)
                {
                    if (newProject.Attributes.ContainsKey("ownerid"))
                        fncl["ownerid"] = newProject["ownerid"];
                }

                if (financilCollection.Entities.Count > 0)
                {
                    Relationship fnclRelationship = new Relationship("ddsm_ddsm_project_ddsm_financial");
                    newProject.RelatedEntities.Add(fnclRelationship, financilCollection);
                }

                newProject["ddsm_calculationrecordstatus"] = new OptionSetValue(962080000);
                newProject["ddsm_creatorrecordtype"] = new OptionSetValue(962080000);

                projectGuid = objCommon.OrgService.Create(newProject);

            }
            catch (Exception e)
            {
                TracingService.Trace("Error:" + e.Message);
            }

            return projectGuid;
        }

        //Get Currency
        public Guid GetGuidofCurrency(IOrganizationService _orgService)
        {
            Guid CurrencyGuid = new Guid();

            QueryExpression queryCurrency = new QueryExpression("currency")
            {
                EntityName = "transactioncurrency",
                ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression()
            };
            queryCurrency.Criteria.AddCondition("isocurrencycode", ConditionOperator.Equal, "CAD");
            try
            {
                DataCollection<Entity> entityData = _orgService.RetrieveMultiple(queryCurrency).Entities;
                int count = entityData.Count;

                if (count > 0)
                {
                    CurrencyGuid = (Guid)entityData[0].Id;

                }
                else
                {
                    queryCurrency = new QueryExpression("currency")
                    {
                        EntityName = "transactioncurrency",
                        ColumnSet = new ColumnSet(true),
                        Criteria = new FilterExpression()
                    };
                    entityData = _orgService.RetrieveMultiple(queryCurrency).Entities;
                    count = entityData.Count;
                    if (count > 0)
                        CurrencyGuid = (Guid)entityData[0].Id;
                }
            }
            catch (Exception e)
            {
            }

            return CurrencyGuid;
        }

        //Get Data fields relation mapping
        private Entity getDataRelationMapping(string targetEntity, string sourceEntity, Guid sourceGuid)
        {
            InitializeFromRequest initialize = new InitializeFromRequest();
            initialize.TargetEntityName = targetEntity;
            initialize.EntityMoniker = new EntityReference(sourceEntity, sourceGuid);
            initialize.TargetFieldType = TargetFieldType.All;
            InitializeFromResponse initialized = (InitializeFromResponse)objCommon.OrgService.Execute(initialize);
            return initialized.Entity;
        }

        //Auto-Numbering
        private static string GetAutoNumber(string logicalName, IOrganizationService orgService)
        {
            return AutoNumberingManager.GenAutoNumber(logicalName, orgService);
        }

        //Create Financials Collection
        private EntityCollection createFinancials(IOrganizationService _orgService, Guid projTplID, Entity parentProj, Guid account, Guid site, EntityCollection milestoneCollection)
        {
            EntityCollection fnclCollection = new EntityCollection();

            Entity newEntiy = new Entity("ddsm_financial");

            DateTime projStartDate = DateTime.UtcNow;

            Relationship relationship = new Relationship();
            relationship.SchemaName = "ddsm_ddsm_projecttemplate_ddsm_financialtemplate";

            QueryExpression query = new QueryExpression();
            query.EntityName = "ddsm_financialtemplate";
            query.ColumnSet = new ColumnSet("ddsm_financialtemplateid");
            query.Criteria = new FilterExpression();
            query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

            RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);

            RetrieveRequest request = new RetrieveRequest();
            request.ColumnSet = new ColumnSet("ddsm_projecttemplateid", "ddsm_name");
            request.Target = new EntityReference { Id = projTplID, LogicalName = "ddsm_projecttemplate" };
            request.RelatedEntitiesQuery = relatedEntity;

            RetrieveResponse response = (RetrieveResponse)_orgService.Execute(request);

            DataCollection<Entity> RelatedEntitis = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship("ddsm_ddsm_projecttemplate_ddsm_financialtemplate")].Entities;

            foreach (var RelatedEntity in RelatedEntitis)
            {
                newEntiy = new Entity("ddsm_financial");
                Entity rmEntity = getDataRelationMapping("ddsm_financial", "ddsm_financialtemplate", new Guid(RelatedEntity["ddsm_financialtemplateid"].ToString()));
                if (rmEntity != null)
                {
                    foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                    {
                        newEntiy[Attribute.Key] = Attribute.Value;
                    }
                    if (parentProj.Attributes.ContainsKey("ddsm_programoffering"))
                        newEntiy["ddsm_programofferingid"] = new EntityReference(parentProj.GetAttributeValue<EntityReference>("ddsm_programoffering").LogicalName, parentProj.GetAttributeValue<EntityReference>("ddsm_programoffering").Id);

                    if (parentProj.Attributes.ContainsKey("ddsm_projectnumber") && !string.IsNullOrEmpty(parentProj.GetAttributeValue<string>("ddsm_projectnumber")))
                        newEntiy["ddsm_name"] = newEntiy["ddsm_name"] + "-" + parentProj.GetAttributeValue<string>("ddsm_projectnumber");

                    int initMsIdx = (newEntiy.Attributes.ContainsKey("ddsm_initiatingmilestoneindex")) ? newEntiy.GetAttributeValue<int>("ddsm_initiatingmilestoneindex") : 1;
                    projStartDate = milestoneCollection[initMsIdx - 1].GetAttributeValue<DateTime>("ddsm_targetstart");
                    if (newEntiy.Attributes.ContainsKey("ddsm_stagename1"))
                    {
                        newEntiy["ddsm_duration1"] = (newEntiy.Attributes.ContainsKey("ddsm_duration1")) ? newEntiy.GetAttributeValue<int>("ddsm_duration1") : 0;
                        newEntiy["ddsm_targetstart1"] = projStartDate;
                        projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration1"));
                        newEntiy["ddsm_targetend1"] = projStartDate;
                    }
                    if (newEntiy.Attributes.ContainsKey("ddsm_stagename2"))
                    {
                        newEntiy["ddsm_duration2"] = (newEntiy.Attributes.ContainsKey("ddsm_duration2")) ? newEntiy.GetAttributeValue<int>("ddsm_duration2") : 0;
                        newEntiy["ddsm_targetstart2"] = projStartDate;
                        projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration2"));
                        newEntiy["ddsm_targetend2"] = projStartDate;
                    }
                    if (newEntiy.Attributes.ContainsKey("ddsm_stagename3"))
                    {
                        newEntiy["ddsm_duration3"] = (newEntiy.Attributes.ContainsKey("ddsm_duration3")) ? newEntiy.GetAttributeValue<int>("ddsm_duration3") : 0;
                        newEntiy["ddsm_targetstart3"] = projStartDate;
                        projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration3"));
                        newEntiy["ddsm_targetend3"] = projStartDate;
                    }
                    if (newEntiy.Attributes.ContainsKey("ddsm_stagename4"))
                    {
                        newEntiy["ddsm_duration4"] = (newEntiy.Attributes.ContainsKey("ddsm_duration4")) ? newEntiy.GetAttributeValue<int>("ddsm_duration4") : 0;
                        newEntiy["ddsm_targetstart4"] = projStartDate;
                        projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration4"));
                        newEntiy["ddsm_targetend4"] = projStartDate;
                    }
                    if (newEntiy.Attributes.ContainsKey("ddsm_stagename5"))
                    {
                        newEntiy["ddsm_duration5"] = (newEntiy.Attributes.ContainsKey("ddsm_duration5")) ? newEntiy.GetAttributeValue<int>("ddsm_duration5") : 0;
                        newEntiy["ddsm_targetstart5"] = projStartDate;
                        projStartDate = projStartDate.AddDays(newEntiy.GetAttributeValue<int>("ddsm_duration5"));
                        newEntiy["ddsm_targetend5"] = projStartDate;
                    }

                    newEntiy["transactioncurrencyid"] = new EntityReference("transactioncurrency", Currency);

                    newEntiy["ddsm_creatorrecordtype"] = new OptionSetValue(962080000);

                    //Get relate fields Account
                    /*
                    Entity rmEntityAcc = getDataRelationMapping("ddsm_financial", "account", account);
                    foreach (KeyValuePair<String, Object> Attribute in rmEntityAcc.Attributes)
                    {
                        newEntiy[Attribute.Key] = Attribute.Value;
                    }
                    */
                    newEntiy["ddsm_accountid"] = new EntityReference("account", account);
                    if (!newEntiy.Attributes.Contains("ddsm_status"))
                        newEntiy["ddsm_status"] = new OptionSetValue(962080000);

                    //AutoNumbering
                    if (newEntiy.Attributes.ContainsKey("ddsm_financialnumber") && !string.IsNullOrEmpty(newEntiy["ddsm_financialnumber"].ToString())) { }
                    else
                        newEntiy["ddsm_financialnumber"] = GetAutoNumber("ddsm_financial", _orgService);

                    fnclCollection.Entities.Add(newEntiy);
                    fnclCollection.EntityName = newEntiy.LogicalName;
                }
                //break;
            }



            return fnclCollection;

        }

        //Create Milestones Collection
        private EntityCollection createMilestones(IOrganizationService _orgService, Guid projTplID, DateTime projStartDate)
        {
            EntityCollection msCollection = new EntityCollection();
            string startPhaseName = string.Empty, startStatusName = string.Empty;

            try
            {
                //
                Relationship relationship = new Relationship();
                relationship.SchemaName = "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate";

                QueryExpression query = new QueryExpression();
                query.EntityName = "ddsm_milestonetemplate";
                query.ColumnSet = new ColumnSet("ddsm_milestonetemplateid", "ddsm_index");
                query.Criteria = new FilterExpression();
                query.AddOrder("ddsm_index", OrderType.Ascending);
                query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

                RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
                relatedEntity.Add(relationship, query);

                RetrieveRequest request = new RetrieveRequest();
                request.ColumnSet = new ColumnSet("ddsm_projecttemplateid", "ddsm_name");
                request.Target = new EntityReference { Id = projTplID, LogicalName = "ddsm_projecttemplate" };
                request.RelatedEntitiesQuery = relatedEntity;

                RetrieveResponse response = (RetrieveResponse)_orgService.Execute(request);

                DataCollection<Entity> RelatedEntitis = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship("ddsm_ddsm_projecttemplate_ddsm_milestonetemplate")].Entities;

                foreach (var RelatedEntity in RelatedEntitis)
                {
                    Entity ms = new Entity("ddsm_milestone");
                    Entity rmEntity = getDataRelationMapping("ddsm_milestone", "ddsm_milestonetemplate", new Guid(RelatedEntity["ddsm_milestonetemplateid"].ToString()));
                    if (rmEntity != null)
                    {
                        foreach (KeyValuePair<String, Object> Attribute in rmEntity.Attributes)
                        {
                            ms[Attribute.Key] = Attribute.Value;
                            //ms[Attribute.Key] = rmEntity.GetAttributeValue(Attribute.Key);

                        }
                        if (ms.Attributes.Contains("ddsm_duration") && ms["ddsm_duration"] != null) { } else ms["ddsm_duration"] = 0;
                        if (Convert.ToInt32(ms["ddsm_index"]) == 1)
                        {
                            ms["ddsm_actualstart"] = projStartDate;
                            ms["ddsm_status"] = new OptionSetValue(962080001);
                        }
                        else
                        {
                            ms["ddsm_status"] = new OptionSetValue(962080000);
                        }
                        ms["ddsm_targetstart"] = projStartDate;
                        projStartDate = projStartDate.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                        ms["ddsm_targetend"] = projStartDate;

                        ms["transactioncurrencyid"] = new EntityReference("transactioncurrency", Currency);

                        ms["ddsm_creatorrecordtype"] = new OptionSetValue(962080000);

                        msCollection.Entities.Add(ms);
                        msCollection.EntityName = ms.LogicalName;

                    }

                }

            }
            catch (Exception e)
            {
                msCollection = new EntityCollection();
            }
            return msCollection;
        }

        //Get Business Process
        private BusinessProcess getBProcessProj(IOrganizationService _orgService, string projTplName)
        {
            BusinessProcess projBP = new BusinessProcess();
            projBP.Id = null;
            //Get Project Template ID
            try
            {
                QueryExpression processQuery = new QueryExpression("workflow");
                processQuery.ColumnSet = new ColumnSet("name", "workflowid");
                processQuery.ColumnSet = new ColumnSet(true);
                processQuery.Criteria.AddCondition("name", ConditionOperator.Equal, projTplName);
                processQuery.Criteria.AddCondition("primaryentity", ConditionOperator.Equal, "ddsm_project");
                EntityCollection processRetrieve = _orgService.RetrieveMultiple(processQuery);
                if (processRetrieve != null && processRetrieve.Entities.Count == 1)
                {
                    projBP.Id = processRetrieve.Entities[0].Attributes["workflowid"].ToString();
                    projBP.Name = processRetrieve.Entities[0].Attributes["name"].ToString();
                    projBP.Stages = new List<BusinessProcessStage>();
                    QueryExpression stageQuery = new QueryExpression("processstage");
                    stageQuery.ColumnSet = new ColumnSet("stagename", "processstageid");
                    stageQuery.Criteria.AddCondition("processid", ConditionOperator.Equal, new Guid(projBP.Id));
                    EntityCollection stageRetrieve = _orgService.RetrieveMultiple(stageQuery);
                    if (stageRetrieve != null && stageRetrieve.Entities.Count > 0)
                    {
                        foreach (var stage in stageRetrieve.Entities)
                        {
                            BusinessProcessStage stageObj = new BusinessProcessStage();
                            stageObj.Id = stage.Attributes["processstageid"].ToString();
                            stageObj.Name = stage.Attributes["stagename"].ToString();
                            projBP.Stages.Add(stageObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                projBP = new BusinessProcess();
                projBP.Id = null;

            }
            return projBP;
        }

    }
}
