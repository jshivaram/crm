﻿using CoreUtils.Utils;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.JSON;

namespace ModelBulkDiscontinue
{
    public class ModelBulkDiscontinue : CodeActivity
    {
        [Input("Bulk Edit Request")]
        public InArgument<string> BulkEditRequest { get; set; }
        [Output("Reply")]
        public OutArgument<string> Reply { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            ITracingService tracer = executionContext.GetExtension<ITracingService>();
            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            EditResponse responseCounts = new EditResponse();

            try
            {
                string requestJson = BulkEditRequest.Get<string>(executionContext);
                EditRequest editRequest = JsonConvert.DeserializeObject<EditRequest>(requestJson);

                string ids = string.Empty;

                foreach (var id in editRequest.ModelIds)
                {
                    ids += $"<value>{id}</value>";
                }

                var fetchXml = @"<fetch>
                  <entity name='ddsm_modelnumber' >
                    <attribute name='statecode' />
                    <attribute name='ddsm_approvalenddate' />
                    <attribute name='ddsm_modelnumberid' />
                    <attribute name='statuscode' />
                    <filter>
                      <condition attribute='ddsm_modelnumberid' operator='in' >"
                        + ids +
                     @"</condition>
                    </filter>
                    <link-entity name='ddsm_sku' from='ddsm_modelnumber' to='ddsm_modelnumberid' link-type='outer' alias='sku' >
                      <attribute name='ddsm_skuid' />
                      <attribute name='statecode' />
                      <attribute name='statuscode' />
                      <attribute name='ddsm_enddate' />
                      <link-entity name='ddsm_skuapproval' from='ddsm_skuid' to='ddsm_skuid' link-type='outer' alias='skuapproval' >
                        <attribute name='ddsm_approvalenddate' />
                        <attribute name='statecode' />
                        <attribute name='statuscode' />
                        <attribute name='ddsm_skuapprovalid' />
                        <link-entity name='ddsm_measuretemplate' from='ddsm_measuretemplateid' to='ddsm_measuretemplateid' link-type='outer' >
                          <attribute name='ddsm_measuretemplateid' />
                          <link-entity name='ddsm_programoffering' from='ddsm_programofferingid' to='ddsm_programoffering' link-type='outer' alias='programoffering_skua' >
                            <attribute name='ddsm_programofferingid' />
                          </link-entity>
                        </link-entity>
                      </link-entity>
                    </link-entity>
                    <link-entity name='ddsm_modelnumberapproval' from='ddsm_modelnumberid' to='ddsm_modelnumberid' link-type='outer' alias='modelnumberapproval' >
                      <attribute name='ddsm_modelnumberapprovalid' />
                      <attribute name='ddsm_approvalenddate' />
                      <attribute name='statecode' />
                      <attribute name='statuscode' />
                      <link-entity name='ddsm_measuretemplate' from='ddsm_measuretemplateid' to='ddsm_measuretemplateid' link-type='outer' >
                        <attribute name='ddsm_measuretemplateid' />
                        <link-entity name='ddsm_programoffering' from='ddsm_programofferingid' to='ddsm_programoffering' link-type='outer' alias='programoffering_mna' >
                          <attribute name='ddsm_programofferingid' />
                        </link-entity>
                      </link-entity>
                    </link-entity>
                  </entity>
                </fetch>";

                FetchXmlToQueryExpressionRequest conversionRequest = new FetchXmlToQueryExpressionRequest();
                conversionRequest.FetchXml = fetchXml;

                var conversionResponse = (FetchXmlToQueryExpressionResponse)service.Execute(conversionRequest);
                QueryExpression queryExpression = conversionResponse.Query;
                
                EntityCollection entities = service.RetrieveMultiple(queryExpression);

                var requestWithResults = new ExecuteMultipleRequest()
                { 
                    Settings = new ExecuteMultipleSettings()
                    {
                        ContinueOnError = false,
                        ReturnResponses = true
                    },
                    Requests = new OrganizationRequestCollection()
                };

                

                ISet<Guid> processedIds = new HashSet<Guid>();
                ISet<Guid> processedSkuIds = new HashSet<Guid>();
                ISet<Guid> processedMnaIds = new HashSet<Guid>();
                ISet<Guid> processedSkuaIds = new HashSet<Guid>();

                foreach (var entity in entities.Entities)
                {
                    bool MNUpdated = false;
                    bool MNAUpdated = false;
                    bool SKUUpdated = false;
                    bool SKUAUpdated = false;

                    if (!processedIds.Contains(entity.Id)) {
                        if (editRequest.MakeInactiveMN)
                        {
                            Entity e = new Entity
                            {
                                Id = entity.Id,
                                LogicalName = entity.LogicalName
                            };
                            e.Attributes.Add("statecode", new OptionSetValue(1));
                            e.Attributes.Add("statuscode", new OptionSetValue(2));

                            UpdateRequest createRequest = new UpdateRequest { Target = e };
                            requestWithResults.Requests.Add(createRequest);
                            MNUpdated = true;
                        }

                        if (editRequest.EndDateMN != null)
                        {
                            Entity e = new Entity
                            {
                                Id = entity.Id,
                                LogicalName = entity.LogicalName
                            };
                            e.Attributes.Add("ddsm_approvalenddate", editRequest.EndDateMN);

                            UpdateRequest createRequest = new UpdateRequest { Target = e };
                            requestWithResults.Requests.Add(createRequest);
                            MNUpdated = true;
                        }
                    }

                    if (entity.Attributes.Contains("sku.ddsm_skuid"))
                    {
                        AliasedValue av = (AliasedValue)entity["sku.ddsm_skuid"];

                        if (editRequest.MakeInactiveSKU && !processedSkuIds.Contains((Guid)av.Value))
                        {
                            requestWithResults.Requests.Add(DeactivateLinkedEntity(entity, "sku.ddsm_skuid"));
                            SKUUpdated = true;
                        }

                        if (editRequest.EndDateSKU != null && !processedSkuIds.Contains((Guid)av.Value))
                        {

                            Entity e = new Entity
                            {
                                Id = (Guid)av.Value,
                                LogicalName = av.EntityLogicalName
                            };
                            e.Attributes.Add("ddsm_enddate", editRequest.EndDateSKU);

                            UpdateRequest createRequest = new UpdateRequest { Target = e };
                            requestWithResults.Requests.Add(createRequest);
                            SKUUpdated = true;
                        }

                        processedSkuIds.Add((Guid)av.Value);
                    }

                    

                    Guid MNAprogramOfferingId = Guid.Empty;

                    if (entity.Attributes.Contains("programoffering_mna.ddsm_programofferingid"))
                    {
                        AliasedValue av = (AliasedValue)entity["programoffering_mna.ddsm_programofferingid"];
                        MNAprogramOfferingId = (Guid)av.Value;
                    }

                    if (editRequest.MNAonlyForProgramOfferingId == Guid.Empty || editRequest.MNAonlyForProgramOfferingId == MNAprogramOfferingId)
                    {
                        if (entity.Attributes.Contains("modelnumberapproval.ddsm_modelnumberapprovalid"))
                        {
                            AliasedValue av = (AliasedValue)entity["modelnumberapproval.ddsm_modelnumberapprovalid"];

                            if (editRequest.MakeInactiveMNA && !processedMnaIds.Contains((Guid)av.Value))
                            {
                                requestWithResults.Requests.Add(DeactivateLinkedEntity(entity, "modelnumberapproval.ddsm_modelnumberapprovalid"));
                                MNAUpdated = true;
                            }

                            if (editRequest.EndDateMNA != null && !processedMnaIds.Contains((Guid)av.Value))
                            {
                                Entity e = new Entity
                                {
                                    Id = (Guid)av.Value,
                                    LogicalName = av.EntityLogicalName
                                };
                                e.Attributes.Add("ddsm_approvalenddate", editRequest.EndDateMNA);

                                UpdateRequest createRequest = new UpdateRequest { Target = e };
                                requestWithResults.Requests.Add(createRequest);
                                MNAUpdated = true;
                            }

                            processedMnaIds.Add((Guid)av.Value);
                        }

                        
                    }

                    Guid SKUAprogramOfferingId = Guid.Empty;

                    if (entity.Attributes.Contains("programoffering_skua.ddsm_programofferingid"))
                    {
                        AliasedValue av = (AliasedValue)entity["programoffering_skua.ddsm_programofferingid"];
                        SKUAprogramOfferingId = (Guid)av.Value;
                    }

                    if (editRequest.SKUAonlyForProgramOfferingId == Guid.Empty || editRequest.SKUAonlyForProgramOfferingId == SKUAprogramOfferingId)
                    {
                        if (entity.Attributes.Contains("skuapproval.ddsm_skuapprovalid"))
                        {
                            AliasedValue av = (AliasedValue)entity["skuapproval.ddsm_skuapprovalid"];

                            if (editRequest.MakeInactiveSKUA && !processedSkuaIds.Contains((Guid)av.Value))
                            {
                                requestWithResults.Requests.Add(DeactivateLinkedEntity(entity, "skuapproval.ddsm_skuapprovalid"));
                                SKUAUpdated = true;
                            }

                            if (editRequest.EndDateSKUA != null && !processedSkuaIds.Contains((Guid)av.Value))
                            {
                                Entity e = new Entity
                                {
                                    Id = (Guid)av.Value,
                                    LogicalName = av.EntityLogicalName
                                };
                                e.Attributes.Add("ddsm_approvalenddate", editRequest.EndDateSKUA);

                                UpdateRequest createRequest = new UpdateRequest { Target = e };
                                requestWithResults.Requests.Add(createRequest);
                                SKUAUpdated = true;
                            }

                            processedSkuaIds.Add((Guid)av.Value);
                        }
                    }

                    if (MNUpdated)
                        responseCounts.MNCount++;

                    if (MNAUpdated)
                        responseCounts.MNACount++;

                    if (SKUUpdated)
                        responseCounts.SKUCount++;

                    if (SKUAUpdated)
                        responseCounts.SKUACount++;

                    processedIds.Add(entity.Id);
                }

                ExecuteMultipleResponse response = (ExecuteMultipleResponse)service.Execute(requestWithResults);
                responseCounts.IsSuccessfull = !response.IsFaulted;

                string responseJson = JsonConvert.SerializeObject(responseCounts);

                Reply.Set(executionContext, responseJson);
                return;
            }
            catch (Exception e)
            {
                responseCounts.IsSuccessfull = false;
                string responseJson = JsonConvert.SerializeObject(responseCounts);

                Reply.Set(executionContext, responseJson);
                throw new InvalidPluginExecutionException(e.Message);
            }

            
        }

        private UpdateRequest DeactivateLinkedEntity(Entity entity, string attrName)
        {
            AliasedValue av = (AliasedValue)entity[attrName];

            Entity e = new Entity
            {
                Id = (Guid)av.Value,
                LogicalName = av.EntityLogicalName
            }; 
            e.Attributes.Add("statecode", new OptionSetValue(1));
            e.Attributes.Add("statuscode", new OptionSetValue(2));

            return new UpdateRequest { Target = e };
        }
    }

 

    
}
