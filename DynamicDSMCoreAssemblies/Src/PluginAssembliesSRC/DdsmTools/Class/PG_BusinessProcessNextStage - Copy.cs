﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Linq;
using static DdsmTools.ddsm;

namespace DdsmTools
{
    public class PG_BusinessProcessNextStage : CodeActivity
    {
        #region "Parameter Definition"
        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        [Output("Result")]
        public OutArgument<string> Result { get; set; }
        #endregion

        //[Input("Only Send Email To Group")]
        //public InArgument<bool> OnlySendEmailToGroup { get; set; }

        public enum MilestoneStatus
        {
            NotStarted = 962080000,
            Active = 962080001,
            Completed = 962080002,
            Skipped = 962080003
        }

        ApprovalsAmmountsRules approvalsAmmountsRules = ApprovalsAmmountsRules.SearchRange;
        ApprovalsFlowRules approvalsFlowRules = ApprovalsFlowRules.SkipSteps;
        Entity _projectGroup;
        ddsm objDdsm;
        string CurrentMsStepName;
        int ActiveMsIndex = 1, ActiveMsIndexTmp = 1;

        enum ResultMsgCode
        {
            Ok = 1,
            ApprovalLevelIsNotHighEnough = -200002,//You can not move project group forward, because your approval level is not high enough.
            YouDoNotBelongtotheProgram = -200001,//You can not move project group forward, because you do not belong to the program/group
            ProjectGroupDoesNotBelongtoAnyProgram = -200000,//You can not move project group forward, because the project group does not belong to any program/group. Notify system administrator!
            NoUserInTeam = -300000,//No one user in your team which have approval amount for next stage. Please add user with needed approval amount for next stage.

        }

        private EntityReference getTargetData()
        {
            EntityReference Target = null;

            if (objDdsm.context.InputParameters["Target"] is EntityReference)
            {
                Target = new EntityReference(((EntityReference)objDdsm.context.InputParameters["Target"]).LogicalName, ((EntityReference)objDdsm.context.InputParameters["Target"]).Id);
            }
            else if (objDdsm.context.InputParameters["Target"] is Entity)
            {
                Target = new EntityReference(((Entity)objDdsm.context.InputParameters["Target"]).LogicalName, ((Entity)objDdsm.context.InputParameters["Target"]).Id);
            }

            return Target;
        }

        protected override void Execute(CodeActivityContext executionContext)
        {
            objDdsm = new ddsm(executionContext);

            objDdsm.tracingService.Trace("Load CRM Service from context --- OK");

            var Target = getTargetData();

            Guid recordGuid = Target.Id;
            string LogicalName = Target.LogicalName;
            ResultMsgCode ResultMsg = ResultMsgCode.Ok;
            bool CompleteStatus = true, startStatus = false, UserOwnedTeam = false;


            decimal MsApprovalLimit = -1,
                UserApprovalLimit = -1,
                PgApprovalAmount = -1,
                PrevMsApprovalLimit = -1;
            DateTime currentDate = DateTime.Now;
            try
            {
                //Get Data Project Group
                _projectGroup = objDdsm.orgService.Retrieve("ddsm_projectgroup", Target.Id, new ColumnSet("ddsm_projectgroupid", "ddsm_name", "ddsm_approvalsammountsrules", "ddsm_approvalsflowrules", "ddsm_approvalamount", "ownerid", "ddsm_projectgrouptemplates", "traversedpath", "processid", "stageid"));
                if (_projectGroup.Attributes.ContainsKey("ddsm_approvalamount"))
                    PgApprovalAmount = _projectGroup.GetAttributeValue<Money>("ddsm_approvalamount").Value;

                getCurrentApprovalAmountRules();
                objDdsm.tracingService.Trace("Pg Approval Amount: " + PgApprovalAmount);

                //Get Initiating User
                Guid UserGuid = objDdsm.context.InitiatingUserId;
                objDdsm.tracingService.Trace("Initiating User Id: " + objDdsm.context.InitiatingUserId);

                var recordTeam = ddsm.getTeam(_projectGroup["ownerid"] as EntityReference, (_projectGroup["ddsm_projectgrouptemplates"] as EntityReference).Name, objDdsm.orgService);
                if (recordTeam.LogicalName != null)
                    UserOwnedTeam = ddsm.CheckUserInTeam(objDdsm.orgService, recordTeam.Id, UserGuid);

                //Get User Approval Limit
                UserApprovalLimit = getUserApprovalLimit(UserGuid);

                //Get BP PG
                var traversedpath = _projectGroup.GetAttributeValue<string>("traversedpath");
                var processid = _projectGroup.GetAttributeValue<Guid>("processid");
                var stageid = _projectGroup.GetAttributeValue<Guid>("stageid");
                var PG_BP = new BusinessProcess();
                PG_BP = objDdsm.getBusinessProcess(objDdsm, processid, LogicalName);
                //Get Milestone PG
                var MSs = getMilestone(Target.Id, objDdsm.orgService);


                MsApprovalLimit = getActiveMSData(MSs, out ActiveMsIndex, out CurrentMsStepName);


                if (ActiveMsIndex > 1 && MSs[ActiveMsIndex - 1].Attributes.ContainsKey("ddsm_approvalthresholdcust"))
                    PrevMsApprovalLimit = MSs[ActiveMsIndex - 1].GetAttributeValue<Money>("ddsm_approvalthresholdcust").Value;

                //For Approvals Logic
                if (MsApprovalLimit != -1 && MsApprovalLimit != -2)
                {
               
                    if (UserApprovalLimit > PrevMsApprovalLimit && UserApprovalLimit <= MsApprovalLimit && approvalsAmmountsRules == ApprovalsAmmountsRules.SearchRange)
                        startStatus = true;
                    else if (UserApprovalLimit >= PgApprovalAmount && ActiveMsIndex == 1)
                        startStatus = true;
                    else if (MsApprovalLimit == 0)
                        startStatus = true;
                    // разрешает скипать с любого шага до Payment Processed
                    else if (UserApprovalLimit > PgApprovalAmount)
                    {
                        startStatus = true;
                    }
                    else
                    {
                        startStatus = false;
                        ResultMsg = ResultMsgCode.ApprovalLevelIsNotHighEnough; //You can not move project group forward, because your approval level is not high enough.
                    }

                    if (ActiveMsIndex == MSs.Entities.Count - 3 && UserApprovalLimit <= PgApprovalAmount)
                    {
                        startStatus = false;
                        ResultMsg = ResultMsgCode.ApprovalLevelIsNotHighEnough; //You can not move project group forward, because your approval level is not high enough.
                    }
                    else if (ActiveMsIndex == MSs.Entities.Count - 3 && UserApprovalLimit > PgApprovalAmount)
                    {
                        startStatus = true;
                        ResultMsg = ResultMsgCode.ApprovalLevelIsNotHighEnough; //You can not move project group forward, because your approval level is not high enough.
                    }


                }

                //Business Process Steps
                if ((MsApprovalLimit == -1 || MsApprovalLimit == -2 || (MsApprovalLimit != -1 && MsApprovalLimit != -2 && startStatus)) && recordTeam.LogicalName != null && UserOwnedTeam)
                {
                    var updatePG = new Entity("ddsm_projectgroup", _projectGroup.GetAttributeValue<Guid>("ddsm_projectgroupid"));

                    for (int i = ActiveMsIndex; i < MSs.Entities.Count; i++)
                    {
                        if ((MsApprovalLimit == -1 || MsApprovalLimit == -2) && (UserApprovalLimit > PgApprovalAmount))
                        {
                            //Standart Stage Logic
                            if (MsApprovalLimit == -1)
                            {
                                if (ActiveMsIndex == ActiveMsIndexTmp && (int)MSs[i].GetAttributeValue<OptionSetValue>("ddsm_status").Value == (int)MilestoneStatus.Active)
                                {
                                    MSs[i]["ddsm_status"] = new OptionSetValue((int)MilestoneStatus.Completed);
                                    MSs[i]["ddsm_actualend"] = currentDate;
                                }
                            }
                            else
                            {
                                MSs[i]["ddsm_status"] = new OptionSetValue((int)MilestoneStatus.Skipped);
                                MSs[i]["ddsm_actualend"] = MSs[i]["ddsm_actualstart"];
                                currentDate = MSs[i].GetAttributeValue<DateTime>("ddsm_actualstart");
                            }
                        }
                        else
                        {
                            //Approval Logic
                            bool statusUser = false;
                            if (UserApprovalLimit > PrevMsApprovalLimit && UserApprovalLimit <= MsApprovalLimit && approvalsAmmountsRules == ApprovalsAmmountsRules.SearchRange)
                                statusUser = true;
                            else if (UserApprovalLimit >= PgApprovalAmount)
                                statusUser = true;
                            else if (MsApprovalLimit == 0)
                                statusUser = true;

                            if (ActiveMsIndex == MSs.Entities.Count - 3 && UserApprovalLimit <= PgApprovalAmount)
                                statusUser = false;

                            if (i == MSs.Entities.Count - 2 && MsApprovalLimit != 0)
                                statusUser = false;
                        
                            if (statusUser && ActiveMsIndex == ActiveMsIndexTmp && (int)MSs[i].GetAttributeValue<OptionSetValue>("ddsm_status").Value == (int)MilestoneStatus.Active)
                            {
                                MSs[i]["ddsm_status"] = new OptionSetValue((int)MilestoneStatus.Completed);
                                MSs[i]["ddsm_actualend"] = currentDate;

                                objDdsm.tracingService.Trace("Milestone: " + MSs[i]["ddsm_name"].ToString() + " status: " + Enum.GetName(typeof(MilestoneStatus), ((OptionSetValue)MSs[i]["ddsm_status"]).Value));
                                ddsm.setApprovalAuditRecord(objDdsm.orgService, MSs[i]["ddsm_name"].ToString(), new EntityReference("ddsm_projectgroup", new Guid(_projectGroup["ddsm_projectgroupid"].ToString())), new EntityReference("systemuser", UserGuid), PgApprovalAmount);
                            }
                            else if (statusUser && (int)MSs[i].GetAttributeValue<OptionSetValue>("ddsm_status").Value == (int)MilestoneStatus.Active && UserApprovalLimit >= PgApprovalAmount && approvalsFlowRules == ApprovalsFlowRules.SkipSteps)
                            {
                                MSs[i]["ddsm_status"] = new OptionSetValue((int)MilestoneStatus.Skipped);
                                MSs[i]["ddsm_actualend"] = MSs[i]["ddsm_actualstart"];
                                currentDate = MSs[i].GetAttributeValue<DateTime>("ddsm_actualstart");

                                objDdsm.tracingService.Trace("Milestone: " + MSs[i]["ddsm_name"].ToString() + " status: " + Enum.GetName(typeof(MilestoneStatus), ((OptionSetValue)MSs[i]["ddsm_status"]).Value));
                                ddsm.setApprovalAuditRecord(objDdsm.orgService, MSs[i]["ddsm_name"].ToString(), new EntityReference("ddsm_projectgroup", new Guid(_projectGroup["ddsm_projectgroupid"].ToString())), new EntityReference("systemuser", UserGuid), PgApprovalAmount);
                            }
                        }

                        if (i + 1 != MSs.Entities.Count)
                        {
                            MSs[i + 1]["ddsm_targetstart"] = currentDate;
                            MSs[i + 1]["ddsm_actualstart"] = currentDate;

                            //если предыдущий шаг пропущен или закончен, то активируем следеющий шаг
                            if (MSs[i].GetAttributeValue<OptionSetValue>("ddsm_status").Value == (int)MilestoneStatus.Completed || MSs[i].GetAttributeValue<OptionSetValue>("ddsm_status").Value == (int)MilestoneStatus.Skipped)
                            {
                                ActiveMsIndexTmp = i + 1;
                                MSs[i + 1]["ddsm_status"] = new OptionSetValue((int)MilestoneStatus.Active);
                                

                                objDdsm.tracingService.Trace("Milestone: " + MSs[i + 1]["ddsm_name"].ToString() + " status: " + Enum.GetName(typeof(MilestoneStatus), ((OptionSetValue)MSs[i + 1]["ddsm_status"]).Value));

                                if (MSs[i + 1].Attributes.ContainsKey("ddsm_approvalthresholdcust"))
                                    MsApprovalLimit = MSs[i + 1].GetAttributeValue<Money>("ddsm_approvalthresholdcust").Value;
                                else
                                    MsApprovalLimit = -1;

                                //Update BP
                                string newStageid = PG_BP.Stages.Find(x => x.Name.Contains(MSs[i + 1]["ddsm_name"].ToString())).Id;
                                traversedpath = traversedpath + "," + newStageid;
                                updatePG["stageid"] = new Guid(newStageid);
                                updatePG["traversedpath"] = traversedpath;
                            }
                            else
                            {
                                MSs[i + 1]["ddsm_status"] = new OptionSetValue((int)MilestoneStatus.NotStarted);
                                objDdsm.tracingService.Trace("Milestone: " + MSs[i + 1]["ddsm_name"].ToString() + " status: " + Enum.GetName(typeof(MilestoneStatus), ((OptionSetValue)MSs[i + 1]["ddsm_status"]).Value));
                                MSs[i + 1]["ddsm_targetend"] = currentDate.AddMilliseconds(Convert.ToInt32(MSs[i + 1]["ddsm_duration"]) * 86400000);
                            }
                            if (MSs[i].Attributes.ContainsKey("ddsm_approvalthresholdcust"))
                                PrevMsApprovalLimit = MSs[i].GetAttributeValue<Money>("ddsm_approvalthresholdcust").Value;
                            else
                                PrevMsApprovalLimit = -1;
                        }

                        //Project Group Completed
                        if (i + 1 == MSs.Entities.Count - 1 && (int)MSs[i].GetAttributeValue<OptionSetValue>("ddsm_status").Value == (int)MilestoneStatus.Completed)
                        {
                            updatePG["ddsm_enddate"] = MSs[i]["ddsm_actualend"];
                            updatePG["ddsm_projectgroupstatus"] = new OptionSetValue(962080003); //Completed
                        }
                    }

                    // Update Records 
                    if (CompleteStatus)
                    {
                        var emRequest = new ExecuteMultipleRequest
                        {
                            Requests = new OrganizationRequestCollection(),
                            Settings = new ExecuteMultipleSettings
                            {
                                ContinueOnError = false,
                                ReturnResponses = true
                            }
                        };

                        var updateRequest = new UpdateRequest();
                        updateRequest.Target = updatePG;
                        emRequest.Requests.Add(updateRequest);

                        for (int i = ActiveMsIndex; i < MSs.Entities.Count; i++)
                        {
                            updateRequest = new UpdateRequest();
                            updateRequest.Target = MSs[i];
                            emRequest.Requests.Add(updateRequest);
                        }
                        var emResponse = (ExecuteMultipleResponse)objDdsm.orgService.Execute(emRequest);
                        foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
                        {
                            // A valid response.
                            if (responseItem.Response != null)
                            {
                                if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                                {
                                }
                            }
                            // An error has occurred.
                            else if (responseItem.Fault != null)
                            {
                                objDdsm.tracingService.Trace("Error: Name: " + emRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message);
                            }
                        }
                    }
                    ResultMsg = ResultMsgCode.Ok;
                }
                else
                {
                    if (recordTeam.LogicalName == null)
                        ResultMsg = ResultMsgCode.ProjectGroupDoesNotBelongtoAnyProgram;// "-200000"; //You can not move project group forward, because the project group does not belong to any program/group. Notify system administrator!
                    else if (!UserOwnedTeam)
                        ResultMsg = ResultMsgCode.YouDoNotBelongtotheProgram; //"-200001"; //You can not move project group forward, because you do not belong to the program/group
                                                                              // if (ResultMsg == string.Empty)
                                                                              //    ResultMsg = "-200002"; //You can not move project group forward, because your approval level is not high enough.
                    CompleteStatus = false;

                    // Search new User
                    if (ResultMsg != ResultMsgCode.ProjectGroupDoesNotBelongtoAnyProgram) //"-200000"
                    {
                        var currentActiveStep = MSs.Entities.FirstOrDefault(x=> (x["ddsm_status"] as OptionSetValue).Value ==(int) MilestoneStatus.Active);
                        CurrentMsStepName = currentActiveStep["ddsm_name"].ToString();

 

                        ddsm.searchAndSendEmailToUser(objDdsm, recordTeam.Id, approvalsAmmountsRules, PgApprovalAmount, PrevMsApprovalLimit, MsApprovalLimit, CurrentMsStepName, _projectGroup.Id, MSs);
                    }

                }
                //if (CompleteStatus)
                //{
                //    // Create Project Group Financial Task Queu
                //}
            }
            catch (NoUserinTeamException ex)
            {
                ResultMsg = ResultMsgCode.NoUserInTeam;
                CompleteStatus = false;
            }
            catch (Exception ex)
            {
                objDdsm.tracingService.Trace("Error:" + ex.Message + ex.StackTrace);
                // ResultMsg = "Error:" + ex.Message + ex.StackTrace;
                CompleteStatus = false;
            }
            finally
            {
                objDdsm.tracingService.Trace("CompleteStatus: " + CompleteStatus + " --- ResultMsg: " + ResultMsg);
                //Callback plugin
                this.Result.Set(executionContext, ((int)ResultMsg).ToString());
                this.Complete.Set(executionContext, CompleteStatus);
            }
        }

        private decimal getActiveMSData(EntityCollection MSs, out int activeMsIndex, out string currentMsStepName)
        {
            decimal MsApprovalLimit = 0;
            activeMsIndex = -1;
            currentMsStepName = "";

            foreach (Entity milestone in MSs.Entities)
            {
                if (milestone.GetAttributeValue<OptionSetValue>("ddsm_status").Value == (int)MilestoneStatus.Active)
                {
                    activeMsIndex = ActiveMsIndexTmp = (milestone.GetAttributeValue<int>("ddsm_index") - 1);
                    object objTmp = null;
                    if (milestone.Attributes.TryGetValue("ddsm_approvalthresholdcust", out objTmp))
                    {
                        MsApprovalLimit = milestone.GetAttributeValue<Money>("ddsm_approvalthresholdcust").Value;

                        if (milestone.Attributes.TryGetValue("ddsm_name", out objTmp))
                            currentMsStepName = (string)objTmp;
                        break;
                    }
                }
            }
            return MsApprovalLimit;

        }

        private decimal getUserApprovalLimit(Guid userGuid)
        {
            Decimal result = -1;
            var query = new QueryExpression
            {
                EntityName = "ddsm_userapproval",
                ColumnSet = new ColumnSet("ddsm_userapprovalid", "ddsm_name", "ddsm_approvalthresholdcust"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                        {
                            new ConditionExpression("statecode", ConditionOperator.Equal, "Active"),
                            new ConditionExpression("statuscode", ConditionOperator.Equal, 1),
                            new ConditionExpression("ddsm_user", ConditionOperator.Equal, userGuid)
                        }
                }
            };
            var UARetrieve = objDdsm.orgService.RetrieveMultiple(query);
            if (UARetrieve.Entities.Count > 0 && UARetrieve[0].Attributes.ContainsKey("ddsm_approvalthresholdcust"))
                result = UARetrieve[0].GetAttributeValue<Money>("ddsm_approvalthresholdcust").Value;

            objDdsm.tracingService.Trace("User Approval Limit: " + result);
            return result;
        }

        //Get PG Milestones Collection
        private EntityCollection getMilestone(Guid recordGuid, IOrganizationService orgService)
        {
            var query = new QueryExpression()
            {
                EntityName = "ddsm_projectgroupmilestone",
                ColumnSet = new ColumnSet("ddsm_projectgroupmilestoneid", "ddsm_name", "ddsm_index", "ddsm_status", "ddsm_targetstart", "ddsm_targetend", "ddsm_actualstart", "ddsm_actualend", "ddsm_duration", "ddsm_approvalthresholdcust"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("statecode", ConditionOperator.Equal, "Active"),
                        new ConditionExpression("statuscode", ConditionOperator.Equal, 1),
                        new ConditionExpression("ddsm_projectgrouptoprojectgroupapproid", ConditionOperator.Equal, recordGuid),
                    }
                }
            };
            query.AddOrder("ddsm_index", OrderType.Ascending);

            var MSRetrieve = orgService.RetrieveMultiple(query);

            return MSRetrieve;
        }

        private void getCurrentApprovalAmountRules()
        {
            object sss = null;
            if (_projectGroup.Attributes.TryGetValue("ddsm_approvalsammountsrules", out sss))
            {
                approvalsAmmountsRules = (ApprovalsAmmountsRules)(sss as OptionSetValue).Value;
            }
            if (_projectGroup.Attributes.TryGetValue("ddsm_approvalsflowrules", out sss))
            {
                approvalsFlowRules = (ApprovalsFlowRules)(sss as OptionSetValue).Value;
            }
        }
    }
}
