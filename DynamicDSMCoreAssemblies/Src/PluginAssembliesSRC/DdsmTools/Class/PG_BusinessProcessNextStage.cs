﻿using DdsmTools.Model;
using DdsmTools.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Workflow;
using System.Activities;
using System.Linq;
using static DdsmTools.Model.Enums;
using System;

namespace DdsmTools
{
    public class PG_BusinessProcessNextStage : CodeActivity
    {
        #region "Parameter Definition"
        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        [Output("Result")]
        public OutArgument<string> Result { get; set; }
        #endregion


        ProjectGroup _projectGroup;
        UserApproval _userApproval;

        protected override void Execute(CodeActivityContext executionContext)
        {
            try
            {
                //initialize PG
                _projectGroup = new ProjectGroup(executionContext);
                _userApproval = new UserApproval(_projectGroup);

                if (!UserIsValid() && _projectGroup.Approvals)
                {
                    _projectGroup.ProcessingError = ErrorCode.UserNotInTeam;
                    var errorMsg = Helper.ErrorsList.Single(x => x.Key.Equals(ErrorCode.UserNotInTeam)).Value;
                    errorMsg = FormatErrorMsg(errorMsg);
                    _projectGroup.ObjCommon.TracingService.Trace(errorMsg);
                    Result.Set(executionContext, errorMsg);
                    Complete.Set(executionContext, false);
                    return;
                }

                var emRequest = _projectGroup.NextStage(_userApproval);

                if (emRequest == null || emRequest.Requests.Count == 0 || _projectGroup.ProcessingError != ErrorCode.Ok)
                {
                    var errorMsg = Helper.ErrorsList.Single(x => x.Key.Equals(_projectGroup.ProcessingError)).Value;
                    errorMsg = FormatErrorMsg(errorMsg);
                    _projectGroup.ObjCommon.TracingService.Trace(errorMsg);
                    Result.Set(executionContext, errorMsg);
                    Complete.Set(executionContext, false);
                    return;
                }

                var result = (ExecuteMultipleResponse)_projectGroup.ObjCommon.GetOrgService().Execute(emRequest);
                if (!result.IsFaulted)
                {
                    Result.Set(executionContext, "OK");
                    Complete.Set(executionContext, true);
                }
                else
                {
                    foreach (ExecuteMultipleResponseItem responseItem in result.Responses)
                    {
                        // An error has occurred.
                        if (responseItem.Fault != null)
                        {
                            var msg = "Error: Name: " + emRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message;
                            _projectGroup.ObjCommon.TracingService.Trace(msg);
                            Result.Set(executionContext, msg);
                        }
                    }
                    Complete.Set(executionContext, false);
                }
            }
            catch (System.Exception ex)
            {
                _projectGroup.ObjCommon.TracingService.Trace(ex.Message);
                Result.Set(executionContext, ex.Message);
                Complete.Set(executionContext, false);
            }
        }

        private string FormatErrorMsg(string errorMsg)
        {
            var result = "";
            switch (_projectGroup.ProcessingError)
            {
                case ErrorCode.YouAreNotInTheIntervalOfStep:
                    result = string.Format(errorMsg, Convert.ToInt32(_projectGroup.UserApproval.Amount),
                        Convert.ToInt32(_projectGroup.Milestones.PrevStage.Amount),
                        Convert.ToInt32(_projectGroup.Milestones.CurrentStage.Amount));
                    break;
                case ErrorCode.YouAreNotInTheIntervalOfStep2:
                    result = string.Format(errorMsg, Convert.ToInt32(_projectGroup.UserApproval.Amount),
                         Convert.ToInt32(_projectGroup.Milestones.PrevStage.Amount),
                        Convert.ToInt32(_projectGroup.Milestones.CurrentStage.Amount));
                    break;
                case ErrorCode.UserNotInTeam:
                    result = string.Format(errorMsg, _projectGroup.Tempalte?.Name.Split(' ')[0]);
                    break;
            }
            return result;
        }

        private bool UserIsValid()
        {
            var result = false;
            var recordTeam = _projectGroup.Team;

            if (recordTeam.LogicalName != null)
                result = Ddsm.CheckUserInTeam(_projectGroup.ObjCommon.GetOrgService(systemCall: true), recordTeam.Id, _userApproval.Id);

            return result;
        }
        /*
private decimal getActiveMSData(EntityCollection MSs, out int activeMsIndex, out string currentMsStepName)
{
   decimal MsApprovalLimit = 0;
   activeMsIndex = -1;
   currentMsStepName = "";

   foreach (Entity milestone in MSs.Entities)
   {
       if (milestone.GetAttributeValue<OptionSetValue>("ddsm_status").Value == (int)MilestoneStatus.Active)
       {
           activeMsIndex = ActiveMsIndexTmp = (milestone.GetAttributeValue<int>("ddsm_index") - 1);
           object objTmp = null;
           if (milestone.Attributes.TryGetValue("ddsm_approvalthresholdcust", out objTmp))
           {
               MsApprovalLimit = milestone.GetAttributeValue<Money>("ddsm_approvalthresholdcust").Value;

               if (milestone.Attributes.TryGetValue("ddsm_name", out objTmp))
                   currentMsStepName = (string)objTmp;
               break;
           }
       }
   }
   return MsApprovalLimit;

}

private decimal getUserApprovalLimit(Guid userGuid)
{
   Decimal result = -1;
   var query = new QueryExpression
   {
       EntityName = "ddsm_userapproval",
       ColumnSet = new ColumnSet("ddsm_userapprovalid", "ddsm_name", "ddsm_approvalthresholdcust"),
       Criteria = new FilterExpression(LogicalOperator.And)
       {
           Conditions =
               {
                   new ConditionExpression("statecode", ConditionOperator.Equal, "Active"),
                   new ConditionExpression("statuscode", ConditionOperator.Equal, 1),
                   new ConditionExpression("ddsm_user", ConditionOperator.Equal, userGuid)
               }
       }
   };
   var UARetrieve = objDdsm.orgService.RetrieveMultiple(query);
   if (UARetrieve.Entities.Count > 0 && UARetrieve[0].Attributes.ContainsKey("ddsm_approvalthresholdcust"))
       result = UARetrieve[0].GetAttributeValue<Money>("ddsm_approvalthresholdcust").Value;

   objDdsm.tracingService.Trace("User Approval Limit: " + result);
   return result;
}

//Get PG Milestones Collection
private EntityCollection getMilestone(Guid recordGuid, IOrganizationService orgService)
{
   var query = new QueryExpression()
   {
       EntityName = "ddsm_projectgroupmilestone",
       ColumnSet = new ColumnSet("ddsm_projectgroupmilestoneid", "ddsm_name", "ddsm_index", "ddsm_status", "ddsm_targetstart", "ddsm_targetend", "ddsm_actualstart", "ddsm_actualend", "ddsm_duration", "ddsm_approvalthresholdcust"),
       Criteria = new FilterExpression(LogicalOperator.And)
       {
           Conditions =
           {
               new ConditionExpression("statecode", ConditionOperator.Equal, "Active"),
               new ConditionExpression("statuscode", ConditionOperator.Equal, 1),
               new ConditionExpression("ddsm_projectgrouptoprojectgroupapproid", ConditionOperator.Equal, recordGuid),
           }
       }
   };
   query.AddOrder("ddsm_index", OrderType.Ascending);

   var MSRetrieve = orgService.RetrieveMultiple(query);

   return MSRetrieve;
}

private void getCurrentApprovalAmountRules()
{
   object sss = null;
   if (_projectGroup.Attributes.TryGetValue("ddsm_approvalsammountsrules", out sss))
   {
       approvalsAmmountsRules = (ApprovalsAmmountsRules)(sss as OptionSetValue).Value;
   }
   if (_projectGroup.Attributes.TryGetValue("ddsm_approvalsflowrules", out sss))
   {
       approvalsFlowRules = (ApprovalsFlowRules)(sss as OptionSetValue).Value;
   }
}
*/
    }
}
