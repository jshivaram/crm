﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;

namespace DdsmTools
{
    public class PreDeleteAttachDoc : IPlugin
    {

        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService orgService = serviceFactory.CreateOrganizationService(context.UserId);
            orgService = GetSystemOrgService(serviceFactory, orgService);

            try
            {

                EntityReference targetEntityRef = (EntityReference)context.InputParameters["Target"]; ;

                tracingService.Trace("Target: " + targetEntityRef.Id);
                
                string fetchCollection = @"<fetch version='1.0' output-format='xml-platform' mapping='logical'>
                    <entity name='ddsm_attachmentdocument' >
                        <attribute name='ddsm_attachmentdocumentid' />
                        <filter type='and' >
                            <condition attribute='ddsm_parent' operator='eq' value='" + targetEntityRef.Id + @"' />
                        </filter>
                    </entity>
                </fetch>";

                var fetchRequest = new RetrieveMultipleRequest
                {
                    Query = new FetchExpression(fetchCollection)
                };

                EntityCollection entityCollection = ((RetrieveMultipleResponse)orgService.Execute(fetchRequest)).EntityCollection;

                if (entityCollection.Entities.Count == 0) {
                    entityCollection = GetEntityCollection(orgService, targetEntityRef, "ddsm_requireddocument", tracingService);

                    if (entityCollection.Entities.Count > 0) {
                        foreach (Entity en in entityCollection.Entities)
                        {
                            fetchCollection = @"<fetch aggregate='true' returntotalrecordcount='true' >
                                <entity name='ddsm_attachmentdocument' >
                                    <attribute name='ddsm_name' alias='countDocs' aggregate='count' />
                                    <link-entity name='ddsm_attachdoc_ddsm_requireddocument' from='ddsm_attachmentdocumentid' to='ddsm_attachmentdocumentid' >
                                        <filter type='and' >
                                            <condition attribute='ddsm_requireddocumentid' operator='eq' value='" + en.Id + @"' />
                                        </filter>
                                    </link-entity>
                                </entity>
                            </fetch>";

                            fetchRequest = new RetrieveMultipleRequest
                            {
                                Query = new FetchExpression(fetchCollection)
                            };

                            EntityCollection attachCollection = ((RetrieveMultipleResponse)orgService.Execute(fetchRequest)).EntityCollection;
                            AliasedValue countDocs = (AliasedValue)attachCollection.Entities[0].Attributes["countDocs"];

                            tracingService.Trace(en.Id + " | " + en.LogicalName + " | " + ((int)countDocs.Value - 1));
                            if ((int)countDocs.Value < 2) {
                                Entity reqDoc = new Entity(en.LogicalName, en.Id);
                                reqDoc["ddsm_uploaded"] = false;
                                orgService.Update(reqDoc);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException(e.Message);
            }
        }

        private EntityCollection GetEntityCollection(IOrganizationService _orgService, EntityReference enRef, string entityName, ITracingService tracingService)
        {
            EntityCollection enCollection = new EntityCollection();

            try
            {
                string fetchCollection = @"<fetch version='1.0' output-format='xml-platform' mapping='logical'>
                <entity name='" + entityName + @"' >
                    <attribute name='" + entityName + @"id' />
                    <filter type='and' >
                        <condition entityname='ddsm_attachdoc_" + entityName + @"' attribute='ddsm_attachmentdocumentid' operator='eq' value='" + enRef.Id + @"' />
                    </filter>
                    <link-entity name='ddsm_attachdoc_" + entityName + @"' from='" + entityName + @"id' to='" + entityName + @"id' intersect='true' />
                </entity>
            </fetch>";

                var fetchRequest = new RetrieveMultipleRequest
                {
                    Query = new FetchExpression(fetchCollection)
                };

                enCollection = ((RetrieveMultipleResponse)_orgService.Execute(fetchRequest)).EntityCollection;
            }
            catch (Exception e)
            {
                tracingService.Trace(e.Message);
                //throw new InvalidPluginExecutionException(e.Message);
            }

            return enCollection;
        }

        private static Guid GetSystemUserId(string name, IOrganizationService orgService)
        {
            QueryByAttribute queryUsers = new QueryByAttribute
            {
                EntityName = "systemuser",
                ColumnSet = new ColumnSet("systemuserid")
            };

            queryUsers.AddAttributeValue("fullname", name);
            EntityCollection retrievedUsers = orgService.RetrieveMultiple(queryUsers);
            Guid systemUserId = ((Entity)retrievedUsers.Entities[0]).Id;

            return systemUserId;
        }

        private static IOrganizationService GetSystemOrgService(IOrganizationServiceFactory serviceFactory, IOrganizationService orgService)
        {
            IOrganizationService SystemService = serviceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", orgService));
            return SystemService;
            //return SystemService ?? (SystemService = serviceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", orgService)));
        }


    }
}
