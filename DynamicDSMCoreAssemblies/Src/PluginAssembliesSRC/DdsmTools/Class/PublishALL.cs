﻿using System;
using System.Activities;
using DDSM.CommonProvider;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Workflow;


public class PublishALL : CodeActivity
{
    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }
    [Output("Result")]
    public OutArgument<string> Result { get; set; }

    protected override void Execute(CodeActivityContext context)
    {
        try
        {
            var common = new Common(context);

            PublishAllXmlRequest publishReq = new PublishAllXmlRequest();
            
            common.GetOrgService().Execute(publishReq);
            Complete.Set(context, true);
            Result.Set(context, "");
        }
        catch (Exception e)
        {
            Complete.Set(context, false);
            Result.Set(context, e.Message);
        }
    }
}
