﻿using DdsmTools.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using DDSM.CommonProvider;

namespace DdsmTools.Class
{
    public class SendEmail : CodeActivity
    {
        [Input("Milestone Index")]
        public InArgument<int> Index { get; set; }

        Common _objCommon;
        Entity _target;
        ProjectGroup _projectGroup;
        protected override void Execute(CodeActivityContext context)
        {
            try
            {
                _objCommon = new Common(context);
                _target = (Entity)_objCommon.Context.InputParameters["Target"];

                //current milestone index
                var index = Index.Get(context);

                //current milestone data
                var targetData = GetMsData(_target);
                // get PG Ref
                var pG = GetPg(targetData);
                // get current PG
                _projectGroup = new ProjectGroup(pG, _objCommon);

                //get user for sending email
                var user = GetUserForNotification();

                sendEmail(user);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #region Internal Methods

        private void sendEmail(Entity user)
        {
            if (user != null)
            {
                Ddsm.SetApprovalAuditRecord(_objCommon.GetOrgService(systemCall: true), _projectGroup.Milestones.CurrentStage.Name, new EntityReference("ddsm_projectgroup", _projectGroup.Id), new EntityReference("systemuser", user.GetAttributeValue<EntityReference>("ddsm_user").Id), _projectGroup.AppprovalAmount, false);

                var emailTask = Ddsm.CreateEmailTask(user, _projectGroup.Milestones.CurrentStage.Name, _projectGroup.Id);
                if (!Ddsm.EmailTaskExist(_objCommon.GetOrgService(systemCall: true), emailTask.Target))
                    _objCommon.GetOrgService(systemCall: true).Create(emailTask.Target);
            }
            else {
                _objCommon.TracingService.Trace("Can't find user for Sending Email ((");
            }
            //    throw new Exception("Can't find user for Sending Email ((");
        }

        private Entity GetUserForNotification()
        {
            EntityCollection users;
            var randomUser = new Entity();
            //  if (_projectGroup.ApprovalsAmmountsRules == Ddsm.ApprovalsAmmountsRules.SearchRange)
            // {
            users = Ddsm.GetUserForSendEmail(_projectGroup.Milestones.PrevStage.Amount, _projectGroup.Team.Id, _projectGroup.Milestones.CurrentStage.Amount, _objCommon.GetOrgService(systemCall: true));
            randomUser = users.Entities.Random();
            // }
            //else
            //{
            //    users = Ddsm.GetUserForSendEmail(_projectGroup.AppprovalAmount, _projectGroup.Team.Id, _projectGroup.Milestones.CurrentStage.Amount, _objCommon.OrgService);
            //    randomUser = users.Entities[0];
            //}
            return randomUser;
        }

        private EntityReference GetPg(Entity targetData)
        {
            object obj = null;
            if (targetData.Attributes.TryGetValue("ddsm_projectgrouptoprojectgroupapproid", out obj))
                obj = (EntityReference)obj;

            return (EntityReference)obj;
        }

        private Entity GetMsData(Entity record)
        {
            return _objCommon.GetOrgService(systemCall: true).Retrieve(record.LogicalName, record.Id, new ColumnSet("ddsm_projectgroupmilestoneid", "ddsm_projectgrouptoprojectgroupapproid", "ddsm_name", "ddsm_index", "ddsm_status", "ddsm_targetstart", "ddsm_targetend", "ddsm_actualstart", "ddsm_actualend", "ddsm_duration", "ddsm_approvalthresholdcust"));
        }
        #endregion
    }
}
