﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace DdsmTools.Class
{
    public class SendEmailWhenCaseSharing : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracing = null;
            try
            {
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                tracing = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
                IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

                if (context.MessageName == "GrantAccess")
                {
                    object PrincipalAccess;
                    context.InputParameters.TryGetValue("PrincipalAccess", out PrincipalAccess); ;
                    var userOrTeam = (PrincipalAccess as PrincipalAccess).Principal;

                    if (userOrTeam.LogicalName == "systemuser")
                    {
                        Entity userTo = service.Retrieve("systemuser", userOrTeam.Id, new ColumnSet("fullname", "personalemailaddress", "internalemailaddress"));

                        if(userTo.GetAttributeValue<string>("personalemailaddress") == null && userTo.GetAttributeValue<string>("internalemailaddress") == null)
                        {
                            throw new InvalidPluginExecutionException("User " + userTo.GetAttributeValue<string>("fullname") + " does not have mail");
                        }

                        EntityReference EntityRef = (EntityReference)context.InputParameters["Target"];

                        Entity incident = service.Retrieve(EntityRef.LogicalName, EntityRef.Id, new ColumnSet("ddsm_sharedtoname"));
                        incident["ddsm_sharedtoname"] = userTo.GetAttributeValue<string>("fullname");
                        service.Update(incident);

                        OrganizationRequest req = new OrganizationRequest("ddsm_DDSMSendEmailWhenCaseSharing");
                        req["RecipientReference"] = userOrTeam;
                        req["Target"] = EntityRef;
                        OrganizationResponse response = service.Execute(req);

                        incident["ddsm_sharedtoname"] = null;
                        service.Update(incident);
                    }
                }
            }
            catch (Exception e)
            {
                if (tracing != null)
                {
                    tracing.Trace(e.ToString());
                }
            }
        }
    }
}
