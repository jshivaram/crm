﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.ServiceModel;
using DDSM.CommonProvider;

namespace DdsmTools
{

    public class SetLookupByName : CodeActivity
    {
        [RequiredArgument]
        [Input("LookupEntity")]
        public InArgument<String> LookupEntity { get; set; }

        [RequiredArgument]
        [Input("LookupField")]
        public InArgument<String> LookupField { get; set; }

        [RequiredArgument]
        [Input("LookupName")]
        public InArgument<String> LookupName { get; set; }

        [RequiredArgument]
        [Input("TargetEntity")]
        public InArgument<String> TargetEntity { get; set; }

        [RequiredArgument]
        [Input("TargetField")]
        public InArgument<String> TargetField { get; set; }

        /*[Input("MN")]
        [ArgumentEntity("ddsm_modelnumber")]
        [ReferenceTarget("ddsm_modelnumber")]
        public InArgument<EntityReference> MN { get; set; }*/

        [Output("EntityReference")]
        [ArgumentEntity("account")]
        [ReferenceTarget("account")]
        public OutArgument<EntityReference> EntityRef { get; set; }

        //private Ddsm objCommon;

        protected override void Execute(CodeActivityContext executionContext)
        {
            ITracingService tracer = executionContext.GetExtension<ITracingService>();
            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            try
            {

                /*EntityReference entityTarget = new EntityReference(context.PrimaryEntityName, context.PrimaryEntityId);
                if (entityTarget.Id == Guid.Empty )
                {
                    tracer.Trace("Cannot get target");
                    return;
                }*/
                object target = null;
                if (context.InputParameters.Contains("Target") && (context.InputParameters.Contains("Target") != null))
                {
                    target = context.InputParameters["Target"];
                }
                else
                {
                    tracer.Trace("Cannot get target");
                    return;
                }

                var entityTarget = GetTargetData(target);
                Entity MN = new Entity();
                EntityReference partnerLookup = new EntityReference();
                //tracer.Trace("01. Start");

                if (context.InputParameters.Contains("Target"))
                {
                    //tracer.Trace("02. Target EXIST");
                    if (context.InputParameters["Target"] is Entity)
                    {
                        //tracer.Trace("02-2. Target Entity");
                        MN = (Entity)context.InputParameters["Target"];
                        //tracer.Trace("02-3. Target MN: " + MN.LogicalName.ToString() + " ID: " + MN.Id.ToString());
                        object mnPartnerLookup;
                        if (MN.Attributes.TryGetValue("ddsm_partner", out mnPartnerLookup))
                        {
                            partnerLookup = (EntityReference)mnPartnerLookup;
                            //tracer.Trace("03. Get MN lookup Partner OLD : " + partnerLookup.LogicalName.ToString() + " ID: " + partnerLookup.Id.ToString());
                        }
                    }
                }

                //tracer.Trace("04. Get MN lookup Partner END!!!.");

                var lookupEntity = LookupEntity.Get(executionContext)?.Trim();
                var lookupField = LookupField.Get(executionContext)?.Trim();
                var lookupName = LookupName.Get(executionContext)?.Trim();
                var targetEntity = TargetEntity.Get(executionContext)?.Trim();
                var targetField = TargetField.Get(executionContext)?.Trim();


                //tracer.Trace("05. lookupName: " + lookupName);
                //tracer.Trace("05. lookupName: " + lookupName);
                var lookupRef = GetLookupRef(service, lookupEntity, lookupField, lookupName, partnerLookup); //
                //tracer.Trace("06. Before Update Lookup Partner.");
                if (entityTarget != null && !String.IsNullOrEmpty(targetField) && lookupRef.Id != Guid.Empty)
                {
                    //tracer.Trace("07. In Update Lookup Partner.");
                    var ent = new Entity(entityTarget.LogicalName, entityTarget.Id);
                    ent.Attributes[targetField] = lookupRef;
                    service.Update(ent);
                    EntityRef.Set(executionContext, lookupRef);

                }
                else
                {
                    if (partnerLookup.Id == Guid.Empty)
                    {
                        //tracer.Trace("08. partnerLookup: " + partnerLookup.Id + " NAME: " + partnerLookup.LogicalName);
                        partnerLookup = GetLookupRefByID(service, targetEntity, MN.Id, targetField, tracer);
                        //tracer.Trace("09. partnerLookup: " + partnerLookup.Id + " NAME: " + partnerLookup.LogicalName);
                    }
                    //tracer.Trace("10. partnerLookup: " + partnerLookup.Id + " NAME: " + partnerLookup.LogicalName);
                    EntityRef.Set(executionContext, partnerLookup);
                }

            }
            catch (Exception e)
            {
                tracer.Trace(e.Message);
                throw new InvalidPluginExecutionException(e.Message);
            }
        }

        private EntityReference GetLookupRefByID(IOrganizationService service, string targetEntity, Guid RecordId, string targetField, ITracingService tracer)
        {
            //tracer.Trace("11. In GetLookupRefByID");
            var crmData = service.Retrieve(targetEntity, RecordId, new ColumnSet(targetField));
            //tracer.Trace("12. In GetLookupRefByID");
            object LookupPartner;
            if (crmData != null && crmData.Attributes.TryGetValue(targetField, out LookupPartner))
            {
                //tracer.Trace("13. In GetLookupRefByID");
                return (EntityReference)LookupPartner;
            }
            //tracer.Trace("14. In GetLookupRefByID");
            return null;
        }

        private EntityReference GetLookupRef(IOrganizationService service, string lookupEntity, string lookupField, string lookupName, EntityReference partnerLookup)
        {

            if (String.IsNullOrEmpty(lookupName))
            {
                return partnerLookup;
            }
            var query = new QueryExpression
            {
                EntityName = lookupEntity,
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = { new ConditionExpression(lookupField, ConditionOperator.Equal, lookupName) }
                }

            };
            var crmData = service.RetrieveMultiple(query);

            if (crmData.Entities.Count > 0)
            {
                return crmData.Entities[0].ToEntityReference();
            }
            else { return partnerLookup; }

        }
        private EntityReference GetTargetData(object target)
        {
            try
            {
                if (target is EntityReference)
                    return (EntityReference)target;

                if (target is Entity)
                {
                    var sourceEntity = (Entity)target;

                    return new EntityReference(sourceEntity.LogicalName, sourceEntity.Id);
                }
            }
            catch (Exception)
            {
                throw new Exception($"Can't get target from Context!");
            }

            return null;
        }

    }
}
