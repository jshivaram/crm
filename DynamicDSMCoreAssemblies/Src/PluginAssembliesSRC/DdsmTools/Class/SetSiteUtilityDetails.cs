﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;

namespace DdsmTools
{
    public class SetSiteUtilityDetails : CodeActivity
    {

        [Input("Utility Service Provider")]
        [AttributeTarget("ddsm_rateclassreference", "ddsm_utility")]
        public InArgument<OptionSetValue> UtilityServiceProvider { get; set; }

        [Input("Rate Class")]
        [AttributeTarget("ddsm_rateclassreference", "ddsm_rateclass")]
        public InArgument<OptionSetValue> RateClass { get; set; }

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            var objCommon = new Ddsm(executionContext);

            try
            {
                Entity entity = (Entity)objCommon.Context.InputParameters["Target"];

                var utilityServiceProvider = this.UtilityServiceProvider.Get(executionContext);
                var rateClass = this.RateClass.Get(executionContext);

                Entity updateSite = new Entity("ddsm_site", entity.Id);

                if (utilityServiceProvider != null)
                {
                    updateSite["ddsm_utility"] = new OptionSetValue((int)utilityServiceProvider.Value);
                }

                if (rateClass != null)
                {
                    updateSite["ddsm_rateclass"] = new OptionSetValue((int)rateClass.Value);
                }

                objCommon.OrgService.Update(updateSite);

                this.Complete.Set(executionContext, true);
            }
            catch (Exception e)
            {
                objCommon.TracingService.Trace("Error:" + e.Message);
                this.Complete.Set(executionContext, false);
            }
        }
    }
}
