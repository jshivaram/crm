using System;
using System.Collections.Generic;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using DdsmTools.Model;
using DDSM.CommonProvider;

namespace DdsmTools.Class
{

    public class TriggerFnclToProjectNextStage : CodeActivity
    {
        [RequiredArgument]
        [Input("Parent Project")]
        [ArgumentEntity("ddsm_project")]
        [ReferenceTarget("ddsm_project")]
        public InArgument<EntityReference> ParentProject { get; set; }

        [Input("Fncl Actual End")]
        public InArgument<DateTime> FnclActualEnd { get; set; }

        [Output("Complete")]
        public OutArgument<bool> Complete { get; set; }

        private TimeZoneInfo timeZoneInfo;

        Common _common;

        protected override void Execute(CodeActivityContext executionContext)
        {
            var objCommon = new Ddsm(executionContext);
            _common = new Common(executionContext);
            objCommon.TracingService.Trace("Load CRM Service from context --->>> OK");

            string windowsTimeZoneName = objCommon.GetTimeZoneByCRMConnection(objCommon.OrgService, objCommon.Context.UserId);
            timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZoneName);

            try
            {
                var parentProject = this.ParentProject.Get(executionContext);
                var actualEnd = this.FnclActualEnd.Get(executionContext);
                objCommon.TracingService.Trace("Fncl ActualEnd: {0}", actualEnd.ToString());
                objCommon.TracingService.Trace("Parent Project: {0}", parentProject.Id.ToString());

                if (actualEnd == null)
                    actualEnd = objCommon.ConverAttributToDateTimeUtc("today", timeZoneInfo);

                NextBpStage(objCommon, parentProject.Id, actualEnd);
                this.Complete.Set(executionContext, true);

            }
            catch (Exception ex)
            {
                objCommon.TracingService.Trace("Error:" + ex.Message);
                this.Complete.Set(executionContext, false);
            }

        }

        private void NextBpStage(Ddsm objCommon, Guid projectId, DateTime actualEnd)
        {
            //objCommon.tracingService.Trace("Start Next Stage BP --->>> OK");

            bool findActiveMs = false, phaseChenged = false; int idxActiveMs = 1, activeNewIdx = 2000000000, ForecastCompleteLastRecalcIndex = -1, phaseNumber = 0;
            BusinessProcess projBP = new BusinessProcess();
            string traversedpath = string.Empty, stageid = string.Empty, processid = string.Empty;

            var relationship = new Relationship();
            relationship.SchemaName = "ddsm_ddsm_project_ddsm_milestone";

            var query = new QueryExpression();
            query.EntityName = "ddsm_milestone";
            query.ColumnSet = new ColumnSet("ddsm_milestoneid", "ddsm_name", "ddsm_index", "ddsm_status", "ddsm_duration", "ddsm_actualstart", "ddsm_actualend", "ddsm_targetstart", "ddsm_targetend", "ddsm_responsible", "ddsm_projectphasename", "ddsm_projectstatus");
            query.Criteria = new FilterExpression();
            query.AddOrder("ddsm_index", OrderType.Ascending);
            query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

            var relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);

            var request = new RetrieveRequest();
            request.ColumnSet = new ColumnSet("ddsm_projectid","ddsm_name"
                , "ddsm_requirepayeeinfoindex"
                , "ddsm_payeeaddress1"
                , "ddsm_payeeaddress2"
                , "ddsm_payeecity"
                , "ddsm_payeestate"
                , "ddsm_payeezipcode"
                , "ddsm_payeecompany"
                , "ddsm_payeeattnto"
                , "ddsm_payeenumber"
                , "ddsm_payeephone"
                , "ddsm_payeeemail"
                , "ddsm_taxentity"
                , "ddsm_taxid"
                , "stageid"
                , "processid"
                , "ddsm_completeonlastmilestone"
                , "ddsm_lastmilestoneindex"
                , "traversedpath"
                , "ddsm_phasenumber"
                , "ddsm_phase"
                , "ddsm_projectstatus"
                , "ddsm_forecastcompletelastrecalcindex"
                );
            request.Target = new EntityReference { Id = projectId, LogicalName = "ddsm_project" };
            request.RelatedEntitiesQuery = relatedEntity;

            var response = (RetrieveResponse)objCommon.OrgService.Execute(request);

            DataCollection<Entity> relatedEntitis = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship("ddsm_ddsm_project_ddsm_milestone")].Entities;


            var projPhase = response.Entity.GetAttributeValue<string>("ddsm_phase");
            //Current phase number
            if (response.Entity.Attributes.ContainsKey("ddsm_phasenumber"))
            {
                phaseNumber = response.Entity.GetAttributeValue<int>("ddsm_phasenumber");
            }
            var updateProj = new Entity("ddsm_project", response.Entity.GetAttributeValue<Guid>("ddsm_projectid"));

            if (response.Entity.Attributes.ContainsKey("ddsm_forecastcompletelastrecalcindex"))
                ForecastCompleteLastRecalcIndex = response.Entity.GetAttributeValue<int>("ddsm_forecastcompletelastrecalcindex");

            if (response.Entity.Attributes.ContainsKey("processid"))
            {
                processid = Convert.ToString(response.Entity["processid"]);
            }

            if (response.Entity.Attributes.ContainsKey("stageid"))
            {
                stageid = Convert.ToString(response.Entity["stageid"]);
            }
            if (response.Entity.Attributes.ContainsKey("traversedpath"))
            {
                traversedpath = Convert.ToString(response.Entity["traversedpath"]);
            }

            if (!string.IsNullOrEmpty(processid))
            {
                projBP = GetBusinessProcess(objCommon.OrgService, new Guid(processid));
            }
            else
            { projBP.Id = null; }

            var updateMs = new Entity("ddsm_milestone");


            var msRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = true,
                    ReturnResponses = false
                }
            };
            DateTime msPrevEndtDay = actualEnd, ForecastComplete = DateTime.UtcNow;

            foreach (var ms in relatedEntitis)
            {
                if ((int)ms.GetAttributeValue<OptionSetValue>("ddsm_status").Value == 962080001 && !findActiveMs)
                {
                    objCommon.TracingService.Trace("Old Active: {0}: {1} --->>> (ActualEnd: {2})", ms.GetAttributeValue<int>("ddsm_index"), ms["ddsm_name"].ToString(), msPrevEndtDay);

                    findActiveMs = true;
                    idxActiveMs = (int)ms.GetAttributeValue<int>("ddsm_index");
                    activeNewIdx = idxActiveMs + 1;

                    updateMs = new Entity("ddsm_milestone", ms.GetAttributeValue<Guid>("ddsm_milestoneid"));
                    updateMs["ddsm_actualend"] = msPrevEndtDay;
                    updateMs["ddsm_status"] = new OptionSetValue(962080002);

                    UpdateRequest updateRequest = new UpdateRequest();
                    updateRequest.Target = updateMs;
                    msRequest.Requests.Add(updateRequest);

                    updateProj["ddsm_completedmilestone"] = ms["ddsm_name"].ToString();
                    updateProj["ddsm_completedstatus"] = ms["ddsm_projectstatus"].ToString();
                    updateProj["ddsm_completephase"] = ms["ddsm_projectphasename"].ToString();
                    //updateProj["ddsm_milestoneactualend"] = msPrevEndtDay;

                    if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0 && ForecastCompleteLastRecalcIndex == Convert.ToInt32(ms["ddsm_index"]))
                        ForecastComplete = msPrevEndtDay;

                    objCommon.TracingService.Trace("findActiveMs: {0}; idxActiveMs: {1}; MS Name: {2}", findActiveMs, idxActiveMs, ms["ddsm_name"].ToString());
                    break;
                }
            }

            foreach (var ms in relatedEntitis)
            {
                if ((int)ms.GetAttributeValue<OptionSetValue>("ddsm_status").Value == 962080000)
                {
                    if (ms.Contains("ddsm_duration") && ms["ddsm_duration"] != null)
                    {
                    }
                    else { ms["ddsm_duration"] = 0; }

                    if ((int)ms.GetAttributeValue<int>("ddsm_index") == idxActiveMs + 1)
                    {
                        updateMs = new Entity("ddsm_milestone", ms.GetAttributeValue<Guid>("ddsm_milestoneid"));
                        updateMs["ddsm_targetstart"] = msPrevEndtDay;
                        updateMs["ddsm_actualstart"] = msPrevEndtDay;
                        updateMs["ddsm_status"] = new OptionSetValue(962080001);

                        objCommon.TracingService.Trace("New Active: {0}: {1} --->>> (ActualStart: {2};)", ms.GetAttributeValue<int>("ddsm_index"), ms["ddsm_name"].ToString(), msPrevEndtDay);

                        updateProj["ddsm_pendingmilestone"] = ms["ddsm_name"].ToString();
                        updateProj["ddsm_pendingmilestoneindex"] = Convert.ToInt32(ms["ddsm_index"]);
                        updateProj["ddsm_pendingactualstart"] = msPrevEndtDay;
                        updateProj["ddsm_responsible"] = ms["ddsm_responsible"];

                        objCommon.TracingService.Trace("New Active: {0}: {1} --->>> (Duration: {2})", ms.GetAttributeValue<int>("ddsm_index"), ms["ddsm_name"].ToString(), Convert.ToDouble(ms["ddsm_duration"]));

                        msPrevEndtDay = msPrevEndtDay.AddDays(Convert.ToDouble(ms["ddsm_duration"]));

                        updateMs["ddsm_targetend"] = msPrevEndtDay;

                        objCommon.TracingService.Trace("New Active: {0}: {1} --->>> (TargetEnd: {2})", ms.GetAttributeValue<int>("ddsm_index"), ms["ddsm_name"].ToString(), msPrevEndtDay);

                        var updateRequest = new UpdateRequest();
                        updateRequest.Target = updateMs;
                        msRequest.Requests.Add(updateRequest);

                        updateProj["ddsm_pendingtargetend"] = updateMs["ddsm_targetend"];
                        updateProj["ddsm_projectstatus"] = ms["ddsm_projectstatus"].ToString();
                        updateProj["ddsm_phase"] = ms["ddsm_projectphasename"].ToString();

                        if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0 && ForecastCompleteLastRecalcIndex == Convert.ToInt32(ms["ddsm_index"]))
                            ForecastComplete = ms.GetAttributeValue<DateTime>("ddsm_targetend");

                        if (projBP.Id != null)
                        {
                            if ((projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())) != null))
                            {
                                stageid = projBP.Stages.Find(x => x.Name.Contains(ms["ddsm_name"].ToString())).Id;
                            }

                        }

                        if (projPhase.ToLower() != (ms["ddsm_projectphasename"].ToString()).ToLower())
                        {
                            phaseNumber += 1;
                            phaseChenged = true;
                            updateProj["ddsm_phasenumber"] = phaseNumber;
                        }
                    }
                    else
                    {
                        updateMs = new Entity("ddsm_milestone", ms.GetAttributeValue<Guid>("ddsm_milestoneid"));
                        updateMs["ddsm_targetstart"] = msPrevEndtDay;
                        msPrevEndtDay = msPrevEndtDay.AddDays(Convert.ToDouble(ms["ddsm_duration"]));
                        updateMs["ddsm_targetend"] = msPrevEndtDay;

                        if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0 && ForecastCompleteLastRecalcIndex == Convert.ToInt32(ms["ddsm_index"]))
                            ForecastComplete = ms.GetAttributeValue<DateTime>("ddsm_targetend");

                        var updateRequest = new UpdateRequest();
                        updateRequest.Target = updateMs;
                        msRequest.Requests.Add(updateRequest);
                    }
                }

                if (Convert.ToBoolean(response.Entity["ddsm_completeonlastmilestone"]) && Convert.ToInt32(ms["ddsm_index"]) == Convert.ToInt32(response.Entity["ddsm_lastmilestoneindex"]) && activeNewIdx == Convert.ToInt32(response.Entity["ddsm_lastmilestoneindex"]))
                {
                    updateProj["ddsm_responsible"] = null;

                    updateProj["ddsm_pendingmilestone"] = null;
                    updateProj["ddsm_pendingmilestoneindex"] = null;
                    updateProj["ddsm_pendingactualstart"] = null;
                    updateProj["ddsm_pendingtargetend"] = null;

                    updateProj["ddsm_completedmilestone"] = null;
                    updateProj["ddsm_completedstatus"] = null;
                    updateProj["ddsm_completephase"] = null;

                    updateProj["ddsm_projectstatus"] = ms["ddsm_projectstatus"].ToString();
                    updateProj["ddsm_phase"] = ms["ddsm_projectphasename"].ToString();
                    updateProj["ddsm_enddate"] = msPrevEndtDay;

                    break;
                }

            }

            //Clear field "Completed Milestone Date"
            updateProj["ddsm_milestoneactualend"] = null;

            if (ForecastCompleteLastRecalcIndex != -1 && ForecastCompleteLastRecalcIndex != 0)
            {
                updateProj["ddsm_estimatedprojectcomplete"] = ForecastComplete;
                updateProj["ddsm_projectcompletiondateupdatedestimate"] = ForecastComplete;
            }
            else
            {
                updateProj["ddsm_estimatedprojectcomplete"] = msPrevEndtDay;
                updateProj["ddsm_projectcompletiondateupdatedestimate"] = msPrevEndtDay;
            }
            updateProj["ddsm_targetprojectcompletion"] = msPrevEndtDay;

            //Update Business Process
            if (projBP.Id != null)
            {
                if (!string.IsNullOrEmpty(stageid))
                {
                    traversedpath = traversedpath + "," + stageid;
                    updateProj["stageid"] = new Guid(stageid);
                    if (!string.IsNullOrEmpty(traversedpath))
                    {
                        updateProj["traversedpath"] = traversedpath;
                    }
                }
            }


            //Update Ms records
            var msResponse = (ExecuteMultipleResponse)objCommon.OrgService.Execute(msRequest);
            foreach (ExecuteMultipleResponseItem responseItem in msResponse.Responses)
            {
                // A valid response.
                if (responseItem.Response != null)
                {
                    if (((OrganizationResponse)responseItem.Response).Results.ContainsKey("id"))
                    {
                    }
                }

                // An error has occurred.
                else if (responseItem.Fault != null)
                {
                    objCommon.TracingService.Trace("Error: " + msRequest.Requests[responseItem.RequestIndex].RequestName + " Index: " + (responseItem.RequestIndex + 1) + " Message: " + responseItem.Fault.Message);
                }
            }

            //Update project records
            if (updateProj.Attributes.Count > 1)
                objCommon.OrgService.Update(updateProj);


            //Calling Action clone project measures
            if (phaseChenged)
            {
                IsChangedProjectPhase(objCommon.OrgServiceUser, new EntityReference { Id = projectId, LogicalName = "ddsm_project" }, updateProj["ddsm_phase"].ToString(), updateProj["ddsm_projectstatus"].ToString(), actualEnd, phaseNumber);
            }


        }

        private BusinessProcess GetBusinessProcess(IOrganizationService orgService, Guid id)
        {
            BusinessProcess projBP = new BusinessProcess();
            projBP.Id = null;
            //Get Project Template ID
            try
            {
                QueryExpression processQuery = new QueryExpression("workflow");
                processQuery.ColumnSet = new ColumnSet("name", "workflowid");
                processQuery.ColumnSet = new ColumnSet(true);
                processQuery.Criteria.AddCondition("workflowid", ConditionOperator.Equal, id);
                processQuery.Criteria.AddCondition("primaryentity", ConditionOperator.Equal, "ddsm_project");
                EntityCollection processRetrieve = orgService.RetrieveMultiple(processQuery);
                if (processRetrieve != null && processRetrieve.Entities.Count == 1)
                {
                    projBP.Id = processRetrieve.Entities[0].Attributes["workflowid"].ToString();
                    projBP.Name = processRetrieve.Entities[0].Attributes["name"].ToString();
                    projBP.Stages = new List<BusinessProcessStage>();
                    QueryExpression stageQuery = new QueryExpression("processstage");
                    stageQuery.ColumnSet = new ColumnSet("stagename", "processstageid");
                    stageQuery.Criteria.AddCondition("processid", ConditionOperator.Equal, new Guid(projBP.Id));
                    EntityCollection stageRetrieve = orgService.RetrieveMultiple(stageQuery);
                    if (stageRetrieve != null && stageRetrieve.Entities.Count > 0)
                    {
                        foreach (var stage in stageRetrieve.Entities)
                        {
                            BusinessProcessStage stageObj = new BusinessProcessStage();
                            stageObj.Id = stage.Attributes["processstageid"].ToString();
                            stageObj.Name = stage.Attributes["stagename"].ToString();
                            projBP.Stages.Add(stageObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                projBP = new BusinessProcess();
                projBP.Id = null;
            }
            return projBP;

        }

        private void IsChangedProjectPhase(IOrganizationService _orgService, EntityReference eRef, string projPhase, string projStatus, DateTime initialPhaseDate, int phaseNumber)
        {

            OrganizationRequest req = new OrganizationRequest("ddsm_CloneMeasProjectNextStage");
            req["Project"] = eRef;
            req["ActualEnd"] = initialPhaseDate;
            req["ProjectPhaseName"] = projPhase;
            req["ProjectStatus"] = projStatus;
            req["ProjectPhaseNumber"] = phaseNumber;
            _orgService.Execute(req);

            //objCommon.CloneMeasuresProject(objCommon, eRef.Id, projPhase, phaseNumber, projStatus, initialPhaseDate);

        }

    }
}
