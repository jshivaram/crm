﻿using System.Activities;
using DdsmTools.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

public class AsyncOperationDTO
{
    public AsyncOperationDTO(CodeActivityContext executionContext, IWorkflowContext context,
        IOrganizationService service,
        ITracingService tracer, EntityReference targetEntityRef, RelatedEntity relEntitiesList,
        bool isCheckRelatedEntities, bool isCheckDMNJobs)
    {
        Context = context;
        Service = service;
        Tracer = tracer;
        ExecutionContext = executionContext;
        TargetEntityReference = targetEntityRef;
        RelatedEntities = relEntitiesList;
        IsCheckRelatedEntities = isCheckRelatedEntities;
        IsCheckDMNJobs = isCheckDMNJobs;
    }

    //Execution Context
    public IWorkflowContext Context { get; private set; }

    //Oganization Service
    public IOrganizationService Service { get; private set; }

    //Target Entity
    public EntityReference TargetEntityReference { get; private set; }

    //Tracing Service
    public ITracingService Tracer { get; private set; }

    public CodeActivityContext ExecutionContext { get; private set; }

    public bool IsCheckRelatedEntities { get; private set; }

    public bool IsCheckDMNJobs { get; set; }

    public RelatedEntity RelatedEntities { get; private set; }
}