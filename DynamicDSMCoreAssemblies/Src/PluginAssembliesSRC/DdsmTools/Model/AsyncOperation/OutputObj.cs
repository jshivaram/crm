﻿using System.Collections.Generic;

public class OutputObj
{
    public bool Result { get; set; }
    public List<RsPerEntity> Message { get; set; }
}