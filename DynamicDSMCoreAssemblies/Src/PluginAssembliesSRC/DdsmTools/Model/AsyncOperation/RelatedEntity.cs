﻿using System.Collections.Generic;

public class RelatedEntity
{
    public List<Entities> EntityNames { get; set; }

    public class Entities
    {
        public string EntityName { get; set; }
    }
}