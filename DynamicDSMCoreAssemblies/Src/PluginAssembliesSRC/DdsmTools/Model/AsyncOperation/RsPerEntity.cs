﻿public class RsPerEntity
{
    public RsPerEntity(string entityLogicalName, string entityName, bool isExistAsyncOperation)
    {
        EntityName = entityName;
        EntityLogicalName = entityLogicalName;
        IsExistAsyncOperation = isExistAsyncOperation;
    }

    public string EntityLogicalName { get; set; }
    public string EntityName { get; set; }
    public bool IsExistAsyncOperation { get; set; }
}