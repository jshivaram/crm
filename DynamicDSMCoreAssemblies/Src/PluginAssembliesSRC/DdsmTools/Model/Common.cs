using System;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;

namespace DdsmTools.Model
{
    public class Common
    {
        public ITracingService TracingService { get; private set; }
        public IWorkflowContext Context { get; set; }
        public IOrganizationServiceFactory ServiceFactory { get; private set; }
        public IOrganizationService OrgService { get; private set; }
        private IOrganizationService SystemService;
        public CodeActivityContext ExecutionContext { get; private set; }

        public Common(CodeActivityContext executionContext)
        {
            ExecutionContext = executionContext;
            TracingService = executionContext.GetExtension<ITracingService>();
            Context = executionContext.GetExtension<IWorkflowContext>();
            ServiceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            OrgService = ServiceFactory.CreateOrganizationService(Context.UserId);
            // reinit context to System User
          //  OrgService = ServiceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", OrgService));
        }

        public Guid GetSystemUserId(string name, IOrganizationService service)
        {
            QueryByAttribute queryUsers = new QueryByAttribute
            {
                EntityName = "systemuser",
                ColumnSet = new ColumnSet("systemuserid")
            };

            queryUsers.AddAttributeValue("fullname", name);
            EntityCollection retrievedUsers = service.RetrieveMultiple(queryUsers);
            Guid systemUserId = ((Entity)retrievedUsers.Entities[0]).Id;

            return systemUserId;
        }

        /// <summary>
        /// Return OrgService from System User
        /// </summary>
        /// <returns></returns>
        public IOrganizationService GetSystemOrgService()
        {
            return SystemService ??
                   (SystemService = ServiceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", OrgService)));
        }




    }
}