﻿namespace DdsmTools.Model
{
    public class Enums
    {


        //public enum MilestoneStatus
        //{
        //    NotStarted = 962080000,
        //    Active = 962080001,
        //    Completed = 962080002,
        //    Skipped = 962080003
        //}

        public enum ResultMsgCode
        {
            Ok = 1,
            ApprovalLevelIsNotHighEnough = -200002,//You can not move project group forward, because your approval level is not high enough.
            YouDoNotBelongtotheProgram = -200001,//You can not move project group forward, because you do not belong to the program/group
            ProjectGroupDoesNotBelongtoAnyProgram = -200000,//You can not move project group forward, because the project group does not belong to any program/group. Notify system administrator!
            NoUserInTeam = -300000,//No one user in your team which have approval amount for next stage. Please add user with needed approval amount for next stage.

        }

        public enum ProjectGroupStatus
        {
            WaitingForProjects= 962080000,
            Approvals = 962080001,
            Payment = 962080002,
            Completed = 962080003
        }


        public enum StepRule
        {
            Step = 962080000,
            MultiStep = 962080001
        }

        /// <summary>
        /// for User than UAA > lastMax
        /// </summary>
        public enum SupervisedApprovals
        {//
            Allowed = 962080000,
            Denied = 962080001
        }

        public enum ErrorCode
        {
            Ok = 962080000,
            YouAreNotInTheIntervalOfStep = 962080001,
            UaaLessAa = 962080002,
            UserNotInTeam = 962080003,
            PgFisNotCompletedForLastStage= 962080004,
            YouAreNotInTheIntervalOfStep2 = 962080005,
        }





    }
}
