using System;
using Microsoft.Xrm.Sdk;

namespace DdsmTools.Model
{
    public class Milestone
    {
        public enum MilestoneStatus
        {
            NotStarted = 962080000,
            Active = 962080001,
            Completed = 962080002,
            Skipped = 962080003
        }


        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public int Index { get; set; }
        public DateTime TargetStart { get; set; }
        public DateTime TargetEnd { get; set; }
        public DateTime ActualStart { get; set; }
        public DateTime ActualEnd { get; set; }
        public MilestoneStatus Status { get; set; }

        public Milestone(Entity milestone)
        {
            if (milestone != null)
            {
                Id = milestone.Id;

                object mData = null;
                if (milestone.Attributes.TryGetValue("ddsm_name", out mData))
                    Name = (string)mData;

                if (milestone.Attributes.TryGetValue("ddsm_approvalthresholdcust", out mData))
                    Amount = (mData as Money).Value;

                if (milestone.Attributes.TryGetValue("ddsm_status", out mData))
                    Status = (MilestoneStatus)(mData as OptionSetValue).Value;

                if (milestone.Attributes.TryGetValue("ddsm_index", out mData))
                    Index = (int)mData;

                if (milestone.Attributes.TryGetValue("ddsm_targetstart", out mData))
                    TargetStart = (DateTime)mData;

                if (milestone.Attributes.TryGetValue("ddsm_targetend", out mData))
                    TargetEnd = (DateTime)mData;

                if (milestone.Attributes.TryGetValue("ddsm_actualstart", out mData))
                    ActualStart = (DateTime)mData;

                if (milestone.Attributes.TryGetValue("ddsm_actualend", out mData))
                    ActualEnd = (DateTime)mData;
            }
        }

        public Entity GetEntity()
        {
            var result = new Entity("ddsm_projectgroupmilestone", Id);

            result["ddsm_status"] = new OptionSetValue((int)Status);

            if (!DateTime.MinValue.Equals(ActualStart))
                result["ddsm_actualstart"] = ActualStart;

            if (!DateTime.MinValue.Equals(ActualEnd))
                result["ddsm_actualend"] = ActualEnd;

            if (!DateTime.MinValue.Equals(TargetStart))
                result["ddsm_targetstart"] = TargetStart;

            if (!DateTime.MinValue.Equals(TargetEnd))
                result["ddsm_targetend"] = TargetEnd;


            result["ddsm_index"] = Index;
            result["ddsm_approvalthresholdcust"] = new Money(Amount);
            return result;
        }
    }
}