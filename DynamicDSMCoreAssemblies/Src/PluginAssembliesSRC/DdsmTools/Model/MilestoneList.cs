using System;
using System.Collections.Generic;
using System.Linq;

namespace DdsmTools.Model
{
    public class MilestoneList : List<Milestone>
    {
        /// <summary>
        /// Current Active Stage of PG
        /// </summary>
        public Milestone CurrentStage { get { return this.GetActive(); } }

        /// <summary>
        /// Previous Stage of PG
        /// </summary>
        public Milestone PrevStage { get { return this.ElementAtOrDefault(this.IndexOf(this.CurrentStage) - 1); } }

        public void Complete(Milestone ms, ref List<Milestone> result)
        {
            ms.Status = Milestone.MilestoneStatus.Completed;
            ms.TargetEnd = DateTime.UtcNow;

            result?.Add(ms);
        }

        public void Activate(Milestone ms, ref List<Milestone> result)
        {
            Complete(CurrentStage, ref result);
            ms.Status = Milestone.MilestoneStatus.Active;
            ms.ActualEnd = DateTime.UtcNow;

            result?.Add(ms);
        }

        public void Skip(Milestone ms, ref List<Milestone> result)
        {
            if (ms.Index == this.Count - 1)
            {
                Activate(ms, ref result);
            }
            else
            {
                ms.Status = Milestone.MilestoneStatus.Skipped;
                ms.ActualEnd = CurrentStage.ActualStart;

                ms.TargetStart = CurrentStage.ActualStart;
                ms.ActualStart = CurrentStage.ActualStart;

                result?.Add(ms);
            }

        }
    }
}