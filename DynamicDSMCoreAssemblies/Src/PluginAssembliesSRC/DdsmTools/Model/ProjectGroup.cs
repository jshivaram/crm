﻿using DdsmTools.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider;
using static DdsmTools.Model.Enums;
using static DdsmTools.Model.Milestone;

namespace DdsmTools.Model
{
    public class ProjectGroup
    {
        #region Properties

        public string Name { get; private set; }

        /// <summary>
        /// Project Group Approval
        /// </summary>
        public decimal AppprovalAmount { get; private set; }

        public StepRule StepRule { get; private set; }

        public ProjectGroupStatus Status { get; set; }

        public MilestoneList Milestones { get; private set; }

        public EntityReference Owner { get; set; }

        public Guid UserId { get; set; }

        public bool UserOwnedTeam { get; private set; }

        public EntityReference Team { get; private set; }

        public Common ObjCommon { get; private set; }

        public BusinessProcess BusinessProcesses { get; private set; }

        public EntityReference Tempalte { get; set; }

        public SupervisedApprovals SupervisedApprovals { get; private set; }

        public ErrorCode ProcessingError { get; set; }

        public string TraversedPath { get; set; }

        public UserApproval UserApproval { get; private set; }

        public bool Approvals { get; private set; }
        #endregion

        #region fields
        EntityReference _target;

        Entity _projectGroup;

        public string LogicalName { get { return _target.LogicalName; } }

        internal Guid Id;
        #endregion

        #region Constructor
        public ProjectGroup(CodeActivityContext executionContext)
        {
            if (ObjCommon == null)
                ObjCommon = new Common(executionContext);

            if (_target == null)
                _target = (EntityReference)ObjCommon.Context.InputParameters["Target"];

            InitPg();
        }

        public ProjectGroup(EntityReference target, Common objCommon)
        {
            _target = target;
            ObjCommon = objCommon;

            InitPg();
        }
        #endregion Constructor

        public ExecuteMultipleRequest NextStage(UserApproval userAppr)
        {
            UserApproval = userAppr;
            var emRequest = new ExecuteMultipleRequest
            {
                Requests = new OrganizationRequestCollection(),
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                }
            };

            var stages = GetNextStages();
            if (stages != null)
            {
                // add PG to update Request
                var updateRequest = new UpdateRequest {Target = GetEntity()};
                emRequest.Requests.Add(updateRequest);

                foreach (var item in stages)
                {
                    updateRequest = new UpdateRequest {Target = item.GetEntity()};
                    emRequest.Requests.Add(updateRequest);
                }
                ProcessingError = ErrorCode.Ok;
            }
            else
            {
                Ddsm.SearchAndSendEmailToUser(ObjCommon, Team.Id, AppprovalAmount, Milestones.PrevStage.Amount, Milestones.CurrentStage.Amount, Milestones.CurrentStage.Name, Id, Milestones);
            }
            return emRequest;
        }

        #region Internal methods


        /// <summary>
        /// Return Stages of MS which shoul be processed
        /// </summary>
        /// <returns></returns>
        private List<Milestone> GetNextStages()
        {
            var result = new List<Milestone>();

            if (!Approvals)
            {
                return SingleStep();
            }


            if (IsLastStage(Milestones.CurrentStage))
            {
                if (!IsCompletedAnyPgFinancials())
                {
                    ProcessingError = ErrorCode.PgFisNotCompletedForLastStage;
                    throw new Exception(Helper.ErrorsList.Single(x => x.Key.Equals(ProcessingError)).Value);
                }
                
                var nextStage = Milestones[Milestones.CurrentStage.Index];
                Milestones.Activate(nextStage, ref result);
                //result.Add(Milestones.PrevStage);
                //result.Add(nextStage);

                Status = ProjectGroupStatus.Completed;
                return result;
            }

            //1)
            if (UserApproval.Amount <= Milestones.PrevStage.Amount)
            {
                ProcessingError = ErrorCode.YouAreNotInTheIntervalOfStep;
                return null;                //throw new Exception("1.You can't move next stage!");
            }

            //2)
            if ((UserApproval.Amount > Milestones.PrevStage.Amount) && (UserApproval.Amount <= Milestones.CurrentStage.Amount))
            {
                //2.1
                if (UserApproval.Amount < AppprovalAmount)
                {
                    return SingleStep();
                }
                //2.2
                else if (UserApproval.Amount >= AppprovalAmount)
                {
                    return MultiStep();
                }
            }
            //3
            if (UserApproval.Amount > Milestones.CurrentStage.Amount && SupervisedApprovals == SupervisedApprovals.Allowed)
            {
                ////3.1
                //if (UserApproval.Amount < AppprovalAmount)
                //{
                //    ProcessingError = ErrorCode.UaaLessAa;
                //    return null;
                //}
                //3.2
            //    else if (UserApproval.Amount >= AppprovalAmount && SupervisedApprovals == SupervisedApprovals.Allowed) //maybe
                    
                {
                    return MultiStep();
               }
              
            }
            else
            {
                ProcessingError = ErrorCode.YouAreNotInTheIntervalOfStep2;
                return null;                //throw new Exception("1.You can't move next stage!");
            }
            return result;
        }

        private void GetPgData()
        {
            _projectGroup = ObjCommon.GetOrgService(systemCall: true).Retrieve("ddsm_projectgroup", _target.Id, new ColumnSet("ddsm_projectgroupid", "ddsm_name", "ddsm_steprules","ddsm_approvals", "ddsm_supervisedapprovals", "ddsm_projectgroupstatus", "ddsm_approvalamount", "ownerid", "ddsm_projectgrouptemplates", "traversedpath", "processid", "stageid"));

            object sss;
            if (_projectGroup.Attributes.TryGetValue("ddsm_approvalamount", out sss))
                AppprovalAmount = ((Money)sss).Value;
            ObjCommon.TracingService.Trace("AA: " + AppprovalAmount);

            //if (_projectGroup.Attributes.TryGetValue("ddsm_approvalsammountsrules", out sss))
            //    ApprovalsAmmountsRules = (ApprovalsAmmountsRules)((OptionSetValue) sss).Value;

            //if (_projectGroup.Attributes.TryGetValue("ddsm_approvalsflowrules", out sss))
            //    ApprovalsFlowRules = (ApprovalsFlowRules)((OptionSetValue) sss).Value;

            if (_projectGroup.Attributes.TryGetValue("ownerid", out sss))
                Owner = sss as EntityReference;

            if (_projectGroup.Attributes.TryGetValue("ddsm_projectgrouptemplates", out sss))
                Tempalte = sss as EntityReference;

            if (_projectGroup.Attributes.TryGetValue("ddsm_steprules", out sss))
                StepRule = (StepRule)((OptionSetValue) sss).Value;

            if (_projectGroup.Attributes.TryGetValue("ddsm_supervisedapprovals", out sss))
                SupervisedApprovals = (SupervisedApprovals)((OptionSetValue)sss).Value;

            if (_projectGroup.Attributes.TryGetValue("ddsm_name", out sss))
                Name = sss.ToString();

            if (_projectGroup.Attributes.TryGetValue("traversedpath", out sss))
                TraversedPath = sss.ToString();

            if (_projectGroup.Attributes.TryGetValue("ddsm_projectgroupstatus", out sss))
                Status = (ProjectGroupStatus)((OptionSetValue)sss).Value;


            if (_projectGroup.Attributes.TryGetValue("ddsm_approvals", out sss))
                Approvals = (bool) sss;

            

            Team = Ddsm.GetTeam(Owner, Tempalte?.Name, ObjCommon.GetOrgService(systemCall: true));

            Id = _projectGroup.Id;

            UserId = ObjCommon.Context.InitiatingUserId;
        }

        public Entity GetEntity()
        {
            var result = new Entity("ddsm_projectgroup", Id);

            if ((int)Status != 0)
            {
                result["ddsm_projectgroupstatus"] = new OptionSetValue((int)Status);
            }
            result["ddsm_approvalamount"] = new Money(AppprovalAmount);
            var newStageid = BusinessProcesses.Stages.FirstOrDefault(x => x.Name.Equals(Milestones.CurrentStage.Name))?.Id;
            result["stageid"] = Guid.Parse(newStageid);
            var traversedpath = TraversedPath + "," + newStageid;
            result["traversedpath"] = traversedpath;


            return result;
        }

        private MilestoneList GetPgMilestones()
        {
            MilestoneList result = new MilestoneList();

            var query = new QueryExpression()
            {
                EntityName = "ddsm_projectgroupmilestone",
                ColumnSet = new ColumnSet("ddsm_projectgroupmilestoneid", "ddsm_name", "ddsm_index", "ddsm_status", "ddsm_targetstart", "ddsm_targetend", "ddsm_actualstart", "ddsm_actualend", "ddsm_duration", "ddsm_approvalthresholdcust"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("statecode", ConditionOperator.Equal, "Active"),
                        new ConditionExpression("statuscode", ConditionOperator.Equal, 1),
                        new ConditionExpression("ddsm_projectgrouptoprojectgroupapproid", ConditionOperator.Equal, _target.Id),
                    }
                }
            };
            query.AddOrder("ddsm_index", OrderType.Ascending);

            var msRetrieve = ObjCommon.GetOrgService(systemCall: true).RetrieveMultiple(query);

            foreach (var milestone in msRetrieve.Entities)
            {
                result.Add(new Milestone(milestone));
            }

            return result;
        }

        private void InitPg()
        {
            //default values
            //ApprovalsAmmountsRules = ApprovalsAmmountsRules.SearchRange;
            //ApprovalsFlowRules = ApprovalsFlowRules.SkipSteps;

            SupervisedApprovals = SupervisedApprovals.Denied;
            StepRule = StepRule.Step;


            GetPgData();

            GetBp();

            Milestones = GetPgMilestones();
            //   PrevStage = Milestones.FirstOrDefault(x => x.Index == CurrentStage.Index - 1);
        }

        private void GetBp()
        {
            //Get BP PG
            var traversedpath = _projectGroup.GetAttributeValue<string>("traversedpath");
            var processid = _projectGroup.GetAttributeValue<Guid>("processid");
            var stageid = _projectGroup.GetAttributeValue<Guid>("stageid");
            BusinessProcesses = new BusinessProcess();
            BusinessProcesses = Ddsm.GetBusinessProcess(ObjCommon, processid, LogicalName);
        }

        /// <summary>
        /// Return true if milestone is last 
        /// </summary>
        /// <returns></returns>
        private bool IsLastStage(Milestone ms)
        {
            return ms.Index == Milestones.Count - 1 ? true : false;
        }

        private bool IsUserCanSkip()
        {
            return this.StepRule == StepRule.MultiStep;
        }

        private List<Milestone> MultiStep()
        {
            var result = new List<Milestone>();
            int index = Milestones.CurrentStage.Index;
            if (IsUserCanSkip() && !IsLastStage(Milestones[index]))
            {
                while (index < Milestones.Count && !IsLastStage(Milestones[index]))
                {
                    if (UserApproval.Amount > Milestones.PrevStage.Amount &&
                        UserApproval.Amount <= Milestones.CurrentStage.Amount)
                    {
                        Milestones.Activate(Milestones[index], ref result);
                        Ddsm.SetApprovalAuditRecord(ObjCommon.GetOrgService(systemCall: true), Milestones.PrevStage.Name, new EntityReference(LogicalName, Id), new EntityReference("systemuser", UserId), AppprovalAmount);
                        ObjCommon.TracingService.Trace(Milestones.PrevStage.Name + " Status: " + Enum.GetName(typeof(MilestoneStatus), Milestones[index].Status));
                        // ObjCommon.TracingService.Trace(Milestones.PrevStage.Name + " Status: " + Enum.GetName(typeof(MilestoneStatus), Milestones[index].Status));
                    }

                    if (AppprovalAmount < Milestones.CurrentStage.Amount &&
                        Milestones[index].Status != MilestoneStatus.Completed &&
                        Milestones[index].Status != MilestoneStatus.Active)
                    {
                        Milestones.Skip(Milestones[index], ref result);
                    }

                    Ddsm.SetApprovalAuditRecord(ObjCommon.GetOrgService(systemCall: true), Milestones[index].Name, new EntityReference(LogicalName, Id), new EntityReference("systemuser", UserId), AppprovalAmount);
                   //   result.Add(Milestones[index]);
                    ObjCommon.TracingService.Trace(Milestones[index].Name + " Status: " + Enum.GetName(typeof(MilestoneStatus), Milestones[index].Status));
                    index++;
                }
                Milestones.Activate(Milestones[index], ref result);
                ObjCommon.TracingService.Trace(Milestones[index].Name + " Status: " + Enum.GetName(typeof(MilestoneStatus), Milestones[index].Status));
            }
            else
            {
                return SingleStep();
            }
            return result;
        }

        private List<Milestone> SingleStep()
        {
            var result = new List<Milestone>();
            var nextStage = Milestones[Milestones.CurrentStage.Index];

            // if (!isLastStage(nextStage))
            {
                Milestones.Activate(nextStage, ref result);
                Ddsm.SetApprovalAuditRecord(ObjCommon.GetOrgService(systemCall: true), Milestones.PrevStage.Name, new EntityReference("ddsm_projectgroup", new Guid(_projectGroup["ddsm_projectgroupid"].ToString())), new EntityReference("systemuser", UserId), AppprovalAmount);
                ObjCommon.TracingService.Trace(nextStage.Name + " Status: " + Enum.GetName(typeof(MilestoneStatus), nextStage.Status));
                //result.Add(Milestones.PrevStage);
                //result.Add(nextStage);
            }
            return result;
        }


        // verify fields pendingsatge in all child pgf entity  
        private bool IsCompletedAnyPgFinancials()
        {
            // Construct query            
            bool isCompleted = true; 
            ConditionExpression condition = new ConditionExpression();
            condition.AttributeName = "ddsm_projectgrouptoprojectgroupfinanid";
            condition.Operator = ConditionOperator.Equal;
            condition.Values.Add(Id);

            //Create a column set.
            ColumnSet columns = new ColumnSet("ddsm_pendingstage");

            // Create query expression.
            QueryExpression query = new QueryExpression();
            query.ColumnSet = columns;
            query.EntityName = "ddsm_projectgroupfinancials";
            query.Criteria.AddCondition(condition);

            EntityCollection listPgf = ObjCommon.GetOrgService(systemCall: true).RetrieveMultiple(query);

            if (listPgf != null)
            {
                foreach (Entity entityPgf in listPgf.Entities)
                {
                    object tempVal = null;
                    if (entityPgf.Attributes.TryGetValue("ddsm_pendingstage", out tempVal))
                    {
                        isCompleted = !((string)tempVal).Equals("Completed") ? false : true;
                        break;
                    }
                }
            }

            return isCompleted;
        }

        #endregion Internal methods
    }
}
