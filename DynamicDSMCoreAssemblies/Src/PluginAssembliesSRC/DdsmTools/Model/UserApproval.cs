﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using DDSM.CommonProvider;

namespace DdsmTools.Model
{
    public class UserApproval
    {
        Common _objCommon;

        public UserApproval(ProjectGroup pg)
        {
            _objCommon = pg.ObjCommon;
            Id = _objCommon.Context.InitiatingUserId;
            if(pg.Approvals)
               Init();
            else
            {
                Amount = 0;
                Name = "";
                User = new EntityReference();
                Id= Guid.Empty;
            }
        }

        private void Init()
        {
            try
            {
                var query = new QueryExpression
                {
                    EntityName = "ddsm_userapproval",
                    ColumnSet = new ColumnSet("ddsm_user", "ddsm_approvalthresholdcust"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                       {
                            new ConditionExpression("ddsm_user", ConditionOperator.Equal,Id ),
                            new ConditionExpression("statuscode",ConditionOperator.Equal, 1)    // only active record userapproval                        
                       }
                    }
                };

                // var uaa = _objCommon.OrgService.RetrieveMultiple(query);
                var uaa = _objCommon.GetOrgService(systemCall:true).RetrieveMultiple(query);
                if (uaa.Entities.Count > 0)
                {
                    object sss = null;
                    if (uaa[0].Attributes.TryGetValue("ddsm_approvalthresholdcust", out sss))
                    {
                        Amount = (sss as Money).Value;
                    }
                    _objCommon.TracingService.Trace("UAA: " + Amount);

                    if (uaa[0].Attributes.TryGetValue("ddsm_user", out sss))
                        User = (sss as EntityReference);

                    if (uaa[0].Attributes.TryGetValue("ddsm_name", out sss))
                        Name = sss.ToString();
                }
                else {
                    throw new Exception("You are not allowed to Approve. Your Approval Level is not configured. Please contact administrator.");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public decimal Amount { get; set; }

        public Guid Id { get; set; }

        public EntityReference User { get; set; }

        public string Name { get; set; }

    }
}
