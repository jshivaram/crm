﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DdsmTools.Model
{
    /// <summary>
    /// Object for Recalulate SM from View
    /// </summary>
    public class UserInputObj2
    {
        public List<Guid> SmartMeasures { get; set; }
        public Dictionary<string, string> DataFields { get; set; }
    }
}
