﻿using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using static DdsmTools.Model.Enums;
using System.Diagnostics.Contracts;

namespace DdsmTools.Utils
{
    public static class Helper
    {

        public static EntityReference GetTargetData(Ddsm objDdsm)
        {
            Contract.Ensures(Contract.Result<EntityReference>() != null);
            EntityReference target = null;

            if (objDdsm.Context.InputParameters["Target"] is EntityReference)
            {
                target = new EntityReference(((EntityReference)objDdsm.Context.InputParameters["Target"]).LogicalName, ((EntityReference)objDdsm.Context.InputParameters["Target"]).Id);
            }
            else if (objDdsm.Context.InputParameters["Target"] is Entity)
            {
                target = new EntityReference(((Entity)objDdsm.Context.InputParameters["Target"]).LogicalName, ((Entity)objDdsm.Context.InputParameters["Target"]).Id);
            }

            return target;
        }


        public static Dictionary<ErrorCode, string> ErrorsList = new Dictionary<ErrorCode, string>
        {
            {ErrorCode.Ok,"Ok"  },                                                                                                                                      //prev stage       current stage
            {ErrorCode.YouAreNotInTheIntervalOfStep,"You are not permitted to approve this step, as your approval threshold is below the approval amount."},
            {ErrorCode.YouAreNotInTheIntervalOfStep2,"You are not permitted to approve this step, as your approval threshold is below the approval amount. Supervised Approvals are forbidden for this Program."},
            {ErrorCode.UaaLessAa,"You approval amount less than Project Group Amount."  },
            { ErrorCode.UserNotInTeam, "You are not allowed to approve this record. Your are not in team {0}."},
            { ErrorCode.PgFisNotCompletedForLastStage, "You can't Complete this Project Group record because on of Project Group Financial records isn't completed yet."},

        };




    }
}
