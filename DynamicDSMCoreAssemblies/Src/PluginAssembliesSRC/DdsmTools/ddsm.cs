﻿using DdsmTools.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using DDSM.CommonProvider;
using Microsoft.Xrm.Sdk.Metadata;
using System.Text.RegularExpressions;
using System.Linq;

namespace DdsmTools
{
    public class BusinessProcessStage
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }

    public class NoUserinTeamException : Exception
    {
        public NoUserinTeamException(string message) : base(message)
        {

        }
    }

    public class BusinessProcess
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public List<BusinessProcessStage> Stages { get; set; }
    }

    public class AutoNumberingManager
    {
        private static string _logicalName;
        private static string _autoNumberingEntityName = "ddsm_autonumbering";
        private static string _autoNumberingSearcAttrName = "ddsm_name";
        private static Entity _autoNumberingEntity = null;
        private static string newNumber = string.Empty;
        private static IOrganizationService _orgService;

        public static string GenAutoNumber(string logicalName, IOrganizationService orgService)
        {
            _logicalName = logicalName;
            _orgService = orgService;

            newNumber = GetNumber();
            UpdateCounter();

            return newNumber;
        }

        /// <summary>
        /// Prepare and return unique number of entity 
        /// </summary>
        /// <returns></returns>
        private static string GetNumber()
        {
            //Get auto numbering rules
            var autoNumberingMetaData = GetAutoNumberingRules();
            if (autoNumberingMetaData.Entities.Count == 0)
            {
                throw new InvalidPluginExecutionException("Auto Numbering Metadata does not exist.");
            }
            //Extract first record
            _autoNumberingEntity = autoNumberingMetaData.Entities[0];

            return GenerateNumber();
        }
        /// <summary>
        /// Generate unique number for entity by specific rules for each entity
        /// </summary>
        /// <param name="metaData"></param>
        /// <returns></returns>
        private static string GenerateNumber()
        {
            var number = "";
            try
            {
                //Get counter
                var counter = GetDataByField("ddsm_counter");
                //Get prefix
                var prefix = GetDataByField("ddsm_prefix");
                //Get length of number
                var length = Convert.ToInt32(GetDataByField("ddsm_length"));

                if (counter.Length < length)
                    number = counter.PadLeft(length, '0');
                else
                    number = counter;

                //Join prefix and counter
                number = prefix + number;

                //Replace specific character
                number = GetStringByPattern(number);
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.Message);
            }
            return number;
        }


        /// <summary>
        /// Get value by specific field
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private static string GetDataByField(string fieldName)
        {

            object value = null;
            try
            {
                if (_autoNumberingEntity.Attributes.TryGetValue(fieldName, out value))
                {
                    return value.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.Message);
            }
            return string.Empty;
        }

        /// <summary>
        /// Get rules for generate number
        /// </summary>
        /// <returns></returns>
        private static EntityCollection GetAutoNumberingRules()
        {
            // Construct query            
            var condition = new ConditionExpression
            {
                AttributeName = _autoNumberingSearcAttrName,
                Operator = ConditionOperator.Equal
            };
            condition.Values.Add(_logicalName);

            //Create a column set.
            var columns = new ColumnSet("ddsm_counter", "ddsm_length", "ddsm_prefix", "ddsm_increment");

            // Create query expression.
            var query = new QueryExpression
            {
                ColumnSet = columns,
                EntityName = _autoNumberingEntityName
            };
            query.Criteria.AddCondition(condition);

            var result = _orgService.RetrieveMultiple(query);
            return result;
        }

        /// <summary>
        /// Update Auto Numbering Entity to next value
        /// </summary>
        private static void UpdateCounter()
        {
            var counter = Convert.ToInt32(GetDataByField("ddsm_counter"));
            var increment = Convert.ToInt32(GetDataByField("ddsm_increment"));
            if (increment == int.MinValue)
            {
                increment = 1;
            }
            counter = counter + increment;
            _autoNumberingEntity["ddsm_counter"] = counter;
            _orgService.Update(_autoNumberingEntity);
        }

        /// <summary>
        /// Return string by Pattern
        /// </summary>
        /// <param name="value">input string</param>
        /// <param name="pattern"> default pattern is [^a-zA-Z0-9]</param>
        /// <returns></returns>
        private static string GetStringByPattern(string value, string pattern = "[^a-zA-Z0-9]")
        {
            var replacedString = Regex.Replace(value, pattern, "");
            return replacedString;

        }
    }

    public class Ddsm
    {
        #region Secure/Unsecure Configuration Setup
        private string _secureConfig = null;
        private string _unsecureConfig = null;

        public Ddsm(string unsecureConfig, string secureConfig)
        {
            _secureConfig = secureConfig;
            _unsecureConfig = unsecureConfig;
        }
        #endregion
        public ITracingService TracingService;
        public IWorkflowContext Context;
        public IOrganizationServiceFactory ServiceFactory;
        public IOrganizationService OrgService;
        public IOrganizationService OrgServiceUser;
        private readonly string _usersettingsLogicalName = "usersettings";

        //  privat IOrganizationService OrgService;

        public enum ApprovalsAmmountsRules
        {
            FindingTheNearest = 962080000,
            SearchRange = 962080001
        }
        public enum ApprovalsFlowRules
        {
            SkipSteps = 962080000,
            StepByStep = 962080001
        }

        public enum TaskEntity
        {
            Measure = 962080000,
            Project = 962080001,
            ProgramInterval = 962080002,
            ProgramOffering = 962080003,
            Portfolio = 962080004,
            Financial = 962080005,
            ProjectGroupFinancial = 962080006,
            Program = 962080007,
            DsmPlan = 962080008,
            Email = 962080009

        }

        public enum ApprovalAuditStatus
        {
            Approved = 962080000,
            Draft = 962080001
        }

        protected string GetParticipation(string attributeName)
        {
            string sReturn = "";
            switch (attributeName)
            {
                case "from":
                    sReturn = "1";
                    break;
                case "to":
                    sReturn = "2";
                    break;
                case "cc":
                    sReturn = "3";
                    break;
                case "bcc":
                    sReturn = "4";
                    break;

                case "organizer":
                    sReturn = "7";
                    break;
                case "requiredattendees":
                    sReturn = "5";
                    break;
                case "optionalattendees":
                    sReturn = "6";
                    break;
                case "customer":
                    sReturn = "11";
                    break;
                case "resources":
                    sReturn = "10";
                    break;
            }
            return sReturn;
        }

        protected List<string> GetEntityAttributesToClone(string entityName, IOrganizationService service, ref string primaryIdAttribute)
        {

            var atts = new List<string>();
            var req = new RetrieveEntityRequest()
            {
                EntityFilters = EntityFilters.Attributes,
                LogicalName = entityName
            };

            var res = (RetrieveEntityResponse)service.Execute(req);
            primaryIdAttribute = res.EntityMetadata.PrimaryIdAttribute;

            foreach (AttributeMetadata attMetadata in res.EntityMetadata.Attributes)
            {
                if ((attMetadata.IsValidForCreate.Value || attMetadata.IsValidForUpdate.Value)
                    && !attMetadata.IsPrimaryId.Value)
                {
                    //tracingService.Trace("Tipo:{0}", attMetadata.AttributeTypeName.Value.ToLower());
                    if (attMetadata.AttributeTypeName.Value.ToLower() == "partylisttype")
                    {
                        atts.Add("partylist-" + attMetadata.LogicalName);
                        //atts.Add(attMetadata.LogicalName);
                    }
                    else
                    {
                        atts.Add(attMetadata.LogicalName);
                    }
                }
            }

            return (atts);
        }

        public Ddsm(CodeActivityContext executionContext)
        {
            TracingService = executionContext.GetExtension<ITracingService>();
            Context = executionContext.GetExtension<IWorkflowContext>();
            ServiceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            OrgServiceUser = ServiceFactory.CreateOrganizationService(Context.UserId);

            OrgService = ServiceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", OrgServiceUser));

        }

        public Guid GetSystemUserId(string name, IOrganizationService service)
        {
            QueryByAttribute queryUsers = new QueryByAttribute
            {
                EntityName = "systemuser",
                ColumnSet = new ColumnSet("systemuserid")
            };

            queryUsers.AddAttributeValue("fullname", name);
            EntityCollection retrievedUsers = service.RetrieveMultiple(queryUsers);
            Guid systemUserId = ((Entity)retrievedUsers.Entities[0]).Id;

            return systemUserId;
        }

        public string GetTimeZoneByCRMConnection(IOrganizationService _orgService, Guid userId)
        {
            string timeZoneCode = "92"; //UTC
            var query = new QueryExpression(_usersettingsLogicalName);
            query.ColumnSet = new ColumnSet("systemuserid", "timezonecode");
            query.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, userId);

            Entity userSettings = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();
            if (userSettings != null && userSettings.Attributes.ContainsKey("timezonecode"))
            {
                timeZoneCode = userSettings.Attributes["timezonecode"].ToString();
            }
            else {
                return TimeZone.CurrentTimeZone.StandardName; //return local
            }

            query = new QueryExpression("timezonedefinition");
            query.ColumnSet = new ColumnSet("timezonecode", "standardname");

            query.Criteria.AddCondition("timezonecode", ConditionOperator.Equal, timeZoneCode);
            Entity timeZoneDefinitions = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            return timeZoneDefinitions.Attributes["standardname"].ToString();
        }

        public dynamic ConverAttributToDateTimeUtc(Object inputDate, TimeZoneInfo timeZoneInfo)
        {
            try
            {
                if (inputDate.GetType().Name == "DateTime")
                {
                    var convertDate = Convert.ToDateTime(inputDate);
                    return TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(convertDate.ToShortDateString()), timeZoneInfo);
                }
                else if (inputDate.GetType().Name == "Double")
                {
                    var convertDate = DateTime.FromOADate(Convert.ToDouble(inputDate));
                    return TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(convertDate.ToShortDateString()), timeZoneInfo);
                }
                else if (inputDate.GetType().Name == "String")
                {
                    if (inputDate.ToString() == "")
                    {
                        return null;
                    }
                    else if (inputDate.ToString() == "today")
                    {
                        var convertDate = DateTime.Now;
                        return TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(convertDate.ToShortDateString()), timeZoneInfo);
                    }
                    else
                    {
                        var convertDate = Convert.ToDateTime(inputDate.ToString());
                        return TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(convertDate.ToShortDateString()), timeZoneInfo);
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }


        //Get Business Process
        public static BusinessProcess GetBusinessProcess(Common objCommon, Guid bpId, string primaryentity)
        {
            var projBp = new BusinessProcess();

            try
            {
                var processQuery = new QueryExpression("workflow")
                {
                    ColumnSet = new ColumnSet("workflowid", "name"),
                    Criteria = new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("workflowid", ConditionOperator.Equal, bpId),
                            new ConditionExpression("primaryentity", ConditionOperator.Equal, primaryentity),
                            new ConditionExpression("statecode", ConditionOperator.Equal, 1)
                        }
                    }
                };

                var processRetrieve = objCommon.GetOrgService(systemCall: true).RetrieveMultiple(processQuery);
                if (processRetrieve != null && processRetrieve.Entities.Count == 1)
                {
                    projBp.Id = processRetrieve.Entities[0].Attributes["workflowid"].ToString();
                    projBp.Name = processRetrieve.Entities[0].Attributes["name"].ToString();
                    projBp.Stages = new List<BusinessProcessStage>();

                    var stageQuery = new QueryExpression("processstage");
                    stageQuery.ColumnSet = new ColumnSet("stagename", "processstageid");
                    //stageQuery.ColumnSet = new ColumnSet(true);
                    stageQuery.Criteria.AddCondition("processid", ConditionOperator.Equal, new Guid(projBp.Id));
                    var stageRetrieve = objCommon.GetOrgService(systemCall: true).RetrieveMultiple(stageQuery);
                    if (stageRetrieve != null && stageRetrieve.Entities.Count > 0)
                    {
                        foreach (var stage in stageRetrieve.Entities)
                        {
                            var stageObj = new BusinessProcessStage();
                            stageObj.Id = stage.Attributes["processstageid"].ToString();
                            stageObj.Name = stage.Attributes["stagename"].ToString();
                            projBp.Stages.Add(stageObj);
                        }
                    }
                }
                else
                {
                    throw new Exception(" not found. ");
                }
            }
            catch (Exception e)
            {
                objCommon.TracingService.Trace("Project Business Process: " + e.Message);
            }
            return projBp;
        }

        //Create New Record Approvals Audid
        public static void SetApprovalAuditRecord(IOrganizationService objCommon, string approvalNameStep, EntityReference referenceRecord, EntityReference user, decimal amountApproved, bool approved = true)
        {

            var approvalAudit = new Entity("ddsm_approvalsaudit");
            approvalAudit["ddsm_name"] = approvalNameStep;
            approvalAudit["ddsm_user"] = user;
            if (approved)
                approvalAudit["ddsm_status"] = new OptionSetValue((int)ApprovalAuditStatus.Approved);
            else
                approvalAudit["ddsm_status"] = new OptionSetValue((int)ApprovalAuditStatus.Draft);

            approvalAudit["ddsm_amountapproved"] = new Money(amountApproved);
            if (referenceRecord.LogicalName == "ddsm_project")
                approvalAudit["ddsm_project"] = referenceRecord;
            else if (referenceRecord.LogicalName == "ddsm_projectgroup")
                approvalAudit["ddsm_projectgroup"] = referenceRecord;

            var existedApprovalAudit = ExistDraftApprovalAuditUser(objCommon, approvalAudit);

            if (Guid.Empty.Equals(existedApprovalAudit))
            {
                objCommon.Create(approvalAudit);
            }
            else // if (!Guid.Empty.Equals(existedApprovalAudit) && approved)
            {
                //  approvalAudit["ddsm_status"] = new OptionSetValue((int)ApprovalAuditStatus.Approved);
                approvalAudit.Id = existedApprovalAudit;
                objCommon.Update(approvalAudit);
            }
        }

        static Guid ExistDraftApprovalAuditUser(IOrganizationService objCommon, Entity approvalAudit)
        {
            Guid result = Guid.Empty;

            var query = new QueryExpression
            {
                EntityName = approvalAudit.LogicalName,
                ColumnSet = new ColumnSet("ddsm_name"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_user", ConditionOperator.Equal, (approvalAudit["ddsm_user"] as EntityReference).Id ),
                        new ConditionExpression("ddsm_status", ConditionOperator.Equal,(int)ApprovalAuditStatus.Draft ),
                        new ConditionExpression("ddsm_projectgroup", ConditionOperator.Equal, (approvalAudit["ddsm_projectgroup"]as EntityReference).Id),
                        new ConditionExpression("ddsm_name", ConditionOperator.Equal, approvalAudit["ddsm_name"])
                    }
                }
            };

            var crmResult = objCommon.RetrieveMultiple(query);
            if (crmResult.Entities.Count > 0)
            {
                //   objCommon.tracingService.Trace("ApprovalAudit Record in status Draft FOUND: "  + crmResult.Entities[0].Id);
                result = crmResult.Entities[0].Id;
            }

            return result;
        }

        //Check User In Team
        public static bool CheckUserInTeam(IOrganizationService objCommon, Guid teamGuid, Guid userGuid)
        {

            string fetchXml = @"<fetch version=""1.0"" output-format=""xml - platform"" mapping=""logical"" distinct=""true""><entity name=""team"">
                         <attribute name=""teamid""/>
                         <filter type=""and"">
                          <condition attribute=""teamid"" operator=""eq"" value=""" + teamGuid + @"""/>
                                </filter>
                                <link-entity name=""teammembership"" from=""teamid"" to=""teamid"" visible=""false"" intersect=""true"">
                                             <link-entity name=""systemuser"" from=""systemuserid"" to=""systemuserid"" alias=""ag"">
                                                        <filter type=""and"">
                                                           <condition attribute=""systemuserid"" operator=""eq""  uitype=""systemuser"" value= """ + userGuid + @"""/>
                                                                 </filter>
                                                               </link-entity>
                                                             </link-entity>
                                                           </entity></fetch> ";

            var givenTeams = objCommon.RetrieveMultiple(new FetchExpression(fetchXml));

            Boolean userInTeam = (givenTeams.Entities.Count > 0);

            return userInTeam;
        }

        // Search Team
        public static EntityReference GetTeam(EntityReference objTeamReference, string tplName, IOrganizationService orgService)
        {
            var team = new EntityReference();
            if (objTeamReference.LogicalName != "team")
            {
                var teamName = tplName.Split(' ')[0];
                var teamsQuery = new QueryExpression { EntityName = "team", ColumnSet = new ColumnSet("teamid") };
                teamsQuery.Criteria.AddCondition("name", ConditionOperator.Equal, teamName);
                var teamsRetrieve = orgService.RetrieveMultiple(teamsQuery);
                if (teamsRetrieve.Entities.Count > 0)
                    team = new EntityReference("team", new Guid(teamsRetrieve.Entities[0].Attributes["teamid"].ToString()));

                // Если Team не была найдена, то бралась первая запись Teams которой принадлежит User. Данная логика не корректна, но написана  была для теста
                /*
                else if (objTeamReference.LogicalName == "systemuser")
                {
                    string fetchXML = @"<fetch version='1.0' output-format='xml - platform' mapping='logical' distinct='true'><entity name='team'>
                         <attribute name='teamid'/>
                                <link-entity name='teammembership' from='teamid' to='teamid' visible='false' intersect='true'>
                                             <link-entity name='systemuser' from='systemuserid' to='systemuserid' alias='ag'>
                                                        <filter type='and'>
                                                           <condition attribute='systemuserid' operator='eq'  uitype='systemuser' value= '" + objTeamReference.Id.ToString() + @"'/>
                                                                 </filter>
                                                               </link-entity>
                                                             </link-entity>
                                                           </entity></fetch> ";
                    TeamsRetrieve = orgService.RetrieveMultiple(new FetchExpression(fetchXML));
                    if (TeamsRetrieve != null)
                        Team = new EntityReference("team", new Guid(TeamsRetrieve.Entities[0].Attributes["teamid"].ToString()));
                }
                */
            }
            else
                team = objTeamReference;

            return team;
        }

        // Search for a new user to the approver stage and sending e-mail
        public static void SearchAndSendEmailToUser(Common objDdsm, Guid recordTeam, decimal approvalAmount, decimal stageMinApprovalLimit, decimal stageMaxApprovalLimit, string currentMSstepName, Guid currentPGid, MilestoneList mSs)
        {
            var userInTeam = GetUserForNotification(stageMinApprovalLimit, recordTeam, stageMaxApprovalLimit, objDdsm.GetOrgService(systemCall: true), approvalAmount);// getUserForSendEmail(StageMinApprovalLimit, recordTeam, StageMaxApprovalLimit, objDdsm.orgService);

            if (userInTeam == null)
            {
                var pairs = GetPairs(mSs, stageMaxApprovalLimit);

                foreach (var item in pairs)
                {

                    var max = item;
                    userInTeam = GetUserForNotification(stageMaxApprovalLimit, recordTeam, max, objDdsm.GetOrgService(systemCall: true), approvalAmount);


                    stageMaxApprovalLimit = max;

                    if (userInTeam != null)
                        break;
                }

                if (userInTeam == null)
                {
                    objDdsm.TracingService.Trace("No one user in your team which have approval amount for next stage. Please add user with needed approval amount for next stage.");
                    // throw new NoUserinTeamException("No one user in your team which have approval amount for next stage. Please add user with needed approval amount for next stage.");
                    return;
                }
            }

            Ddsm.SetApprovalAuditRecord(objDdsm.GetOrgService(systemCall: true), currentMSstepName, new EntityReference("ddsm_projectgroup", currentPGid), new EntityReference("systemuser", userInTeam.GetAttributeValue<EntityReference>("ddsm_user").Id), approvalAmount, false);

            var tastCreationReq = CreateEmailTask(userInTeam, currentMSstepName, currentPGid);

            if (!EmailTaskExist(objDdsm.GetOrgService(systemCall: true), tastCreationReq.Target))
                objDdsm.GetOrgService(systemCall: true).Create(tastCreationReq.Target);
            else
            {
                objDdsm.TracingService.Trace("Notification already sended.");
            }
        }

        private static List<decimal> GetPairs(MilestoneList entities, decimal min)
        {
            List<decimal> result = new List<decimal>();

            foreach (var item in entities)
            {
                //  object ssss = null;
                //   if (item.Attributes.TryGetValue("ddsm_approvalthresholdcust", out ssss))
                //     {
                //     var mSapprovalAmount = ssss as Money;
                if (item.Amount > min)
                    result.Add(item.Amount);
                //   }

            }
            return result;


        }

        private static Entity GetUserForNotification(decimal stageMinApprovalLimit, Guid recordTeam, decimal stageMaxApprovalLimit, IOrganizationService objCommon, decimal approvalAmount)
        {
            EntityCollection users;
            var randomUser = new Entity();
            //  if (approvalsAmmountsRules == Ddsm.ApprovalsAmmountsRules.SearchRange)
            // {
            users = Ddsm.GetUserForSendEmail(stageMinApprovalLimit, recordTeam, stageMaxApprovalLimit, objCommon);
            randomUser = users.Entities.Random();
            //}
            //else
            //{
            //    users = Ddsm.GetUserForSendEmail(approvalAmount, recordTeam, stageMaxApprovalLimit, objCommon);
            //    randomUser = users.Entities[0];
            //}
            return randomUser;
        }

        public static EntityCollection GetUserForSendEmail(decimal prevMsApprovalLimit, Guid recordTeam, decimal currentMsApprovalLimit, IOrganizationService objDdsm)
        {
            var fetch0 = @"<fetch>
                          <entity name='ddsm_userapproval' >
                            <attribute name='ddsm_name' />
                            <attribute name='ddsm_approvalthresholdcust' />
                            <attribute name='ddsm_user' />
                            <filter type='and' >
                                <condition attribute='ddsm_approvalthresholdcust' operator='gt' value='{1}'/>
                                <condition attribute='ddsm_approvalthresholdcust' operator='le' value='{2}' />
                            </filter>
                            <order attribute='ddsm_approvalthresholdcust' descending='true' />
                            <link-entity name='systemuser' from='systemuserid' to='ddsm_user' >
                              <link-entity name='teammembership' from='systemuserid' to='systemuserid' intersect='true' >
                                <link-entity name='team' from='teamid' to='teamid' >
                                  <filter type='and' >
                                    <condition attribute='teamid' operator='eq' value='{0}' />
                                  </filter>
                                </link-entity>
                              </link-entity>
                            </link-entity>
                          </entity>
                        </fetch>";


            fetch0 = string.Format(fetch0, recordTeam, prevMsApprovalLimit, currentMsApprovalLimit);


            return objDdsm.RetrieveMultiple(new FetchExpression(fetch0));
        }

        public static CreateRequest CreateEmailTask(Entity user, string currentMSstepName, Guid currentPGid)
        {
            var tastCreationReq = new CreateRequest();

            tastCreationReq.Target = new Entity("ddsm_taskqueue")
            {
                Attributes = new AttributeCollection {
                                            new KeyValuePair<string, object>("ddsm_name"," Email Task " + DateTime.UtcNow),
                                            new KeyValuePair<string, object>("ddsm_taskentity",new OptionSetValue((int)TaskEntity.Email)),
                                            new KeyValuePair<string, object>("ddsm_recipientuserid", user.GetAttributeValue<EntityReference>("ddsm_user")  ),
                                            new KeyValuePair<string, object>("ddsm_projectgroupid", new EntityReference("ddsm_projectgroup",currentPGid)  ),
                                            new KeyValuePair<string, object>("ddsm_currentmsstep", currentMSstepName)
                        }
            };

            return tastCreationReq;

        }

        public static bool EmailTaskExist(IOrganizationService objDdsm, Entity target)
        {
            var query = new QueryExpression
            {
                EntityName = target.LogicalName,
                ColumnSet = new ColumnSet("ddsm_name"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions = {
                        new ConditionExpression("ddsm_projectgroupid",ConditionOperator.Equal, target.GetAttributeValue<EntityReference>("ddsm_projectgroupid").Id ),
                        new ConditionExpression("ddsm_currentmsstep",ConditionOperator.Equal, target.GetAttributeValue<string>("ddsm_currentmsstep") ),
                        new ConditionExpression("ddsm_recipientuserid",ConditionOperator.Equal, target.GetAttributeValue<EntityReference>("ddsm_recipientuserid").Id )
                    }
                }
            };

            var existingEmailTask = objDdsm.RetrieveMultiple(query);

            if (existingEmailTask.Entities.Count > 0)
            {
                //  objDdsm.
                return true;
            }

            else
                return false;
        }


        //Conver to UTC Date

        public void CloneMeasuresProject(Ddsm objCommon, Guid projectId, string projectPhaseName, int projPhaseNumber, string projectStatus, DateTime actualEnd)
        {
            objCommon.TracingService.Trace("Start cloning Measures ...");

            var relationship = new Relationship();
            relationship.SchemaName = "ddsm_ddsm_project_ddsm_measure";

            var query = new QueryExpression();
            query.EntityName = "ddsm_measure";
            query.ColumnSet = new ColumnSet("ddsm_measureid");
            query.Criteria = new FilterExpression();
            query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));
            //query.Criteria.AddCondition(new ConditionExpression("ddsm_uptodatephase", ConditionOperator.Equal, (projPhaseNumber - 1)));

            var relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);

            var request = new RetrieveRequest();
            request.ColumnSet = new ColumnSet("ddsm_projectid");
            request.Target = new EntityReference { Id = projectId, LogicalName = "ddsm_project" };
            request.RelatedEntitiesQuery = relatedEntity;

            var response = (RetrieveResponse)objCommon.OrgService.Execute(request);

            if (response == null || response.Results.Count == 0 || response.Entity == null || response.Entity.RelatedEntities.Count == 0) {
                return;
            }

            DataCollection<Entity> relatedEntitis = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship("ddsm_ddsm_project_ddsm_measure")].Entities;
            foreach (var meas in relatedEntitis)
            {

                objCommon.TracingService.Trace("Rcord Meas ID: " + meas.GetAttributeValue<Guid>("ddsm_measureid"));

                //Get Measure Attr
                var retrievedObject = objCommon.OrgService.Retrieve("ddsm_measure", meas.GetAttributeValue<Guid>("ddsm_measureid"), new ColumnSet(allColumns: true));

                objCommon.TracingService.Trace("retrieved object OK");

                var newEntity = new Entity("ddsm_measure");
                string primaryIdAttribute = "";
                var atts = GetEntityAttributesToClone("ddsm_measure", objCommon.OrgService, ref primaryIdAttribute);

                foreach (string att in atts)
                {
                    if (retrievedObject.Attributes.Contains(att) && att != "statuscode" && att != "statecode"
                        || att.StartsWith("partylist-"))
                    {
                        if (att.StartsWith("partylist-"))
                        {
                            var att2 = att.Replace("partylist-", "");

                            string fetchParty = @"<fetch version='1.0' output-format='xml - platform' mapping='logical' distinct='true'>
                                                <entity name='activityparty'>
                                                    <attribute name = 'partyid'/>
                                                        <filter type = 'and' >
                                                            <condition attribute = 'activityid' operator= 'eq' value = '" + (meas.GetAttributeValue<Guid>("ddsm_measureid")) + @"' />
                                                            <condition attribute = 'participationtypemask' operator= 'eq' value = '" + GetParticipation(att2) + @"' />
                                                         </filter>
                                                </entity>
                                            </fetch> ";

                            var fetchRequest1 = new RetrieveMultipleRequest
                            {
                                Query = new FetchExpression(fetchParty)
                            };
                            //objCommon.tracingService.Trace(fetchParty);
                            EntityCollection returnCollection = ((RetrieveMultipleResponse)objCommon.OrgService.Execute(fetchRequest1)).EntityCollection;


                            var arrPartiesNew = new EntityCollection();
                            //objCommon.tracingService.Trace("attribute:{0}", att2);

                            foreach (Entity ent in returnCollection.Entities)
                            {
                                var party = new Entity("activityparty");
                                var partyid = (EntityReference)ent.Attributes["partyid"];


                                party.Attributes.Add("partyid", new EntityReference(partyid.LogicalName, partyid.Id));
                                //objCommon.tracingService.Trace("attribute:{0}:{1}:{2}", att2, partyid.LogicalName, partyid.Id.ToString());
                                arrPartiesNew.Entities.Add(party);
                            }

                            newEntity.Attributes.Add(att2, arrPartiesNew);
                            continue;

                        }

                        //objCommon.tracingService.Trace("attribute:{0}", att);
                        newEntity.Attributes.Add(att, retrievedObject.Attributes[att]);
                    }
                }

                //newEntity["statecode"] = new OptionSetValue(1);

                newEntity["ddsm_projectphasename"] = projectPhaseName;
                newEntity["ddsm_uptodatephase"] = projPhaseNumber;
                newEntity["ddsm_projectstatus"] = projectStatus;
                newEntity["ddsm_initialphasedate"] = actualEnd;
                newEntity["ddsm_parentmeasureid"] = new EntityReference("ddsm_measure", meas.GetAttributeValue<Guid>("ddsm_measureid"));

                var updateMeas = new Entity("ddsm_measure", meas.GetAttributeValue<Guid>("ddsm_measureid"));
                updateMeas["ddsm_disablerecalculation"] = true;
                updateMeas["statecode"] = new OptionSetValue(1);
                updateMeas["statuscode"] = new OptionSetValue(2);
                objCommon.OrgService.Update(updateMeas);

                //Get Measure Reporting Attr
                EntityCollection mrCollection = new EntityCollection();
                QueryExpression queryMR = new QueryExpression();
                queryMR.EntityName = "ddsm_measurereporting";
                queryMR.ColumnSet = new ColumnSet(allColumns: true);
                queryMR.Criteria = new FilterExpression(LogicalOperator.And);
                queryMR.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));
                queryMR.Criteria.AddCondition(new ConditionExpression("ddsm_measure", ConditionOperator.Equal, meas.GetAttributeValue<Guid>("ddsm_measureid")));
                EntityCollection recordMR = objCommon.OrgService.RetrieveMultiple(queryMR);
                if (recordMR != null && recordMR.Entities.Count > 0)
                {
                    foreach (Entity mr in recordMR.Entities)
                    {
                        var newMR = new Entity("ddsm_measurereporting");
                        string primaryIdAttributeMR = "";
                        var attsMR = GetEntityAttributesToClone("ddsm_measurereporting", objCommon.OrgService, ref primaryIdAttributeMR);
                        foreach (string _att in attsMR)
                        {
                            if (mr.Attributes.Contains(_att) && _att != "statuscode" && _att != "statecode" && !_att.StartsWith("partylist-"))
                            {
                                newMR.Attributes.Add(_att, mr.Attributes[_att]);

                            }
                        }

                        if (newMR.Attributes.Count > 1) {
                            mrCollection.Entities.Add(newMR);
                            mrCollection.EntityName = newMR.LogicalName;
                        }

                        var updateMR = new Entity("ddsm_measurereporting", mr.GetAttributeValue<Guid>("ddsm_measurereportingid"));
                        updateMR["statecode"] = new OptionSetValue(1);
                        updateMR["statuscode"] = new OptionSetValue(2);
                        objCommon.OrgService.Update(updateMR);

                    }
                }

                if (mrCollection.Entities.Count > 0) {
                    Relationship mrRelationship = new Relationship("ddsm_ddsm_measure_ddsm_measurereporting_Measure");
                    newEntity.RelatedEntities.Add(mrRelationship, mrCollection);
                }


                objCommon.TracingService.Trace("creting cloned object...");
                var createdGuid = objCommon.OrgService.Create(newEntity);

                //StateCode = 1 and StatusCode = 2 for deactivating record
                /*
                var setStateRequest = new SetStateRequest()
                {
                    EntityMoniker = new EntityReference
                    {
                        Id = createdGuid,
                        LogicalName = "ddsm_measure",
                    },
                    State = new OptionSetValue(1),
                    Status = new OptionSetValue(2)
                };
                objCommon.OrgService.Execute(setStateRequest);
                */

                objCommon.TracingService.Trace("created cloned object OK");
                /*
                                var updateMeas = new Entity("ddsm_measure", meas.GetAttributeValue<Guid>("ddsm_measureid"));
                                updateMeas["ddsm_projectphasename"] = projectPhaseName;
                                updateMeas["ddsm_uptodatephase"] = projPhaseNumber;
                                updateMeas["ddsm_projectstatus"] = projectStatus;
                                updateMeas["ddsm_initialphasedate"] = actualEnd;
                                objCommon.OrgService.Update(updateMeas);
                */
                /*
                Entity record = objCommon.orgService.Retrieve("ddsm_measure", createdGUID, new ColumnSet("statuscode", "statecode"));

                if (retrievedObject.Attributes["statuscode"] != record.Attributes["statuscode"] ||
                    retrievedObject.Attributes["statecode"] != record.Attributes["statecode"])
                {
                    Entity setStatusEnt = new Entity("ddsm_measure", createdGUID);
                    setStatusEnt.Attributes.Add("statuscode", retrievedObject.Attributes["statuscode"]);
                    setStatusEnt.Attributes.Add("statecode", retrievedObject.Attributes["statecode"]);

                    objCommon.orgService.Update(setStatusEnt);
                }

                objCommon.tracingService.Trace("cloned object OK");
                */
            }
        }

        public DateTime ResetTimeToStartOfDay(DateTime dateTime)
        {
            return new DateTime(
               dateTime.Year,
               dateTime.Month,
               dateTime.Day,
               0, 0, 0, 0, DateTimeKind.Utc);
        }

        public DateTime ResetTimeToEndOfDay(DateTime dateTime)
        {
            return new DateTime(
               dateTime.Year,
               dateTime.Month,
               dateTime.Day,
               23, 59, 59, 999, DateTimeKind.Utc);
        }


    }
}
