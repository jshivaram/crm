﻿using System;
using System.Activities;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Moq;

namespace UnitTests.Tests
{
    [TestClass]
    public class CalculationPluginUnitTest
    {
        #region Class Constructor
        string _namespaceClassAssembly;

        public CalculationPluginUnitTest()
        {
            //[Namespace.class name, assembly name] for the class/assembly being tested
            //Namespace and class name can be found on the class file being tested
            //Assembly name can be found under the project properties on the Application tab

            _namespaceClassAssembly = "GetAllMadInfo" + ", " + "DDSM.SmartMeasureCalculationPlugin";

            //"DDSM.Helper.Class.CreateProject"
        }
        #endregion
        #region Test Initialization and Cleanup
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext) { }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void ClassCleanup() { }

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void TestMethodInitialize() { }

        // Use TestCleanup to run code after each test has run
        [TestCleanup]
        public void TestMethodCleanup() { }
        #endregion

        [TestMethod]
        public void TestRecalculateRelatedRateClassesFromMeasure()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "RecalculateRelatedRateClassesFromMeasure" + ", " + "DDSM.CalculationPlugin";

            //Target
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_measure", Id = Guid.Parse("2B008011-DFEE-E611-8138-323862316463") };


            //Input parameters
            var inputs = new Dictionary<string, object>
            {
               { "Measure", new EntityReference("ddsm_measure",Guid.Parse("2B008011-DFEE-E611-8138-323862316463")) }
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestCalculateDSMPalnTotalsField()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "CalculateDSMPalnTotalsField" + ", " + "DDSM.CalculationPlugin";

            //Target            
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_taskqueue", Id = Guid.NewGuid() };


            //Input parameters
            var inputs = new Dictionary<string, object>
            {
               { "UserInput","{'SmartMeasures':['0070c854-3406-e611-80c6-323166616637'],'DataFields':null}"},

            };
            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestCalculatePortfolioTotalsField()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "CalculatePortfolioTotalsField" + ", " + "DDSM.CalculationPlugin";

            //Target            
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_taskqueue", Id = Guid.NewGuid() };


            //Input parameters
            var inputs = new Dictionary<string, object>
            {
               { "UserInput","{'SmartMeasures':['f4d07aaf-abf5-e511-80bf-323166616637'],'DataFields':null}"},

            };
            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestCalculateProgramTotalsField()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "CalculateProgramTotalsField" + ", " + "DDSM.CalculationPlugin";

            //Target            
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_taskqueue", Id = Guid.NewGuid() };


            //Input parameters
            var inputs = new Dictionary<string, object>
            {
               { "UserInput","{'SmartMeasures':['c82681a0-70f6-e511-80c0-323166616637'],'DataFields':null}"},

            };
            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestCalculateProgramOfferingTotalsField()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "CalculateProgramOfferingTotalsField" + ", " + "DDSM.CalculationPlugin";

            //Target            
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_taskqueue", Id = Guid.NewGuid() };


            //Input parameters
            var inputs = new Dictionary<string, object>
            {
               { "UserInput","{'SmartMeasures':['82f4ac3a-25fb-e511-80c0-323166616637'],'DataFields':null}"},

            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestFinancialTotalField()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "ClaculateFinancialTotalField" + ", " + "DDSM.CalculationPlugin";

            //Target            
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_taskqueue", Id = Guid.Parse("{491D02AC-D7B7-E611-80EF-366464306133}") };


            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                   { "UserInput","{'SmartMeasures':['3669b430-ce3f-e711-8118-323863623963'],'DataFields':{}}"}
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestCalculateProjectGroupFinancialTotalsField()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "CalculateProjectGroupFinancialTotalsField" + ", " + "DDSM.CalculationPlugin";

            //Target            
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_taskqueue", Id = Guid.Parse("{BFA64E2C-2CC3-E611-80C2-363538646466}") };
            
            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                 { "UserInput","{'SmartMeasures':['ab8565fd-2bc3-e611-80c2-363538646466','d18565fd-2bc3-e611-80c2-363538646466','e48565fd-2bc3-e611-80c2-363538646466','be8565fd-2bc3-e611-80c2-363538646466'],'DataFields':null}"},
              //  {"DisablePGRecalculation",true }
            };

            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestProgramIntervalsFromMeasure()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "CalculateProgramIntrervalTotalsField" + ", " + "DDSM.CalculationPlugin";

            //Target            new for test b8f80961-bb16-e611-80c7-323166616637   /old ebcab351-f711-e611-80c6-323166616637
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_measure", Id = Guid.Parse("170C2B4D-B51D-E611-80BF-00505601015C") };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
             //{"FromMeasureGrid", true }, //qty =5
             //{ "UserInput","{'DataFields':{'Units':'33','Tank Volume (gal) (for tankless units enter 0)':'','New Water Heater Efficiency (0.00 - 1.00)':'','Water Heater Type':'','Building Type':'','Climate Zone':'','Unit Participant Incremental Cost ($)':'','Net to Gross Ratio':'','ddsm_recalculationgroup':''},'SmartMeasures':['{D2668382-100E-E611-80DC-C4346BDCD151}','{64E91F41-100E-E611-80DC-C4346BDCD151}']}"},
               { "UserInput","{'SmartMeasures':['7dbc7fb9-5623-e611-80ca-323166616637'],'DataFields':null}"},

            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void CalculateProgramIntrervalTotalsFieldForecastActual()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "CalculateProgramIntrervalTotalsFieldForecastActual" + ", " + "DDSM.CalculationSandboxPlugin";//"DDSM.CalculationPlugin";

            //Target            new for test b8f80961-bb16-e611-80c7-323166616637   /old ebcab351-f711-e611-80c6-323166616637
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_measure", Id = Guid.Parse("10DD5A3E-8D13-E711-813A-666665393634") };
            //Input parameters
            var inputs = new Dictionary<string, object>
            {
             //{"FromMeasureGrid", true }, //qty =5
             //{ "UserInput","{'DataFields':{'Units':'33','Tank Volume (gal) (for tankless units enter 0)':'','New Water Heater Efficiency (0.00 - 1.00)':'','Water Heater Type':'','Building Type':'','Climate Zone':'','Unit Participant Incremental Cost ($)':'','Net to Gross Ratio':'','ddsm_recalculationgroup':''},'SmartMeasures':['{D2668382-100E-E611-80DC-C4346BDCD151}','{64E91F41-100E-E611-80DC-C4346BDCD151}']}"},
               { "UserInput","{'SmartMeasures':['10DD5A3E-8D13-E711-813A-666665393634'],'DataFields':null}"},

            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestProjectFromMeasure()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "CalculateProjectTotalsField" + ", " + "DDSM.CalculationPlugin";

            //Target            new for test b8f80961-bb16-e611-80c7-323166616637   /old ebcab351-f711-e611-80c6-323166616637
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_taskqueue", Id = Guid.Parse("{C583E30B-F7EE-E611-8124-666665393634}") };


            //Input parameters
            var inputs = new Dictionary<string, object>
            {
              // { "Project", null }
               { "UserInput", "{'SmartMeasures':['98a8b263-f9ee-e611-8124-666665393634','b2a8b263-f9ee-e611-8124-666665393634','cca8b263-f9ee-e611-8124-666665393634','e6a8b263-f9ee-e611-8124-666665393634','33a9b263-f9ee-e611-8124-666665393634'],'DataFields':null}"},
              // { "Complete", ""}
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMeasureCalculation()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "MeasureCalculation" + ", " + "DDSM.CalculationPlugin";
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_taskqueue", Id = Guid.NewGuid() };
            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                  { "UserInput","{'DataFields':null,'SmartMeasures':['11f31db1-fa27-e611-80bf-00505601015c','3af31db1-fa27-e611-80bf-00505601015c','63f31db1-fa27-e611-80bf-00505601015c','8cf31db1-fa27-e611-80bf-00505601015c','b5f31db1-fa27-e611-80bf-00505601015c','def31db1-fa27-e611-80bf-00505601015c']}"},
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMeasureOEMRecoveryCalculation()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "RecalculateMeasureOEMRecovery" + ", " + "DDSM.CalculationPlugin";
            EntityReference targetEntity = new EntityReference { LogicalName = "ddsm_project", Id = Guid.Parse("972C819B-E01C-E611-80C8-323166616637") };
            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                { "RecoveryPercentage",0.10}
             // { "UserInput","{'DataFields':null,'SmartMeasures':['11f31db1-fa27-e611-80bf-00505601015c','3af31db1-fa27-e611-80bf-00505601015c','63f31db1-fa27-e611-80bf-00505601015c','8cf31db1-fa27-e611-80bf-00505601015c','b5f31db1-fa27-e611-80bf-00505601015c','def31db1-fa27-e611-80bf-00505601015c']}"},
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        /// <summary>
        /// Modify to mock CRM Organization Service actions
        /// </summary>
        /// <param name="serviceMock">The Organization Service to mock</param>
        /// <returns>Configured Organization Service</returns>
        static Mock<IOrganizationService> TestMethod1Setup(Mock<IOrganizationService> serviceMock)
        {
            EntityCollection queryResult = new EntityCollection();
            ////Add created items to EntityCollection

            serviceMock.Setup(t =>
                t.RetrieveMultiple(It.IsAny<Microsoft.Xrm.Sdk.Query.QueryExpression>()))
                .ReturnsInOrder(queryResult);

            return serviceMock;
        }

        /// <summary>
        /// Invokes the workflow.
        /// </summary>
        /// <param name="name">Namespace.Class, Assembly</param>
        /// <param name="target">The target entity</param>
        /// <param name="inputs">The workflow input parameters</param>
        /// <param name="configuredServiceMock">The function to configure the Organization Service</param>
        /// <returns>The workflow output parameters</returns>
        static IDictionary<string, object> InvokeWorkflow(string name, ref EntityReference target, Dictionary<string, object> inputs,
            Func<Mock<IOrganizationService>, Mock<IOrganizationService>> configuredServiceMock)
        {
            var testClass = Activator.CreateInstance(Type.GetType(name)) as CodeActivity;

            var serviceMock = new Mock<IOrganizationService>();
            var factoryMock = new Mock<IOrganizationServiceFactory>();
            var tracingServiceMock = new Mock<ITracingService>();
            var workflowContextMock = new Mock<IWorkflowContext>();

            //Apply configured Organization Service Mock
            if (configuredServiceMock != null)
                serviceMock = configuredServiceMock(serviceMock);
           // CrmConnection connection = CrmConnection.Parse("Url=http://192.168.1.12/RLS; Username=Administrator; Password=Ghjuhfvth89!");

            //CrmConnection connection = CrmConnection.Parse("Url=http://ddsm-build-e1.dynamicdsm.com/RLS; Username=yuriyn; Password=AgAJfH6T4p");
            //CrmConnection connection = CrmConnection.Parse("Url=http://dev3.dynamicdsm.com/STAGING/; Username=Administrator; Password=Rjnecz1219693");
            // for Auto tester 
             CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS/; Username=Administrator; Password=Dark#zone#19");

            //CrmConnection connection = CrmConnection.Parse("Url=http://ddsm-build-e1.dynamicdsm.com/XPRODGIT/;  Username=Administrator; Password=ghjuhfvth; Timeout=00:10:00;");
            // CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/EfficiencyOneDDSM; Username=Administrator; Password=dark#zone#19; Timeout=00:10:00;");
            //  CrmConnection connection = CrmConnection.Parse("Url=https://egddemo1.crm.dynamics.com/; ; Username=admin@EGDdemo1.onmicrosoft.com; Password=P@ssword1; Timeout=00:10:00;");
            //CrmConnection connection = CrmConnection.Parse("Url=http://e1-dev1.dynamicdsm.com/RLS/; Domain=e1-dev1.dynamicdsm.com; Username=dev2; Password=Development2pass!; Timeout=00:10:00;");
            // CrmConnection connection = CrmConnection.Parse("Url=https://crmdev.efficiencyns.ca:443/EfficiencyOneCRMDev/; Domain=ensc; Username=agsadmin; Password=Winter2016; Timeout=00:10:00;");
            // CrmConnection connection = CrmConnection.Parse("Url=https://dynamicdsmdemo1.crm.dynamics.com/; Username=yuri.nazarenko@accentgold.com; Password=dark#zone#19;");
            //  CrmConnection connection = CrmConnection.Parse("Url=https://dynamicdsmdemo2.crm.dynamics.com/; Username=yuri.nazarenko@accentgold.com; Password=dark#zone#19;");
            // CrmConnection connection = CrmConnection.Parse("Url=http://demo1.dynamicdsm.com/rls; Username=qa1; Password=Quality1pass!;");



            IOrganizationService service = new OrganizationService(connection);
            //  IOrganizationService service = serviceMock.Object;


            //Mock workflow Context
            var workflowUserId = Guid.NewGuid();
            var workflowCorrelationId = Guid.NewGuid();
            var workflowInitiatingUserId = Guid.NewGuid();

            //Workflow Context Mock
            workflowContextMock.Setup(t => t.InitiatingUserId).Returns(workflowInitiatingUserId);
            workflowContextMock.Setup(t => t.CorrelationId).Returns(workflowCorrelationId);
            workflowContextMock.Setup(t => t.UserId).Returns(workflowUserId);
            var workflowContext = workflowContextMock.Object;

            //Organization Service Factory Mock
            factoryMock.Setup(t => t.CreateOrganizationService(It.IsAny<Guid>())).Returns(service);
            var factory = factoryMock.Object;

            //Tracing Service - Content written appears in output
            tracingServiceMock.Setup(t => t.Trace(It.IsAny<string>(), It.IsAny<object[]>())).Callback<string, object[]>(MoqExtensions.WriteTrace);
            var tracingService = tracingServiceMock.Object;

            //Parameter Collection
            Microsoft.Xrm.Sdk.ParameterCollection inputParameters = new Microsoft.Xrm.Sdk.ParameterCollection { { "Target", target } };
            workflowContextMock.Setup(t => t.InputParameters).Returns(inputParameters);

            //Workflow Invoker
            var invoker = new WorkflowInvoker(testClass);
            invoker.Extensions.Add(() => tracingService);
            invoker.Extensions.Add(() => workflowContext);
            invoker.Extensions.Add(() => factory);

            return invoker.Invoke(inputs);
        }
    }
}
