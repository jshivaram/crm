﻿using System;
using System.Activities;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Workflow;
using Moq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace UnitTests.Tests
{
    [TestClass]
    public class DdsmToolsUnitTest
    {
        #region Class Constructor
        private string _namespaceClassAssembly;

        public DdsmToolsUnitTest()
        {
            _namespaceClassAssembly = "GetAllMadInfo" + ", " + "DDSM.SmartMeasureCalculationPlugin";
        }
        #endregion
        #region Test Initialization and Cleanup
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext) { }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void ClassCleanup() { }

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void TestMethodInitialize() { }

        // Use TestCleanup to run code after each test has run
        [TestCleanup]
        public void TestMethodCleanup() { }
        #endregion

        [TestMethod]
        public void TestMethod__CloneMeasProjectNextStage()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "DdsmTools.Class.CloneMeasProjectNextStage" + ", " + "DdsmTools";

            var targetEntity = new EntityReference { LogicalName = "ddsm_project", Id = Guid.Parse("896EA98B-7D29-E711-8141-666665393634") };

            //Input parameters
            var inputs = new Dictionary<string, object> {
               { "Project", new EntityReference("ddsm_project",Guid.Parse("896EA98B-7D29-E711-8141-666665393634")) },
               { "ActualEnd", DateTime.Now }
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethod__AssociateAttachDoc()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "DdsmTools.AssociateAttachDoc" + ", " + "DdsmTools";

            var targetEntity = new EntityReference { LogicalName = "ddsm_attachmentdocument", Id = Guid.Parse("BA03056B-6909-E711-8139-666665393634") };

            //Input parameters
            var inputs = new Dictionary<string, object> {
               { "TargetVersion", new EntityReference("ddsm_attachmentdocument",Guid.Parse("C45E64EE-6E09-E711-8139-666665393634")) },
               { "ParentVersion", new EntityReference("ddsm_attachmentdocument",Guid.Parse("CC7B9E72-6909-E711-8139-666665393634")) }
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethod__BusinessProcessNextStage()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "DdsmTools.BusinessProcessNextStage" + ", " + "DdsmTools";

            var targetEntity = new EntityReference { LogicalName = "ddsm_project", Id = Guid.Parse("469BD7CD-C007-E711-8135-666665393634") };

            //Input parameters
            var inputs = new Dictionary<string, object> { };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethod__ManuallyCreatingProject()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "DdsmTools.ManuallyCreatingProject" + ", " + "DdsmTools";
            var targetEntity = new EntityReference { LogicalName = "ddsm_site", Id = Guid.Parse("F0D49434-3EF4-E611-812D-666665393634") };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                { "ParentAccount", "CCD49434-3EF4-E611-812D-666665393634" },
                { "ParentSite", "F0D49434-3EF4-E611-812D-666665393634" },
                { "ProgramOffering", "07F2B366-55D8-E611-80FA-666665393634" },
                { "ProjectTemplate", "8784FDE1-5ED8-E611-80FA-666665393634" },
                { "StartDate", "04/13/2017 0:0:0" },
                { "PartnerContact", "" },
                { "PartnerCompany", "" }

            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethod__PG_BusinessProcessNextStage()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "DdsmTools.PG_BusinessProcessNextStage" + ", " + "DdsmTools";

            //Target            new for test b8f80961-bb16-e611-80c7-323166616637   /old ebcab351-f711-e611-80c6-323166616637
            var targetEntity = new EntityReference { LogicalName = "ddsm_projectgroup", Id = Guid.Parse("803BF357-7874-E611-80CB-366464306133") };

            //Input parameters
            var inputs = new Dictionary<string, object> { };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethod__SendEmail()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "DdsmTools.Class.SendEmail" + ", " + "DdsmTools";
            var targetEntity = new EntityReference { LogicalName = "ddsm_projectgroupmilestone", Id = Guid.Parse("ECB71DEC-DB57-E611-80ED-323166616637") };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                { "Index", 6 }
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }


        [TestMethod]
        public void TestMethod__GroupSendEmail()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "DdsmTools.Class.GroupSendEmail" + ", " + "DdsmTools";

            var targetEntity = new EntityReference { LogicalName = "ddsm_taskqueue", Id = Guid.NewGuid() };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                { "PGIds","{'SmartMeasures':['10aced81-1e70-e611-80db-366637386334'],'DataFields':{}}" }
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethod__CreateMNAmultipleByMeasureID()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "DdsmTools.CreateMNAmultipleByMeasureID" + ", " + "DdsmTools";

            var targetEntity = new EntityReference { LogicalName = "ddsm_modelnumber", Id = Guid.Parse("3402D553-4324-E711-815C-3A3035663562") };

            //Input parameters
            var inputs = new Dictionary<string, object> {
               { "ModelNumberApproval", new EntityReference("ddsm_modelnumber",Guid.Parse("EDDC728C-2115-E711-8122-080027D22A46")) },
               { "ModelNumber", new EntityReference("ddsm_modelnumberapproval",Guid.Parse("3402D553-4324-E711-815C-3A3035663562")) }
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethod__CreateSKUAmultipleByMeasureID()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "DdsmTools.CreateSKUAmultipleByMeasureID" + ", " + "DdsmTools";

            var targetEntity = new EntityReference { LogicalName = "ddsm_sku", Id = Guid.Parse("98F7DBAD-5C15-E711-8123-080027D22A46") };

            //Input parameters
            var inputs = new Dictionary<string, object> {
               { "SKUApproval", new EntityReference("ddsm_skuapproval",Guid.Parse("FECCA7B2-5C15-E711-8123-080027D22A46")) },
               { "SKU", new EntityReference("ddsm_sku",Guid.Parse("98F7DBAD-5C15-E711-8123-080027D22A46")) }
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethod__CreateSKULinkSKUAbyCertificationID()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "DdsmTools.CreateSKULinkSKUAbyCertificationID" + ", " + "DdsmTools";

            var targetEntity = new EntityReference { LogicalName = "ddsm_sku", Id = Guid.Parse("08291B21-1014-E711-813A-666665393634") };

            //Input parameters
            var inputs = new Dictionary<string, object> {
               { "SKU", new EntityReference("ddsm_sku",Guid.Parse("08291B21-1014-E711-813A-666665393634")) }
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethod__GetEntityReferenceByID()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "DdsmTools.GetEntityReferenceByID" + ", " + "DdsmTools";

            var targetEntity = new EntityReference { LogicalName = "ddsm_modelnumber", Id = Guid.Parse("3402D553-4324-E711-815C-3A3035663562") };

            //Input parameters
            var inputs = new Dictionary<string, object> {
               { "EntityID", "BERIR000500511" },
               { "EntityName", "ddsm_measuretemplate" }
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        /// <summary>
        /// Modify to mock CRM Organization Service actions
        /// </summary>
        /// <param name="serviceMock">The Organization Service to mock</param>
        /// <returns>Configured Organization Service</returns>
        private static Mock<IOrganizationService> TestMethod1Setup(Mock<IOrganizationService> serviceMock)
        {
            EntityCollection queryResult = new EntityCollection();
            ////Add created items to EntityCollection

            serviceMock.Setup(t =>
                t.RetrieveMultiple(It.IsAny<QueryExpression>()))
                .ReturnsInOrder(queryResult);

            return serviceMock;
        }

        /// <summary>
        /// Invokes the workflow.
        /// </summary>
        /// <param name="name">Namespace.Class, Assembly</param>
        /// <param name="target">The target entity</param>
        /// <param name="inputs">The workflow input parameters</param>
        /// <param name="configuredServiceMock">The function to configure the Organization Service</param>
        /// <returns>The workflow output parameters</returns>
        private static IDictionary<string, object> InvokeWorkflow(string name, ref EntityReference target, Dictionary<string, object> inputs,
            Func<Mock<IOrganizationService>, Mock<IOrganizationService>> configuredServiceMock)
        {
            var testClass = Activator.CreateInstance(Type.GetType(name)) as CodeActivity;

            var serviceMock = new Mock<IOrganizationService>();
            var factoryMock = new Mock<IOrganizationServiceFactory>();
            var tracingServiceMock = new Mock<ITracingService>();
            var workflowContextMock = new Mock<IWorkflowContext>();

            //Apply configured Organization Service Mock
            if (configuredServiceMock != null)
                serviceMock = configuredServiceMock(serviceMock);

            //CrmConnection connection = CrmConnection.Parse("Url=http://192.168.1.12/RLS/;Username=Administrator; Password=Ghjuhfvth89!; Timeout=00:10:00;");
            //CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.191/EfficiencyOneDDSM/;Username=Administrator; Password=Rjnecz1219694; Timeout=00:10:00;");
            CrmConnection connection = CrmConnection.Parse("Url=http://ddsm-build-e1.ddsm.online/RLS/;Username=sergeyd; Password=GhjuhfvtH!; Timeout=00:10:00;");
             //CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS090916/; Username=Administrator; Password=c#102612; Timeout=00:10:00;");
            //  CrmConnection connection = CrmConnection.Parse("Url=https://egddemo1.crm.dynamics.com/; ; Username=admin@EGDdemo1.onmicrosoft.com; Password=P@ssword1; Timeout=00:10:00;");
            // CrmConnection connection = CrmConnection.Parse("Url=http://e1-dev1.dynamicdsm.com/RLS/; Domain=e1-dev1.dynamicdsm.com; Username=dev2; Password=Development2pass!; Timeout=00:10:00;");
            //  CrmConnection connection = CrmConnection.Parse("Url=http://e1-dev1.dynamicdsm.com/RLS/; Domain=e1-dev1.dynamicdsm.com; Username=pgatestuser5; Password=Pgatest!User5; Timeout=00:10:00;");
            //  CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS; Domain=RLS; Username=dev2; Password=Development2pass!; Timeout=00:10:00;");
            //   CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS310816/; Domain=RLS; Username=dev2; Password=Development2pass!; Timeout=00:10:00;");
            //CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS; Domain=RLS; Username=Administrator; Password=Rjnecz121969; Timeout=00:10:00;");
            IOrganizationService service = new OrganizationService(connection);
            //  IOrganizationService service = serviceMock.Object;


            //Mock workflow Context
            var workflowUserId = Guid.Parse("{3D4AB943-A3D2-E611-80F9-666665393634}");
            var workflowCorrelationId = Guid.NewGuid();
            var workflowInitiatingUserId = Guid.Parse("{3D4AB943-A3D2-E611-80F9-666665393634}");

            //Workflow Context Mock
            workflowContextMock.Setup(t => t.InitiatingUserId).Returns(workflowInitiatingUserId);
            workflowContextMock.Setup(t => t.CorrelationId).Returns(workflowCorrelationId);
            workflowContextMock.Setup(t => t.UserId).Returns(workflowUserId);
            var workflowContext = workflowContextMock.Object;

            //Organization Service Factory Mock
            factoryMock.Setup(t => t.CreateOrganizationService(It.IsAny<Guid>())).Returns(service);
            var factory = factoryMock.Object;

            //Tracing Service - Content written appears in output
            tracingServiceMock.Setup(t => t.Trace(It.IsAny<string>(), It.IsAny<object[]>())).Callback<string, object[]>(MoqExtensions.WriteTrace);
            var tracingService = tracingServiceMock.Object;

            //Parameter Collection
            ParameterCollection inputParameters = new ParameterCollection { { "Target", target } };
            workflowContextMock.Setup(t => t.InputParameters).Returns(inputParameters);

            //Workflow Invoker
            var invoker = new WorkflowInvoker(testClass);
            invoker.Extensions.Add(() => tracingService);
            invoker.Extensions.Add(() => workflowContext);
            invoker.Extensions.Add(() => factory);

            return invoker.Invoke(inputs);
        }
    }
}
