﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Moq;

namespace UnitTests.Tests
{
    [TestClass]
    public class SmartMeasureCalculationPluginUnitTest
    {
        #region Class Constructor
        private string _namespaceClassAssembly;

        public SmartMeasureCalculationPluginUnitTest()
        {
            //[Namespace.class name, assembly name] for the class/assembly being tested
            //Namespace and class name can be found on the class file being tested
            //Assembly name can be found under the project properties on the Application tab
            _namespaceClassAssembly = "GetAllMadInfo" + ", " + "DDSM.SmartMeasureCalculationPlugin";

            //"DDSM.Helper.Class.CreateProject"
        }
        #endregion
        #region Test Initialization and Cleanup
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext) { }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void ClassCleanup() { }

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void TestMethodInitialize() { }

        // Use TestCleanup to run code after each test has run
        [TestCleanup]
        public void TestMethodCleanup() { }
        #endregion

        [TestMethod]
        public void TestNewSettingTest()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "NewSettingTest" + ", " + "DDSM.SmartMeasureCalculationPlugin";

            //Target            new for test b8f80961-bb16-e611-80c7-323166616637   /old ebcab351-f711-e611-80c6-323166616637
            Entity targetEntity = new Entity { LogicalName = "ddsm_measure", Id = Guid.Parse("6241453D-9C22-E611-80CA-323166616637") };


            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                //{ "FetchXML", "<fetch aggregate='true' > <entity name='ddsm_eligibility' > <attribute name='ddsm_name' alias='ddsm_name' aggregate='count' /> <filter type='and' > <condition attribute='ddsm_programoffering' operator='eq' value='{b2ce3847-0333-e611-80e5-5065f38a59a1}' /> <condition attribute='ddsm_name' operator='like' value='%low income required%' /> </filter> </entity> </fetch>" },
                //  { "MeasureType", "ESP" },
                //  { "DisableRecalculation", false},
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethodRollUp()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "RollupFunctions" + ", " + "msdyncrmWorkflowTools";

            //Target            new for test b8f80961-bb16-e611-80c7-323166616637   /old ebcab351-f711-e611-80c6-323166616637
            Entity targetEntity = new Entity { LogicalName = "ddsm_measure", Id = Guid.Parse("6241453D-9C22-E611-80CA-323166616637") };


            //Input parameters
            var inputs = new Dictionary<string, object>
            {
               { "FetchXML", "<fetch aggregate='true' > <entity name='ddsm_eligibility' > <attribute name='ddsm_name' alias='ddsm_name' aggregate='count' /> <filter type='and' > <condition attribute='ddsm_programoffering' operator='eq' value='{b2ce3847-0333-e611-80e5-5065f38a59a1}' /> <condition attribute='ddsm_name' operator='like' value='%low income required%' /> </filter> </entity> </fetch>" },
             //  { "MeasureType", "ESP" },
             //  { "DisableRecalculation", false},
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethodRecalculateMeasure()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "RecalculateMeasure" + ", " + "DDSM.SmartMeasureCalculationPlugin";

            //Target            new for test b8f80961-bb16-e611-80c7-323166616637   /old ebcab351-f711-e611-80c6-323166616637
            Entity targetEntity = new Entity { LogicalName = "ddsm_measure", Id = Guid.Parse("6241453D-9C22-E611-80CA-323166616637") };


            //Input parameters
            var inputs = new Dictionary<string, object>
            {
               { "Measure", new EntityReference("ddsm-measure",Guid.Parse("6241453D-9C22-E611-80CA-323166616637")) },
               { "MeasureType", "ESP" },
                { "DisableRecalculation", false},
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethodDebugGetAllMadInfo()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "GetAllMadInfo" + ", " + "DDSM.SmartMeasureCalculationPlugin";

            //Target            
            Entity targetEntity = new Entity { LogicalName = "ddsm_admindata", Id = Guid.Parse("{F56C3086-9AF2-E411-80EC-FC15B4284D68}") };


            //Input parameters
            var inputs = new Dictionary<string, object>
            {
               { "ForceUpdate", true},
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethodGroupCalculateMeasure()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "GroupCalculateMeasure" + ", " + "DDSM.SmartMeasureCalculationPlugin";
            //Target            
            Entity targetEntity = new Entity { LogicalName = "ddsm_taskqueue", Id = Guid.Parse("{FE9D3885-E655-E711-80EB-363538646466}") };
            //Input parameters
            var inputs = new Dictionary<string, object>
            {
               {"FromMeasureGrid", true }, //qty =5
               // { "Project", new EntityReference{ Id = new Guid("1EDBAAFC-993F-E711-81AA-080027D22A46") , LogicalName = "ddsm_project"} }
                //{"UserInput","{'SmartMeasures':['8e6aa9e5-fb3e-e711-81aa-080027d22a46','246ba9e5-fb3e-e711-81aa-080027d22a46','c06aa9e5-fb3e-e711-81aa-080027d22a46','f26aa9e5-fb3e-e711-81aa-080027d22a46','5c6aa9e5-fb3e-e711-81aa-080027d22a46'],'DataFields':null}" }
              //{"UserInput","{'SmartMeasures':['9ca6d501-c429-e711-8100-623535653264','73a6d501-c429-e711-8100-623535653264','eea6d501-c429-e711-8100-623535653264','c5a6d501-c429-e711-8100-623535653264','c163ddfb-c329-e711-8100-623535653264','9863ddfb-c329-e711-8100-623535653264','1c64ddfb-c329-e711-8100-623535653264','eb63ddfb-c329-e711-8100-623535653264','40a7d501-c429-e711-8100-623535653264','17a7d501-c429-e711-8100-623535653264','8e1ce5f5-c329-e711-8100-623535653264','651ce5f5-c329-e711-8100-623535653264','e01ce5f5-c329-e711-8100-623535653264','b71ce5f5-c329-e711-8100-623535653264','d0ddecef-c329-e711-8100-623535653264','a7ddecef-c329-e711-8100-623535653264','22deecef-c329-e711-8100-623535653264','f9ddecef-c329-e711-8100-623535653264','321de5f5-c329-e711-8100-623535653264','091de5f5-c329-e711-8100-623535653264','cb62ddfb-c329-e711-8100-623535653264','a262ddfb-c329-e711-8100-623535653264','1d63ddfb-c329-e711-8100-623535653264','f462ddfb-c329-e711-8100-623535653264','2762ddfb-c329-e711-8100-623535653264','fe61ddfb-c329-e711-8100-623535653264','7962ddfb-c329-e711-8100-623535653264','5062ddfb-c329-e711-8100-623535653264','6f63ddfb-c329-e711-8100-623535653264','4663ddfb-c329-e711-8100-623535653264','32dbecef-c329-e711-8100-623535653264','09dbecef-c329-e711-8100-623535653264','84dbecef-c329-e711-8100-623535653264','5bdbecef-c329-e711-8100-623535653264','86daecef-c329-e711-8100-623535653264','5ddaecef-c329-e711-8100-623535653264','d8daecef-c329-e711-8100-623535653264','afdaecef-c329-e711-8100-623535653264','d6dbecef-c329-e711-8100-623535653264','addbecef-c329-e711-8100-623535653264','d01fe5f5-c329-e711-8100-623535653264','a71fe5f5-c329-e711-8100-623535653264','3020e5f5-c329-e711-8100-623535653264','0420e5f5-c329-e711-8100-623535653264','2c1fe5f5-c329-e711-8100-623535653264','031fe5f5-c329-e711-8100-623535653264','7e1fe5f5-c329-e711-8100-623535653264','551fe5f5-c329-e711-8100-623535653264','8220e5f5-c329-e711-8100-623535653264','5920e5f5-c329-e711-8100-623535653264','ccdcecef-c329-e711-8100-623535653264','a3dcecef-c329-e711-8100-623535653264','1eddecef-c329-e711-8100-623535653264','f5dcecef-c329-e711-8100-623535653264','28dcecef-c329-e711-8100-623535653264','ffdbecef-c329-e711-8100-623535653264','7adcecef-c329-e711-8100-623535653264','51dcecef-c329-e711-8100-623535653264','40a8d501-c429-e711-8100-623535653264','17a8d501-c429-e711-8100-623535653264','92a8d501-c429-e711-8100-623535653264','69a8d501-c429-e711-8100-623535653264','92a7d501-c429-e711-8100-623535653264','7eddecef-c329-e711-8100-623535653264','69a7d501-c429-e711-8100-623535653264','47ddecef-c329-e711-8100-623535653264','eea7d501-c429-e711-8100-623535653264','bba7d501-c429-e711-8100-623535653264','e4a8d501-c429-e711-8100-623535653264','bba8d501-c429-e711-8100-623535653264','2461ddfb-c329-e711-8100-623535653264','fb60ddfb-c329-e711-8100-623535653264','7661ddfb-c329-e711-8100-623535653264','4d61ddfb-c329-e711-8100-623535653264','8060ddfb-c329-e711-8100-623535653264','5760ddfb-c329-e711-8100-623535653264','d260ddfb-c329-e711-8100-623535653264','a960ddfb-c329-e711-8100-623535653264','c961ddfb-c329-e711-8100-623535653264','9f61ddfb-c329-e711-8100-623535653264','90d9ecef-c329-e711-8100-623535653264','e2d9ecef-c329-e711-8100-623535653264','b9d9ecef-c329-e711-8100-623535653264','34daecef-c329-e711-8100-623535653264','0bdaecef-c329-e711-8100-623535653264','b1a9d501-c429-e711-8100-623535653264','0da9d501-c429-e711-8100-623535653264','36a9d501-c429-e711-8100-623535653264','5fa9d501-c429-e711-8100-623535653264','88a9d501-c429-e711-8100-623535653264','361ee5f5-c329-e711-8100-623535653264','0d1ee5f5-c329-e711-8100-623535653264','881ee5f5-c329-e711-8100-623535653264','5f1ee5f5-c329-e711-8100-623535653264','841de5f5-c329-e711-8100-623535653264','5b1de5f5-c329-e711-8100-623535653264','e41de5f5-c329-e711-8100-623535653264','ad1de5f5-c329-e711-8100-623535653264','da1ee5f5-c329-e711-8100-623535653264','b11ee5f5-c329-e711-8100-623535653264'],'DataFields':null}"},

            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestMethodCalculateMeasure()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "CalculateMeasure" + ", " + "DDSM.SmartMeasureCalculationPlugin";

            //Target            
            Entity targetEntity = new Entity { LogicalName = "ddsm_project", Id = Guid.Parse("804045D0-5117-E611-80C7-323166616637") };


            //Input parameters
            var inputs = new Dictionary<string, object>
            {
               { "UserInput", Convert.ToBase64String(Encoding.UTF8.GetBytes("{'accountid':'{17E05B49-E72A-E711-8141-666665393634}','parentsiteid':'{4B0E0780-E72A-E711-8141-666665393634}','tradeally':'{EB61900E-D825-E711-8141-666665393634}','projectId':'{1064CA83-A940-E711-80CA-326539636430}','projectName':'148238-GH-David Adams','projectstatus':'1-Initiated','phase':'1. Proposed','phasenumber':1,'initialphasedate':'05/30/2017','measureId':'{00000000-0000-0000-0000-000000000000}','measureTplId':'c387b311-ecc5-e611-80f8-666665393634','calculationType':'c387b311-ecc5-e611-80f8-666665393634','espSmartMeasureId':'5e5f50d8-9f87-49e3-a5b4-aae85ebbc5dd','dataFields':{'kWh Gross Savings at Meter per unit':'10443','Units':'3','kW Gross Savings at Meter per unit':'4','Incentive Payment-Gross Per Unit':'500','Line Loss Factor':'0.1','Net to Gross Ratio-Standard':'0.57','Project Type':962080000,'Primary Heating System ':962080013,'ddsm_purchasepricefinancing':'15000'}}"))},
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        /// <summary>
        /// Modify to mock CRM Organization Service actions
        /// </summary>
        /// <param name="serviceMock">The Organization Service to mock</param>
        /// <returns>Configured Organization Service</returns>
        private static Mock<IOrganizationService> TestMethod1Setup(Mock<IOrganizationService> serviceMock)
        {
            EntityCollection queryResult = new EntityCollection();
            ////Add created items to EntityCollection

            serviceMock.Setup(t =>
                t.RetrieveMultiple(It.IsAny<QueryExpression>()))
                .ReturnsInOrder(queryResult);

            return serviceMock;
        }

        /// <summary>
        /// Invokes the workflow.
        /// </summary>
        /// <param name="name">Namespace.Class, Assembly</param>
        /// <param name="target">The target entity</param>
        /// <param name="inputs">The workflow input parameters</param>
        /// <param name="configuredServiceMock">The function to configure the Organization Service</param>
        /// <returns>The workflow output parameters</returns>
        private static IDictionary<string, object> InvokeWorkflow(string name, ref Entity target, Dictionary<string, object> inputs,
            Func<Mock<IOrganizationService>, Mock<IOrganizationService>> configuredServiceMock)
        {
            var testClass = Activator.CreateInstance(Type.GetType(name)) as CodeActivity;

            var serviceMock = new Mock<IOrganizationService>();
            var factoryMock = new Mock<IOrganizationServiceFactory>();
            var tracingServiceMock = new Mock<ITracingService>();
            var workflowContextMock = new Mock<IWorkflowContext>();

            //Apply configured Organization Service Mock
            if (configuredServiceMock != null)
                serviceMock = configuredServiceMock(serviceMock);

            CrmConnection connection = CrmConnection.Parse("Url=https://dev4.ddsm.online/EfficiencyOneDDSM; Domain=crmserver; Username=Administrator; Password=oGpjM9iE; AuthType=AD; Timeout=01:00:00;");
            //  CrmConnection connection = CrmConnection.Parse("Url=http://fortisbc-demo1.ddsm.online/RLS; Username=Administrator; Password=ghjuhfvth;");
            //  CrmConnection connection = CrmConnection.Parse("Url=http://ddsm-build-e1.ddsm.online/RLS; Username=yuriyn; Password=AgAJfH6T4p");
            //CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.190/RLS; Username=Administrator; Password=ghjuhfvth; Timeout=00:10:00;");
            //CrmConnection connection = CrmConnection.Parse("Url=http://dev3.dynamicdsm.com/EfficiencyOneDDSM; Username=Administrator; Password=Rjnecz1219694");
            // CrmConnection connection = CrmConnection.Parse("Url=http://enbridge-demo1.ddsm.online/RLS; Username=Administrator; Password=Index139240!");
            // CrmConnection connection = CrmConnection.Parse("Url=http://dev3.dynamicdsm.com/STAGING; Username=Administrator; Password=Rjnecz1219693");
            //CrmConnection connection = CrmConnection.Parse("Url=http://192.168.1.144/RLS; Username=Administrator; Password=Index139240!; Timeout=00:10:00;");
            // CrmConnection connection = CrmConnection.Parse("Url=http://192.168.1.203/RLS; Username=Administrator; Password=Cjytxrf12041969; Timeout=00:10:00;");
            // CrmConnection connection = CrmConnection.Parse("Url=http://192.168.1.9/RLS/; Username=Administrator; Password=Ghjuhfvth89!");
            // CrmConnection connection = CrmConnection.Parse("Url=http://ddsm-build-e1.dynamicdsm.com/RLS; Username=yuriyn; Password=AgAJfH6T4p");
            // CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.190/RLS; Username=Administrator; Password=ghjuhfvth");
            //CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.181/RLSR; Username=Administrator; Password=Cjytxrf12041969; Timeout=00:10:00;");
            //  CrmConnection connection = CrmConnection.Parse("Url=http://e1-test4.dynamicdsm.com/EfficiencyOneDDSM; Username=Administrator; Password=Ghjuhfvth89!; Timeout=00:10:00;");
            // CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS; Username=Administrator; Password=Dark#zone#19; Timeout=00:10:00;");
            //   CrmConnection connection = CrmConnection.Parse("Url=https://jokerrogue.crm.dynamics.com; Domain=jokerrogue.crm.dynamics.com; Username=MyCompany@jokerrogue.onmicrosoft.com; Password=123456Aa_; Timeout=00:10:00;");
            //  CrmConnection connection = CrmConnection.Parse("Url=https://egddemo1.crm.dynamics.com/; Username=admin@EGDdemo1.onmicrosoft.com; Password=P@ssword1; Timeout=00:10:00;");
            //CrmConnection connection = CrmConnection.Parse("Url=http://e1-dev1.dynamicdsm.com/RLS/; Domain=e1-dev1.dynamicdsm.com; Username=dev2; Password=Development2pass!; Timeout=00:10:00;");
            //  CrmConnection connection = CrmConnection.Parse("Url=http://ddsm-build-e1.dynamicdsm.com/RLS/; Username=yuriyn; Password=AgAJfH6T4p; Timeout=00:10:00;");
            //CrmConnection connection = CrmConnection.Parse("Url=https://crmdev.efficiencyns.ca:443/EfficiencyOneCRMDev/; Domain=ensc; Username=agsadmin; Password=Winter2016; Timeout=00:10:00;");
            //   CrmConnection connection = CrmConnection.Parse("Url=https://dynamicdsmdemo1.crm.dynamics.com/; Username=yuri.nazarenko@accentgold.com; Password=dark#zone#19;");
            IOrganizationService service = new OrganizationService(connection);
            //  IOrganizationService service = serviceMock.Object;


            //Mock workflow Context
            var workflowUserId = Guid.NewGuid();
            var workflowCorrelationId = Guid.NewGuid();
            var workflowInitiatingUserId = Guid.NewGuid();

            //Workflow Context Mock
            workflowContextMock.Setup(t => t.InitiatingUserId).Returns(workflowInitiatingUserId);
            workflowContextMock.Setup(t => t.CorrelationId).Returns(workflowCorrelationId);
            workflowContextMock.Setup(t => t.UserId).Returns(workflowUserId);
            var workflowContext = workflowContextMock.Object;

            //Organization Service Factory Mock
            factoryMock.Setup(t => t.CreateOrganizationService(It.IsAny<Guid>())).Returns(service);
            var factory = factoryMock.Object;

            //Tracing Service - Content written appears in output
            tracingServiceMock.Setup(t => t.Trace(It.IsAny<string>(), It.IsAny<object[]>())).Callback<string, object[]>(MoqExtensions.WriteTrace);
            var tracingService = tracingServiceMock.Object;

            //Parameter Collection
            ParameterCollection inputParameters = new ParameterCollection { { "Target", target } };
            workflowContextMock.Setup(t => t.InputParameters).Returns(inputParameters);

            //Workflow Invoker
            var invoker = new WorkflowInvoker(testClass);
            invoker.Extensions.Add(() => tracingService);
            invoker.Extensions.Add(() => workflowContext);
            invoker.Extensions.Add(() => factory);

            return invoker.Invoke(inputs);
        }
    }
}
