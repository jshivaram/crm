

/// <reference path="../../abcore_/scripts/Xrm.Page.js" />


function load_dm() {
    document.getElementById("WebResource_docmgr").style.height = "100%";
}

function openNoteWindow(id) {
    var ctxt;
    if (typeof GetGlobalContext != "undefined") {
        ctxt = GetGlobalContext();
    }
    else {
        if (typeof Xrm != "undefined") {
            ctxt = Xrm.Page.context;
        }
    }

    var sUrl = ctxt.getClientUrl();
    if (sUrl.match(/\/$/)) {
        sUrl = sUrl.substring(0, sUrl.length - 1);
    }

    var c_url = sUrl + '/WebResources/abdm_/html/downloadfile.htm?id=' + id;
    var w = 425, h = 200;

    if (typeof (openStdWin) == "undefined") {
        return window.open(c_url, '', "width=" + w + ",height=" + h + "," + "resizable=1");
    } else {
        //return openStdWin(c_url, '', w, h, "resizable=1");
        Xrm.Utility.openWebResource('abdm_/html/downloadfile.htm?id=' + id, null, w, h);
    }
}
