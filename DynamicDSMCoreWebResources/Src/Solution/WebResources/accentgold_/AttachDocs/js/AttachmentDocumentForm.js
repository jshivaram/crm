/*Attachment Documents
 * Created 03/07/2017 by Sergey Dergunov
 * Updated 03/13/2017 by Sergey Dergunov
 * Version: 4.5.2.0
 */
 
 function customFilterToAttachDocs(){
 
 return;

    if(typeof DocumentsJs == "undefined" || typeof DocumentsJs.Attachments == "undefined" || typeof DocumentsJs.Attachments._getViewName != "function")
    {
        setTimeout(customFilterToAttachDocs,1000);
        return;
    }
    var attachDocsView = DocumentsJs.Attachments._getViewName();

return;

    var ctrl = Xrm.Page.getControl(attachDocsView);
    if(!ctrl){
        setTimeout(customFilterToAttachDocs,1000);
        return;
    }

    var isGridLoaded = null;
    try {isGridLoaded = ctrl.getEntityName();}
    catch(err)
    {
        return;
    }
    
    if (!!isGridLoaded)
    {
        if(!(typeof reqDocParentField[Xrm.Page.data.entity.getEntityName()] == "string" && !!reqDocParentField[Xrm.Page.data.entity.getEntityName()])) {
            return;
        }

        var GUIDvalue = Xrm.Page.data.entity.getId().replace(/[{}]/g,'');

        var FetchXml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false' >\
            <entity name='ddsm_attachmentsdocument' >\
            <attribute name='subject' />\
            <attribute name='description' />\
            <attribute name='createdon' />\
            <order attribute='createdon' descending='true' />\
            <filter type='or' >\
            <filter type='and' >\
            <condition attribute='regardingobjectid' operator='eq' value='" + GUIDvalue + "' />\
            <condition attribute='statecode' operator='eq' value='0' />\
            </filter>\
            <filter type='and' >\
            <condition entityname='ddsm_requireddocument' attribute='" + reqDocParentField[Xrm.Page.data.entity.getEntityName()] + "' operator='eq' value='" + GUIDvalue + "' />\
            <condition attribute='statecode' operator='eq' value='0' />\
            </filter>\
            </filter>\
            <link-entity name='ddsm_requireddocument' from='ddsm_requireddocumentid' to='regardingobjectid' link-type='outer' />\
            </entity>\
            </fetch>";

        var Grid = document.getElementById(attachDocsView);
        if (!!Grid.control) {
            Grid.control.SetParameter("fetchXml", FetchXml);
            Grid.control.refresh();
        } else {
            setTimeout(customFilterToRequiredAttachDocs, 500);
            return;
        }

    }

}
