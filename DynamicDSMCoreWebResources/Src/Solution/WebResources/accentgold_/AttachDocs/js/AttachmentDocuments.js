/*Attachment Documents
 * Created 03/07/2017 by Sergey Dergunov
 * Updated 03/29/2017 by Sergey Dergunov
 * Version: 4.5.4.0
 */
;window.Modernizr=function(a,b,c){function u(a){i.cssText=a}function v(a,b){return u(prefixes.join(a+";")+(b||""))}function w(a,b){return typeof a===b}function x(a,b){return!!~(""+a).indexOf(b)}function y(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:w(f,"function")?f.bind(d||b):f}return!1}var d="2.7.0",e={},f=b.documentElement,g="modernizr",h=b.createElement(g),i=h.style,j,k={}.toString,l={},m={},n={},o=[],p=o.slice,q,r=function(){function d(d,e){e=e||b.createElement(a[d]||"div"),d="on"+d;var f=d in e;return f||(e.setAttribute||(e=b.createElement("div")),e.setAttribute&&e.removeAttribute&&(e.setAttribute(d,""),f=w(e[d],"function"),w(e[d],"undefined")||(e[d]=c),e.removeAttribute(d))),e=null,f}var a={select:"input",change:"input",submit:"form",reset:"form",error:"img",load:"img",abort:"img"};return d}(),s={}.hasOwnProperty,t;!w(s,"undefined")&&!w(s.call,"undefined")?t=function(a,b){return s.call(a,b)}:t=function(a,b){return b in a&&w(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=p.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(p.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(p.call(arguments)))};return e}),l.draganddrop=function(){var a=b.createElement("div");return"draggable"in a||"ondragstart"in a&&"ondrop"in a};for(var z in l)t(l,z)&&(q=z.toLowerCase(),e[q]=l[z](),o.push((e[q]?"":"no-")+q));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)t(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof enableClasses!="undefined"&&enableClasses&&(f.className+=" "+(b?"":"no-")+a),e[a]=b}return e},u(""),h=j=null,e._version=d,e.hasEvent=r,e}(this,this.document),Modernizr.addTest("filereader",function(){return!!(window.File&&window.FileList&&window.FileReader)});

"use strict";
var baseUrl = "/api/data/v8.1/";
var DocumentsJs = window.DocumentsJs || {};
(function () {
    this.crmWindow = function () {
        return Xrm.Internal.isTurboForm() ? partent.window : window;
    };

    if(typeof DocumentsJs.crmWindow().initDocumentsJs != 'undefined' && typeof DocumentsJs.crmWindow().initDocumentsJs =='boolean' && DocumentsJs.crmWindow().initDocumentsJs) {
        return;
    }

    DocumentsJs.crmWindow().spinnerGlobal = null;
    DocumentsJs.crmWindow().initDocumentsJs = true;

    //Example:
    //Show: showSpinner(true) //window.top
    //showSpinner(true, null, "Text Message")
    //showSpinner(true, document.getElementsById("Element Id"), "Text Message") //for current element
    //Stop: showSpinner(false)
    this.showSpinner = function (show, target, msg) {
        if (show) {
            if (typeof DocumentsJs.crmWindow().CreaLab != "undefined" && typeof DocumentsJs.crmWindow().CreaLab.Spinner != "undefined" && typeof DocumentsJs.crmWindow().CreaLab.Spinner.spin == "function")
                DocumentsJs.crmWindow().spinnerGlobal = CreaLab.Spinner.spin(target || null, msg || "Loading data ...");
        }
        else {
            if (!!DocumentsJs.crmWindow().spinnerGlobal)
                DocumentsJs.crmWindow().spinnerGlobal.stop();
        }
    };
}).call(DocumentsJs);

DocumentsJs.ContentPanel = DocumentsJs.ContentPanel || {};
(function () {
    if(typeof DocumentsJs.crmWindow().initDocumentsJsContentPanel != 'undefined' && typeof DocumentsJs.crmWindow().initDocumentsJsContentPanel =='boolean' && DocumentsJs.crmWindow().initDocumentsJsContentPanel) {
        return;
    }

    DocumentsJs.crmWindow().initDocumentsJsContentPanel = true;

    this.onLoad = function (callback) {
        $(document).ready(function() {
            $($("#crmContentPanel iframe").contents()).ready(function() {
                console.log("content IFrame Loaded");
                if(!!callback && typeof callback != "undefined" && typeof callback == "function") {
                    callback();
                }
            });
        })
    };
}).call(DocumentsJs.ContentPanel);

DocumentsJs.Attachments = DocumentsJs.Attachments || {};
(function () {

    if (typeof DocumentsJs.crmWindow().initAttachments != 'undefined' && typeof DocumentsJs.crmWindow().initAttachments == 'boolean' && DocumentsJs.crmWindow().initAttachments) {
        return;
    }
    DocumentsJs.crmWindow().initAttachments = true;

    this.Settings = {};
    this.Settings["DragControlGlobal"] = true;
    this.Settings["AttachedEntitiesList"] = ["all"];
    this.Settings["IgnoredEntitiesList"] = ["none"];
    this.Settings["RequiredDocumentsEntitiesList"] = ["none"];
    this.Settings["ObjectName"] = "AttachmentDocument";
    this.Settings["ViewName"] = [this.Settings["ObjectName"] + "_View"];
    this.Settings["TabName"] = [this.Settings["ObjectName"] + "_Tab"];
    this.Settings["RenderTo"] = this.Settings["ViewName"] + "_d";
    this.Settings["FilesCount"] = 0;

    this._getViewName = function () {
        var viewName = null;
        /*
         for(var i = 0; i < (DocumentsJs.Attachments.Settings["ViewName"]).length; i++) {
         if(!!Xrm.Page.getControl(DocumentsJs.Attachments.Settings["ViewName"][i])) {
         viewName = DocumentsJs.Attachments.Settings["ViewName"][i];
         break;
         }
         }
         if(!viewName && !!Xrm.Page.getControl( DocumentsJs.Attachments.Settings["ObjectName"] + "_View")) {
         viewName = DocumentsJs.Attachments.Settings["ObjectName"] + "_View";
         }
         */

        for(var i = 0; i < (DocumentsJs.Attachments.Settings["ViewName"]).length; i++) {
            if(!!document.getElementById(DocumentsJs.Attachments.Settings["ViewName"][i])) {
                viewName = DocumentsJs.Attachments.Settings["ViewName"][i];
                break;
            }
        }
        if(!viewName && !!document.getElementById( DocumentsJs.Attachments.Settings["ObjectName"] + "_View")) {
            viewName = DocumentsJs.Attachments.Settings["ObjectName"] + "_View";
        }


        return viewName;
    },

        this._addImageButton = function (viewName) {
            var viewName  = viewName;
            var tabName = DocumentsJs.Attachments.Settings["ObjectName"] + "_Tab";

            if (Xrm.Page.data.entity.getEntityName() == "ddsm_attachmentdocument") {
                viewName = DocumentsJs.Attachments.Settings["ObjectName"] + "_View";
            }

            if(!viewName) {
                return;
            } else if (!Xrm.Page.getControl(viewName)) {
                setTimeout(function(){
                    DocumentsJs.Attachments._addImageButton(viewName);
                }, 250);
            } else {

                setTimeout(function(){
                    var parentContainer = $("#" + viewName + "_contextualButtonsContainer").parent();
                    $("#" + viewName + "_contextualButtonsContainer").hide();
                    $("#" + viewName + "_openAssociatedGridViewImageButton").hide();
                    $("#" + viewName + "_ccDiv .ms-crm-grid-databodycontainer-Ex table tbody tr [id^=gridBodyTable_delete]").css({'display': 'none'});

                    if (Xrm.Page.getAttribute("statecode").getValue() == 0) {
                        var btnContainer = '<div class="ms-crm-Grid-ContextualButtonsContainer" id="' + viewName + '_contextualButtonsContainer"><div class="ms-crm-contextButton"><a href="#" id="' + viewName + '_addImageButtonCustom" style="display: block; cursor: pointer;" class="ms-crm-ImageStrip-addButton" title="Add Attachment Documents record."><img src="/_imgs/imagestrips/transparent_spacer.gif?ver=-293175106" id="' + viewName + '_addImageButtonCustomImage" alt="Add Attachment Documents record." title="Add Attachment Documents record." class="ms-crm-add-button-icon"></a><input id="attachedFiles" type="file" multiple="multiple" name="file[]" style="display:none;visibility:hidden;" onchange="DocumentsJs.Attachments._preProcessFiles(this.files);" /></div></div>';
                        $(parentContainer).append(btnContainer);
                        $("#" + viewName + "_addImageButtonCustom").click(function () {
                            $('#attachedFiles').trigger('click');
                        });
                    }
                }, 250);

                Xrm.Page.getControl(viewName).addOnLoad(function(e){
                    setTimeout(function(){
                        $("#" + e._element.id + "_contextualButtonsContainer").hide();
                        $("#" + e._element.id + "_openAssociatedGridViewImageButton").hide();
                        $("#" + e._element.id + "_ccDiv .ms-crm-grid-databodycontainer-Ex table tbody tr [id^=gridBodyTable_delete]").css({'display': 'none'});
                    }, 250);
                });

                if(Xrm.Page.data.entity.getEntityName() == "ddsm_attachmentdocument") {

                    setTimeout(function(){
                        $("#All_" + viewName + "_contextualButtonsContainer").hide();
                        $("#All_" + viewName + "_openAssociatedGridViewImageButton").hide();
                        $("#All_" + viewName + "_ccDiv .ms-crm-grid-databodycontainer-Ex table tbody tr [id^=gridBodyTable_delete]").css({'display': 'none'});
                    }, 250);
/*
                    Xrm.Page.getControl("All_" + viewName).addOnLoad(function(e){
                        setTimeout(function(){
                            $("#" + e._element.id + "_contextualButtonsContainer").hide();
                            $("#" + e._element.id + "_openAssociatedGridViewImageButton").hide();
                            $("#" + e._element.id + "_ccDiv .ms-crm-grid-databodycontainer-Ex table tbody tr [id^=gridBodyTable_delete]").css({'display': 'none'});
                        }, 250);
                    });
*/
                    //Create link to download file
                    DocumentsJs.Attachments._getFileId(function (id) {
                        DocumentsJs.Attachments._createLinkToFile(id);
                    });

                }

            }

        },

        this._initDragAndDrop = function() {
            var viewName = DocumentsJs.Attachments._getViewName();

            if (window.FileReader && Modernizr.draganddrop /* && Xrm.Page.getAttribute("statecode").getValue() == 0 */) {
                var n = null;
                if (DocumentsJs.Attachments.Settings["DragControlGlobal"]) {
                    n = document.getElementsByTagName("body")[0];
                } else if(!!viewName) {
                    n = document.getElementById(viewName + "_d");
                } else {
                    n = document.getElementsByTagName("body")[0];
                }
                n.addEventListener("drop", DocumentsJs.Attachments._handleDrop, !1);
                n.addEventListener("dragover", DocumentsJs.Attachments._handleDragOver, !1)
            } else console.log("Drag and drop api not supported");

            DocumentsJs.Attachments._addImageButton(viewName);

        },

        this.Init = function () {
            console.dir(">>> DocumentsJs.Attachments.Init");

            if (!!Xrm && !!Xrm.Page && !!Xrm.Page.data && !!Xrm.Page.data.entity) {
                if (Xrm.Page.ui.getFormType() == 1) {
                    return;
                }

                DocumentsJs.Attachments._loadSettings(function(){
                    if(!DocumentsJs.Attachments._enableRuleFeature())
                        return;
                    DocumentsJs.ContentPanel.onLoad(function(){
                        DocumentsJs.Attachments._initDragAndDrop();
                    });

                });

            }
        },

        this._create = function (entitySetName, entity, successCallback, errorCallback) {
            var req = new XMLHttpRequest();
            req.open("POST", encodeURI(getWebAPIPath() + entitySetName), true);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.onreadystatechange = function () {
                if (this.readyState == 4) {
                    req.onreadystatechange = null;
                    if (this.status == 204) {
                        if (successCallback)
                            successCallback(this.getResponseHeader("OData-EntityId"));
                    }
                    else {
                        if (errorCallback)
                            errorCallback(DocumentsJs.Attachments._errorHandler(this.response));
                    }
                }
            };
            req.send(JSON.stringify(entity));
        },

        this._update = function (_type, url, entity, callback) {
            var req = new XMLHttpRequest();
            req.open(_type, encodeURI(url), true);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.onreadystatechange = function () {
                if (this.readyState == 4) {
                    req.onreadystatechange = null;
                    if (this.status == 204) {
                        if (callback)
                            callback();
                    }
                    else {
                        console.log(DocumentsJs.Attachments._errorHandler(this.response));
                        if (callback)
                            callback();
                    }
                }
            };

            req.send(JSON.stringify(entity));

        },

        this._getFileId = function (successCallback, errorCallback) {
            var req = new XMLHttpRequest();
            req.open("GET", encodeURI(getWebAPIPath() + "annotations?$select=annotationid&$filter=_objectid_value eq " + (Xrm.Page.data.entity.getId()).replace(/\{|\}/g, '') + " and objecttypecode eq 'ddsm_attachmentdocument'"), true);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            req.setRequestHeader("Prefer", "odata.maxpagesize=1");
            req.onreadystatechange = function () {
                if (this.readyState == 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (this.status == 200) {
                        var results = JSON.parse(this.response);
                        //console.dir(results);
                        var fileId = null;
                        if (results.value.length == 1)
                            fileId = results.value[0].annotationid;
                        if (successCallback)
                            successCallback(fileId);
                    }
                    else {
                        if (errorCallback)
                            errorCallback(DocumentsJs.Attachments._errorHandler(this.response));
                    }
                }
            };
            req.send();
        },

        this._getAllPrevious = function (successCallback, errorCallback) {
            var req = new XMLHttpRequest();
            req.open("GET", encodeURI(getWebAPIPath() + "ddsm_attachmentdocuments?$select=ddsm_attachmentdocumentid&$filter=_ddsm_parent_value eq " + ((Xrm.Page.data.entity.getId()).replace(/\{|\}/g, '')).toUpperCase()), false);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
            req.onreadystatechange = function () {
                if (this.readyState == 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (this.status == 200) {
                        var results = JSON.parse(this.response);
                        if (successCallback)
                            successCallback(results.value);
                    }
                    else {
                        if (errorCallback)
                            errorCallback(DocumentsJs.Attachments._errorHandler(this.response));
                    }
                }
            };
            req.send();
        },

        this._createLinkToFile = function (id) {

            if (!!Xrm.Page.getAttribute("ddsm_downloadlink")) {
                console.log(">>> fn _createLinkToFile");
                var noteUrl = getClientUrl() + "/notes/edit.aspx?id=" + id;
                var notePage = null;
                var req = {};
                req.type = "GET";
                req.url = noteUrl;
                req.async = false;
                req.dataType = "html";
                req.success = function (data, textStatus, XmlHttpRequest) {
                    notePage = data;
                    console.dir(notePage);
                };
                $.ajax(req);
                if (!!notePage) {
                    var tage = 'span';
                    var regexp = new RegExp('<span[^>]+WRPCTokenUrl[^>]+>');
                    var SpanTages = notePage.match(regexp);
                    if (!!SpanTages) {
                        var TokenTage = SpanTages[0] + '</span>';
                        var div = $('<div/>').html(TokenTage);
                        var WRPCToken = $(tage, div).attr('WRPCTokenUrl');
                        if (!!WRPCToken) {
                            //console.dir(WRPCToken);
                            var fileUrl = getClientUrl() + "/Activities/Attachment/download.aspx?AttachmentType=5&AttachmentId=" + id + WRPCToken;
                            $("div#ddsm_downloadlink span.ms-crm-Inline-LockIcon").css({"left": "0"});
                            $("div#ddsm_downloadlink span.ms-crm-Inline-LockIcon img.ms-crm-ImageStrip-inlineedit_locked").css({"background": "transparent url('/_imgs/ribbon/Attachment_16.png') no-repeat scroll -3px -0px", "width": "16px", "height": "16px"});
                            $("div#ddsm_downloadlink div.ms-crm-Inline-Value span").css({"display": "block"});
                            $("div#ddsm_downloadlink div.ms-crm-Inline-Value span").html('<a href="' + fileUrl + '" target="_blank" style="display:block;">' + 'Download file' + '</a><div class="ms-crm-Inline-GradientMask"></div>')
                        }
                    }
                }
            }
        },

        this._handleDragOver = function (n) {
            n.stopPropagation();
            n.preventDefault()
        },

        this._handleDrop = function (n) {
            n.stopPropagation();
            n.preventDefault();
            var t = Xrm.Page.data.entity.getId();
            if (typeof t == "undefined" || t == null || t.length < 36) {
                alert("The file was unable to be uploaded. Please save the file and reload the page.");
                return;
            }
            DocumentsJs.Attachments._preProcessFiles(n.dataTransfer.files)
        },

        this._preProcessFiles = function (n) {

            if (Xrm.Page.ui.getFormType() != 2) {
                Alert.show(null, "Record Read only or inactive", [{
                    label: "Ok",
                    callback: function () {
                        return;
                    }
                }], "WARNING", 500, 200);
            } else {

                var n = n;
                var entityReferenceCollection = [], entityReference = {};
                entityReference.id = Xrm.Page.data.entity.getId();
                entityReference.typename = Xrm.Page.data.entity.getEntityName();
                entityReference.type = Xrm.Internal.getEntityCode(Xrm.Page.data.entity.getEntityName());
                entityReferenceCollection.push(entityReference);

                //Verify Required Documents
                var entityName = (Xrm.Page.data.entity.getEntityName()).toLowerCase();
                var isRequiredDocumentsEntity = false;
                if((DocumentsJs.Attachments.Settings["RequiredDocumentsEntitiesList"]).length == 1 && DocumentsJs.Attachments.Settings["RequiredDocumentsEntitiesList"][0] == "none") {
                    isRequiredDocumentsEntity = false;
                } else if(!!((DocumentsJs.Attachments.Settings["RequiredDocumentsEntitiesList"]).indexOf(entityName) + 1)) {
                    isRequiredDocumentsEntity = true;
                }

                if(isRequiredDocumentsEntity && typeof GetRequiredDocumentList == "function") {
                    var reqDocList = GetRequiredDocumentList();
                    if(reqDocList.length == 0) {
                        isRequiredDocumentsEntity = false;
                    }
                } else {
                    isRequiredDocumentsEntity = false;
                }

                if(isRequiredDocumentsEntity) {

                    if(typeof SelectedRequiredDocument == "function") {
                        SelectedRequiredDocument(n);
                    } else {
                        console.log("function SelectedRequiredDocument not found!");
                        DocumentsJs.Attachments._processFiles(n, entityReferenceCollection);
                    }

                } else {
                    DocumentsJs.Attachments._processFiles(n, entityReferenceCollection);
                }
            }
        },

        this._processFiles = function (n, entityRef) {
            //console.dir(entityRef);
            DocumentsJs.showSpinner(true, null, "Uploading files ...")
            if (n && n.length) {
                DocumentsJs.Attachments.Settings["FilesCount"] = n.length;
                for (var t = 0; t < n.length; t++) DocumentsJs.Attachments._uploadSingle(n[t], entityRef);
            }
        },

        this._uploadSingle = function (n, entityRef) {

            var t = new FileReader;
            t.onloadstart = function () {

            };
            t.onload = function (t) {

                var u = t.target.result, s = u.indexOf("base64,") + 7;

                var newAttachmentDocument = {}, newAnnotation = {};
                if (Xrm.Page.data.entity.getEntityName() != "ddsm_attachmentdocument") {
                    newAttachmentDocument.ddsm_name = n.name;
                    //newAttachmentDocument["regardingobjectid_" + entityRef.typename + "@odata.bind"] = "/" + entityRef.typename + "s(" + (entityRef.id).replace(/\{|\}/g, '') + ")";

                    newAttachmentDocument["ddsm_attachmentdocument_Annotations"] = [];
                    newAnnotation.isdocument = !0;
                    newAnnotation.filename = n.name;
                    newAnnotation.documentbody = u.toString().substring(s);
                    newAttachmentDocument["ddsm_attachmentdocument_Annotations"].push(newAnnotation);

                } else if (Xrm.Page.data.entity.getEntityName() == "ddsm_attachmentdocument") {
                    newAttachmentDocument.ddsm_name = (!!Xrm.Page.getAttribute("ddsm_name") && !!Xrm.Page.getAttribute("ddsm_name").getValue()) ? Xrm.Page.getAttribute("ddsm_name").getValue() : n.name;
                    /*
                    if (!!Xrm.Page.getAttribute("regardingobjectid") && !!Xrm.Page.getAttribute("regardingobjectid").getValue() && !!Xrm.Page.getAttribute("regardingobjectid").getValue()[0].id) {
                        newAttachmentDocument["regardingobjectid_" + Xrm.Page.getAttribute("regardingobjectid").getValue()[0].entityType + "@odata.bind"] = "/" + Xrm.Page.data.entity.getEntityName() + "s(" + (Xrm.Page.getAttribute("regardingobjectid").getValue()[0].id).replace(/\{|\}/g, '') + ")";
                    }
                    */
                    newAttachmentDocument.ddsm_description = (!!Xrm.Page.getAttribute("ddsm_description") && !!Xrm.Page.getAttribute("ddsm_description").getValue()) ? "1-" + Xrm.Page.getAttribute("ddsm_description").getValue() : "";

                    newAttachmentDocument["ddsm_attachmentdocument_Annotations"] = [];
                    newAnnotation.isdocument = !0;
                    newAnnotation.filename = n.name;
                    newAnnotation.documentbody = u.toString().substring(s);
                    newAttachmentDocument["ddsm_attachmentdocument_Annotations"].push(newAnnotation);

                } else {
                    console.log("fn _uploadSingle >>> Error");
                    DocumentsJs.Attachments.Settings["FilesCount"]--;
                    if (DocumentsJs.Attachments.Settings["FilesCount"] == 0) {
                        setTimeout(function(){
                            DocumentsJs.showSpinner(false);
                            Xrm.Page.getControl(DocumentsJs.Attachments._getViewName()).refresh();
                        }, 500);
                    }
                }

                var attachmentDocumentsId, updateAttachmentDocument = {}, urlActual = "", urlParent = "";
                //console.dir(newAttachmentDocument);
                DocumentsJs.Attachments._create("ddsm_attachmentdocuments"
                    , newAttachmentDocument
                    , function (oData) {
                        attachmentDocumentsId = oData.split(/[()]/);
                        attachmentDocumentsId = attachmentDocumentsId[1];
                        console.dir(attachmentDocumentsId);

                        //New Version
                        if (Xrm.Page.data.entity.getEntityName() == "ddsm_attachmentdocument") {

                            updateAttachmentDocument["@odata.id"] = getWebAPIPath() + Xrm.Page.data.entity.getEntityName() + "s(" + ((Xrm.Page.data.entity.getId()).replace(/\{|\}/g, '')).toUpperCase() + ")";
                            //Update Parent filed
                            urlParent = getWebAPIPath() + Xrm.Page.data.entity.getEntityName() + "s(" + ((attachmentDocumentsId).replace(/\{|\}/g, '')).toUpperCase() + ")/ddsm_ddsm_attachmentdocument_ddsm_attachmentdocument_Parent/$ref";
                            //console.log(urlParent);
                            DocumentsJs.Attachments._update("PUT", urlParent, updateAttachmentDocument, function(){

                                //Update Actual field
                                /*
                                 urlActual = getWebAPIPath() + Xrm.Page.data.entity.getEntityName() + "s(" + ((attachmentDocumentsId).replace(/\{|\}/g, '')).toUpperCase() + ")/ddsm_attachmentsdoc_ddsm_attachmentsdoc_Actual/$ref";
                                 DocumentsJs.Attachments._update("PUT", urlActual, updateAttachmentDocument, function(){

                                 //Get Previous All Versions
                                 DocumentsJs.Attachments._getAllPrevious(function(data){
                                 console.log(">>> All Prev");
                                 console.dir(data);
                                 DocumentsJs.Attachments.Settings["FilesCount"] = data.length;
                                 for(let i = 0; i < data.length; i++) {
                                 updateAttachmentDocument = {};
                                 updateAttachmentDocument["@odata.id"] = getWebAPIPath() + Xrm.Page.data.entity.getEntityName() + "s(" + ((data[i]["activityid"]).replace(/\{|\}/g, '')).toUpperCase() + ")";
                                 urlActual = getWebAPIPath() + Xrm.Page.data.entity.getEntityName() + "s(" + ((attachmentDocumentsId).replace(/\{|\}/g, '')).toUpperCase() + ")/ddsm_attachmentsdoc_ddsm_attachmentsdoc_Actual/$ref";

                                 //Update Actual field
                                 DocumentsJs.Attachments._update("PUT", urlActual, updateAttachmentDocument, function(){

                                 DocumentsJs.Attachments.Settings["FilesCount"]--;
                                 if (DocumentsJs.Attachments.Settings["FilesCount"] == 0) {
                                 setTimeout(function(){
                                 DocumentsJs.showSpinner(false);
                                 Xrm.Page.getControl(DocumentsJs.Attachments._getViewName()).refresh();
                                 }, 500);
                                 }

                                 });
                                 }

                                 });

                                 });
                                 */
                                setTimeout(function(){
                                    Xrm.Utility.openEntityForm(Xrm.Page.data.entity.getEntityName(), attachmentDocumentsId);
                                }, 500);
                            });

                        } else {

                            if(entityRef.length == 1) {
                                if(entityRef[0].typename == "ddsm_requireddocument" && Xrm.Page.data.entity.getEntityName() =="ddsm_requireddocument" ){
                                    if(!!Xrm.Page.getAttribute("ddsm_project") && !!Xrm.Page.getAttribute("ddsm_project").getValue()[0].id && Xrm.Page.getAttribute("ddsm_project").getValue()[0].id != "") {
                                        var entityReference = {};
                                        entityReference.id = Xrm.Page.getAttribute("ddsm_project").getValue()[0].id;
                                        entityReference.typename = Xrm.Page.getAttribute("ddsm_project").getValue()[0].typename;
                                        entityReference.type = Xrm.Page.getAttribute("ddsm_project").getValue()[0].type;
                                        entityRef.push(entityReference);
                                    }
                                }
                            }

                            if(entityRef.length > 0) {

                                for(var i = 0; i < entityRef.length; i++){
                                    var lookupItems = {};
                                    lookupItems.items = [];
                                    lookupItems.items.push(entityRef[i]);
                                    AssociateObjects(Xrm.Internal.getEntityCode("ddsm_attachmentdocument"), attachmentDocumentsId, lookupItems.items[0].type, lookupItems, true, "", "ddsm_attachdoc_" + lookupItems.items[0].typename);

                                    //Update Required Document records
                                    if(entityRef[i].typename == "ddsm_requireddocument") {
                                        var updateReqDoc = {};
                                        updateReqDoc["ddsm_uploaded"] = true;
                                        var urlReqDoc = getWebAPIPath() + entityRef[i].typename + "s(" + ((entityRef[i].id).replace(/\{|\}/g, '')).toUpperCase() + ")";
                                        DocumentsJs.Attachments._update("PATCH", urlReqDoc, updateReqDoc, null);
                                    }
                                }
                            }

                            DocumentsJs.Attachments.Settings["FilesCount"]--;
                            if (DocumentsJs.Attachments.Settings["FilesCount"] == 0) {
                                setTimeout(function(){
                                    DocumentsJs.showSpinner(false);
                                    Xrm.Page.getControl(DocumentsJs.Attachments._getViewName()).refresh();
                                }, 500);
                            }
                        }

                    }
                    , function (msg) {
                        DocumentsJs.Attachments.Settings["FilesCount"]--;
                        if (DocumentsJs.Attachments.Settings["FilesCount"] == 0) {
                            setTimeout(function(){
                                DocumentsJs.showSpinner(false);
                                Xrm.Page.getControl(DocumentsJs.Attachments._getViewName()).refresh();
                            }, 500);
                        }
                    });
            };

            t.onerror = function (n) {
                alert("File could not be read! Code " + n.target.error.code);
                DocumentsJs.Attachments.Settings["FilesCount"]--;
                if (DocumentsJs.Attachments.Settings["FilesCount"] == 0) {
                    setTimeout(function(){
                        DocumentsJs.showSpinner(false);
                        Xrm.Page.getControl(DocumentsJs.Attachments._getViewName()).refresh();
                        if(Xrm.Page.data.entity.getEntityName() =="ddsm_requireddocument") {
                            Xrm.Page.data.refresh();
                        }
                    }, 500);
                }
            };
            t.readAsDataURL(n);
        },

        this._errorHandler = function (resp) {
            try {
                return JSON.parse(resp).error;
            } catch (e) {
                return new Error("Unexpected Error")
            }
        },

        this._loadSettings = function(callback) {
            Process.callAction("ddsm_AttachmentDocumentSettings", [/* {key: "Target",type: Process.Type.EntityReference,value: {id: Xrm.Page.data.entity.getId(),entityType: Xrm.Page.data.entity.getEntityName()}} */],
                function(params) {
                    //var rs = JSON.parse(params[0].value);
                    var rs = params;
                    console.dir(rs);
                    for(let i = 0; i < rs.length; i++)
                    {
                        let key = rs[i].key;
                        let val = rs[i].value;
                        if(DocumentsJs.Attachments.Settings.hasOwnProperty(key) && !!val) {
                            if(key == "DragControlGlobal") {
                                DocumentsJs.Attachments.Settings[key] = val;
                            } else {
                                DocumentsJs.Attachments.Settings[key] = [];
                                var valArr = val.split(",");
                                for(var j = 0; j < valArr.length; j++) {
                                    if(!!valArr[j]) {
                                        (DocumentsJs.Attachments.Settings[key]).push(valArr[j]);
                                    }
                                }
                            }
                        }
                    }
                    if((DocumentsJs.Attachments.Settings["AttachedEntitiesList"]).length == 1 && DocumentsJs.Attachments.Settings["AttachedEntitiesList"][0] == "none") {
                        DocumentsJs.Attachments.Settings["AttachedEntitiesList"][0] = "all";
                    } else if((DocumentsJs.Attachments.Settings["AttachedEntitiesList"]).length > 1) {
                        (DocumentsJs.Attachments.Settings["AttachedEntitiesList"]).push("ddsm_attachmentdocument");
                    }

                    if (callback)
                        callback();
                },
                function(e) {
                    console.log("error: " + e);
                    if (callback)
                        callback();
                }
            );
        },

        this._enableRuleFeature = function() {
            var entityName = (Xrm.Page.data.entity.getEntityName()).toLowerCase();
            if(entityName == "ddsm_attachmentdocument") {
                return true;
            }
            //Attached
            var attached = false;
            if((DocumentsJs.Attachments.Settings["AttachedEntitiesList"]).length == 1 && DocumentsJs.Attachments.Settings["AttachedEntitiesList"][0] == "all") {
                attached = true;
            } else if(!!((DocumentsJs.Attachments.Settings["AttachedEntitiesList"]).indexOf(entityName) + 1)) {
                attached = true;
            }
            //Ignored
            var ignored = true;
            if((DocumentsJs.Attachments.Settings["IgnoredEntitiesList"]).length == 1 && DocumentsJs.Attachments.Settings["IgnoredEntitiesList"][0] == "none") {
                ignored = false;
            } else if(!!((DocumentsJs.Attachments.Settings["IgnoredEntitiesList"]).indexOf(entityName) + 1)) {
                ignored = true;
            } else {
                ignored = false;
            }
            if(attached && !ignored) {
                return true;
            } else {
                return false;
            }

        },

        this.EnableRuleButton = function() {

            if (!!Xrm && !!Xrm.Page && !!Xrm.Page.data && !!Xrm.Page.data.entity) {
                if (Xrm.Page.ui.getFormType() == 1) {
                    return false;
                } else {
                    if(Xrm.Page.getAttribute("statecode").getValue() == 0) {
                        return DocumentsJs.Attachments._enableRuleFeature();
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }

        },
        this.NewVersion = function() {
            $('#attachedFiles').trigger('click');
        };

    function getClientUrl() {
        if (typeof GetGlobalContext == "function" &&
            typeof GetGlobalContext().getClientUrl == "function") {
            return GetGlobalContext().getClientUrl();
        }
        else {
            if (typeof Xrm != "undefined" &&
                typeof Xrm.Page != "undefined" &&
                typeof Xrm.Page.context != "undefined" &&
                typeof Xrm.Page.context.getClientUrl == "function") {
                try {
                    return Xrm.Page.context.getClientUrl();
                } catch (e) {
                    throw new Error("Xrm.Page.context.getClientUrl is not available.");
                }
            }
            else {
                throw new Error("Context is not available.");
            }
        }
    }

    function getWebAPIPath() {
        return getClientUrl() + baseUrl;
    }

    function refreshView() {
        if (!!Xrm.Page.ui.controls.getByName(DocumentsJs.Attachments.Settings["ViewName"])) {
            Xrm.Page.ui.controls.getByName(DocumentsJs.Attachments.Settings["ViewName"]).refresh();
        }
    }


    this.Init();


}).call(DocumentsJs.Attachments);


function DummyAttachments() {
    console.log(">>> DocumentsJs.Attachments Loaded");
}

/*

 <fetch count="50" >
 <entity name="ddsm_attachmentdocument" >
 <attribute name="ddsm_name" />
 <attribute name="createdon" />
 <attribute name="ddsm_description" />
 <filter type="or" >
 <filter type="and" >
 <condition attribute="regardingobjectid" operator="eq" value="A375E0C6-BB04-E711-810A-0800276CDAC1" />
 <condition attribute="statecode" operator="eq" value="0" />
 </filter>
 <filter type="and" >
 <condition entityname="ddsm_requireddocument" attribute="ddsm_project" operator="eq" value="A375E0C6-BB04-E711-810A-0800276CDAC1" />
 <condition attribute="statecode" operator="eq" value="0" />
 </filter>
 </filter>
 <link-entity name="ddsm_requireddocument" from="ddsm_requireddocumentid" to="regardingobjectid" link-type="outer" />
 </entity>
 </fetch>

 */
