// JavaScript source code

//Discontinuation the project
function discontinueProject() {
	try {
		Alert.showWebResource("accentgold_/DiscontinueProject/html/ddsm_reasonSelection.html", 320, 400, "Discontinue popup", [
			new Alert.Button("Discontinue", function () {

				if (!(Xrm.Page.getAttribute("ddsm_discontinuedreason") &&
					Xrm.Page.getAttribute("ddsm_discontinuetheproject") &&
					Xrm.Page.getAttribute("ddsm_discontinueddate") &&
					Xrm.Page.getAttribute("ddsm_discontinueddateentry") &&
					Xrm.Page.getAttribute("ddsm_enddate") &&
					Xrm.Page.getAttribute("ddsm_projectstatus"))) {

					console.error("Error in JS code Dsicontinue. Function: discontinueProject. Error: one of fields wasn't found");
					return;
				}

				Xrm.Page.ui.controls.forEach(function (control, index) {
					var controlName = control.getName();
					if (controlName === "ddsm_discontinuedreason" ||
						controlName === "ddsm_discontinuetheproject" ||
						controlName === "ddsm_discontinueddate" ||
						controlName === "ddsm_discontinueddateentry" ||
						controlName === "ddsm_enddate" ||
						controlName === "ddsm_projectstatus") {

						control.setDisabled(false);
					}
				});

				//Setting values
				Xrm.Page.getAttribute("ddsm_discontinuedreason").setValue(window.parent.selectedReason);
				Xrm.Page.getAttribute("ddsm_discontinuedcost").setValue(window.parent.parent.selectedCost);
				Xrm.Page.getAttribute("ddsm_discontinuedcomments").setValue(window.parent.parent.selectedComments);
				Xrm.Page.getAttribute("ddsm_discontinuetheproject").setValue(true);
				window.parent.parent.selectedComments = null;
				window.parent.parent.selectedCost = null;
				window.parent.parent.selectedReason = null;
				var now = new Date();
				Xrm.Page.getAttribute("ddsm_discontinueddate").setValue(now);
				Xrm.Page.getAttribute("ddsm_discontinueddateentry").setValue(now);
				Xrm.Page.getAttribute("ddsm_enddate").setValue(now);
				Xrm.Page.getAttribute("ddsm_projectstatus").setValue("Discontinued");

				//Saving changes
				Xrm.Page.data.save().then(
					function () {
						Xrm.Page.ui.refreshRibbon();
						LockFieldsOnDiscontinue();
					},
					function () {
						console.error("Error in JS code Dsicontinue. Function: discontinueProject. Error: Error when save.");
					});

			}, true),//Button 'Discontinue'
			new Alert.Button("Сancel")],
			null, null, 20);//Discontinue popup
	} catch (e) {
		console.error("Error in JS code Dsicontinue. Function: discontinueProject. Error: " + e);
	}
}

//Locking fields
function LockFieldsOnDiscontinue() {
	try {
		//If one of fields that filled by function 'discontinueProject' is null, fields won't locked
		if (Xrm.Page.getAttribute("ddsm_discontinuetheproject") === null ||
			!Xrm.Page.getAttribute("ddsm_discontinuetheproject").getValue() ||
			Xrm.Page.getAttribute("ddsm_discontinuedreason") === null ||
			Xrm.Page.getAttribute("ddsm_discontinuedreason").getValue() === null) {
			return;
		}

		//Geting all controls and locking them
		Xrm.Page.ui.controls.forEach(function (control, index) {
			var controlType = control.getControlType();
			var controlName = control.getName();
			if (controlType !== "iframe" &&
				controlType !== "webresource" &&
				controlType !== "subgrid" &&
				controlType !== "kbsearch" &&
				controlName !== "ddsm_discontinuedcomments" &&
				controlName !== "ddsm_discontinueddate" &&
				controlName !== "ddsm_discontinuedcost") {

				control.setDisabled(true);
			}
		});

		$(".ms-crm-grid-titlecontainer").hide();
		$("#appGridQueryFilterContainer_SmartMeasure_view").hide();
		$("#appGridQueryFilterContainer_Measure_ImplementationSite").hide();

		//Hiding the buttons 'next stage' and 'previous stage'
		$("#processStagesContainer").attr('style', "min-width: 100%; width: 100%;");
		$("#stageBackActionContainer").hide();
		$("#stageAdvanceActionContainer").hide();
		$("#stageSetActiveActionContainer").hide();
		$(".processStageContainer").click(function (e) {
			e.preventDefault();
			$("#stageBackActionContainer").hide();
			$("#stageAdvanceActionContainer").hide();
			$("#stageSetActiveActionContainer").hide();
		});

	} catch (e) {
		console.error("Error in JS code Dsicontinue. Function: LockFieldsOnDiscontinue. Error: " + e);
	}
}

//Verification of sufficient security roles for ribbon
function UserHavePermisions() {
	try {
		var reply;
		window.$.ajax({
			async: false,
			url: Xrm.Page.context.getClientUrl() + "/WebResources/mag_/js/process.js",
			dataType: "script",
			cache: true
		});

		if (!Xrm.Page.getAttribute("ddsm_completedmilestone")) {
			console.error("Error in JS code Dsicontinue. Method: UserHavePermisions. Field 'ddsm_completedmilestone' is null");
			return false;
		}

		if (Xrm.Page.getAttribute("ddsm_enddate") && Xrm.Page.getAttribute("ddsm_enddate").getValue() !== null) {
			return false;
		}

		if (!Xrm.Page.getAttribute("ddsm_discontinuetheproject")) {
			console.error("Error in JS code Dsicontinue. Method: UserHavePermisions. Field 'ddsm_discontinuetheproject' is null");
			return false;
		}

		//If is discontinued or closed return false
		if (Xrm.Page.getAttribute("ddsm_completedmilestone").getValue() === "Closed" ||
			Xrm.Page.getAttribute("ddsm_discontinuetheproject").getValue()) {
			return false;
		}

		//Getting current security roles
		var currentRols = Xrm.Page.context.getUserRoles();

		//Calling action that return hide button or not
		Process.callAction("ddsm_DDSMCheckRolesForDiscontinue",
			[{
				key: "CurrentRoles",
				type: Process.Type.String,
				value: JSON.stringify(currentRols)
			}],
			function (output) {
				reply = output[0].value;
			},
			function (error) {
				console.error("Error in JS code Dsicontinue. Method: UserHavePermisions. Error: " + error);
				reply = false;
			}, false);

		if (reply === "true") {
			return true;
		}
		else {
			return false;
		}
	} catch (e) {
		console.error("Error in JS code Dsicontinue. Function: UserHavePermisions. Error: " + e);
	}
}