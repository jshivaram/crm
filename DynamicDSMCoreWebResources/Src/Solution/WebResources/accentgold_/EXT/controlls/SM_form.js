/**
 * Smart Measure form manipulations
 *
 * Dependencies: ags.core.js; alert.js;
 *
 * Version: 0.1.05.20016
 */

//TODO:
//1. make all glopal parameters as properties of the only object
//2. separate by strict regions (comment region)


//region GENERAL PARAMETER DECLARATION

var
    globalContainerProps = {
        id: "smContainer",
        //styles: "position:absolute; z-index:999; opacity:0.5; filter:alpha(opacity=50); top:0; right:0; left:0; bottom:0; background:#000",
        //appendTo: top.document.body
    },
    smLibraryStore,
    adminData = {},
    smLibraryTemplatesData = {},
    deliveryAgentData = [],
    ESPMeasureLibraryId = 'undefined',
    smRequiredFields = [],
    smCurrentVersionData = {},
    smComboStoreSourecesCRM = {},
    objMappingSM,
    hide_columns = [],
    viewColumns = [],
    dataOk,
    results = [],
    SmartMeasureButton = false,
    loadData,
    _getODataEndpoint,
    entitySchemaName,
    saveGridStatus,
    updateProjectStatus,
    lookupActive,
    newRecordRow = false,
    get_saveGridStatus,
    get_dataOkgrid,
    refreshGrid,
    reloadGrid,
    idGrid,
    rowEditing,
    entityName_getLookupData,
    fnclTpl_ID = null,
    EnableBtnFincl = false,
    AutoMeasureFncl,
    getModelNumberStatus,
    arrayIds = [],
    Picklist_stores = {},
    MADFields = {},
    SelectableProgramOffering = null,
    resorceLoaded = false,
    smLibraryForm,
    SmartMeasureForm2,
    currentMeasureId = null,
    selMeasTplId = null,
    isESPMeasTpl = false,
    globalModelNumberFields = [],
    globalModelNumberCustomeMap = {}
    ;


var listOfIds = [], measTpl = {};
measTpl.Id = null;
measTpl.MAD = null;
measTpl.MSD = null;


//endregion

//region GENERAL HELPER METHODS

function LoadScript(url, type, callback) {

    url = AGS.getServerUrl() + url;

    // Adding the script tag to the head as suggested before
    var head = document.getElementsByTagName('head')[0];
    if (type == "js") {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;

        // Then bind the event to the callback function.
        // There are several events for cross browser compatibility.
        script.onreadystatechange = callback;
        script.onload = callback;

        // Fire the loading
        head.appendChild(script);
        console.log | (url);
    }

    if (type == "css") {
        var css = document.createElement('link');
        css.type = 'text/css';
        css.rel = 'stylesheet';
        css.href = url;

        css.onreadystatechange = callback;
        css.onload = callback;

        head.appendChild(css);
        console.log | (url);
    }
}

Base64 = (function () {
    // Private property
    var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

    // Private method for UTF-8 encoding

    function utf8Encode(string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    }

    // Public method for encoding
    return {
        encode: (typeof btoa == 'function') ? function (input) {
            return btoa(utf8Encode(input));
        } : function (input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;
            input = utf8Encode(input);
            while (i < input.length) {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output +
                    keyStr.charAt(enc1) + keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) + keyStr.charAt(enc4);
            }
            return output;
        }
    };
})();

//region DOM OPERATIONS

/**
 * Remove DOM element by ID
 * @param {string} el_id
 * @return {Boolean} True on success
 * TODO: move to AGS.core
 */
function removeHtmlEl(el_id) {
    if (!el_id || typeof(el_id) != 'string') {
        return false;
    }
    if (el_id[0] == '#') el_id = el_id.slice(1);
    var elem = document.getElementById(el_id);
    if (!elem) {
        return true;
    }
    elem.parentNode.removeChild(elem);
    return true;
}

/**
 * Create a DOM element with id, type and parent from the parameters
 * @param paramObj {object} with parameters:
 *      id {string} - dom element id (required)
 *      type {string} - type of an element
 *      appendTo  {string} - id of the parent element
 *      styles: {string} - inline styles for a new element
 * @returns {object} - new DOM element or null
 */
function createDOMElement(paramObj) {
    if (!paramObj) {
        console.log("DOM element is not created! No option is set. It needs at least ID to be set");
        return null;
    }

    if (typeof(paramObj) == 'string') {
        paramObj = {id: paramObj};
    }

    if (!paramObj.id) {
        console.log("DOM element is not created! Id is not specified");
        return null;
    }


    var appendObj;
    if (typeof(paramObj.appendTo) == 'object') {
        appendObj = paramObj.appendTo;
    } else if (typeof(paramObj.appendTo) == 'string') {
        appendObj = document.getElementById(paramObj.appendTo);
    } else {
        appendObj = document.body;
    }

    if (!appendObj) {
        console.log("DOM element is not created! Failed to get an object to be appended to");
        return null;
    }

    if (!paramObj.type) {
        paramObj.type = "div";
    }

    var newEl = document.createElement(paramObj.type);
    newEl.id = paramObj.id;

    if (paramObj.styles) {
        newEl.setAttribute('style', paramObj.styles);
    }

    appendObj.appendChild(newEl);

    return newEl;
}

//endregion


//region EXT.js OPERATIONS

function setExtDefaults() {

    /*
    Ext.Loader.setConfig({
        enabled: true,
        disableCaching: false,
        path: {'Ext': '../../WebResources/accentgold_/EditableGrid/js/'}
    });
    Ext.Loader.setPath('Ext.ux', '../../WebResources/accentgold_/EditableGrid/js/ux');
*/

    //----LOOKUP Widget field initialization
    Ext.define('Ext.form.LookupCombo', {
        extend: 'Ext.form.ComboBox',
        store: Ext.create('Ext.data.Store', {
            fields: ['value', 'display'],
            data: []
        }),
        alias: 'widget.lookup',
        queryMode: 'local',
        displayField: 'display',
        valueField: 'value',
        forceSelection: true,
        triggerCls: "x-form-search-trigger",
        listeners: {
            beforequery: function (qe) {
                let val = this.value;
                if (!val || !val.split('|')[0]) {
                    val = this.store.getData().items[0].data.value;
                    if (!val || !val.split('|')[0]) {
                        //Ext.Msg.alert("Error", "Can not identify the type of lookup. Please check the source data and retry to create a measure.");
                        Alert.show("Error","Can not identify the type of lookup. Please check the source data and retry to create a measure.", [{
                            label: "Ok",
                            callback: function () {
                            }
                        }], "ERROR", 500, 200);
                        return false;
                    }
                }


                let lookupParts = val.split('|');

                lkp_toUpdate = this.name;
                //CALL
                getSMLookupData(lookupParts[0].toLowerCase(), setNewLookupData);

                return false;
            }
        }
    });


    //----EXT requires
    Ext.require([
        'Ext.grid.*',
        'Ext.data.*',
        //'Ext.ux.grid.FiltersFeature',
        //'Ext.ux.grid.menu.ListMenu',
        'Ext.util.*',
        'Ext.state.*',
        'Ext.form.*',
        'Ext.slider.*',
        'Ext.tip.QuickTip',
        'widget.lookup',
        'Ext.selection.CheckboxModel'
    ]);
    window.extElements = {
        MADFields: {},
        smLibraryForm: null,
        SmartMeasureForm2: null,
        objMappingSM: {},
        adminData: {}
    };
}

//endregion

function initSMData(curMeasId) {

    if(!!smLibraryForm){
        return;
    }

    if(!!curMeasId){
        currentMeasureId = curMeasId;
    }
if(!!GlobalJs.SpinnerSettings["OnLoadShow"])
{
    showSpinner(true);
}
    // load webResources
    // CSS


    var smFormCss = function () {
        if(!!currentMeasureId){

            return;
        }
        LoadScript("/WebResources/accentgold_/EXT/controlls/SM_form.css", "css", createSMselectorForm);
    };

    setExtDefaults();
    getSmartMeasureLibrary();

    getDeliveryAgents();

    LoadScript("/WebResources/accentgold_/Resource/css/extall.css", "css", smFormCss);
}

function getDeliveryAgents()
{
    AGS.REST.retrieveMultipleRecords("Account", "$select=Name,AccountId&$filter=ddsm_RecordType/Value eq 962080002 and StateCode/Value eq 0", null, null, function (deliveryAgent) {
        deliveryAgentData = new Ext.data.SimpleStore({
            fields: ["value", "display"],
            data: []
        });

        deliveryAgentData.add({
            value: "00000000-0000-0000-0000-000000000000",
            display: "No Delivery Agent"
        });

        for (var j = 0; j < deliveryAgent.length; j++) {
            var obj = {
                value: deliveryAgent[j].AccountId,
                display: deliveryAgent[j].Name
            };
            deliveryAgentData.add(obj);
        }
        // deliveryAgentData[ESPMeasureLibraryId] = deliveryAgentData;
    }, false, []);
    // }
    /*
     if (ESPMeasureLibraryId == 'undefined') {
     smDeliveryAgent.getStore().loadData([]);
     smDeliveryAgent.reset();
     } else {
     smDeliveryAgent.getStore().loadData(deliveryAgentData);
     smDeliveryAgent.reset();
     }
     */
}


//Get Smart Measure Library
function getSmartMeasureLibrary() {
    sel = "$select=ddsm_name,ddsm_espmeasurelibraryId,ddsm_Organizationname,ddsm_ESPMeasureLibId,ddsm_ShowHideDeliveryAgent&$orderby=ddsm_name asc"+ "&$filter=statecode/Value eq 0";
    AGS.REST.retrieveMultipleRecords("ddsm_espmeasurelibrary", sel, null, null, function (smLibrary) {
        smLibraryStore = new Ext.data.SimpleStore({
            fields: ["value", "display", "OrganizationName", "ESPMeasureLibId", "ShowHideDeliveryAgent"],
            data: []
        });

        smLibraryStore.add({
            value: "00000000-0000-0000-0000-000000000000",
            display: "All",
            OrganizationName: "DDSM",
            ESPMeasureLibId: "00000000-0000-0000-0000-000000000000",
            ShowHideDeliveryAgent: false
        });
        //console.dir(smLibrary);
        for (var i = 0; i < smLibrary.length; i++) {
            smLibraryStore.add({
                value: smLibrary[i].ddsm_espmeasurelibraryId,
                display: smLibrary[i].ddsm_name,
                OrganizationName: smLibrary[i].ddsm_Organizationname,
                ESPMeasureLibId: smLibrary[i].ddsm_ESPMeasureLibId,
                ShowHideDeliveryAgent: smLibrary[i].ddsm_ShowHideDeliveryAgent
            });
        }
    }, false, []);

    //Get Object Mapping Smart Measures and default SM library
    sel = "$select=ddsm_ESPMapingData,ddsm_DefaultESPLibraryId,ddsm_AllMADFieldsarerequired"
        + "&$filter=ddsm_name eq 'Admin Data' and statecode/Value eq 0";
    AGS.REST.retrieveMultipleRecords("ddsm_admindata", sel, null, null, function (data) {

        if (!data || !data[0]) {
            return;
        }
        if (!!data[0].ddsm_DefaultESPLibraryId && data[0].ddsm_DefaultESPLibraryId.Id) {
            adminData.smLibDefault = data[0].ddsm_DefaultESPLibraryId.Id;
            adminData.RequiredAllFields = !!data[0].ddsm_AllMADFieldsarerequired;
        }
        //console.dir(adminData);
    }, false, null);
}

//Get Smart Measure Library
function getSmartMeasureMapping(smId) {
    //Get Object Mapping Smart Measures and default SM library

    //console.dir(smId);

    let sel = "ddsm_measurecalculationtype, ddsm_ddsm_mapping_ddsm_measuretemplate/ddsm_JSONData";
    let expand = "ddsm_ddsm_mapping_ddsm_measuretemplate";
    AGS.REST.retrieveRecord(smId, "ddsm_measuretemplate", sel, expand, function (data) {
        isESPMeasTpl = false;
        //console.dir(data);
        if(!!data && !!data.ddsm_measurecalculationtype && !!data.ddsm_measurecalculationtype.Name && !!(String(data.ddsm_measurecalculationtype.Name).match(/ESP/))){
            isESPMeasTpl = true;
        }
        if(!data || (!!data && !data.ddsm_ddsm_mapping_ddsm_measuretemplate) || (!!data && !!data.ddsm_ddsm_mapping_ddsm_measuretemplate && !data.ddsm_ddsm_mapping_ddsm_measuretemplate.ddsm_JSONData))
        {
            //Ext.Msg.alert('Mapping', 'ESP Fields are not mapped!');
/*
            Alert.show("Mapping","ESP Fields are not mapped!", [{
                label: "Ok",
                callback: function () {
                }
            }], "ERROR", 500, 200);
*/
            objMappingSM = {};
            return;
        }

        let madMappingJson = JSON.parse(data.ddsm_ddsm_mapping_ddsm_measuretemplate.ddsm_JSONData);
        //console.dir(madMappingJson);

        if (!!madMappingJson)
        {
            objMappingSM = {};
            for (let k in madMappingJson) {
                let arrayEntity = madMappingJson[k];
                //console.dir(arrayEntity);
                for(let i = 0; i < arrayEntity.length; i++ )
                {
                    if(!!arrayEntity[i].AttrLogicalName && !!arrayEntity[i].AttrDisplayName && !!arrayEntity[i].FieldDisplayName){
                        objMappingSM[(arrayEntity[i].FieldDisplayName).toLowerCase()] = {
                            AttrName: arrayEntity[i].AttrLogicalName,
                            DispName: arrayEntity[i].FieldDisplayName,
                            FieldType: arrayEntity[i].FieldType,
                            Entity: k
                        };
                    }
                }
            }
            //console.dir(objMappingSM);
        }

    }, null, false);

    /*
    sel = "$select=ddsm_ESPMapingData,ddsm_DefaultESPLibraryId,ddsm_AllMADFieldsarerequired"
        + "&$filter=ddsm_name eq 'Admin Data' and statecode/Value eq 0";
    AGS.REST.retrieveMultipleRecords("ddsm_admindata", sel, null, null, function (data) {

        if (!data || !data[0]) {
            return;
        }
        //objMappingSM = JSON.parse(data[0].ddsm_ESPMapingData);
        objMappingSM = {};
        let objMappingSM_1 = JSON.parse(data[0].ddsm_ESPMapingData);
        console.dir(objMappingSM_1);
        if (!!objMappingSM_1) {
            for (let k in objMappingSM_1) {
                objMappingSM[k.toLowerCase()] = objMappingSM_1[k];
            }
        }
    }, false, null);
    */
}



//Verify MeasTpl
function VerifyMeasTpl(measGuids) {
    return null;
}

/**
 * Initialize a Smart Measure creation
 */
function createSMselectorForm() {
  if(!Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()){
    //Ext.Msg.alert('Warning', 'Parent Site is empty. Please choose the Parent Site.');
      Alert.show("Warning","Parent Site is empty. Please choose the Parent Site.", [{
          label: "Ok",
          callback: function () {
          }
      }], "WARNING", 500, 200);

      showSpinner(false);
      return;
  }
//ESPMeasureLibraryId
    var defaultsmTemplateStore = new Ext.data.SimpleStore({
        fields: ["value", "display", "MADFields", "ddsm_ESPSmartMeasureID", "ddsm_ESPMeasureLibraryId"],
        data: []
    });

    var ShowCombo_smDA = function(isShow) {
        var isShow = isShow;
        if(!!Ext.getCmp("Combo_smDA") && !!Ext.getCmp("Combo_smDA").el) {
            Ext.getCmp("Combo_smDA").el.setVisible(isShow);
        } else {
            setTimeout(function(){
                ShowCombo_smDA(isShow);
            }, 250);
        }
    };

    var smLibraryCombo = Ext.create('Ext.form.field.ComboBox', {
        id: "Combo_smLib",
        fieldLabel: 'Measure&nbsp;Library',
        queryMode: 'local',
        displayField: 'display',
        valueField: 'value',
        store: smLibraryStore,
        editable: false,
        anchor: '100%',
        //labelWidth: 100,
        width: 200,
        listeners: {
            beforeRender: function () {
                //set Default SM Library
                if (!!adminData.smLibDefault && (this.store.findExact('value', adminData.smLibDefault) >= 0)) {
                    this.setValue(this.store.getAt(this.store.findExact('value', adminData.smLibDefault)));
                    this.fireEvent('select', this);
                }
            },
            select: function (combo, value) {
                smLibraryForm.mask("Loading data ...");
                Ext.getCmp("Combo_smDA").setValue(deliveryAgentData.getAt(deliveryAgentData.findExact('value', "00000000-0000-0000-0000-000000000000")));
                Ext.getCmp("Combo_smDA").fireEvent('select', Ext.getCmp("Combo_smDA"));
                if(!smLibraryStore.getAt(smLibraryStore.findExact('value', this.value)).data["ShowHideDeliveryAgent"]) {
                    ShowCombo_smDA(false);
                } else {
                    ShowCombo_smDA(true);
                }
            }
        }
    });


    var smDeliveryAgent = Ext.create('Ext.form.field.ComboBox', {
        id: "Combo_smDA",
        fieldLabel: 'Delivery&nbsp;Agent',
        queryMode: 'local',
        displayField: 'display',
        valueField: 'value',
        store: deliveryAgentData,
        editable: true,
        emptyText: "Is not Required",
        anchor: '100%',
        //labelWidth: 100,
        width: 200,
        listeners: {
            select: function (combo, value) {
                //debugger;
                smLibraryForm.mask("Loading data ...");
                var daId = combo.getValue();
                //console.log(combo.getValue());
                ESPMeasureLibraryId = smLibraryCombo.getValue();
                console.log("ESPMeasureLibraryId: " + ESPMeasureLibraryId);
                var selector = "$select=ddsm_name,ddsm_MSDFields,ddsm_MADFields,ddsm_measuretemplateId,ddsm_ESPSmartMeasureID,ddsm_DeliveryAgent,ddsm_ESPMeasureLibraryId,ddsm_CalculationTypeId";
                if(ESPMeasureLibraryId != "00000000-0000-0000-0000-000000000000")
                {
                    selector += "&$filter=ddsm_ESPMeasureLibraryId/Id eq guid'" + ESPMeasureLibraryId + "'";
                }
                //Delivery Agent filter
                if(daId != "00000000-0000-0000-0000-000000000000")
                {
                    if(selector.indexOf("&$filter=") == -1){
                        selector += "&$filter="
                    } else {
                        selector += " and "
                    }
                    selector += "ddsm_DeliveryAgent/Id eq guid'" + daId + "'";
                } else {
                    //selector += " and ddsm_DeliveryAgent/Id eq null";
                }
                //Program Offering filter
                if(!!Xrm.Page.getAttribute("ddsm_programoffering") && !!Xrm.Page.getAttribute("ddsm_programoffering").getValue()[0].id)
                {
                    if(selector.indexOf("&$filter=") == -1){
                        selector += "&$filter="
                    } else {
                        selector += " and "
                    }
                    selector += "ddsm_ProgramOffering/Id eq guid'" + Xrm.Page.getAttribute("ddsm_programoffering").getValue()[0].id +"'";
                }
                //Active record filter
                if(selector.indexOf("&$filter=") == -1){
                    selector += "&$filter="
                } else {
                    selector += " and "
                }
                selector += "statecode/Value eq 0";
                if ( ESPMeasureLibraryId != 'undefined') {
// /typeof smLibraryTemplatesData[ESPMeasureLibraryId] == 'undefined' &&
                    smTemplateCombo.getStore().loadData([]);
                    smTemplateCombo.reset();
                    smLibraryTemplatesData[ESPMeasureLibraryId]=[];

                    AGS.REST.retrieveMultipleRecords("ddsm_measuretemplate",selector , null, null, function (smTemplates) {
                        //console.dir(smTemplates);
                        var smTemplatesData = [];
                        for (var j = 0; j < smTemplates.length; j++) {
                            //debugger;
                            var obj = {
                                value: smTemplates[j].ddsm_measuretemplateId,
                                display: $.trim(smTemplates[j].ddsm_name),
                                //ddsm_MADFields: smTemplates[j].ddsm_MADFields,
                                ddsm_MADFields: smTemplates[j].ddsm_MADFields,
                                ddsm_MSDFields: smTemplates[j].ddsm_MSDFields,
                                ddsm_ESPSmartMeasureID: smTemplates[j].ddsm_ESPSmartMeasureID,
                                ddsm_ESPMeasureLibraryId: smTemplates[j].ddsm_ESPMeasureLibraryId,
                                ddsm_CalculationType: smTemplates[j].ddsm_CalculationTypeId.Name
                            };
                            smTemplatesData.push(obj);
                        }
                        smLibraryTemplatesData[ESPMeasureLibraryId] = smTemplatesData;
                    }, false, []);
                }

                if (ESPMeasureLibraryId == 'undefined') {
                    smTemplateCombo.getStore().loadData([]);
                    smTemplateCombo.reset();
                } else {
                    smTemplateCombo.getStore().loadData(smLibraryTemplatesData[ESPMeasureLibraryId]);
                    smTemplateCombo.reset();
                }

                smLibraryForm.unmask();
            },
            beforeRender: function () {
                //set Default value "Is not Required"
                this.setValue(this.store.getAt(this.store.findExact('value', "00000000-0000-0000-0000-000000000000")));
                this.fireEvent('select', this);
            },
            beforequery: function(record){
                record.query = new RegExp(record.query, 'i');
                record.forceAll = true;
            },
            blur: function(){
                var value = this.getRawValue();
                if(value == ''){
                    this.setValue(this.store.getAt(this.store.findExact('value', "00000000-0000-0000-0000-000000000000")));
                    this.fireEvent('select', this);
                }
            }
        }
    });

    var smTemplateCombo = Ext.create('Ext.form.field.ComboBox', {
        id: "Combo_smTpl",
        fieldLabel: 'Measure Name',
        queryMode: 'local',
        displayField: 'display',
        valueField: 'value',
        store: defaultsmTemplateStore,
        anchor: '100%',
        //labelWidth: 100,
        width: 200,
        editable: true,
        emptyText: " - ",
        listeners: {
            select: function (combo, value) {
                //console.log(combo.getValue());
            },
            beforequery: function(record){
                record.query = new RegExp(record.query, 'i');
                record.forceAll = true;
            }
        }
    });

    smLibraryForm = new Ext.window.Window({
        //width: 475,
        width: 400,
        title: 'Create New Measure',
        floating: true,
        closable: false,
        border: false,
        resizable: false,
        titleAlign: 'center',
        cls: 'form2css1',
        renderTo: Ext.getBody(),
        items: Ext.create('Ext.form.Panel', {
            items:[{
            layout: 'form'
            , defaults: {
                //columnWidth: 0.95
                columnWidth: 1
                , layout: 'form'
                , border: false
                , padding: 5
            }
            , items: [smLibraryCombo,smDeliveryAgent, smTemplateCombo]

        }]})
        ,
        buttons: [{
            text: 'Ok',
            margin: 3,
            handler: function () {
//                var formValues = this.up('form').getForm().getValues();
//                var formValues = this.up('form').getForm().getValues();
                var win = this.up('window'),
                    form = win.down('form');
                var formValues = form.getForm().getValues();

                console.dir(formValues);

                if (!formValues['Combo_smLib-inputEl']) {
                    //Ext.Msg.alert('Form population', 'There is no Measure Library selected!');
                    Alert.show("Form population","There is no Measure Library selected!", [{
                        label: "Ok",
                        callback: function () {
                        }
                    }], "WARNING", 500, 200);

                    return;
                }
                if (!formValues['Combo_smTpl-inputEl']) {
                    //Ext.Msg.alert('Form population', 'There is no Measure selected!');
                    Alert.show("Form population","There is no Measure selected!", [{
                        label: "Ok",
                        callback: function () {
                        }
                    }], "WARNING", 500, 200);

                    return;
                }

                getSmartMeasureMapping(formValues['Combo_smTpl-inputEl']);


                if(!!GlobalJs.SpinnerSettings["OnLoadShow"])
                {
                    showSpinner(true);
                }
                let measTplLst = smLibraryTemplatesData[formValues['Combo_smLib-inputEl']];
                let curMeasTpl;
                for (let i = 0; i < measTplLst.length; i++) {
                    let el = measTplLst[i];

                    if (el.value == formValues['Combo_smTpl-inputEl']) {
                        curMeasTpl = el;
                        break;
                    }
                }
                //try{
                if(!!curMeasTpl){
                    smCurrentVersionData.MAD = curMeasTpl.ddsm_MADFields;
                    smCurrentVersionData.CalculationID = curMeasTpl.ddsm_ESPSmartMeasureID;
                }
                smCurrentVersionData.MeasTplID = formValues['Combo_smTpl-inputEl'];
                smCurrentVersionData.EditSM = false;

                if(isESPMeasTpl && !smCurrentVersionData.MAD) {
                    showSpinner(false);
                    Alert.show("Mapping","ESP Fields are not mapped!", [{
                        label: "Ok",
                        callback: function () {
                        }
                    }], "ERROR", 500, 200);
                    return;
                }

                createSMForm(formValues['Combo_smLib-inputEl'], formValues['Combo_smTpl-inputEl']);
                smLibraryForm.close();

                //}catch(ex) {
                //   Ext.Msg.alert('Error', 'Some error ocure on form render ' ) ;
                //    console.error(ex);
                //}

            }
        }, {
            text: 'Cancel',
            margin: 3,
            handler: function () {
                SmartMeasureButton = false;
                smLibraryForm.close();

                clearFormObj();
            }
        }],
        listeners: {
            close: function (panel) {
            },
            resize: function () {
            },
            afterrender: function (cmp) {
            }
        }
    });

    Ext.EventManager.onWindowResize(function(w, h){
        if(!!smLibraryForm){
            smLibraryForm.center();
        }
    });

    smLibraryForm.show();

    showSpinner(false);
}

function createSMForm(smLibId, smTplId) {
    if (!objMappingSM) {
        //Ext.Msg.alert('Data Error', "There is no actual mapping for current MAD! \nPlease populate the mapping and try again.");
        Alert.show("Data Error","There is no actual mapping for current MAD! Please populate the mapping and try again.", [{
            label: "Ok",
            callback: function () {
            }
        }], "ERROR", 500, 200);

        return;
    }

    if (smCurrentVersionData.EditSM) {
        updateMADFieldsDataEdit();
        return;
    }
    console.log("SM Lib ID: " + smLibId + " | SM Tpl ID: " + smTplId);
    selMeasTplId = smTplId;
    let smLibraryTplData = smLibraryTemplatesData[smLibId], smTpl = {};

    for (let i = 0; i < smLibraryTplData.length; i++) {
        if (smLibraryTplData[i].value == smTplId) {
            smTpl = smLibraryTplData[i];
            break;
        }
    }

    ////Check for empty MAD
    //if (!smTpl.ddsm_MADFields) {
    //    Ext.Msg.alert('Form population',"There is no MAD data for current Measure Template! \nPlease load MAD data and retry to create a Smart Measure");
    //    return;
    //}

    //var MADFields = JSON.parse(smTpl.ddsm_MADFields);
    MADFields = JSON.parse(smCurrentVersionData.MAD);
    var MSDFields = JSON.parse(smTpl.ddsm_MSDFields);

    //updateMADFieldsData(smTpl,MADFields,MSDFields);
    updateMADFieldsData(smTpl, MSDFields);
}
//TODO:

function updateMADFieldsData(smTpl, MSDFields) {


    //function updateMADFieldsData(smTpl, MADFields, MSDFields){
    var allData = [], parentProjectFields = [], parentProjectData = {}, parentAccountFields = [],
        parentAccountData = {}, parentSiteFields = [], parentSiteData = {}, thisMeasureFields = [],
        thisMeasureData = {}, parentRateClassRefFields = [], parentRateClassRefData = {}, fundingSourceFields = [], modelNumberFields = [],
        fundingSourceData = {}, siteExpand = null, measExpand = null;

    if (!MSDFields) {
        MSDFields = {};
    }

    if (!!MSDFields["ddsm_recalculationgroup"]) {
        delete MSDFields["ddsm_recalculationgroup"];
    }

    //Add MSD mapping to objMappingSM
    for (var key in MSDFields) {

        //objMappingSM[key] = MSDFields[key];
        objMappingSM[key.toLowerCase()] = MSDFields[key];
    }

/*
    let customMap = AGS.Entity.getTargetMapping(null, null, ['ddsm_measuretemplate', 'ddsm_sourcecalculationtype', 'Account', 'ddsm_site', 'ddsm_project', 'ddsm_rateclassreference']);
    let customMapAdvanced = AGS.Entity.getTargetMapping(962080005, null, ['ddsm_site']);
    if (!!customMapAdvanced && customMapAdvanced.hasOwnProperty('ddsm_site')) {
        let advSiteField = customMapAdvanced['ddsm_site'];
        if (!customMap.hasOwnProperty('ddsm_site')) {
            customMap['ddsm_site'] = advSiteField;
        } else {
            for (let k in customMapAdvanced['ddsm_site']) {
                customMap['ddsm_site'][k] = customMapAdvanced['ddsm_site'][k];
            }
        }
    }
*/
    let customMap = AGS.Entity.getTargetNewMapping();
    let msdMap = AGS.Entity.getTargetNewMapping(962080003);

    //console.dir(customMap);
    //console.dir(msdMap);

    for (var key in objMappingSM) {
        var el = objMappingSM[key];

        switch (el.Entity) {

            case "ddsm_rateclassreference":
                if (parentRateClassRefFields.indexOf(el.AttrName) < 0) {
                    parentRateClassRefFields.push(el.AttrName);
                }
                break;
            case "ddsm_project":
                if (parentProjectFields.indexOf(el.AttrName) < 0) {
                    parentProjectFields.push(el.AttrName);
                }
                break;
            case "ddsm_site":
                if (parentSiteFields.indexOf(el.AttrName) < 0) {
                    parentSiteFields.push(el.AttrName);
                }
                break;
            case "ddsm_measure":
                if (thisMeasureFields.indexOf(el.AttrName) < 0) {
                    thisMeasureFields.push(el.AttrName);
                }
                break;
            case "Account":
                if (parentAccountFields.indexOf(el.AttrName) < 0) {
                    parentAccountFields.push(el.AttrName);
                }
                break;
            case "ddsm_sourcecalculationtype":
                if (fundingSourceFields.indexOf(el.AttrName) < 0) {
                    fundingSourceFields.push(el.AttrName);
                }
                break;
            case "ddsm_modelnumberapproval":
                if (modelNumberFields.indexOf(el.AttrName) < 0) {
                    modelNumberFields.push(el.AttrName);
                }
                break;
        }
    }

    for (var key in customMap){
        for (var key2 in customMap[key]){

            if (thisMeasureFields.indexOf(customMap[key][key2]) < 0) {
            } else {
                switch(key.toLowerCase())
                {
                    case "ddsm_sourcecalculationtype":
                        thisMeasureFields.splice( thisMeasureFields.indexOf(customMap[key][key2]), 1 );
                        fundingSourceFields.push(key2);
                        break;
                    case "ddsm_rateclassreference":
                        thisMeasureFields.splice( thisMeasureFields.indexOf(customMap[key][key2]), 1 );
                        parentRateClassRefFields.push(key2);
                        break;
                    case "ddsm_project":
                        thisMeasureFields.splice( thisMeasureFields.indexOf(customMap[key][key2]), 1 );
                        parentProjectFields.push(key2);
                        break;
                    case "ddsm_site":
                        thisMeasureFields.splice( thisMeasureFields.indexOf(customMap[key][key2]), 1 );
                        parentSiteFields.push(key2);
                        break;
                    case "account":
                        thisMeasureFields.splice( thisMeasureFields.indexOf(customMap[key][key2]), 1 );
                        parentAccountFields.push(key2);
                        break;
                    case "ddsm_modelnumberapproval":
                        thisMeasureFields.splice( thisMeasureFields.indexOf(customMap[key][key2]), 1 );
                        modelNumberFields.push(key2);
                        break;
                }
            }
        }
    }

    //Add MSD mapping fields data

    for (var k in MSDFields) {


        var el = MSDFields[k];

        for (var key in msdMap){
            for (var key2 in msdMap[key]){
                if(el.AttrName == msdMap[key][key2]) {
                    //console.log("MSD Entity: " + el.Entity + " | Entity: " + key + " | Source: " + key2 + " | Target: " + msdMap[key][key2]);
                    //Delete current
                    switch (el.Entity) {

                        case "ddsm_rateclassreference":
                            parentRateClassRefFields.splice( parentRateClassRefFields.indexOf(msdMap[key][key2]), 1 );
                            break;
                        case "ddsm_project":
                            parentProjectFields.splice( parentProjectFields.indexOf(msdMap[key][key2]), 1 );
                            break;
                        case "ddsm_site":
                            parentSiteFields.splice( parentSiteFields.indexOf(msdMap[key][key2]), 1 );
                            break;
                        case "ddsm_measure":
                            thisMeasureFields.splice( thisMeasureFields.indexOf(msdMap[key][key2]), 1 );
                            break;
                        case "Account":
                            parentAccountFields.splice( parentAccountFields.indexOf(msdMap[key][key2]), 1 );
                            break;
                        case "ddsm_sourcecalculationtype":
                            fundingSourceFields.splice( fundingSourceFields.indexOf(msdMap[key][key2]), 1 );
                            break;
                        case "ddsm_modelnumberapproval":
                            modelNumberFields.splice( modelNumberFields.indexOf(msdMap[key][key2]), 1 );
                            break;
                    }

                    //Add new mapping
                    switch(key.toLowerCase())
                    {
                        case "ddsm_sourcecalculationtype":
                            fundingSourceFields.push(key2);
                            break;
                        case "ddsm_rateclassreference":
                            parentRateClassRefFields.push(key2);
                            break;
                        case "ddsm_project":
                            parentProjectFields.push(key2);
                            break;
                        case "ddsm_measure":
                            thisMeasureFields.push(key2);
                            break;
                        case "ddsm_site":
                            parentSiteFields.push(key2);
                            break;
                        case "account":
                            parentAccountFields.push(key2);
                            break;
                        case "ddsm_modelnumberapproval":
                            modelNumberFields.push(key2);
                            break;
                    }
                }
            }
        }

    }
    globalModelNumberFields =[];
    globalModelNumberCustomeMap = {};
    globalModelNumberFields = modelNumberFields;
    if(customMap.hasOwnProperty('ddsm_modelnumberapproval'))
    {
        globalModelNumberCustomeMap  = customMap['ddsm_modelnumberapproval'];
    }
    //console.dir(globalModelNumberFields);
    //console.dir(globalModelNumberCustomeMap);

    if (!!parentRateClassRefFields.length) {
        //update rate class refferene fields with expand preffix
        siteExpand = "ddsm_ddsm_rateclassreference_ddsm_site";
    }

    if (!!fundingSourceFields.length) {
        //update rate class refferene fields with expand preffix
        measExpand = "ddsm_ddsm_sourcecalculationtype_ddsm_measure_FundingSourceCalculationType";
    }

    if (!!thisMeasureFields.length || !!measExpand) {

        if (customMap.hasOwnProperty('ddsm_measuretemplate') && !!Object.keys(customMap['ddsm_measuretemplate'])) {

            AGS.REST.retrieveRecord(smCurrentVersionData.MeasTplID, "ddsm_measuretemplate", Object.keys(customMap['ddsm_measuretemplate']).join(','), measExpand, function (data) {
                //console.dir(data);
                let measureTplMap = customMap['ddsm_measuretemplate'];
                let sourceCalcMap = customMap['ddsm_sourcecalculationtype'];

                for (let k in measureTplMap) {
                    if (!!data[k]) {
                        thisMeasureData[measureTplMap[k]] = data[k];
                    }
                }

                for (let k in sourceCalcMap) {
                    if (!!data[k]) {
                        thisMeasureData[sourceCalcMap[k]] = data[k];
                    }
                }

            }, null, false);
        }
    }

    if (!parentAccountFields) {
        parentAccountFields = ['Name'];
    }
    if (!!parentAccountFields.length) {
        AGS.REST.retrieveRecord(Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id, "Account", parentAccountFields.join(',') + ',' + Object.keys(customMap['account']).join(','), null, function (data) {
            //console.dir(data);
            parentAccountData = data;
        }, null, false);
    }


    if (!parentSiteFields) {
        parentSiteFields = ['ddsm_name'];
    }
    if (!!parentSiteFields.length || !!siteExpand) {
        AGS.REST.retrieveRecord(Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id, "ddsm_site", parentSiteFields.join(',') + ',' + Object.keys(customMap['ddsm_site']).join(','), siteExpand, function (data) {
            //console.dir(data);
            parentSiteData = data;
            if (!!data[siteExpand]) {
                for (var i = 0; i < parentRateClassRefFields.length; i++) {

                    var k = parentRateClassRefFields[i];
                    var el = data[siteExpand][k];
                    if (!!el) {
                        parentRateClassRefData[k] = el;
                    }

                }
                delete data[siteExpand];
            }
            else{


                let siteTplMap = customMap['ddsm_site'];
                for (let k in siteTplMap) {
                    if (!!parentSiteData[k]) {
                        thisMeasureData[siteTplMap[k]] = parentSiteData[k];
                    }
                }
            }

        }, null, false);
    }

    if (!parentProjectFields) {
        parentProjectFields = ['ddsm_name'];
    }
    if (!!parentProjectFields.length) {
        AGS.REST.retrieveRecord(Xrm.Page.data.entity.getId(), "ddsm_project", parentProjectFields.join(',') + ',' + Object.keys(customMap['ddsm_project']).join(','), null, function (data) {
            //console.dir(data);
            parentProjectData = data;
        }, null, false);
    }


    /*
     if(parentProjectFields.length > 0){
     AGS.REST.retrieveRecord(Xrm.Page.data.entity.getId(), "ddsm_measure", thisMeasureFields.join(','), null, function(data){
     thisMeasureData = data;
     }, null, false);
     }
     */

    allData = allData.concat(thisMeasureData, parentProjectData, parentAccountData, parentSiteData, parentRateClassRefData);
    for (var i = 0; i < allData.length; i++) {
        //console.dir(allData[i]);

        for (key in objMappingSM) {
            for (key2 in allData[i]) {
                //console.log(objMappingSM[key].AttrName +" | " + key2);

                if (!!objMappingSM[key].DispNameOver) {
                    objMappingSM[key].DispName = objMappingSM[key].DispNameOver;
                }

                if (objMappingSM[key].AttrName != key2) {
                    continue;
                }
                //console.dir(allData[i][key2]);
                if (objMappingSM[key].FieldType == "String" || objMappingSM[key].FieldType == "Integer" || objMappingSM[key].FieldType == "Decimal") {
                    objMappingSM[key].Value = allData[i][key2];
                } else if (objMappingSM[key].FieldType == "Money") {
                    objMappingSM[key].Value = allData[i][key2].Value || 0;
                } else if (objMappingSM[key].FieldType == "Picklist") {
                    objMappingSM[key].Value = allData[i][key2].Value;
                } else if (objMappingSM[key].FieldType == "Boolean") {
                    objMappingSM[key].Value = allData[i][key2];
                } else if (objMappingSM[key].FieldType == "Lookup") {
                    //objMappingSM[key].Value = allData[i][key2].Name;
                    //objMappingSM[key].Value = AGS.String.format("{0}|{1}|{2}",allData[i][key2].LogicalName, allData[i][key2].Id, allData[i][key2].Name);
                    objMappingSM[key].Value = AGS.String.format("{0}|{1}|{2}", objMappingSM[key].LookupEntity, allData[i][key2].Id || '', allData[i][key2].Name || '');
                }
            }
        }

    }

    var usedMSD = [], plists = [];

    if(Array.isArray(MADFields)) {

        for (var i = 0; i < MADFields.length; i++) {

            //var k = MADFields[i].Name;
            let k = MADFields[i].Name.toLowerCase();
            MADFields[i].IsMSD = false;

            //if(typeof objMappingSM[k] == 'undefined' || typeof objMappingSM[k].Value == 'undefined') {continue;}

            if (MSDFields.hasOwnProperty(k.toLowerCase())) {
                //MADFields[i].IsMSD = true;
                usedMSD.push(k);
            }

            if (typeof objMappingSM[k] == 'undefined') {
                continue;
            }

            MADFields[i].Entity = !!objMappingSM[k].Entity ? objMappingSM[k].Entity.toLowerCase() : '';

            if (typeof objMappingSM[k].Value == 'undefined') {
                objMappingSM[k].Value = '';
            }

            if (MSDFields.hasOwnProperty(objMappingSM[k].AttrName.toLowerCase()) && usedMSD.indexOf(objMappingSM[k].AttrName.toLowerCase()) < 0) {
                MADFields[i].IsMSD = true;
                usedMSD.push(objMappingSM[k].AttrName.toLowerCase());
            }

            if (objMappingSM[k].FieldType == "String") {
                MADFields[i].StringValue = !!objMappingSM[k].Value ? objMappingSM[k].Value.trim() : '';
                if (!MADFields[i].DisplayFormat) {
                    MADFields[i].DisplayFormat = 'TextboxStringValue';
                }
            }
            if (objMappingSM[k].FieldType == "Integer") {
                MADFields[i].DoubleValue = !!objMappingSM[k].Value ? objMappingSM[k].Value : 0;
                if (!MADFields[i].DisplayFormat) {
                    MADFields[i].DisplayFormat = 'Integer';
                }
            }
            if (objMappingSM[k].FieldType == "Decimal") {
                MADFields[i].DoubleValue = !!objMappingSM[k].Value ? objMappingSM[k].Value : 0;
                if (!MADFields[i].DisplayFormat) {
                    MADFields[i].DisplayFormat = 'TextboxDouble4Decimal';
                }
            }
            if (objMappingSM[k].FieldType == "Money") {
                MADFields[i].DoubleValue = !!objMappingSM[k].Value ? objMappingSM[k].Value : 0;
                if (!MADFields[i].DisplayFormat) {
                    MADFields[i].DisplayFormat = 'TextboxDoubleDollars2Decimal';
                }
            }
            if (objMappingSM[k].FieldType == "Picklist") {
                MADFields[i].ComboBoxDoubleValues = !!objMappingSM[k].Value ? objMappingSM[k].Value : '';
                MADFields[i].ComboBoxStringValues = !!objMappingSM[k].Value ? objMappingSM[k].Value : '';
                //if(!MADFields[i].DisplayFormat){ MADFields[i].DisplayFormat = 'Combobox';}
                MADFields[i].DisplayFormat = 'Combobox';
                MADFields[i].AttrName = objMappingSM[k].AttrName.toLowerCase();
                plists.push({
                    entity: objMappingSM[k].Entity,
                    attrName: objMappingSM[k].AttrName
                });
            }
        }

    } else {
        MADFields = [];
    }

    for (var key in MSDFields) {
        var el = MSDFields[key];
        if (usedMSD.indexOf(el.AttrName.toLowerCase()) >= 0) {
            continue;
        }

        var m_obj = {};

        m_obj.IsMSD = true;
        usedMSD.push(key);
        m_obj.IsReadOnly = el.IsReadOnly;
        m_obj.IsRequired = el.IsRequired;

        m_obj.Name = key;
        m_obj.Entity = !!objMappingSM[key].Entity ? objMappingSM[key].Entity.toLowerCase() : '';

        if (!!el.DispNameOver) {
            objMappingSM[key].DispName = el.DispNameOver;
        }

        switch (el.FieldType) {
            case("Boolean"):
                m_obj.BooleanValue = !!el.Value ? el.Value : false;
                m_obj.DisplayFormat = 'Boolean';
                break;

            case("Integer"):
                m_obj.DoubleValue = !!el.Value ? el.Value : 0;
                m_obj.DisplayFormat = 'Integer';
                break;
            case("Decimal"):
                let _displayFormatDecimal = (typeof el.Precision != 'undefined' && !!el.Precision) ? ("TextboxDouble"+el.Precision+"Decimal") : "TextboxDouble4Decimal";
                m_obj.DoubleValue = !!el.Value ? el.Value : 0;
                m_obj.DisplayFormat = _displayFormatDecimal;
                break;
            case("Money"):
                let _displayFormatMoney = (typeof el.Precision != 'undefined' && !!el.Precision) ? ("TextboxDoubleDollars"+el.Precision+"Decimal") : "TextboxDoubleDollars2Decimal";
                m_obj.DoubleValue = !!el.Value ? el.Value : 0;
                m_obj.DisplayFormat = _displayFormatMoney;
                break;
            case("Picklist"):
                m_obj.DisplayFormat = 'Combobox';
                //m_obj.ComboBoxStringValues = el.Value || '';
                m_obj.ComboBoxDoubleValues = !!el.Value ? el.Value : '';
                m_obj.AttrName = objMappingSM[key].AttrName.toLowerCase();
                plists.push({
                    entity: objMappingSM[key].Entity,
                    attrName: objMappingSM[key].AttrName
                });
                break;
            case("Lookup"):
                m_obj.DisplayFormat = 'Lookup';
                m_obj.ComboBoxStringValues = !!el.Value ? el.Value.trim() : AGS.String.format("{0}|{1}|{2}", objMappingSM[key].LookupEntity, '', '');
                m_obj.AttrName = objMappingSM[key].AttrName.toLowerCase();
                break;
            default:
                m_obj.StringValue = !!el.Value ? String(el.Value).trim() : '';
                m_obj.DisplayFormat = 'TextboxStringValue';
                break;
        }

        MADFields.push(m_obj);
    }

    buildSMPicklist(plists, smTpl);

}

function updateMADFieldsDataEdit() {
    var allData = [], parentProjectFields = [], parentProjectData = {}, parentAccountFields = [], parentAccountData = {}, parentSiteFields = [], parentSiteData = {}, thisMeasureFields = [], parentRateClassRefFields = [], parentRateClassRefData = {}, siteExpand = null, measExpand = null;

    var MADFields = JSON.parse(smCurrentVersionData.MAD);
    var MSDFields = JSON.parse(smCurrentVersionData.MeasTplData.ddsm_MSDFields);

    if (!MSDFields) {
        MSDFields = {};
    }

    //Add MSD mapping to objMappingSM
    for (let key in MSDFields) {
        objMappingSM[key] = MSDFields[key];
    }

    for (let key in objMappingSM) {
        let el = objMappingSM[key];

        switch (el.Entity) {

            case "ddsm_rateclassreference":
                if (parentRateClassRefFields.indexOf(el.AttrName) < 0) {
                    parentRateClassRefFields.push(el.AttrName);
                }
                break;
            case "ddsm_project":
                if (parentProjectFields.indexOf(el.AttrName) < 0) {
                    parentProjectFields.push(el.AttrName);
                }
                break;
            case "ddsm_site":
                if (parentSiteFields.indexOf(el.AttrName) < 0) {
                    parentSiteFields.push(el.AttrName);
                }
                break;
            case "Account":
                if (parentAccountFields.indexOf(el.AttrName) < 0) {
                    parentAccountFields.push(el.AttrName);
                }
                break;
            case "ddsm_measure":
                if (thisMeasureFields.indexOf(el.AttrName) < 0) {
                    thisMeasureFields.push(el.AttrName);
                }
                break;
        }
    }
    //if(!!parentRateClassRefFields.length)
    //{
    //    //update rate class refferene fields with expand preffix
    //    siteExpand = "ddsm_ddsm_rateclassreference_ddsm_site";
    //}

    //if(!!parentProjectFields.length){
    //    AGS.REST.retrieveRecord(b.Xrm.Page.data.entity.getId(), "ddsm_project", parentProjectFields.join(','), null, function(data){
    //        //console.dir(data);
    //        parentProjectData = data;
    //    }, null, false);
    //}


    //----------DEBUG-----------START<<<<<<<<<<<<<

    //----------DEBUG COMMENTED !!!!!! <<<<<<<<<<<<<<<<< !!!!!!!!!!!!!!!!!!! <<<<<<<<<<<<<<<<<<-----------START<<<<<<<<<<<<<
    /*------------
     //  debugger;
     var measureMap = {};

     let entityList = ['Account','ddsm_site','ddsm_project','ddsm_rateclassreference','ddsm_sourcecalculationtype'];

     var sel = "?$select=ddsm_PrimaryFieldLogicalName,ddsm_SecondaryFieldLogicalName,ddsm_SecondaryEntityLogicalName"
     + "&$filter= ddsm_PrimaryEntityLogicalName eq 'ddsm_measure' and ddsm_RelatedEntitiesGroup/Value eq 962080002"
     + " and (ddsm_SecondaryEntityLogicalName eq 'Account' or ddsm_SecondaryEntityLogicalName eq 'ddsm_site' or ddsm_SecondaryEntityLogicalName 'ddsm_project'"
     + " or ddsm_SecondaryEntityLogicalName eq 'ddsm_rateclassreference' or ddsm_SecondaryEntityLogicalName eq 'ddsm_sourcecalculationtype')";

     AGS.REST.retrieveMultipleRecords("ddsm_mappingfields", sel, function(data){

     for (let i = 0; i < data.length; i++) {
     let el = data[i];
     measureMap[el.ddsm_SecondaryFieldLogicalName] = el.ddsm_PrimaryFieldLogicalName;

     if (parentAccountFields.indexOf((el.ddsm_PrimaryFieldLogicalName) < 0)
     && (el.ddsm_PrimaryFieldLogicalName >= 0
     || parentAccountFields.indexOf(el.ddsm_PrimaryFieldLogicalName) >= 0
     || parentAccountFields.indexOf(el.ddsm_PrimaryFieldLogicalName) >= 0
     || parentAccountFields.indexOf(el.ddsm_PrimaryFieldLogicalName) >= 0 )) {

     thisMeasureFields.push(el.ddsm_PrimaryFieldLogicalName);
     }
     }
     }, null, null, false, null) ;
     --------------------------*/
    //----------DEBUG COMMENTED !!!!!! <<<<<<<<<<<<<<<<< !!!!!!!!!!!!!!!!!!! <<<<<<<<<<<<<<<<<<-------------END<<<<<<<<<<<<<

    //let customMap = AGS.Entity.getTargetMapping(null, null, ['Account','ddsm_site','ddsm_project','ddsm_rateclassreference','ddsm_sourcecalculationtype','ddsm_measuretemplate',]);

    let customMap = AGS.Entity.getTargetNewMapping();

    let fieldsObj = MADFields.concat(MSDFields);

    for(let i = 0; i< fieldsObj.length; i++){

        let mad_obj = fieldsObj[i];
        console.dir(mad_obj);
        let k = !!mad_obj.Name ? mad_obj.Name.toLowerCase() : '';
        console.log(k);
        if (!k) {
            continue;
        }

        for (let ent in customMap) {
            console.dir(ent);
            if(!ent.hasOwnProperty(k)){
                continue;
            }

            if(thisMeasureFields.indexOf(ent[k])>=0){
                thisMeasureFields.push(ent[k]);
            }
        }
    }


    //if(!!Object.keys(measureTplMap).length){
    //
    //    AGS.REST.retrieveRecord(smCurrentVersionData.MeasTplID, "ddsm_measuretemplate", Object.keys(measureTplMap).join(','), null, function(data){
    //        //console.dir(data);
    //        for (var k in measureTplMap) {
    //            if(!!data[k]) { thisMeasureData[measureTplMap[k]] = data[k]; }
    //        }
    //    }, null, false);
    //}
    //----------DEBUG-------------END<<<<<<<<<<<<<

    //(parentAccountFields.concat(parentRateClassRefFields,parentProjectFields, parentSiteFields)).forEach(function(el){
    //    if (thisMeasureFields.indexOf(el) < 0) {
    //        thisMeasureFields.push(el);
    //    }
    //
    //}) ;
    if (!!thisMeasureFields.length) {
        let selFields = [];

        //for (let i = 0; i < thisMeasureFields.length; i++) {
        //    if (!smCurrentVersionData.MeasureData.hasOwnProperty(thisMeasureFields[i])) {
        //        selFields.push(thisMeasureFields[i]);
        //    }
        //}

        selFields = thisMeasureFields;

        if (!!selFields.length) {
            AGS.REST.retrieveRecord(smCurrentVersionData.MeasID, "ddsm_measure", selFields.join(','), null, function (data) {
                //console.dir(data);
                for (let i = 0; i < selFields.length; i++) {
                    let k = selFields[i];
                    smCurrentVersionData.MeasureData[k] = (data.hasOwnProperty(k)) ? data[k] : null;
                }
            }, null, false);
        }
    }

    allData = smCurrentVersionData.MeasureData;
    //for(let i = 0; i < allData.length; i++){
    //console.dir(allData[i]);

    for (let key in objMappingSM) {
        for (let key2 in allData) {
            //console.log(objMappingSM[key].AttrName +" | " + key2);

            if (!!objMappingSM[key].DispNameOver) {
                objMappingSM[key].DispName = objMappingSM[key].DispNameOver;
            }

            if (objMappingSM[key].AttrName != key2) {
                continue;
            }

            ////console.dir(allData[i][key2]);
            //if (objMappingSM[key].FieldType == "String" || objMappingSM[key].FieldType == "Integer" || objMappingSM[key].FieldType == "Decimal" || objMappingSM[key].FieldType == "Boolean") {
            //    objMappingSM[key].Value = allData[key2];
            //} else if (objMappingSM[key].FieldType == "Money") {
            //    objMappingSM[key].Value = allData[key2].Value || 0;
            //} else if (objMappingSM[key].FieldType == "Picklist") {
            //    objMappingSM[key].Value = allData[key2].Value;
            //} else if (objMappingSM[key].FieldType == "Lookup") {
            //    objMappingSM[key].Value = allData[key2].Name;
            //}
            //console.dir(allData[i][key2]);
            if (objMappingSM[key].FieldType == "String" || objMappingSM[key].FieldType == "Integer" || objMappingSM[key].FieldType == "Decimal" || objMappingSM[key].FieldType == "Boolean") {
                objMappingSM[key].Value = allData[key2];
            } else if (objMappingSM[key].FieldType == "Money") {
                objMappingSM[key].Value = allData[key2] || 0;
            } else if (objMappingSM[key].FieldType == "Picklist") {
                objMappingSM[key].Value = allData[key2];
            } else if (objMappingSM[key].FieldType == "Lookup") {
                objMappingSM[key].Value = allData[key2].Name;
            }
        }
    }

    //}

    var usedMSD = [], plists = [];

    for (let i = 0; i < MADFields.length; i++) {

        let k = MADFields[i].Name;
        MADFields[i].IsMSD = false;

        //if(typeof objMappingSM[k] == 'undefined' || typeof objMappingSM[k].Value == 'undefined') {continue;}

        if (MSDFields.hasOwnProperty(k.toLowerCase())) {
            MADFields[i].IsMSD = true;
            usedMSD.push(k);
        }

        if (typeof objMappingSM[k] == 'undefined') {
            continue;
        }

        MADFields[i].Entity = !!objMappingSM[k].Entity ? objMappingSM[k].Entity.toLowerCase() : '';

        if (typeof objMappingSM[k].Value == 'undefined') {
            objMappingSM[k].Value = '';
        }

        if (MSDFields.hasOwnProperty(objMappingSM[k].AttrName.toLowerCase()) && usedMSD.indexOf(objMappingSM[k].AttrName.toLowerCase()) < 0) {
            MADFields[i].IsMSD = true;
            usedMSD.push(objMappingSM[k].AttrName.toLowerCase());
        }

        if (objMappingSM[k].FieldType == "Boolean") {
            MADFields[i].BooleanValue = !!objMappingSM[k].Value ? objMappingSM[k].Value : false;
            if (!MADFields[i].DisplayFormat) {
                MADFields[i].DisplayFormat = 'Boolean';
            }
        }

        if (objMappingSM[k].FieldType == "String" || objMappingSM[k].FieldType == "Lookup") {
            MADFields[i].StringValue = !!objMappingSM[k].Value ? objMappingSM[k].Value.trim() : '';
            if (!MADFields[i].DisplayFormat) {
                MADFields[i].DisplayFormat = 'TextboxStringValue';
            }
        }
        if (objMappingSM[k].FieldType == "Integer" || objMappingSM[k].FieldType == "Decimal" || objMappingSM[k].FieldType == "Money") {
            MADFields[i].DoubleValue = !!objMappingSM[k].Value ? objMappingSM[k].Value : 0;
            if (!MADFields[i].DisplayFormat) {
                MADFields[i].DisplayFormat = 'Decimal';
            }
        }
        if (objMappingSM[k].FieldType == "Picklist") {
            MADFields[i].ComboBoxDoubleValues = !!objMappingSM[k].Value ? objMappingSM[k].Value : '';
            MADFields[i].ComboBoxStringValues = !!objMappingSM[k].Value ? objMappingSM[k].Value : '';
            //if(!MADFields[i].DisplayFormat){ MADFields[i].DisplayFormat = 'Combobox';}
            MADFields[i].DisplayFormat = 'Combobox';
            MADFields[i].AttrName = objMappingSM[k].AttrName.toLowerCase();
            plists.push({
                entity: objMappingSM[k].Entity,
                attrName: objMappingSM[k].AttrName
            });
        }
    }

    for (let key in MSDFields) {
        let el = MSDFields[key];
        if (usedMSD.indexOf(el.AttrName.toLowerCase()) >= 0) {
            continue;
        }

        var m_obj = {};

        m_obj.IsMSD = true;
        usedMSD.push(key);

        m_obj.Name = key;
        m_obj.Entity = !!objMappingSM[key].Entity ? objMappingSM[key].Entity.toLowerCase() : '';

        if (!!el.DispNameOver) {
            objMappingSM[key].DispName = el.DispNameOver;
        }

        if (el.FieldType == "String") {
            m_obj.StringValue = !!el.Value ? el.Value.trim() : '';
            m_obj.DisplayFormat = 'TextboxStringValue';
        }
        if (el.FieldType == "Lookup") {
            m_obj.StringValue = !!el.Value ? el.Value.trim() : '';
            m_obj.DisplayFormat = 'Lookup';
        }
        if (el.FieldType == "Integer" || el.FieldType == "Decimal" || el.FieldType == "Money") {
            m_obj.DoubleValue = el.Value || 0;
            m_obj.DisplayFormat = 'Decimal';
        }
        if (el.FieldType == "Boolean") {
            m_obj.BooleanValue = !!el.Value ? el.Value : false;
            m_obj.DisplayFormat = 'BooleanValue';
        }
        if (el.FieldType == "Picklist") {
            m_obj.DisplayFormat = 'Combobox';
            m_obj.ComboBoxStringValues = el.Value || '';
            m_obj.AttrName = objMappingSM[key].AttrName.toLowerCase();
            plists.push({
                entity: objMappingSM[key].Entity,
                attrName: objMappingSM[key].AttrName
            });
        }

        MADFields.push(m_obj);
    }

    //return MADFields;
    //buildSMPicklist(plists,smCurrentVersionData.MeasTplData,MADFields);
    buildSMPicklist(plists, smCurrentVersionData.MeasTplData);
}

/**
 * Generate pick lists and initiate form generation if thea are ready
 * @param plists
 * @param smTpl
 * @param MADFields
 */
function buildSMPicklist(plists, smTpl) {

    var plist_count = plists.length || 0;

    if (!plists.length) {
        //generateSMForm(smTpl, MADFields);
        generateSMForm(smTpl);
        return;
    }

    for (var i = 0; i < plists.length; i++) {
        var obj = plists[i];

        if (!(obj.hasOwnProperty('entity') && !!obj.entity) || !(obj.hasOwnProperty('attrName') && obj.attrName)) {
            plist_count--;
            continue;
        }

        //inner method
        var _retrieveOptionSetSM = function (entName, attrName) {
            SDK.Metadata.RetrieveAttribute(entName, attrName, null, false,
                function (res) {
                    //console.dir(res);
                    //Success
                    plist_count--;
                    var key = AGS.String.format("{0}|{1}", entName, attrName);
                    smComboStoreSourecesCRM[key] = generateAttrComboStore(key, res.OptionSet.Options);
                    if (!plist_count) {
                        //generateSMForm(smTpl, MADFields, smComboStoreSourecesCRM);
                        generateSMForm(smTpl, smComboStoreSourecesCRM);
                    }
                },
                function (res) {
                    //Error
                    console.error('Failed to create a picklist store', res.message);
                    plist_count--;
                    if (!plist_count) {
                        //generateSMForm(smTpl, MADFields, smComboStoreSourecesCRM);
                        generateSMForm(smTpl, smComboStoreSourecesCRM);
                    }
                })
        };

        _retrieveOptionSetSM(obj.entity.toLowerCase(), obj.attrName.toLowerCase());
    }
}

function generateSMForm(smTpl) {

    Ext.tip.QuickTipManager.init();

    var arrayFields, countFieldsColumn = 5, itemsColumns = [];

    allowCreation = true;

    arrayFields = getSMfields(MADFields);

    var j = 1, tmpArr = [],
            itemsCol = {
            //defaults: {anchor:'100%'},
            //layout: 'form',
            items: []
        };
    for (var i = 0; i < arrayFields.length; i++) {

        if (!arrayFields[i].allowBlank) {
            smRequiredFields.push(arrayFields[i].name);
            arrayFields[i].labelClsExtra = 'x-required';
        }

        if (i == countFieldsColumn * j) {
            itemsCol = {
                //defaults: {anchor:'100%'},
                //layout: 'form',
                items: []
            };
            itemsCol.items = tmpArr;
            itemsColumns.push(itemsCol);
            j++;
            tmpArr = [];
            tmpArr.push(arrayFields[i]);
        } else {
            tmpArr.push(arrayFields[i]);
        }
        if (i == arrayFields.length - 1 && tmpArr.length > 0) {
            itemsCol = {
                //defaults: {anchor:'100%'},
                //layout: 'form',
                items: []
            };
            itemsCol.items = tmpArr;
            itemsColumns.push(itemsCol);
        }
    }
    //console.dir(arrayFields);
    //console.dir(itemsColumns);
    //Create form sm fields
    SmartMeasureForm2 = new Ext.window.Window({
        id: "smForm1",
        //width: itemsColumns.length * 225,
        width: itemsColumns.length * 220 - 10,
        //minWidth: 600,
        //height: 265,
        title: smTpl.display,
        //floating: true,
        closable: false,
        border: false,
        //layout: 'anchor',
        //defaults: {anchor: '100%'},
        //bodyPadding: 5,
        //header: false,
        cls: 'form2css1',
        resizable: false,
        titleAlign: 'center',
        //scroll: 'both',
        //autoScroll: true,
        renderTo: Ext.getBody(),
        items: Ext.create('Ext.form.Panel', {
            //items: [{
            //width: itemsColumns.length * 160,
            layout: 'column'
            , defaults: {
                //columnWidth: parseFloat(1 / itemsColumns.length).toFixed(2) -0.05
                width: 200
                //, layout: 'form'
                , border: false
                ,xtype: 'panel'
                , padding: 10

                //,minHeight: 140
                //,width: '100%'
            }
            , items: itemsColumns
        //}]
        }),
        listeners: {
            close: function (panel) {
            },
            resize: function () {
            },
            afterrender: function (cmp) {
            }
        },
        buttons: [{
            text: 'Ok',
            margin: 3,
            //disabled: !allowCreation,
            handler: function () {
                //spinnerForm = CreaLab.Spinner.spin(document.getElementById("bodyGrid"), "Creating New Measure");

                //
                if(!!GlobalJs.SpinnerSettings["OnLoadShow"])
                {
                    showSpinner(true, null, "Creating New Measure");
                }


                var win = this.up('window'),
                    form = win.down('form');
                var formValues = form.getForm().getValues();

                console.dir(formValues);
                let createNewMeas = true;
                createNewMeas = VerifiedCreatingMeas(((formValues.hasOwnProperty('ddsm_purchasepricefinancing') && !!formValues['ddsm_purchasepricefinancing'])?parseFloat(formValues['ddsm_purchasepricefinancing']):null));

                if(!createNewMeas) {
                    //Ext.Msg.alert('Form population', "The amount in the Purchase Price-Financing field is greater than the Maximum Project Purchase Price-Financing for this Program Offering.<br/> In order to save this record, please update the amount to be equal to or less than the Project Purchase Price-Financing for this Program Offering.");
                    Alert.show("Form population","The amount in the Purchase Price-Financing field is greater than the Maximum Project Purchase Price-Financing for this Program Offering. In order to save this record, please update the amount to be equal to or less than the Project Purchase Price-Financing for this Program Offering.", [{
                        label: "Ok",
                        callback: function () {
                        }
                    }], "WARNING", 500, 200);

                    showSpinner(false);
                    return;
                }


                var _smCorrectPicklistValue = function (formKey, formValue) {
                    if (!formValue || !Picklist_stores[formKey]) {
                        return -1;
                    }
                    let store = Picklist_stores[formKey];
                    //return store.findExact('value', formValue);
                    return parseInt(formValue);
                };

                //validation
                var hasEmpty = false;
                var dataFields = {};
                for (var key in formValues) {

                    var picklist_i = -1;
                    var lookup_i = null;

                    //if(typeof(formValues[key])=='number'){

                    picklist_i = _smCorrectPicklistValue(key, formValues[key]);
                    //}

                    //lookup_i = _smCorrectLookupValue(key, formValues[key]);

                    if (smRequiredFields.indexOf(key) >= 0 && !formValues[key]) {
                        hasEmpty = true;
                        break;
                    }


                    var key_f = !!(key.match(/^ddsm_/ig)) ? key : key.replace(/_/ig, ' ');

                    if (picklist_i >= 0) {
                        dataFields[key_f] = picklist_i;
                    } else {
                        dataFields[key_f] = (!!formValues[key] || formValues[key] == 0) ? String(formValues[key]) : '';
                    }
                }

                if (hasEmpty) {
                    //Ext.Msg.alert('Form population', "All required fields must be populated!");
                    Alert.show("Form population","All required fields must be populated!", [{
                        label: "Ok",
                        callback: function () {
                        }
                    }], "WARNING", 500, 200);

                    showSpinner(false);
                    return;
                }

                if(!!GlobalJs.SpinnerSettings["OnLoadShow"])
                {
                    showSpinner(true, null, "Creating a New Measure");
                }


                //console.dir(formValues);
                //debugger;
                //console.dir(dataFields);
                //debugger;

                sendSMDataFields(
                    (!!Xrm.Page.getAttribute("ddsm_accountid") && !!Xrm.Page.getAttribute("ddsm_accountid").getValue()) ? (Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id) : "",
                    (!!Xrm.Page.getAttribute("ddsm_parentsiteid") && !!Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()) ? (Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id) : "",
                    (!!Xrm.Page.getAttribute("ddsm_parentsiteid") && !!Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()) ? (Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id) : "",
                    (!!Xrm.Page.getAttribute("ddsm_tradeally") && !!Xrm.Page.getAttribute("ddsm_tradeally").getValue()) ? (Xrm.Page.getAttribute("ddsm_tradeally").getValue()[0].id) : "",
                    Xrm.Page.data.entity.getId(),
                    Xrm.Page.getAttribute("ddsm_name").getValue(),
                    Xrm.Page.getAttribute("ddsm_projectstatus").getValue(),
                    Xrm.Page.getAttribute("ddsm_phase").getValue(),
                    Xrm.Page.getAttribute("ddsm_phasenumber").getValue(),
                    new Date(),
                    '{00000000-0000-0000-0000-000000000000}',
                    smTpl.value,
                    smTpl.ddsm_CalculationType,
                    //smTpl.ddsm_ESPSmartMeasureID,
                    smCurrentVersionData.CalculationID,
                    dataFields);
                SmartMeasureForm2.close();
                clearFormObj();

            }
        }, {
            text: 'Cancel',
            margin: 3,
            handler: function () {
                SmartMeasureForm2.close();
                clearFormObj();
            }
        }]
    });

    Ext.EventManager.onWindowResize(function(w, h){
        if(!!SmartMeasureForm2){
            SmartMeasureForm2.center();
        }
    });

    SmartMeasureForm2.show();
    showSpinner(false);
    if (!allowCreation) {
        //Ext.Msg.alert("Mapping", "ESP Fields are not mapped completely!");
        Alert.show("Mapping","ESP Fields are not mapped completely!", [{
            label: "Ok",
            callback: function () {
            }
        }], "WARNING", 500, 200);


    }
}

function getSMfields(fieldsOjb) {

    var res_a = [];
    nestedPairs = {};// global

    console.dir(fieldsOjb);

    for (var i = 0; i < fieldsOjb.length; i++) {

        //console.log(MADFields[i].DisplayFormat);
        //var isMapped = objMappingSM.hasOwnProperty(fieldsOjb[i].Name);
        var isMapped = objMappingSM.hasOwnProperty(fieldsOjb[i].Name.toLowerCase());

        if (!isMapped && !fieldsOjb[i].IsReadOnly) {
            allowCreation = false;
        }

        var Label_f = fieldsOjb[i].Name;
        var c_styles = "";

        //if(!!objMappingSM[MADFields[i].Name]){
        if (isMapped) {
            //Label_f = objMappingSM[fieldsOjb[i].Name].DispName || Label_f;
            Label_f = objMappingSM[fieldsOjb[i].Name.toLowerCase()].DispName || Label_f;
        } else {
            c_styles = "background:#ffb3b3";
        }

        if (fieldsOjb[i].IsReadOnly) {
            c_styles = "background:#ccc;";
        }

        if (!!((fieldsOjb[i].DisplayFormat).indexOf("Decimal") + 1)) {
            var precision = 0;
            if (!!((fieldsOjb[i].DisplayFormat).indexOf("Dollars") + 1)) {
                precision = parseInt((fieldsOjb[i].DisplayFormat).substring((fieldsOjb[i].DisplayFormat).indexOf("Dollars") + 7, (fieldsOjb[i].DisplayFormat).indexOf("Decimal")));
                let Decimal = {
                    xtype: 'numberfield',
                    name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                    fieldLabel: Label_f,
                    labelAlign: 'top',
                    width: '100%',
                    //allowBlank: false,
                    //allowBlank: fieldsOjb[i].IsReadOnly,
                    //allowBlank: fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired),
                    allowBlank: (fieldsOjb[i].IsMSD)?!fieldsOjb[i].IsRequired:(fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired)),
                    readOnly: fieldsOjb[i].IsReadOnly,
                    //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                    fieldStyle: c_styles,
                    step: 1,
                    decimalPrecision: precision,
                    value: (!!fieldsOjb[i].DoubleValue) ? parseFloat(fieldsOjb[i].DoubleValue) : null,
                    hidden: false,
                    listeners: {
                        afterrender: function(me) {
                            Ext.tip.QuickTipManager.register({
                                target: me.getId(),
                                title : '',
                                text  : me.fieldLabel
                            });

                        }
                    }
                };

                res_a.push(Decimal);
            } else if (!!((fieldsOjb[i].DisplayFormat).indexOf("Double") + 1)) {
                precision = parseInt((fieldsOjb[i].DisplayFormat).substring((fieldsOjb[i].DisplayFormat).indexOf("Double") + 6, (fieldsOjb[i].DisplayFormat).indexOf("Decimal")));
                let Dollars = {
                    xtype: 'numberfield',
                    name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                    fieldLabel: Label_f,
                    labelAlign: 'top',
                    width: '100%',
                    //allowBlank: false,
                    //allowBlank: fieldsOjb[i].IsReadOnly,
                    //allowBlank: fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired),
                    allowBlank: (fieldsOjb[i].IsMSD)?!fieldsOjb[i].IsRequired:(fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired)),
                    readOnly: fieldsOjb[i].IsReadOnly,
                    //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                    fieldStyle: c_styles,
                    step: 1,
                    decimalPrecision: precision,
                    value: (!!fieldsOjb[i].DoubleValue) ? parseFloat(fieldsOjb[i].DoubleValue) : null,
                    hidden: false,
                    listeners: {
                        afterrender: function(me) {
                            Ext.tip.QuickTipManager.register({
                                target: me.getId(),
                                title : '',
                                text  : me.fieldLabel
                            });

                        }
                    }
                };
                res_a.push(Dollars);
            }
        }
        if (!!((fieldsOjb[i].DisplayFormat).indexOf("Integer") + 1)) {
            let Integer = {
                xtype: 'numberfield',
                name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                fieldLabel: Label_f,
                labelAlign: 'top',
                width: '100%',
                //allowBlank: false,
                //allowBlank: fieldsOjb[i].IsReadOnly,
                //allowBlank: fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired),
                allowBlank: (fieldsOjb[i].IsMSD)?!fieldsOjb[i].IsRequired:(fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired)),
                readOnly: fieldsOjb[i].IsReadOnly,
                //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                fieldStyle: c_styles,
                step: 1,
                decimalPrecision: 0,
                value: (!!fieldsOjb[i].DoubleValue) ? parseInt(fieldsOjb[i].DoubleValue) : null,
                hidden: false,
                listeners: {
                    afterrender: function(me) {
                        Ext.tip.QuickTipManager.register({
                            target: me.getId(),
                            title : '',
                            text  : me.fieldLabel
                        });

                    }
                }
            };
            res_a.push(Integer);
        }
        if (!!((fieldsOjb[i].DisplayFormat).indexOf("TextboxStringValue") + 1)) {
            let Textbox = {
                xtype: 'textfield',
                name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                fieldLabel: Label_f,
                labelAlign: 'top',
                width: '100%',
                //allowBlank: false,
                //allowBlank: fieldsOjb[i].IsReadOnly,
                //allowBlank: fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired),
                allowBlank: (fieldsOjb[i].IsMSD)?!fieldsOjb[i].IsRequired:(fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired)),
                readOnly: fieldsOjb[i].IsReadOnly,
                //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                fieldStyle: c_styles,
                value: fieldsOjb[i].StringValue,
                hidden: false,
                listeners: {
                    afterrender: function(me) {
                        Ext.tip.QuickTipManager.register({
                            target: me.getId(),
                            title : '',
                            text  : me.fieldLabel
                        });

                    }
                }
            };
            res_a.push(Textbox);
        }
        if (!!((fieldsOjb[i].DisplayFormat).indexOf("Lookup") + 1)) {
            let lookupVal = !!fieldsOjb[i].ComboBoxStringValues ? fieldsOjb[i].ComboBoxStringValues : "";
            let lookupParts = lookupVal.split('|');
            let lookupStore = new Ext.data.Store({
                fields: ["value", "display"],
                data: []
            });
            if (!!lookupParts.length) {
                lookupStore.add({value: lookupVal, display: lookupParts[2] || ''});
            } else {
            }

            let Lookup = {
                xtype: 'lookup',
                name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                fieldLabel: Label_f,
                labelAlign: 'top',
                width: '100%',
                //allowBlank: false,
                //allowBlank: fieldsOjb[i].IsReadOnly,
                //allowBlank: fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired),
                allowBlank: (fieldsOjb[i].IsMSD)?!fieldsOjb[i].IsRequired:(fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired)),
                readOnly: fieldsOjb[i].IsReadOnly,
                //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                fieldStyle: c_styles,
                value: lookupVal,
                hidden: false,
                store: lookupStore,
                listeners: {
                    afterrender: function(me) {
                        Ext.tip.QuickTipManager.register({
                            target: me.getId(),
                            title : '',
                            text  : me.fieldLabel
                        });

                    }
                }
            };
            res_a.push(Lookup);
        }
        if (!!((fieldsOjb[i].DisplayFormat).indexOf("Boolean") + 1)) {
            let Checkbox = {
                //xtype: 'textfield',
                xtype: 'checkboxfield',
                name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                fieldLabel: Label_f,
                labelWidth: 120,
                width: '100%',
                //allowBlank: false,
                //allowBlank: fieldsOjb[i].IsReadOnly,
                //allowBlank: fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired),
                allowBlank: (fieldsOjb[i].IsMSD)?!fieldsOjb[i].IsRequired:(fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired)),
                readOnly: fieldsOjb[i].IsReadOnly,
                //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                fieldStyle: c_styles,
                value: fieldsOjb[i].BooleanValue,
                hidden: false,
                listeners: {
                    afterrender: function(me) {
                        Ext.tip.QuickTipManager.register({
                            target: me.getId(),
                            title : '',
                            text  : me.fieldLabel
                        });

                    }
                }
                //fieldLabel: 'Toppings',
                //defaultType: 'checkboxfield',
                //items: [
                //    {
                //        //boxLabel  : 'Anchovies',
                //        name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                //        inputValue: '1'
                //    }
                //]
            };
            res_a.push(Checkbox);
        }
        if (!!((fieldsOjb[i].DisplayFormat).indexOf("Combobox") + 1)) {
            let comboStore;

            let stKey = AGS.String.format("{0}|{1}", fieldsOjb[i].Entity, fieldsOjb[i].Name);
            let stKey2 = AGS.String.format("{0}|{1}", fieldsOjb[i].Entity, fieldsOjb[i].AttrName);

            if (isMapped && (!!smComboStoreSourecesCRM[stKey] || !!smComboStoreSourecesCRM[stKey2])) {
                comboStore = smComboStoreSourecesCRM[stKey] || smComboStoreSourecesCRM[stKey2];
            } else {
                //comboStore = (!!((fieldsOjb[i].DisplayFormat).indexOf("String") + 1))?generateSMComboboxStore(fieldsOjb[i].ComboBoxStringValues):generateSMComboboxStore(fieldsOjb[i].ComboBoxDoubleValues);
                comboStore = generateSMComboboxStore(fieldsOjb[i]);
            }

            Combobox = {
                xtype: 'combobox',
                name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                fieldLabel: Label_f,
                qtip: Label_f,
                labelAlign: 'top',
                width: '100%',
                queryMode: 'local',
                displayField: 'display',
                valueField: 'value',
//                allowBlank: !adminData.RequiredAllFields && !fieldsOjb[i].IsRequired,
                allowBlank: (fieldsOjb[i].IsMSD)?!fieldsOjb[i].IsRequired:(fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired)),
                readOnly: fieldsOjb[i].IsReadOnly,
                //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                fieldStyle: c_styles,
                store: comboStore,
                ComboBoxSelectionDependencyVariableID: fieldsOjb[i].ComboBoxSelectionDependencyVariableID,
                ValIDComboBoxIndexPairs: fieldsOjb[i].ValIDComboBoxIndexPairs,
                value: fieldsOjb[i].ComboBoxDoubleValues || fieldsOjb[i].ComboBoxStringValues,
                ComboboxMadID: fieldsOjb[i].ID,
                hidden: false,
                listeners: {
                    select: function (combo, comboval) {
                        if (!nestedPairs[combo.ComboboxMadID]) {
                            return true;
                        }

                        var form_f = combo.up('form').getForm();

                        for (var j = 0; j < nestedPairs[combo.ComboboxMadID].length; j++) {
                            var nested_name = nestedPairs[combo.ComboboxMadID][j];
                            var nested_comp = form_f.findField(nested_name);
                            if (!nested_comp) {
                                continue;
                            }

                            var custFilter = new Ext.util.Filter({
                                filterFn: function (item) {
                                    return !comboval[0] ? false : (item.data.index1 == comboval[0].data.index1);
                                }
                            });
//
                            nested_comp.getStore().clearFilter();
                            nested_comp.getStore().addFilter(custFilter);
                            nested_comp.setValue();
                        }
                    },
                    beforerender: function (editor) {
                        if (!!editor.ComboBoxSelectionDependencyVariableID) {
                            var custFilter = new Ext.util.Filter({
                                filterFn: function (item) {
                                    return false;
                                }
                            });
                            editor.getStore().clearFilter();
                            editor.getStore().addFilter(custFilter);
                        }
                    },
                    afterrender: function(me) {
                        Ext.tip.QuickTipManager.register({
                            target: me.getId(),
                            title : '',
                            text  : me.fieldLabel
                        });

                    }
                }
            };

            Picklist_stores[Combobox.name] = comboStore;

            res_a.push(Combobox);

            //add nested comboboxes
            if (!!fieldsOjb[i].ComboBoxSelectionDependencyVariableID) {
                if (!nestedPairs[fieldsOjb[i].ComboBoxSelectionDependencyVariableID]) {
                    nestedPairs[fieldsOjb[i].ComboBoxSelectionDependencyVariableID] = [];
                }
                nestedPairs[fieldsOjb[i].ComboBoxSelectionDependencyVariableID].push(Combobox.name);
            }

        }
    }

    return res_a;
}


function generateSMComboboxStore(ComboEl) {
    ComboVal = (!!((ComboEl.DisplayFormat).indexOf("String") + 1)) ? ComboEl.ComboBoxStringValues : ComboEl.ComboBoxDoubleValues;
    if (ComboVal) {
        var SMComboboxStore = new Ext.data.SimpleStore({
            fields: ["value", "display", "index1"],
            data: []
        });
        //console.dir(entities);
        //SMComboboxStore.add({value:"undefined", display:"Select"});

        if (Array.isArray(ComboEl.ValIDComboBoxIndexPairs)) {
            //Nested Combobox
            for (var i = 0; i < ComboEl.ValIDComboBoxIndexPairs.length; i++) {
                var pairEl = ComboEl.ValIDComboBoxIndexPairs[i];
                SMComboboxStore.add({
                    value: ComboVal[pairEl['Index2']].toString(),
                    display: ComboVal[pairEl['Index2']],
                    index1: pairEl['Index1']
                });
            }

        } else {
            //Usual Combobox
            for (var i = 0; i < ComboVal.length; i++) {
                SMComboboxStore.add({value: ComboVal[i].toString(), display: ComboVal[i], index1: i});
            }
        }
        return SMComboboxStore;
    }
    else {
        return;
    }
}

function sendSMDataFields(accountid, parentsiteid, implSiteId, tradeally, projectId, projectName, projectstatus, phase, phasenumber, initialphasedate, measureId, measureTplId, calculationType, espSmartMeasureId, dataFields) {
    //Send JSON Object to server plugin
    var smObj = {};

    //ddsm_AccountId
    //smObj.accountid = accountid.replace(/\{|\}/g, '');
    smObj.accountid = accountid;
    //ddsm_parentsite
    //smObj.parentsiteid = parentsiteid.replace(/\{|\}/g, '');
    smObj.parentsiteid = parentsiteid;
    //----------TODO: UNCOMMMENT-----------START<<<<<<<<<<<<<
    ////ddsm_ImplementationSite
    //smObj.implSiteId = parentsiteid.replace(/\{|\}/g, '');
    //----------TODO: UNCOMMMENT-------------END<<<<<<<<<<<<<
    //ddsm_TradeAllyId
    //smObj.tradeally = tradeally.replace(/\{|\}/g, '');
    smObj.tradeally = tradeally;
    //ddsm_ProjectToMeasureId
    //smObj.projectId = projectId.replace(/\{|\}/g, '');
    smObj.projectId = projectId;
    smObj.projectName = projectName;
    //ddsm_ProjectStatus
    smObj.projectstatus = projectstatus;
    //ddsm_ProjectPhaseName
    smObj.phase = phase;
    //ddsm_UpToDatePhase
    smObj.phasenumber = phasenumber;
    //ddsm_InitialPhaseDate
    smObj.initialphasedate = ('0' + (initialphasedate.getUTCMonth() + 1)).slice(-2) + '/' + ('0' + initialphasedate.getUTCDate()).slice(-2) + '/' + initialphasedate.getFullYear();

    //smObj.measureId = measureId.replace(/\{|\}/g, '');
    //smObj.measureTplId = measureTplId.replace(/\{|\}/g, '');
    //smObj.calculationType = measureTplId.replace(/\{|\}/g, '');
    //smObj.espSmartMeasureId = espSmartMeasureId.replace(/\{|\}/g, '');
    smObj.measureId = measureId;
    smObj.measureTplId = measureTplId;
    smObj.calculationType = measureTplId;
    smObj.espSmartMeasureId = espSmartMeasureId;
    smObj.dataFields = dataFields;

    //debugger;
    //console.dir(smObj);
    console.dir(JSON.stringify(smObj));

    setTimeout(function () {
        //Send method .... JSON.stringify(smObj)
        Process.callAction("ddsm_CalculateESPMeasure",
            [{
                key: "Target",
                type: Process.Type.EntityReference,
                value: {id: Xrm.Page.data.entity.getId(), entityType: "ddsm_project"}
            },
                {
                    key: "UserInput",
                    type: Process.Type.String,
                    //value: JSON.stringify(smObj)
                    value: Base64.encode(JSON.stringify(smObj))
                }
            ],
            function (params) {
                // Success
                for (var i = 0; i < params.length; i++) {
                    params[i].key + "=" + params[i].value;
                    if (params[i].key === "Result") {
                        console.log("Result : ");
                        console.dir(params[i]);

                        //check for error response
                        try {
                            var res = JSON.parse(params[i].value);
                            if (res.hasOwnProperty('ErrorMsg')) {
                                //Ext.Msg.alert('Error', res.ErrorMsg);
                                console.log(res.ErrorMsg);
                                showSpinner(false);
                                Alert.show("Error",res.ErrorMsg, [{
                                    label: "Ok",
                                    callback: function () {
                                    }
                                }], "ERROR", 500, 200);
                                return;
                            }
                        } catch (e) {
                            console.error('Response error:', e);
                            showSpinner(false);
                            return;
                        }

                        callbackSendSMData(JSON.parse(params[i].value))
                    }
                }
            },
            function (e) {
                // Error
                console.log(e);
                //Ext.Msg.alert('Action failed', '[ESP] Calculate ESP Measure: ' + e);
                showSpinner(false);
                Alert.show("Action failed","[ESP] Calculate ESP Measure: " + e, [{
                    label: "Ok",
                    callback: function () {
                    }
                }], "ERROR", 500, 200);
            }
        );

    }, 100);

    //Clear current values

    smCurrentVersionData.MAD = null;
    smCurrentVersionData.CalculationID = null;
    smCurrentVersionData.MeasTplID = null;
    smCurrentVersionData.MeasID = null;
    smCurrentVersionData.MeasTplData = null;
    smCurrentVersionData.EditSM = false;
    smCurrentVersionData.MeasureData = null;
}

//Callback Smart measure object sends
function callbackSendSMData(obj) {
    if(typeof CheckBackgroundProcess != 'undefined' && !!CheckBackgroundProcess && typeof CheckBackgroundProcess == 'function') {
        CheckBackgroundProcess(
            '{"EntityNames":[ { "EntityName":"ddsm_measure"}, {"EntityName":"ddsm_financial"}]}'
            , function() {
                if(!!GlobalJs.SpinnerSettings["OnLoadShow"])
                {
                    showSpinner(true, null, "Please note! <br>There are background activities <br>working with the record.");
                }
            }
            , function(checkStatus) {
                console.log(checkStatus);
                showSpinner(false);
            }
            , function(msg){
                console.log(msg);
                showSpinner(false);
            }
            , true
            , 5000);
    } else {
        showSpinner(false);
    }


    refreshSMsubgrid('SmartMeasure_view');
}

/**
 * Refresh subgrid on project screen.
 * @param ctrl_name
 * @Note: to be overridden on other pages
 */
function refreshSMsubgrid(ctrl_name) {
    if (!ctrl_name) {
        return;
    }
    Xrm.Page.getControl(ctrl_name).refresh();

    setTimeout(function () {
        addCustomAddButton();
        ChangeEditMeasureViewOnRefresh();
        rmvDeleteMeasureView();
    }, 2000)
}


function clearFormObj(){
    smLibraryForm = null;
    SmartMeasureForm2 = null;
    currentMeasureId = null;
}


//region CUSTOM LOOKUP METHODS

function setNewLookupData(res) {
    if (!res || !res.items || !res.items[0]) {
        return;
    }
    let lookupVal = AGS.String.format("{0}|{1}|{2}", res.items[0].typename, res.items[0].id, res.items[0].name);
    let lookupParts = lookupVal.split('|');
    let lkp_field = Ext.getCmp("smForm1").down('form').getForm().findField(lkp_toUpdate);
    if (!lkp_field) {
        return;
    }
    lkp_field.store.removeAll();
    lkp_field.store.add({value: lookupVal, display: res.items[0].name || ''});
    lkp_field.setValue(lookupVal);

    //clear global field name storage
    lkp_toUpdate = null;

    if(res.items[0].typename == "ddsm_modelnumberapproval" && !!res.items[0].id)
    {
        GetModelNumberData(res.items[0].id);
    }
}

function GetModelNumberData(mnId)
{
    let modelNumberData = {};
    if (!!globalModelNumberFields.length) {
        AGS.REST.retrieveRecord(mnId, "ddsm_modelnumberapproval", globalModelNumberFields.join(','), null, function (data) {
            //console.dir(data);
            modelNumberData = data;
        }, null, false);
    }
    console.dir(modelNumberData);


    //debugger;
        for (key in objMappingSM) {
            for (key2 in modelNumberData) {

                let key3 = globalModelNumberCustomeMap[key2];
                if (!key3) {
                    continue;
                }
                if(key3.toLowerCase() == "ddsm_modelnumberapprovalid")
                {
                    continue;
                }
                if (objMappingSM[key].AttrName != key3) {
                    continue;
                }

                var fieldName = objMappingSM[key].DispName.replace(/\s/ig, '_');
                var formField = Ext.getCmp("smForm1").down('form').getForm().findField(fieldName);
                if (!formField) {
                    formField = Ext.getCmp("smForm1").down('form').getForm().findField(objMappingSM[key].DispName);
                    if (!formField) {
                        formField = Ext.getCmp("smForm1").down('form').getForm().findField(key3.toLowerCase());
                        if (!formField) {
                            continue;
                        }
                    }
                }

                console.dir(formField);

                //console.dir(modelNumberData[key2]);
                let thisData = null;
                if (objMappingSM[key].FieldType == "String" || objMappingSM[key].FieldType == "Integer" || objMappingSM[key].FieldType == "Decimal") {
                    thisData = modelNumberData[key2];
                } else if (objMappingSM[key].FieldType == "Money") {
                    thisData = modelNumberData[key2].Value || 0;
                } else if (objMappingSM[key].FieldType == "Picklist") {
                    thisData = modelNumberData[key2].Value;
                } else if (objMappingSM[key].FieldType == "Boolean") {
                    thisData = modelNumberData[key2];
                } else if (objMappingSM[key].FieldType == "Lookup") {
                    thisData = AGS.String.format("{0}|{1}|{2}", objMappingSM[key].LookupEntity, modelNumberData[key2].Id || '', modelNumberData[key2].Name || '');
                }

                formField.setValue(thisData);
            }
        }


        //debugger;
}

function getSMLookupData(entityName, lkpCallback) {

    var localname = entityName.toLowerCase();
    var objecttypes = Xrm.Internal.getEntityCode(localname);
    if(localname != "ddsm_modelnumberapproval" && localname != "ddsm_skuapproval"){
        var lookupURI = "/_controls/lookup/lookupinfo.aspx"
            + "?LookupStyle=single"
            + "&objecttypes=" + objecttypes
            + "&ShowNewButton=0"
            + "&ShowPropButton=1"
            + "&browse=false"
            + "&AllowFilterOff=0"
            + "&DefaultType=" + objecttypes
            + "&DisableQuickFind=0"
            + "&DisableViewPicker=0";

        window.setTimeout(function () {
            var DialogOption = new Xrm.DialogOptions;
            DialogOption.width = 550;
            DialogOption.height = 550;
            Xrm.Internal.openDialog(lookupURI, DialogOption, null, null, (lkpCallback instanceof Function) ? lkpCallback : null);
        }, 200);
    } else {
        //Get View Model Number Approved or SKU Approved records for selected Measure Tpl

        var viewId = '{00000000-0000-0000-0000-000000000001}';
        var fetchXml = '';

        //console.log(selMeasTplId);

        let MN_LookupValue = (!!Ext.getCmp("smForm1").down('form').getForm().getValues().ddsm_modelnumberapprovalid) ? (Ext.getCmp("smForm1").down('form').getForm().getValues().ddsm_modelnumberapprovalid).split('|') : [];
        console.dir(MN_LookupValue);

        var measTplData = AGS.REST.retrieveRecord(selMeasTplId, "ddsm_measuretemplate", "ddsm_ModelNumberApprovalStatusMulti, ddsm_SKUApprovalStatusMulti", null, null, null, false);
        console.dir(measTplData);

        //Create filter
        fetchXml = '<fetch>';

        if(localname == "ddsm_modelnumberapproval")
            fetchXml +=  '<entity name="ddsm_modelnumberapproval" >';

        if(localname == "ddsm_skuapproval")
            fetchXml +=  '<entity name="ddsm_skuapproval" >';

        fetchXml +=  '<filter type="and" >'
            + '<condition attribute="ddsm_measuretemplateid" operator="eq" value="' + selMeasTplId + '" />'
            + '<condition attribute="statecode" operator="eq" value="0" />';

        //START FortisBC Demo
/*
        let Efficiency = (!!Ext.getCmp("smForm1").down('form').getForm().getValues()["Efficiency_(%)"]) ? parseFloat(Ext.getCmp("smForm1").down('form').getForm().getValues()["Efficiency_(%)"]).toFixed(6) : -100;
        console.dir(Efficiency);
        if(Efficiency > 0) {
            fetchXml += '<condition attribute="ddsm_efficiency" operator="ge" value="' + Efficiency + '" />';
        }
*/
        //END FprtisBC Demo

        if(localname == "ddsm_modelnumberapproval" && !!measTplData.ddsm_ModelNumberApprovalStatusMulti && measTplData.ddsm_ModelNumberApprovalStatusMulti.split(',').length > 0)
        {
            fetchXml += '<filter type="or" >';
            let MNA_Status = measTplData.ddsm_ModelNumberApprovalStatusMulti.split(',');
            for(let i = 0; i < MNA_Status.length; i++)
                fetchXml +=  '<condition attribute="ddsm_approvalstatus" operator="eq" value="' + MNA_Status[i] +'" />';

            fetchXml +=  '</filter>'
                + '</filter>';
        } else if(localname == "ddsm_modelnumberapproval") {
            fetchXml +=  '</filter>'
        }

        if(localname == "ddsm_skuapproval" && !!measTplData.ddsm_SKUApprovalStatusMulti && measTplData.ddsm_SKUApprovalStatusMulti.split(',').length > 0)
        {

            fetchXml += '<filter type="or" >';
            let SKUA_Status = measTplData.ddsm_SKUApprovalStatusMulti.split(',');
            for(let i = 0; i < SKUA_Status.length; i++)
                fetchXml +=  '<condition attribute="ddsm_approvalstatus" operator="eq" value="' + SKUA_Status[i] +'" />';

            fetchXml +=  '</filter>'
                + '</filter>';



            fetchXml += '<link-entity name="ddsm_sku" from="ddsm_skuid" to="ddsm_skuid" >'
                + '<link-entity name="ddsm_modelnumber" from="ddsm_modelnumberid" to="ddsm_modelnumber" >'
                + '<link-entity name="ddsm_modelnumberapproval" from="ddsm_modelnumberid" to="ddsm_modelnumberid" >'
                + '<filter type="and" >'
                + '<condition attribute="ddsm_measuretemplateid" operator="eq" value="' + selMeasTplId + '" />';

            if(MN_LookupValue.length > 0 && !!MN_LookupValue[1]){
                fetchXml += '<condition attribute="ddsm_modelnumberapprovalid" operator="eq" value="' + MN_LookupValue[1].replace(/\{|\}/g, '') + '" />';
            }

            fetchXml += '</filter>'
                + '</link-entity>'
                + '</link-entity>'
                + '</link-entity>';


        } else if(localname == "ddsm_skuapproval") {
            fetchXml +=  '</filter>'
        }

        fetchXml += '</entity>'
            + '</fetch>';

        console.log(fetchXml);

        //Create layout
        var layoutXml = "<grid name='' object='1' jump='name' select='1' icon='1' preview='1'>";

        if(localname == "ddsm_modelnumberapproval")
            layoutXml += "<row name='result' id='ddsm_modelnumberapprovalid'>";

        if(localname == "ddsm_skuapproval")
            layoutXml += "<row name='result' id='ddsm_skuapprovalid'>";

        layoutXml += "<cell name='ddsm_name' width='300' />"
            + "</row>"
            + "</grid>";

        var customView = {
            fetchXml: fetchXml,
            id: viewId,
            layoutXml: layoutXml,
            name: (localname == "ddsm_modelnumberapproval") ? "Model Number View" : "SKU View",
            recordType: objecttypes,
            Type: 0
        };

        var callbackReference = {
            callback: (lkpCallback instanceof Function) ? lkpCallback : null
        }

        window.setTimeout(function(){
            LookupObjectsWithCallback(callbackReference, null, "single", objecttypes, 0, null, "", null, null, null, null, null, null, viewId, [customView]);
        }, 200);
    }
}

//endregion

function generateAttrComboStore(key, c_options) {

    var tmpArr = [];
    for (var i = 0; i < c_options.length; i++) {
        tmpArr.push(
            [c_options[i].Value, c_options[i].Label.UserLocalizedLabel.Label]
        );
    }

    return Ext.create('Ext.data.ArrayStore', {
        storeId: 'comboStore_' + key,
        fields: ["value", "display"],
        data: tmpArr
    });
}

function VerifiedCreatingMeas(valField)
{
    let createNewMeas = true;

    if(!valField) {return createNewMeas;}
    let progOffering = Xrm.Page.getAttribute("ddsm_programoffering").getValue();
    let progOffering_Name = progOffering[0].name;
    let progOffering_Id = progOffering[0].id;
    //Program Offering Name contains "Green Heat"
    if(!(progOffering_Name.indexOf("Green Heat") + 1))
    {} else {

        AGS.REST.retrieveRecord(progOffering_Id, "ddsm_programoffering", "ddsm_MaximumProjectPurchasePriceFinancing", null, function (data) {
            //console.dir(data);
            if(!data)
            {
                return;
            }

            if(!!data.ddsm_MaximumProjectPurchasePriceFinancing && !!data.ddsm_MaximumProjectPurchasePriceFinancing.Value && valField > parseFloat(data.ddsm_MaximumProjectPurchasePriceFinancing.Value))
            {
                createNewMeas = false;
            }

        }, null, false);

    }

    return createNewMeas;

}
