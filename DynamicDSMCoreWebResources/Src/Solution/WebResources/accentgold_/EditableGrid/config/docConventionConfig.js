/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 2.1.3
 * Last updated on 09/27/2015
 * Document Name Convention Grid
 */
///// START CONFIG GRID
var configJson = {
    entitySchemaName: "ddsm_documentconvention",
    dateFormat: "m/d/Y",
    sorters: { property: 'ddsm_RequiredByStatus', direction: 'ASC' },
    modelGrid: "docConv",
    idGrid: "docConvGridId",
    border: true,
    height: 160,
    title: '',
    fields: [
        {
            header: 'Milestone Index',
            name: 'ddsm_RequiredByStatus',
            type: 'number',
            defaultValue: null,
            sortable: true,
            filterable: true,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        },{
            header: 'Document Name Convention',
            name: 'ddsm_name',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: true,
            readOnly: false,
            allowBlank: true,
            flex: 0.2,
            hidden: false
        },{
            header: 'Uploaded',
            name: 'ddsm_Uploaded',
            type: 'boolean',
            defaultValue: false,
            sortable: false,
            filterable: true,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: false
        },{
            header: 'Project',
            name: 'ddsm_ProjectId',
            type: 'lookup',
            entityNameLookup: 'ddsm_project',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        },{
            header: 'Project Template',
            name: 'ddsm_ProjectTemplateId',
            type: 'lookup',
            entityNameLookup: 'ddsm_projecttemplate',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        },{
            header: 'Measure Template',
            name: 'ddsm_MeasureTemplateId',
            type: 'lookup',
            entityNameLookup: 'ddsm_measuretemplate',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        },{
            header: 'Milestone Sets',
            name: 'ddsm_MilestoneSet',
            type: 'lookup',
            entityNameLookup: 'ddsm_milestoneset',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        },{
            header: 'Model Number',
            name: 'ddsm_ModelNumber',
            type: 'lookup',
            entityNameLookup: 'ddsm_modelnumber',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        },{
            header: 'SKU',
            name: 'ddsm_SKU',
            type: 'lookup',
            entityNameLookup: 'ddsm_sku',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        },{
            header: 'UploadedId',
            name: 'ddsm_UploadedId',
            type: 'lookup',
            entityNameLookup: 'ddsm_uploadeddocumens',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }
    ]
};
///// END CONFIG GRID