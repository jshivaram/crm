/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 2.1.3
 * Last updated on 05/21/2015.
 * Uploads Docs Grid
 */
///// START CONFIG GRID
var configJson = {
    entitySchemaName: "ddsm_uploadeddocumens",
    dateFormat: "m/d/Y",
    sorters: { property: 'CreatedOn', direction: 'DESC' },
    modelGrid: "uploadsDocsGrid",
    idGrid: "uploadsGridId",
    border: true,
    height: 288,
    title: '',
    renderTo: 'docList',
    fields: [
        {
            header: 'Docs ID',
            name: 'AnnotationId',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: true,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        },{
            header: 'Uploading Date',
            name: 'CreatedOn',
            type: 'date',
            defaultValue: null,
            sortable: true,
            filterable: true,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: false
        },{
            header: 'Modified Date',
            name: 'ModifiedOn',
            type: 'date',
            defaultValue: null,
            sortable: true,
            filterable: true,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: false
        },{
            header: 'File Name',
            name: 'FileName',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: true,
            readOnly: true,
            allowBlank: true,
            flex: 0.2,
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                if (value != null) {
                    return Ext.String.format(
                        '<a href="javascript:openDoc({1})">{0}</a>',
                        value,
                        "'" + record.data["AnnotationId"] + "'"
                    );
                } else { return ''; }
            },
            hidden: false
        },{
            header: 'Title',
            name: 'ddsm_Title',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: true,
            readOnly: false,
            allowBlank: true,
            flex: 0.2,
            hidden: false
        },{
            header: 'Description/Comment',
            name: 'ddsm_Description',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: true,
            readOnly: false,
            allowBlank: true,
            flex: 0.6,
            hidden: false
        },{
            header: 'Status',
            name: 'ddsm_Status',
            type: 'combobox',
            defaultValue: '962080000',
            sortable: false,
            filterable: false,
            readOnly: false,
            allowBlank: false,
            width: 100,
            hidden: false,
            editor: 'ddsm_StatusColumnEditor',
            renderer: 'ddsm_StatusColumnRenderer'
        },{
            header: 'Modified By',
            name: 'ddsm_ModifiedByName',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: true,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: false
        },{
            header: 'Modified System',
            name: 'ddsm_ModifiedSystem',
            type: 'combobox',
            defaultValue: '962080000',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: false,
            width: 75,
            hidden: false,
            editor: 'ddsm_ModifiedSystemColumnEditor',
            renderer: 'ddsm_ModifiedSystemColumnRenderer'
        },{
            header: 'File Size',
            name: 'FileSize',
            type: 'number',
            defaultValue: 0,
            sortable: true,
            filterable: true,
            readOnly: true,
            allowBlank: true,
            width: 100,
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                if (value != null) {
                    var sizeFile = parseInt(value/1048576);
                    var sizeType = "Mb";
                    if(sizeFile == 0){sizeFile = parseInt(value/1024);sizeType = "Kb";if(sizeFile == 0){sizeFile = value;sizeType = "b";}}

                    return Ext.String.format(
                        '{0}{1}',
                        sizeFile,
                        sizeType);
                } else { return ''; }
            },
            hidden: false
        },{
            header: 'Site',
            name: 'ddsm_SiteId',
            type: 'lookup',
            entityNameLookup: 'ddsm_site',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: false
        },{
            header: 'Project',
            name: 'ddsm_ProjectId',
            type: 'lookup',
            entityNameLookup: 'ddsm_project',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: false
        },{
            header: 'ConventionId',
            name: 'ddsm_ConventionId',
            type: 'lookup',
            entityNameLookup: 'ddsm_documentconvention',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }
    ]
};
///// END CONFIG GRID

function openDoc(Id){

    var noteUrl = window.parent.Xrm.Page.context.getClientUrl() + "/notes/edit.aspx?id=" + Id;

    var notePage = null;
    var req = {};
    req.type = "GET";
    req.url = noteUrl;
    req.async = false;
    req.dataType = "html";
    req.success = function (data, textStatus, XmlHttpRequest)
    {
        notePage = data;
    };
    $.ajax(req);

    if (!!notePage) {
        var tage = 'span';
        var regexp = new RegExp('<span[^>]+WRPCTokenUrl[^>]+>');
        var SpanTages = notePage.match(regexp);
        if (!!SpanTages) {
            var TokenTage = SpanTages[0] + '</span>';
            var div = $('<div/>').html(TokenTage);
            var WRPCToken = $(tage, div).attr('WRPCTokenUrl');
            if (!!WRPCToken) {
                window.open(window.parent.Xrm.Page.context.getClientUrl() + "/Activities/Attachment/download.aspx?AttachmentType=5&AttachmentId=" + Id + WRPCToken);
            }
        }
    }
}
function _getOptionSetDataArray(entityName, optionSetName, customStore){
    SDK.Metadata.RetrieveAttribute(entityName, optionSetName, "00000000-0000-0000-0000-000000000000", true,
        function (result) {
            var Data = [];
            for (var i = 0; i < result.OptionSet.Options.length; i++) {
                var dataSet = [];
                var text = result.OptionSet.Options[i].Label.LocalizedLabels[0].Label;
                var value = result.OptionSet.Options[i].Value;
                dataSet = [value.toString(), text.toString()];
                Data.push(dataSet);
            }
            if(customStore !== null) {
                eval(customStore).add(Data);
            } else {
                eval(optionSetName + "Combo").add(Data);
            }
        },
        function (error) { }
    );
}

////////////////////////////////////////////////////////////////////////

//eval('var '+ 'ddsm_ProgramCycle' + 'Combo = new Ext.data.SimpleStore({fields: ["value", "display"],data: []});');

for(var i = 0;i<configJson.fields.length;i++){
    if(typeof configJson.fields[i].group !== 'undefined'){
        for(var j = 0;j<configJson.fields[i].group.length;j++){
            if(configJson.fields[i].group[j].type == 'combobox'){

                eval('var '+ configJson.fields[i].group[j].name + 'Combo = new Ext.data.SimpleStore({fields: ["value", "display"],data: []});');

                _getOptionSetDataArray(configJson.entitySchemaName, configJson.fields[i].group[j].name, null);

                eval('var '+ configJson.fields[i].group[j].name + 'ColumnEditor = {queryMode: "local",xtype: "combobox",store: ' + configJson.fields[i].group[j].name + 'Combo,displayField: "display",valueField: "value",listeners: {focus: function (editor, e) { lookupActive = ""; }}};');

                eval('var ' +configJson.fields[i].group[j].name + 'ColumnRenderer = function (value) {var index = ' + configJson.fields[i].group[j].name + 'Combo.findExact("value", value);if (index != -1) {var rs = ' + configJson.fields[i].group[j].name + 'Combo.getAt(index).data;return rs.display;}};');


            }
        }
    } else {
        if(configJson.fields[i].type == 'combobox'){

            eval('var '+ configJson.fields[i].name + 'Combo = new Ext.data.SimpleStore({fields: ["value", "display"],data: []});');

            _getOptionSetDataArray(configJson.entitySchemaName, configJson.fields[i].name, null);

            eval('var '+ configJson.fields[i].name + 'ColumnEditor = {queryMode: "local",xtype: "combobox",store: ' + configJson.fields[i].name + 'Combo,displayField: "display",valueField: "value",listeners: {focus: function (editor, e) { lookupActive = ""; }}};');

            eval('var ' +configJson.fields[i].name + 'ColumnRenderer = function (value) {var index = ' + configJson.fields[i].name + 'Combo.findExact("value", value);if (index != -1) {var rs = ' + configJson.fields[i].name + 'Combo.getAt(index).data;return rs.display;}};');

        }
    }
}
