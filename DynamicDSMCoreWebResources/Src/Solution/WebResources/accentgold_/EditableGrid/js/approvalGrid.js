/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 1.0.5
 * Last updated on 08/03/2015.
 * Approval Project Group Grid
 */
(function (fallback) {

    fallback = fallback || function () { };

    var trap = function () {
        var args = Array.prototype.slice.call(arguments);
        var message = args.join(' ');
        fallback(message);
    };

    if (typeof console === 'undefined') {
        console = {
            messages: [],
            raw: [],
            dump: function () { return console.messages.join('\n'); },
            log: trap,
            debug: trap,
            info: trap,
            warn: trap,
            error: trap,
            assert: trap,
            clear: function () {
                console.messages.length = 0;
                console.raw.length = 0;
            },
            dir: trap,
            dirxml: trap,
            trace: trap,
            group: trap,
            groupCollapsed: trap,
            groupEnd: trap,
            time: trap,
            timeEnd: trap,
            timeStamp: trap,
            profile: trap,
            profileEnd: trap,
            count: trap,
            exception: trap,
            table: trap
        };
    }

})(null);

var b = window.parent;

if (b.Xrm.Page.getAttribute("ddsm_projectgrouptemplates").getValue() != null) {
    var loading3GridInterval = null, validLoading3Grid, dataOk, results = [], loadData, recalcActualEnd, _getODataEndpoint, entitySchemaName, Save_RecordAll, saveGridStatus, lookupActive, get_saveGridStatus, get_dataOkgrid, refreshGrid, reloadGrid, idGrid, entityName_getLookupData, activeRowIdx = 0, filteringCategory = "000000000", groupedCategory = false, rowEditing, AutoFncCreationIndex = 0;
    var completedOutofSequence = '962080001'; // 962080000 - user is not able to insert anything anywhere; 962080001 - user can insert the appropriate Set in the specific location; 962080002. - user can insert any Set in any place.
    var sequenceRequired = true; // DSM/MPM logic; default DSM logic

    $(b.document).ready(function () {

        // ---- START Excel export code
        var Base64 = (function() {
            // Private property
            var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

            // Private method for UTF-8 encoding

            function utf8Encode(string) {
                string = string.replace(/\r\n/g, "\n");
                var utftext = "";
                for (var n = 0; n < string.length; n++) {
                    var c = string.charCodeAt(n);
                    if (c < 128) {
                        utftext += String.fromCharCode(c);
                    } else if ((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                    } else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                }
                return utftext;
            }

            // Public method for encoding
            return {
                encode: (typeof btoa == 'function') ? function(input) {
                    return btoa(utf8Encode(input));
                } : function(input) {
                    var output = "";
                    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
                    var i = 0;
                    input = utf8Encode(input);
                    while (i < input.length) {
                        chr1 = input.charCodeAt(i++);
                        chr2 = input.charCodeAt(i++);
                        chr3 = input.charCodeAt(i++);
                        enc1 = chr1 >> 2;
                        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                        enc4 = chr3 & 63;
                        if (isNaN(chr2)) {
                            enc3 = enc4 = 64;
                        } else if (isNaN(chr3)) {
                            enc4 = 64;
                        }
                        output = output +
                        keyStr.charAt(enc1) + keyStr.charAt(enc2) +
                        keyStr.charAt(enc3) + keyStr.charAt(enc4);
                    }
                    return output;
                }
            };
        })();
        // ---- END Excel export code

        Ext.Loader.setConfig({
            enabled: true,
            disableCaching: false,
            path: { 'Ext': 'accentgold_/EditableGrid/js/' }
        });
        Ext.Loader.setPath('Ext.ux', 'accentgold_/EditableGrid/js/ux');
        Ext.require([
            'Ext.Button',
            'Ext.grid.*',
            'Ext.data.*',
            'Ext.ux.grid.FiltersFeature',
            'Ext.ux.grid.menu.ListMenu',
            'Ext.util.*',
            'Ext.state.*',
            'Ext.form.*',
            'Ext.tip.QuickTip',
            'Ext.selection.CheckboxModel'
        ]);

        Ext.override('Ext.ux.grid.FiltersFeature', {
            show: function () {
                var lastArgs = null;
                return function () {
                    if (!arguments) {
                        this.callParent(lastArgs);
                    } else {
                        lastArgs = arguments;
                        if (this.loadOnShow && !this.loaded) {
                            this.store.load();
                        }
                        this.callParent(arguments);
                    }
                };
            }()
        });

        Ext.onReady(function () {

            // ---- START Excel export code
            var reg = /[^\.\,\;\:\?\!\"\'\(\)\+\№\%\#\@\$\[\]\{\}\~\^\&\*\/\\\|\<\>\s]+/g;
            var strName = b.Xrm.Page.getAttribute("ddsm_name").getValue();
            strName = strName.match(reg).join(' ');

            var exportExcelXml = function(includeHidden, title) {

                if (!title) {title = "Project Milestones"}

                var vExportContent = getExcelXml(includeHidden, title);
                var location = 'data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);


                if (!Ext.isIE) {
                    var gridEl = Ext.getCmp(idGrid).getEl();

                    var el = Ext.DomHelper.append(gridEl, {
                        tag: "a",
                        download: strName + " - " + title + " - " + Ext.Date.format(new Date(), 'm-d-Y H-i') + '.xls',
                        href: location
                    });
                    el.click();
                    Ext.fly(el).destroy();
                } else {
                    var fileName = strName + " - " + title + " - " + Ext.Date.format(new Date(), 'm-d-Y H-i') + '.xls';
                    var form = Ext.getCmp(idGrid).down('form#uploadForm');
                    if (form) {
                        form.destroy();
                    }
                    form = Ext.getCmp(idGrid).add({
                        xtype: 'form',
                        itemId: 'uploadForm',
                        hidden: true,
                        standardSubmit: true,
                        url: 'http://dynamicdsm.richlode.biz/crmIExls.asmx/GetXsl',
                        items: [{
                            xtype: 'hiddenfield',
                            name: 'Name',
                            value: fileName
                        },{
                            xtype: 'hiddenfield',
                            name: 'strXml',
                            value: Base64.encode(vExportContent)
                        }]
                    });

                    form.getForm().submit();

                    //alert("The xls export isn't supported by IE.");
                }

            };

            var getExcelXml = function(includeHidden, title) {
                var theTitle = title || this.title;
                var worksheet = createWorksheet(includeHidden, theTitle);
                var totalWidth =  Ext.getCmp(idGrid).columnManager.columns.length;
                return ''.concat(
                    '<?xml version="1.0"?>',
                    '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">',
                    '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Title>' + theTitle + '</Title></DocumentProperties>',
                    '<OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office"><AllowPNG/></OfficeDocumentSettings>',
                    '<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">',
                    '<WindowHeight>' + worksheet.height + '</WindowHeight>',
                    '<WindowWidth>' + 12585 + '</WindowWidth>',
                    '<ProtectStructure>False</ProtectStructure>',
                    '<ProtectWindows>False</ProtectWindows>',
                    '</ExcelWorkbook>',

                    '<Styles>',

                    '<Style ss:ID="Default" ss:Name="Normal">',
                    '<Alignment ss:Vertical="Bottom"/>',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>',
                    '<Interior/>',
                    '<NumberFormat/>',
                    '<Protection/>',
                    '</Style>',

                    '<Style ss:ID="title">',
                    '<Borders />',
                    '<Font ss:Bold="1" ss:Size="18" />',
                    '<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1" />',
                    '<Interior ss:Color="#E0ECF0" ss:Pattern="Solid" />',
                    '<NumberFormat ss:Format="@" />',
                    '</Style>',

                    '<Style ss:ID="headercell">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:Bold="1" ss:Size="10" />',
                    '<Alignment ss:Horizontal="Center" ss:WrapText="1" />',
                    '<Interior ss:Color="#CFE1E8" ss:Pattern="Solid" />',
                    '</Style>',


                    '<Style ss:ID="even">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Interior ss:Color="#FFFFFF" ss:Pattern="Solid" />',
                    '</Style>',

                    /*
                     '<Style ss:ID="evendate" ss:Parent="even">',
                     '<NumberFormat ss:Format="m-d-yyyy" />',
                     '</Style>',

                     '<Style ss:ID="evenint" ss:Parent="even">',
                     '<Numberformat ss:Format="0" />',
                     '</Style>',

                     '<Style ss:ID="evenfloat" ss:Parent="even">',
                     '<Numberformat ss:Format="0.00" />',
                     '</Style>',
                     */

                    '<Style ss:ID="odd">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Interior ss:Color="#F1F7F8" ss:Pattern="Solid" />',
                    '</Style>',

                    '<Style ss:ID="groupSeparator">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:Bold="1" ss:Size="10" />',
                    '<Interior ss:Color="#D3D3D3" ss:Pattern="Solid" />',
                    '</Style>',
                    /*
                     '<Style ss:ID="odddate" ss:Parent="odd">',
                     '<NumberFormat ss:Format="m-d-yyyy" />',
                     '</Style>',
                     '<Style ss:ID="oddint" ss:Parent="odd">',
                     '<NumberFormat Format="0" />',
                     '</Style>',

                     '<Style ss:ID="oddfloat" ss:Parent="odd">',
                     '<NumberFormat Format="0.00" />',
                     '</Style>',
                     */


                    '</Styles>',
                    worksheet.xml,
                    '</Workbook>'
                );
            };

            var getModelField = function(fieldName) {
                var fields = Ext.getCmp(idGrid).store.model.getFields();
                for (var i = 0; i < fields.length; i++) {
                    if (fields[i].name === fieldName) {
                        return fields[i];
                    }
                }
            };

            var generateEmptyGroupRow = function(dataIndex, value, cellTypes, includeHidden) {
                var cm =  Ext.getCmp(idGrid).columnManager.columns;
                var colCount = cm.length;
                var rowTpl = '<Row ss:AutoFitHeight="0"><Cell ss:StyleID="groupSeparator" ss:MergeAcross="{0}"><Data ss:Type="String"><html:b>{1}</html:b></Data></Cell></Row>';
                var visibleCols = 0;
                // rowXml += '<Cell ss:StyleID="groupSeparator">'
                for (var j = 0; j < colCount; j++) {
                    if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && (includeHidden || !cm[j].hidden)) {
                        // rowXml += '<Cell ss:StyleID="groupSeparator"/>';
                        visibleCols++;
                    }
                }
                // rowXml += "</Row>";
                return Ext.String.format(rowTpl, visibleCols - 1, value);
            };


            var createWorksheet = function(includeHidden, theTitle) {
                // Calculate cell data types and extra class names which affect formatting
                var cellType = [];
                var cellTypeClass = [];
                var cm = Ext.getCmp(idGrid).columnManager.columns;

                var totalWidthInPixels = 0;
                var colXml = '';
                var headerXml = '';
                var visibleColumnCountReduction = 0;
                var colCount = cm.length;
                for (var i = 0; i < colCount; i++) {
                    if (cm[i].xtype != 'actioncolumn' && (cm[i].dataIndex != '') && (includeHidden || !cm[i].hidden)) {
                        var w = cm[i].getEl().getWidth();
                        totalWidthInPixels += w;

                        if (cm[i].text === "") {
                            cellType.push("None");
                            cellTypeClass.push("");
                            ++visibleColumnCountReduction;
                        } else {
                            colXml += '<Column ss:AutoFitWidth="1" ss:Width="' + w + '" />';
                            headerXml += '<Cell ss:StyleID="headercell">' +
                            '<Data ss:Type="String">' + cm[i].text + '</Data>' +
                            '<NamedCell ss:Name="Print_Titles"></NamedCell></Cell>';


                            var fld = getModelField(cm[i].dataIndex);
                            switch (fld.type.type) {
                                case "int":
                                    cellType.push("Number");
                                    cellTypeClass.push("int");
                                    break;
                                case "float":
                                    cellType.push("Number");
                                    cellTypeClass.push("float");
                                    break;

                                case "bool":

                                case "boolean":
                                    cellType.push("String");
                                    cellTypeClass.push("");
                                    break;
                                case "date":
                                    cellType.push("DateTime");
                                    cellTypeClass.push("date");
                                    break;
                                default:
                                    cellType.push("String");
                                    cellTypeClass.push("");
                                    break;
                            }
                        }
                    }
                }
                var visibleColumnCount = cellType.length - visibleColumnCountReduction;
                var result = {
                    height: 9000,
                    width: Math.floor(totalWidthInPixels * 30) + 50
                };

                var numGridRows = Ext.getCmp(idGrid).store.getCount() + 2;
                if (!Ext.isEmpty(Ext.getCmp(idGrid).store.groupField) || Ext.getCmp(idGrid).store.groupers.items.length > 0) {
                    numGridRows = numGridRows + Ext.getCmp(idGrid).store.getGroups().length;
                }

                // create header for worksheet
                var t = ''.concat(
                    '<Worksheet ss:Name="' + theTitle + '">',

                    '<Names>',
                    '<NamedRange ss:Name="Print_Titles" ss:RefersTo="=\'' + theTitle + '\'!R1:R2">',
                    '</NamedRange></Names>',

                    '<Table ss:ExpandedColumnCount="' + (visibleColumnCount + 2),
                    '" ss:ExpandedRowCount="' + numGridRows + '" x:FullColumns="1" x:FullRows="1" ss:DefaultColumnWidth="65" ss:DefaultRowHeight="15">',
                    colXml,
                    '<Row ss:Height="38">',
                    '<Cell ss:MergeAcross="' + (visibleColumnCount - 1) + '" ss:StyleID="title">',
                    '<Data ss:Type="String" xmlns:html="http://www.w3.org/TR/REC-html40">',
                    '<html:b>' + strName + ' - '+ theTitle + '</html:b></Data><NamedCell ss:Name="Print_Titles">',
                    '</NamedCell></Cell>',
                    '</Row>',
                    '<Row ss:AutoFitHeight="1">',
                    headerXml +
                    '</Row>'
                );

                var groupVal = "";
                var groupField = "";
                if (Ext.getCmp(idGrid).store.groupers.keys.length > 0) {
                    groupField = Ext.getCmp(idGrid).store.groupers.keys[0];
                }
                for (var i = 0, it = Ext.getCmp(idGrid).store.data.items, l = it.length; i < l; i++) {

                    if (!Ext.isEmpty(groupField)) {
                        if (groupVal != Ext.getCmp(idGrid).store.getAt(i).get(groupField)) {
                            groupVal = Ext.getCmp(idGrid).store.getAt(i).get(groupField);
                            t += generateEmptyGroupRow(groupField, "Category: " + ddsm_CategoryColumnRenderer(groupVal), cellType, includeHidden);
                        }
                    }
                    t += '<Row>';
                    var cellClass = (i & 1) ? 'odd' : 'even';
                    r = it[i].data;
                    var k = 0;
                    for (var j = 0; j < colCount; j++) {
                        if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && (includeHidden || !cm[j].hidden)) {
                            var v = r[cm[j].dataIndex];
                            if(cm[j].dataIndex == "ddsm_Category"){v = ddsm_CategoryColumnRenderer(v);}
                            if(cm[j].dataIndex == "ddsm_Status"){v = ddsm_StatusColumnRenderer(v);}
                            if(typeof v === 'undefined'){v='';}
                            if (cellType[k] !== "None") {
                                //t += '<Cell ss:StyleID="' + cellClass + cellTypeClass[k] + '"><Data ss:Type="' + cellType[k] + '">';
                                t += '<Cell ss:StyleID="' + cellClass + '"><Data ss:Type="' + 'String' + '">';
                                if (cellType[k] == 'DateTime') {
                                    if(v !== null){t += (Ext.Date.format(v, 'm-d-Y')).toString();}else{t += '';}
                                } else {
                                    if(v !== null){t += (v).toString();}else{t += '';}
                                }
                                t += '</Data></Cell>';
                            }
                            k++;
                        }
                    }
                    t += '</Row>';
                }

                result.xml = t.concat(
                    '</Table>',
                    '<WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">',
                    '<PageLayoutZoom>0</PageLayoutZoom>',
                    '<Selected/>',
                    '<Panes>',
                    '<Pane>',
                    '<Number>3</Number>',
                    '<ActiveRow>2</ActiveRow>',
                    '</Pane>',
                    '</Panes>',
                    '<ProtectObjects>False</ProtectObjects>',
                    '<ProtectScenarios>False</ProtectScenarios>',
                    '</WorksheetOptions>',
                    '</Worksheet>'
                );
                return result;
            };
            // ---- END Excel export code

            var defaultFieldsValue = {};
            dataOk = false;
            saveGridStatus = false;
            var ctrlEnter = false;
            var altEnter = false;
            var ddsm_index_toUpdateFncls = null;
            var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
            var _odataEndpoint = "/XRMServices/2011/OrganizationData.svc";
            var oneday = 1000 * 60 * 60 * 24;
            var startDate = function () {
                var startdateValue = b.Xrm.Page.getAttribute("ddsm_startdate").getValue();
                if (startdateValue != null) {
                    return startdateValue.getTime();
                } else {
                    Ext.Msg.alert("Error", "Start Date of a Project is not valid!");
                    return null;
                }
            }
            var ddsm_projectstatus = b.Xrm.Page.getAttribute("ddsm_projectstatus");
            var ddsm_pendingmilestone = b.Xrm.Page.getAttribute("ddsm_pendingmilestone");
            var ddsm_responsible = b.Xrm.Page.getAttribute("ddsm_responsible");
            var ddsm_pendingactualstart = b.Xrm.Page.getAttribute("ddsm_pendingactualstart");
            var ddsm_pendingtargetend = b.Xrm.Page.getAttribute("ddsm_pendingtargetend");
            var ddsm_pendingmilestoneindex = b.Xrm.Page.getAttribute("ddsm_pendingmilestoneindex");
            var ddsm_phase = b.Xrm.Page.getAttribute("ddsm_phase");
            //var ddsm_completedstatus = b.Xrm.Page.getAttribute("ddsm_completedstatus");
            var ddsm_completedmilestone = b.Xrm.Page.getAttribute("ddsm_completedmilestone");
            //var ddsm_initialoffered = b.Xrm.Page.getAttribute("ddsm_initialoffered");

            var modelGrid, renderTo, dateFormat, heightGrid, borderGrid, titleGrid, sortersGrid;
            if (typeof configJson.entitySchemaName !== 'undefined') { entitySchemaName = configJson.entitySchemaName; } else { entitySchemaName = ""; Ext.Msg.alert("Error", "The entitySchemaName parameter value is missing in the config file. Please specify the correct value in the config file of this grid (Web Resources)."); }
            if (typeof configJson.modelGrid !== 'undefined') { modelGrid = configJson.modelGrid; } else { modelGrid = "mGrid" + randomnumber; }
            if (typeof configJson.idGrid !== 'undefined') { idGrid = configJson.idGrid; } else { idGrid = "idGrid" + randomnumber; }
            if (typeof configJson.renderTo !== 'undefined') { renderTo = configJson.renderTo; } else { renderTo = Ext.getBody(); }
            if (typeof configJson.dateFormat !== 'undefined') { dateFormat = configJson.dateFormat; } else { dateFormat = "m/d/Y"; }
            if (typeof configJson.height !== 'undefined') { heightGrid = configJson.height; } else { heightGrid = 400; }
            if (typeof configJson.border !== 'undefined') { borderGrid = configJson.border; } else { borderGrid = false; }
            if (typeof configJson.title !== 'undefined') { titleGrid = configJson.title; } else { titleGrid = ''; }
            if (typeof configJson.sorters !== 'undefined') { sortersGrid = configJson.sorters; } else { sortersGrid = { property: configJson.entitySchemaName + 'Id', direction: 'ASC' }; }
            if (typeof configJson.entityConcat !== 'undefined') { entityConcat = configJson.entityConcat; }
            if (typeof configJson.fieldConcat !== 'undefined') { fieldConcat = configJson.fieldConcat; }

            _getODataEndpoint = function (entitySchemaName) {
                return Ext.String.format("{0}{1}/{2}Set", b.Xrm.Page.context.getClientUrl(), _odataEndpoint, entitySchemaName);
            }

            var _makeObjFields = function (obj) {
                var fields = [];
                var new_field = {};
                if (obj.type != "lookup") {
                    if (typeof obj.defaultValue !== 'undefined') {
                        defaultFieldsValue[obj.name] = obj.defaultValue;
                    }
                    new_field.name = obj.name;
                    switch (obj.type) {
                        case 'date':
                            new_field.type = obj.type;
                            //new_field.dateFormat = dateFormat;
                            break;
                        case 'number':
                            new_field.type = obj.type;
                            break;
                        case 'boolean':
                            new_field.type = obj.type;
                            break;
                        case 'checkcolumn':
                            new_field.type = 'boolean';
                            break;
                        case 'combobox':
                            new_field.type = 'string';
                            break;
                        case 'currency':
                            new_field.type = 'number';
                            break;
                        default:
                            break;
                    }
                    return new_field;
                } else {
                    defaultFieldsValue[obj.name] = "";
                    new_field.name = obj.name;
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                    new_field.name = obj.name + '__Id';
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__LogicalName'] = Ext.util.Format.lowercase(obj.entityNameLookup);
                    new_field.name = obj.name + '__LogicalName';
                    fields.push(new_field);
                    return fields;
                }
            }
            var _createFields = function () {
                var fields = [];
                var new_field = {};
                new_field.name = entitySchemaName + 'Id';
                defaultFieldsValue[entitySchemaName + 'Id'] = '';
                fields.push(new_field);
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFields(configJson.fields[i].group[j]);
                            if ($.isArray(obj)) {
                                fields = fields.concat(obj);
                            } else {
                                fields.push(obj);
                            }
                        }
                    } else {
                        obj = _makeObjFields(configJson.fields[i]);
                        if ($.isArray(obj)) {
                            fields = fields.concat(obj);
                        } else {
                            fields.push(obj);
                        }
                    }
                }
                return fields;
            }
            var _createColumns = function () {
                var columns = [];
                var hide_columns = [];
                //columns.push(Ext.create('Ext.grid.RowNumberer'));
                var new_column = {};
                var new_editor = {};
                var new_filter = {};
                var renderer = null;
                new_column.dataIndex = entitySchemaName + 'Id';
                new_column.text = 'Id';
                new_column.filterable = false;
                new_column.sortable = false;
                new_column.hidden = true;
                new_column.field = { xtype: 'hidden'};
                new_column.width = 150;
                new_editor.readOnly = true;
                new_editor.allowBlank = true;
                new_column.editor = new_editor;
                hide_columns.push(new_column);
                for (var i = 0; i < configJson.fields.length; i++) {
                    new_column = {};
                    new_column.menuDisabled = true,
                        new_editor = {};
                    new_filter = {};
                    renderer = null;
                    new_column.dataIndex = configJson.fields[i].name;
                    new_column.header = configJson.fields[i].header;
                    new_column.tooltip = configJson.fields[i].header;
                    if (typeof configJson.fields[i].sortable !== 'undefined') {
                        new_column.sortable = configJson.fields[i].sortable;
                    }
                    if (typeof configJson.fields[i].filterable !== 'undefined') {
                        new_column.filterable = configJson.fields[i].filterable;
                    }
                    if (typeof configJson.fields[i].hidden !== 'undefined' && configJson.fields[i].hidden) {
                        new_column.hidden = configJson.fields[i].hidden;
                    }
                    if (typeof configJson.fields[i].flex !== 'undefined') {
                        new_column.flex = configJson.fields[i].flex;
                    } else {
                        if (typeof configJson.fields[i].width !== 'undefined') {
                            new_column.width = configJson.fields[i].width;
                        }
                    }
                    /*
                     if(typeof configJson.fields[i].format !== 'undefined'){
                     new_editor.format = configJson.fields[i].format;
                     }
                     */
                    if (configJson.fields[i].type != "lookup") {
                        switch (configJson.fields[i].type) {
                            case 'date':
                                if (!new_column.hidden) {
                                    if (!configJson.fields[i].readOnly) {
                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        } else {
                                            new_editor.itemId = configJson.fields[i].name;
                                            //new_editor.id = configJson.fields[i].name;
                                            new_editor.xtype = 'datefield';
                                            //new_editor.readOnly = configJson.fields[i].readOnly;
                                            new_editor.allowBlank = configJson.fields[i].allowBlank;
                                            if (configJson.fields[i].name == 'ddsm_ActualEnd') {
                                                new_editor.maxValue = new Date();
                                            }
                                            new_editor.listeners = {
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                            new_column.editor = new_editor;
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    } else {
                                        if (typeof configJson.fields[i].format !== 'undefined') {
                                            new_column.renderer = Ext.util.Format.dateRenderer(configJson.fields[i].format);
                                        } else {
                                            new_column.renderer = Ext.util.Format.dateRenderer(dateFormat);
                                        }
                                    }
                                } else {
                                    //new_editor.xtype = 'datefield';
                                new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                            case 'number':
                                if (!new_column.hidden) {
                                    if (!configJson.fields[i].readOnly) {
                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        } else {
                                            new_editor.itemId = configJson.fields[i].name;
                                            new_editor.xtype = 'numberfield';
                                            //new_editor.readOnly = configJson.fields[i].readOnly;
                                            new_editor.allowBlank = configJson.fields[i].allowBlank;
                                            new_editor.listeners = {
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                            new_column.editor = new_editor;
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    } else if (typeof configJson.fields[i].format !== 'undefined') {
                                        new_column.renderer = Ext.util.Format.numberRenderer(configJson.fields[i].format);
                                    }
                                } else {
                                    new_column.field = { xtype: 'hidden'};
                                    //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                            case 'combobox':
                                if (!new_column.hidden) {
                                    if (!configJson.fields[i].readOnly) {
                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        } else {
                                            new_editor.itemId = configJson.fields[i].name;
                                            new_editor.xtype = 'combo';
                                            //new_editor.readOnly = configJson.fields[i].readOnly;
                                            new_editor.allowBlank = configJson.fields[i].allowBlank;
                                            new_editor.listeners = {
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                            new_column.editor = new_editor;
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    }
                                } else {
                                    new_column.field = { xtype: 'hidden'};
                                    //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                            case 'currency':
                                if (!new_column.hidden) {
                                    if (!configJson.fields[i].readOnly) {
                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        } else {
                                            new_editor.itemId = configJson.fields[i].name;
                                            new_editor.xtype = 'numberfield';
                                            //new_editor.readOnly = configJson.fields[i].readOnly;
                                            new_editor.allowBlank = configJson.fields[i].allowBlank;
                                            new_editor.listeners = {
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                            new_column.editor = new_editor;
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    } else {
                                        new_column.renderer = Ext.util.Format.usMoney;
                                    }
                                } else {
                                    new_column.field = { xtype: 'hidden'};
                                    //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                            case 'boolean':
                                if (!new_column.hidden) {
                                    if (!configJson.fields[i].readOnly) {

                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        } else {
                                            new_editor.itemId = configJson.fields[i].name;
                                            //                                new_editor.xtype = 'textfield';
                                            //new_editor.readOnly = configJson.fields[i].readOnly;
                                            new_editor.allowBlank = configJson.fields[i].allowBlank;
                                            new_editor.listeners = {
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                            new_column.editor = new_editor;
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    }
                                } else {
                                    new_column.field = { xtype: 'hidden'};
                                    //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                            case 'checkcolumn':
                                if (!new_column.hidden) {
                                    new_column.xtype = 'checkcolumn';
                                    new_column.listeners = {
                                        beforecheckchange: beforeCheckChangeSkip
                                    };
                                    //new_column.disabled = true;
                                    if (!configJson.fields[i].readOnly) {

                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    }
                                } else {
                                    new_column.field = { xtype: 'hidden'};
                                    //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                            default:
                                if (!new_column.hidden) {
                                    if (!configJson.fields[i].readOnly) {

                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        } else {
                                            new_editor.itemId = configJson.fields[i].name;
                                            new_editor.xtype = 'textfield';
                                            //new_editor.readOnly = configJson.fields[i].readOnly;
                                            new_editor.allowBlank = configJson.fields[i].allowBlank;
                                            new_column.editor = new_editor;
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    }
                                } else {
                                    new_column.field = { xtype: 'hidden'};
                                    //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                        }
                        if (typeof configJson.fields[i].hidden !== 'undefined' && configJson.fields[i].hidden) {
                            hide_columns.push(new_column);
                        } else {
                            columns.push(new_column);
                        }
                    } else {
                        if (!new_column.hidden) {
                            if (!configJson.fields[i].readOnly) {

                                if (typeof configJson.fields[i].editor !== 'undefined') {
                                    if (typeof configJson.fields[i].editor !== 'object') {
                                        new_column.editor = eval(configJson.fields[i].editor);
                                    } else {
                                        new_column.editor = configJson.fields[i].editor;
                                    }
                                } else {
                                    new_editor.itemId = configJson.fields[i].name;
                                    //new_editor.readOnly = configJson.fields[i].readOnly;
                                    new_editor.allowBlank = configJson.fields[i].allowBlank;
                                    new_editor.listeners = {
                                        //scope:this,
                                        focus: function (e) {
                                            if ((this.value == "" || this.value == null) && lookupActive != e.name) {
                                                lookupActive = e.name;
                                                _getLookupData(e.name);
                                            }
                                        }
                                    };

                                    new_column.editor = new_editor;
                                }
                            }
                            if (typeof configJson.fields[i].renderer !== 'undefined') {
                                if (typeof configJson.fields[i].renderer !== 'object') {
                                    new_column.renderer = eval(configJson.fields[i].renderer);
                                } else {
                                    new_column.renderer = configJson.fields[i].renderer;
                                }
                            } else {
                                new_column.renderer = function (value, metaData, record, rowIdx, colIdx, store, view) {
                                    var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                                    var column = view.getHeaderAtIndex(colIdx);
                                    if (value != null) {
                                        return Ext.String.format(
                                            '<a href="/main.aspx?etn={2}&extraqs=&histKey={3}&id={1}&newWindow=true&pagetype=entityrecord" target="_blank">{0}</a>',
                                            value,
                                            "{" + record.data[column.dataIndex + "__Id"] + "}",
                                            record.data[column.dataIndex + "__LogicalName"],
                                            randomnumber
                                        );
                                    } else { return ''; }
                                };
                            }
                        } else {
                            new_column.field = { xtype: 'hidden'};
                            //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                        }

                        if (typeof configJson.fields[i].hidden !== 'undefined' && configJson.fields[i].hidden) {
                            hide_columns.push(new_column);
                        } else {
                            columns.push(new_column);
                        }

                        new_column = {};
                        new_editor = {};
                        new_column.dataIndex = configJson.fields[i].name + '__Id';
                        new_column.text = configJson.fields[i].name + '__Id';
                        new_column.filterable = false;
                        new_column.sortable = false;
                        new_column.hidden = true;
                        new_column.width = 50;
                        new_column.field = { xtype: 'hidden'};
                        new_editor.itemId = configJson.fields[i].name + '__Id';
                        new_editor.readOnly = true;
                        new_editor.allowBlank = true;
                        new_column.editor = new_editor;
                        hide_columns.push(new_column);

                        new_column = {};
                        new_editor = {};
                        new_column.dataIndex = configJson.fields[i].name + '__LogicalName';
                        new_column.text = configJson.fields[i].name + '__LogicalName';
                        new_column.filterable = false;
                        new_column.sortable = false;
                        new_column.hidden = true;
                        new_column.width = 50;
                        new_column.field = { xtype: 'hidden'};
                        new_editor.itemId = configJson.fields[i].name + '__LogicalName';
                        new_editor.readOnly = true;
                        new_editor.allowBlank = true;
                        new_column.editor = new_editor;
                        hide_columns.push(new_column);
                    }
                }
                return columns.concat(hide_columns);
            }
            var _makeObjFilters = function (obj) {
                var filterType = {};
                if (obj.filterable) {
                    if (typeof obj.filter !== 'undefined') {
                        if (typeof obj.filter !== 'filterTypeect') {
                            filterType = eval(obj.filter);
                        } else {
                            filterType = obj.filter;
                        }
                    } else {
                        switch (obj.type) {
                            case 'date':
                                filterType.type = 'date';
                                break;
                            case 'number':
                                filterType.type = 'numeric';
                                break;
                            case 'combobox':
                                /*
                                 if(typeof obj.editor !== 'undefined'){
                                 if(typeof obj.editor!== 'filterTypeect'){
                                 var cb = eval(obj.editor);
                                 } else {
                                 var cb = obj.editor;
                                 }
                                 filterType.type = 'list';
                                 filterType.labelField = 'nom_categorie';
                                 filterType.options = [
                                 [ "0", "inactive" ],
                                 [ "1", "in work" ],
                                 [ "2", "executed" ],
                                 [ "3", "canceled" ]
                                 ];
                                 }
                                 */
                                break;
                            case 'currency':
                                filterType.type = 'numeric';
                                break;
                            case 'lookup':
                                filterType.type = 'string';
                                break;
                            default:
                                filterType.type = 'string';
                                break;
                        }
                        filterType.dataIndex = obj.name;
                    }
                }
                return filterType;

            }
            var _createFilters = function () {
                var filterObj = {
                    ftype: 'filters',
                    encode: false,
                    local: true,
                    filters: []
                }
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFilters(configJson.fields[i].group[j]);
                            if (obj != null) {
                                if ($.isArray(obj)) {
                                    filterObj.filters = filterObj.filters.concat(obj);
                                } else {
                                    filterObj.filters.push(obj);
                                }
                            }
                        }
                    } else {
                        obj = _makeObjFilters(configJson.fields[i]);
                        if (obj != null) {
                            if ($.isArray(obj)) {
                                filterObj.filters = filterObj.filters.concat(obj);
                            } else {
                                filterObj.filters.push(obj);
                            }
                        }
                    }
                }
                return filterObj;
            };
            GetUserTimeZoneDetails = function () {

                var ODataPath = b.Xrm.Page.context.getClientUrl() + "/XRMServices/2011/OrganizationData.svc";
                var filter = "/UserSettingsSet(guid'" + b.Xrm.Page.context.getUserId() + "')";

                var userSettingReq = new XMLHttpRequest();
                userSettingReq.open('GET', ODataPath + filter, false);
                userSettingReq.setRequestHeader("Accept", "application/json");
                userSettingReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                userSettingReq.send(null);

                var userSetting = this.JSON.parse(userSettingReq.responseText).d;
                var timeZoneBias = 0;
                try {
                    timeZoneBias = userSetting.TimeZoneBias;
                }
                catch (e) {
                    timeZoneBias = 0;
                }

                var timeZoneDayLightBias = 0;

                try {
                    timeZoneDayLightBias = userSetting.TimeZoneDaylightBias;
                }
                catch (e) {
                    timeZoneDayLightBias = 0;
                }

                return [timeZoneBias, timeZoneDayLightBias];
            }
            var _toUTCDate = function (date) {
                return new Date(date.getTime());
            }
            function UTCToLocalTime(d) {
                var date = new Date(d);
                return ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();

            }

            var _getLookupData = function (entityName) {
                entityName_getLookupData = entityName;
                var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                var localname = record.get(entityName + '__LogicalName');
                var objecttypes = b.Xrm.Internal.getEntityCode(localname);
                var lookupURI = "/_controls/lookup/lookupinfo.aspx";
                lookupURI += "?LookupStyle=single";
                lookupURI += "&objecttypes=" + objecttypes;
                lookupURI += "&ShowNewButton=0";
                lookupURI += "&ShowPropButton=1";
                lookupURI += "&browse=false";
                lookupURI += "&AllowFilterOff=0";
                lookupURI += "&DefaultType=" + objecttypes;
                lookupURI += "&DisableQuickFind=0";
                lookupURI += "&DisableViewPicker=0";
                //console.log("lookupURI: " + lookupURI);

                window.setTimeout(function () {
                    var DialogOption = new b.Xrm.DialogOptions;
                    DialogOption.width = 550; DialogOption.height = 550;
                    b.Xrm.Internal.openDialog(lookupURI, DialogOption, null, null, Callback_lkp_popup);
                }, 200);
            }
            function Callback_lkp_popup(lkp_popup) {
                try {
                    if (lkp_popup) {
                        if (lkp_popup.items) {
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData).setValue(lkp_popup.items[0].name);
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData + '__Id').setValue((lkp_popup.items[0].id).replace(/\{|\}/g, ''));
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData + '__LogicalName').setValue(lkp_popup.items[0].typename);
                            var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                            record.set(entityName_getLookupData, lkp_popup.items[0].name);
                            record.set(entityName_getLookupData + '__Id', (lkp_popup.items[0].id).replace(/\{|\}/g, ''));
                            record.set(entityName_getLookupData + '__LogicalName', lkp_popup.items[0].typename);
                        }
                        lookupActive = "";
                    }
                }
                catch (err) {
                    alert(err);
                }
            }
            var qtip = Ext.create('Ext.tip.QuickTip', {});
            rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToMoveEditor: 1,
                autoCancel: false
            });
            var viewConfig = {
                //loadingText: "Loading data ...",
                stripeRows: true,
                //trackOver:false,
                getRowClass: function (record, index, rowParams, store) {
                    if (record.get('ddsm_Status') == '962080001') {
                        return 'activeMS';
                    }
                }
            };
            //// START CHECKBOX SKIP
            function beforeCheckChangeSkip(column, rowIndex, checked, eOpts) {
/*
                var record = Ext.getCmp(idGrid).getStore().getAt(rowIndex);
                if(record.get('ddsm_Status') == 962080001 || record.get('ddsm_ActualEnd') != null) {
                    return false;
                } else {return true;}
*/
                return false;
            }

            function onCheckChangeSkip(column, rowIndex, checked, eOpts) {

                var record = Ext.getCmp(idGrid).getStore().getAt(rowIndex);
                /*
                var idx = parseInt(record.get('ddsm_Index'));

                groupedCategory = Ext.getCmp(idGrid).getStore().isGrouped();
                if (filteringCategory != '000000000') {
                    Ext.getCmp(idGrid).getStore().clearFilter();
                }
                groupingFeature.disable();
                Ext.getCmp(idGrid).getStore().clearGrouping();
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                recordsGrid[idx - 1].set("ddsm_SkipMilestone", record.get('ddsm_SkipMilestone'));
                Ext.getCmp(idGrid).getStore().commitChanges();
*/

                Save_Record(record);
                Ext.getCmp(idGrid).getStore().commitChanges();
/*
                Ext.getCmp(idGrid).getStore().group('ddsm_Category');
                if (filteringCategory != '000000000') {
                    Ext.getCmp(idGrid).getStore().filter('ddsm_Category', filteringCategory, true);
                }
                if (groupedCategory) {
                    groupingFeature.enable();
                } else {
                    groupingFeature.disable();
                }
                Ext.getCmp(idGrid).getView().refresh();

                Ext.getCmp(idGrid).getStore().commitChanges();
*/
                /*
                 saveGridStatus = false;

                 Ext.getCmp(idGrid).getStore().commitChanges();
                 Ext.getCmp(idGrid).getView().refresh()
                 */
                //var record = Ext.getCmp(idGrid).getStore().getAt(rowIndex);
                //console.dir(record);
            }
            //// END CHECKBOX SKIP


            loadData = function (URI, reload) {
                var pg_ID = b.Xrm.Page.data.entity.getId();
                if (pg_ID != "") {

                    if(URI == _getODataEndpoint(entitySchemaName)) {
                        results = [];
                        URI = b.Xrm.Page.context.getClientUrl() + "/XRMServices/2011/OrganizationData.svc/" + entitySchemaName + "Set?"
                        + "$select="
                        + "ddsm_name,"
                        + "ddsm_ApprovalDate,"
                        + "ddsm_Index,"
                        + "ddsm_Skip,"
                        + "ddsm_projectgroupapprovalId,"
                        + "ddsm_ApprovalThresholdCust,"
                        + "ddsm_Responsible"
                        + "&$orderby=ddsm_Index asc"
                        + "&$filter=ddsm_ProjectGroupToProjectGroupApproId/Id eq guid'" + pg_ID + "' and statecode/Value eq 0";
                    }
                    //console.log(URI_);
                    var req = new XMLHttpRequest();
                    req.open("GET", URI, false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.onreadystatechange = function () {
                        if (this.readyState == 4) {
                            req.onreadystatechange = null;
                            if (this.status == 200) {

                                var responseData = JSON.parse(this.responseText).d;
                                //console.log(responseData);
                                var newData = [];
                                var modeResponse = responseData.results;
                                if (modeResponse.length > 0) {
                                    for (var i = 0; i < modeResponse.length; i++) {
                                        //console.dir(modeResponse[i]);

                                        var objRecord = new Object();
                                        objRecord[entitySchemaName + "Id"] = modeResponse[i][entitySchemaName + "Id"];
                                        for (var j = 0; j < configJson.fields.length; j++) {
                                            switch (configJson.fields[j].type) {
                                                case 'date':
                                                    if ((typeof modeResponse[i][configJson.fields[j].name] !== 'undefined') && (modeResponse[i][configJson.fields[j].name] != null)) {
                                                        //objRecord[configJson.fields[j].name] = parseJsonDate(modeResponse[i][configJson.fields[j].name]);
                                                        objRecord[configJson.fields[j].name] = eval((modeResponse[i][configJson.fields[j].name]).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                                    } else if (modeResponse[i][configJson.fields[j].name] == null) { objRecord[configJson.fields[j].name] = modeResponse[i][configJson.fields[j].name] }
                                                    break
                                                case 'lookup':
                                                    if (typeof modeResponse[i][configJson.fields[j].name] !== 'undefined' && modeResponse[i][configJson.fields[j].name].Id != null) {
                                                        objRecord[configJson.fields[j].name] = modeResponse[i][configJson.fields[j].name].Name;
                                                        objRecord[configJson.fields[j].name + '__Id'] = modeResponse[i][configJson.fields[j].name].Id;
                                                        objRecord[configJson.fields[j].name + '__LogicalName'] = modeResponse[i][configJson.fields[j].name].LogicalName;
                                                    } else if (modeResponse[i][configJson.fields[j].name].Id == null || modeResponse[i][configJson.fields[j].name].Id == '') {
                                                        objRecord[configJson.fields[j].name] = '';
                                                        objRecord[configJson.fields[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                                                        objRecord[configJson.fields[j].name + '__LogicalName'] = configJson.fields[j].entityNameLookup;
                                                    }
                                                    break
                                                case 'combobox':
                                                    if (typeof modeResponse[i][configJson.fields[j].name] !== 'undefined') {
                                                        objRecord[configJson.fields[j].name] = modeResponse[i][configJson.fields[j].name].Value;
                                                    }
                                                    break
                                                case 'currency':
                                                    if (typeof modeResponse[i][configJson.fields[j].name] !== 'undefined') {
                                                        objRecord[configJson.fields[j].name] = modeResponse[i][configJson.fields[j].name].Value;
                                                    }
                                                    break
                                                case 'number':
                                                    if (typeof modeResponse[i][configJson.fields[j].name] !== 'undefined') {
                                                        objRecord[configJson.fields[j].name] = modeResponse[i][configJson.fields[j].name];
                                                    }
                                                    break
                                                default:
                                                    if (typeof modeResponse[i][configJson.fields[j].name] !== 'undefined') {
                                                        objRecord[configJson.fields[j].name] = modeResponse[i][configJson.fields[j].name];
                                                    }
                                            }
                                        }
                                        newData.push(objRecord);
                                    }
                                    results = results.concat(newData);
                                    if (responseData.__next) {
                                        //console.log(responseData.__next);
                                        loadData(responseData.__next, reload);
                                    } else {
                                        console.log(">>>>> load data ok! records: " + results.length);
                                        dataOk = true;
                                        if (!reload) {
                                            Ext.define(modelGrid, {
                                                extend: 'Ext.data.Model',
                                                preventFocus: true,
                                                fields: _createFields()
                                            });
                                            Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                            Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            Ext.getCmp(idGrid).getView().refresh();

                                        } else {
                                            Ext.getCmp(idGrid).getStore().loadData(results);
                                            Ext.getCmp(idGrid).filters.clearFilters();
                                            Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            Ext.getCmp(idGrid).getView().refresh();
                                        }
                                        saveGridStatus = true;
                                    }
                                } else {
                                    console.log(">>>>> data entity null!");
                                    dataOk = false;

                                    if (!reload) {
                                        Ext.define(modelGrid, {
                                            extend: 'Ext.data.Model',
                                            preventFocus: true,
                                            fields: _createFields()
                                        });
                                        Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    } else {
                                        Ext.getCmp(idGrid).getStore().loadData(results);
                                        Ext.getCmp(idGrid).filters.clearFilters();
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        //allRecordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                                        Ext.getCmp(idGrid).getView().refresh();
                                    }
                                    saveGridStatus = true;
                                }
                            } else {
                                if (results.length > 0) {
                                    //loadMilestoneGrid(results);
                                    console.log(">>>>> load data ok! records: " + results.length);
                                    dataOk = true;

                                    if (!reload) {
                                        Ext.define(modelGrid, {
                                            extend: 'Ext.data.Model',
                                            preventFocus: true,
                                            fields: _createFields()
                                        });
                                        Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    } else {
                                        Ext.getCmp(idGrid).getStore().loadData(results);
                                        Ext.getCmp(idGrid).filters.clearFilters();
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    }
                                    saveGridStatus = true;
                                }
                            }
                        }
                    };
                    lookupActive = "";
                    req.send(null);
                }
            }

            function Save_Record(record) {
                var guidId = record.data[entitySchemaName + 'Id'];

                var changes = {};
                for (var j = 0; j < configJson.fields.length; j++) {
                    if (configJson.fields[j].saved) {
                        //console.log(">>> " + i +" : " + j + " >>> type: " + configJson.fields[j].type + " | name " + configJson.fields[j].name + " | value " + record.data[configJson.fields[j].name]);
                        if (record.data[configJson.fields[j].name] != null && (((record.data[configJson.fields[j].name]).toString()).replace(/\s+/g, '')).length == 0) { record.data[configJson.fields[j].name] = null; }
                        switch (configJson.fields[j].type) {

                            case 'date':

                                if (record.data[configJson.fields[j].name] != null) {
                                    changes[configJson.fields[j].name] = _toUTCDate(record.data[configJson.fields[j].name]);
                                } else {
                                    changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];
                                }

                                break;
                            case 'lookup':

                                if (record.data[configJson.fields[j].name] == null) { record.data[configJson.fields[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000'; }
                                if (record.data[configJson.fields[j].name + '__Id'] != '00000000-0000-0000-0000-000000000000' && record.data[configJson.fields[j].name] != null) {
                                    var lookup = {};
                                    lookup.Name = record.data[configJson.fields[j].name];
                                    lookup.Id = record.data[configJson.fields[j].name + '__Id'];
                                    lookup.LogicalName = record.data[configJson.fields[j].name + '__LogicalName'];
                                    changes[configJson.fields[j].name] = lookup;
                                } else {
                                    var lookup = {};
                                    lookup.Name = null;
                                    lookup.Id = null;
                                    lookup.LogicalName = null;
                                    changes[configJson.fields[j].name] = lookup;
                                }

                                break;
                            case 'combobox':

                                if (record.data[configJson.fields[j].name] != null) {
                                    changes[configJson.fields[j].name] = { Value: (record.data[configJson.fields[j].name]).toString() };
                                } else {
                                    changes[configJson.fields[j].name] = { Value: record.data[configJson.fields[j].name] };
                                }

                                break;
                            case 'number':

                                if (record.data[configJson.fields[j].name] != null) {
                                    changes[configJson.fields[j].name] = (record.data[configJson.fields[j].name]).toString();
                                } else {
                                    changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];
                                }

                                break;
                            case 'checkcolumn':

                                changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];

                                break;
                            case 'boolean':

                                changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];

                                break;
                            case 'currency':

                                if (record.data[configJson.fields[j].name] != null) {
                                    changes[configJson.fields[j].name] = { Value: (record.data[configJson.fields[j].name]).toString() };
                                } else {
                                    changes[configJson.fields[j].name] = { Value: record.data[configJson.fields[j].name] };
                                }

                                break;

                            default:
                                if (record.data[configJson.fields[j].name] != null) {
                                    changes[configJson.fields[j].name] = (record.data[configJson.fields[j].name]).toString();
                                } else {
                                    changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];
                                }
                                break;
                        }
                    }
                }

                var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";
                //console.log(URI);
                var req = new XMLHttpRequest();
                req.open("POST", URI, false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("X-HTTP-Method", "MERGE");
                req.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        req.onreadystatechange = null;
                        if (this.status == 204) {
                            console.log("Saved Aprovals record Ok!");
                            saveGridStatus = true;
                        }
                    }
                };
                lookupActive = "";
                req.send(JSON.stringify(changes));
                //loadData(_getODataEndpoint(entitySchemaName), false);

            }

            Save_RecordAll = function () {
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                //console.dir(recordsGrid);
                var recordsCount = recordsGrid.length;
                if (recordsCount > 0) {
                    for (var i = 0; i < recordsCount; i++) {
                        var record = recordsGrid[i];
                        var guidId = record.data[entitySchemaName + 'Id'];
                        var changes = {};
                        for (var j = 0; j < configJson.fields.length; j++) {
                            if (configJson.fields[j].saved) {
                                //console.log(">>> " + i +" : " + j + " >>> type: " + configJson.fields[j].type + " | name " + configJson.fields[j].name + " | value " + record.data[configJson.fields[j].name]);
                                if (record.data[configJson.fields[j].name] != null && (((record.data[configJson.fields[j].name]).toString()).replace(/\s+/g, '')).length == 0) { record.data[configJson.fields[j].name] = null; }
                                switch (configJson.fields[j].type) {

                                    case 'date':

                                        if (record.data[configJson.fields[j].name] != null) {
                                            changes[configJson.fields[j].name] = _toUTCDate(record.data[configJson.fields[j].name]);
                                        } else {
                                            changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];
                                        }

                                        break;
                                    case 'lookup':

                                        if (record.data[configJson.fields[j].name] == null) { record.data[configJson.fields[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000'; }
                                        if (record.data[configJson.fields[j].name + '__Id'] != '00000000-0000-0000-0000-000000000000' && record.data[configJson.fields[j].name] != null) {
                                            var lookup = {};
                                            lookup.Name = record.data[configJson.fields[j].name];
                                            lookup.Id = record.data[configJson.fields[j].name + '__Id'];
                                            lookup.LogicalName = record.data[configJson.fields[j].name + '__LogicalName'];
                                            changes[configJson.fields[j].name] = lookup;
                                        } else {
                                            var lookup = {};
                                            lookup.Name = null;
                                            lookup.Id = null;
                                            lookup.LogicalName = null;
                                            changes[configJson.fields[j].name] = lookup;
                                        }

                                        break;
                                    case 'combobox':

                                        if (record.data[configJson.fields[j].name] != null) {
                                            changes[configJson.fields[j].name] = { Value: (record.data[configJson.fields[j].name]).toString() };
                                        } else {
                                            changes[configJson.fields[j].name] = { Value: record.data[configJson.fields[j].name] };
                                        }

                                        break;
                                    case 'number':

                                        if (record.data[configJson.fields[j].name] != null) {
                                            changes[configJson.fields[j].name] = (record.data[configJson.fields[j].name]).toString();
                                        } else {
                                            changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];
                                        }

                                        break;
                                    case 'checkcolumn':

                                        changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];

                                        break;
                                    case 'boolean':

                                        changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];

                                        break;
                                    case 'currency':

                                        if (record.data[configJson.fields[j].name] != null) {
                                            changes[configJson.fields[j].name] = { Value: (record.data[configJson.fields[j].name]).toString() };
                                        } else {
                                            changes[configJson.fields[j].name] = { Value: record.data[configJson.fields[j].name] };
                                        }

                                        break;

                                    default:
                                        if (record.data[configJson.fields[j].name] != null) {
                                            changes[configJson.fields[j].name] = (record.data[configJson.fields[j].name]).toString();
                                        } else {
                                            changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];
                                        }
                                        break;
                                }
                            }
                        }

                        var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";

                        //console.log(URI);
                        var req = new XMLHttpRequest();
                        req.open("POST", URI, false);
                        req.setRequestHeader("Accept", "application/json");
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader("X-HTTP-Method", "MERGE");
                        req.onreadystatechange = function () {
                            if (this.readyState == 4) {
                                req.onreadystatechange = null;
                                if (this.status == 204) {
                                }
                            }
                        };
                        lookupActive = "";
                        req.send(JSON.stringify(changes));
                    }
                    console.log(">>>>> save data all records");
                    saveGridStatus = true;
                }
            }
            function Delete_Record(records, grid) {
                for (var i = 0; i < records.length; i++) {
                    var guidId = records[i].data[entitySchemaName + 'Id'];
                    if (guidId != "") {
                        var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";
                        //console.log(URI);
                        var req = new XMLHttpRequest();
                        req.open("POST", URI, false);
                        req.setRequestHeader("Accept", "application/json");
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader("X-HTTP-Method", "DELETE");
                        req.onreadystatechange = function () {
                            if (this.readyState == 4) {
                                req.onreadystatechange = null;
                                //console.log(this.status);
                                if ((this.status == 204) || (this.status == 1223)) {
                                    console.log("DELETE: " + guidId);
                                }
                            }
                        };
                        lookupActive = "";
                        req.send(null);
                    }
                }
                grid.getStore().commitChanges();
                Ext.getCmp(idGrid).getView().refresh();

            }


            var _createStore = function (data, fields) {
                return Ext.create('Ext.data.Store', {
                    storeId: 'store' + idGrid,
                    model: modelGrid,
                    autoDestroy: true,
                    fields: fields,
                    data: data,
                    autoLoad: false,
                    autoSync: false,
                    sortOnLoad: true,
                    remoteSort: false,
                    remoteFilter: false,
                    simpleSortMode: true,
                    //groupField: 'ddsm_Category',
                    //remoteGroup: true,
                    sorters: sortersGrid,
                    proxy: {
                        type: 'memory',
                        reader: {
                            type: 'json'
                        }
                    }
                });
            };
            var grid = Ext.create('Ext.grid.Panel', {
                id: idGrid,
                border: borderGrid,
                loadMask: true,
                restful: true,
                autoSizeGrid: true,
                features: [_createFilters()],
                columns: [],
                selType: 'checkboxmodel',
                renderTo: renderTo,
                autoExpandColumn: 'lastUpdated',
                autoScroll: true,
                autoWidth: true,
                height: heightGrid,
                title: titleGrid,
                frame: false,
                plugins: [rowEditing],
                viewConfig: viewConfig,
                tbar: [{
                    itemId: 'saveAll',
                    icon: 'accentgold_/EditableGrid/icons/grid/save.gif',
                    text: 'Save',
                    handler: function () {
                        Save_RecordAll();
                        Ext.Msg.alert("Status", "Your changes saved!");
                    }
                }, '-', {
                    itemId: 'reload',
                    icon: 'accentgold_/EditableGrid/icons/grid/reload.png',
                    text: 'Reload',
                    handler: function () {
                        if (!saveGridStatus) {
                            Ext.Msg.confirm('Confirmation', 'You have not saved changes to the grid Approval! Are you sure you want to reload Approval data grid?', function (btn, text) {
                                if (btn == 'yes') {
                                    loadData(_getODataEndpoint(entitySchemaName), false);
                                }
                            });
                        } else
                        loadData(_getODataEndpoint(entitySchemaName), false);
                    }
                }, '-',{
                    itemId: 'excel',
                    text: 'Export to Excel',
                    icon: 'accentgold_/EditableGrid/icons/grid/excel.png',
                    handler: function(b, e) {
                        exportExcelXml();
                    }
                }],
                listeners: {
                    selectionchange: function (view, records) {
                        //var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                    },
                    edit: function (editor, e) {
                        e.record.commit();

                        ApprovalDate_onChange((parseInt(e.record.get('ddsm_Index')) - 1 >= 0) ? parseInt(e.record.get('ddsm_Index')) - 1 : 0);
                        //Save_RecordAll();

                        if (altEnter) {
                            var rowIndex = e.rowIdx;
                            var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                            var recordsCount = recordsGrid.length;
                            while (recordsGrid[rowIndex + 1].get("ddsm_Skip") == true) {
                                rowIndex++;
                            }
                            rowEditing.startEdit(rowIndex + 1, 7);
                            altEnter = false;
                        }

                    },
                    canceledit: function (editor, e) {
                        lookupActive = "";
                        if (e.record.data[entitySchemaName + 'Id'] == "") { e.grid.getStore().removeAt(e.rowIdx); }
                    },
                        beforeedit: function (editor, e) {
                            var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                            var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                            if(!b.AGS.Users.UserRole.isAdmin() && !b.AGS.Users.UserTeam.verify(record.data['ddsm_Responsible'])) {Ext.Msg.alert("Alert", "Access denied: not enough rights.");return false;}
                            if (b.Xrm.Page.getAttribute("ddsm_projectgroupstatus").getValue() == 962080000) {
                                if (b.Xrm.Page.getAttribute("ddsm_requirecompletedprojectstoapprove").getValue()) {
                                    editor.getEditor().child('[name="ddsm_ApprovalDate"]').disable();
                                    Ext.Msg.alert("Alert", "Some project is not complete!");
                                } else {
                                    editor.getEditor().child('[name="ddsm_ApprovalDate"]').enable();
                                }
                            } else {
                                editor.getEditor().child('[name="ddsm_ApprovalDate"]').enable();
                            }
                            if(record.data['ddsm_Index'] != 1){
                                if(recordsGrid[record.data['ddsm_Index'] - 2].data['ddsm_ApprovalDate'] == null) {editor.getEditor().child('[name="ddsm_ApprovalDate"]').disable();} else {
                                    editor.getEditor().child('[name="ddsm_ApprovalDate"]').enable();
                                }
                            } else {
                                editor.getEditor().child('[name="ddsm_ApprovalDate"]').enable();
                            }

                            window.setTimeout(function () {
                            $("#" + idGrid).find("input").keydown(function (event) {
                                if (event.altKey && event.keyCode == 13) {
                                    //console.log("Hey! Alt+ENTER event captured!");
                                    altEnter = true;
                                    event.preventDefault();
                                }
                            });
                        }, 100);

                    }
                }
            });
            new Ext.util.KeyMap(Ext.getBody(),
                [{
                    key: Ext.EventObject.E,
                    ctrl: true,
                    scope: this,
                    defaultEventAction: 'preventDefault',
                    fn: function (e) {
                        //console.log("Hey! Ctrl+E event captured!");
                        var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                        if (typeof record !== 'undefined') {
                            rowEditing.startEdit(record.index, 7);
                        } else { Ext.Msg.alert("Error with editing", "No Approval record has been selected!"); }
                    }
                }, {
                    key: Ext.EventObject.S,
                    ctrl: true,
                    scope: this,
                    defaultEventAction: 'preventDefault',
                    fn: function (e) {
                        //console.log("Hey! Ctrl+S event captured!");
                        Save_RecordAll();
                        Ext.Msg.alert("Status", "Your changes saved!");
                    }
                }]);


            function ApprovalDate_onChange(rowIdx) {
                //console.log(rowIdx);
                if(rowIdx < 0) {rowIdx = 0;}
                //console.log("start ApprovalDate_onChange");
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                //console.dir(recordsGrid);

                var tempObj = {};
                tempObj = checkApprovalSkipGeneral(rowIdx);
                if(tempObj.nextIndex < recordsGrid.length) {
                    b.Xrm.Page.getAttribute("ddsm_availabletoapprovesince").setValue(new Date);
                    b.Xrm.Page.getAttribute("ddsm_pendingapproval").setValue(recordsGrid[tempObj.nextIndex].data['ddsm_name']);
                    b.Xrm.Page.getAttribute("ddsm_responsible").setValue(recordsGrid[tempObj.nextIndex].data['ddsm_Responsible']);
                }
                if(rowIdx + 1 == recordsGrid.length) {b.Xrm.Page.getAttribute("ddsm_projectgroupstatus").setValue(962080002);updateFncl(recordsGrid[rowIdx].data['ddsm_ApprovalDate'])}
                b.Xrm.Page.data.save();
                Save_RecordAll();

            }
            function checkApprovalSkipGeneral(rowIdx) {
                //console.log("start checkApprovalSkipGeneral");

                //Only look at Approval Threshold for only Custom
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var recordsCount = recordsGrid.length;

                var pg_ID = b.Xrm.Page.data.entity.getId();

                var projData = projQuery(pg_ID);
                var proj = null;
                if (projData.results[0] != null) {
                    proj = projData.results[0].ddsm_project_ProjectGroup.results;
                }

                var incentive = 0.0;
                if (proj[0] != null) {
                    for (var i in proj) {
                        if (!isNaN(parseFloat(proj[i].ddsm_CurrentIncentiveTotal.Value))) {
                                incentive += parseFloat(proj[i].ddsm_CurrentIncentiveTotal.Value);
                        }
                    }
                }

                var apprThresh = 0;
                var prevActEnd;
                var actEnd;
                var index;
                var oldRowIdx = 0;
                var nextRowIdx;
                var nextIndex = -1;

                var runApprSkip = true;
                var keepSkipping = false;

                var newCMI = -1;
                for (var i = 0; i < recordsCount; i++) {
                    if (recordsGrid[i].get("ddsm_ApprovalThresholdCust") != null) {
                        apprThresh = parseFloat(recordsGrid[i].get("ddsm_ApprovalThresholdCust"));
                    }
                    index = parseInt(recordsGrid[i].get("ddsm_Index"));
                    actEnd = recordsGrid[i].get("ddsm_ApprovalDate");
                    if (index > 1) {
                        prevActEnd = recordsGrid[oldRowIdx].get("ddsm_ApprovalDate");
                    } else {
                        prevActEnd = null;
                    }

                    oldRowIdx = i;
                    if (recordsGrid[rowIdx].get("ddsm_Index") == index) {
                        nextIndex = index;
                        nextRowIdx = i;
                    }

                    if (apprThresh == -1) {
                        keepSkipping = false;
                        continue;
                    }

                    runApprSkip = true;
                    if ((apprThresh != -2) && (incentive >= apprThresh)) {
                        runApprSkip = false;
                    }

                    if (runApprSkip == true) {
                        approvalSkip(prevActEnd, actEnd, i);
                        if ((recordsGrid[rowIdx].get("ddsm_Index") + 1) == index) {
                            keepSkipping = true;
                        }
                    } else {
                        approvalUnSkip(i);
                        keepSkipping = false;
                    }

                    if ((keepSkipping) && (index > recordsGrid[rowIdx].get("ddsm_Index"))) {
                        nextIndex = index;
                    }
                }

                var tempObj = new Object();
                tempObj.rowIdx = nextRowIdx;
                tempObj.nextIndex = nextIndex;
                if (nextIndex != -1) {

                }
                return tempObj;
            }
            function approvalSkip(prevActEnd, actEnd, rowIdx) {
                //console.log("start approvalSkip");
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                dtPrev = prevActEnd;
                dtAct = actEnd

                recordsGrid[rowIdx].set("ddsm_SkipMilestone", true);

                if (dtPrev != null) {
                    recordsGrid[rowIdx].set("ddsm_ActualEnd", prevActEnd);
                }
            }
            function approvalUnSkip(rowIdx) {
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                recordsGrid[rowIdx].set("ddsm_SkipMilestone", false);
            }

            loadData(_getODataEndpoint(entitySchemaName), false);

            function projQuery(pg_ID) {
                var serverUrl = Xrm.Page.context.getClientUrl();
                var req = new XMLHttpRequest();
                req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projectgroupSet?"
                + "$select="
                + "ddsm_project_ProjectGroup/ddsm_CurrentIncentiveTotal"
                + "&$expand=ddsm_project_ProjectGroup"
                + "&$filter=ddsm_projectgroupId eq guid'" + pg_ID + "' and statecode/Value eq 0", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.send();
                if (req.readyState == 4 && req.status == 200) {
                    var oProj = JSON.parse(req.responseText).d;

                } else if (req.readyState == 4 && req.status != 200) {
                    alert("Error: Approval Threshold: Bad Query: " + req.responseText);
                    oProj = null;
                }
                return oProj;
            }

            function updateFncl(actStart){
                var pg_ID = b.Xrm.Page.data.entity.getId();

                var projData = projQuery(pg_ID);
                var proj = null;
                if (projData.results[0] != null) {
                    proj = projData.results[0].ddsm_project_ProjectGroup.results;
                }

                var IncentiveTotal = 0.0;
                if (proj[0] != null) {
                    for (var i in proj) {
                        if (!isNaN(parseFloat(proj[i].ddsm_CurrentIncentiveTotal.Value))) {
                            IncentiveTotal += parseFloat(proj[i].ddsm_CurrentIncentiveTotal.Value);
                        }
                    }
                }

                var req = new XMLHttpRequest();
                var serverUrl = Xrm.Page.context.getClientUrl();
                //Query if this ID is already in use
                req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projectgroupSet?"
                + "$select="
                + "ddsm__financial_multisiteproject/ddsm_financialId,"
                + "ddsm__financial_multisiteproject/ddsm_name,"
                + "ddsm__financial_multisiteproject/ddsm_status,"
                + "ddsm__financial_multisiteproject/ddsm_stagename1,"
                + "ddsm__financial_multisiteproject/ddsm_stagename2,"
                + "ddsm__financial_multisiteproject/ddsm_stagename3,"
                + "ddsm__financial_multisiteproject/ddsm_stagename4,"
                + "ddsm__financial_multisiteproject/ddsm_stagename5,"
                + "ddsm__financial_multisiteproject/ddsm_duration1,"
                + "ddsm__financial_multisiteproject/ddsm_duration2,"
                + "ddsm__financial_multisiteproject/ddsm_duration3,"
                + "ddsm__financial_multisiteproject/ddsm_duration4,"
                + "ddsm__financial_multisiteproject/ddsm_duration5,"
                + "ddsm__financial_multisiteproject/ddsm_actualend1,"
                + "ddsm__financial_multisiteproject/ddsm_actualend2,"
                + "ddsm__financial_multisiteproject/ddsm_actualend3,"
                + "ddsm__financial_multisiteproject/ddsm_actualend4,"
                + "ddsm__financial_multisiteproject/ddsm_actualend5"
                + "&$filter=ddsm_projectgroupId eq guid'" + pg_ID + "'"
                + "&$expand=ddsm__financial_multisiteproject", false);

                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.send();

                if (req.readyState == 4 && req.status == 200) {
                    var projGroup = JSON.parse(req.responseText).d;
                    if (projGroup.results[0] != null) {
                        var Fncls = projGroup.results[0].ddsm__financial_multisiteproject.results;

                        var startDate = null;
                        var thisStartDate = null;

                        for(var i = 0; i <Fncls.length; i++) {

                            if (typeof Fncls[i].ddsm_financialId != 'undefined') {
                                if (actStart != null) {
                                    startDate = new Date(actStart);
                                } else {
                                    startDate = new Date();
                                }
                                var fncl_obj = new Object();
                                if (Fncls[i].ddsm_stagename1 != null) {
                                    if (Fncls[i].ddsm_status.Value == 962080000) {
                                        fncl_obj.ddsm_status = new Object();
                                        fncl_obj.ddsm_status.Value = 962080001;
                                        fncl_obj.ddsm_pendingstage = Fncls[i].ddsm_stagename1;
                                        fncl_obj.ddsm_actualstart1 = startDate;
                                    }
                                    //console.log(startDate);
                                    fncl_obj.ddsm_targetstart1 = new Date(startDate.getTime());
                                    thisStartDate = new Date(startDate.getTime() + (Fncls[i].ddsm_duration1 * 86400000));
                                    fncl_obj.ddsm_targetend1 = new Date(thisStartDate.getTime());
                                }
                                if (Fncls[i].ddsm_stagename2 != null) {
                                    if (Fncls[i].ddsm_actualend1 != null) {
                                        thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend1).substr(6)));
                                        //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                                    }
                                    fncl_obj.ddsm_targetstart2 = new Date(thisStartDate.getTime());
                                    thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration2 * 86400000));
                                    fncl_obj.ddsm_targetend2 = new Date(thisStartDate.getTime());
                                }
                                if (Fncls[i].ddsm_stagename3 != null) {
                                    if (Fncls[i].ddsm_actualend2 != null) {
                                        thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend2).substr(6)));
                                        //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                                    }
                                    fncl_obj.ddsm_targetstart3 = new Date(thisStartDate.getTime());
                                    thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration3 * 86400000));
                                    fncl_obj.ddsm_targetend3 = new Date(thisStartDate.getTime());
                                }
                                if (Fncls[i].ddsm_stagename4 != null) {
                                    if (Fncls[i].ddsm_actualend3 != null) {
                                        thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend3).substr(6)));
                                        //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                                    }
                                    fncl_obj.ddsm_targetstart4 = new Date(thisStartDate.getTime());
                                    thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration4 * 86400000));
                                    fncl_obj.ddsm_targetend4 = new Date(thisStartDate.getTime());
                                }
                                if (Fncls[i].ddsm_stagename5 != null) {
                                    if (Fncls[i].ddsm_actualend4 != null) {
                                        thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend4).substr(6)));
                                        //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                                    }
                                    fncl_obj.ddsm_targetstart5 = new Date(thisStartDate.getTime());
                                    thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration5 * 86400000));
                                    fncl_obj.ddsm_targetend5 = new Date(thisStartDate.getTime());
                                }
                                fncl_obj.ddsm_calculatedamount = {};
                                fncl_obj.ddsm_calculatedamount.Value = parseFloat(IncentiveTotal).toFixed(2);
                                b.gen_H_updateEntitySync(Fncls[i].ddsm_financialId, fncl_obj, "ddsm_financialSet");
                            }
                        }
                        var webResource = b.Xrm.Page.ui.controls.get("WebResource_fncl_ProjGroup");
                        webResource.setSrc(webResource.getSrc());

                    }
                }
            }

        });

        // Refresh Grid
        reloadGrid = function () {
            loadData(_getODataEndpoint(entitySchemaName), true);
        }
        get_dataOkgrid = function () {
            return dataOk;
        }
        get_saveGridStatus = function () {
            return saveGridStatus;
        }
        refreshGrid = function () {
            Ext.getCmp(idGrid).getView().refresh();
        }
        $(b).resize(function () {
            Ext.getCmp(idGrid).getView().refresh();
        });
    });
} else {

}