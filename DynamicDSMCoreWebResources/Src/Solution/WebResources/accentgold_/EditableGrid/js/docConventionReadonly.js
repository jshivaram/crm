/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 1.1.1
 * Last updated on 05/21/2015.
 * Document Name Convention Grid ReadOnly
 */
(function (fallback) {

    fallback = fallback || function () { };

    var trap = function () {
        var args = Array.prototype.slice.call(arguments);
        var message = args.join(' ');
        fallback(message);
    };

    if (typeof console === 'undefined') {
        console = {
            messages: [],
            raw: [],
            dump: function () { return console.messages.join('\n'); },
            log: trap,
            debug: trap,
            info: trap,
            warn: trap,
            error: trap,
            assert: trap,
            clear: function () {
                console.messages.length = 0;
                console.raw.length = 0;
            },
            dir: trap,
            dirxml: trap,
            trace: trap,
            group: trap,
            groupCollapsed: trap,
            groupEnd: trap,
            time: trap,
            timeEnd: trap,
            timeStamp: trap,
            profile: trap,
            profileEnd: trap,
            count: trap,
            exception: trap,
            table: trap
        };
    }

})(null);

var b = window.parent;
if (b.Xrm.Page.context.getQueryStringParameters().id != "") {
    var dataOk, results = [], loadData, _getODataEndpoint, entitySchemaName, saveGridStatus, lookupActive, get_saveGridStatus, get_dataOkgrid, refreshGrid, reloadGrid, idGrid, filesCount = 0;

    $(b.document).ready(function () {

        Ext.Loader.setConfig({
            enabled: true,
            disableCaching: false,
            path: { 'Ext': 'accentgold_/EditableGrid/js/' }
        });
        Ext.Loader.setPath('Ext.ux', 'js/ux');
        Ext.require([
            'Ext.grid.*',
            'Ext.data.*',
            'Ext.ux.grid.FiltersFeature',
            'Ext.ux.grid.menu.ListMenu',
            'Ext.util.*',
            'Ext.state.*',
            'Ext.form.*',
            'Ext.tip.QuickTip',
            'Ext.selection.CheckboxModel'
        ]);

        Ext.override('Ext.ux.grid.FiltersFeature', {
            show: function () {
                var lastArgs = null;
                return function () {
                    if (!arguments) {
                        this.callParent(lastArgs);
                    } else {
                        lastArgs = arguments;
                        if (this.loadOnShow && !this.loaded) {
                            this.store.load();
                        }
                        this.callParent(arguments);
                    }
                };
            }()
        });
        Ext.onReady(function () {

            ///// START ACTION COLUMNS
            ///// DELETE COLUMN
            var delActColumn = {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                items: [{
                    icon: 'icons/grid/delete.gif',
                    tooltip: 'Delete Document',
                    handler: function (grid, rowIndex) {
                        rowEditing.cancelEdit();
                        var record = [];
                        record.push(grid.getStore().getAt(rowIndex));
                        Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete select records?', function (btn, text) {
                            if (btn == 'yes') {
                                Delete_Record(record, grid);
                                grid.getStore().removeAt(rowIndex);
                            }
                        });
                    }
                }]
            };
            var editActColumn = {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                items: [{
                    icon: 'icons/grid/edit.png',
                    tooltip: 'Edit Document',
                    handler: function (grid, rowIndex) {
                        rowEditing.cancelEdit();
                        var record = grid.getStore().getAt(rowIndex);
                        window.open(Xrm.Page.context.getClientUrl() + "/notes/edit.aspx?id=" + record.data["AnnotationId"]);
                    }
                }]
            };
            ///// END ACTION COLUMNS


            var defaultFieldsValue = {};
            dataOk = false;
            saveGridStatus = false;
            var hide_columns = [];
            var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
            var _odataEndpoint = "/XRMServices/2011/OrganizationData.svc";

            var modelGrid, renderTo, dateFormat, heightGrid, borderGrid, titleGrid, sortersGrid;
            if (typeof configJson.entitySchemaName !== 'undefined') { entitySchemaName = configJson.entitySchemaName; } else { entitySchemaName = ""; Ext.Msg.alert("Error", "The entitySchemaName parameter value is missing in the config file. Please specify the correct value in the config file of this grid (Web Resources)."); }
            if (typeof configJson.modelGrid !== 'undefined') { modelGrid = configJson.modelGrid; } else { modelGrid = "mGrid" + randomnumber; }
            if (typeof configJson.idGrid !== 'undefined') { idGrid = configJson.idGrid; } else { idGrid = "idGrid" + randomnumber; }
            if (typeof configJson.renderTo !== 'undefined') { renderTo = configJson.renderTo; } else { renderTo = Ext.getBody(); }
            if (typeof configJson.dateFormat !== 'undefined') { dateFormat = configJson.dateFormat; } else { dateFormat = "m/d/Y"; }
            if (typeof configJson.height !== 'undefined') { heightGrid = configJson.height; } else { heightGrid = 400; }
            if (typeof configJson.border !== 'undefined') { borderGrid = configJson.border; } else { borderGrid = false; }
            if (typeof configJson.title !== 'undefined') { titleGrid = configJson.title; } else { titleGrid = ''; }
            if (typeof configJson.sorters !== 'undefined') { sortersGrid = configJson.sorters; } else { sortersGrid = { property: configJson.entitySchemaName + 'Id', direction: 'ASC' }; }

            _getODataEndpoint = function (entitySchemaName) {
                return Ext.String.format("{0}{1}/{2}Set", Xrm.Page.context.getClientUrl(), _odataEndpoint, entitySchemaName);
            }

            var _configLoadSaveData = function () {
                var _config = [];
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            _config.push(configJson.fields[i].group[j]);
                        }
                    } else {
                        _config.push(configJson.fields[i]);
                    }
                }
                return _config;
            }
            var modConfigLoadSaveData = _configLoadSaveData();

            var _makeObjFields = function (obj) {
                var fields = [];
                var new_field = {};
                if (obj.type != "lookup") {
                    if (typeof obj.defaultValue !== 'undefined') {
                        defaultFieldsValue[obj.name] = obj.defaultValue;
                    }
                    new_field.name = obj.name;
                    switch (obj.type) {
                        case 'date':
                            new_field.type = obj.type;
                            //new_field.dateFormat = dateFormat;
                            break;
                        case 'number':
                            new_field.type = obj.type;
                            break;
                        case 'boolean':
                            new_field.type = obj.type;
                            break;
                        case 'checkcolumn':
                            new_field.type = 'boolean';
                            break;
                        case 'combobox':
                            new_field.type = 'string';
                            break;
                        case 'currency':
                            new_field.type = 'number';
                            break;
                        default:
                            break;
                    }
                    return new_field;
                } else {
                    defaultFieldsValue[obj.name] = "";
                    new_field.name = obj.name;
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                    new_field.name = obj.name + '__Id';
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__LogicalName'] = Ext.util.Format.lowercase(obj.entityNameLookup);
                    new_field.name = obj.name + '__LogicalName';
                    fields.push(new_field);
                    return fields;
                }
            }
            var _createFields = function () {
                var fields = [];
                var new_field = {};
                new_field.name = entitySchemaName + 'Id';
                defaultFieldsValue[entitySchemaName + 'Id'] = '';
                fields.push(new_field);
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFields(configJson.fields[i].group[j]);
                            if ($.isArray(obj)) {
                                fields = fields.concat(obj);
                            } else {
                                fields.push(obj);
                            }
                        }
                    } else {
                        obj = _makeObjFields(configJson.fields[i]);
                        if ($.isArray(obj)) {
                            fields = fields.concat(obj);
                        } else {
                            fields.push(obj);
                        }
                    }
                }
                return fields;
            }
            var _makeObjColumns = function (obj) {
                var new_column = {};
                var new_editor = {};
                var new_filter = {};
                var renderer = null;
                if (obj.type != "lookup") {
                    new_column.dataIndex = obj.name;
                    new_column.header = obj.header;
                    new_column.tooltip = obj.header;
                    //new_column.draggable = false;
                    if (typeof obj.sortable !== 'undefined') {
                        new_column.sortable = obj.sortable;
                    }
                    if (typeof obj.filterable !== 'undefined') {
                        new_column.filterable = obj.filterable;
                    }
                    if (typeof obj.hidden !== 'undefined') {
                        new_column.hidden = obj.hidden;
                    }
                    if (typeof obj.flex !== 'undefined') {
                        new_column.flex = obj.flex;
                    } else {
                        if (typeof obj.width !== 'undefined') {
                            new_column.width = obj.width;
                        }
                    }
                    /*
                     if(typeof obj.format !== 'undefined'){
                     new_editor.format = obj.format;
                     }
                     */
                    switch (obj.type) {
                        case 'date':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'datefield';
                                        //new_editor.format = dateFormat;
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else {
                                    if (typeof obj.format !== 'undefined') {
                                        new_column.renderer = Ext.util.Format.dateRenderer(obj.format);
                                    } else {
                                        new_column.renderer = Ext.util.Format.dateRenderer(dateFormat);
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'number':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'numberfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else if (typeof obj.format !== 'undefined') {
                                    new_column.renderer = Ext.util.Format.numberRenderer(obj.format);
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'combobox':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'combo';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'currency':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'numberfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else {
                                    new_column.renderer = Ext.util.Format.usMoney;
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'boolean':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        //                                new_editor.xtype = 'textfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'checkcolumn':
                            if (!new_column.hidden) {
                                new_column.xtype = 'checkcolumn';
                                new_column.listeners = {
                                    checkChange: onCheckChange
                                };
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        default:
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'textfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                    }
                    if (typeof obj.hidden !== 'undefined' && obj.hidden) {
                        hide_columns.push(new_column);
                        return null;
                    } else {
                        return new_column;
                    }
                } else {

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name + '__Id';
                    new_column.header = obj.name + '__Id';
                    new_column.filterable = false;
                    new_column.sortable = false;
                    new_column.hidden = true;
                    new_column.width = 50;
                    new_editor.itemId = obj.name + '__Id';
                    new_editor.readOnly = true;
                    new_editor.allowBlank = true;
                    new_editor.xtype = 'hidden';
                    new_column.editor = new_editor;
                    hide_columns.push(new_column);

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name + '__LogicalName';
                    new_column.header = obj.name + '__LogicalName';
                    new_column.filterable = false;
                    new_column.sortable = false;
                    new_column.hidden = true;
                    new_column.width = 50;
                    new_editor.itemId = obj.name + '__LogicalName';
                    new_editor.readOnly = true;
                    new_editor.allowBlank = true;
                    new_editor.xtype = 'hidden';
                    new_column.editor = new_editor;
                    hide_columns.push(new_column);

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name;
                    new_column.header = obj.header;
                    new_column.tooltip = obj.header;
                    //new_column.draggable = false;
                    if (typeof obj.sortable !== 'undefined') {
                        new_column.sortable = obj.sortable;
                    }
                    if (typeof obj.filterable !== 'undefined') {
                        new_column.filterable = obj.filterable;
                    }
                    if (typeof obj.hidden !== 'undefined') {
                        new_column.hidden = obj.hidden;
                    }
                    if (typeof obj.flex !== 'undefined') {
                        new_column.flex = obj.flex;
                    } else {
                        if (typeof obj.width !== 'undefined') {
                            new_column.width = obj.width;
                        }
                    }
                    /*
                     if(typeof obj.format !== 'undefined'){
                     new_editor.format = obj.format;
                     }
                     */
                    if (!new_column.hidden) {
                        if (!obj.readOnly) {

                            if (typeof obj.editor !== 'undefined') {
                                if (typeof obj.editor !== 'object') {
                                    new_column.editor = eval(obj.editor);
                                } else {
                                    new_column.editor = obj.editor;
                                }
                            } else {
                                new_editor.itemId = obj.name;
                                //new_editor.readOnly = obj.readOnly;
                                new_editor.allowBlank = obj.allowBlank;
                                new_editor.listeners = {
                                    //scope:this,
                                    focus: function (e) {
                                        if ((this.value == "" || this.value == null) && lookupActive != e.name) {
                                            lookupActive = e.name;
                                            _getLookupData(e.name);
                                        }
                                    }
                                };

                                new_column.editor = new_editor;
                            }
                        }
                        if (typeof obj.renderer !== 'undefined') {
                            if (typeof obj.renderer !== 'object') {
                                new_column.renderer = eval(obj.renderer);
                            } else {
                                new_column.renderer = obj.renderer;
                            }
                        } else {
                            new_column.renderer = function (value, metaData, record, rowIdx, colIdx, store, view) {
                                var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                                var column = view.getHeaderAtIndex(colIdx);
                                if (value != null) {
                                    return Ext.String.format(
                                        '<a href="/main.aspx?etn={2}&extraqs=&histKey={3}&id={1}&newWindow=true&pagetype=entityrecord" target="_blank">{0}</a>',
                                        value,
                                        "{" + record.data[column.dataIndex + "__Id"] + "}",
                                        record.data[column.dataIndex + "__LogicalName"],
                                        randomnumber
                                    );
                                } else { return ''; }
                            };
                        }
                    } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }

                    if (typeof obj.hidden !== 'undefined' && obj.hidden) {
                        hide_columns.push(new_column);
                        return null;
                    } else {
                        return new_column;
                    }
                }

            }
            var _createColumns = function () {
                var columns = [];
                //hide_columns = [];
                columns.push(Ext.create('Ext.grid.RowNumberer'));
                var new_column = {};
                var new_editor = {};
                var new_filter = {};
                var renderer = null;
                new_column.dataIndex = entitySchemaName + 'Id';
                new_column.header = 'Id';
                new_column.filterable = false;
                new_column.sortable = false;
                new_column.hidden = true;
                new_column.width = 150;
                new_editor.readOnly = true;
                new_editor.allowBlank = true;
                new_editor.xtype = 'hidden';
                new_column.editor = new_editor;
                hide_columns.push(new_column);

                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        var group = {};
                        group.header = configJson.fields[i].groupHeader;
                        group.menuDisabled = true,
                            group.columns = [];
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjColumns(configJson.fields[i].group[j]);
                            if (obj != null) {
                                if ($.isArray(obj)) {
                                    group.columns = group.columns.concat(obj);
                                } else {
                                    group.columns.push(obj);
                                }
                            }
                        }
                        columns.push(group);
                    } else {
                        obj = _makeObjColumns(configJson.fields[i]);
                        if (obj != null) {
                            if ($.isArray(obj)) {
                                columns = columns.concat(obj);
                            } else {
                                columns.push(obj);
                            }
                        }
                    }
                }
                //columns.push(editActColumn);
                //columns.push(delActColumn);
                columns = columns.concat(hide_columns);
                return columns;
            }
            var _makeObjFilters = function (obj) {
                var filterType = {};
                if (obj.filterable) {
                    if (typeof obj.filter !== 'undefined') {
                        if (typeof obj.filter !== 'filterTypeect') {
                            filterType = eval(obj.filter);
                        } else {
                            filterType = obj.filter;
                        }
                    } else {
                        switch (obj.type) {
                            case 'date':
                                filterType.type = 'date';
                                break;
                            case 'number':
                                filterType.type = 'numeric';
                                break;
                            case 'boolean':
                                filterType.type = 'boolean';
                                break;
                            case 'currency':
                                filterType.type = 'numeric';
                                break;
                            case 'lookup':
                                filterType.type = 'string';
                                break;
                            default:
                                filterType.type = 'string';
                                break;
                        }
                        filterType.dataIndex = obj.name;
                    }
                }
                return filterType;

            }
            var _createFilters = function () {
                var filterObj = {
                    ftype: 'filters',
                    encode: false,
                    local: true,
                    filters: []
                }
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFilters(configJson.fields[i].group[j]);
                            if (obj != null) {
                                if ($.isArray(obj)) {
                                    filterObj.filters = filterObj.filters.concat(obj);
                                } else {
                                    filterObj.filters.push(obj);
                                }
                            }
                        }
                    } else {
                        obj = _makeObjFilters(configJson.fields[i]);
                        if (obj != null) {
                            if ($.isArray(obj)) {
                                filterObj.filters = filterObj.filters.concat(obj);
                            } else {
                                filterObj.filters.push(obj);
                            }
                        }
                    }
                }
                return filterObj;
            };
            var _createStore = function (data, fields) {
                return Ext.create('Ext.data.Store', {
                    model: modelGrid,
                    autoDestroy: true,
                    fields: fields,
                    data: data,
                    autoLoad: false,
                    autoSync: false,
                    sortOnLoad: true,
                    remoteSort: false,
                    remoteFilter: false,
                    simpleSortMode: true,
                    sorters: sortersGrid,
                    proxy: {
                        type: 'memory',
                        reader: {
                            type: 'json'
                        }
                    }
                });
            };
            var _toUTCDate = function (date) {
                return new Date(date.getTime());
            }
            function UTCToLocalTime(d) {
                var date = new Date(d);
                return ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();

            }

            var qtip = Ext.create('Ext.tip.QuickTip', {});
            var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToMoveEditor: 1,
                autoCancel: false
            });
            var viewConfig = {
                //loadingText: "Loading data ...",
                stripeRows: true,
                //trackOver:false,
                getRowClass: function (record, index, rowParams, store) {
                    if (record.get('ddsm_Uploaded') == true) {
                        return 'aploaded';
                    }
                }
            };

            loadData = function (URI, reload) {
                var entity_ID = Xrm.Page.context.getQueryStringParameters().id;
                if (entity_ID !== "") {
                        var filter= "";
                        if(URI == _getODataEndpoint(entitySchemaName)) {
                            results = [];
                        if(Xrm.Page.context.getQueryStringParameters().typename == 'ddsm_project'){
                            filter = "?$filter=ddsm_ProjectId/Id eq guid'" + entity_ID + "'";
                        }
                        if(Xrm.Page.context.getQueryStringParameters().typename == 'ddsm_projecttemplate'){
                            filter = "?$filter=ddsm_ProjectTemplateId/Id eq guid'" + entity_ID + "'";
                        }
                        if(Xrm.Page.context.getQueryStringParameters().typename == 'ddsm_measuretemplate'){
                            filter = "?$filter=ddsm_MeasureTemplateId/Id eq guid'" + entity_ID + "'";
                        }
                        if(Xrm.Page.context.getQueryStringParameters().typename == 'ddsm_milestoneset'){
                            filter = "?$filter=ddsm_MilestoneSet/Id eq guid'" + entity_ID + "'";
                        }
                        if(Xrm.Page.context.getQueryStringParameters().typename == 'ddsm_modelnumber'){
                            filter = "?$filter=ddsm_ModelNumber/Id eq guid'" + entity_ID + "'";
                        }
                        if(Xrm.Page.context.getQueryStringParameters().typename == 'ddsm_sku'){
                            filter = "?$filter=ddsm_SKU/Id eq guid'" + entity_ID + "'";
                        }
                        URI = URI + filter;
                    }
                    //console.log(URI + filter);
                    Ext.getCmp(idGrid).mask("Loading data, please wait...");
                    var req = new XMLHttpRequest();
                    req.open("GET", URI, false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.onreadystatechange = function () {
                        if (this.readyState == 4) {
                            req.onreadystatechange = null;
                            if (this.status == 200) {

                                var responseData = JSON.parse(this.responseText).d;
                                //console.log(responseData);
                                var newData = [];
                                var modeResponse = responseData.results;
                                if (modeResponse.length > 0) {

                                    for (var i = 0; i < modeResponse.length; i++) {
                                        //                        console.dir(modeResponse[i]);

                                        var objRecord = new Object();
                                        objRecord[entitySchemaName + "Id"] = modeResponse[i][entitySchemaName + "Id"];
                                        for (var j = 0; j < modConfigLoadSaveData.length; j++) {
                                            switch (modConfigLoadSaveData[j].type) {
                                                case 'date':
                                                    if ((typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') && (modeResponse[i][modConfigLoadSaveData[j].name] != null)) {
                                                        objRecord[modConfigLoadSaveData[j].name] = eval((modeResponse[i][modConfigLoadSaveData[j].name]).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                                    } else if (modeResponse[i][modConfigLoadSaveData[j].name] == null) { objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name] }
                                                    break
                                                case 'lookup':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined' && modeResponse[i][modConfigLoadSaveData[j].name].Id != null) {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name].Name;
                                                        objRecord[modConfigLoadSaveData[j].name + '__Id'] = modeResponse[i][modConfigLoadSaveData[j].name].Id;
                                                        objRecord[modConfigLoadSaveData[j].name + '__LogicalName'] = modeResponse[i][modConfigLoadSaveData[j].name].LogicalName;
                                                    } else if (modeResponse[i][modConfigLoadSaveData[j].name].Id == null || modeResponse[i][modConfigLoadSaveData[j].name].Id === '') {
                                                        objRecord[modConfigLoadSaveData[j].name] = '';
                                                        objRecord[modConfigLoadSaveData[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                                                        objRecord[modConfigLoadSaveData[j].name + '__LogicalName'] = modConfigLoadSaveData[j].entityNameLookup;
                                                    }
                                                    break
                                                case 'combobox':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name].Value;
                                                    }
                                                    break
                                                case 'currency':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name].Value;
                                                    }
                                                    break
                                                case 'number':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name];
                                                    }
                                                    break
                                                default:
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name];
                                                    }
                                            }
                                        }
                                        newData.push(objRecord);
                                    }
                                    results = results.concat(newData);
                                    if (responseData.__next) {
                                        //console.log(responseData.__next);
                                        Ext.getCmp(idGrid).unmask();
                                        loadData(responseData.__next, reload);
                                    } else {
                                        console.log(">>>>> load data ok! records: " + results.length);
                                        dataOk = true;

                                        if (!reload) {
                                            Ext.define(modelGrid, {
                                                extend: 'Ext.data.Model',
                                                fields: _createFields()
                                            });
                                            Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                            Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            Ext.getCmp(idGrid).getView().refresh();
                                        } else {
                                            Ext.getCmp(idGrid).getStore().loadData(results);
                                            Ext.getCmp(idGrid).filters.clearFilters();
                                            Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            Ext.getCmp(idGrid).getView().refresh();
                                        }
                                        Ext.getCmp(idGrid).unmask();
                                    }
                                } else {
                                    console.log(">>>>> data entity null!");
                                    dataOk = false;
                                    if (!reload) {
                                        Ext.define(modelGrid, {
                                            extend: 'Ext.data.Model',
                                            fields: _createFields()
                                        });
                                        Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    } else {
                                        Ext.getCmp(idGrid).getStore().loadData(results);
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    }
                                    Ext.getCmp(idGrid).unmask();
                                }
                            }
                        }
                    };
                    lookupActive = "";
                    req.send(null);
                }
            }

            function Delete_Record(records, grid) {
                var popupHTML = '<div id="overlay" style="display:none; position:fixed; z-index:998; opacity:0.5; filter:alpha(opacity=50); top:0; right:0; left:0; bottom:0; background:#000000;"></div>';
                popupHTML += '<div id="loading" style="display:none; position:fixed; width:320px; height:320px; top:50%; left:50%; margin:-160px 0 0 -160px; z-index:999;"><img src="/WebResources/ddsm_loading.gif" alt="loading"/></div>';
                $('body').append(popupHTML);
                $('#overlay, #loading').show();
                filesCount = records.length;
                for (var i = 0; i < records.length; i++) {
                    CrmRestKit.Delete(entitySchemaName, records[i].data[entitySchemaName + 'Id']).done(function (data, status, xhr) {
                        //console.dir(data);
                        filesCount--;
                        if (filesCount == 0) {
                            $('#overlay, #loading').hide();
                            $('#overlay').remove(); $('#loading').remove();
                            grid.getStore().commitChanges();
                            Ext.getCmp(idGrid).getView().refresh();
                        }
                    });
                }
            }

            var grid = Ext.create('Ext.grid.Panel', {
                id: idGrid,
                border: borderGrid,
                loadMask: true,
                restful: true,
                autoSizeGrid: true,
                columns: [],
                selType: 'checkboxmodel',
                renderTo: "docList",
                autoExpandColumn: 'lastUpdated',
                features: [_createFilters()],
                autoScroll: true,
                autoWidth: true,
                //height: heightGrid,
                height: '190',
                //title: titleGrid,
                title: 'Document Uploaded List',
                frame: false,
                viewConfig: viewConfig,
                plugins: [rowEditing],
                tbar: [{
                    itemId: 'reload',
                    icon: 'icons/grid/reload.png',
                    text: 'Reload',
                    handler: function () {
                        loadData(_getODataEndpoint(entitySchemaName), true);
                    }
                }],
                listeners: {
                    selectionchange: function (view, records) {
                        //Ext.getCmp(idGrid).down('#remove').setDisabled(!records.length);
                    },
                    edit: function (editor, e) {
                        e.record.commit();
                        var obj = {};
                        var objLookup = {};
                        if(e.record.get('ddsm_documentconventionId') == "" || e.record.get('ddsm_documentconventionId') == '00000000-0000-0000-0000-000000000000'){
                            if(Xrm.Page.context.getQueryStringParameters().typename == 'ddsm_project'){
                                objLookup = {Id: e.record.get('ddsm_ProjectId__Id'), Name: e.record.get('ddsm_ProjectId'), LogicalName: e.record.get('ddsm_ProjectId__LogicalName')};
                                obj = { ddsm_RequiredByStatus: e.record.get('ddsm_RequiredByStatus'), ddsm_name: e.record.get('ddsm_name'), ddsm_Uploaded: e.record.get('ddsm_Uploaded'), ddsm_ProjectId: objLookup };
                            }
                            if(Xrm.Page.context.getQueryStringParameters().typename == 'ddsm_projecttemplate'){
                                objLookup = {Id: e.record.get('ddsm_ProjectTemplateId__Id'), Name: e.record.get('ddsm_ProjectTemplateId'), LogicalName: e.record.get('ddsm_ProjectTemplateId__LogicalName')};
                                obj = { ddsm_RequiredByStatus: e.record.get('ddsm_RequiredByStatus'), ddsm_name: e.record.get('ddsm_name'), ddsm_Uploaded: e.record.get('ddsm_Uploaded'), ddsm_ProjectTemplateId: objLookup };
                            }
                            if(Xrm.Page.context.getQueryStringParameters().typename == 'ddsm_measuretemplate'){
                                objLookup = {Id: e.record.get('ddsm_MeasureTemplateId__Id'), Name: e.record.get('ddsm_MeasureTemplateId'), LogicalName: e.record.get('ddsm_MeasureTemplateId__LogicalName')};
                                obj = { ddsm_RequiredByStatus: e.record.get('ddsm_RequiredByStatus'), ddsm_name: e.record.get('ddsm_name'), ddsm_Uploaded: e.record.get('ddsm_Uploaded'), ddsm_MeasureTemplateId: objLookup };
                            }
                            if(Xrm.Page.context.getQueryStringParameters().typename == 'ddsm_milestoneset'){
                                objLookup = {Id: e.record.get('ddsm_MilestoneSet__Id'), Name: e.record.get('espMilestoneSet'), LogicalName: e.record.get('ddsm_MilestoneSet__LogicalName')};
                                obj = { ddsm_RequiredByStatus: e.record.get('ddsm_RequiredByStatus'), ddsm_name: e.record.get('ddsm_name'), ddsm_Uploaded: e.record.get('ddsm_Uploaded'), ddsm_MilestoneSet: objLookup };
                            }
                            if(Xrm.Page.context.getQueryStringParameters().typename == 'ddsm_modelnumber'){
                                objLookup = {Id: e.record.get('ddsm_ModelNumber__Id'), Name: e.record.get('ddsm_ModelNumber'), LogicalName: e.record.get('ddsm_ModelNumber__LogicalName')};
                                obj = { ddsm_RequiredByStatus: e.record.get('ddsm_RequiredByStatus'), ddsm_name: e.record.get('ddsm_name'), ddsm_Uploaded: e.record.get('ddsm_Uploaded'), ddsm_ModelNumber: objLookup };
                            }
                            if(Xrm.Page.context.getQueryStringParameters().typename == 'ddsm_sku'){
                                objLookup = {Id: e.record.get('ddsm_SKU__Id'), Name: e.record.get('ddsm_SKU'), LogicalName: e.record.get('ddsm_SKU__LogicalName')};
                                obj = { ddsm_RequiredByStatus: e.record.get('ddsm_RequiredByStatus'), ddsm_name: e.record.get('ddsm_name'), ddsm_Uploaded: e.record.get('ddsm_Uploaded'), ddsm_SKU: objLookup };
                            }
                            CrmRestKit.Create(entitySchemaName, obj, !1).fail(onRestError).done(function (data, status, xhr) {
                            });
                        }else{
                            obj = { ddsm_RequiredByStatus: e.record.get('ddsm_RequiredByStatus'), ddsm_name: e.record.get('ddsm_name'), ddsm_Uploaded: e.record.get('ddsm_Uploaded') };
                            CrmRestKit.Update(entitySchemaName, e.record.get(entitySchemaName + 'Id'), obj, !1).fail(onRestError).done(function (data, status, xhr) {
                            });
                        }


                    },
                    canceledit: function (editor, e) {
                        if (e.record.data[entitySchemaName + 'Id'] == "") { e.grid.getStore().removeAt(e.rowIdx); }
                        Ext.getCmp(idGrid).getView().refresh();
                    },
                    beforeedit: function (editor, e) {return false;},
                    afterrender: function() {
                        spinnerForm.stop();
                        loadData(_getODataEndpoint(entitySchemaName), false);
                    }
                }
            });

            //loadData(_getODataEndpoint(entitySchemaName), false);

            function onRestError() { alert("Error Create/Update Document Name Convention record."); }

        });

        // Refresh Grid
        reloadGrid = function () {
            loadData(_getODataEndpoint(entitySchemaName), true);
        }
        get_dataOkgrid = function () {
            return dataOk;
        }
        get_saveGridStatus = function () {
            return saveGridStatus;
        }
        refreshGrid = function () {
            Ext.getCmp(idGrid).getView().refresh();
        }
        $(b).resize(function () {
            Ext.getCmp(idGrid).getView().refresh();
        });
    });
}