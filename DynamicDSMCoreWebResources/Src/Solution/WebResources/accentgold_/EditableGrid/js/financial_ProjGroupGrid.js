/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 1.0.1
 * Last updated on 06/29/2015.
 * Project Group Financials Grid
 */
(function (fallback) {

    fallback = fallback || function () { };

    var trap = function () {
        var args = Array.prototype.slice.call(arguments);
        var message = args.join(' ');
        fallback(message);
    };

    if (typeof console === 'undefined') {
        console = {
            messages: [],
            raw: [],
            dump: function () { return console.messages.join('\n'); },
            log: trap,
            debug: trap,
            info: trap,
            warn: trap,
            error: trap,
            assert: trap,
            clear: function () {
                console.messages.length = 0;
                console.raw.length = 0;
            },
            dir: trap,
            dirxml: trap,
            trace: trap,
            group: trap,
            groupCollapsed: trap,
            groupEnd: trap,
            time: trap,
            timeEnd: trap,
            timeStamp: trap,
            profile: trap,
            profileEnd: trap,
            count: trap,
            exception: trap,
            table: trap
        };
    }

})(null);

var b = window.parent;
if (b.Xrm.Page.getAttribute("ddsm_projectgrouptemplates").getValue() != null) {
    var dataOk, results = [], loadData, _getODataEndpoint, entitySchemaName, saveGridStatus, lookupActive, FNCL_NUM_STAGES = 5, get_saveGridStatus, get_dataOkgrid, refreshGrid, reloadGrid, idGrid, entityName_getLookupData;

    $(b.document).ready(function () {

        // ---- START Excel export code
        var Base64 = (function() {
            // Private property
            var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

            // Private method for UTF-8 encoding

            function utf8Encode(string) {
                string = string.replace(/\r\n/g, "\n");
                var utftext = "";
                for (var n = 0; n < string.length; n++) {
                    var c = string.charCodeAt(n);
                    if (c < 128) {
                        utftext += String.fromCharCode(c);
                    } else if ((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                    } else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                }
                return utftext;
            }

            // Public method for encoding
            return {
                encode: (typeof btoa == 'function') ? function(input) {
                    return btoa(utf8Encode(input));
                } : function(input) {
                    var output = "";
                    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
                    var i = 0;
                    input = utf8Encode(input);
                    while (i < input.length) {
                        chr1 = input.charCodeAt(i++);
                        chr2 = input.charCodeAt(i++);
                        chr3 = input.charCodeAt(i++);
                        enc1 = chr1 >> 2;
                        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                        enc4 = chr3 & 63;
                        if (isNaN(chr2)) {
                            enc3 = enc4 = 64;
                        } else if (isNaN(chr3)) {
                            enc4 = 64;
                        }
                        output = output +
                        keyStr.charAt(enc1) + keyStr.charAt(enc2) +
                        keyStr.charAt(enc3) + keyStr.charAt(enc4);
                    }
                    return output;
                }
            };
        })();
        // ---- END Excel export code

        Ext.Loader.setConfig({
            enabled: true,
            disableCaching: false,
            path: { 'Ext': 'accentgold_/EditableGrid/js/' }
        });
        Ext.Loader.setPath('Ext.ux', 'accentgold_/EditableGrid/js/ux');
        Ext.require([
            'Ext.grid.*',
            'Ext.data.*',
            'Ext.ux.grid.FiltersFeature',
            'Ext.ux.grid.menu.ListMenu',
            'Ext.util.*',
            'Ext.state.*',
            'Ext.form.*',
            'Ext.tip.QuickTip',
            'Ext.selection.CheckboxModel'
        ]);

        Ext.override('Ext.ux.grid.FiltersFeature', {
            show: function () {
                var lastArgs = null;
                return function () {
                    if (!arguments) {
                        this.callParent(lastArgs);
                    } else {
                        lastArgs = arguments;
                        if (this.loadOnShow && !this.loaded) {
                            this.store.load();
                        }
                        this.callParent(arguments);
                    }
                };
            }()
        });
        Ext.onReady(function () {
            // ---- START Excel export code
            var reg = /[^\.\,\;\:\?\!\"\'\(\)\+\№\%\#\@\$\[\]\{\}\~\^\&\*\/\\\|\<\>\s]+/g;
            var strName = b.Xrm.Page.getAttribute("ddsm_name").getValue();
            strName = strName.match(reg).join(' ');

            var exportExcelXml = function(includeHidden, title) {

                if (!title) {title = "Project Financials"}

                var vExportContent = getExcelXml(includeHidden, title);
                var location = 'data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);
                if (!Ext.isIE) {
                    var gridEl = Ext.getCmp(idGrid).getEl();

                    var el = Ext.DomHelper.append(gridEl, {
                        tag: "a",
                        download: strName + " - " + title + " - " + Ext.Date.format(new Date(), 'm-d-Y H-i') + '.xls',
                        href: location
                    });
                    el.click();
                    Ext.fly(el).destroy();
                } else {
                    var fileName = strName + " - " + title + " - " + Ext.Date.format(new Date(), 'm-d-Y H-i') + '.xls';
                    var form = Ext.getCmp(idGrid).down('form#uploadForm');
                    if (form) {
                        form.destroy();
                    }
                    form = Ext.getCmp(idGrid).add({
                        xtype: 'form',
                        itemId: 'uploadForm',
                        hidden: true,
                        standardSubmit: true,
                        url: 'http://dynamicdsm.richlode.biz/crmIExls.asmx/GetXsl',
                        items: [{
                            xtype: 'hiddenfield',
                            name: 'Name',
                            value: fileName
                        },{
                            xtype: 'hiddenfield',
                            name: 'strXml',
                            value: Base64.encode(vExportContent)
                        }]
                    });

                    form.getForm().submit();

//                    alert("The xls export isn't supported by IE.");
                }

            };

            var getExcelXml = function(includeHidden, title) {
                var theTitle = title || this.title;
                var worksheet = createWorksheet(includeHidden, theTitle);
                var totalWidth =  Ext.getCmp(idGrid).columnManager.columns.length;
                return ''.concat(
                    '<?xml version="1.0"?>',
                    '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">',
                    '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Title>' + theTitle + '</Title></DocumentProperties>',
                    '<OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office"><AllowPNG/></OfficeDocumentSettings>',
                    '<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">',
                    '<WindowHeight>' + worksheet.height + '</WindowHeight>',
                    '<WindowWidth>' + 12585 + '</WindowWidth>',
                    '<ProtectStructure>False</ProtectStructure>',
                    '<ProtectWindows>False</ProtectWindows>',
                    '</ExcelWorkbook>',

                    '<Styles>',

                    '<Style ss:ID="Default" ss:Name="Normal">',
                    '<Alignment ss:Vertical="Bottom"/>',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>',
                    '<Interior/>',
                    '<NumberFormat/>',
                    '<Protection/>',
                    '</Style>',

                    '<Style ss:ID="title">',
                    '<Borders />',
                    '<Font ss:Bold="1" ss:Size="18" />',
                    '<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1" />',
                    '<Interior ss:Color="#E0ECF0" ss:Pattern="Solid" />',
                    '<NumberFormat ss:Format="@" />',
                    '</Style>',

                    '<Style ss:ID="headercell">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:Bold="1" ss:Size="10" />',
                    '<Alignment ss:Horizontal="Center" ss:WrapText="1" />',
                    '<Interior ss:Color="#CFE1E8" ss:Pattern="Solid" />',
                    '</Style>',


                    '<Style ss:ID="even">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Interior ss:Color="#FFFFFF" ss:Pattern="Solid" />',
                    '</Style>',

                    /*
                     '<Style ss:ID="evendate" ss:Parent="even">',
                     '<NumberFormat ss:Format="m-d-yyyy" />',
                     '</Style>',

                     '<Style ss:ID="evenint" ss:Parent="even">',
                     '<Numberformat ss:Format="0" />',
                     '</Style>',

                     '<Style ss:ID="evenfloat" ss:Parent="even">',
                     '<Numberformat ss:Format="0.00" />',
                     '</Style>',
                     */

                    '<Style ss:ID="odd">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Interior ss:Color="#F1F7F8" ss:Pattern="Solid" />',
                    '</Style>',

                    '<Style ss:ID="groupSeparator">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:Bold="1" ss:Size="10" />',
                    '<Interior ss:Color="#D3D3D3" ss:Pattern="Solid" />',
                    '</Style>',
                    /*
                     '<Style ss:ID="odddate" ss:Parent="odd">',
                     '<NumberFormat ss:Format="m-d-yyyy" />',
                     '</Style>',
                     '<Style ss:ID="oddint" ss:Parent="odd">',
                     '<NumberFormat Format="0" />',
                     '</Style>',

                     '<Style ss:ID="oddfloat" ss:Parent="odd">',
                     '<NumberFormat Format="0.00" />',
                     '</Style>',
                     */


                    '</Styles>',
                    worksheet.xml,
                    '</Workbook>'
                );
            };

            var getModelField = function(fieldName) {
                var fields = Ext.getCmp(idGrid).store.model.getFields();
                for (var i = 0; i < fields.length; i++) {
                    if (fields[i].name === fieldName) {
                        return fields[i];
                    }
                }
            };

            var generateEmptyGroupRow = function(dataIndex, value, cellTypes, includeHidden) {
                var cm =  Ext.getCmp(idGrid).columnManager.columns;
                var colCount = cm.length;
                var rowTpl = '<Row ss:AutoFitHeight="0"><Cell ss:StyleID="groupSeparator" ss:MergeAcross="{0}"><Data ss:Type="String"><html:b>{1}</html:b></Data></Cell></Row>';
                var visibleCols = 0;
                // rowXml += '<Cell ss:StyleID="groupSeparator">'
                for (var j = 0; j < colCount; j++) {
                    if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && (includeHidden || !cm[j].hidden)) {
                        // rowXml += '<Cell ss:StyleID="groupSeparator"/>';
                        visibleCols++;
                    }
                }
                // rowXml += "</Row>";
                return Ext.String.format(rowTpl, visibleCols - 1, value);
            };


            var createWorksheet = function(includeHidden, theTitle) {
                // Calculate cell data types and extra class names which affect formatting
                var cellType = [];
                var cellTypeClass = [];
                var cm = Ext.getCmp(idGrid).columnManager.columns;

                var totalWidthInPixels = 0;
                var colXml = '';
                var headerXml = '';
                var visibleColumnCountReduction = 0;
                var colCount = cm.length;
                for (var i = 0; i < colCount; i++) {
                    if (cm[i].xtype != 'actioncolumn' && (cm[i].dataIndex != '') && (includeHidden || !cm[i].hidden)) {
                        var w = cm[i].getEl().getWidth();
                        totalWidthInPixels += w;

                        if (cm[i].text === "") {
                            cellType.push("None");
                            cellTypeClass.push("");
                            ++visibleColumnCountReduction;
                        } else {
                            colXml += '<Column ss:AutoFitWidth="1" ss:Width="' + w + '" />';
                            headerXml += '<Cell ss:StyleID="headercell">' +
                            '<Data ss:Type="String">' + cm[i].text + '</Data>' +
                            '<NamedCell ss:Name="Print_Titles"></NamedCell></Cell>';


                            var fld = getModelField(cm[i].dataIndex);
                            switch (fld.type.type) {
                                case "int":
                                    cellType.push("Number");
                                    cellTypeClass.push("int");
                                    break;
                                case "float":
                                    cellType.push("Number");
                                    cellTypeClass.push("float");
                                    break;

                                case "bool":

                                case "boolean":
                                    cellType.push("String");
                                    cellTypeClass.push("");
                                    break;
                                case "date":
                                    cellType.push("DateTime");
                                    cellTypeClass.push("date");
                                    break;
                                default:
                                    cellType.push("String");
                                    cellTypeClass.push("");
                                    break;
                            }
                        }
                    }
                }
                var visibleColumnCount = cellType.length - visibleColumnCountReduction;
                var result = {
                    height: 9000,
                    width: Math.floor(totalWidthInPixels * 30) + 50
                };

                var numGridRows = Ext.getCmp(idGrid).store.getCount() + 2;
                if (!Ext.isEmpty(Ext.getCmp(idGrid).store.groupField) || Ext.getCmp(idGrid).store.groupers.items.length > 0) {
                    numGridRows = numGridRows + Ext.getCmp(idGrid).store.getGroups().length;
                }

                // create header for worksheet
                var t = ''.concat(
                    '<Worksheet ss:Name="' + theTitle + '">',

                    '<Names>',
                    '<NamedRange ss:Name="Print_Titles" ss:RefersTo="=\'' + theTitle + '\'!R1:R2">',
                    '</NamedRange></Names>',

                    '<Table ss:ExpandedColumnCount="' + (visibleColumnCount + 2),
                    '" ss:ExpandedRowCount="' + numGridRows + '" x:FullColumns="1" x:FullRows="1" ss:DefaultColumnWidth="65" ss:DefaultRowHeight="15">',
                    colXml,
                    '<Row ss:Height="38">',
                    '<Cell ss:MergeAcross="' + (visibleColumnCount - 1) + '" ss:StyleID="title">',
                    '<Data ss:Type="String" xmlns:html="http://www.w3.org/TR/REC-html40">',
                    '<html:b>' + strName + ' - '+ theTitle + '</html:b></Data><NamedCell ss:Name="Print_Titles">',
                    '</NamedCell></Cell>',
                    '</Row>',
                    '<Row ss:AutoFitHeight="1">',
                    headerXml +
                    '</Row>'
                );

                var groupVal = "";
                var groupField = "";
                if (Ext.getCmp(idGrid).store.groupers.keys.length > 0) {
                    groupField = Ext.getCmp(idGrid).store.groupers.keys[0];
                }
                for (var i = 0, it = Ext.getCmp(idGrid).store.data.items, l = it.length; i < l; i++) {

                    if (!Ext.isEmpty(groupField)) {
                        if (groupVal != Ext.getCmp(idGrid).store.getAt(i).get(groupField)) {
                            groupVal = Ext.getCmp(idGrid).store.getAt(i).get(groupField);
                            t += generateEmptyGroupRow(groupField, groupVal, cellType, includeHidden);
                        }
                    }
                    t += '<Row>';
                    var cellClass = (i & 1) ? 'odd' : 'even';
                    r = it[i].data;
                    var k = 0;
                    for (var j = 0; j < colCount; j++) {
                        if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && (includeHidden || !cm[j].hidden)) {
                            var v = r[cm[j].dataIndex];
                            if(cm[j].dataIndex == "ddsm_status"){v = ddsm_StatusColumnRenderer(v);}
                            if(typeof v === 'undefined'){v='';}
                            if (cellType[k] !== "None") {
                                //t += '<Cell ss:StyleID="' + cellClass + cellTypeClass[k] + '"><Data ss:Type="' + cellType[k] + '">';
                                t += '<Cell ss:StyleID="' + cellClass + '"><Data ss:Type="' + 'String' + '">';
                                if (cellType[k] == 'DateTime') {
                                    if(v !== null){t += (Ext.Date.format(v, 'm-d-Y')).toString();}else{t += '';}
                                } else {
                                    if(v !== null){t += (v).toString();}else{t += '';}
                                }
                                t += '</Data></Cell>';
                            }
                            k++;
                        }
                    }
                    t += '</Row>';
                }

                result.xml = t.concat(
                    '</Table>',
                    '<WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">',
                    '<PageLayoutZoom>0</PageLayoutZoom>',
                    '<Selected/>',
                    '<Panes>',
                    '<Pane>',
                    '<Number>3</Number>',
                    '<ActiveRow>2</ActiveRow>',
                    '</Pane>',
                    '</Panes>',
                    '<ProtectObjects>False</ProtectObjects>',
                    '<ProtectScenarios>False</ProtectScenarios>',
                    '</WorksheetOptions>',
                    '</Worksheet>'
                );
                return result;
            };
            // ---- END Excel export code

            ///// START ACTION COLUMNS
            ///// DELETE COLUMN
            var delActColumn = {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                items: [{
                    icon: 'accentgold_/EditableGrid/icons/grid/delete.gif',
                    tooltip: 'Delete record',
                    handler: function (grid, rowIndex) {
                        rowEditing.cancelEdit();
                        var record = [];
                        record.push(grid.getStore().getAt(rowIndex));
                        Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete select records?', function (btn, text) {
                            if (btn == 'yes') {
                                Delete_Record(record, grid);
                                grid.getStore().removeAt(rowIndex);
                            }
                        });
                    }
                }]
            };
            ///// END ACTION COLUMNS


            var defaultFieldsValue = {};
            dataOk = false;
            saveGridStatus = false;
            var hide_columns = [];
            var ctrlEnter = false;
            var altEnter = false;
            var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
            var _odataEndpoint = "/XRMServices/2011/OrganizationData.svc";
            var ddsm_projectstatus = b.Xrm.Page.getAttribute("ddsm_projectstatus");
            var ddsm_pendingmilestone = b.Xrm.Page.getAttribute("ddsm_pendingmilestone");
            var ddsm_responsible = b.Xrm.Page.getAttribute("ddsm_responsible");
            var ddsm_pendingactualstart = b.Xrm.Page.getAttribute("ddsm_pendingactualstart");
            var ddsm_pendingtargetend = b.Xrm.Page.getAttribute("ddsm_pendingtargetend");

            var modelGrid, renderTo, dateFormat, heightGrid, borderGrid, titleGrid, sortersGrid;
            if (typeof configJson.entitySchemaName !== 'undefined') { entitySchemaName = configJson.entitySchemaName; } else { entitySchemaName = ""; Ext.Msg.alert("Error", "The entitySchemaName parameter value is missing in the config file. Please specify the correct value in the config file of this grid (Web Resources)."); }
            if (typeof configJson.modelGrid !== 'undefined') { modelGrid = configJson.modelGrid; } else { modelGrid = "mGrid" + randomnumber; }
            if (typeof configJson.idGrid !== 'undefined') { idGrid = configJson.idGrid; } else { idGrid = "idGrid" + randomnumber; }
            if (typeof configJson.renderTo !== 'undefined') { renderTo = configJson.renderTo; } else { renderTo = Ext.getBody(); }
            if (typeof configJson.dateFormat !== 'undefined') { dateFormat = configJson.dateFormat; } else { dateFormat = "m/d/Y"; }
            if (typeof configJson.height !== 'undefined') { heightGrid = configJson.height; } else { heightGrid = 400; }
            if (typeof configJson.border !== 'undefined') { borderGrid = configJson.border; } else { borderGrid = false; }
            if (typeof configJson.title !== 'undefined') { titleGrid = configJson.title; } else { titleGrid = ''; }
            if (typeof configJson.sorters !== 'undefined') { sortersGrid = configJson.sorters; } else { sortersGrid = { property: configJson.entitySchemaName + 'Id', direction: 'ASC' }; }
            if (typeof configJson.entityConcat !== 'undefined') { entityConcat = configJson.entityConcat; }
            if (typeof configJson.fieldConcat !== 'undefined') { fieldConcat = configJson.fieldConcat; }

            _getODataEndpoint = function (entitySchemaName) {
                return Ext.String.format("{0}{1}/{2}Set", b.Xrm.Page.context.getClientUrl(), _odataEndpoint, entitySchemaName);
            }
            // Add New Milestone in the new window
            function addFN_newWindow() {
                // Data for Parent Project
                var ProjectNumber = b.Xrm.Page.getAttribute("ddsm_name").getValue();
                var histKey = 100000000 + Math.floor(Math.random() * 900000000);

                var guid = b.Xrm.Page.data.entity.getId();
                //console.log("Project Number: " + ProjectNumber + ", Entity Id: " + guid);

                if (ProjectNumber != null) {
                    var features = "location=yes,menubar=no,status=yes,toolbar=yes,resizable=yes";
                    //var extraqs = "?_CreateFromId=" + guid + "&_CreateFromType=10025&etn=ddsm_financial";
                    var extraqs = "?_CreateFromId=" + guid + "&_CreateFromType=" + b.Xrm.Page.context.getQueryStringParameters().etc + "&etc=" + b.Xrm.Internal.getEntityCode("ddsm_financial");
                    //var url = b.Xrm.Page.context.getClientUrl();
                    window.open("/main.aspx?etc=" + b.Xrm.Internal.getEntityCode("ddsm_financial") + "&extraqs=" + encodeURIComponent(extraqs) + "&histKey=" + histKey + "&newWindow=true&pagetype=entityrecord", "New Financial", features, false);
                }
                else { Ext.Msg.alert("Error", "Sorry, the 'Project Number' field is empty."); }
            }
            var _configLoadSaveData = function () {
                var _config = [];
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            _config.push(configJson.fields[i].group[j]);
                        }
                    } else {
                        _config.push(configJson.fields[i]);
                    }
                }
                return _config;
            }
            var modConfigLoadSaveData = _configLoadSaveData();

            var _makeObjFields = function (obj) {
                var fields = [];
                var new_field = {};
                if (obj.type != "lookup") {
                    if (typeof obj.defaultValue !== 'undefined') {
                        defaultFieldsValue[obj.name] = obj.defaultValue;
                    }
                    new_field.name = obj.name;
                    switch (obj.type) {
                        case 'date':
                            new_field.type = obj.type;
                            new_field.dateFormat = dateFormat;
                            break;
                        case 'number':
                            new_field.type = obj.type;
                            break;
                        case 'boolean':
                            new_field.type = obj.type;
                            break;
                        case 'checkcolumn':
                            new_field.type = 'boolean';
                            break;
                        case 'combobox':
                            new_field.type = 'string';
                            break;
                        case 'currency':
                            new_field.type = 'number';
                            break;
                        default:
                            break;
                    }
                    return new_field;
                } else {
                    defaultFieldsValue[obj.name] = "";
                    new_field.name = obj.name;
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                    new_field.name = obj.name + '__Id';
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__LogicalName'] = Ext.util.Format.lowercase(obj.entityNameLookup);
                    new_field.name = obj.name + '__LogicalName';
                    fields.push(new_field);
                    return fields;
                }
            }
            var _createFields = function () {
                var fields = [];
                var new_field = {};
                new_field.name = entitySchemaName + 'Id';
                defaultFieldsValue[entitySchemaName + 'Id'] = '';
                fields.push(new_field);
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFields(configJson.fields[i].group[j]);
                            if ($.isArray(obj)) {
                                fields = fields.concat(obj);
                            } else {
                                fields.push(obj);
                            }
                        }
                    } else {
                        obj = _makeObjFields(configJson.fields[i]);
                        if ($.isArray(obj)) {
                            fields = fields.concat(obj);
                        } else {
                            fields.push(obj);
                        }
                    }
                }
                return fields;
            }
            var _makeObjColumns = function (obj) {
                var new_column = {};
                var new_editor = {};
                var new_filter = {};
                var renderer = null;
                if (obj.type != "lookup") {
                    new_column.dataIndex = obj.name;
                    new_column.header = obj.header;
                    new_column.tooltip = obj.header;
                    //new_column.draggable = false;
                    if (typeof obj.sortable !== 'undefined') {
                        new_column.sortable = obj.sortable;
                    }
                    if (typeof obj.filterable !== 'undefined') {
                        new_column.filterable = obj.filterable;
                    }
                    if (typeof obj.hidden !== 'undefined') {
                        new_column.hidden = obj.hidden;
                    }
                    if (typeof obj.flex !== 'undefined') {
                        new_column.flex = obj.flex;
                    } else {
                        if (typeof obj.width !== 'undefined') {
                            new_column.width = obj.width;
                        }
                    }
                    /*
                     if(typeof obj.format !== 'undefined'){
                     new_editor.format = obj.format;
                     }
                     */
                    switch (obj.type) {
                        case 'date':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'datefield';
                                        //new_editor.format = dateFormat;
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        if (obj.name == 'ddsm_actualend1') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    Fncl_ActEnd_OnChange(1, editor.value);
                                                },
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                        }
                                        if (obj.name == 'ddsm_actualend2') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    Fncl_ActEnd_OnChange(2, editor.value);
                                                },
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                        }
                                        if (obj.name == 'ddsm_actualend3') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    Fncl_ActEnd_OnChange(3, editor.value);
                                                },
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                        }
                                        if (obj.name == 'ddsm_actualend4') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    Fncl_ActEnd_OnChange(4, editor.value);
                                                },
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                        }
                                        if (obj.name == 'ddsm_actualend5') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    Fncl_ActEnd_OnChange(5, editor.value);
                                                },
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                        }
                                        if (obj.name != 'ddsm_actualend1' && obj.name != 'ddsm_actualend2' && obj.name != 'ddsm_actualend3' && obj.name != 'ddsm_actualend4' && obj.name != 'ddsm_actualend5') {
                                            new_editor.listeners = {
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else {
                                    if (typeof obj.format !== 'undefined') {
                                        new_column.renderer = Ext.util.Format.dateRenderer(obj.format);
                                    } else {
                                        new_column.renderer = Ext.util.Format.dateRenderer(dateFormat);
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'number':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'numberfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else if (typeof obj.format !== 'undefined') {
                                    new_column.renderer = Ext.util.Format.numberRenderer(obj.format);
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'combobox':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'combo';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'currency':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'numberfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else {
                                    new_column.renderer = Ext.util.Format.usMoney;
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'boolean':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        //                                new_editor.xtype = 'textfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'checkcolumn':
                            if (!new_column.hidden) {
                                new_column.xtype = 'checkcolumn';
                                new_column.listeners = {
                                    checkChange: onCheckChange
                                };
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        default:
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'textfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                    }
                    if (typeof obj.hidden !== 'undefined' && obj.hidden) {
                        hide_columns.push(new_column);
                        return null;
                    } else {
                        return new_column;
                    }
                } else {

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name + '__Id';
                    new_column.header = obj.name + '__Id';
                    new_column.filterable = false;
                    new_column.sortable = false;
                    new_column.hidden = true;
                    new_column.width = 50;
                    new_editor.itemId = obj.name + '__Id';
                    new_editor.readOnly = true;
                    new_editor.allowBlank = true;
                    new_editor.xtype = 'hidden';
                    new_column.editor = new_editor;
                    hide_columns.push(new_column);

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name + '__LogicalName';
                    new_column.header = obj.name + '__LogicalName';
                    new_column.filterable = false;
                    new_column.sortable = false;
                    new_column.hidden = true;
                    new_column.width = 50;
                    new_editor.itemId = obj.name + '__LogicalName';
                    new_editor.readOnly = true;
                    new_editor.allowBlank = true;
                    new_editor.xtype = 'hidden';
                    new_column.editor = new_editor;
                    hide_columns.push(new_column);

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name;
                    new_column.header = obj.header;
                    new_column.tooltip = obj.header;
                    //new_column.draggable = false;
                    if (typeof obj.sortable !== 'undefined') {
                        new_column.sortable = obj.sortable;
                    }
                    if (typeof obj.filterable !== 'undefined') {
                        new_column.filterable = obj.filterable;
                    }
                    if (typeof obj.hidden !== 'undefined') {
                        new_column.hidden = obj.hidden;
                    }
                    if (typeof obj.flex !== 'undefined') {
                        new_column.flex = obj.flex;
                    } else {
                        if (typeof obj.width !== 'undefined') {
                            new_column.width = obj.width;
                        }
                    }
                    /*
                     if(typeof obj.format !== 'undefined'){
                     new_editor.format = obj.format;
                     }
                     */
                    if (!new_column.hidden) {
                        if (!obj.readOnly) {

                            if (typeof obj.editor !== 'undefined') {
                                if (typeof obj.editor !== 'object') {
                                    new_column.editor = eval(obj.editor);
                                } else {
                                    new_column.editor = obj.editor;
                                }
                            } else {
                                new_editor.itemId = obj.name;
                                //new_editor.readOnly = obj.readOnly;
                                new_editor.allowBlank = obj.allowBlank;
                                new_editor.listeners = {
                                    //scope:this,
                                    focus: function (e) {
                                        if ((this.value == "" || this.value == null) && lookupActive != e.name) {
                                            lookupActive = e.name;
                                            _getLookupData(e.name);
                                        }
                                    }
                                };

                                new_column.editor = new_editor;
                            }
                        }
                        if (typeof obj.renderer !== 'undefined') {
                            if (typeof obj.renderer !== 'object') {
                                new_column.renderer = eval(obj.renderer);
                            } else {
                                new_column.renderer = obj.renderer;
                            }
                        } else {
                            new_column.renderer = function (value, metaData, record, rowIdx, colIdx, store, view) {
                                var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                                var column = view.getHeaderAtIndex(colIdx);
                                if (value != null) {
                                    return Ext.String.format(
                                        '<a href="/main.aspx?etn={2}&extraqs=&histKey={3}&id={1}&newWindow=true&pagetype=entityrecord" target="_blank">{0}</a>',
                                        value,
                                        "{" + record.data[column.dataIndex + "__Id"] + "}",
                                        record.data[column.dataIndex + "__LogicalName"],
                                        randomnumber
                                    );
                                } else { return ''; }
                            };
                        }
                    } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }

                    if (typeof obj.hidden !== 'undefined' && obj.hidden) {
                        hide_columns.push(new_column);
                        return null;
                    } else {
                        return new_column;
                    }
                }

            }
            var _createColumns = function () {
                var columns = [];
                var firstColumns = [];
                var endColumns = [];
                var offeredColumns = [];
                var committedColumns = [];
                var insalledColumns = [];
                //hide_columns = [];
                columns.push(Ext.create('Ext.grid.RowNumberer'));
                var new_column = {};
                var new_editor = {};
                var new_filter = {};
                var renderer = null;
                new_column.dataIndex = entitySchemaName + 'Id';
                new_column.header = 'Id';
                new_column.filterable = false;
                new_column.sortable = false;
                new_column.hidden = true;
                new_column.width = 150;
                new_editor.readOnly = true;
                new_editor.allowBlank = true;
                new_editor.xtype = 'hidden';
                new_column.editor = new_editor;
                hide_columns.push(new_column);

                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        var group = {};
                        group.header = configJson.fields[i].groupHeader;
                        group.menuDisabled = true,
                            group.columns = [];
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjColumns(configJson.fields[i].group[j]);
                            if (obj != null) {
                                if ($.isArray(obj)) {
                                    group.columns = group.columns.concat(obj);
                                } else {
                                    group.columns.push(obj);
                                }
                            }
                        }
                        columns.push(group);
                    } else {
                        obj = _makeObjColumns(configJson.fields[i]);
                        if (obj != null) {
                            if ($.isArray(obj)) {
                                columns = columns.concat(obj);
                            } else {
                                columns.push(obj);
                            }
                        }
                    }
                }
                columns.push(delActColumn);
                columns = columns.concat(hide_columns);
                return columns;
            }
            var _createStore = function (data, fields) {
                return Ext.create('Ext.data.Store', {
                    model: modelGrid,
                    autoDestroy: true,
                    fields: fields,
                    data: data,
                    autoLoad: false,
                    autoSync: false,
                    sortOnLoad: true,
                    remoteSort: false,
                    remoteFilter: false,
                    simpleSortMode: true,
                    sorters: sortersGrid,
                    proxy: {
                        type: 'memory',
                        reader: {
                            type: 'json'
                        }
                    }
                });
            };
            var _makeObjFilters = function (obj) {
                var filterType = {};
                if (obj.filterable) {
                    if (typeof obj.filter !== 'undefined') {
                        if (typeof obj.filter !== 'filterTypeect') {
                            filterType = eval(obj.filter);
                        } else {
                            filterType = obj.filter;
                        }
                    } else {
                        switch (obj.type) {
                            case 'date':
                                filterType.type = 'date';
                                break;
                            case 'number':
                                filterType.type = 'numeric';
                                break;
                            case 'combobox':
                                /*
                                 if(typeof obj.editor !== 'undefined'){
                                 if(typeof obj.editor!== 'filterTypeect'){
                                 var cb = eval(obj.editor);
                                 } else {
                                 var cb = obj.editor;
                                 }
                                 filterType.type = 'list';
                                 filterType.labelField = 'nom_categorie';
                                 filterType.options = [
                                 [ "0", "inactive" ],
                                 [ "1", "in work" ],
                                 [ "2", "executed" ],
                                 [ "3", "canceled" ]
                                 ];
                                 }
                                 */
                                break;
                            case 'currency':
                                filterType.type = 'numeric';
                                break;
                            case 'lookup':
                                filterType.type = 'string';
                                break;
                            default:
                                filterType.type = 'string';
                                break;
                        }
                        filterType.dataIndex = obj.name;
                    }
                }
                return filterType;

            }
            var _createFilters = function () {
                var filterObj = {
                    ftype: 'filters',
                    encode: false,
                    local: true,
                    filters: []
                }
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFilters(configJson.fields[i].group[j]);
                            if (obj != null) {
                                if ($.isArray(obj)) {
                                    filterObj.filters = filterObj.filters.concat(obj);
                                } else {
                                    filterObj.filters.push(obj);
                                }
                            }
                        }
                    } else {
                        obj = _makeObjFilters(configJson.fields[i]);
                        if (obj != null) {
                            if ($.isArray(obj)) {
                                filterObj.filters = filterObj.filters.concat(obj);
                            } else {
                                filterObj.filters.push(obj);
                            }
                        }
                    }
                }
                return filterObj;
            };
            var _toUTCDate = function (date) {
                return new Date(date.getTime());
            }
            function UTCToLocalTime(d) {
                var date = new Date(d);
                return ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();

            }
            var _getLookupData = function (entityName) {
                entityName_getLookupData = entityName;
                var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                var localname = record.get(entityName + '__LogicalName');
                var objecttypes = b.Xrm.Internal.getEntityCode(localname);
                var lookupURI = "/_controls/lookup/lookupinfo.aspx";
                lookupURI += "?LookupStyle=single";
                lookupURI += "&objecttypes=" + objecttypes;
                lookupURI += "&ShowNewButton=0";
                lookupURI += "&ShowPropButton=1";
                lookupURI += "&browse=false";
                lookupURI += "&AllowFilterOff=0";
                lookupURI += "&DefaultType=" + objecttypes;
                lookupURI += "&DisableQuickFind=0";
                lookupURI += "&DisableViewPicker=0";
                //console.log("lookupURI: " + lookupURI);

                window.setTimeout(function () {
                    var DialogOption = new b.Xrm.DialogOptions;
                    DialogOption.width = 550; DialogOption.height = 550;
                    b.Xrm.Internal.openDialog(lookupURI, DialogOption, null, null, Callback_lkp_popup);
                }, 200);
            }
            function Callback_lkp_popup(lkp_popup) {
                try {
                    if (lkp_popup) {
                        if (lkp_popup.items) {
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData).setValue(lkp_popup.items[0].name);
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData + '__Id').setValue((lkp_popup.items[0].id).replace(/\{|\}/g, ''));
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData + '__LogicalName').setValue(lkp_popup.items[0].typename);
                            var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                            record.set(entityName_getLookupData, lkp_popup.items[0].name);
                            record.set(entityName_getLookupData + '__Id', (lkp_popup.items[0].id).replace(/\{|\}/g, ''));
                            record.set(entityName_getLookupData + '__LogicalName', lkp_popup.items[0].typename);
                        }
                        lookupActive = "";
                    }
                }
                catch (err) {
                    alert(err);
                }
            }
            var qtip = Ext.create('Ext.tip.QuickTip', {});
            var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToMoveEditor: 1,
                autoCancel: false
            });
            var viewConfig = {
                //loadingText: "Loading data ...",
                stripeRows: true
                //trackOver:false,
            };
            //// START CHECKBOX
            function onCheckChange(column, rowIndex, checked, eOpts) {
                //var record = Ext.getCmp(idGrid).getStore().getAt(rowIndex);
                //Save check/unchek record row
                //saveGridStatus = false;

                //Ext.getCmp(idGrid).getStore().commitChanges();
                //Ext.getCmp(idGrid).getView().refresh();

                //var record = Ext.getCmp(idGrid).getStore().getAt(rowIndex);
                //console.dir(record);
            }
            //// END CHECKBOX


            loadData = function (URI, reload) {
                var projGroup_ID = b.Xrm.Page.data.entity.getId();
                if (projGroup_ID !== "") {
                    if(URI == _getODataEndpoint(entitySchemaName)) {
                        URI = URI + "?$filter=ddsm_ProjectGroupToProjectGroupFinanId/Id eq guid'" + projGroup_ID + "'";
                        results = [];
                    }

                    //console.log(URI + filter);
                    Ext.getCmp(idGrid).mask("Loading data, please wait...");

                    var req = new XMLHttpRequest();
                    req.open("GET", URI, false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.onreadystatechange = function () {
                        if (this.readyState == 4) {
                            req.onreadystatechange = null;
                            if (this.status == 200) {

                                var responseData = JSON.parse(this.responseText).d;
                                //console.log(responseData);
                                var newData = [];
                                var modeResponse = responseData.results;
                                if (modeResponse.length > 0) {

                                    for (var i = 0; i < modeResponse.length; i++) {
                                        //                        console.dir(modeResponse[i]);

                                        var objRecord = new Object();
                                        objRecord[entitySchemaName + "Id"] = modeResponse[i][entitySchemaName + "Id"];
                                        for (var j = 0; j < modConfigLoadSaveData.length; j++) {
                                            switch (modConfigLoadSaveData[j].type) {
                                                case 'date':
                                                    if ((typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') && (modeResponse[i][modConfigLoadSaveData[j].name] != null)) {
                                                        objRecord[modConfigLoadSaveData[j].name] = eval((modeResponse[i][modConfigLoadSaveData[j].name]).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                                    } else if (modeResponse[i][modConfigLoadSaveData[j].name] == null) { objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name] }
                                                    break
                                                case 'lookup':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined' && modeResponse[i][modConfigLoadSaveData[j].name].Id != null) {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name].Name;
                                                        objRecord[modConfigLoadSaveData[j].name + '__Id'] = modeResponse[i][modConfigLoadSaveData[j].name].Id;
                                                        objRecord[modConfigLoadSaveData[j].name + '__LogicalName'] = modeResponse[i][modConfigLoadSaveData[j].name].LogicalName;
                                                    } else if (modeResponse[i][modConfigLoadSaveData[j].name].Id == null || modeResponse[i][modConfigLoadSaveData[j].name].Id === '') {
                                                        objRecord[modConfigLoadSaveData[j].name] = '';
                                                        objRecord[modConfigLoadSaveData[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                                                        objRecord[modConfigLoadSaveData[j].name + '__LogicalName'] = modConfigLoadSaveData[j].entityNameLookup;
                                                    }
                                                    break
                                                case 'combobox':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name].Value;
                                                    }
                                                    break
                                                case 'currency':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name].Value;
                                                    }
                                                    break
                                                case 'number':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name];
                                                    }
                                                    break
                                                default:
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name];
                                                    }
                                            }
                                        }
                                        newData.push(objRecord);
                                    }
                                    results = results.concat(newData);
                                    if (responseData.__next) {
                                        //console.log(responseData.__next);
                                        Ext.getCmp(idGrid).unmask();
                                        loadData(responseData.__next, reload);
                                    } else {
                                        console.log(">>>>> load data ok! records: " + results.length);
                                        dataOk = true;
                                        if (!reload) {
                                            Ext.define(modelGrid, {
                                                extend: 'Ext.data.Model',
                                                fields: _createFields()
                                            });
                                            Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                            Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            Ext.getCmp(idGrid).getView().refresh();
                                        } else {
                                            Ext.getCmp(idGrid).getStore().loadData(results);
                                            Ext.getCmp(idGrid).filters.clearFilters();
                                            Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            Ext.getCmp(idGrid).getView().refresh();
                                        }
                                        saveGridStatus = true;
                                        Ext.getCmp(idGrid).unmask();
                                    }
                                } else {
                                    console.log(">>>>> data entity null!");
                                    dataOk = false;
                                    if (!reload) {
                                        Ext.define(modelGrid, {
                                            extend: 'Ext.data.Model',
                                            fields: _createFields()
                                        });
                                        Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    } else {
                                        Ext.getCmp(idGrid).getStore().loadData(results);
                                        Ext.getCmp(idGrid).filters.clearFilters();
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    }
                                    saveGridStatus = true;
                                    Ext.getCmp(idGrid).unmask();
                                }
                            } else {
                                if (results.length > 0) {
                                    console.log(">>>>> load data ok! records: " + results.length);
                                    dataOk = false;
                                    if (!reload) {
                                        Ext.define(modelGrid, {
                                            extend: 'Ext.data.Model',
                                            fields: _createFields()
                                        });
                                        Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    } else {
                                        Ext.getCmp(idGrid).getStore().loadData(results);
                                        Ext.getCmp(idGrid).filters.clearFilters();
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    }
                                    saveGridStatus = true;
                                }
                                Ext.getCmp(idGrid).unmask();
                            }
                        }
                    };
                    lookupActive = "";
                    req.send(null);
                }
            }
            function Update_Create_Record(grid, record, rowIndex) {
                var guidId = record.data[entitySchemaName + 'Id'];

                var changes = {};
                for (var j = 0; j < modConfigLoadSaveData.length; j++) {
                    if (modConfigLoadSaveData[j].saved || typeof modConfigLoadSaveData[j].saved === 'undefined') {
                        if (record.data[modConfigLoadSaveData[j].name] != null && (((record.data[modConfigLoadSaveData[j].name]).toString()).replace(/\s+/g, '')).length === 0) { record.data[modConfigLoadSaveData[j].name] = null; }
                        switch (modConfigLoadSaveData[j].type) {

                            case 'date':
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = _toUTCDate(record.data[modConfigLoadSaveData[j].name]);
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                }
                                break;
                            case 'lookup':
                                if (record.data[modConfigLoadSaveData[j].name] == null) { record.data[modConfigLoadSaveData[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000'; }
                                if (record.data[modConfigLoadSaveData[j].name + '__Id'] !== '00000000-0000-0000-0000-000000000000' && record.data[modConfigLoadSaveData[j].name] != null) {
                                    var lookup = {};
                                    lookup.Name = record.data[modConfigLoadSaveData[j].name];
                                    lookup.Id = record.data[modConfigLoadSaveData[j].name + '__Id'];
                                    lookup.LogicalName = record.data[modConfigLoadSaveData[j].name + '__LogicalName'];
                                    changes[modConfigLoadSaveData[j].name] = lookup;
                                } else {
                                    var lookup = {};
                                    lookup.Name = null;
                                    lookup.Id = null;
                                    lookup.LogicalName = null;
                                    changes[configJson.fields[j].name] = lookup;
                                }
                                break;
                            case 'combobox':
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = { Value: (record.data[modConfigLoadSaveData[j].name]).toString() };
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = { Value: record.data[modConfigLoadSaveData[j].name] };
                                }
                                break;
                            case 'number':
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = (record.data[modConfigLoadSaveData[j].name]).toString();
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                }
                                break;
                            case 'checkcolumn':
                                changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                break;
                            case 'boolean':
                                changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                break;
                            case 'currency':
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = { Value: (record.data[modConfigLoadSaveData[j].name]).toString() };
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = { Value: record.data[modConfigLoadSaveData[j].name] };
                                }
                                break;

                            default:
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = (record.data[modConfigLoadSaveData[j].name]).toString();
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                }
                                break;
                        }
                    }
                }
                if (guidId != "") {
                    var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";
                } else {
                    var URI = _getODataEndpoint(entitySchemaName);
                }
                //console.log(URI);
                var req = new XMLHttpRequest();
                req.open("POST", URI, true);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                if (guidId != "") {
                    req.setRequestHeader("X-HTTP-Method", "MERGE");
                } else {

                }
                req.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        req.onreadystatechange = null;
                        if (this.status == 201) {
                            var responseData = JSON.parse(this.responseText).d;
                            console.log("CREATE: " + responseData[entitySchemaName + 'Id']);
                            var upd_record = grid.getStore().getAt(rowIndex);
                            upd_record.set(entitySchemaName + 'Id', responseData[entitySchemaName + 'Id']);
                            upd_record.commit();
                            grid.getStore().commitChanges();
                            grid.getView().refresh();
                            //var sm = grid.getSelectionModel();
                            //sm.select(rowIndex);
                        } else if (this.status == 204) {
                            var upd_record = grid.getStore().getAt(rowIndex);
                            upd_record.set('ModifiedOn', new Date());
                            upd_record.commit();
                            grid.getStore().commitChanges();
                            grid.getView().refresh();

                            //var sm = grid.getSelectionModel();
                            //sm.select(rowIndex);
                        }
                    }
                };
                lookupActive = "";
                req.send(JSON.stringify(changes));
            }
            function Delete_Record(records, grid) {
                for (var i = 0; i < records.length; i++) {
                    var guidId = records[i].data[entitySchemaName + 'Id'];
                    if (guidId != "") {
                        var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";
                        //console.log(URI);
                        var req = new XMLHttpRequest();
                        req.open("POST", URI, false);
                        req.setRequestHeader("Accept", "application/json");
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader("X-HTTP-Method", "DELETE");
                        req.onreadystatechange = function () {
                            if (this.readyState == 4) {
                                req.onreadystatechange = null;
                                //console.log(this.status);
                                if ((this.status == 204) || (this.status == 1223)) {
                                    console.log("DELETE: " + guidId);
                                }
                            }
                        };
                        lookupActive = "";
                        req.send(null);
                    }
                }
                grid.getStore().commitChanges();
                Ext.getCmp(idGrid).getView().refresh();

            }

            var grid = Ext.create('Ext.grid.Panel', {
                id: idGrid,
                border: borderGrid,
                loadMask: true,
                restful: true,
                autoSizeGrid: true,
                features: [_createFilters()],
                columns: [],
                selType: 'checkboxmodel',
                renderTo: "divGrid",
                autoExpandColumn: 'lastUpdated',
                autoScroll: true,
                autoWidth: true,
                height: heightGrid,
                title: titleGrid,
                frame: false,
                plugins: [rowEditing],
                viewConfig: viewConfig,
                tbar: [
/*                    {
                    text: 'Add',
                    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAByElEQVQ4T5WTTShEURiG33OVMcntLgwj89O946dQFEqNhLKwkVhYKKWUpUkpi1lYTFFTGkulZGEhiWwoC0U0hYUyMphr/OS/CCPGzBzde+cOxl3MfKtzvvN+z/nOe84h0AjPFkuT0w77C9HSaiYlQH/NDS4wBQv6MLlbgLQBpXwz3kJ34FgzfKer6QOKLPUIvT+CzSmA/2w9PcD4Jkd5UxU+w6/I1htwEvRisOFZ2wP3eq5iGAEIBVT3rIU2fEU/odexCJwfJvyTKKpmqOmRkLE1Ix1oCMiGqSEZt3hmRzQaAcNkoEPY/rc+sWHDcMstIa4VExUsZoTpk8xWupDa+eno967SOJPJQ+BChLP1ipCRZYEKfA4isTe5SOlPrVZAoFIyPgaFjsnFifiAkTaREOdCScIDtZgQCqsQlqcEFEFRpwBk5c8bc3UeE01nh+fKKV/8rFhCgeAph9Eu3x+ta6nC7Gw/uNQEDM1WUr70Tt5dCtFvhLt7P/WnPDhTTW1l5/LRparAkRXjPXupAxzTtbSuLitxrV7vBzy9OykDDI7p2vvkn+fp3eEaGxF6MIDxzSMCIBa/rz/SfAAcgGwAbBLkOD6//p3/BrMkphEjG7gXAAAAAElFTkSuQmCC',
                    handler: function () {
                        rowEditing.cancelEdit();
                        addFN_newWindow();
                    }
                },
                    */
                    {
                    itemId: 'remove',
                    text: 'Remove',
                    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAADZklEQVQ4T22Tb2xTVRiHf2f29s+5ucfbrQwCNFNoiW3FMdbpyDSSzcQxyDSRENliBuLCUOKGSSNxX4yLhH7gkzEuVT4sG5YFDEZBE0JMNqLrtIMWOkqQjZFqSAndtN3t3W3vvTX3RoxB32/n5H2e88t7ziEA8I4o1vlqasaqK5WGlCS9dSybPW3sP1ofM7bTX1sbVTRt7u6dO13vA2liwE86nWM9zc3PuxjDNxcuyFOlUu/x+/dP/VswROmuBpfr1I6+PiZpGr6LRGYWMpke8kkgMN1VX/9sdX8/4PFAHRzEpfFxeQo48NHSUtSQDDHW0chx0dZ9+5jt6FGgUMDy2bO4NDJyg0SDweLr27Y5cPAgwPPA8jJWwmH8cOaMFOf5HmhafouqftW2Z4/AHzkC2GyApgHXr2NieFglH6xe3dWydm2kvamJr9q9G2AMKJchhcOYOH9eLRGitXZ02FgoBHAcoCiozM4ifu6c8svly+8RI2LI6dz7cm3tye3BoOOxzk6AUuiqiqUTJ6ArCmpCIVTZ7Sasz87i6sWL6o+x2EC/rn9qCowadDq7X2Dsi9ZAwM61twOCAL1YBCoVVFGKiixDS6UQn5xUf06nTdjg/hEYiw9F8dUmm238Rb/fyre1mUkMQUVRICcSuDI1pV3JZPb3A6MPD35UsD0oit83Li7aybp1qFqzxuzTJAnI53GrVCon5+dfexf49j+CIUHYtdXt/rIllxP+yGYhAZD/7rLzPHiXC4+73UgXCvK1ZLL3EGC+EzPBEKUdz/n90ZZslkmZjAmvALAQAsJxKAsCbIzBIQgQ6uqQXlyUE7HYgV5Ni5JjgtC5xe8feymXE+Tbt7EMoGTAHIe7FoteJkT38LxF5XlwggDKGHiPB9cyGenmxMTb5Ovm5vwrsiwoySQKAFQDphS/OhzKVUnqq5TLf9aL4uimVat4zWqFxZBQCr6hAdMzMyvk9ObNqZ12e8CWSCBfLoMTRdyiVJ3O5QYOK4p5VcPA3q3r15/0ut0OjRBQpxPE60VscnKBHLdaA09s3Diyw2JppA8eYIZSNX7v3sDhYtGEH1YE6K73ej9/xudz6NXV+Ckev/lbKrXfHGIY8D21YcOYyHFP31hYGDikKJ/933ceBbo3+XwRuVT6fX5u7o03gem/AGqKWxhL3xdrAAAAAElFTkSuQmCC',
                    handler: function () {
                        rowEditing.cancelEdit();
                        var sm = Ext.getCmp(idGrid).getSelectionModel();
                        Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete select records?', function (btn, text) {
                            if (btn == 'yes') {
                                Delete_Record(sm.getSelection(), Ext.getCmp(idGrid));
                                Ext.getCmp(idGrid).getStore().remove(sm.getSelection());
                            }
                        });
                    }
                }, {
                    itemId: 'reload',
                    icon: 'accentgold_/EditableGrid/icons/grid/reload.png',
                    text: 'Reload',
                    handler: function () {
                        loadData(_getODataEndpoint(entitySchemaName), true);
                    }
                },{
                    itemId: 'excel',
                    text: 'Export to Excel',
                    icon: 'accentgold_/EditableGrid/icons/grid/excel.png',
                    handler: function(b, e) {
                        exportExcelXml();
                    }
                }],
                listeners: {
                    selectionchange: function (view, records) {
                        Ext.getCmp(idGrid).down('#remove').setDisabled(!records.length);
                    },
                    edit: function (editor, e) {
                        lookupActive = "";
                        Update_Create_Record(e.grid, e.record, e.rowIdx);
                        if (altEnter) {
                            if (e.rowIdx + 1 < Ext.getCmp(idGrid).getStore().getCount()) {
                                rowEditing.startEdit(e.rowIdx + 1, 2);
                            } else {
                                addFN_newWindow();
                            }
                            altEnter = false;
                        }
                        if (ctrlEnter) {
                            addFN_newWindow();
                            ctrlEnter = false;
                        }
                    },
                    beforeedit: function (editor, e) {
                        window.setTimeout(function () {
                            $("#" + idGrid).find("input").keydown(function (event) {
                                if (event.ctrlKey && event.keyCode == 13) {
                                    //log("Hey! Ctrl+ENTER event captured!");
                                    ctrlEnter = true;
                                    event.preventDefault();
                                }
                                if (event.altKey && event.keyCode == 13) {
                                    //console.log("Hey! Alt+ENTER event captured!");
                                    altEnter = true;
                                    event.preventDefault();
                                }
                            });
                        }, 100);
                    },
                    canceledit: function (editor, e) {
                        lookupActive = "";
                        if (e.record.data[entitySchemaName + 'Id'] === "") { e.grid.getStore().removeAt(e.rowIdx); }
                        //console.log("cancel edit. rowIndex: " + e.rowIdx);
                    },
                    afterrender: function() {
                        spinnerForm.stop();
                        loadData(_getODataEndpoint(entitySchemaName), false);
                    }
                }
            });

            new Ext.util.KeyMap(Ext.getBody(),
                [{
                    key: Ext.EventObject.E,
                    ctrl: true,
                    scope: this,
                    defaultEventAction: 'preventDefault',
                    fn: function (e) {
                        //console.log("Hey! Ctrl+E event captured!");
                        var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                        if (typeof record !== 'undefined') {
                            rowEditing.startEdit(record.index, 2);
                        } else { Ext.Msg.alert("Error with editing", "No Financial record has been selected!"); }
                    }
                }, {
                    key: 45,
                    ctrl: true,
                    scope: this,
                    defaultEventAction: 'preventDefault',
                    fn: function (e) {
                        //console.log("Hey! Ctrl+Insert event captured!");
                        rowEditing.cancelEdit();
                        addFN_newWindow();
                    }
                }, {
                    key: 46,
                    ctrl: true,
                    scope: this,
                    defaultEventAction: 'preventDefault',
                    fn: function (e) {
                        //console.log("Hey! Ctrl+Delete event captured!");
                        rowEditing.cancelEdit();
                        var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                        if (typeof record !== 'undefined') {
                            var sm = Ext.getCmp(idGrid).getSelectionModel();
                            rowEditing.cancelEdit();
                            Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete select records?', function (btn, text) {
                                if (btn == 'yes') {
                                    Delete_Record(sm.getSelection(), Ext.getCmp(idGrid));
                                    Ext.getCmp(idGrid).getStore().remove(sm.getSelection());
                                }
                            });
                        } else { Ext.Msg.alert("Error with deleting", "No Financial record has been selected!"); }
                    }
                }]);

            function Fncl_ActEnd_OnChange(idx, idxValue) {
                var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];

                try {
                    var fieldIndex = parseInt(idx);
                    var ActEnd = idxValue;
                    var ActStart = record.get("ddsm_actualstart" + fieldIndex);
                    if (ActEnd == null) return;

                    var isValid = true;
                    var today = new Date();

                    if (ActStart == null) {
                        Ext.Msg.alert("Error", "Sorry, the Actual Start shouldn't be empty.");
                        record.set("ddsm_actualend" + fieldIndex, null);
                        rowEditing.getEditor().getForm().findField("ddsm_actualend" + fieldIndex).setValue(null);
                        return;
                    }

                    var actStartClean = new Date(UTCToLocalTime(ActStart));
                    var actEndClean = new Date(UTCToLocalTime(ActEnd));

                    //Validity checks///////////////////////////
                    if (record.get("ddsm_status") == 962080000) {
                        Ext.Msg.alert("Error", "This Financial has not been started. Please Initiate it from the Approvals Table before editing this Financial");
                        isValid = false;
                    } else if (ActStart == null) {
                        Ext.Msg.alert("Error", "Actual Start Date must be filled in before filling in Actual End Date for this Stage");
                        isValid = false;
                        //Check: Actual Start <= Actual End
                    } else if (actStartClean > actEndClean) {
                        Ext.Msg.alert("Error", "Actual End Date cannot be before the associated Actual Start Date. Please enter a valid Date.");
                        isValid = false;
                        //Check: Actual End <= Today
                    } else if (ActEnd > today) {
                        Ext.Msg.alert("Error", "Actual End Date cannot be in the future. Please enter a valid Date.");
                        isValid = false;
                    }

                    if (!isValid) {
                        record.set("ddsm_actualend" + fieldIndex, null);
                        rowEditing.getEditor().getForm().findField("ddsm_actualend" + fieldIndex).setValue(null);
                        return;
                    }
                    record.data["ddsm_actualend" + fieldIndex] = new Date(ActEnd);
                    //Complete Project if necessary or update Active Stage if correct

                    if (fieldIndex == FNCL_NUM_STAGES) {
                        record.set("ddsm_status", '962080002');
                        rowEditing.getEditor().getForm().findField("ddsm_status").setValue('962080002');
                        record.set("ddsm_pendingstage", "Completed");
                        rowEditing.getEditor().getForm().findField("ddsm_pendingstage").setValue("Completed");
                    } else if (fieldIndex < FNCL_NUM_STAGES && record.get("ddsm_stagename" + (fieldIndex + 1)) == null) {
                        record.set("ddsm_status", '962080002');
                        rowEditing.getEditor().getForm().findField("ddsm_status").setValue('962080002');
                        record.set("ddsm_pendingstage", "Completed");
                        rowEditing.getEditor().getForm().findField("ddsm_pendingstage").setValue("Completed");
                        b.Xrm.Page.getAttribute("ddsm_projectgroupstatus").setValue(962080002);
                        b.Xrm.Page.data.save();
                    } else if (fieldIndex < FNCL_NUM_STAGES && record.get("ddsm_pendingstage") == record.get("ddsm_stagename" + fieldIndex)) {
                        record.set("ddsm_pendingstage", record.get("ddsm_stagename" + (fieldIndex + 1)));
                        rowEditing.getEditor().getForm().findField("ddsm_pendingstage").setValue(record.get("ddsm_stagename" + (fieldIndex + 1)));
                    }



                    //loop through date updates///////////////////////////
                    for (var i = 2; i <= FNCL_NUM_STAGES; i++) {
                        if (record.get("ddsm_stagename" + i) == null) {
                            continue;
                        }
                        if (record.get("ddsm_actualend" + (i - 1)) != null) {
                            var dtActEnd = new Date();
                            dtActEnd = new Date(record.get("ddsm_actualend" + (i - 1)));
                            var dtNextTargEnd = new Date(dtActEnd.getTime() + record.get("ddsm_duration" + i) * 86400000);
                            record.set("ddsm_actualstart" + i, dtActEnd);
                            rowEditing.getEditor().getForm().findField("ddsm_actualstart" + i).setValue(UTCToLocalTime(dtActEnd));

                            record.set("ddsm_targetstart" + i, dtActEnd);
                            rowEditing.getEditor().getForm().findField("ddsm_targetstart" + i).setValue(UTCToLocalTime(dtActEnd));

                            record.set("ddsm_targetend" + i, dtNextTargEnd);
                            rowEditing.getEditor().getForm().findField("ddsm_targetend" + i).setValue(UTCToLocalTime(dtNextTargEnd));
                        } else {
                            var dtTargEnd = new Date();
                            dtTargEnd = record.get("ddsm_targetend" + (i - 1));

                            var dtNextTargEnd = new Date(dtTargEnd.getTime() + record.get("ddsm_duration" + i) * 86400000);
                            record.set("ddsm_targetstart" + i, dtTargEnd);
                            rowEditing.getEditor().getForm().findField("ddsm_targetstart" + i).setValue(UTCToLocalTime(dtTargEnd));

                            record.set("ddsm_targetend" + i, dtNextTargEnd);
                            rowEditing.getEditor().getForm().findField("ddsm_targetend" + i).setValue(UTCToLocalTime(dtNextTargEnd));
                        }
                    }
                }
                catch (err) {
                    Ext.Msg.alert("Error", "Error: " + err + ".");
                }
            }


            //loadData(_getODataEndpoint(entitySchemaName), false);

        });

        // Refresh Grid
        reloadGrid = function () {
            loadData(_getODataEndpoint(entitySchemaName), true);
        }
        get_dataOkgrid = function () {
            return dataOk;
        }
        get_saveGridStatus = function () {
            return saveGridStatus;
        }
        refreshGrid = function () {
            Ext.getCmp(idGrid).getView().refresh();
        }
        $(b).resize(function () {
            Ext.getCmp(idGrid).getView().refresh();
        });
    });
} else {

}
