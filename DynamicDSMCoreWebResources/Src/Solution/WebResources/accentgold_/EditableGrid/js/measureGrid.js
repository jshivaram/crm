/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 2.2.7
 * Last updated on 04/25/2016.
 * Measures Grid not BPA fields
 * 12/24/2014 add logic Create Document Name Convention List to project
 * NOTE: the source of current file is used in measureGrid.js in CRM
 */
(function (fallback) {

    fallback = fallback || function () {
        };

    // function to trap most of the console functions from the FireBug Console API.
    var trap = function () {
        var args = Array.prototype.slice.call(arguments);
        var message = args.join(' ');
        fallback(message);
    };

    if (typeof console === 'undefined') {
        console = {
            messages: [],
            raw: [],
            dump: function () {
                return console.messages.join('\n');
            },
            log: trap,
            debug: trap,
            info: trap,
            warn: trap,
            error: trap,
            assert: trap,
            clear: function () {
                console.messages.length = 0;
                console.raw.length = 0;
            },
            dir: trap,
            dirxml: trap,
            trace: trap,
            group: trap,
            groupCollapsed: trap,
            groupEnd: trap,
            time: trap,
            timeEnd: trap,
            timeStamp: trap,
            profile: trap,
            profileEnd: trap,
            count: trap,
            exception: trap,
            table: trap
        };
    }

})(null);

var b = window.parent;
if (b.Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue() != null) {
    var smLibraryStore, adminData = {}, smLibraryTemplatesData = {}, ESPMeasureLibraryId = 'undefined', smRequiredFields = [], smCurrentVersionData = {}, smComboStoreSourecesCRM = {}, objMappingSM, hide_columns = [], viewColumns = [], dataOk, results = [], SmartMeasureButton = false, loadData, _getODataEndpoint, entitySchemaName, saveGridStatus, updateProjectStatus, lookupActive, newRecordRow = false, get_saveGridStatus, get_dataOkgrid, refreshGrid, reloadGrid, idGrid, rowEditing, entityName_getLookupData, fnclTpl_ID = null, EnableBtnFincl = false, AutoMeasureFncl, getModelNumberStatus, arrayIds = [], Picklist_stores = {}, MADFields = {}, SelectableProgramOffering = null, lkp_toUpdate = null;

    //mapping from the ddsm_mappingfields enity
    //var customMap = AGS.Entity.getTargetMapping(null, null, ['ddsm_measuretemplate', 'ddsm_sourcecalculationtype', 'Account', 'ddsm_site', 'ddsm_project', 'ddsm_rateclassreference']);
    //var mtMapEntities = ['ddsm_measuretemplate', 'ddsm_sourcecalculationtype'];

    $(b.document).ready(function () {

        // ---- START Excel export code
        var Base64 = (function () {
            // Private property
            var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

            // Private method for UTF-8 encoding

            function utf8Encode(string) {
                string = string.replace(/\r\n/g, "\n");
                var utftext = "";
                for (var n = 0; n < string.length; n++) {
                    var c = string.charCodeAt(n);
                    if (c < 128) {
                        utftext += String.fromCharCode(c);
                    } else if ((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                    } else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                }
                return utftext;
            }

            // Public method for encoding
            return {
                encode: (typeof btoa == 'function') ? function (input) {
                    return btoa(utf8Encode(input));
                } : function (input) {
                    var output = "";
                    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
                    var i = 0;
                    input = utf8Encode(input);
                    while (i < input.length) {
                        chr1 = input.charCodeAt(i++);
                        chr2 = input.charCodeAt(i++);
                        chr3 = input.charCodeAt(i++);
                        enc1 = chr1 >> 2;
                        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                        enc4 = chr3 & 63;
                        if (isNaN(chr2)) {
                            enc3 = enc4 = 64;
                        } else if (isNaN(chr3)) {
                            enc4 = 64;
                        }
                        output = output +
                            keyStr.charAt(enc1) + keyStr.charAt(enc2) +
                            keyStr.charAt(enc3) + keyStr.charAt(enc4);
                    }
                    return output;
                }
            };
        })();
        // ---- END Excel export code

        Ext.Loader.setConfig({
            enabled: true,
            disableCaching: false,
            path: {'Ext': 'accentgold_/EditableGrid/js/'}
        });
        Ext.Loader.setPath('Ext.ux', 'accentgold_/EditableGrid/js/ux');


        //----------DEBUG LOOKUP template-----------START<<<<<<<<<<<<<

        //----------DEBUG LOOKUP template-------------END<<<<<<<<<<<<<

        //----------DEBUG LOOKUP field initialization-----------START<<<<<<<<<<<<<
        Ext.define('Ext.form.LookupCombo', {
            extend: 'Ext.form.ComboBox',
            store: Ext.create('Ext.data.Store', {
                fields: ['value', 'display'],
                data: []
            }),
            alias: 'widget.lookup',
            queryMode: 'local',
            displayField: 'display',
            valueField: 'value',
            forceSelection: true,
            triggerCls: "x-form-search-trigger",
            listeners:
            {
                beforequery:function(qe)
                {
                    //INNER METHODS
                    ////TODO: Move to global scope
                    //var setNewLookupData = function(res){
                    //    if(!res || !res.items || !res.items[0]){
                    //        return;
                    //    }
                    //
                    //    let lookupVal = AGS.String.format("{0}|{1}|{2}", res.items[0].typename, res.items[0].id, res.items[0].name);
                    //    let lookupParts = lookupVal.split('|');
                    //    let lkp_field = Ext.getCmp('smForm1').getForm().findField(lkp_toUpdate);
                    //    if (!lkp_field){
                    //        return;
                    //    }
                    //    lkp_field.store.removeAll();
                    //    lkp_field.store.add({value: lookupVal, display: res.items[0].name || ''} );
                    //    lkp_field.setValue(lookupVal);
                    //
                    //    //clear global field name storage
                    //    lkp_toUpdate = null;
                    //};
                    //
                    //var getSMLookupData = function (entityName, lkpCallback) {
                    //    //var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                    //    //var localname = record.get(entityName + '__LogicalName');
                    //    var localname = entityName.toLowerCase();
                    //    var objecttypes = b.Xrm.Internal.getEntityCode(localname);
                    //    var lookupURI = "/_controls/lookup/lookupinfo.aspx";
                    //    lookupURI += "?LookupStyle=single";
                    //    lookupURI += "&objecttypes=" + objecttypes;
                    //    lookupURI += "&ShowNewButton=0";
                    //    lookupURI += "&ShowPropButton=1";
                    //    lookupURI += "&browse=false";
                    //    lookupURI += "&AllowFilterOff=0";
                    //    lookupURI += "&DefaultType=" + objecttypes;
                    //    lookupURI += "&DisableQuickFind=0";
                    //    lookupURI += "&DisableViewPicker=0";
                    //    //console.log("lookupURI: " + lookupURI);
                    //
                    //    window.setTimeout(function () {
                    //        var DialogOption = new b.Xrm.DialogOptions;
                    //        DialogOption.width = 550; DialogOption.height = 550;
                    //        b.Xrm.Internal.openDialog(lookupURI, DialogOption, null, null, (lkpCallback instanceof Function)? lkpCallback : null);
                    //    }, 200);
                    //};

                    let val = this.value;
                    if(!val || !val.split('|')[0]){
                        Ext.Msg.alert("Error", "Can not identify the type of lookup. Please check the source data and retry to create a measure.");
                       return false;
                    }


                    let lookupParts = val.split('|');

                    lkp_toUpdate = this.name;
                    //CALL
                    getSMLookupData(lookupParts[0].toLowerCase(),setNewLookupData);

                    return false;
                }
            }
        });
        //----------DEBUG LOOKUP field initialization-------------END<<<<<<<<<<<<<


        Ext.require([
            'Ext.grid.*',
            'Ext.data.*',
            'Ext.ux.grid.FiltersFeature',
            'Ext.ux.grid.menu.ListMenu',
            'Ext.util.*',
            'Ext.state.*',
            'Ext.form.*',
            'Ext.slider.*',
            'Ext.tip.QuickTip',
            //----------DEBUG-----------START<<<<<<<<<<<<<
            'widget.lookup',
            //----------DEBUG-------------END<<<<<<<<<<<<<
            'Ext.selection.CheckboxModel'
        ]);

        Ext.override('Ext.ux.grid.FiltersFeature', {
            show: function () {
                var lastArgs = null;
                return function () {
                    if (!arguments) {
                        this.callParent(lastArgs);
                    } else {
                        lastArgs = arguments;
                        if (this.loadOnShow && !this.loaded) {
                            this.store.load();
                        }
                        this.callParent(arguments);
                    }
                };
            }()
        });
        Ext.grid.RowEditor.prototype.saveBtnText = "Update & Save";

        Ext.onReady(function () {

            // ---- START Excel export code
            var reg = /[^\.\,\;\:\?\!\"\'\(\)\+\№\%\#\@\$\[\]\{\}\~\^\&\*\/\\\|\<\>\s]+/g;
            var strName = b.Xrm.Page.getAttribute("ddsm_name").getValue();
            strName = strName.match(reg).join(' ');

            var exportExcelXml = function (includeHidden, title) {

                if (!title) {
                    title = "Project Measures"
                }

                var vExportContent = getExcelXml(includeHidden, title);
                var location = 'data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);
                if (!Ext.isIE) {
                    var gridEl = Ext.getCmp(idGrid).getEl();

                    var el = Ext.DomHelper.append(gridEl, {
                        tag: "a",
                        download: strName + " - " + title + " - " + Ext.Date.format(new Date(), 'm-d-Y H-i') + '.xls',
                        href: location
                    });
                    el.click();
                    Ext.fly(el).destroy();
                } else {
                    var fileName = strName + " - " + title + " - " + Ext.Date.format(new Date(), 'm-d-Y H-i') + '.xls';
                    var form = Ext.getCmp(idGrid).down('form#uploadForm');
                    if (form) {
                        form.destroy();
                    }
                    form = Ext.getCmp(idGrid).add({
                        xtype: 'form',
                        itemId: 'uploadForm',
                        hidden: true,
                        standardSubmit: true,
                        url: 'http://dynamicdsm.richlode.biz/crmIExls.asmx/GetXsl',
                        items: [{
                            xtype: 'hiddenfield',
                            name: 'Name',
                            value: fileName
                        }, {
                            xtype: 'hiddenfield',
                            name: 'strXml',
                            value: Base64.encode(vExportContent)
                        }]
                    });

                    form.getForm().submit();

//                    alert("The xls export isn't supported by IE.");
                }

            };

            var getExcelXml = function (includeHidden, title) {
                var theTitle = title || this.title;
                var worksheet = createWorksheet(includeHidden, theTitle);
                var totalWidth = Ext.getCmp(idGrid).columnManager.columns.length;
                return ''.concat(
                    '<?xml version="1.0"?>',
                    '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">',
                    '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Title>' + theTitle + '</Title></DocumentProperties>',
                    '<OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office"><AllowPNG/></OfficeDocumentSettings>',
                    '<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">',
                    '<WindowHeight>' + worksheet.height + '</WindowHeight>',
                    '<WindowWidth>' + 12585 + '</WindowWidth>',
                    '<ProtectStructure>False</ProtectStructure>',
                    '<ProtectWindows>False</ProtectWindows>',
                    '</ExcelWorkbook>',

                    '<Styles>',

                    '<Style ss:ID="Default" ss:Name="Normal">',
                    '<Alignment ss:Vertical="Bottom"/>',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>',
                    '<Interior/>',
                    '<NumberFormat/>',
                    '<Protection/>',
                    '</Style>',

                    '<Style ss:ID="title">',
                    '<Borders />',
                    '<Font ss:Bold="1" ss:Size="18" />',
                    '<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1" />',
                    '<Interior ss:Color="#E0ECF0" ss:Pattern="Solid" />',
                    '<NumberFormat ss:Format="@" />',
                    '</Style>',

                    '<Style ss:ID="headercell">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:Bold="1" ss:Size="10" />',
                    '<Alignment ss:Horizontal="Center" ss:WrapText="1" />',
                    '<Interior ss:Color="#CFE1E8" ss:Pattern="Solid" />',
                    '</Style>',


                    '<Style ss:ID="even">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Interior ss:Color="#FFFFFF" ss:Pattern="Solid" />',
                    '</Style>',

                    /*
                     '<Style ss:ID="evendate" ss:Parent="even">',
                     '<NumberFormat ss:Format="m-d-yyyy" />',
                     '</Style>',

                     '<Style ss:ID="evenint" ss:Parent="even">',
                     '<Numberformat ss:Format="0" />',
                     '</Style>',

                     '<Style ss:ID="evenfloat" ss:Parent="even">',
                     '<Numberformat ss:Format="0.00" />',
                     '</Style>',
                     */

                    '<Style ss:ID="odd">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Interior ss:Color="#F1F7F8" ss:Pattern="Solid" />',
                    '</Style>',

                    '<Style ss:ID="groupSeparator">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:Bold="1" ss:Size="10" />',
                    '<Interior ss:Color="#D3D3D3" ss:Pattern="Solid" />',
                    '</Style>',
                    /*
                     '<Style ss:ID="odddate" ss:Parent="odd">',
                     '<NumberFormat ss:Format="m-d-yyyy" />',
                     '</Style>',
                     '<Style ss:ID="oddint" ss:Parent="odd">',
                     '<NumberFormat Format="0" />',
                     '</Style>',

                     '<Style ss:ID="oddfloat" ss:Parent="odd">',
                     '<NumberFormat Format="0.00" />',
                     '</Style>',
                     */


                    '</Styles>',
                    worksheet.xml,
                    '</Workbook>'
                );
            };

            var getModelField = function (fieldName) {
                var fields = Ext.getCmp(idGrid).store.model.getFields();
                for (var i = 0; i < fields.length; i++) {
                    if (fields[i].name === fieldName) {
                        return fields[i];
                    }
                }
            };

            var generateEmptyGroupRow = function (dataIndex, value, cellTypes, includeHidden) {
                var cm = Ext.getCmp(idGrid).columnManager.columns;
                var colCount = cm.length;
                var rowTpl = '<Row ss:AutoFitHeight="0"><Cell ss:StyleID="groupSeparator" ss:MergeAcross="{0}"><Data ss:Type="String"><html:b>{1}</html:b></Data></Cell></Row>';
                var visibleCols = 0;
                // rowXml += '<Cell ss:StyleID="groupSeparator">'
                for (var j = 0; j < colCount; j++) {
                    if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && (includeHidden || !cm[j].hidden)) {
                        // rowXml += '<Cell ss:StyleID="groupSeparator"/>';
                        visibleCols++;
                    }
                }
                // rowXml += "</Row>";
                return Ext.String.format(rowTpl, visibleCols - 1, value);
            };


            var createWorksheet = function (includeHidden, theTitle) {
                // Calculate cell data types and extra class names which affect formatting
                var cellType = [];
                var cellTypeClass = [];
                var cm = Ext.getCmp(idGrid).columnManager.columns;

                var totalWidthInPixels = 0;
                var colXml = '';
                var headerXml = '';
                var visibleColumnCountReduction = 0;
                var colCount = cm.length;
                for (var i = 0; i < colCount; i++) {
                    if (cm[i].xtype != 'actioncolumn' && (cm[i].dataIndex != '') && (includeHidden || !cm[i].hidden)) {
                        var w = cm[i].getEl().getWidth();
                        totalWidthInPixels += w;

                        if (cm[i].text === "") {
                            cellType.push("None");
                            cellTypeClass.push("");
                            ++visibleColumnCountReduction;
                        } else {
                            colXml += '<Column ss:AutoFitWidth="1" ss:Width="' + w + '" />';
                            headerXml += '<Cell ss:StyleID="headercell">' +
                                '<Data ss:Type="String">' + cm[i].text + '</Data>' +
                                '<NamedCell ss:Name="Print_Titles"></NamedCell></Cell>';


                            var fld = getModelField(cm[i].dataIndex);
                            switch (fld.type.type) {
                                case "int":
                                    cellType.push("Number");
                                    cellTypeClass.push("int");
                                    break;
                                case "float":
                                    cellType.push("Number");
                                    cellTypeClass.push("float");
                                    break;

                                case "bool":

                                case "boolean":
                                    cellType.push("String");
                                    cellTypeClass.push("");
                                    break;
                                case "date":
                                    cellType.push("DateTime");
                                    cellTypeClass.push("date");
                                    break;
                                default:
                                    cellType.push("String");
                                    cellTypeClass.push("");
                                    break;
                            }
                        }
                    }
                }
                var visibleColumnCount = cellType.length - visibleColumnCountReduction;
                var result = {
                    height: 9000,
                    width: Math.floor(totalWidthInPixels * 30) + 50
                };

                var numGridRows = Ext.getCmp(idGrid).store.getCount() + 2;
                if (!Ext.isEmpty(Ext.getCmp(idGrid).store.groupField) || Ext.getCmp(idGrid).store.groupers.items.length > 0) {
                    numGridRows = numGridRows + Ext.getCmp(idGrid).store.getGroups().length;
                }

                // create header for worksheet
                var t = ''.concat(
                    '<Worksheet ss:Name="' + theTitle + '">',

                    '<Names>',
                    '<NamedRange ss:Name="Print_Titles" ss:RefersTo="=\'' + theTitle + '\'!R1:R2">',
                    '</NamedRange></Names>',

                    '<Table ss:ExpandedColumnCount="' + (visibleColumnCount + 2),
                    '" ss:ExpandedRowCount="' + numGridRows + '" x:FullColumns="1" x:FullRows="1" ss:DefaultColumnWidth="65" ss:DefaultRowHeight="15">',
                    colXml,
                    '<Row ss:Height="38">',
                    '<Cell ss:MergeAcross="' + (visibleColumnCount - 1) + '" ss:StyleID="title">',
                    '<Data ss:Type="String" xmlns:html="http://www.w3.org/TR/REC-html40">',
                    '<html:b>' + strName + ' - ' + theTitle + '</html:b></Data><NamedCell ss:Name="Print_Titles">',
                    '</NamedCell></Cell>',
                    '</Row>',
                    '<Row ss:AutoFitHeight="1">',
                    headerXml +
                    '</Row>'
                );

                var groupVal = "";
                var groupField = "";
                if (Ext.getCmp(idGrid).store.groupers.keys.length > 0) {
                    groupField = Ext.getCmp(idGrid).store.groupers.keys[0];
                }
                for (var i = 0, it = Ext.getCmp(idGrid).store.data.items, l = it.length; i < l; i++) {

                    if (!Ext.isEmpty(groupField)) {
                        if (groupVal != Ext.getCmp(idGrid).store.getAt(i).get(groupField)) {
                            groupVal = Ext.getCmp(idGrid).store.getAt(i).get(groupField);
                            t += generateEmptyGroupRow(groupField, groupVal, cellType, includeHidden);
                        }
                    }
                    t += '<Row>';
                    var cellClass = (i & 1) ? 'odd' : 'even';
                    r = it[i].data;
                    var k = 0;
                    for (var j = 0; j < colCount; j++) {
                        if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && (includeHidden || !cm[j].hidden)) {
                            var v = r[cm[j].dataIndex];
                            if (cm[j].dataIndex == "ddsm_EnergyType") {
                                v = ddsm_EnergyTypeColumnRenderer(v);
                            }

                            if (cm[j].dataIndex == "ddsm_MeasureType") {
                                v = ddsm_MeasureTypeColumnRenderer(v);
                            }
                            if (typeof v === 'undefined') {
                                v = '';
                            }
                            if (cellType[k] !== "None") {
                                //t += '<Cell ss:StyleID="' + cellClass + cellTypeClass[k] + '"><Data ss:Type="' + cellType[k] + '">';
                                t += '<Cell ss:StyleID="' + cellClass + '"><Data ss:Type="' + 'String' + '">';
                                if (cellType[k] == 'DateTime') {
                                    if (v !== null) {
                                        t += (Ext.Date.format(v, 'm-d-Y')).toString();
                                    } else {
                                        t += '';
                                    }
                                } else {
                                    if (v !== null) {
                                        t += (v).toString();
                                    } else {
                                        t += '';
                                    }
                                }
                                t += '</Data></Cell>';
                            }
                            k++;
                        }
                    }
                    t += '</Row>';
                }

                result.xml = t.concat(
                    '</Table>',
                    '<WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">',
                    '<PageLayoutZoom>0</PageLayoutZoom>',
                    '<Selected/>',
                    '<Panes>',
                    '<Pane>',
                    '<Number>3</Number>',
                    '<ActiveRow>2</ActiveRow>',
                    '</Pane>',
                    '</Panes>',
                    '<ProtectObjects>False</ProtectObjects>',
                    '<ProtectScenarios>False</ProtectScenarios>',
                    '</WorksheetOptions>',
                    '</Worksheet>'
                );
                return result;
            };
            // ---- END Excel export code

            //Get Smar Measure Library
            function getSmartMeasureLibrary() {
                sel = "$select=ddsm_name,ddsm_espmeasurelibraryId,ddsm_Organizationname,ddsm_ESPMeasureLibId&$orderby=ddsm_name asc";
                AGS.REST.retrieveMultipleRecords("ddsm_espmeasurelibrary", sel, null, null, function (smLibrary) {
                    smLibraryStore = new Ext.data.SimpleStore({
                        fields: ["value", "display", "OrganizationName", "ESPMeasureLibId"],
                        data: []
                    });
                    //console.dir(smLibrary);
                    for (var i = 0; i < smLibrary.length; i++) {
                        smLibraryStore.add({
                            value: smLibrary[i].ddsm_espmeasurelibraryId,
                            display: smLibrary[i].ddsm_name,
                            OrganizationName: smLibrary[i].ddsm_Organizationname,
                            ESPMeasureLibId: smLibrary[i].ddsm_ESPMeasureLibId
                        });
                    }
                }, true, []);

                //Get Object Mapping Smart Measures and default SM library
                sel = "$select=ddsm_ESPMapingData,ddsm_DefaultESPLibraryId,ddsm_AllMADFieldsarerequired"
                    + "&$filter=ddsm_name eq 'Admin Data' and statecode/Value eq 0";
                AGS.REST.retrieveMultipleRecords("ddsm_admindata", sel, null, null, function (data) {
                    if (!data || !data[0]) {
                        return;
                    }
                    //objMappingSM = JSON.parse(data[0].ddsm_ESPMapingData);
                    objMappingSM = {};
                    let objMappingSM_1 = JSON.parse(data[0].ddsm_ESPMapingData);
                    if(!!objMappingSM_1){
                        for (let k in objMappingSM_1) {
                            objMappingSM[k.toLowerCase()] = objMappingSM_1[k];
                        }
                    }


                    if (!!data[0].ddsm_DefaultESPLibraryId && data[0].ddsm_DefaultESPLibraryId.Id) {
                        adminData.smLibDefault = data[0].ddsm_DefaultESPLibraryId.Id;
                        adminData.RequiredAllFields = !!data[0].ddsm_AllMADFieldsarerequired;
                    }

                }, true, null);
            }

            getSmartMeasureLibrary();

            //get Selectable Program Offering
            if(!!b.Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue())
                AGS.REST.retrieveRecord(b.Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue()[0].id, b.Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue()[0].typename, "ddsm_SelectableProgramOffering", null, function(data){SelectableProgramOffering = data.ddsm_SelectableProgramOffering.Value;}, null, true);


            ///// START ACTION COLUMNS
            ///// DELETE COLUMN
            var delActColumn = {
                xtype: 'actioncolumn',
                width: 30,
                align: 'center',
                sortable: false,
                menuDisabled: true,
                items: [{
                    icon: 'accentgold_/EditableGrid/icons/grid/delete.gif',
                    tooltip: 'Delete record',
                    handler: function (grid, rowIndex) {
                        rowEditing.cancelEdit();
                        var record = [];
                        record.push(grid.getStore().getAt(rowIndex));
                        Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete select records?', function (btn, text) {
                            if (btn == 'yes') {
                                Delete_Record(record, grid);
                                grid.getStore().removeAt(rowIndex);
                            }
                        });
                    }
                }]
            };
            ///// END ACTION COLUMNS

            ///// DELETE COLUMN
            var changeSKUColumn = {
                xtype: 'actioncolumn',
                align: 'center',
                text: 'Change Model Number',
                width: 75,
                sortable: false,
                menuDisabled: true,
                items: [{
                    icon: 'accentgold_/EditableGrid/icons/grid/table_refresh.png',
                    tooltip: 'Change Model Number',
                    handler: function (grid, rowIndex) {
                        var record = grid.getStore().getAt(rowIndex);
                        getMeasureLibrary(record.data.ddsm_MeasureSelector__Id, record, rowIndex);
                    }
                }]
            };
            ///// END ACTION COLUMNS

            var defaultFieldsValue = {};
            dataOk = false;
            var ctrlEnter = false;
            saveGridStatus = false;
            var altEnter = false;
            //var results;
            var phase1Columns = [];
            var phase2Columns = [];
            var phase3Columns = [];
            var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
            var _odataEndpoint = "/XRMServices/2011/OrganizationData.svc";
            var ddsm_projectstatus = b.Xrm.Page.getAttribute("ddsm_projectstatus");
            var ddsm_pendingmilestone = b.Xrm.Page.getAttribute("ddsm_pendingmilestone");
            var ddsm_responsible = b.Xrm.Page.getAttribute("ddsm_responsible");
            var ddsm_pendingactualstart = b.Xrm.Page.getAttribute("ddsm_pendingactualstart");
            var ddsm_pendingtargetend = b.Xrm.Page.getAttribute("ddsm_pendingtargetend");
            var ddsm_phase = b.Xrm.Page.getAttribute("ddsm_phase").getValue();

            var modelGrid, renderTo, dateFormat, heightGrid, borderGrid, titleGrid, sortersGrid;
            if (typeof configJson.entitySchemaName !== 'undefined') {
                entitySchemaName = configJson.entitySchemaName;
            } else {
                entitySchemaName = "";
                Ext.Msg.alert("Error", "The entitySchemaName parameter value is missing in the config file. Please specify the correct value in the config file of this grid (Web Resources).");
            }
            if (typeof configJson.modelGrid !== 'undefined') {
                modelGrid = configJson.modelGrid;
            } else {
                modelGrid = "mGrid" + randomnumber;
            }
            if (typeof configJson.idGrid !== 'undefined') {
                idGrid = configJson.idGrid;
            } else {
                idGrid = "idGrid" + randomnumber;
            }
            if (typeof configJson.renderTo !== 'undefined') {
                renderTo = configJson.renderTo;
            } else {
                renderTo = Ext.getBody();
            }
            if (typeof configJson.dateFormat !== 'undefined') {
                dateFormat = configJson.dateFormat;
            } else {
                dateFormat = "m/d/Y";
            }
            if (typeof configJson.height !== 'undefined') {
                heightGrid = configJson.height;
            } else {
                heightGrid = 400;
            }
            if (typeof configJson.border !== 'undefined') {
                borderGrid = configJson.border;
            } else {
                borderGrid = false;
            }
            if (typeof configJson.title !== 'undefined') {
                titleGrid = configJson.title;
            } else {
                titleGrid = '';
            }
            if (typeof configJson.sorters !== 'undefined') {
                sortersGrid = configJson.sorters;
            } else {
                sortersGrid = {property: configJson.entitySchemaName + 'Id', direction: 'ASC'};
            }
            if (typeof configJson.entityConcat !== 'undefined') {
                entityConcat = configJson.entityConcat;
            }
            if (typeof configJson.fieldConcat !== 'undefined') {
                fieldConcat = configJson.fieldConcat;
            }

            _getODataEndpoint = function (entitySchemaName) {
                return Ext.String.format("{0}{1}/{2}Set", b.Xrm.Page.context.getClientUrl(), _odataEndpoint, entitySchemaName);
            }

            function getProjTplAttribute() {
                var req = new XMLHttpRequest();
                var URI_ = _getODataEndpoint("ddsm_projecttemplate")
                    + "?$select=ddsm_FinancialTemplateforAutoIncentive,ddsm_EnableManualFinancialCreation&$filter=ddsm_projecttemplateId eq guid'" + b.Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue()[0].id + "'";
                req.open("GET", URI_, false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        req.onreadystatechange = null;
                        if (this.status == 200) {
                            var responseData = JSON.parse(this.responseText).d;
                            var modeResponse = responseData.results;
                            if (modeResponse[0].ddsm_FinancialTemplateforAutoIncentive.Id == null) {
                                fnclTpl_ID = "e71126c8-45f0-e411-80ef-fc15b428ddb0";
                            } else {
                                fnclTpl_ID = modeResponse[0].ddsm_FinancialTemplateforAutoIncentive.Id;
                            }
                            if (modeResponse[0].ddsm_EnableManualFinancialCreation != null) {
                                EnableBtnFincl = modeResponse[0].ddsm_EnableManualFinancialCreation;
                            } else {
                                EnableBtnFincl = false;
                            }
                        }
                    }
                }
                req.send(null);
            }

            getProjTplAttribute();

            var _configLoadSaveData = function () {
                var _config = [];
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            _config.push(configJson.fields[i].group[j]);
                        }
                    } else {
                        _config.push(configJson.fields[i]);
                    }
                }
                return _config;
            }
            var modConfigLoadSaveData = _configLoadSaveData();
            var _makeObjFields = function (obj) {
                var fields = [];
                var new_field = {};
                if (obj.type != "lookup") {
                    if (typeof obj.defaultValue !== 'undefined') {
                        defaultFieldsValue[obj.name] = obj.defaultValue;
                    }
                    new_field.name = obj.name;
                    switch (obj.type) {
                        case 'date':
                            new_field.type = obj.type;
                            //new_field.dateFormat = dateFormat;
                            break;
                        case 'number':
                            new_field.type = obj.type;
                            break;
                        case 'decimal':
                            new_field.type = 'float';
                            break;
                        case 'int':
                        case 'int32':
                        case 'int64':
                            new_field.type = 'int';
                            break;
                        case 'boolean':
                            new_field.type = obj.type;
                            break;
                        case 'checkcolumn':
                            new_field.type = 'boolean';
                            break;
                        case 'combobox':
                            new_field.type = 'string';
                            break;
                        case 'currency':
                            new_field.type = 'float';
                            break;
                        default:
                            break;
                    }
                    return new_field;
                } else {
                    defaultFieldsValue[obj.name] = "";
                    new_field.name = obj.name;
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                    new_field.name = obj.name + '__Id';
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__LogicalName'] = Ext.util.Format.lowercase(obj.entityNameLookup);
                    new_field.name = obj.name + '__LogicalName';
                    fields.push(new_field);
                    return fields;
                }
            }
            var _createFields = function () {
                var fields = [];
                var new_field = {};
                new_field.name = entitySchemaName + 'Id';
                defaultFieldsValue[entitySchemaName + 'Id'] = '';
                fields.push(new_field);
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFields(configJson.fields[i].group[j]);
                            if ($.isArray(obj)) {
                                fields = fields.concat(obj);
                            } else {
                                fields.push(obj);
                            }
                        }
                    } else {
                        obj = _makeObjFields(configJson.fields[i]);
                        if ($.isArray(obj)) {
                            fields = fields.concat(obj);
                        } else {
                            fields.push(obj);
                        }
                    }
                }
                return fields;
            }
            var _makeObjColumns = function (obj) {
                var new_column = {};
                var new_editor = {};
                var new_filter = {};
                var renderer = null;
                if (obj.type != "lookup") {
                    new_column.dataIndex = obj.name;
                    new_column.header = obj.header;
                    new_column.tooltip = obj.header;
                    //new_column.draggable = false;
                    if (typeof obj.sortable !== 'undefined') {
                        new_column.sortable = obj.sortable;
                    }

                    if (typeof obj.filterable !== 'undefined') {
                        new_column.filterable = obj.filterable;
                    } else {
                        new_column.filterable = false;
                    }
                    if (!new_column.filterable) {
                        new_column.filter = false;
                    }

                    if (typeof obj.hidden !== 'undefined') {
                        new_column.hidden = obj.hidden;
                    } else {
                        new_column.hidden == false;
                    }
                    if (typeof obj.flex !== 'undefined') {
                        new_column.flex = obj.flex;
                    } else {
                        if (typeof obj.width !== 'undefined') {
                            new_column.width = obj.width;
                        }
                    }

                    /*
                     if(typeof obj.format !== 'undefined'){
                     new_editor.format = obj.format;
                     }
                     */
                    switch (obj.type) {
                        case 'date':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        //new_editor.id = obj.name;
                                        new_editor.xtype = 'datefield';
                                        //new_editor.format = dateFormat;
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) {
                                                lookupActive = "";
                                            }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else {
                                    if (typeof obj.format !== 'undefined') {
                                        new_column.renderer = Ext.util.Format.dateRenderer(obj.format);
                                    } else {
                                        new_column.renderer = Ext.util.Format.dateRenderer(dateFormat);
                                    }
                                }
                            } else {
                                new_editor.xtype = 'hidden';
                                new_column.editor = new_editor;
                            }
                            break;
                        case 'number':
                        case 'decimal':
                        case 'int':
                        case 'int32':
                        case 'int64':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'numberfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;

                                        if (obj.name == 'ddsm_Units') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    measCalc_Offered(editor.value, Ext.getCmp(idGrid).getSelectionModel().getSelection()[0], 'listeners change');

                                                },
                                                focus: function (editor, e) {
                                                    lookupActive = "";
                                                }
                                            }
                                        }

                                        if (obj.name == 'ddsm_kWhGrossSavingsatMeter') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    //Ext.getCmp(idGrid).getSelectionModel().getSelection()[0].set("ddsm_SavingskWh", editor.value);
                                                    //rowEditing.getEditor().getForm().findField('ddsm_SavingskWh').setValue(editor.value);
                                                },
                                                focus: function (editor, e) {
                                                    lookupActive = "";
                                                }
                                            }
                                        }

                                        if (obj.name == 'ddsm_kWGrossSavingsatMeter') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    //Ext.getCmp(idGrid).getSelectionModel().getSelection()[0].set("ddsm_SavingsKW", editor.value);
                                                    //rowEditing.getEditor().getForm().findField('ddsm_SavingsKW').setValue(editor.value);
                                                },
                                                focus: function (editor, e) {
                                                    lookupActive = "";
                                                }
                                            }
                                        }

                                        if (obj.name == 'ddsm_Savingsthm') {
                                            //if (obj.name == 'ddsm_phase1savingsthm') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    //Ext.getCmp(idGrid).getSelectionModel().getSelection()[0].set("ddsm_Savingsthm", editor.value);
                                                    //rowEditing.getEditor().getForm().findField('ddsm_Savingsthm').setValue(editor.value);
                                                },
                                                focus: function (editor, e) {
                                                    lookupActive = "";
                                                }
                                            }
                                        }


                                        if (typeof new_editor.listeners != "object") {
                                            new_editor.listeners = {
                                                focus: function (editor, e) {
                                                    lookupActive = "";
                                                }
                                            }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else if (typeof obj.format !== 'undefined') {
                                    new_column.renderer = Ext.util.Format.numberRenderer(obj.format);
                                } else if (obj.type == 'decimal') {
                                    new_column.renderer = Ext.util.Format.numberRenderer("0.00");
                                }
                            } else {
                                new_editor.xtype = 'hidden';
                                new_column.editor = new_editor;
                            }
                            break;
                        case 'combobox':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'combo';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) {
                                                lookupActive = "";
                                            }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else {
                                new_editor.xtype = 'hidden';
                                new_column.editor = new_editor;
                            }
                            break;
                        case 'currency':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'numberfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;

                                        if (obj.name == 'ddsm_CostEquipment') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    calcCostBreakdown();
                                                },
                                                focus: function (editor, e) {
                                                    lookupActive = "";
                                                }
                                            }
                                        } else if (obj.name == 'ddsm_CostLabor') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    calcCostBreakdown();
                                                },
                                                focus: function (editor, e) {
                                                    lookupActive = "";
                                                }
                                            }
                                        } else if (obj.name == 'ddsm_CostOther') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    calcCostBreakdown();
                                                },
                                                focus: function (editor, e) {
                                                    lookupActive = "";
                                                }
                                            }
                                        } else if (obj.name == 'ddsm_IncrementalCost') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    calcCostBreakdown();
                                                },
                                                focus: function (editor, e) {
                                                    lookupActive = "";
                                                }
                                            }
                                        }

                                        if (obj.name == 'ddsm_IncentivePaymentTotalGross') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    Ext.getCmp(idGrid).getSelectionModel().getSelection()[0].set("ddsm_IncentivePaymentTotalGross", editor.value);
                                                    rowEditing.getEditor().getForm().findField('ddsm_IncentivePaymentTotalGross').setValue(editor.value);
                                                },
                                                focus: function (editor, e) {
                                                    lookupActive = "";
                                                }
                                            }
                                        }

                                        if (typeof new_editor.listeners != "object") {
                                            new_editor.listeners = {
                                                focus: function (editor, e) {
                                                    lookupActive = "";
                                                }
                                            }
                                        }

                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else {
                                    new_column.renderer = Ext.util.Format.usMoney;
                                }
                            } else {
                                new_editor.xtype = 'hidden';
                                new_column.editor = new_editor;
                            }
                            break;
                        case 'boolean':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        //                                new_editor.xtype = 'textfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) {
                                                lookupActive = "";
                                            }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else {
                                new_editor.xtype = 'hidden';
                                new_column.editor = new_editor;
                            }
                            break;
                        case 'checkcolumn':
                            if (!new_column.hidden) {
                                new_column.xtype = 'checkcolumn';
                                new_column.listeners = {
                                    checkChange: onCheckChange
                                };
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else {
                                new_editor.xtype = 'hidden';
                                new_column.editor = new_editor;
                            }
                            break;
                        default:
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'textfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) {
                                                lookupActive = "";
                                            }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }

                                if (new_column.filterable) {
                                    if (obj.name == "ddsm_name") {
                                        //new_column.filterElement = new Ext.form.TextField();
                                        /*
                                         new_column.items = [{
                                         xtype: 'searchtrigger',
                                         autoSearch: true,
                                         anyMatch: true
                                         }];
                                         */
                                    }
                                }

                            } else {
                                new_editor.xtype = 'hidden';
                                new_column.editor = new_editor;
                            }
                            break;
                    }
                    if (typeof obj.hidden !== 'undefined' && obj.hidden) {
                        hide_columns.push(new_column);
                        return null;
                    } else {
                        return new_column;
                    }
                } else {

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name + '__Id';
                    new_column.header = obj.name + '__Id';
                    new_column.filterable = false;
                    new_column.sortable = false;
                    new_column.hidden = true;
                    new_column.width = 50;
                    new_editor.itemId = obj.name + '__Id';
                    new_editor.readOnly = true;
                    new_editor.allowBlank = true;
                    new_editor.xtype = 'hidden';
                    new_column.editor = new_editor;
                    hide_columns.push(new_column);

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name + '__LogicalName';
                    new_column.header = obj.name + '__LogicalName';
                    new_column.filterable = false;
                    new_column.sortable = false;
                    new_column.hidden = true;
                    new_column.width = 50;
                    new_editor.itemId = obj.name + '__LogicalName';
                    new_editor.readOnly = true;
                    new_editor.allowBlank = true;
                    new_editor.xtype = 'hidden';
                    new_column.editor = new_editor;
                    hide_columns.push(new_column);

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name;
                    new_column.header = obj.header;
                    new_column.tooltip = obj.header;
                    //new_column.draggable = false;
                    if (typeof obj.sortable !== 'undefined') {
                        new_column.sortable = obj.sortable;
                    }
                    if (typeof obj.filterable !== 'undefined') {
                        new_column.filterable = obj.filterable;
                    }
                    if (typeof obj.hidden !== 'undefined') {
                        new_column.hidden = obj.hidden;
                    }
                    if (typeof obj.flex !== 'undefined') {
                        new_column.flex = obj.flex;
                    } else {
                        if (typeof obj.width !== 'undefined') {
                            new_column.width = obj.width;
                        }
                    }
                    /*
                     if(typeof obj.format !== 'undefined'){
                     new_editor.format = obj.format;
                     }
                     */
                    if (!new_column.hidden) {
                        if (!obj.readOnly) {

                            if (typeof obj.editor !== 'undefined') {
                                if (typeof obj.editor !== 'object') {
                                    new_column.editor = eval(obj.editor);
                                } else {
                                    new_column.editor = obj.editor;
                                }
                            } else {
                                new_editor.itemId = obj.name;
                                //new_editor.readOnly = obj.readOnly;
                                new_editor.allowBlank = obj.allowBlank;
                                new_editor.listeners = {
                                    //scope:this,
                                    focus: function (e) {

                                        //if ((this.value == "" || this.value == null || typeof this.value == 'undefined') && lookupActive != e.name && !newRecordRow) {
                                        lookupActive = e.name;
                                        _getLookupData(e.name);
                                        //}
                                    }
                                };

                                new_column.editor = new_editor;
                            }
                        }
                        if (typeof obj.renderer !== 'undefined') {
                            if (typeof obj.renderer !== 'object') {
                                new_column.renderer = eval(obj.renderer);
                            } else {
                                new_column.renderer = obj.renderer;
                            }
                        } else {
                            new_column.renderer = function (value, metaData, record, rowIdx, colIdx, store, view) {
                                var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                                var column = view.getHeaderAtIndex(colIdx);
                                if (value != null) {
                                    return Ext.String.format(
                                        '<a href="/main.aspx?etn={2}&extraqs=&histKey={3}&id={1}&newWindow=true&pagetype=entityrecord" target="_blank">{0}</a>',
                                        value,
                                        "{" + record.data[column.dataIndex + "__Id"] + "}",
                                        record.data[column.dataIndex + "__LogicalName"],
                                        randomnumber
                                    );
                                } else {
                                    return '';
                                }
                            };
                        }
                    } else {
                        new_editor.xtype = 'hidden';
                        new_column.editor = new_editor;
                    }

                    if (typeof obj.hidden !== 'undefined' && obj.hidden) {
                        hide_columns.push(new_column);
                        return null;
                    } else {
                        return new_column;
                    }
                }

            }
            var _createColumns = function () {
                var tmpOrder = false;
                var columns = [];
                var firstColumns = [];
                var endColumns = [];
                var offeredColumns = [];
                var committedColumns = [];
                var insalledColumns = [];
                var viewColumnsBase = [];
                columns.push(Ext.create('Ext.grid.RowNumberer'));
                viewColumnsBase.push(Ext.create('Ext.grid.RowNumberer'));
                var new_column = {};
                var new_editor = {};
                var new_filter = {};
                var renderer = null;
                new_column.dataIndex = entitySchemaName + 'Id';
                new_column.header = 'Id';
                new_column.filterable = false;
                new_column.sortable = false;
                new_column.hidden = true;
                new_column.width = 150;
                new_editor.readOnly = true;
                new_editor.allowBlank = true;
                new_editor.xtype = 'hidden';
                new_column.editor = new_editor;
                hide_columns.push(new_column);

                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        var group = {};
                        group.header = configJson.fields[i].groupHeader;
                        group.menuDisabled = true,
                            group.columns = [];
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjColumns(configJson.fields[i].group[j]);
                            if (obj != null) {
                                if ($.isArray(obj)) {
                                    group.columns = group.columns.concat(obj);
                                } else {
                                    group.columns.push(obj);
                                }
                            }
                        }

                        if (group.header == '1. Opportunity' || group.header == '1. Offered') {
                            if (!tmpOrder) {
                                firstColumns = firstColumns.concat(columns);
                                columns = [];
                                tmpOrder = true;
                            }
                            offeredColumns.push(group);
                            viewColumnsBase = viewColumnsBase.concat(group.columns);
                        } else if (group.header == '2. Project') {
                            if (!tmpOrder) {
                                firstColumns = firstColumns.concat(columns);
                                columns = [];
                                tmpOrder = true;
                            }
                            committedColumns.push(group);
                            viewColumnsBase = viewColumnsBase.concat(group.columns);
                        } else if (group.header == '3. Evaluation') {
                            if (!tmpOrder) {
                                firstColumns = firstColumns.concat(columns);
                                columns = [];
                                tmpOrder = true;
                            }
                            insalledColumns.push(group);
                            viewColumnsBase = viewColumnsBase.concat(group.columns);
                        } else {
                            columns.push(group);
                            viewColumnsBase = viewColumnsBase.concat(group.columns);
                        }

                    } else {
                        obj = _makeObjColumns(configJson.fields[i]);
                        if (obj != null) {
                            if ($.isArray(obj)) {
                                columns = columns.concat(obj);
                                viewColumnsBase = viewColumnsBase.concat(obj);
                            } else {
                                columns.push(obj);
                                viewColumnsBase.push(obj);
                            }
                        }
                    }
                }

                columns.push(changeSKUColumn);
                columns.push(delActColumn);
                columns = columns.concat(hide_columns);
                endColumns = endColumns.concat(columns);
                phase1Columns = phase1Columns.concat(firstColumns, offeredColumns, committedColumns, insalledColumns, endColumns);
                phase2Columns = phase2Columns.concat(firstColumns, committedColumns, insalledColumns, offeredColumns, endColumns);
                phase3Columns = phase3Columns.concat(firstColumns, insalledColumns, committedColumns, offeredColumns, endColumns);
                if (b.Xrm.Page.getAttribute("ddsm_phase").getValue() == "2. Project") viewColumns = phase2Columns;
                else if (b.Xrm.Page.getAttribute("ddsm_phase").getValue() == "3. Evaluation") viewColumns = phase3Columns;
                else viewColumns = phase1Columns;

                return viewColumns;
            }
            var _makeObjFilters = function (obj) {
                var filterType = {};
                if (obj.filterable) {
                    if (typeof obj.filter !== 'undefined') {
                        if (typeof obj.filter !== 'filterTypeect') {
                            filterType = eval(obj.filter);
                        } else {
                            filterType = obj.filter;
                        }
                    } else {
                        switch (obj.type) {
                            case 'date':
                                filterType.type = 'date';
                                break;
                            case 'number':
                            case 'currency':
                            case 'decimal':
                            case 'int':
                            case 'int32':
                            case 'in64':
                                filterType.type = 'numeric';
                                break;
                            case 'combobox':
                                /*
                                 if(typeof obj.editor !== 'undefined'){
                                 if(typeof obj.editor!== 'filterTypeect'){
                                 var cb = eval(obj.editor);
                                 } else {
                                 var cb = obj.editor;
                                 }
                                 filterType.type = 'list';
                                 filterType.labelField = 'nom_categorie';
                                 filterType.options = [
                                 [ "0", "inactive" ],
                                 [ "1", "in work" ],
                                 [ "2", "executed" ],
                                 [ "3", "canceled" ]
                                 ];
                                 }
                                 */
                                break;
                            case 'currency':
                                filterType.type = 'numeric';
                                break;
                            case 'lookup':
                                filterType.type = 'string';
                                break;
                            default:
                                filterType.type = 'string';
                                break;
                        }
                        filterType.dataIndex = obj.name;
                    }
                }
                return filterType;

            }
            var _createFilters = function () {
                var filterObj = {
                    ftype: 'filters',
                    encode: false,
                    local: true,
                    filters: []
                }
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFilters(configJson.fields[i].group[j]);
                            if (obj != null) {
                                if ($.isArray(obj)) {
                                    filterObj.filters = filterObj.filters.concat(obj);
                                } else {
                                    filterObj.filters.push(obj);
                                }
                            }
                        }
                    } else {
                        obj = _makeObjFilters(configJson.fields[i]);
                        if (obj != null) {
                            if ($.isArray(obj)) {
                                filterObj.filters = filterObj.filters.concat(obj);
                            } else {
                                filterObj.filters.push(obj);
                            }
                        }
                    }
                }
                return filterObj;
            };

            Ext.define(modelGrid, {
                extend: 'Ext.data.Model',
                fields: _createFields()
            });

            var _createStore = function (data, fields) {
                return Ext.create('Ext.data.Store', {
                    model: modelGrid,
                    //autoDestroy: true,
                    fields: fields,
                    data: data,
                    autoLoad: false,
                    autoSync: false,
                    sortOnLoad: true,
                    remoteSort: false,
                    remoteFilter: false,
                    simpleSortMode: true,
                    sorters: sortersGrid,
                    //groupField: 'ddsm_ProjectPhaseName',
                    proxy: {
                        type: 'memory',
                        reader: {
                            type: 'json'
                        }
                    }
                });
            };
            var _toUTCDate = function (date) {
                return new Date(date.getTime());

                var monthString;
                var rawMonth = (date.getUTCMonth() + 1).toString();
                //var rawMonth = (date.getMonth()+1).toString();
                if (rawMonth.length == 1) {
                    monthString = "0" + rawMonth;
                }
                else {
                    monthString = rawMonth;
                }

                var dateString;
                var rawDate = date.getUTCDate().toString();
                //var rawDate = date.getDate().toString();
                if (rawDate.length == 1) {
                    dateString = "0" + rawDate;
                }
                else {
                    dateString = rawDate;
                }

                var hoursString;
                var rawHours = date.getUTCHours().toString();
                //var rawHours = date.getHours().toString();
                if (rawHours.length == 1) {
                    hoursString = "0" + rawHours;
                }
                else {
                    hoursString = rawHours;
                }

                var minutesString;
                var rawMinutes = date.getUTCMinutes().toString();
                //var rawMinutes = date.getMinutes().toString();
                if (rawMinutes.length == 1) {
                    minutesString = "0" + rawMinutes;
                }
                else {
                    minutesString = rawMinutes;
                }

                var secondsString;
                var rawSeconds = date.getUTCSeconds().toString();
                //var rawSeconds = date.getSeconds().toString();
                if (rawSeconds.length == 1) {
                    secondsString = "0" + rawSeconds;
                }
                else {
                    secondsString = rawSeconds;
                }

                var serverDateFormat = "";
                serverDateFormat += date.getUTCFullYear() + "-";
                //serverDateFormat += date.getFullYear() + "-";
                serverDateFormat += monthString + "-";
                serverDateFormat += dateString;
                serverDateFormat += "T" + hoursString + ":";
                serverDateFormat += minutesString + ":";
                serverDateFormat += secondsString;
                serverDateFormat += "Z";
                return serverDateFormat;
            };

            function UTCToLocalTime(d) {
                /*
                 var timeOffset = -((new Date()).getTimezoneOffset()/60);
                 d.setHours(d.getHours() + timeOffset);
                 return d;
                 */

                var date = new Date(d);
                return ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();

            }

            var _getLookupDataMT = function(entityName) {
                //var programOffering = b.Xrm.Page.getAttribute("ddsm_programoffering").getValue();
                entityName_getLookupData = entityName;
                //var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                //var localname = record.get(entityName + '__LogicalName');
                var localname = entityName;
                var objecttypes = b.Xrm.Internal.getEntityCode(localname);

                var viewId = "{00000000-0000-0000-0000-000000000001}";
                var fetchXml = "";
                if (addMeasureType == 1) {
                    fetchXml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                        "<entity name='ddsm_measuretemplate'>" +
                        "<attribute name='ddsm_measuretemplateid' />" +
                        "<attribute name='statecode' />" +
                        "<attribute name='ddsm_name' />" +
                        "<order attribute='ddsm_name' descending='false' />" +
                        "<filter type='and'>" +
                        "<condition attribute='ddsm_suto' operator='eq' value='962080000' />" +
                        "</filter>" +
                        "<filter type='and'>" +
                        "<condition attribute='statecode' operator='eq' value='0' />" +
                        "</filter>" +
                        "<filter type='and'>" +
                        "<condition attribute='statecode' operator='eq' value='0' />";
                    if (SelectableProgramOffering) {
                        fetchXml +=    "<condition attribute='ddsm_selectableprogramoffering' operator='eq' value='" + SelectableProgramOffering + "' />";
                    }
                    fetchXml += "</filter>" +
                        "<filter type='or' >"+
                        "<condition attribute='ddsm_selectablestartdate' operator='today' />"+
                        "<condition attribute='ddsm_selectablestartdate' operator='last-x-days' value='500' />"+
                        "</filter>"+
                        "<filter type='or' >"+
                        "<condition attribute='ddsm_selectableenddate' operator='today' />"+
                        "<condition attribute='ddsm_selectableenddate' operator='next-x-days' value='500' />" +
                        "</filter>"+
                        "</entity>" +
                        "</fetch>";
                } else if (addMeasureType == 2) {
                    fetchXml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                        "<entity name='ddsm_measuretemplate'>" +
                        "<attribute name='ddsm_measuretemplateid' />" +
                        "<attribute name='statecode' />" +
                        "<attribute name='ddsm_name' />" +
                        "<order attribute='ddsm_name' descending='false' />" +
                        "<filter type='and'>" +
                        "<condition attribute='ddsm_suto' operator='eq' value='962080003' />" +
                        "</filter>" +
                        "<filter type='and'>" +
                        "<condition attribute='statecode' operator='eq' value='0' />";
                    if (SelectableProgramOffering) {
                        fetchXml +=    "<condition attribute='ddsm_selectableprogramoffering' operator='eq' value='" + SelectableProgramOffering + "' />";
                    }
                    fetchXml += "</filter>" +
                        "<filter type='or' >"+
                        "<condition attribute='ddsm_selectablestartdate' operator='today' />"+
                        "<condition attribute='ddsm_selectablestartdate' operator='last-x-days' value='500' />"+
                        "</filter>"+
                        "<filter type='or' >"+
                        "<condition attribute='ddsm_selectableenddate' operator='today' />"+
                        "<condition attribute='ddsm_selectableenddate' operator='next-x-days' value='500' />" +
                        "</filter>"+
                        "</entity>" +
                        "</fetch>";
                } else {
                    fetchXml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                        "<entity name='ddsm_measuretemplate'>" +
                        "<attribute name='ddsm_measuretemplateid' />" +
                        "<attribute name='statecode' />" +
                        "<attribute name='ddsm_name' />" +
                        "<order attribute='ddsm_name' descending='false' />" +
                        "<filter type='and'>" +
                        "<condition attribute='ddsm_measurecalculationtype' operator='eq' value='34f3fa1c-e5d6-e511-80e4-c4346bac0250' />" +
                        "</filter>" +
                        "<filter type='and'>" +
                        "<condition attribute='statecode' operator='eq' value='0' />" +
                        "</filter>" +
                        "</entity>" +
                        "</fetch>";
                }
                var layoutXml = "" +
                    "<grid name='' object='1' jump='name' select='1' icon='1' preview='1'>" +
                    "<row name='result' id='ddsm_measuretemplateid'>" +
                    "<cell name='ddsm_name' width='300' />" +
                    "</row>" +
                    "</grid>";

                var customView = {
                    fetchXml: fetchXml,
                    id: viewId,
                    layoutXml: layoutXml,
                    name: "Measure Tpl LookupView (PlaceholderMeasure=False)",
                    recordType: objecttypes,
                    Type: 0
                };

                var callbackReference = {
                    callback: function (lkp_popup) {
                        try {
                            if (lkp_popup && lkp_popup.items) {
                                if (lkp_popup.items) {
                                    var measTpl = {};
                                    measTpl.id = lkp_popup.items[0].id;
                                    measTpl.name = lkp_popup.items[0].name;
                                    measTpl.typename = lkp_popup.items[0].typename;
                                    //console.dir(measTpl);
                                    createNewMeasure(measTpl);
                                    /*
                                     rowEditing.getEditor().getForm().findField(entityName_getLookupData).setValue(lkp_popup.items[0].name);
                                     rowEditing.getEditor().getForm().findField(entityName_getLookupData + '__Id').setValue((lkp_popup.items[0].id).replace(/\{|\}/g, ''));
                                     rowEditing.getEditor().getForm().findField(entityName_getLookupData + '__LogicalName').setValue(lkp_popup.items[0].typename);
                                     var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                                     record.set(entityName_getLookupData, lkp_popup.items[0].name);
                                     record.set(entityName_getLookupData + '__Id', (lkp_popup.items[0].id).replace(/\{|\}/g, ''));
                                     record.set(entityName_getLookupData + '__LogicalName', lkp_popup.items[0].typename);
                                     if (lkp_popup.items[0].typename === "ddsm_measuretemplate") { loadMeasTpl(lkp_popup.items[0]); }
                                     */
                                }
                                lookupActive = "";
                            } else {alert("no selected record");}
                        } catch (err) {
                            alert("Error select measure template: " + err);
                        }
                    }
                }

                b.LookupObjectsWithCallback(callbackReference, null, "single", objecttypes, 0, null, "", null, null, null, null, null, null, viewId, [customView]);

            }

            var _getLookupData = function (entityName) {
                entityName_getLookupData = entityName;
                var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                var localname = record.get(entityName + '__LogicalName');
                var objecttypes = b.Xrm.Internal.getEntityCode(localname);
                var lookupURI = "/_controls/lookup/lookupinfo.aspx";
                lookupURI += "?LookupStyle=single";
                lookupURI += "&objecttypes=" + objecttypes;
                lookupURI += "&ShowNewButton=0";
                lookupURI += "&ShowPropButton=1";
                lookupURI += "&browse=false";
                lookupURI += "&AllowFilterOff=0";
                lookupURI += "&DefaultType=" + objecttypes;
                lookupURI += "&DisableQuickFind=0";
                lookupURI += "&DisableViewPicker=0";
                //console.log("lookupURI: " + lookupURI);

                window.setTimeout(function () {
                    var DialogOption = new b.Xrm.DialogOptions;
                    DialogOption.width = 550; DialogOption.height = 550;
                    b.Xrm.Internal.openDialog(lookupURI, DialogOption, null, null, Callback_lkp_popup);
                }, 200);
            }

            function Callback_lkp_popup(lkp_popup) {
                try {
                    if (lkp_popup) {
                        if (lkp_popup.items) {
                            //console.dir(lkp_popup.items);
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData).setValue(lkp_popup.items[0].name);
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData + '__Id').setValue((lkp_popup.items[0].id).replace(/\{|\}/g, ''));
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData + '__LogicalName').setValue(lkp_popup.items[0].typename);
                            var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                            record.set(entityName_getLookupData, lkp_popup.items[0].name);
                            record.set(entityName_getLookupData + '__Id', (lkp_popup.items[0].id).replace(/\{|\}/g, ''));
                            record.set(entityName_getLookupData + '__LogicalName', lkp_popup.items[0].typename);
                            //if (lkp_popup.items[0].typename === "ddsm_measuretemplate") {
                            //loadMeasTpl(lkp_popup.items[0]);
                            //}
                        }
                        lookupActive = "";
                    }
                }
                catch (err) {
                    alert(err);
                }
            }

            var qtip = Ext.create('Ext.tip.QuickTip', {});
            rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToMoveEditor: 1,
                autoCancel: false
            });
            var viewConfig = {
                //loadingText: "Loading data ...",
                stripeRows: true,
                //trackOver:false,
                getRowClass: function (record, index, rowParams, store) {
                    if (record.get('ddsm_IncludedinFinancial')) {
                        return 'fnclMeas';
                    }
                    if (record.get('ddsm_ModelNumberStatus') == '962080002') {
                        return 'denied';
                    } else if (record.get('ddsm_ModelNumberStatus') == '962080000') {
                        return 'approved';
                    }
                }
            };

            //// START CHECKBOX
            function onCheckChange(column, rowIndex, checked, eOpts) {
                //var record = Ext.getCmp(idGrid).getStore().getAt(rowIndex);
                //Save check/unchek record row
                //saveGridStatus = false;

                //Ext.getCmp(idGrid).getStore().commitChanges();
                //Ext.getCmp(idGrid).getView().refresh();

                //var record = Ext.getCmp(idGrid).getStore().getAt(rowIndex);
                //console.dir(record);
            }

            //// END CHECKBOX


            loadData = function (URI, reload) {
                var proj_ID = b.Xrm.Page.data.entity.getId();
                if (proj_ID !== "") {
                    if (URI == _getODataEndpoint(entitySchemaName)) {
                        var phasenumber = parseInt(b.Xrm.Page.getAttribute("ddsm_phasenumber").getValue());
                        URI = URI + "?$filter=ddsm_ProjectToMeasureId/Id eq guid'" + proj_ID + "' and ddsm_UpToDatePhase eq " + phasenumber;
                        URI = URI + " and ddsm_RecalculationGroup eq null";
                        //URI = URI + "?$orderby=ddsm_UpToDatePhase desc&$filter=ddsm_ProjectToMeasureId/Id eq guid'" + proj_ID + "'";
                        results = [];
                    }
                    //console.log(URI + filter);
                    Ext.getCmp(idGrid).mask("Loading data, please wait...");

                    var req = new XMLHttpRequest();
                    req.open("GET", URI, false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.onreadystatechange = function () {
                        if (this.readyState == 4) {
                            req.onreadystatechange = null;
                            if (this.status == 200) {

                                var responseData = JSON.parse(this.responseText).d;
                                var newData = [];
                                var modeResponse = responseData.results;
                                if (modeResponse.length > 0) {
                                    for (var i = 0; i < modeResponse.length; i++) {
                                        //                        console.dir(modeResponse[i]);
                                        var objRecord = new Object();
                                        objRecord[entitySchemaName + "Id"] = modeResponse[i][entitySchemaName + "Id"];
                                        for (var j = 0; j < modConfigLoadSaveData.length; j++) {
                                            switch (modConfigLoadSaveData[j].type) {
                                                case 'date':
                                                    if ((typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') && (modeResponse[i][modConfigLoadSaveData[j].name] != null)) {
                                                        objRecord[modConfigLoadSaveData[j].name] = eval((modeResponse[i][modConfigLoadSaveData[j].name]).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                                    } else if (modeResponse[i][modConfigLoadSaveData[j].name] == null) {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name]
                                                    }
                                                    break
                                                case 'lookup':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined' && modeResponse[i][modConfigLoadSaveData[j].name].Id != null) {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name].Name;
                                                        objRecord[modConfigLoadSaveData[j].name + '__Id'] = modeResponse[i][modConfigLoadSaveData[j].name].Id;
                                                        objRecord[modConfigLoadSaveData[j].name + '__LogicalName'] = modeResponse[i][modConfigLoadSaveData[j].name].LogicalName;
                                                    } else if (modeResponse[i][modConfigLoadSaveData[j].name].Id == null || modeResponse[i][modConfigLoadSaveData[j].name].Id == '') {
                                                        objRecord[modConfigLoadSaveData[j].name] = '';
                                                        objRecord[modConfigLoadSaveData[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                                                        objRecord[modConfigLoadSaveData[j].name + '__LogicalName'] = modConfigLoadSaveData[j].entityNameLookup;
                                                    }
                                                    break
                                                case 'combobox':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name].Value;
                                                    }
                                                    break
                                                case 'currency':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name].Value;
                                                    }
                                                    break
                                                case 'number':
                                                case 'decimal':
                                                case 'int':
                                                case 'int32':
                                                case 'int64':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name];
                                                    }
                                                    break
                                                default:
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name];
                                                    }
                                            }
                                        }
                                        /*
                                         var measSelector = modeResponse[i]["ddsm_MeasureSelector"];
                                         var updateObj = verifyVersionMeasTpl(measSelector);

                                         if(updateObj != null) {
                                         for(key in updateObj) {
                                         objRecord[key] = updateObj[key];
                                         }
                                         }
                                         */
                                        //console.dir(measSelector.Id);
                                        /*
                                         if(measSelector.Id != null) {
                                         var objSKU = getSKUdata(measSelector.Id);
                                         if(objSKU != null) {
                                         //console.dir(objSKU);
                                         objRecord.ddsm_Manufacturer = objSKU.ddsm_Manufacturer;
                                         objRecord.ddsm_ModelNumber = objSKU.ddsm_ModelNumber;
                                         objRecord.ddsm_ModelNumberStatus = objSKU.ddsm_Status.Value.toString();

                                         //update measure
                                         var changes = {};
                                         changes.ddsm_Manufacturer = objSKU.ddsm_Manufacturer;
                                         changes.ddsm_ModelNumber = objSKU.ddsm_ModelNumber;
                                         changes.ddsm_ModelNumberStatus = objSKU.ddsm_Status;
                                         var reqUpd = new XMLHttpRequest();
                                         reqUpd.open("POST", _getODataEndpoint(entitySchemaName) + "(guid'" + modeResponse[i]["ddsm_measureId"] + "')", true);
                                         reqUpd.setRequestHeader("Accept", "application/json");
                                         reqUpd.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                                         reqUpd.setRequestHeader("X-HTTP-Method", "MERGE");
                                         reqUpd.send(JSON.stringify(changes));
                                         }
                                         }
                                         */
                                        newData.push(objRecord);

                                    }
                                    results = results.concat(newData);
                                    if (responseData.__next) {
                                        //console.log(responseData.__next);
                                        Ext.getCmp(idGrid).unmask();
                                        loadData(responseData.__next, reload);
                                    } else {
                                        console.log(">>>>> load data ok! records: " + results.length);
                                        dataOk = true;
                                        //b.Xrm.Page.ui.tabs.get("Tab_Measures").setDisplayState('expanded');
                                        //b.Xrm.Page.ui.tabs.get("Tab_Measures").setVisible(true);

                                        if (!reload) {
                                            Ext.define(modelGrid, {
                                                extend: 'Ext.data.Model',
                                                fields: _createFields()
                                            });
                                            Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                            Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            Ext.getCmp(idGrid).getView().refresh();
                                            //console.log(b.Xrm.Page.getAttribute("ddsm_phase").getValue());
                                            /*
                                             if(b.Xrm.Page.getAttribute("ddsm_phase").getValue() == "2. Project") {
                                             _moveColumnPhase12();
                                             } else if(b.Xrm.Page.getAttribute("ddsm_phase").getValue() == "3. Evaluation") {
                                             _moveColumnPhase23();
                                             }
                                             */
                                        } else {
                                            Ext.getCmp(idGrid).getStore().loadData(results);
                                            Ext.getCmp(idGrid).filters.clearFilters();
                                            Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            Ext.getCmp(idGrid).getView().refresh();
                                        }
                                        /*
                                         window.setTimeout(function () {
                                         if (b.firstLoadGrid != 3) {
                                         b.firstLoadGrid++;
                                         b.Xrm.Page.ui.tabs.get("Tab_Measures").setVisible(false);
                                         }
                                         }, 1000);
                                         */
                                        saveGridStatus = true;
                                        Ext.getCmp(idGrid).unmask();
                                        recalcMeasAfterUpdMeasTpl(arrayIds);
                                        b.oldProjPhase = b.Xrm.Page.getAttribute("ddsm_phase").getValue();
                                    }
                                    calcCostBreakdownAll();
                                } else {
                                    console.log(">>>>> data entity null!");
                                    b.oldProjPhase = b.Xrm.Page.getAttribute("ddsm_phase").getValue();
                                    dataOk = false;
                                    //b.Xrm.Page.ui.tabs.get("Tab_Measures").setDisplayState('expanded');
                                    //b.Xrm.Page.ui.tabs.get("Tab_Measures").setVisible(true);

                                    if (!reload) {
                                        Ext.define(modelGrid, {
                                            extend: 'Ext.data.Model',
                                            fields: _createFields()
                                        });
                                        Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                        /*
                                         if(b.Xrm.Page.getAttribute("ddsm_phase").getValue() == "2. Project") {
                                         _moveColumnPhase12();
                                         } else if(b.Xrm.Page.getAttribute("ddsm_phase").getValue() == "3. Evaluation") {
                                         _moveColumnPhase23();
                                         }
                                         */
                                    } else {
                                        Ext.getCmp(idGrid).getStore().loadData(results);
                                        Ext.getCmp(idGrid).filters.clearFilters();
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    }
                                    /*
                                     window.setTimeout(function () {
                                     if (b.firstLoadGrid != 3) {
                                     b.firstLoadGrid++;
                                     b.Xrm.Page.ui.tabs.get("Tab_Measures").setVisible(false);
                                     }
                                     }, 1000);
                                     */
                                    saveGridStatus = true;
                                    Ext.getCmp(idGrid).unmask();
                                    b.oldProjPhase = b.Xrm.Page.getAttribute("ddsm_phase").getValue();
                                }
                            } else {
                                if (results.length > 0) {
                                    console.log(">>>>> load data ok! records: " + results.length);
                                    dataOk = true;
                                    //b.Xrm.Page.ui.tabs.get("Tab_Measures").setDisplayState('expanded');
                                    //b.Xrm.Page.ui.tabs.get("Tab_Measures").setVisible(true);

                                    if (!reload) {
                                        Ext.define(modelGrid, {
                                            extend: 'Ext.data.Model',
                                            fields: _createFields()
                                        });
                                        Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                        /*
                                         if(b.Xrm.Page.getAttribute("ddsm_phase").getValue() == "2. Project") {
                                         _moveColumnPhase12();
                                         } else if(b.Xrm.Page.getAttribute("ddsm_phase").getValue() == "3. Evaluation") {
                                         _moveColumnPhase23();
                                         }
                                         */
                                    } else {
                                        Ext.getCmp(idGrid).getStore().loadData(results);
                                        Ext.getCmp(idGrid).filters.clearFilters();
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    }
                                    /*
                                     window.setTimeout(function () {
                                     if (b.firstLoadGrid != 3) {
                                     b.firstLoadGrid++;
                                     b.Xrm.Page.ui.tabs.get("Tab_Measures").setVisible(false);
                                     }
                                     }, 1000);
                                     */
                                    saveGridStatus = true;
                                }
                                b.oldProjPhase = b.Xrm.Page.getAttribute("ddsm_phase").getValue();
                                Ext.getCmp(idGrid).unmask();
                            }
                        }
                    };
                    lookupActive = "";
                    req.send(null);
                    if(newRecordRow) {
                        newRecordRow = false;
                    }
                }
            }

            function getSKUdata(measTplId) {

                var URI = _getODataEndpoint("ddsm_measuretemplate") + "?$select=ddsm_SKU&$filter=ddsm_measuretemplateId eq guid'" + measTplId + "'";
                var returnObj = null;
                var req = new XMLHttpRequest();
                req.open("GET", URI, false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        req.onreadystatechange = null;
                        if (this.status == 200) {
                            var responseData = JSON.parse(this.responseText).d;
                            var modeResponse = responseData.results;
                            //console.log("SKU.Id = " + modeResponse[0].ddsm_SKU.Id);
                            if (modeResponse[0].ddsm_SKU.Id != null) {
                                var URI1 = _getODataEndpoint("ddsm_measurelibrary") + "?$select=ddsm_ModelNumber,ddsm_Manufacturer,ddsm_Status&$filter=ddsm_measurelibraryId eq guid'" + modeResponse[0].ddsm_SKU.Id + "'";
                                var req1 = new XMLHttpRequest();
                                req1.open("GET", URI1, false);
                                req1.setRequestHeader("Accept", "application/json");
                                req1.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                                req1.onreadystatechange = function () {
                                    if (this.readyState == 4) {
                                        req1.onreadystatechange = null;
                                        if (this.status == 200) {
                                            var responseData1 = JSON.parse(this.responseText).d;
                                            var modeResponse1 = responseData1.results;
                                            //console.dir(modeResponse1[0]);
                                            returnObj = modeResponse1[0];
                                        }
                                    }
                                }
                                req1.send(null);
                            }
                        }
                    }
                }
                req.send(null);
                return returnObj;
            }

            function Update_Create_Record(grid, record, rowIndex) {
                var guidId = record.data[entitySchemaName + 'Id'];

                var changes = {};
                for (var j = 0; j < modConfigLoadSaveData.length; j++) {
                    if (modConfigLoadSaveData[j].saved || typeof modConfigLoadSaveData[j].saved === 'undefined') {
                        if (record.data[modConfigLoadSaveData[j].name] != null && (((record.data[modConfigLoadSaveData[j].name]).toString()).replace(/\s+/g, '')).length === 0) {
                            record.data[modConfigLoadSaveData[j].name] = null;
                        }

                        switch (modConfigLoadSaveData[j].type) {
                            case 'date':
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = _toUTCDate(record.data[modConfigLoadSaveData[j].name]);
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                }
                                break;
                            case 'lookup':
                                if (record.data[modConfigLoadSaveData[j].name] == null) {
                                    record.data[modConfigLoadSaveData[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                                }
                                if (record.data[modConfigLoadSaveData[j].name + '__Id'] !== '00000000-0000-0000-0000-000000000000' && record.data[modConfigLoadSaveData[j].name] != null) {
                                    var lookup = {};
                                    lookup.Name = record.data[modConfigLoadSaveData[j].name];
                                    lookup.Id = record.data[modConfigLoadSaveData[j].name + '__Id'];
                                    lookup.LogicalName = record.data[modConfigLoadSaveData[j].name + '__LogicalName'];
                                    changes[modConfigLoadSaveData[j].name] = lookup;
                                } else {
                                    var lookup = {};
                                    lookup.Name = null;
                                    lookup.Id = null;
                                    lookup.LogicalName = null;
                                    changes[modConfigLoadSaveData[j].name] = lookup;
                                }
                                break;
                            case 'combobox':
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = {Value: (record.data[modConfigLoadSaveData[j].name]).toString()};
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = {Value: record.data[modConfigLoadSaveData[j].name]};
                                }
                                break;
                            case 'number':
                            case 'decimal':
                            case 'int':
                            case 'int32':
                            case 'int64':
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = (record.data[modConfigLoadSaveData[j].name]).toString();
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                }
                                break;
                            case 'number':
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = (record.data[modConfigLoadSaveData[j].name]).toString();
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                }
                                break;
                            case 'checkcolumn':
                                changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                break;
                            case 'boolean':
                                changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                break;
                            case 'currency':
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = {Value: (record.data[modConfigLoadSaveData[j].name]).toString()};
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = {Value: record.data[modConfigLoadSaveData[j].name]};
                                }
                                break;

                            default:
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = (record.data[modConfigLoadSaveData[j].name]).toString();
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                }
                                break;
                        }
                    }
                }

                var InitialLead = b.getInitialLeadSite(b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id);
                if (InitialLead != null) {
                    changes.ddsm_InitialLead = InitialLead;
                }
                //console.dir(changes);

                if (guidId != "") {
                    var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";
                } else {
                    var URI = _getODataEndpoint(entitySchemaName);
                }
                //console.log(URI);
                var req = new XMLHttpRequest();
                req.open("POST", URI, true);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                if (guidId != "") {
                    req.setRequestHeader("X-HTTP-Method", "MERGE");
                } else {

                }
                req.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        req.onreadystatechange = null;
                        if (this.status == 201) {
                            var responseData = JSON.parse(this.responseText).d;
                            console.log("CREATE: " + responseData[entitySchemaName + 'Id']);
                            var upd_record = grid.getStore().getAt(rowIndex);
                            upd_record.set(entitySchemaName + 'Id', responseData[entitySchemaName + 'Id']);
                            upd_record.commit();
                            grid.getStore().commitChanges();
                            grid.getView().refresh();
                            //window.setTimeout(function () {b.proj_calcMeasSums();b.programNames();}, 10);
                            //var sm = grid.getSelectionModel();
                            //sm.select(rowIndex);
                        } else if (this.status == 204) {
                            var upd_record = grid.getStore().getAt(rowIndex);
                            upd_record.set('ModifiedOn', new Date());
                            upd_record.commit();
                            grid.getStore().commitChanges();
                            grid.getView().refresh();
                            //window.setTimeout(function () {b.proj_calcMeasSums();b.programNames();}, 10);

                            //var sm = grid.getSelectionModel();
                            //sm.select(rowIndex);
                        }
                    }
                };
                lookupActive = "";
                req.send(JSON.stringify(changes));
                console.log('Update Project after save Grid');
                setTimeout(function(){ b.Xrm.Page.data.refresh(); }, 2000);
            }

            function Delete_Record(records, grid) {
                for (var i = 0; i < records.length; i++) {
                    var guidId = records[i].data[entitySchemaName + 'Id'];
                    if (guidId != "") {
                        var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";
                        //console.log(URI);
                        var req = new XMLHttpRequest();
                        req.open("POST", URI, false);
                        req.setRequestHeader("Accept", "application/json");
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader("X-HTTP-Method", "DELETE");
                        req.onreadystatechange = function () {
                            if (this.readyState == 4) {
                                req.onreadystatechange = null;
                                //console.log(this.status);
                                if ((this.status == 204) || (this.status == 1223)) {
                                    //window.setTimeout(function () {b.proj_calcMeasSums();b.programNames();}, 10);
                                    //console.log("DELETE: " + guidId);
                                }
                            }
                        };
                        lookupActive = "";
                        req.send(null);
                    }
                }
                grid.getStore().commitChanges();
                Ext.getCmp(idGrid).getView().refresh();

            }

            var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
                id: 'group',
                //ftype: 'groupingsummary',
                ftype: 'grouping',
                startCollapsed: true,
                //groupHeaderTpl: ['{columnName}: {name:this.rendererName} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})', { rendererName: function (name) { return name; } }]
                groupHeaderTpl: ['{columnName}: {name:this.rendererName} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})', {
                    rendererName: function (name) {
                        return name;
                    }
                }]
            });

            var grid = Ext.create('Ext.grid.Panel', {
                id: idGrid,
                border: borderGrid,
                loadMask: true,
                restful: true,
                autoSizeGrid: true,
                //columnLines: true,
                /*
                 features: [_createFilters(), {
                 id: 'group',
                 ftype: 'grouping',
                 startCollapsed: true,
                 groupHeaderTpl: ['{columnName}: {name:this.rendererName} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})', { rendererName: function (name) { return name; } }]
                 }],
                 */
                features: [_createFilters()],
                store: _createStore([], _createFields()),
                columns: _createColumns(),
                selType: 'checkboxmodel',
                renderTo: "divGrid",
                autoExpandColumn: 'lastUpdated',
                autoScroll: true,
                autoWidth: true,
                //forceFit: true,
                height: heightGrid,
                title: titleGrid,
                frame: false,
                plugins: [rowEditing, {
                    ptype: "ux-multisearch",
                    searchPanelAlign: "right",
                    searchPanelPosition: "last"
                }],
                viewConfig: viewConfig,
                //enableKeyEvents: true,
                scroll: "both",
                tbar: [{
                    itemId: 'add',
                    text: 'Add Prescriptive',
                    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAByElEQVQ4T5WTTShEURiG33OVMcntLgwj89O946dQFEqNhLKwkVhYKKWUpUkpi1lYTFFTGkulZGEhiWwoC0U0hYUyMphr/OS/CCPGzBzde+cOxl3MfKtzvvN+z/nOe84h0AjPFkuT0w77C9HSaiYlQH/NDS4wBQv6MLlbgLQBpXwz3kJ34FgzfKer6QOKLPUIvT+CzSmA/2w9PcD4Jkd5UxU+w6/I1htwEvRisOFZ2wP3eq5iGAEIBVT3rIU2fEU/odexCJwfJvyTKKpmqOmRkLE1Ix1oCMiGqSEZt3hmRzQaAcNkoEPY/rc+sWHDcMstIa4VExUsZoTpk8xWupDa+eno967SOJPJQ+BChLP1ipCRZYEKfA4isTe5SOlPrVZAoFIyPgaFjsnFifiAkTaREOdCScIDtZgQCqsQlqcEFEFRpwBk5c8bc3UeE01nh+fKKV/8rFhCgeAph9Eu3x+ta6nC7Gw/uNQEDM1WUr70Tt5dCtFvhLt7P/WnPDhTTW1l5/LRparAkRXjPXupAxzTtbSuLitxrV7vBzy9OykDDI7p2vvkn+fp3eEaGxF6MIDxzSMCIBa/rz/SfAAcgGwAbBLkOD6//p3/BrMkphEjG7gXAAAAAElFTkSuQmCC',
                    handler: function () {
                        addMeasureType = 1;
                        SmartMeasureButton = false;
                        rowEditing.cancelEdit();
                        newMeasureGridRow();
                    }

                }, '-',
                    {
                        itemId: 'add1',
                        text: 'Add Custom',
                        icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAByElEQVQ4T5WTTShEURiG33OVMcntLgwj89O946dQFEqNhLKwkVhYKKWUpUkpi1lYTFFTGkulZGEhiWwoC0U0hYUyMphr/OS/CCPGzBzde+cOxl3MfKtzvvN+z/nOe84h0AjPFkuT0w77C9HSaiYlQH/NDS4wBQv6MLlbgLQBpXwz3kJ34FgzfKer6QOKLPUIvT+CzSmA/2w9PcD4Jkd5UxU+w6/I1htwEvRisOFZ2wP3eq5iGAEIBVT3rIU2fEU/odexCJwfJvyTKKpmqOmRkLE1Ix1oCMiGqSEZt3hmRzQaAcNkoEPY/rc+sWHDcMstIa4VExUsZoTpk8xWupDa+eno967SOJPJQ+BChLP1ipCRZYEKfA4isTe5SOlPrVZAoFIyPgaFjsnFifiAkTaREOdCScIDtZgQCqsQlqcEFEFRpwBk5c8bc3UeE01nh+fKKV/8rFhCgeAph9Eu3x+ta6nC7Gw/uNQEDM1WUr70Tt5dCtFvhLt7P/WnPDhTTW1l5/LRparAkRXjPXupAxzTtbSuLitxrV7vBzy9OykDDI7p2vvkn+fp3eEaGxF6MIDxzSMCIBa/rz/SfAAcgGwAbBLkOD6//p3/BrMkphEjG7gXAAAAAElFTkSuQmCC',
                        handler: function () {
                            addMeasureType = 2;
                            SmartMeasureButton = false;
                            rowEditing.cancelEdit();
                            newMeasureGridRow();
                        }

                    }, '-',
                    {
                        itemId: 'addSmartMeasure',
                        text: 'Add Quasi-Prescriptive',
                        icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAByElEQVQ4T5WTTShEURiG33OVMcntLgwj89O946dQFEqNhLKwkVhYKKWUpUkpi1lYTFFTGkulZGEhiWwoC0U0hYUyMphr/OS/CCPGzBzde+cOxl3MfKtzvvN+z/nOe84h0AjPFkuT0w77C9HSaiYlQH/NDS4wBQv6MLlbgLQBpXwz3kJ34FgzfKer6QOKLPUIvT+CzSmA/2w9PcD4Jkd5UxU+w6/I1htwEvRisOFZ2wP3eq5iGAEIBVT3rIU2fEU/odexCJwfJvyTKKpmqOmRkLE1Ix1oCMiGqSEZt3hmRzQaAcNkoEPY/rc+sWHDcMstIa4VExUsZoTpk8xWupDa+eno967SOJPJQ+BChLP1ipCRZYEKfA4isTe5SOlPrVZAoFIyPgaFjsnFifiAkTaREOdCScIDtZgQCqsQlqcEFEFRpwBk5c8bc3UeE01nh+fKKV/8rFhCgeAph9Eu3x+ta6nC7Gw/uNQEDM1WUr70Tt5dCtFvhLt7P/WnPDhTTW1l5/LRparAkRXjPXupAxzTtbSuLitxrV7vBzy9OykDDI7p2vvkn+fp3eEaGxF6MIDxzSMCIBa/rz/SfAAcgGwAbBLkOD6//p3/BrMkphEjG7gXAAAAAElFTkSuQmCC',
                        handler: function () {
                            addMeasureType = 0;
                            SmartMeasureButton = true;
                            rowEditing.cancelEdit();
                            newSmartMeasureGridRow();
                        }

                    }, '-', {
                        itemId: 'remove',
                        text: 'Remove',
                        icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAADZklEQVQ4T22Tb2xTVRiHf2f29s+5ucfbrQwCNFNoiW3FMdbpyDSSzcQxyDSRENliBuLCUOKGSSNxX4yLhH7gkzEuVT4sG5YFDEZBE0JMNqLrtIMWOkqQjZFqSAndtN3t3W3vvTX3RoxB32/n5H2e88t7ziEA8I4o1vlqasaqK5WGlCS9dSybPW3sP1ofM7bTX1sbVTRt7u6dO13vA2liwE86nWM9zc3PuxjDNxcuyFOlUu/x+/dP/VswROmuBpfr1I6+PiZpGr6LRGYWMpke8kkgMN1VX/9sdX8/4PFAHRzEpfFxeQo48NHSUtSQDDHW0chx0dZ9+5jt6FGgUMDy2bO4NDJyg0SDweLr27Y5cPAgwPPA8jJWwmH8cOaMFOf5HmhafouqftW2Z4/AHzkC2GyApgHXr2NieFglH6xe3dWydm2kvamJr9q9G2AMKJchhcOYOH9eLRGitXZ02FgoBHAcoCiozM4ifu6c8svly+8RI2LI6dz7cm3tye3BoOOxzk6AUuiqiqUTJ6ArCmpCIVTZ7Sasz87i6sWL6o+x2EC/rn9qCowadDq7X2Dsi9ZAwM61twOCAL1YBCoVVFGKiixDS6UQn5xUf06nTdjg/hEYiw9F8dUmm238Rb/fyre1mUkMQUVRICcSuDI1pV3JZPb3A6MPD35UsD0oit83Li7aybp1qFqzxuzTJAnI53GrVCon5+dfexf49j+CIUHYtdXt/rIllxP+yGYhAZD/7rLzPHiXC4+73UgXCvK1ZLL3EGC+EzPBEKUdz/n90ZZslkmZjAmvALAQAsJxKAsCbIzBIQgQ6uqQXlyUE7HYgV5Ni5JjgtC5xe8feymXE+Tbt7EMoGTAHIe7FoteJkT38LxF5XlwggDKGHiPB9cyGenmxMTb5Ovm5vwrsiwoySQKAFQDphS/OhzKVUnqq5TLf9aL4uimVat4zWqFxZBQCr6hAdMzMyvk9ObNqZ12e8CWSCBfLoMTRdyiVJ3O5QYOK4p5VcPA3q3r15/0ut0OjRBQpxPE60VscnKBHLdaA09s3Diyw2JppA8eYIZSNX7v3sDhYtGEH1YE6K73ej9/xudz6NXV+Ckev/lbKrXfHGIY8D21YcOYyHFP31hYGDikKJ/933ceBbo3+XwRuVT6fX5u7o03gem/AGqKWxhL3xdrAAAAAElFTkSuQmCC',
                        handler: function () {
                            var sm = Ext.getCmp(idGrid).getSelectionModel();
                            rowEditing.cancelEdit();
                            Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete select records?', function (btn, text) {
                                if (btn == 'yes') {
                                    Delete_Record(sm.getSelection(), Ext.getCmp(idGrid));
                                    Ext.getCmp(idGrid).getStore().remove(sm.getSelection());
                                }
                            });
                        },
                        disabled: true
                    }, '-', {
                        itemId: 'reload',
                        icon: 'accentgold_/EditableGrid/icons/grid/reload.png',
                        text: 'Reload',
                        handler: function () {
                            loadData(_getODataEndpoint(entitySchemaName), true);
                        }
                    }, '-', {
                        itemId: 'excel',
                        text: 'Export to Excel',
                        icon: 'accentgold_/EditableGrid/icons/grid/excel.png',
                        handler: function (b, e) {
                            exportExcelXml();
                        }
                    }, '-', {
                        itemId: 'fnlc_create',
                        text: 'Manual Financial Creation',
                        icon: 'accentgold_/EditableGrid/icons/grid/application_go.png',
                        hidden: !EnableBtnFincl,
                        handler: function () {
                            var sm = Ext.getCmp(idGrid).getSelectionModel();
                            rowEditing.cancelEdit();
                            var message = "", progFlag = false, taFlag = false;
                            var records = sm.getSelection();
                            if (records.length > 1) {
                                var InitMSIndex = b.document.getElementById('WebResource_milestone_grid').contentWindow.getActiveIndex();
                                if (InitMSIndex == -1) {
                                    InitMSIndex = 0;
                                }
                                for (var i = 0; i < records.length; i++) {
                                    //if(records[i].get("ddsm_ProgramId") != records[(i+1 < records.length)?i+1:i].get("ddsm_ProgramId")){progFlag = true;}
                                    if (records[i].get("ddsm_TradeAllyId") != records[(i + 1 < records.length) ? i + 1 : i].get("ddsm_TradeAllyId")) {
                                        taFlag = true;
                                    }
                                }
                                if (progFlag) {
                                    message += "The measures you have selected include more than one Program.\n"
                                }
                                if (taFlag) {
                                    message += "The measures you have selected include more than one Trade Ally.\n"
                                }
                                message += "Do you still wish to make a financial from these measures?";
                                if (progFlag || taFlag) {
                                    Ext.Msg.confirm('Confirmation', message, function (btn, text) {
                                        if (btn == 'yes') {
                                            createMeasureFncl(records, InitMSIndex);
                                        }
                                    });
                                }
                            } else {
                                createMeasureFncl(records, InitMSIndex);
                            }

                        },
                        disabled: true
                    }, {
                        itemId: 'split',
                        text: 'Split Method',
                        icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAByElEQVQ4T5WTTShEURiG33OVMcntLgwj89O946dQFEqNhLKwkVhYKKWUpUkpi1lYTFFTGkulZGEhiWwoC0U0hYUyMphr/OS/CCPGzBzde+cOxl3MfKtzvvN+z/nOe84h0AjPFkuT0w77C9HSaiYlQH/NDS4wBQv6MLlbgLQBpXwz3kJ34FgzfKer6QOKLPUIvT+CzSmA/2w9PcD4Jkd5UxU+w6/I1htwEvRisOFZ2wP3eq5iGAEIBVT3rIU2fEU/odexCJwfJvyTKKpmqOmRkLE1Ix1oCMiGqSEZt3hmRzQaAcNkoEPY/rc+sWHDcMstIa4VExUsZoTpk8xWupDa+eno967SOJPJQ+BChLP1ipCRZYEKfA4isTe5SOlPrVZAoFIyPgaFjsnFifiAkTaREOdCScIDtZgQCqsQlqcEFEFRpwBk5c8bc3UeE01nh+fKKV/8rFhCgeAph9Eu3x+ta6nC7Gw/uNQEDM1WUr70Tt5dCtFvhLt7P/WnPDhTTW1l5/LRparAkRXjPXupAxzTtbSuLitxrV7vBzy9OykDDI7p2vvkn+fp3eEaGxF6MIDxzSMCIBa/rz/SfAAcgGwAbBLkOD6//p3/BrMkphEjG7gXAAAAAElFTkSuQmCC',
                        handler: function () {
                            rowEditing.cancelEdit();
                            var sm = Ext.getCmp(idGrid).getSelectionModel(),
                                maxRengeUnits = 0, tmpArr = [], records = sm.getSelection();
                            for (var x = 0; x < records.length; x++) {
                                tmpArr.push(records[x].data.ddsm_CurrentUnits);
                            }
                            maxRengeUnits = Math.min.apply(null, tmpArr);
                            var splitList = Ext.create('Ext.data.Store', {
                                fields: ['name', 'value'],
                                data: [
                                    {"name": "By Units", "value": 0},
                                    {"name": "By Percentage", "value": 1},
                                    {"name": "Duplicate", "value": 2}
                                ]
                            });
                            var SplitMethodForm = new Ext.form.Panel({
                                width: 300,
                                title: 'Measure Split Form',
                                floating: true,
                                closable: true,
                                border: false,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%'
                                },
                                bodyPadding: 5,
                                header: true,
                                items: [
                                    {
                                        xtype: 'combobox',
                                        store: splitList,
                                        name: 'SplitMethod',
                                        fieldLabel: 'Split Method',
                                        displayField: 'name',
                                        valueField: 'value'
                                    }
                                ],
                                buttons: [{
                                    text: 'Split',
                                    handler: function () {
                                        var formValues = this.up('form').getForm().getValues();
                                        switch (formValues.SplitMethod) {
                                            case 0:
                                                SplitMethodForm.close();
                                                sliderUnitForm.show();
                                                break;
                                            case 1:
                                                SplitMethodForm.close();
                                                sliderPercentForm.show();
                                                break;
                                            case 2:
                                                splitMeas(0, 2);
                                                SplitMethodForm.close();
                                                break;
                                            default:
                                                Ext.Msg.alert("Error", "Choose the split method");
                                                break;

                                        }
                                    }
                                }, {
                                    text: 'Cancel',
                                    handler: function () {
                                        SplitMethodForm.close();
                                    }
                                }]
                            });
                            var sliderUnitForm = new Ext.form.Panel({
                                width: 300,
                                title: 'Measure Split Form',
                                floating: true,
                                closable: true,
                                border: false,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%'
                                },
                                bodyPadding: 5,
                                header: true,
                                items: [
                                    {
                                        xtype: 'slider',
                                        name: 'sliderUnit',
                                        width: 200,
                                        value: 50,
                                        increment: 1,
                                        minValue: 0,
                                        maxValue: maxRengeUnits - 1
                                    }
                                ],
                                buttons: [{
                                    text: 'OK',
                                    handler: function () {
                                        var formValues = this.up('form').getForm().getValues();
                                        splitMeas(formValues.sliderUnit, 0);
                                        sliderUnitForm.close();
                                    }
                                }, {
                                    text: 'Cancel',
                                    handler: function () {
                                        sliderUnitForm.close();
                                    }
                                }]
                            });
                            var sliderPercentForm = new Ext.form.Panel({
                                width: 300,
                                title: 'Measure Split Form',
                                floating: true,
                                closable: true,
                                border: false,
                                layout: 'anchor',
                                defaults: {
                                    anchor: '100%'
                                },
                                bodyPadding: 5,
                                header: true,
                                items: [
                                    {
                                        xtype: 'slider',
                                        name: 'sliderPercent',
                                        width: 200,
                                        value: 50,
                                        increment: 1,
                                        minValue: 0,
                                        maxValue: 100
                                    }
                                ],
                                buttons: [{
                                    text: 'OK',
                                    handler: function () {
                                        var formValues = this.up('form').getForm().getValues();
                                        splitMeas(formValues.sliderPercent, 1);
                                        sliderPercentForm.close();
                                    }
                                }, {
                                    text: 'Cancel',
                                    handler: function () {
                                        sliderPercentForm.close();
                                    }
                                }]
                            });
                            SplitMethodForm.show();
                        },
                        disabled: true
                    }],
                listeners: {
                    selectionchange: function (view, records) {
                        Ext.getCmp(idGrid).down('#remove').setDisabled(!records.length);
                        if (b.Xrm.Page.getAttribute("ddsm_phase").getValue() == "2. Project") {
                            Ext.getCmp(idGrid).down('#fnlc_create').setDisabled(!records.length);
                        }
                        Ext.getCmp(idGrid).down('#split').setDisabled(!records.length);
                    },
                    edit: function (editor, e) {
                        calcCostBreakdown();

                        measCalc_Offered(e.record.get('ddsm_Units'), e.record, 'listeners edit');

                        window.setTimeout(function () {
                            //create Document Name Convention List to project
                            if (e.record.get('ddsm_measureId') == "" || e.record.get('ddsm_measureId') == '00000000-0000-0000-0000-000000000000') {
                                createDocList_measTpl(e.record.get('ddsm_MeasureSelector__Id'));
                            }
                            calcCostBreakdownAll();
                            Update_Create_Record(e.grid, e.record, e.rowIdx);
                        }, 100);
                        if (altEnter) {
                            if (e.rowIdx + 1 < Ext.getCmp(idGrid).getStore().getCount()) {
                                rowEditing.startEdit(e.rowIdx + 1, 9);
                            } else {
                                rowEditing.cancelEdit();
                                newMeasureGridRow();
                            }
                            altEnter = false;
                        }
                        if (ctrlEnter) {
                            rowEditing.cancelEdit();
                            newMeasureGridRow();
                            ctrlEnter = false;
                        }
                    },
                    beforeedit: function (editor, e) {
                        lookupActive = "";

                        if (String(e.record.get("ddsm_CalculationType")).toLowerCase() == "esp") {
                            Ext.Msg.alert('Denied', "Smart measure is not allowed for edit from the grid");
                            ////TODO: test edit form and uncomment
                            //initEditSMForm(e.record);
                            return false;
                        }
                        if (!!e.record.get("ddsm_DisableRecalculation")) {
                            Ext.Msg.alert('Denied', "Manual Overrided measure is not allowed for edit");
                            return false;
                        }
                        //disableEditorCell(editor, true);
                        if (e.record.get("ddsm_MeasureSelector") != "") {
                            editor.getEditor().child('[name="ddsm_MeasureSelector"]').disable();
                        } else {
                            editor.getEditor().child('[name="ddsm_MeasureSelector"]').enable();
                        }
                        if (b.Xrm.Page.getAttribute("ddsm_phase").getValue() == "1. Opportunity" || b.Xrm.Page.getAttribute("ddsm_phase").getValue() == "1. Offered") {
                            //editor.getEditor().child('[name="ddsm_Units"]').enable();
                            //disablePhase1EditorCell(editor, false);
                        }
                        window.setTimeout(function () {

                            $("#" + idGrid).find("input").keydown(function (event) {
                                if (event.ctrlKey && event.keyCode == 13) {
                                    //log("Hey! Ctrl+ENTER event captured!");
                                    ctrlEnter = true;
                                    event.preventDefault();
                                }
                                if (event.altKey && event.keyCode == 13) {
                                    //console.log("Hey! Alt+ENTER event captured!");
                                    altEnter = true;
                                    event.preventDefault();
                                }
                            });

                            //$("#button-1028").focus(function(e) {console.log("updateBtn focus");e.stopPropagation();$("#button-1028").trigger(jQuery.Event('keypress', {which: 13}));});
                        }, 100);
                        /*
                         window.setTimeout(function () {
                         onLoadMeas(e.record);
                         }, 100);
                         */
                    },
                    canceledit: function (editor, e) {
                        lookupActive = "";
                        if (e.record.data[entitySchemaName + 'Id'] == "") {
                            e.grid.getStore().removeAt(e.rowIdx);
                        }
                        Ext.getCmp(idGrid).getView().refresh();
                    },
                    columnmove: function (container, coulmn, from, to) {
                        //console.log('Column Moved From ' + from+ ' To ' +to);
                    },
                    afterrender: function () {
                        spinnerForm.stop();
                        loadData(_getODataEndpoint(entitySchemaName), true);
                        //Ext.getCmp(idGrid).getStore().group('ddsm_ProjectPhaseName');
                    }
                }
            });

            new Ext.util.KeyMap(Ext.getBody(),
                [{
                    key: Ext.EventObject.E,
                    ctrl: true,
                    scope: this,
                    defaultEventAction: 'preventDefault',
                    fn: function (e) {
                        //console.log("Hey! Ctrl+E event captured!");
                        var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                        if (typeof record !== 'undefined') {
                            rowEditing.startEdit(record.index, 9);
                        } else {
                            Ext.Msg.alert("Error with editing", "No Measure record has been selected!");
                        }
                    }
                }, {
                    key: 45,
                    ctrl: true,
                    scope: this,
                    defaultEventAction: 'preventDefault',
                    fn: function (e) {
                        //console.log("Hey! Ctrl+Insert event captured!");
                        rowEditing.cancelEdit();
                        newMeasureGridRow();
                    }
                }, {
                    key: 46,
                    ctrl: true,
                    scope: this,
                    defaultEventAction: 'preventDefault',
                    fn: function (e) {
                        //console.log("Hey! Ctrl+Delete event captured!");
                        rowEditing.cancelEdit();
                        var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                        if (typeof record !== 'undefined') {
                            var sm = Ext.getCmp(idGrid).getSelectionModel();
                            rowEditing.cancelEdit();
                            Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete select records?', function (btn, text) {
                                if (btn == 'yes') {
                                    Delete_Record(sm.getSelection(), Ext.getCmp(idGrid));
                                    Ext.getCmp(idGrid).getStore().remove(sm.getSelection());
                                }
                            });
                        } else {
                            Ext.Msg.alert("Error with deleting", "No Measure record has been selected!");
                        }
                    }
                }]);
            var getValueMAD_Combo = function (combo, value) {
                var index = combo.findExact('value', value);
                if (index != -1) {
                    var rs = combo.getAt(index).data;
                    return rs.display;
                }
            };

            //new Smart Measure Grid Row
            function newSmartMeasureGridRow() {
//ESPMeasureLibraryId
                var defaultsmTemplateStore = new Ext.data.SimpleStore({
                    fields: ["value", "display", "MADFields", "ddsm_ESPSmartMeasureID", "ddsm_ESPMeasureLibraryId"],
                    data: []
                });

                var smLibraryCombo = Ext.create('Ext.form.field.ComboBox', {
                    id: "Combo_smLib",
                    fieldLabel: 'Smart&nbsp;Measure&nbsp;Library',
                    queryMode: 'local',
                    displayField: 'display',
                    valueField: 'value',
                    store: smLibraryStore,
                    maxWidth: 200,
                    listeners: {
                        select: function (combo, value) {
                            //console.log(combo.getValue());
                            ESPMeasureLibraryId = combo.getValue();
                            if (typeof smLibraryTemplatesData[ESPMeasureLibraryId] == 'undefined' && ESPMeasureLibraryId != 'undefined') {

                                //AGS.REST.retrieveMultipleRecords("ddsm_measuretemplate", "$select=ddsm_name,ddsm_MADFields,ddsm_MSDFields,ddsm_measuretemplateId,ddsm_ESPSmartMeasureID,ddsm_ESPMeasureLibraryId,ddsm_CalculationTypeId&$filter=ddsm_ESPMeasureLibraryId/Id eq guid'" + ESPMeasureLibraryId + "'", null, null, function (smTemplates) {
                                AGS.REST.retrieveMultipleRecords("ddsm_measuretemplate", "$select=ddsm_name,ddsm_MSDFields,ddsm_MADFields,ddsm_measuretemplateId,ddsm_ESPSmartMeasureID,ddsm_ESPMeasureLibraryId,ddsm_CalculationTypeId&$filter=ddsm_ESPMeasureLibraryId/Id eq guid'" + ESPMeasureLibraryId + "'", null, null, function (smTemplates) {
                                    //console.dir(smTemplates);
                                    var smTemplatesData = [];
                                    for (var j = 0; j < smTemplates.length; j++) {
                                        var obj = {
                                            value: smTemplates[j].ddsm_measuretemplateId,
                                            display: smTemplates[j].ddsm_name,
                                            //ddsm_MADFields: smTemplates[j].ddsm_MADFields,
                                            ddsm_MADFields: smTemplates[j].ddsm_MADFields,
                                            ddsm_MSDFields: smTemplates[j].ddsm_MSDFields,
                                            ddsm_ESPSmartMeasureID: smTemplates[j].ddsm_ESPSmartMeasureID,
                                            ddsm_ESPMeasureLibraryId: smTemplates[j].ddsm_ESPMeasureLibraryId,
                                            ddsm_CalculationType: smTemplates[j].ddsm_CalculationTypeId.Name
                                        };
                                        smTemplatesData.push(obj);
                                    }
                                    smLibraryTemplatesData[ESPMeasureLibraryId] = smTemplatesData;
                                }, false, []);
                            }
                            if (ESPMeasureLibraryId == 'undefined') {
                                smTemplateCombo.getStore().loadData([]);
                                smTemplateCombo.reset();
                            } else {
                                smTemplateCombo.getStore().loadData(smLibraryTemplatesData[ESPMeasureLibraryId]);
                                smTemplateCombo.reset();
                            }
                        },
                        beforeRender: function () {
                            //set Default SM Library
                            if (!!adminData.smLibDefault && (this.store.findExact('value', adminData.smLibDefault) >= 0)) {
                                this.setValue(this.store.getAt(this.store.findExact('value', adminData.smLibDefault)));
                                this.fireEvent('select', this);
                            }
                        }
                    }
                });

                var smTemplateCombo = Ext.create('Ext.form.field.ComboBox', {
                    id: "Combo_smTpl",
                    fieldLabel: 'Smart&nbsp;Measure',
                    queryMode: 'local',
                    displayField: 'display',
                    valueField: 'value',
                    store: defaultsmTemplateStore,
                    maxWidth: 200,
                    listeners: {
                        select: function (combo, value) {
                            //console.log(combo.getValue());
                        }
                    }
                });

                var smLibraryForm = new Ext.form.Panel({
                    width: 475,
                    //height: 265,
                    //title: '',
                    floating: true,
                    closable: true,
                    border: false,
                    //layout: 'anchor',
                    //defaults: {anchor: '100%'},
                    bodyPadding: 5,
                    header: false,
                    cls: 'form2css1',
                    items: [{
                        layout: 'form'
                        , defaults: {
                            columnWidth: 0.95
                            , layout: 'form'
                            , border: false
                            , padding: 5
                        }
                        , items: [smLibraryCombo, smTemplateCombo]

                    }]
                    ,
                    buttons: [{
                        text: 'Ok',
                        handler: function () {
                            var formValues = this.up('form').getForm().getValues();
                            //console.dir(formValues);
                            if (!formValues['Combo_smLib-inputEl']) {
                                Ext.Msg.alert('Form population', 'There is no Smart Measure Library selected!');
                                return;
                            }
                            if (!formValues['Combo_smTpl-inputEl']) {
                                Ext.Msg.alert('Form population', 'There is no Smart Measure selected!');
                                return;
                            }

                            let measTplLst = smLibraryTemplatesData[formValues['Combo_smLib-inputEl']];
                            let curMeasTpl ;
                            for (let i = 0; i < measTplLst.length; i++) {
                                let el = measTplLst[i];

                                if (el.value == formValues['Combo_smTpl-inputEl']) {
                                    curMeasTpl = el;
                                    break;
                                }
                            }
                            //try{
                            smCurrentVersionData.MAD = curMeasTpl.ddsm_MADFields;
                            smCurrentVersionData.CalculationID = curMeasTpl.ddsm_ESPSmartMeasureID;
                            smCurrentVersionData.MeasTplID = formValues['Combo_smTpl-inputEl'];
                            smCurrentVersionData.EditSM = false;
                            createSMForm(formValues['Combo_smLib-inputEl'], formValues['Combo_smTpl-inputEl']);
                            smLibraryForm.close();

                            //}catch(ex) {
                            //   Ext.Msg.alert('Error', 'Some error ocure on form render ' ) ;
                            //    console.error(ex);
                            //}

                        }
                    }, {
                        text: 'Cancel',
                        handler: function () {
                            SmartMeasureButton = false;
                            smLibraryForm.close();
                        }
                    }],
                    dockedItems: [{
                        xtype: 'toolbar',
                        dock: 'top',
                        border: false,
                        items: [
                            //{ xtype: 'button', text: '<<',
                            //    handler: function() {
                            //        SmartMeasureButton = false;
                            //        smLibraryForm.close();
                            //    }
                            //}, {
                            {
                                xtype: 'label',
                                text: 'Smart Measure Library',
                                width: '100%',
                                cls: 'formLabel',
                                margin: '0 auto 0 auto'
                                //},
                                //{
                                //    xtype: 'button', text: 'Reset',
                                //    handler: function () {
                                //        this.up('form').getForm().reset();
                                //    }
                            }
                        ]
                    }]
                });

                smLibraryForm.show();

                /*
                 AGS.REST.retrieveMultipleRecords("ddsm_measuretemplate", "$select=ddsm_name,ddsm_MADFields,ddsm_measuretemplateId,ddsm_ESPSmartMeasureID,ddsm_ESPMeasureLibraryId&$filter=ddsm_ESPMeasureLibraryId/Id eq guid'" + ESPMeasureLibraryId + "'", null, null, function(smTemplates){
                 console.dir(smTemplates);

                 if(smTemplates.length > 0) {
                 smLibraryTemplateStores[smTemplates[0].ddsm_ESPMeasureLibraryId] = new Ext.data.SimpleStore({
                 fields: ["value", "display", "MADFields", "ddsm_ESPSmartMeasureID", "ddsm_ESPMeasureLibraryId"],
                 data: []
                 });
                 smLibraryTemplateStores[smTemplates[0].ddsm_ESPMeasureLibraryId].add({value: "undefined", display: "Select Measure Template"});
                 for (var j = 0; j < smTemplates.length; j++) {
                 smLibraryTemplateStores[smTemplates[0].ddsm_ESPMeasureLibraryId].add({value:smTemplates[j].ddsm_measuretemplateId, display:smTemplates[j].ddsm_name, ddsm_MADFields:smTemplates[j].ddsm_MADFields, ddsm_ESPSmartMeasureID:smTemplates[j].ddsm_ESPSmartMeasureID, ddsm_ESPMeasureLibraryId:smTemplates[j].ddsm_ESPMeasureLibraryId})
                 }

                 }
                 }, false, []);
                 */
            }

            //TODO:
            function initEditSMForm(record) {
                if (!record.get('ddsm_MeasureSelector__Id')) {
                    Ext.Msg.alert('Wrong record', "Measure selector of selected Smart Measure shouldn't be empty");
                    return;
                }
                //var formValues = this.up('form').getForm().getValues();

                //TODO: use limits by date

                smCurrentVersionData.expand = 'ddsm_ddsm_measuretemplate_ddsm_smartmeasurever';
                smCurrentVersionData.MeasureData = record.data;

                //RELATED FIELDS:
                //ddsm_name,ddsm_MADFields,ddsm_MSDFields,ddsm_measuretemplateId,ddsm_ESPSmartMeasureID,ddsm_ESPMeasureLibraryId,ddsm_CalculationTypeId

                //try{
                var sel = "$select=ddsm_MADFields,ddsm_CalculationID,ddsm_StartDateTime"
                    + AGS.String.format(",{0}/ddsm_MSDFields", smCurrentVersionData.expand)
                    + "&$top=1"
                    + "&$orderby=ddsm_StartDateTime desc"
                    + AGS.String.format("&$expand={0}", smCurrentVersionData.expand)
                    + AGS.String.format("&$filter=ddsm_MeasureTemplateId/Id eq guid'{0}'", record.get('ddsm_MeasureSelector__Id'));
                AGS.REST.retrieveMultipleRecords("ddsm_smartmeasureversion", sel, function (res) {
                        //Success
                        if (!res[0]) {
                            Ext.Msg.alert('Data Error', "There is no actual MAD data for current Measure Template! \nPlease load MAD data and retry to edit a Smart Measure");
                            return;
                        }
                        smCurrentVersionData.MAD = res[0].ddsm_MADFields;
                        smCurrentVersionData.CalculationID = res[0].ddsm_CalculationID;
                        smCurrentVersionData.MeasTplID = record.get('ddsm_MeasureSelector__Id');
                        smCurrentVersionData.MeasID = record.get('ddsm_measureId');
                        smCurrentVersionData.MeasName = record.get('ddsm_name');
                        smCurrentVersionData.MeasTplData = res[0][smCurrentVersionData.expand];
                        smCurrentVersionData.EditSM = true;

                        createSMForm();

                        ////smLibraryForm.close();
                    }, function () {
                        //Error
                        Ext.Msg.alert('Data Error', "There is no actual MAD data for current Measure Template! \nPlease load MAD data and retry to edit a Smart Measure");
                    },
                    null, false, []);
            }

            function createSMForm(smLibId, smTplId) {

                if (!objMappingSM) {
                    Ext.Msg.alert('Data Error', "There is no actual mapping for current MAD! \nPlease populate the mapping and try again.");
                    return;
                }

                if (smCurrentVersionData.EditSM) {
                    updateMADFieldsDataEdit();
                    return;
                }
                console.log("SM Lib ID: " + smLibId + " | SM Tpl ID: " + smTplId);
                let smLibraryTplData = smLibraryTemplatesData[smLibId], smTpl = {};

                for (let i = 0; i < smLibraryTplData.length; i++) {
                    if (smLibraryTplData[i].value == smTplId) {
                        smTpl = smLibraryTplData[i];
                        break;
                    }
                }

                ////Check for empty MAD
                //if (!smTpl.ddsm_MADFields) {
                //    Ext.Msg.alert('Form population',"There is no MAD data for current Measure Template! \nPlease load MAD data and retry to create a Smart Measure");
                //    return;
                //}

                //var MADFields = JSON.parse(smTpl.ddsm_MADFields);
                MADFields = JSON.parse(smCurrentVersionData.MAD);
                var MSDFields = JSON.parse(smTpl.ddsm_MSDFields);

                //updateMADFieldsData(smTpl,MADFields,MSDFields);
                updateMADFieldsData(smTpl, MSDFields);
            }

            //function generateSMForm(smTpl, MADFields){
            function generateSMForm(smTpl) {
                var arrayFields = [], countFieldsColumn = 5, itemsColumns = [];

                allowCreation = true;

                arrayFields = getSMfields(MADFields);

                var j = 1, tmpArr = [],
                    itemsCol = {
                        //defaults: {anchor:'100%'},
                        layout: 'form',
                        items: []
                    };
                for (var i = 0; i < arrayFields.length; i++) {

                    if (!arrayFields[i].allowBlank) {
                        smRequiredFields.push(arrayFields[i].name);
                        arrayFields[i].labelClsExtra = 'x-required';
                    }

                    if (i == countFieldsColumn * j) {
                        itemsCol = {
                            //defaults: {anchor:'100%'},
                            layout: 'form',
                            items: []
                        };
                        itemsCol.items = tmpArr;
                        itemsColumns.push(itemsCol);
                        j++;
                        tmpArr = [];
                        tmpArr.push(arrayFields[i]);
                    } else {
                        tmpArr.push(arrayFields[i]);
                    }
                    if (i == arrayFields.length - 1 && tmpArr.length > 0) {
                        itemsCol = {
                            //defaults: {anchor:'100%'},
                            layout: 'form',
                            items: []
                        };
                        itemsCol.items = tmpArr;
                        itemsColumns.push(itemsCol);
                    }
                }
                //console.dir(arrayFields);
                //console.dir(itemsColumns);

                //Create form sm fields
                var SmartMeasureForm2 = new Ext.form.Panel({
                    id: "smForm1",
                    width: itemsColumns.length * 225,
                    minWidth: 350,
                    //height: 265,
                    maxHeight: 250,
                    //title: '',
                    floating: true,
                    closable: true,
                    border: false,
                    //layout: 'anchor',
                    //defaults: {anchor: '100%'},
                    bodyPadding: 5,
                    header: false,
                    cls: 'form2css1',
                    resizable: true,
                    scroll: 'both',
                    autoScroll: true,
                    items: [{
                        layout: 'column'
                        , defaults: {
                            columnWidth: parseFloat(1 / itemsColumns.length).toFixed(2)
                            , layout: 'form'
                            , border: false
                            //,xtype: 'panel'
                            , padding: 5

                            //,minHeight: 140
                            //,width: '100%'
                        }
                        , items: itemsColumns
                    }],
                    buttons: [{
                        text: 'Ok',
                        //disabled: !allowCreation,
                        handler: function () {
                            spinnerForm = CreaLab.Spinner.spin(document.getElementById("bodyGrid"), "Creating New Smart Measure");
                            var formValues = this.up('form').getForm().getValues();

                            var _smCorrectPicklistValue = function (formKey, formValue) {
                                if (!formValue || !Picklist_stores[formKey]) {
                                    return -1;
                                }


                                let store = Picklist_stores[formKey];
                                return store.findExact('value', formValue);
                            };

                            //validation
                            var hasEmpty = false;
                            var dataFields = {};
                            for (var key in formValues) {

                                var picklist_i = -1;

                                //if(typeof(formValues[key])=='number'){

                                picklist_i = _smCorrectPicklistValue(key, formValues[key]);
                                //}

                                if (smRequiredFields.indexOf(key) >= 0 && !formValues[key]) {
                                    hasEmpty = true;
                                    break;
                                }


                                var key_f = !!(key.match(/^ddsm_/ig)) ? key : key.replace(/_/ig, ' ');

                                if (picklist_i >= 0) {
                                    dataFields[key_f] = picklist_i;
                                } else {
                                    dataFields[key_f] = (!!formValues[key] || formValues[key] == 0) ? String(formValues[key]) : '';
                                }
                            }
                            
                            if (hasEmpty) {
                                Ext.Msg.alert('Form population', "All required fields must be populated!");
                                spinnerForm.stop();
                                return;
                            }

                            //console.dir(fieldsData);
                            sendSMDataFields(
                                (!!b.Xrm.Page.getAttribute("ddsm_accountid").getValue()) ? (b.Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id) : "",
                                (!!b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()) ? (b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id) : "",
                                (!!b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()) ? (b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id) : "",
                                (!!b.Xrm.Page.getAttribute("ddsm_tradeally").getValue()) ? (b.Xrm.Page.getAttribute("ddsm_tradeally").getValue()[0].id) : "",
                                b.Xrm.Page.data.entity.getId(),
                                b.Xrm.Page.getAttribute("ddsm_name").getValue(),
                                b.Xrm.Page.getAttribute("ddsm_projectstatus").getValue(),
                                b.Xrm.Page.getAttribute("ddsm_phase").getValue(),
                                b.Xrm.Page.getAttribute("ddsm_phasenumber").getValue(),
                                new Date(),
                                '{00000000-0000-0000-0000-000000000000}',
                                smTpl.value,
                                smTpl.ddsm_CalculationType,
                                //smTpl.ddsm_ESPSmartMeasureID,
                                smCurrentVersionData.CalculationID,
                                dataFields);
                            SmartMeasureForm2.close();

                        }
                    }, {
                        text: 'Cancel',
                        handler: function () {
                            SmartMeasureForm2.close();
                        }
                    }],
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'top',
                            border: false,
                            items: [
                                //{ xtype: 'button', text: '<<',
                                //    handler: function() {
                                //        SmartMeasureForm2.close();
                                //    }
                                //}, {
                                {
                                    xtype: 'label',
                                    text: smTpl.display,
                                    width: itemsColumns.length * 200,
                                    cls: 'formLabel',
                                    margin: '0 auto 0 auto'
                                },
                                {
                                    xtype: 'button', text: 'Reset',
                                    style: 'float: right;',
                                    handler: function () {
                                        this.up('form').getForm().reset();
                                    }
                                }
                            ]
                        }
                    ]
                });

                SmartMeasureForm2.show();
                if (!allowCreation) {
                    Ext.Msg.alert("Mapping", "ESP Fields are not mapped completely!");

                }
            }

            function getSMfields(fieldsOjb) {

                var res_a = [];
                nestedPairs = {};// global

                for (var i = 0; i < fieldsOjb.length; i++) {

                    //console.log(MADFields[i].DisplayFormat);
                    //var isMapped = objMappingSM.hasOwnProperty(fieldsOjb[i].Name);
                    var isMapped = objMappingSM.hasOwnProperty(fieldsOjb[i].Name.toLowerCase());

                    if (!isMapped && !fieldsOjb[i].IsReadOnly) {
                        allowCreation = false;
                    }

                    var Label_f = fieldsOjb[i].Name;
                    var c_styles = "";

                    //if(!!objMappingSM[MADFields[i].Name]){
                    if (isMapped) {
                        //Label_f = objMappingSM[fieldsOjb[i].Name].DispName || Label_f;
                        Label_f = objMappingSM[fieldsOjb[i].Name.toLowerCase()].DispName || Label_f;
                    } else {
                        c_styles = "background:#ffb3b3";
                    }

                    if (fieldsOjb[i].IsReadOnly) {
                        c_styles = "background:#ccc;";
                    }

                    if (!!((fieldsOjb[i].DisplayFormat).indexOf("Decimal") + 1)) {
                        var precision = 0;
                        if (!!((fieldsOjb[i].DisplayFormat).indexOf("Dollars") + 1)) {
                            precision = parseInt((fieldsOjb[i].DisplayFormat).substring((fieldsOjb[i].DisplayFormat).indexOf("Dollars") + 7, (fieldsOjb[i].DisplayFormat).indexOf("Decimal")));
                            let Decimal = {
                                xtype: 'numberfield',
                                name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                                fieldLabel: Label_f,
                                labelWidth: 120,
                                //allowBlank: false,
                                //allowBlank: fieldsOjb[i].IsReadOnly,
                                allowBlank: fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired),
                                readOnly: fieldsOjb[i].IsReadOnly,
                                //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                                fieldStyle: c_styles,
                                step: 1,
                                decimalPrecision: precision,
                                value: (!!fieldsOjb[i].DoubleValue) ? parseFloat(fieldsOjb[i].DoubleValue) : null,
                                hidden: false
                            };

                            res_a.push(Decimal);
                        } else if (!!((fieldsOjb[i].DisplayFormat).indexOf("Double") + 1)) {
                            precision = parseInt((fieldsOjb[i].DisplayFormat).substring((fieldsOjb[i].DisplayFormat).indexOf("Double") + 6, (fieldsOjb[i].DisplayFormat).indexOf("Decimal")));
                            let Dollars = {
                                xtype: 'numberfield',
                                name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                                fieldLabel: Label_f,
                                labelWidth: 120,
                                //allowBlank: false,
                                //allowBlank: fieldsOjb[i].IsReadOnly,
                                allowBlank: fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired),
                                readOnly: fieldsOjb[i].IsReadOnly,
                                //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                                fieldStyle: c_styles,
                                step: 1,
                                decimalPrecision: precision,
                                value: (!!fieldsOjb[i].DoubleValue) ? parseFloat(fieldsOjb[i].DoubleValue) : null,
                                hidden: false
                            };
                            res_a.push(Dollars);
                        }
                    }
                    if (!!((fieldsOjb[i].DisplayFormat).indexOf("Integer") + 1)) {
                        let Integer = {
                            xtype: 'numberfield',
                            name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                            fieldLabel: Label_f,
                            labelWidth: 120,
                            //allowBlank: false,
                            //allowBlank: fieldsOjb[i].IsReadOnly,
                            allowBlank: fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired),
                            readOnly: fieldsOjb[i].IsReadOnly,
                            //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                            fieldStyle: c_styles,
                            step: 1,
                            decimalPrecision: 0,
                            value: (!!fieldsOjb[i].DoubleValue) ? parseInt(fieldsOjb[i].DoubleValue) : null,
                            hidden: false
                        };
                        res_a.push(Integer);
                    }
                    if (!!((fieldsOjb[i].DisplayFormat).indexOf("TextboxStringValue") + 1)) {
                        let Textbox = {
                            xtype: 'textfield',
                            name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                            fieldLabel: Label_f,
                            labelWidth: 120,
                            //allowBlank: false,
                            //allowBlank: fieldsOjb[i].IsReadOnly,
                            allowBlank: fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired),
                            readOnly: fieldsOjb[i].IsReadOnly,
                            //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                            fieldStyle: c_styles,
                            value: fieldsOjb[i].StringValue,
                            hidden: false
                        };
                        res_a.push(Textbox);
                    }
                    if (!!((fieldsOjb[i].DisplayFormat).indexOf("Lookup") + 1)) {

                        let lookupVal = !!fieldsOjb[i].ComboBoxStringValues ? fieldsOjb[i].ComboBoxStringValues : "";
                        let lookupParts = lookupVal.split('|');
                        let lookupStore = new Ext.data.Store({
                            fields: ["value", "display"],
                            data: []
                        });
                        if(!!lookupParts.length){
                            lookupStore.add({value: lookupVal, display: lookupParts[2] || ''} );
                        }else{
                        }

                        let Lookup = {
                            xtype: 'lookup',
                            name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                            fieldLabel: Label_f,
                            labelWidth: 120,
                            //allowBlank: false,
                            //allowBlank: fieldsOjb[i].IsReadOnly,
                            allowBlank: fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired),
                            readOnly: fieldsOjb[i].IsReadOnly,
                            //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                            fieldStyle: c_styles,
                            value: lookupVal,
                            hidden: false,
                            store: lookupStore
                        };
                        res_a.push(Lookup);
                    }
                    if (!!((fieldsOjb[i].DisplayFormat).indexOf("Boolean") + 1)) {
                        let Checkbox = {
                            //xtype: 'textfield',
                            xtype: 'checkboxfield',
                            name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                            fieldLabel: Label_f,
                            labelWidth: 120,
                            //allowBlank: false,
                            //allowBlank: fieldsOjb[i].IsReadOnly,
                            allowBlank: fieldsOjb[i].IsReadOnly || (!adminData.RequiredAllFields && !fieldsOjb[i].IsRequired),
                            readOnly: fieldsOjb[i].IsReadOnly,
                            //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                            fieldStyle: c_styles,
                            value: fieldsOjb[i].BooleanValue,
                            hidden: false
                            //fieldLabel: 'Toppings',
                            //defaultType: 'checkboxfield',
                            //items: [
                            //    {
                            //        //boxLabel  : 'Anchovies',
                            //        name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                            //        inputValue: '1'
                            //    }
                            //]
                        };
                        res_a.push(Checkbox);
                    }
                    if (!!((fieldsOjb[i].DisplayFormat).indexOf("Combobox") + 1)) {
                        let comboStore;

                        let stKey = AGS.String.format("{0}|{1}", fieldsOjb[i].Entity, fieldsOjb[i].Name);
                        let stKey2 = AGS.String.format("{0}|{1}", fieldsOjb[i].Entity, fieldsOjb[i].AttrName);

                        if (isMapped && (!!smComboStoreSourecesCRM[stKey] || !!smComboStoreSourecesCRM[stKey2])) {
                            comboStore = smComboStoreSourecesCRM[stKey] || smComboStoreSourecesCRM[stKey2];
                        } else {
                            //comboStore = (!!((fieldsOjb[i].DisplayFormat).indexOf("String") + 1))?generateSMComboboxStore(fieldsOjb[i].ComboBoxStringValues):generateSMComboboxStore(fieldsOjb[i].ComboBoxDoubleValues);
                            comboStore = generateSMComboboxStore(fieldsOjb[i]);
                        }

                        Combobox = {
                            xtype: 'combobox',
                            name: (fieldsOjb[i].Name).replace(/\s/ig, '_'),
                            fieldLabel: Label_f,
                            queryMode: 'local',
                            displayField: 'display',
                            valueField: 'value',
                            allowBlank: !adminData.RequiredAllFields && !fieldsOjb[i].IsRequired,
                            readOnly: fieldsOjb[i].IsReadOnly,
                            //fieldStyle: (fieldsOjb[i].IsReadOnly) ? "background:#ccc;":"",
                            fieldStyle: c_styles,
                            store: comboStore,
                            ComboBoxSelectionDependencyVariableID: fieldsOjb[i].ComboBoxSelectionDependencyVariableID,
                            ValIDComboBoxIndexPairs: fieldsOjb[i].ValIDComboBoxIndexPairs,
                            value: fieldsOjb[i].ComboBoxDoubleValues || fieldsOjb[i].ComboBoxStringValues,
                            ComboboxMadID: fieldsOjb[i].ID,
                            hidden: false,
                            listeners: {
                                select: function (combo, comboval) {
                                    if (!nestedPairs[combo.ComboboxMadID]) {
                                        return true;
                                    }

                                    var form_f = combo.up('form').getForm();

                                    for (var j = 0; j < nestedPairs[combo.ComboboxMadID].length; j++) {
                                        var nested_name = nestedPairs[combo.ComboboxMadID][j];
                                        var nested_comp = form_f.findField(nested_name);
                                        if (!nested_comp) {
                                            continue;
                                        }

                                        var custFilter = new Ext.util.Filter({
                                            filterFn: function (item) {
                                                return !comboval[0] ? false : (item.data.index1 == comboval[0].data.index1);
                                            }
                                        });
//
                                        nested_comp.getStore().clearFilter();
                                        nested_comp.getStore().addFilter(custFilter);
                                        nested_comp.setValue();
                                    }
                                },
                                beforerender: function (editor) {
                                    if (!!editor.ComboBoxSelectionDependencyVariableID) {
                                        var custFilter = new Ext.util.Filter({
                                            filterFn: function (item) {
                                                return false;
                                            }
                                        });
                                        editor.getStore().clearFilter();
                                        editor.getStore().addFilter(custFilter);
                                    }
                                }
                            }
                        };

                        Picklist_stores[Combobox.name] = comboStore;

                        res_a.push(Combobox);

                        //add nested comboboxes
                        if (!!fieldsOjb[i].ComboBoxSelectionDependencyVariableID) {
                            if (!nestedPairs[fieldsOjb[i].ComboBoxSelectionDependencyVariableID]) {
                                nestedPairs[fieldsOjb[i].ComboBoxSelectionDependencyVariableID] = [];
                            }
                            nestedPairs[fieldsOjb[i].ComboBoxSelectionDependencyVariableID].push(Combobox.name);
                        }

                    }
                }

                return res_a;
            }

            function generateSMComboboxStore(ComboEl) {
                ComboVal = (!!((ComboEl.DisplayFormat).indexOf("String") + 1)) ? ComboEl.ComboBoxStringValues : ComboEl.ComboBoxDoubleValues;
                if (ComboVal) {
                    var SMComboboxStore = new Ext.data.SimpleStore({
                        fields: ["value", "display", "index1"],
                        data: []
                    });
                    //console.dir(entities);
                    //SMComboboxStore.add({value:"undefined", display:"Select"});

                    if (Array.isArray(ComboEl.ValIDComboBoxIndexPairs)) {
                        //Nested Combobox
                        for (var i = 0; i < ComboEl.ValIDComboBoxIndexPairs.length; i++) {
                            var pairEl = ComboEl.ValIDComboBoxIndexPairs[i];
                            SMComboboxStore.add({
                                value: ComboVal[pairEl['Index2']].toString(),
                                display: ComboVal[pairEl['Index2']],
                                index1: pairEl['Index1']
                            });
                        }

                    } else {
                        //Usual Combobox
                        for (var i = 0; i < ComboVal.length; i++) {
                            SMComboboxStore.add({value: ComboVal[i].toString(), display: ComboVal[i], index1: i});
                        }
                    }
                    return SMComboboxStore;
                }
                else {
                    return;
                }
            }

            function sendSMDataFields(accountid, parentsiteid, implSiteId, tradeally, projectId, projectName, projectstatus, phase, phasenumber, initialphasedate, measureId, measureTplId, calculationType, espSmartMeasureId, dataFields) {
                //Send JSON Object to server plugin
                var smObj = {};

                //ddsm_AccountId
                //smObj.accountid = accountid.replace(/\{|\}/g, '');
                smObj.accountid = accountid;
                //ddsm_parentsite
                //smObj.parentsiteid = parentsiteid.replace(/\{|\}/g, '');
                smObj.parentsiteid = parentsiteid;
                //----------TODO: UNCOMMMENT-----------START<<<<<<<<<<<<<
                ////ddsm_ImplementationSite
                //smObj.implSiteId = parentsiteid.replace(/\{|\}/g, '');
                //----------TODO: UNCOMMMENT-------------END<<<<<<<<<<<<<
                //ddsm_TradeAllyId
                //smObj.tradeally = tradeally.replace(/\{|\}/g, '');
                smObj.tradeally = tradeally;
                //ddsm_ProjectToMeasureId
                //smObj.projectId = projectId.replace(/\{|\}/g, '');
                smObj.projectId = projectId;
                smObj.projectName = projectName;
                //ddsm_ProjectStatus
                smObj.projectstatus = projectstatus;
                //ddsm_ProjectPhaseName
                smObj.phase = phase;
                //ddsm_UpToDatePhase
                smObj.phasenumber = phasenumber;
                //ddsm_InitialPhaseDate
                smObj.initialphasedate = ('0' + (initialphasedate.getUTCMonth() + 1)).slice(-2) + '/' + ('0' + initialphasedate.getUTCDate()).slice(-2) + '/' + initialphasedate.getFullYear();

                //smObj.measureId = measureId.replace(/\{|\}/g, '');
                //smObj.measureTplId = measureTplId.replace(/\{|\}/g, '');
                //smObj.calculationType = measureTplId.replace(/\{|\}/g, '');
                //smObj.espSmartMeasureId = espSmartMeasureId.replace(/\{|\}/g, '');
                smObj.measureId = measureId;
                smObj.measureTplId = measureTplId;
                smObj.calculationType = measureTplId;
                smObj.espSmartMeasureId = espSmartMeasureId;
                smObj.dataFields = dataFields;

                //console.dir(smObj);
                console.dir(JSON.stringify(smObj));

                setTimeout(function () {
                    //Send method .... JSON.stringify(smObj)
                    Process.callAction("ddsm_CalculateESPMeasure",
                        [{
                            key: "Target",
                            type: Process.Type.EntityReference,
                            value: {id: b.Xrm.Page.data.entity.getId(), entityType: "ddsm_project"}
                        },
                            {
                                key: "UserInput",
                                type: Process.Type.String,
                                //value: JSON.stringify(smObj)
                                value: Base64.encode(JSON.stringify(smObj))
                            }
                        ],
                        function (params) {
                            // Success
                            for (var i = 0; i < params.length; i++) {
                                params[i].key + "=" + params[i].value;
                                if (params[i].key === "Result") {
                                    console.log("Result : ");
                                    console.dir(params[i]);

                                    //check for error response
                                    try {
                                        var res = JSON.parse(params[i].value);
                                        if (res.hasOwnProperty('ErrorMsg')) {
                                            Ext.Msg.alert('Error', res.ErrorMsg);
                                            console.log(res.ErrorMsg);
                                            spinnerForm.stop();
                                            return;
                                        }
                                    } catch (e) {
                                        console.error('Response error:', e);
                                        spinnerForm.stop();
                                        return;
                                    }

                                    callbackSendSMData(JSON.parse(params[i].value))
                                }
                            }
                            // showPreloader(false);
                        },
                        function (e) {
                            // Error
                            console.log(e);
                            Ext.Msg.alert('Action failed','[ESP] Calculate ESP Measure: ' + e);

                            spinnerForm.stop();
                            // showPreloader(false);
                        }
                    );

                }, 100);


                //Clear current values

                smCurrentVersionData.MAD = null;
                smCurrentVersionData.CalculationID = null;
                smCurrentVersionData.MeasTplID = null;
                smCurrentVersionData.MeasID = null;
                smCurrentVersionData.MeasTplData = null;
                smCurrentVersionData.EditSM = false;
                smCurrentVersionData.MeasureData = null;
            }

            //Callback Smart measure object sends
            function callbackSendSMData(obj) {
                spinnerForm.stop();
                loadData(_getODataEndpoint(entitySchemaName), true);
            }

            function updateMADFieldsData(smTpl, MSDFields) {
                //function updateMADFieldsData(smTpl, MADFields, MSDFields){
                var allData = [], parentProjectFields = [], parentProjectData = {}, parentAccountFields = [],
                    parentAccountData = {}, parentSiteFields = [], parentSiteData = {}, thisMeasureFields = [],
                    thisMeasureData = {}, parentRateClassRefFields = [], parentRateClassRefData = {}, fundingSourceFields = [],
                    fundingSourceData = {}, siteExpand = null, measExpand = null;

                if (!MSDFields) {
                    MSDFields = {};
                }

                if(!!MSDFields["ddsm_recalculationgroup"]){
                    delete MSDFields["ddsm_recalculationgroup"];
                }

                //Add MSD mapping to objMappingSM
                for (var key in MSDFields) {

                    //objMappingSM[key] = MSDFields[key];
                    objMappingSM[key.toLowerCase()] = MSDFields[key];
                }

                let customMap = AGS.Entity.getTargetMapping(null, null, ['ddsm_measuretemplate','ddsm_sourcecalculationtype','Account','ddsm_site','ddsm_project','ddsm_rateclassreference']);
                let customMapAdvanced = AGS.Entity.getTargetMapping(962080005, null, ['ddsm_site']);
                if(!!customMapAdvanced && customMapAdvanced.hasOwnProperty('ddsm_site')){
                    let advSiteField = customMapAdvanced['ddsm_site'];
                    if(!customMap.hasOwnProperty('ddsm_site')){
                        customMap['ddsm_site'] = advSiteField;
                    }else{
                        for (let k in customMapAdvanced['ddsm_site']) {
                            customMap['ddsm_site'][k] = customMapAdvanced['ddsm_site'][k];
                        }
                    }
                }

                for (var key in objMappingSM) {
                    var el = objMappingSM[key];

                    switch (el.Entity) {

                        case "ddsm_rateclassreference":
                            if (parentRateClassRefFields.indexOf(el.AttrName) < 0) {
                                parentRateClassRefFields.push(el.AttrName);
                            }
                            break;
                        case "ddsm_project":
                            if (parentProjectFields.indexOf(el.AttrName) < 0) {
                                parentProjectFields.push(el.AttrName);
                            }
                            break;
                        case "ddsm_site":
                            if (parentSiteFields.indexOf(el.AttrName) < 0) {
                                parentSiteFields.push(el.AttrName);
                            }
                            break;
                        case "ddsm_measure":
                            if (thisMeasureFields.indexOf(el.AttrName) < 0) {
                                thisMeasureFields.push(el.AttrName);
                            }
                            break;
                        case "Account":
                            if (parentAccountFields.indexOf(el.AttrName) < 0) {
                                parentAccountFields.push(el.AttrName);
                            }
                            break;
                        case "ddsm_sourcecalculationtype":
                            if (fundingSourceFields.indexOf(el.AttrName) < 0) {
                                fundingSourceFields.push(el.AttrName);
                            }
                            break;
                    }
                }
                if (!!parentRateClassRefFields.length) {
                    //update rate class refferene fields with expand preffix
                    siteExpand = "ddsm_ddsm_rateclassreference_ddsm_site";
                }

                if (!!fundingSourceFields.length) {
                    //update rate class refferene fields with expand preffix
                    measExpand = "ddsm_ddsm_sourcecalculationtype_ddsm_measure_FundingSourceCalculationType";
                }

                if (!!parentProjectFields.length) {
                    AGS.REST.retrieveRecord(b.Xrm.Page.data.entity.getId(), "ddsm_project", parentProjectFields.join(','), null, function (data) {
                        //console.dir(data);
                        parentProjectData = data;
                    }, null, false);
                }

                if (!!thisMeasureFields.length || !!measExpand) {

                    //----------DEBUG-----------START<<<<<<<<<<<<<
                    //if(!!fundingSourceFields.length){
                    //    AGS.REST.retrieveRecord(b.Xrm.Page.getAttribute("ddsm_sourcecalculationtype").getValue()[0].id, "Account", parentAccountFields.join(','), null, function(data){
                    //        //console.dir(data);
                    //        fundingSourceData = data;
                    //    }, null, false);
                    //}
                    //----------DEBUG-------------END<<<<<<<<<<<<<

                    /* ----- COMMENTED. TO BE REBUILT
                     var measureTplMap = {};
                     var measureRateClassMap = {};
                     var sel = "?$select=ddsm_PrimaryFieldLogicalName,ddsm_SecondaryFieldLogicalName"
                     + "&$filter= ddsm_PrimaryEntityLogicalName eq 'ddsm_measure' and ddsm_SecondaryEntityLogicalName eq 'ddsm_measuretemplate' and ddsm_RelatedEntitiesGroup/Value eq 962080002";

                     AGS.REST.retrieveMultipleRecords("ddsm_mappingfields", sel, function(data){

                     for (var i = 0; i < data.length; i++) {
                     var el = data[i];
                     measureTplMap[data[i].ddsm_SecondaryFieldLogicalName] = data[i].ddsm_PrimaryFieldLogicalName;
                     }
                     }, null, null, false, null) ;

                     if(!!Object.keys(measureTplMap).length){

                     AGS.REST.retrieveRecord(smCurrentVersionData.MeasTplID, "ddsm_measuretemplate", Object.keys(measureTplMap).join(','), measExpand, function(data){
                     //console.dir(data);
                     for (var k in measureTplMap) {
                     if(!!data[k]) { thisMeasureData[measureTplMap[k]] = data[k]; }
                     }
                     }, null, false);
                     }

                     -----*/

                    //var measureTplMap = {};
                    //var measureRateClassMap = {};


                    if (customMap.hasOwnProperty('ddsm_measuretemplate') && !!Object.keys(customMap['ddsm_measuretemplate'])) {

                        AGS.REST.retrieveRecord(smCurrentVersionData.MeasTplID, "ddsm_measuretemplate", Object.keys(customMap['ddsm_measuretemplate']).join(','), measExpand, function (data) {
                            //console.dir(data);
                            let measureTplMap = customMap['ddsm_measuretemplate'];
                            let sourceCalcMap = customMap['ddsm_sourcecalculationtype'];

                            for (let k in measureTplMap) {
                                if (!!data[k]) {
                                    thisMeasureData[measureTplMap[k]] = data[k];
                                }
                            }

                            for (let k in sourceCalcMap) {
                                if (!!data[k]) {
                                    thisMeasureData[sourceCalcMap[k]] = data[k];
                                }
                            }

                        }, null, false);
                    }
                }

                if (!!parentAccountFields.length) {
                    AGS.REST.retrieveRecord(b.Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id, "Account", parentAccountFields.join(','), null, function (data) {
                        //console.dir(data);
                        parentAccountData = data;
                    }, null, false);
                }


                if (!parentSiteFields) {
                    parentSiteFields = ['ddsm_name'];
                }
                if (!!parentSiteFields.length || !!siteExpand) {
                    AGS.REST.retrieveRecord(b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id, "ddsm_site", parentSiteFields.join(','), siteExpand, function (data) {
                        //console.dir(data);
                        parentSiteData = data;
                        if (!!data[siteExpand]) {
                            for (var i = 0; i < parentRateClassRefFields.length; i++) {

                                var k = parentRateClassRefFields[i];
                                var el = data[siteExpand][k];
                                if (!!el) {
                                    parentRateClassRefData[k] = el;
                                }

                            }
                            delete data[siteExpand];
                        }

                    }, null, false);
                }
                /*
                 if(parentProjectFields.length > 0){
                 AGS.REST.retrieveRecord(b.Xrm.Page.data.entity.getId(), "ddsm_measure", thisMeasureFields.join(','), null, function(data){
                 thisMeasureData = data;
                 }, null, false);
                 }
                 */

                allData = allData.concat(thisMeasureData, parentProjectData, parentAccountData, parentSiteData, parentRateClassRefData);
                for (var i = 0; i < allData.length; i++) {
                    //console.dir(allData[i]);

                    for (key in objMappingSM) {
                        for (key2 in allData[i]) {
                            //console.log(objMappingSM[key].AttrName +" | " + key2);

                            if (!!objMappingSM[key].DispNameOver) {
                                objMappingSM[key].DispName = objMappingSM[key].DispNameOver;
                            }

                            if (objMappingSM[key].AttrName != key2) {
                                continue;
                            }
                            //console.dir(allData[i][key2]);
                            if (objMappingSM[key].FieldType == "String" || objMappingSM[key].FieldType == "Integer" || objMappingSM[key].FieldType == "Decimal") {
                                objMappingSM[key].Value = allData[i][key2];
                            } else if (objMappingSM[key].FieldType == "Money") {
                                objMappingSM[key].Value = allData[i][key2].Value || 0;
                            } else if (objMappingSM[key].FieldType == "Picklist") {
                                objMappingSM[key].Value = allData[i][key2].Value;
                            } else if (objMappingSM[key].FieldType == "Boolean") {
                                objMappingSM[key].Value = allData[i][key2];
                            } else if (objMappingSM[key].FieldType == "Lookup") {
                                //objMappingSM[key].Value = allData[i][key2].Name;
                                //objMappingSM[key].Value = AGS.String.format("{0}|{1}|{2}",allData[i][key2].LogicalName, allData[i][key2].Id, allData[i][key2].Name);
                                objMappingSM[key].Value = AGS.String.format("{0}|{1}|{2}",objMappingSM[key].LookupEntity, allData[i][key2].Id ||'', allData[i][key2].Name ||'');
                            }
                        }
                    }

                }

                var usedMSD = [], plists = [];

                for (var i = 0; i < MADFields.length; i++) {

                    //var k = MADFields[i].Name;
                    let k = MADFields[i].Name.toLowerCase();
                    MADFields[i].IsMSD = false;

                    //if(typeof objMappingSM[k] == 'undefined' || typeof objMappingSM[k].Value == 'undefined') {continue;}

                    if (MSDFields.hasOwnProperty(k.toLowerCase())) {
                        MADFields[i].IsMSD = true;
                        usedMSD.push(k);
                    }

                    if (typeof objMappingSM[k] == 'undefined') {
                        continue;
                    }

                    MADFields[i].Entity = !!objMappingSM[k].Entity ? objMappingSM[k].Entity.toLowerCase() : '';

                    if (typeof objMappingSM[k].Value == 'undefined') {
                        objMappingSM[k].Value = '';
                    }

                    if (MSDFields.hasOwnProperty(objMappingSM[k].AttrName.toLowerCase()) && usedMSD.indexOf(objMappingSM[k].AttrName.toLowerCase()) < 0) {
                        MADFields[i].IsMSD = true;
                        usedMSD.push(objMappingSM[k].AttrName.toLowerCase());
                    }

                    if (objMappingSM[k].FieldType == "String") {
                        MADFields[i].StringValue = !!objMappingSM[k].Value ? objMappingSM[k].Value.trim() : '';
                        if (!MADFields[i].DisplayFormat) {
                            MADFields[i].DisplayFormat = 'TextboxStringValue';
                        }
                    }
                    if (objMappingSM[k].FieldType == "Integer" || objMappingSM[k].FieldType == "Decimal" || objMappingSM[k].FieldType == "Money") {
                        MADFields[i].DoubleValue = !!objMappingSM[k].Value ? objMappingSM[k].Value : 0;
                        if (!MADFields[i].DisplayFormat) {
                            MADFields[i].DisplayFormat = 'Decimal';
                        }
                    }
                    if (objMappingSM[k].FieldType == "Picklist") {
                        MADFields[i].ComboBoxDoubleValues = !!objMappingSM[k].Value ? objMappingSM[k].Value : '';
                        MADFields[i].ComboBoxStringValues = !!objMappingSM[k].Value ? objMappingSM[k].Value : '';
                        //if(!MADFields[i].DisplayFormat){ MADFields[i].DisplayFormat = 'Combobox';}
                        MADFields[i].DisplayFormat = 'Combobox';
                        MADFields[i].AttrName = objMappingSM[k].AttrName.toLowerCase();
                        plists.push({
                            entity: objMappingSM[k].Entity,
                            attrName: objMappingSM[k].AttrName
                        });
                    }
                }


                for (var key in MSDFields) {
                    var el = MSDFields[key];
                    if (usedMSD.indexOf(el.AttrName.toLowerCase()) >= 0) {
                        continue;
                    }

                    var m_obj = {};

                    m_obj.IsMSD = true;
                    usedMSD.push(key);

                    m_obj.Name = key;
                    m_obj.Entity = !!objMappingSM[key].Entity ? objMappingSM[key].Entity.toLowerCase() : '';

                    if (!!el.DispNameOver) {
                        objMappingSM[key].DispName = el.DispNameOver;
                    }

                    //if (el.FieldType == "String" || el.FieldType == "Lookup") {
                    //    m_obj.StringValue = !!el.Value ? el.Value.trim() : '';
                    //    m_obj.DisplayFormat = 'TextboxStringValue';
                    //}
                    //if (el.FieldType == "Integer" || el.FieldType == "Decimal" || el.FieldType == "Money") {
                    //    m_obj.DoubleValue = el.Value || 0;
                    //    m_obj.DisplayFormat = 'DecimalDouble';
                    //}
                    //if (el.FieldType == "Picklist") {
                    //    m_obj.DisplayFormat = 'Combobox';
                    //    //m_obj.ComboBoxStringValues = el.Value || '';
                    //    m_obj.ComboBoxDoubleValues = el.Value || '';
                    //    m_obj.AttrName = objMappingSM[key].AttrName.toLowerCase();
                    //    plists.push({
                    //        entity: objMappingSM[key].Entity,
                    //        attrName: objMappingSM[key].AttrName
                    //    });
                    //}
                    switch (el.FieldType) {
                        case("Boolean"):
                            m_obj.BooleanValue = el.Value || false;
                            m_obj.DisplayFormat = 'Boolean';
                            break;

                        case("Integer" || "Decimal" || "Money"):
                            m_obj.DoubleValue = el.Value || 0;
                            m_obj.DisplayFormat = 'DecimalDouble';
                            break;
                        case("Picklist"):
                            m_obj.DisplayFormat = 'Combobox';
                            //m_obj.ComboBoxStringValues = el.Value || '';
                            m_obj.ComboBoxDoubleValues = el.Value || '';
                            m_obj.AttrName = objMappingSM[key].AttrName.toLowerCase();
                            plists.push({
                                entity: objMappingSM[key].Entity,
                                attrName: objMappingSM[key].AttrName
                            });
                            break;
                        case("Lookup"):
                                m_obj.DisplayFormat = 'Lookup';
                                m_obj.ComboBoxStringValues = el.Value.trim()|| '';
                                m_obj.AttrName = objMappingSM[key].AttrName.toLowerCase();
                            break;
                        default:
                            m_obj.StringValue = !!el.Value ? String(el.Value).trim() : '';
                            m_obj.DisplayFormat = 'TextboxStringValue';
                            break;
                    }

                    MADFields.push(m_obj);
                }

                //return MADFields;
                //buildSMPicklist(plists,smTpl,MADFields);
                buildSMPicklist(plists, smTpl);

            }

            function updateMADFieldsDataEdit() {
                var allData = [], parentProjectFields = [], parentProjectData = {}, parentAccountFields = [], parentAccountData = {}, parentSiteFields = [], parentSiteData = {}, thisMeasureFields = [], parentRateClassRefFields = [], parentRateClassRefData = {}, siteExpand = null, measExpand = null;

                var MADFields = JSON.parse(smCurrentVersionData.MAD);
                var MSDFields = JSON.parse(smCurrentVersionData.MeasTplData.ddsm_MSDFields);

                if (!MSDFields) {
                    MSDFields = {};
                }

                //Add MSD mapping to objMappingSM
                for (let key in MSDFields) {
                    objMappingSM[key] = MSDFields[key];
                }

                for (let key in objMappingSM) {
                    let el = objMappingSM[key];

                    switch (el.Entity) {

                        case "ddsm_rateclassreference":
                            if (parentRateClassRefFields.indexOf(el.AttrName) < 0) {
                                parentRateClassRefFields.push(el.AttrName);
                            }
                            break;
                        case "ddsm_project":
                            if (parentProjectFields.indexOf(el.AttrName) < 0) {
                                parentProjectFields.push(el.AttrName);
                            }
                            break;
                        case "ddsm_site":
                            if (parentSiteFields.indexOf(el.AttrName) < 0) {
                                parentSiteFields.push(el.AttrName);
                            }
                            break;
                        case "Account":
                            if (parentAccountFields.indexOf(el.AttrName) < 0) {
                                parentAccountFields.push(el.AttrName);
                            }
                            break;
                        case "ddsm_measure":
                            if (thisMeasureFields.indexOf(el.AttrName) < 0) {
                                thisMeasureFields.push(el.AttrName);
                            }
                            break;
                    }
                }
                //if(!!parentRateClassRefFields.length)
                //{
                //    //update rate class refferene fields with expand preffix
                //    siteExpand = "ddsm_ddsm_rateclassreference_ddsm_site";
                //}

                //if(!!parentProjectFields.length){
                //    AGS.REST.retrieveRecord(b.Xrm.Page.data.entity.getId(), "ddsm_project", parentProjectFields.join(','), null, function(data){
                //        //console.dir(data);
                //        parentProjectData = data;
                //    }, null, false);
                //}


                //----------DEBUG-----------START<<<<<<<<<<<<<

                //----------DEBUG COMMENTED !!!!!! <<<<<<<<<<<<<<<<< !!!!!!!!!!!!!!!!!!! <<<<<<<<<<<<<<<<<<-----------START<<<<<<<<<<<<<
                /*------------
                 debugger;
                 var measureMap = {};

                 let entityList = ['Account','ddsm_site','ddsm_project','ddsm_rateclassreference','ddsm_sourcecalculationtype'];

                 var sel = "?$select=ddsm_PrimaryFieldLogicalName,ddsm_SecondaryFieldLogicalName,ddsm_SecondaryEntityLogicalName"
                 + "&$filter= ddsm_PrimaryEntityLogicalName eq 'ddsm_measure' and ddsm_RelatedEntitiesGroup/Value eq 962080002"
                 + " and (ddsm_SecondaryEntityLogicalName eq 'Account' or ddsm_SecondaryEntityLogicalName eq 'ddsm_site' or ddsm_SecondaryEntityLogicalName 'ddsm_project'"
                 + " or ddsm_SecondaryEntityLogicalName eq 'ddsm_rateclassreference' or ddsm_SecondaryEntityLogicalName eq 'ddsm_sourcecalculationtype')";

                 AGS.REST.retrieveMultipleRecords("ddsm_mappingfields", sel, function(data){

                 for (let i = 0; i < data.length; i++) {
                 let el = data[i];
                 measureMap[el.ddsm_SecondaryFieldLogicalName] = el.ddsm_PrimaryFieldLogicalName;

                 if (parentAccountFields.indexOf((el.ddsm_PrimaryFieldLogicalName) < 0)
                 && (el.ddsm_PrimaryFieldLogicalName >= 0
                 || parentAccountFields.indexOf(el.ddsm_PrimaryFieldLogicalName) >= 0
                 || parentAccountFields.indexOf(el.ddsm_PrimaryFieldLogicalName) >= 0
                 || parentAccountFields.indexOf(el.ddsm_PrimaryFieldLogicalName) >= 0 )) {

                 thisMeasureFields.push(el.ddsm_PrimaryFieldLogicalName);
                 }
                 }
                 }, null, null, false, null) ;
                 --------------------------*/
                //----------DEBUG COMMENTED !!!!!! <<<<<<<<<<<<<<<<< !!!!!!!!!!!!!!!!!!! <<<<<<<<<<<<<<<<<<-------------END<<<<<<<<<<<<<

                let customMap = AGS.Entity.getTargetMapping(null, null, ['Account','ddsm_site','ddsm_project','ddsm_rateclassreference','ddsm_sourcecalculationtype','ddsm_measuretemplate',]);

                let fieldsObj = MADFields.concat(MSDFields);
                for(let i = 0; i< fieldsObj.length; i++){

                    let mad_obj = fieldsObj[i];
                    let k = !!mad_obj.Name ? mad_obj.Name.toLowerCase() : '';

                    if (!k) {
                        continue;
                    }

                    for (let ent in customMap) {
                        if(!ent.hasOwnProperty(k)){
                            continue;
                        }

                        if(thisMeasureFields.indexOf(ent[k])>=0){
                            thisMeasureFields.push(ent[k]);
                        }
                    }
                }


                //if(!!Object.keys(measureTplMap).length){
                //
                //    AGS.REST.retrieveRecord(smCurrentVersionData.MeasTplID, "ddsm_measuretemplate", Object.keys(measureTplMap).join(','), null, function(data){
                //        //console.dir(data);
                //        for (var k in measureTplMap) {
                //            if(!!data[k]) { thisMeasureData[measureTplMap[k]] = data[k]; }
                //        }
                //    }, null, false);
                //}
                //----------DEBUG-------------END<<<<<<<<<<<<<

                //(parentAccountFields.concat(parentRateClassRefFields,parentProjectFields, parentSiteFields)).forEach(function(el){
                //    if (thisMeasureFields.indexOf(el) < 0) {
                //        thisMeasureFields.push(el);
                //    }
                //
                //}) ;
                if (!!thisMeasureFields.length) {
                    let selFields = [];

                    //for (let i = 0; i < thisMeasureFields.length; i++) {
                    //    if (!smCurrentVersionData.MeasureData.hasOwnProperty(thisMeasureFields[i])) {
                    //        selFields.push(thisMeasureFields[i]);
                    //    }
                    //}

                    selFields = thisMeasureFields;

                    if (!!selFields.length) {
                        AGS.REST.retrieveRecord(smCurrentVersionData.MeasID, "ddsm_measure", selFields.join(','), null, function (data) {
                            //console.dir(data);
                            for (let i = 0; i < selFields.length; i++) {
                                let k = selFields[i];
                                smCurrentVersionData.MeasureData[k] = (data.hasOwnProperty(k)) ? data[k] : null;
                            }
                        }, null, false);
                    }
                }

                allData = smCurrentVersionData.MeasureData;
                //for(let i = 0; i < allData.length; i++){
                //console.dir(allData[i]);

                for (let key in objMappingSM) {
                    for (let key2 in allData) {
                        //console.log(objMappingSM[key].AttrName +" | " + key2);

                        if (!!objMappingSM[key].DispNameOver) {
                            objMappingSM[key].DispName = objMappingSM[key].DispNameOver;
                        }

                        if (objMappingSM[key].AttrName != key2) {
                            continue;
                        }

                        ////console.dir(allData[i][key2]);
                        //if (objMappingSM[key].FieldType == "String" || objMappingSM[key].FieldType == "Integer" || objMappingSM[key].FieldType == "Decimal" || objMappingSM[key].FieldType == "Boolean") {
                        //    objMappingSM[key].Value = allData[key2];
                        //} else if (objMappingSM[key].FieldType == "Money") {
                        //    objMappingSM[key].Value = allData[key2].Value || 0;
                        //} else if (objMappingSM[key].FieldType == "Picklist") {
                        //    objMappingSM[key].Value = allData[key2].Value;
                        //} else if (objMappingSM[key].FieldType == "Lookup") {
                        //    objMappingSM[key].Value = allData[key2].Name;
                        //}
                        //console.dir(allData[i][key2]);
                        if (objMappingSM[key].FieldType == "String" || objMappingSM[key].FieldType == "Integer" || objMappingSM[key].FieldType == "Decimal" || objMappingSM[key].FieldType == "Boolean") {
                            objMappingSM[key].Value = allData[key2];
                        } else if (objMappingSM[key].FieldType == "Money") {
                            objMappingSM[key].Value = allData[key2] || 0;
                        } else if (objMappingSM[key].FieldType == "Picklist") {
                            objMappingSM[key].Value = allData[key2];
                        } else if (objMappingSM[key].FieldType == "Lookup") {
                            objMappingSM[key].Value = allData[key2].Name;
                        }
                    }
                }

                //}

                var usedMSD = [], plists = [];

                for (let i = 0; i < MADFields.length; i++) {

                    let k = MADFields[i].Name;
                    MADFields[i].IsMSD = false;

                    //if(typeof objMappingSM[k] == 'undefined' || typeof objMappingSM[k].Value == 'undefined') {continue;}

                    if (MSDFields.hasOwnProperty(k.toLowerCase())) {
                        MADFields[i].IsMSD = true;
                        usedMSD.push(k);
                    }

                    if (typeof objMappingSM[k] == 'undefined') {
                        continue;
                    }

                    MADFields[i].Entity = !!objMappingSM[k].Entity ? objMappingSM[k].Entity.toLowerCase() : '';

                    if (typeof objMappingSM[k].Value == 'undefined') {
                        objMappingSM[k].Value = '';
                    }

                    if (MSDFields.hasOwnProperty(objMappingSM[k].AttrName.toLowerCase()) && usedMSD.indexOf(objMappingSM[k].AttrName.toLowerCase()) < 0) {
                        MADFields[i].IsMSD = true;
                        usedMSD.push(objMappingSM[k].AttrName.toLowerCase());
                    }

                    if (objMappingSM[k].FieldType == "Boolean") {
                        MADFields[i].BooleanValue = !!objMappingSM[k].Value ? objMappingSM[k].Value : false;
                        if (!MADFields[i].DisplayFormat) {
                            MADFields[i].DisplayFormat = 'Boolean';
                        }
                    }

                    if (objMappingSM[k].FieldType == "String" || objMappingSM[k].FieldType == "Lookup") {
                        MADFields[i].StringValue = !!objMappingSM[k].Value ? objMappingSM[k].Value.trim() : '';
                        if (!MADFields[i].DisplayFormat) {
                            MADFields[i].DisplayFormat = 'TextboxStringValue';
                        }
                    }
                    if (objMappingSM[k].FieldType == "Integer" || objMappingSM[k].FieldType == "Decimal" || objMappingSM[k].FieldType == "Money") {
                        MADFields[i].DoubleValue = !!objMappingSM[k].Value ? objMappingSM[k].Value : 0;
                        if (!MADFields[i].DisplayFormat) {
                            MADFields[i].DisplayFormat = 'Decimal';
                        }
                    }
                    if (objMappingSM[k].FieldType == "Picklist") {
                        MADFields[i].ComboBoxDoubleValues = !!objMappingSM[k].Value ? objMappingSM[k].Value : '';
                        MADFields[i].ComboBoxStringValues = !!objMappingSM[k].Value ? objMappingSM[k].Value : '';
                        //if(!MADFields[i].DisplayFormat){ MADFields[i].DisplayFormat = 'Combobox';}
                        MADFields[i].DisplayFormat = 'Combobox';
                        MADFields[i].AttrName = objMappingSM[k].AttrName.toLowerCase();
                        plists.push({
                            entity: objMappingSM[k].Entity,
                            attrName: objMappingSM[k].AttrName
                        });
                    }
                }

                for (let key in MSDFields) {
                    let el = MSDFields[key];
                    if (usedMSD.indexOf(el.AttrName.toLowerCase()) >= 0) {
                        continue;
                    }

                    var m_obj = {};

                    m_obj.IsMSD = true;
                    usedMSD.push(key);

                    m_obj.Name = key;
                    m_obj.Entity = !!objMappingSM[key].Entity ? objMappingSM[key].Entity.toLowerCase() : '';

                    if (!!el.DispNameOver) {
                        objMappingSM[key].DispName = el.DispNameOver;
                    }

                    if (el.FieldType == "String") {
                        m_obj.StringValue = !!el.Value ? el.Value.trim() : '';
                        m_obj.DisplayFormat = 'TextboxStringValue';
                    }
                    if (el.FieldType == "Lookup") {
                        m_obj.StringValue = !!el.Value ? el.Value.trim() : '';
                        m_obj.DisplayFormat = 'Lookup';
                    }
                    if (el.FieldType == "Integer" || el.FieldType == "Decimal" || el.FieldType == "Money") {
                        m_obj.DoubleValue = el.Value || 0;
                        m_obj.DisplayFormat = 'Decimal';
                    }
                    if (el.FieldType == "Boolean") {
                        m_obj.BooleanValue = !!el.Value ? el.Value : false;
                        m_obj.DisplayFormat = 'BooleanValue';
                    }
                    if (el.FieldType == "Picklist") {
                        m_obj.DisplayFormat = 'Combobox';
                        m_obj.ComboBoxStringValues = el.Value || '';
                        m_obj.AttrName = objMappingSM[key].AttrName.toLowerCase();
                        plists.push({
                            entity: objMappingSM[key].Entity,
                            attrName: objMappingSM[key].AttrName
                        });
                    }

                    MADFields.push(m_obj);
                }

                //return MADFields;
                //buildSMPicklist(plists,smCurrentVersionData.MeasTplData,MADFields);
                buildSMPicklist(plists, smCurrentVersionData.MeasTplData);
            }

            /**
             * Generate pick lists and initate form generation if thea are ready
             * @param plists
             * @param smTpl
             * @param MADFields
             */
            //function buildSMPicklist(plists,smTpl,MADFields){
            function buildSMPicklist(plists, smTpl) {

                var plist_count = plists.length || 0;

                if (!plists.length) {
                    //generateSMForm(smTpl, MADFields);
                    generateSMForm(smTpl);
                    return;
                }

                for (var i = 0; i < plists.length; i++) {
                    var obj = plists[i];

                    if (!(obj.hasOwnProperty('entity') && !!obj.entity) || !(obj.hasOwnProperty('attrName') && obj.attrName)) {
                        plist_count--;
                        continue;
                    }

                    //inner method
                    var _retrieveOptionSetSM = function (entName, attrName) {
                        SDK.Metadata.RetrieveAttribute(entName, attrName, null, false,
                            function (res) {
                                console.dir(res);
                                //Success
                                plist_count--;
                                var key = AGS.String.format("{0}|{1}", entName, attrName);
                                smComboStoreSourecesCRM[key] = generateAttrComboStore(key, res.OptionSet.Options);
                                if (!plist_count) {
                                    //generateSMForm(smTpl, MADFields, smComboStoreSourecesCRM);
                                    generateSMForm(smTpl, smComboStoreSourecesCRM);
                                }
                            },
                            function (res) {
                                //Error
                                console.error('Failed to create a picklist store', res.message);
                                plist_count--;
                                if (!plist_count) {
                                    //generateSMForm(smTpl, MADFields, smComboStoreSourecesCRM);
                                    generateSMForm(smTpl, smComboStoreSourecesCRM);
                                }
                            })
                    };

                    _retrieveOptionSetSM(obj.entity.toLowerCase(), obj.attrName.toLowerCase());
                }
            }

            // icluded function
            function generateAttrComboStore(key, c_options) {

                var tmpArr = [];
                for (var i = 0; i < c_options.length; i++) {
                    tmpArr.push(
                        [c_options[i].Value, c_options[i].Label.UserLocalizedLabel.Label]
                    );
                }

                return Ext.create('Ext.data.ArrayStore', {
                    storeId: 'comboStore_' + key,
                    fields: ["value", "display"],
                    data: tmpArr
                });
            }

            //Create New Measure
            function createNewMeasure(lkp_meas_tpl) {
                //console.dir(lkp_meas_tpl);
                var thisMeasureData = {};
                let customMap = AGS.Entity.getTargetMapping(null, null, ['ddsm_measuretemplate','Account','ddsm_site','ddsm_project','ddsm_rateclassreference']);
                //console.dir(customMap);
                if (customMap.hasOwnProperty('ddsm_measuretemplate') && !!Object.keys(customMap['ddsm_measuretemplate'])) {
                    AGS.REST.retrieveRecord(lkp_meas_tpl.id, "ddsm_measuretemplate", Object.keys(customMap['ddsm_measuretemplate']).join(','), null, function (data) {
                        //console.dir(data);
                        let measureTplMap = customMap['ddsm_measuretemplate'];
                        for (let k in measureTplMap) {
                            if (!!data[k]) {
                                thisMeasureData[measureTplMap[k]] = data[k];
                            }
                        }
                    }, null, false);
                }
                //console.dir(thisMeasureData);
                if (customMap.hasOwnProperty('Account') && !!Object.keys(customMap['Account'])) {
                    AGS.REST.retrieveRecord((b.Xrm.Page.getAttribute("ddsm_accountid").getValue())[0].id, "Account", Object.keys(customMap['Account']).join(','), null, function (data) {
                        //console.dir(data);
                        let AccountMap = customMap['Account'];
                        for (let k in AccountMap) {
                            if (!!data[k]) {
                                thisMeasureData[AccountMap[k]] = data[k];
                            }
                        }
                    }, null, false);
                }
                //console.dir(thisMeasureData);
                if (customMap.hasOwnProperty('ddsm_site') && !!Object.keys(customMap['ddsm_site'])) {
                    AGS.REST.retrieveRecord((b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue())[0].id, "ddsm_site", Object.keys(customMap['ddsm_site']).join(','), null, function (data) {
                        //console.dir(data);
                        let siteMap = customMap['ddsm_site'];
                        for (let k in siteMap) {
                            if (!!data[k]) {
                                thisMeasureData[siteMap[k]] = data[k];
                            }
                        }
                    }, null, false);
                }
                //console.dir(thisMeasureData);
                if (customMap.hasOwnProperty('ddsm_project') && !!Object.keys(customMap['ddsm_project'])) {
                    AGS.REST.retrieveRecord(b.Xrm.Page.data.entity.getId(), "ddsm_project", Object.keys(customMap['ddsm_project']).join(','), null, function (data) {
                        //console.dir(data);
                        let projectMap = customMap['ddsm_project'];
                        for (let k in projectMap) {
                            if (!!data[k]) {
                                thisMeasureData[projectMap[k]] = data[k];
                            }
                        }
                    }, null, false);
                }
                //console.dir(thisMeasureData);

                let parentsiteid = b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue();
                let accountid = b.Xrm.Page.getAttribute("ddsm_accountid").getValue();
                let tradeally = b.Xrm.Page.getAttribute("ddsm_tradeally").getValue();

                if(!!parentsiteid)
                    thisMeasureData["ddsm_parentsite"] = {Id: parentsiteid[0].id, Name: parentsiteid[0].name, LogicalName: parentsiteid[0].typename};
                if(!!accountid)
                    thisMeasureData["ddsm_AccountId"] = {Id: accountid[0].id, Name: accountid[0].name, LogicalName: accountid[0].typename};
                if(!!tradeally)
                    thisMeasureData["ddsm_TradeAllyId"] = {Id: tradeally[0].id, Name: tradeally[0].name, LogicalName: tradeally[0].typename};
                thisMeasureData["ddsm_ProjectToMeasureId"] = {Id: b.Xrm.Page.data.entity.getId(), Name: b.Xrm.Page.getAttribute("ddsm_name").getValue(), LogicalName: "ddsm_project"};
                thisMeasureData["ddsm_MeasureSelector"] = {Id: lkp_meas_tpl.id, Name: lkp_meas_tpl.name, LogicalName: "ddsm_measuretemplate"};

                var now = new Date();
//                thisMeasureData["ddsm_name"] = (b.Xrm.Page.getAttribute("ddsm_name").getValue()).substr(0, 6) + "-" + lkp_meas_tpl.name + "_" + now.getFullYear() + now.getMonth() + now.getDate() + "-" + now.getHours() + now.getMinutes() + now.getSeconds();
                //thisMeasureData["ddsm_name"] = lkp_meas_tpl.name + "_" + now.getFullYear() + now.getMonth() + now.getDate() + "-" + now.getHours() + now.getMinutes() + now.getSeconds();
                thisMeasureData["ddsm_name"] = lkp_meas_tpl.name;
                thisMeasureData["ddsm_InitialPhaseDate"] = now;

                AGS.REST.retrieveMultipleRecords("ddsm_measuretemplate", "$select=ddsm_ddsm_measurecalculationtemplate_ddsm_measur&$expand=ddsm_ddsm_measurecalculationtemplate_ddsm_measur&$filter=ddsm_measuretemplateId eq guid'" + lkp_meas_tpl.id + "' and statecode/Value eq 0", null, null, function(data) {
                    let measCalcTpl_Data = data[0].ddsm_ddsm_measurecalculationtemplate_ddsm_measur;
                    console.dir(measCalcTpl_Data);
                    let measCalcTpl_CalcTypeValues = "";
                    if (!!measCalcTpl_Data) {
                        if (measCalcTpl_Data.ddsm_CalculateSavings) measCalcTpl_CalcTypeValues += "Calculate Savings; ";
                        if (measCalcTpl_Data.ddsm_CalculateIncentive) measCalcTpl_CalcTypeValues += "Calculate Incentives; ";
                        if (measCalcTpl_Data.ddsm_PerUnitLock) measCalcTpl_CalcTypeValues += "Lock Per Units; ";
                        if (measCalcTpl_Data.ddsm_SavingsLock) measCalcTpl_CalcTypeValues += "Lock Savings; ";
                        if (measCalcTpl_Data.ddsm_IncentiveRatesLock) measCalcTpl_CalcTypeValues += "Lock Incentive Rates; ";
                        if (measCalcTpl_Data.ddsm_IncentivesLock) measCalcTpl_CalcTypeValues += "Lock Incentives; ";
                    }
                    thisMeasureData["ddsm_CalculationTypeValues"] = measCalcTpl_CalcTypeValues;
                }, false, []);


                //console.dir(thisMeasureData);
                var newGuid = AGS.REST.createRecord(thisMeasureData, "ddsm_measure", null, function(msg){console.log(msg);}, false);
                //console.dir(newGuid);

                loadData(_getODataEndpoint(entitySchemaName), true);

                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var recordsCount = recordsGrid.length;
                for(let i = 0; i<=recordsCount;i++){
                    if(newGuid.ddsm_measureId == recordsGrid[i].get("ddsm_measureId")){
                        Ext.getCmp(idGrid).getSelectionModel().select(i);
                        rowEditing.startEdit(i, 6);
                        break;
                    }
                }
            }

            // New Measure Grid Row
            function newMeasureGridRow() {
                _getLookupDataMT("ddsm_measuretemplate");
                newRecordRow = true;
                /*
                 var newRow = Ext.create(modelGrid, defaultFieldsValue);
                 var count = Ext.getCmp(idGrid).getStore().getCount();
                 Ext.getCmp(idGrid).getStore().insert(count, newRow);
                 Ext.getCmp(idGrid).getSelectionModel().select(count);
                 rowEditing.startEdit(count, 3);
                 newRecord();
                 */
            }

            function newRecord() {
                var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];

                var ddsm_parentsiteid = b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue();
                var ddsm_accountid = b.Xrm.Page.getAttribute("ddsm_accountid").getValue();
                var ddsm_tradeally = b.Xrm.Page.getAttribute("ddsm_tradeally").getValue();
                //var ddsm_programid = b.Xrm.Page.getAttribute("ddsm_programid").getValue();
                var ddsm_projectstatus = b.Xrm.Page.getAttribute("ddsm_projectstatus").getValue();
                var proj_ID = b.Xrm.Page.data.entity.getId();
                var proj_Name = b.Xrm.Page.getAttribute("ddsm_name").getValue();
                var proj_TypeName = "ddsm_project";

                rowEditing.getEditor().getForm().findField('ddsm_parentsite').setValue(ddsm_parentsiteid[0].name);
                rowEditing.getEditor().getForm().findField('ddsm_parentsite' + '__Id').setValue((ddsm_parentsiteid[0].id).replace(/\{|\}/g, ''));
                rowEditing.getEditor().getForm().findField('ddsm_parentsite' + '__LogicalName').setValue(ddsm_parentsiteid[0].typename);

                record.set('ddsm_parentsite', ddsm_parentsiteid[0].name);
                record.set('ddsm_parentsite' + '__Id', (ddsm_parentsiteid[0].id).replace(/\{|\}/g, ''));
                record.set('ddsm_parentsite' + '__LogicalName', ddsm_parentsiteid[0].typename);

                rowEditing.getEditor().getForm().findField('ddsm_AccountId').setValue(ddsm_accountid[0].name);
                rowEditing.getEditor().getForm().findField('ddsm_AccountId' + '__Id').setValue((ddsm_accountid[0].id).replace(/\{|\}/g, ''));
                rowEditing.getEditor().getForm().findField('ddsm_AccountId' + '__LogicalName').setValue(ddsm_accountid[0].typename);
                record.set('ddsm_AccountId', ddsm_accountid[0].name);
                record.set('ddsm_AccountId' + '__Id', (ddsm_accountid[0].id).replace(/\{|\}/g, ''));
                record.set('ddsm_AccountId' + '__LogicalName', ddsm_accountid[0].typename);

                if (ddsm_tradeally != null) {
                    rowEditing.getEditor().getForm().findField('ddsm_TradeAllyId').setValue(ddsm_tradeally[0].name);
                    rowEditing.getEditor().getForm().findField('ddsm_TradeAllyId' + '__Id').setValue((ddsm_tradeally[0].id).replace(/\{|\}/g, ''));
                    rowEditing.getEditor().getForm().findField('ddsm_TradeAllyId' + '__LogicalName').setValue(ddsm_tradeally[0].typename);
                    record.set('ddsm_TradeAllyId', ddsm_tradeally[0].name);
                    record.set('ddsm_TradeAllyId' + '__Id', (ddsm_tradeally[0].id).replace(/\{|\}/g, ''));
                    record.set('ddsm_TradeAllyId' + '__LogicalName', ddsm_tradeally[0].typename);
                }
                /*
                 if (ddsm_programid != null) {
                 rowEditing.getEditor().getForm().findField('ddsm_ProgramId').setValue(ddsm_programid[0].name);
                 rowEditing.getEditor().getForm().findField('ddsm_ProgramId' + '__Id').setValue((ddsm_programid[0].id).replace(/\{|\}/g, ''));
                 rowEditing.getEditor().getForm().findField('ddsm_ProgramId' + '__LogicalName').setValue(ddsm_programid[0].typename);
                 record.set('ddsm_ProgramId', ddsm_programid[0].name);
                 record.set('ddsm_ProgramId' + '__Id', (ddsm_programid[0].id).replace(/\{|\}/g, ''));
                 record.set('ddsm_ProgramId' + '__LogicalName', ddsm_programid[0].typename);
                 }
                 */

                rowEditing.getEditor().getForm().findField('ddsm_ProjectToMeasureId').setValue(proj_Name);
                rowEditing.getEditor().getForm().findField('ddsm_ProjectToMeasureId' + '__Id').setValue((proj_ID).replace(/\{|\}/g, ''));
                rowEditing.getEditor().getForm().findField('ddsm_ProjectToMeasureId' + '__LogicalName').setValue(proj_TypeName);
                record.set('ddsm_ProjectToMeasureId', proj_Name);
                record.set('ddsm_ProjectToMeasureId' + '__Id', (proj_ID).replace(/\{|\}/g, ''));
                record.set('ddsm_ProjectToMeasureId' + '__LogicalName', proj_TypeName);

                record.set("ddsm_ProjectStatus", ddsm_projectstatus);
                rowEditing.getEditor().getForm().findField('ddsm_ProjectStatus').setValue(ddsm_projectstatus);
//
                record.set("ddsm_ProjectPhaseName", b.Xrm.Page.getAttribute("ddsm_phase").getValue());
                rowEditing.getEditor().getForm().findField('ddsm_ProjectPhaseName').setValue(ddsm_phase);

                record.set("ddsm_InitialPhaseDate", new Date());
                rowEditing.getEditor().getForm().findField('ddsm_InitialPhaseDate').setValue(new Date());

                record.set("ddsm_UpToDatePhase", b.Xrm.Page.getAttribute("ddsm_phasenumber").getValue());
                rowEditing.getEditor().getForm().findField('ddsm_UpToDatePhase').setValue(b.Xrm.Page.getAttribute("ddsm_phasenumber").getValue());
            }

            function loadMeasMAD(smMeasID) {
                var req = new XMLHttpRequest();
                var serverUrl = b.Xrm.Page.context.getClientUrl();

                try {
                    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_measuretemplateSet?"
                        + "$select="
                        + "ddsm_name,"
                        + "ddsm_MeasureLifeMAD,"
                        + "ddsm_M3SavingsMAD,"
                        + "ddsm_SqFtMAD,"
                        + "ddsm_M3SavingsMAD,"
                        + "ddsm_RateClassMAD,"
                        + "ddsm_InsulationQualityMAD,"
                        + "ddsm_BaseEqWattMAD,"
                        + "ddsm_kWhsavingsMAD,"
                        + "ddsm_PersistenceMAD,"
                        + "ddsm_AttributionMAD,"
                        + "ddsm_FreeriderMAD,"
                        + "ddsm_SpilloverMAD,"
                        + "ddsm_IncrementalcostMAD,"

                        + "ddsm_MeasureLifeDemo,"
                        + "ddsm_M3Savings,"
                        + "ddsm_SqFt,"
                        + "ddsm_M3Savings,"
                        + "ddsm_RateClass,"
                        + "ddsm_InsulationQuality,"
                        + "ddsm_BaseEqWatt,"
                        + "ddsm_kWhsavings,"
                        + "ddsm_Persistence,"
                        + "ddsm_Attribution,"
                        + "ddsm_Freerider,"
                        + "ddsm_Spillover,"
                        + "ddsm_IncrementalCost,"


                        + "&$filter=ddsm_measuretemplateId eq guid'" + smMeasID + "' and statecode/Value eq 0", false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.send();
                    if (req.readyState == 4 && req.status == 200) {
                        //From the project template - will also contain milestone information
                        var _tplData = JSON.parse(req.responseText).d;
                        if (_tplData.results[0] != null) {
                            //console.dir(_tplData.results[0]);
                            return _tplData.results[0];
                        }
                    }
                    return null;
                }
                catch (err) {
                    Ext.Msg.alert("Failed to load Measure Template & Measure Calculation Template", err);
                    return null;
                }
            }

            //Synchronous query and field update
            function loadMeasTpl(lkp_meas_tpl) {
                var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                var req = new XMLHttpRequest();
                var measTpl_ID = lkp_meas_tpl.id;
                //console.log("Function loadMeas()... Measure selector id: " + measTpl_ID + "Measure selector name: " +lkp_meas_tpl.name);
                //var meas_ID = Xrm.Page.data.entity.getId();
                var now = new Date();
                //var context = GetGlobalContext();
                var serverUrl = b.Xrm.Page.context.getClientUrl();
                var measname = record.get('ddsm_ProjectToMeasureId').substr(0, 6) + "-" + lkp_meas_tpl.name + "_" + now.getFullYear() + now.getMonth() + now.getDate() + "-" + now.getHours() + now.getMinutes() + now.getSeconds();
                record.set('ddsm_name', measname);
                rowEditing.getEditor().getForm().findField('ddsm_name').setValue(measname);


                try {
                    /*
                     var objSKU = getSKUdata(measTpl_ID);
                     //console.dir(objSKU);
                     if(objSKU != null) {

                     if (objSKU.ddsm_Manufacturer != null) {
                     record.set("ddsm_Manufacturer", objSKU.ddsm_Manufacturer);
                     rowEditing.getEditor().getForm().findField('ddsm_Manufacturer').setValue(objSKU.ddsm_Manufacturer);
                     }
                     if (objSKU.ddsm_ModelNumber != null) {
                     record.set("ddsm_ModelNumber", objSKU.ddsm_ModelNumber);
                     rowEditing.getEditor().getForm().findField('ddsm_ModelNumber').setValue(objSKU.ddsm_ModelNumber);
                     }
                     if (objSKU.ddsm_Status.Value != null) {
                     record.set("ddsm_ModelNumberStatus", objSKU.ddsm_Status.Value.toString());
                     rowEditing.getEditor().getForm().findField('ddsm_ModelNumberStatus').setValue(objSKU.ddsm_Status.Value.toString());
                     }
                     }
                     */
                    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_measuretemplateSet?"
                            /*
                             + "$select="
                             + "ddsm_IncRateUnit,"
                             + "ddsm_IncRateKW,"
                             + "ddsm_IncRatekWh,"
                             + "ddsm_IncRatethm,"
                             + "ddsm_SavingsperUnitKW,"
                             + "ddsm_SavingsperUnitkWh,"
                             + "ddsm_SavingsperUnitthm,"
                             + "ddsm_Program,"
                             + "ddsm_MeasureCode,"
                             + "ddsm_MeasureNumber,"
                             + "ddsm_EnergyType,"
                             + "ddsm_Category,"
                             + "ddsm_MeasureGroup,"
                             + "ddsm_MeasureSubgroup,"
                             + "ddsm_Size,"
                             + "ddsm_EUL,"
                             + "ddsm_BaseEquipDescr,"
                             + "ddsm_EffEquipDescr,"
                             + "ddsm_AnnualFixedCosts,"
                             + "ddsm_ClientDescription,"
                             + "ddsm_EndUse,"
                             + "ddsm_EquipmentDescription,"
                             + "ddsm_LMLightingorNon,"
                             + "ddsm_MeasureNotes,"
                             + "ddsm_Source,"
                             + "ddsm_Unit,"
                             + "ddsm_StudyMeasure,"

                             + "ddsm_MeasureType,"
                             + "ddsm_AnnualHoursBefore,"
                             + "ddsm_AnnualHoursAfter,"
                             + "ddsm_SummerDemandReductionKW,"
                             + "ddsm_WinterDemandReductionKW,"
                             + "ddsm_WattsBase,"
                             + "ddsm_WattsEff,"
                             + "ddsm_ClientPeakKWSavings,"
                             + "ddsm_MotorHP,"
                             + "ddsm_measuresubgroup2,"
                             + "ddsm_SavingsperUnitCCF,"
                             + "ddsm_ProgramCycle,"

                             + "ddsm_SizeRanges,"
                             + "ddsm_Sector,"
                             + "ddsm_BaselineTechnology,"
                             + "ddsm_EfficiencyTechnology,"
                             + "ddsm_MeasureCategory,"
                             + "ddsm_MeasureType,"
                             + "ddsm_MarketType,"
                             + "ddsm_Measurelift,"
                             + "ddsm_ExplainRestrictionsLimitations,"
                             + "ddsm_Equipmentsize,"
                             + "ddsm_EquipmentsizeType,"

                             + "ddsm_MeasureLifeDemo,"
                             + "ddsm_M3Savings,"
                             + "ddsm_SqFt,"
                             + "ddsm_RateClass,"
                             + "ddsm_InsulationQuality,"
                             + "ddsm_BaseEqWatt,"
                             + "ddsm_kWhsavings,"
                             + "ddsm_Persistence,"
                             + "ddsm_Attribution,"
                             + "ddsm_Freerider,"
                             + "ddsm_Spillover,"
                             + "ddsm_IncrementalCost,"
                             + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_name,"
                             + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateSavings,"
                             + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateIncentive,"
                             + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_PerUnitLock,"
                             + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_SavingsLock,"
                             + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentiveRatesLock,"
                             + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentivesLock"//+ "&$orderby=ddsm_projTplToMilestoneTpl/ddsm_Index asc"
                             */
                        + "$expand=ddsm_ddsm_measurecalculationtemplate_ddsm_measur"
                        + "&$filter=ddsm_measuretemplateId eq guid'" + measTpl_ID + "' and statecode/Value eq 0", false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.send();
                    if (req.readyState == 4 && req.status == 200) {
                        //From the project template - will also contain milestone information
                        var _tplData = JSON.parse(req.responseText).d;
                        if (_tplData.results[0] != null) {


                            //console.dir(_tplData.results[0]);

                            var irpu = parseFloat(_tplData.results[0].ddsm_IncRateUnit.Value);
                            var irkw = parseFloat(_tplData.results[0].ddsm_IncRateKW.Value);
                            var irkwh = parseFloat(_tplData.results[0].ddsm_IncRatekWh.Value);
                            var irthm = parseFloat(_tplData.results[0].ddsm_IncRatethm.Value);
                            var incCost = parseFloat(_tplData.results[0].ddsm_IncrementalCost.Value);
                            var skw = parseFloat(_tplData.results[0].ddsm_SavingsperUnitKW);
                            var skwh = parseFloat(_tplData.results[0].ddsm_SavingsperUnitkWh);
                            var sthm = parseFloat(_tplData.results[0].ddsm_SavingsperUnitthm);
                            var sccf = parseFloat(_tplData.results[0].ddsm_SavingsperUnitCCF);

                            var MeasureCode = _tplData.results[0].ddsm_MeasureCode;
                            var MeasureNumber = _tplData.results[0].ddsm_MeasureNumber;
                            var EnergyType = _tplData.results[0].ddsm_EnergyType.Value;
                            var Category = _tplData.results[0].ddsm_Category;
                            var MeasureGroup = _tplData.results[0].ddsm_MeasureGroup;
                            var MeasureSubgroup = _tplData.results[0].ddsm_MeasureSubgroup;
                            var Size = parseFloat(_tplData.results[0].ddsm_Size);
                            var EUL = parseFloat(_tplData.results[0].ddsm_EUL);
                            var BaseEquipDescr = _tplData.results[0].ddsm_BaseEquipDescr;
                            var EffEquipDescr = _tplData.results[0].ddsm_EffEquipDescr;
                            var ClientDesc = _tplData.results[0].ddsm_ClientDescription;
                            var EndUse = _tplData.results[0].ddsm_EndUse;
                            var EquipDesc = _tplData.results[0].ddsm_EquipmentDescription;
                            var LMLightNon = _tplData.results[0].ddsm_LMLightingorNon;
                            var MeasNotes = _tplData.results[0].ddsm_MeasureNotes;
                            var Source = _tplData.results[0].ddsm_Source;
                            var Unit = _tplData.results[0].ddsm_Unit;
                            var StudyMeas = _tplData.results[0].ddsm_StudyMeasure;

                            var measType = _tplData.results[0].ddsm_MeasureType.Value;
                            var annHoursBefore = _tplData.results[0].ddsm_AnnualHoursBefore;
                            var annHoursAfter = _tplData.results[0].ddsm_AnnualHoursAfter;
                            var sumDemReducKW = _tplData.results[0].ddsm_SummerDemandReductionKW;
                            var wintDemReducKW = _tplData.results[0].ddsm_WinterDemandReductionKW;
                            var wattsBase = _tplData.results[0].ddsm_WattsBase;
                            var wattsEff = _tplData.results[0].ddsm_WattsEff;
                            var clientPeakKWSave = _tplData.results[0].ddsm_ClientPeakKWSavings;

                            var motorHP = _tplData.results[0].ddsm_MotorHP;

                            var measureSubgroup2 = _tplData.results[0].ddsm_measuresubgroup2;
                            var measCalcTpl_Data = _tplData.results[0].ddsm_ddsm_measurecalculationtemplate_ddsm_measur;
                            var measCalcTpl_Name = measCalcTpl_Data.ddsm_name;
                            var measCalcTpl_CalcSavings = measCalcTpl_Data.ddsm_CalculateSavings;
                            var measCalcTpl_CalcIncentives = measCalcTpl_Data.ddsm_CalculateIncentive;
                            var measCalcTpl_PerUnitLock = measCalcTpl_Data.ddsm_PerUnitLock;
                            var measCalcTpl_SavingsLock = measCalcTpl_Data.ddsm_SavingsLock;
                            var measCalcTpl_IncentiveRatesLock = measCalcTpl_Data.ddsm_IncentiveRatesLock;
                            var measCalcTpl_IncentivesLock = measCalcTpl_Data.ddsm_IncentivesLock;
                            var measCalcTpl_CalcTypeValues = "";


                            if (_tplData.results[0].ddsm_MeasureCode != null) {
                                record.set("ddsm_MeasureCode", MeasureCode);
                                rowEditing.getEditor().getForm().findField('ddsm_MeasureCode').setValue(MeasureCode);
                            }

                            if (_tplData.results[0].ddsm_MeasureNumber != null) {
                                record.set("ddsm_MeasureNumber", MeasureNumber);
                                rowEditing.getEditor().getForm().findField('ddsm_MeasureNumber').setValue(MeasureNumber);
                            }

                            if (_tplData.results[0].ddsm_EnergyType.Value != null) {
                                record.set("ddsm_EnergyType", EnergyType.toString());
                                rowEditing.getEditor().getForm().findField('ddsm_EnergyType').setValue(EnergyType.toString());
                            }

                            if (_tplData.results[0].ddsm_Category != null) {
                                record.set("ddsm_Category", Category);
                                rowEditing.getEditor().getForm().findField('ddsm_Category').setValue(Category);
                            }

                            if (_tplData.results[0].ddsm_MeasureGroup != null) {
                                record.set("ddsm_MeasureGroup", MeasureGroup);
                                rowEditing.getEditor().getForm().findField('ddsm_MeasureGroup').setValue(MeasureGroup);
                            }

                            if (_tplData.results[0].ddsm_MeasureSubgroup != null) {
                                record.set("ddsm_MeasureSubgroup", MeasureSubgroup);
                                rowEditing.getEditor().getForm().findField('ddsm_MeasureSubgroup').setValue(MeasureSubgroup);
                            }

                            if (_tplData.results[0].ddsm_Size != null) {
                                record.set("ddsm_Size", Size);
                                rowEditing.getEditor().getForm().findField('ddsm_Size').setValue(Size);
                            }

                            if (_tplData.results[0].ddsm_EUL != null) {
                                record.set("ddsm_EUL", EUL);
                                rowEditing.getEditor().getForm().findField('ddsm_EUL').setValue(EUL);
                            }

                            if (_tplData.results[0].ddsm_BaseEquipDescr != null) {
                                record.set("ddsm_BaseEquipDescr", BaseEquipDescr);
                                rowEditing.getEditor().getForm().findField('ddsm_BaseEquipDescr').setValue(BaseEquipDescr);
                            }

                            if (_tplData.results[0].ddsm_EffEquipDescr != null) {
                                record.set("ddsm_EffEquipDescr", EffEquipDescr);
                                rowEditing.getEditor().getForm().findField('ddsm_EffEquipDescr').setValue(EffEquipDescr);
                            }

                            if (_tplData.results[0].ddsm_EndUse != null) {
                                record.set("ddsm_EndUse", EndUse);
                                rowEditing.getEditor().getForm().findField('ddsm_EndUse').setValue(EndUse);
                            }

                            if (_tplData.results[0].ddsm_EquipmentDescription != null) {
                                record.set("ddsm_EquipmentDescription", EquipDesc);
                                rowEditing.getEditor().getForm().findField('ddsm_EquipmentDescription').setValue(EquipDesc);
                            }


                            if (_tplData.results[0].ddsm_MeasureNotes != null) {
                                record.set("ddsm_MeasureNotes", MeasNotes);
                                rowEditing.getEditor().getForm().findField('ddsm_MeasureNotes').setValue(MeasNotes);
                            }

                            if (_tplData.results[0].ddsm_Source != null) {
                                record.set("ddsm_Source", Source);
                                rowEditing.getEditor().getForm().findField('ddsm_Source').setValue(Source);
                            }

                            if (_tplData.results[0].ddsm_Unit != null) {
                                record.set("ddsm_Unit", Unit);
                                rowEditing.getEditor().getForm().findField('ddsm_Unit').setValue(Unit);
                            }

                            if (_tplData.results[0].ddsm_MeasureType.Value != null) {
                                record.set("ddsm_MeasureType", measType.toString());
                                rowEditing.getEditor().getForm().findField('ddsm_MeasureType').setValue(measType.toString());
                            }

                            if (_tplData.results[0].ddsm_AnnualHoursBefore != null) {
                                record.set("ddsm_AnnualHoursBefore", parseFloat(annHoursBefore));
                                rowEditing.getEditor().getForm().findField('ddsm_AnnualHoursBefore').setValue(annHoursBefore);
                            }

                            if (_tplData.results[0].ddsm_AnnualHoursAfter != null) {
                                record.set("ddsm_AnnualHoursAfter", parseFloat(annHoursAfter));
                                rowEditing.getEditor().getForm().findField('ddsm_AnnualHoursAfter').setValue(annHoursAfter);
                            }
                            /*
                             if (_tplData.results[0].ddsm_SummerDemandReductionKW != null) {
                             record.set("ddsm_SummerDemandReductionKW", parseFloat(sumDemReducKW));
                             rowEditing.getEditor().getForm().findField('ddsm_SummerDemandReductionKW').setValue(sumDemReducKW);
                             }
                             */
                            if (_tplData.results[0].ddsm_WinterDemandReductionKW != null) {
                                record.set("ddsm_WinterDemandReductionKW", parseFloat(wintDemReducKW));
                                rowEditing.getEditor().getForm().findField('ddsm_WinterDemandReductionKW').setValue(parseFloat(wintDemReducKW));
                            }

                            if (_tplData.results[0].ddsm_WattsBase != null) {
                                record.set("ddsm_WattsBase", parseFloat(wattsBase));
                                rowEditing.getEditor().getForm().findField('ddsm_WattsBase').setValue(parseFloat(wattsBase));
                            }

                            if (_tplData.results[0].ddsm_WattsEff != null) {
                                record.set("ddsm_WattsEff", parseFloat(wattsEff));
                                rowEditing.getEditor().getForm().findField('ddsm_WattsEff').setValue(parseFloat(wattsEff));
                            }


                            if (_tplData.results[0].ddsm_MotorHP != null) {
                                record.set("ddsm_MotorHP", parseInt(motorHP, 10));
                                rowEditing.getEditor().getForm().findField('ddsm_MotorHP').setValue(parseInt(motorHP, 10));
                            }

                            if (_tplData.results[0].ddsm_measuresubgroup2 != null) {
                                record.set("ddsm_measuresubgroup2", measureSubgroup2);
                                rowEditing.getEditor().getForm().findField('ddsm_measuresubgroup2').setValue(measureSubgroup2);
                            }

                            //if (_tplData.results[0].ddsm_ProgramCycle.Id != null) {
                            //    rowEditing.getEditor().getForm().findField('ddsm_ProgramCycleId').setValue(_tplData.results[0].ddsm_ProgramCycle.Name);
                            //    rowEditing.getEditor().getForm().findField('ddsm_ProgramCycleId' + '__Id').setValue((_tplData.results[0].ddsm_ProgramCycle.Id).replace(/\{|\}/g, ''));
                            //    rowEditing.getEditor().getForm().findField('ddsm_ProgramCycleId' + '__LogicalName').setValue(_tplData.results[0].ddsm_ProgramCycle.LogicalName);
                            //    record.set('ddsm_ProgramCycleId', _tplData.results[0].ddsm_ProgramCycle.Name);
                            //    record.set('ddsm_ProgramCycleId' + '__Id', (_tplData.results[0].ddsm_ProgramCycle.Id).replace(/\{|\}/g, ''));
                            //    record.set('ddsm_ProgramCycleId' + '__LogicalName', _tplData.results[0].ddsm_ProgramCycle.LogicalName);
                            //}

                            rowEditing.getEditor().getForm().findField('ddsm_Measure_ID').setValue(_tplData.results[0].ddsm_MeasureID);
                            record.set('ddsm_Measure_ID', _tplData.results[0].ddsm_MeasureID);

                            if (!!_tplData.results[0].ddsm_measurecalculationtype && !!_tplData.results[0].ddsm_measurecalculationtype.Id) {
                                rowEditing.getEditor().getForm().findField('ddsm_CalculationType').setValue(_tplData.results[0].ddsm_measurecalculationtype.Name);
                                rowEditing.getEditor().getForm().findField('ddsm_CalculationType' + '__Id').setValue((_tplData.results[0].ddsm_measurecalculationtype.Id).replace(/\{|\}/g, ''));
                                rowEditing.getEditor().getForm().findField('ddsm_CalculationType' + '__LogicalName').setValue(_tplData.results[0].ddsm_measurecalculationtype.LogicalName);
                                record.set('ddsm_CalculationType', _tplData.results[0].ddsm_measurecalculationtype.Name);
                                record.set('ddsm_CalculationType' + '__Id', (_tplData.results[0].ddsm_measurecalculationtype.Id).replace(/\{|\}/g, ''));
                                record.set('ddsm_CalculationType' + '__LogicalName', _tplData.results[0].ddsm_measurecalculationtype.LogicalName);
                            }

                            if (_tplData.results[0].ddsm_FundingSourceCalculationType.Id != null) {
                                rowEditing.getEditor().getForm().findField('ddsm_FundingSourceCalculationType').setValue(_tplData.results[0].ddsm_FundingSourceCalculationType.Name);
                                rowEditing.getEditor().getForm().findField('ddsm_FundingSourceCalculationType' + '__Id').setValue((_tplData.results[0].ddsm_FundingSourceCalculationType.Id).replace(/\{|\}/g, ''));
                                rowEditing.getEditor().getForm().findField('ddsm_FundingSourceCalculationType' + '__LogicalName').setValue(_tplData.results[0].ddsm_FundingSourceCalculationType.LogicalName);
                                record.set('ddsm_FundingSourceCalculationType', _tplData.results[0].ddsm_FundingSourceCalculationType.Name);
                                record.set('ddsm_FundingSourceCalculationType' + '__Id', (_tplData.results[0].ddsm_FundingSourceCalculationType.Id).replace(/\{|\}/g, ''));
                                record.set('ddsm_FundingSourceCalculationType' + '__LogicalName', _tplData.results[0].ddsm_FundingSourceCalculationType.LogicalName);
                            }
                            /*
                             rowEditing.getEditor().getForm().findField('ddsm_GJSavingsatMeterperunit').setValue(_tplData.results[0].ddsm_GJSavingsatMeterperunit);
                             record.set('ddsm_GJSavingsatMeterperunit', _tplData.results[0].ddsm_GJSavingsatMeterperunit);

                             rowEditing.getEditor().getForm().findField('ddsm_NettoGrossRatio').setValue(_tplData.results[0].ddsm_NettoGrossRatio);
                             record.set('ddsm_NettoGrossRatio', _tplData.results[0].ddsm_NettoGrossRatio);

                             rowEditing.getEditor().getForm().findField('ddsm_IncentiveCostMeasurePerUnit').setValue(_tplData.results[0].ddsm_IncentiveCostMeasurePerUnit.Value);
                             record.set('ddsm_IncentiveCostMeasurePerUnit', _tplData.results[0].ddsm_IncentiveCostMeasurePerUnit.Value);
                             */
                            if (_tplData.results[0].ddsm_MeasureModelingType.Value != null) {
                                record.set("ddsm_MeasureModelingType", (_tplData.results[0].ddsm_MeasureModelingType.Value).toString());
                                rowEditing.getEditor().getForm().findField('ddsm_MeasureModelingType').setValue((_tplData.results[0].ddsm_MeasureModelingType.Value).toString());
                            }
                            if (_tplData.results[0].ddsm_MeasureRelevance.Value != null) {
                                record.set("ddsm_MeasureRelevance", (_tplData.results[0].ddsm_MeasureRelevance.Value).toString());
                                rowEditing.getEditor().getForm().findField('ddsm_MeasureRelevance').setValue((_tplData.results[0].ddsm_MeasureRelevance.Value).toString());
                            }

                            rowEditing.getEditor().getForm().findField('ddsm_IncentivePaymentGrossLevel1PerUnit').setValue(_tplData.results[0].ddsm_IncentivePaymentGrossLevel1PerUnit.Value);
                            record.set('ddsm_IncentivePaymentGrossLevel1PerUnit', _tplData.results[0].ddsm_IncentivePaymentGrossLevel1PerUnit.Value);

                            rowEditing.getEditor().getForm().findField('ddsm_IncentivePaymentGrossLevel2PerUnit').setValue(_tplData.results[0].ddsm_IncentivePaymentGrossLevel2PerUnit.Value);
                            record.set('ddsm_IncentivePaymentGrossLevel2PerUnit', _tplData.results[0].ddsm_IncentivePaymentGrossLevel2PerUnit.Value);

                            rowEditing.getEditor().getForm().findField('ddsm_IncentivePaymentGrossLevel3PerUnit').setValue(_tplData.results[0].ddsm_IncentivePaymentGrossLevel3PerUnit.Value);
                            record.set('ddsm_IncentivePaymentGrossLevel3PerUnit', _tplData.results[0].ddsm_IncentivePaymentGrossLevel3PerUnit.Value);
                            //------------------------------------------------------------------//

                            if (_tplData.results[0].ddsm_IncRateUnit.Value != null) {
                                record.set("ddsm_IncRateUnit", irpu);
                                rowEditing.getEditor().getForm().findField('ddsm_IncRateUnit').setValue(irpu);
                            } else {
                                record.set("ddsm_IncRateUnit", 0);
                                rowEditing.getEditor().getForm().findField('ddsm_IncRateUnit').setValue(0);
                            }
                            if (_tplData.results[0].ddsm_IncRateKW.Value != null) {
                                record.set("ddsm_IncRateKW", irkw);
                                rowEditing.getEditor().getForm().findField('ddsm_IncRateKW').setValue(irkw);
                            } else {
                                record.set("ddsm_IncRateKW", 0);
                                rowEditing.getEditor().getForm().findField('ddsm_IncRateKW').setValue(0);
                            }
                            if (_tplData.results[0].ddsm_IncRatekWh.Value != null) {
                                record.set("ddsm_IncRatekWh", irkwh);
                                rowEditing.getEditor().getForm().findField('ddsm_IncRatekWh').setValue(irkwh);
                            } else {
                                record.set("ddsm_IncRatekWh", 0);
                                rowEditing.getEditor().getForm().findField('ddsm_IncRatekWh').setValue(0);
                            }
                            if (_tplData.results[0].ddsm_IncRatethm.Value != null) {
                                record.set("ddsm_IncRatethm", irthm);
                                rowEditing.getEditor().getForm().findField('ddsm_IncRatethm').setValue(irthm);
                            } else {
                                record.set("ddsm_IncRatethm", 0);
                                rowEditing.getEditor().getForm().findField('ddsm_IncRatethm').setValue(0);
                            }
                            if (_tplData.results[0].ddsm_IncrementalCost.Value != null) {
                                record.set("ddsm_IncrementalCost", incCost);
                                rowEditing.getEditor().getForm().findField('ddsm_IncrementalCost').setValue(incCost);
                            } else {
                                record.set("ddsm_IncrementalCost", 0);
                                rowEditing.getEditor().getForm().findField('ddsm_IncrementalCost').setValue(0);
                            }

                            //------------------------------------------------------------------//

                            //copies savings
                            //Copy to Phase 1
                            if (_tplData.results[0].ddsm_SavingsperUnitKW != null) {
                                record.set("ddsm_kWGrossSavingsatMeterperunit", skw);
                                rowEditing.getEditor().getForm().findField('ddsm_kWGrossSavingsatMeterperunit').setValue(skw);
                            } else {
                                record.set("ddsm_kWGrossSavingsatMeterperunit", 0);
                                rowEditing.getEditor().getForm().findField('ddsm_kWGrossSavingsatMeterperunit').setValue(0);
                            }
                            if (_tplData.results[0].ddsm_SavingsperUnitkWh != null) {
                                record.set("ddsm_kWhGrossSavingsatMeterperunit", skwh);
                                rowEditing.getEditor().getForm().findField('ddsm_kWhGrossSavingsatMeterperunit').setValue(skwh);
                            } else {
                                record.set("ddsm_kWhGrossSavingsatMeterperunit", 0);
                                rowEditing.getEditor().getForm().findField('ddsm_kWhGrossSavingsatMeterperunit').setValue(0);
                            }
                            //if (_tplData.results[0].ddsm_SavingsperUnitthm != null) {
                            //    record.set("ddsm_phase1perunitthm", sthm);
                            //    rowEditing.getEditor().getForm().findField('ddsm_phase1perunitthm').setValue(sthm);
                            //} else {
                            //    record.set("ddsm_phase1perunitthm", 0);
                            //    rowEditing.getEditor().getForm().findField('ddsm_phase1perunitthm').setValue(0);
                            //}

                            if (_tplData.results[0].ddsm_SavingsperUnitCCF != null) {
                                record.set("ddsm_Phase1PerUnitCCF", sccf);
                                rowEditing.getEditor().getForm().findField('ddsm_Phase1PerUnitCCF').setValue(sccf);
                            } else {
                                record.set("ddsm_Phase1PerUnitCCF", 0);
                                rowEditing.getEditor().getForm().findField('ddsm_Phase1PerUnitCCF').setValue(0);
                            }

                            if (_tplData.results[0].ddsm_IncRateUnit.Value != null) {
                                record.set("ddsm_IncentivePaymentGross", irpu);
                                rowEditing.getEditor().getForm().findField('ddsm_IncentivePaymentGross').setValue(irpu);
                            } else {
                                record.set("ddsm_IncentivePaymentGross", 0);
                                rowEditing.getEditor().getForm().findField('ddsm_IncentivePaymentGross').setValue(0);
                            }


                            //Measure Calc Template Data
                            if (measCalcTpl_CalcSavings == true) {
                                measCalcTpl_CalcTypeValues += "Calculate Savings; ";
                            }
                            if (measCalcTpl_CalcIncentives == true) {
                                measCalcTpl_CalcTypeValues += "Calculate Incentives; ";
                            }
                            if (measCalcTpl_PerUnitLock == true) {
                                measCalcTpl_CalcTypeValues += "Lock Per Units; ";
                            }
                            if (measCalcTpl_SavingsLock == true) {
                                measCalcTpl_CalcTypeValues += "Lock Savings; ";
                            }
                            if (measCalcTpl_IncentiveRatesLock == true) {
                                measCalcTpl_CalcTypeValues += "Lock Incentive Rates; ";
                            }
                            if (measCalcTpl_IncentivesLock == true) {
                                measCalcTpl_CalcTypeValues += "Lock Incentives; ";
                            }

                            if (measCalcTpl_CalcTypeValues != null) {
                                record.set("ddsm_CalculationTypeValues", measCalcTpl_CalcTypeValues);
                                rowEditing.getEditor().getForm().findField('ddsm_CalculationTypeValues').setValue(measCalcTpl_CalcTypeValues);
                            }
                        }
                    }
                }
                catch (err) {
                    //console.dir(err);
                    Ext.Msg.alert("Failed to load Measure Template & Measure Calculation Template", err);
                }

            }

            function onLoadMeas(record) {
                try {

                    var calcType = record.get("ddsm_CalculationType");
                    if (calcType == null && record.get("ddsm_MeasureSelector") != null) {
                        var req = new XMLHttpRequest();
                        var measTpl_ID = record.get("ddsm_MeasureSelector" + '__Id');
                        //console.log("Function onLoadMeas()... The measure selector id is: " + measTpl_ID);
                        var serverUrl = b.Xrm.Page.context.getClientUrl();

                        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_measuretemplateSet?"
                            + "$select="
                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_name,"
                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateSavings,"
                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateIncentive,"
                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_PerUnitLock,"
                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_SavingsLock,"
                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentiveRatesLock,"
                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentivesLock"
                            + "&$expand=ddsm_ddsm_measurecalculationtemplate_ddsm_measur"
                            + "&$filter=ddsm_measuretemplateId eq guid'" + measTpl_ID + "' and statecode/Value eq 0", false);
                        req.setRequestHeader("Accept", "application/json");
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.send();
                        if (req.readyState == 4 && req.status == 200) {
                            var _tplData = JSON.parse(req.responseText).d;
                            if (_tplData.results[0] != null) {
                                //console.dir(_tplData.results);
                                var measCalcType = _tplData.results[0].ddsm_ddsm_measurecalculationtemplate_ddsm_measur;
                                //console.dir(measCalcType);

                                if (measCalcType != null) {
                                    var calcTypeValues = "";

                                    if (measCalcType.ddsm_CalculateSavings == true) {
                                        calcTypeValues += "Calculate Savings; ";
                                    }
                                    if (measCalcType.ddsm_CalculateIncentive == true) {
                                        calcTypeValues += "Calculate Incentives; ";
                                    }
                                    if (measCalcType.ddsm_PerUnitLock == true) {
                                        calcTypeValues += "Lock Per Units; ";
                                    }
                                    if (measCalcType.ddsm_SavingsLock == true) {
                                        calcTypeValues += "Lock Savings; ";
                                    }
                                    if (measCalcType.ddsm_IncentiveRatesLock == true) {
                                        calcTypeValues += "Lock Incentive Rates; ";
                                    }
                                    if (measCalcType.ddsm_IncentivesLock == true) {
                                        calcTypeValues += "Lock Incentives; ";
                                    }
                                    record.set("ddsm_CalculationType", measCalcType.ddsm_name);
                                    rowEditing.getEditor().getForm().findField('ddsm_CalculationType').setValue(measCalcType.ddsm_name);
                                    record.set("ddsm_CalculationTypeValues", measCalcType.ddsm_name);
                                    rowEditing.getEditor().getForm().findField('ddsm_CalculationTypeValues').setValue(calcTypeValues);

                                    //console.log('Function onLoadMeas()... The calculation type values are: ' + record.get("ddsm_CalculationTypeValues"));
                                }

                            }
                        }
                    }
                }
                catch (err) {
                    Ext.Msg.alert("Error", "Error: " + err + ".");
                }
            }

            function measCalc_Offered(units, record, f) {
                //console.log("measCalc_Offered starting: " + f);
                record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                var CalculationType = record.get("ddsm_CalculationType");
                if(CalculationType.toLowerCase() == "esp") return;
                var calcTypeValues = record.get("ddsm_CalculationTypeValues");
                var calcSavings = false;
                var calcIncentives = false;

                if (calcTypeValues != null) {
                    if ((calcTypeValues.indexOf("Calculate Savings")) != -1) {
                        calcSavings = true;
                    }
                    if ((calcTypeValues.indexOf("Calculate Incentives")) != -1) {
                        calcIncentives = true;
                    }
                }
                if (units == null) {
                    units = 0;
                    record.set("ddsm_Units", 0);
                    rowEditing.getEditor().getForm().findField('ddsm_Units').setValue(0);
                }
                //Calculates savings if calculation type is set to do so.
                if (calcSavings) {
                    var perUnitkWh = record.get("ddsm_kWhGrossSavingsatMeterperunit");
                    var perUnitKW = record.get("ddsm_kWGrossSavingsatMeterperunit");
                    //var perUnitthm = record.get("ddsm_Savingsthm");
                    //Comment because field status "ddsm_phase1savingsthm" = deleted in E1 Schema
                    var perUnitthm = record.get("ddsm_M3SavingsperUnit");
                    /*Demo Enbr*/
                    var perUnitwater = record.get("ddsm_WaterSavingsperUnit");
                    var perUnitgj = record.get("ddsm_GJSavingsatMeterperunit");
                    /*Demo Enbr*/
                    var perUnitCCF = record.get("ddsm_Phase1PerUnitCCF");

                    if (perUnitkWh == null) {
                        perUnitkWh = 0;
                    }
                    if (perUnitKW == null) {
                        perUnitKW = 0;
                    }
                    if (perUnitthm == null) {
                        perUnitthm = 0;
                    }
                    /*Demo Enbr*/
                    if (perUnitwater == null) {
                        perUnitwater = 0;
                    }
                    if (perUnitgj == null) {
                        perUnitgj = 0;
                    }
                    /*Demo Enbr*/
                    if (perUnitCCF == null) {
                        perUnitCCF = 0;
                    }
                    var savingskWh;
                    var savingsKW;
                    var savingsthm;
                    /*Demo Enbr*/
                    var savingswater;
                    var savingsgj;

                    //Savings calculations
                    savingskWh = (units * perUnitkWh);
                    savingsKW = (units * perUnitKW);
                    savingsthm = (units * perUnitthm);
                    /*Demo Enbr*/
                    savingswater = (units * perUnitwater);
                    savingsgj = (units * perUnitgj);

                    //Sets fields to savings values
                    record.set("ddsm_kWhGrossSavingsatMeter", savingskWh);
                    rowEditing.getEditor().getForm().findField('ddsm_kWhGrossSavingsatMeter').setValue(savingskWh);
                    record.set("ddsm_kWGrossSavingsatMeter", savingsKW);
                    rowEditing.getEditor().getForm().findField('ddsm_kWGrossSavingsatMeter').setValue(savingsKW);
                    record.set("ddsm_M3Savings", savingsthm);
                    rowEditing.getEditor().getForm().findField('ddsm_M3Savings').setValue(savingsthm);
                    /*Demo Enbr*/
                    record.set("ddsm_WaterSavings", savingswater);
                    rowEditing.getEditor().getForm().findField('ddsm_WaterSavings').setValue(savingswater);
                    record.set("ddsm_GJSavingsatMeter", savingsgj);
                    rowEditing.getEditor().getForm().findField('ddsm_GJSavingsatMeter').setValue(savingsgj);
                    /*Demo Enbr*/

                }

                //Calculates incentives if calculation type is set to do so.
                if (calcIncentives) {
                    var incRateUnits = record.get("ddsm_IncentivePaymentGrossPerUnit");
                    var incRatekWh = record.get("ddsm_IncRatekWh");
                    var incRateKW = record.get("ddsm_IncRateKW");
                    var incRatethm = record.get("ddsm_IncRatethm");

                    if (incRateUnits == null) {
                        incRateUnits = 0;
                    }
                    if (incRatekWh == null) {
                        incRatekWh = 0;
                    }
                    if (incRateKW == null) {
                        incRateKW = 0;
                    }
                    if (incRatethm == null) {
                        incRatethm = 0;
                    }
                    var savingskWh = record.get("ddsm_kWhGrossSavingsatMeter");
                    var savingsKW = record.get("ddsm_kWGrossSavingsatMeter");

                    //Comment because field status "ddsm_phase1savingsthm" = deleted in E1 Schema
                    var savingsthm = record.get("ddsm_M3Savings");

                    if (savingskWh == null) {
                        savingskWh = 0;
                    }
                    if (savingsKW == null) {
                        savingsKW = 0;
                    }
                    if (savingsthm == null) {
                        savingsthm = 0;
                    }
                    var incUnits;
                    var inckWh;
                    var incKW;
                    var incthm;
                    var incElectric;
                    var incGas;
                    var incTotal;

                    //Incentive calculations
                    incUnits = (units * incRateUnits);
                    inckWh = (savingskWh * incRatekWh);
                    incKW = (savingsKW * incRateKW);
                    incthm = (savingsthm * incRatethm);
                    incElectric = (inckWh + incKW);
                    incGas = incthm;
                    incTotal = (incElectric + incGas + incUnits);

                    //Sets fields to incentive values
                    record.set("ddsm_IncentivePaymentGross", incUnits);
                    rowEditing.getEditor().getForm().findField('ddsm_IncentivePaymentGross').setValue(incUnits);

                    record.set("ddsm_IncentivePaymentElectricGross", incElectric);
                    rowEditing.getEditor().getForm().findField('ddsm_IncentivePaymentElectricGross').setValue(incElectric);

                    record.set("ddsm_IncentivePaymentGasGross", incGas);
                    rowEditing.getEditor().getForm().findField('ddsm_IncentivePaymentGasGross').setValue(incGas);

                    record.set("ddsm_IncentivePaymentTotalGross", incTotal);
                    rowEditing.getEditor().getForm().findField('ddsm_IncentivePaymentTotalGross').setValue(incTotal);
                }

                var incTotal = record.get("ddsm_IncentivePaymentElectricGross") + record.get("ddsm_IncentivePaymentGasGross") + record.get("ddsm_IncentivePaymentGross");
                record.set("ddsm_IncentivePaymentTotalGross", incTotal);
                rowEditing.getEditor().getForm().findField('ddsm_IncentivePaymentTotalGross').setValue(incTotal);

                //Sends phase values to current fields


                var IncentivePaymentNet = record.get("ddsm_IncentivePaymentNet");
                if (IncentivePaymentNet == null) {
                    IncentivePaymentNet = 0;
                }
                var IncentivePaymentGross = record.get("ddsm_IncentivePaymentGross");
                if (IncentivePaymentGross == null) {
                    IncentivePaymentGross = 0;
                }
                var OEMRecoveryCost = record.get("ddsm_OEMRecoveryCost");
                if (OEMRecoveryCost == null) {
                    OEMRecoveryCost = 0;
                }

                IncentivePaymentNet= IncentivePaymentGross - OEMRecoveryCost;
                if(IncentivePaymentNet < 0) IncentivePaymentNet = 0;
                record.set("ddsm_IncentivePaymentNet", IncentivePaymentNet);
                rowEditing.getEditor().getForm().findField('ddsm_IncentivePaymentNet').setValue(IncentivePaymentNet);


                var phase1units = units;
                var phase1savingskWh = record.get("ddsm_kWhGrossSavingsatMeter");
                var phase1savingsKW = record.get("ddsm_kWGrossSavingsatMeter");

                var phase1savingsthm = record.get("ddsm_M3Savings");
                //Comment because field status "ddsm_phase1savingsthm" = deleted in E1 Schema
                //var phase1savingsthm = record.get("ddsm_phase1savingsthm");
                var phase1incTotal = record.get("ddsm_IncentivePaymentTotalGross");

                var eul = record.get("ddsm_EUL");
                var lifecycleKW = 0.0;
                var lifecyclekWh = 0.0;
                var lifecyclethm = 0.0;

                if (phase1savingskWh == null) {
                    phase1savingskWh = 0;
                }
                if (phase1savingsKW == null) {
                    phase1savingsKW = 0;
                }
                if (phase1savingsthm == null) {
                    phase1savingsthm = 0;
                }
                if (eul == null) {
                    eul = 0;
                }
                lifecycleKW = (phase1savingsKW * eul);
                lifecyclekWh = (phase1savingskWh * eul);
                lifecyclethm = (phase1savingsthm * eul);
                /*
                 record.set("ddsm_CurrentUnits", phase1units);
                 rowEditing.getEditor().getForm().findField('ddsm_Units').setValue(phase1units);

                 record.set("ddsm_CurrentSavingskWh", phase1savingskWh);
                 rowEditing.getEditor().getForm().findField('ddsm_CurrentSavingskWh').setValue(phase1savingskWh);

                 record.set("ddsm_currentsavingskw", phase1savingsKW);
                 rowEditing.getEditor().getForm().findField('ddsm_currentsavingskw').setValue(phase1savingsKW);

                 record.set("ddsm_CurrentSavingsthm", phase1savingsthm);
                 rowEditing.getEditor().getForm().findField('ddsm_CurrentSavingsthm').setValue(phase1savingsthm);
                 */


                record.set("ddsm_lifecyclesavingskw", lifecycleKW);
                //rowEditing.getEditor().getForm().findField('ddsm_lifecyclesavingskw').setValue(lifecycleKW);

                record.set("ddsm_lifecyclesavingskwh", lifecyclekWh);
                //rowEditing.getEditor().getForm().findField('ddsm_lifecyclesavingskwh').setValue(lifecyclekWh);

                record.set("ddsm_lifecyclesavingsthm", lifecyclethm);
                //rowEditing.getEditor().getForm().findField('ddsm_lifecyclesavingsthm').setValue(lifecyclethm);

                record.commit();
            }

            function calcCostBreakdown() {
                //console.log("calcCostBreakdown");
                var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                var costTotal = 0, costEquipment = 0, costLabor = 0, costOther = 0, costIncremental = 0;
                if (typeof rowEditing.getEditor().getForm().findField('ddsm_CostEquipment') !== 'undefined') {
                    if (rowEditing.getEditor().getForm().findField('ddsm_CostEquipment').getValue() == null) {
                        costEquipment = 0;
                    } else {
                        costEquipment = parseFloat(rowEditing.getEditor().getForm().findField('ddsm_CostEquipment').getValue());
                    }
                } else {
                    if (record.get('ddsm_CostEquipment') == null) {
                        costEquipment = 0;
                    } else {
                        costEquipment = parseFloat(record.get('ddsm_CostEquipment'));
                    }
                }
                if (typeof rowEditing.getEditor().getForm().findField('ddsm_CostLabor') !== 'undefined') {
                    if (rowEditing.getEditor().getForm().findField('ddsm_CostLabor').getValue() == null) {
                        costLabor = 0;
                    } else {
                        costLabor = parseFloat(rowEditing.getEditor().getForm().findField('ddsm_CostLabor').getValue());
                    }
                } else {
                    if (record.get('ddsm_CostLabor') == null) {
                        costLabor = 0;
                    } else {
                        costLabor = parseFloat(record.get('ddsm_CostLabor'));
                    }
                }
                if (typeof rowEditing.getEditor().getForm().findField('ddsm_CostOther') !== 'undefined') {
                    if (rowEditing.getEditor().getForm().findField('ddsm_CostOther').getValue() == null) {
                        costOther = 0;
                    } else {
                        costOther = parseFloat(rowEditing.getEditor().getForm().findField('ddsm_CostOther').getValue());
                    }
                } else {
                    if (record.get('ddsm_CostOther') == null) {
                        costOther = 0;
                    } else {
                        costOther = parseFloat(record.get('ddsm_CostOther'));
                    }
                }
                /*
                 if (typeof rowEditing.getEditor().getForm().findField('ddsm_IncrementalCost') !== 'undefined') {
                 if (rowEditing.getEditor().getForm().findField('ddsm_IncrementalCost').getValue() == null) {
                 costIncremental = 0;
                 } else {
                 costIncremental = parseFloat(rowEditing.getEditor().getForm().findField('ddsm_IncrementalCost').getValue());
                 }
                 } else {
                 if (record.get('ddsm_IncrementalCost') == null) {
                 costIncremental = 0;
                 } else {
                 costIncremental = parseFloat(record.get('ddsm_IncrementalCost'));
                 }
                 }
                 */
                costTotal = costEquipment + costLabor + costOther + costIncremental;
                record.set("ddsm_CostTotal", costTotal);
                rowEditing.getEditor().getForm().findField('ddsm_CostTotal').setValue(costTotal);
            }

            function calcCostBreakdownAll() {
                //console.log("calcCostBreakdown");

                var costTotal = 0, costEquipment = 0, costLabor = 0, costOther = 0, costIncremental = 0, costInstallation = 0, costPromotion = 0, costEvaluation = 0;
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange(),
                    recordsCount = recordsGrid.length;
                for (var i = 0; i < recordsCount; i++) {
                    var record = recordsGrid[i];

                    if (record.get('ddsm_CostEquipment') != null) {
                        costEquipment += parseFloat(record.get('ddsm_CostEquipment'));
                    }
                    if (record.get('ddsm_CostLabor') != null) {
                        costLabor += parseFloat(record.get('ddsm_CostLabor'));
                    }
                    if (record.get('ddsm_CostOther') != null) {
                        costOther += parseFloat(record.get('ddsm_CostOther'));
                    }
                    /*
                     if (record.get('ddsm_IncrementalCost') != null) {
                     costIncremental += parseFloat(record.get('ddsm_IncrementalCost'));
                     }
                     */
                }
                b.Xrm.Page.getAttribute("ddsm_projectmaterialscost").setValue(costEquipment);
                b.Xrm.Page.getAttribute("ddsm_projectlaborcost").setValue(costLabor);
                b.Xrm.Page.getAttribute("ddsm_projectothercost").setValue(costOther);

                //b.Xrm.Page.getAttribute("ddsm_projectincrementalcost").setValue(costIncremental);

                costTotal = costEquipment + costLabor + costOther;
                b.Xrm.Page.getAttribute("ddsm_totalprojectcost").setValue(costTotal);
            }

            // Update project status measure
            updateProjectStatus = function () {
                var ddsm_projectstatus = b.Xrm.Page.getAttribute("ddsm_projectstatus").getValue();
                if (ddsm_projectstatus != null) {
                    var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                    var recordsCount = recordsGrid.length;
                    if (recordsCount > 0) {
                        for (var i = 0; i < recordsCount; i++) {
                            var record = recordsGrid[i];

                            var guidId = record.data[entitySchemaName + 'Id'];
                            var changes = {};
                            changes['ddsm_ProjectStatus'] = ddsm_projectstatus;
                            record.data['ddsm_ProjectStatus'] = ddsm_projectstatus;

                            var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";

                            //console.log(URI);
                            var req = new XMLHttpRequest();
                            req.open("POST", URI, true);
                            req.setRequestHeader("Accept", "application/json");
                            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                            req.setRequestHeader("X-HTTP-Method", "MERGE");
                            req.onreadystatechange = function () {
                                if (this.readyState == 4) {
                                    req.onreadystatechange = null;
                                    if (this.status == 204) {
                                    }
                                }
                            };
                            req.send(JSON.stringify(changes));
                        }
                        grid.getStore().commitChanges();
                        Ext.getCmp(idGrid).getView().refresh();
                    }
                }
            }
            // Disable/Enable editor grid row cell
            function disableEditorCell(editor, status) {
                if (status) {
                    // Phase 1
                    editor.getEditor().child('[name="ddsm_Units"]').disable();
                    editor.getEditor().child('[name="ddsm_kWhGrossSavingsatMeter"]').disable();
                    editor.getEditor().child('[name="ddsm_kWGrossSavingsatMeter"]').disable();
                    editor.getEditor().child('[name="ddsm_phase1savingsthm"]').disable();

                    editor.getEditor().child('[name="ddsm_IncentivePaymentTotalGross"]').disable();
                    editor.getEditor().child('[name="ddsm_kWhGrossSavingsatMeterperunit"]').disable();
                    editor.getEditor().child('[name="ddsm_kWGrossSavingsatMeterperunit"]').disable();
                    editor.getEditor().child('[name="ddsm_phase1perunitthm"]').disable();
                    editor.getEditor().child('[name="ddsm_IncentivePaymentGross"]').disable();

                } else {
                    // Phase 1
                    editor.getEditor().child('[name="ddsm_Units"]').enable();
                    editor.getEditor().child('[name="ddsm_kWhGrossSavingsatMeter"]').enable();
                    editor.getEditor().child('[name="ddsm_kWGrossSavingsatMeter"]').enable();
                    editor.getEditor().child('[name="ddsm_phase1savingsthm"]').enable();

                    editor.getEditor().child('[name="ddsm_IncentivePaymentTotalGross"]').enable();
                    editor.getEditor().child('[name="ddsm_kWhGrossSavingsatMeterperunit"]').enable();
                    editor.getEditor().child('[name="ddsm_kWGrossSavingsatMeterperunit"]').enable();
                    editor.getEditor().child('[name="ddsm_phase1perunitthm"]').enable();
                    editor.getEditor().child('[name="ddsm_IncentivePaymentGross"]').enable();
                }
            }

            function disablePhase1EditorCell(editor, status) {
                if (status) {
                    // Phase 1
                    editor.getEditor().child('[name="ddsm_Units"]').disable();
                    editor.getEditor().child('[name="ddsm_kWhGrossSavingsatMeter"]').disable();
                    editor.getEditor().child('[name="ddsm_kWGrossSavingsatMeter"]').disable();
                    editor.getEditor().child('[name="ddsm_phase1savingsthm"]').disable();

                    editor.getEditor().child('[name="ddsm_IncentivePaymentTotalGross"]').disable();
                    editor.getEditor().child('[name="ddsm_kWhGrossSavingsatMeterperunit"]').disable();
                    editor.getEditor().child('[name="ddsm_kWGrossSavingsatMeterperunit"]').disable();
                    editor.getEditor().child('[name="ddsm_phase1perunitthm"]').disable();
                    editor.getEditor().child('[name="ddsm_IncentivePaymentGross"]').disable();
                } else {
                    // Phase 1
                    editor.getEditor().child('[name="ddsm_Units"]').enable();
                    editor.getEditor().child('[name="ddsm_kWhGrossSavingsatMeter"]').enable();
                    editor.getEditor().child('[name="ddsm_kWGrossSavingsatMeter"]').enable();
                    editor.getEditor().child('[name="ddsm_phase1savingsthm"]in').enable();

                    editor.getEditor().child('[name="ddsm_IncentivePaymentTotalGross"]').enable();
                    editor.getEditor().child('[name="ddsm_kWhGrossSavingsatMeterperunit"]').enable();
                    editor.getEditor().child('[name="ddsm_kWGrossSavingsatMeterperunit"]').enable();
                    editor.getEditor().child('[name="ddsm_phase1perunitthm"]').enable();
                    editor.getEditor().child('[name="ddsm_IncentivePaymentGross"]').enable();
                }
            }

            //create Document Name Convention List to project
            function createDocList_measTpl(measTpl_ID) {
                var proj_ID = Xrm.Page.context.getQueryStringParameters().id;
                var proj_Name = b.Xrm.Page.getAttribute("ddsm_name").getValue();

                var docProjList = null;
                var _req = new XMLHttpRequest();
                var serverUrl = b.Xrm.Page.context.getClientUrl();
                _req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
                    + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
                    + "&$filter=ddsm_ProjectId/Id eq guid'" + proj_ID + "'", false);
                _req.setRequestHeader("Accept", "application/json");
                _req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                _req.send();
                if (_req.readyState == 4 && _req.status == 200) {
                    var _tplData = JSON.parse(_req.responseText).d;
                    if (_tplData.results[0] != null) {
                        docProjList = _tplData.results;
                    }
                }

                var req = new XMLHttpRequest();
                req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
                    + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
                    + "&$filter=ddsm_MeasureTemplateId/Id eq guid'" + measTpl_ID + "'", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.send();
                if (req.readyState == 4 && req.status == 200) {
                    var _tplData = JSON.parse(req.responseText).d;
                    if (_tplData.results[0] != null) {
                        for (var x = 0; x < _tplData.results.length; x++) {
                            var addDocConv = true;
                            if (docProjList !== null) {
                                for (var i = 0; i < docProjList.length; i++) {
                                    if (_tplData.results[x].ddsm_name == docProjList[i].ddsm_name) {
                                        addDocConv = false;
                                        break;
                                    }
                                }
                            }
                            if (addDocConv) {
                                var proj_obj = {};
                                if (_tplData.results[x].ddsm_RequiredByStatus != null) {
                                    proj_obj["ddsm_RequiredByStatus"] = _tplData.results[x].ddsm_RequiredByStatus;
                                }
                                if (_tplData.results[x].ddsm_name != null) {
                                    proj_obj["ddsm_name"] = _tplData.results[x].ddsm_name;
                                }
                                if (_tplData.results[x].ddsm_Uploaded != null) {
                                    proj_obj["ddsm_Uploaded"] = _tplData.results[x].ddsm_Uploaded;
                                }

                                proj_obj.ddsm_ProjectId = new Object();
                                proj_obj.ddsm_ProjectId.Id = proj_ID;
                                proj_obj.ddsm_ProjectId.Name = proj_Name;
                                proj_obj.ddsm_ProjectId.LogicalName = "ddsm_project";

                                //console.dir(proj_obj);

                                b.gen_H_createEntitySync(proj_obj, "ddsm_documentconventionSet");
                                b.document.getElementById('WebResource_docConvention').contentWindow.reloadGrid();
                            }
                        }
                    }
                }
            }

            function createMeasureFncl(records, InitMSIndex) {
                //Measure record
                var calculatedamount = 0;
                for (var i = 0; i < records.length; i++) {
                    calculatedamount += parseFloat(records[i].get("ddsm_IncentivePaymentTotalGross"));
                    var measObj = {};
                    measObj.ddsm_IncludedinFinancial = true;
                    records[i].set("ddsm_IncludedinFinancial", true);
                    b.gen_H_updateEntitySync(records[i].get("ddsm_measureId"), measObj, "ddsm_measureSet")
                }
                Ext.getCmp(idGrid).getStore().commitChanges();
                //Milestone Active
                var startDate = new Date();
                var req = new XMLHttpRequest();
                req.open("GET", b.Xrm.Page.context.getClientUrl() + "/xrmservices/2011/OrganizationData.svc/ddsm_milestoneSet?"
                    + "$select="
                    + "ddsm_ActualStart,"
                    + "&$filter=ddsm_ProjectToMilestoneId/Id eq guid'" + b.Xrm.Page.data.entity.getId() + "' and ddsm_Status/Value eq 962080001 and statecode/Value eq 0", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.send();
                if (req.readyState == 4 && req.status == 200) {
                    var _tplData = JSON.parse(req.responseText).d;
                    startDate = new Date(eval((_tplData.results[0].ddsm_ActualStart).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))')));
                }

                //  Financial Template
                req = new XMLHttpRequest();
                req.open("GET", b.Xrm.Page.context.getClientUrl() + "/xrmservices/2011/OrganizationData.svc/ddsm_financialtemplateSet?"
                    + "$select="
                    + "ddsm_name,"
                    + "ddsm_FinancialType,"
                    + "ddsm_PaymentType,"
                    + "ddsm_InitiatingMilestoneIndex,"
                    + "ddsm_StageName1,"
                    + "ddsm_StageName2,"
                    + "ddsm_StageName3,"
                    + "ddsm_StageName4,"
                    + "ddsm_StageName5,"
                    + "ddsm_Duration1,"
                    + "ddsm_Duration2,"
                    + "ddsm_Duration3,"
                    + "ddsm_Duration4,"
                    + "ddsm_Duration5,"
                    + "ddsm_CalculationType,"
                    + "ddsm_ProjectCompletableIndex"
                    + "&$filter=ddsm_financialtemplateId eq guid'" + fnclTpl_ID + "' and statecode/Value eq 0", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.send();

                if (req.readyState == 4 && req.status == 200) {
                    var _tplData = JSON.parse(req.responseText).d;

                    if (_tplData.results != null) {
                        var parentSite = b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue();
                        var parentCo = b.Xrm.Page.getAttribute("ddsm_accountid").getValue();

                        var fncl_tpl = _tplData.results[0];
                        var date = new Date();

                        var fncl_obj = new Object();
                        fncl_obj.ddsm_name = "Manual Incentive Payment - " + ('0' + (date.getMonth() + 1)).slice(-2) + ('0' + date.getDate()).slice(-2) + date.getFullYear();

                        fncl_obj.ddsm_projecttofinancialid = new Object();
                        fncl_obj.ddsm_projecttofinancialid.Id = (b.Xrm.Page.data.entity.getId()).replace(/\{|\}/g, '');
                        fncl_obj.ddsm_projecttofinancialid.Name = b.Xrm.Page.getAttribute("ddsm_name").getValue();
                        fncl_obj.ddsm_projecttofinancialid.LogicalName = "ddsm_project";

                        fncl_obj.ddsm_financialtype = new Object();
                        fncl_obj.ddsm_financialtype.Value = fncl_tpl.ddsm_FinancialType.Value;

                        fncl_obj.ddsm_status = new Object();
                        fncl_obj.ddsm_status.Value = 962080001;

                        fncl_obj.ddsm_initiatingmilestoneindex = InitMSIndex;
                        fncl_obj.ddsm_stagename1 = fncl_tpl.ddsm_StageName1;
                        fncl_obj.ddsm_stagename2 = fncl_tpl.ddsm_StageName2;
                        fncl_obj.ddsm_stagename3 = fncl_tpl.ddsm_StageName3;
                        fncl_obj.ddsm_stagename4 = fncl_tpl.ddsm_StageName4;
                        fncl_obj.ddsm_stagename5 = fncl_tpl.ddsm_StageName5;
                        fncl_obj.ddsm_duration1 = fncl_tpl.ddsm_Duration1;
                        fncl_obj.ddsm_duration2 = fncl_tpl.ddsm_Duration2;
                        fncl_obj.ddsm_duration3 = fncl_tpl.ddsm_Duration3;
                        fncl_obj.ddsm_duration4 = fncl_tpl.ddsm_Duration4;
                        fncl_obj.ddsm_duration5 = fncl_tpl.ddsm_Duration5;

                        //fncl_obj.ddsm_calculationtype = fncl_tpl.ddsm_CalculationType;
                        //fncl_obj.ddsm_projectcompletableindex = fncl_tpl.ddsm_ProjectCompletableIndex;

                        fncl_obj.ddsm_financialtemplate = new Object();
                        fncl_obj.ddsm_financialtemplate.Id = fnclTpl_ID;
                        fncl_obj.ddsm_financialtemplate.Name = fncl_tpl.ddsm_name;
                        fncl_obj.ddsm_financialtemplate.LogicalName = "ddsm_financialtemplate";

                        if (parentSite != null) {
                            fncl_obj.ddsm_siteid = new Object();
                            fncl_obj.ddsm_siteid.Id = (parentSite[0].id).replace(/\{|\}/g, '');
                            fncl_obj.ddsm_siteid.Name = parentSite[0].name;
                            fncl_obj.ddsm_siteid.LogicalName = parentSite[0].entityType;
                        }

                        if (parentCo != null) {
                            fncl_obj.ddsm_accountid = new Object();
                            fncl_obj.ddsm_accountid.Id = (parentCo[0].id).replace(/\{|\}/g, '');
                            fncl_obj.ddsm_accountid.Name = parentCo[0].name;
                            fncl_obj.ddsm_accountid.LogicalName = parentCo[0].entityType;
                        }

                        fncl_obj.ddsm_pendingstage = fncl_obj.ddsm_stagename1;
                        fncl_obj.ddsm_targetstart1 = startDate;
                        fncl_obj.ddsm_actualstart1 = startDate;
                        startDate = new Date(startDate.getTime() + (fncl_obj.ddsm_duration1 * 86400000));
                        fncl_obj.ddsm_targetend1 = startDate;
                        fncl_obj.ddsm_targetstart2 = startDate;
                        startDate = new Date(startDate.getTime() + (fncl_obj.ddsm_duration2 * 86400000));
                        fncl_obj.ddsm_targetend2 = startDate;
                        fncl_obj.ddsm_targetstart3 = startDate;
                        startDate = new Date(startDate.getTime() + (fncl_obj.ddsm_duration3 * 86400000));
                        fncl_obj.ddsm_targetend3 = startDate;
                        fncl_obj.ddsm_targetstart4 = startDate;
                        startDate = new Date(startDate.getTime() + (fncl_obj.ddsm_duration4 * 86400000));
                        fncl_obj.ddsm_targetend4 = startDate;

                        fncl_obj.ddsm_calculatedamount = {Value: parseFloat(calculatedamount).toFixed(2)};

                        var newRecord = b.gen_H_createEntitySync(fncl_obj, "ddsm_financialSet");
                        for (var i = 0; i < records.length; i++) {
                            var measObj = {};
                            measObj.ddsm_MeastoFnclId = {
                                Id: newRecord.ddsm_financialId,
                                Name: newRecord.ddsm_name,
                                LogicalName: "ddsm_financial"
                            };
                            b.gen_H_updateEntitySync(records[i].get("ddsm_measureId"), measObj, "ddsm_measureSet")
                        }

                        b.document.getElementById('WebResource_financial_grid').contentWindow.reloadGrid();
                    }

                }
            }

            AutoMeasureFncl = function (InitMSIndex) {
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var recordsCount = recordsGrid.length;
                var arr = [];
                if (recordsCount > 0) {
                    for (var i = 0; i < recordsCount; i++) {
                        var record = recordsGrid[i];
                        if (!record.get("ddsm_IncludedinFinancial")) {
                            arr.push(record);
                        }
                    }
                    if (arr.length > 0) {
                        arr.sort(function (a, b) {
                            if ((a.get("ddsm_TradeAllyId")).toLowerCase() > (b.get("ddsm_TradeAllyId")).toLowerCase()) {
                                return 1;
                            }
                            if ((a.get("ddsm_TradeAllyId")).toLowerCase() < (b.get("ddsm_TradeAllyId")).toLowerCase()) {
                                return -1;
                            }
                            return 0;
                        });
                        if (arr.length > 1) {
                            var tmp_rec = [];
                            tmp_rec.push(arr[0]);
                            for (var i = 1; i < arr.length; i++) {
                                if ((tmp_rec[tmp_rec.length - 1].get("ddsm_TradeAllyId")).toLowerCase() == (arr[i].get("ddsm_TradeAllyId")).toLowerCase()) {
                                    tmp_rec.push(arr[i]);
                                } else {
                                    createMeasureFncl(tmp_rec, InitMSIndex);
                                    tmp_rec = [];
                                    tmp_rec.push(arr[i]);
                                    if (i == arr.length - 1) {
                                        createMeasureFncl(tmp_rec, InitMSIndex);
                                    }
                                }
                            }
                        } else {
                            createMeasureFncl(arr, InitMSIndex);
                        }
                    }
                }
            }

            function splitMeas(units, splitMethod) {
                var grid = Ext.getCmp(idGrid),
                    sm = grid.getSelectionModel(),
                    records = sm.getSelection(),
                    projPhase = 1, per_units = 0, new_units = 0;
                if (b.Xrm.Page.getAttribute("ddsm_phase").getValue() == "2. Project") projPhase = 2;
                else if (b.Xrm.Page.getAttribute("ddsm_phase").getValue() == "3. Evaluation") projPhase = 3;

                if (splitMethod != 2) {
                    for (var i = 0; i < records.length; i++) {
                        switch (splitMethod) {
                            case 0:
                                new_units = records[i].data.ddsm_CurrentUnits - units;
                                switch (projPhase) {
                                    case 1:
                                        rowEditing.startEdit(records[i].index, 3);
                                        rowEditing.getEditor().getForm().findField('ddsm_Units').setValue(new_units);
                                        rowEditing.completeEdit();
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            case 1:
                                per_units = Math.round((records[i].data.ddsm_CurrentUnits * units) / 100);
                                new_units = records[i].data.ddsm_CurrentUnits - per_units;

                                switch (projPhase) {
                                    case 1:
                                        rowEditing.startEdit(records[i].index, 3);
                                        rowEditing.getEditor().getForm().findField('ddsm_Units').setValue(new_units);
                                        rowEditing.completeEdit();
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            case 2:
                                break;
                            default:
                                break;
                        }
                    }
                }

                for (var i = 0; i < records.length; i++) {
                    var new_record = records[i].copy();
                    new_record.data.ddsm_measureId = "";
                    //console.dir(new_record);
                    var newRow = Ext.create(modelGrid, new_record.data),
                        count = grid.getStore().getCount();
                    grid.getStore().insert(count, newRow);
                    switch (splitMethod) {
                        case 0:
                            new_units = records[i].data.ddsm_CurrentUnits - units;
                            switch (projPhase) {
                                case 1:
                                    rowEditing.startEdit(count, 3);
                                    rowEditing.getEditor().getForm().findField('ddsm_Units').setValue(units);
                                    rowEditing.completeEdit();
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 1:
                            per_units = Math.round((records[i].data.ddsm_CurrentUnits * units) / 100);
                            new_units = records[i].data.ddsm_CurrentUnits - per_units;

                            switch (projPhase) {
                                case 1:
                                    rowEditing.startEdit(count, 3);
                                    rowEditing.getEditor().getForm().findField('ddsm_Units').setValue(per_units);
                                    rowEditing.completeEdit();
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2:
                            rowEditing.startEdit(count, 3);
                            rowEditing.completeEdit();
                            break;
                        default:
                            break;
                    }
                }

            }

            function getMeasureLibrary(measTpl_Id, record, rowIndex) {
                //var measTpl_Id = Xrm.Page.data.entity.getId();
                var record = record,
                    grid = grid, rowIndex = rowIndex;
                //console.log(measTpl_Id);
                //console.dir(record);
                var objecttypes = Xrm.Internal.getEntityCode("ddsm_measurelibrary");
                var viewId = "{00000000-0000-0000-0000-000000000001}";

                var fetchXml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                    "<entity name='ddsm_measurelibrary'>" +
                    "<attribute name='ddsm_measurelibraryid' />" +
                    "<attribute name='ddsm_name' />" +
                    "<attribute name='ddsm_manufacturer' />" +
                    "<attribute name='ddsm_modelnumber' />" +
                    "<attribute name='ddsm_status' />" +
                    "<attribute name='statecode' />" +
                    "<order attribute='ddsm_name' descending='false' />" +
                    "<filter type='and'>" +
                    "<condition attribute='ddsm_measuretemplateid' operator='eq' value='" + measTpl_Id + "' />" +
                    "</filter>" +
                    "<filter type='and'>" +
                    "<condition attribute='statecode' operator='eq' value='0' />" +
                    "</filter>" +
                    "</entity>" +
                    "</fetch>";

                var layoutXml = "" +
                    "<grid name='' object='1' jump='name' select='1' icon='1' preview='1'>" +
                    "<row name='result' id='ddsm_measurelibraryid'>" +
                    "<cell name='ddsm_modelnumber' width='200' />" +
                    "<cell name='ddsm_manufacturer' width='100' />" +
                    "<cell name='ddsm_status' ishidden='1' width='50' />" +
//                    "<cell name='ddsm_status' width='50' />" +
                    "</row>" +
                    "</grid>";

                var customView = {
                    fetchXml: fetchXml,
                    id: viewId,
                    layoutXml: layoutXml,
                    name: "Projects LookupView",
                    recordType: objecttypes,
                    Type: 0
                };

                var callbackReference = {
                    callback: function (lkp_popup) {
                        try {
                            if (lkp_popup && lkp_popup.items.length > 0) {
                                var tmpObj = JSON.parse(lkp_popup.items[0].values);
                                //console.dir(tmpObj);
                                for (var j = 0; j < tmpObj.length; j++) {
                                    if (tmpObj[j].name == "ddsm_modelnumber" && (tmpObj[j].value != null && tmpObj[j].value != "")) {
                                        record.set("ddsm_ModelNumber", tmpObj[j].value)
                                    }
                                    if (tmpObj[j].name == "ddsm_manufacturer" && (tmpObj[j].value != null && tmpObj[j].value != "")) {
                                        record.set("ddsm_Manufacturer", tmpObj[j].value)
                                    }
                                    if (tmpObj[j].name == "ddsm_status" && (tmpObj[j].value != null && tmpObj[j].value != "")) {
                                        record.set("ddsm_ModelNumberStatus", tmpObj[j].value)
                                    }
                                }
                                record.commit();
                                Update_Create_Record(Ext.getCmp(idGrid), record, rowIndex);
                            }
                        } catch (err) {
                            alert("Error: Measure Library: " + err);
                        }
                    }
                }

                b.LookupObjectsWithCallback(callbackReference, null, "single", objecttypes, 0, null, "", null, null, null, null, null, null, viewId, [customView]);

            }

            //Load New Version Measute Template
            function verifyVersionMeasTpl(measTpl) {
                var projPhase = b.Xrm.Page.getAttribute("ddsm_phase").getValue(),
                    serverUrl = b.Xrm.Page.context.getClientUrl(),
                    req = new XMLHttpRequest(),
                    req1 = new XMLHttpRequest(),
                    projPhaseOld = b.oldProjPhase,
                    measTpl = measTpl,
                    updateObj = null;
                if (projPhase != projPhaseOld) {
                    try {
                        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_measuretemplateSet?"
                            + "$select="
                            + "ddsm_name,"
                            + "ddsm_measuretemplateId,"
                            + "&$orderby=ddsm_version desc"
                            + "&$filter=ddsm_name eq '" + encodeURI(measTpl.Name) + "' and statecode/Value eq 0", false);
                        req.setRequestHeader("Accept", "application/json");
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.send();
                        if (req.readyState == 4 && req.status == 200) {
                            var _tplData1 = JSON.parse(req.responseText).d;
                            if (_tplData1.results[0] != null) {
                                //console.dir(_tplData1.results[0]);
                                if (measTpl.Id != _tplData1.results[0].ddsm_measuretemplateId) {
                                    var measTpl_ID = _tplData1.results[0].ddsm_measuretemplateId;
                                    try {

                                        req1.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_measuretemplateSet?"
                                            + "$select="
                                                /*
                                                 + "ddsm_name,"
                                                 + "ddsm_measuretemplateId,"
                                                 + "ddsm_IncRateUnit,"
                                                 + "ddsm_IncRateKW,"
                                                 + "ddsm_IncRatekWh,"
                                                 + "ddsm_IncRatethm,"
                                                 + "ddsm_SavingsperUnitKW,"
                                                 + "ddsm_SavingsperUnitkWh,"
                                                 + "ddsm_SavingsperUnitthm,"
                                                 + "ddsm_Program,"
                                                 + "ddsm_MeasureCode,"
                                                 + "ddsm_MeasureNumber,"
                                                 + "ddsm_EnergyType,"
                                                 + "ddsm_Category,"
                                                 + "ddsm_MeasureGroup,"
                                                 + "ddsm_MeasureSubgroup,"
                                                 + "ddsm_Size,"
                                                 + "ddsm_EUL,"
                                                 + "ddsm_BaseEquipDescr,"
                                                 + "ddsm_EffEquipDescr,"
                                                 + "ddsm_AnnualFixedCosts,"
                                                 + "ddsm_ClientDescription,"
                                                 + "ddsm_EndUse,"
                                                 + "ddsm_EquipmentDescription,"
                                                 + "ddsm_LMLightingorNon,"
                                                 + "ddsm_MeasureNotes,"
                                                 + "ddsm_Source,"
                                                 + "ddsm_Unit,"
                                                 + "ddsm_StudyMeasure,"

                                                 + "ddsm_MeasureType,"
                                                 + "ddsm_AnnualHoursBefore,"
                                                 + "ddsm_AnnualHoursAfter,"
                                                 + "ddsm_SummerDemandReductionKW,"
                                                 + "ddsm_WinterDemandReductionKW,"
                                                 + "ddsm_WattsBase,"
                                                 + "ddsm_WattsEff,"
                                                 + "ddsm_ClientPeakKWSavings,"
                                                 + "ddsm_MotorHP,"
                                                 + "ddsm_measuresubgroup2,"
                                                 + "ddsm_SavingsperUnitCCF,"
                                                 + "ddsm_ProgramCycle,"

                                                 + "ddsm_SizeRanges,"
                                                 + "ddsm_Sector,"
                                                 + "ddsm_BaselineTechnology,"
                                                 + "ddsm_EfficiencyTechnology,"
                                                 + "ddsm_MeasureCategory,"
                                                 + "ddsm_MeasureType,"
                                                 + "ddsm_MarketType,"
                                                 + "ddsm_Measurelift,"
                                                 + "ddsm_ExplainRestrictionsLimitations,"
                                                 + "ddsm_Equipmentsize,"
                                                 + "ddsm_EquipmentsizeType,"

                                                 + "ddsm_MeasureLifeDemo,"
                                                 + "ddsm_M3Savings,"
                                                 + "ddsm_SqFt,"
                                                 + "ddsm_RateClass,"
                                                 + "ddsm_InsulationQuality,"
                                                 + "ddsm_BaseEqWatt,"
                                                 + "ddsm_kWhsavings,"
                                                 + "ddsm_Persistence,"
                                                 + "ddsm_Attribution,"
                                                 + "ddsm_Freerider,"
                                                 + "ddsm_Spillover,"
                                                 + "ddsm_IncrementalCost,"
                                                 */
                                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_name,"
                                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateSavings,"
                                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateIncentive,"
                                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_PerUnitLock,"
                                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_SavingsLock,"
                                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentiveRatesLock,"
                                            + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentivesLock"
                                            + "&$expand=ddsm_ddsm_measurecalculationtemplate_ddsm_measur"
                                            + "&$filter=ddsm_measuretemplateId eq guid'" + measTpl_ID + "' and statecode/Value eq 0", false);
                                        req1.setRequestHeader("Accept", "application/json");
                                        req1.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                                        req1.send();
                                        if (req1.readyState == 4 && req1.status == 200) {
                                            var _tplData = JSON.parse(req1.responseText).d;
                                            if (_tplData.results[0] != null) {
                                                //console.dir(_tplData.results[0]);
                                                updateObj = {};
                                                arrayIds.push(_tplData.results[0].ddsm_measuretemplateId);

                                                updateObj.ddsm_MeasureSelector = _tplData.results[0].ddsm_name;
                                                updateObj.ddsm_MeasureSelector__Id = _tplData.results[0].ddsm_measuretemplateId;

                                                updateObj.ddsm_MeasureLifeDemo = (_tplData.results[0].ddsm_MeasureLifeDemo.Value != null) ? (_tplData.results[0].ddsm_MeasureLifeDemo.Value).toString() : _tplData.results[0].ddsm_MeasureLifeDemo.Value;
                                                updateObj.ddsm_rateclass = (_tplData.results[0].ddsm_RateClass.Value != null) ? (_tplData.results[0].ddsm_RateClass.Value).toString() : _tplData.results[0].ddsm_RateClass.Value;
                                                updateObj.ddsm_InsulationQuality = (_tplData.results[0].ddsm_InsulationQuality.Value != null) ? (_tplData.results[0].ddsm_InsulationQuality.Value).toString() : _tplData.results[0].ddsm_InsulationQuality.Value;
                                                updateObj.ddsm_BaseEqWatt = (_tplData.results[0].ddsm_BaseEqWatt.Value != null) ? (_tplData.results[0].ddsm_BaseEqWatt.Value).toString() : _tplData.results[0].ddsm_BaseEqWatt.Value;
                                                updateObj.ddsm_Persistence = (_tplData.results[0].ddsm_Persistence.Value != null) ? (_tplData.results[0].ddsm_Persistence.Value).toString() : _tplData.results[0].ddsm_Persistence.Value;
                                                updateObj.ddsm_Attribution = (_tplData.results[0].ddsm_Attribution.Value != null) ? (_tplData.results[0].ddsm_Attribution.Value).toString() : _tplData.results[0].ddsm_Attribution.Value;
                                                updateObj.ddsm_Freerider = (_tplData.results[0].ddsm_Freerider.Value != null) ? (_tplData.results[0].ddsm_Freerider.Value).toString() : _tplData.results[0].ddsm_Freerider.Value;
                                                updateObj.ddsm_Spillover = (_tplData.results[0].ddsm_Spillover.Value != null) ? (_tplData.results[0].ddsm_Spillover.Value).toString() : _tplData.results[0].ddsm_Spillover.Value;
                                                updateObj.ddsm_M3Savings = _tplData.results[0].ddsm_M3Savings;
                                                updateObj.ddsm_sqft = _tplData.results[0].ddsm_SqFt;
                                                updateObj.ddsm_kWhsavings = _tplData.results[0].ddsm_kWhsavings;

                                                updateObj.ddsm_Sector = (_tplData.results[0].ddsm_Sector.Value != null) ? (_tplData.results[0].ddsm_Sector.Value).toString() : _tplData.results[0].ddsm_Sector.Value;
                                                updateObj.ddsm_MeasureCategory = (_tplData.results[0].ddsm_MeasureCategory.Value != null) ? (_tplData.results[0].ddsm_MeasureCategory.Value).toString() : _tplData.results[0].ddsm_MeasureCategory.Value;

                                                updateObj.ddsm_MarketType = (_tplData.results[0].ddsm_MarketType.Value != null) ? (_tplData.results[0].ddsm_MarketType.Value).toString() : _tplData.results[0].ddsm_MarketType.Value;
                                                updateObj.ddsm_EquipmentsizeType = (_tplData.results[0].ddsm_EquipmentsizeType.Value != null) ? (_tplData.results[0].ddsm_EquipmentsizeType.Value).toString() : _tplData.results[0].ddsm_EquipmentsizeType.Value;

                                                updateObj.ddsm_SizeRanges = _tplData.results[0].ddsm_SizeRanges;
                                                updateObj.ddsm_BaselineTechnology = _tplData.results[0].ddsm_BaselineTechnology;
                                                updateObj.ddsm_EfficiencyTechnology = _tplData.results[0].ddsm_EfficiencyTechnology;
                                                updateObj.ddsm_Measurelift = _tplData.results[0].ddsm_Measurelift;
                                                updateObj.ddsm_ExplainRestrictionsLimitations = _tplData.results[0].ddsm_ExplainRestrictionsLimitations;
                                                updateObj.ddsm_Equipmentsize = _tplData.results[0].ddsm_Equipmentsize;

                                                var irpu = (_tplData.results[0].ddsm_IncRateUnit.Value != null) ? parseFloat(_tplData.results[0].ddsm_IncRateUnit.Value) : 0;
                                                var irkw = (_tplData.results[0].ddsm_IncRateKW.Value != null) ? parseFloat(_tplData.results[0].ddsm_IncRateKW.Value) : 0;
                                                var irkwh = (_tplData.results[0].ddsm_IncRatekWh.Value != null) ? parseFloat(_tplData.results[0].ddsm_IncRatekWh.Value) : 0;
                                                var irthm = (_tplData.results[0].ddsm_IncRatethm.Value != null) ? parseFloat(_tplData.results[0].ddsm_IncRatethm.Value) : 0;
                                                var incCost = (_tplData.results[0].ddsm_IncrementalCost.Value != null) ? parseFloat(_tplData.results[0].ddsm_IncrementalCost.Value) : 0;
                                                var skw = (_tplData.results[0].ddsm_SavingsperUnitKW != null) ? parseFloat(_tplData.results[0].ddsm_SavingsperUnitKW) : 0;
                                                var skwh = (_tplData.results[0].ddsm_SavingsperUnitkWh != null) ? parseFloat(_tplData.results[0].ddsm_SavingsperUnitkWh) : 0;
                                                var sthm = (_tplData.results[0].ddsm_SavingsperUnitthm != null) ? parseFloat(_tplData.results[0].ddsm_SavingsperUnitthm) : 0;
                                                var sccf = (_tplData.results[0].ddsm_SavingsperUnitCCF != null) ? parseFloat(_tplData.results[0].ddsm_SavingsperUnitCCF) : 0;
                                                var AnnFixedCosts = (_tplData.results[0].ddsm_AnnualFixedCosts != null) ? parseFloat(_tplData.results[0].ddsm_AnnualFixedCosts.Value) : 0;

                                                var MeasureCode = _tplData.results[0].ddsm_MeasureCode;
                                                var MeasureNumber = _tplData.results[0].ddsm_MeasureNumber;
                                                var EnergyType = _tplData.results[0].ddsm_EnergyType.Value;
                                                var Category = _tplData.results[0].ddsm_Category;
                                                var MeasureGroup = _tplData.results[0].ddsm_MeasureGroup;
                                                var MeasureSubgroup = _tplData.results[0].ddsm_MeasureSubgroup;
                                                var Size = (_tplData.results[0].ddsm_Size != null) ? parseFloat(_tplData.results[0].ddsm_Size) : 0;
                                                var EUL = (_tplData.results[0].ddsm_EUL != null) ? parseFloat(_tplData.results[0].ddsm_EUL) : 0;
                                                var BaseEquipDescr = _tplData.results[0].ddsm_BaseEquipDescr;
                                                var EffEquipDescr = _tplData.results[0].ddsm_EffEquipDescr;
                                                var ClientDesc = _tplData.results[0].ddsm_ClientDescription;
                                                var EndUse = _tplData.results[0].ddsm_EndUse;
                                                var EquipDesc = _tplData.results[0].ddsm_EquipmentDescription;
                                                var LMLightNon = _tplData.results[0].ddsm_LMLightingorNon;
                                                var MeasNotes = _tplData.results[0].ddsm_MeasureNotes;
                                                var Source = _tplData.results[0].ddsm_Source;
                                                var Unit = _tplData.results[0].ddsm_Unit;
                                                var StudyMeas = _tplData.results[0].ddsm_StudyMeasure;

                                                var measType = _tplData.results[0].ddsm_MeasureType.Value;
                                                var annHoursBefore = _tplData.results[0].ddsm_AnnualHoursBefore;
                                                var annHoursAfter = _tplData.results[0].ddsm_AnnualHoursAfter;
                                                var sumDemReducKW = _tplData.results[0].ddsm_SummerDemandReductionKW;
                                                var wintDemReducKW = _tplData.results[0].ddsm_WinterDemandReductionKW;
                                                var wattsBase = _tplData.results[0].ddsm_WattsBase;
                                                var wattsEff = _tplData.results[0].ddsm_WattsEff;
                                                var clientPeakKWSave = _tplData.results[0].ddsm_ClientPeakKWSavings;

                                                var motorHP = _tplData.results[0].ddsm_MotorHP;

                                                var measureSubgroup2 = _tplData.results[0].ddsm_measuresubgroup2;
                                                var measCalcTpl_Data = _tplData.results[0].ddsm_ddsm_measurecalculationtemplate_ddsm_measur;
                                                var measCalcTpl_Name = measCalcTpl_Data.ddsm_name;
                                                var measCalcTpl_CalcSavings = measCalcTpl_Data.ddsm_CalculateSavings;
                                                var measCalcTpl_CalcIncentives = measCalcTpl_Data.ddsm_CalculateIncentive;
                                                var measCalcTpl_PerUnitLock = measCalcTpl_Data.ddsm_PerUnitLock;
                                                var measCalcTpl_SavingsLock = measCalcTpl_Data.ddsm_SavingsLock;
                                                var measCalcTpl_IncentiveRatesLock = measCalcTpl_Data.ddsm_IncentiveRatesLock;
                                                var measCalcTpl_IncentivesLock = measCalcTpl_Data.ddsm_IncentivesLock;
                                                var measCalcTpl_CalcTypeValues = "";

                                                updateObj.ddsm_MeasureCode = MeasureCode;
                                                updateObj.ddsm_MeasureGroup = MeasureGroup;
                                                updateObj.ddsm_MeasureNumber = MeasureNumber;
                                                updateObj.ddsm_Program = (_tplData.results[0].ddsm_EnergyType.Value != null) ? EnergyType.toString() : null;
                                                updateObj.ddsm_Category = Category;
                                                updateObj.ddsm_MeasureSubgroup = MeasureSubgroup;
                                                updateObj.ddsm_Size = Size;
                                                updateObj.ddsm_EUL = EUL;

                                                updateObj.ddsm_BaseEquipDescr = BaseEquipDescr;
                                                updateObj.ddsm_EffEquipDescr = EffEquipDescr;
                                                updateObj.ddsm_ClientDescription = ClientDesc;
                                                updateObj.ddsm_EndUse = EndUse;
                                                updateObj.ddsm_EquipmentDescription = EquipDesc;
                                                updateObj.ddsm_LMLightingorNon = LMLightNon;
                                                updateObj.ddsm_MeasureNotes = MeasNotes;
                                                updateObj.ddsm_Source = Source;
                                                updateObj.ddsm_Unit = Unit;
                                                updateObj.ddsm_StudyMeasure = StudyMeas;

                                                updateObj.ddsm_MeasureType = (_tplData.results[0].ddsm_MeasureType.Value != null) ? measType.toString() : null;

                                                updateObj.ddsm_AnnualHoursBefore = (annHoursBefore != null) ? parseFloat(annHoursBefore) : 0;
                                                updateObj.ddsm_AnnualHoursAfter = (annHoursAfter != null) ? parseFloat(annHoursAfter) : 0;
                                                updateObj.ddsm_SummerDemandReductionKW = (sumDemReducKW != null) ? parseFloat(sumDemReducKW) : 0;
                                                updateObj.ddsm_WinterDemandReductionKW = (wintDemReducKW != null) ? parseFloat(wintDemReducKW) : 0;
                                                updateObj.ddsm_WattsBase = (wattsBase != null) ? parseFloat(wattsBase) : 0;
                                                updateObj.ddsm_WattsEff = (wattsEff != null) ? parseFloat(wattsEff) : 0;
                                                updateObj.ddsm_clientpeakkwsavings = (clientPeakKWSave != null) ? parseFloat(clientPeakKWSave) : 0;
                                                updateObj.ddsm_MotorHP = (motorHP != null) ? parseInt(motorHP, 10) : 0;
                                                updateObj.ddsm_measuresubgroup2 = measureSubgroup2;

                                                //updateObj.ddsm_ProgramCycleId = _tplData.results[0].ddsm_ProgramCycle.Name;
                                                //updateObj.ddsm_ProgramCycleId__Id = (_tplData.results[0].ddsm_ProgramCycle.Id != null)?(_tplData.results[0].ddsm_ProgramCycle.Id).replace(/\{|\}/g, ''):'00000000-0000-0000-0000-000000000000';
                                                //updateObj.ddsm_ProgramCycleId__LogicalName = (_tplData.results[0].ddsm_ProgramCycle.LogicalName != null)?_tplData.results[0].ddsm_ProgramCycle.LogicalName:'ddsm_programcycle';

                                                //------------------------------------------------------------------//

                                                updateObj.ddsm_IncRateUnit = (_tplData.results[0].ddsm_IncRateUnit.Value != null) ? irpu : 0;
                                                updateObj.ddsm_IncRateKW = (_tplData.results[0].ddsm_IncRateKW.Value != null) ? irkw : 0;
                                                updateObj.ddsm_IncRatekWh = (_tplData.results[0].ddsm_IncRatekWh.Value != null) ? irkwh : 0;
                                                updateObj.ddsm_IncRatethm = (_tplData.results[0].ddsm_IncRatethm.Value != null) ? irthm : 0;
                                                updateObj.ddsm_IncRateUnit = (_tplData.results[0].ddsm_IncRateUnit.Value != null) ? irpu : 0;
                                                updateObj.ddsm_IncrementalCost = (_tplData.results[0].ddsm_IncrementalCost.Value != null) ? incCost : 0;
                                                updateObj.ddsm_AnnualFixedCosts = (_tplData.results[0].ddsm_AnnualFixedCosts.Value != null) ? AnnFixedCosts : 0;

                                                //------------------------------------------------------------------//

                                                //copies savings
                                                //Copy to Phase 1
                                                if (parseInt(projPhase.substring(0, 1)) == 1) {
                                                    updateObj.ddsm_kWGrossSavingsatMeterperunit = (_tplData.results[0].ddsm_SavingsperUnitKW != null) ? skw : 0;
                                                    updateObj.ddsm_kWhGrossSavingsatMeterperunit = (_tplData.results[0].ddsm_SavingsperUnitkWh != null) ? skwh : 0;
                                                    updateObj.ddsm_phase1perunitthm = (_tplData.results[0].ddsm_SavingsperUnitthm != null) ? sthm : 0;
                                                    updateObj.ddsm_Phase1PerUnitCCF = (_tplData.results[0].ddsm_SavingsperUnitCCF != null) ? sccf : 0;
                                                    updateObj.ddsm_IncentivePaymentGross = (_tplData.results[0].ddsm_IncRateUnit.Value != null) ? irpu : 0;
                                                }

                                                //Measure Calc Template Data
                                                if (measCalcTpl_CalcSavings == true) {
                                                    measCalcTpl_CalcTypeValues += "Calculate Savings; ";
                                                }
                                                if (measCalcTpl_CalcIncentives == true) {
                                                    measCalcTpl_CalcTypeValues += "Calculate Incentives; ";
                                                }
                                                if (measCalcTpl_PerUnitLock == true) {
                                                    measCalcTpl_CalcTypeValues += "Lock Per Units; ";
                                                }
                                                if (measCalcTpl_SavingsLock == true) {
                                                    measCalcTpl_CalcTypeValues += "Lock Savings; ";
                                                }
                                                if (measCalcTpl_IncentiveRatesLock == true) {
                                                    measCalcTpl_CalcTypeValues += "Lock Incentive Rates; ";
                                                }
                                                if (measCalcTpl_IncentivesLock == true) {
                                                    measCalcTpl_CalcTypeValues += "Lock Incentives; ";
                                                }

                                                updateObj.ddsm_CalculationTypeValues = measCalcTpl_CalcTypeValues;
                                            }
                                        }
                                    }
                                    catch (err) {
                                        Ext.Msg.alert("Error load New Version Measure Template & Measure Calculation Template:", err);
                                    }

                                }
                            }
                        }
                    }
                    catch (err) {
                        Ext.Msg.alert("Error verify Measure Template:", err);
                    }
                }
                return updateObj;
            }

            function recalcMeasAfterUpdMeasTpl(arrayIds) {
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var recordsCount = recordsGrid.length;
                if (recordsCount > 0) {
                    for (var j = 0; j < arrayIds.length; j++) {
                        for (var i = 0; i < recordsCount; i++) {
                            var record = recordsGrid[i];
                            if (record.data['ddsm_MeasureSelector__Id'] == arrayIds[j]) {
                                //console.dir(record);
                                Ext.getCmp(idGrid).getSelectionModel().select(i);
                                measCalc_Offered(record.get('ddsm_Units'), Ext.getCmp(idGrid).getSelectionModel().getSelection()[0], 'listeners update tpl');
                                Update_Create_Record(Ext.getCmp(idGrid), record, i);
                                //rowEditing.startEdit(record.index, 9);
                                //rowEditing.completeEdit();
                            }
                        }
                    }
                }
                arrayIds = [];
            }


            /*
             Ext.getCmp(idGrid).headerCt.suspendLayout = true;
             Ext.getCmp(idGrid).headerCt.removeAll();
             Ext.getCmp(idGrid).headerCt.add(viewColumns);
             //Ext.getCmp(idGrid).bindStore(gridStore);
             Ext.getCmp(idGrid).forceComponentLayout();
             */
            //loadData(_getODataEndpoint(entitySchemaName), true);
            b.firstLoadGrid++;

        });

        // Refresh  Grid
        reloadGrid = function () {
            loadData(_getODataEndpoint(entitySchemaName), true);
        };
        get_dataOkgrid = function () {
            return dataOk;
        };
        get_saveGridStatus = function () {
            return saveGridStatus;
        };
        refreshGrid = function () {
            Ext.getCmp(idGrid).getView().refresh();
        };
        $(b).resize(function () {
            Ext.getCmp(idGrid).getView().refresh();
        });
        getModelNumberStatus = function () {
            var recordsGrid = Ext.getCmp(idGrid).getStore().getRange(),
                status = "",
                recordsCount = recordsGrid.length;
            if (recordsCount > 0) {
                for (var i = 0; i < recordsCount; i++) {
                    var record = recordsGrid[i];

                    if (record.data['ddsm_ModelNumberStatus'] == '962080002') {
                        status = status + "\n" + record.data["ddsm_name"];
                    }
                }
            }
            return status;
        }

        //region CUSTOM LOOKUP METHODS

        function setNewLookupData (res){
            if(!res || !res.items || !res.items[0]){
                return;
            }

            let lookupVal = AGS.String.format("{0}|{1}|{2}", res.items[0].typename, res.items[0].id, res.items[0].name);
            let lookupParts = lookupVal.split('|');
            let lkp_field = Ext.getCmp('smForm1').getForm().findField(lkp_toUpdate);
            if (!lkp_field){
                return;
            }
            lkp_field.store.removeAll();
            lkp_field.store.add({value: lookupVal, display: res.items[0].name || ''} );
            lkp_field.setValue(lookupVal);

            //clear global field name storage
            lkp_toUpdate = null;
        };

        function getSMLookupData(entityName, lkpCallback) {
            var localname = entityName.toLowerCase();
            var objecttypes = b.Xrm.Internal.getEntityCode(localname);
            var lookupURI = "/_controls/lookup/lookupinfo.aspx";
            lookupURI += "?LookupStyle=single";
            lookupURI += "&objecttypes=" + objecttypes;
            lookupURI += "&ShowNewButton=0";
            lookupURI += "&ShowPropButton=1";
            lookupURI += "&browse=false";
            lookupURI += "&AllowFilterOff=0";
            lookupURI += "&DefaultType=" + objecttypes;
            lookupURI += "&DisableQuickFind=0";
            lookupURI += "&DisableViewPicker=0";

            window.setTimeout(function () {
                var DialogOption = new b.Xrm.DialogOptions;
                DialogOption.width = 550; DialogOption.height = 550;
                b.Xrm.Internal.openDialog(lookupURI, DialogOption, null, null, (lkpCallback instanceof Function)? lkpCallback : null);
            }, 200);
        };

        //endregion
    });
} else {
    b.Xrm.Page.ui.tabs.get("Tab_Measures").setVisible(false);
}