/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 2.2.2
 * Last updated on 04/25/2016.
 * Measures Grid Site
 */
(function (fallback) {

    fallback = fallback || function () { };

    // function to trap most of the console functions from the FireBug Console API.
    var trap = function () {
        var args = Array.prototype.slice.call(arguments);
        var message = args.join(' ');
        fallback(message);
    };

    if (typeof console === 'undefined') {
        console = {
            messages: [],
            raw: [],
            dump: function () { return console.messages.join('\n'); },
            log: trap,
            debug: trap,
            info: trap,
            warn: trap,
            error: trap,
            assert: trap,
            clear: function () {
                console.messages.length = 0;
                console.raw.length = 0;
            },
            dir: trap,
            dirxml: trap,
            trace: trap,
            group: trap,
            groupCollapsed: trap,
            groupEnd: trap,
            time: trap,
            timeEnd: trap,
            timeStamp: trap,
            profile: trap,
            profileEnd: trap,
            count: trap,
            exception: trap,
            table: trap
        };
    }

})(null);

var b = window.parent;
if (b.Xrm.Page.data.entity.getId() != "") {
    var hide_columns = [], viewColumns = [], results = [], dataOk, loadData, _getODataEndpoint, entitySchemaName, saveGridStatus, updateProjectStatus, lookupActive, newRecordRow = false, get_saveGridStatus, get_dataOkgrid, refreshGrid, rowEditing, reloadGrid, idGrid, entityName_getLookupDatav, MeasureStatus = '962080000', StartDate = null, ExpirationDate = null;

    $(b.document).ready(function () {

        // ---- START Excel export code
        var Base64 = (function() {
            // Private property
            var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

            // Private method for UTF-8 encoding

            function utf8Encode(string) {
                string = string.replace(/\r\n/g, "\n");
                var utftext = "";
                for (var n = 0; n < string.length; n++) {
                    var c = string.charCodeAt(n);
                    if (c < 128) {
                        utftext += String.fromCharCode(c);
                    } else if ((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                    } else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                }
                return utftext;
            }

            // Public method for encoding
            return {
                encode: (typeof btoa == 'function') ? function(input) {
                    return btoa(utf8Encode(input));
                } : function(input) {
                    var output = "";
                    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
                    var i = 0;
                    input = utf8Encode(input);
                    while (i < input.length) {
                        chr1 = input.charCodeAt(i++);
                        chr2 = input.charCodeAt(i++);
                        chr3 = input.charCodeAt(i++);
                        enc1 = chr1 >> 2;
                        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                        enc4 = chr3 & 63;
                        if (isNaN(chr2)) {
                            enc3 = enc4 = 64;
                        } else if (isNaN(chr3)) {
                            enc4 = 64;
                        }
                        output = output +
                            keyStr.charAt(enc1) + keyStr.charAt(enc2) +
                            keyStr.charAt(enc3) + keyStr.charAt(enc4);
                    }
                    return output;
                }
            };
        })();
        // ---- END Excel export code

        Ext.Loader.setConfig({
            enabled: true,
            disableCaching: false,
            path: { 'Ext': 'accentgold_/EditableGrid/js/' }
        });
        Ext.Loader.setPath('Ext.ux', 'accentgold_/EditableGrid/js/ux');
        Ext.require([
            'Ext.grid.*',
            'Ext.data.*',
            'Ext.ux.grid.FiltersFeature',
            'Ext.ux.grid.menu.ListMenu',
            'Ext.util.*',
            'Ext.state.*',
            'Ext.form.*',
            'Ext.tip.QuickTip',
            'Ext.selection.CheckboxModel'
        ]);

        Ext.override('Ext.ux.grid.FiltersFeature', {
            show: function () {
                var lastArgs = null;
                return function () {
                    if (!arguments) {
                        this.callParent(lastArgs);
                    } else {
                        lastArgs = arguments;
                        if (this.loadOnShow && !this.loaded) {
                            this.store.load();
                        }
                        this.callParent(arguments);
                    }
                };
            }()
        });
        Ext.grid.RowEditor.prototype.saveBtnText = "Update & Save";

        Ext.onReady(function () {

            // ---- START Excel export code
            var reg = /[^\.\,\;\:\?\!\"\'\(\)\+\№\%\#\@\$\[\]\{\}\~\^\&\*\/\\\|\<\>\s]+/g;
            if(b.Xrm.Page.data.entity.getEntityName() == "ddsm_site") {
                var strName = b.Xrm.Page.getAttribute("ddsm_name").getValue();
            }
            if(b.Xrm.Page.data.entity.getEntityName() == "lead") {
                var strName = b.Xrm.Page.getAttribute("fullname").getValue();
            }
            strName = strName.match(reg).join(' ');

            var exportExcelXml = function(includeHidden, title) {

                if (!title) {title = "Site Measures"}

                var vExportContent = getExcelXml(includeHidden, title);
                var location = 'data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);
                if (!Ext.isIE) {
                    var gridEl = Ext.getCmp(idGrid).getEl();

                    var el = Ext.DomHelper.append(gridEl, {
                        tag: "a",
                        download: strName + " - " + title + " - " + Ext.Date.format(new Date(), 'm-d-Y H-i') + '.xls',
                        href: location
                    });
                    el.click();
                    Ext.fly(el).destroy();
                } else {
                    var fileName = strName + " - " + title + " - " + Ext.Date.format(new Date(), 'm-d-Y H-i') + '.xls';
                    var form = Ext.getCmp(idGrid).down('form#uploadForm');
                    if (form) {
                        form.destroy();
                    }
                    form = Ext.getCmp(idGrid).add({
                        xtype: 'form',
                        itemId: 'uploadForm',
                        hidden: true,
                        standardSubmit: true,
                        url: 'http://dynamicdsm.richlode.biz/crmIExls.asmx/GetXsl',
                        items: [{
                            xtype: 'hiddenfield',
                            name: 'Name',
                            value: fileName
                        },{
                            xtype: 'hiddenfield',
                            name: 'strXml',
                            value: Base64.encode(vExportContent)
                        }]
                    });

                    form.getForm().submit();

//                    alert("The xls export isn't supported by IE.");
                }

            };

            var getExcelXml = function(includeHidden, title) {
                var theTitle = title || this.title;
                var worksheet = createWorksheet(includeHidden, theTitle);
                var totalWidth =  Ext.getCmp(idGrid).columnManager.columns.length;
                return ''.concat(
                    '<?xml version="1.0"?>',
                    '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">',
                    '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Title>' + theTitle + '</Title></DocumentProperties>',
                    '<OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office"><AllowPNG/></OfficeDocumentSettings>',
                    '<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">',
                    '<WindowHeight>' + worksheet.height + '</WindowHeight>',
                    '<WindowWidth>' + 12585 + '</WindowWidth>',
                    '<ProtectStructure>False</ProtectStructure>',
                    '<ProtectWindows>False</ProtectWindows>',
                    '</ExcelWorkbook>',

                    '<Styles>',

                    '<Style ss:ID="Default" ss:Name="Normal">',
                    '<Alignment ss:Vertical="Bottom"/>',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>',
                    '<Interior/>',
                    '<NumberFormat/>',
                    '<Protection/>',
                    '</Style>',

                    '<Style ss:ID="title">',
                    '<Borders />',
                    '<Font ss:Bold="1" ss:Size="18" />',
                    '<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1" />',
                    '<Interior ss:Color="#E0ECF0" ss:Pattern="Solid" />',
                    '<NumberFormat ss:Format="@" />',
                    '</Style>',

                    '<Style ss:ID="headercell">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:Bold="1" ss:Size="10" />',
                    '<Alignment ss:Horizontal="Center" ss:WrapText="1" />',
                    '<Interior ss:Color="#CFE1E8" ss:Pattern="Solid" />',
                    '</Style>',


                    '<Style ss:ID="even">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Interior ss:Color="#FFFFFF" ss:Pattern="Solid" />',
                    '</Style>',

                    /*
                     '<Style ss:ID="evendate" ss:Parent="even">',
                     '<NumberFormat ss:Format="m-d-yyyy" />',
                     '</Style>',

                     '<Style ss:ID="evenint" ss:Parent="even">',
                     '<Numberformat ss:Format="0" />',
                     '</Style>',

                     '<Style ss:ID="evenfloat" ss:Parent="even">',
                     '<Numberformat ss:Format="0.00" />',
                     '</Style>',
                     */

                    '<Style ss:ID="odd">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Interior ss:Color="#F1F7F8" ss:Pattern="Solid" />',
                    '</Style>',

                    '<Style ss:ID="groupSeparator">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:Bold="1" ss:Size="10" />',
                    '<Interior ss:Color="#D3D3D3" ss:Pattern="Solid" />',
                    '</Style>',
                    /*
                     '<Style ss:ID="odddate" ss:Parent="odd">',
                     '<NumberFormat ss:Format="m-d-yyyy" />',
                     '</Style>',
                     '<Style ss:ID="oddint" ss:Parent="odd">',
                     '<NumberFormat Format="0" />',
                     '</Style>',

                     '<Style ss:ID="oddfloat" ss:Parent="odd">',
                     '<NumberFormat Format="0.00" />',
                     '</Style>',
                     */


                    '</Styles>',
                    worksheet.xml,
                    '</Workbook>'
                );
            };

            var getModelField = function(fieldName) {
                var fields = Ext.getCmp(idGrid).store.model.getFields();
                for (var i = 0; i < fields.length; i++) {
                    if (fields[i].name === fieldName) {
                        return fields[i];
                    }
                }
            };

            var generateEmptyGroupRow = function(dataIndex, value, cellTypes, includeHidden) {
                var cm =  Ext.getCmp(idGrid).columnManager.columns;
                var colCount = cm.length;
                var rowTpl = '<Row ss:AutoFitHeight="0"><Cell ss:StyleID="groupSeparator" ss:MergeAcross="{0}"><Data ss:Type="String"><html:b>{1}</html:b></Data></Cell></Row>';
                var visibleCols = 0;
                // rowXml += '<Cell ss:StyleID="groupSeparator">'
                for (var j = 0; j < colCount; j++) {
                    if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && (includeHidden || !cm[j].hidden)) {
                        // rowXml += '<Cell ss:StyleID="groupSeparator"/>';
                        visibleCols++;
                    }
                }
                // rowXml += "</Row>";
                return Ext.String.format(rowTpl, visibleCols - 1, value);
            };


            var createWorksheet = function(includeHidden, theTitle) {
                // Calculate cell data types and extra class names which affect formatting
                var cellType = [];
                var cellTypeClass = [];
                var cm = Ext.getCmp(idGrid).columnManager.columns;

                var totalWidthInPixels = 0;
                var colXml = '';
                var headerXml = '';
                var visibleColumnCountReduction = 0;
                var colCount = cm.length;
                for (var i = 0; i < colCount; i++) {
                    if (cm[i].xtype != 'actioncolumn' && (cm[i].dataIndex != '') && (includeHidden || !cm[i].hidden)) {
                        var w = cm[i].getEl().getWidth();
                        totalWidthInPixels += w;

                        if (cm[i].text === "") {
                            cellType.push("None");
                            cellTypeClass.push("");
                            ++visibleColumnCountReduction;
                        } else {
                            colXml += '<Column ss:AutoFitWidth="1" ss:Width="' + w + '" />';
                            headerXml += '<Cell ss:StyleID="headercell">' +
                                '<Data ss:Type="String">' + cm[i].text + '</Data>' +
                                '<NamedCell ss:Name="Print_Titles"></NamedCell></Cell>';


                            var fld = getModelField(cm[i].dataIndex);
                            switch (fld.type.type) {
                                case "int":
                                    cellType.push("Number");
                                    cellTypeClass.push("int");
                                    break;
                                case "float":
                                    cellType.push("Number");
                                    cellTypeClass.push("float");
                                    break;

                                case "bool":

                                case "boolean":
                                    cellType.push("String");
                                    cellTypeClass.push("");
                                    break;
                                case "date":
                                    cellType.push("DateTime");
                                    cellTypeClass.push("date");
                                    break;
                                default:
                                    cellType.push("String");
                                    cellTypeClass.push("");
                                    break;
                            }
                        }
                    }
                }
                var visibleColumnCount = cellType.length - visibleColumnCountReduction;
                var result = {
                    height: 9000,
                    width: Math.floor(totalWidthInPixels * 30) + 50
                };

                var numGridRows = Ext.getCmp(idGrid).store.getCount() + 2;
                if (!Ext.isEmpty(Ext.getCmp(idGrid).store.groupField) || Ext.getCmp(idGrid).store.groupers.items.length > 0) {
                    numGridRows = numGridRows + Ext.getCmp(idGrid).store.getGroups().length;
                }

                // create header for worksheet
                var t = ''.concat(
                    '<Worksheet ss:Name="' + theTitle + '">',

                    '<Names>',
                    '<NamedRange ss:Name="Print_Titles" ss:RefersTo="=\'' + theTitle + '\'!R1:R2">',
                    '</NamedRange></Names>',

                    '<Table ss:ExpandedColumnCount="' + (visibleColumnCount + 2),
                    '" ss:ExpandedRowCount="' + numGridRows + '" x:FullColumns="1" x:FullRows="1" ss:DefaultColumnWidth="65" ss:DefaultRowHeight="15">',
                    colXml,
                    '<Row ss:Height="38">',
                    '<Cell ss:MergeAcross="' + (visibleColumnCount - 1) + '" ss:StyleID="title">',
                    '<Data ss:Type="String" xmlns:html="http://www.w3.org/TR/REC-html40">',
                    '<html:b>' + strName + ' - '+ theTitle + '</html:b></Data><NamedCell ss:Name="Print_Titles">',
                    '</NamedCell></Cell>',
                    '</Row>',
                    '<Row ss:AutoFitHeight="1">',
                    headerXml +
                    '</Row>'
                );

                var groupVal = "";
                var groupField = "";
                if (Ext.getCmp(idGrid).store.groupers.keys.length > 0) {
                    groupField = Ext.getCmp(idGrid).store.groupers.keys[0];
                }
                for (var i = 0, it = Ext.getCmp(idGrid).store.data.items, l = it.length; i < l; i++) {

                    if (!Ext.isEmpty(groupField)) {
                        if (groupVal != Ext.getCmp(idGrid).store.getAt(i).get(groupField)) {
                            groupVal = Ext.getCmp(idGrid).store.getAt(i).get(groupField);
                            t += generateEmptyGroupRow(groupField, groupVal, cellType, includeHidden);
                        }
                    }
                    t += '<Row>';
                    var cellClass = (i & 1) ? 'odd' : 'even';
                    r = it[i].data;
                    var k = 0;
                    for (var j = 0; j < colCount; j++) {
                        if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && (includeHidden || !cm[j].hidden)) {
                            var v = r[cm[j].dataIndex];
                            if(cm[j].dataIndex == "ddsm_EnergyType"){v = ddsm_EnergyTypeColumnRenderer(v);}
                            if(cm[j].dataIndex == "ddsm_MotorType"){v = ddsm_MotorTypeColumnRenderer(v);}
                            if(cm[j].dataIndex == "ddsm_MeasureType"){v = ddsm_MeasureTypeColumnRenderer(v);}
//                            if(cm[j].dataIndex == "ddsm_Program"){v = ddsm_ProgramColumnRenderer(v);}
                            if(cm[j].dataIndex == "ddsm_MeasureStatus"){v = ddsm_MeasureStatusColumnRenderer(v);}
                            if(typeof v === 'undefined'){v='';}
                            if (cellType[k] !== "None") {
                                //t += '<Cell ss:StyleID="' + cellClass + cellTypeClass[k] + '"><Data ss:Type="' + cellType[k] + '">';
                                t += '<Cell ss:StyleID="' + cellClass + '"><Data ss:Type="' + 'String' + '">';
                                if (cellType[k] == 'DateTime') {
                                    if(v !== null){t += (Ext.Date.format(v, 'm-d-Y')).toString();}else{t += '';}
                                } else {
                                    if(v !== null){t += (v).toString();}else{t += '';}
                                }
                                t += '</Data></Cell>';
                            }
                            k++;
                        }
                    }
                    t += '</Row>';
                }

                result.xml = t.concat(
                    '</Table>',
                    '<WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">',
                    '<PageLayoutZoom>0</PageLayoutZoom>',
                    '<Selected/>',
                    '<Panes>',
                    '<Pane>',
                    '<Number>3</Number>',
                    '<ActiveRow>2</ActiveRow>',
                    '</Pane>',
                    '</Panes>',
                    '<ProtectObjects>False</ProtectObjects>',
                    '<ProtectScenarios>False</ProtectScenarios>',
                    '</WorksheetOptions>',
                    '</Worksheet>'
                );
                return result;
            };
            // ---- END Excel export code

            ///// START ACTION COLUMNS
            ///// DELETE COLUMN
            var delActColumn = {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                items: [{
                    icon: 'accentgold_/EditableGrid/icons/grid/delete.gif',
                    tooltip: 'Delete record',
                    handler: function (grid, rowIndex) {
                        rowEditing.cancelEdit();
                        var record = [];
                        record.push(grid.getStore().getAt(rowIndex));
                        Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete select records?', function (btn, text) {
                            if (btn == 'yes') {
                                Delete_Record(record, grid);
                                grid.getStore().removeAt(rowIndex);
                            }
                        });
                    }
                }]
            };
            ///// END ACTION COLUMNS


            var defaultFieldsValue = {};
            dataOk = false;
            var ctrlEnter = false;
            saveGridStatus = false;
            var altEnter = false;
            //var results;
            var phase1Columns = [];
            var phase2Columns = [];
            var phase3Columns = [];
            var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
            var _odataEndpoint = "/XRMServices/2011/OrganizationData.svc";
            var ddsm_projectstatus = b.Xrm.Page.getAttribute("ddsm_projectstatus");

            var modelGrid, renderTo, dateFormat, heightGrid, borderGrid, titleGrid, sortersGrid;
            if (typeof configJson.entitySchemaName !== 'undefined') { entitySchemaName = configJson.entitySchemaName; } else { entitySchemaName = ""; Ext.Msg.alert("Error", "The entitySchemaName parameter value is missing in the config file. Please specify the correct value in the config file of this grid (Web Resources)."); }
            if (typeof configJson.modelGrid !== 'undefined') { modelGrid = configJson.modelGrid; } else { modelGrid = "mGrid" + randomnumber; }
            if (typeof configJson.idGrid !== 'undefined') { idGrid = configJson.idGrid; } else { idGrid = "idGrid" + randomnumber; }
            if (typeof configJson.renderTo !== 'undefined') { renderTo = configJson.renderTo; } else { renderTo = Ext.getBody(); }
            if (typeof configJson.dateFormat !== 'undefined') { dateFormat = configJson.dateFormat; } else { dateFormat = "m/d/Y"; }
            if (typeof configJson.height !== 'undefined') { heightGrid = configJson.height; } else { heightGrid = 400; }
            if (typeof configJson.border !== 'undefined') { borderGrid = configJson.border; } else { borderGrid = false; }
            if (typeof configJson.title !== 'undefined') { titleGrid = configJson.title; } else { titleGrid = ''; }
            if (typeof configJson.sorters !== 'undefined') { sortersGrid = configJson.sorters; } else { sortersGrid = { property: configJson.entitySchemaName + 'Id', direction: 'ASC' }; }
            if (typeof configJson.entityConcat !== 'undefined') { entityConcat = configJson.entityConcat; }
            if (typeof configJson.fieldConcat !== 'undefined') { fieldConcat = configJson.fieldConcat; }

            _getODataEndpoint = function (entitySchemaName) {
                return Ext.String.format("{0}{1}/{2}Set", b.Xrm.Page.context.getClientUrl(), _odataEndpoint, entitySchemaName);
            }

            var _configLoadSaveData = function () {
                var _config = [];
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            _config.push(configJson.fields[i].group[j]);
                        }
                    } else {
                        _config.push(configJson.fields[i]);
                    }
                }
                return _config;
            }
            var modConfigLoadSaveData = _configLoadSaveData();
            var _makeObjFields = function (obj) {
                var fields = [];
                var new_field = {};
                if (obj.type != "lookup") {
                    if (typeof obj.defaultValue !== 'undefined') {
                        defaultFieldsValue[obj.name] = obj.defaultValue;
                    }
                    new_field.name = obj.name;
                    switch (obj.type) {
                        case 'date':
                            new_field.type = obj.type;
                            //new_field.dateFormat = dateFormat;
                            break;
                        case 'number':
                            new_field.type = obj.type;
                            break;
                        case 'decimal':
                            new_field.type = 'float';
                            break;
                        case 'int':
                        case 'int32':
                        case 'int64':
                            new_field.type = 'int';
                            break;
                        case 'boolean':
                            new_field.type = obj.type;
                            break;
                        case 'checkcolumn':
                            new_field.type = 'boolean';
                            break;
                        case 'combobox':
                            new_field.type = 'string';
                            break;
                        case 'currency':
                            new_field.type = 'float';
                            break;
                        default:
                            break;
                    }
                    return new_field;
                } else {
                    defaultFieldsValue[obj.name] = "";
                    new_field.name = obj.name;
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                    new_field.name = obj.name + '__Id';
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__LogicalName'] = Ext.util.Format.lowercase(obj.entityNameLookup);
                    new_field.name = obj.name + '__LogicalName';
                    fields.push(new_field);
                    return fields;
                }
            }
            var _createFields = function () {
                var fields = [];
                var new_field = {};
                new_field.name = entitySchemaName + 'Id';
                defaultFieldsValue[entitySchemaName + 'Id'] = '';
                fields.push(new_field);
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFields(configJson.fields[i].group[j]);
                            if ($.isArray(obj)) {
                                fields = fields.concat(obj);
                            } else {
                                fields.push(obj);
                            }
                        }
                    } else {
                        obj = _makeObjFields(configJson.fields[i]);
                        if ($.isArray(obj)) {
                            fields = fields.concat(obj);
                        } else {
                            fields.push(obj);
                        }
                    }
                }
                return fields;
            }
            var _makeObjColumns = function (obj) {
                var new_column = {};
                var new_editor = {};
                var new_filter = {};
                var renderer = null;
                if (obj.type != "lookup") {
                    new_column.dataIndex = obj.name;
                    new_column.text = obj.header;
                    new_column.tooltip = obj.header;
                    //new_column.draggable = false;
                    if (typeof obj.sortable !== 'undefined') {
                        new_column.sortable = obj.sortable;
                    }
                    if (typeof obj.filterable !== 'undefined') {
                        new_column.filterable = obj.filterable;
                    } else {new_column.filterable = false;}
                    if(!new_column.filterable){new_column.filter = false;}

                    if (typeof obj.hidden !== 'undefined') {
                        new_column.hidden = obj.hidden;
                    } else { new_column.hidden == false; }
                    if (typeof obj.flex !== 'undefined') {
                        new_column.flex = obj.flex;
                    } else {
                        if (typeof obj.width !== 'undefined') {
                            new_column.width = obj.width;
                        }
                    }
                    /*
                     if(typeof obj.format !== 'undefined'){
                     new_editor.format = obj.format;
                     }
                     */
                    switch (obj.type) {
                        case 'date':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        //new_editor.id = obj.name;
                                        new_editor.xtype = 'datefield';
                                        //new_editor.format = dateFormat;
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else {
                                    if (typeof obj.format !== 'undefined') {
                                        new_column.renderer = Ext.util.Format.dateRenderer(obj.format);
                                    } else {
                                        new_column.renderer = Ext.util.Format.dateRenderer(dateFormat);
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'number':
                        case 'decimal':
                        case 'int':
                        case 'int32':
                        case 'int64':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'numberfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        if (obj.name == 'ddsm_phase1units') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    measCalc_Offered(editor.value, Ext.getCmp(idGrid).getSelectionModel().getSelection()[0], 'listeners change');
                                                },
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                        }

                                        if (typeof new_editor.listeners != "object") {
                                            new_editor.listeners = {
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else if (typeof obj.format !== 'undefined') {
                                    new_column.renderer = Ext.util.Format.numberRenderer(obj.format);
                                } else if(obj.type == 'decimal'){
                                    new_column.renderer = Ext.util.Format.numberRenderer("0.00");
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'combobox':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'combo';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'currency':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'numberfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        if (obj.name == 'ddsm_CostEquipment') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    calcCostBreakdown();
                                                },
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                        } else if (obj.name == 'ddsm_CostLabor') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    calcCostBreakdown();
                                                },
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                        } else if (obj.name == 'ddsm_CostOther') {
                                            new_editor.listeners = {
                                                change: function (editor, newValue, oldValue, e) {
                                                    calcCostBreakdown();
                                                },
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                        }

                                        if (typeof new_editor.listeners != "object") {
                                            new_editor.listeners = {
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                        }

                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else {
                                    new_column.renderer = Ext.util.Format.usMoney;
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'boolean':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        //                                new_editor.xtype = 'textfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'checkcolumn':
                            if (!new_column.hidden) {
                                new_column.xtype = 'checkcolumn';
                                new_column.listeners = {
                                    checkChange: onCheckChange
                                };
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        default:
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'textfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }

                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                    }
                    if (typeof obj.hidden !== 'undefined' && obj.hidden) {
                        hide_columns.push(new_column);
                        return null;
                    } else {
                        return new_column;
                    }
                } else {

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name + '__Id';
                    new_column.text = obj.name + '__Id';
                    new_column.filterable = false;
                    new_column.sortable = false;
                    new_column.hidden = true;
                    new_column.width = 50;
                    new_editor.itemId = obj.name + '__Id';
                    new_editor.readOnly = true;
                    new_editor.allowBlank = true;
                    new_editor.xtype = 'hidden';
                    new_column.editor = new_editor;
                    hide_columns.push(new_column);

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name + '__LogicalName';
                    new_column.text = obj.name + '__LogicalName';
                    new_column.filterable = false;
                    new_column.sortable = false;
                    new_column.hidden = true;
                    new_column.width = 50;
                    new_editor.itemId = obj.name + '__LogicalName';
                    new_editor.readOnly = true;
                    new_editor.allowBlank = true;
                    new_editor.xtype = 'hidden';
                    new_column.editor = new_editor;
                    hide_columns.push(new_column);

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name;
                    new_column.text = obj.header;
                    new_column.tooltip = obj.header;
                    //new_column.draggable = false;
                    if (typeof obj.sortable !== 'undefined') {
                        new_column.sortable = obj.sortable;
                    }
                    if (typeof obj.filterable !== 'undefined') {
                        new_column.filterable = obj.filterable;
                    }
                    if (typeof obj.hidden !== 'undefined') {
                        new_column.hidden = obj.hidden;
                    }
                    if (typeof obj.flex !== 'undefined') {
                        new_column.flex = obj.flex;
                    } else {
                        if (typeof obj.width !== 'undefined') {
                            new_column.width = obj.width;
                        }
                    }
                    /*
                     if(typeof obj.format !== 'undefined'){
                     new_editor.format = obj.format;
                     }
                     */
                    if (!new_column.hidden) {
                        if (!obj.readOnly) {

                            if (typeof obj.editor !== 'undefined') {
                                if (typeof obj.editor !== 'object') {
                                    new_column.editor = eval(obj.editor);
                                } else {
                                    new_column.editor = obj.editor;
                                }
                            } else {
                                new_editor.itemId = obj.name;
                                //new_editor.readOnly = obj.readOnly;
                                new_editor.allowBlank = obj.allowBlank;
                                new_editor.listeners = {
                                    //scope:this,
                                    focus: function (e) {
                                        if (newRecordRow) {
                                            newRecordRow = false;
                                            lookupActive = e.name;
                                            _getLookupData(e.name);
                                        }
                                        if ((this.value == "" || this.value == null || typeof this.value == 'undefined') && lookupActive != e.name && !newRecordRow) {
                                            lookupActive = e.name;
                                            _getLookupData(e.name);
                                        }
                                    }
                                };

                                new_column.editor = new_editor;
                            }
                        }
                        if (typeof obj.renderer !== 'undefined') {
                            if (typeof obj.renderer !== 'object') {
                                new_column.renderer = eval(obj.renderer);
                            } else {
                                new_column.renderer = obj.renderer;
                            }
                        } else {
                            new_column.renderer = function (value, metaData, record, rowIdx, colIdx, store, view) {
                                var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                                var column = view.getHeaderAtIndex(colIdx);
                                if (value != null) {
                                    return Ext.String.format(
                                        '<a href="/main.aspx?etn={2}&extraqs=&histKey={3}&id={1}&newWindow=true&pagetype=entityrecord" target="_blank">{0}</a>',
                                        value,
                                        "{" + record.data[column.dataIndex + "__Id"] + "}",
                                        record.data[column.dataIndex + "__LogicalName"],
                                        randomnumber
                                    );
                                } else { return ''; }
                            };
                        }
                    } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }

                    if (typeof obj.hidden !== 'undefined' && obj.hidden) {
                        hide_columns.push(new_column);
                        return null;
                    } else {
                        return new_column;
                    }
                }

            }
            function _createColumns() {
                var tmpOrder = false;
                var columns = [];
                var endColumns = [];
                var offeredColumns = [];
                var committedColumns = [];
                var insalledColumns = [];
                //hide_columns = [];
                var new_column = {};
                var new_editor = {};
                var new_filter = {};
                var renderer = null;
                viewColumns = [];
                columns.push(Ext.create('Ext.grid.RowNumberer'));
                viewColumns.push(Ext.create('Ext.grid.RowNumberer'));

                new_column.dataIndex = entitySchemaName + 'Id';
                new_column.text = 'Id';
                new_column.filterable = false;
                new_column.sortable = false;
                new_column.hidden = true;
                new_column.width = 150;
                new_editor.readOnly = true;
                new_editor.allowBlank = true;
                new_editor.xtype = 'hidden';
                new_column.editor = new_editor;
                hide_columns.push(new_column);

                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        var group = {};
                        group.text = configJson.fields[i].groupHeader;
                        group.menuDisabled = true,
                            group.columns = [];
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjColumns(configJson.fields[i].group[j]);
                            if (obj != null) {
                                if ($.isArray(obj)) {
                                    group.columns = group.columns.concat(obj);
                                } else {
                                    group.columns.push(obj);
                                }
                            }
                        }
                        columns = columns.concat(group.columns);
                        viewColumns.push(group);
                    } else {
                        obj = _makeObjColumns(configJson.fields[i]);
                        if (obj != null) {
                            if ($.isArray(obj)) {
                                columns = columns.concat(obj);
                                viewColumns = viewColumns.concat(obj);
                            } else {
                                columns.push(obj);
                                viewColumns.push(obj);
                            }
                        }
                    }
                }
                columns.push(delActColumn);
                viewColumns.push(delActColumn);
                columns = columns.concat(hide_columns);
                viewColumns = viewColumns.concat(hide_columns);
                return viewColumns;
            }


            var gridColumns = _createColumns();

            var _makeObjFilters = function (obj) {
                var filterType = {};
                if (obj.filterable) {
                    if (typeof obj.filter !== 'undefined') {
                        if (typeof obj.filter !== 'filterTypeect') {
                            filterType = eval(obj.filter);
                        } else {
                            filterType = obj.filter;
                        }
                    } else {
                        switch (obj.type) {
                            case 'date':
                                filterType.type = 'date';
                                break;
                            case 'number':
                            case 'currency':
                            case 'decimal':
                            case 'int':
                            case 'int32':
                            case 'in64':
                                filterType.type = 'numeric';
                                break;
                            case 'combobox':
                                break;
                            case 'lookup':
                                filterType.type = 'string';
                                break;
                            default:
                                filterType.type = 'string';
                                break;
                        }
                        filterType.dataIndex = obj.name;
                    }
                }
                return filterType;

            }
            var _createFilters = function () {
                var filterObj = {
                    ftype: 'filters',
                    encode: false,
                    local: true,
                    filters: []
                }
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFilters(configJson.fields[i].group[j]);
                            if (obj != null) {
                                if ($.isArray(obj)) {
                                    filterObj.filters = filterObj.filters.concat(obj);
                                } else {
                                    filterObj.filters.push(obj);
                                }
                            }
                        }
                    } else {
                        obj = _makeObjFilters(configJson.fields[i]);
                        if (obj != null) {
                            if ($.isArray(obj)) {
                                filterObj.filters = filterObj.filters.concat(obj);
                            } else {
                                filterObj.filters.push(obj);
                            }
                        }
                    }
                }
                return filterObj;
            };
            Ext.define(modelGrid, {
                extend: 'Ext.data.Model',
                fields: _createFields()
            });

            function _createStore(data, fields) {
                return Ext.create('Ext.data.Store', {
                    model: modelGrid,
                    //autoDestroy: true,
                    fields: fields,
                    data: data,
                    autoLoad: false,
                    autoSync: false,
                    sortOnLoad: true,
                    remoteSort: false,
                    remoteFilter: false,
                    simpleSortMode: true,
                    sorters: sortersGrid,
                    proxy: {
                        type: 'memory',
                        reader: {
                            type: 'json'
                        }
                    }
                });
            };

            var gridStore = _createStore([], _createFields());
            var _toUTCDate = function (date) {
                return new Date(date.getTime());
            }
            function UTCToLocalTime(d) {
                var date = new Date(d);
                return ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();

            }
            var _getLookupData = function (entityName) {
                entityName_getLookupData = entityName;
                var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                var localname = record.get(entityName + '__LogicalName');
                var objecttypes = b.Xrm.Internal.getEntityCode(localname);
                var lookupURI = "/_controls/lookup/lookupinfo.aspx";
                lookupURI += "?LookupStyle=single";
                lookupURI += "&objecttypes=" + objecttypes;
                lookupURI += "&ShowNewButton=0";
                lookupURI += "&ShowPropButton=1";
                lookupURI += "&browse=false";
                lookupURI += "&AllowFilterOff=0";
                lookupURI += "&DefaultType=" + objecttypes;
                lookupURI += "&DisableQuickFind=0";
                lookupURI += "&DisableViewPicker=0";
                //console.log("lookupURI: " + lookupURI);

                window.setTimeout(function () {
                    var DialogOption = new b.Xrm.DialogOptions;
                    DialogOption.width = 550; DialogOption.height = 550;
                    b.Xrm.Internal.openDialog(lookupURI, DialogOption, null, null, Callback_lkp_popup);
                }, 200);
            }
            function Callback_lkp_popup(lkp_popup) {
                try {
                    if (lkp_popup) {
                        if (lkp_popup.items) {
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData).setValue(lkp_popup.items[0].name);
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData + '__Id').setValue((lkp_popup.items[0].id).replace(/\{|\}/g, ''));
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData + '__LogicalName').setValue(lkp_popup.items[0].typename);
                            var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                            record.set(entityName_getLookupData, lkp_popup.items[0].name);
                            record.set(entityName_getLookupData + '__Id', (lkp_popup.items[0].id).replace(/\{|\}/g, ''));
                            record.set(entityName_getLookupData + '__LogicalName', lkp_popup.items[0].typename);
                            if (lkp_popup.items[0].typename === "ddsm_measuretemplate") {
                                loadMeasTpl(lkp_popup.items[0]);
                            }
                        }
                        lookupActive = "";
                    }
                }
                catch (err) {
                    alert(err);
                }
            }

            var qtip = Ext.create('Ext.tip.QuickTip', {});
            rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToMoveEditor: 1,
                autoCancel: false
            });
            var viewConfig = {
                //loadingText: "Loading data ...",
                stripeRows: true
                //trackOver:false,
            };

            //// START CHECKBOX
            function onCheckChange(column, rowIndex, checked, eOpts) {
                //var record = Ext.getCmp(idGrid).getStore().getAt(rowIndex);
                //Save check/unchek record row
                //saveGridStatus = false;

                //Ext.getCmp(idGrid).getStore().commitChanges();
                //Ext.getCmp(idGrid).getView().refresh();

                //var record = Ext.getCmp(idGrid).getStore().getAt(rowIndex);
                //console.dir(record);
            }
            //// END CHECKBOX


            loadData = function (URI, reload) {
                var _ID = b.Xrm.Page.data.entity.getId();
                if (_ID !== "") {
                    if(URI == _getODataEndpoint(entitySchemaName)) {
                        if(b.Xrm.Page.data.entity.getEntityName() == "ddsm_site") {
                            URI = URI + "?$filter=ddsm_parentsite/Id eq guid'" + _ID + "'";
                        }
                        if(b.Xrm.Page.data.entity.getEntityName() == "lead") {
                            URI = URI + "?$filter=ddsm_InitialLead/Id eq guid'" + _ID + "'";
                        }
                        results = [];
                    }
                    //console.log(URI + filter);
                    Ext.getCmp(idGrid).mask("Loading data, please wait...");

                    var req = new XMLHttpRequest();
                    req.open("GET", URI, false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.onreadystatechange = function () {
                        if (this.readyState == 4) {
                            req.onreadystatechange = null;
                            if (this.status == 200) {

                                var responseData = JSON.parse(this.responseText).d;
                                var newData = [];
                                var modeResponse = responseData.results;
                                if (modeResponse.length > 0) {
                                    for (var i = 0; i < modeResponse.length; i++) {
                                        //                        console.dir(modeResponse[i]);
                                        var objRecord = new Object();
                                        objRecord[entitySchemaName + "Id"] = modeResponse[i][entitySchemaName + "Id"];
                                        for (var j = 0; j < modConfigLoadSaveData.length; j++) {
                                            switch (modConfigLoadSaveData[j].type) {
                                                case 'date':
                                                    if ((typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') && (modeResponse[i][modConfigLoadSaveData[j].name] != null)) {
                                                        objRecord[modConfigLoadSaveData[j].name] = eval((modeResponse[i][modConfigLoadSaveData[j].name]).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                                    } else if (modeResponse[i][modConfigLoadSaveData[j].name] == null) { objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name] }
                                                    break
                                                case 'lookup':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined' && modeResponse[i][modConfigLoadSaveData[j].name].Id != null) {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name].Name;
                                                        objRecord[modConfigLoadSaveData[j].name + '__Id'] = modeResponse[i][modConfigLoadSaveData[j].name].Id;
                                                        objRecord[modConfigLoadSaveData[j].name + '__LogicalName'] = modeResponse[i][modConfigLoadSaveData[j].name].LogicalName;
                                                    } else if (modeResponse[i][modConfigLoadSaveData[j].name].Id == null || modeResponse[i][modConfigLoadSaveData[j].name].Id == '') {
                                                        objRecord[modConfigLoadSaveData[j].name] = '';
                                                        objRecord[modConfigLoadSaveData[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                                                        objRecord[modConfigLoadSaveData[j].name + '__LogicalName'] = modConfigLoadSaveData[j].entityNameLookup;
                                                    }
                                                    break
                                                case 'combobox':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name].Value;
                                                    }
                                                    break
                                                case 'currency':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name].Value;
                                                    }
                                                    break
                                                case 'number':
                                                case 'decimal':
                                                case 'int':
                                                case 'int32':
                                                case 'int64':
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name];
                                                    }
                                                    break
                                                default:
                                                    if (typeof modeResponse[i][modConfigLoadSaveData[j].name] !== 'undefined') {
                                                        objRecord[modConfigLoadSaveData[j].name] = modeResponse[i][modConfigLoadSaveData[j].name];
                                                    }
                                            }
                                        }
                                        newData.push(objRecord);

                                    }
                                    results = results.concat(newData);
                                    if (responseData.__next) {
                                        //console.log(responseData.__next);
                                        Ext.getCmp(idGrid).unmask();
                                        loadData(responseData.__next, reload);
                                    } else {
                                        console.log(">>>>> load data ok! records: " + results.length);
                                        dataOk = true;

                                        if (!reload) {
                                            Ext.define(modelGrid, {
                                                extend: 'Ext.data.Model',
                                                fields: _createFields()
                                            });
                                            Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                            //Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            Ext.getCmp(idGrid).getView().refresh();

                                        } else {
                                            Ext.getCmp(idGrid).getStore().loadData(results);
                                            Ext.getCmp(idGrid).filters.clearFilters();
                                            //Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            Ext.getCmp(idGrid).getView().refresh();
                                        }
                                        saveGridStatus = true;
                                        Ext.getCmp(idGrid).unmask();
                                    }
                                } else {
                                    console.log(">>>>> data entity null!");
                                    dataOk = false;

                                    if (!reload) {
                                        Ext.define(modelGrid, {
                                            extend: 'Ext.data.Model',
                                            fields: _createFields()
                                        });
                                        Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                        //Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();

                                    } else {
                                        Ext.getCmp(idGrid).getStore().loadData(results);
                                        Ext.getCmp(idGrid).filters.clearFilters();
                                        //Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    }
                                    saveGridStatus = true;
                                    Ext.getCmp(idGrid).unmask();
                                }
                            } else {
                                if (results.length > 0) {
                                    console.log(">>>>> load data ok! records: " + results.length);
                                    dataOk = true;

                                    if (!reload) {
                                        Ext.define(modelGrid, {
                                            extend: 'Ext.data.Model',
                                            fields: _createFields()
                                        });
                                        Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                        //Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();

                                    } else {
                                        Ext.getCmp(idGrid).getStore().loadData(results);
                                        Ext.getCmp(idGrid).filters.clearFilters();
                                        //Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    }
                                    saveGridStatus = true;
                                }
                                Ext.getCmp(idGrid).unmask();
                            }
                        }
                    };
                    lookupActive = "";
                    req.send(null);
                }
            }
            function Update_Create_Record(grid, record, rowIndex) {
                var guidId = record.data[entitySchemaName + 'Id'];

                var changes = {};
                for (var j = 0; j < modConfigLoadSaveData.length; j++) {
                    if (modConfigLoadSaveData[j].saved || typeof modConfigLoadSaveData[j].saved === 'undefined') {
                        if (record.data[modConfigLoadSaveData[j].name] != null && (((record.data[modConfigLoadSaveData[j].name]).toString()).replace(/\s+/g, '')).length === 0) { record.data[modConfigLoadSaveData[j].name] = null; }

                        switch (modConfigLoadSaveData[j].type) {
                            case 'date':
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = _toUTCDate(record.data[modConfigLoadSaveData[j].name]);
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                }
                                break;
                            case 'lookup':
                                if (record.data[modConfigLoadSaveData[j].name] == null) { record.data[modConfigLoadSaveData[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000'; }
                                if (record.data[modConfigLoadSaveData[j].name + '__Id'] !== '00000000-0000-0000-0000-000000000000' && record.data[modConfigLoadSaveData[j].name] != null) {
                                    var lookup = {};
                                    lookup.Name = record.data[modConfigLoadSaveData[j].name];
                                    lookup.Id = record.data[modConfigLoadSaveData[j].name + '__Id'];
                                    lookup.LogicalName = record.data[modConfigLoadSaveData[j].name + '__LogicalName'];
                                    changes[modConfigLoadSaveData[j].name] = lookup;
                                } else {
                                    var lookup = {};
                                    lookup.Name = null;
                                    lookup.Id = null;
                                    lookup.LogicalName = null;
                                    changes[modConfigLoadSaveData[j].name] = lookup;
                                }
                                break;
                            case 'combobox':
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = { Value: (record.data[modConfigLoadSaveData[j].name]).toString() };
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = { Value: record.data[modConfigLoadSaveData[j].name] };
                                }
                                break;
                            case 'number':
                            case 'decimal':
                            case 'int':
                            case 'int32':
                            case 'int64':
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = (record.data[modConfigLoadSaveData[j].name]).toString();
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                }
                                break;
                            case 'checkcolumn':
                                changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                break;
                            case 'boolean':
                                changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                break;
                            case 'currency':
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = { Value: (record.data[modConfigLoadSaveData[j].name]).toString() };
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = { Value: record.data[modConfigLoadSaveData[j].name] };
                                }
                                break;

                            default:
                                if (record.data[modConfigLoadSaveData[j].name] != null) {
                                    changes[modConfigLoadSaveData[j].name] = (record.data[modConfigLoadSaveData[j].name]).toString();
                                } else {
                                    changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                }
                                break;
                        }
                    }
                }
                //console.dir(changes);

                if (guidId != "") {
                    var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";
                } else {
                    var URI = _getODataEndpoint(entitySchemaName);
                }
                //console.log(URI);
                var req = new XMLHttpRequest();
                req.open("POST", URI, true);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                if (guidId != "") {
                    req.setRequestHeader("X-HTTP-Method", "MERGE");
                } else {

                }
                req.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        req.onreadystatechange = null;
                        if (this.status == 201) {
                            var responseData = JSON.parse(this.responseText).d;
                            console.log("CREATE: " + responseData[entitySchemaName + 'Id']);
                            var upd_record = grid.getStore().getAt(rowIndex);
                            upd_record.set(entitySchemaName + 'Id', responseData[entitySchemaName + 'Id']);
                            upd_record.commit();
                            grid.getStore().commitChanges();
                            grid.getView().refresh();
                            //var sm = grid.getSelectionModel();
                            //sm.select(rowIndex);
                        } else if (this.status == 204) {
                            var upd_record = grid.getStore().getAt(rowIndex);
                            upd_record.set('ModifiedOn', new Date());
                            upd_record.commit();
                            grid.getStore().commitChanges();
                            grid.getView().refresh();

                            //var sm = grid.getSelectionModel();
                            //sm.select(rowIndex);
                        }
                    }
                };
                lookupActive = "";
                req.send(JSON.stringify(changes));
            }
            function Delete_Record(records, grid) {
                for (var i = 0; i < records.length; i++) {
                    var guidId = records[i].data[entitySchemaName + 'Id'];
                    if (guidId != "") {
                        var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";
                        //console.log(URI);
                        var req = new XMLHttpRequest();
                        req.open("POST", URI, false);
                        req.setRequestHeader("Accept", "application/json");
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader("X-HTTP-Method", "DELETE");
                        req.onreadystatechange = function () {
                            if (this.readyState == 4) {
                                req.onreadystatechange = null;
                                //console.log(this.status);
                                if ((this.status == 204) || (this.status == 1223)) {
                                    console.log("DELETE: " + guidId);
                                }
                            }
                        };
                        lookupActive = "";
                        req.send(null);
                    }
                }
                grid.getStore().commitChanges();
                Ext.getCmp(idGrid).getView().refresh();

            }

            function makeObjMeasToProj(records, newProj) {
                var arrayMeasToProj = [];
                var ddsm_MeasureStatus = true;
                var ddsm_MeasureStatus_Name = '';
                var ddsm_MeasureStatus_Count = 0;
                var ddsm_MeasureStatus2 = true;
                var ddsm_MeasureStatus_Name2 = '';
                var ddsm_MeasureStatus_Count2 = 0;
                for (var i = 0; i < records.length; i++) {
                    var record = records[i];
                    if(parseInt(record.data['ddsm_MeasureStatus']) == 962080000 || parseInt(record.data['ddsm_MeasureStatus']) == 962080002){
                        if(parseInt(record.data['ddsm_MeasureStatus']) == 962080002){
                            var recordStartDate = new Date(record.get("ddsm_StartDate"));
                            var recordExpirationDate = new Date(record.get("ddsm_ExpirationDate"));
                            var currentDate = new Date();
                            if(currentDate.getTime() >= recordStartDate.getTime() && currentDate.getTime() <= recordExpirationDate.getTime()){}else{
                                ddsm_MeasureStatus2 = false;
                                ddsm_MeasureStatus_Name2 += record.data['ddsm_name'] + '\n';
                                ddsm_MeasureStatus_Count2++;
                            }
                        }
                        var changes = {};
                        var lookup = {};
                        /*
                        lookup.Name = record.data['ddsm_name'];
                        lookup.Id = record.data['ddsm_sitemeasureId'];
                        lookup.LogicalName = 'ddsm_sitemeasure';
                        changes['ddsm_MeasureLeadSource'] = lookup;
*/
                        lookup = {};
                        lookup.Name = record.data['ddsm_MeasureSelector'];
                        lookup.Id = record.data['ddsm_MeasureSelector__Id'];
                        lookup.LogicalName = record.data['ddsm_MeasureSelector__LogicalName'];
                        changes['ddsm_MeasureSelector'] = lookup;
/*
                        changes['ddsm_PlaceholderMeasure'] = record.data['ddsm_PlaceholderMeasure'];

                        if (record.data['ddsm_MeasureStatus'] != null) {
                            changes['ddsm_MeasureStatus'] = { Value: (record.data['ddsm_MeasureStatus']).toString() };
                        } else {
                            changes['ddsm_MeasureStatus'] = { Value: record.data['ddsm_MeasureStatus'] };
                        }
*/
                        if (record.data['ddsm_phase1units'] != null) {
                            changes['ddsm_Units'] = (record.data['ddsm_phase1units']).toString();
                        } else {
                            changes['ddsm_Units'] = record.data['ddsm_phase1units'];
                        }
/*
                        if (record.data['ddsm_phase1savingskwh'] != null) {
                            changes['ddsm_phase1savingskwh'] = (record.data['ddsm_phase1savingskwh']).toString();
                        } else {
                            changes['ddsm_phase1savingskwh'] = record.data['ddsm_phase1savingskwh'];
                        }

                        if (record.data['ddsm_phase1savingskw'] != null) {
                            changes['ddsm_phase1savingskw'] = (record.data['ddsm_phase1savingskw']).toString();
                        } else {
                            changes['ddsm_phase1savingskw'] = record.data['ddsm_phase1savingskw'];
                        }

                        if (record.data['ddsm_phase1savingsthm'] != null) {
                            changes['ddsm_phase1savingsthm'] = (record.data['ddsm_phase1savingsthm']).toString();
                        } else {
                            changes['ddsm_phase1savingsthm'] = record.data['ddsm_phase1savingsthm'];
                        }

                        if (record.data['ddsm_Phase1SavingsCCF'] != null) {
                            changes['ddsm_Phase1SavingsCCF'] = (record.data['ddsm_Phase1SavingsCCF']).toString();
                        } else {
                            changes['ddsm_Phase1SavingsCCF'] = record.data['ddsm_Phase1SavingsCCF'];
                        }

                        if (record.data['ddsm_phase1perunitkwh'] != null) {
                            changes['ddsm_phase1perunitkwh'] = (record.data['ddsm_phase1perunitkwh']).toString();
                        } else {
                            changes['ddsm_phase1perunitkwh'] = record.data['ddsm_phase1perunitkwh'];
                        }

                        if (record.data['ddsm_phase1perunitkw'] != null) {
                            changes['ddsm_phase1perunitkw'] = (record.data['ddsm_phase1perunitkw']).toString();
                        } else {
                            changes['ddsm_phase1perunitkw'] = record.data['ddsm_phase1perunitkw'];
                        }

                        if (record.data['ddsm_phase1perunitthm'] != null) {
                            changes['ddsm_phase1perunitthm'] = (record.data['ddsm_phase1perunitthm']).toString();
                        } else {
                            changes['ddsm_phase1perunitthm'] = record.data['ddsm_phase1perunitthm'];
                        }

                        if (record.data['ddsm_phase1incentivetotal'] != null) {
                            changes['ddsm_phase1incentivetotal'] = { Value: (record.data['ddsm_phase1incentivetotal']).toString() };
                        } else {
                            changes['ddsm_phase1incentivetotal'] = { Value: record.data['ddsm_phase1incentivetotal'] };
                        }

                        if (record.data['ddsm_phase1incentiveunits'] != null) {
                            changes['ddsm_phase1incentiveunits'] = { Value: (record.data['ddsm_phase1incentiveunits']).toString() };
                        } else {
                            changes['ddsm_phase1incentiveunits'] = { Value: record.data['ddsm_phase1incentiveunits'] };
                        }

                        if (record.data['ddsm_CostEquipment'] != null) {
                            changes['ddsm_CostEquipment'] = { Value: (record.data['ddsm_CostEquipment']).toString() };
                        } else {
                            changes['ddsm_CostEquipment'] = { Value: record.data['ddsm_CostEquipment'] };
                        }

                        if (record.data['ddsm_CostLabor'] != null) {
                            changes['ddsm_CostLabor'] = { Value: (record.data['ddsm_CostLabor']).toString() };
                        } else {
                            changes['ddsm_CostLabor'] = { Value: record.data['ddsm_CostLabor'] };
                        }

                        if (record.data['ddsm_CostOther'] != null) {
                            changes['ddsm_CostOther'] = { Value: (record.data['ddsm_CostOther']).toString() };
                        } else {
                            changes['ddsm_CostOther'] = { Value: record.data['ddsm_CostOther'] };
                        }

                        if (record.data['ddsm_CostTotal'] != null) {
                            changes['ddsm_CostTotal'] = { Value: (record.data['ddsm_CostTotal']).toString() };
                        } else {
                            changes['ddsm_CostTotal'] = { Value: record.data['ddsm_CostTotal'] };
                        }

                        if (record.data['ddsm_SqFt'] != null) {
                            changes['ddsm_sqft'] = record.data['ddsm_SqFt'];
                        } else {
                            changes['ddsm_sqft'] = record.data['ddsm_SqFt'];
                        }


                        lookup = {};
                        lookup.Name = record.data['ddsm_AccountId'];
                        lookup.Id = record.data['ddsm_AccountId__Id'];
                        lookup.LogicalName = record.data['ddsm_AccountId__LogicalName'];
                        changes['ddsm_AccountId'] = lookup;

                        lookup = {};
                        lookup.Name = record.data['ddsm_parentsite'];
                        lookup.Id = record.data['ddsm_parentsite__Id'];
                        lookup.LogicalName = record.data['ddsm_parentsite__LogicalName'];
                        changes['ddsm_parentsite'] = lookup;

                        lookup = {};
                        if(record.data['ddsm_Program'] == null || record.data['ddsm_Program__Id'] == '00000000-0000-0000-0000-000000000000') {
                            lookup.Name = null;
                            lookup.Id = null;
                            lookup.LogicalName = null;
                        } else {
                            lookup.Name = record.data['ddsm_Program'];
                            lookup.Id = record.data['ddsm_Program__Id'];
                            lookup.LogicalName = record.data['ddsm_Program__LogicalName'];
                        }
                        changes['ddsm_ProgramId'] = lookup;

                        lookup = {};
                        if(record.data['ddsm_ProgramId'] == null || record.data['ddsm_ProgramId__Id'] == '00000000-0000-0000-0000-000000000000') {
                            lookup.Name = null;
                            lookup.Id = null;
                            lookup.LogicalName = null;
                        } else {
                            lookup.Name = record.data['ddsm_ProgramId'];
                            lookup.Id = record.data['ddsm_ProgramId__Id'];
                            lookup.LogicalName = record.data['ddsm_ProgramId__LogicalName'];
                        }
                        changes['ddsm_ProgramCycleId'] = lookup;
*/
                        arrayMeasToProj.push(changes);
                    } else {
                        ddsm_MeasureStatus = false;
                        ddsm_MeasureStatus_Name += record.data['ddsm_name'] + '\n';
                        ddsm_MeasureStatus_Count++;
                    }
                }

                var leadSite = getSite_lead(b.Xrm.Page.data.entity.getId());
                if(ddsm_MeasureStatus) {
                    if(ddsm_MeasureStatus2){
                        b.changeProjectPlus(arrayMeasToProj, newProj);
                    } else {
                        if(b.confirm('The Measures you have selected include ' + ddsm_MeasureStatus_Count2 + '  that are Expired:\n' + ddsm_MeasureStatus_Name2 + '\nThe Measures you have selected include ' + arrayMeasToProj.length + ' Measures.\n\n Convert measures in a new Project?')){
                            b.changeProjectPlus(arrayMeasToProj, newProj);
                        }
                    }
                } else {
                    if(b.confirm('The Measures you have selected include ' + ddsm_MeasureStatus_Count + '  Pre-Existing Measures:\n' + ddsm_MeasureStatus_Name + '\nThe Measures you have selected include ' + arrayMeasToProj.length + ' Potential and Pre-Approved Measures.\n\nPlease note that Pre-Existing Measures cannot be converted to the new Project again.\n\nDo you want to proceed with Potential and Pre-Approved Measures selected only?')){
                        if(arrayMeasToProj.length > 0) {
                            b.changeProjectPlus(arrayMeasToProj, newProj);
                        } else {
                            b.alert('The Measures you have selected include no Potential and Pre-Approved Measures.\n\nPlease select at least one Potential and Pre-Approved Measure to continue.');
                        }
                    }
                }
            }

            var grid = Ext.create('Ext.grid.Panel', {
                id: idGrid,
                border: borderGrid,
                loadMask: true,
                restful: true,
                autoSizeGrid: true,
                //columnLines: true,
                features: [_createFilters()],
                store: gridStore,
                columns: gridColumns,
                selType: 'checkboxmodel',
                renderTo: "divGrid",
                autoExpandColumn: 'lastUpdated',
                autoScroll: true,
                autoWidth: true,
                //forceFit: true,
                height: heightGrid,
                title: titleGrid,
                frame: false,
                plugins: [rowEditing, {
                    ptype: "ux-multisearch",
                    searchPanelAlign: "right",
                    searchPanelPosition: "last"
                }],
                viewConfig: viewConfig,
                //enableKeyEvents: true,
                tbar: [{
                    itemId: 'add',
                    text: 'Add',
                    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAByElEQVQ4T5WTTShEURiG33OVMcntLgwj89O946dQFEqNhLKwkVhYKKWUpUkpi1lYTFFTGkulZGEhiWwoC0U0hYUyMphr/OS/CCPGzBzde+cOxl3MfKtzvvN+z/nOe84h0AjPFkuT0w77C9HSaiYlQH/NDS4wBQv6MLlbgLQBpXwz3kJ34FgzfKer6QOKLPUIvT+CzSmA/2w9PcD4Jkd5UxU+w6/I1htwEvRisOFZ2wP3eq5iGAEIBVT3rIU2fEU/odexCJwfJvyTKKpmqOmRkLE1Ix1oCMiGqSEZt3hmRzQaAcNkoEPY/rc+sWHDcMstIa4VExUsZoTpk8xWupDa+eno967SOJPJQ+BChLP1ipCRZYEKfA4isTe5SOlPrVZAoFIyPgaFjsnFifiAkTaREOdCScIDtZgQCqsQlqcEFEFRpwBk5c8bc3UeE01nh+fKKV/8rFhCgeAph9Eu3x+ta6nC7Gw/uNQEDM1WUr70Tt5dCtFvhLt7P/WnPDhTTW1l5/LRparAkRXjPXupAxzTtbSuLitxrV7vBzy9OykDDI7p2vvkn+fp3eEaGxF6MIDxzSMCIBa/rz/SfAAcgGwAbBLkOD6//p3/BrMkphEjG7gXAAAAAElFTkSuQmCC',
                    handler: function () {
                        rowEditing.cancelEdit();
                        selectTypeMeasure();
                        //newMeasureGridRow();
                    }
                },{
                    itemId: 'remove',
                    text: 'Remove',
                    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAADZklEQVQ4T22Tb2xTVRiHf2f29s+5ucfbrQwCNFNoiW3FMdbpyDSSzcQxyDSRENliBuLCUOKGSSNxX4yLhH7gkzEuVT4sG5YFDEZBE0JMNqLrtIMWOkqQjZFqSAndtN3t3W3vvTX3RoxB32/n5H2e88t7ziEA8I4o1vlqasaqK5WGlCS9dSybPW3sP1ofM7bTX1sbVTRt7u6dO13vA2liwE86nWM9zc3PuxjDNxcuyFOlUu/x+/dP/VswROmuBpfr1I6+PiZpGr6LRGYWMpke8kkgMN1VX/9sdX8/4PFAHRzEpfFxeQo48NHSUtSQDDHW0chx0dZ9+5jt6FGgUMDy2bO4NDJyg0SDweLr27Y5cPAgwPPA8jJWwmH8cOaMFOf5HmhafouqftW2Z4/AHzkC2GyApgHXr2NieFglH6xe3dWydm2kvamJr9q9G2AMKJchhcOYOH9eLRGitXZ02FgoBHAcoCiozM4ifu6c8svly+8RI2LI6dz7cm3tye3BoOOxzk6AUuiqiqUTJ6ArCmpCIVTZ7Sasz87i6sWL6o+x2EC/rn9qCowadDq7X2Dsi9ZAwM61twOCAL1YBCoVVFGKiixDS6UQn5xUf06nTdjg/hEYiw9F8dUmm238Rb/fyre1mUkMQUVRICcSuDI1pV3JZPb3A6MPD35UsD0oit83Li7aybp1qFqzxuzTJAnI53GrVCon5+dfexf49j+CIUHYtdXt/rIllxP+yGYhAZD/7rLzPHiXC4+73UgXCvK1ZLL3EGC+EzPBEKUdz/n90ZZslkmZjAmvALAQAsJxKAsCbIzBIQgQ6uqQXlyUE7HYgV5Ni5JjgtC5xe8feymXE+Tbt7EMoGTAHIe7FoteJkT38LxF5XlwggDKGHiPB9cyGenmxMTb5Ovm5vwrsiwoySQKAFQDphS/OhzKVUnqq5TLf9aL4uimVat4zWqFxZBQCr6hAdMzMyvk9ObNqZ12e8CWSCBfLoMTRdyiVJ3O5QYOK4p5VcPA3q3r15/0ut0OjRBQpxPE60VscnKBHLdaA09s3Diyw2JppA8eYIZSNX7v3sDhYtGEH1YE6K73ej9/xudz6NXV+Ckev/lbKrXfHGIY8D21YcOYyHFP31hYGDikKJ/933ceBbo3+XwRuVT6fX5u7o03gem/AGqKWxhL3xdrAAAAAElFTkSuQmCC',
                    handler: function () {
                        var sm = Ext.getCmp(idGrid).getSelectionModel();
                        rowEditing.cancelEdit();
                        Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete select records?', function (btn, text) {
                            if (btn == 'yes') {
                                Delete_Record(sm.getSelection(), Ext.getCmp(idGrid));
                                Ext.getCmp(idGrid).getStore().remove(sm.getSelection());
                            }
                        });
                    },
                    disabled: true
                }, {
                    itemId: 'reload',
                    icon: 'accentgold_/EditableGrid/icons/grid/reload.png',
                    text: 'Reload',
                    handler: function () {
                        loadData(_getODataEndpoint(entitySchemaName), true);
                    }
                },{
                    itemId: 'excel',
                    text: 'Export to Excel',
                    icon: 'accentgold_/EditableGrid/icons/grid/excel.png',
                    handler: function(b, e) {
                        exportExcelXml();
                    }
                },{
                    itemId: 'convert1',
                    text: 'Convert to New Opportunity',
                    icon: 'accentgold_/EditableGrid/icons/grid/add.gif',
                    handler: function () {
                        b.Clobal_ddsm_ProjectType = {Value: 962080002};
                        var sm = Ext.getCmp(idGrid).getSelectionModel();
                        rowEditing.cancelEdit();
                        makeObjMeasToProj(sm.getSelection(), false);
                    },
                    disabled: true,
                    hidden: (b.Xrm.Page.data.entity.getEntityName() == "lead")?true:false
                },{
                    itemId: 'convert2',
                    text: 'Convert to New Project',
                    icon: 'accentgold_/EditableGrid/icons/grid/add.gif',
                    handler: function () {
                        b.Clobal_ddsm_ProjectType = {Value: 962080000};
                        var sm = Ext.getCmp(idGrid).getSelectionModel();
                        rowEditing.cancelEdit();
                        makeObjMeasToProj(sm.getSelection(), true);
                    },
                    disabled: true,
                    hidden: (b.Xrm.Page.data.entity.getEntityName() == "lead")?true:false
                },{
                    itemId: 'duplicate',
                    text: 'Duplicate',
                    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAByElEQVQ4T5WTTShEURiG33OVMcntLgwj89O946dQFEqNhLKwkVhYKKWUpUkpi1lYTFFTGkulZGEhiWwoC0U0hYUyMphr/OS/CCPGzBzde+cOxl3MfKtzvvN+z/nOe84h0AjPFkuT0w77C9HSaiYlQH/NDS4wBQv6MLlbgLQBpXwz3kJ34FgzfKer6QOKLPUIvT+CzSmA/2w9PcD4Jkd5UxU+w6/I1htwEvRisOFZ2wP3eq5iGAEIBVT3rIU2fEU/odexCJwfJvyTKKpmqOmRkLE1Ix1oCMiGqSEZt3hmRzQaAcNkoEPY/rc+sWHDcMstIa4VExUsZoTpk8xWupDa+eno967SOJPJQ+BChLP1ipCRZYEKfA4isTe5SOlPrVZAoFIyPgaFjsnFifiAkTaREOdCScIDtZgQCqsQlqcEFEFRpwBk5c8bc3UeE01nh+fKKV/8rFhCgeAph9Eu3x+ta6nC7Gw/uNQEDM1WUr70Tt5dCtFvhLt7P/WnPDhTTW1l5/LRparAkRXjPXupAxzTtbSuLitxrV7vBzy9OykDDI7p2vvkn+fp3eEaGxF6MIDxzSMCIBa/rz/SfAAcgGwAbBLkOD6//p3/BrMkphEjG7gXAAAAAElFTkSuQmCC',
                    handler: function () {
                        var sm = Ext.getCmp(idGrid).getSelectionModel();
                        rowEditing.cancelEdit();
                        duplicateMeas(sm.getSelection());
                    },
                    disabled: true
                }],
                listeners: {
                    selectionchange: function (view, records) {
                        Ext.getCmp(idGrid).down('#remove').setDisabled(!records.length);
                        Ext.getCmp(idGrid).down('#convert1').setDisabled(!records.length);
                        Ext.getCmp(idGrid).down('#convert2').setDisabled(!records.length);
                        Ext.getCmp(idGrid).down('#duplicate').setDisabled(!records.length);

                        var sm = Ext.getCmp(idGrid).getSelectionModel();
                        Ext.each(records, function(record) {
                            var row = record.index;
                            if(record.get("ddsm_MeasureStatus") == "962080001"){
                                sm.deselect(row);
                            }
                        });

                    },
                    edit: function (editor, e) {
                        //calcCostBreakdown();

                        window.setTimeout(function () {
                            Update_Create_Record(e.grid, e.record, e.rowIdx);
                        }, 100);
                        if (altEnter) {
                            if (e.rowIdx + 1 < Ext.getCmp(idGrid).getStore().getCount()) {
                                rowEditing.startEdit(e.rowIdx + 1, 9);
                            } else {
                                rowEditing.cancelEdit();
                                newMeasureGridRow();
                            }
                            altEnter = false;
                        }
                        if (ctrlEnter) {
                            rowEditing.cancelEdit();
                            newMeasureGridRow();
                            ctrlEnter = false;
                        }
                    },
                    beforeedit: function (editor, e) {
                        lookupActive = "";
                        if (e.record.get("ddsm_MeasureStatus") == "962080001") {return false;}
                        if (e.record.get("ddsm_MeasureSelector") != "") {
                            editor.getEditor().child('[name="ddsm_MeasureSelector"]').disable();
                        } else {
                            editor.getEditor().child('[name="ddsm_MeasureSelector"]').enable();
                        }

                        window.setTimeout(function () {
                            $("#" + idGrid).find("input").keydown(function (event) {
                                if (event.ctrlKey && event.keyCode == 13) {
                                    //log("Hey! Ctrl+ENTER event captured!");
                                    ctrlEnter = true;
                                    event.preventDefault();
                                }
                                if (event.altKey && event.keyCode == 13) {
                                    //console.log("Hey! Alt+ENTER event captured!");
                                    altEnter = true;
                                    event.preventDefault();
                                }
                            });
                        }, 100);
                    },
                    canceledit: function (editor, e) {
                        lookupActive = "";
                        if (e.record.data[entitySchemaName + 'Id'] == "") { e.grid.getStore().removeAt(e.rowIdx); }
                        Ext.getCmp(idGrid).getView().refresh();
                    },
                    columnmove: function (container, coulmn, from, to) {
                        //console.log('Column Moved From ' + from+ ' To ' +to);
                    },
                    afterrender: function() {
                        spinnerForm.stop();
                        loadData(_getODataEndpoint(entitySchemaName), true);
                    }
                }
            });

            new Ext.util.KeyMap(Ext.getBody(),
                [{
                    key: Ext.EventObject.E,
                    ctrl: true,
                    scope: this,
                    defaultEventAction: 'preventDefault',
                    fn: function (e) {
                        //console.log("Hey! Ctrl+E event captured!");
                        var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                        if (typeof record !== 'undefined') {
                            rowEditing.startEdit(record.index, 9);
                        } else { Ext.Msg.alert("Error with editing", "Not selected any record Measure!"); }
                    }
                }, {
                    key: 45,
                    ctrl: true,
                    scope: this,
                    defaultEventAction: 'preventDefault',
                    fn: function (e) {
                        //console.log("Hey! Ctrl+Insert event captured!");
                        rowEditing.cancelEdit();
                        newMeasureGridRow();
                    }
                }, {
                    key: 46,
                    ctrl: true,
                    scope: this,
                    defaultEventAction: 'preventDefault',
                    fn: function (e) {
                        //console.log("Hey! Ctrl+Delete event captured!");
                        rowEditing.cancelEdit();
                        var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                        if (typeof record !== 'undefined') {
                            var sm = Ext.getCmp(idGrid).getSelectionModel();
                            rowEditing.cancelEdit();
                            Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete select records?', function (btn, text) {
                                if (btn == 'yes') {
                                    Delete_Record(sm.getSelection(), Ext.getCmp(idGrid));
                                    Ext.getCmp(idGrid).getStore().remove(sm.getSelection());
                                }
                            });
                        } else { Ext.Msg.alert("Error with deleting", "Not selected any record Measure!"); }
                    }
                }]);

            // New Measure Grid Row
            function selectTypeMeasure(){
                var typeMeasureForm = new Ext.form.Panel({
                    width: 300,
                    title: 'Measure Template Type',
                    floating: true,
                    closable : true,
                    border:false,
                    bodyPadding: 5,
                    //header: false,
                    cls: 'form2css',
                    items:[{
                        xtype: 'combobox',
                        name: 'typeMeasure',
                        id: 'typeMeasure',
                        fieldLabel: 'Type:',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['962080000', 'Potential'],
                                ['962080002', 'Pre-Approved'],
                                ['962080003', 'Pre-Existing']
                            ],
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        listeners:{
                            'select': function( combo, records, eOpts ){
                                var val = combo.getValue();
                                if(val != '962080002'){
                                    this.up('form').getForm().setValues({
                                        start_date: null,
                                        exp_date: null
                                    });
                                    Ext.getCmp('start_date').setReadOnly(true);
                                    Ext.getCmp('exp_date').setReadOnly(true);
                                } else {
                                    Ext.getCmp('start_date').setReadOnly(false);
                                    Ext.getCmp('exp_date').setReadOnly(false);
                                }
                            }
                        }
                    },{
                        xtype: 'datefield',
                        fieldLabel: 'Start Date:',
                        readOnly: true,
                        name: 'start_date',
                        id: 'start_date'
                    },{
                        xtype: 'datefield',
                        fieldLabel: 'Expiration Date',
                        readOnly: true,
                        name: 'exp_date',
                        id: 'exp_date'
                    }],
                    buttons: [{
                        text: 'Ok',
                        handler: function() {
                            if(Ext.getCmp('typeMeasure').getValue() != null){
                                MeasureStatus = Ext.getCmp('typeMeasure').getValue();
                                StartDate = Ext.getCmp('start_date').getValue();
                                ExpirationDate = Ext.getCmp('exp_date').getValue();
                                typeMeasureForm.close();
                                newMeasureGridRow();
                            } else {
                                Ext.Msg.alert("Error", "Select Measure Template Type");
                            }
                        }
                    },{
                        text: 'Cancel',
                        handler: function() {
                            typeMeasureForm.close();
                        }
                    }]
                });
                typeMeasureForm.show();
            }
            function newMeasureGridRow() {
                newRecordRow = true;
                var newRow = Ext.create(modelGrid, defaultFieldsValue);
                var count = Ext.getCmp(idGrid).getStore().getCount();
                Ext.getCmp(idGrid).getStore().insert(count, newRow);
                Ext.getCmp(idGrid).getSelectionModel().select(count);
                rowEditing.startEdit(count, 3);
                newRecord();
            }

            function newRecord() {
                var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0],
                    ddsm_accountid, ddsm_parentsite;
                if(b.Xrm.Page.data.entity.getEntityName() == "ddsm_site") {
                    var ddsm_parentsiteid = b.Xrm.Page.data.entity.getId();
                    var ddsm_parentsitename = b.Xrm.Page.getAttribute("ddsm_name").getValue();
                    var ddsm_parentsitetypename = b.Xrm.Page.context.getQueryStringParameters().typename;
                    ddsm_accountid = b.Xrm.Page.getAttribute("ddsm_parentaccount").getValue();
                    //var ddsm_initiallead = b.Xrm.Page.getAttribute("ddsm_initiallead").getValue();
                    var ddsm_sqft = b.Xrm.Page.getAttribute("ddsm_sqft").getValue();

                    rowEditing.getEditor().getForm().findField('ddsm_parentsite').setValue(ddsm_parentsitename);
                    rowEditing.getEditor().getForm().findField('ddsm_parentsite' + '__Id').setValue(ddsm_parentsiteid.replace(/\{|\}/g, ''));
                    rowEditing.getEditor().getForm().findField('ddsm_parentsite' + '__LogicalName').setValue(ddsm_parentsitetypename);
                    record.set('ddsm_parentsite', ddsm_parentsitename);
                    record.set('ddsm_parentsite' + '__Id', ddsm_parentsiteid.replace(/\{|\}/g, ''));
                    record.set('ddsm_parentsite' + '__LogicalName', ddsm_parentsitetypename);


                    rowEditing.getEditor().getForm().findField('ddsm_AccountId').setValue(ddsm_accountid[0].name);
                    rowEditing.getEditor().getForm().findField('ddsm_AccountId' + '__Id').setValue((ddsm_accountid[0].id).replace(/\{|\}/g, ''));
                    rowEditing.getEditor().getForm().findField('ddsm_AccountId' + '__LogicalName').setValue(ddsm_accountid[0].typename);
                    record.set('ddsm_AccountId', ddsm_accountid[0].name);
                    record.set('ddsm_AccountId' + '__Id', (ddsm_accountid[0].id).replace(/\{|\}/g, ''));
                    record.set('ddsm_AccountId' + '__LogicalName', ddsm_accountid[0].typename);
                    /*
                     if (ddsm_initiallead != null) {
                     rowEditing.getEditor().getForm().findField('ddsm_InitialLead').setValue(ddsm_initiallead[0].name);
                     rowEditing.getEditor().getForm().findField('ddsm_InitialLead' + '__Id').setValue((ddsm_initiallead[0].id).replace(/\{|\}/g, ''));
                     rowEditing.getEditor().getForm().findField('ddsm_InitialLead' + '__LogicalName').setValue(ddsm_initiallead[0].typename);
                     record.set('ddsm_InitialLead', ddsm_initiallead[0].name);
                     record.set('ddsm_InitialLead' + '__Id', (ddsm_initiallead[0].id).replace(/\{|\}/g, ''));
                     record.set('ddsm_InitialLead' + '__LogicalName', ddsm_initiallead[0].typename);
                     }
                     */
                    //rowEditing.getEditor().getForm().findField('ddsm_SqFt').setValue(parseInt(ddsm_sqft));
                    //record.set('ddsm_SqFt', parseInt(ddsm_sqft));
                }
                if(b.Xrm.Page.data.entity.getEntityName() == "lead") {
                    ddsm_accountid = getAccount_lead(b.Xrm.Page.data.entity.getId());
                    ddsm_parentsite = getSite_lead(b.Xrm.Page.data.entity.getId());
                    console.dir(ddsm_accountid);
                    console.dir(!!ddsm_accountid);
                    console.dir(ddsm_parentsite);
                    console.dir(!!ddsm_parentsite);
                    if(!!ddsm_accountid) {
                        rowEditing.getEditor().getForm().findField('ddsm_AccountId').setValue(ddsm_accountid[0].name);
                        rowEditing.getEditor().getForm().findField('ddsm_AccountId' + '__Id').setValue((ddsm_accountid[0].id).replace(/\{|\}/g, ''));
                        rowEditing.getEditor().getForm().findField('ddsm_AccountId' + '__LogicalName').setValue(ddsm_accountid[0].typename);
                        record.set('ddsm_AccountId', ddsm_accountid[0].name);
                        record.set('ddsm_AccountId' + '__Id', (ddsm_accountid[0].id).replace(/\{|\}/g, ''));
                        record.set('ddsm_AccountId' + '__LogicalName', ddsm_accountid[0].typename);
                    }
                    if(!!ddsm_parentsite) {
                        rowEditing.getEditor().getForm().findField('ddsm_parentsite').setValue(ddsm_parentsite.Name);
                        rowEditing.getEditor().getForm().findField('ddsm_parentsite' + '__Id').setValue((ddsm_parentsite.Id).replace(/\{|\}/g, ''));
                        rowEditing.getEditor().getForm().findField('ddsm_parentsite' + '__LogicalName').setValue(ddsm_parentsite.LogicalName);
                        record.set('ddsm_parentsite', ddsm_parentsite.Name);
                        record.set('ddsm_parentsite' + '__Id', (ddsm_parentsite.Id).replace(/\{|\}/g, ''));
                        record.set('ddsm_parentsite' + '__LogicalName', ddsm_parentsite.LogicalName);
                    }
                    rowEditing.getEditor().getForm().findField('ddsm_InitialLead').setValue(b.Xrm.Page.getAttribute("fullname").getValue());
                    rowEditing.getEditor().getForm().findField('ddsm_InitialLead' + '__Id').setValue((b.Xrm.Page.data.entity.getId()).replace(/\{|\}/g, ''));
                    rowEditing.getEditor().getForm().findField('ddsm_InitialLead' + '__LogicalName').setValue(b.Xrm.Page.data.entity.getEntityName());
                    record.set('ddsm_InitialLead', b.Xrm.Page.getAttribute("fullname").getValue());
                    record.set('ddsm_InitialLead' + '__Id', (b.Xrm.Page.data.entity.getId()).replace(/\{|\}/g, ''));
                    record.set('ddsm_InitialLead' + '__LogicalName', b.Xrm.Page.data.entity.getEntityName());
/*
                    var measProgram = b.Xrm.Page.getAttribute("ddsm_programid").getValue();
                    if(measProgram != null) {
                        rowEditing.getEditor().getForm().findField('ddsm_Program').setValue(measProgram[0].name);
                        rowEditing.getEditor().getForm().findField('ddsm_Program' + '__Id').setValue((measProgram[0].id).replace(/\{|\}/g, ''));
                        rowEditing.getEditor().getForm().findField('ddsm_Program' + '__LogicalName').setValue(measProgram[0].typename);
                        record.set('ddsm_Program', measProgram[0].name);
                        record.set('ddsm_Program' + '__Id', (measProgram[0].id).replace(/\{|\}/g, ''));
                        record.set('ddsm_Program' + '__LogicalName', measProgram[0].typename);
                    }
                    var measProgramOff = b.Xrm.Page.getAttribute("ddsm_programofferingid").getValue();
                    if(measProgramOff != null) {
                        rowEditing.getEditor().getForm().findField('ddsm_ProgramId').setValue(measProgramOff[0].name);
                        rowEditing.getEditor().getForm().findField('ddsm_ProgramId' + '__Id').setValue((measProgramOff[0].id).replace(/\{|\}/g, ''));
                        rowEditing.getEditor().getForm().findField('ddsm_ProgramId' + '__LogicalName').setValue(measProgramOff[0].typename);
                        record.set('ddsm_ProgramId', measProgramOff[0].name);
                        record.set('ddsm_ProgramId' + '__Id', (measProgramOff[0].id).replace(/\{|\}/g, ''));
                        record.set('ddsm_ProgramId' + '__LogicalName', measProgramOff[0].typename);
                    }

                    var ddsm_sqft = getSiteSqFt(ddsm_parentsite.Id);
                    rowEditing.getEditor().getForm().findField('ddsm_SqFt').setValue(parseInt(ddsm_sqft));
                    record.set('ddsm_SqFt', parseInt(ddsm_sqft));
*/
                }

            }
            //Synchronous query and field update
            function loadMeasTpl(lkp_meas_tpl) {
                var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0],
                    parentSiteName = "";
                var req = new XMLHttpRequest();
                var measTpl_ID = lkp_meas_tpl.id;
                //console.log("Function loadMeas()... Measure selector id: " + measTpl_ID + "Measure selector name: " +lkp_meas_tpl.name);
                //var meas_ID = Xrm.Page.data.entity.getId();
                var now = new Date();
                //var context = GetGlobalContext();
                var serverUrl = b.Xrm.Page.context.getClientUrl();
                //if(b.Xrm.Page.data.entity.getEntityName() == "lead") {parentSiteName = (getSite_lead(b.Xrm.Page.data.entity.getId())).Name;}
                if(b.Xrm.Page.data.entity.getEntityName() == "ddsm_site") {parentSiteName = b.Xrm.Page.getAttribute("ddsm_name").getValue()}
                //var measname = parentSiteName + "-" + lkp_meas_tpl.name + "_" + now.getFullYear() + now.getMonth() + now.getDate() + "-" + now.getHours() + now.getMinutes() + now.getSeconds();
                var measname = lkp_meas_tpl.name + "_" + now.getFullYear() + now.getMonth() + now.getDate() + "-" + now.getHours() + now.getMinutes() + now.getSeconds();
                record.set('ddsm_name', measname);
                rowEditing.getEditor().getForm().findField('ddsm_name').setValue(measname);

                record.set("ddsm_MeasureStatus", MeasureStatus);
                rowEditing.getEditor().getForm().findField('ddsm_MeasureStatus').setValue(MeasureStatus);
                record.set("ddsm_StartDate", StartDate);
                rowEditing.getEditor().getForm().findField('ddsm_StartDate').setValue(StartDate);
                record.set("ddsm_ExpirationDate", ExpirationDate);
                rowEditing.getEditor().getForm().findField('ddsm_ExpirationDate').setValue(ExpirationDate);


                /*
                                try {
                                    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_measuretemplateSet?"

                                        + "$select="
                                        + "ddsm_IncRateUnit,"
                                        + "ddsm_IncRateKW,"
                                        + "ddsm_IncRatekWh,"
                                        + "ddsm_IncRatethm,"
                                        + "ddsm_SavingsperUnitKW,"
                                        + "ddsm_SavingsperUnitkWh,"
                                        + "ddsm_SavingsperUnitthm,"
                //                        + "ddsm_Program,"
                                        + "ddsm_PlaceholderMeasure,"
                                        + "ddsm_MeasureCode,"
                                        + "ddsm_MeasureNumber,"
                                        + "ddsm_EnergyType,"
                                        + "ddsm_Category,"
                                        + "ddsm_MeasureGroup,"
                                        + "ddsm_MeasureSubgroup,"
                                        + "ddsm_Size,"
                                        + "ddsm_EUL,"
                                        + "ddsm_BaseEquipDescr,"
                                        + "ddsm_EffEquipDescr,"
                                        + "ddsm_AnnualFixedCosts,"
                                        + "ddsm_EndUse,"
                                        + "ddsm_EquipmentDescription,"
                                        + "ddsm_IncrementalCost,"
                                        + "ddsm_LMLightingorNon,"
                                        + "ddsm_MeasureLife,"
                                        + "ddsm_MeasureNotes,"
                                        + "ddsm_Source,"
                                        + "ddsm_Unit,"
                                        + "ddsm_StudyMeasure,"

                                        + "ddsm_MeasureType,"
                                        + "ddsm_AnnualHoursBefore,"
                                        + "ddsm_AnnualHoursAfter,"
                                        + "ddsm_SummerDemandReductionKW,"
                                        + "ddsm_WinterDemandReductionKW,"
                                        + "ddsm_WattsBase,"
                                        + "ddsm_WattsEff,"
                                        + "ddsm_ClientPeakKWSavings,"
                                        + "ddsm_MotorType,"
                                        + "ddsm_MotorHP,"
                                        + "ddsm_measuresubgroup2,"
                                        + "ddsm_SavingsperUnitCCF,"

                                        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_name,"
                                        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateSavings,"
                                        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateIncentive,"
                                        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_PerUnitLock,"
                                        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_SavingsLock,"
                                        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentiveRatesLock,"
                                        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentivesLock"//+ "&$orderby=ddsm_projTplToMilestoneTpl/ddsm_Index asc"

                                        + "$expand=ddsm_ddsm_measurecalculationtemplate_ddsm_measur"
                                        + "&$filter=ddsm_measuretemplateId eq guid'" + measTpl_ID + "' and statecode/Value eq 0", false);
                                    req.setRequestHeader("Accept", "application/json");
                                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                                    req.send();
                                    if (req.readyState == 4 && req.status == 200) {
                                        //From the project template - will also contain milestone information
                                        var _tplData = JSON.parse(req.responseText).d;
                                        if (_tplData.results[0] != null) {

                                            //console.dir(_tplData.results);

                                            var irpu = parseFloat(_tplData.results[0].ddsm_IncRateUnit.Value);
                                            var irkw = parseFloat(_tplData.results[0].ddsm_IncRateKW.Value);
                                            var irkwh = parseFloat(_tplData.results[0].ddsm_IncRatekWh.Value);
                                            var irthm = parseFloat(_tplData.results[0].ddsm_IncRatethm.Value);
                                            var incCost = parseFloat(_tplData.results[0].ddsm_IncrementalCost.Value);
                                            var skw = parseFloat(_tplData.results[0].ddsm_SavingsperUnitKW);
                                            var skwh = parseFloat(_tplData.results[0].ddsm_SavingsperUnitkWh);
                                            var sthm = parseFloat(_tplData.results[0].ddsm_SavingsperUnitthm);
                                            var sccf = parseFloat(_tplData.results[0].ddsm_SavingsperUnitCCF);
                                            var measLife = parseFloat(_tplData.results[0].ddsm_MeasureLife);
                                            var AnnFixedCosts = parseFloat(_tplData.results[0].ddsm_AnnualFixedCosts.Value);

                                            var MeasureCode = _tplData.results[0].ddsm_MeasureCode;
                                            var MeasureNumber = _tplData.results[0].ddsm_MeasureNumber;
                                            var EnergyType = _tplData.results[0].ddsm_EnergyType.Value;
                                            var Category = _tplData.results[0].ddsm_Category;
                                            var MeasureGroup = _tplData.results[0].ddsm_MeasureGroup;
                                            var MeasureSubgroup = _tplData.results[0].ddsm_MeasureSubgroup;
                                            var Size = parseFloat(_tplData.results[0].ddsm_Size);
                                            var EUL = parseFloat(_tplData.results[0].ddsm_EUL);
                                            var BaseEquipDescr = _tplData.results[0].ddsm_BaseEquipDescr;
                                            var EffEquipDescr = _tplData.results[0].ddsm_EffEquipDescr;

                                            var EndUse = _tplData.results[0].ddsm_EndUse;
                                            var EquipDesc = _tplData.results[0].ddsm_EquipmentDescription;
                                            var LMLightNon = _tplData.results[0].ddsm_LMLightingorNon;
                                            var MeasNotes = _tplData.results[0].ddsm_MeasureNotes;
                                            var Source = _tplData.results[0].ddsm_Source;
                                            var Unit = _tplData.results[0].ddsm_Unit;
                                            var StudyMeas = _tplData.results[0].ddsm_StudyMeasure;
                                            var PlaceholderMeasure = _tplData.results[0].ddsm_PlaceholderMeasure;

                                            var measType = _tplData.results[0].ddsm_MeasureType.Value;
                                            var annHoursBefore = _tplData.results[0].ddsm_AnnualHoursBefore;
                                            var annHoursAfter = _tplData.results[0].ddsm_AnnualHoursAfter;
                                            var sumDemReducKW = _tplData.results[0].ddsm_SummerDemandReductionKW;
                                            var wintDemReducKW = _tplData.results[0].ddsm_WinterDemandReductionKW;
                                            var wattsBase = _tplData.results[0].ddsm_WattsBase;
                                            var wattsEff = _tplData.results[0].ddsm_WattsEff;
                                            var clientPeakKWSave = _tplData.results[0].ddsm_ClientPeakKWSavings;
                                            var motorType = _tplData.results[0].ddsm_MotorType.Value;
                                            var motorHP = _tplData.results[0].ddsm_MotorHP;

                                            var measureSubgroup2 = _tplData.results[0].ddsm_measuresubgroup2;
                                            var measCalcTpl_Data = _tplData.results[0].ddsm_ddsm_measurecalculationtemplate_ddsm_measur;
                                            var measCalcTpl_Name = measCalcTpl_Data.ddsm_name;
                                            var measCalcTpl_CalcSavings = measCalcTpl_Data.ddsm_CalculateSavings;
                                            var measCalcTpl_CalcIncentives = measCalcTpl_Data.ddsm_CalculateIncentive;
                                            var measCalcTpl_PerUnitLock = measCalcTpl_Data.ddsm_PerUnitLock;
                                            var measCalcTpl_SavingsLock = measCalcTpl_Data.ddsm_SavingsLock;
                                            var measCalcTpl_IncentiveRatesLock = measCalcTpl_Data.ddsm_IncentiveRatesLock;
                                            var measCalcTpl_IncentivesLock = measCalcTpl_Data.ddsm_IncentivesLock;
                                            var measCalcTpl_CalcTypeValues = "";

                                             if (_tplData.results[0].ddsm_Program.Value != null) {
                                             record.set("ddsm_Program", Program.toString());
                                             rowEditing.getEditor().getForm().findField('ddsm_Program').setValue(Program.toString());
                                             }

                                            if (_tplData.results[0].ddsm_MeasureCode != null) {
                                                record.set("ddsm_MeasureCode", MeasureCode);
                                                rowEditing.getEditor().getForm().findField('ddsm_MeasureCode').setValue(MeasureCode);
                                            }

                                            if (_tplData.results[0].ddsm_MeasureNumber != null) {
                                                record.set("ddsm_MeasureNumber", MeasureNumber);
                                                rowEditing.getEditor().getForm().findField('ddsm_MeasureNumber').setValue(MeasureNumber);
                                            }

                                            if (_tplData.results[0].ddsm_EnergyType != null) {
                                                record.set("ddsm_EnergyType", EnergyType.toString());
                                                rowEditing.getEditor().getForm().findField('ddsm_EnergyType').setValue(EnergyType.toString());
                                            }

                                            if (_tplData.results[0].ddsm_Category != null) {
                                                record.set("ddsm_Category", Category);
                                                rowEditing.getEditor().getForm().findField('ddsm_Category').setValue(Category);
                                            }

                                            if (_tplData.results[0].ddsm_MeasureGroup != null) {
                                                record.set("ddsm_MeasureGroup", MeasureGroup);
                                                rowEditing.getEditor().getForm().findField('ddsm_MeasureGroup').setValue(MeasureGroup);
                                            }

                                            if (_tplData.results[0].ddsm_MeasureSubgroup != null) {
                                                record.set("ddsm_MeasureSubgroup", MeasureSubgroup);
                                                rowEditing.getEditor().getForm().findField('ddsm_MeasureSubgroup').setValue(MeasureSubgroup);
                                            }

                                            if (_tplData.results[0].ddsm_Size != null) {
                                                record.set("ddsm_Size", Size);
                                                rowEditing.getEditor().getForm().findField('ddsm_Size').setValue(Size);
                                            }

                                            if (_tplData.results[0].ddsm_EUL != null) {
                                                record.set("ddsm_EUL", EUL);
                                                rowEditing.getEditor().getForm().findField('ddsm_EUL').setValue(EUL);
                                            }

                                            if (_tplData.results[0].ddsm_BaseEquipDescr != null) {
                                                record.set("ddsm_BaseEquipDescr", BaseEquipDescr);
                                                rowEditing.getEditor().getForm().findField('ddsm_BaseEquipDescr').setValue(BaseEquipDescr);
                                            }

                                            if (_tplData.results[0].ddsm_EffEquipDescr != null) {
                                                record.set("ddsm_EffEquipDescr", EffEquipDescr);
                                                rowEditing.getEditor().getForm().findField('ddsm_EffEquipDescr').setValue(EffEquipDescr);
                                            }
                                            if (_tplData.results[0].ddsm_EndUse != null) {
                                                record.set("ddsm_EndUse", EndUse);
                                                rowEditing.getEditor().getForm().findField('ddsm_EndUse').setValue(EndUse);
                                            }

                                            if (_tplData.results[0].ddsm_EquipmentDescription != null) {
                                                record.set("ddsm_EquipmentDescription", EquipDesc);
                                                rowEditing.getEditor().getForm().findField('ddsm_EquipmentDescription').setValue(EquipDesc);
                                            }

                                            if (_tplData.results[0].ddsm_LMLightingorNon != null) {
                                                record.set("ddsm_LMLightingorNon", LMLightNon);
                                                rowEditing.getEditor().getForm().findField('ddsm_LMLightingorNon').setValue(LMLightNon);
                                            }

                                            if (_tplData.results[0].ddsm_MeasureNotes != null) {
                                                record.set("ddsm_MeasureNotes", MeasNotes);
                                                rowEditing.getEditor().getForm().findField('ddsm_MeasureNotes').setValue(MeasNotes);
                                            }

                                            if (_tplData.results[0].ddsm_Source != null) {
                                                record.set("ddsm_Source", Source);
                                                rowEditing.getEditor().getForm().findField('ddsm_Source').setValue(Source);
                                            }

                                            if (_tplData.results[0].ddsm_Unit != null) {
                                                record.set("ddsm_Unit", Unit);
                                                rowEditing.getEditor().getForm().findField('ddsm_Unit').setValue(Unit);
                                            }

                                            if (_tplData.results[0].ddsm_StudyMeasure != null) {
                                                record.set("ddsm_StudyMeasure", StudyMeas);
                                                rowEditing.getEditor().getForm().findField('ddsm_StudyMeasure').setValue(StudyMeas);
                                            }

                                            if (_tplData.results[0].ddsm_PlaceholderMeasure != null) {
                                                record.set("ddsm_PlaceholderMeasure", PlaceholderMeasure);
                                                rowEditing.getEditor().getForm().findField('ddsm_PlaceholderMeasure').setValue(PlaceholderMeasure);
                                            }

                                            if (_tplData.results[0].ddsm_MeasureType.Value != null) {
                                                record.set("ddsm_MeasureType", measType.toString());
                                                rowEditing.getEditor().getForm().findField('ddsm_MeasureType').setValue(measType.toString());
                                            }

                                            if (_tplData.results[0].ddsm_AnnualHoursBefore != null) {
                                                record.set("ddsm_AnnualHoursBefore", parseFloat(annHoursBefore));
                                                rowEditing.getEditor().getForm().findField('ddsm_AnnualHoursBefore').setValue(annHoursBefore);
                                            }

                                            if (_tplData.results[0].ddsm_AnnualHoursAfter != null) {
                                                record.set("ddsm_AnnualHoursAfter", parseFloat(annHoursAfter));
                                                rowEditing.getEditor().getForm().findField('ddsm_AnnualHoursAfter').setValue(annHoursAfter);
                                            }

                                            if (_tplData.results[0].ddsm_SummerDemandReductionKW != null) {
                                                record.set("ddsm_SummerDemandReductionKW", parseFloat(sumDemReducKW));
                                                rowEditing.getEditor().getForm().findField('ddsm_SummerDemandReductionKW').setValue(sumDemReducKW);
                                            }

                                            if (_tplData.results[0].ddsm_WinterDemandReductionKW != null) {
                                                record.set("ddsm_WinterDemandReductionKW", parseFloat(wintDemReducKW));
                                                rowEditing.getEditor().getForm().findField('ddsm_WinterDemandReductionKW').setValue(parseFloat(wintDemReducKW));
                                            }

                                            if (_tplData.results[0].ddsm_WattsBase != null) {
                                                record.set("ddsm_WattsBase", parseFloat(wattsBase));
                                                rowEditing.getEditor().getForm().findField('ddsm_WattsBase').setValue(parseFloat(wattsBase));
                                            }

                                            if (_tplData.results[0].ddsm_WattsEff != null) {
                                                record.set("ddsm_WattsEff", parseFloat(wattsEff));
                                                rowEditing.getEditor().getForm().findField('ddsm_WattsEff').setValue(parseFloat(wattsEff));
                                            }

                                            if (_tplData.results[0].ddsm_ClientPeakKWSavings != null) {
                                                record.set("ddsm_clientpeakkwsavings", parseFloat(clientPeakKWSave));
                                                rowEditing.getEditor().getForm().findField('ddsm_clientpeakkwsavings').setValue(parseFloat(clientPeakKWSave));
                                            }

                                            if (_tplData.results[0].ddsm_MotorType.Value != null) {
                                                record.set("ddsm_MotorType", motorType.toString());
                                                rowEditing.getEditor().getForm().findField('ddsm_MotorType').setValue(motorType.toString());
                                            }

                                            if (_tplData.results[0].ddsm_MotorHP != null) {
                                                record.set("ddsm_MotorHP", parseInt(motorHP, 10));
                                                rowEditing.getEditor().getForm().findField('ddsm_MotorHP').setValue(parseInt(motorHP, 10));
                                            }

                                            if (_tplData.results[0].ddsm_measuresubgroup2 != null) {
                                                record.set("ddsm_measuresubgroup2", measureSubgroup2);
                                                rowEditing.getEditor().getForm().findField('ddsm_measuresubgroup2').setValue(measureSubgroup2);
                                            }

                                            //------------------------------------------------------------------//

                                            if (_tplData.results[0].ddsm_IncRateUnit.Value != null) {
                                                record.set("ddsm_IncRateUnit", irpu);
                                                rowEditing.getEditor().getForm().findField('ddsm_IncRateUnit').setValue(irpu);

                                                record.set("ddsm_phase1incentiveunits", irpu);
                                                rowEditing.getEditor().getForm().findField('ddsm_phase1incentiveunits').setValue(irpu);
                                            } else if (_tplData.results[0].ddsm_IncRateUnit.Value == null) {
                                                record.set("ddsm_IncRateUnit", 0);
                                                rowEditing.getEditor().getForm().findField('ddsm_IncRateUnit').setValue(0);

                                                record.set("ddsm_phase1incentiveunits", 0);
                                                rowEditing.getEditor().getForm().findField('ddsm_phase1incentiveunits').setValue(0);
                                            }
                                            if (_tplData.results[0].ddsm_IncRateKW.Value != null) {
                                                record.set("ddsm_IncRateKW", irkw);
                                                rowEditing.getEditor().getForm().findField('ddsm_IncRateKW').setValue(irkw);
                                            } else if (_tplData.results[0].ddsm_IncRateKW.Value == null) {
                                                record.set("ddsm_IncRateKW", 0);
                                                rowEditing.getEditor().getForm().findField('ddsm_IncRateKW').setValue(0);
                                            }
                                            if (_tplData.results[0].ddsm_IncRatekWh.Value != null) {
                                                record.set("ddsm_IncRatekWh", irkwh);
                                                rowEditing.getEditor().getForm().findField('ddsm_IncRatekWh').setValue(irkwh);
                                            } else if (_tplData.results[0].ddsm_IncRatekWh.Value == null) {
                                                record.set("ddsm_IncRatekWh", 0);
                                                rowEditing.getEditor().getForm().findField('ddsm_IncRatekWh').setValue(0);
                                            }
                                            if (_tplData.results[0].ddsm_IncRatethm.Value != null) {
                                                record.set("ddsm_IncRatethm", irthm);
                                                rowEditing.getEditor().getForm().findField('ddsm_IncRatethm').setValue(irthm);
                                            } else if (_tplData.results[0].ddsm_IncRatethm.Value == null) {
                                                record.set("ddsm_IncRatethm", 0);
                                                rowEditing.getEditor().getForm().findField('ddsm_IncRatethm').setValue(0);
                                            }
                                            if (_tplData.results[0].ddsm_IncrementalCost.Value != null) {
                                                record.set("ddsm_IncrementalCost", incCost);
                                                rowEditing.getEditor().getForm().findField('ddsm_IncrementalCost').setValue(incCost);
                                            } else if (_tplData.results[0].ddsm_IncrementalCost.Value == null) {
                                                record.set("ddsm_IncrementalCost", 0);
                                                rowEditing.getEditor().getForm().findField('ddsm_IncrementalCost').setValue(0);
                                            }
                                            if (_tplData.results[0].ddsm_MeasureLife != null) {
                                                record.set("ddsm_MeasureLife", measLife);
                                                rowEditing.getEditor().getForm().findField('ddsm_MeasureLife').setValue(measLife);
                                            } else if (_tplData.results[0].ddsm_MeasureLife == null) {
                                                record.set("ddsm_MeasureLife", 0);
                                                rowEditing.getEditor().getForm().findField('ddsm_MeasureLife').setValue(0);
                                            }
                                            if (_tplData.results[0].ddsm_AnnualFixedCosts.Value != null) {
                                                record.set("ddsm_AnnualFixedCosts", AnnFixedCosts);
                                                rowEditing.getEditor().getForm().findField('ddsm_AnnualFixedCosts').setValue(AnnFixedCosts);
                                            } else if (_tplData.results[0].ddsm_AnnualFixedCosts.Value == null) {
                                                record.set("ddsm_AnnualFixedCosts", 0);
                                                rowEditing.getEditor().getForm().findField('ddsm_AnnualFixedCosts').setValue(0);
                                            }

                                            //copies savings
                                            //Copy to Phase 1
                                            if (_tplData.results[0].ddsm_SavingsperUnitKW != null) {
                                                record.set("ddsm_phase1perunitkw", skw);
                                                rowEditing.getEditor().getForm().findField('ddsm_phase1perunitkw').setValue(skw);
                                            } else {
                                                record.set("ddsm_phase1perunitkw", 0);
                                                rowEditing.getEditor().getForm().findField('ddsm_phase1perunitkw').setValue(0);
                                            }
                                            if (_tplData.results[0].ddsm_SavingsperUnitkWh != null) {
                                                record.set("ddsm_phase1perunitkwh", skwh);
                                                rowEditing.getEditor().getForm().findField('ddsm_phase1perunitkwh').setValue(skwh);
                                            } else {
                                                record.set("ddsm_phase1perunitkwh", 0);
                                                rowEditing.getEditor().getForm().findField('ddsm_phase1perunitkwh').setValue(0);
                                            }
                                            if (_tplData.results[0].ddsm_SavingsperUnitthm != null) {
                                                record.set("ddsm_phase1perunitthm", sthm);
                                                rowEditing.getEditor().getForm().findField('ddsm_phase1perunitthm').setValue(sthm);
                                            } else {
                                                record.set("ddsm_phase1perunitthm", 0);
                                                rowEditing.getEditor().getForm().findField('ddsm_phase1perunitthm').setValue(0);
                                            }
                                            if (_tplData.results[0].ddsm_SavingsperUnitCCF != null) {
                                                record.set("ddsm_Phase1PerUnitCCF", sccf);
                                                rowEditing.getEditor().getForm().findField('ddsm_Phase1PerUnitCCF').setValue(sccf);
                                            } else {
                                                record.set("ddsm_Phase1PerUnitCCF", 0);
                                                rowEditing.getEditor().getForm().findField('ddsm_Phase1PerUnitCCF').setValue(0);
                                            }

                                            record.set("ddsm_MeasureStatus", MeasureStatus);
                                            rowEditing.getEditor().getForm().findField('ddsm_MeasureStatus').setValue(MeasureStatus);
                                            record.set("ddsm_StartDate", StartDate);
                                            rowEditing.getEditor().getForm().findField('ddsm_StartDate').setValue(StartDate);
                                            record.set("ddsm_ExpirationDate", ExpirationDate);
                                            rowEditing.getEditor().getForm().findField('ddsm_ExpirationDate').setValue(ExpirationDate);

                                            //Measure Calc Template Data
                                            if (measCalcTpl_CalcSavings == true) {
                                                measCalcTpl_CalcTypeValues += "Calculate Savings; ";
                                            }
                                            if (measCalcTpl_CalcIncentives == true) {
                                                measCalcTpl_CalcTypeValues += "Calculate Incentives; ";
                                            }
                                            if (measCalcTpl_PerUnitLock == true) {
                                                measCalcTpl_CalcTypeValues += "Lock Per Units; ";
                                            }
                                            if (measCalcTpl_SavingsLock == true) {
                                                measCalcTpl_CalcTypeValues += "Lock Savings; ";
                                            }
                                            if (measCalcTpl_IncentiveRatesLock == true) {
                                                measCalcTpl_CalcTypeValues += "Lock Incentive Rates; ";
                                            }
                                            if (measCalcTpl_IncentivesLock == true) {
                                                measCalcTpl_CalcTypeValues += "Lock Incentives; ";
                                            }

                                            if (measCalcTpl_Name != null) {
                                                record.set("ddsm_CalculationType", measCalcTpl_Name);
                                                rowEditing.getEditor().getForm().findField('ddsm_CalculationType').setValue(measCalcTpl_Name);
                                            }
                                            if (measCalcTpl_CalcTypeValues != null) {
                                                record.set("ddsm_CalculationTypeValues", measCalcTpl_CalcTypeValues);
                                                rowEditing.getEditor().getForm().findField('ddsm_CalculationTypeValues').setValue(measCalcTpl_CalcTypeValues);
                                            }
                                        }
                                    }
                                }
                                catch (err) {
                                    Ext.Msg.alert("Error load Measure Template & Measure Calculation Template", err);
                                }
                */
            }
            function measCalc_Offered(units, record, f) {
                //console.log("measCalc_Offered starting: " + f);
                record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];

                var calcTypeValues = record.get("ddsm_CalculationTypeValues");
                var calcSavings = false;
                var calcIncentives = false;

                if (calcTypeValues != null) {
                    if ((calcTypeValues.indexOf("Calculate Savings")) != -1) {
                        calcSavings = true;
                    }
                    if ((calcTypeValues.indexOf("Calculate Incentives")) != -1) {
                        calcIncentives = true;
                    }
                }
                if (units == null) {
                    units = 0;
                    record.set("ddsm_phase1units", 0);
                    rowEditing.getEditor().getForm().findField('ddsm_phase1units').setValue(0);
                }
                //Calculates savings if calculation type is set to do so.
                if (calcSavings) {
                    var perUnitkWh = record.get("ddsm_phase1perunitkwh");
                    var perUnitKW = record.get("ddsm_phase1perunitkw");
                    var perUnitthm = record.get("ddsm_phase1perunitthm");
                    var perUnitCCF = record.get("ddsm_Phase1PerUnitCCF");

                    if (perUnitkWh == null) {
                        perUnitkWh = 0;
                    }
                    if (perUnitKW == null) {
                        perUnitKW = 0;
                    }
                    if (perUnitthm == null) {
                        perUnitthm = 0;
                    }
                    if (perUnitCCF == null) {
                        perUnitCCF = 0;
                    }
                    var savingskWh;
                    var savingsKW;
                    var savingsthm;
                    var savingsCCF;

                    //Savings calculations
                    savingskWh = (units * perUnitkWh);
                    savingsKW = (units * perUnitKW);
                    savingsthm = (units * perUnitthm);
                    savingsCCF = (units * perUnitCCF);

                    //Sets fields to savings values
                    record.set("ddsm_phase1savingskwh", savingskWh);
                    rowEditing.getEditor().getForm().findField('ddsm_phase1savingskwh').setValue(savingskWh);
                    record.set("ddsm_phase1savingskw", savingsKW);
                    rowEditing.getEditor().getForm().findField('ddsm_phase1savingskw').setValue(savingsKW);
                    record.set("ddsm_phase1savingsthm", savingsthm);
                    rowEditing.getEditor().getForm().findField('ddsm_phase1savingsthm').setValue(savingsthm);
                    record.set("ddsm_Phase1SavingsCCF", savingsCCF);
                    rowEditing.getEditor().getForm().findField('ddsm_Phase1SavingsCCF').setValue(savingsCCF);
                }

                //Calculates incentives if calculation type is set to do so.
                if (calcIncentives) {
                    var incRateUnits = record.get("ddsm_phase1incentiveunits");
                    var incRatekWh = record.get("ddsm_IncRateKW");
                    var incRateKW = record.get("ddsm_IncRatekWh");
                    var incRatethm = record.get("ddsm_IncRatethm");

                    if (incRateUnits == null) {
                        incRateUnits = 0;
                    }
                    if (incRatekWh == null) {
                        incRatekWh = 0;
                    }
                    if (incRateKW == null) {
                        incRateKW = 0;
                    }
                    if (incRatethm == null) {
                        incRatethm = 0;
                    }
                    var savingskWh = record.get("ddsm_phase1savingskwh");
                    var savingsKW = record.get("ddsm_phase1savingskw");
                    var savingsthm = record.get("ddsm_phase1savingsthm");

                    if (savingskWh == null) {
                        savingskWh = 0;
                    }
                    if (savingsKW == null) {
                        savingsKW = 0;
                    }
                    if (savingsthm == null) {
                        savingsthm = 0;
                    }
                    var incUnits;
                    var inckWh;
                    var incKW;
                    var incthm;
                    var incElectric;
                    var incGas;
                    var incTotal;

                    //Incentive calculations
                    incUnits = (units * incRateUnits);
                    inckWh = (savingskWh * incRatekWh);
                    incKW = (savingsKW * incRateKW);
                    incthm = (savingsthm * incRatethm);
                    incElectric = (inckWh + incKW);
                    incGas = incthm;
                    incTotal = (incElectric + incGas + incUnits);

                    //Sets fields to incentive values
//                    record.set("ddsm_phase1incentiveunits", incUnits);
//                    rowEditing.getEditor().getForm().findField('ddsm_phase1incentiveunits').setValue(incUnits);

                    record.set("ddsm_phase1incentivetotal", incTotal);
                    rowEditing.getEditor().getForm().findField('ddsm_phase1incentivetotal').setValue(incTotal);
                }


                //Sends phase values to current fields
                var phase1savingskWh = record.get("ddsm_phase1savingskwh");
                var phase1savingsKW = record.get("ddsm_phase1savingskw");
                var phase1savingsthm = record.get("ddsm_phase1savingsthm");

                var eul = record.get("ddsm_EUL");
                var lifecycleKW = 0.0;
                var lifecyclekWh = 0.0;
                var lifecyclethm = 0.0;

                if (phase1savingskWh == null) {
                    phase1savingskWh = 0;
                }
                if (phase1savingsKW == null) {
                    phase1savingsKW = 0;
                }
                if (phase1savingsthm == null) {
                    phase1savingsthm = 0;
                }
                if (eul == null) {
                    eul = 0;
                }
                lifecycleKW = (phase1savingsKW * eul);
                lifecyclekWh = (phase1savingskWh * eul);
                lifecyclethm = (phase1savingsthm * eul);

                record.set("ddsm_lifecyclesavingskw", lifecycleKW);
                //rowEditing.getEditor().getForm().findField('ddsm_lifecyclesavingskw').setValue(lifecycleKW);

                record.set("ddsm_lifecyclesavingskwh", lifecyclekWh);
                //rowEditing.getEditor().getForm().findField('ddsm_lifecyclesavingskwh').setValue(lifecyclekWh);

                record.set("ddsm_lifecyclesavingsthm", lifecyclethm);
                //rowEditing.getEditor().getForm().findField('ddsm_lifecyclesavingsthm').setValue(lifecyclethm);

            }
            function calcCostBreakdown() {
                //console.log("calcCostBreakdown");
                var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                var costTotal = 0;
                var costEquipment = 0;
                var costLabor = 0;
                var costOther = 0;
                if (typeof rowEditing.getEditor().getForm().findField('ddsm_CostEquipment') !== 'undefined') {
                    if(rowEditing.getEditor().getForm().findField('ddsm_CostEquipment').getValue() == null) {
                        costEquipment = 0;
                    } else {
                        costEquipment = parseFloat(rowEditing.getEditor().getForm().findField('ddsm_CostEquipment').getValue());
                    }
                } else {
                    if(record.get('ddsm_CostEquipment') == null) {
                        costEquipment = 0;
                    } else {
                        costEquipment = parseFloat(record.get('ddsm_CostEquipment'));
                    }
                }
                if (typeof rowEditing.getEditor().getForm().findField('ddsm_CostLabor') !== 'undefined') {
                    if(rowEditing.getEditor().getForm().findField('ddsm_CostLabor').getValue() == null) {
                        costLabor = 0;
                    } else {
                        costLabor = parseFloat(rowEditing.getEditor().getForm().findField('ddsm_CostLabor').getValue());
                    }
                } else {
                    if(record.get('ddsm_CostLabor') == null) {
                        costLabor = 0;
                    } else {
                        costLabor = parseFloat(record.get('ddsm_CostLabor'));
                    }
                }
                if (typeof rowEditing.getEditor().getForm().findField('ddsm_CostOther') !== 'undefined') {
                    if(rowEditing.getEditor().getForm().findField('ddsm_CostOther').getValue() == null) {
                        costOther = 0;
                    } else {
                        costOther = parseFloat(rowEditing.getEditor().getForm().findField('ddsm_CostOther').getValue());
                    }
                } else {
                    if(record.get('ddsm_CostOther') == null) {
                        costOther = 0;
                    } else {
                        costOther = parseFloat(record.get('ddsm_CostOther'));
                    }
                }
                costTotal = costEquipment + costLabor + costOther;
                record.set("ddsm_CostTotal", costTotal);
                rowEditing.getEditor().getForm().findField('ddsm_CostTotal').setValue(costTotal);
            }


            function duplicateMeas(records){
                //console.dir(records);
                var countRecords = records.length;
                for(var x= 0; x < records.length; x++) {
                    var record = records[x];
                    var changes = {};
                    for (var j = 0; j < modConfigLoadSaveData.length; j++) {
                        if (modConfigLoadSaveData[j].saved || typeof modConfigLoadSaveData[j].saved === 'undefined') {
                            if (record.data[modConfigLoadSaveData[j].name] != null && (((record.data[modConfigLoadSaveData[j].name]).toString()).replace(/\s+/g, '')).length === 0) {
                                record.data[modConfigLoadSaveData[j].name] = null;
                            }

                            switch (modConfigLoadSaveData[j].type) {
                                case 'date':
                                    if (record.data[modConfigLoadSaveData[j].name] != null) {
                                        changes[modConfigLoadSaveData[j].name] = _toUTCDate(record.data[modConfigLoadSaveData[j].name]);
                                    } else {
                                        changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                    }
                                    break;
                                case 'lookup':
                                    if (record.data[modConfigLoadSaveData[j].name] == null) {
                                        record.data[modConfigLoadSaveData[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                                    }
                                    if (record.data[modConfigLoadSaveData[j].name + '__Id'] !== '00000000-0000-0000-0000-000000000000' && record.data[modConfigLoadSaveData[j].name] != null) {
                                        var lookup = {};
                                        lookup.Name = record.data[modConfigLoadSaveData[j].name];
                                        lookup.Id = record.data[modConfigLoadSaveData[j].name + '__Id'];
                                        lookup.LogicalName = record.data[modConfigLoadSaveData[j].name + '__LogicalName'];
                                        changes[modConfigLoadSaveData[j].name] = lookup;
                                    } else {
                                        var lookup = {};
                                        lookup.Name = null;
                                        lookup.Id = null;
                                        lookup.LogicalName = null;
                                        changes[modConfigLoadSaveData[j].name] = lookup;
                                    }
                                    break;
                                case 'combobox':
                                    if (record.data[modConfigLoadSaveData[j].name] != null) {
                                        changes[modConfigLoadSaveData[j].name] = {Value: (record.data[modConfigLoadSaveData[j].name]).toString()};
                                    } else {
                                        changes[modConfigLoadSaveData[j].name] = {Value: record.data[modConfigLoadSaveData[j].name]};
                                    }
                                    break;
                                case 'number':
                                case 'decimal':
                                case 'int':
                                case 'int32':
                                case 'int64':
                                    if (record.data[modConfigLoadSaveData[j].name] != null) {
                                        changes[modConfigLoadSaveData[j].name] = (record.data[modConfigLoadSaveData[j].name]).toString();
                                    } else {
                                        changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                    }
                                    break;
                                case 'number':
                                    if (record.data[modConfigLoadSaveData[j].name] != null) {
                                        changes[modConfigLoadSaveData[j].name] = (record.data[modConfigLoadSaveData[j].name]).toString();
                                    } else {
                                        changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                    }
                                    break;
                                case 'checkcolumn':
                                    changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                    break;
                                case 'boolean':
                                    changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                    break;
                                case 'currency':
                                    if (record.data[modConfigLoadSaveData[j].name] != null) {
                                        changes[modConfigLoadSaveData[j].name] = {Value: (record.data[modConfigLoadSaveData[j].name]).toString()};
                                    } else {
                                        changes[modConfigLoadSaveData[j].name] = {Value: record.data[modConfigLoadSaveData[j].name]};
                                    }
                                    break;

                                default:
                                    if (record.data[modConfigLoadSaveData[j].name] != null) {
                                        changes[modConfigLoadSaveData[j].name] = (record.data[modConfigLoadSaveData[j].name]).toString();
                                    } else {
                                        changes[modConfigLoadSaveData[j].name] = record.data[modConfigLoadSaveData[j].name];
                                    }
                                    break;
                            }
                        }
                    }

                    var URI = _getODataEndpoint(entitySchemaName);
                    //console.log(URI);
                    var req = new XMLHttpRequest();
                    req.open("POST", URI, false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.onreadystatechange = function () {
                        if (this.readyState == 4) {
                            req.onreadystatechange = null;
                            if (this.status == 201) {
                                var responseData = JSON.parse(this.responseText).d;
                                //console.log("CREATE: " + responseData[entitySchemaName + 'Id'] + " | " + responseData['ddsm_name']);
                                countRecords--;
                                if(countRecords == 0) {
                                    loadData(_getODataEndpoint(entitySchemaName), true);
                                }
                            }
                        }
                    };
                    req.send(JSON.stringify(changes));
                }
                lookupActive = "";

            }

            function getAccount_lead(GUID){
                return b.Xrm.Page.getAttribute("parentaccountid").getValue();
            }

            function getSite_lead(GUID){
                return null;
                var siteObj = {}, reqData;
                if(b.Xrm.Page.data.entity.getEntityName() == "lead") {
                    /*
                     var Req = "$select=ddsm_name,"
                     + "ddsm_siteId"
                     + "&$filter=ddsm_InitialLead/Id eq guid'" + GUID + "' and statecode/Value eq 0";

                     AGS.REST.retrieveMultipleRecords("ddsm_site", Req, null, null, function (data) {
                     reqData = data[0];
                     }, false, null);
                     console.dir(reqData);
                     siteObj.Id = reqData.ddsm_siteId;
                     siteObj.Name = reqData.ddsm_name;
                     siteObj.LogicalName = "ddsm_site";
                     */
                    /*
                    var ParentSiteforlead = b.Xrm.Page.getAttribute("ddsm_parentsiteforlead").getValue();
                    if (ParentSiteforlead != null) {
                        siteObj.Id = ParentSiteforlead[0].id;
                        siteObj.Name = ParentSiteforlead[0].name;
                        siteObj.LogicalName = ParentSiteforlead[0].typename;
                    } else {
                        siteObj.Id = null;
                        siteObj.Name = null;
                        siteObj.LogicalName = null;
                    }
                    */
                }else {
                    siteObj.Id = null;
                    siteObj.Name = null;
                    siteObj.LogicalName = null;
                }
            }
            function getSiteSqFt(GUID){
                var reqData;
                var Req = "$select=ddsm_name,"
                    + "ddsm_sqft"
                    + "&$filter=ddsm_siteId eq guid'" + GUID + "' and statecode/Value eq 0";

                AGS.REST.retrieveMultipleRecords("ddsm_site", Req, null, null, function (data) {
                    reqData = data[0];
                }, false, null);
                return (reqData.ddsm_sqft != null)?parseInt(reqData.ddsm_sqft):0;
            }


            /*
             Ext.getCmp(idGrid).headerCt.suspendLayout = true;
             Ext.getCmp(idGrid).headerCt.removeAll();
             Ext.getCmp(idGrid).headerCt.add(viewColumns);
             //Ext.getCmp(idGrid).bindStore(gridStore);
             Ext.getCmp(idGrid).forceComponentLayout();
             */
            //Ext.getCmp(idGrid).reconfigure(Ext.getCmp(idGrid).getStore(), viewColumns);

            //loadData(_getODataEndpoint(entitySchemaName), true);
        });

        // Refresh  Grid
        reloadGrid = function () {
            loadData(_getODataEndpoint(entitySchemaName), true);
        }
        get_dataOkgrid = function () {
            return dataOk;
        }
        get_saveGridStatus = function () {
            return saveGridStatus;
        }
        refreshGrid = function () {
            Ext.getCmp(idGrid).getView().refresh();
        }
        $(b).resize(function () {
            Ext.getCmp(idGrid).getView().refresh();
        });
    });
} else {
    //b.Xrm.Page.ui.tabs.get("Tab_Measures").setVisible(false);
}