/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 2.5.0 DSM/MPM base logic; before "MPM Active by Category" logic
 * Last updated on 06/16/2016.
 * Milestones Grid Demo
 * 12/24/2014 add check the status of the document is uploaded into the project
 * 01/29/2015 add export to excel
 * 03/31/2015 add Project Task
 * 07/08/2015 add verify Responsible Team
 // 12/20/2015  : cleared off the code usage of removed fields of ddsm_project entity
 */
(function (fallback) {

    fallback = fallback || function () { };

    var trap = function () {
        var args = Array.prototype.slice.call(arguments);
        var message = args.join(' ');
        fallback(message);
    };

    if (typeof console === 'undefined') {
        console = {
            messages: [],
            raw: [],
            dump: function () { return console.messages.join('\n'); },
            log: trap,
            debug: trap,
            info: trap,
            warn: trap,
            error: trap,
            assert: trap,
            clear: function () {
                console.messages.length = 0;
                console.raw.length = 0;
            },
            dir: trap,
            dirxml: trap,
            trace: trap,
            group: trap,
            groupCollapsed: trap,
            groupEnd: trap,
            time: trap,
            timeEnd: trap,
            timeStamp: trap,
            profile: trap,
            profileEnd: trap,
            count: trap,
            exception: trap,
            table: trap
        };
    }

})(null);

var b = window.parent;

if (b.Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue() != null) {
    var loading3GridInterval = null, validLoading3Grid, dataOk, results = [], loadData, recalcActualEnd, _getODataEndpoint, entitySchemaName, Save_RecordAll, saveGridStatus, lookupActive, get_saveGridStatus, get_dataOkgrid, refreshGrid, reloadGrid, idGrid, entityName_getLookupData, activeRowIdx = 0, filteringCategory = "000000000", groupedCategory = false, rowEditing, AutoFncCreationIndex = 0, nextMilestone, prevMilestone, validStepBP, bussinessProcessFlag = null, getActiveIndex, NextPrevBP_MS_Flag = null, bpClick_btn = false, recordStatus = 0, automationNextStep = 0;
    var completedOutofSequence = '962080001'; // 962080000 - user is not able to insert anything anywhere; 962080001 - user can insert the appropriate Set in the specific location; 962080002. - user can insert any Set in any place.
    var sequenceRequired = true; // DSM/MPM logic; default DSM logic

    $(b.document).ready(function () {

        // ---- START Excel export code
        var Base64 = (function() {
            // Private property
            var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

            // Private method for UTF-8 encoding

            function utf8Encode(string) {
                string = string.replace(/\r\n/g, "\n");
                var utftext = "";
                for (var n = 0; n < string.length; n++) {
                    var c = string.charCodeAt(n);
                    if (c < 128) {
                        utftext += String.fromCharCode(c);
                    } else if ((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                    } else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                }
                return utftext;
            }

            // Public method for encoding
            return {
                encode: (typeof btoa == 'function') ? function(input) {
                    return btoa(utf8Encode(input));
                } : function(input) {
                    var output = "";
                    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
                    var i = 0;
                    input = utf8Encode(input);
                    while (i < input.length) {
                        chr1 = input.charCodeAt(i++);
                        chr2 = input.charCodeAt(i++);
                        chr3 = input.charCodeAt(i++);
                        enc1 = chr1 >> 2;
                        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                        enc4 = chr3 & 63;
                        if (isNaN(chr2)) {
                            enc3 = enc4 = 64;
                        } else if (isNaN(chr3)) {
                            enc4 = 64;
                        }
                        output = output +
                            keyStr.charAt(enc1) + keyStr.charAt(enc2) +
                            keyStr.charAt(enc3) + keyStr.charAt(enc4);
                    }
                    return output;
                }
            };
        })();
        // ---- END Excel export code

        function _getAdminDataSettings() {
            var results = [];
            var req = new XMLHttpRequest();
            var URI_ = b.Xrm.Page.context.getClientUrl() + "/XRMServices/2011/OrganizationData.svc/ddsm_admindataSet?$select=ddsm_CompletedOutofSequence, ddsm_SequenceRequired"
            req.open("GET", URI_, false);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.onreadystatechange = function () {
                if (this.readyState == 4) {
                    req.onreadystatechange = null;
                    if (this.status == 200) {

                        var responseData = JSON.parse(this.responseText).d;
                        if (typeof responseData.results[0] !== 'undefined') {
                            if (responseData.results[0].ddsm_CompletedOutofSequence.Value != null) {
                                completedOutofSequence = (responseData.results[0].ddsm_CompletedOutofSequence.Value).toString();
                            }
                            if (responseData.results[0].ddsm_SequenceRequired != null) {
                                sequenceRequired = responseData.results[0].ddsm_SequenceRequired;
                            }
                        }
                    }
                }
            };
            req.send(null);
        }

        Ext.Loader.setConfig({
            enabled: true,
            disableCaching: false,
            path: { 'Ext': 'accentgold_/EditableGrid/js/' }
        });
        Ext.Loader.setPath('Ext.ux', 'accentgold_/EditableGrid/js/ux');
        Ext.require([
            'Ext.Button',
            'Ext.grid.*',
            'Ext.data.*',
            'Ext.ux.grid.FiltersFeature',
            'Ext.ux.grid.menu.ListMenu',
            'Ext.util.*',
            'Ext.state.*',
            'Ext.form.*',
            'Ext.tip.QuickTip',
            'Ext.selection.CheckboxModel'
        ]);

        Ext.override('Ext.ux.grid.FiltersFeature', {
            show: function () {
                var lastArgs = null;
                return function () {
                    if (!arguments) {
                        this.callParent(lastArgs);
                    } else {
                        lastArgs = arguments;
                        if (this.loadOnShow && !this.loaded) {
                            this.store.load();
                        }
                        this.callParent(arguments);
                    }
                };
            }()
        });

        Ext.onReady(function () {

            // ---- START Excel export code
            var reg = /[^\.\,\;\:\?\!\"\'\(\)\+\№\%\#\@\$\[\]\{\}\~\^\&\*\/\\\|\<\>\s]+/g;
            var strName = b.Xrm.Page.getAttribute("ddsm_name").getValue();
            strName = strName.match(reg).join(' ');

            var exportExcelXml = function(includeHidden, title) {

                if (!title) {title = "Project Milestones"}

                var vExportContent = getExcelXml(includeHidden, title);
                var location = 'data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);


                if (!Ext.isIE) {
                    var gridEl = Ext.getCmp(idGrid).getEl();

                    var el = Ext.DomHelper.append(gridEl, {
                        tag: "a",
                        download: strName + " - " + title + " - " + Ext.Date.format(new Date(), 'm-d-Y H-i') + '.xls',
                        href: location
                    });
                    el.click();
                    Ext.fly(el).destroy();
                } else {
                    var fileName = strName + " - " + title + " - " + Ext.Date.format(new Date(), 'm-d-Y H-i') + '.xls';
                    var form = Ext.getCmp(idGrid).down('form#uploadForm');
                    if (form) {
                        form.destroy();
                    }
                    form = Ext.getCmp(idGrid).add({
                        xtype: 'form',
                        itemId: 'uploadForm',
                        hidden: true,
                        standardSubmit: true,
                        url: 'http://dynamicdsm.richlode.biz/crmIExls.asmx/GetXsl',
                        items: [{
                            xtype: 'hiddenfield',
                            name: 'Name',
                            value: fileName
                        },{
                            xtype: 'hiddenfield',
                            name: 'strXml',
                            value: Base64.encode(vExportContent)
                        }]
                    });

                    form.getForm().submit();

                    //alert("The xls export isn't supported by IE.");
                }

            };

            var getExcelXml = function(includeHidden, title) {
                var theTitle = title || this.title;
                var worksheet = createWorksheet(includeHidden, theTitle);
                var totalWidth =  Ext.getCmp(idGrid).columnManager.columns.length;
                return ''.concat(
                    '<?xml version="1.0"?>',
                    '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">',
                    '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Title>' + theTitle + '</Title></DocumentProperties>',
                    '<OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office"><AllowPNG/></OfficeDocumentSettings>',
                    '<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">',
                    '<WindowHeight>' + worksheet.height + '</WindowHeight>',
                    '<WindowWidth>' + 12585 + '</WindowWidth>',
                    '<ProtectStructure>False</ProtectStructure>',
                    '<ProtectWindows>False</ProtectWindows>',
                    '</ExcelWorkbook>',

                    '<Styles>',

                    '<Style ss:ID="Default" ss:Name="Normal">',
                    '<Alignment ss:Vertical="Bottom"/>',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>',
                    '<Interior/>',
                    '<NumberFormat/>',
                    '<Protection/>',
                    '</Style>',

                    '<Style ss:ID="title">',
                    '<Borders />',
                    '<Font ss:Bold="1" ss:Size="18" />',
                    '<Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1" />',
                    '<Interior ss:Color="#E0ECF0" ss:Pattern="Solid" />',
                    '<NumberFormat ss:Format="@" />',
                    '</Style>',

                    '<Style ss:ID="headercell">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:Bold="1" ss:Size="10" />',
                    '<Alignment ss:Horizontal="Center" ss:WrapText="1" />',
                    '<Interior ss:Color="#CFE1E8" ss:Pattern="Solid" />',
                    '</Style>',


                    '<Style ss:ID="even">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Interior ss:Color="#FFFFFF" ss:Pattern="Solid" />',
                    '</Style>',

                    /*
                     '<Style ss:ID="evendate" ss:Parent="even">',
                     '<NumberFormat ss:Format="m-d-yyyy" />',
                     '</Style>',

                     '<Style ss:ID="evenint" ss:Parent="even">',
                     '<Numberformat ss:Format="0" />',
                     '</Style>',

                     '<Style ss:ID="evenfloat" ss:Parent="even">',
                     '<Numberformat ss:Format="0.00" />',
                     '</Style>',
                     */

                    '<Style ss:ID="odd">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Interior ss:Color="#F1F7F8" ss:Pattern="Solid" />',
                    '</Style>',

                    '<Style ss:ID="groupSeparator">',
                    '<Borders>',
                    '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>',
                    '</Borders>',
                    '<Font ss:Bold="1" ss:Size="10" />',
                    '<Interior ss:Color="#D3D3D3" ss:Pattern="Solid" />',
                    '</Style>',
                    /*
                     '<Style ss:ID="odddate" ss:Parent="odd">',
                     '<NumberFormat ss:Format="m-d-yyyy" />',
                     '</Style>',
                     '<Style ss:ID="oddint" ss:Parent="odd">',
                     '<NumberFormat Format="0" />',
                     '</Style>',

                     '<Style ss:ID="oddfloat" ss:Parent="odd">',
                     '<NumberFormat Format="0.00" />',
                     '</Style>',
                     */


                    '</Styles>',
                    worksheet.xml,
                    '</Workbook>'
                );
            };

            var getModelField = function(fieldName) {
                var fields = Ext.getCmp(idGrid).store.model.getFields();
                for (var i = 0; i < fields.length; i++) {
                    if (fields[i].name === fieldName) {
                        return fields[i];
                    }
                }
            };

            var generateEmptyGroupRow = function(dataIndex, value, cellTypes, includeHidden) {
                var cm =  Ext.getCmp(idGrid).columnManager.columns;
                var colCount = cm.length;
                var rowTpl = '<Row ss:AutoFitHeight="0"><Cell ss:StyleID="groupSeparator" ss:MergeAcross="{0}"><Data ss:Type="String"><html:b>{1}</html:b></Data></Cell></Row>';
                var visibleCols = 0;
                // rowXml += '<Cell ss:StyleID="groupSeparator">'
                for (var j = 0; j < colCount; j++) {
                    if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && (includeHidden || !cm[j].hidden)) {
                        // rowXml += '<Cell ss:StyleID="groupSeparator"/>';
                        visibleCols++;
                    }
                }
                // rowXml += "</Row>";
                return Ext.String.format(rowTpl, visibleCols - 1, value);
            };


            var createWorksheet = function(includeHidden, theTitle) {
                // Calculate cell data types and extra class names which affect formatting
                var cellType = [];
                var cellTypeClass = [];
                var cm = Ext.getCmp(idGrid).columnManager.columns;

                var totalWidthInPixels = 0;
                var colXml = '';
                var headerXml = '';
                var visibleColumnCountReduction = 0;
                var colCount = cm.length;
                for (var i = 0; i < colCount; i++) {
                    if (cm[i].xtype != 'actioncolumn' && (cm[i].dataIndex != '') && (includeHidden || !cm[i].hidden)) {
                        var w = cm[i].getEl().getWidth();
                        totalWidthInPixels += w;

                        if (cm[i].text === "") {
                            cellType.push("None");
                            cellTypeClass.push("");
                            ++visibleColumnCountReduction;
                        } else {
                            colXml += '<Column ss:AutoFitWidth="1" ss:Width="' + w + '" />';
                            headerXml += '<Cell ss:StyleID="headercell">' +
                                '<Data ss:Type="String">' + cm[i].text + '</Data>' +
                                '<NamedCell ss:Name="Print_Titles"></NamedCell></Cell>';


                            var fld = getModelField(cm[i].dataIndex);
                            switch (fld.type.type) {
                                case "int":
                                    cellType.push("Number");
                                    cellTypeClass.push("int");
                                    break;
                                case "float":
                                    cellType.push("Number");
                                    cellTypeClass.push("float");
                                    break;

                                case "bool":

                                case "boolean":
                                    cellType.push("String");
                                    cellTypeClass.push("");
                                    break;
                                case "date":
                                    cellType.push("DateTime");
                                    cellTypeClass.push("date");
                                    break;
                                default:
                                    cellType.push("String");
                                    cellTypeClass.push("");
                                    break;
                            }
                        }
                    }
                }
                var visibleColumnCount = cellType.length - visibleColumnCountReduction;
                var result = {
                    height: 9000,
                    width: Math.floor(totalWidthInPixels * 30) + 50
                };

                var numGridRows = Ext.getCmp(idGrid).store.getCount() + 2;
                if (!Ext.isEmpty(Ext.getCmp(idGrid).store.groupField) || Ext.getCmp(idGrid).store.groupers.items.length > 0) {
                    numGridRows = numGridRows + Ext.getCmp(idGrid).store.getGroups().length;
                }

                // create header for worksheet
                var t = ''.concat(
                    '<Worksheet ss:Name="' + theTitle + '">',

                    '<Names>',
                    '<NamedRange ss:Name="Print_Titles" ss:RefersTo="=\'' + theTitle + '\'!R1:R2">',
                    '</NamedRange></Names>',

                    '<Table ss:ExpandedColumnCount="' + (visibleColumnCount + 2),
                    '" ss:ExpandedRowCount="' + numGridRows + '" x:FullColumns="1" x:FullRows="1" ss:DefaultColumnWidth="65" ss:DefaultRowHeight="15">',
                    colXml,
                    '<Row ss:Height="38">',
                    '<Cell ss:MergeAcross="' + (visibleColumnCount - 1) + '" ss:StyleID="title">',
                    '<Data ss:Type="String" xmlns:html="http://www.w3.org/TR/REC-html40">',
                    '<html:b>' + strName + ' - '+ theTitle + '</html:b></Data><NamedCell ss:Name="Print_Titles">',
                    '</NamedCell></Cell>',
                    '</Row>',
                    '<Row ss:AutoFitHeight="1">',
                    headerXml +
                    '</Row>'
                );

                var groupVal = "";
                var groupField = "";
                if (Ext.getCmp(idGrid).store.groupers.keys.length > 0) {
                    groupField = Ext.getCmp(idGrid).store.groupers.keys[0];
                }
                for (var i = 0, it = Ext.getCmp(idGrid).store.data.items, l = it.length; i < l; i++) {

                    if (!Ext.isEmpty(groupField)) {
                        if (groupVal != Ext.getCmp(idGrid).store.getAt(i).get(groupField)) {
                            groupVal = Ext.getCmp(idGrid).store.getAt(i).get(groupField);
                            t += generateEmptyGroupRow(groupField, "Category: " + ddsm_CategoryColumnRenderer(groupVal), cellType, includeHidden);
                        }
                    }
                    t += '<Row>';
                    var cellClass = (i & 1) ? 'odd' : 'even';
                    r = it[i].data;
                    var k = 0;
                    for (var j = 0; j < colCount; j++) {
                        if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && (includeHidden || !cm[j].hidden)) {
                            var v = r[cm[j].dataIndex];
                            if(cm[j].dataIndex == "ddsm_Category"){v = ddsm_CategoryColumnRenderer(v);}
                            if(cm[j].dataIndex == "ddsm_Status"){v = ddsm_StatusColumnRenderer(v);}
                            if(typeof v === 'undefined'){v='';}
                            if (cellType[k] !== "None") {
                                //t += '<Cell ss:StyleID="' + cellClass + cellTypeClass[k] + '"><Data ss:Type="' + cellType[k] + '">';
                                t += '<Cell ss:StyleID="' + cellClass + '"><Data ss:Type="' + 'String' + '">';
                                if (cellType[k] == 'DateTime') {
                                    if(v !== null){t += (Ext.Date.format(v, 'm-d-Y')).toString();}else{t += '';}
                                } else {
                                    if(v !== null){t += (v).toString();}else{t += '';}
                                }
                                t += '</Data></Cell>';
                            }
                            k++;
                        }
                    }
                    t += '</Row>';
                }

                result.xml = t.concat(
                    '</Table>',
                    '<WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">',
                    '<PageLayoutZoom>0</PageLayoutZoom>',
                    '<Selected/>',
                    '<Panes>',
                    '<Pane>',
                    '<Number>3</Number>',
                    '<ActiveRow>2</ActiveRow>',
                    '</Pane>',
                    '</Panes>',
                    '<ProtectObjects>False</ProtectObjects>',
                    '<ProtectScenarios>False</ProtectScenarios>',
                    '</WorksheetOptions>',
                    '</Worksheet>'
                );
                return result;
            };
            // ---- END Excel export code

            var defaultFieldsValue = {};
            dataOk = false;
            saveGridStatus = false;
            var ctrlEnter = false;
            var altEnter = false;
            var ddsm_index_toUpdateFncls = null;
            var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
            var _odataEndpoint = "/XRMServices/2011/OrganizationData.svc";
            var oneday = 1000 * 60 * 60 * 24;
            var startDate = function () {
                var startdateValue = b.Xrm.Page.getAttribute("ddsm_startdate").getValue();
                if (startdateValue != null) {
                    return startdateValue.getTime();
                } else {
                    b.alert( "Start Date of a Project is not valid!");
                    return null;
                }
            }
            var ddsm_projectstatus = b.Xrm.Page.getAttribute("ddsm_projectstatus");
            var ddsm_pendingmilestone = b.Xrm.Page.getAttribute("ddsm_pendingmilestone");
            var ddsm_responsible = b.Xrm.Page.getAttribute("ddsm_responsible");
            var ddsm_pendingactualstart = b.Xrm.Page.getAttribute("ddsm_pendingactualstart");
            var ddsm_pendingtargetend = b.Xrm.Page.getAttribute("ddsm_pendingtargetend");
            var ddsm_pendingmilestoneindex = b.Xrm.Page.getAttribute("ddsm_pendingmilestoneindex");
            var ddsm_phase = b.Xrm.Page.getAttribute("ddsm_phase");
            //var ddsm_completedstatus = b.Xrm.Page.getAttribute("ddsm_completedstatus");
            var ddsm_completedmilestone = b.Xrm.Page.getAttribute("ddsm_completedmilestone");
            //var ddsm_initialoffered = b.Xrm.Page.getAttribute("ddsm_initialoffered");

            var modelGrid, renderTo, dateFormat, heightGrid, borderGrid, titleGrid, sortersGrid;
            if (typeof configJson.entitySchemaName !== 'undefined') { entitySchemaName = configJson.entitySchemaName; } else { entitySchemaName = ""; b.alert( "The entitySchemaName parameter value is missing in the config file. Please specify the correct value in the config file of this grid (Web Resources)."); }
            if (typeof configJson.modelGrid !== 'undefined') { modelGrid = configJson.modelGrid; } else { modelGrid = "mGrid" + randomnumber; }
            if (typeof configJson.idGrid !== 'undefined') { idGrid = configJson.idGrid; } else { idGrid = "idGrid" + randomnumber; }
            if (typeof configJson.renderTo !== 'undefined') { renderTo = configJson.renderTo; } else { renderTo = Ext.getBody(); }
            if (typeof configJson.dateFormat !== 'undefined') { dateFormat = configJson.dateFormat; } else { dateFormat = "m/d/Y"; }
            if (typeof configJson.height !== 'undefined') { heightGrid = configJson.height; } else { heightGrid = 400; }
            if (typeof configJson.border !== 'undefined') { borderGrid = configJson.border; } else { borderGrid = false; }
            if (typeof configJson.title !== 'undefined') { titleGrid = configJson.title; } else { titleGrid = ''; }
            if (typeof configJson.sorters !== 'undefined') { sortersGrid = configJson.sorters; } else { sortersGrid = { property: configJson.entitySchemaName + 'Id', direction: 'ASC' }; }
            if (typeof configJson.entityConcat !== 'undefined') { entityConcat = configJson.entityConcat; }
            if (typeof configJson.fieldConcat !== 'undefined') { fieldConcat = configJson.fieldConcat; }

            _getODataEndpoint = function (entitySchemaName) {
                return Ext.String.format("{0}{1}/{2}Set", b.Xrm.Page.context.getClientUrl(), _odataEndpoint, entitySchemaName);
            }

            _getAdminDataSettings();

            function getProjTplAttribute() {
                var req = new XMLHttpRequest();
                var URI_ = _getODataEndpoint("ddsm_projecttemplate")
                    + "?$select=ddsm_AutomaticFinancialCreationIndex&$filter=ddsm_projecttemplateId eq guid'" + b.Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue()[0].id + "'";
                req.open("GET", URI_, false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        req.onreadystatechange = null;
                        if (this.status == 200) {
                            var responseData = JSON.parse(this.responseText).d;
                            var modeResponse = responseData.results;
                            if(modeResponse[0].ddsm_AutomaticFinancialCreationIndex != null){AutoFncCreationIndex = parseInt(modeResponse[0].ddsm_AutomaticFinancialCreationIndex);}else{AutoFncCreationIndex = 0;}
                        }
                    }
                }
                req.send(null);
            }
            getProjTplAttribute();

            var _makeObjFields = function (obj) {
                var fields = [];
                var new_field = {};
                if (obj.type != "lookup") {
                    if (typeof obj.defaultValue !== 'undefined') {
                        defaultFieldsValue[obj.name] = obj.defaultValue;
                    }
                    new_field.name = obj.name;
                    switch (obj.type) {
                        case 'date':
                            new_field.type = obj.type;
                            //new_field.dateFormat = dateFormat;
                            break;
                        case 'number':
                            new_field.type = obj.type;
                            break;
                        case 'boolean':
                            new_field.type = obj.type;
                            break;
                        case 'checkcolumn':
                            new_field.type = 'boolean';
                            break;
                        case 'combobox':
                            new_field.type = 'string';
                            break;
                        case 'currency':
                            new_field.type = 'number';
                            break;
                        default:
                            break;
                    }
                    return new_field;
                } else {
                    defaultFieldsValue[obj.name] = "";
                    new_field.name = obj.name;
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                    new_field.name = obj.name + '__Id';
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__LogicalName'] = Ext.util.Format.lowercase(obj.entityNameLookup);
                    new_field.name = obj.name + '__LogicalName';
                    fields.push(new_field);
                    return fields;
                }
            }
            var _createFields = function () {
                var fields = [];
                var new_field = {};
                new_field.name = entitySchemaName + 'Id';
                defaultFieldsValue[entitySchemaName + 'Id'] = '';
                fields.push(new_field);
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFields(configJson.fields[i].group[j]);
                            if ($.isArray(obj)) {
                                fields = fields.concat(obj);
                            } else {
                                fields.push(obj);
                            }
                        }
                    } else {
                        obj = _makeObjFields(configJson.fields[i]);
                        if ($.isArray(obj)) {
                            fields = fields.concat(obj);
                        } else {
                            fields.push(obj);
                        }
                    }
                }
                return fields;
            }
            var _createColumns = function () {
                var columns = [];
                var hide_columns = [];
                //columns.push(Ext.create('Ext.grid.RowNumberer'));
                var new_column = {};
                var new_editor = {};
                var new_filter = {};
                var renderer = null;
                new_column.dataIndex = entitySchemaName + 'Id';
                new_column.text = 'Id';
                new_column.filterable = false;
                new_column.sortable = false;
                new_column.hidden = true;
                new_column.field = { xtype: 'hidden'};
                new_column.width = 150;
                new_editor.readOnly = true;
                new_editor.allowBlank = true;
                new_column.editor = new_editor;
                hide_columns.push(new_column);
                for (var i = 0; i < configJson.fields.length; i++) {
                    new_column = {};
                    new_column.menuDisabled = true,
                        new_editor = {};
                    new_filter = {};
                    renderer = null;
                    new_column.dataIndex = configJson.fields[i].name;
                    new_column.header = configJson.fields[i].header;
                    new_column.tooltip = configJson.fields[i].header;
                    if (typeof configJson.fields[i].sortable !== 'undefined') {
                        new_column.sortable = configJson.fields[i].sortable;
                    }
                    if (typeof configJson.fields[i].filterable !== 'undefined') {
                        new_column.filterable = configJson.fields[i].filterable;
                    }
                    if (typeof configJson.fields[i].hidden !== 'undefined' && configJson.fields[i].hidden) {
                        new_column.hidden = configJson.fields[i].hidden;
                    }
                    if (typeof configJson.fields[i].flex !== 'undefined') {
                        new_column.flex = configJson.fields[i].flex;
                    } else {
                        if (typeof configJson.fields[i].width !== 'undefined') {
                            new_column.width = configJson.fields[i].width;
                        }
                    }
                    /*
                     if(typeof configJson.fields[i].format !== 'undefined'){
                     new_editor.format = configJson.fields[i].format;
                     }
                     */
                    if (configJson.fields[i].type != "lookup") {
                        switch (configJson.fields[i].type) {
                            case 'date':
                                if (!new_column.hidden) {
                                    if (!configJson.fields[i].readOnly) {
                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        } else {
                                            new_editor.itemId = configJson.fields[i].name;
                                            //new_editor.id = configJson.fields[i].name;
                                            new_editor.xtype = 'datefield';
                                            //new_editor.readOnly = configJson.fields[i].readOnly;
                                            new_editor.allowBlank = configJson.fields[i].allowBlank;
                                            if (configJson.fields[i].name == 'ddsm_ActualEnd') {
                                                new_editor.maxValue = new Date();
                                            }
                                            new_editor.listeners = {
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                            new_column.editor = new_editor;
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    } else {
                                        if (typeof configJson.fields[i].format !== 'undefined') {
                                            new_column.renderer = Ext.util.Format.dateRenderer(configJson.fields[i].format);
                                        } else {
                                            new_column.renderer = Ext.util.Format.dateRenderer(dateFormat);
                                        }
                                    }
                                } else {
                                    //new_editor.xtype = 'datefield';
                                    new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                            case 'number':
                                if (!new_column.hidden) {
                                    if (!configJson.fields[i].readOnly) {
                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        } else {
                                            new_editor.itemId = configJson.fields[i].name;
                                            new_editor.xtype = 'numberfield';
                                            //new_editor.readOnly = configJson.fields[i].readOnly;
                                            new_editor.allowBlank = configJson.fields[i].allowBlank;
                                            new_editor.listeners = {
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                            new_column.editor = new_editor;
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    } else if (typeof configJson.fields[i].format !== 'undefined') {
                                        new_column.renderer = Ext.util.Format.numberRenderer(configJson.fields[i].format);
                                    }
                                } else {
                                    new_column.field = { xtype: 'hidden'};
                                    //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                            case 'combobox':
                                if (!new_column.hidden) {
                                    if (!configJson.fields[i].readOnly) {
                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        } else {
                                            new_editor.itemId = configJson.fields[i].name;
                                            new_editor.xtype = 'combo';
                                            //new_editor.readOnly = configJson.fields[i].readOnly;
                                            new_editor.allowBlank = configJson.fields[i].allowBlank;
                                            new_editor.listeners = {
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                            new_column.editor = new_editor;
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    }
                                } else {
                                    new_column.field = { xtype: 'hidden'};
                                    //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                            case 'currency':
                                if (!new_column.hidden) {
                                    if (!configJson.fields[i].readOnly) {
                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        } else {
                                            new_editor.itemId = configJson.fields[i].name;
                                            new_editor.xtype = 'numberfield';
                                            //new_editor.readOnly = configJson.fields[i].readOnly;
                                            new_editor.allowBlank = configJson.fields[i].allowBlank;
                                            new_editor.listeners = {
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                            new_column.editor = new_editor;
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    } else {
                                        new_column.renderer = Ext.util.Format.usMoney;
                                    }
                                } else {
                                    new_column.field = { xtype: 'hidden'};
                                    //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                            case 'boolean':
                                if (!new_column.hidden) {
                                    if (!configJson.fields[i].readOnly) {

                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        } else {
                                            new_editor.itemId = configJson.fields[i].name;
                                            //                                new_editor.xtype = 'textfield';
                                            //new_editor.readOnly = configJson.fields[i].readOnly;
                                            new_editor.allowBlank = configJson.fields[i].allowBlank;
                                            new_editor.listeners = {
                                                focus: function (editor, e) { lookupActive = ""; }
                                            }
                                            new_column.editor = new_editor;
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    }
                                } else {
                                    new_column.field = { xtype: 'hidden'};
                                    //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                            case 'checkcolumn':
                                if (!new_column.hidden) {
                                    new_column.xtype = 'checkcolumn';
                                    new_column.listeners = {
                                        beforecheckchange: beforeCheckChangeSkip,
                                        checkChange: onCheckChangeSkip
                                    };
                                    //new_column.disabled = true;
                                    if (!configJson.fields[i].readOnly) {

                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    }
                                } else {
                                    new_column.field = { xtype: 'hidden'};
                                    //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                            default:
                                if (!new_column.hidden) {
                                    if (!configJson.fields[i].readOnly) {

                                        if (typeof configJson.fields[i].editor !== 'undefined') {
                                            if (typeof configJson.fields[i].editor !== 'object') {
                                                new_column.editor = eval(configJson.fields[i].editor);
                                            } else {
                                                new_column.editor = configJson.fields[i].editor;
                                            }
                                        } else {
                                            new_editor.itemId = configJson.fields[i].name;
                                            new_editor.xtype = 'textfield';
                                            //new_editor.readOnly = configJson.fields[i].readOnly;
                                            new_editor.allowBlank = configJson.fields[i].allowBlank;
                                            new_column.editor = new_editor;
                                        }
                                    }
                                    if (typeof configJson.fields[i].renderer !== 'undefined') {
                                        if (typeof configJson.fields[i].renderer !== 'object') {
                                            new_column.renderer = eval(configJson.fields[i].renderer);
                                        } else {
                                            new_column.renderer = configJson.fields[i].renderer;
                                        }
                                    }
                                } else {
                                    new_column.field = { xtype: 'hidden'};
                                    //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                                }
                                break;
                        }
                        if (typeof configJson.fields[i].hidden !== 'undefined' && configJson.fields[i].hidden) {
                            hide_columns.push(new_column);
                        } else {
                            columns.push(new_column);
                        }
                    } else {
                        if (!new_column.hidden) {
                            if (!configJson.fields[i].readOnly) {

                                if (typeof configJson.fields[i].editor !== 'undefined') {
                                    if (typeof configJson.fields[i].editor !== 'object') {
                                        new_column.editor = eval(configJson.fields[i].editor);
                                    } else {
                                        new_column.editor = configJson.fields[i].editor;
                                    }
                                } else {
                                    new_editor.itemId = configJson.fields[i].name;
                                    //new_editor.readOnly = configJson.fields[i].readOnly;
                                    new_editor.allowBlank = configJson.fields[i].allowBlank;
                                    new_editor.listeners = {
                                        //scope:this,
                                        focus: function (e) {
                                            if ((this.value == "" || this.value == null) && lookupActive != e.name) {
                                                lookupActive = e.name;
                                                _getLookupData(e.name);
                                            }
                                        }
                                    };

                                    new_column.editor = new_editor;
                                }
                            }
                            if (typeof configJson.fields[i].renderer !== 'undefined') {
                                if (typeof configJson.fields[i].renderer !== 'object') {
                                    new_column.renderer = eval(configJson.fields[i].renderer);
                                } else {
                                    new_column.renderer = configJson.fields[i].renderer;
                                }
                            } else {
                                new_column.renderer = function (value, metaData, record, rowIdx, colIdx, store, view) {
                                    var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                                    var column = view.getHeaderAtIndex(colIdx);
                                    if (value != null) {
                                        return Ext.String.format(
                                            '<a href="/main.aspx?etn={2}&extraqs=&histKey={3}&id={1}&newWindow=true&pagetype=entityrecord" target="_blank">{0}</a>',
                                            value,
                                            "{" + record.data[column.dataIndex + "__Id"] + "}",
                                            record.data[column.dataIndex + "__LogicalName"],
                                            randomnumber
                                        );
                                    } else { return ''; }
                                };
                            }
                        } else {
                            new_column.field = { xtype: 'hidden'};
                            //new_editor.xtype = 'hidden'; new_column.editor = new_editor;
                        }

                        if (typeof configJson.fields[i].hidden !== 'undefined' && configJson.fields[i].hidden) {
                            hide_columns.push(new_column);
                        } else {
                            columns.push(new_column);
                        }

                        new_column = {};
                        new_editor = {};
                        new_column.dataIndex = configJson.fields[i].name + '__Id';
                        new_column.text = configJson.fields[i].name + '__Id';
                        new_column.filterable = false;
                        new_column.sortable = false;
                        new_column.hidden = true;
                        new_column.width = 50;
                        new_column.field = { xtype: 'hidden'};
                        new_editor.itemId = configJson.fields[i].name + '__Id';
                        new_editor.readOnly = true;
                        new_editor.allowBlank = true;
                        new_column.editor = new_editor;
                        hide_columns.push(new_column);

                        new_column = {};
                        new_editor = {};
                        new_column.dataIndex = configJson.fields[i].name + '__LogicalName';
                        new_column.text = configJson.fields[i].name + '__LogicalName';
                        new_column.filterable = false;
                        new_column.sortable = false;
                        new_column.hidden = true;
                        new_column.width = 50;
                        new_column.field = { xtype: 'hidden'};
                        new_editor.itemId = configJson.fields[i].name + '__LogicalName';
                        new_editor.readOnly = true;
                        new_editor.allowBlank = true;
                        new_column.editor = new_editor;
                        hide_columns.push(new_column);
                    }
                }
                return columns.concat(hide_columns);
            }
            var _makeObjFilters = function (obj) {
                var filterType = {};
                if (obj.filterable) {
                    if (typeof obj.filter !== 'undefined') {
                        if (typeof obj.filter !== 'filterTypeect') {
                            filterType = eval(obj.filter);
                        } else {
                            filterType = obj.filter;
                        }
                    } else {
                        switch (obj.type) {
                            case 'date':
                                filterType.type = 'date';
                                break;
                            case 'number':
                                filterType.type = 'numeric';
                                break;
                            case 'combobox':
                                /*
                                 if(typeof obj.editor !== 'undefined'){
                                 if(typeof obj.editor!== 'filterTypeect'){
                                 var cb = eval(obj.editor);
                                 } else {
                                 var cb = obj.editor;
                                 }
                                 filterType.type = 'list';
                                 filterType.labelField = 'nom_categorie';
                                 filterType.options = [
                                 [ "0", "inactive" ],
                                 [ "1", "in work" ],
                                 [ "2", "executed" ],
                                 [ "3", "canceled" ]
                                 ];
                                 }
                                 */
                                break;
                            case 'currency':
                                filterType.type = 'numeric';
                                break;
                            case 'lookup':
                                filterType.type = 'string';
                                break;
                            default:
                                filterType.type = 'string';
                                break;
                        }
                        filterType.dataIndex = obj.name;
                    }
                }
                return filterType;

            }
            var _createFilters = function () {
                var filterObj = {
                    ftype: 'filters',
                    encode: false,
                    local: true,
                    filters: []
                }
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFilters(configJson.fields[i].group[j]);
                            if (obj != null) {
                                if ($.isArray(obj)) {
                                    filterObj.filters = filterObj.filters.concat(obj);
                                } else {
                                    filterObj.filters.push(obj);
                                }
                            }
                        }
                    } else {
                        obj = _makeObjFilters(configJson.fields[i]);
                        if (obj != null) {
                            if ($.isArray(obj)) {
                                filterObj.filters = filterObj.filters.concat(obj);
                            } else {
                                filterObj.filters.push(obj);
                            }
                        }
                    }
                }
                return filterObj;
            };
            GetUserTimeZoneDetails = function () {

                var ODataPath = b.Xrm.Page.context.getClientUrl() + "/XRMServices/2011/OrganizationData.svc";
                var filter = "/UserSettingsSet(guid'" + b.Xrm.Page.context.getUserId() + "')";

                var userSettingReq = new XMLHttpRequest();
                userSettingReq.open('GET', ODataPath + filter, false);
                userSettingReq.setRequestHeader("Accept", "application/json");
                userSettingReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                userSettingReq.send(null);

                var userSetting = this.JSON.parse(userSettingReq.responseText).d;
                var timeZoneBias = 0;
                try {
                    timeZoneBias = userSetting.TimeZoneBias;
                }
                catch (e) {
                    timeZoneBias = 0;
                }

                var timeZoneDayLightBias = 0;

                try {
                    timeZoneDayLightBias = userSetting.TimeZoneDaylightBias;
                }
                catch (e) {
                    timeZoneDayLightBias = 0;
                }

                return [timeZoneBias, timeZoneDayLightBias];
            }
            var _toUTCDate = function (date) {
                return new Date(date.getTime());
            }
            function UTCToLocalTime(d) {
                var date = new Date(d);
                return ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();

            }

            var _getLookupData = function (entityName) {
                entityName_getLookupData = entityName;
                var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                var localname = record.get(entityName + '__LogicalName');
                var objecttypes = b.Xrm.Internal.getEntityCode(localname);
                var lookupURI = "/_controls/lookup/lookupinfo.aspx";
                lookupURI += "?LookupStyle=single";
                lookupURI += "&objecttypes=" + objecttypes;
                lookupURI += "&ShowNewButton=0";
                lookupURI += "&ShowPropButton=1";
                lookupURI += "&browse=false";
                lookupURI += "&AllowFilterOff=0";
                lookupURI += "&DefaultType=" + objecttypes;
                lookupURI += "&DisableQuickFind=0";
                lookupURI += "&DisableViewPicker=0";
                //console.log("lookupURI: " + lookupURI);

                window.setTimeout(function () {
                    var DialogOption = new b.Xrm.DialogOptions;
                    DialogOption.width = 550; DialogOption.height = 550;
                    b.Xrm.Internal.openDialog(lookupURI, DialogOption, null, null, Callback_lkp_popup);
                }, 200);
            }
            function Callback_lkp_popup(lkp_popup) {
                try {
                    if (lkp_popup) {
                        if (lkp_popup.items) {
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData).setValue(lkp_popup.items[0].name);
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData + '__Id').setValue((lkp_popup.items[0].id).replace(/\{|\}/g, ''));
                            rowEditing.getEditor().getForm().findField(entityName_getLookupData + '__LogicalName').setValue(lkp_popup.items[0].typename);
                            var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                            record.set(entityName_getLookupData, lkp_popup.items[0].name);
                            record.set(entityName_getLookupData + '__Id', (lkp_popup.items[0].id).replace(/\{|\}/g, ''));
                            record.set(entityName_getLookupData + '__LogicalName', lkp_popup.items[0].typename);
                        }
                        lookupActive = "";
                    }
                }
                catch (err) {
                    alert(err);
                }
            }
            var qtip = Ext.create('Ext.tip.QuickTip', {});
            rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToMoveEditor: 1,
                autoCancel: false
            });
            var viewConfig = {
                //loadingText: "Loading data ...",
                stripeRows: true,
                //trackOver:false,
                getRowClass: function (record, index, rowParams, store) {
                    if (record.get('ddsm_Status') == '962080001') {
                        return 'activeMS';
                    }
                }
            };
            //// START CHECKBOX SKIP
            function beforeCheckChangeSkip(column, rowIndex, checked, eOpts) {

                var record = Ext.getCmp(idGrid).getStore().getAt(rowIndex);
//                if(record.get('ddsm_Status') == 962080001 || record.get('ddsm_ActualEnd') != null) {
                if(record.get('ddsm_Status') == 962080002) {
                    return false;
                } else {return true;}

                return true;
            }

            function onCheckChangeSkip(column, rowIndex, checked, eOpts) {

//                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
//                var recordsCount = recordsGrid.length;

                var record = Ext.getCmp(idGrid).getStore().getAt(rowIndex);
                /*
                 if(checked) {
                 if (record.get('ddsm_Status') == 962080001) {
                 record.set("ddsm_ActualEnd", record.get("ddsm_ActualStart"));
                 record.set("ddsm_Status", "962080003");
                 } else {
                 record.set("ddsm_Status", "962080003");
                 }
                 } else {

                 }
                 */
                /*
                 var idx = parseInt(record.get('ddsm_Index'));

                 groupedCategory = Ext.getCmp(idGrid).getStore().isGrouped();
                 if (filteringCategory != '000000000') {
                 Ext.getCmp(idGrid).getStore().clearFilter();
                 }
                 groupingFeature.disable();
                 Ext.getCmp(idGrid).getStore().clearGrouping();
                 var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                 recordsGrid[idx - 1].set("ddsm_SkipMilestone", record.get('ddsm_SkipMilestone'));
                 Ext.getCmp(idGrid).getStore().commitChanges();
                 */

                Save_Record(record);
                Ext.getCmp(idGrid).getStore().commitChanges();
                /*
                 Ext.getCmp(idGrid).getStore().group('ddsm_Category');
                 if (filteringCategory != '000000000') {
                 Ext.getCmp(idGrid).getStore().filter('ddsm_Category', filteringCategory, true);
                 }
                 if (groupedCategory) {
                 groupingFeature.enable();
                 } else {
                 groupingFeature.disable();
                 }
                 Ext.getCmp(idGrid).getView().refresh();

                 Ext.getCmp(idGrid).getStore().commitChanges();
                 */
                /*
                 saveGridStatus = false;

                 Ext.getCmp(idGrid).getStore().commitChanges();
                 Ext.getCmp(idGrid).getView().refresh()
                 */
                //var record = Ext.getCmp(idGrid).getStore().getAt(rowIndex);
                //console.dir(record);
            }
            //// END CHECKBOX SKIP


            loadData = function (URI, reload) {
                var proj_ID = b.Xrm.Page.data.entity.getId();
                if (proj_ID != "") {

                    if(URI == _getODataEndpoint(entitySchemaName)) {
                        results = [];
                        URI = b.Xrm.Page.context.getClientUrl() + "/XRMServices/2011/OrganizationData.svc/" + entitySchemaName + "Set?"
                            + "$select="
                            + "ddsm_name,"
                            + "ddsm_Category,"
                            + "ddsm_ActualStart,"
                            + "ddsm_ActualEnd,"
                            + "ddsm_Status,"
                            + "ddsm_Index,"
                            + "ddsm_SkipMilestone,"
                            + "ddsm_milestoneId,"
                            + "ddsm_TargetStart,"
                            + "ddsm_TargetEnd,"
                            + "ddsm_EstimatedEnd,"
                            + "ddsm_MsTpltoMs,"
                            + "ddsm_UseValidation,"
                            + "ddsm_RequiresEmptyMeasure,"
                            + "ddsm_RequiresValidMeasure,"
                            + "ddsm_ApprovalThresholdCust,"
                            + "ddsm_ApprovalThresholdStd,"
                            + "ddsm_ApprovalThresholdNC,"
                            + "ddsm_ApprovalThresholdRCx,"
                            + "ddsm_Duration,"
                            + "ddsm_ActualDuration,"
                            + "ddsm_ProjectPhaseName,"
                            + "ddsm_ProjectStatus,"
                            + "ddsm_Responsible,"
                            + "ddsm_MSSpecialNote,"
                            + "ddsm_InitialOffered,"
                            + "ddsm_SecurityIncludeOnlyGUID,"
                            + "ddsm_SecurityExcludeGUID,"
                            + "ddsm_insertset,"
                            + "ddsm_indexoffset,"
                            + "CreatedOn,"
                            + "CreatedBy,"
                            + "ModifiedOn,"
                            + "ModifiedBy,"
                            + "ddsm_InsertedSetsID,"
                            + "ddsm_CurrEscalOverdueLength,"
                            + "ddsm_TradeAllyTemplateGUID,"
                            + "ddsm_CustomerTemplateGUID,"
                            + "ddsm_ProjectTaskCount"
                            + "&$orderby=ddsm_Index asc"
                            + "&$filter=ddsm_ProjectToMilestoneId/Id eq guid'" + proj_ID + "' and statecode/Value eq 0";
                    }
                    //console.log(URI_);
                    Ext.getCmp(idGrid).mask("Loading data, please wait...");
                    var req = new XMLHttpRequest();
                    req.open("GET", URI, false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.onreadystatechange = function () {
                        if (this.readyState == 4) {
                            req.onreadystatechange = null;
                            if (this.status == 200) {

                                var responseData = JSON.parse(this.responseText).d;
                                //console.log(responseData);
                                var newData = [];
                                var modeResponse = responseData.results;
                                if (modeResponse.length > 0) {
                                    /*
                                     for(var i=0;i<modeResponse.length;i++){
                                     for (var key in modeResponse[i].ddsm_MilestoneTpltoMilestone) {
                                     modeResponse[i][key] = modeResponse[i].ddsm_MilestoneTpltoMilestone[key];
                                     }
                                     }
                                     */
                                    for (var i = 0; i < modeResponse.length; i++) {
                                        //console.dir(modeResponse[i]);

                                        var objRecord = new Object();
                                        objRecord[entitySchemaName + "Id"] = modeResponse[i][entitySchemaName + "Id"];
                                        for (var j = 0; j < configJson.fields.length; j++) {
                                            switch (configJson.fields[j].type) {
                                                case 'date':
                                                    if ((typeof modeResponse[i][configJson.fields[j].name] !== 'undefined') && (modeResponse[i][configJson.fields[j].name] != null)) {
                                                        //objRecord[configJson.fields[j].name] = parseJsonDate(modeResponse[i][configJson.fields[j].name]);
                                                        objRecord[configJson.fields[j].name] = eval((modeResponse[i][configJson.fields[j].name]).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                                    } else if (modeResponse[i][configJson.fields[j].name] == null) { objRecord[configJson.fields[j].name] = modeResponse[i][configJson.fields[j].name] }
                                                    break
                                                case 'lookup':
                                                    if (typeof modeResponse[i][configJson.fields[j].name] !== 'undefined' && modeResponse[i][configJson.fields[j].name].Id != null) {
                                                        objRecord[configJson.fields[j].name] = modeResponse[i][configJson.fields[j].name].Name;
                                                        objRecord[configJson.fields[j].name + '__Id'] = modeResponse[i][configJson.fields[j].name].Id;
                                                        objRecord[configJson.fields[j].name + '__LogicalName'] = modeResponse[i][configJson.fields[j].name].LogicalName;
                                                    } else if (modeResponse[i][configJson.fields[j].name].Id == null || modeResponse[i][configJson.fields[j].name].Id == '') {
                                                        objRecord[configJson.fields[j].name] = '';
                                                        objRecord[configJson.fields[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                                                        objRecord[configJson.fields[j].name + '__LogicalName'] = configJson.fields[j].entityNameLookup;
                                                    }
                                                    break
                                                case 'combobox':
                                                    if (typeof modeResponse[i][configJson.fields[j].name] !== 'undefined') {
                                                        objRecord[configJson.fields[j].name] = modeResponse[i][configJson.fields[j].name].Value;
                                                    }
                                                    break
                                                case 'currency':
                                                    if (typeof modeResponse[i][configJson.fields[j].name] !== 'undefined') {
                                                        objRecord[configJson.fields[j].name] = modeResponse[i][configJson.fields[j].name].Value;
                                                    }
                                                    break
                                                case 'number':
                                                    if (typeof modeResponse[i][configJson.fields[j].name] !== 'undefined') {
                                                        objRecord[configJson.fields[j].name] = modeResponse[i][configJson.fields[j].name];
                                                    }
                                                    break
                                                default:
                                                    if (typeof modeResponse[i][configJson.fields[j].name] !== 'undefined') {
                                                        objRecord[configJson.fields[j].name] = modeResponse[i][configJson.fields[j].name];
                                                    }
                                            }
                                        }
                                        newData.push(objRecord);
                                    }
                                    results = results.concat(newData);
                                    if (responseData.__next) {
                                        //console.log(responseData.__next);
                                        Ext.getCmp(idGrid).unmask();
                                        loadData(responseData.__next, reload);
                                    } else {
                                        console.log(">>>>> load data ok! records: " + results.length);
                                        dataOk = true;
                                        //b.Xrm.Page.ui.tabs.get("Tab_Milestones").setDisplayState('expanded');
                                        //b.Xrm.Page.ui.tabs.get("Tab_Milestones").setVisible(true);

                                        if (!reload) {
                                            Ext.define(modelGrid, {
                                                extend: 'Ext.data.Model',
                                                preventFocus: true,
                                                fields: _createFields()
                                            });
                                            Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                            //Ext.getStore('store' + idGrid).clearGrouping();
                                            //groupingFeature.disable();
                                            Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            Ext.getCmp(idGrid).getView().refresh();

                                        } else {
                                            //groupingFeature.disable();
                                            Ext.getCmp(idGrid).getStore().loadData(results);
                                            Ext.getCmp(idGrid).filters.clearFilters();
                                            Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            //allRecordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                                            Ext.getCmp(idGrid).getView().refresh();
                                        }
                                        /*
                                         window.setTimeout(function () {
                                         if (b.firstLoadGrid != 3) {
                                         b.firstLoadGrid++;
                                         b.Xrm.Page.ui.tabs.get("Tab_Milestones").setVisible(false);
                                         }
                                         }, 1000);
                                         */
                                        saveGridStatus = true;
                                        Ext.getCmp(idGrid).unmask();
                                    }
                                } else {
                                    console.log(">>>>> data entity null!");
                                    dataOk = false;
                                    //b.Xrm.Page.ui.tabs.get("Tab_Milestones").setDisplayState('expanded');
                                    //b.Xrm.Page.ui.tabs.get("Tab_Milestones").setVisible(true);

                                    if (!reload) {
                                        Ext.define(modelGrid, {
                                            extend: 'Ext.data.Model',
                                            preventFocus: true,
                                            fields: _createFields()
                                        });
                                        Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                        //Ext.getStore('store' + idGrid).clearGrouping();
                                        //groupingFeature.disable();
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    } else {
                                        //groupingFeature.disable();
                                        Ext.getCmp(idGrid).getStore().loadData(results);
                                        Ext.getCmp(idGrid).filters.clearFilters();
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        //allRecordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                                        Ext.getCmp(idGrid).getView().refresh();
                                    }
                                    /*
                                     window.setTimeout(function () {
                                     if (b.firstLoadGrid != 3) {
                                     b.firstLoadGrid++;
                                     b.Xrm.Page.ui.tabs.get("Tab_Milestones").setVisible(false);
                                     }
                                     }, 1000);
                                     */
                                    saveGridStatus = true;
                                    Ext.getCmp(idGrid).unmask();
                                }
                            } else {
                                if (results.length > 0) {
                                    //loadMilestoneGrid(results);
                                    console.log(">>>>> load data ok! records: " + results.length);
                                    dataOk = true;
                                    //b.Xrm.Page.ui.tabs.get("Tab_Milestones").setDisplayState('expanded');
                                    //b.Xrm.Page.ui.tabs.get("Tab_Milestones").setVisible(true);

                                    if (!reload) {
                                        Ext.define(modelGrid, {
                                            extend: 'Ext.data.Model',
                                            preventFocus: true,
                                            fields: _createFields()
                                        });
                                        Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                        //Ext.getStore('store' + idGrid).clearGrouping();
                                        //groupingFeature.disable();
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    } else {
                                        //groupingFeature.disable();
                                        Ext.getCmp(idGrid).getStore().loadData(results);
                                        Ext.getCmp(idGrid).filters.clearFilters();
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        //allRecordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                                        Ext.getCmp(idGrid).getView().refresh();
                                    }
                                    /*
                                     window.setTimeout(function () {
                                     if (b.firstLoadGrid != 3) {
                                     b.firstLoadGrid++;
                                     b.Xrm.Page.ui.tabs.get("Tab_Milestones").setVisible(false);
                                     }
                                     }, 1000);
                                     */
                                    saveGridStatus = true;
                                }
                            }
                            Ext.getCmp(idGrid).unmask();
                        }
                    };
                    lookupActive = "";
                    req.send(null);
                }
            }

            function Save_Record(record) {
                var guidId = record.data[entitySchemaName + 'Id'];

                var changes = {};
                for (var j = 0; j < configJson.fields.length; j++) {
                    if (configJson.fields[j].saved) {
                        //console.log(">>> " + i +" : " + j + " >>> type: " + configJson.fields[j].type + " | name " + configJson.fields[j].name + " | value " + record.data[configJson.fields[j].name]);
                        if (record.data[configJson.fields[j].name] != null && (((record.data[configJson.fields[j].name]).toString()).replace(/\s+/g, '')).length == 0) { record.data[configJson.fields[j].name] = null; }
                        switch (configJson.fields[j].type) {

                            case 'date':

                                if (record.data[configJson.fields[j].name] != null) {
                                    changes[configJson.fields[j].name] = _toUTCDate(record.data[configJson.fields[j].name]);
                                } else {
                                    changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];
                                }

                                break;
                            case 'lookup':

                                if (record.data[configJson.fields[j].name] == null) { record.data[configJson.fields[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000'; }
                                if (record.data[configJson.fields[j].name + '__Id'] != '00000000-0000-0000-0000-000000000000' && record.data[configJson.fields[j].name] != null) {
                                    var lookup = {};
                                    lookup.Name = record.data[configJson.fields[j].name];
                                    lookup.Id = record.data[configJson.fields[j].name + '__Id'];
                                    lookup.LogicalName = record.data[configJson.fields[j].name + '__LogicalName'];
                                    changes[configJson.fields[j].name] = lookup;
                                } else {
                                    var lookup = {};
                                    lookup.Name = null;
                                    lookup.Id = null;
                                    lookup.LogicalName = null;
                                    changes[configJson.fields[j].name] = lookup;
                                }

                                break;
                            case 'combobox':

                                if (record.data[configJson.fields[j].name] != null) {
                                    changes[configJson.fields[j].name] = { Value: (record.data[configJson.fields[j].name]).toString() };
                                } else {
                                    changes[configJson.fields[j].name] = { Value: record.data[configJson.fields[j].name] };
                                }

                                break;
                            case 'number':

                                if (record.data[configJson.fields[j].name] != null) {
                                    changes[configJson.fields[j].name] = (record.data[configJson.fields[j].name]).toString();
                                } else {
                                    changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];
                                }

                                break;
                            case 'checkcolumn':

                                changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];

                                break;
                            case 'boolean':

                                changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];

                                break;
                            case 'currency':

                                if (record.data[configJson.fields[j].name] != null) {
                                    changes[configJson.fields[j].name] = { Value: (record.data[configJson.fields[j].name]).toString() };
                                } else {
                                    changes[configJson.fields[j].name] = { Value: record.data[configJson.fields[j].name] };
                                }

                                break;

                            default:
                                if (record.data[configJson.fields[j].name] != null) {
                                    changes[configJson.fields[j].name] = (record.data[configJson.fields[j].name]).toString();
                                } else {
                                    changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];
                                }
                                break;
                        }
                    }
                }

                var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";
                //console.log(URI);
                var req = new XMLHttpRequest();
                req.open("POST", URI, false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader("X-HTTP-Method", "MERGE");
                req.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        req.onreadystatechange = null;
                        if (this.status == 204) {
                            console.log("Saved Milestone record Ok!");
                            saveGridStatus = true;
                        }
                    }
                };
                lookupActive = "";
                req.send(JSON.stringify(changes));
                //loadData(_getODataEndpoint(entitySchemaName), false);

            }

            Save_RecordAll = function () {
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                //console.dir(recordsGrid);
                var recordsCount = recordsGrid.length;
                var activMsIdx = -1;
                if (recordsCount > 0) {
                    for (var i = 0; i < recordsCount; i++) {
                        var record = recordsGrid[i];
                        if(record.get("ddsm_Status") == "962080001" && record.get("ddsm_ActualEnd") == null)
                        {
                            activMsIdx = i;
                        }

                        var guidId = record.data[entitySchemaName + 'Id'];
                        var changes = {};
                        for (var j = 0; j < configJson.fields.length; j++) {
                            if (configJson.fields[j].saved) {
                                //console.log(">>> " + i +" : " + j + " >>> type: " + configJson.fields[j].type + " | name " + configJson.fields[j].name + " | value " + record.data[configJson.fields[j].name]);
                                if (record.data[configJson.fields[j].name] != null && (((record.data[configJson.fields[j].name]).toString()).replace(/\s+/g, '')).length == 0) { record.data[configJson.fields[j].name] = null; }
                                switch (configJson.fields[j].type) {

                                    case 'date':

                                        if (record.data[configJson.fields[j].name] != null) {
                                            changes[configJson.fields[j].name] = _toUTCDate(record.data[configJson.fields[j].name]);
                                        } else {
                                            changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];
                                        }

                                        break;
                                    case 'lookup':

                                        if (record.data[configJson.fields[j].name] == null) { record.data[configJson.fields[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000'; }
                                        if (record.data[configJson.fields[j].name + '__Id'] != '00000000-0000-0000-0000-000000000000' && record.data[configJson.fields[j].name] != null) {
                                            var lookup = {};
                                            lookup.Name = record.data[configJson.fields[j].name];
                                            lookup.Id = record.data[configJson.fields[j].name + '__Id'];
                                            lookup.LogicalName = record.data[configJson.fields[j].name + '__LogicalName'];
                                            changes[configJson.fields[j].name] = lookup;
                                        } else {
                                            var lookup = {};
                                            lookup.Name = null;
                                            lookup.Id = null;
                                            lookup.LogicalName = null;
                                            changes[configJson.fields[j].name] = lookup;
                                        }

                                        break;
                                    case 'combobox':

                                        if (record.data[configJson.fields[j].name] != null) {
                                            changes[configJson.fields[j].name] = { Value: (record.data[configJson.fields[j].name]).toString() };
                                        } else {
                                            changes[configJson.fields[j].name] = { Value: record.data[configJson.fields[j].name] };
                                        }

                                        break;
                                    case 'number':

                                        if (record.data[configJson.fields[j].name] != null) {
                                            changes[configJson.fields[j].name] = (record.data[configJson.fields[j].name]).toString();
                                        } else {
                                            changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];
                                        }

                                        break;
                                    case 'checkcolumn':

                                        changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];

                                        break;
                                    case 'boolean':

                                        changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];

                                        break;
                                    case 'currency':

                                        if (record.data[configJson.fields[j].name] != null) {
                                            changes[configJson.fields[j].name] = { Value: (record.data[configJson.fields[j].name]).toString() };
                                        } else {
                                            changes[configJson.fields[j].name] = { Value: record.data[configJson.fields[j].name] };
                                        }

                                        break;

                                    default:
                                        if (record.data[configJson.fields[j].name] != null) {
                                            changes[configJson.fields[j].name] = (record.data[configJson.fields[j].name]).toString();
                                        } else {
                                            changes[configJson.fields[j].name] = record.data[configJson.fields[j].name];
                                        }
                                        break;
                                }
                            }
                        }

                        var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";

                        //console.log(URI);
                        var req = new XMLHttpRequest();
                        req.open("POST", URI, false);
                        req.setRequestHeader("Accept", "application/json");
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader("X-HTTP-Method", "MERGE");
                        req.onreadystatechange = function () {
                            if (this.readyState == 4) {
                                req.onreadystatechange = null;
                                if (this.status == 204) {
                                }
                            }
                        };
                        lookupActive = "";
                        req.send(JSON.stringify(changes));
                        //console.log("Saved Milestone record " + i + " Ok!");
                    }
                    console.log(">>>>> save data all records");

                    if(activMsIdx != -1)
                        ddsm_Proj_CopyPhaseCheck(((activMsIdx - 1) > 0)?(activMsIdx - 1):0);

                    bussinessProcessFlag = true;
                    saveGridStatus = true;
                    //loadData(_getODataEndpoint(entitySchemaName), false);
                    //b.RecalculateCurrMSDaysPastDue();
/*
                    if(!bpClick_btn && b.Xrm.Page.getAttribute("processid").getValue() != null) {
                        if(NextPrevBP_MS_Flag != null) {
                            if (NextPrevBP_MS_Flag) {
                                if(recordStatus == 962080001) {
                                    //b.Xrm.Page.data.process.moveNext(function(d){console.dir(d);});
                                    if(activMsIdx != -1)
                                        ddsm_Proj_CopyPhaseCheck(((activMsIdx - 1) > 0)?(activMsIdx - 1):0);
                                    b.BP_NextStage();
                                }
                            } else {
                                if (b.Xrm.Page.getAttribute("ddsm_pendingmilestoneindex").getValue() != recordsCount) {
                                    //b.Xrm.Page.data.process.movePrevious(function(d){console.dir(d);});
                                    b.BP_PrevStage();
                                }
                            }
                        }
                    }
                    */
                    bpClick_btn = false;
                    NextPrevBP_MS_Flag = null;
                }
            }
            function Delete_Record(records, grid) {
                for (var i = 0; i < records.length; i++) {
                    var guidId = records[i].data[entitySchemaName + 'Id'];
                    if (guidId != "") {
                        var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";
                        //console.log(URI);
                        var req = new XMLHttpRequest();
                        req.open("POST", URI, false);
                        req.setRequestHeader("Accept", "application/json");
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader("X-HTTP-Method", "DELETE");
                        req.onreadystatechange = function () {
                            if (this.readyState == 4) {
                                req.onreadystatechange = null;
                                //console.log(this.status);
                                if ((this.status == 204) || (this.status == 1223)) {
                                    console.log("DELETE: " + guidId);
                                }
                            }
                        };
                        lookupActive = "";
                        req.send(null);
                    }
                }
                grid.getStore().commitChanges();
                Ext.getCmp(idGrid).getView().refresh();

            }

            var Insert_MS_Set = function (insertbeforeindex, status, indexoffset, startDate, insertedSetsID) {
                var projTpl_ID = b.Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue()[0].id;
                var insertSetId, emptySetNameList = true;
                var selectedIndex = 0, idxOffset = 0;
                if (indexoffset != null) idxOffset = parseInt(indexoffset);
                var req = new XMLHttpRequest();
                var URI_ = b.Xrm.Page.context.getClientUrl();
                if (completedOutofSequence != '962080002') {
                    URI_ = b.Xrm.Page.context.getClientUrl() + "/XRMServices/2011/OrganizationData.svc/ddsm_milestonesetSet?"
                        + "$select="
                        + "ddsm_milestonesetId,"
                        + "ddsm_numberofsets,"
                        + "ddsm_name,"
                        + "ddsm_Category,"
                        + "ddsm_ProjectTemplate"
                        + "&$filter=ddsm_insertbeforeindex eq " + (parseInt(insertbeforeindex) - idxOffset) + " and ddsm_ProjectTemplate/Id eq guid'" + projTpl_ID + "' and statecode/Value eq 0";
                } else {
                    URI_ = b.Xrm.Page.context.getClientUrl() + "/XRMServices/2011/OrganizationData.svc/ddsm_milestonesetSet?"
                        + "$select="
                        + "ddsm_milestonesetId,"
                        + "ddsm_numberofsets,"
                        + "ddsm_name,"
                        + "ddsm_Category,"
                        + "ddsm_ProjectTemplate"
                        + "&$filter=ddsm_ProjectTemplate/Id eq guid'" + projTpl_ID + "' and statecode/Value eq 0";
                }

                //console.log(URI_);
                req.open("GET", URI_, false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        req.onreadystatechange = null;
                        if (this.status == 200) {

                            var responseData = JSON.parse(this.responseText).d;
                            var modeResponse = responseData.results;
                            if (modeResponse.length > 0) {

                                var popupHTML = '<div id="overlay" style="display:none; position:fixed; z-index:998; opacity:0.5; filter:alpha(opacity=50); top:0; right:0; left:0; bottom:0; background:#000000 url(/WebResources/ddsm_loading.gif) 50% 50% no-repeat;"></div>';
                                popupHTML += '<div id="loading" style="display:none; position:fixed; width:320px; height:320px; top:50%; left:50%; margin:-160px 0 0 -160px; z-index:999;"><img src="/WebResources/ddsm_loading.gif" alt="loading"/></div>';
                                popupHTML += '<div id="popup" style="display:none; position:fixed; border:3px solid #999; background:#fff; width:auto; height:auto; top:50%; left:50%; margin:-100px 0 0 -300px; z-index:1000;  border-radius:10px; padding:10px 30px 30px 30px;">';
                                popupHTML += '<h2 style="color: #aa0000;font-size: 16px;">Insert Milestone Set</h2>';
                                popupHTML += '<table><tr><td style="font-size: 12px;"><strong>Name&nbsp;MS&nbsp;Set:&nbsp;</strong></td><td id="tdSelect">';
                                //popupHTML += '<select id="SetNameList"><option value="0000-0000-0000-0000-000000000000" selected>Select Milestone Set</option></select>';
                                popupHTML += '</td><td style="padding-left:10px;width:100px;"><input type="button" value="Next" id="insertSet" name="insertSet" style="margin:0 5px 0 80px;width:50px;"></td><td>';
                                popupHTML += '</td><td style="padding-lefty10px;"><input type="button" value="Cancel" id="cancelSet" name="cancelSet"></td><td></tr></table>';
                                popupHTML += '</div>';

                                var NumberOfSet = [];
                                var SetNameList = $("<select>").attr("id", "SetNameList").attr("name", "SetNameList");
                                for (var i = 0; i < modeResponse.length; i++) {
                                    var textOpt = '[' + ddsm_CategoryColumnRenderer((modeResponse[i].ddsm_Category.Value).toString()) + '] ' + modeResponse[i].ddsm_name + ' [';
                                    if (modeResponse[i].ddsm_numberofsets != null) {
                                        textOpt += modeResponse[i].ddsm_numberofsets;
                                        NumberOfSet.push(parseInt(modeResponse[i].ddsm_numberofsets));
                                    } else {
                                        textOpt += "0";
                                        NumberOfSet.push(0);
                                    }
                                    textOpt += ']';
                                    if (((insertedSetsID != null) ? insertedSetsID : "").indexOf(modeResponse[i].ddsm_milestonesetId) == -1) {
                                        emptySetNameList = false;
                                        SetNameList.append($('<option>', {
                                            value: modeResponse[i].ddsm_milestonesetId,
                                            text: textOpt
                                        }));
                                    }
                                }
                                if (!emptySetNameList) {
                                    $('body').append(popupHTML);
                                    SetNameList.change(function () {
                                        insertSetId = $(this).val();
                                        selectedIndex = $(this)[0].selectedIndex;
                                    }).trigger('change');
                                    $('#tdSelect').append(SetNameList);

                                    $('#popup').show();
                                    $('#overlay').show();

                                    $('#insertSet').on('click', function () {
                                        if (NumberOfSet[selectedIndex] != 0) {
                                            $('#popup').hide();
                                            $('#loading').show();
                                            Add_MS_Set(insertSetId, insertbeforeindex, idxOffset, startDate, status);
                                        } else {
                                            $('#overlay, #popup, #loading').hide();
                                            $('#overlay').remove();
                                            $('#popup').remove();
                                            $('#loading').remove();
                                            b.alert( "The Milestone Set you chose has no Templates. Please choose another Milestone Set.");
                                        }
                                    });
                                    $('#cancelSet').on('click', function () {
                                        $('#overlay, #popup, #loading').hide();
                                        $('#overlay').remove();
                                        $('#popup').remove();
                                        $('#loading').remove();
                                    });
                                } else {
                                    b.alert( "There is no Milestone Set.");
                                }

                            } else {
                                b.alert( "There is no Milestone Set.");
                            }
                        }
                    }
                }
                req.send(null);

            }
            var Add_MS_Set = function (idSet, indexBefore, indexoffset, startDate, status) {
                var req = new XMLHttpRequest();
                var URI_ = b.Xrm.Page.context.getClientUrl() + "/XRMServices/2011/OrganizationData.svc/ddsm_milestonesetstepSet?"
                    + "$select="
                    + "ddsm_milestonesetstepId,"
                    + "ddsm_name,"
                    + "ddsm_Category,"
                    + "ddsm_duration,"
                    + "ddsm_index,"
                    + "ddsm_pendingphase,"
                    + "ddsm_resultingphase,"
                    + "ddsm_msspecialnote,"
                    + "ddsm_responsible,"
                    + "ddsm_requiresemptymeasure,"
                    + "ddsm_requiresvalidmeasure,"
                    + "ddsm_approvalthresholdcust,"
                    + "ddsm_approvalthresholdstd,"
                    + "ddsm_approvalthresholdnc,"
                    + "ddsm_approvalthresholdrcx,"
                    + "ddsm_projectphasename,"
                    + "ddsm_projectstatus,"
                    + "ddsm_initialoffered,"
                    + "ddsm_securityincludeonlyguid,"
                    + "ddsm_securityexcludeguid,"
                    + "ddsm_insertset,"
                    + "ddsm_TradeAllyTemplateGUID,"
                    + "ddsm_CustomerTemplateGUID,"
                    + "ddsm_UserTemplateGUID,"
                    + "ddsm_PushNotifications"
                    + "&$filter=ddsm_milestonetemplatesetnameid/Id eq guid'" + idSet + "' and statecode/Value eq 0";

                req.open("GET", URI_, false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");

                req.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        req.onreadystatechange = null;
                        if (this.status == 200) {

                            var responseData = JSON.parse(this.responseText).d;
                            var modeResponse = responseData.results;
                            if (modeResponse.length > 0) {

                                for (var i = 0; i < modeResponse.length; i++) {
                                    var ms_obj = new Object();

                                    ms_obj.ddsm_name = modeResponse[i].ddsm_name;
                                    ms_obj.ddsm_Category = modeResponse[i].ddsm_Category;
                                    ms_obj.ddsm_Index = indexBefore + i;
                                    ms_obj.ddsm_PendingPhase = modeResponse[i].ddsm_pendingphase;
                                    ms_obj.ddsm_ResultingPhase = modeResponse[i].ddsm_resultingphase;
                                    ms_obj.ddsm_MSSpecialNote = modeResponse[i].ddsm_msspecialnote;

                                    ms_obj.ddsm_Responsible = modeResponse[i].ddsm_responsible;
                                    ms_obj.ddsm_Duration = modeResponse[i].ddsm_duration;
                                    ms_obj.ddsm_RequiresEmptyMeasure = modeResponse[i].ddsm_requiresemptymeasure;
                                    ms_obj.ddsm_RequiresValidMeasure = modeResponse[i].ddsm_requiresvalidmeasure;
                                    ms_obj.ddsm_ApprovalThresholdCust = new Object();
                                    ms_obj.ddsm_ApprovalThresholdCust.Value = modeResponse[i].ddsm_approvalthresholdcust.Value;
                                    ms_obj.ddsm_ApprovalThresholdStd = new Object();
                                    ms_obj.ddsm_ApprovalThresholdStd.Value = modeResponse[i].ddsm_approvalthresholdstd.Value;
                                    ms_obj.ddsm_ApprovalThresholdNC = new Object();
                                    ms_obj.ddsm_ApprovalThresholdNC.Value = modeResponse[i].ddsm_approvalthresholdnc.Value;
                                    ms_obj.ddsm_ApprovalThresholdRCx = new Object();
                                    ms_obj.ddsm_ApprovalThresholdRCx.Value = modeResponse[i].ddsm_approvalthresholdrcx.Value;
                                    ms_obj.ddsm_ProjectPhaseName = modeResponse[i].ddsm_projectphasename;
                                    ms_obj.ddsm_ProjectStatus = modeResponse[i].ddsm_projectstatus;
                                    ms_obj.ddsm_InitialOffered = modeResponse[i].ddsm_initialoffered;
                                    ms_obj.ddsm_SecurityIncludeOnlyGUID = modeResponse[i].ddsm_securityincludeonlyguid;
                                    ms_obj.ddsm_SecurityExcludeGUID = modeResponse[i].ddsm_securityexcludeguid;
                                    ms_obj.ddsm_insertset = modeResponse[i].ddsm_insertset;

                                    //ms_obj.ddsm_indexoffset = (indexoffset + modeResponse.length);

                                    ms_obj.ddsm_TradeAllyTemplateGUID = modeResponse[i].ddsm_TradeAllyTemplateGUID;
                                    ms_obj.ddsm_CustomerTemplateGUID = modeResponse[i].ddsm_CustomerTemplateGUID;
                                    ms_obj.ddsm_PushNotifications = modeResponse[i].ddsm_PushNotifications;
                                    ms_obj.ddsm_UserTemplateGUID = modeResponse[i].ddsm_UserTemplateGUID;

                                    ms_obj.ddsm_ProjectToMilestoneId = new Object();
                                    ms_obj.ddsm_ProjectToMilestoneId.Id = b.Xrm.Page.data.entity.getId();
                                    ms_obj.ddsm_ProjectToMilestoneId.Name = b.Xrm.Page.getAttribute("ddsm_name").getValue();
                                    ms_obj.ddsm_ProjectToMilestoneId.LogicalName = "ddsm_project";

                                    ms_obj.ddsm_Status = new Object();
                                    if (i == 0 && status == "962080001") {
                                        ms_obj.ddsm_Status.Value = 962080001;
                                        ms_obj.ddsm_TargetStart = _toUTCDate(startDate);
                                        ms_obj.ddsm_ActualStart = _toUTCDate(startDate);
                                        startDate = new Date((startDate).getTime() + parseInt(ms_obj.ddsm_Duration) * oneday);
                                        ms_obj.ddsm_TargetEnd = _toUTCDate(startDate);
                                    } else {
                                        ms_obj.ddsm_Status.Value = 962080000;
                                        ms_obj.ddsm_TargetStart = _toUTCDate(startDate);
                                        startDate = new Date((startDate).getTime() + parseInt(ms_obj.ddsm_Duration) * oneday);
                                        ms_obj.ddsm_TargetEnd = _toUTCDate(startDate);
                                    }

                                    b.gen_H_createEntitySync(ms_obj, "ddsm_milestoneSet");
                                }
                                $('#overlay, #popup, #loading').hide();
                                $('#overlay').remove();
                                $('#popup').remove();
                                $('#loading').remove();
                                //Ext.getCmp(idGrid).getSelectionModel().clearSelections();
                                Ext.getCmp(idGrid).down('#insertSet').setDisabled(true);

                                changeIndex(modeResponse.length, indexBefore, indexoffset, startDate, idSet);

                            }

                        }
                    }
                }
                req.send(null);
            }

            var changeIndex = function (count, startIndex, indexoffset, startDate, idSet) {
                //console.log("count = " + count);
                //console.log("indexoffset = " + indexoffset);
                /*
                 groupedCategory = Ext.getCmp(idGrid).getStore().isGrouped();
                 if (filteringCategory != '000000000') {
                 Ext.getCmp(idGrid).getStore().clearFilter();
                 }
                 groupingFeature.disable();
                 Ext.getCmp(idGrid).getStore().clearGrouping();
                 */
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var recordsCount = recordsGrid.length;
                if (recordsCount > 0) {
                    for (var i = startIndex - 1; i < recordsCount; i++) {
                        var record = recordsGrid[i];
                        var guidId = record.data[entitySchemaName + 'Id'];
                        var changes = {};
                        if (i == (startIndex - 1)) {
                            changes['ddsm_InsertedSetsID'] = (record.data['ddsm_InsertedSetsID'] !== null) ? record.data['ddsm_InsertedSetsID'] + "|" + idSet : idSet;
                            //changes['ddsm_insertset'] = false;
                            changes['ddsm_Status'] = { Value: '962080000' };
                            changes['ddsm_ActualStart'] = null;
                        }
                        changes['ddsm_Index'] = count + i + 1;
                        changes['ddsm_indexoffset'] = (record.data['ddsm_indexoffset'] != null) ? count + parseInt(record.data['ddsm_indexoffset']) : count;
                        changes['ddsm_TargetStart'] = _toUTCDate(startDate);
                        startDate = new Date((startDate).getTime() + parseInt(record.data['ddsm_Duration']) * oneday);
                        changes['ddsm_TargetEnd'] = _toUTCDate(startDate);
                        var URI = _getODataEndpoint(entitySchemaName) + "(guid'" + guidId + "')";
                        var req = new XMLHttpRequest();
                        req.open("POST", URI, false);
                        req.setRequestHeader("Accept", "application/json");
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader("X-HTTP-Method", "MERGE");
                        req.onreadystatechange = function () {
                            if (this.readyState == 4) {
                                req.onreadystatechange = null;
                                if (this.status == 204) {
                                }
                            }
                        };
                        req.send(JSON.stringify(changes));
                    }
                    loadData(_getODataEndpoint(entitySchemaName), false);
                    /*
                     Ext.getCmp(idGrid).getStore().group('ddsm_Category');
                     if (groupedCategory) {
                     groupingFeature.enable();
                     } else {
                     groupingFeature.disable();
                     }
                     if (filteringCategory != '000000000') {
                     Ext.getCmp(idGrid).getStore().filter('ddsm_Category', filteringCategory, true);
                     }
                     Ext.getCmp(idGrid).getView().refresh();
                     */
                    b.Xrm.Page.getAttribute("ddsm_estimatedprojectcomplete").setValue(_toUTCDate(startDate));
                    updateFncl_initMSindex(count);
                    updateDocConvention(startIndex, count);
                    createDocList_msSet(idSet, startIndex)
                    b.alert("Milestone Set was added successfully!");
                }
            }

            var updateFncl_initMSindex = function (indexoffset) {
                var proj_ID = b.Xrm.Page.data.entity.getId();
                var req = new XMLHttpRequest();
                var serverUrl = b.Xrm.Page.context.getClientUrl();
                req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projectSet?"
                    + "$select="
                    + "ddsm_ddsm_project_ddsm_financial/ddsm_financialId,"
                    + "ddsm_ddsm_project_ddsm_financial/ddsm_initiatingmilestoneindex"
                    + "&$filter=ddsm_projectId eq guid'" + proj_ID + "'"
                    + "&$expand=ddsm_ddsm_project_ddsm_financial", false);

                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");

                req.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        req.onreadystatechange = null;
                        if (this.status == 200) {

                            var responseData = JSON.parse(this.responseText).d;
                            var Fncls = responseData.results[0].ddsm_ddsm_project_ddsm_financial.results;
                            if (Fncls.length > 0) {
                                for (var i = 0; i < Fncls.length; i++) {
                                    if (typeof Fncls[i].ddsm_financialId != 'undefined') {
                                        var fncl_obj = {};
                                        fncl_obj.ddsm_initiatingmilestoneindex = parseInt(Fncls[i].ddsm_initiatingmilestoneindex) + indexoffset;
                                        b.gen_H_updateEntitySync(Fncls[i].ddsm_financialId, fncl_obj, "ddsm_financialSet");
                                    }
                                }
                                b.document.getElementById('WebResource_financial_grid').contentWindow.reloadGrid();
                            }
                        }
                    }
                }
                req.send();
            }

            var updateDocConvention = function (index, indexoffset){
                //console.log("index: " + index + " / indexoffset: " + indexoffset);
                var entity_ID = b.Xrm.Page.data.entity.getId();
                var URI = _getODataEndpoint("ddsm_documentconvention");
                if (entity_ID !== "") {
                    var filter =  "?$filter=ddsm_ProjectId/Id eq guid'" + entity_ID + "'";
                    URI = URI + filter;
                    //console.log(URI);

                    var req = new XMLHttpRequest();
                    req.open("GET", URI, false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.onreadystatechange = function () {
                        if (this.readyState == 4) {
                            req.onreadystatechange = null;
                            if (this.status == 200) {

                                var responseData = JSON.parse(this.responseText).d;
                                var modeResponse = responseData.results;
                                if (modeResponse.length > 0) {
                                    for (var i = 0; i < modeResponse.length; i++) {
                                        if (parseInt(modeResponse[i]["ddsm_RequiredByStatus"]) >= index) {
                                            var obj = {};
                                            obj.ddsm_RequiredByStatus = parseInt(modeResponse[i]["ddsm_RequiredByStatus"]) + indexoffset;
                                            b.gen_H_updateEntitySync(modeResponse[i]["ddsm_documentconventionId"], obj, "ddsm_documentconventionSet");
                                        }
                                    }
                                    //b.document.getElementById('WebResource_docConvention').contentWindow.reloadGrid();
                                }
                            }
                        }
                    }
                    req.send(null);
                }
            }

            function createDocList_msSet(msSet_ID, index){
                var proj_ID = Xrm.Page.context.getQueryStringParameters().id;
                var proj_Name = b.Xrm.Page.getAttribute("ddsm_name").getValue();

                var docProjList = null;
                var _req = new XMLHttpRequest();
                var serverUrl = b.Xrm.Page.context.getClientUrl();
                _req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
                    + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
                    + "&$filter=ddsm_ProjectId/Id eq guid'" + proj_ID + "'", false);
                _req.setRequestHeader("Accept", "application/json");
                _req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                _req.send();
                if (_req.readyState == 4 && _req.status == 200) {
                    var _tplData = JSON.parse(_req.responseText).d;
                    if (_tplData.results[0] != null) {
                        docProjList = _tplData.results;
                    }
                }

                var req = new XMLHttpRequest();
                req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
                    + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
                    + "&$filter=ddsm_MilestoneSet/Id eq guid'" + msSet_ID + "'", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.send();
                if (req.readyState == 4 && req.status == 200) {
                    var _tplData = JSON.parse(req.responseText).d;
                    if (_tplData.results[0] != null) {
                        for (var x = 0; x < _tplData.results.length; x++) {
                            var addDocConv = true;
                            if(docProjList !== null){
                                for(var i =0; i < docProjList.length; i++){
                                    if(_tplData.results[x].ddsm_name == docProjList[i].ddsm_name) {addDocConv = false;break;}
                                }
                            }
                            if(addDocConv){
                                var proj_obj = {};
                                if (_tplData.results[x].ddsm_RequiredByStatus != null) {
                                    proj_obj["ddsm_RequiredByStatus"] = parseInt(_tplData.results[x].ddsm_RequiredByStatus) + index - 1;
                                }
                                if (_tplData.results[x].ddsm_name != null) {
                                    proj_obj["ddsm_name"] = _tplData.results[x].ddsm_name;
                                }
                                if (_tplData.results[x].ddsm_Uploaded != null) {
                                    proj_obj["ddsm_Uploaded"] = _tplData.results[x].ddsm_Uploaded;
                                }

                                proj_obj.ddsm_ProjectId = new Object();
                                proj_obj.ddsm_ProjectId.Id = proj_ID;
                                proj_obj.ddsm_ProjectId.Name = proj_Name;
                                proj_obj.ddsm_ProjectId.LogicalName = "ddsm_project";

                                //console.dir(proj_obj);

                                b.gen_H_createEntitySync(proj_obj, "ddsm_documentconventionSet");
                                b.document.getElementById('WebResource_docConvention').contentWindow.reloadGrid();
                            }
                        }
                    }
                }
            }

            var _createStore = function (data, fields) {
                return Ext.create('Ext.data.Store', {
                    storeId: 'store' + idGrid,
                    model: modelGrid,
                    autoDestroy: true,
                    fields: fields,
                    data: data,
                    autoLoad: false,
                    autoSync: false,
                    sortOnLoad: true,
                    remoteSort: false,
                    remoteFilter: false,
                    simpleSortMode: true,
                    //groupField: 'ddsm_Category',
                    //remoteGroup: true,
                    sorters: sortersGrid,
                    proxy: {
                        type: 'memory',
                        reader: {
                            type: 'json'
                        }
                    }
                });
            };
            var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
                id: 'group',
                ftype: 'groupingsummary',
                //groupHeaderTpl: '{columnName}: {name} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})'
                groupHeaderTpl: ['{columnName}: {name:this.rendererName} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})', { rendererName: function (name) { return ddsm_CategoryColumnRenderer(name); } }]
                //groupHeaderTpl: 'columnName: ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})'
            });
            var filteringCategoryCombo = Ext.create('Ext.form.field.ComboBox', {
                fieldLabel: 'Category&nbsp;Filter',
                queryMode: 'local',
                displayField: 'display',
                valueField: 'value',
                store: ddsm_CategoryComboFilter,
                style: {
                    marginRight: '25px'
                },
                listeners: {
                    select: function (combo, value) {
                        Ext.getCmp(idGrid).getStore().clearFilter();
                        if (combo.getValue() != '000000000') {
                            Ext.getCmp(idGrid).getStore().filter('ddsm_Category', combo.getValue(), true);
                        }
                        filteringCategory = combo.getValue();
                    }
                }
            });
            var groupedCategoryButton = Ext.create('Ext.Button', {
                text: 'Enable Grouping by Category',
                icon: 'accentgold_/EditableGrid/icons/grid/shape_group.png',
                enableToggle: true,
                listeners: {
                    click: function () {
                        if (Ext.getCmp(idGrid).getStore().isGrouped()) {
                            this.setText('Enable Grouping by Category');
                            groupingFeature.disable();
                        } else {
                            this.setText('Disable Grouping by Category');
                            //console.dir(this);
                            groupingFeature.enable();
                        }
                        Ext.getCmp(idGrid).getView().refresh();
                    }
                }
            });
            var addProjectTask = Ext.create('Ext.Button', {
                itemId: 'newtask',
                text: 'Add Project Task',
                icon: 'accentgold_/EditableGrid/icons/grid/addactivity_w.png',
                disabled: true,
                listeners: {
                    click: function () {

                        var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                        var newProjectTask = {};
                        var project = {};
                        project.Id = b.Xrm.Page.data.entity.getId();
                        project.Name = b.Xrm.Page.getAttribute("ddsm_name").getValue();
                        project.LogicalName = b.Xrm.Page.data.entity.getEntityName();
                        newProjectTask.ddsm_Project = project;
                        var milestone = {};
                        milestone.Id = record.get("ddsm_milestoneId");
                        milestone.Name = record.get("ddsm_name");
                        milestone.LogicalName = "ddsm_milestone";
                        newProjectTask.ddsm_Milestone = milestone;
                        newProjectTask.ddsm_MilestoneIndex = parseInt(record.get("ddsm_Index"));
                        newProjectTask.Subject = "Project Task - " + record.get("ddsm_name");

                        var parentAccount = {};
                        parentAccount.Id = (b.Xrm.Page.getAttribute("ddsm_accountid").getValue() != null) ? b.Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id : null;
                        parentAccount.Name = (b.Xrm.Page.getAttribute("ddsm_accountid").getValue() != null) ? b.Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].name : null;
                        parentAccount.LogicalName = "account";

                        if(parentAccount.Id != null){
                            newProjectTask.ddsm_Account = parentAccount;
                        }
                        var newTask = b.gen_H_createEntitySync(newProjectTask, "ddsm_projecttaskSet");

                        if(newTask.ActivityId != "" && newTask.ActivityId != null){
                            var ProjectTaskCount = (record.get("ddsm_ProjectTaskCount") != null && record.get("ddsm_ProjectTaskCount") != "") ? parseInt(record.get("ddsm_ProjectTaskCount")): 0;
                            ProjectTaskCount++;
                            record.set("ddsm_ProjectTaskCount", ProjectTaskCount);
                            record.commit();
                            b.document.getElementById("ProjectTask").control.refresh();
                            b.gen_H_updateEntitySync(record.get("ddsm_milestoneId"), {"ddsm_ProjectTaskCount": ProjectTaskCount}, "ddsm_milestoneSet");
                            var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                            var features = "location=yes,menubar=no,status=yes,toolbar=yes,resizable=yes";
                            var url = "/main.aspx?etn=ddsm_projecttask&extraqs=&histKey=" + randomnumber + "&id=" + newTask.ActivityId + "&newWindow=true&pagetype=entityrecord";
                            window.open(url, "", features, false);
                        }
                    }
                }
            });
            //filteringCategoryCombo.select("000000000");
            var grid = Ext.create('Ext.grid.Panel', {
                id: idGrid,
                border: borderGrid,
                loadMask: true,
                restful: true,
                autoSizeGrid: true,
                features: [_createFilters()],
                columns: [],
                selType: 'checkboxmodel',
                renderTo: "divGrid",
                autoExpandColumn: 'lastUpdated',
                autoScroll: true,
                autoWidth: true,
                height: heightGrid,
                title: titleGrid,
                frame: false,
                plugins: [rowEditing],
                viewConfig: viewConfig,
                tbar: [{
                    itemId: 'saveAll',
                    icon: 'accentgold_/EditableGrid/icons/grid/save.gif',
                    text: 'Save',
                    handler: function () {
                        Save_RecordAll();
                        b.alert( "Your changes saved!");
                    }
                }, '-', {
                    itemId: 'reload',
                    icon: 'accentgold_/EditableGrid/icons/grid/reload.png',
                    text: 'Reload',
                    handler: function () {
                        //b.document.getElementById("ProjectTask").control.refresh();
                        if (!saveGridStatus) {
                            Ext.Msg.confirm('Confirmation', 'You have not saved changes to the grid Milestone! Are you sure you want to reload Milestone data grid?', function (btn, text) {
                                if (btn == 'yes') {
                                    //filteringCategoryCombo.select("000000000");
                                    loadData(_getODataEndpoint(entitySchemaName), false);
                                }
                            });
                        } else
                        //filteringCategoryCombo.select("000000000");
                            loadData(_getODataEndpoint(entitySchemaName), false);
                    }
                }, '-',{
                    itemId: 'excel',
                    text: 'Export to Excel',
                    icon: 'accentgold_/EditableGrid/icons/grid/excel.png',
                    handler: function(b, e) {
                        exportExcelXml();
                    }
                }, '-',
                    addProjectTask,
                    '-', {
                        itemId: 'insertSet',
                        icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAByElEQVQ4T5WTTShEURiG33OVMcntLgwj89O946dQFEqNhLKwkVhYKKWUpUkpi1lYTFFTGkulZGEhiWwoC0U0hYUyMphr/OS/CCPGzBzde+cOxl3MfKtzvvN+z/nOe84h0AjPFkuT0w77C9HSaiYlQH/NDS4wBQv6MLlbgLQBpXwz3kJ34FgzfKer6QOKLPUIvT+CzSmA/2w9PcD4Jkd5UxU+w6/I1htwEvRisOFZ2wP3eq5iGAEIBVT3rIU2fEU/odexCJwfJvyTKKpmqOmRkLE1Ix1oCMiGqSEZt3hmRzQaAcNkoEPY/rc+sWHDcMstIa4VExUsZoTpk8xWupDa+eno967SOJPJQ+BChLP1ipCRZYEKfA4isTe5SOlPrVZAoFIyPgaFjsnFifiAkTaREOdCScIDtZgQCqsQlqcEFEFRpwBk5c8bc3UeE01nh+fKKV/8rFhCgeAph9Eu3x+ta6nC7Gw/uNQEDM1WUr70Tt5dCtFvhLt7P/WnPDhTTW1l5/LRparAkRXjPXupAxzTtbSuLitxrV7vBzy9OykDDI7p2vvkn+fp3eEaGxF6MIDxzSMCIBa/rz/SfAAcgGwAbBLkOD6//p3/BrMkphEjG7gXAAAAAElFTkSuQmCC',
                        text: 'Insert Milestone Above',
                        handler: function () {
                            var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                            //Insert_MS_Set(record.data['ddsm_Index'], record.data['ddsm_indexoffset'], record.data['ddsm_ActualStart'], record.data['ddsm_InsertedSetsID']);
                            Insert_MS_Set(record.data['ddsm_Index'], record.data['ddsm_Status'], record.data['ddsm_indexoffset'], record.data['ddsm_TargetStart'], record.data['ddsm_InsertedSetsID']);
                        },
                        disabled: true
                        //}, '->', filteringCategoryCombo, groupedCategoryButton],
                    }],
                listeners: {
                    selectionchange: function (view, records) {
                        Ext.getCmp(idGrid).down('#newtask').setDisabled(!records.length);
                        var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];

                        if (record != null && record.data['ddsm_insertset'] && record.data['ddsm_Status'] != '962080002' && completedOutofSequence != '962080000') {
                            Ext.getCmp(idGrid).down('#insertSet').setDisabled(false);
                        } else if (record != null && record.data['ddsm_Status'] != '962080002' && completedOutofSequence == '962080002') {
                            Ext.getCmp(idGrid).down('#insertSet').setDisabled(false);
                        } else {
                            Ext.getCmp(idGrid).down('#insertSet').setDisabled(true);
                        }
                    },
                    edit: function (editor, e) {
                        e.record.commit();
                        recordStatus = parseInt(e.record.get('ddsm_Status'));
                        //console.dir(e.record);
                        /*
                         groupedCategory = Ext.getCmp(idGrid).getStore().isGrouped();
                         if (filteringCategory != '000000000') {
                         Ext.getCmp(idGrid).getStore().clearFilter();
                         }
                         groupingFeature.disable();
                         Ext.getCmp(idGrid).getStore().clearGrouping();
                         var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                         recordsGrid[parseInt(e.record.get('ddsm_Index')) - 1].data.ddsm_ActualEnd = e.record.get('ddsm_ActualEnd');
                         recordsGrid[parseInt(e.record.get('ddsm_Index')) - 1].data.ddsm_EstimatedEnd = e.record.get('ddsm_EstimatedEnd');
                         Ext.getCmp(idGrid).getStore().commitChanges();
                         */

                        if (e.record.get('ddsm_EstimatedEnd') != null && e.record.get('ddsm_ActualEnd') == null) {
                            //console.log("EstimateEnd changes");
                            //ddsm_Proj_EstEnd_onChange(e.rowIdx);
                            ddsm_Proj_EstEnd_onChange((parseInt(e.record.get('ddsm_Index')) - 1 >= 0) ? parseInt(e.record.get('ddsm_Index')) - 1 : 0);
                        } else if (e.record.get('ddsm_EstimatedEnd') == null && e.record.get('ddsm_ActualEnd') == null) {
                            //console.log("EstimateEnd && ActualEnd = null");
                            //ddsm_Proj_ActEnd_onChange(e.rowIdx - 1);
                            NextPrevBP_MS_Flag = false;
                            //automationNextStep = 0;
                            ddsm_Proj_ActEnd_onChange((parseInt(e.record.get('ddsm_Index')) - 2 >= 0) ? parseInt(e.record.get('ddsm_Index')) - 2 : 0);
                        } else if (e.record.get('ddsm_ActualEnd') != null) {
                            //console.log("ActualEnd changes");
                            //ddsm_Proj_ActEnd_onChange(e.rowIdx);
                            NextPrevBP_MS_Flag = true;
                            //automationNextStep = 0;
                            ddsm_Proj_ActEnd_onChange((parseInt(e.record.get('ddsm_Index')) - 1 >= 0) ? parseInt(e.record.get('ddsm_Index')) - 1 : 0);
                        }
                        Ext.getCmp(idGrid).getStore().commitChanges();
//                        Save_RecordAll();

                        /*
                         Ext.getCmp(idGrid).getStore().group('ddsm_Category');
                         if (filteringCategory != '000000000') {
                         Ext.getCmp(idGrid).getStore().filter('ddsm_Category', filteringCategory, true);
                         }
                         if (groupedCategory) {
                         groupingFeature.enable();
                         } else {
                         groupingFeature.disable();
                         }

                         Ext.getCmp(idGrid).getView().refresh();
                         */
                        //не корректно работает при групировке и фильтрации. нужно переписать код
                        if (altEnter) {
                            var rowIndex = e.rowIdx;
                            var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                            var recordsCount = recordsGrid.length;
                            while (recordsGrid[rowIndex + 1].get("ddsm_SkipMilestone") == true) {
                                rowIndex++;
                            }
                            rowEditing.startEdit(rowIndex + 1, 7);
                            altEnter = false;
                        }

                    },
                    canceledit: function (editor, e) {
                        lookupActive = "";
                        if (e.record.data[entitySchemaName + 'Id'] == "") { e.grid.getStore().removeAt(e.rowIdx); }
                    },
                    beforeedit: function (editor, e) {
                        //var docUpload = verifyUploadDocs(parseInt(e.record.get('ddsm_Index')));
                        var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                        /*
                         if(!b.AGS.Users.UserRole.isAdmin() && !b.AGS.Users.UserTeam.verify(record.data['ddsm_Responsible'])) {
                         b.alert( "Access denied: not enough rights.");
                         bussinessProcessFlag =  false;
                         return false;
                         }
                         */
//Verify Access
                        var accessOptions = {};
                        accessOptions.targetEntityName = b.Xrm.Page.data.entity.getEntityName();
                        accessOptions.targetEntityId = b.Xrm.Page.data.entity.getId();
                        accessOptions.principalEntityName = "systemuser";
                        accessOptions.principalEntityId = b.Xrm.Page.context.getUserId();
                        var editRetrive = b.XrmServiceToolkit.Soap.RetrievePrincipalAccess(accessOptions);
                        var editNext = (!(editRetrive.indexOf("WriteAccess") + 1))?false:true;
                        if(!editNext) {
                            b.alert( "Access denied: not enough rights.");
                            bussinessProcessFlag =  false;
                            return false;
                        }

                        window.setTimeout(function () {
                            $("#" + idGrid).find("input").keydown(function (event) {
                                if (event.altKey && event.keyCode == 13) {
                                    //console.log("Hey! Alt+ENTER event captured!");
                                    altEnter = true;
                                    event.preventDefault();
                                }
                            });
                        }, 100);

                        window.setTimeout(function () {
                            var ddsm_ActualEndField = editor.getEditor().child('[name="ddsm_ActualEnd"]');
                            ddsm_ActualEndField.focus(true, 200);
                        }, 1);
                        console.log(e.record.get('ddsm_milestoneId'))
                        var msObj = b.AccentGold.REST.retrieveRecord(e.record.get('ddsm_milestoneId'), "ddsm_milestone", "ddsm_ProjectTaskCount", null, null, null, false);
                        console.dir(msObj);
                        var ProjectTaskCount = 0;
                        if(msObj.ddsm_ProjectTaskCount != null) {ProjectTaskCount = parseInt(msObj.ddsm_ProjectTaskCount)}
                        if (ProjectTaskCount != 0) {
                            editor.getEditor().child('[name="ddsm_ActualEnd"]').disable();
                            editor.getEditor().child('[name="ddsm_EstimatedEnd"]').disable();
                            bussinessProcessFlag =  false;

                            b.alert( "Some project task is not complete!");
                            return false;
                        } else if (e.record.get('ddsm_ActualStart') == null) {
                            //change DSM/MPM base logic; True DSM logic
                            if (sequenceRequired) {
                                editor.getEditor().child('[name="ddsm_ActualEnd"]').disable();
                                editor.getEditor().child('[name="ddsm_EstimatedEnd"]').disable();
                            } else {
                                /*
                                 var minDate = new Date(e.record.get('ddsm_TargetStart'));
                                 if(e.record.get('ddsm_ActualStart') !== null) {
                                 minDate = new Date(e.record.get('ddsm_ActualStart'));
                                 }
                                 editor.getEditor().child('[name="ddsm_ActualEnd"]').setMinValue(minDate);
                                 */
                                /*
                                if(!verifyUploadDocs(parseInt(e.record.get('ddsm_Index')))){
                                    editor.getEditor().child('[name="ddsm_ActualEnd"]').disable();
                                    editor.getEditor().child('[name="ddsm_EstimatedEnd"]').enable();
                                    bussinessProcessFlag =  false;
                                    return false;
                                } else {
                                    editor.getEditor().child('[name="ddsm_ActualEnd"]').enable();
                                    editor.getEditor().child('[name="ddsm_EstimatedEnd"]').enable();
                                }
                                */
                            }
                        }
                        else if (e.record.get('ddsm_SkipMilestone') == true) {
                            editor.getEditor().child('[name="ddsm_ActualEnd"]').disable();
                            editor.getEditor().child('[name="ddsm_EstimatedEnd"]').disable();
                        }
                        else {
                            /*
                            if(!verifyUploadDocs(parseInt(e.record.get('ddsm_Index')))){
                                editor.getEditor().child('[name="ddsm_ActualEnd"]').disable();
                                editor.getEditor().child('[name="ddsm_EstimatedEnd"]').enable();
                                bussinessProcessFlag =  false;
                                return false;
                            } else {
                                var minDate = new Date(e.record.get('ddsm_ActualStart'));
                                editor.getEditor().child('[name="ddsm_ActualEnd"]').setMinValue(minDate);
                                editor.getEditor().child('[name="ddsm_ActualEnd"]').enable();
                                editor.getEditor().child('[name="ddsm_EstimatedEnd"]').enable();
                            }
                            */
                        }

                    },
                    afterrender: function() {
                        spinnerForm.stop();
                        loadData(_getODataEndpoint(entitySchemaName), false);
                    }
                }
            });
            new Ext.util.KeyMap(Ext.getBody(),
                [{
                    key: Ext.EventObject.E,
                    ctrl: true,
                    scope: this,
                    defaultEventAction: 'preventDefault',
                    fn: function (e) {
                        //console.log("Hey! Ctrl+E event captured!");
                        var record = Ext.getCmp(idGrid).getSelectionModel().getSelection()[0];
                        if (typeof record !== 'undefined') {
                            rowEditing.startEdit(record.index, 7);
                        } else { b.alert( "No Milestone record has been selected!"); }
                    }
                }, {
                    key: Ext.EventObject.S,
                    ctrl: true,
                    scope: this,
                    defaultEventAction: 'preventDefault',
                    fn: function (e) {
                        //console.log("Hey! Ctrl+S event captured!");
                        Save_RecordAll();
                        b.alert( "Your changes saved!");
                    }
                }]);

            function autoBPStage(){
                console.log("automationNextStep=" + automationNextStep);
                return;
                    if(automationNextStep > 0) {
                                b.Xrm.Page.data.save().then(function () {
                                    b.Xrm.Page.data.process.moveNext(function (d) {
                                        debugger;
                                        if (d == "success"){
                                            automationNextStep--;
                                            setTimeout(function () {autoBPStage();},100);
                                        } else {
                                            console.log(d);
                                    setTimeout(function () {
                                        b.Xrm.Page.data.process.moveNext(function (d) {
                                            debugger;
                                            if (d == "success") {
                                                automationNextStep--;
                                                setTimeout(function () {autoBPStage();},100);
                                            }
                                        });
                                    },250);
                                        }
                                    });
                                }, function (errorCode, message) {
                                    console.log(message);
                                });
                    }
                    if(automationNextStep < 0) {
                        b.Xrm.Page.data.process.movePrevious(function (d) {
                            if (d == "success"){
                                automationNextStep++;
                                setTimeout(function () {autoBPStage();},100);
                            } else {
                                console.log(d);
                                b.Xrm.Page.data.save().then(function () {
                                    debugger;
                                    setTimeout(function () {
                                        b.Xrm.Page.data.process.movePrevious(function (d) {
                                            debugger;
                                            if (d == "success") {
                                                automationNextStep++;
                                                setTimeout(function () {autoBPStage();},100);
                                            }
                                        });
                                    },250);
                                }, function (errorCode, message) {
                                    console.log(message);
                                });
                            }
                        });
                    }
            }

            function getActEndPrevIndex(rowIdx) {
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var dataIndex = recordsGrid[rowIdx].get("ddsm_Index");
                for (var i = 0; i < recordsGrid.length; i++) {
                    if ((dataIndex - 1) == recordsGrid[i].get("ddsm_Index")) {
                        break;
                    }
                }
                return recordsGrid[i].get("ddsm_Index");
            }

            function ddsm_Proj_EstEnd_onChange(rowIdx) {
                //console.log("start ddsm_Proj_EstEnd_onChange");
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                ddsm_Proj_RecalcDates(rowIdx);

                if (recordsGrid[rowIdx].get("ddsm_name") == "Completion Paperwork Received") {
                    var estDate = recordsGrid[rowIdx].get("ddsm_ActualEnd");
                    b.Xrm.Page.getAttribute("ddsm_projectedinstallation").setValue(estDate);
                }

                window.setTimeout(function () {
                    //b.Fncl_UpdateFncls(b.Xrm.Page.data.entity.getId(), ddsm_index_toUpdateFncls);
                }, 500);

            }

            function ddsm_Proj_ActEnd_onChange(rowIdx) {
                debugger;
                //console.log(rowIdx);
                if(rowIdx < 0) {rowIdx = 0;}
                console.log("start ddsm_Proj_ActEnd_onChange");

                var retval = true;
                /*
                retval = verifyUserApproval(rowIdx);
                if (!retval) {
                    bussinessProcessFlag = false;
                    return false;
                }
                */

                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();


                if(!verifyUploadDocs(parseInt(recordsGrid[rowIdx].get("ddsm_Index")))){
                    bussinessProcessFlag =  false;
                    return false;
                }

                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                //console.dir(recordsGrid);
                retval = ddsm_Proj_ValidCheck(rowIdx);
                if (!retval) {
                    bussinessProcessFlag = false;
                    return false;
                }

                var tempObj = {};
                //tempObj = checkApprovalSkipGeneral(rowIdx);
                tempObj.rowIdx = rowIdx;
/*
                if (!tempObj.retval) {
                    bussinessProcessFlag = false;
                    return false;
                }
*/
                retval = ddsm_Proj_RecalcDates(tempObj.rowIdx);
                if (!retval) {
                    bussinessProcessFlag = false;
                    return false;
                }

                //retval = ddsm_Proj_UpdateSummary(CMI, sourceRowId);
                if (!retval) {
                    bussinessProcessFlag = false;
                    return false;
                }

                //retval = ddsm_Proj_ApprovalSecurity(oRows, CMI);
                if (!retval) {
                    bussinessProcessFlag = false;
                    return false;
                }

                if(AutoFncCreationIndex != 0 && AutoFncCreationIndex == parseInt(recordsGrid[rowIdx].get("ddsm_Index"))) {b.document.getElementById('WebResource_measure_grid').contentWindow.AutoMeasureFncl(parseInt(recordsGrid[rowIdx].get("ddsm_Index")));}
/*
                retval = b.ddsm_Proj_FnclKickoff(recordsGrid[tempObj.rowIdx].get("ddsm_Index"), recordsGrid[tempObj.rowIdx].get("ddsm_ActualEnd"));

                if (!retval) {
                    bussinessProcessFlag = false;
                    return false;
                }
*/
                autoBPStage();
                Save_RecordAll();

                b.recalcMsTplAverageDuration(recordsGrid[rowIdx].get("ddsm_MsTpltoMs__Id"));
                //b.ms_AutoEmail(rowIdx);

                //Save_RecordAll();
/*
                window.setTimeout(function () {
                    //b.Fncl_UpdateFncls(b.Xrm.Page.data.entity.getId(), ddsm_index_toUpdateFncls);
                    Proj_FnclKickoff(recordsGrid[tempObj.rowIdx].get("ddsm_Index") + 1, recordsGrid[tempObj.rowIdx].get("ddsm_ActualEnd"),  b.Xrm.Page.data.entity.getId());
                }, 500);
*/
////
                // debugger;
                //var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                /*
                var estProjCompl= recordsGrid[recordsGrid.length - 1].get("ddsm_TargetEnd");
                if(b.Xrm.Page.getAttribute("ddsm_estimatedprojectcomplete"))
                {
                    b.Xrm.Page.getAttribute("ddsm_estimatedprojectcomplete").setValue(estProjCompl);
                }
                */
                //b.proj_calcMeasSumsAsync();
            }

            function getMeasuresTotalSum(){
                var totalField = null;
                var adminReq = "$select=ddsm_ApprovalMeasureAmount1"
                    + "&$filter=ddsm_name eq 'Admin Data' and statecode/Value eq 0";

                var reqAdminData;
                AccentGold.REST.retrieveMultipleRecords("ddsm_admindata", adminReq, null, null, function(data){reqAdminData = data;}, false, null);
                totalField = reqAdminData[0].ddsm_ApprovalMeasureAmount1;
//debugger;
                if(!!totalField) {
//                    var sel_fields = [['ddsm_incentivepaymentnet', 'sum', 'total']];
                    var sel_fields = [[totalField, 'sum', 'total']];
                    var conditions = [['ddsm_projecttomeasureid', 'eq', (b.Xrm.Page.data.entity.getId()).replace(/\{|\}/g, '')],
                        ['ddsm_uptodatephase', 'eq', ((!!b.Xrm.Page.getAttribute("ddsm_phasenumber").getValue())?b.Xrm.Page.getAttribute("ddsm_phasenumber").getValue():1)]];
                    var filter = [conditions];
                    var fetchData = AGS.Fetch.aggregateFXML("ddsm_measure", sel_fields, filter);
                    //console.dir(fetchData.attributes.total.value);
                    return (!!fetchData.attributes.total.value)?fetchData.attributes.total.value:0;
                } else {
                    return 0;
                }
            }

            function verifyUserApproval(rowIdx){
                var measTotal = getMeasuresTotalSum();
                console.log("start verifyUserApproval");
                debugger;
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var apprThresh = parseFloat((!!recordsGrid[rowIdx].data.ddsm_ApprovalThresholdCust)?recordsGrid[rowIdx].data.ddsm_ApprovalThresholdCust:"-1");
                var retval = true, userApprThresh = 0;
                AGS.REST.retrieveMultipleRecords("ddsm_userapproval", "$select=ddsm_ApprovalThresholdCust&$orderby=ddsm_ApprovalThresholdCust desc&$filter=ddsm_User/Id eq guid'" + AGS.Users.getUserId() + "' and statecode/Value eq 0", null, function(msg){console.log(msg);}, function(data){
                    console.dir(data);
                    if(data.length > 0 && !!data[0].ddsm_ApprovalThresholdCust.Value)
                        userApprThresh = parseFloat(data[0].ddsm_ApprovalThresholdCust.Value);
                    else
                        userApprThresh = 0;
                }, false, null);
                //debugger;
                if((apprThresh != -1 || apprThresh != -2) && userApprThresh > measTotal)
                    retval = true;
                else if (apprThresh == -1 || apprThresh == -2)
                    retval = true;
                else
                    retval = false;
                if(!retval) {
                    recordsGrid[rowIdx].set("ddsm_ActualEnd", null);
                    AGS.REST.retrieveMultipleRecords("ddsm_userapproval", "$select=ddsm_ApprovalThresholdCust,ddsm_User&$orderby=ddsm_ApprovalThresholdCust asc&$filter=ddsm_ApprovalThresholdCust/Value ge " + measTotal + " and statecode/Value eq 0", null, function(msg){console.log(msg);}, function(data){
                        console.dir(data);
                        if(data.length > 0){
                            var foundUser = false;
                            for(let i = 0; i < data.length; i++) {
                                let accessOptions = {};
                                accessOptions.targetEntityName = b.Xrm.Page.data.entity.getEntityName();
                                accessOptions.targetEntityId = b.Xrm.Page.data.entity.getId();
                                accessOptions.principalEntityName = "systemuser";
                                accessOptions.principalEntityId = data[i].ddsm_User.Id;
                                let editRetrive = b.XrmServiceToolkit.Soap.RetrievePrincipalAccess(accessOptions);
                                let editNext = (!(editRetrive.indexOf("WriteAccess") + 1))?false:true;
                                if(editNext) {
                                    //create Task for new User
                                    alert("You can not move project forward, because your approval level is not high enough.\nEmail will be created and sent to " + data[i].ddsm_User.Name + " User");

                                    var createEmail = new b.XrmServiceToolkit.Soap.BusinessEntity("email");
                                    var from = [
                                        {
                                            id: (AGS.Users.getUserId()).replace(/\{|\}/g, ''),
                                            logicalName: "systemuser",
                                            type: "EntityReference"
                                        }
                                    ];
                                    createEmail.attributes["from"] = {value: from, type: "EntityCollection"};
                                    var to = [
                                        {
                                            id: (data[i].ddsm_User.Id).replace(/\{|\}/g, ''),
                                            logicalName: data[i].ddsm_User.LogicalName,
                                            type: "EntityReference"
                                        }
                                    ];
                                    createEmail.attributes["to"] = {value: to, type: "EntityCollection"};

                                    var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                                    var url_proj = AGS.getServerUrl() + "/main.aspx?etn=ddsm_project&extraqs=&histKey=" + randomnumber + "&id=" + b.Xrm.Page.data.entity.getId() + "&newWindow=true&pagetype=entityrecord";
                                    createEmail.attributes["subject"] = "Approval levels to Project: " + b.Xrm.Page.getAttribute("ddsm_name").getValue();

                                    var textEmail = ""
                                        + "URL Project: " + "<a href='" + url_proj + "' target='_blank'>" + b.Xrm.Page.getAttribute("ddsm_name").getValue() + "</a>";

//                                    createEmail.attributes["description"] = createEmailBody(null, "Approval levels to Project: " + b.Xrm.Page.getAttribute("ddsm_name").getValue(), AGS.Users.getUserName(), data[i].ddsm_User.Name, textEmail);
                                    createEmail.attributes["description"] = "Approval levels to Project: " + "<a href='" + url_proj + "' target='_blank'>" + b.Xrm.Page.getAttribute("ddsm_name").getValue() + "</a>";

                                    createEmail.attributes["directioncode"] = true;
                                    //console.dir(createEmail);
                                    var emailId = b.XrmServiceToolkit.Soap.Create(createEmail);

                                    debugger;
                                    b.SendEmailRequest(emailId);

                                    foundUser = true;
                                    break;
                                }
                            }
                            if(!foundUser){
                                //Users not found
                                alert("You can not move project forward, because your approval level is not high enough.\nThere is no users in the system that match the requirements. Contact your administrator.");
                            }
                        } else {
                            //Users not found
                            alert("You can not move project forward, because your approval level is not high enough.\nThere is no users in the system that match the requirements. Contact your administrator.");
                        }

                    }, false, null);
                }

                return retval;
            }

            function checkApprovalSkipGeneral(rowIdx) {
                console.log("start checkApprovalSkipGeneral");
                //Only look at Approval Threshold for only Custom
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var recordsCount = recordsGrid.length;

                var proj_ID = b.Xrm.Page.data.entity.getId(),
                    userApprThresh = 0;

                AGS.REST.retrieveMultipleRecords("ddsm_userapproval", "$select=ddsm_ApprovalThresholdCust&$orderby=ddsm_ApprovalThresholdCust desc&$filter=ddsm_User/Id eq guid'" + AGS.Users.getUserId() + "' and statecode/Value eq 0", null, function(msg){console.log(msg);}, function(data){
                    console.dir(data);
                    if(data.length > 0 && !!data[0].ddsm_ApprovalThresholdCust.Value)
                        userApprThresh = parseFloat(data[0].ddsm_ApprovalThresholdCust.Value);
                    else
                        userApprThresh = 0;
                }, false, null);

                var retval = true;
                var measTotal = getMeasuresTotalSum();

                //debugger;

                var apprThresh = 0;
                var prevActEnd;
                var actEnd;
                var index;
                var oldRowIdx = rowIdx;
                var nextRowIdx;
                var nextIndex = -1;

                var runApprSkip = true;
                var keepSkipping = false;

                var newCMI = -1;
                for (var i = rowIdx; i < recordsCount; i++) {
                    debugger;
                    if (recordsGrid[i].data.ddsm_ApprovalThresholdCust != null) {
                        apprThresh = parseFloat(recordsGrid[i].data.ddsm_ApprovalThresholdCust);
                    } else {
                        apprThresh = -1;
                    }

                    console.log("measTotal = " + measTotal + "; apprThresh = " + apprThresh + "; userApprThresh = " + userApprThresh);

                    index = parseInt(recordsGrid[i].data.ddsm_Index);
                    actEnd = recordsGrid[i].data.ddsm_ActualEnd;
                    if (oldRowIdx >= 1) {
                        prevActEnd = recordsGrid[oldRowIdx].data.ddsm_ActualEnd;
                    } else {
                        prevActEnd = null;
                    }

                    oldRowIdx = i;

                    if (recordsGrid[rowIdx].data.ddsm_Index == index) {
                        nextIndex = index;
                        nextRowIdx = i;
                    }

                    if (apprThresh == -1 && !actEnd) {
//                        nextIndex = index;
//                        nextRowIdx = i;
                        keepSkipping = false;
                        //continue;
                        break;
                    }

                    runApprSkip = true;
                    if (apprThresh == -2) {
                        runApprSkip = true;
                    } else if (apprThresh == -1) {
                        runApprSkip = false;
                    } else if (measTotal < apprThresh && measTotal < userApprThresh) {
                        runApprSkip = true;
                    } else if (measTotal < apprThresh && measTotal >= userApprThresh) {
                        runApprSkip = false;
                    } else if (measTotal >= apprThresh && measTotal < userApprThresh) {
                        runApprSkip = true;
                    } else false;

                    if (runApprSkip) {
                        approvalSkip(prevActEnd, actEnd, i);
                        if ((recordsGrid[rowIdx].data.ddsm_Index + 1) == index) {
                            keepSkipping = true;
                        }
                    } else {
                        //approvalUnSkip(i);
                        keepSkipping = false;
                        if(!actEnd)
                            break;
                    }

                    if ((keepSkipping) && (index > recordsGrid[rowIdx].data.ddsm_Index)) {
                        nextIndex = index;
                    }
                }

                var tempObj = new Object();
                tempObj.retval = retval;
                tempObj.rowIdx = nextRowIdx;
                tempObj.nextIndex = nextIndex;
                if (nextIndex != -1) {
                    b.Xrm.Page.getAttribute("ddsm_pendingmilestoneindex").setValue(parseInt(nextIndex));
                }
                return tempObj;
            }
            function approvalSkip(prevActEnd, actEnd, rowIdx) {
                console.log("start approvalSkip");
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                recordsGrid[rowIdx].set("ddsm_SkipMilestone", true);

                if (prevActEnd != null) {
                    recordsGrid[rowIdx].set("ddsm_ActualEnd", prevActEnd);
                    recordsGrid[rowIdx].set("ddsm_ActualStart", prevActEnd);
                    automationNextStep++;
                }
            }
            function approvalUnSkip(rowIdx) {
                console.log("start approvalUnSkip");
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                recordsGrid[rowIdx].set("ddsm_SkipMilestone", false);
                automationNextStep--;
            }

            function ddsm_Proj_ValidCheck(rowIdx) {
                console.log("start ddsm_Proj_ValidCheck");
                //console.log("rowIdx = " + rowIdx);

                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                //console.dir(recordsGrid);
                var today = new Date();

                var ActEndPrev;
                var reqPayeeInfoIndex = b.Xrm.Page.getAttribute("ddsm_requirepayeeinfoindex").getValue();


                if (rowIdx > 0) {
                    ActEndPrev = recordsGrid[rowIdx - 1].get("ddsm_ActualEnd");
                    //ActEndPrev = recordsGrid[getActEndPrevIndex(rowIdx)].get("ddsm_ActualEnd");
                } else {
                    ActEndPrev = null;
                }

                var ActEnd = recordsGrid[rowIdx].get("ddsm_ActualEnd");
                var ActStart = recordsGrid[rowIdx].get("ddsm_ActualStart");
                //console.log("ActStart = " + ActStart);
                //console.log("ActEnd = " + ActEnd);
                //change DSM/MPM base logic; True DSM logic
                if (sequenceRequired) {

                    //Check if ActEnd is empty
                    if (ActEnd == null) {
                        return false;
                    }

                    if (recordsGrid[rowIdx].get("ddsm_UseValidation") != true) {
                        return true;
                    }

                    // Check if ActEnd(CMI) is in future
                    if (ActEnd <= today) {
                    } else {
                        b.alert( "Actual End can not be later than today's date.");
                        recordsGrid[rowIdx].set("ddsm_ActualEnd", null);
                        return false;
                    }

                    // Check if ActEnd(CMI) is null
                    if (ActEnd == "" || ActEnd == null) {
                        return false;
                    }

                    // Check if ActStart(CMI) exists
                    if (ActStart != null && ActStart > 0) {
                    } else {
                        b.alert( "Actual Start must be a valid date.");
                        recordsGrid[rowIdx].set("ddsm_ActualEnd", null);
                        return false;
                    }
                    // Check if ActEnd(CMI-1) exists
                    if (rowIdx > 0) {
                        if (ActEndPrev == null) {
                            b.alert( "The previous line's Actual End must be a valid date.");
                            recordsGrid[rowIdx].set("ddsm_ActualEnd", null);
                            return false;
                        }
                    }

                    // Check if ActEnd is later than ActStart
                    if (ActEnd >= ActStart) {
                    } else {
                        b.alert( "Actual End must be later than Actual Start's date.");
                        recordsGrid[rowIdx].set("ddsm_ActualEnd", null);
                        return false;
                    }
                }

                //Comment 01/04/2016
                /*
                 if (validMeasCheck(rowIdx)) {
                 } else {
                 return false;
                 }
                 */

                if (recordsGrid[rowIdx].get("ddsm_Index") == reqPayeeInfoIndex) {
                    if (!payeeInfoCheck(rowIdx)) {
                        b.alert( "Payee information incomplete. Please fill in missing fields to continue.")
                        return false;
                    }
                }

                return true;
            }
            function payeeInfoCheck(rowIdx) {
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var payeeCompany = b.Xrm.Page.getAttribute("ddsm_payeecompany").getValue();
                var payeeAttnTo = b.Xrm.Page.getAttribute("ddsm_payeeattnto").getValue();
                var payeeAddress1 = b.Xrm.Page.getAttribute("ddsm_payeeaddress1").getValue();
                var payeePhone = b.Xrm.Page.getAttribute("ddsm_payeephone").getValue();
                var payeeCity = b.Xrm.Page.getAttribute("ddsm_payeecity").getValue();
                var payeeState = b.Xrm.Page.getAttribute("ddsm_payeestate").getValue();
                var payeeZip = b.Xrm.Page.getAttribute("ddsm_payeezipcode").getValue();
                var taxEntity = b.Xrm.Page.getAttribute("ddsm_taxentity").getValue();
                var taxID = b.Xrm.Page.getAttribute("ddsm_taxid").getValue();

                if (payeeCompany == null
                    || payeeAttnTo == null
                    || payeeAddress1 == null
                    || payeePhone == null
                    || payeeCity == null
                    || payeeState == null
                    || payeeZip == null
                    || taxEntity == null
                    || taxID == null) {
                    recordsGrid[rowIdx].set("ddsm_ActualEnd", null);
                    return false;
                } else {
                    return true;
                }
            }
            function validMeasCheck(rowIdx) {
                console.log("start validMeasCheck");

                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                //console.log(recordsGrid[rowIdx].get("ddsm_RequiresValidMeasure"));
                if (recordsGrid[rowIdx].get("ddsm_RequiresValidMeasure") == true) {
                    //console.log("ddsm_RequiresValidMeasure");
                    var curSaveKW = b.Xrm.Page.getAttribute("ddsm_currentsavingskw").getValue();
                    var curSavekWh = b.Xrm.Page.getAttribute("ddsm_currentsavingskwh").getValue();
                    var curSavethm = b.Xrm.Page.getAttribute("ddsm_currentsavingsthm").getValue();
                    var curIncTotal = b.Xrm.Page.getAttribute("ddsm_currentincentivetotal").getValue();

                    var curSaveKWBool = (curSaveKW != null && curSaveKW > 0);
                    var curSavekWhBool = (curSavekWh != null && curSavekWh > 0);
                    var curSavethmBool = (curSavethm != null && curSavethm > 0);
                    var curIncTotalBool = (curIncTotal != null && curIncTotal > 0)

                    if (curSaveKWBool || curSavekWhBool || curSavethmBool || curIncTotalBool) {
                        return true;
                    } else {

                        b.proj_calcMeasSums();
                        curSaveKW = b.Xrm.Page.getAttribute("ddsm_currentsavingskw").getValue();
                        curSavekWh = b.Xrm.Page.getAttribute("ddsm_currentsavingskwh").getValue();
                        curSavethm = b.Xrm.Page.getAttribute("ddsm_currentsavingsthm").getValue();
                        curIncTotal = b.Xrm.Page.getAttribute("ddsm_currentincentivetotal").getValue();

                        curSaveKWBool = (curSaveKW != null && curSaveKW > 0);
                        curSavekWhBool = (curSavekWh != null && curSavekWh > 0);
                        curSavethmBool = (curSavethm != null && curSavethm > 0);
                        curIncTotalBool = (curIncTotal != null && curIncTotal > 0)

//                        if (curSaveKWBool || curSavekWhBool || curSavethmBool || curIncTotalBool) {
                        if (curIncTotalBool) {
                            return true;
                        } else {
                            b.alert( "This project contains no valid measures.");
                            recordsGrid[rowIdx].set("ddsm_ActualEnd", null);
                            return false;
                        }
                    }

                } else if (recordsGrid[rowIdx].get("ddsm_RequiresEmptyMeasure") == true) {
                    //console.log("ddsm_RequiresEmptyMeasure");
                    var curSaveKW = b.Xrm.Page.getAttribute("ddsm_currentsavingskw").getValue();
                    var curSavekWh = b.Xrm.Page.getAttribute("ddsm_currentsavingskwh").getValue();
                    var curSavethm = b.Xrm.Page.getAttribute("ddsm_currentsavingsthm").getValue();
                    var curIncTotal = b.Xrm.Page.getAttribute("ddsm_currentincentivetotal").getValue();

                    var curSaveKWBool = (curSaveKW != null);
                    var curSavekWhBool = (curSavekWh != null);
                    var curSavethmBool = (curSavethm != null);
                    var curIncTotalBool = (curIncTotal != null)

                    if (curSaveKWBool && curSavekWhBool && curSavethmBool && curIncTotalBool) {
                        return true;
                    } else {
                        var emptyMeas = b.proj_calcMeasSums_EmptyMeasCheck();

                        if (emptyMeas == true) {
                            return true;
                        } else {
                            b.alert( "This project requires at least one measure.");
                            recordsGrid[rowIdx].set("ddsm_ActualEnd", null);
                            return false;
                        }
                    }
                }

                return true;
            }
            // Recalculation of the milestone data grid
            function ddsm_Proj_RecalcDates(rowIdx) {
                console.log("start ddsm_Proj_RecalcDates");
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var recordsCount = recordsGrid.length;
                if (startDate() == null) return false;
                //recordsGrid[0].set("ddsm_ActualStart", new Date(startDate()));
                //recordsGrid[0].set("ddsm_TargetStart", recordsGrid[0].get("ddsm_ActualStart");
                recordsGrid[0].set("ddsm_TargetEnd", new Date((recordsGrid[0].get("ddsm_TargetStart")).getTime() + parseInt(recordsGrid[0].get("ddsm_Duration")) * oneday));
                if (recordsGrid[0].get("ddsm_ActualStart") != null && recordsGrid[0].get("ddsm_ActualEnd") == null) { recordsGrid[0].set("ddsm_Status", "962080001"); }
                var forecastcompletelastrecalcindex = parseInt(b.Xrm.Page.getAttribute("ddsm_forecastcompletelastrecalcindex").getValue());
                var ForecastComplete = new Date();

                var dtPrevActEnd;
                var dtPrevEstEnd;
                var dtPrevTargEnd;
                var dtPrevBest;
                var dtTargEnd;
                var intDur;

                var upToDateRowIdx = 0;
                ddsm_index_toUpdateFncls = null;
                var maxRow;

                var currStatus = parseInt(recordsGrid[rowIdx].get("ddsm_Status"));

                var oRowPrevIdx = rowIdx - 1;
                var oRowNextIdx = rowIdx + 1;
                var currentActEnd = recordsGrid[rowIdx].get("ddsm_ActualEnd");
                var prevActEnd = null;
                if (oRowPrevIdx >= 0) {
                    if (recordsGrid[oRowPrevIdx].get("ddsm_ActualEnd") != null) {
                        prevActEnd = new Date(recordsGrid[oRowPrevIdx].get("ddsm_ActualEnd"));
                    }
                }

                //change DSM/MPM base logic; True DSM logic
                if (!sequenceRequired && recordsGrid[rowIdx].get("ddsm_ActualEnd") != null) {
                    recordsGrid[rowIdx].set("ddsm_Duration", 0);
                    recordsGrid[rowIdx].set("ddsm_TargetEnd", recordsGrid[rowIdx].get("ddsm_TargetStart"));
                }


                //change DSM/MPM base logic; True DSM logic
                if (!sequenceRequired) {
                    for (var i = 0; i < recordsCount; i++) {
                        if (recordsGrid[i].get("ddsm_Status") == "962080001" && recordsGrid[i].get("ddsm_ActualEnd") == null && i != (rowIdx + 1)) {
                            recordsGrid[i].set("ddsm_Status", "962080000");
                            activeRowIdx = i;
                        }
                    }
                }

                if (currentActEnd != null) {
                    recordsGrid[rowIdx].set("ddsm_Status", "962080002");
                    var actualDuration = parseInt(((recordsGrid[rowIdx].get("ddsm_ActualEnd")).getTime() - (recordsGrid[rowIdx].get("ddsm_ActualStart")).getTime()) / oneday);
                    recordsGrid[rowIdx].set("ddsm_ActualDuration", actualDuration);
                } else {
                    recordsGrid[rowIdx].set("ddsm_Status","962080001");
                }

                var mpmStep = 1;

                for (var i = 1; i < recordsCount; i++) {

                    var skipCheckStatus = false;

                    dtPrevActEnd = recordsGrid[i - 1].get("ddsm_ActualEnd");
                    dtPrevEstEnd = recordsGrid[i - 1].get("ddsm_EstimatedEnd");
                    dtPrevTargEnd = recordsGrid[i - 1].get("ddsm_TargetEnd");

                    //change DSM/MPM base logic; True DSM logic
                    if (sequenceRequired) {
                        if (dtPrevActEnd != null) {
                            dtPrevBest = dtPrevActEnd;
                        } else if (dtPrevEstEnd != null) {
                            dtPrevBest = dtPrevEstEnd;
                        } else if (dtPrevTargEnd != null) {
                            dtPrevBest = dtPrevTargEnd;
                        } else {
                            b.alert( "Error: ddsm_Proj_recalcDates Target End Date is missing");
                            return false;
                        }
                    } else {
                        if (dtPrevTargEnd != null) {
                            dtPrevBest = dtPrevTargEnd;
                        } else {
                            b.alert( "Error: ddsm_Proj_recalcDates Target End Date is missing");
                            return false;
                        }
                    }

                    // Copy Best Previous to Target Start
                    recordsGrid[i].set("ddsm_TargetStart", dtPrevBest);

                    // Copy Previous Actual End to Actual Start
                    if (dtPrevActEnd != null) {
                        recordsGrid[i].set("ddsm_ActualStart", dtPrevActEnd);
                    } else {
                        recordsGrid[i].set("ddsm_ActualStart", null);
                        recordsGrid[i].set("ddsm_ActualEnd", null);
                    }

                    intDur = parseInt(recordsGrid[i].get("ddsm_Duration"));
                    dtTargEnd = new Date(dtPrevBest.getTime() + (intDur * oneday));
                    recordsGrid[i].set("ddsm_TargetEnd", dtTargEnd);

                    oRowPrevIdx = i - 1;
                    oRowNextIdx = i + 1;

                    if (recordsGrid[i].get("ddsm_SkipMilestone") != null) {
                        skipCheckStatus = recordsGrid[i].get("ddsm_SkipMilestone");
                        if (skipCheckStatus == true) {
                            recordsGrid[i].set("ddsm_ActualEnd", recordsGrid[i].get("ddsm_ActualStart"));
                            recordsGrid[i].set("ddsm_Status", "962080003");
                        }
                    }
                    currentActEnd = recordsGrid[i].get("ddsm_ActualEnd");

                    //console.log("1. recordsGrid[" + i + "].get('ddsm_Status') = " + recordsGrid[i].get("ddsm_Status"));

                    if (recordsGrid[i - 1].get("ddsm_ActualEnd") != null) {
                        prevActEnd = recordsGrid[i - 1].get("ddsm_ActualEnd");
                    } else {
                        prevActEnd = null;
                    }

                    if (currentActEnd != null) {
                        //change DSM/MPM base logic; True DSM logic
                        if (!sequenceRequired && i == (rowIdx + mpmStep)) { mpmStep += 1; }
                        if(!skipCheckStatus){
                            recordsGrid[i].set("ddsm_Status","962080002");
                            var actualDuration = parseInt(((recordsGrid[i].get("ddsm_ActualEnd")).getTime() - (recordsGrid[i].get("ddsm_ActualStart")).getTime()) / oneday);
                            recordsGrid[i].set("ddsm_ActualDuration", actualDuration);
                            upToDateRowIdx = i;
                            //console.log("comlete milestone " + i);
                        }
                    } else if (currentActEnd == null && prevActEnd != null) {

                        //change DSM/MPM base logic; True DSM logic
                        if (!sequenceRequired) {
                            if (i == (rowIdx + mpmStep)) {
                                recordsGrid[i].set("ddsm_Status", "962080001");
                                var PMI = parseInt(recordsGrid[i].get("ddsm_Index"));
                                b.Xrm.Page.getAttribute("ddsm_pendingmilestoneindex").setValue(PMI);
                                upToDateRowIdx = i;
                                ddsm_index_toUpdateFncls = parseInt(recordsGrid[i].get("ddsm_Index"));
                            }
                        } else {
                            recordsGrid[i].set("ddsm_Status", "962080001");
                            var PMI = parseInt(recordsGrid[i].get("ddsm_Index"));
                            b.Xrm.Page.getAttribute("ddsm_pendingmilestoneindex").setValue(PMI);
                            upToDateRowIdx = i;
                            ddsm_index_toUpdateFncls = parseInt(recordsGrid[i].get("ddsm_Index"));
                        }

                    } else {
                        recordsGrid[i].set("ddsm_Status", "962080000");
                    }
                    //maxRowIdx = i;
                    //console.log("2. recordsGrid[" + i + "].get('ddsm_Status') = " + recordsGrid[i].get("ddsm_Status"));

                    updateInitialDates(i);


                    //Forecast Complete: Last Recalc Index
                    if(forecastcompletelastrecalcindex != -1 && forecastcompletelastrecalcindex != 0 && forecastcompletelastrecalcindex == i)
                    {
                        if(parseInt(recordsGrid[i - 1].get("ddsm_Status")) == 962080002)
                            ForecastComplete = recordsGrid[i - 1].get("ddsm_ActualEnd");
                        else
                            ForecastComplete = recordsGrid[i - 1].get("ddsm_TargetEnd");
                    }

                }
                currentActEnd = recordsGrid[rowIdx].get("ddsm_ActualEnd");
                if (currentActEnd != null) {
                    //ddsm_Proj_CopyPhaseCheck(rowIdx);
                }
                console.log(">>>>>>>>>>>");
                debugger;
                if(forecastcompletelastrecalcindex != -1 && forecastcompletelastrecalcindex != 0)
                    b.Xrm.Page.getAttribute("ddsm_estimatedprojectcomplete").setValue(ForecastComplete);
                else
                    b.Xrm.Page.getAttribute("ddsm_estimatedprojectcomplete").setValue(dtTargEnd);

                var retval = false;
                retval = ddsm_Proj_UpdateSummary(upToDateRowIdx, recordsCount - 1);
                if (!retval) {
                    return false;
                }
                saveGridStatus = false;
                return true;
            }
            function ddsm_Proj_UpdateSummary(rowIdx, maxRow) {
                console.log("start ddsm_Proj_UpdateSummary");
                //console.log(rowIdx);
                //console.log(maxRow);
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var recordsCount = recordsGrid.length;

                var PMI = b.Xrm.Page.getAttribute("ddsm_pendingmilestoneindex").getValue();
                var msComplete = b.Xrm.Page.getAttribute("ddsm_completeonlastmilestone").getValue();

                if (rowIdx == maxRow) {
                    var ActEnd = recordsGrid[maxRow].get("ddsm_ActualEnd");
                    console.log("rowIdx[" + (rowIdx - 1) +"] ActualEnd = " + recordsGrid[rowIdx - 1].get("ddsm_ActualEnd"));
                    console.log("rowIdx[" + rowIdx +"] ActualEnd = " + recordsGrid[rowIdx].get("ddsm_ActualEnd"));

                    b.setDisabledProjFields(false);

                    b.Xrm.Page.getAttribute("ddsm_pendingmilestone").setValue("");
                    if (msComplete) {
                        b.Xrm.Page.getAttribute("ddsm_projectstatus").setValue(recordsGrid[maxRow].get("ddsm_ProjectStatus"));
                        b.Xrm.Page.getAttribute("ddsm_phase").setValue(recordsGrid[maxRow].get("ddsm_ProjectPhaseName"));
                        b.Xrm.Page.getAttribute("ddsm_enddate").setValue(recordsGrid[maxRow - 1].get("ddsm_ActualEnd"));
                    } else {
                        b.Xrm.Page.getAttribute("ddsm_phase").setValue(recordsGrid[maxRow].get("ddsm_ProjectPhaseName"));
                        b.Xrm.Page.getAttribute("ddsm_projectstatus").setValue(recordsGrid[maxRow].get("ddsm_ProjectStatus"));
                        b.Xrm.Page.getAttribute("ddsm_enddate").setValue(recordsGrid[maxRow - 1].get("ddsm_ActualEnd"));
                    }
                    b.Xrm.Page.getAttribute("ddsm_pendingactualstart").setValue(null);
                    b.Xrm.Page.getAttribute("ddsm_pendingtargetend").setValue(null);
                    b.Xrm.Page.getAttribute("ddsm_pendingmilestoneindex").setValue(null);
                    b.Xrm.Page.getAttribute("ddsm_responsible").setValue("");
                    //b.Xrm.Page.getAttribute("ddsm_completedstatus").setValue("");
                    b.Xrm.Page.getAttribute("ddsm_completedmilestone").setValue("");

                    //b.setDisabledProjFields(true);

                    return true;
                }

                var sourceRowNextIdx = rowIdx + 1;
                var sourceRowPrevIdx = rowIdx - 1;

                b.setDisabledProjFields(false);

                var pendMstName = recordsGrid[rowIdx].get("ddsm_name");
                var statusNext = recordsGrid[rowIdx].get("ddsm_ProjectStatus");
                var phaseNext = recordsGrid[rowIdx].get("ddsm_ProjectPhaseName");
                var targEndNext = recordsGrid[rowIdx].get("ddsm_TargetEnd");
                var responsible = recordsGrid[rowIdx].get("ddsm_Responsible");
                if(sourceRowPrevIdx >= 0){
                    var ActEnd = recordsGrid[sourceRowPrevIdx].get("ddsm_ActualEnd");
                    var completedStatus = recordsGrid[sourceRowPrevIdx].get("ddsm_ProjectStatus");
                    var completedMS = recordsGrid[sourceRowPrevIdx].get("ddsm_name");
                } else {
                    var ActEnd = recordsGrid[0].get("ddsm_TargetStart");
                }

                b.Xrm.Page.getAttribute("ddsm_pendingmilestone").setValue(pendMstName);
                b.Xrm.Page.getAttribute("ddsm_projectstatus").setValue(statusNext);
                b.Xrm.Page.getAttribute("ddsm_phase").setValue(phaseNext);
                b.Xrm.Page.getAttribute("ddsm_pendingactualstart").setValue(ActEnd);
                b.Xrm.Page.getAttribute("ddsm_pendingtargetend").setValue(targEndNext);
                b.Xrm.Page.getAttribute("ddsm_responsible").setValue(responsible);
                //b.Xrm.Page.getAttribute("ddsm_completedstatus").setValue(completedStatus);
                b.Xrm.Page.getAttribute("ddsm_completedmilestone").setValue(completedMS);
                //Update project status Measure
                //b.document.getElementById('WebResource_measure_grid').contentWindow.updateProjectStatus();

                if(sourceRowPrevIdx >= 0) {
                    var initialOffered = recordsGrid[sourceRowPrevIdx].get("ddsm_InitialOffered");
                    if (initialOffered == true) {
                        //b.Xrm.Page.getAttribute("ddsm_initialoffered").setValue(ActEnd);
                    }
                }

                //b.setDisabledProjFields(true);

                return true;
            }

            function updateInitialDates(rowIdx) {
                console.log("start updateInitialDates");
                //Includes code to update Projected Installation Date and Completed Milestones fields.
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var recordsCount = recordsGrid.length;

                var date = recordsGrid[rowIdx].get("ddsm_ActualEnd");
                var phaseCurrent = recordsGrid[rowIdx].get("ddsm_ProjectPhaseName");
                var phaseNext;

                var RowNext;
                if ((rowIdx + 1) < recordsCount) {
                    RowNext = rowIdx + 1;
                } else {
                    RowNext = null;
                }
                var RowPrev;
                if (rowIdx > 0) {
                    RowPrev = rowIdx - 1;
                } else {
                    RowPrev = null;
                }



                //Sets Initial Commitment and Installation
                if (RowNext != null) {
                    if (recordsGrid[RowNext].get("ddsm_ProjectPhaseName") == null) {
                        return;
                    } else {
                        phaseNext = recordsGrid[RowNext].get("ddsm_ProjectPhaseName");
                    }
                    /*
                     if ((phaseCurrent == "1. Pre-Commit" || phaseCurrent == "1. Offered") && phaseNext == "3. Installed") {
                     b.Xrm.Page.getAttribute("ddsm_initialinstallation").setValue(date);
                     } else if (phaseCurrent == "2. Committed" && phaseNext == "3. Installed") {
                     b.Xrm.Page.getAttribute("ddsm_initialinstallation").setValue(date);
                     }
                     */
                    //Sets Projected Installation
                    for (var i = 0; i < recordsCount; i++) {
                        var actEnd = null;
                        var estEnd = null;
                        var targEnd = null;
                        var instPhasePrev = null;
                        var instPhaseNext = null;
                        var instPhaseCurrent = recordsGrid[i].get("ddsm_ProjectPhaseName");
                        if (recordsGrid[i + 1] != null && recordsGrid[i + 1].get("ddsm_ProjectPhaseName") != null) {
                            instPhaseNext = recordsGrid[i + 1].get("ddsm_ProjectPhaseName");
                        }
                        if (i - 1 >= 0) {
                            if (recordsGrid[i - 1].get("ddsm_ProjectPhaseName") != null) {
                                instPhasePrev = recordsGrid[i - 1].get("ddsm_ProjectPhaseName");
                            }
                        }
                        if (recordsGrid[i].get("ddsm_ActualEnd") != null && typeof recordsGrid[i].get("ddsm_ActualEnd") != 'undefined') {
                            actEnd = recordsGrid[i].get("ddsm_ActualEnd");
                        }
                        if (recordsGrid[i].get("ddsm_EstimatedEnd") != null && typeof recordsGrid[i].get("ddsm_EstimatedEnd") != 'undefined') {
                            estEnd = recordsGrid[i].get("ddsm_EstimatedEnd");
                        }
                        if (recordsGrid[i].get("ddsm_TargetEnd") != null && typeof recordsGrid[i].get("ddsm_TargetEnd") != 'undefined') {
                            targEnd = recordsGrid[i].get("ddsm_TargetEnd");
                        }

                        if (instPhaseCurrent == "3. Installed" && ((instPhasePrev == "1. Proposed" || instPhasePrev == "1. Offered") || instPhasePrev == "2. Committed")) {
                            if (actEnd != null) {
                                b.Xrm.Page.getAttribute("ddsm_projectedinstallation").setValue(actEnd);
                                break;
                            } else if (estEnd != null) {
                                b.Xrm.Page.getAttribute("ddsm_projectedinstallation").setValue(estEnd);
                                break;
                            } else if (targEnd != null) {
                                b.Xrm.Page.getAttribute("ddsm_projectedinstallation").setValue(targEnd);
                                //console.log(targEnd);
                                break;
                            }
                            break;
                        }

                    }

                }
            }
            function ddsm_Proj_CopyPhaseCheck(rowIdx) {
                console.log("start ddsm_Proj_CopyPhaseCheck");

                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var recordsCount = recordsGrid.length;
                var phaseCurrent = recordsGrid[rowIdx].get("ddsm_ProjectPhaseName");
                //var upToDatePhase = b.Xrm.Page.getAttribute("ddsm_uptodatephase").getValue();

                //console.log(recordsGrid[rowIdx + 1].get("ddsm_ProjectPhaseName"));

                //change DSM/MPM base logic; True DSM logic
                if (!sequenceRequired) {
                    if (rowIdx != activeRowIdx) {
                        //console.log(">> MPM Logic Start >>");
                        if (activeRowIdx - 1 >= 0) {
                            var phasePrev = recordsGrid[activeRowIdx - 1].get("ddsm_ProjectPhaseName");
                            /*
                             if (((phasePrev == "1. Pre-Commit" || phasePrev == "1. Offered") || phasePrev == "2. Committed") && phaseCurrent == "3. Installed" && upToDatePhase == null) {
                             b.ddsm_Proj_CopyPhaseSkip2();
                             }
                             */
                        }
                    }
                }
                if (rowIdx + 1 < recordsCount) {
                    if (recordsGrid[rowIdx + 1].get("ddsm_ProjectPhaseName") != null) {
                        var phaseNext = recordsGrid[rowIdx + 1].get("ddsm_ProjectPhaseName");
                        if(phaseCurrent != phaseNext) {
                            if(!!b.Xrm.Page.getAttribute("ddsm_phasenumber"))
                            {
                                var phasenumber =parseInt(b.Xrm.Page.getAttribute("ddsm_phasenumber").getValue());
                                phasenumber+=1;
                                b.Xrm.Page.getAttribute("ddsm_phasenumber").setValue(phasenumber);
                            }

                            b.ddsm_Proj_CopyPhase(phaseNext, recordsGrid[rowIdx + 1].get("ddsm_ActualStart"), recordsGrid[rowIdx + 1].get("ddsm_ProjectStatus"));
                        }
                        /*
                         if ((phaseCurrent == "1. Pre-Commit" || phaseCurrent == "1. Offered") && phaseNext == "3. Installed" && upToDatePhase == null) {
                         b.ddsm_Proj_CopyPhaseSkip2();
                         } else if ((phaseCurrent == "1. Pre-Commit" || phaseCurrent == "1. Offered") && phaseNext == "2. Committed" && upToDatePhase == null) {
                         b.ddsm_Proj_CopyPhase1();
                         } else if (phaseCurrent == "2. Committed" && phaseNext == "3. Installed" && upToDatePhase == 2) {
                         b.ddsm_Proj_CopyPhase2();
                         }
                         */
                    }
                }
            }

            //add 12/24/2014 check the status of the document is uploaded into the project
            function verifyUploadDocs(Index){
                console.log("start verifyUploadDocs");

                var proj_ID = b.Xrm.Page.data.entity.getId();
                var URL = _getODataEndpoint('ddsm_documentconvention');
                var select = '?$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded';
                var filter = "&$filter=ddsm_ProjectId/Id eq guid'" + proj_ID + "' and ddsm_RequiredByStatus eq " + Index;
                var docNames = "";
                var req = new XMLHttpRequest();
                req.open("GET", URL + select + filter, false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.send();
                if (req.readyState == 4 && req.status == 200) {
                    var _tplData = JSON.parse(req.responseText).d;
                    debugger;
                    if (_tplData.results[0] != null) {
                        for (var x = 0; x < _tplData.results.length; x++) {
                            if(!_tplData.results[x].ddsm_Uploaded) {docNames += _tplData.results[x].ddsm_name +"\n";}
                        }
                        if(docNames == "") {return true;}else{
                            bussinessProcessFlag =  false;
                            //window.parent.document.getElementById("WebResource_headerQickNav").contentWindow.Xrm.QuickNavigation.ClickDocsTab();
                            alert("The following documents must be attached to this Project before completing this Milestone:\n" + docNames);
                            return false; }
                    } else {return true;}
                }

            }

            function ComplLastRecord(){
                console.log("Start ComplLastRecord function");
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var projStartDate = recordsGrid[0].get("ddsm_TargetStart");
                var currIdx = recordsGrid.length - 1;
                recordsGrid[currIdx].set("ddsm_ActualEnd", projStartDate);
                recordsGrid[currIdx].set("ddsm_ActualStart", projStartDate);
                recordsGrid[currIdx].set("ddsm_Status","962080002");
                Ext.getCmp(idGrid).getStore().commitChanges();
                ddsm_Proj_ActEnd_onChange(currIdx);
            }


            function calcFinancial(proj_ID, fncl_ID, PhaseNumber, ActualEnd, CalcType) {
                //debugger;
                let thisFinancialData = {};
                let customMap = AGS.Entity.getTargetMapping(962080003, 'ddsm_financial', ['ddsm_project']);
                if (customMap.hasOwnProperty('ddsm_project') && !!Object.keys(customMap['ddsm_project'])) {
                    AGS.REST.retrieveRecord(proj_ID, "ddsm_project", Object.keys(customMap['ddsm_project']).join(','), null, function (data) {
                        //console.dir(data);
                        let projectMap = customMap['ddsm_project'];
                        for (let k in projectMap) {
                            if (!!data[k]) {
                                thisFinancialData[projectMap[k]] = data[k];
                            }
                        }
                    }, null, false);
                };
                //debugger;
                if(CalcType == 962080009) {
                    let fetch = "";
                    fetch += '<fetch aggregate="true" >';
                    fetch += '<entity name="ddsm_measure" >';
                    fetch += '<attribute name="ddsm_incentivepaymentnet" alias="incPaymentNet" aggregate="sum" />';
                    fetch += '<attribute name="ddsm_incentivepaymentnetdsm" alias="incPaymentNetDSM" aggregate="sum" />';
                    fetch += '<filter type="and" >';
                    fetch += '<condition attribute="ddsm_projecttomeasureid" operator="eq" value="' + proj_ID + '" />';
                    fetch += '<condition attribute="ddsm_uptodatephase" operator="eq" value="' + PhaseNumber + '" />';
                    fetch += '</filter>';
                    fetch += '</entity>';
                    fetch += '</fetch>';

                    var fetchData = XrmServiceToolkit.Soap.Fetch(fetch);
                    if (fetchData != null) {

                        if (!!fetchData[0].attributes.incPaymentNet.value) {
                            thisFinancialData["ddsm_actualamount"] = {};
                            thisFinancialData["ddsm_actualamount"].Value = parseFloat(fetchData[0].attributes.incPaymentNet.value).toFixed(6);
                            thisFinancialData["ddsm_calculatedamount"] = {};
                            thisFinancialData["ddsm_calculatedamount"].Value = parseFloat(fetchData[0].attributes.incPaymentNet.value).toFixed(6);
                        }
                        if (!!fetchData[0].attributes.incPaymentNetDSM.value) {
                            thisFinancialData["ddsm_IncentivesCustomerRebatesDSMAmount"] = {};
                            thisFinancialData["ddsm_IncentivesCustomerRebatesDSMAmount"].Value = parseFloat(fetchData[0].attributes.incPaymentNetDSM.value).toFixed(6);
                            thisFinancialData["ddsm_CC1_Amount"] = {};
                            thisFinancialData["ddsm_CC1_Amount"].Value = parseFloat(fetchData[0].attributes.incPaymentNetDSM.value).toFixed(6);
                        }

                    }

                    thisFinancialData["ddsm_IncentivesCustomerRebatesDSMProgram"] = {};
                    thisFinancialData["ddsm_IncentivesCustomerRebatesDSMProgram"].Value = 962080006;
                    thisFinancialData["ddsm_CC1_CostDate"] = ActualEnd;
                    thisFinancialData["ddsm_CC1_ProgramCode"] = {};
                    thisFinancialData["ddsm_CC1_ProgramCode"].Value = 962080006;
                    thisFinancialData["ddsm_CC1_ProgramCodeDescription"] = {};
                    thisFinancialData["ddsm_CC1_ProgramCodeDescription"].Value = 962080006;
                }
                //debugger;

                AGS.REST.updateRecord(fncl_ID, thisFinancialData, "ddsm_financial", null, null, false);
            }
            ///
/*
            recalcActualEnd = function(){
                //console.log("recalcActualEnd");
                var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
                var projStartDate = recordsGrid[0].get("ddsm_TargetStart");
                var ddsm_phase = b.Xrm.Page.getAttribute("ddsm_phase").getValue();
                var ddsm_projectstatus = b.Xrm.Page.getAttribute("ddsm_projectstatus").getValue();
                var phaseIdx = -1, projstatusIdx = -1, currIdx = -1;

                for(var i = 0; i < recordsGrid.length;i++){
                    if(recordsGrid[i].get("ddsm_ProjectPhaseName") == ddsm_phase) {phaseIdx = i; break;}
                }
                if(ddsm_projectstatus == "7-Completed"){phaseIdx = recordsGrid.length;}
                if(phaseIdx != -1){
                    for(var i = phaseIdx; i < recordsGrid.length;i++){
                        if(recordsGrid[i].get("ddsm_ProjectPhaseName") == ddsm_phase && recordsGrid[i].get("ddsm_ProjectStatus") == ddsm_projectstatus) {projstatusIdx = i; break;}
                    }
                    if(projstatusIdx != -1){currIdx = projstatusIdx;}else{currIdx = phaseIdx;}
                    if(ddsm_projectstatus == "7-Completed"){currIdx = currIdx - 1;}

                    if(currIdx - 1 >= 0){
                        for(var i = 0;i < currIdx; i++){
                            recordsGrid[i].set("ddsm_ActualEnd", projStartDate);
                            recordsGrid[i].set("ddsm_ActualStart", projStartDate);
                            recordsGrid[i].set("ddsm_Status","962080002");
                        }
                        recordsGrid[currIdx].set("ddsm_ActualStart", projStartDate);
                        recordsGrid[currIdx].set("ddsm_Status", "962080001");

                        Ext.getCmp(idGrid).getStore().commitChanges();
                        ddsm_Proj_ActEnd_onChange(currIdx - 1);

                        if(ddsm_projectstatus == "7-Completed"){
                            setTimeout(function () {
                                b.document.getElementById('WebResource_financial_grid').contentWindow.proj7Completed();

                                setTimeout(function () {
                                    ComplLastRecord();
                                }, 250);
                            }, 500);
                        }

                        //Save_RecordAll();
                    }
                }
                loading3GridInterval = null;
            }
*/
            validLoading3Grid = function(){
                if(b.firstLoadGrid >= 3){
                    loading3GridInterval = null;
                    //b.PreCommitCalcOnLoad();
                } else {
                    setTimeout(validLoading3Grid, 250);
                }
            }
            //loadData(_getODataEndpoint(entitySchemaName), false);
            b.firstLoadGrid++;

            //loading3GridInterval = setInterval(validLoading3Grid, 250);
            //setTimeout(validLoading3Grid, 250);

        });

        // Refresh Grid
        reloadGrid = function () {
            loadData(_getODataEndpoint(entitySchemaName), true);
        }
        get_dataOkgrid = function () {
            return dataOk;
        }
        get_saveGridStatus = function () {
            return saveGridStatus;
        }
        refreshGrid = function () {
            Ext.getCmp(idGrid).getView().refresh();
        }

        //window.parent.Xrm.Page.getControl("WebResource_milestone_grid").getObject().contentWindow.window.nextMilestone("08/17/2015");
        nextMilestone = function(completeMsDate){
            var actualEnd = new Date(completeMsDate);
            actualEnd.setHours(0,0,0,0);
            debugger;
            bpClick_btn = true;
            //if(actualEnd == null){}{actualEnd = new Date();}
            //console.log(actualEnd);
            bussinessProcessFlag = null;
            var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
            //console.dir(recordsGrid);
            var recordsCount = recordsGrid.length;
            var activeMS = -1;
            if (recordsCount > 0) {
                for (var i = 0; i < recordsCount; i++) {
                    var record = recordsGrid[i];
                    if(parseInt(record.data.ddsm_Status) == 962080001) {activeMS = i;break;}
                }
                if(activeMS != -1){
                    var actualStart = new Date(recordsGrid[activeMS].data.ddsm_ActualStart), today = new Date();
                    actualStart.setHours(0,0,0,0);
                    today.setHours(23,59,59,59);

                    if (actualStart.getTime() > today.getTime())
                    {
                        //b.alert( "Workflow Step Actual Start date is greater than today's date.");
                        b.Alert.show("Workflow Step Actual Start date is greater than today's date.", null, [{
                            label: "Ok",
                            callback: function () {
                            }
                        }], "ERROR", 500, 200);
                        bussinessProcessFlag =  false;
                    } else {
                        if (actualStart.getTime() > actualEnd.getTime()) {
                            //b.alert( "Workflow Step Complete Date must be later than Actual Start's date.");
                            b.Alert.show("Workflow Step Complete Date must be later than Actual Start's date.", null, [{
                                label: "Ok",
                                callback: function () {
                                }
                            }], "ERROR", 500, 200);
                            bussinessProcessFlag =  false;
                        } else {
                            if (today.getTime() >= actualEnd.getTime()) {
                                rowEditing.startEdit(activeMS, 7);
                                var date = new Date(actualEnd);
                                actualEnd = ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();
                                //console.log(actualEnd);
                                rowEditing.getEditor().getForm().findField("ddsm_ActualEnd").setValue(actualEnd);
                                automationNextStep = 1;
                                rowEditing.completeEdit();
                            } else {
                                //b.alert( "Workflow Step Complete Date can not be later than today's date.");
                                b.Alert.show("Workflow Step Complete Date can not be later than today's date.", null, [{
                                    label: "Ok",
                                    callback: function () {
                                    }
                                }], "ERROR", 500, 200);
                                bussinessProcessFlag =  false;
                            }
                        }
                    }

                } else {bussinessProcessFlag =  false;}
            }
        };

        prevMilestone = function(){
            bpClick_btn = true;
            bussinessProcessFlag = null;
            var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
            //console.dir(recordsGrid);
            var recordsCount = recordsGrid.length;
            var activeMS = -1;
            if (recordsCount > 0) {
                for (var i = 0; i < recordsCount; i++) {
                    var record = recordsGrid[i];
                    if(parseInt(record.data.ddsm_Status) == 962080001) {activeMS = i;break;}
                }
                if(parseInt(recordsGrid[recordsCount - 1].data.ddsm_Status) == 962080002) {activeMS = recordsCount;}
                if(activeMS != -1){
                    rowEditing.startEdit(activeMS - 1, 7);
                    rowEditing.getEditor().getForm().findField("ddsm_ActualEnd").setValue(null);
                    automationNextStep = -1;
                    rowEditing.completeEdit();
                } else {bussinessProcessFlag =  false;}
            }
        };
        validStepBP = function(){
            return bussinessProcessFlag;
        };
        getActiveIndex = function () {
            var recordsGrid = Ext.getCmp(idGrid).getStore().getRange();
            //console.dir(recordsGrid);
            var recordsCount = recordsGrid.length;
            var activeMS = -1;
            if (recordsCount > 0) {
                for (var i = 0; i < recordsCount; i++) {
                    var record = recordsGrid[i];
                    if(parseInt(record.data.ddsm_Status) == 962080001) {activeMS = i;break;}
                }
                if(parseInt(recordsGrid[recordsCount - 1].data.ddsm_Status) == 962080002) {activeMS = recordsCount;}
            }
            return activeMS;
        };
        $(b).resize(function () {
            Ext.getCmp(idGrid).getView().refresh();
        });
    });
} else {
    b.Xrm.Page.ui.tabs.get("Tab_Milestones").setVisible(false);
}