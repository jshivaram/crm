/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 2.1.4
 * Last updated on 06/02/2015.
 * Uploads Docs Grid & verify Document Name Convention
 */
(function (fallback) {

    fallback = fallback || function () { };

    var trap = function () {
        var args = Array.prototype.slice.call(arguments);
        var message = args.join(' ');
        fallback(message);
    };

    if (typeof console === 'undefined') {
        console = {
            messages: [],
            raw: [],
            dump: function () { return console.messages.join('\n'); },
            log: trap,
            debug: trap,
            info: trap,
            warn: trap,
            error: trap,
            assert: trap,
            clear: function () {
                console.messages.length = 0;
                console.raw.length = 0;
            },
            dir: trap,
            dirxml: trap,
            trace: trap,
            group: trap,
            groupCollapsed: trap,
            groupEnd: trap,
            time: trap,
            timeEnd: trap,
            timeStamp: trap,
            profile: trap,
            profileEnd: trap,
            count: trap,
            exception: trap,
            table: trap
        };
    }

})(null);

var b = window.parent;
if (b.Xrm.Page.data.entity.getId() != "") {
    var dataOk, results = [], loadData, _getODataEndpoint, entitySchemaName, saveGridStatus, lookupActive, get_saveGridStatus, get_dataOkgrid, refreshGrid, reloadGrid, idGrid, filesCount = 0, AutoEmailIncomplete = false, PrimaryContact = null, EMailAddress = null;

    $(b.document).ready(function () {

        Ext.Loader.setConfig({
            enabled: true,
            disableCaching: false,
            path: { 'Ext': 'accentgold_/EditableGrid/js/' }
        });
        Ext.Loader.setPath('Ext.ux', 'js/ux');
        Ext.require([
            'Ext.grid.*',
            'Ext.data.*',
            'Ext.ux.grid.FiltersFeature',
            'Ext.ux.grid.menu.ListMenu',
            'Ext.util.*',
            'Ext.state.*',
            'Ext.form.*',
            'Ext.tip.QuickTip',
            'Ext.selection.CheckboxModel'
        ]);

        Ext.override('Ext.ux.grid.FiltersFeature', {
            show: function () {
                var lastArgs = null;
                return function () {
                    if (!arguments) {
                        this.callParent(lastArgs);
                    } else {
                        lastArgs = arguments;
                        if (this.loadOnShow && !this.loaded) {
                            this.store.load();
                        }
                        this.callParent(arguments);
                    }
                };
            }()
        });
        Ext.onReady(function () {

            ///// START ACTION COLUMNS
            ///// DELETE COLUMN
            var delActColumn = {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                items: [{
                    icon: 'icons/grid/delete.gif',
                    tooltip: 'Delete Document',
                    handler: function (grid, rowIndex) {
                        rowEditing.cancelEdit();
                        var record = [];
                        record.push(grid.getStore().getAt(rowIndex));
                        Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete select records?', function (btn, text) {
                            if (btn == 'yes') {
                                initSpinner("Deleting file ...");
                                Delete_Record(record, grid);
                                grid.getStore().removeAt(rowIndex);
                            }
                        });
                    }
                }]
            };
            var editActColumn = {
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                items: [{
                    icon: 'icons/grid/edit.png',
                    tooltip: 'Edit Document',
                    handler: function (grid, rowIndex) {
                        rowEditing.cancelEdit();
                        var record = grid.getStore().getAt(rowIndex);
                        window.open(Xrm.Page.context.getClientUrl() + "/notes/edit.aspx?id=" + record.data["AnnotationId"]);
                    }
                }]
            };
            ///// END ACTION COLUMNS


            var defaultFieldsValue = {};
            dataOk = false;
            saveGridStatus = false;
            var hide_columns = [];
            var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
            var _odataEndpoint = "/XRMServices/2011/OrganizationData.svc";

            var modelGrid, renderTo, dateFormat, heightGrid, borderGrid, titleGrid, sortersGrid;
            if (typeof configJson.entitySchemaName !== 'undefined') { entitySchemaName = configJson.entitySchemaName; } else { entitySchemaName = ""; Ext.Msg.alert("Error", "The entitySchemaName parameter value is missing in the config file. Please specify the correct value in the config file of this grid (Web Resources)."); }
            if (typeof configJson.modelGrid !== 'undefined') { modelGrid = configJson.modelGrid; } else { modelGrid = "mGrid" + randomnumber; }
            if (typeof configJson.idGrid !== 'undefined') { idGrid = configJson.idGrid; } else { idGrid = "idGrid" + randomnumber; }
            if (typeof configJson.renderTo !== 'undefined') { renderTo = configJson.renderTo; } else { renderTo = Ext.getBody(); }
            if (typeof configJson.dateFormat !== 'undefined') { dateFormat = configJson.dateFormat; } else { dateFormat = "m/d/Y"; }
            if (typeof configJson.height !== 'undefined') { heightGrid = configJson.height; } else { heightGrid = 400; }
            if (typeof configJson.border !== 'undefined') { borderGrid = configJson.border; } else { borderGrid = false; }
            if (typeof configJson.title !== 'undefined') { titleGrid = configJson.title; } else { titleGrid = ''; }
            if (typeof configJson.sorters !== 'undefined') { sortersGrid = configJson.sorters; } else { sortersGrid = { property: configJson.entitySchemaName + 'Id', direction: 'ASC' }; }

            _getODataEndpoint = function (entitySchemaName) {
                return Ext.String.format("{0}{1}/{2}Set", b.Xrm.Page.context.getClientUrl(), _odataEndpoint, entitySchemaName);
            }

            var _configLoadSaveData = function () {
                var _config = [];
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            _config.push(configJson.fields[i].group[j]);
                        }
                    } else {
                        _config.push(configJson.fields[i]);
                    }
                }
                return _config;
            }
            var modConfigLoadSaveData = _configLoadSaveData();

            var _makeObjFields = function (obj) {
                var fields = [];
                var new_field = {};
                if (obj.type != "lookup") {
                    if (typeof obj.defaultValue !== 'undefined') {
                        defaultFieldsValue[obj.name] = obj.defaultValue;
                    }
                    new_field.name = obj.name;
                    switch (obj.type) {
                        case 'date':
                            new_field.type = obj.type;
                            //new_field.dateFormat = dateFormat;
                            break;
                        case 'number':
                            new_field.type = obj.type;
                            break;
                        case 'boolean':
                            new_field.type = obj.type;
                            break;
                        case 'checkcolumn':
                            new_field.type = 'boolean';
                            break;
                        case 'combobox':
                            new_field.type = 'string';
                            break;
                        case 'currency':
                            new_field.type = 'number';
                            break;
                        default:
                            break;
                    }
                    return new_field;
                } else {
                    defaultFieldsValue[obj.name] = "";
                    new_field.name = obj.name;
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                    new_field.name = obj.name + '__Id';
                    fields.push(new_field);
                    new_field = {};
                    defaultFieldsValue[obj.name + '__LogicalName'] = Ext.util.Format.lowercase(obj.entityNameLookup);
                    new_field.name = obj.name + '__LogicalName';
                    fields.push(new_field);
                    return fields;
                }
            }
            var _createFields = function () {
                var fields = [];
                var new_field = {};
                new_field.name = entitySchemaName + 'Id';
                defaultFieldsValue[entitySchemaName + 'Id'] = '';
                fields.push(new_field);
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFields(configJson.fields[i].group[j]);
                            if ($.isArray(obj)) {
                                fields = fields.concat(obj);
                            } else {
                                fields.push(obj);
                            }
                        }
                    } else {
                        obj = _makeObjFields(configJson.fields[i]);
                        if ($.isArray(obj)) {
                            fields = fields.concat(obj);
                        } else {
                            fields.push(obj);
                        }
                    }
                }
                return fields;
            }
            var _makeObjColumns = function (obj) {
                var new_column = {};
                var new_editor = {};
                var new_filter = {};
                var renderer = null;
                if (obj.type != "lookup") {
                    new_column.dataIndex = obj.name;
                    new_column.header = obj.header;
                    new_column.tooltip = obj.header;
                    //new_column.draggable = false;
                    if (typeof obj.sortable !== 'undefined') {
                        new_column.sortable = obj.sortable;
                    }
                    if (typeof obj.filterable !== 'undefined') {
                        new_column.filterable = obj.filterable;
                    }
                    if (typeof obj.hidden !== 'undefined') {
                        new_column.hidden = obj.hidden;
                    }
                    if (typeof obj.flex !== 'undefined') {
                        new_column.flex = obj.flex;
                    } else {
                        if (typeof obj.width !== 'undefined') {
                            new_column.width = obj.width;
                        }
                    }
                    /*
                     if(typeof obj.format !== 'undefined'){
                     new_editor.format = obj.format;
                     }
                     */
                    switch (obj.type) {
                        case 'date':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'datefield';
                                        //new_editor.format = dateFormat;
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else {
                                    if (typeof obj.format !== 'undefined') {
                                        new_column.renderer = Ext.util.Format.dateRenderer(obj.format);
                                    } else {
                                        new_column.renderer = Ext.util.Format.dateRenderer(dateFormat);
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'number':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'numberfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else if (typeof obj.format !== 'undefined') {
                                    new_column.renderer = Ext.util.Format.numberRenderer(obj.format);
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'combobox':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'combo';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'currency':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {
                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'numberfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                } else {
                                    new_column.renderer = Ext.util.Format.usMoney;
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'boolean':
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        //                                new_editor.xtype = 'textfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        case 'checkcolumn':
                            if (!new_column.hidden) {
                                new_column.xtype = 'checkcolumn';
                                new_column.listeners = {
                                    checkChange: onCheckChange
                                };
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                        default:
                            if (!new_column.hidden) {
                                if (!obj.readOnly) {

                                    if (typeof obj.editor !== 'undefined') {
                                        if (typeof obj.editor !== 'object') {
                                            new_column.editor = eval(obj.editor);
                                        } else {
                                            new_column.editor = obj.editor;
                                        }
                                    } else {
                                        new_editor.itemId = obj.name;
                                        new_editor.xtype = 'textfield';
                                        //new_editor.readOnly = obj.readOnly;
                                        new_editor.allowBlank = obj.allowBlank;
                                        new_editor.listeners = {
                                            focus: function (editor, e) { lookupActive = ""; }
                                        }
                                        new_column.editor = new_editor;
                                    }
                                }
                                if (typeof obj.renderer !== 'undefined') {
                                    if (typeof obj.renderer !== 'object') {
                                        new_column.renderer = eval(obj.renderer);
                                    } else {
                                        new_column.renderer = obj.renderer;
                                    }
                                }
                            } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }
                            break;
                    }
                    if (typeof obj.hidden !== 'undefined' && obj.hidden) {
                        hide_columns.push(new_column);
                        return null;
                    } else {
                        return new_column;
                    }
                } else {

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name + '__Id';
                    new_column.header = obj.name + '__Id';
                    new_column.filterable = false;
                    new_column.sortable = false;
                    new_column.hidden = true;
                    new_column.width = 50;
                    new_editor.itemId = obj.name + '__Id';
                    new_editor.readOnly = true;
                    new_editor.allowBlank = true;
                    new_editor.xtype = 'hidden';
                    new_column.editor = new_editor;
                    hide_columns.push(new_column);

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name + '__LogicalName';
                    new_column.header = obj.name + '__LogicalName';
                    new_column.filterable = false;
                    new_column.sortable = false;
                    new_column.hidden = true;
                    new_column.width = 50;
                    new_editor.itemId = obj.name + '__LogicalName';
                    new_editor.readOnly = true;
                    new_editor.allowBlank = true;
                    new_editor.xtype = 'hidden';
                    new_column.editor = new_editor;
                    hide_columns.push(new_column);

                    new_column = {};
                    new_editor = {};
                    new_column.dataIndex = obj.name;
                    new_column.header = obj.header;
                    new_column.tooltip = obj.header;
                    //new_column.draggable = false;
                    if (typeof obj.sortable !== 'undefined') {
                        new_column.sortable = obj.sortable;
                    }
                    if (typeof obj.filterable !== 'undefined') {
                        new_column.filterable = obj.filterable;
                    }
                    if (typeof obj.hidden !== 'undefined') {
                        new_column.hidden = obj.hidden;
                    }
                    if (typeof obj.flex !== 'undefined') {
                        new_column.flex = obj.flex;
                    } else {
                        if (typeof obj.width !== 'undefined') {
                            new_column.width = obj.width;
                        }
                    }
                    /*
                     if(typeof obj.format !== 'undefined'){
                     new_editor.format = obj.format;
                     }
                     */
                    if (!new_column.hidden) {
                        if (!obj.readOnly) {

                            if (typeof obj.editor !== 'undefined') {
                                if (typeof obj.editor !== 'object') {
                                    new_column.editor = eval(obj.editor);
                                } else {
                                    new_column.editor = obj.editor;
                                }
                            } else {
                                new_editor.itemId = obj.name;
                                //new_editor.readOnly = obj.readOnly;
                                new_editor.allowBlank = obj.allowBlank;
                                new_editor.listeners = {
                                    //scope:this,
                                    focus: function (e) {
                                        if ((this.value == "" || this.value == null) && lookupActive != e.name) {
                                            lookupActive = e.name;
                                            _getLookupData(e.name);
                                        }
                                    }
                                };

                                new_column.editor = new_editor;
                            }
                        }
                        if (typeof obj.renderer !== 'undefined') {
                            if (typeof obj.renderer !== 'object') {
                                new_column.renderer = eval(obj.renderer);
                            } else {
                                new_column.renderer = obj.renderer;
                            }
                        } else {
                            new_column.renderer = function (value, metaData, record, rowIdx, colIdx, store, view) {
                                var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                                var column = view.getHeaderAtIndex(colIdx);
                                if (value != null) {
                                    return Ext.String.format(
                                        '<a href="/main.aspx?etn={2}&extraqs=&histKey={3}&id={1}&newWindow=true&pagetype=entityrecord" target="_blank">{0}</a>',
                                        value,
                                        "{" + record.data[column.dataIndex + "__Id"] + "}",
                                        record.data[column.dataIndex + "__LogicalName"],
                                        randomnumber
                                    );
                                } else { return ''; }
                            };
                        }
                    } else { new_editor.xtype = 'hidden'; new_column.editor = new_editor; }

                    if (typeof obj.hidden !== 'undefined' && obj.hidden) {
                        hide_columns.push(new_column);
                        return null;
                    } else {
                        return new_column;
                    }
                }

            }
            var _createColumns = function () {
                var columns = [];
                //hide_columns = [];
                columns.push(Ext.create('Ext.grid.RowNumberer'));
                var new_column = {};
                var new_editor = {};
                var new_filter = {};
                var renderer = null;
                new_column.dataIndex = entitySchemaName + 'Id';
                new_column.header = 'Id';
                new_column.filterable = false;
                new_column.sortable = false;
                new_column.hidden = true;
                new_column.width = 150;
                new_editor.readOnly = true;
                new_editor.allowBlank = true;
                new_editor.xtype = 'hidden';
                new_column.editor = new_editor;
                hide_columns.push(new_column);

                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    //console.log(configJson.fields[i].name + ".renderer = " + configJson.fields[i].renderer);
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        var group = {};
                        group.header = configJson.fields[i].groupHeader;
                        group.menuDisabled = true,
                            group.columns = [];
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjColumns(configJson.fields[i].group[j]);
                            if (obj != null) {
                                if ($.isArray(obj)) {
                                    group.columns = group.columns.concat(obj);
                                } else {
                                    group.columns.push(obj);
                                }
                            }
                        }
                        columns.push(group);
                    } else {
                        obj = _makeObjColumns(configJson.fields[i]);
                        if (obj != null) {
                            if ($.isArray(obj)) {
                                columns = columns.concat(obj);
                            } else {
                                columns.push(obj);
                            }
                        }
                    }
                }
                //columns.push(editActColumn);
                columns.push(delActColumn);
                columns = columns.concat(hide_columns);
                return columns;
            }
            var _makeObjFilters = function (obj) {
                var filterType = {};
                if (obj.filterable) {
                    if (typeof obj.filter !== 'undefined') {
                        if (typeof obj.filter !== 'filterTypeect') {
                            filterType = eval(obj.filter);
                        } else {
                            filterType = obj.filter;
                        }
                    } else {
                        switch (obj.type) {
                            case 'date':
                                filterType.type = 'date';
                                break;
                            case 'number':
                                filterType.type = 'numeric';
                                break;
                            case 'currency':
                                filterType.type = 'numeric';
                                break;
                            case 'lookup':
                                filterType.type = 'string';
                                break;
                            default:
                                filterType.type = 'string';
                                break;
                        }
                        filterType.dataIndex = obj.name;
                    }
                }
                return filterType;

            }
            var _createFilters = function () {
                var filterObj = {
                    ftype: 'filters',
                    encode: false,
                    local: true,
                    filters: []
                }
                for (var i = 0; i < configJson.fields.length; i++) {
                    var obj;
                    if (typeof configJson.fields[i].group !== 'undefined') {
                        for (var j = 0; j < configJson.fields[i].group.length; j++) {
                            obj = _makeObjFilters(configJson.fields[i].group[j]);
                            if (obj != null) {
                                if ($.isArray(obj)) {
                                    filterObj.filters = filterObj.filters.concat(obj);
                                } else {
                                    filterObj.filters.push(obj);
                                }
                            }
                        }
                    } else {
                        obj = _makeObjFilters(configJson.fields[i]);
                        if (obj != null) {
                            if ($.isArray(obj)) {
                                filterObj.filters = filterObj.filters.concat(obj);
                            } else {
                                filterObj.filters.push(obj);
                            }
                        }
                    }
                }
                return filterObj;
            };
            var _createStore = function (data, fields) {
                return Ext.create('Ext.data.Store', {
                    model: modelGrid,
                    autoDestroy: true,
                    fields: fields,
                    data: data,
                    autoLoad: false,
                    autoSync: false,
                    sortOnLoad: true,
                    remoteSort: false,
                    remoteFilter: false,
                    simpleSortMode: true,
                    sorters: sortersGrid,
                    proxy: {
                        type: 'memory',
                        reader: {
                            type: 'json'
                        }
                    }
                });
            };
            var _toUTCDate = function (date) {
                return new Date(date.getTime());
            }
            function UTCToLocalTime(d) {
                var date = new Date(d);
                return ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();

            }

            var qtip = Ext.create('Ext.tip.QuickTip', {});
            var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToMoveEditor: 1,
                autoCancel: false
            });
            var viewConfig = {
                //loadingText: "Loading data ...",
                stripeRows: true,
                //trackOver:false,
                getRowClass: function (record, index, rowParams, store) {
                    if (record.get('ddsm_Status') == '962080001') {
                        return 'incomplete';
                    } else if (record.get('ddsm_Status') == '962080002') {
                        return 'approved';
                    }
                }
            };

            loadData = function (URI, reload) {
                var entity_ID = b.Xrm.Page.data.entity.getId();
                if (entity_ID !== "") {
                    if(URI == _getODataEndpoint(entitySchemaName)) {
                        results = [];
                        URI = URI + "?$select="
                        + "ddsm_uploadeddocumensId,"
                        + "CreatedOn,"
                        + "ModifiedOn,"
                        + "ddsm_Title,"
                        + "ddsm_Description,"
                        + "ddsm_Status,"
                        + "ddsm_ModifiedByName,"
                        + "ddsm_ModifiedSystem,"
                        + "ddsm_ConventionId,"
                        + "ddsm_uploadeddocumens_Annotations/AnnotationId,"
                        + "ddsm_uploadeddocumens_Annotations/FileName,"
                        + "ddsm_uploadeddocumens_Annotations/FileSize"
                        + "&$expand=ddsm_uploadeddocumens_Annotations";
                        URI = URI + "&$filter=ddsm_ProjectId/Id eq guid'" + entity_ID + "'";

                    }
                    //console.log(URI);
                    Ext.getCmp(idGrid).mask("Loading data, please wait...");

                    var req = new XMLHttpRequest();
                    req.open("GET", URI, false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.onreadystatechange = function () {
                        if (this.readyState == 4) {
                            req.onreadystatechange = null;
                            if (this.status == 200) {

                                var responseData = JSON.parse(this.responseText).d;
                                //console.log(responseData);
                                var newData = [];
                                var modeResponse = responseData.results;
                                if (modeResponse.length > 0) {

                                    for (var i = 0; i < modeResponse.length; i++) {
                                        //                        console.dir(modeResponse[i]);

                                        var objRecord = new Object();
                                        if(modeResponse[i].ddsm_uploadeddocumens_Annotations.results.length > 0) {

                                            //for(var l = 0; l < modeResponse[i].ddsm_uploadeddocumens_Annotations.results.length; l++) {
                                            var l = 0;

                                            objRecord[entitySchemaName + "Id"] = modeResponse[i][entitySchemaName + "Id"];
                                            objRecord["CreatedOn"] = eval((modeResponse[i]["CreatedOn"]).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                            objRecord["ModifiedOn"] = eval((modeResponse[i]["ModifiedOn"]).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                            objRecord["ddsm_Title"] = modeResponse[i]["ddsm_Title"];
                                            objRecord["ddsm_Description"] = modeResponse[i]["ddsm_Description"];
                                            objRecord["ddsm_Status"] = (modeResponse[i]["ddsm_Status"].Value).toString();
                                            objRecord["ddsm_ModifiedByName"] = modeResponse[i]["ddsm_ModifiedByName"];
                                            objRecord["ddsm_ModifiedSystem"] = (modeResponse[i]["ddsm_ModifiedSystem"].Value).toString();
                                            if (typeof modeResponse[i]['ddsm_ConventionId'] !== 'undefined' && modeResponse[i]['ddsm_ConventionId'].Id != null) {
                                                objRecord['ddsm_ConventionId'] = modeResponse[i]['ddsm_ConventionId'].Name;
                                                objRecord['ddsm_ConventionId__Id'] = modeResponse[i]['ddsm_ConventionId'].Id;
                                                objRecord['ddsm_ConventionId__LogicalName'] = modeResponse[i]['ddsm_ConventionId'].LogicalName;
                                            } else if (modeResponse[i]['ddsm_ConventionId'].Id == null || modeResponse[i]['ddsm_ConventionId'].Id === '') {
                                                objRecord['ddsm_ConventionId'] = '';
                                                objRecord['ddsm_ConventionId__Id'] = '00000000-0000-0000-0000-000000000000';
                                                objRecord['ddsm_ConventionId__LogicalName'] = "ddsm_documentconvention";
                                            }
                                            objRecord["AnnotationId"] = modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l]["AnnotationId"];
                                            objRecord["FileName"] = modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l]["FileName"];
                                            objRecord["FileSize"] = modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l]["FileSize"];

                                            /*
                                             for (var j = 0; j < modConfigLoadSaveData.length; j++) {
                                             switch (modConfigLoadSaveData[j].type) {
                                             case 'date':
                                             if ((typeof modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name] !== 'undefined') && (modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name] != null)) {
                                             objRecord[modConfigLoadSaveData[j].name] = eval((modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name]).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                             } else if (modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name] == null) {
                                             objRecord[modConfigLoadSaveData[j].name] = modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name]
                                             }
                                             break
                                             case 'lookup':
                                             if (typeof modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name] !== 'undefined' && modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name].Id != null) {
                                             objRecord[modConfigLoadSaveData[j].name] = modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name].Name;
                                             objRecord[modConfigLoadSaveData[j].name + '__Id'] = modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name].Id;
                                             objRecord[modConfigLoadSaveData[j].name + '__LogicalName'] = modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name].LogicalName;
                                             } else if (modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name].Id == null || modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name].Id === '') {
                                             objRecord[modConfigLoadSaveData[j].name] = '';
                                             objRecord[modConfigLoadSaveData[j].name + '__Id'] = '00000000-0000-0000-0000-000000000000';
                                             objRecord[modConfigLoadSaveData[j].name + '__LogicalName'] = modConfigLoadSaveData[j].entityNameLookup;
                                             }
                                             break
                                             case 'combobox':
                                             if (typeof modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name] !== 'undefined') {
                                             objRecord[modConfigLoadSaveData[j].name] = modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name].Value;
                                             }
                                             break
                                             case 'currency':
                                             if (typeof modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name] !== 'undefined') {
                                             objRecord[modConfigLoadSaveData[j].name] = modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name].Value;
                                             }
                                             break
                                             case 'number':
                                             if (typeof modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name] !== 'undefined') {
                                             objRecord[modConfigLoadSaveData[j].name] = modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name];
                                             }
                                             break
                                             default:
                                             if (typeof modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name] !== 'undefined') {
                                             objRecord[modConfigLoadSaveData[j].name] = modeResponse[i].ddsm_uploadeddocumens_Annotations.results[l][modConfigLoadSaveData[j].name];
                                             }
                                             }
                                             }
                                             */
                                            newData.push(objRecord);
                                            //}

                                        }
                                    }
                                    results = results.concat(newData);
                                    if (responseData.__next) {
                                        //console.log(responseData.__next);
                                        loadData(responseData.__next, reload);
                                    } else {
                                        console.log(">>>>> load data ok! records: " + results.length);
                                        dataOk = true;

                                        if (!reload) {
                                            Ext.define(modelGrid, {
                                                extend: 'Ext.data.Model',
                                                fields: _createFields()
                                            });
                                            Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                            Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            Ext.getCmp(idGrid).getView().refresh();
                                        } else {
                                            Ext.getCmp(idGrid).getStore().loadData(results);
                                            Ext.getCmp(idGrid).filters.clearFilters();
                                            Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                            Ext.getCmp(idGrid).getView().refresh();
                                        }
                                        Ext.getCmp(idGrid).unmask();
                                    }
                                } else {
                                    console.log(">>>>> data entity null!");
                                    dataOk = false;
                                    if (!reload) {
                                        Ext.define(modelGrid, {
                                            extend: 'Ext.data.Model',
                                            fields: _createFields()
                                        });
                                        Ext.getCmp(idGrid).reconfigure(_createStore(results, _createFields()), _createColumns());
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    } else {
                                        Ext.getCmp(idGrid).getStore().loadData(results);
                                        Ext.getCmp(idGrid).getStore().sort(configJson.sorters['property'], configJson.sorters['direction']);
                                        Ext.getCmp(idGrid).getView().refresh();
                                    }
                                    Ext.getCmp(idGrid).unmask();
                                }
                            }
                        }
                    };
                    lookupActive = "";
                    req.send(null);
                }
            }

            function getStorageList() {
                var storageId, storageName, emptyStorageNameList = true;
                var req = new XMLHttpRequest();
                var URI_ = b.Xrm.Page.context.getClientUrl() + "/XRMServices/2011/OrganizationData.svc/ddsm_documentsfolderSet?"
                    + "$select="
                    + "ddsm_name,"
                    + "ddsm_documentsfolderId";
                req.open("GET", URI_, false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        req.onreadystatechange = null;
                        if (this.status == 200) {

                            var responseData = JSON.parse(this.responseText).d;
                            var modeResponse = responseData.results;
                            if (modeResponse.length > 0) {

                                var popupHTML = '<div id="overlay" style="display:none; position:fixed; z-index:998; opacity:0.5; filter:alpha(opacity=50); top:0; right:0; left:0; bottom:0; background:#000000;"></div>';
                                popupHTML += '<div id="loading" style="display:none; position:fixed; width:320px; height:320px; top:50%; left:50%; margin:-160px 0 0 -160px; z-index:999;"><img src="/WebResources/ddsm_loading.gif" alt="loading"/></div>';
                                popupHTML += '<div id="popup" style="display:none; position:fixed; border:3px solid #999; background:#fff; width:auto; height:auto; top:50%; left:50%; margin:-100px 0 0 -300px; z-index:1000;  border-radius:10px; padding:10px 30px 30px 30px;">';
                                popupHTML += '<h2 style="color: #aa0000;font-size: 16px;">Storages for Documents</h2>';
                                popupHTML += '<table><tr><td style="font-size: 12px;"><strong>Storage&nbsp;Name:&nbsp;</strong></td><td id="tdSelect">';
                                popupHTML += '</td><td style="padding-left:10px;width:100px;"><input type="button" value="Next" id="getDocumentsStorage" name="getDocumentsStorage" style="margin:0 5px 0 80px;width:50px;"></td><td>';
                                popupHTML += '</td><td style="padding-lefty10px;"><input type="button" value="Cancel" id="cancelGetStorage" name="cancelGetStorage"></td><td></tr></table>';
                                popupHTML += '</div>';

                                var StorageNameList = $("<select>").attr("id", "StorageNameList").attr("name", "StorageNameList");
                                for (var i = 0; i < modeResponse.length; i++) {
                                    emptyStorageNameList = false;
                                    StorageNameList.append($('<option>', {
                                        value: modeResponse[i].ddsm_documentsfolderId,
                                        text: modeResponse[i].ddsm_name
                                    }));
                                }

                                if (!emptyStorageNameList) {
                                    $('body').append(popupHTML);
                                    StorageNameList.change(function () {
                                        storageId = $(this).val();
                                        storageName = $(this).text();
                                    }).trigger('change');
                                    $('#tdSelect').append(StorageNameList);

                                    $('#popup').show();
                                    $('#overlay').show();
                                    $('#loading').hide();

                                    $('#getDocumentsStorage').on('click', function () {
                                        $('#popup').hide();
                                        $('#loading').show();
                                        getDocumentsStorage(storageId, storageName);
                                    });
                                    $('#cancelGetStorage').on('click', function () {
                                        $('#overlay, #popup, #loading').hide();
                                        $('#overlay').remove();
                                        $('#popup').remove();
                                        $('#loading').remove();
                                    });
                                } else {
                                    Ext.Msg.alert("Error", "There are no Storages for Documents.");
                                }

                            } else {
                                Ext.Msg.alert("Error", "There are no Storages for Documents.");
                            }
                        }
                    }
                }
                req.send(null);

            }

            function getDocumentsStorage(storageId, storageName) {
                $('#overlay, #popup, #loading').hide();
                $('#overlay').remove();
                $('#popup').remove();
                $('#loading').remove();

                //var f = Xrm.Page.context.getQueryStringParameters().typename, o = Xrm.Page.context.getQueryStringParameters().id;
                var emptyDocsList = true;
                var req = new XMLHttpRequest();
                var URI_ = _getODataEndpoint("Annotation")
                    + "?$select=AnnotationId,NoteText,FileName,MimeType,DocumentBody,IsDocument,Subject,FileSize&$filter=ObjectId/Id eq guid'" + storageId + "' and IsDocument eq true";
                req.open("GET", URI_, false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        req.onreadystatechange = null;
                        if (this.status == 200) {

                            var responseData = JSON.parse(this.responseText).d;
                            var modeResponse = responseData.results;
                            if (modeResponse.length > 0) {

                                var popupHTML = '<div id="overlay" style="display:none; position:fixed; z-index:998; opacity:0.5; filter:alpha(opacity=50); top:0; right:0; left:0; bottom:0; background:#000000;"></div>';
                                popupHTML += '<div id="loading" style="display:none; position:fixed; width:320px; height:320px; top:50%; left:50%; margin:-160px 0 0 -160px; z-index:999;"><img src="/WebResources/ddsm_loading.gif" alt="loading"/></div>';
                                popupHTML += '<div id="popup" style="display:none; position:fixed; border:3px solid #999; background:#fff; width:auto; height:auto; top:50%; left:50%; margin:-100px 0 0 -300px; z-index:1000;  border-radius:10px; padding:10px 30px 30px 30px;">';
                                popupHTML += '<h2 style="color: #aa0000;font-size: 16px;" id="storageName"></h2>';
                                popupHTML += '<table><tr><td style="font-size: 12px;"><strong>Document&nbsp;Name:&nbsp;</strong></td><td id="tdSelect">';
                                popupHTML += '</td><td style="padding-left:10px;width:100px;"><input type="button" value="Next" id="copyDocumentsStorage" name="copyDocumentsStorage" style="margin:0 5px 0 80px;width:50px;"></td><td>';
                                popupHTML += '</td><td style="padding-lefty10px;"><input type="button" value="Cancel" id="cancelDocuments" name="cancelDocuments"></td><td></tr></table>';
                                popupHTML += '</div>';

                                $("#storageName").text(storageName);
                                var DocNameList = $("<select  multiple='multiple' size='5'>").attr("id", "DocNameList").attr("name", "DocNameList");
                                for (var i = 0; i < modeResponse.length; i++) {
                                    emptyDocsList = false;
                                    var textOpt = modeResponse[i].FileName + " (" + modeResponse[i].Subject + ")";
                                    DocNameList.append($('<option>', {
                                        //value: modeResponse[i].AnnotationId,
                                        value: i,
                                        text: textOpt
                                    }));
                                }

                                if (!emptyDocsList) {
                                    $('body').append(popupHTML);
                                    DocNameList.change(function () {
                                        //storageId = $(this).val();
                                        //storageName = $(this).text();
                                    }).trigger('change');
                                    $('#tdSelect').append(DocNameList);

                                    $('#popup').show();
                                    $('#overlay').show();
                                    $('#loading').hide();

                                    $('#copyDocumentsStorage').on('click', function () {
                                        $('#popup').hide();
                                        $('#popup').remove();
                                        $('#overlay, #loading').show();

                                        var docCount = (DocNameList.find('option:selected')).length;
                                        for (var i = 0; i < docCount; i++) {
                                            if ($('#overlay')) {
                                                $('#overlay, #loading').show();
                                            } else {
                                                var popupHTML = '<div id="overlay" style="display:none; position:fixed; z-index:998; opacity:0.5; filter:alpha(opacity=50); top:0; right:0; left:0; bottom:0; background:#000000;"></div>';
                                                popupHTML += '<div id="loading" style="display:none; position:fixed; width:320px; height:320px; top:50%; left:50%; margin:-160px 0 0 -160px; z-index:999;"><img src="/WebResources/ddsm_loading.gif" alt="loading"/></div>';
                                                $('body').append(popupHTML);
                                                $('#overlay, #loading').show();
                                            }
                                            //console.dir(DocNameList.find('option:selected:eq(' + i + ')').val());
                                            var idx = parseInt(DocNameList.find('option:selected:eq(' + i + ')').val());
                                            var ProjectId = {}, SiteId = {}, AccountId = {};
                                            ProjectId.Id = b.Xrm.Page.data.entity.getId();
                                            ProjectId.LogicalName = b.Xrm.Page.data.entity.getEntityName();
                                            ProjectId.Name = b.Xrm.Page.getAttribute("ddsm_name").getValue();
                                            if(b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue() != null){
                                                SiteId.Id = b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id;
                                                SiteId.LogicalName = b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].entityType;
                                                SiteId.Name = b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].name;
                                            }
                                            if(b.Xrm.Page.getAttribute("ddsm_accountid").getValue() != null){
                                                AccountId.Id = b.Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id;
                                                AccountId.LogicalName = b.Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].entityType;
                                                AccountId.Name = b.Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].name;
                                            }
                                            var newRecord = b.gen_H_createEntitySync({ddsm_ProjectId: ProjectId, ddsm_SiteId: SiteId, ddsm_AccountId: AccountId, ddsm_name: modeResponse[idx].FileName, ddsm_Title: modeResponse[idx].FileName, ddsm_Status: {Value: 962080000}, ddsm_ModifiedSystem: {Value: 962080000}, ddsm_ModifiedByName: b.Xrm.Page.context.getUserName()},"ddsm_uploadeddocumensSet");

                                            var r = {}, obj, f = "ddsm_uploadeddocumens";
                                            r.LogicalName = f; r.Id = newRecord.ddsm_uploadeddocumensId;r.Name = newRecord.ddsm_name;
                                            obj = { ObjectTypeCode: f, ObjectId: r, Subject: modeResponse[idx].Subject, IsDocument: modeResponse[idx].IsDocument, FileName: modeResponse[idx].FileName, NoteText: modeResponse[idx].NoteText, MimeType: modeResponse[idx].MimeType, FileSize: modeResponse[idx].FileSize, DocumentBody: modeResponse[idx].DocumentBody };
                                            CrmRestKit.Create("Annotation", obj, !1).fail(onRestErrorCopy).done(function (data, status, xhr) {
                                                checkDocNameConvention(data);
                                                $('#overlay, #loading').hide();
                                                if (i == docCount - 1) {
                                                    b.document.getElementById('WebResource_docConvention').contentWindow.reloadGrid();
                                                    loadData(_getODataEndpoint(entitySchemaName), true);
                                                    $('#overlay').remove(); $('#loading').remove();
                                                }
                                            });
                                        }

                                        //console.dir(DocNameList.find('option:selected'));
                                    });
                                    $('#cancelDocuments').on('click', function () {
                                        $('#overlay, #popup, #loading').hide();
                                        $('#overlay').remove();
                                        $('#popup').remove();
                                        $('#loading').remove();
                                    });
                                } else {
                                    Ext.Msg.alert("Error", "There are no documents in this storage.");
                                }

                            } else {
                                Ext.Msg.alert("Error", "There are no documents in this storage.");
                            }
                        }
                    }
                }
                req.send(null);

            }

            function Delete_Record(records, grid) {
                var ConventionArr = [];
                filesCount = records.length;
                for (var i = 0; i < records.length; i++) {
                    if (records[i].data['ddsm_ConventionId__Id'] !== '00000000-0000-0000-0000-000000000000' && records[i].data['ddsm_ConventionId__Id'] != null) {
                        ConventionArr.push(records[i].data);
                    }
                    CrmRestKit.Delete(entitySchemaName, records[i].data[entitySchemaName + 'Id']).done(function (data, status, xhr) {
                        filesCount--;
                        if (filesCount == 0) {
                            grid.getStore().commitChanges();
                            Ext.getCmp(idGrid).getView().refresh();
                            stopSpinner();
                        }
                    });
                }
                /*
                 for (var i = 0; i < records.length; i++) {
                 CrmRestKit.Delete("Annotation", records[i].data['AnnotationId']).done(function (data, status, xhr) {
                 //console.dir(data);
                 filesCount--;
                 if (filesCount == 0) {
                 $('#overlay, #loading').hide();
                 $('#overlay').remove();
                 $('#loading').remove();
                 grid.getStore().commitChanges();
                 Ext.getCmp(idGrid).getView().refresh();
                 }
                 });
                 }
                 */
                if(ConventionArr.length > 0) {updateConvetionGridStatus(ConventionArr);}
            }
            function updateConvetionGridStatus(convArr) {
                var proj_ID = b.Xrm.Page.data.entity.getId();
                var URL = _getODataEndpoint('ddsm_documentconvention');
                var select = '?$select=ddsm_documentconventionId,ddsm_UploadedId';
                var filter = "&$filter=ddsm_ProjectId/Id eq guid'" + proj_ID + "'";
                var req = new XMLHttpRequest();
                req.open("GET", URL + select + filter, false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.send();
                if (req.readyState == 4 && req.status == 200) {
                    var _tplData = JSON.parse(req.responseText).d;
                    if (_tplData.results[0] != null) {
                        for (var i = 0; i < _tplData.results.length; i++) {
                            for(var j = 0; j < convArr.length; j++){
                                if(_tplData.results[i].ddsm_UploadedId.Id != null){
                                    if((_tplData.results[i].ddsm_UploadedId.Id).replace(/\{|\}/g, '') == (convArr[j].ddsm_uploadeddocumensId).replace(/\{|\}/g, '')){
                                        var obj = {};
                                        obj.ddsm_Uploaded = false;
                                        obj.ddsm_UploadedId = {Id: null, LogicalName: null, Name: null};
                                        b.gen_H_updateEntitySync(_tplData.results[i].ddsm_documentconventionId, obj, "ddsm_documentconventionSet");
                                    }
                                }
                            }
                        }
                        b.document.getElementById('WebResource_docConvention').contentWindow.reloadGrid();
                    }
                }
            }

            var uploadDocsButton = Ext.create('Ext.form.field.File', {
                buttonOnly: true,
                hideLabel: true,
                buttonText: 'Upload',
                buttonConfig: {
                    icon: 'icons/grid/image_add.png'
                },
                listeners: {
                    afterrender: function (cmp) {
                        cmp.fileInputEl.set({
                            multiple: 'multiple'
                        });
                    },
                    change: function (me, value) {
                        var t = b.Xrm.Page.data.entity.getId();
                        if (typeof t == "undefined" || t == null || t.length < 36) {
                            alert("The file was unable to be uploaded. Please save the file and reload the page.");
                            return
                        }
                        var fileEl = me.fileInputEl.dom;
                        processFiles(fileEl.files);
                    }
                }
            });
            var grid = Ext.create('Ext.grid.Panel', {
                id: idGrid,
                border: borderGrid,
                loadMask: true,
                restful: true,
                autoSizeGrid: true,
                columns: [],
                selType: 'checkboxmodel',
                renderTo: renderTo,
                autoExpandColumn: 'lastUpdated',
                features: [_createFilters()],
                autoScroll: true,
                autoWidth: true,
                height: heightGrid,
                title: titleGrid,
                frame: false,
                plugins: [rowEditing],
                viewConfig: viewConfig,
                tbar: [uploadDocsButton,
                    /*
                    {
                        itemId: 'folderList',
                        icon: 'icons/grid/image_add.png',
                        text: 'Upload from Storage',
                        handler: function () {
                            getStorageList();
                        }
                    },
                    */
                    {
                        itemId: 'reload',
                        icon: 'icons/grid/table_refresh.png',
                        text: 'Reload',
                        handler: function () {
                            loadData(_getODataEndpoint(entitySchemaName), true);
                        }
                    }, {
                        itemId: 'remove',
                        text: 'Remove',
                        icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAADZklEQVQ4T22Tb2xTVRiHf2f29s+5ucfbrQwCNFNoiW3FMdbpyDSSzcQxyDSRENliBuLCUOKGSSNxX4yLhH7gkzEuVT4sG5YFDEZBE0JMNqLrtIMWOkqQjZFqSAndtN3t3W3vvTX3RoxB32/n5H2e88t7ziEA8I4o1vlqasaqK5WGlCS9dSybPW3sP1ofM7bTX1sbVTRt7u6dO13vA2liwE86nWM9zc3PuxjDNxcuyFOlUu/x+/dP/VswROmuBpfr1I6+PiZpGr6LRGYWMpke8kkgMN1VX/9sdX8/4PFAHRzEpfFxeQo48NHSUtSQDDHW0chx0dZ9+5jt6FGgUMDy2bO4NDJyg0SDweLr27Y5cPAgwPPA8jJWwmH8cOaMFOf5HmhafouqftW2Z4/AHzkC2GyApgHXr2NieFglH6xe3dWydm2kvamJr9q9G2AMKJchhcOYOH9eLRGitXZ02FgoBHAcoCiozM4ifu6c8svly+8RI2LI6dz7cm3tye3BoOOxzk6AUuiqiqUTJ6ArCmpCIVTZ7Sasz87i6sWL6o+x2EC/rn9qCowadDq7X2Dsi9ZAwM61twOCAL1YBCoVVFGKiixDS6UQn5xUf06nTdjg/hEYiw9F8dUmm238Rb/fyre1mUkMQUVRICcSuDI1pV3JZPb3A6MPD35UsD0oit83Li7aybp1qFqzxuzTJAnI53GrVCon5+dfexf49j+CIUHYtdXt/rIllxP+yGYhAZD/7rLzPHiXC4+73UgXCvK1ZLL3EGC+EzPBEKUdz/n90ZZslkmZjAmvALAQAsJxKAsCbIzBIQgQ6uqQXlyUE7HYgV5Ni5JjgtC5xe8feymXE+Tbt7EMoGTAHIe7FoteJkT38LxF5XlwggDKGHiPB9cyGenmxMTb5Ovm5vwrsiwoySQKAFQDphS/OhzKVUnqq5TLf9aL4uimVat4zWqFxZBQCr6hAdMzMyvk9ObNqZ12e8CWSCBfLoMTRdyiVJ3O5QYOK4p5VcPA3q3r15/0ut0OjRBQpxPE60VscnKBHLdaA09s3Diyw2JppA8eYIZSNX7v3sDhYtGEH1YE6K73ej9/xudz6NXV+Ckev/lbKrXfHGIY8D21YcOYyHFP31hYGDikKJ/933ceBbo3+XwRuVT6fX5u7o03gem/AGqKWxhL3xdrAAAAAElFTkSuQmCC',
                        handler: function () {
                            rowEditing.cancelEdit();
                            var sm = Ext.getCmp(idGrid).getSelectionModel();
                            Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete select records?', function (btn, text) {
                                if (btn == 'yes') {
                                    initSpinner("Deleting file ...");
                                    Delete_Record(sm.getSelection(), Ext.getCmp(idGrid));
                                    Ext.getCmp(idGrid).getStore().remove(sm.getSelection());
                                }
                            });
                        }
                    }],
                listeners: {
                    selectionchange: function (view, records) {
                        Ext.getCmp(idGrid).down('#remove').setDisabled(!records.length);
                    },
                    edit: function (editor, e) {
                        var obj = {};
                        e.record.set('ddsm_ModifiedByName', b.Xrm.Page.context.getUserName());
                        e.record.commit();
                        if(e.record.get('ddsm_ConventionId__Id') !== '00000000-0000-0000-0000-000000000000' && e.record.get('ddsm_ConventionId__Id') != null) {
                            if(parseInt(e.record.get('ddsm_Status')) == 962080001){
                                obj.ddsm_Uploaded = false;
                                b.gen_H_updateEntitySync(e.record.get('ddsm_ConventionId__Id'), obj, "ddsm_documentconventionSet");
                            } else {
                                obj.ddsm_Uploaded = true;
                                b.gen_H_updateEntitySync(e.record.get('ddsm_ConventionId__Id'), obj, "ddsm_documentconventionSet");
                            }
                            b.document.getElementById('WebResource_docConvention').contentWindow.reloadGrid();

                        }
                        var obj = { ddsm_Title: e.record.get('ddsm_Title'), ddsm_Description: e.record.get('ddsm_Description'), ddsm_ModifiedByName: e.record.get('ddsm_ModifiedByName'), ddsm_Status: {Value: e.record.get('ddsm_Status')} };

                        CrmRestKit.Update(entitySchemaName, e.record.get(entitySchemaName + "Id"), obj, !1).fail(onRestErrorUpdate).done(function (data, status, xhr) {
                        });

                        if(AutoEmailIncomplete && parseInt(e.record.get('ddsm_Status')) == 962080001){
                            CreateEmailIncomplete(e.record.get('AnnotationId'), e.record.get('FileName'));
                        }
                    },
                    canceledit: function (editor, e) {
                    },
                    afterrender: function() {
                        spinnerForm.stop();
                        loadData(_getODataEndpoint(entitySchemaName), false);
                    }
                }
            });

            //loadData(_getODataEndpoint(entitySchemaName), false);

            function checkDocNameConvention(docData){
                var fileName = docData.d.FileName;
                var proj_ID = b.Xrm.Page.data.entity.getId();
                var URL = _getODataEndpoint('ddsm_documentconvention');
                var select = '?$select=ddsm_documentconventionId,ddsm_name,ddsm_Uploaded';
                var filter = "&$filter=ddsm_ProjectId/Id eq guid'" + proj_ID + "'";
                var req = new XMLHttpRequest();
                req.open("GET", URL + select + filter, false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.send();
                if (req.readyState == 4 && req.status == 200) {
                    var _tplData = JSON.parse(req.responseText).d;
                    if (_tplData.results[0] != null) {
                        var result = false, Idx = _tplData.results.length;
                        for (var x = 0; x < _tplData.results.length; x++) {
                            result = false;
                            var str = _tplData.results[x].ddsm_name;
                            var re = new RegExp(str.replace(/\s/g, '[\\s_]'), "i");
                            //console.dir(re);
                            result = re.test(fileName);
                            if(result){Idx = x;break};
                        }
                        if(result){
                            var proj_obj = {}, doc_obj = {};
                            proj_obj.ddsm_Uploaded = true;
                            proj_obj.ddsm_UploadedId = docData.d.ObjectId;
                            b.gen_H_updateEntitySync(_tplData.results[Idx].ddsm_documentconventionId, proj_obj, "ddsm_documentconventionSet");
                            doc_obj.ddsm_ConventionId = {Id: _tplData.results[Idx].ddsm_documentconventionId, LogicalName: "ddsm_documentconvention", Name: _tplData.results[Idx].ddsm_name};
                            b.gen_H_updateEntitySync(docData.d.ObjectId.Id, doc_obj, "ddsm_uploadeddocumensSet");
                        }
                    }
                }

            }
            function dragcontrolOnLoad() {
                if (window.FileReader && Modernizr.draganddrop) {
                    var n = document.getElementById(renderTo);
                    n.addEventListener("drop", handleDrop, !1);
                    n.addEventListener("dragover", handleDragOver, !1)
                } else console.log("Drag and drop api not supported")
            }

            function handleDragOver(n) {
                n.stopPropagation(); n.preventDefault()
            }

            function handleDrop(n) {
                n.stopPropagation();
                n.preventDefault();
                var t = b.Xrm.Page.data.entity.getId();
                if (typeof t == "undefined" || t == null || t.length < 36) {
                    alert("The file was unable to be uploaded. Please save the file and reload the page.");
                    return;
                }
                processFiles(n.dataTransfer.files)
            }

            function processFiles(n) {

                if (n && n.length) {
                    filesCount = n.length;
                    for (var t = 0; t < n.length; t++) UploadSingleFile(n[t]);
                }
            }

            function UploadSingleFile(n) {
                initSpinner("Uploading files ...");
                var t = new FileReader, i = document.querySelector(".percent");
                t.onloadstart = function () {
                    document.getElementById("progress_bar").className = "loading"
                };
                t.onload = function (t) {

                    var ProjectId = {}, SiteId = {}, AccountId = {};
                    ProjectId.Id = b.Xrm.Page.data.entity.getId();
                    ProjectId.LogicalName = b.Xrm.Page.data.entity.getEntityName();
                    ProjectId.Name = b.Xrm.Page.getAttribute("ddsm_name").getValue();
                    if(b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue() != null){
                        SiteId.Id = b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id;
                        SiteId.LogicalName = b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].entityType;
                        SiteId.Name = b.Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].name;
                    }
                    if(b.Xrm.Page.getAttribute("ddsm_accountid").getValue() != null){
                        AccountId.Id = b.Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id;
                        AccountId.LogicalName = b.Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].entityType;
                        AccountId.Name = b.Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].name;
                    }
                    var newRecord = b.gen_H_createEntitySync({ddsm_ProjectId: ProjectId, ddsm_SiteId: SiteId, ddsm_AccountId: AccountId, ddsm_name: n.name, ddsm_Title: n.name, ddsm_Status: {Value: 962080000}, ddsm_ModifiedSystem: {Value: 962080000}, ddsm_ModifiedByName: b.Xrm.Page.context.getUserName()},"ddsm_uploadeddocumensSet");
                    var u = t.target.result, s = u.indexOf("base64,") + 7, r = {}, obj, f = "ddsm_uploadeddocumens";
                    r.LogicalName = f; r.Id = newRecord.ddsm_uploadeddocumensId; r.Name = newRecord.ddsm_name, obj = { ObjectTypeCode: f, ObjectId: r, IsDocument: !0, FileName: n.name, DocumentBody: u.toString().substring(s) };
                    CrmRestKit.Create("Annotation", obj, !1).fail(onRestError).done(function (data, status, xhr) {
                        checkDocNameConvention(data);
                        filesCount--;
                        if (filesCount == 0) {
                            b.document.getElementById('WebResource_docConvention').contentWindow.reloadGrid();
                            loadData(_getODataEndpoint(entitySchemaName), true);
                            stopSpinner();
                        }
                    });
                    i.style.width = "100%";
                    i.textContent = "";
                    setTimeout("document.getElementById('progress_bar').className='';", 2e3)
                };
                t.onerror = function (n) {
                    alert("File could not be read! Code " + n.target.error.code);
                    filesCount--;
                    if (filesCount == 0) {
                        loadData(_getODataEndpoint(entitySchemaName), true);
                        stopSpinner();
                    }
                };
                t.readAsDataURL(n);
            }

            function onRestError(n) {
                var t = JSON.parse(n.responseText).error.message.value; alert("The file was unable to be uploaded. " + t);
                filesCount--;
                if (filesCount == 0) {
                    loadData(_getODataEndpoint(entitySchemaName), true);
                    stopSpinner();
                }
            }
            function onRestErrorCopy() { var t = JSON.parse(n.responseText).error.message.value; alert("The file was unable to be uploaded from the storage. " + t); stopSpinner(); }
            function onRestErrorUpdate() { var t = JSON.parse(n.responseText).error.message.value; alert("The data file cannot be changed. " + t); stopSpinner();}

            dragcontrolOnLoad();

            function getAutoEmailIncomplete() {
                var req = new XMLHttpRequest();
                var serverUrl = b.Xrm.Page.context.getClientUrl();

                try {
                    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_admindataSet?$select=ddsm_AutoEmailIncompleteDocuments&$filter=ddsm_name eq 'Admin Data'", false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.send();

                    if (req.readyState == 4 && req.status == 200) {
                        var adminData = JSON.parse(req.responseText).d;
                        if (adminData.results[0] != null) {
                            AutoEmailIncomplete = adminData.results[0].ddsm_AutoEmailIncompleteDocuments;
                        }
                    } else {
                        AutoEmailIncomplete = false;
                    }
                } catch (err) {
                    AutoEmailIncomplete = false;
                    alert("Get Auto Email Incomplete: " + err + ".");
                }

                if(!b.Xrm.Page.getAttribute("ddsm_accountid").getValue()){
                	return;
                }

                var req2 = new XMLHttpRequest();
                try {
                    req2.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/AccountSet?$select=PrimaryContactId,EMailAddress1&$filter=AccountId eq guid'" + b.Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id + "'", false);
                    req2.setRequestHeader("Accept", "application/json");
                    req2.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req2.send();

                    if (req2.readyState == 4 && req2.status == 200) {
                        var reqData = JSON.parse(req2.responseText).d;
                        if (reqData.results[0] != null) {
                            if(reqData.results[0].PrimaryContactId.Id != null) {
                                PrimaryContact = reqData.results[0].PrimaryContactId;
                            } else {PrimaryContact = null;}
                            if(reqData.results[0].EMailAddress1 != null && reqData.results[0].EMailAddress1 != '') {
                                EMailAddress = reqData.results[0].EMailAddress1;
                            } else {EMailAddress = null;}
                        }
                    } else {
                        PrimaryContact = null;
                        EMailAddress = null;
                    }
                } catch (err) {
                    PrimaryContact = null;
                    EMailAddress = null;
                    alert("Get Primary Contact: " + err + ".");
                }

            }

            getAutoEmailIncomplete();

            function CreateEmailIncomplete(DocId, DocName){
                var createEmail = new XrmServiceToolkit.Soap.BusinessEntity("email");
                var from = [{
                    id: (Xrm.Page.context.getUserId()).replace(/\{|\}/g, ''),
                    logicalName: 'systemuser',
                    type: "EntityReference"
                }];
                createEmail.attributes["from"] = {value: from, type: "EntityCollection"};
                if(EMailAddress != null){

                    var to = [{
                        id: (b.Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id).replace(/\{|\}/g, ''),
                        logicalName: 'account',
                        type: "EntityReference"
                    }];
                    createEmail.attributes["to"] = {value: to, type: "EntityCollection"};
                } else if(PrimaryContact != null) {
                    var to = [{
                        id: (PrimaryContact.Id).replace(/\{|\}/g, ''),
                        logicalName: PrimaryContact.LogicalName,
                        type: "EntityReference"
                    }];
                    createEmail.attributes["to"] = {value: to, type: "EntityCollection"};
                }
                createEmail.attributes["subject"] = "Energy Efficiency Application Incomplete - " + DocName;
                createEmail.attributes["description"] = "Your document is incomplete. Please review and re-submit."
                + "<br/><br/>"
                + "Best Regards,"
                + "<br/>"
                + Xrm.Page.context.getUserName();
                createEmail.attributes["directioncode"] = true;

                var emailId = XrmServiceToolkit.Soap.Create(createEmail);

                var req = new XMLHttpRequest();
                var serverUrl = b.Xrm.Page.context.getClientUrl();
                try {
                    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/AnnotationSet?$select=FileSize,DocumentBody,FileName,MimeType&$filter=AnnotationId eq guid'" + DocId + "'", false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.send();

                    if (req.readyState == 4 && req.status == 200) {
                        var reqData = JSON.parse(req.responseText).d;
                        if (reqData.results[0] != null) {
                            var attachment = {};
                            attachment.FileName = reqData.results[0].FileName;
                            attachment.FileSize = reqData.results[0].FileSize;
                            attachment.ObjectTypeCode = "email";
                            attachment.AttachmentNumber = 0;
                            attachment.MimeType = reqData.results[0].MimeType;
                            attachment.Body = reqData.results[0].DocumentBody;
                            attachment.ObjectId = {
                                Id: emailId,
                                LogicalName: "email"
                            };
                            CrmRestKit.Create("ActivityMimeAttachment", attachment, !1).fail(onRestErrorAttachFile).done(function (data, status, xhr) {
                                var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                                var url = "/main.aspx?etc=4202&extraqs=&histKey=" + randomnumber + "&id=" + emailId + "&newWindow=true&pagetype=entityrecord";
                                window.open(url, "_blank");

                            });
                        }
                    }
                } catch (err) {
                    alert("Get Document file: " + err + ".");
                }
            }

            function onRestErrorAttachFile(n) {
                var t = JSON.parse(n.responseText).error.message.value;
                alert("The file was unable to be attached. " + t);
                stopSpinner();
            }

            function initSpinner(txt){
                if(!txt) {
                    txt = "Please note! <br>There are background activities <br>working with the record.";
                }

                if(typeof b.CreaLab === "object") {
                    $("body", b.document).attr({"id": "bodyTop1"});
                    b.spinnerForm1 = b.CreaLab.Spinner.spin(b.document.getElementById("bodyTop1"), txt);
                } else if(typeof CreaLab === "object") {
                    $("body", document).attr({"id": "bodyGrid"});
                    spinnerForm = CreaLab.Spinner.spin(document.getElementById("bodyGrid"), txt);
                }
            }
            function stopSpinner(){
                if(typeof b.CreaLab === "object") {
                    b.spinnerForm1.stop();
                } else if(typeof CreaLab === "object") {
                    spinnerForm.stop();
                }
            }

        });

        // Refresh Grid
        reloadGrid = function () {
            loadData(_getODataEndpoint(entitySchemaName), true);
        }
        get_dataOkgrid = function () {
            return dataOk;
        }
        get_saveGridStatus = function () {
            return saveGridStatus;
        }
        refreshGrid = function () {
            Ext.getCmp(idGrid).getView().refresh();
        }
        $(b).resize(function () {
            Ext.getCmp(idGrid).getView().refresh();
        });
    });
}
