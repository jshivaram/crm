function checkUserPermissions()
{
	try
	{
		if (!Xrm.Page.getAttribute("ddsm_enddate") || Xrm.Page.getAttribute("ddsm_enddate").getValue() !== null)
		{
			return false;
		}
		
		window.$.ajax(
		{
			async: false,
			url: Xrm.Page.context.getClientUrl() + "/WebResources/mag_/js/process.js",
			dataType: "script",
			cache: true
		});
		
		var currentRols = Xrm.Page.context.getUserRoles();
		if(!currentRols)
		{
			console.log("[On Hold] currentRols was not found");
			return;
		}
		
		if(!Process)
		{
			console.log("[On Hold] Process was not found");
			return;
		}
		var reply;
		Process.callAction("ddsm_DDSMCheckRolesForHolding", [
			{
				key: "CurrentRoles",
				type: Process.Type.String,
				value: JSON.stringify(currentRols)}],
	
			function (output)
			{
				reply = output[0].value;
			},
	
			function (error)
			{
				console.error("Error in JS code OnHold. Method: UserHavePermisions. Error: " + error);
				reply = false;
			}, false);
			
		if (reply === "true")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	catch (e)
	{
		console.error("Error in JS code OnHold. Function: UserHavePermisions. Error: " + e);
	}
}

function holdProject()
{
	try
	{
		var now = new Date();
		var month = now.getMonth() + 1;
		
		if(!Alert)
		{
			console.log("[On Hold] Alert was not found");
			return;
		}
		
		Alert.show("Place this project ON HOLD?", "Are you sure that you want to place this project ON HOLD from " + month + "/" + now.getDate() + "/" + now.getFullYear() + "?", [
            new Alert.Button("Hold", function ()
		{
			var onHoldDate = Xrm.Page.getAttribute("ddsm_onholddate");
			if (onHoldDate)
			{
				onHoldDate.setValue(now);
			}
			
			var projectStatus = Xrm.Page.getAttribute("ddsm_projectstatus");
			var projectStatusControl = Xrm.Page.getControl("ddsm_projectstatus");
			var projectStatusBeforeOnHold = Xrm.Page.getAttribute("ddsm_statusbeforeonhold");
			if(!projectStatus)
			{
				console.log("[On Hold] projectStatus was not found");
				return;
			}
			
			projectStatusControl.setDisabled(false);
			if(!projectStatusBeforeOnHold)
			{
				console.log("[On Hold] projectStatusBeforeOnHold was not found");
				return;
			}
			
			projectStatusBeforeOnHold.setValue(projectStatus.getValue());
			projectStatus.setValue("On Hold");
			
			Xrm.Page.data.save().then(
				function ()
				{
					projectStatusControl.setDisabled(true);
				},
				function ()
				{
					projectStatusControl.setDisabled(true);
					console.error("Error in JS code onHold. Function: discontinueProject. Error: Error when save.");
				});
			onLoad();
		}, true),
            new Alert.Button("Сancel")
        ], "QUESTION", 500, 200);
	}
	catch (e)
	{
		console.error("Error in JS code Dsicontinue. Function: discontinueProject. Error: " + e);
	}
}

function unhold()
{
	var projectStatus = Xrm.Page.getAttribute("ddsm_projectstatus");
	var projectStatusControl = Xrm.Page.getControl("ddsm_projectstatus");
	if(!projectStatus)
	{
		console.log("[On Hold] projectStatus was not found");
		return;
	}
	
	var projectStatusBeforeOnHold = Xrm.Page.getAttribute("ddsm_statusbeforeonhold");
	if(!projectStatusBeforeOnHold)
	{
		console.log("[On Hold] projectStatusBeforeOnHold was not found");
		return;
	}
	
	projectStatusControl.setDisabled(false);
	projectStatus.setValue(Xrm.Page.getAttribute("ddsm_statusbeforeonhold").getValue());
	Xrm.Page.getAttribute("ddsm_statusbeforeonhold").setValue("");
	
	Xrm.Page.data.save().then(
		function ()
		{
			projectStatusControl.setDisabled(true);
		},
		function ()
		{
			projectStatusControl.setDisabled(true);
			console.error("Error in JS code onHold. Function: discontinueProject. Error: Error when save.");
		});
		
	onLoad();
}

function onLoad()
{
	if(Xrm.Page.getAttribute("ddsm_projectstatus") &&
		Xrm.Page.getAttribute("ddsm_projectstatus").getValue() === "On Hold")
	{
		$("#appGridQueryFilterContainer_Measure_ImplementationSite").hide();
		$("#appGridQueryFilterContainer_SmartMeasure_view").hide();	
	}
	else
	{
		$("#appGridQueryFilterContainer_Measure_ImplementationSite").show();
		$("#appGridQueryFilterContainer_SmartMeasure_view").show();
	}
}