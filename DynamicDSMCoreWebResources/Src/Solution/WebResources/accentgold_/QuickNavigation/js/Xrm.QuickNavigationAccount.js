/* Version 2.7
 * Update 08/20/2015
 */
Xrm = window.Xrm || { __namespace: true };
Xrm.QuickNavigation = Xrm.QuickNavigation || { __namespace: true };
Xrm.QuickNavigation.ShowTab = true;
Xrm.QuickNavigation.ShowRelated = false;
Xrm.QuickNavigation.ShowTabs = [];
Xrm.QuickNavigation.Relations = [];
Xrm.QuickNavigation.RelatedLinks = [];
Xrm.QuickNavigation.TabControl = [];
Xrm.QuickNavigation.TabLinks = [];
Xrm.QuickNavigation.TabControlName = [];
Xrm.QuickNavigation.ShowRelatedInterval = null;
Xrm.QuickNavigation.ShowTabInterval = null;
Xrm.QuickNavigation.prevTab = null;
Xrm.QuickNavigation.prevRelated = null;
Xrm.QuickNavigation.clickAll = false;
Xrm.QuickNavigation.ClickDocsTab;
Xrm.QuickNavigation.countProjComp = 0;
Xrm.QuickNavigation.countProjProgress = 0;
Xrm.QuickNavigation.countProj = 0;
Xrm.QuickNavigation.countSite = 0;
Xrm.QuickNavigation.AnnualKWh = 0;
Xrm.QuickNavigation.countProjTA = 0;
Xrm.QuickNavigation.countCompTA = 0;
Xrm.QuickNavigation.countDoc = 0;
Xrm.QuickNavigation.creditrating = 0;
Xrm.QuickNavigation.countCont = 0;

Xrm.QuickNavigation.countMeas = 0;
Xrm.QuickNavigation.countMeasConv = 0;
Xrm.QuickNavigation.countMeasPre = 0;
Xrm.QuickNavigation.countMeasOpen = 0;

Xrm.QuickNavigation.DefineRecordType;

$(function () {


// Returns:
// true if Company Record Type is TA || Bus/TA
// false if Company Record Type is Bus || Blank
// 962080000 (Bus), 962080001 (Bus/TA), 962080002 (TA)
    Xrm.QuickNavigation.DefineRecordType = function () {
        var isTA = false;
        var type = window.parent.Xrm.Page.getAttribute("ddsm_recordtype").getValue();

        if ( type != null) {
            switch (type) {
                case 962080000:
                    isTA = false;
                    break;
                case 962080001:
                    isTA = true;
                    break;
                case 962080002:
                    isTA = true;
                    break;
                case 962080003:
                    isTA = true;
                    break;
            }
        }
        return isTA;
    }

    function projTab() {
        var fetch = "";
        fetch += '<fetch count="50" aggregate="true" page="1" >';
        fetch += '<entity name="ddsm_project" >';
        fetch += '<attribute name="ddsm_projectid" alias="countProjComp" aggregate="count" />';
        fetch += '<filter type="and" >';
        fetch += '<condition attribute="ddsm_projectstatus" operator="begins-with" value="7-Completed" />';
        fetch += '<condition attribute="statecode" operator="eq" value="0" />';
        fetch += '<condition attribute="ddsm_accountid" operator="eq" value="' + window.parent.Xrm.Page.data.entity.getId() +'" />';
        fetch += '</filter>';
        fetch += '</entity>';
        fetch += '</fetch>';

        var fetchData = XrmServiceToolkit.Soap.Fetch(fetch);
        if(fetchData!=null)
        {
            Xrm.QuickNavigation.countProjComp = (!!fetchData[0].attributes.countProjComp.value) ? fetchData[0].attributes.countProjComp.value : 0;
        }

        fetch = "";
        fetch += '<fetch count="50" aggregate="true" page="1" >';
        fetch += '<entity name="ddsm_project" >';
        fetch += '<attribute name="ddsm_projectid" alias="countProj" aggregate="count" />';
        fetch += '<filter type="and" >';
        fetch += '<condition attribute="statecode" operator="eq" value="0" />';
        fetch += '<condition attribute="ddsm_accountid" operator="eq" value="' + window.parent.Xrm.Page.data.entity.getId() +'" />';
        fetch += '</filter>';
        fetch += '</entity>';
        fetch += '</fetch>';

        fetchData = XrmServiceToolkit.Soap.Fetch(fetch);
        if(fetchData!=null)
        {
            Xrm.QuickNavigation.countProj = (!!fetchData[0].attributes.countProj.value) ? fetchData[0].attributes.countProj.value : 0;
        }

        Xrm.QuickNavigation.countProjProgress = Xrm.QuickNavigation.countProj - Xrm.QuickNavigation.countProjComp;
    }

    function siteTab() {
        var fetch = "";
        fetch += '<fetch count="50" aggregate="true" page="1" >';
        fetch += '<entity name="ddsm_site" >';
        fetch += '<attribute name="ddsm_siteid" alias="countSite" aggregate="count" />';
        fetch += '<attribute name="ddsm_annualkwhusage" alias="annualkwh" aggregate="sum" />';
        fetch += '<filter type="and" >';
        fetch += '<condition attribute="statecode" operator="eq" value="0" />';
        fetch += '<condition attribute="ddsm_parentaccount" operator="eq" value="' + window.parent.Xrm.Page.data.entity.getId() +'" />';
        fetch += '</filter>';
        fetch += '</entity>';
        fetch += '</fetch>';

        var fetchData = XrmServiceToolkit.Soap.Fetch(fetch);
        if(fetchData!=null)
        {
            Xrm.QuickNavigation.countSite = (!!fetchData[0].attributes.countSite.value) ? fetchData[0].attributes.countSite.value : 0;
            //Xrm.QuickNavigation.AnnualKWh = (!!fetchData[0].attributes.annualkwh.value) ? fetchData[0].attributes.annualkwh.value : 0;
        }
    }
    function ann_kwh() {
        var fetch = "";
        fetch += '<fetch count="50" aggregate="true" page="1" >';
        fetch += '<entity name="account" >';
        fetch += '<attribute name="ddsm_annualkwh" alias="annualkwh" aggregate="sum" />';
        fetch += '<filter type="and" >';
        fetch += '<condition attribute="statecode" operator="eq" value="0" />';
        fetch += '<condition attribute="accountid" operator="eq" value="' + window.parent.Xrm.Page.data.entity.getId() +'" />';
        fetch += '</filter>';
        fetch += '</entity>';
        fetch += '</fetch>';

        var fetchData = XrmServiceToolkit.Soap.Fetch(fetch);
        if(fetchData!=null)
        {
            Xrm.QuickNavigation.AnnualKWh = (!!fetchData[0].attributes.annualkwh.value) ? fetchData[0].attributes.annualkwh.value : 0;
        }
    }
    function taTab() {
        var fetch = "";
        fetch += '<fetch count="50" aggregate="true" page="1" >';
        fetch += '<entity name="ddsm_project" >';
        fetch += '<attribute name="ddsm_tradeally" alias="countProjTA" aggregate="count" />';
        fetch += '<filter type="and" >';
        fetch += '<condition attribute="statecode" operator="eq" value="0" />';
        fetch += '<condition attribute="ddsm_tradeally" operator="eq" value="' + window.parent.Xrm.Page.data.entity.getId() +'" />';
        fetch += '</filter>';
        fetch += '</entity>';
        fetch += '</fetch>';

        var fetchData = XrmServiceToolkit.Soap.Fetch(fetch);
        if(fetchData!=null)
        {
            Xrm.QuickNavigation.countProjTA = (!!fetchData[0].attributes.countProjTA.value) ? fetchData[0].attributes.countProjTA.value : 0;
        }

        fetch = "";
        fetch += '<fetch>';
        fetch += '<entity name="ddsm_project" >';
        fetch += '<attribute name="ddsm_accountid" alias="countCompTA" />';
        fetch += '<filter type="and" >';
        fetch += '<condition attribute="statecode" operator="eq" value="0" />';
        fetch += '<condition attribute="ddsm_tradeally" operator="eq" value="' + window.parent.Xrm.Page.data.entity.getId() +'" />';
        fetch += '</filter>';
        fetch += '</entity>';
        fetch += '</fetch>';

        fetchData = XrmServiceToolkit.Soap.Fetch(fetch);

        if(fetchData!=null)
        {

            if(typeof fetchData.length > 0) {
                var tmpArr = [];
                tmpArr.push(fetchData[0].attributes.countCompTA.name);

                for (var i = 1; i < fetchData.length; i++) {
                    var existFlag = false;
                    for (var j = 0; j < tmpArr.length; j++) {
                        if (fetchData[i].attributes.countCompTA.name == tmpArr[j]) {
                            existFlag = true;
                            break;
                        }
                    }
                    if (!existFlag) {
                        tmpArr.push(fetchData[i].attributes.countCompTA.name);
                    }
                }
                Xrm.QuickNavigation.countCompTA = tmpArr.length;
            }
        }

    }

    function docTab() {
        var fetch = "";
        fetch += '<fetch count="50" aggregate="true" page="1" >';
        fetch += '<entity name="ddsm_uploadeddocumens" >';
        fetch += '<attribute name="ddsm_accountid" alias="countDoc" aggregate="count" />';
        fetch += '<filter type="and" >';
        fetch += '<condition attribute="ddsm_accountid" operator="eq" value="' + window.parent.Xrm.Page.data.entity.getId() +'" />';
        fetch += '</filter>';
        fetch += '</entity>';
        fetch += '</fetch>';

        var fetchData = XrmServiceToolkit.Soap.Fetch(fetch);
        if(fetchData!=null)
        {
            Xrm.QuickNavigation.countDoc = (!!fetchData[0].attributes.countDoc.value) ? fetchData[0].attributes.countDoc.value : 0;
        }
    }

    function accTab() {
        var fetch = "";
        /*
         fetch += '<fetch count="50" aggregate="true" page="1" >';
         fetch += '<entity name="account" >';
         fetch += '<attribute name="ddsm_utilitycreditrating" alias="creditrating" aggregate="sum" />';
         fetch += '<filter type="and" >';
         fetch += '<condition attribute="accountid" operator="eq" value="' + window.parent.Xrm.Page.data.entity.getId() +'" />';
         fetch += '</filter>';
         fetch += '</entity>';
         fetch += '</fetch>';

         var fetchData = XrmServiceToolkit.Soap.Fetch(fetch);
         if(fetchData!=null)
         {
         Xrm.QuickNavigation.creditrating = (!!fetchData[0].attributes.creditrating.value) ? fetchData[0].attributes.creditrating.value : 0;
         }
         */
        fetch = "";
        fetch += '<fetch count="50" aggregate="true" page="1" >';
        fetch += '<entity name="contact" >';
        fetch += '<attribute name="contactid" alias="countCont" aggregate="count" />';
        fetch += '<filter type="and" >';
        fetch += '<condition attribute="statecode" operator="eq" value="0" />';
        fetch += '<condition attribute="accountid" operator="eq" value="' + window.parent.Xrm.Page.data.entity.getId() +'" />';
        fetch += '</filter>';
        fetch += '</entity>';
        fetch += '</fetch>';

        var fetchData = XrmServiceToolkit.Soap.Fetch(fetch);
        if(fetchData!=null)
        {
            Xrm.QuickNavigation.countCont = (!!fetchData[0].attributes.countCont.value) ? fetchData[0].attributes.countCont.value : 0;
        }
    }

    function opportTab() {
        var fetch = "";
        fetch += '<fetch count="50" aggregate="true" page="1" >';
        fetch += '<entity name="ddsm_sitemeasure" >';
        fetch += '<attribute name="ddsm_measurestatus" alias="countMeas" aggregate="count" />';
        fetch += '<filter type="and" >';
        fetch += '<condition attribute="statecode" operator="eq" value="0" />';
        fetch += '<condition attribute="ddsm_accountid" operator="eq" value="' + window.parent.Xrm.Page.data.entity.getId() +'" />';
        fetch += '</filter>';
        fetch += '</entity>';
        fetch += '</fetch>';

        var fetchData = XrmServiceToolkit.Soap.Fetch(fetch);
        if(fetchData!=null)
        {
            Xrm.QuickNavigation.countMeas = (!!fetchData[0].attributes.countMeas.value) ? fetchData[0].attributes.countMeas.value : 0;
        }

        fetch = "";
        fetch += '<fetch count="50" aggregate="true" page="1" >';
        fetch += '<entity name="ddsm_sitemeasure" >';
        fetch += '<attribute name="ddsm_measurestatus" alias="countMeasConv" aggregate="count" />';
        fetch += '<filter type="and" >';
        fetch += '<condition attribute="statecode" operator="eq" value="0" />';
        fetch += '<condition attribute="ddsm_accountid" operator="eq" value="' + window.parent.Xrm.Page.data.entity.getId() +'" />';
        fetch += '<condition attribute="ddsm_measurestatus" operator="eq" value="962080001" />';
        fetch += '</filter>';
        fetch += '</entity>';
        fetch += '</fetch>';

        fetchData = XrmServiceToolkit.Soap.Fetch(fetch);
        if(fetchData!=null)
        {
            Xrm.QuickNavigation.countMeasConv = (!!fetchData[0].attributes.countMeasConv.value) ? fetchData[0].attributes.countMeasConv.value : 0;
        }

        fetch = "";
        fetch += '<fetch count="50" aggregate="true" page="1" >';
        fetch += '<entity name="ddsm_sitemeasure" >';
        fetch += '<attribute name="ddsm_measurestatus" alias="countMeasPre" aggregate="count" />';
        fetch += '<filter type="and" >';
        fetch += '<condition attribute="statecode" operator="eq" value="0" />';
        fetch += '<condition attribute="ddsm_accountid" operator="eq" value="' + window.parent.Xrm.Page.data.entity.getId() +'" />';
        fetch += '<condition attribute="ddsm_measurestatus" operator="eq" value="962080003" />';
        fetch += '</filter>';
        fetch += '</entity>';
        fetch += '</fetch>';

        fetchData = XrmServiceToolkit.Soap.Fetch(fetch);
        if(fetchData!=null)
        {
            Xrm.QuickNavigation.countMeasPre = (!!fetchData[0].attributes.countMeasPre.value) ? fetchData[0].attributes.countMeasPre.value : 0;
        }

        Xrm.QuickNavigation.countMeasOpen = Xrm.QuickNavigation.countMeas - Xrm.QuickNavigation.countMeasConv - Xrm.QuickNavigation.countMeasPre;
    }

    projTab();
    siteTab();
    taTab();
    docTab();
    accTab();
    //opportTab();
    ann_kwh();


    Xrm.QuickNavigation.GetDataParam();


    if (Xrm.QuickNavigation.ShowTab) {
        $("#Tabs").show();
        Xrm.QuickNavigation.GetTabs();
        Xrm.QuickNavigation.ShowTabInterval = setInterval(Xrm.QuickNavigation.GetTabs, 5000);
    }
});

//Handles retrieving and displaying the links to the tabs
Xrm.QuickNavigation.GetTabs = function () {
    var tabItems = [], TabControlName = [];
    var i = 0;

    //Get the tabs
    window.parent.Xrm.Page.ui.tabs.forEach(
        function (control) {
            //Ignore if hidden
            //if (control.getVisible()) {
            //Show all tabs if no specifc tabs are specified in the configuration, otherwise only show the allowed tabs
            if (Xrm.QuickNavigation.ShowTabs.length === 0 || $.inArray(control.getName().toUpperCase(), Xrm.QuickNavigation.ShowTabs) !== -1) {
                //Tabs that are configured to not shwo the label do not return a value from getLabel() so a value is being created to show something
                TabControlName.push(control.getName());
                Xrm.QuickNavigation.TabControl[control.getName()] = control;
                if (control.getName() === "tab_ProjectInfo"){
                    tabItems.push("<li style='height: 90px;'><a href='#' id='" + control.getName() + "' class='tabLink' style='width:100px;height:90px;text-align:center;' title='" + ((control.getLabel() === "") ? "Tab Label Not Displayed" : "") + "'>" +
                    ((control.getLabel() !== "") ? control.getLabel() : ("Tab " + i)) + "<span class='big_text' id='big_" + control.getName() +"'>" + Xrm.QuickNavigation.countProj +"</span><span class='small_text' id='small_" + control.getName() +"'>In Progress: " + Xrm.QuickNavigation.countProjProgress +"<br/>Completed: " + Xrm.QuickNavigation.countProjComp + "</span></a></li>");
                } else if (control.getName() === "tab_MultiSites"){
                    tabItems.push("<li style='height: 90px;'><a href='#' id='" + control.getName() + "' class='tabLink' style='width:100px;height:90px;text-align:center;' title='" + ((control.getLabel() === "") ? "Tab Label Not Displayed" : "") + "'>" +
                    ((control.getLabel() !== "") ? control.getLabel() : ("Tab " + i)) + "<span class='big_text' id='big_" + control.getName() +"'>" + Xrm.QuickNavigation.countSite +"</span><span class='small_text' id='small_" + control.getName() +"'>Annual kWh: " + Xrm.QuickNavigation.AnnualKWh + "</span></a></li>");
                } else if (control.getName() === "tab_TradeAlly"){
                    tabItems.push("<li style='height: 90px;'><a href='#' id='" + control.getName() + "' class='tabLink' style='width:100px;height:90px;text-align:center;' title='" + ((control.getLabel() === "") ? "Tab Label Not Displayed" : "") + "'>" +
                    ((control.getLabel() !== "") ? control.getLabel() : ("Tab " + i)) + "<span class='big_text' id='big_" + control.getName() +"'>&nbsp;" +"</span><span class='small_text' id='small_" + control.getName() +"'>CA Projects: "+ Xrm.QuickNavigation.countProjTA + "<br/>CA Companies: " + Xrm.QuickNavigation.countCompTA + "</span></a></li>");
                } else if (control.getName() === "tab_NotesActivities"){
                    tabItems.push("<li style='height: 90px;'><a href='#' id='" + control.getName() + "' class='tabLink' style='width:100px;height:90px;text-align:center;' title='" + ((control.getLabel() === "") ? "Tab Label Not Displayed" : "") + "'>" +
                    ((control.getLabel() !== "") ? control.getLabel() : ("Tab " + i)) + "<span class='big_text' id='big_" + control.getName() +"'>" + Xrm.QuickNavigation.countDoc +"<b style='font-weight: 400;'></b></span><span class='small_text' id='small_" + control.getName() +"'>" + "</span></a></li>");
                } else if (control.getName() === "tab_CompanyOverview"){
                    tabItems.push("<li style='height: 90px;'><a href='#' id='" + control.getName() + "' class='tabLink' style='width:100px;height:90px;text-align:center;' title='" + ((control.getLabel() === "") ? "Tab Label Not Displayed" : "") + "'>" +
                    ((control.getLabel() !== "") ? control.getLabel() : ("Tab " + i)) + "<span class='big_text' id='big_" + control.getName() +"'>&nbsp;" + "</span><span class='small_text' id='small_" + control.getName() +"'>Contacts: " + Xrm.QuickNavigation.countCont + "</span></a></li>");
                } else if (control.getName() === "tab_SitesContacts"){
                    tabItems.push("<li style='height: 90px;'><a href='#' id='" + control.getName() + "' class='tabLink' style='width:100px;height:90px;text-align:center;' title='" + ((control.getLabel() === "") ? "Tab Label Not Displayed" : "") + "'>" +
                    ((control.getLabel() !== "") ? control.getLabel() : ("Tab " + i)) + "<span class='big_text' id='big_" + control.getName() +"'>" + Xrm.QuickNavigation.countMeas + "</span><span class='small_text' id='small_" + control.getName() +"'>Open: " + Xrm.QuickNavigation.countMeasOpen + "<br/>Converted: " + Xrm.QuickNavigation.countMeasConv + "<br/>Pre-Existing: " + Xrm.QuickNavigation.countMeasPre + "</span></a></li>");
                } else if(control.getName() !== "Tab_MeasureSummary"){
                    tabItems.push("<li style='height: 90px;'><a href='#' id='" + control.getName() + "' class='tabLink' style='width:100px;height:90px;text-align:center;' title='" + ((control.getLabel() === "") ? "Tab Label Not Displayed" : "") + "'>" +
                    ((control.getLabel() !== "") ? control.getLabel() : ("Tab " + i)) + "<span class='big_text' id='big_" + control.getName() +"'></span><span class='small_text' id='small_" + control.getName() +"'></span></a></li>");
                }
            }
            //}
        }
    );
    spinnerForm.stop();
    //tabItems.push("<li style='padding-left: 20px;'><a href='#' id='showAllTabs' class='tabShowAll' title='Show All Tabs'>Show All</a></li>");
    //tabItems.push("<li><a href='#' id='expandedAll' class='tabExpandedAll' title='Show All Tabs'>Expand All</a></li>");
    //tabItems.push("<li><a href='#' id='collapsedAll' class='tabCollapsedAll' title='Show All Tabs'>Collapse All</a></li>");
    Xrm.QuickNavigation.TabControlName = TabControlName;
    //If there is a difference (tab was shown or hidden through another mechanism) rebuild the page content
    if (tabItems.join("") !== Xrm.QuickNavigation.TabLinks.join("")) {
        $("#Tabs").html(tabItems.join(""));
        Xrm.QuickNavigation.TabLinks = tabItems;
    }

    //Xrm.QuickNavigation.prevTab = $("a#" + TabControlName[0]);
    //$(Xrm.QuickNavigation.prevTab).addClass("Active");

    var widthConteyner = 0;
    $("#Tabs").find("LI").each(function(){
        widthConteyner += 10 + $(this).width();
    });

    $(".tabLink").click(function (e) {
        e.preventDefault();
        clearInterval(Xrm.QuickNavigation.ShowTabInterval);
        for(i = 0; i < Xrm.QuickNavigation.TabControlName.length; i++) {
            Xrm.QuickNavigation.TabControl[Xrm.QuickNavigation.TabControlName[i]].setDisplayState('collapsed');
        }
        Xrm.QuickNavigation.prevTab = this;
        Xrm.QuickNavigation.TabControl[$(Xrm.QuickNavigation.prevTab).attr("id")].setDisplayState('expanded');

        // Account Form
        if($(Xrm.QuickNavigation.prevTab).attr("id") === "tab_SitesContacts"){
            if(typeof window.parent.document.getElementById('WebResource_measure_grid_site').contentWindow.refreshGrid == 'function') {
                window.parent.document.getElementById('WebResource_measure_grid_site').contentWindow.refreshGrid();
            }

        }
        if($(Xrm.QuickNavigation.prevTab).attr("id") === "tab_NotesActivities"){
            if(typeof window.parent.document.getElementById('WebResource_uploaddocs').contentWindow.refreshGrid == 'function') {
                window.parent.document.getElementById('WebResource_uploaddocs').contentWindow.refreshGrid();
            }
        }

        return false;
    });

};

Xrm.QuickNavigation.GetDataParam = function () {
    var vals = [];
    if (location.search != "") {
        vals = location.search.substr(1).split("&");
        for (var i = 0; i < vals.length; i++) {
            vals[i] = vals[i].replace(/\+/g, " ").split("=");
        }
        for (var j = 0; j < vals.length; j++) {
            if (vals[j][0].toLowerCase() == "data") {
                Xrm.QuickNavigation.ParseData(vals[j][1]);
                break;
            }
        }
    }
};

//Parse the various parts of the incoming data parameter
//ShowTab=true
//ShowRelated=true
//Tabs=tabName1,tabName2
//Relations=navId1,navId2
Xrm.QuickNavigation.ParseData = function (data) {
    var vals = [];
    vals = decodeURIComponent(data).split("&");
    for (var i = 0; i < vals.length; i++) {
        vals[i] = vals[i].replace(/\+/g, " ").split("=");
    }

    for (var x = 0; x < vals.length; x++) {
        switch (vals[x][0].toUpperCase()) {
            case "SHOWTAB":
                Xrm.QuickNavigation.ShowTab = (vals[x][1].toUpperCase() === "TRUE") ? true : false;
                break;
            case "SHOWRELATED":
                Xrm.QuickNavigation.ShowRelated = (vals[x][1].toUpperCase() === "TRUE") ? true : false;
                break;
            case "TABS":
                Xrm.QuickNavigation.ShowTabs = vals[x][1].toUpperCase().split(",");
                if(window.parent.Xrm.Page.data.entity.getEntityName() === "account"){
                    if(!Xrm.QuickNavigation.DefineRecordType()){
                        Xrm.QuickNavigation.ShowTabs.splice( Xrm.QuickNavigation.ShowTabs.indexOf( "TAB_TRADEALLY" ), 1 );
                    }
                }
                break;
            case "RELATIONS":
                Xrm.QuickNavigation.Relations = vals[x][1].toUpperCase().split(",");
                break;
        }
    }
};


