function RecalcRateClass (ids, ref) {
    //console.dir(ids);
    //console.dir(ref);
    //console.dir(typeof ref);
    if(typeof ref != "undefined" && Array.isArray(ref) && ref.length > 0) {
        //debugger;
        InitRecalcRateClass(ref)
    } else {
        if(!!Xrm && !!Xrm.Page && !!Xrm.Page.data && !!Xrm.Page.data.entity) {
            var obj = {}, arrObjs = [];
            obj.Id = Xrm.Page.data.entity.getId();
            obj.TypeName = Xrm.Page.data.entity.getEntityName();
            arrObjs.push(obj);
            debugger;
            InitRecalcRateClass(arrObjs);
        }
    }
}

function InitRecalcRateClass(ref) {
    //console.log(">>> InitRecalcRateClass");
    //console.dir(ref);
    debugger;
    if(ref.length > 0) {
        showSpinner(true, null, "RecalcRateClass");
        RunProcessRecalcRateClass(ref);
    } else {
        showSpinner(false);
    }
}

function RunProcessRecalcRateClass(ref) {
    //debugger;
    var ref = ref;
    var refObj = ref[0];
    ref.splice(0, 1);

    var params = [{
        key: "Target",
        type: Process.Type.EntityReference,
        value: {
            id: refObj.Id.replace(/[{}]/g, ''),
            entityType: refObj.TypeName
        }
    }];

    Process.callAction("ddsm_RecalculateRateClassAllocation", params,
        function(params) {
            //console.dir(params);
            //debugger;
            var rs = JSON.parse(params[0].value);
            for(key in rs[0]) {
                console.log(key + ": " + rs[0][key]);
            }

            InitRecalcRateClass(ref);

            if(!!successCallback && typeof successCallback == "function") {
                successCallback();
            }

        },
        function(e) {
            console.log(e);
            //debugger;
            InitRecalcRateClass(ref);

            if(!!errorCallback && typeof errorCallback == "function") {
                errorCallback(e);
            }
        }
    );


}