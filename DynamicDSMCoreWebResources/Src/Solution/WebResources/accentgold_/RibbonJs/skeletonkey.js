var isFormSkeletonBtn = false;

function onLoadSkeletonKeyLib(entityTypeName, entityIds) {
    var entityId = null;

    if(!entityTypeName) entityTypeName = GlobalJs.crmWindow().Xrm.Page.data.entity.getEntityName();
    if(!!entityIds && isArray(entityIds) && entityIds.length == 1)
        entityId = entityIds[0];
    else
        entityId = GlobalJs.crmWindow().Xrm.Page.data.entity.getId();

    console.dir(entityTypeName);
    console.dir(entityId);

    if (typeof (GlobalJs.crmWindow().XrmServiceToolkit) == "undefined"){
        GlobalJs.crmWindow().$.ajax({
            url: AGS.getServerUrl() + "/WebResources/accentgold_/EditableGrid/js/XrmServiceToolkit.min.js",
            dataType: "script",
            cache: true
        }).done(function () {
                if(isFormSkeletonBtn)
                    ShowSkeletonKey();
                else
                    ShowSkeletonKeyDialog(entityTypeName, entityId);
        });
    } else {
        if(isFormSkeletonBtn)
            ShowSkeletonKey();
        else
            ShowSkeletonKeyDialog(entityTypeName, entityId);
    }

}

function ShowSkeletonKey() {
    if(!!GlobalJs.crmWindow().Xrm.Page.ui.controls.get("ddsm_skeletonkey"))
    {
        GlobalJs.crmWindow().Xrm.Page.ui.controls.get("ddsm_skeletonkey").setVisible(true);
        FieldFormToButtonSKey('ddsm_skeletonkey', 'Unlock Form', 200, showSkeletonKeyDialog, 'Unlock Form');
    }
}

function ShowSkeletonKeyDialog(entityTypeName, entityId) {

    var formURI = AGS.getServerUrl() + '/WebResources/ddsm_SkeletonKeyDialog.html';
    var DialogOptions = new Xrm.DialogOptions();
    DialogOptions.width = 550;
    DialogOptions.height = 200;
    GlobalJs.crmWindow().Xrm.Internal.openDialog(formURI, DialogOptions, null, null, returnDialogResponse);
    function returnDialogResponse(response) {
        if (response) {
            console.log(response);
            if (response.hasOwnProperty('reasonInput')) {
                var objSkeletonKey = {};
                objSkeletonKey.ddsm_SkeletonKeyUnlock = true;
                if (response.hasOwnProperty('reasonType'))
                    objSkeletonKey.ddsm_DisableRecalculation = response.reasonType;
                else
                    objSkeletonKey.ddsm_DisableRecalculation = false;

                CreateSkeletonKeyLog(entityId, entityTypeName, objSkeletonKey.ddsm_DisableRecalculation, response.reasonInput);

                AGS.REST.updateRecord(entityId, objSkeletonKey, entityTypeName,
                    function(data){
                        AGS.Form.disableFormFields(false);
                    },
                    function(msg) {
                        alert(msg);
                    },
                    true
                );
            }

        }
    }
}

function CreateSkeletonKeyLog(entityId, entityTypeName, reasonType, reason)
{

    let params = [{
            key: "RecordId",
            type: Process.Type.String,
            value: entityId.replace(/([{}])+/g,'')
        },
        {
            key: "RecordTypeName",
            type: Process.Type.String,
            value: entityTypeName
        },
        {
            key: "RecordName",
            type: Process.Type.String,
            value: GlobalJs.crmWindow().Xrm.Page.getAttribute("ddsm_name").getValue()
        },
        {
            key: "Reason",
            type: Process.Type.String,
            value: reason
        },
        {
            key: "DisableRecalculation",
            type: Process.Type.Bool,
            value: reasonType
        }];

    console.dir(params);
    Process.callAction("ddsm_CreateSkeletonKeyReason", params,
        function(params) {
            let _Comlete = false;
            debugger;
            console.dir(params);
            for (let i = 0; i < params.length; i++) {
                if (params[i].key == "Complete") _Comlete = params[i].value;
            }

            if (_Comlete !="false")
            {
                return true;
            }
            else
            {
                return false;
            }
        },
        function(e) {
            console.log("error: " + e);
            return false;
        }
    );
}

function FieldFormToButtonSKey(fieldname, buttontext, buttonwidth, clickevent, title) {
    //check if object exists; else return
    if ($("#" + fieldname)[0] == null) { return; }
    var functiontocall = clickevent;
    $("#" + fieldname)[0].textContent = buttontext;
    $("#" + fieldname)[0].setAttribute("readonly", true);
    $("#" + fieldname).css({
        "borderRight": "#3366cc 1px solid", "paddingRight": "5px", "borderTop": "#3366cc 1px solid",
        "paddingLeft": "5px", "fontSize": "11px", "backgroundImage": "url(/_imgs/btn_rest.gif)",
        "borderLeft": "#3366cc 1px solid", "width": buttonwidth, "cursor": "hand",
        "lineHeight": "18px", "borderBottom": "#3366cc 1px solid", "backgroundRepeat": "repeat-x",
        "fontFamily": "Tahoma", "height": "20px", "backgroundColor": "#cee7ff", "textAlign": "center",
        "overflow": "hidden"
    });
    if (window.attachEvent) {
        $("#" + fieldname)[0].attachEvent("onmousedown", MouseDown);
        $("#" + fieldname)[0].attachEvent("onmouseup", MouseUp);
        $("#" + fieldname)[0].attachEvent("onclick", functiontocall);
    } else {
        $("#" + fieldname)[0].addEventListener("mousedown", MouseDown, false);
        $("#" + fieldname)[0].addEventListener("mouseup", MouseUp, false);
        $("#" + fieldname)[0].addEventListener("click", functiontocall, false);
    }
    $("#" + fieldname)[0].title = title;
    GlobalJs.crmWindow().focus();

    function MouseDown(evt) {
        var eventer;
        if (evt.srcElement) {
            eventer = evt.srcElement;   // event.srcElement in IE
        } else {
            eventer = evt.target;   // event.target in most other browsers
        }
        $("#" + eventer).css({
            "borderWidth": "2px", "borderStyle": "groove ridge ridge groove",
            "borderColor": "#3366cc #4080f0 #4080f0 #3366cc"
        });
    }

    function MouseUp(evt) {
        var eventer;
        if (evt.srcElement) {
            eventer = evt.srcElement;
        } else {
            eventer = evt.target;
        }
        $("#" + eventer).css({ "border": "1px solid #3366cc" });
    }
}
