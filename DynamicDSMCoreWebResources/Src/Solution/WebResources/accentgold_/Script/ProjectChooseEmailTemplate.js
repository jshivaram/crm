var configRecordName = "Admin Data";
var baseUrl = "/api/data/v8.1/";
var Xrm = window.parent.Xrm;
var clientUrl = Xrm.Page.context.getClientUrl();

$(document).ready(function() {
    // kendo.ui.progress($("#main"), true);
    $("#templates").kendoDropDownList({
        //dataTextField: "Label",
        //dataValueField: "Label",
         optionLabel: '--',
        dataSource: getEmailTemplate(),
        autoBind: false,
        select: function(e) {
      // debugger;
            var dataItem = this.dataItem(e.item);
            window.parent.selectedEmailTemplate = dataItem;
        },
        dataBound: function(e) {
            $("#templates").data("kendoDropDownList").value("");
            //  kendo.ui.progress($("#main"), false);
        },
    });

    function getEmailTemplate() {
        var url = clientUrl + baseUrl + "ddsm_admindatas?$select=ddsm_projectemailtemplates&$filter=ddsm_name eq '" + configRecordName + "'";
        var ds2 = new kendo.data.DataSource({
            schema: {
                data: function(response) {
                    debugger;
                    if (response.value.length > 0) {
                        // create new mapping if not existing
                        var value = response.value[0];
                        var json = value["ddsm_projectemailtemplates"];
                        if (json) {
                            var listOfEmailTpl = json.split(',');
                            if (listOfEmailTpl.length === 0) {
                                window.parent.Alert.show("Project email templates are not configured.", "Use Admin Data Record for configuring. And try again.", null, "ERROR", 500, 200);
                                return;
                            }
                            return listOfEmailTpl;
                        } else {
                            window.parent.Alert.show("Project email templates are not configured.", "Use Admin Data Record for configuring. And try again.", null, "ERROR", 500, 200);
                        }
                    } else {
                        // console.error("No MAD Mapping data found. please setting up ESP and try again.");
                        window.parent.Alert.show("Project email templates are not configured.", "Use Admin Data Record for configuring. And try again.", null, "ERROR", 500, 200);
                    }
                }
            },
            transport: KendoHelper.CRM.transport(url),
            pageSize: 100
        });
        return ds2;
    }
});