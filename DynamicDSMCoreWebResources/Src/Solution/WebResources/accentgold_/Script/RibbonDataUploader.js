// Data Uploader feature
// on New Data button click
function openDataUploadForm() {
    openWebResourcePage('DataUpload.html');
}
// on Upload New Configuration button click
function openDataUploadConfigForm() {
    openWebResourcePage('DataUploadConfigurations.html');
}


// Open HTML WebResources in the new window
// openWebResourcePage("example.html")
function openWebResourcePage(pageName) {
    var ctxt;
    if (typeof GetGlobalContext != "undefined") {
        ctxt = GetGlobalContext();
    }
    else {
        if (typeof Xrm != "undefined") {
            ctxt = Xrm.Page.context;
        }
    }
    var sUrl = ctxt.getClientUrl();
    if (sUrl.match(/\/$/)) {
        sUrl = sUrl.substring(0, sUrl.length - 1);
    }
    var c_url = sUrl + '/WebResources/accentgold_/' + pageName;
    var w = 1036, h = 600;
    if (typeof (openStdWin) == "undefined") {
        return window.open(c_url, '', "width=" + w + ",height=" + h + "," + "resizable=1");
    } else {
        Xrm.Utility.openWebResource('accentgold_/' + pageName, null, w, h);
    }

}
