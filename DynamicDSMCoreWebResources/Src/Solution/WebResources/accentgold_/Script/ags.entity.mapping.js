/**
 * Created by Sergey  Dergunov (sergey.dergunov@accentgold.com) on 01/21/2016.
 * Last Update
 *  on 01/29/2016.
 *
 * Version: 0.1.03.18016
 */
/// <reference path="./ags.core.js" />
window.$.ajax({
    url: "/WebResources/accentgold_/SDK/SDK.MetaData.js",
    dataType: "script",
    cache: true
}).done(function () {
    //console.log("SDK.MetaData.js");
    newNamespace();
});

function newNamespace() {
    AccentGold = window.AccentGold;
    if (typeof (AccentGold) == "undefined"){
        setTimeout(newNamespace(), 100);
    } else {
        AccentGold.Entity.namespace("Mapping").extend((function () {

            var createEntitiesOptionSet = function (crmEntityFieldName, crmAttributeFieldName) {
                    var entities = AccentGold.Entity.getEntitiesList(), entitiesList = [], crmEntityFieldName = crmEntityFieldName, crmAttributeFieldName = crmAttributeFieldName;
                    /*
                    for (var i = 0; i < entities.length; i++) {
                        var obj = {};
                        obj.name = (!!entities[i].DisplayName.UserLocalizedLabel.Label) ? entities[i].DisplayName.UserLocalizedLabel.Label : entities[i].LogicalName;
                        //obj.value = entities[i].SchemaName;
                        obj.value = entities[i].LogicalName;

                        entitiesList.push(obj);


                         obj.DisplayName = entities[i].DisplayName.UserLocalizedLabel != null ? entities[i].DisplayName.UserLocalizedLabel.Label : "";
                         obj.DisplayCollectionName = entities[i].DisplayCollectionName.UserLocalizedLabel != null ? entities[i].DisplayCollectionName.UserLocalizedLabel.Label : "";
                         obj.IconLargeName = entities[i].IconLargeName || entities[i].IconMediumName;
                         obj.IconSmallName = entities[i].IconSmallName;
                         obj.Etc = entities[i].ObjectTypeCode;
                         obj.Id = entities[i].LogicalName;
                         obj.SchemaName = entities[i].SchemaName;
                         obj.PrimaryIdAttribute = entities[i].PrimaryIdAttribute;
                         obj.PrimaryImageAttribute = entities[i].PrimaryImageAttribute;
                         obj.PrimaryNameAttribute = entities[i].PrimaryNameAttribute;
                         obj.IsValidForAdvancedFind = entities[i].IsValidForAdvancedFind;


                    }
                    */
                    var EntityNameList = $("<select>").attr("id", "Select_" + crmEntityFieldName[0]).attr("name", "Select_" + crmEntityFieldName[0]).attr("class", "ms-crm-SelectBox ms-crm-Inline-OptionSet-AutoOpen");
                    EntityNameList.append($('<option>', {
                        value: "",
                        text: ""
                    }));
                    for (var i = 0; i < entities.length; i++) {
                        EntityNameList.append($('<option>', {
                            value: entities[i].SchemaName,
                            text: (!!entities[i].DisplayName.UserLocalizedLabel.Label) ? entities[i].DisplayName.UserLocalizedLabel.Label : entities[i].SchemaName
                        }));
                    }
                    EntityNameList.change(function () {
                        Xrm.Page.getAttribute(crmEntityFieldName[0]).setValue($(this)[0][$(this)[0].selectedIndex].text);
                        Xrm.Page.getAttribute(crmEntityFieldName[1]).setValue($(this).val());
                        $("#Select_" + crmAttributeFieldName[0]).remove();
                        createAttributesOptionSet($(this).val(), crmAttributeFieldName);
                    }).trigger('change');
                    var containerName = "#" + crmEntityFieldName[0] + "_i";
                    var parentContainer = $(containerName).parent();
                    $(parentContainer).append(EntityNameList);

                },

                createAttributesOptionSet = function (entity, crmAttributeFieldFormObj) {

                    var entity = entity, attributesList = [], crmAttributeFieldFormObj = crmAttributeFieldFormObj;
                    if(entity != "") {
                        var attributes = AccentGold.Entity.getAttributesList(entity);
                        /*
                        for (var i = 0; i < attributes.length; i++) {
                            var obj = {};
                            var item = attributes[i];

                            obj.name = (!!item.DisplayName.UserLocalizedLabel) ? item.DisplayName.UserLocalizedLabel.Label : item.LogicalName;
                            obj.value = item.LogicalName;
                            obj.AttributeType = item.AttributeType;

                             obj.DisplayName = (!!item.DisplayName.UserLocalizedLabel) ? item.DisplayName.UserLocalizedLabel.Label : item.LogicalName;
                             obj.SchemaName = item.SchemaName;
                             obj.MaxLength = ((typeof item.MaxLength !== 'undefined')?item.MaxLength:null); // String, Memo (TextArea)
                             obj.Targets = ((typeof item.Targets !== 'undefined')?item.Targets:null); // Lookup

                             obj.Format = ((typeof item.Format !== 'undefined')?item.Format:null); // String (Text, TextArea, Email, Url, Pone), DateTime (DateOnly, DateAndTime)

                             obj.MaxValue = ((typeof item.MaxValue !== 'undefined')?item.MaxValue:null); // Double, Integer, Money, BigInt, Decimal
                             obj.MinValue = ((typeof item.MinValue !== 'undefined')?item.MinValue:null); // Double, Integer, Money, BigInt, Decimal
                             obj.Precision = ((typeof item.Precision !== 'undefined')?item.Precision:null); // Double, Money, Decimal

                            attributesList.push(obj);
                        }
                        */
                        console.dir(attributes);
                        var AttributeNameList = $("<select>").attr("id", "Select_" + crmAttributeFieldFormObj[0]).attr("name", "Select_" + crmAttributeFieldFormObj[0]).attr("class", "ms-crm-SelectBox ms-crm-Inline-OptionSet-AutoOpen");
                        AttributeNameList.append($('<option>', {
                            value: "",
                            text: ""
                        }));
                        for (var i = 0; i < attributes.length; i++) {
                            AttributeNameList.append($('<option>', {
                                value: (attributes[i].SchemaName + "|" + attributes[i].AttributeType),
                                text: (!!attributes[i].DisplayName.UserLocalizedLabel) ? attributes[i].DisplayName.UserLocalizedLabel.Label : attributes[i].SchemaName
                            }));
                        }
                        AttributeNameList.change(function () {
                            Xrm.Page.getAttribute(crmAttributeFieldFormObj[0]).setValue($(this)[0][$(this)[0].selectedIndex].text);
                            Xrm.Page.getAttribute(crmAttributeFieldFormObj[1]).setValue(($(this).val() != "")?($(this).val()).split("|")[0]:"");
                            Xrm.Page.getAttribute(crmAttributeFieldFormObj[2]).setValue(($(this).val() != "")?($(this).val()).split("|")[1]:"");
                        }).trigger('change');
                        var containerName = "#" + crmAttributeFieldFormObj[0] + "_i";
                        var parentContainer = $(containerName).parent();
                        $(parentContainer).append(AttributeNameList);
                    }
                };


            return {
                createEntitiesOptionSet: createEntitiesOptionSet,
                createAttributesOptionSet: createAttributesOptionSet
            };

        })());
    }
}

function onLoadForm(){
    if (Xrm.Page.ui.getFormType() == 1) {
        $("#ddsm_name_i").hide();
        $("#ddsm_primaryfieldname_i").hide();
        $("#ddsm_secondaryentityname_i").hide();
        $("#ddsm_secondaryfieldname_i").hide();
        if (typeof (AGS.Entity.Mapping) == "undefined") {
            setTimeout(onLoadForm(), 100);
        } else {
            //class ms-crm-SelectBox ms-crm-Inline-OptionSet-AutoOpen
            //ddsm_primaryentitylogicalname
            AGS.Entity.Mapping.createEntitiesOptionSet(["ddsm_name", "ddsm_primaryentitylogicalname"], ["ddsm_primaryfieldname", "ddsm_primaryfieldlogicalname", "ddsm_primaryfieldtype"]);
            AGS.Entity.Mapping.createEntitiesOptionSet(["ddsm_secondaryentityname", "ddsm_secondaryentitylogicalname"], ["ddsm_secondaryfieldname", "ddsm_secondaryfieldlogicalname", "ddsm_secondaryfieldtype"]);
        }
    } else {
        Xrm.Page.ui.controls.get("ddsm_name").setDisabled(true);
        Xrm.Page.ui.controls.get("ddsm_primaryfieldname").setDisabled(true);
        Xrm.Page.ui.controls.get("ddsm_secondaryentityname").setDisabled(true);
        Xrm.Page.ui.controls.get("ddsm_secondaryfieldname").setDisabled(true);
    }
}