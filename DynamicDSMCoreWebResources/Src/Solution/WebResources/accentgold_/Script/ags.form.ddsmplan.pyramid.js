/**
 * Created by Sergey Dergunov on 07/07/2016.
 *
 * Version: 0.1.07.0716
 */

/**
 * Call to the Form Program Interval
 * AGS.Form.DDSMPlanPyramid.calcProgramIntervals([{Id: "5f9be762-8d29-e611-80ca-323166616637", LogicalName: "ddsm_kpiinterval", Name: "June 2016"}], function(status) {alert(status);});
 * AGS.Form.DDSMPlanPyramid.calcProgramIntervals(null, function(status) {alert(status);});
 *
 * Call to the Form Program Offering
 * AGS.Form.DDSMPlanPyramid.calcProgramOfferings([{Id: "5f9be762-8d29-e611-80ca-323166616637", LogicalName: "ddsm_kpiinterval", Name: "June 2016"}], function(status) {alert(status);});
 * AGS.Form.DDSMPlanPyramid.calcProgramOfferings(null, function(status) {alert(status);});
 *
 * Call to the Form Program
 * AGS.Form.DDSMPlanPyramid.calcPrograms([{Id: "5f9be762-8d29-e611-80ca-323166616637", LogicalName: "ddsm_kpiinterval", Name: "June 2016"}], function(status) {alert(status);});
 * AGS.Form.DDSMPlanPyramid.calcPrograms(null, function(status) {alert(status);});
 *
 * Call to the Form Portfolio
 * AGS.Form.DDSMPlanPyramid.calcPortfolios([{Id: "5f9be762-8d29-e611-80ca-323166616637", LogicalName: "ddsm_kpiinterval", Name: "June 2016"}], function(status) {alert(status);});
 * AGS.Form.DDSMPlanPyramid.calcPortfolios(null, function(status) {alert(status);});
 *
 * Call to the Form DSM Plan
 * AGS.Form.DDSMPlanPyramid.calcDSMPlans([{Id: "5f9be762-8d29-e611-80ca-323166616637", LogicalName: "ddsm_kpiinterval", Name: "June 2016"}], function(status) {alert(status);});
 * AGS.Form.DDSMPlanPyramid.calcDSMPlans(null, function(status) {alert(status);});
 */

$("body").attr({"id": "bodyTop1"});
var mySpinner;

function newNamespaceDDSMPlanPyramid() {
    AccentGold = window.AccentGold;
    if (!window.AccentGold) {
        setTimeout(newNamespaceDDSMPlanPyramid, 100);
    } else {
        AccentGold.Form.namespace("DDSMPlanPyramid").extend((function () {
            var EntityName = Xrm.Page.data.entity.getEntityName(),
                RecordId = Xrm.Page.data.entity.getId(),
                RecordName = Xrm.Page.data.entity.getPrimaryAttributeValue(),
                PI = [], PO = [], PROG = [], PORT = [], PLAN = [],
                PI_fields = ["ddsm_kpiintervalId", "ddsm_ProgramOfferingsId"],
                PO_fields = ["ddsm_programofferingId", "ddsm_ProgramId"],
                PROG_fields = ["ddsm_programId", "ddsm_PortfolioId"],
                PORT_fields = ["ddsm_portfolioId", "ddsm_DSMPlanId"],
                DSMPLAN_fields = ["ddsm_dsmplanId"],


                calcProgramIntervals = function(GUIDS, successCallback) {
                    console.log(">>> Calculation of Program Intervals");
                    console.dir(GUIDS);
                    mySpinner = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Calculation of Program Intervals");
                    var RecalcGUIDS = [],
                        GUIDS = GUIDS,
                        successCallback = successCallback,
                        calcInterval;

                    if(!GUIDS || (!!GUIDS && GUIDS.length == 0)){
                        if(EntityName == PI_fields[0].substring(0, (PI_fields[0].length - 2))) {
                            GUIDS = [];
                            GUIDS.push({Id: RecordId, LogicalName: EntityName, Name: RecordName});
                        } else successCallback(true);
                    }

                    console.dir(GUIDS);
                    for(let i = 0; i < GUIDS.length; i++){
                        let params = [];
                        params.push({
                            key: "Target",
                            type: Process.Type.EntityReference,
                            value: {
                                id: GUIDS[i].Id,
                                entityType: GUIDS[i].LogicalName
                            }
                        });
                        console.dir(params);

                        Process.callAction("ddsm_ProgramIntervalFilterChangeAction", params,
                            function(params) {
                                for (let i = 0; i < params.length; i++) {
                                    if (params[i].key == "Complete") {
                                        if (params[i].value == "true")
                                            RecalcGUIDS.push(true);
                                        else
                                            RecalcGUIDS.push(false);
                                    }
                                }
                            },
                            function(e) {
                                console.log("error: " + e);
                                RecalcGUIDS.push(false);
                            }
                        );

                    }

                    calcInterval = setInterval(function(){
                        console.log("Program Intervals: " + GUIDS.length + " ---- " + RecalcGUIDS.length);
                        if(GUIDS.length == RecalcGUIDS.length) {
                            console.log("<<< Calculation of Program Intervals");
                            mySpinner.stop();
                            clearInterval(calcInterval);
                            if(!!successCallback)
                                successCallback(true);
                        }
                    }, 1000);

                },

                _calcProgramIntervalsFromProgramOffering = function(PO_GUID, successCallback){

                    //console.log(PO_GUID + " | " + PROG_GUID);
                    //console.log(AGS.String.format("?$select={0},ddsm_name&$filter={1}/Id eq (guid'{2}') and {3}/Id eq (guid'{4}')", PI_fields[0], PI_fields[2], PO_GUID, PI_fields[1], PROG_GUID));
                    var PO_GUID = PO_GUID,
                        successCallback = successCallback,
                        success = false, GUIDS = [], calcInterval;

                    calcInterval = setInterval(function(){
                        if(success) {
                            clearInterval(calcInterval);
                            if(!!successCallback)
                                successCallback(success);
                        }
                    }, 500);

                    AGS.REST.retrieveMultipleRecords(PI_fields[0].substring(0, (PI_fields[0].length - 2)), AGS.String.format("?$select={0},ddsm_name&$filter={1}/Id eq (guid'{2}')", PI_fields[0], PI_fields[1], PO_GUID), null,
                        function(msg){
                            console.log(msg);
                            clearInterval(calcInterval);
                            success = true;
                            if(!!successCallback)
                                successCallback(success);
                        },
                        function(data){
                            console.dir(data);
                            if(data.length == 0){
                                clearInterval(calcInterval);
                                success = true;
                                if(!!successCallback)
                                    successCallback(success);
                                return;
                            }
                            for(let i = 0; i < data.length; i++){
                                GUIDS.push({Id: data[i][PI_fields[0]], LogicalName: PI_fields[0].substring(0, (PI_fields[0].length - 2)), Name: data[i]["ddsm_name"]})
                            }
                            console.dir(GUIDS);
                            calcProgramIntervals(GUIDS, function(s){success = s;});
                        }, false, null);
                },

                calcProgramOfferings = function(GUIDS, successCallback) {
                    console.log(">>> Calculation of Program Offerings");
                    console.dir(GUIDS);
                    mySpinner = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Calculation of Program Offerings");
                    var RecalcGUIDS = [],
                        GUIDS = GUIDS,
                        calcInterval;

                    if(!GUIDS || (!!GUIDS && GUIDS.length == 0)){
                        if(EntityName == PO_fields[0].substring(0, (PO_fields[0].length - 2))) {
                            GUIDS = [];
                            GUIDS.push({Id: RecordId, LogicalName: EntityName, Name: RecordName});
                        } else successCallback(true);
                    }

                    console.dir(GUIDS);

                    for(let i = 0; i < GUIDS.length; i++){

                        _calcProgramIntervalsFromProgramOffering(GUIDS[i].Id,
                            function(s){

                                mySpinner = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Calculation of Program Offering");

                                let params = [];
                                params.push({
                                    key: "Target",
                                    type: Process.Type.EntityReference,
                                    value: {
                                        id: GUIDS[i].Id,
                                        entityType: GUIDS[i].LogicalName
                                    }
                                });
                                console.dir(params);

                                Process.callAction("ddsm_ProgramOfferingFilterChangeAction", params,
                                    function(params) {
                                        for (let i = 0; i < params.length; i++) {
                                            if (params[i].key == "Complete") {
                                                if (params[i].value == "true")
                                                    RecalcGUIDS.push(true);
                                                else
                                                    RecalcGUIDS.push(false);
                                            }
                                        }
                                    },
                                    function(e) {
                                        console.log("error: " + e);
                                        RecalcGUIDS.push(false);
                                    }
                                );

                            }
                        );

                    }

                    calcInterval = setInterval(function(){
                        console.log("Program Offering: " + GUIDS.length + " ---- " + RecalcGUIDS.length);
                        if(GUIDS.length == RecalcGUIDS.length) {
                            console.log("<<< Calculation of Program Offerings");
                            mySpinner.stop();
                            clearInterval(calcInterval);
                            if(!!successCallback)
                                successCallback(true);
                        }
                    }, 1000);

                },

                _calcProgramOfferingsFromProgram = function(PROG_GUID, successCallback){
                    //console.log("PROG_GUID: " + PROG_GUID);
                    //console.log(AGS.String.format("?$select={0},ddsm_name&$filter={1}/Id eq (guid'{2}')", PO_fields[0], PO_fields[1], PROG_GUID));
                    var PROG_GUID = PROG_GUID,
                        successCallback = successCallback,
                        success = false, GUIDS = [], calcInterval;

                    calcInterval = setInterval(function(){
                        if(success) {
                            clearInterval(calcInterval);
                            if(!!successCallback)
                                successCallback(success);
                        }
                    }, 500);

                    AGS.REST.retrieveMultipleRecords(PO_fields[0].substring(0, (PO_fields[0].length - 2)), AGS.String.format("?$select={0},ddsm_name&$filter={1}/Id eq (guid'{2}')", PO_fields[0], PO_fields[1], PROG_GUID), null,
                        function(msg){
                            console.log(msg);
                            clearInterval(calcInterval);
                            success = true;
                            if(!!successCallback)
                                successCallback(success);
                        },
                        function(data){
                            console.dir(data);
                            if(data.length == 0){
                                success = true;
                                clearInterval(calcInterval);
                                if(!!successCallback)
                                    successCallback(success);
                                return;
                            }
                            for(let i = 0; i < data.length; i++){
                                GUIDS.push({Id: data[i][PO_fields[0]], LogicalName: PO_fields[0].substring(0, (PO_fields[0].length - 2)), Name: data[i]["ddsm_name"]})
                            }
                            console.dir(GUIDS);
                            calcProgramOfferings(GUIDS, function(s){success = s;});
                        }, false, null);

                },

                calcPrograms = function(GUIDS, successCallback) {
                    console.log(">>> Calculation of Program");
                    console.dir(GUIDS);
                    mySpinner = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Calculation of Programs");
                    var RecalcGUIDS = [],
                        GUIDS = GUIDS,
                        successCallback = successCallback,
                        calcInterval;

                    if(!GUIDS || (!!GUIDS && GUIDS.length == 0)){
                        if(EntityName == PROG_fields[0].substring(0, (PROG_fields[0].length - 2))) {
                            GUIDS = [];
                            GUIDS.push({Id: RecordId, LogicalName: EntityName, Name: RecordName});
                        } else successCallback(true);
                    }

                    console.dir(GUIDS);

                    for(let i = 0; i < GUIDS.length; i++){

                        _calcProgramOfferingsFromProgram(GUIDS[i].Id,
                            function(s){

                                mySpinner = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Calculation of Program");

                                let params = [];
                                params.push({
                                    key: "Target",
                                    type: Process.Type.EntityReference,
                                    value: {
                                        id: GUIDS[i].Id,
                                        entityType: GUIDS[i].LogicalName
                                    }
                                });

                                console.dir(params);

                                Process.callAction("ddsm_ProgramFilterChangeAction", params,
                                    function(params) {
                                        for (let i = 0; i < params.length; i++) {
                                            if (params[i].key == "Complete") {
                                                if (params[i].value == "true")
                                                    RecalcGUIDS.push(true);
                                                else
                                                    RecalcGUIDS.push(false);
                                            }
                                        }
                                    },
                                    function(e) {
                                        console.log("error: " + e);
                                        RecalcGUIDS.push(false);
                                    }
                                );

                            }
                        );

                    }

                    calcInterval = setInterval(function(){
                        console.log("Program:" + GUIDS.length + " ---- " + RecalcGUIDS.length);
                        if(GUIDS.length == RecalcGUIDS.length) {
                            console.log("<<< Calculation of Program");
                            mySpinner.stop();
                            clearInterval(calcInterval);
                            if(!!successCallback)
                                successCallback(true);
                        }
                    }, 1000);

                },

                _calcProgramsFromPortfolio = function(PORT_GUID, successCallback){
                    //console.log("PORT_GUID: " + PORT_GUID);
                    //console.log(AGS.String.format("?$select={0},ddsm_name&$filter={1}/Id eq (guid'{2}')", PROG_fields[0], PROG_fields[1], PORT_GUID));
                    var PORT_GUID = PORT_GUID,
                        successCallback = successCallback,
                        success = false, GUIDS = [], calcInterval;

                    calcInterval = setInterval(function(){
                        if(success) {
                            clearInterval(calcInterval);
                            if(!!successCallback)
                                successCallback(success);
                        }
                    }, 500);

                    AGS.REST.retrieveMultipleRecords(PROG_fields[0].substring(0, (PROG_fields[0].length - 2)), AGS.String.format("?$select={0},ddsm_name&$filter={1}/Id eq (guid'{2}')", PROG_fields[0], PROG_fields[1], PORT_GUID), null,
                        function(msg){
                            console.log(msg);
                            clearInterval(calcInterval);
                            success = true;
                            if(!!successCallback)
                                successCallback(success);
                        },
                        function(data){
                            console.dir(data);
                            if(data.length == 0){
                                success = true;
                                clearInterval(calcInterval);
                                if(!!successCallback)
                                    successCallback(success);
                                return;
                            }
                            for(let i = 0; i < data.length; i++){
                                GUIDS.push({Id: data[i][PROG_fields[0]], LogicalName: PROG_fields[0].substring(0, (PROG_fields[0].length - 2)), Name: data[i]["ddsm_name"]})
                            }
                            console.dir(GUIDS);
                            calcPrograms(GUIDS, function(s){success = s;});
                        }, false, null);

                },

                calcPortfolios = function(GUIDS, successCallback) {
                    console.log(">>> Calculation of Portfolio");
                    console.dir(GUIDS);
                    mySpinner = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Calculation of Portfolios");
                    var RecalcGUIDS = [],
                        GUIDS = GUIDS,
                        successCallback = successCallback,
                        calcInterval;

                    if(!GUIDS || (!!GUIDS && GUIDS.length == 0)){
                        if(EntityName == PORT_fields[0].substring(0, (PORT_fields[0].length - 2))) {
                            GUIDS = [];
                            GUIDS.push({Id: RecordId, LogicalName: EntityName, Name: RecordName});
                        } else successCallback(true);
                    }

                    console.dir(GUIDS);

                    for(let i = 0; i < GUIDS.length; i++){

                        _calcProgramsFromPortfolio(GUIDS[i].Id,
                            function(s){

                                mySpinner = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Calculation of Portfolio");

                                let params = [];
                                params.push({
                                    key: "Target",
                                    type: Process.Type.EntityReference,
                                    value: {
                                        id: GUIDS[i].Id,
                                        entityType: GUIDS[i].LogicalName
                                    }
                                });

                                console.dir(params);

                                Process.callAction("ddsm_PortfolioFilterChangeAction", params,
                                    function(params) {
                                        for (let i = 0; i < params.length; i++) {
                                            if (params[i].key == "Complete") {
                                                if (params[i].value == "true")
                                                    RecalcGUIDS.push(true);
                                                else
                                                    RecalcGUIDS.push(false);
                                            }
                                        }
                                    },
                                    function(e) {
                                        console.log("error: " + e);
                                        RecalcGUIDS.push(false);
                                    }
                                );

                            }
                        );

                    }

                    calcInterval = setInterval(function(){
                        console.log("Portfolio:" + GUIDS.length + " ---- " + RecalcGUIDS.length);
                        if(GUIDS.length == RecalcGUIDS.length) {
                            console.log("<<< Calculation of Portfolio");
                            mySpinner.stop();
                            clearInterval(calcInterval);
                            if(!!successCallback)
                                successCallback(true);
                        }
                    }, 1000);

                },

                _calcPortfoliosFromDSMPlan = function(DSMPLAN_GUID, successCallback){
                    console.log("DSMPLAN_GUID: " + DSMPLAN_GUID);
                    console.log(AGS.String.format("?$select={0},ddsm_name&$filter={1}/Id eq (guid'{2}')", PORT_fields[0], PORT_fields[1], DSMPLAN_GUID));
                    var DSMPLAN_GUID = DSMPLAN_GUID,
                        successCallback = successCallback,
                        success = false, GUIDS = [], calcInterval;

                    calcInterval = setInterval(function(){
                        if(success) {
                            clearInterval(calcInterval);
                            if(!!successCallback)
                                successCallback(success);
                        }
                    }, 500);

                    AGS.REST.retrieveMultipleRecords(PORT_fields[0].substring(0, (PORT_fields[0].length - 2)), AGS.String.format("?$select={0},ddsm_name&$filter={1}/Id eq (guid'{2}')", PORT_fields[0], PORT_fields[1], DSMPLAN_GUID), null,
                        function(msg){
                            console.log(msg);
                            clearInterval(calcInterval);
                            success = true;
                            if(!!successCallback)
                                successCallback(success);
                        },
                        function(data){
                            console.dir(data);
                            if(data.length == 0){
                                success = true;
                                clearInterval(calcInterval);
                                if(!!successCallback)
                                    successCallback(success);
                                return;
                            }
                            for(let i = 0; i < data.length; i++){
                                GUIDS.push({Id: data[i][PORT_fields[0]], LogicalName: PORT_fields[0].substring(0, (PORT_fields[0].length - 2)), Name: data[i]["ddsm_name"]})
                            }
                            console.dir(GUIDS);
                            calcPortfolios(GUIDS, function(s){success = s;});
                        }, false, null);

                },

                calcDSMPlans = function(GUIDS, successCallback) {
                    console.log(">>> Calculation of DDSM Plan");
                    console.dir(GUIDS);
                    mySpinner = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Calculation of DDSM Plans");
                    var RecalcGUIDS = [],
                        GUIDS = GUIDS,
                        successCallback = successCallback,
                        calcInterval;

                    console.log(EntityName + " | " + DSMPLAN_fields[0].substring(0, (DSMPLAN_fields[0].length - 2)));
                    console.log(RecordId);
                    console.log(RecordName);
                    console.log(!GUIDS);
                    console.log(!!GUIDS);

                    if(!GUIDS || (!!GUIDS && GUIDS.length == 0)){
                        console.log(">>>>>>>>");
                        if(EntityName == DSMPLAN_fields[0].substring(0, (DSMPLAN_fields[0].length - 2))) {
                            GUIDS = [];
                            GUIDS.push({Id: RecordId, LogicalName: EntityName, Name: RecordName});
                        } else successCallback(true);
                    }

                    console.dir(GUIDS);

                    for(let i = 0; i < GUIDS.length; i++){
                        _calcPortfoliosFromDSMPlan(GUIDS[i].Id,
                            function(s){

                                mySpinner = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Calculation of DDSM Plan");

                                let params = [];
                                params.push({
                                    key: "Target",
                                    type: Process.Type.EntityReference,
                                    value: {
                                        id: GUIDS[i].Id,
                                        entityType: GUIDS[i].LogicalName
                                    }
                                });

                                console.dir(params);

                                Process.callAction("ddsm_DSMPlanFilterChangeAction", params,
                                    function(params) {
                                        for (let i = 0; i < params.length; i++) {
                                            if (params[i].key == "Complete") {
                                                if (params[i].value == "true")
                                                    RecalcGUIDS.push(true);
                                                else
                                                    RecalcGUIDS.push(false);
                                            }
                                        }
                                    },
                                    function(e) {
                                        console.log("error: " + e);
                                        RecalcGUIDS.push(false);
                                    }
                                );

                            }
                        );
                    }

                    calcInterval = setInterval(function(){
                        console.log("DDSM Plan:" + GUIDS.length + " ---- " + RecalcGUIDS.length);
                        if(GUIDS.length == RecalcGUIDS.length) {
                            console.log("<<< Calculation of DDSM Plan");
                            mySpinner.stop();
                            clearInterval(calcInterval);
                            if(!!successCallback)
                                successCallback(true);
                        }
                    }, 1000);

                };

            return {
                calcProgramIntervals: calcProgramIntervals,
                calcProgramOfferings: calcProgramOfferings,
                calcPrograms: calcPrograms,
                calcPortfolios: calcPortfolios,
                calcDSMPlans: calcDSMPlans
            };

        })());
    }
}

//Initialize creation of a namespace
newNamespaceDDSMPlanPyramid();
