/**
 * Created by Vladyslav Poliukhovych on 3/2/2016.
 *
 * Version: 0.1.03.03016
 */

/**
 * ------ REQUIREMENTS -------
 * 1. form must contain an optionSet field
 * 2. form must contain a text field, to storage selected values (may be hidden)
 * 3. be sure that the length of a text field is enough to contain all selected values
 */

function newNamespaceMultiselect() {
    AccentGold = window.AccentGold;
    if (!window.AccentGold) {
        setTimeout(newNamespaceMultiselect, 100);
    } else {
        AccentGold.Form.namespace("Multiselect").extend((function () {

            var html_tpl = '<div class="osmContainer ms-crm-Inline-Edit"><dl class="dropdown"> \
                <dt><a href="#"><span class="osmTitle">--</span><p class="multiSel"></p> </a> </dt> \
                    <dd><div class="mutliSelect ms-crm-SelectBox ms-crm-Inline-OptionSet-AutoOpen ms-crm-Inline-HideByZeroHeight-Ie7"><ul data-attrname="{0}" data-relval="{1}" data-reltext="{2}">{3}</ul></div></dd> \
                </dl></div>',

                /**
                 * Add styles to the page
                 */
                initStyles = function(){
                    if(!!window.osmStyles){ return;}
                    
                    var css_txt = ".osmContainer{ height: 20px;}\
                     .selBoxShadow{box-shadow: 1px 1px 2px rgba(0,0,0,0.5);}\
                    .dropdown { position: absolute; transform: translateY(-50%); background:none; z-index:500; width: 100%; } \
                    .dropdown dd, .dropdown dt { margin: 0px; padding: 0px; } \
                    .dropdown ul { margin: -1px 0 0 0; } .dropdown dd { position: relative; } \
                    .dropdown a, .dropdown a:visited { color: #000; text-decoration: none; outline: none; } \
                    .dropdown a:hover {padding-left: 5px; } \
                    .dropdown dt a { display: block; padding: 0; overflow: hidden; border: 0; width: 272px; }  \
                    .multiSel {margin:0; max-height: 1.5em;}\
                    .dropdown dt a span, .multiSel span { cursor: pointer; display: inline-block; padding: 0 3px 2px 0; margin:0; font-weight:bold; } \
                    .dropdown dd ul { border: 1px solid #ccc; color: #000; background: #fff; display: none; left: 0px; padding: 2px 15px 2px 5px; position: absolute; top: -10px; width: 87%; list-style: none; height: 100px; overflow-y: scroll; } \
                    .dropdown span.value { display: none; }  \
                    .dropdown dd ul li:hover { background-color: #1e90ff; color: #fff} ";

                    AGS.String.format('<style></style>');
                    $('head:last').append(AGS.String.format('<style>{0}</style>', css_txt));
                    
                    window.osmStyles = true;
                },

                /**
                 * Attache needed events to make multi-select work
                 */
                setEvents = function () {

                    $(".osmContainer").hover(
                        function(){
                            $(this).toggleClass('ms-crm-Inline-EditHintState');
                            $(this).toggleClass('selBoxShadow');
                        },
                        function(){
                            $(this).toggleClass('ms-crm-Inline-EditHintState');
                            $(this).toggleClass('selBoxShadow');
                        }
                    );

                    $(".dropdown dt a").on('click', function () {
                        $(this).parent().parent().find('dd ul').slideToggle('fast');
                    });

                    $(".dropdown dd ul li a").on('click', function () {
                        $(".dropdown dd ul").hide();
                    });

                    function getSelectedValue(id) {
                        return $("#" + id).find("dt a span.value").html();
                    }

                    $(document).bind('click', function (e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {

                        //TODO: remove operations with spans, make it more simple if possible

                        //var val = this.value + ',',
                        var val = this.value,
                            title = $(this).parent().text(),
                            attrName = $(this).parent().parent().attr('data-attrname'),
                            relVal = $(this).parent().parent().attr('data-relval'),
                            relText = $(this).parent().parent().attr('data-reltext'),
                            multiVal = Xrm.Page.getAttribute(relVal).getValue() || '',
                            //multiText = Xrm.Page.getAttribute(relText).getValue() || '',
                            multiText = "",// full title string
                            showEmpty = true;

                        if ($(this).is(':checked')) {
                            //var html = '<span title="' + title + '">' + title + ', </span>';
                            let sepr = !!$('#'+attrName + ' .multiSel span').text() ? ',' : '';
                            var html = AGS.String.format('<span title="{0}">{1}{0}</span>',title,sepr);
                            $('#'+attrName + ' .multiSel').append(html);
                            $('#'+attrName + ' .osmTitle').hide();
                            multiVal += (!!multiVal ? "," : '') + val;
                            showEmpty = false;
                        } else {
                            $('span[title="' + title + '"]').remove();
                            //var ret = $('#'+attrName + ' .osmTitle');
                            //$('#'+attrName + ' .dropdown dt a').append(ret);
                            multiVal = multiVal.replace(new RegExp("(,*)" + val,'g'), '').replace(/(^,+)|(,+$)/g,'').trim();
                            if(!!multiVal){
                                showEmpty = false;
                            }
                        }

                        if(showEmpty){ $('#'+attrName + ' .osmTitle').show();}
                        multiText = $('#'+attrName + ' .multiSel span').text();

                        //remove lead commas
                        if(!!multiText && !!multiText.match(/(^,+)/g)){
                            multiText = multiText.replace(/(^,+)/g,'');
                            let q = AGS.String.format('span[title="{0}"]', multiText.split(',')[0]);
                            $span = $(q);
                            $span.text($span.text().replace(',',''));
                        }

                        if(!!multiText && !!multiText.match(/(,+$)/g)){
                            multiText = multiText.replace(/(,+$)/g,'');
                            let q = AGS.String.format('span[title="{0}"]', multiText.split(',')[multiText.split(',').length-1]);
                            $span = $(q);
                            $span.text($span.text().replace(',',''));
                        }

                        //Set new values to data fields
                        Xrm.Page.getAttribute(relVal).setValue(multiVal);
                        Xrm.Page.getAttribute(relText).setValue(multiText);
                        //Xrm.Page.getAttribute(relText).setValue(multiVal);
                        var newOpn = !multiVal ? null : multiVal.split(',')[0];
                        Xrm.Page.getAttribute(attrName).setValue(newOpn);

                    });
                },
                setValuesToTXT = function (osm_field, osm_strValField, osm_strTextField) {
                    if(!Xrm.Page.getAttribute(osm_strValField)){ return false;}
                    var osm_val = Xrm.Page.getAttribute(osm_field).getValue();
                    var txt_store =  Xrm.Page.getAttribute(osm_strValField).getValue();
                    if(!osm_val || !!txt_store || txt_store != '--'){return false;}

                    $('#' + osm_field +' input[type=checkbox]').each(function(){

                        if(osm_val == this.value){
                            this.checked = true;
                        }
                    });
                    return true;
                },
                setValuesFromTXT = function (osm_field, osm_strValField, osm_strTextField) {
                    if (!Xrm.Page.getAttribute(osm_strValField)) {
                        return;
                    }

                    var txt_store = Xrm.Page.getAttribute(osm_strValField).getValue();

                    if (!txt_store || txt_store == '--') {
                        return;
                    }

                    var showEmpty = true;
                     $('#' + osm_field +' input[type=checkbox]').each(function(){
                         var v = (new RegExp(this.value)).test(txt_store);
                         this.checked = v;
                         if(v){
                             var title = $(this).parent().text();
                             //var html = AGS.String.format('<span title="{0}">{0},</span>',title);
                             let sepr = !!$('#'+osm_field + ' .multiSel span').text() ? ',' : '';
                             var html = AGS.String.format('<span title="{0}">{1}{0}</span>',title,sepr);
                             $('#'+osm_field + ' .multiSel').append(html);
                             $('#'+osm_field + ' .osmTitle').hide();
                             showEmpty = false;
                         }
                     });

                    if (showEmpty) {
                        $('#' + osm_field + ' .osmTitle').show();
                    }

                    //Check saved titles
                    if(!!osm_strTextField && !!Xrm.Page.getAttribute(osm_strTextField)){
                        let titlesBase = Xrm.Page.getAttribute(osm_strTextField).getValue();
                        let titlesForm = $('#'+osm_field + ' .osmTitle span').text();

                        if(titlesBase !== titlesForm){
                            Xrm.Page.getAttribute(osm_strTextField).setValue(titlesForm);
                        }
                    }
                },

                /**
                 * Creates a lister with checkboxes instead of option set dropbox
                 * @param os_field {String} - name of option set attribute
                 * @param txt_fieldVal {String} - name of text attribute to store option set values
                 * @param txt_fieldTxt {String} - name of text attribute to store option set titles
                 * @NOTE: if this method called directly be sure that the method setEvents() is called at least once.
                 */
                createMultiselect = function (os_field, txt_fieldVal, txt_fieldTxt) {
                    os_field_lc = os_field.toLowerCase();
                    txt_fieldVal_lc = txt_fieldVal.toLowerCase();
                    txt_fieldTxt_lc = (!!Xrm.Page.getAttribute(txt_fieldTxt.toLowerCase())? txt_fieldTxt.toLowerCase() : '');

                    if (!os_field_lc || !txt_fieldVal_lc || !Xrm.Page.getAttribute(os_field_lc) || !Xrm.Page.getAttribute(txt_fieldVal_lc)) {
                        return;
                    }

                    var curOptions = Xrm.Page.getAttribute(os_field_lc).getOptions();

                    var selOptionsHTML = "";
                    for (var i = 0; i < curOptions.length; i++) {
                        var el = curOptions[i];
                        if (!el || !el.value || !el.text) {
                            continue;
                        }
                        selOptionsHTML += AGS.String.format('<li><input type="checkbox" value="{0}" style="width:16px;" />{1}</li></li>', el.value, el.text);
                    }

                    //Add new html element
                    $('#' + os_field_lc).html(AGS.String.format(html_tpl, os_field_lc , txt_fieldVal_lc, txt_fieldTxt_lc, selOptionsHTML));

                    initStyles();

                    //fix checkers
                    if(!setValuesToTXT(os_field_lc,txt_fieldVal_lc, txt_fieldTxt_lc)){
                        setValuesFromTXT(os_field_lc, txt_fieldVal_lc, txt_fieldTxt_lc);
                    }
                },
                /**
                 * Create multiple optionSets for a group of fields
                 * @param list {object} : key - optionSet field; value - text field to store values //OBSOLETE
                 * @param list {Array[object]} : [ { osField: <optionSet field name>, osValField: <text field to store values>, osTxtField: <text field to store titles>  }]
                 */
                createMultiselectList = function (list) {
                    if (!Array.isArray(list)) {
                        return;
                    }
                    //for (var fpair in list) {
                    for (let i=0; i< list.length; i++) {
                        let el = list[i];
                        //createMultiselect(fpair, list[fpair]);
                        createMultiselect(el.osField, el.osValField,  el.osTxtField);
                    }
                    setEvents();
                };

            return {
                createMultiselect: createMultiselect,
                createMultiselectList: createMultiselectList,
                setEvents: setEvents
            };

        })());
    }
}

//Initialize creation of a namespace
newNamespaceMultiselect();

/**
* EXAMPLE OF USAGE

 ///---OBSOLETE
 //var opts = {
 //       ddsm_RecordType: "ddsm_RecordTypeMulti"
 //   };
 //AGS.Form.Multiselect.createMultiselectList(opts)  ;
 ///----------

 var optsList = [{
        osField:  'ddsm_RecordType',
        osValField: 'ddsm_RecordTypeMulti',
        osTxtField: 'ddsm_RecordTypeMulti',
    }];
 AGS.Form.Multiselect.createMultiselectList(optsList)  ;

 */