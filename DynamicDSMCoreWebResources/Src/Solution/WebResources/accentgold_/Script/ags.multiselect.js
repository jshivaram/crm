/**
 * Created by Vladyslav Poliukhovych on 3/2/2016.
 *
 * Version: 0.0.1.00001
 */
function newNamespaceMultiselect() {
    AccentGold = window.AccentGold;
    if (!window.AccentGold) {
        setTimeout(newNamespaceMultiselect, 100);
    } else {
        AccentGold.namespace("Multiselect").extend((function () {

            var html_tpl = '<div class="osmContainer ms-crm-Inline-Edit"><dl class="dropdown"> \
                <dt><a href="#"><span class="osmTitle">--</span><p class="multiSel"></p> </a> </dt> \
                    <dd><div class="mutliSelect ms-crm-SelectBox ms-crm-Inline-OptionSet-AutoOpen ms-crm-Inline-HideByZeroHeight-Ie7"><ul data-attrname="{0}" data-reltext="{1}">{2}</ul></div></dd> \
                </dl></div>',

                /**
                 * Add styles to the page
                 */
                initStyles = function(){
                    if(!!window.osmStyles){ return;}
                    
                    var css_txt = ".osmContainer{ height: 20px;}\
                     .selBoxShadow{box-shadow: 1px 1px 2px rgba(0,0,0,0.5);}\
                    .dropdown { position: absolute; transform: translateY(-50%); background:none; z-index:500; width: 100%; } \
                    .dropdown dd, .dropdown dt { margin: 0px; padding: 0px; } \
                    .dropdown ul { margin: -1px 0 0 0; } .dropdown dd { position: relative; } \
                    .dropdown a, .dropdown a:visited { color: #fff; text-decoration: none; outline: none; } \
                    .dropdown a:hover {padding-left: 5px; } \
                    .dropdown dt a { display: block; padding: 0; overflow: hidden; border: 0; width: 272px; }  \
                    .multiSel {margin:0; max-height: 1.5em;}\
                    .dropdown dt a span, .multiSel span { cursor: pointer; display: inline-block; padding: 0 3px 2px 0; margin:0; font-weight:bold; } \
                    .dropdown dd ul { border: 1px solid #ccc; color: #000; background: #fff; display: none; left: 0px; padding: 2px 15px 2px 5px; position: absolute; top: -10px; width: 87%; list-style: none; height: 100px; overflow-y: scroll; } \
                    .dropdown span.value { display: none; }  \
                    .dropdown dd ul li:hover { background-color: #1e90ff; color: #fff} ";

                    //TODO: move it to global
                    AGS.String.format('<style></style>');
                    $('head:last').append(AGS.String.format('<style>{0}</style>', css_txt));
                    
                    window.osmStyles = true;
                },

                /**
                 * Attache needed events to make multi-select work
                 */
                setEvents = function () {

                    $(".osmContainer").hover( 
                        function(){
                            $(this).toggleClass('ms-crm-Inline-EditHintState');
                            $(this).toggleClass('selBoxShadow');
                        },
                        function(){
                            $(this).toggleClass('ms-crm-Inline-EditHintState');
                            $(this).toggleClass('selBoxShadow');
                        }
                    );

                    $(".dropdown dt a").on('click', function () {
                        $(".dropdown dd ul").slideToggle('fast');
                    });

                    $(".dropdown dd ul li a").on('click', function () {
                        $(".dropdown dd ul").hide();
                    });

                    function getSelectedValue(id) {
                        return $("#" + id).find("dt a span.value").html();
                    }

                    $(document).bind('click', function (e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {

                        var val = this.value + ',',
                            title = $(this).parent().text(),
                            attrName = $(this).parent().parent().attr('data-attrname'),
                            relText = $(this).parent().parent().attr('data-reltext'),
                            multiVal = Xrm.Page.getAttribute(relText).getValue() || '',
                            showEmpty = true;

                        if ($(this).is(':checked')) {
                            //var html = '<span title="' + title + '">' + title + ', </span>';
                            var html = AGS.String.format('<span title="{0}">{0},</span>',title);
                            $('#'+attrName + ' .multiSel').append(html);
                            $('#'+attrName + ' .osmTitle').hide();
                            multiVal += val;
                            showEmpty = false;
                        } else {
                            $('span[title="' + title + '"]').remove();
                            var ret = $('#'+attrName + ' .osmTitle');
                            $('#'+attrName + ' .dropdown dt a').append(ret);
                            multiVal = multiVal.replace(val, '');
                        }

                        if(showEmpty){ $('#'+attrName + ' .osmTitle').show();}

                        //Set new values to data fields
                        Xrm.Page.getAttribute(relText).setValue(multiVal);
                        var newOpn = !multiVal ? null : multiVal.split(',')[0];
                        Xrm.Page.getAttribute(attrName).setValue(newOpn);

                    });
                },
                setValuesToTXT = function (osm_field, osm_text) {
                    if(!Xrm.Page.getAttribute(osm_text)){ return false;}
                    var osm_val = Xrm.Page.getAttribute(osm_field).getValue();
                    var txt_store =  Xrm.Page.getAttribute(osm_text).getValue();
                    if(!osm_val || !!txt_store || txt_store != '--'){return false;}

                    $('#' + osm_field +' input[type=checkbox]').each(function(){

                        if(osm_val == this.value){
                            this.checked = true;
                        }
                    });
                    return true;
                },
                setValuesFromTXT = function (osm_field, osm_text) {
                    if(!Xrm.Page.getAttribute(osm_text)){ return;}
                    var txt_store =  Xrm.Page.getAttribute(osm_text).getValue();
                    if(!txt_store || txt_store == '--'){return;}

                    var showEmpty = true;
                     $('#' + osm_field +' input[type=checkbox]').each(function(){
                         var v = (new RegExp(this.value)).test(txt_store);
                         this.checked = v;
                         if(v){
                             var title = $(this).parent().text();
                             var html = AGS.String.format('<span title="{0}">{0},</span>',title);
                             $('#'+osm_field + ' .multiSel').append(html);
                             $('#'+osm_field + ' .osmTitle').hide();
                             showEmpty = false;
                         }
                     });

                    if(showEmpty){ $('#'+osm_field + ' .osmTitle').show();}
                },

            //TODO:
                /**
                 * Creates a lister with checkboxes instead of option set dropbox
                 * @param os_field {String} - name of option set attribute
                 * @param txt_field {String} - name of text attribute to store option set values
                 * @NOTE: if this method called directly be sure that the method setEvents() is called at least once.
                 */
                createMultiselect = function (os_field, txt_field) {
                    os_field_lc = os_field.toLowerCase();
                    txt_field_lc = txt_field.toLowerCase();

                    if (!os_field_lc || !txt_field_lc || !Xrm.Page.getAttribute(os_field_lc) || !Xrm.Page.getAttribute(txt_field_lc)) {
                        return;
                    }

                    var curOptions = Xrm.Page.getAttribute(os_field_lc).getOptions();

                    var selOptionsHTML = "";
                    for (var i = 0; i < curOptions.length; i++) {
                        var el = curOptions[i];
                        if (!el || !el.value || !el.text) {
                            continue;
                        }
                        selOptionsHTML += AGS.String.format('<li><input type="checkbox" value="{0}" style="width:16px;" />{1}</li></li>', el.value, el.text);

                    }

                    //Add new html element
                    $('#' + os_field_lc).html(AGS.String.format(html_tpl, os_field_lc, txt_field_lc, selOptionsHTML));

                    initStyles();

                    //fix checkers
                    if(!setValuesToTXT(os_field_lc,txt_field_lc)){
                        setValuesFromTXT(os_field_lc, txt_field_lc);
                    }
                },
            //TODO:
                /**
                 * Create multiple optionSets for a group of fields
                 * @param list {object} : key - optionSet field; value - text field to store values
                 */
                createMultiselectList = function (list) {
                    if (!list) {
                        return;
                    }
                    for (var fpair in list) {
                        createMultiselect(fpair, list[fpair]);
                    }
                    setEvents();
                };

            return {
                createMultiselect: createMultiselect,
                createMultiselectList: createMultiselectList,
                setEvents: setEvents
            };

        })());
    }
}

//Initialize creation of a namespace
newNamespaceMultiselect();


/**
* EXAMPLE OF USAGE

var opts = {
    optionSetName1: "txtOptSetStorage1",
    optionSetName2: "txtOptSetStorage2"
};
AGS.Multiselect.createMultiselectList(opts)  ;

 */