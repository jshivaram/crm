/**
 * Created by Sergey  Dergunov (sergey.dergunov@accentgold.com) on 08/03/2015.
 * Last Update by
 * Sergey Dergunov (sergey.dergunov@accentgold.com) on 08/20/2015.
 *
 * Version: 0.0.10.00000
 */
/// <reference path="./ags.core.js" />

AccentGold.namespace("Project").extend((function () {

})());

AccentGold.Project.namespace("Update").extend((function () {
    var PYMTPREF_SITE = 962080000,
        PYMTPREF_PPN = 962080001,
        PYMTPREF_LPC = 962080003,
        PYMTPREF_LPC_SITE = null;

    var multiUpdateProjMSs = function (GUIDS, MS_DATE, STEPS, MsgFlag, newWinErrProj, logConteynerName) {
        var updateLogs = {};
        updateLogs.proj = [];
        var _target = document.getElementById(logConteynerName),
            spinnerForm = CreaLab.Spinner.spin(_target);

        var projSelect = "";
/*            "ddsm_name"
            + ",ddsm_projectId"
            + ",CreatedBy"
            + ",ddsm_ProjectTemplateId"
            + ",ddsm_AccountId"
            + ",ddsm_ParentSiteId"
            + ",ddsm_PendingMilestoneIndex"
            + ",ddsm_requirepayeeinfoindex"
            + ",ddsm_completeonlastmilestone"
            + ",ddsm_PendingActualStart"
            + ",ddsm_PendingTargetEnd"
            + ",ddsm_PendingMilestone"
            + ",ddsm_CompletedMilestone"
//            + ",ddsm_CompletedStatus"
            + ",ddsm_Responsible"
            + ",ddsm_ProjectStatus"
            + ",ddsm_Phase"
            + ",ddsm_EndDate"
            + ",ddsm_StartDate"

            + ",ddsm_CurrentSavingsKW"
            + ",ddsm_CurrentSavingskWh"
            + ",ddsm_CurrentSavingsthm"
            + ",ddsm_CurrentIncentiveTotal"
            + ",ddsm_UpToDatePhase"

            + ",ddsm_Phase1SavingsKW"
            + ",ddsm_Phase1Savingsthm"
//            + ",ddsm_Phase1PerUnitCCF"
            + ",ddsm_Phase2Savingsthm"
            + ",ddsm_Phase2SavingsCCF"
//            + ",ddsm_Phase2PerUnitCCF"
            + ",ddsm_Phase3IncentiveTotal"

            + ",ddsm_payeepreference"
            + ",ddsm_payeeemail"
            + ",ddsm_payeecompany"
            + ",ddsm_payeeattnto"
            + ",ddsm_payeeaddress1"
            + ",ddsm_payeeaddress2"
            + ",ddsm_payeephone"
            + ",ddsm_payeecity"
            + ",ddsm_payeestate"
            + ",ddsm_payeezipcode"
            + ",ddsm_taxentity"
            + ",ddsm_taxid"

            + ",ddsm_LocalPowerCompanyId"
            + ",ddsm_ParentSiteId"
            +",ddsm_TradeAlly"
            + ",ddsm_TradeAllyContact"
            + ",ddsm_EscalatedToOverdue";
*/
        var ProjExpand = "ddsm_ddsm_project_ddsm_milestone,ddsm_ddsm_project_ddsm_financial,ddsm_ddsm_project_ddsm_measure,ddsm_ddsm_project_ddsm_uploadeddocumens_ProjectId,ddsm_ddsm_project_ddsm_documentconvention,ddsm_ddsm_projecttemplate_ddsm_project";
        var ProjectsID = [], Projects = [], intervalId = null, IntervalCompletedWork = null;
        if (!!GUIDS){
            if(GUIDS.length > 0) {
                for(var i = 0; i < GUIDS.length; i++){
                    if(AccentGold.guidParamCheck(GUIDS[i], '11')) ProjectsID.push(GUIDS[i]);
                }
            }
        }
        if (!!MS_DATE){}else{MS_DATE = new Date();}
        if (!!STEPS){}else{STEPS = 1;}
        if(ProjectsID.length > 0) {
            for(var i = 0; i < ProjectsID.length; i++) {
                AccentGold.REST.retrieveRecord(ProjectsID[i], "ddsm_project", projSelect, ProjExpand, _successGetProject, null, true);
            }

            intervalId = setInterval( function() {
                if(ProjectsID.length == Projects.length) {
                    clearInterval(intervalId);
                    updateLogs.proj = [Projects.length];
                    for(var j = 0; j < Projects.length; j++) {
                        updateLogs.proj[j] = {};
                        nextStepMilestone(Projects[j], MS_DATE, STEPS, updateLogs.proj[j], MsgFlag, newWinErrProj, logConteynerName);
                        //console.dir(updateLogs);
                    }
                }
            }, 1000 );
            IntervalCompletedWork = setInterval( function() {
                var flagComp = true;
                for(var i = 0; i < updateLogs.proj.length; i++){
                    if(updateLogs.proj[i].Success == null) {flagComp = false;break;}
                }
                if(flagComp){
                    clearInterval(IntervalCompletedWork);
                    spinnerForm.stop();
                }
            }, 2000 );
        }
        function _successGetProject(objProj){
            var proj = {},
                milestones = objProj.ddsm_ddsm_project_ddsm_milestone.results;
            milestones.sort(function (a, b) {
                if (a.ddsm_Index < b.ddsm_Index)
                { return -1 }
                if (a.ddsm_Index > b.ddsm_Index)
                { return 1 }
                return 0;
            });

            for(var key in objProj) {
                if(key != "ddsm_ddsm_project_ddsm_milestone" && key != "ddsm_ddsm_project_ddsm_financial" && key != "ddsm_ddsm_project_ddsm_measure" && key != "ddsm_ddsm_project_ddsm_uploadeddocumens_ProjectId" && key != "ddsm_ddsm_project_ddsm_documentconvention" && key != "ddsm_ddsm_projecttemplate_ddsm_project") {
                    proj[key] = objProj[key];
                }
            }

            proj.financials = objProj.ddsm_ddsm_project_ddsm_financial.results;
            proj.measures = objProj.ddsm_ddsm_project_ddsm_measure.results;
            proj.docs_u = objProj.ddsm_ddsm_project_ddsm_uploadeddocumens_ProjectId.results;
            proj.docs_c = objProj.ddsm_ddsm_project_ddsm_documentconvention.results;
            proj.AutoFncCreationIndex = objProj.ddsm_ddsm_projecttemplate_ddsm_project.ddsm_AutomaticFinancialCreationIndex;
            proj.fnclTplId = (!!objProj.ddsm_ddsm_projecttemplate_ddsm_project.ddsm_FinancialTemplateforAutoIncentive.Id) ? objProj.ddsm_ddsm_projecttemplate_ddsm_project.ddsm_FinancialTemplateforAutoIncentive.Id : AccentGold.default.financialTemplateId;
            //milestones[proj.ddsm_PendingMilestoneIndex - 1].ddsm_ActualEnd = AccentGold.Date.zeroTime(AccentGold.Date.convert(MS_DATE));
            proj.milestones = milestones;
            //console.dir(proj);
            Projects.push(proj);
        }

    };

    var nextStepMilestone = function (proj, MS_DATE, STEPS, updateLog, MsgFlag, newWinErrProj, logConteynerName) {
        console.log(">> Start projUpdate");
        //console.dir(proj);
        debugger;
        updateLog.Success = null;
        updateLog.SuccessTxt = "";
        updateLog.Name = proj.ddsm_name;
        updateLog.Id = proj.ddsm_projectId;
        updateLog.Log= "";

        var proj = proj,
            MS_DATE = MS_DATE,
            STEPS = STEPS - 1,
            updateLog = updateLog,
            MsgFlag = MsgFlag,
            newWinErrProj = newWinErrProj,
            _oneDay = 1000 * 60 * 60 * 24,
            _indexToUpdateFncls = null,
            AutoFncCreationIndex = (!!proj.AutoFncCreationIndex) ? parseInt(proj.AutoFncCreationIndex) : 0;

        //console.log("Next Speps = " + STEPS);

        if(!!proj.ddsm_PendingMilestoneIndex) {
            proj.milestones[proj.ddsm_PendingMilestoneIndex - 1].ddsm_ActualEnd = AccentGold.Date.zeroTime(AccentGold.Date.convert(MS_DATE));

            var status = _projActualEnd_onChange(proj.ddsm_PendingMilestoneIndex - 1);
            if(status) {
                if(STEPS > 0) {
                    nextStepMilestone(proj, MS_DATE, STEPS, updateLog, MsgFlag, newWinErrProj);
                } else {
                    if(!!logConteynerName && !!$){rendererLog(logConteynerName, updateLog);}
                }
            } else {
                if(!!logConteynerName && !!$){rendererLog(logConteynerName, updateLog);}
                if(newWinErrProj) {
                    var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                    var url = AccentGold.getServerUrl() + "/main.aspx?etn=ddsm_project&extraqs=&histKey=" + randomnumber + "&id=" + proj.ddsm_projectId + "&newWindow=true&pagetype=entityrecord";
                    eval("var win" + (proj.ddsm_name).substr(0, 6) + "= window.open('" + url + "', '_blank');");
                    var str = "Project Name: " + updateLog.Name + "\\n\\nUpdate Status: " + updateLog.SuccessTxt + "\\nUpdate Log: " + updateLog.Log;
                    setTimeout(function(){
                        eval("win" + (proj.ddsm_name).substr(0, 6) + ".alert('" + str + "');");
                    }, 1000);
                }
            }

        } else {
            updateLog.SuccessTxt = "Error";
            updateLog.Log = "\\nThis project is now complete."
            updateLog.Success = false;
            rendererLog(logConteynerName, updateLog);
        }

        function _projActualEnd_onChange(rowIdx) {
            debugger;
            console.log(">> Start  _projActualEnd_onChange");
            if(rowIdx < 0) {rowIdx = 0;}
            var retval = true;
/*
            if(proj.milestones[rowIdx].ddsm_ProjectTaskCount != 0){
                if(MsgFlag) {
                    alert("Some project task is not complete!");
                }
                updateLog.Log += "\\nSome project task is not complete!";
                proj.milestones[rowIdx].ddsm_ActualEnd = null;
                updateLog.SuccessTxt = "Error";
                updateLog.Success = false;
                return false;
            }
*/

            retval = _verifyUploadDocs(proj.ddsm_PendingMilestoneIndex);
            if (!retval) {
                updateLog.SuccessTxt = "Error";
                updateLog.Success = false;
                return false;
            }

            retval = _projValidCheck(rowIdx);
            if (!retval) {
                updateLog.SuccessTxt = "Error";
                updateLog.Success = false;
                return false;
            }

            var tempObj = {};
            tempObj = _checkApprovalSkipGeneral(rowIdx);
            tempObj.rowIdx = rowIdx;

            retval = _projRecalcDates(tempObj.rowIdx);
            if (!retval) {
                updateLog.SuccessTxt = "Error";
                updateLog.Success = false;
                return false;
            }

            if(AutoFncCreationIndex == parseInt(proj.milestones[rowIdx].ddsm_Index)) {
                AutoMeasureFncl(proj.measures, proj.milestones[rowIdx + 1].ddsm_ActualStart, proj.fnclTplId, AutoFncCreationIndex);
                var objFncl = AccentGold.REST.retrieveRecord(proj.ddsm_projectId, "ddsm_project", "ddsm_projectId", "ddsm_ddsm_project_ddsm_financial", null, null, false);
                proj.financials = objFncl.ddsm_ddsm_project_ddsm_financial.results;
            }

            retval = _projFncls_Kickoff(proj.milestones[tempObj.rowIdx].ddsm_Index, proj.milestones[tempObj.rowIdx].ddsm_ActualEnd);

            if (!retval) {
                updateLog.SuccessTxt = "Error";
                updateLog.Success = false;
                return false;
            }

            // update milestone
            projMSsData_Update(proj.milestones, MsgFlag, updateLog);

            RecalculateCurrMSDaysPastDue(proj);

            //update project
            _projData_Update();

            recalcMsTplAverageDuration(proj.milestones[rowIdx].ddsm_MsTpltoMs.Id, MsgFlag, updateLog);

            //Library ddsm_milestoneComplete_main.js
            //ms_AutoEmail(rowIdx);

            _projFncls_Update(_indexToUpdateFncls);

            if(updateLog.Log.length > 0) {
                updateLog.SuccessTxt = "Error";
                updateLog.Success = false;
                return false;
            } else {
                nextBProcess(proj, proj.milestones);
                updateLog.SuccessTxt = "Successfull";
                updateLog.Success = true;
                return true;
            }

        }

        function nextBProcess(projTpl, msActiveName){
            console.dir(projTpl);
            console.dir(msActiveName);
        }

        function _verifyUploadDocs(Index){
            console.log(">> Start  _verifyUploadDocs: " + proj.docs_c.length);
            var docNames = "";
            if (proj.docs_c.length > 0) {
                for (var i = 0; i < proj.docs_c.length; i++) {
                    if(parseInt(proj.docs_c[i].ddsm_RequiredByStatus) == Index && !proj.docs_c[i].ddsm_Uploaded) {docNames += proj.docs_c[i].ddsm_name +"\\n";}
                }
                if(docNames == "") {
                    return true;
                } else {
                    if(MsgFlag) {
                        alert("The following documents must be attached to this Project before completing this Milestone:\\n" + docNames);
                    }
                    updateLog.Log += "\\nThe following documents must be attached to this Project before completing this Milestone:\\n" + docNames;
                    return false;
                }
            } else {return true;}
        }

        function _checkApprovalSkipGeneral(rowIdx) {
            console.log(">> Start  _checkApprovalSkipGeneral");

            var recordsCount = proj.milestones.length,
                meas = proj.measures,
                incentive = 0.0;
            try {
                for (var i = 0; i < meas.length; i++) {
                    if (!isNaN(parseFloat(meas[i].ddsm_CurrentIncTotal.Value))) {
                        if (meas[i].ddsm_StudyMeasure == true) {
                            incentive += parseFloat(meas[i].ddsm_CurrentIncTotal.Value);
                        } else {
                            incentive += parseFloat(meas[i].ddsm_CurrentIncTotal.Value);
                        }
                    }
                }
            } catch (err) {
                alert(err);
                updateLog.Log += "\\nError: fnclCalcPymt - " + err;
            }

            var apprThresh = 0, prevActEnd, actEnd, index, oldRowIdx = 0, nextRowIdx, nextIndex = -1, runApprSkip = true, keepSkipping = false;

            for (var i = 0; i < recordsCount; i++) {

                if (proj.milestones[i].ddsm_ApprovalThresholdCust != null) {
                    apprThresh = parseFloat(proj.milestones[i].ddsm_ApprovalThresholdCust.Value);
                }
                index = parseInt(proj.milestones[i].ddsm_Index);
                actEnd = proj.milestones[i].ddsm_ActualEnd;
                if (index > 1) {
                    prevActEnd = proj.milestones[oldRowIdx].ddsm_ActualEnd;
                } else {
                    prevActEnd = null;
                }

                oldRowIdx = i;
                if (proj.milestones[rowIdx].ddsm_Index == index) {
                    nextIndex = index;
                    nextRowIdx = i;
                }

                if (apprThresh == -1) {
                    //no thresholds, skip this milestone
                    keepSkipping = false;
                    continue;
                }

                runApprSkip = true;
                if ((apprThresh != -2) && (incentive >= apprThresh)) {
                    runApprSkip = false;
                }

                if (runApprSkip == true) {
                    _approvalSkip(prevActEnd, actEnd, i);
                    if ((proj.milestones[rowIdx].ddsm_Index + 1) == index) {
                        keepSkipping = true;
                    }
                } else {
                    _approvalUnSkip(i);
                    keepSkipping = false;
                }

                if ((keepSkipping) && (index > proj.milestones[rowIdx].ddsm_Index)) {
                    nextIndex = index;
                }
            }

            var tempObj = new Object();
            tempObj.rowIdx = nextRowIdx;
            tempObj.nextIndex = nextIndex;
            if (nextIndex != -1) {
                proj.ddsm_PendingMilestoneIndex = parseInt(nextIndex);
            }
            return tempObj;

        }

        function _approvalSkip(prevActEnd, actEnd, rowIdx) {
            console.log(">> Start _approvalSkip");
            var dtPrev = prevActEnd, dtAct = actEnd;
            proj.milestones[rowIdx].ddsm_SkipMilestone = true;
            if (dtPrev != null) {
                proj.milestones[rowIdx].ddsm_ActualEnd = prevActEnd;
            }
        }

        function _approvalUnSkip(rowIdx) {
            console.log(">> Start _approvalUnSkip");
            proj.milestones[rowIdx].ddsm_SkipMilestone = false;
        }

        function _projValidCheck(rowIdx) {
            debugger;
            console.log(">> Start _projValidCheck " + rowIdx);
            var today = AccentGold.Date.zeroTime(new Date()), ActEndPrev = new Date(), reqPayeeInfoIndex = proj.ddsm_requirepayeeinfoindex;
            if (rowIdx > 0) {
                ActEndPrev = proj.milestones[rowIdx - 1].ddsm_ActualEnd;
            } else {
                ActEndPrev = null;
            }
            var ActStart = AccentGold.Date.zeroTime(new Date(proj.milestones[rowIdx].ddsm_ActualStart));
            //console.log("ActStart = " + ActStart);
            var ActEnd = AccentGold.Date.zeroTime(new Date(proj.milestones[rowIdx].ddsm_ActualEnd));
            if (!!ActEnd) {}else{
                return false;
            }
            if (proj.milestones[rowIdx].ddsm_UseValidation != true) {
                return true;
            }
            if (ActEnd <= today) {
            } else {
                if(MsgFlag) {
                    alert("Actual End can not be later than today`s date.");
                }
                updateLog.Log += "\\nActual End can not be later than today`s date.";
                proj.milestones[rowIdx].ddsm_ActualEnd = null;
                return false;
            }
            if (!!ActStart) {
            } else {
                //console.log(ActStart);
                if(MsgFlag) {
                    alert("Actual Start must be a valid date.");
                }
                updateLog.Log += "\\nActual Start must be a valid date.";
                proj.milestones[rowIdx].ddsm_ActualEnd = null;
                return false;
            }
            if (rowIdx > 0) {
                if (ActEndPrev == null) {
                    if(MsgFlag) {
                        alert("The previous line`s Actual End must be a valid date.");
                    }
                    updateLog.Log += "\\nThe previous line`s Actual End must be a valid date.";
                    proj.milestones[rowIdx].ddsm_ActualEnd = null;
                    return false;
                }
            }
            if (ActEnd >= ActStart) {
            } else {
                if(MsgFlag) {
                    alert("Actual End must be later than Actual Start`s date.");
                }
                updateLog.Log += "\\nActual End must be later than Actual Start`s date.";
                proj.milestones[rowIdx].ddsm_ActualEnd = null;
                return false;
            }
            if (_validMeasCheck(rowIdx)) {
            } else {
                return false;
            }
            if (proj.milestones[rowIdx].ddsm_Index == reqPayeeInfoIndex) {
                if (!_payeeInfoCheck(rowIdx)) {
                    if(MsgFlag) {
                        alert("Payee information incomplete. Please fill in missing fields to continue.");
                    }
                    updateLog.Log += "\\nPayee information incomplete. Please fill in missing fields to continue.";
                    return false;
                }
            }
            return true;
        }

        function _validMeasCheck(rowIdx) {
            console.log(">> Start _validMeasCheck");
            if ( proj.milestones[rowIdx].ddsm_RequiresValidMeasure == true) {
                var curSaveKW = proj.ddsm_CurrentSavingsKW;
                var curSavekWh = proj.ddsm_CurrentSavingskWh;
                var curSavethm = proj.ddsm_CurrentSavingsthm;
                var curIncTotal = proj.ddsm_CurrentIncentiveTotal;

                var curSaveKWBool = (curSaveKW != null && curSaveKW > 0);
                var curSavekWhBool = (curSavekWh != null && curSavekWh > 0);
                var curSavethmBool = (curSavethm != null && curSavethm > 0);
                var curIncTotalBool = (curIncTotal != null && curIncTotal > 0)

                //if (curSaveKWBool || curSavekWhBool || curSavethmBool || curIncTotalBool) {
                if (curIncTotalBool) {
                    return true;
                } else {

                    calcMeasSums(proj);
                    curSaveKW = proj.ddsm_CurrentSavingsKW;
                    curSavekWh = proj.ddsm_CurrentSavingskWh;
                    curSavethm = proj.ddsm_CurrentSavingsthm;
                    curIncTotal = proj.ddsm_CurrentIncentiveTotal.Value;

                    curSaveKWBool = (curSaveKW != null && curSaveKW > 0);
                    curSavekWhBool = (curSavekWh != null && curSavekWh > 0);
                    curSavethmBool = (curSavethm != null && curSavethm > 0);
                    curIncTotalBool = (curIncTotal != null && curIncTotal > 0)

//                        if (curSaveKWBool || curSavekWhBool || curSavethmBool || curIncTotalBool) {
                    if (curIncTotalBool) {
                        return true;
                    } else {
                        if(MsgFlag) {
                            alert("This project contains no valid measures.");
                        }
                        updateLog.Log += "\\nThis project contains no valid measures.";
                        proj.milestones[rowIdx].ddsm_ActualEnd = null;
                        return false;
                    }
                }

            } else if (proj.milestones[rowIdx].ddsm_RequiresEmptyMeasure == true) {
                var curSaveKW = proj.ddsm_CurrentSavingsKW;
                var curSavekWh = proj.ddsm_CurrentSavingskWh;
                var curSavethm = proj.ddsm_CurrentSavingsthm;
                var curIncTotal = proj.ddsm_CurrentIncentiveTotal;

                var curSaveKWBool = (curSaveKW != null);
                var curSavekWhBool = (curSavekWh != null);
                var curSavethmBool = (curSavethm != null);
                var curIncTotalBool = (curIncTotal != null)

                //if (curSaveKWBool && curSavekWhBool && curSavethmBool && curIncTotalBool) {
                if (curIncTotalBool) {
                    return true;
                } else {
                    var emptyMeas = (proj.measures.length != 0) ? true : false;

                    if (emptyMeas == true) {
                        return true;
                    } else {
                        if(MsgFlag) {
                            alert("This project requires at least one measure.");
                        }
                        updateLog.Log += "\\nThis project requires at least one measure.";
                        proj.milestones[rowIdx].ddsm_ActualEnd = null;
                        return false;
                    }
                }
            }
            return true;
        }

        function _payeeInfoCheck(rowIdx) {
            console.log(">> Start _payeeInfoCheck");
            if (proj.ddsm_payeecompany == null
                || proj.ddsm_payeeattnto == null
                || proj.ddsm_payeeaddress1 == null
                || proj.ddsm_payeephone == null
                || proj.ddsm_payeecity == null
                || proj.ddsm_payeestate == null
                || proj.ddsm_payeezipcode == null
                || proj.ddsm_taxentity.Value == null
                || proj.ddsm_taxid == null) {
                proj.milestones[rowIdx].ddsm_ActualEnd = null;
                return false;
            } else {
                return true;
            }
        }


        function _projRecalcDates(rowIdx) {
            console.log(">> Start _projRecalcDates");
            var recordsCount = proj.milestones.length;
            if (proj.ddsm_StartDate == null) return false;
            proj.milestones[0].ddsm_TargetEnd =  AccentGold.Date.addDays(proj.milestones[0].ddsm_TargetStart, proj.milestones[0].ddsm_Duration);
            if (proj.milestones[0].ddsm_ActualStart != null && proj.milestones[0].ddsm_ActualEnd == null) { proj.milestones[0].ddsm_Status.Value = 962080001; }

            var dtPrevActEnd = new Date(), dtPrevEstEnd = new Date(), dtPrevTargEnd = new Date(), dtPrevBest = new Date(), dtTargEnd = new Date(), intDur;

            var upToDateRowIdx = 0;
            _indexToUpdateFncls = null;
            var maxRow;

            var currStatus = parseInt(proj.milestones[rowIdx].ddsm_Status.Value);

            var oRowPrevIdx = rowIdx - 1;
            var oRowNextIdx = rowIdx + 1;
            var currentActEnd = proj.milestones[rowIdx].ddsm_ActualEnd;
            var prevActEnd = null;
            if (oRowPrevIdx >= 0) {
                if (proj.milestones[oRowPrevIdx].ddsm_ActualEnd != null) {
                    prevActEnd = new Date(proj.milestones[oRowPrevIdx].ddsm_ActualEnd);
                }
            }


            if (currentActEnd != null) {
                proj.milestones[rowIdx].ddsm_Status.Value = 962080002;
                var actualDuration = parseInt(((proj.milestones[rowIdx].ddsm_ActualEnd).getTime() - (proj.milestones[rowIdx].ddsm_ActualStart).getTime()) / _oneDay);
                proj.milestones[rowIdx].ddsm_ActualDuration = actualDuration;
            } else {
                proj.milestones[rowIdx].ddsm_Status.Value = 962080001;
            }

            for (var i = 1; i < recordsCount; i++) {

                var skipCheckStatus = false;

                dtPrevActEnd = proj.milestones[i - 1].ddsm_ActualEnd;
                dtPrevEstEnd = proj.milestones[i - 1].ddsm_EstimatedEnd;
                dtPrevTargEnd = proj.milestones[i - 1].ddsm_TargetEnd;

                if (dtPrevActEnd != null) {
                    dtPrevBest = dtPrevActEnd;
                } else if (dtPrevEstEnd != null) {
                    dtPrevBest = dtPrevEstEnd;
                } else if (dtPrevTargEnd != null) {
                    dtPrevBest = dtPrevTargEnd;
                } else {
                    if(MsgFlag) {
                        alert("Error: ddsm_Proj_recalcDates Target End Date is missing");
                    }
                    updateLog.Log += "\\nError: ddsm_Proj_recalcDates Target End Date is missing";
                    return false;
                }

                proj.milestones[i].ddsm_TargetStart = dtPrevBest;

                if (dtPrevActEnd != null) {
                    proj.milestones[i].ddsm_ActualStart = dtPrevActEnd;
                } else {
                    proj.milestones[i].ddsm_ActualStart = null;
                }

                dtTargEnd.setTime((AccentGold.Date.addDays(dtPrevBest, proj.milestones[i].ddsm_Duration)).getTime());
                proj.milestones[i].ddsm_TargetEnd = dtTargEnd;

                oRowPrevIdx = i - 1;
                oRowNextIdx = i + 1;

                if (proj.milestones[i].ddsm_SkipMilestone != null) {
                    skipCheckStatus = proj.milestones[i].ddsm_SkipMilestone;
                    if (skipCheckStatus == true) {
                        proj.milestones[i].ddsm_ActualEnd = proj.milestones[i].ddsm_ActualStart;
                        proj.milestones[i].ddsm_Status.Value = 962080003;
                    }
                }
                currentActEnd = proj.milestones[i].ddsm_ActualEnd;


                if (proj.milestones[i - 1].ddsm_ActualEnd != null) {
                    prevActEnd = proj.milestones[i - 1].ddsm_ActualEnd;
                } else {
                    prevActEnd = null;
                }

                if (currentActEnd != null) {
                    if(!skipCheckStatus){
                        proj.milestones[i].ddsm_Status.Value = 962080002;
                        var actualDuration = parseInt(((proj.milestones[i].ddsm_ActualEnd).getTime() - (proj.milestones[i].ddsm_ActualStart).getTime()) / _oneDay);
                        proj.milestones[i].ddsm_ActualDuration = actualDuration;
                        upToDateRowIdx = i;
                    }
                } else if (currentActEnd == null && prevActEnd != null) {
                    proj.milestones[i].ddsm_Status.Value = 962080001;
                    proj.ddsm_PendingMilestoneIndex = parseInt(proj.milestones[i].ddsm_Index);;
                    upToDateRowIdx = i;
                    _indexToUpdateFncls = parseInt(proj.milestones[i].ddsm_Index);


                } else {
                    proj.milestones[i].ddsm_Status.Value = 962080000;
                }

                _updateInitialDates(i);
            }
            //console.dir(proj.milestones);
            currentActEnd = proj.milestones[rowIdx].ddsm_ActualEnd;
            if (currentActEnd != null) {
                _projCopyPhaseCheck(rowIdx);
            }
            proj.ddsm_EstimatedProjectComplete = dtTargEnd;

            var retval = false;
            retval = _projData_UpdateSummary(upToDateRowIdx, recordsCount - 1);
            if (!retval) {
                return false;
            }
            return true;
        }

        function _updateInitialDates(rowIdx) {
            console.log(">> Start _updateInitialDates");
            //Includes code to update Projected Installation Date and Completed Milestones fields.
            var recordsCount = proj.milestones.length;

            var date = proj.milestones[rowIdx].ddsm_ActualEnd;
            var phaseCurrent = proj.milestones[rowIdx].ddsm_ProjectPhaseName;
            var phaseNext;

            var RowNext;
            if ((rowIdx + 1) < recordsCount) {
                RowNext = rowIdx + 1;
            } else {
                RowNext = null;
            }
            var RowPrev;
            if (rowIdx > 0) {
                RowPrev = rowIdx - 1;
            } else {
                RowPrev = null;
            }

            //Sets Initial Commitment and Installation
            if (RowNext != null) {
                if (proj.milestones[RowNext].ddsm_ProjectPhaseName == null) {
                    return;
                } else {
                    phaseNext = proj.milestones[RowNext].ddsm_ProjectPhaseName;
                }
                //Sets Projected Installation
                for (var i = 0; i < recordsCount; i++) {
                    var actEnd = null;
                    var estEnd = null;
                    var targEnd = null;
                    var instPhasePrev = null;
                    var instPhaseNext = null;
                    var instPhaseCurrent = proj.milestones[i].ddsm_ProjectPhaseName;
                    if (proj.milestones[i + 1] != null && proj.milestones[i + 1].ddsm_ProjectPhaseName != null) {
                        instPhaseNext = proj.milestones[i + 1].ddsm_ProjectPhaseName;
                    }
                    if (i - 1 >= 0) {
                        if (proj.milestones[i - 1].ddsm_ProjectPhaseName != null) {
                            instPhasePrev = proj.milestones[i - 1].ddsm_ProjectPhaseName;
                        }
                    }
                    if (proj.milestones[i].ddsm_ActualEnd != null && typeof proj.milestones[i].ddsm_ActualEnd != 'undefined') {
                        actEnd = proj.milestones[i].ddsm_ActualEnd;
                    }
                    if (proj.milestones[i].ddsm_EstimatedEnd != null && typeof proj.milestones[i].ddsm_EstimatedEnd != 'undefined') {
                        estEnd = proj.milestones[i].ddsm_EstimatedEnd;
                    }
                    if (proj.milestones[i].ddsm_TargetEnd != null && typeof proj.milestones[i].ddsm_TargetEnd != 'undefined') {
                        targEnd = proj.milestones[i].ddsm_TargetEnd;
                    }

                    if (instPhaseCurrent == "3. Evaluation" && ((instPhasePrev == "1. Opportunity" || instPhasePrev == "1. Offered") || instPhasePrev == "2. Project")) {
                        if (actEnd != null) {
                            proj.ddsm_ProjectedInstallation = actEnd;
                            break;
                        } else if (estEnd != null) {
                            proj.ddsm_ProjectedInstallation = estEnd;
                            break;
                        } else if (targEnd != null) {
                            proj.ddsm_ProjectedInstallation = targEnd;
                            break;
                        }
                        break;
                    }

                }

            }
        }

        function _projCopyPhaseCheck(rowIdx) {
            console.log(">> Start _projCopyPhaseCheck");
            var recordsCount = proj.milestones.length;
            var phaseCurrent = proj.milestones[rowIdx].ddsm_ProjectPhaseName;
            var upToDatePhase = proj.ddsm_UpToDatePhase;

            if (rowIdx + 1 < recordsCount) {
                if (proj.milestones[rowIdx + 1].ddsm_ProjectPhaseName != null) {
                    var phaseNext = proj.milestones[rowIdx + 1].ddsm_ProjectPhaseName;

                    if ((phaseCurrent == "1. Opportunity" || phaseCurrent == "1. Offered") && phaseNext == "3. Evaluation" && upToDatePhase == null) {
                        proj.measures = measCopyPhase(proj.measures, 1, 3);
                        proj.ddsm_Phase3Savingsthm = proj.ddsm_Phase1Savingsthm;
                        proj.ddsm_Phase3IncentiveTotal = proj.ddsm_Phase1IncentiveTotal;
                        proj.ddsm_Phase3SavingsCCF = proj.ddsm_Phase1SavingsCCF;
                        //proj.ddsm_Phase3PerUnitCCF = proj.ddsm_Phase1PerUnitCCF;
                        proj.ddsm_UpToDatePhase = 3;
                    } else if ((phaseCurrent == "1. Opportunity" || phaseCurrent == "1. Offered") && phaseNext == "2. Project" && upToDatePhase == null) {
                        proj.measures = measCopyPhase(proj.measures, 1, 2);
                        proj.ddsm_Phase2SavingsKW = proj.ddsm_Phase1SavingsKW;
                        proj.ddsm_Phase2SavingskWh = proj.ddsm_Phase1SavingskWh;
                        proj.ddsm_Phase2Savingsthm = proj.ddsm_Phase1Savingsthm;
                        proj.ddsm_Phase2IncentiveTotal = proj.ddsm_Phase1IncentiveTotal;
                        proj.ddsm_Phase2SavingsCCF = proj.ddsm_Phase1SavingsCCF;
                        //proj.ddsm_Phase2PerUnitCCF = proj.ddsm_Phase1PerUnitCCF;
                        proj.ddsm_UpToDatePhase = 2;
                    } else if (phaseCurrent == "2. Project" && phaseNext == "3. Evaluation" && upToDatePhase == 2) {
                        proj.measures = measCopyPhase(proj.measures, 2, 3);
                        proj.ddsm_Phase3Savingsthm = proj.ddsm_Phase2Savingsthm;
                        proj.ddsm_Phase3IncentiveTotal = proj.ddsm_Phase2IncentiveTotal;
                        proj.ddsm_Phase3SavingsCCF = proj.ddsm_Phase2SavingsCCF;
                        //proj.ddsm_Phase3PerUnitCCF = proj.ddsm_Phase2PerUnitCCF;
                        proj.ddsm_UpToDatePhase = 3;
                    }
                }
            }
        }

        function _projData_UpdateSummary(rowIdx, maxRow) {
            console.log(">> Start _projData_UpdateSummary");
            var msComplete = proj.ddsm_completeonlastmilestone;
            if (rowIdx == maxRow && proj.milestones[rowIdx].ddsm_ActualEnd != null) {
                var ActEnd = proj.milestones[maxRow].ddsm_ActualEnd;
                var maxStatus = proj.milestones[maxRow].ddsm_ProjectStatus;
                proj.ddsm_PendingMilestone = "";
                if (msComplete) {
                    proj.ddsm_ProjectStatus = "7-Completed";
                    proj.ddsm_Phase = "3. Evaluation";
                    proj.ddsm_EndDate = ActEnd;
                } else {
                    proj.ddsm_ProjectStatus = maxStatus;
                    proj.ddsm_EndDate = ActEnd;
                }
                proj.ddsm_PendingActualStart = null;
                proj.ddsm_PendingTargetEnd = null;
                proj.ddsm_PendingMilestoneIndex = null;
                proj.ddsm_Responsible = "";
                proj.ddsm_CompletedMilestone = "";
                return true;
            }

            var sourceRowNextIdx = rowIdx + 1;
            var sourceRowPrevIdx = rowIdx - 1;

            var pendMstName = proj.milestones[rowIdx].ddsm_name;
            var statusNext = proj.milestones[rowIdx].ddsm_ProjectStatus;
            var phaseNext = proj.milestones[rowIdx].ddsm_ProjectPhaseName;
            var targEndNext = proj.milestones[rowIdx].ddsm_TargetEnd;
            var responsible = proj.milestones[rowIdx].ddsm_Responsible;
            if(sourceRowPrevIdx >= 0){
                var ActEnd = proj.milestones[sourceRowPrevIdx].ddsm_ActualEnd;
                var completedStatus = proj.milestones[sourceRowPrevIdx].ddsm_ProjectStatus;
                var completedMS = proj.milestones[sourceRowPrevIdx].ddsm_name;
            } else {
                var ActEnd = proj.milestones[0].ddsm_TargetStart;
            }

            proj.ddsm_PendingMilestone = pendMstName;
            proj.ddsm_ProjectStatus = statusNext;
            proj.ddsm_Phase = phaseNext;
            proj.ddsm_PendingActualStart = ActEnd;
            proj.ddsm_PendingTargetEnd = targEndNext;
            proj.ddsm_Responsible = responsible;
            //proj.ddsm_CompletedStatus = completedStatus;
            proj.ddsm_CompletedMilestone = completedMS;

            //Update project status Measure
            for(var i = 0; i < proj.measures.length; i++) {
                proj.measures[i].ddsm_ProjectStatus = proj.ddsm_ProjectStatus;
            }

            if(sourceRowPrevIdx >= 0) {
                var initialOffered = proj.milestones[sourceRowPrevIdx].ddsm_InitialOffered;
                if (initialOffered == true) {
                    //proj.ddsm_InitialOffered = ActEnd;
                }
            }
            return true;
        }

        function _projFncls_Kickoff(rowIdx, actualEnd) {
            console.log(">> Start _projFncls_Kickoff");
            var adminReq = "$select=ddsm_NTG2013Est,"
                + "ddsm_NTG2014Est,"
                + "ddsm_NTG2015Est,"
                + "ddsm_NTG2013Eval,"
                + "ddsm_NTG2014Eval,"
                + "ddsm_NTG2015Eval,"
                + "ddsm_PerformanceTier,"
                + "ddsm_PerfRateCommitkWh,"
                + "ddsm_PerfRateCommitKW,"
                + "ddsm_PerfRateCommitthm,"
                + "ddsm_PerfRateInstalledkWh1,"
                + "ddsm_PerfRateInstalledKW1,"
                + "ddsm_PerfRateInstalledthm1,"
                + "ddsm_PerfRateInstalledkWh2,"
                + "ddsm_PerfRateInstalledKW2,"
                + "ddsm_PerfRateInstalledthm2,"
                + "ddsm_PerfRateInstalledkWh3,"
                + "ddsm_PerfRateInstalledKW3,"
                + "ddsm_PerfRateInstalledthm3"
                + "&$filter=ddsm_name eq 'Admin Data' and statecode/Value eq 0";

            var reqAdminData;
            AccentGold.REST.retrieveMultipleRecords("ddsm_admindata", adminReq, null, null, function(data){reqAdminData = data;}, false, null);
            var adminData = reqAdminData[0];
try {
            if(proj.financials.length > 0) {
                for (var i = 0; i < proj.financials.length; i++) {
                    if (rowIdx == proj.financials[i].ddsm_initiatingmilestoneindex && proj.financials[i].ddsm_status.Value == 962080000) {
                        var calcType = ((proj.financials[i].ddsm_calculationtype.Value).toString()).substring(7, 9);
                        var checkNo = (proj.ddsm_projectId).substring(0, 6);
                        var objFncl = new Object();
                        objFncl.ddsm_pendingstage = proj.financials[i].ddsm_pendingstage = proj.financials[i].ddsm_stagename1;
                        objFncl.ddsm_status = new Object();
                        objFncl.ddsm_status.Value = '962080001';
                        proj.financials[i].ddsm_status = objFncl.ddsm_status;
                        objFncl.ddsm_actualstart1 = proj.financials[i].ddsm_actualstart1 = actualEnd;
                        objFncl.ddsm_checknumber = proj.financials[i].ddsm_checknumber = checkNo.toString();

                        fnclCalcPymt(objFncl, proj.financials, proj.measures, adminData, actualEnd, calcType, MsgFlag, updateLog);

                        for (var key in objFncl) {
                            proj.financials[i][key] = objFncl[key];
                        }

                        var tempDate = new Date(objFncl.ddsm_actualstart1);
                        objFncl.ddsm_targetstart1 = proj.financials[i].ddsm_targetstart1 = tempDate;

                        if (proj.financials[i].ddsm_duration1 == null) proj.financials[i].ddsm_duration1 = 0;
                        tempDate.setTime((AccentGold.Date.addDays(tempDate, proj.financials[i].ddsm_duration1)).getTime());

                        objFncl.ddsm_targetend1 = proj.financials[i].ddsm_targetend1 = tempDate;
                        objFncl.ddsm_targetstart2 = proj.financials[i].ddsm_targetstart2 = tempDate;

                        if (proj.financials[i].ddsm_duration2 == null) proj.financials[i].ddsm_duration2 = 0;
                        tempDate.setTime((AccentGold.Date.addDays(tempDate, proj.financials[i].ddsm_duration2)).getTime());

                        objFncl.ddsm_targetend2 = proj.financials[i].ddsm_targetend2 = tempDate;
                        objFncl.ddsm_targetstart3 = proj.financials[i].ddsm_targetstart3 = tempDate;

                        if (proj.financials[i].ddsm_duration3 == null) proj.financials[i].ddsm_duration3 = 0;
                        tempDate.setTime((AccentGold.Date.addDays(tempDate, proj.financials[i].ddsm_duration3)).getTime());

                        objFncl.ddsm_targetend3 = proj.financials[i].ddsm_targetend3 = tempDate;
                        objFncl.ddsm_targetstart4 = proj.financials[i].ddsm_targetstart4 = tempDate;

                        if (proj.financials[i].ddsm_duration4 == null) proj.financials[i].ddsm_duration4 = 0;
                        tempDate.setTime((AccentGold.Date.addDays(tempDate, proj.financials[i].ddsm_duration4)).getTime());

                        objFncl.ddsm_targetend4 = proj.financials[i].ddsm_targetend4 = tempDate;
                        objFncl.ddsm_targetstart5 = proj.financials[i].ddsm_targetstart5 = tempDate;

                        if (proj.financials[i].ddsm_duration5 == null) proj.financials[i].ddsm_duration5 = 0;
                        tempDate.setTime((AccentGold.Date.addDays(tempDate, proj.financials[i].ddsm_duration5)).getTime());
                        objFncl.ddsm_targetend5 = proj.financials[i].ddsm_targetend5 = tempDate;


                        var valOther = 962080002;

                        if (proj.ddsm_payeepreference.Value == valOther) {

                            if (proj.ddsm_payeeaddress1 != null) {
                                objFncl.ddsm_payeeaddress1 = proj.ddsm_payeeaddress1;
                            }
                            if (proj.ddsm_payeeaddress2 != null) {
                                objFncl.ddsm_payeeaddress2 = proj.ddsm_payeeaddress2;
                            }
                            if (proj.ddsm_payeeattnto != null) {
                                objFncl.ddsm_payeeattnto = proj.ddsm_payeeattnto;
                            }
                            if (proj.ddsm_payeecity != null) {
                                objFncl.ddsm_payeecity = proj.ddsm_payeecity;
                            }
                            if (proj.ddsm_payeecompany != null) {
                                objFncl.ddsm_payeecompany = proj.ddsm_payeecompany;
                            }
                            if (proj.ddsm_payeephone != null) {
                                objFncl.ddsm_payeephone = proj.ddsm_payeephone;
                            }
                            if (proj.ddsm_payeestate != null) {
                                objFncl.ddsm_payeestate = proj.ddsm_payeestate;
                            }
                            if (proj.ddsm_payeestate != null) {
                                objFncl.ddsm_payeezipcode = proj.ddsm_payeestate;
                            }
                            if (proj.ddsm_taxid != null) {
                                objFncl.ddsm_taxid = proj.ddsm_taxid;
                            }
                            if (proj.ddsm_taxentity.Value != null) {
                                objFncl.ddsm_taxentity = proj.ddsm_taxentity;
                            }
                            if (proj.ddsm_payeeemail != null) {
                                objFncl.ddsm_payeeemail = proj.ddsm_payeeemail;
                            }
                            objFncl.ddsm_whogetspaid = {Value: 962080002};
                        } else {
                            switch (proj.financials[i].ddsm_whogetspaid.Value) {
                                case PYMTPREF_LPC:
                                    objFncl.ddsm_payeelpcid = new Object();
                                    objFncl.ddsm_payeelpcid.Id = proj.ddsm_LocalPowerCompanyId.Id;
                                    objFncl.ddsm_payeelpcid.LogicalName = proj.ddsm_LocalPowerCompanyId.LogicalName;
                                    objFncl.ddsm_payeelpcid.Name = proj.ddsm_LocalPowerCompanyId.Name;
                                    fnclFillPayeeInfo(objFncl, proj.ddsm_LocalPowerCompanyId.Id, "ddsm_localpowercompany", "ddsm_localpowercompanyid", MsgFlag, updateLog);
                                    break;
                                case PYMTPREF_LPC_SITE:
                                case PYMTPREF_SITE: //This should be the same as above.
                                    objFncl.ddsm_payeesiteid = new Object();
                                    objFncl.ddsm_payeesiteid.Id = proj.ddsm_ParentSiteId.Id;
                                    objFncl.ddsm_payeesiteid.LogicalName = proj.ddsm_ParentSiteId.LogicalName;
                                    objFncl.ddsm_payeesiteid.Name = proj.ddsm_ParentSiteId.Name;
                                    objFncl.ddsm_whogetspaid = {Value: 962080000};
                                    fnclFillPayeeInfo(objFncl, proj.ddsm_ParentSiteId.Id, "ddsm_site", "ddsm_siteId", MsgFlag, updateLog);
                                    break;
                                case PYMTPREF_PPN:
                                    objFncl.ddsm_payeeid = new Object();
                                    objFncl.ddsm_payeeid.Id = proj.ddsm_TradeAlly.Id;
                                    objFncl.ddsm_payeeid.LogicalName = proj.ddsm_TradeAlly.LogicalName;
                                    objFncl.ddsm_payeeid.Name = proj.ddsm_TradeAlly.Name;
                                    objFncl.ddsm_whogetspaid = {Value: 962080001};
                                    fnclFillPayeeInfo(objFncl, proj.ddsm_TradeAlly.Id, "Account", "AccountId", MsgFlag, updateLog);
                                    break;
                            }
                        }

                        for (var key in objFncl) {
                            proj.financials[i][key] = objFncl[key];
                        }

                        AccentGold.REST.updateRecord(proj.financials[i].ddsm_financialId, objFncl, "ddsm_financial", null, null, false);

                    }
                }
                return true;
            } else {
                return true;
                /*
                if(MsgFlag) { alert("Error: There is no project financial."); }
                updateLog.Log += "\\nError: There is no project financial.";
                return false;
                */
            }
} catch (err) {
    if(MsgFlag) { alert("Error: _projFncls_Kickoff - " + err); }
    updateLog.Log += "\\nError: _projFncls_Kickoff - " + err;
}
        }

        function _projFncls_Update (CMS) {  //CMS == Current Milestone
            console.log(">> Start _projFncls_Update");
            var initMS; //first this will be the starting index
            var initMSJson = -1; //This is the index of the Init MS in the JSON object
            var startDate = new Date(), thisStartDate = new Date();

            for (var i = 0; i < proj.financials.length; i++) {
                initMS = proj.financials[i].ddsm_initiatingmilestoneindex;
                var actStart = null;
                for (var j = 0; j < proj.milestones.length; j++) {
                    if (parseInt(initMS) == parseInt(proj.milestones[j].ddsm_Index)) {
                        initMSJson = j;
                        if (proj.milestones[j].ddsm_ActualEnd != null) {
                            actStart = proj.milestones[j].ddsm_ActualEnd;
                            startDate = proj.milestones[j].ddsm_ActualEnd;
                            break;
                        } else if (proj.milestones[j].ddsm_EstimatedEnd != null) {
                            startDate = proj.milestones[j].ddsm_EstimatedEnd;
                            break;
                        } else if (proj.milestones[j].ddsm_TargetEnd != null) {
                            startDate = proj.milestones[j].ddsm_TargetEnd;
                            break;
                        } else {
                            if(MsgFlag) {
                                alert("Error Updating Financial: " + proj.financials[i].ddsm_name + "\\nInitiating MS Target Date is missing");
                            }
                            updateLog.Log += "\\nError Updating Financial: " + proj.financials[i].ddsm_name + "\\nInitiating MS Target Date is missing";
                            return;
                        }
                    }
                }
                if (initMSJson == -1) {
                    if(MsgFlag) {
                        alert("Error Updating Financial:" + proj.financials[i].ddsm_name + "\\nCannot find initiating milestone (index=" + initMS + ")");
                    }
                    updateLog.Log += "\\nError Updating Financial:" + proj.financials[i].ddsm_name + "\\nCannot find initiating milestone (index=" + initMS + ")";
                    continue;
                }

                if (typeof proj.financials[i].ddsm_financialId != 'undefined' && parseInt(initMS) == CMS) {
                    startDate = new Date(startDate);
                    var objFncl = new Object();
                    if (proj.financials[i].ddsm_stagename1 != null) {
                        if (proj.financials[i].ddsm_initiatingmilestoneindex <= CMS && proj.financials[i].ddsm_status.Value == 962080000 &&
                            actStart != null) { //only run if CMS is past Init MS AND Status == Not Started
                            objFncl.ddsm_status = new Object();
                            objFncl.ddsm_status.Value = 962080001; // setting status = Active
                            proj.financials[i].ddsm_status = objFncl.ddsm_status;
                            objFncl.ddsm_pendingstage = proj.financials[i].ddsm_pendingstage = proj.financials[i].ddsm_stagename1;
                            objFncl.ddsm_actualstart1 = proj.financials[i].ddsm_actualstart1 = actStart;
                        }
                        objFncl.ddsm_targetstart1 = proj.financials[i].ddsm_targetstart1 = startDate;
                        thisStartDate.setTime((AccentGold.Date.addDays(startDate, proj.financials[i].ddsm_duration1)).getTime())
                        objFncl.ddsm_targetend1 = proj.financials[i].ddsm_targetend1 = thisStartDate;
                    }
                    if (proj.financials[i].ddsm_stagename2 != null) {
                        if (proj.financials[i].ddsm_actualend1 != null) {
                            thisStartDate.setTime(proj.financials[i].ddsm_actualend1);
                        }
                        objFncl.ddsm_targetstart2 = proj.financials[i].ddsm_targetstart2 = thisStartDate;
                        thisStartDate.setTime((AccentGold.Date.addDays(thisStartDate, proj.financials[i].ddsm_duration2)).getTime())
                        objFncl.ddsm_targetend2 = proj.financials[i].ddsm_targetend2 = thisStartDate;
                    }
                    if (proj.financials[i].ddsm_stagename3 != null) {
                        if (proj.financials[i].ddsm_actualend2 != null) {
                            thisStartDate.setTime(proj.financials[i].ddsm_actualend2);
                        }
                        objFncl.ddsm_targetstart3 = proj.financials[i].ddsm_targetstart3 = thisStartDate;
                        thisStartDate.setTime((AccentGold.Date.addDays(thisStartDate, proj.financials[i].ddsm_duration3)).getTime())
                        objFncl.ddsm_targetend3 = proj.financials[i].ddsm_targetend3 = thisStartDate;
                    }
                    if (proj.financials[i].ddsm_stagename4 != null) {
                        if (proj.financials[i].ddsm_actualend3 != null) {
                            thisStartDate.setTime(proj.financials[i].ddsm_actualend3);
                        }
                        objFncl.ddsm_targetstart4 = proj.financials[i].ddsm_targetstart4 = thisStartDate;
                        thisStartDate.setTime((AccentGold.Date.addDays(thisStartDate, proj.financials[i].ddsm_duration4)).getTime())
                        objFncl.ddsm_targetend4 = proj.financials[i].ddsm_targetend4 = thisStartDate;
                    }
                    if (proj.financials[i].ddsm_stagename5 != null) {
                        if (proj.financials[i].ddsm_actualend4 != null) {
                            thisStartDate.setTime(proj.financials[i].ddsm_actualend4);
                        }
                        objFncl.ddsm_targetstart5 = proj.financials[i].ddsm_targetstart5 = thisStartDate;
                        thisStartDate.setTime((AccentGold.Date.addDays(thisStartDate, proj.financials[i].ddsm_duration5)).getTime())
                        objFncl.ddsm_targetend5 = proj.financials[i].ddsm_targetend5 = thisStartDate;
                    }

                    AccentGold.REST.updateRecord(proj.financials[i].ddsm_financialId, objFncl, "ddsm_financial", null, function (err) {
                        if(MsgFlag) { alert("Error: measCopyPhase - " + err); }
                        updateLog.Log += "\\nError: measCopyPhase - " + err;
                    }, false);
                }
            }
        }

        ///

        function _projData_Update () {
            console.log(">> Start _projData_Update");
            var objProj = {};
            for(var key in proj) {
                if(key != "milestones" && key != "financials" && key != "measures" && key != "ddsm_projectId" && key != "docs_u" && key != "docs_c" && key != "AutoFncCreationIndex" && key != "fnclTplId") {
                    objProj[key] = proj[key];
                }
            }
            AccentGold.REST.updateRecord(proj.ddsm_projectId, objProj, "ddsm_project", null, function (err) {
                if(MsgFlag) { alert("Error: _projData_Update - " + err); }
                updateLog.Log += "\\nError: _projData_Update - " + err;
            }, false);
        }

    };

    var measCopyPhase = function (measures, phaseCurrent, phaseNext, MsgFlag, updateLog) {
        console.log(">> Start measCopyPhase");
        var pref_ph = "ddsm_phase",
            measureFieldNames= ["units", "perunitkw", "perunitkwh", "perunitthm", "savingskw", "savingskwh", "incentiveunits", "incentivetotal", "perunitsavingskwwinter", "clientsavingskw", "incentivepartner", "incentiveelectric", "incentivegas", "SavingsCCF" /*, "PerUnitCCF" */];
        if(measures.length > 0){
            for(var i = 0; i < measures.length; i++){
                var objMeas = {};
                for(var j = 0; j < measureFieldNames.length; j++) {
                    objMeas[pref_ph + phaseNext + measureFieldNames[j]] = measures[i][pref_ph + phaseNext + measureFieldNames[j]] = measures[i][pref_ph + phaseCurrent + measureFieldNames[j]];
                }
                objMeas.ddsm_UpToDatePhase = measures[i].ddsm_UpToDatePhase = phaseNext;

                AccentGold.REST.updateRecord(measures[i].ddsm_measureId, objMeas, "ddsm_measure", null, function (err) {
                    if(MsgFlag) { alert("Error: measCopyPhase - " + err); }
                    updateLog.Log += "\\nError: measCopyPhase - " + err;
                }, true);
            }
        }
        return measures;
    };

    var calcMeasSums = function (proj) {
        console.log(">> Start calcMeasSums");
        try {
            var p1SavingsKW = 0.0;
            var p1SavingsKWh = 0.0;
            var p1Savingsthm = 0.0;
            var p1IncentTtl = 0.0;
            var p2SavingsKW = 0.0;
            var p2SavingskWh = 0.0;
            var p2Savingsthm = 0.0;
            var p2IncentTtl = 0.0;
            var p3SavingsKW = 0.0;
            var p3SavingskWh = 0.0;
            var p3Savingsthm = 0.0;
            var p3IncentTtl = 0.0;
            var CurrSavingsKW = 0.0;
            var CurrSavingskWh = 0.0;
            var CurrSavingsthm = 0.0;
            var CurrIncentTtl = 0.0;
            var CurrSavingsWinterKW = 0.0;
            var p1SavingsWinterKW = 0.0;
            var p2SavingsWinterKW = 0.0;
            var p3SavingsWinterKW = 0.0;
            var cost = 0.0;
            var incCost = 0.0;

            var CustomInc = 0.0;
            var CustomKW = 0.0;
            var CustomkWh = 0.0;
            var NCInc = 0.0;
            var NCKW = 0.0;
            var NCkWh = 0.0;
            var RCxInc = 0.0;
            var RCxKW = 0.0;
            var RCxkWh = 0.0;
            var StandardInc = 0.0;
            var StandardKW = 0.0;
            var StandardkWh = 0.0;

            var commitCustomkWh = 0.0;
            var commitCustomKW = 0.0;
            var commitCustomInc = 0.0;
            var commitStandardkWh = 0.0;
            var commitStandardKW = 0.0;
            var commitStandardInc = 0.0;
            var commitNCkWh = 0.0;
            var commitNCKW = 0.0;
            var commitNCInc = 0.0;
            var commitRCxkWh = 0.0;
            var commitRCxKW = 0.0;
            var commitRCxInc = 0.0;

            var p1SavingsCCF = 0.0;
            var p2SavingsCCF = 0.0;
            var CurrSavingsCCF = 0.0;

            if (proj.measures.length == 0) {
                //console.log("No measure");
                //SetZeroValues();
                return proj;
            } else {
                for (var x in proj.measures) {//Loop the Result Set
                    var meas = new Object();
                    meas = proj.measures[x];
                    if (!isNaN(parseFloat(meas.ddsm_phase1savingskw))) {
                        p1SavingsKW += parseFloat(meas.ddsm_phase1savingskw);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_phase1savingskwh))) {
                        p1SavingsKWh += parseFloat(meas.ddsm_phase1savingskwh);
                    }
                    if (meas.ddsm_phase1savingsthm != null && !isNaN(parseFloat(meas.ddsm_phase1savingsthm))) {
                        p1Savingsthm += parseFloat(meas.ddsm_phase1savingsthm);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_incentivepaymentgross.Value))) {
                        p1IncentTtl += parseFloat(meas.ddsm_incentivepaymentgross.Value);
                    }

                    if (!isNaN(parseFloat(meas.ddsm_phase2savingskw))) {
                        p2SavingsKW += parseFloat(meas.ddsm_phase2savingskw);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_phase2savingskwh))) {
                        p2SavingskWh += parseFloat(meas.ddsm_phase2savingskwh);
                    }
                    if (meas.ddsm_phase2savingsthm != null && !isNaN(parseFloat(meas.ddsm_phase2savingsthm))) {
                        p2Savingsthm += parseFloat(meas.ddsm_phase2savingsthm);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_phase2incentivetotal.Value))) {
                        p2IncentTtl += parseFloat(meas.ddsm_phase2incentivetotal.Value);
                    }

                    if (!isNaN(parseFloat(meas.ddsm_phase3savingskw))) {
                        p3SavingsKW += parseFloat(meas.ddsm_phase3savingskw);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_phase3savingskwh))) {
                        p3SavingskWh += parseFloat(meas.ddsm_phase3savingskwh);
                    }
                    if (meas.ddsm_phase3savingsthm != null && !isNaN(parseFloat(meas.ddsm_phase3savingsthm))) {
                        p3Savingsthm += parseFloat(meas.ddsm_phase3savingsthm);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_phase3incentivetotal.Value))) {
                        p3IncentTtl += parseFloat(meas.ddsm_phase3incentivetotal.Value);
                    }

                    if (!isNaN(parseFloat(meas.ddsm_currentsavingskw))) {
                        CurrSavingsKW += parseFloat(meas.ddsm_currentsavingskw);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_CurrentSavingskWh))) {
                        CurrSavingskWh += parseFloat(meas.ddsm_CurrentSavingskWh);
                    }
                    if (meas.ddsm_CurrentSavingsthm != null && !isNaN(parseFloat(meas.ddsm_CurrentSavingsthm))) {
                        CurrSavingsthm += parseFloat(meas.ddsm_CurrentSavingsthm);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_CurrentIncTotal.Value))) {
                        CurrIncentTtl += parseFloat(meas.ddsm_CurrentIncTotal.Value);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_CostTotal.Value))) {
                        cost += parseFloat(meas.ddsm_CostTotal.Value);
                    }

                    if (!isNaN(parseFloat(meas.ddsm_currentsavingsclientkw))) {
                        CurrSavingsWinterKW += parseFloat(meas.ddsm_currentsavingsclientkw);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_phase1clientsavingskw))) {
                        p1SavingsWinterKW += parseFloat(meas.ddsm_phase1clientsavingskw);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_phase2clientsavingskw))) {
                        p2SavingsWinterKW += parseFloat(meas.ddsm_phase2clientsavingskw);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_phase3clientsavingskw))) {
                        p3SavingsWinterKW += parseFloat(meas.ddsm_phase3clientsavingskw);
                    }
                    //if(meas.ddsm_StudyMeasure == "No") {
                    if (!isNaN(parseFloat(meas.ddsm_IncrementalCost.Value))) {
                        incCost += parseFloat(meas.ddsm_IncrementalCost.Value);
                    }
                    //}

                    if (!isNaN(parseFloat(meas.ddsm_Phase1SavingsCCF))) {
                        p1SavingsCCF += parseFloat(meas.ddsm_Phase1SavingsCCF);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_Phase2SavingsCCF))) {
                        p2SavingsCCF += parseFloat(meas.ddsm_Phase2SavingsCCF);
                    }
                    if (!isNaN(parseFloat(meas.ddsm_CurrentSavingsCCF))) {
                        CurrSavingsCCF += parseFloat(meas.ddsm_CurrentSavingsCCF);
                    }

                    switch (meas.ddsm_Program.Value) {
                        case 962080000:
                            if (!isNaN(parseFloat(meas.ddsm_CurrentIncTotal.Value))) {
                                CustomInc += parseFloat(meas.ddsm_CurrentIncTotal.Value);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_currentsavingskw))) {
                                CustomKW += parseFloat(meas.ddsm_currentsavingskw);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_CurrentSavingskWh))) {
                                CustomkWh += parseFloat(meas.ddsm_CurrentSavingskWh);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_phase2savingskwh))) {
                                commitCustomkWh += parseFloat(meas.ddsm_phase2savingskwh);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_phase2savingskw))) {
                                commitCustomKW += parseFloat(meas.ddsm_phase2savingskw);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_phase2incentivetotal.Value))) {
                                commitCustomInc += parseFloat(meas.ddsm_phase2incentivetotal.Value);
                            }
                            break;

                        case 962080001:
                            if (!isNaN(parseFloat(meas.ddsm_CurrentIncTotal.Value))) {
                                StandardInc += parseFloat(meas.ddsm_CurrentIncTotal.Value);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_currentsavingskw))) {
                                StandardKW += parseFloat(meas.ddsm_currentsavingskw);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_CurrentSavingskWh))) {
                                StandardkWh += parseFloat(meas.ddsm_CurrentSavingskWh);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_phase2savingskwh))) {
                                commitStandardkWh += parseFloat(meas.ddsm_phase2savingskwh);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_phase2savingskw))) {
                                commitStandardKW += parseFloat(meas.ddsm_phase2savingskw);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_phase2incentivetotal.Value))) {
                                commitStandardInc += parseFloat(meas.ddsm_phase2incentivetotal.Value);
                            }
                            break;

                        case 962080003:
                            if (!isNaN(parseFloat(meas.ddsm_CurrentIncTotal.Value))) {
                                RCxInc += parseFloat(meas.ddsm_CurrentIncTotal.Value);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_currentsavingskw))) {
                                RCxKW += parseFloat(meas.ddsm_currentsavingskw);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_CurrentSavingskWh))) {
                                RCxkWh += parseFloat(meas.ddsm_CurrentSavingskWh);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_phase2savingskwh))) {
                                commitRCxkWh += parseFloat(meas.ddsm_phase2savingskwh);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_phase2savingskw))) {
                                commitRCxKW += parseFloat(meas.ddsm_phase2savingskw);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_phase2incentivetotal.Value))) {
                                commitRCxInc += parseFloat(meas.ddsm_phase2incentivetotal.Value);
                            }
                            break;

                        case 962080004:
                            if (!isNaN(parseFloat(meas.ddsm_CurrentIncTotal.Value))) {
                                NCInc += parseFloat(meas.ddsm_CurrentIncTotal.Value);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_currentsavingskw))) {
                                NCKW += parseFloat(meas.ddsm_currentsavingskw);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_CurrentSavingskWh))) {
                                NCkWh += parseFloat(meas.ddsm_CurrentSavingskWh);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_phase2savingskwh))) {
                                commitNCkWh += parseFloat(meas.ddsm_phase2savingskwh);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_phase2savingskw))) {
                                commitNCKW += parseFloat(meas.ddsm_phase2savingskw);
                            }
                            if (!isNaN(parseFloat(meas.ddsm_phase2incentivetotal.Value))) {
                                commitNCInc += parseFloat(meas.ddsm_phase2incentivetotal.Value);
                            }
                            break;
                    }
                }

                if (p1SavingsKW == 0 && p1SavingsKWh == 0 && p1Savingsthm == 0 && p1IncentTtl == 0) {
                    proj.ddsm_CurrentIncentiveTotal = {};
                    proj.ddsm_CurrentIncentiveTotal.Value = parseFloat(CurrIncentTtl).toFixed(10);
                    proj.ddsm_CurrentSavingskWh = parseFloat(CurrSavingskWh).toFixed(10);
                    proj.ddsm_CurrentSavingsKW = parseFloat(CurrSavingsKW).toFixed(10);
                    proj.ddsm_CurrentSavingsthm = parseFloat(CurrSavingsthm).toFixed(10);
                    //return proj;
                }

                proj.ddsm_Phase1SavingsKW = parseFloat(p1SavingsKW).toFixed(10);
                proj.ddsm_Phase1SavingskWh = parseFloat(p1SavingsKWh).toFixed(10);
                proj.ddsm_Phase1Savingsthm = parseFloat(p1Savingsthm).toFixed(10);
                proj.ddsm_Phase1IncentiveTotal = {};
                proj.ddsm_Phase1IncentiveTotal.Value = parseFloat(p1IncentTtl).toFixed(10);

                proj.ddsm_Phase2SavingskWh = parseFloat(p2SavingsKW).toFixed(10);
                proj.ddsm_Phase2SavingskWh = parseFloat(p2SavingskWh).toFixed(10);
                proj.ddsm_Phase2Savingsthm = parseFloat(p2Savingsthm).toFixed(10);
                proj.ddsm_Phase2IncentiveTotal = {};
                proj.ddsm_Phase2IncentiveTotal.Value = parseFloat(p2IncentTtl).toFixed(10);

                proj.ddsm_Phase3Savingsthm = parseFloat(p3Savingsthm).toFixed(10);
                proj.ddsm_Phase3IncentiveTotal = {};
                proj.ddsm_Phase3IncentiveTotal.Value = parseFloat(p3IncentTtl).toFixed(10);

                proj.ddsm_CurrentSavingsKW = parseFloat(CurrSavingsKW).toFixed(10);
                proj.ddsm_CurrentSavingskWh = parseFloat(CurrSavingskWh).toFixed(10);
                proj.ddsm_CurrentSavingsthm = parseFloat(CurrSavingsthm).toFixed(10);
                proj.ddsm_CurrentIncentiveTotal = {};
                proj.ddsm_CurrentIncentiveTotal.Value = parseFloat(CurrIncentTtl).toFixed(10);

                proj.ddsm_IncrementalInstalledCosts = {};
                proj.ddsm_IncrementalInstalledCosts.Value = parseFloat(incCost).toFixed(10);

                //return proj;
            }
        } catch (err) {
            alert(err);
        }

    };

    var fnclCalcPymt = function (objFncl, financials, measures, adminData, actualEnd, calcType, MsgFlag, updateLog) {
        console.log(">> Start fnclCalcPymt");
        var PROGTYPE_CUST = 962080000,
            PROGTYPE_STD = 962080001,
            savkWh = 0.0,
            savKW = 0.0,
            savthm = 0.0,
            incentive = 0.0,
            incCust = 0.0,
            incStd = 0.0,
            study = 0.0,
            savCCF = 0.0;

        try {
            for (var i in meas) {
                if (measures[i].ddsm_CurrentSavingskWh != null) {
                    if (!isNaN(parseFloat(measures[i].ddsm_CurrentSavingskWh))) {
                        savkWh += parseFloat(measures[i].ddsm_CurrentSavingskWh);
                    }
                }
                if (measures[i].ddsm_currentsavingskw != null) {
                    if (!isNaN(parseFloat(measures[i].ddsm_currentsavingskw))) {
                        savKW += parseFloat(measures[i].ddsm_currentsavingskw);
                    }
                }
                if (measures[i].ddsm_CurrentSavingsthm != null) {
                    if (!isNaN(parseFloat(measures[i].ddsm_CurrentSavingsthm))) {
                        savthm += parseFloat(measures[i].ddsm_CurrentSavingsthm);
                    }
                }
                if (measures[i].ddsm_CurrentIncTotal.Value != null) {
                    if (!isNaN(parseFloat(measures[i].ddsm_CurrentIncTotal.Value))) {
                        if (measures[i].ddsm_StudyMeasure == true) {
                            study += parseFloat(measures[i].ddsm_CurrentIncTotal.Value); //This is a Study Measure
                        } else {
                            incentive += parseFloat(measures[i].ddsm_CurrentIncTotal.Value); //This is NOT a Study Measure
                            if (measures[i].ddsm_Program.Value == PROGTYPE_CUST) {
                                incCust += parseFloat(measures[i].ddsm_CurrentIncTotal.Value); //This is a Custom Measure
                            } else if (measures[i].ddsm_Program.Value == PROGTYPE_STD) {
                                incStd += parseFloat(measures[i].ddsm_CurrentIncTotal.Value); //This is a Standard Measure
                            }
                        }
                    }
                }
                if (measures[i].ddsm_CurrentSavingsCCF != null) {
                    if (!isNaN(parseFloat(measures[i].ddsm_CurrentSavingsCCF))) {
                        savCCF += parseFloat(measures[i].ddsm_CurrentSavingsCCF);
                    }
                }
            }

            var perfAmtCommitTtl_OLD = 0.0,
                validOldCommit = false;

            for (var i in financials) {
                if (financials[i].ddsm_calculationtype.Value == 962080000 && financials[i].ddsm_CalculatedAlready == false && financials[i].ddsm_calculatedamount != null) {
                    perfAmtCommitTtl_OLD += financials[i].ddsm_calculatedamount.Value;
                    validOldCommit = true;
                }
            }

            var perfRateCommitkWh = parseFloat(adminData.ddsm_PerfRateCommitkWh.Value),
                perfRateCommitKW = parseFloat(adminData.ddsm_PerfRateCommitKW.Value),
                perfRateCommitthm = parseFloat(adminData.ddsm_PerfRateCommitthm.Value),
                perfRateInstallkWh = 0,
                perfRateInstallKW = 0,
                perfRateInstallthm = 0,
                tier = parseInt(adminData.ddsm_PerformanceTier);

            switch (tier) {
                case 1:
                    perfRateInstallkWh = parseFloat(adminData.ddsm_PerfRateInstalledkWh1.Value);
                    perfRateInstallKW = parseFloat(adminData.ddsm_PerfRateInstalledKW1.Value);
                    perfRateInstallthm = parseFloat(adminData.ddsm_PerfRateInstalledthm1.Value);
                    break;
                case 2:
                    perfRateInstallkWh = parseFloat(adminData.ddsm_PerfRateInstalledkWh2.Value);
                    perfRateInstallKW = parseFloat(adminData.ddsm_PerfRateInstalledKW2.Value);
                    perfRateInstallthm = parseFloat(adminData.ddsm_PerfRateInstalledthm2.Value);
                    break;
                case 3:
                    perfRateInstallkWh = parseFloat(adminData.ddsm_PerfRateInstalledkWh3.Value);
                    perfRateInstallKW = parseFloat(adminData.ddsm_PerfRateInstalledKW3.Value);
                    perfRateInstallthm = parseFloat(adminData.ddsm_PerfRateInstalledthm3.Value);
                    break;
            }

            perfRateCommitkWh = (isNaN(perfRateCommitkWh)) ? 0 : perfRateCommitkWh;
            perfRateCommitKW = (isNaN(perfRateCommitKW)) ? 0 : perfRateCommitKW;
            perfRateCommitthm = (isNaN(perfRateCommitthm)) ? 0 : perfRateCommitthm;
            perfRateInstallkWh = (isNaN(perfRateInstallkWh)) ? 0 : perfRateInstallkWh;
            perfRateInstallKW = (isNaN(perfRateInstallKW)) ? 0 : perfRateInstallKW;
            perfRateInstallthm = (isNaN(perfRateInstallthm)) ? 0 : perfRateInstallthm;

            var progYear = actualEnd.getFullYear(),
                NTG = 0;

            if (progYear == null || progYear < 2013) {
                progYear = 2013;
            }

            switch (progYear) {
                case 2013:
                    if (!isNaN(parseFloat(adminData.ddsm_NTG2013Eval))) {
                        NTG = parseFloat(adminData.ddsm_NTG2013Eval);
                    } else if (!isNaN(parseFloat(adminData.ddsm_NTG2013Est))) {
                        NTG = parseFloat(adminData.ddsm_NTG2013Est);
                    } else {
                        if(MsgFlag) {
                            alert("Error: fnclCalcPymt - No valid NTG for program year. Please contact your system administrator.");
                        }
                        updateLog.Log += "\\nError: fnclCalcPymt - No valid NTG for program year. Please contact your system administrator.";
                        return 0;
                    }
                    break;
                case 2014:
                    if (!isNaN(parseFloat(adminData.ddsm_NTG2014Eval))) {
                        NTG = parseFloat(adminData.ddsm_NTG2014Eval);
                    } else if (!isNaN(parseFloat(adminData.ddsm_NTG2014Est))) {
                        NTG = parseFloat(adminData.ddsm_NTG2014Est);
                    } else {
                        if(MsgFlag) {
                            alert("Error: fnclCalcPymt - No valid NTG for program year. Please contact your system administrator.");
                        }
                        updateLog.Log += "\\nError: fnclCalcPymt - No valid NTG for program year. Please contact your system administrator.";
                        return 0;
                    }
                    break;
                case 2015:
                    if (!isNaN(parseFloat(adminData.ddsm_NTG2015Eval))) {
                        NTG = parseFloat(adminData.ddsm_NTG2015Eval);
                    } else if (!isNaN(parseFloat(adminData.ddsm_NTG2015Est))) {
                        NTG = parseFloat(adminData.ddsm_NTG2015Est);
                    } else {
                        if(MsgFlag) {
                            alert("Error: fnclCalcPymt - No valid NTG for program year. Please contact your system administrator.");
                        }
                        updateLog.Log += "\\nError: fnclCalcPymt - No valid NTG for program year. Please contact your system administrator.";
                        return 0;
                    }
                    break;
            }

            var perfAmtCommitkWh = savkWh * NTG * perfRateCommitkWh,
                perfAmtCommitKW = savKW * NTG * perfRateCommitKW,
                perfAmtCommitthm = savthm * NTG * perfRateCommitthm,
                perfAmtCommitCCF = 0;

            if (!isNaN(savCCF)) {
                perfAmtCommitCCF = savCCF * NTG * perfAmtCommitCCF;
            }

            var perfAmtCommitTtl = perfAmtCommitkWh + perfAmtCommitKW + perfAmtCommitthm + perfAmtCommitCCF,
                perfAmtInstallkWh = savkWh * NTG * perfRateInstallkWh,
                perfAmtInstallKW = savKW * NTG * perfRateInstallKW,
                perfAmtInstallthm = savthm * NTG * perfRateInstallthm,
                perfAmtInstallCCF = 0;

            if (!isNaN(savCCF)) {
                perfAmtInstallCCF = savCCF * NTG * perfAmtInstallCCF;
            }

            var perfAmtInstallTtl = perfAmtInstallkWh + perfAmtInstallKW + perfAmtInstallthm + perfAmtInstallCCF,
                calcAmt = 0;

            switch (calcType) {
                case 962080000: //performance Committment Payment
                    calcAmt = perfAmtCommitTtl;
                    break;
                case 962080001: //performance Installed Payment
                    calcAmt = perfAmtInstallTtl
                    break;
                case 962080002: //performance Commit + Installed Payment
                    calcAmt = perfAmtInstallTtl + perfAmtCommitTtl;
                    break;
                case 962080004: //Adjustment-Discontinued
                    calcAmt = 0;
                    break;
                case 962080005: //Measure Payment
                    calcAmt = incentive;
                    break;
                case 962080006: //Study Payment
                    calcAmt = study;
                    break;
                case 962080008: //Adjustment-Commit Update
                    calcAmt = perfAmtCommitTtl - perfAmtCommitTtl_OLD;
                    break;
                default:
                    calcAmt = 0;
            }

            var calculationdate = new Date();

            objFncl.ddsm_calculationdate = AccentGold.Date.zeroTime(calculationdate);
            objFncl.ddsm_performancetier = tier;
            objFncl.ddsm_ntgrate = NTG;
            objFncl.ddsm_incentiveamount = { Value: parseFloat(incentive) };
            objFncl.ddsm_prefratecommitkwh = { Value: parseFloat(perfRateCommitkWh) };
            objFncl.ddsm_prefratecommitkw = { Value: parseFloat(perfRateCommitKW) };
            objFncl.ddsm_prefratecommitthm = { Value: parseFloat(perfRateCommitthm) };
            objFncl.ddsm_perfrateinstallkwh = { Value: parseFloat(perfRateInstallkWh) };
            objFncl.ddsm_perfrateinstallkw = { Value: parseFloat(perfRateInstallKW) };
            objFncl.ddsm_perfrateinstallthm = { Value: parseFloat(perfRateInstallthm) };
            objFncl.ddsm_performanceamountcommittedactual = { Value: parseFloat(perfAmtCommitTtl_OLD) };
            objFncl.ddsm_performanceamountcommittedupdate = { Value: parseFloat(perfAmtCommitTtl) };
            objFncl.ddsm_performanceamountinstalled = { Value: parseFloat(perfAmtInstallTtl) };
            objFncl.ddsm_savingskwhgross = savkWh;
            objFncl.ddsm_savingskwgross = savKW;
            objFncl.ddsm_savingsthmgross = savthm;
            objFncl.ddsm_savingskwhnet = (savkWh * NTG);
            objFncl.ddsm_savingskwnet = (savKW * NTG);
            objFncl.ddsm_savingsthmnet = (savthm * NTG);
            objFncl.ddsm_incentivecustom = { Value: parseFloat(incCust) };
            objFncl.ddsm_incentivestandard = { Value: parseFloat(incStd) };
            objFncl.ddsm_calculatedamount = { Value: parseFloat(calcAmt) };
            objFncl.ddsm_actualamount = { Value: parseFloat(calcAmt) };
            objFncl.ddsm_CalculatedAlready = true;

            //return objFncl;
        }
        catch (err) {
            if(MsgFlag) { alert("Error: fnclCalcPymt - " + err); }
            updateLog.Log += "\\nError: fnclCalcPymt - " + err;
            //return objFncl;
        }
    };

    var fnclFillPayeeInfo = function (objFncl, id, entitySet, entityId, MsgFlag, updateLog) {
        console.log(">> Start fnclFillPayeeInfo");
        var strStateCode = "statecode",
            queryReq = "",
            reqData = [];
        try {

            if (entitySet == "ddsm_site") {
                queryReq = "$select="
                + "ddsm_payeeaddress1,"
                + "ddsm_payeeaddress2,"
                + "ddsm_payeeattnto,"
                + "ddsm_payeecity,"
                + "ddsm_payeecompany,"
                + "ddsm_payeephone,"
                + "ddsm_payeestate,"
                + "ddsm_payeezipcode,"
                + "ddsm_taxentity,"
                + "ddsm_taxid,"
                    //+ "ddsm_payeeaccountnumber,"
                    // + "ddsm_payeeroutingnumber,"
                + "ddsm_payeeemail"
                + "&$filter=" + entityId + " eq guid'" + id + "' and " + strStateCode + "/Value eq 0";

                AccentGold.REST.retrieveMultipleRecords(entitySet, queryReq, null, null, function(data){reqData = data;}, false, null);

                if (!!reqData[0]) {
                    objFncl.ddsm_payeeaddress1 = reqData[0].ddsm_payeeaddress1;
                    objFncl.ddsm_payeeaddress2 = reqData[0].ddsm_payeeaddress2;
                    objFncl.ddsm_payeeattnto = reqData[0].ddsm_payeeattnto;
                    objFncl.ddsm_payeecity = reqData[0].ddsm_payeecity;
                    objFncl.ddsm_payeecompany = reqData[0].ddsm_payeecompany;
                    objFncl.ddsm_payeephone = reqData[0].ddsm_payeephone;
                    objFncl.ddsm_payeestate = reqData[0].ddsm_payeestate;
                    objFncl.ddsm_payeezipcode = reqData[0].ddsm_payeezipcode;
                    objFncl.ddsm_taxid = reqData[0].ddsm_taxid;
                    objFncl.ddsm_taxentity = reqData[0].ddsm_taxentity;
                    // objFncl.ddsm_payeeaccountnumber = reqData[0].ddsm_payeeaccountnumber;
                    //  objFncl.ddsm_payeeroutingnumber = reqData.ddsm_payeeroutingnumber;
                    objFncl.ddsm_payeeemail = reqData[0].ddsm_payeeemail;
                }
            }

            else if (entitySet == "Account") {
                strStateCode = "StateCode";

                queryReq = "$select="
                + "ddsm_PayeeAddress1,"
                + "ddsm_PayeeAddress2,"
                + "ddsm_PayeeAttnTo,"
                + "ddsm_PayeeCity,"
                + "ddsm_PayeeCompany,"
                + "ddsm_PayeePhone,"
                + "ddsm_PayeeState,"
                + "ddsm_PayeeZipCode,"
                + "ddsm_TaxEntity,"
                + "ddsm_TaxID,"
                    // + "ddsm_PayeeAccountNumber,"
                    // + "ddsm_PayeeRoutingNumber,"
                + "ddsm_PayeeEmail"
                + "&$filter=" + entityId + " eq guid'" + id + "' and " + strStateCode + "/Value eq 0";

                AccentGold.REST.retrieveMultipleRecords(entitySet, queryReq, null, null, function(data){reqData = data;}, false, null);

                if (!!reqData[0]) {
                    objFncl.ddsm_payeeaddress1 = reqData[0].ddsm_PayeeAddress1;
                    objFncl.ddsm_payeeaddress2 = reqData[0].ddsm_PayeeAddress2;
                    objFncl.ddsm_payeeattnto = reqData[0].ddsm_PayeeAttnTo;
                    objFncl.ddsm_payeecity = reqData[0].ddsm_PayeeCity;
                    objFncl.ddsm_payeecompany = reqData[0].ddsm_PayeeCompany;
                    objFncl.ddsm_payeephone = reqData[0].ddsm_PayeePhone;
                    objFncl.ddsm_payeestate = reqData[0].ddsm_PayeeState;
                    objFncl.ddsm_payeezipcode = reqData[0].ddsm_PayeeZipCode;
                    objFncl.ddsm_taxid = reqData[0].ddsm_TaxID;
                    objFncl.ddsm_taxentity = reqData[0].ddsm_TaxEntity;
                    //  objFncl.ddsm_payeeaccountnumber = reqData[0].ddsm_PayeeAccountNumber;
                    //  objFncl.ddsm_payeeroutingnumber = reqData[0].ddsm_PayeeRoutingNumber;
                    objFncl.ddsm_payeeemail = reqData[0].ddsm_PayeeEmail;
                }
            }

            else if (entitySet == "ddsm_localpowercompany") {
                queryReq = "$select="
                + "ddsm_payeeaddress1,"
                + "ddsm_payeeaddress2,"
                + "ddsm_payeeattnto,"
                + "ddsm_payeecity,"
                + "ddsm_payeecompany,"
                + "ddsm_PayeePhone,"
                + "ddsm_payeestate,"
                + "ddsm_payeezipcode,"
                + "ddsm_taxentity,"
                + "ddsm_taxid,"
                    // + "ddsm_payeeaccountnumber,"
                    //  + "ddsm_payeeroutingnumber,"
                + "ddsm_payeeemail"
                + "&$filter=" + entityId + " eq guid'" + id + "' and " + strStateCode + "/Value eq 0";

                reqData = AccentGold.REST.retrieveMultipleRecords(entitySet, queryReq, null, null, function(data){reqData = data;}, false, null);

                if (!!reqData[0]) {
                    objFncl.ddsm_payeeaddress1 = reqData[0].ddsm_payeeaddress1;
                    objFncl.ddsm_payeeaddress2 = reqData[0].ddsm_payeeaddress2;
                    objFncl.ddsm_payeeattnto = reqData[0].ddsm_payeeattnto;
                    objFncl.ddsm_payeecity = reqData[0].ddsm_payeecity;
                    objFncl.ddsm_payeecompany = reqData[0].ddsm_payeecompany;
                    objFncl.ddsm_payeephone = reqData[0].ddsm_PayeePhone;
                    objFncl.ddsm_payeestate = reqData[0].ddsm_payeestate;
                    objFncl.ddsm_payeezipcode = reqData[0].ddsm_payeezipcode;
                    objFncl.ddsm_taxid = reqData[0].ddsm_taxid;
                    objFncl.ddsm_taxentity.Value = reqData[0].ddsm_taxentity;
                    //objFncl.ddsm_payeeaccountnumber = reqData[0].ddsm_payeeaccountnumber;
                    // objFncl.ddsm_payeeroutingnumber = reqData[0].ddsm_payeeroutingnumber;
                    objFncl.ddsm_payeeemail = reqData[0].ddsm_payeeemail;
                }
            }

        } catch (err) {
            if(MsgFlag) { alert("Error: fnclFillPayeeInfo - " + err); }
            updateLog.Log += "\\nError: fnclFillPayeeInfo - " + err;
        }
    };

    var recalcMsTplAverageDuration = function (msTplID, MsgFlag, updateLog){
        console.log(">> Start recalcMsTplAverageDuration " + msTplID);
        try {
            var countMs = 0, ActualDuration = 0.00;
            var queryReq = "$select=ddsm_ActualDuration"
                + "&$filter=ddsm_MsTpltoMs/Id eq guid'" + msTplID + "' and ddsm_Status/Value eq 962080002 and statecode/Value eq 0";

            var msReq;
            AccentGold.REST.retrieveMultipleRecords("ddsm_milestone", queryReq, null, null, function(data){msReq = data;}, false, null);

            for (var i = 0; i < msReq.length; i++) {
                ActualDuration += parseInt((msReq[i].ddsm_ActualDuration != null) ? msReq[i].ddsm_ActualDuration : 0);
                countMs++;
            }

            queryReq = "$select=ddsm_Duration"
            + "&$filter=ddsm_milestonetemplateId eq guid'" + msTplID + "' and statecode/Value eq 0";

            AccentGold.REST.retrieveMultipleRecords("ddsm_milestonetemplate", queryReq, null, null, function(data){msReq = data;}, false, null);

            if (countMs > 0) {
                var msTpl = {};
                msTpl.ddsm_AverageActualDuration = parseFloat(ActualDuration/countMs).toFixed(2);
                msTpl.ddsm_AverageDurationDifference = parseFloat(parseInt((msReq[0].ddsm_Duration != null) ? msReq[0].ddsm_Duration : 0) - parseFloat(ActualDuration/countMs)).toFixed(2);
                AccentGold.REST.updateRecord(msTplID, msTpl, "ddsm_milestonetemplate", null, function (err) {
                    if(MsgFlag) { alert("Error: recalcMsTplAverageDuration - " + err); }
                    updateLog.Log += "\\nError: recalcMsTplAverageDuration - " + err;
                }, false);
            }
        } catch (err) {
            if(MsgFlag) { alert("Error: recalcMsTplAverageDuration - " + err); }
            updateLog.Log += "\\nError: recalcMsTplAverageDuration - " + err;
        }

    };

    //Update Project Milestones Data
    var projMSsData_Update = function (milestone, MsgFlag, updateLog) {
        console.log(">> Start projMSsData_Update");
        try {
            var milestoneFieldNames = ["ddsm_Status", "ddsm_ActiveByCategory", "ddsm_TargetStart", "ddsm_ActualStart", "ddsm_TargetEnd", "ddsm_EstimatedEnd", "ddsm_ActualEnd", "ddsm_SkipMilestone", "ddsm_Duration", "ddsm_ActualDuration"],
                objMs = {};
            for(var i = 0; i < milestone.length; i++) {
                objMs = {};
                for(var j = 0; j < milestoneFieldNames.length; j++) {
                    objMs[milestoneFieldNames[j]] = milestone[i][milestoneFieldNames[j]];
                }
                //objMs.ddsm_ActualEnd = null;
                AccentGold.REST.updateRecord(milestone[i].ddsm_milestoneId, objMs, "ddsm_milestone", null, function (err) {
                    if(MsgFlag) { alert("Error: projMSsData_Update - " + err); }
                    updateLog.Log += "\\nError: projMSsData_Update - " + err;
                }, false);
            }
        } catch (err) {
            if(MsgFlag) { alert("Error: projMSsData_Update - " + err); }
            updateLog.Log += "\\nError: projMSsData_Update - " + err;
        }

    }

    // Escalation-Overdue programming
    var RecalculateCurrMSDaysPastDue = function (proj) {
        console.log(">> Start RecalculateCurrMSDaysPastDue");

        var daysPastDue, targetEnd, escalOverdueLength;
        for (var i = 0; i < proj.milestones.length; i++) {

            if(proj.milestones[i].ddsm_Status == 962080001) {
                targetEnd = proj.milestones[i].ddsm_TargetEnd;
                escalOverdueLength = (proj.milestones[i].ddsm_CurrEscalOverdueLength != null) ? proj.milestones[i].ddsm_CurrEscalOverdueLength : 0;
                var currDate = AccentGold.Date.zeroTime(new Date());

                daysPastDue = AccentGold.Date.differenceDates(targetEnd, currDate) + parseInt(escalOverdueLength);

                proj.ddsm_CurrEscalOverdueLength = escalOverdueLength;

                if (daysPastDue < 0) {
                    var escalatedToOverdue = new Array();
                    escalatedToOverdue[0] = new Object();

                    var queryReq = "?$select=ddsm_OverdueManager"
                        + "&$filter=ddsm_projecttemplateId eq guid'" + proj.ddsm_ProjectTemplateId.Id + "'";

                    var reqData;
                    AccentGold.REST.retrieveMultipleRecords("ddsm_projecttemplate", queryReq, null, null, function(data){reqData = data;}, false, null);
                    if (reqData[0] != null) {
                        escalatedToOverdue[0].id = reqData[0].ddsm_OverdueManager.Id;
                        escalatedToOverdue[0].name = reqData[0].ddsm_OverdueManager.Name;
                        escalatedToOverdue[0].entityType = reqData[0].ddsm_OverdueManager.LogicalName;
                        proj.ddsm_EscalatedToOverdue = escalatedToOverdue;
                    }
                    proj.ddsm_CurrentMSDaysPastDue = daysPastDue;

                    // SEND EMAILS
                    var toName = "", fromName = "", from;
                    var createEmail = new AccentGold.Soap.BusinessEntity("email");

                    if(!!AccentGold.Users.getUserId()) {
                        from = [{
                            id: (AccentGold.Users.getUserId()).replace(/\{|\}/g, ''),
                            logicalName: 'systemuser',
                            type: "EntityReference"
                        }];
                        fromName = AccentGold.Users.getUserName();
                    } else {
                        from = [{
                            id: (proj.CreatedBy.Id).replace(/\{|\}/g, ''),
                            logicalName: proj.CreatedBy.LogicalName,
                            type: "EntityReference"
                        }];
                        fromName = proj.CreatedBy.Name;
                    }
                    createEmail.attributes["from"] = new AccentGold.Soap.xrmEntityCollection(from);

                    if (proj.ddsm_EscalatedToOverdue.Id != null) {
                        var to = [{
                            id: (proj.ddsm_EscalatedToOverdue.Id).replace(/\{|\}/g, ''),
                            logicalName: proj.ddsm_EscalatedToOverdue.LogicalName,
                            type: "EntityReference"
                        }];
                        toName = proj.ddsm_EscalatedToOverdue.Name;
                        createEmail.attributes["to"] = new AccentGold.Soap.xrmEntityCollection(to);
                    }
                    if (proj.ddsm_TradeAllyContact.Id != null) {
                        if (typeof to != 'undefined') {
                            var cc = [{
                                id: (proj.ddsm_TradeAllyContact.Id).replace(/\{|\}/g, ''),
                                logicalName: proj.ddsm_TradeAllyContact.LogicalName,
                                type: "EntityReference"
                            }];
                            createEmail.attributes["cc"] = new AccentGold.Soap.xrmEntityCollection(cc);
                        } else {
                            var to = [{
                                id: (proj.ddsm_TradeAllyContact.Id).replace(/\{|\}/g, ''),
                                logicalName: proj.ddsm_TradeAllyContact.LogicalName,
                                type: "EntityReference"
                            }];
                            toName = proj.ddsm_TradeAllyContact.Name;
                            createEmail.attributes["to"] = new AccentGold.Soap.xrmEntityCollection(to);
                        }
                    }
                    if (typeof to != 'undefined') {
                        var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                        var url_proj = AccentGold.getServerUrl() + "/main.aspx?etn=ddsm_project&extraqs=&histKey=" + randomnumber + "&id="
                            + proj.ddsm_projectId + "&newWindow=true&pagetype=entityrecord";
                        createEmail.attributes["subject"] = "Overdue Project: " + proj.ddsm_name;

                        var textEmail = "A short note saying that a Project is overdue.<br/><br/>"
                            + "Project Name: " + proj.ddsm_name + "<br/>"
                            + "URL Project: " + "<a href='" + url_proj + "' target='_blank'>" + proj.ddsm_name + "</a>";
                        createEmail.attributes["description"] = AccentGold.Email.createEmailBody(null, "Overdue Project", toName, fromName, textEmail, proj.ddsm_projectId, "ddsm_project");
                        createEmail.attributes["directioncode"] = true;
                        var emailId = XrmServiceToolkit.Soap.Create(createEmail);
                        AccentGold.Email.sendEmailRequest(emailId);
                    }
                } else {
                    proj.ddsm_CurrentMSDaysPastDue = null;
                }
            }
        }
    };

    var AutoMeasureFncl = function (measures, actualStart, fnclTplId, InitMSIndex) {
        console.log(">> Start AutoMeasureFncl");

        var recordsCount = measures.length;
        var arr = [];
        if (recordsCount > 0) {
            for (var i = 0; i < recordsCount; i++) {
                if (!measures[i].ddsm_IncludedinFinancial) {
                    arr.push(measures[i]);
                }
            }
            if (arr.length > 0) {
                arr.sort(function (a, b) {
                    if ((a.get("ddsm_TradeAllyId")).toLowerCase() > (b.get("ddsm_TradeAllyId")).toLowerCase()) {
                        return 1;
                    }
                    if ((a.get("ddsm_TradeAllyId")).toLowerCase() < (b.get("ddsm_TradeAllyId")).toLowerCase()) {
                        return -1;
                    }
                    return 0;
                });
                if (arr.length > 1) {
                    var tmp_rec = [];
                    tmp_rec.push(arr[0]);
                    for(var i = 1; i < arr.length; i++){
                        if((tmp_rec[tmp_rec.length - 1].get("ddsm_TradeAllyId")).toLowerCase() == (arr[i].get("ddsm_TradeAllyId")).toLowerCase()){
                            tmp_rec.push(arr[i]);
                        } else {
                            createMeasureFncl(tmp_rec, actualStart, fnclTplId, InitMSIndex);
                            tmp_rec = [];
                            tmp_rec.push(arr[i]);
                            if(i == arr.length - 1) {createMeasureFncl(tmp_rec, actualStart, fnclTplId, InitMSIndex);}
                        }
                    }
                } else {
                    createMeasureFncl(arr, actualStart, fnclTplId, InitMSIndex);
                }
            }
        }
    };

    var createMeasureFncl = function (measures, actualStart, fnclTplId, InitMSIndex) {
        console.log(">> Start createMeasureFncl");

        var calculatedamount = 0;
        var startDate = AccentGold.Date.zeroTime(actualStart);
        for(var i = 0; i < measures.length; i++){
            calculatedamount += parseFloat(measures[i].ddsm_CurrentIncTotal.Value);
            var measObj = {};
            measObj.ddsm_IncludedinFinancial =  measures[i].ddsm_IncludedinFinancial = true;
            AccentGold.REST.updateRecord(measures[i].ddsm_measureId,measObj,"ddsm_measure", null, null, true);
        }

        var parentAccount = measures[0].ddsm_AccountId,
            parentSite= measures[0].ddsm_parentsite,
            parentProject = measures[0].ddsm_ProjectToMeasureId;

        var queryReq ="$select="
            + "ddsm_name,"
            + "ddsm_FinancialType,"
            + "ddsm_PaymentType,"
            + "ddsm_InitiatingMilestoneIndex,"
            + "ddsm_StageName1,"
            + "ddsm_StageName2,"
            + "ddsm_StageName3,"
            + "ddsm_StageName4,"
            + "ddsm_StageName5,"
            + "ddsm_Duration1,"
            + "ddsm_Duration2,"
            + "ddsm_Duration3,"
            + "ddsm_Duration4,"
            + "ddsm_Duration5,"
            + "ddsm_CalculationType,"
            + "ddsm_ProjectCompletableIndex"
            + "&$filter=ddsm_financialtemplateId eq guid'" + fnclTplId + "' and statecode/Value eq 0";

        var fnclTplReq;
        AccentGold.REST.retrieveMultipleRecords("ddsm_financialtemplate", queryReq, null, null, function(data){fnclTplReq = data;}, false, null);

        if(!!fnclTplReq[0]) {
            var date = AccentGold.Date.zeroTime(new Date());
            var objFncl = new Object();
            objFncl.ddsm_name = "Manual Incentive Payment - " + ('0' + (date.getMonth() + 1)).slice(-2) + ('0' + date.getDate()).slice(-2) + date.getFullYear();
            objFncl.ddsm_financialtype = new Object();
            objFncl.ddsm_financialtype.Value = fnclTplReq[0].ddsm_FinancialType.Value;
            objFncl.ddsm_status = new Object();
            objFncl.ddsm_status.Value = 962080001;
            //objFncl.ddsm_initiatingmilestoneindex = fnclTplReq[0].ddsm_InitiatingMilestoneIndex;
            objFncl.ddsm_stagename1 = fnclTplReq[0].ddsm_StageName1;
            objFncl.ddsm_stagename2 = fnclTplReq[0].ddsm_StageName2;
            objFncl.ddsm_stagename3 = fnclTplReq[0].ddsm_StageName3;
            objFncl.ddsm_stagename4 = fnclTplReq[0].ddsm_StageName4;
            objFncl.ddsm_stagename5 = fnclTplReq[0].ddsm_StageName5;
            objFncl.ddsm_duration1 = fnclTplReq[0].ddsm_Duration1;
            objFncl.ddsm_duration2 = fnclTplReq[0].ddsm_Duration2;
            objFncl.ddsm_duration3 = fnclTplReq[0].ddsm_Duration3;
            objFncl.ddsm_duration4 = fnclTplReq[0].ddsm_Duration4;
            objFncl.ddsm_duration5 = fnclTplReq[0].ddsm_Duration5;
            //objFncl.ddsm_calculationtype = fnclTplReq[0].ddsm_CalculationType;
            //objFncl.ddsm_projectcompletableindex = fnclTplReq[0].ddsm_ProjectCompletableIndex;
            objFncl.ddsm_financialtemplate = new Object();
            objFncl.ddsm_financialtemplate.Id = fnclTplId;
            objFncl.ddsm_financialtemplate.Name = fnclTplReq[0].ddsm_name;
            objFncl.ddsm_financialtemplate.LogicalName = "ddsm_financialtemplate";
            if (!!parentAccount) {objFncl.ddsm_accountid = parentAccount;}
            if (!!parentSite) {objFncl.ddsm_siteid = parentSite;}
            if (!!parentProject) {objFncl.ddsm_projecttofinancialid = parentProject;}
            objFncl.ddsm_pendingstage = objFncl.ddsm_stagename1;
            objFncl.ddsm_targetstart1 = startDate;
            objFncl.ddsm_actualstart1 = startDate;
            startDate = AccentGold.Date.addDays(startDate, objFncl.ddsm_duration1);
            objFncl.ddsm_targetend1 = startDate;
            objFncl.ddsm_targetstart2 = startDate;
            startDate = AccentGold.Date.addDays(startDate, objFncl.ddsm_duration2);
            objFncl.ddsm_targetend2 = startDate;
            objFncl.ddsm_targetstart3 = startDate;
            startDate = AccentGold.Date.addDays(startDate, objFncl.ddsm_duration3);
            objFncl.ddsm_targetend3 = startDate;
            objFncl.ddsm_targetstart4 = startDate;
            startDate = AccentGold.Date.addDays(startDate, objFncl.ddsm_duration4);
            objFncl.ddsm_targetend4 = startDate;
            objFncl.ddsm_calculatedamount = {Value: parseFloat(calculatedamount).toFixed(10)};
            objFncl.ddsm_initiatingmilestoneindex = InitMSIndex;


            var newFncl = AccentGold.REST.createRecord(objFncl, "ddsm_financial", null, function (err) {console.log(err);}, false);
            for(var i = 0; i < measures.length; i++){
                var measObj = {};
                measObj.ddsm_MeastoFnclId = measures[i].ddsm_MeastoFnclId = {Id: newFncl.ddsm_financialId, Name: newFncl.ddsm_name, LogicalName: "ddsm_financial"};
                AccentGold.REST.updateRecord(measures[i].ddsm_measureId, measObj, "ddsm_measure", null, null, false);
            }

        }

    };

    var rendererLog = function (logConteynerName, updateLog) {
        var status = (updateLog.Success) ? "<strong style='color:green'>" + updateLog.SuccessTxt + "</strong>" : "<strong style='color:red'>" + updateLog.SuccessTxt + "</strong>";
        var str = updateLog.Log;
        str = str.split("\\n").join("<br/>");
        var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
        var url = AccentGold.getServerUrl() + "/main.aspx?etn=ddsm_project&extraqs=&histKey=" + randomnumber + "&id=" + updateLog.Id + "&newWindow=true&pagetype=entityrecord";

        var htmlLog = "------------------------------------------------------------------------------------------"
            + "<br/>"
            + "<strong>Project Name: </strong>" + "<a href='" + url + "' target='_blank'>" + updateLog.Name + "</a>"
            + "<br/>"
            + "<strong>Status Update: </strong>" + status
            + "<br/>"
            + "<strong>Update Log: </strong>"
//            + (updateLog.Log).replace("\\n/g", "<br/>")
            + str
            + "<br/>"
            + "------------------------------------------------------------------------------------------"
            + "<br/>";


        $("#" + logConteynerName).append(htmlLog);

    }

    return {
        multiUpdateProjMSs: multiUpdateProjMSs,
        nextStepMilestone: nextStepMilestone,
        measCopyPhase: measCopyPhase,
        fnclCalcPymt: fnclCalcPymt,
        fnclFillPayeeInfo: fnclFillPayeeInfo,
        recalcMsTplAverageDuration: recalcMsTplAverageDuration,
        projMSsData_Update: projMSsData_Update,
        RecalculateCurrMSDaysPastDue: RecalculateCurrMSDaysPastDue,
        rendererLog: rendererLog
    };

})());
// 12/20/2015  : cleared off the code usage of removed fields of ddsm_project entity
// 12/25/2015  : cleared off the code usage of removed fields of ddsm_project entity
