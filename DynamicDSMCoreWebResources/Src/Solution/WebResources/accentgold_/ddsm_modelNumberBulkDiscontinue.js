function EnableRule(){
    return true;
}

function Action(ids, count){

    var currentWindow = parent.Xrm.Internal.isTurboForm() ? parent.window : window;
    GlobalJs = currentWindow.GlobalJs;

    if (typeof ($) === 'undefined') {
        $ = currentWindow.$;
        jQuery = currentWindow.jQuery;
    }
    
    var message;
    if (count > 0){
        message = "Discontinue " + count;// + " Models?"
        if (count == 1)
            message += " Model?";
        else if (count > 1)
            message += " Models?";

        currentWindow.Alert.show(message, null, [{ label: "OK", callback: OkAction }, { label: "Cancel" }], "QUESTION");
    } else {
        message = "Please select Models to discontinue"
        currentWindow.Alert.show(message, null, [{ label: "OK" }], "WARNING");
        return;
    }

    function OkAction(){
        injectWindow();

        var dialogContent = '<table id="dialog-table"> 	<tr> 		<th></th> 		<th>Make Inactive</th> 		<th>Set End Date to</th> 		<th>Only for Program Offering</th> 	</tr> 	<tr> 		<td>Model Number</td> 		<td class="dialog-chkbox"> 			<input type="checkbox" id="make-inactive-MN" data-bind="checked: MakeInactiveMN"> 		</td> 		<td> 			<input id="end-date-MN" data-role="datepicker" data-bind="value: EndDateMN"> 		</td> 		<td></td> 	  </tr> 	<tr> 		<td>SKU</td> 		<td class="dialog-chkbox"> 			<input type="checkbox" id="make-inactive-SKU" data-bind="checked: MakeInactiveSKU"> 		</td> 		<td> 			<input id="end-date-SKU" data-role="datepicker" data-bind="value: EndDateSKU"> 		</td> 		<td></td> 	</tr> 	<tr> 		<td>MN Approvals</td> 		<td class="dialog-chkbox"> 			<input type="checkbox" id="make-inactive-MNA" data-bind="checked: MakeInactiveMNA"> 		</td> 		<td> 			<input id="end-date-MNA" data-role="datepicker" data-bind="value: EndDateMNA"> 		</td> 		<td> 			<input id="only-PO-MNA"> 		</td> 	</tr> 	<tr> 		<td>SKU Approvals</td> 		<td class="dialog-chkbox"> 			<input type="checkbox" id="make-inactive-SKUA" data-bind="checked: MakeInactiveSKUA"> 		</td> 		<td> 			<input id="end-date-SKUA" data-role="datepicker" data-bind="value: EndDateSKUA"> 		</td> 		<td> 			<input id="only-PO-SKUA"> 		</td> 	</tr> </table>';

        var message = "Do you want to update selected and related records? You can't undo this action.";

        if (!$("#action_dialog", currentWindow.document).data("kendoDialog")){
            $("#action_dialog", currentWindow.document).kendoDialog({
                width: "500px",
                title: '<span class="dialog-title">Choose options for bulk edit Measure Library records</span>',
                closable: true,
                modal: true,
                content: dialogContent,
                close: function () {
                    $("#only-PO-MNA", currentWindow.document).data("kendoDropDownList").select(-1);
                    $("#only-PO-SKUA", currentWindow.document).data("kendoDropDownList").select(-1);
                },
                actions: [
                    { text: 'Cancel', action: function () { $("#action_dialog", currentWindow.document).data("kendoDialog").close(); } },
                    { text: 'Discontinue', primary: true, action: function(){
                        currentWindow.Alert.show(message, null, [{ label: "Update", callback: actionDiscontinue }, { label: "Cancel" }], "QUESTION");
                    } }
                ],
            });
        } else {
            $("#action_dialog", currentWindow.document).data("kendoDialog").open();
        }

        currentWindow.viewModel = kendo.observable({
            MakeInactiveMN: false,
            MakeInactiveSKU: false,
            MakeInactiveMNA: false,
            MakeInactiveSKUA: false,

            EndDateMN: null,
            EndDateSKU: null,
            EndDateMNA: null,
            EndDateSKUA: null,

            MNAonlyForProgramOfferingId: null,
            SKUAonlyForProgramOfferingId: null,
            ModelIds: ids
        });
        kendo.bind($("#dialog-table", currentWindow.document), viewModel);

        CreateDropdown("#only-PO-MNA", onSelectMNApo);
        CreateDropdown("#only-PO-SKUA", onSelectSKUApo);

        function onSelectMNApo(e){
            var dataItem = this.dataItem(e.item);
            currentWindow.viewModel.MNAonlyForProgramOfferingId = dataItem.Id;
        }

        function onSelectSKUApo(e){
            var dataItem = this.dataItem(e.item);
            currentWindow.viewModel.SKUAonlyForProgramOfferingId = dataItem.Id;
        }
    }

    function CreateDropdown(id, selectCallback){
        $(id, currentWindow.document).kendoDropDownList({
            dataTextField: "Label",
            dataValueField: "Id",
            optionLabel: "--",
            filter: "contains",
            select: selectCallback,
            dataSource: new currentWindow.kendo.data.DataSource({
                schema: {
                    data: function (response) {
                        var value = response.value;

                        var model = value.map(function (el) {
                            return {
                                Label: el.ddsm_name,
                                Id: el.ddsm_programofferingid,
                            }
                        });
                        return model;
                    }
                },
                sort: {
                    field: "Label",
                    dir: "asc"
                },
                transport: currentWindow.KendoHelper.CRM.transport(currentWindow.clientUrl + currentWindow.baseUrl + 'ddsm_programofferings?$select=ddsm_name'),
            }),  
        });
    }

    function actionDiscontinue(){
        debugger;
        var data = [{
          key: "BulkEditRequest",
          type: Process.Type.String,
          value: JSON.stringify(currentWindow.viewModel)
        }]

        currentWindow.showSpinner(true, null, "Updating...");

        Process.callAction("ddsm_ExecuteBulkModelDiscontinue", data,
            function(params) {
                currentWindow.showSpinner(false);

                var response = JSON.parse(params[0].value);

                if(response.IsSuccessfull){
                    var info = "";

                    if (response.MNCount > 1)
                        info += "<p>" + response.MNCount + " Model Numbers were updated</p>";
                    else if (response.MNCount == 1)
                        info += "<p>" + response.MNCount + " Model Number was updated</p>";

                    if (response.SKUCount > 1)
                        info += "<p>" + response.SKUCount + " SKUs were updated</p>";
                    else if (response.SKUCount == 1)
                        info += "<p>" + response.SKUCount + " SKU was updated</p>";

                    if (response.MNACount > 1)
                        info += "<p>" + response.MNACount + " Model Number Approvals were updated</p>";
                    else if (response.MNACount == 1)
                        info += "<p>" + response.MNACount + " Model Number Approval was updated</p>";

                    if (response.SKUACount > 1)
                        info += "<p>" + response.SKUACount + " SKU Approvals were updated</p>";
                    if (response.SKUACount == 1)
                        info += "<p>" + response.SKUACount + " SKU Approval was updated</p>";

                    if (response.MNCount == 0 && response.SKUCount == 0 && response.MNACount == 0 && response.SKUACount == 0)
                        info += "No Updates were made";

                    currentWindow.Alert.show("Models updated successfully", info, [{ label: "OK" }], "SUCCESS");
                } else{
                    currentWindow.Alert.show("Could not update Models", null, [{ label: "OK" }], "WARNING");
                }
 
                
            },
            function(e) {
                currentWindow.showSpinner(false);
                currentWindow.Alert.show("Error while updating Models", null, [{ label: "OK" }], "WARNING");
            }
        );
        
    }

    function create(htmlStr, htmlTag) {
        if (!htmlTag) {
            htmlTag = 'div';
        }
        var frag = currentWindow.document.createDocumentFragment(),
            temp = currentWindow.document.createElement(htmlTag);
        temp.innerHTML = htmlStr;
        while (temp.firstChild) {
            frag.appendChild(temp.firstChild);
        }
        return frag;
    }

    function injectWindow(){
        if ($('#action_dialog', currentWindow.document).length == 0){
            var dialogTemplate = '<div id="action_dialog"></div>';
            var dialogFragment = create(dialogTemplate);
            currentWindow.document.body.insertBefore(dialogFragment, currentWindow.document.body.childNodes[0]);
        }
    }
}

