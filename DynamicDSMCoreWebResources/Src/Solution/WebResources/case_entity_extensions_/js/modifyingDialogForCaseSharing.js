function addListenerToShare()
{
	var intervalOfAppearing = setInterval(function ()
	{
		if (window.parent.document.getElementById("InlineDialog_Iframe"))
		{
			var dialog = window.parent.document.getElementById("InlineDialog_Iframe").contentWindow.document.getElementById("tableUsers");
			if (dialog)
			{
				clearInterval(intervalOfAppearing);
				workWithDialogWindow();
			}
		}
	}, 100);
}
var baseOnClickAdduser;

function onClickAddUser()
{
	baseOnClickAdduser();
	wait(addListenerOnSecondDialog);
}

function workWithDialogWindow()
{
	var buttonAddUser = window.parent.document.getElementById("InlineDialog_Iframe").contentWindow.document.getElementById("_TBAddUsersToShare");
	if (typeof (baseOnClickAdduser) === "undefined")
	{
		baseOnClickAdduser = buttonAddUser.onclick;
	}
	buttonAddUser.onclick = onClickAddUser;
	var checkAll = window.parent.document.getElementById("InlineDialog_Iframe").contentWindow.document.getElementById("chkSelectAll");
	checkAll.onchange = function (e)
	{
		setForAllCheckBoxes(e.target.checked);
	}
	setForAllCheckBoxes(true);
	checkAll.checked = isAllSelected();
	var baseCancel = window.parent.document.getElementById("InlineDialog_Iframe")
		.contentWindow.document.getElementById("cmdDialogCancel").onclick;
	window.parent.document.getElementById("InlineDialog_Iframe")
		.contentWindow.document.getElementById("cmdDialogCancel").onclick = function ()
	{
		buttonAddUser.onclick = baseOnClickAdduser;
		baseOnClickAdduser = undefined;
		baseCancel();
	}
	var baseClose = window.parent.document.getElementById("InlineDialog_Iframe")
		.contentWindow.document.getElementById("btnCross").children[0].onclick;
	window.parent.document.getElementById("InlineDialog_Iframe")
		.contentWindow.document.getElementById("btnCross").children[0].onclick = function ()
	{
		buttonAddUser.onclick = baseOnClickAdduser;
		baseOnClickAdduser = undefined;
		baseClose();
	}
	var baseShare = window.parent.document.getElementById("InlineDialog_Iframe")
		.contentWindow.document.getElementById("butBegin").onclick;
	window.parent.document.getElementById("InlineDialog_Iframe")
		.contentWindow.document.getElementById("butBegin").onclick = function ()
	{
		if (window.parent.document.getElementById("InlineDialog_Iframe").contentWindow.document.getElementById("tableUsers").rows === null || window.parent.document.getElementById("InlineDialog_Iframe").contentWindow.document.getElementById("tableUsers").rows.length === 0)
		{
			alert("No one record was not selected!");
		}
		else
		{
			buttonAddUser.onclick = baseOnClickAdduser;
			baseOnClickAdduser = undefined;
			baseShare();
		}
	}
}

function wait(func)
{
	setTimeout(function ()
	{
		var element = window.parent.document.getElementById("InlineDialog1_Iframe").contentWindow.document.getElementById("butBegin");
		if (typeof (element) === "undefined" || element === null)
		{
			wait(func, element);
		}
		else
		{
			func();
		}
	}, 500);
}

function addListenerOnSecondDialog()
{
	var button = window.parent.document.getElementById("InlineDialog1_Iframe").contentWindow.document.getElementById("butBegin");
	var base = button.onclick;
	button.onclick = function ()
	{
		var rows = window.parent.document.getElementById("InlineDialog_Iframe").contentWindow.document.getElementById("tableUsers").rows;
		if (rows === null)
		{
			return;
		}
		var rowsnumber = rows.length;
		base();
		for (var i = rowsnumber; i < rows.length; i++)
		{
			rows[i].children[0].children[0].checked = true;
			checkedRow(rows[i], true);
		}
		window.parent.document.getElementById("InlineDialog_Iframe").contentWindow.document.getElementById("chkSelectAll").checked = isAllSelected();
	}
}

function checkedRow(row, value)
{
	row.firstChild.children[0].onchange = function (e)
	{
		checkedRow(e.target.parentElement.parentElement, e.target.checked);
	}
	for (var i = 0; i < row.children.length; i++)
	{
		if (!row.children[i].children[0].id.endsWith("selectUser") && row.children[i].children[0].className === "checkbox")
		{
			row.children[i].children[0].disabled = true;
			row.children[i].children[0].checked = value;
		}
	}
}

function setForAllCheckBoxes(value)
{
	var table = window.parent.document.getElementById("InlineDialog_Iframe").contentWindow.document.getElementById("tableUsers");
	if (table === null || table.rows === null || table.rows.length === 0)
	{
		return;
	}
	for (var i = 0; i < table.rows.length; i++)
	{
		table.rows[i].children[0].children[0].checked = value;
		checkedRow(table.rows[i], value)
	}
}

function isAllSelected()
{
	var table = window.parent.document.getElementById("InlineDialog_Iframe").contentWindow.document.getElementById("tableUsers");
	if (table === null || table.rows === null || table.rows.length === 0)
	{
		return false;
	}
	for (var i = 0; i < table.rows.length; i++)
	{
		if (table.rows[i].children[0].children[0].checked === false)
		{
			return false;
		}
	}
	return true;
}