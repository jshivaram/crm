function addLinkToForm() {
    function createLinkString(entityName, entityRecordId) {
        var baseUrl = Xrm.Page.context.getClientUrl();
        var fullUrl = `${baseUrl}/main.aspx?etn=${entityName}&pagetype=entityrecord&id=${entityRecordId}`;

        return fullUrl;
    }

    $(window.document).on('mousedown', function (e) {
        let $target = $(e.target);
        if ($target.is('span.ms-crm-Lookup-Item')) {
            e.preventDefault();
            e.stopPropagation();
            let entityName = $target.attr('otypename');
            let id = $target.attr('oid');

            if (!entityName || !id) {
                console.warn('Case form redirect module: id or entity name is undef, cannot redirect.');
                return;
            }

            if (typeof id === 'string') {
                id = id.substring(1, id.length - 1);
            } else {
                throw 'id is not string';
            }

            let link = createLinkString(entityName, id);
            window.open(link, "_blank");
        }
    });
}