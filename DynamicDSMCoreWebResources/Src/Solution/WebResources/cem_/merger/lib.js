

﻿function GetAttributeMetaData(entityName, Url) {
    var soapBody = "<soap:Body><Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">" +
                        "<request i:type=\"a:RetrieveEntityRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">" +
                        "<a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">" +
                        "<a:KeyValuePairOfstringanyType>" +
                          "<b:key>EntityFilters</b:key>" +
                          "<b:value i:type=\"c:EntityFilters\" xmlns:c=\"http://schemas.microsoft.com/xrm/2011/Metadata\">Attributes</b:value>" +
                        "</a:KeyValuePairOfstringanyType>" +
                        "<a:KeyValuePairOfstringanyType>" +
                          "<b:key>MetadataId</b:key>" +
                          "<b:value i:type=\"ser:guid\"  xmlns:ser=\"http://schemas.microsoft.com/2003/10/Serialization/\">00000000-0000-0000-0000-000000000000</b:value>" +
                        "</a:KeyValuePairOfstringanyType>" +
                        "<a:KeyValuePairOfstringanyType>" +
                          "<b:key>RetrieveAsIfPublished</b:key>" +
                          "<b:value i:type=\"c:boolean\" xmlns:c=\"http://www.w3.org/2001/XMLSchema\">true</b:value>" +
                        "</a:KeyValuePairOfstringanyType>" +
                        "<a:KeyValuePairOfstringanyType>" +
                          "<b:key>LogicalName</b:key>" +
                          "<b:value i:type=\"c:string\"   xmlns:c=\"http://www.w3.org/2001/XMLSchema\">" + entityName + "</b:value>" +
                        "</a:KeyValuePairOfstringanyType>" +
                        "</a:Parameters>" +
                        "<a:RequestId i:nil=\"true\" /><a:RequestName>RetrieveEntity</a:RequestName></request>" +
                    "</Execute></soap:Body>";
    var soapXml = "<soap:Envelope " +
                "  xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' " +
                "  xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
                "  xmlns:xsd='http://www.w3.org/2001/XMLSchema'>" +
                soapBody +
                "</soap:Envelope>";
    var xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    var postTo = Url + '/XRMServices/2011/Organization.svc/web';
    xmlhttp.open("POST", postTo , true);
    xmlhttp.setRequestHeader("Accept", "application/xml, text/xml, */*");
    xmlhttp.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
    xmlhttp.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute");
    xmlhttp.setRequestHeader("Content-Length", soapXml.length);
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                data['Attributes'] = $(xmlhttp.responseXML).find('c\\:Attributes');
            }
            else {
                data['Error'] = 'Error retrieving attribute meta data for entity';
            }
        }
    };
    xmlhttp.send(soapXml);
}
function isValidFieldType(t)
{
    switch (t) {
        case 'Uniqueidentifier':
        case 'Status':
            return false;
        default:
            return true;
    }
}
function getColumnValue(o, t, x) {
    try {
        if (o == null)
            return null;

        switch (t) {
            case 'Customer':
            case 'Lookup':
                return o.Name;
                break;
            case 'Memo':
            case 'String':
            case 'Boolean':
            case 'Integer':
                return o;
                break;
            case 'Money':
                return o.Value;
                break;
            case 'DateTime':
                var d = Date(parseInt(o));
                return d.toLocaleString();
            case 'Picklist':
                if (o.Value == null)
                    return null;
                return $(x).find("c\\:OptionSet c\\:Value:contains('" + o.Value + "')").parent().find('a\\:Label').first().text();
        }
    } catch (e) {
        alert('ERROR: ' + e.message);
    }
}
function GetMergeXml(PrimId, OtherFields) {
    try {
        var x = '<merge PrimaryId="' + PrimId + '">';
        $("#EntFields input[value='" + OtherFields + "']").each(function () {
            if (this.name != 'cem_ent_select' && this.checked)
                x += '<field column="' + this.name.toLowerCase() + '" />';
        });
        x += '</merge>';
        return x;
    } catch (e) {
        return null;
    }
}
function DoMerge() {
    try {
        if (confirm('Are you sure you want to merge these records?')) {
            var PrimId = null;
            var SecId = null;
            var OtherFieldsName = null;
            if ($('#Ent_Select1').is(':checked')) {
                PrimId = Id1;
                SecId = Id2;
                OtherFieldsName = 'Ent2';
            } else {
                PrimId = Id2;
                SecId = Id1;
                OtherFieldsName = 'Ent1';
            }

            var XmlText = GetMergeXml(PrimId, OtherFieldsName);
            if (XmlText == null)
                throw new Error('Xml text was null :(');

            var oObj = new Object();
            oObj.cem_mergexml = XmlText;
            var joObj = window.JSON.stringify(oObj);
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                async:false,
                data: joObj,
                url: serverUrl + "/xrmservices/2011/OrganizationData.svc/" + entitySetName + "(guid'" + SecId + "')",
                beforeSend: function (XMLHttpRequest) {
                    XMLHttpRequest.setRequestHeader("Accept", "application/json");
                    XMLHttpRequest.setRequestHeader("X-HTTP-Method", "MERGE");
                },
                success: function (data, textStatus, XmlHttpRequest) {
                    alert('Merge completed');
                    window.close();
                },
                error: function (e, textStatus, errorThrown) {
                    var eText = null;
                    var detail = $.parseJSON(e.responseText);
                    eText = detail.error.message.value;
                    alert('ERROR: Merge did not complete.' + "\n" + 'Detail: ' + eText);
                    window.close();
                }
            });
        }
    } catch (e) {
        alert('ERROR: ' + e.message);
    }
}
function LoadEntity(CacheName, Id, Url) {
    var q = Url += "/xrmservices/2011/OrganizationData.svc/" + entitySetName + "(guid'" + Id + "')?$select=*";
    $.ajax({
        url: q,
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: data,
        success: function (r) {
            data[CacheName] = r.d;
        }, error: function (e, textStatus, errorThrown) {
            data['Error'] = 'Error retrieving oData for entity: ' + CacheName + ' Id: ' + Id + ' - Detail: ' + e.responseText;
        }
    });
}
function EntitySelect(Ent) {
    $("#EntFields input[value='" + Ent + "']").each(function () {
        this.checked = true;
    });
}
function getQueryStringData(vals) {
    for (var i in vals) {
        vals[i] = vals[i].replace(/\+/g, " ").split("=");
    }
    for (var i in vals) {
        if (vals[i][0].toLowerCase() == "data") {
            parseDataValue(vals[i][1]);
        }
    }
}
function parseDataValue(datavalue) {
    if (datavalue != "") {
        var vals = new Array();
        vals = decodeURIComponent(datavalue).split("&");
        for (var i in vals) {
            vals[i] = vals[i].replace(/\+/g, " ").split("=");
            switch (vals[i][0]) {
                case 'sUrl':
                    serverUrl = vals[i][1];
                    break;
                case 'entTypeName':
                    entityName = vals[i][1];
                    break;
                case 'Id1':
                    Id1 = vals[i][1];
                    break;
                case 'Id2':
                    Id2 = vals[i][1];
                    break;
            }
        }
    }
}
function getSchemaName(Url, eName) {
    try {
        var sName = null;
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            async: false, datatype: "json", beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
            url: Url + "/xrmservices/2011/OrganizationData.svc/cem_customentitymergeconfigurationSet?$select=cem_Schemaname&$filter=cem_name eq '" + eName + "'",
            success: function (data, textStatus, XmlHttpRequest) {
                if (data.d.results.length == 0) {
                    alert('Could not find CEM Configuration record for entity: ' + eName);
                } else {
                    sName = data.d.results[0].cem_Schemaname;
                }
            }
        });
        return sName;
    } catch (e) {
        alert('ERROR: ' + e.message);
    }
}
