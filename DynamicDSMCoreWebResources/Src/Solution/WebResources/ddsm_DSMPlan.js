/**
 * Created by Yaroslav Khoroshenko on 4/20/16.
 */

function OnLoad() {
    var formLabel = Xrm.Page.ui.formSelector.getCurrentItem().getLabel();
    if (formLabel.indexOf("DDSM Information")>=0)
    {
        MakeButton(1); //Create Button with launch Plugin to Recalcualte by Phases
    }
    else if (formLabel.indexOf("E1 MOCKUP")>=0)
    {
        MakeButton(0); //Create Button to Recalcualte Forecast logic
    }
    else
    {
        MakeButton(0);
    }
    //MakeButton();
    //  ConvertToButton("ddsm_applyfilter", "Apply Filter", 80, applyFilter, "Apply Filter");
    //  Xrm.Page.ui.controls.get("ddsm_applyfilter").setVisible(false);
}


function boldLabel() {
    $("span[id^='WebResource_section_label_']").each(function(){$(this).css('font-weight', 'bold')});
}

function removeTabsPadding() {
    $('.ms-crm-tabcolumn1, .ms-crm-tabcolumn2').css('padding-left', '0');
    $('TABLE.ms-crm-FormSection').css({'padding-left': '0', 'padding-right': '0'});
}

function applyFilterPyramida(idBut) {
    //$("#"+idBut).hide();
    document.getElementById(idBut).disabled = true;
    AGS.Form.DDSMPlanPyramid.calcDSMPlans(null, function(status) {RefreshData(idBut);});

}
function RefreshData(id){
    //$("#"+id).show();
    document.getElementById(id).disabled = false;
    setTimeout(function(){ location.reload();}, 1000);
}

function applyFilterPhasesPyramida(idBut) {
    //$("#"+idBut).hide();
    /*document.getElementById(idBut).disabled = true;
     AGS.Form.DDSMPlanPyramid.calcProgramIntervals(null, function(status) {RefreshData(idBut);});*/
    document.getElementById(idBut).disabled = true;
    AGS.Form.DDSMPlanPyramidPhases.calcDSMPlans(null, function(status) {RefreshData(idBut);});

/*
    var ret;
    //$("#fieldddsm_applyfilter").hide();

    $('#overlay').show();
    showPreloader(true);

    var params = [];
    var i=0;
    params[i] = {
        key: "Target",
        type: Process.Type.EntityReference,
        value: {
            id: Xrm.Page.data.entity.getId(),
            entityType: "ddsm_dsmplan"
        }
    };


    var successCallback = function() {
        Process.callAction("ddsm_DSMPlanPhasesFilterChangeAction", params,
            function(params) {
                //  debugger;
                // Success
                for (var i = 0; i < params.length; i++) {
                    if (params[i].key == "Complete") {
                        if (params[i].value == "true") {
                            //Xrm.Page.data.refresh();
                            setTimeout(function(){ location.reload();}, 1000);
                            //setTimeout(function(){ WeightSetDefault(); }, 1000);
                            showPreloader(false);
                            $('#overlay').hide();
                        }
                    }
                }
                //showPreloader(false);
                //$('#overlay').hide();
            },
            function(e) {
                // Error
                console.log("error: " + e);
                showPreloader(false);
                $('#overlay').hide();
                //  $("#fieldddsm_applyfilter").hide();
            }
        );
    };

    var errorCallback = function(error) {
        console.log("Error: " + error);
        showPreloader(false);
        $('#overlay').hide();
    };

    Xrm.Page.data.save().then(successCallback, errorCallback);
    */
}

function MakeButton(forms) {

    var atrname = "ddsm_applyfilter";
    if (document.getElementById(atrname) !== null) {
        var fieldId = "field" + atrname;
        if (document.getElementById(fieldId) === null) {
            var elementId = document.getElementById(atrname + "_d");
            var div = document.createElement("div");
            div.style.width = "100px";
            div.style.textAlign = "right";
            div.style.display = "inline";
            //  elementId.appendChild(div, elementId);

            //old style
            //margin-left: 3px; width: 80; display: none;
            div.innerHTML = '<button id="' + fieldId + '"  type="button" style="display: none;" >Recalculate</button>';
            document.getElementById(atrname).style.width = "0%";
            elementId.innerHTML = div.innerHTML;
            //  $(atrname+"_cl").hide();
            document.getElementById(fieldId).onclick = function() {
                if (forms == 0) {
                    applyFilterPyramida(fieldId);
                }
                else
                {
                    applyFilterPhasesPyramida(fieldId);
                }
                //applyFilterPyramida(fieldId);
            };
            $("#"+fieldId).show();
        }
        // hide main button
        //Xrm.Page.ui.controls.get(atrname).setVisible(false);
    }
}

function showPreloader(show)
{
    if (show) {
// show overlay
        spinnerForm = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Loading data");
    }
    else {
        if(!!spinnerForm && !!spinnerForm.stop)
            spinnerForm.stop();
    }
/*
    //var spinnerForm = CreaLab.Spinner.spin(_targetSpinner);
    var _targetSpinner = document.getElementById("bodyTop1");

    if(show)
    {
        $("body").attr({"id": "bodyTop1"});


// show overlay
        var popupHTML = '<div id="overlay" style="display:none; position:fixed; z-index:998; opacity:0.5; filter:alpha(opacity=50); top:0;    right:0; left:0; bottom:0;background:#000000;"></div>';
        $('body').append(popupHTML);
        $('#overlay').show();
        // spinnerForm = CreaLab.Spinner.spin(_targetSpinner);
        var spinnerForm = CreaLab.Spinner.spin(_targetSpinner);
    }
    else
    {
        $('#overlay').hide();
        var spinnerForm = CreaLab.Spinner.spin(_targetSpinner);
        spinnerForm.stop();

    }
*/

}

function applyFilter() {

    //debugger;
    var ret;
    //$("#fieldddsm_applyfilter").hide();

    $('#overlay').show();
    showPreloader(true);

    var params = [];
    var i=0;
    params[i] = {
        key: "Target",
        type: Process.Type.EntityReference,
        value: {
            id: Xrm.Page.data.entity.getId(),
            entityType: "ddsm_dsmplan"
        }
    };
    /*i++;
     params[i] = {
     key: "ProgramOffering",
     type: Process.Type.EntityReference,
     value: {
     id: Xrm.Page.getAttribute("ddsm_programofferingsid").getValue()[0].id,
     entityType: "ddsm_programoffering"
     }
     };
     i++;

     params[i] = {
     key: "Program",
     type: Process.Type.EntityReference,
     value: {
     id: Xrm.Page.getAttribute("ddsm_programid").getValue()[0].id,
     entityType: "ddsm_program"
     }
     };
     i++;*/


    var successCallback = function() {
        Process.callAction("ddsm_DSMPlanFilterChangeAction", params,
            function(params) {
                //  debugger;
                // Success
                for (var i = 0; i < params.length; i++) {
                    if (params[i].key == "Complete") {
                        if (params[i].value == "true") {
                            Xrm.Page.data.refresh();
                            //setTimeout(function(){ WeightSetDefault(); }, 1000);
                            //showPreloader(false);
                            $('#overlay').hide();
                        }
                        showPreloader(false);
                    }
                }
                //showPreloader(false);
                //$('#overlay').hide();
            },
            function(e) {
                // Error
                console.log("error: " + e);
                showPreloader(false);
                $('#overlay').hide();
                //  $("#fieldddsm_applyfilter").hide();
            }
        );
    };

    var errorCallback = function(error) {
        console.log("Error: " + error);
        showPreloader(false);
        $('#overlay').hide();
    };
    Xrm.Page.data.save().then(successCallback, errorCallback);


}