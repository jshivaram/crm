var intervalVerify = null;
function onLoad()
{
    $("#notesWallContainer .deleteNotesAction").remove();
    $("#notesWallContainer .deleteAttachmentAction").remove();

    var pageValue = Xrm.Page.getAttribute("ddsm_statusfiledatauploading").getValue();
    if(parseInt(pageValue) == 962080002 || parseInt(pageValue) == 962080005 || parseInt(pageValue) == 962080022 || parseInt(pageValue) == 962080023) {
        $("#FormTitle h1.ms-crm-TextAutoEllipsis").css("font-weight", "500");
        $("#FormTitle h1.ms-crm-TextAutoEllipsis").css("color", "red");

        $(".processStepsContent .processStepColumn .processStepLabel span span").css("color", "red");
        $(".processStepsContent .processStepColumn .processStepLabel span span").css("font-weight", "500");
    } else if(parseInt(pageValue) == 962080021){
        $("#FormTitle h1.ms-crm-TextAutoEllipsis").css("font-weight", "500");
        $("#FormTitle h1.ms-crm-TextAutoEllipsis").css("color", "green");

        $(".processStepsContent .processStepColumn .processStepLabel span span").css("color", "green");
        $(".processStepsContent .processStepColumn .processStepLabel span span").css("font-weight", "500");
    }

    intervalVerify = setInterval(verifyStatus,10000);
}
function onSave(){
    setTimeout(function(){
        $("#processStagesScrollRegion div.processStageContainer.selectedStage.completedStage").addClass("unselectedStage");
        $("#processStagesScrollRegion div.processStageContainer.selectedStage.completedStage").removeClass("selectedStage");
        $("#processStagesScrollRegion div.processStageContainer.activeStage").removeClass("unselectedStage");
        $("#processStagesScrollRegion div.processStageContainer.activeStage").addClass("selectedStage");

        $(".processStepsContent").width(450);
        $(".processStepsContent .processStepColumn").width(450);
        $(".processStepsContent .processStepColumn .processStep").width(450);
        $(".processStepsContent .processStepColumn .processStepValue").width(450 - 31 - $(".processStepsContent .processStepColumn .processStepLabel").width());

        var tmpWidth = $(".processStagesContainer").width();
        tmpWidth = tmpWidth + 91;
        $(".processStagesContainer").width(tmpWidth);
    }, 250);
}
function verifyStatus(){
    var pageValue = Xrm.Page.getAttribute("ddsm_statusfiledatauploading").getValue();
    if(parseInt(pageValue) == 962080002 || parseInt(pageValue) == 962080005 || parseInt(pageValue) == 962080021 || parseInt(pageValue) == 962080022 || parseInt(pageValue) == 962080023) {

        if(parseInt(pageValue) == 962080002 || parseInt(pageValue) == 962080005 || parseInt(pageValue) == 962080022 || parseInt(pageValue) == 962080023) {
            $("#FormTitle h1.ms-crm-TextAutoEllipsis").css("font-weight", "500");
            $("#FormTitle h1.ms-crm-TextAutoEllipsis").css("color", "red");

            $(".processStepsContent .processStepColumn .processStepLabel span span").css("color", "red");
            $(".processStepsContent .processStepColumn .processStepLabel span span").css("font-weight", "500");
        } else if(parseInt(pageValue) == 962080021){
            $("#FormTitle h1.ms-crm-TextAutoEllipsis").css("font-weight", "500");
            $("#FormTitle h1.ms-crm-TextAutoEllipsis").css("color", "green");

            $(".processStepsContent .processStepColumn .processStepLabel span span").css("color", "green");
            $(".processStepsContent .processStepColumn .processStepLabel span span").css("font-weight", "500");
        }

        clearInterval(intervalVerify);
        return;
    }

    AGS.REST.retrieveRecord(
        Xrm.Page.data.entity.getId(),
        'ddsm_datauploader',
        'ddsm_StatusFileDataUploading',
        null,
        function (data) {
            //console.log("pageValue: " + pageValue + " dbValue: " + data.ddsm_StatusFileDataUploading.Value);
            if(parseInt(data.ddsm_StatusFileDataUploading.Value) != parseInt(pageValue)) {
                if(parseInt(data.ddsm_StatusFileDataUploading.Value) == 962080021) location.reload();
                pageValue = data.ddsm_StatusFileDataUploading.Value;
                Xrm.Page.data.refresh().then(function(){
                    setTimeout(function(){
                        $("#processActionsContainer").remove();
                        $("#leftScrollButton").remove();
                        $("#rightScrollButton").remove();

                        $("#processStagesScrollRegion div.processStageContainer.completedStage").removeClass("selectedStage");
                        $("#processStagesScrollRegion div.processStageContainer.completedStage").removeClass("unselectedStage");
                        $("#processStagesScrollRegion div.processStageContainer.completedStage").addClass("unselectedStage");
                        $("#processStagesScrollRegion div.processStageContainer.activeStage").removeClass("unselectedStage");
                        $("#processStagesScrollRegion div.processStageContainer.activeStage").addClass("selectedStage");

                        $(".processStepsContent").width(450);
                        $(".processStepsContent .processStepColumn").width(450);
                        $(".processStepsContent .processStepColumn .processStep").width(450);
                        $(".processStepsContent .processStepColumn .processStepValue").width(450 - 31 - $(".processStepsContent .processStepColumn .processStepLabel").width());

                        var tmpWidth = $(".processStagesContainer").width();
                        tmpWidth = tmpWidth + 91;
                        $(".processStagesContainer").width(tmpWidth);

                    }, 250);

                }, function(){
                    setTimeout(function(){
                        $("#processActionsContainer").remove();
                        $("#leftScrollButton").remove();
                        $("#rightScrollButton").remove();

                        $("#processStagesScrollRegion div.processStageContainer.completedStage").removeClass("selectedStage");
                        $("#processStagesScrollRegion div.processStageContainer.completedStage").removeClass("unselectedStage");
                        $("#processStagesScrollRegion div.processStageContainer.completedStage").addClass("unselectedStage");
                        $("#processStagesScrollRegion div.processStageContainer.activeStage").removeClass("unselectedStage");
                        $("#processStagesScrollRegion div.processStageContainer.activeStage").addClass("selectedStage");

                        $(".processStepsContent").width(450);
                        $(".processStepsContent .processStepColumn").width(450);
                        $(".processStepsContent .processStepColumn .processStep").width(450);
                        $(".processStepsContent .processStepColumn .processStepValue").width(450 - 31 - $(".processStepsContent .processStepColumn .processStepLabel").width());

                        var tmpWidth = $(".processStagesContainer").width();
                        tmpWidth = tmpWidth + 91;
                        $(".processStagesContainer").width(tmpWidth);

                    }, 250);

                })
            }
        },
        function (err) {
            console.log(err)
        },
        false
    );

}


function registerBusinessProcessEvents() {

    var StepsField = Xrm.Page.data.process.getActiveStage().getSteps().getAll();
    for(var i = 0; i < StepsField.length; i++) {
        //var fieldName = StepsField[i].getAttribute();
        if(!!StepsField[i])
            Xrm.Page.getControl("header_process_" + StepsField[i].getAttribute()).setDisabled(true);
    }

    $("#stageBackActionContainer").unbind("click"); //Back
    $("#stageAdvanceActionContainer").unbind("click"); //Next
    $("#stageSetActiveActionContainer").unbind("click"); //Set Active
    $(".processStageContainer").unbind("click"); //Stage Conteyner
    $("#stageBackActionContainer").click(function (e){e.preventDefault();});
    $("#stageAdvanceActionContainer").click(function (e){e.preventDefault();});
    $("#stageSetActiveActionContainer").click(function (e){e.preventDefault();});
    $(".processStageContainer").click(function (e){e.preventDefault();});
    $("#processActionsContainer").remove();
    $("#leftScrollButton").remove();
    $("#rightScrollButton").remove();

    var tmpWidth = $(".processStagesContainer").width();
    tmpWidth = tmpWidth + 91;
    $(".processStagesContainer").width(tmpWidth)

    $( window ).resize(function() {
        setTimeout(function(){

            $("#processStagesScrollRegion div.processStageContainer.completedStage").removeClass("selectedStage");
            $("#processStagesScrollRegion div.processStageContainer.completedStage").removeClass("unselectedStage");
            $("#processStagesScrollRegion div.processStageContainer.completedStage").addClass("unselectedStage");
            $("#processStagesScrollRegion div.processStageContainer.activeStage").removeClass("unselectedStage");
            $("#processStagesScrollRegion div.processStageContainer.activeStage").addClass("selectedStage");

            $(".processStepsContent").width(450);
            $(".processStepsContent .processStepColumn").width(450);
            $(".processStepsContent .processStepColumn .processStep").width(450);
            $(".processStepsContent .processStepColumn .processStepValue").width(450 - 31 - $(".processStepsContent .processStepColumn .processStepLabel").width());


            var tmpWidth = $(".processStagesContainer").width();
            tmpWidth = tmpWidth + 91;
            $(".processStagesContainer").width(tmpWidth)
        }, 250);
    });
}