function onLoad()
{
    if(!!Xrm.Page.getAttribute("ddsm_contact").getValue())
        Xrm.Page.getControl("ddsm_contact").setVisible(true);

    if(!!Xrm.Page.getAttribute("ddsm_account").getValue())
        Xrm.Page.getControl("ddsm_account").setVisible(true);

    if(!!Xrm.Page.getAttribute("ddsm_site").getValue())
        Xrm.Page.getControl("ddsm_site").setVisible(true);

    if(!!Xrm.Page.getAttribute("ddsm_project").getValue())
        Xrm.Page.getControl("ddsm_project").setVisible(true);

    if(!!Xrm.Page.getAttribute("ddsm_projectgroup").getValue())
        Xrm.Page.getControl("ddsm_projectgroup").setVisible(true);

    if(!!Xrm.Page.getAttribute("ddsm_measure").getValue())
        Xrm.Page.getControl("ddsm_measure").setVisible(true);

    if(!!Xrm.Page.getAttribute("ddsm_projectreporting").getValue())
        Xrm.Page.getControl("ddsm_projectreporting").setVisible(true);

    if(!!Xrm.Page.getAttribute("ddsm_measurereporting").getValue())
        Xrm.Page.getControl("ddsm_measurereporting").setVisible(true);
}