//************************************************************************/
// Filename: ddsm_Fncl.js
// Usage: Contains the scripts associated with Fncls
//************************************************************************/

var FNCL_NUM_STAGES = 5;


function Fncl_OnLoad() {
    Xrm.Page.ui.controls.get("ddsm_payeeaccount").setVisible(false);
    Xrm.Page.ui.controls.get("ddsm_payeeid").setVisible(false);
    if(Xrm.Page.getAttribute("ddsm_pendingstage").getValue()=="Completed"){
          disableFormFields(true);
    }
    Fncl_HideEmptyStages(false);
    //OMAHA DEMO
    HideFields();
}

function onChangePendingStage()
{
  if(Xrm.Page.getAttribute("ddsm_pendingstage").getValue()!=="Completed"){
    return;
  }
  Xrm.Page.data.entity.save();
  disableFormFields(true);
}

function GetActualStage()
{
    return;
    for(i=1; i<=5;i++)
    {
     var dataEnd = Xrm.Page.getAttribute("ddsm_actualend"+i).getValue();
        if (dataEnd == null && !!Xrm.Page.getAttribute("ddsm_stagename"+i).getValue())
        {
            var stage = Xrm.Page.getAttribute("ddsm_stagename"+i).getValue();
            Xrm.Page.getAttribute("ddsm_pendingstage").setValue(stage);
            Xrm.Page.data.entity.save();
            return;
        }
    }
}

function Fncl_ActEnd_OnChange(eventContext) {
    //console.log("Fncl_ActEnd_OnChange().....");
// debugger;
    try {
        var fieldIndex = parseInt(eventContext.getEventSource().getName().substr(14));
        //console.log("fieldIndex: " + fieldIndex);
        var ActEnd = eventContext.getEventSource();
        var ActStart = Xrm.Page.getAttribute("ddsm_actualstart" + fieldIndex);

        var proj_ID = Xrm.Page.getAttribute("ddsm_projecttofinancialid").getValue()[0].id;   // Parent Project

        var isValid = true;
        var today = new Date();
        var dtActEnd;
        var dtTargEnd;
        var dtNextTargEnd;
        if (ActStart.getValue() == null) {
            alert("Sorry, the Actual Start shouldn't be empty.");
            Xrm.Page.getAttribute("ddsm_actualend" + fieldIndex).setValue(null);
            return;
        }

        var actStartClean = new Date(dateToMDY(ActStart.getValue()));
        var actEndClean = new Date(dateToMDY(ActEnd.getValue()));

        // Validity checks///////////////////////////
        if (Xrm.Page.getAttribute("ddsm_status").getValue() == 962080000) {
            alert("This Financial record has not yet been kicked off. Please initiate it by moving forward the Stage on the Project prior to editing this Financial.");
            isValid = false;
        } else if (ActStart.getValue() == null) {
            alert("Actual Start Date must be filled in before filling in Actual End Date for this Stage");
            isValid = false;
            //Check: Actual Start <= Actual End
        }
/*        else if (actStartClean > actEndClean) {
            alert("Actual End Date cannot be before the associated Actual Start Date. Please enter a valid Date.");
            isValid = false;
            //Check: Actual End <= Today
        } else if (ActEnd.getValue() > today) {
            alert("Actual End Date cannot be in the future. Please enter a valid Date.");
            isValid = false;
        }
*/
        if (!isValid) {
            ActEnd.setValue(null);
            return;
        }

        //Complete Project if necessary or update Active Stage if correct
        if (Xrm.Page.getAttribute("ddsm_projectcompletableindex").getValue() == fieldIndex) {
            //console.log("ddsm_projectcompletableindex == fieldIndex, it's " + fieldIndex);
            Fncl_UpdateProjectCompletable(proj_ID, ActEnd.getValue());
        }

        if (fieldIndex == FNCL_NUM_STAGES) {
            Xrm.Page.getAttribute("ddsm_status").setValue(962080002); //set to complete if last stage
            Xrm.Page.getAttribute("ddsm_pendingstage").setValue("Completed");
        } else if (fieldIndex < FNCL_NUM_STAGES && Xrm.Page.getAttribute("ddsm_stagename" + (fieldIndex + 1)).getValue() == null) {
            Xrm.Page.getAttribute("ddsm_status").setValue(962080002); //set to complete if next stage name is empty
            Xrm.Page.getAttribute("ddsm_pendingstage").setValue("Completed");
        } else if (fieldIndex < FNCL_NUM_STAGES && Xrm.Page.getAttribute("ddsm_pendingstage").getValue() == Xrm.Page.getAttribute("ddsm_stagename" + fieldIndex).getValue()) {
            Xrm.Page.getAttribute("ddsm_pendingstage").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_pendingstage").setValue(Xrm.Page.getAttribute("ddsm_stagename" + (fieldIndex + 1)).getValue());
        }



        //loop through date updates///////////////////////////
        dtActEnd = new Date();
        dtTargEnd = new Date();
        dtNextTargEnd = new Date();
        for (var i = 2; i <= FNCL_NUM_STAGES; i++) {
            if (Xrm.Page.getAttribute("ddsm_stagename" + i).getValue() == null) {
                continue;
            }
            if (Xrm.Page.getAttribute("ddsm_actualend" + (i - 1)).getValue() != null) {
                dtActEnd = Xrm.Page.getAttribute("ddsm_actualend" + (i - 1)).getValue();
                dtNextTargEnd = new Date(dtActEnd.getTime() + Xrm.Page.getAttribute("ddsm_duration" + i).getValue() * 86400000);
                Xrm.Page.getAttribute("ddsm_actualstart" + i).setValue(dtActEnd);
                Xrm.Page.getAttribute("ddsm_targetstart" + i).setValue(dtActEnd);
                Xrm.Page.getAttribute("ddsm_targetend" + i).setValue(dtNextTargEnd);
                //tempStartDate.setDate(tempStartDate.getDate() + ms_array[j[k]].ddsm_Duration);
            } else {
                dtTargEnd = Xrm.Page.getAttribute("ddsm_targetend" + (i - 1)).getValue();
                dtNextTargEnd = new Date(dtTargEnd.getTime() + Xrm.Page.getAttribute("ddsm_duration" + i).getValue() * 86400000);
                Xrm.Page.getAttribute("ddsm_targetstart" + i).setValue(dtTargEnd);
                Xrm.Page.getAttribute("ddsm_targetend" + i).setValue(dtNextTargEnd);
            }
        }
        Xrm.Page.getAttribute("ddsm_pendingstage").fireOnChange();
        //Fncl_UpdateFncls(proj_ID);
        //alert("It worked!");
    }
    catch (err) {
        alert("Error: " + err + ".");
    }
}


function Fncl_InvoiceDate_onChange() {
    var projId = Xrm.Page.getAttribute("ddsm_projecttofinancialid").getValue()[0].id;
    var invoicedate = Xrm.Page.getAttribute("ddsm_invoicedate").getValue();
    if (invoicedate != null) {
        var proj_obj = new Object();
        proj_obj.ddsm_invoicedate = new Date(invoicedate);
        updateRecord(projId, proj_obj, "ddsm_projectSet", null, null);
    }
}


//updated 7/18/2014, 7/24/2014
function Fncl_UpdateFncls(projId, CMS) { //CMS == Current Milestone

    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    //Query if this ID is already in use
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projectSet?"
    		+ "$select=ddsm_name,"
    		+ "ddsm_ddsm_project_ddsm_milestone/ddsm_TargetEnd,"
    		+ "ddsm_ddsm_project_ddsm_milestone/ddsm_EstimatedEnd,"
    		+ "ddsm_ddsm_project_ddsm_milestone/ddsm_ActualEnd,"
    		+ "ddsm_ddsm_project_ddsm_milestone/ddsm_Index,"
    		+ "ddsm_ddsm_project_ddsm_milestone/ddsm_name,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_financialId,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_initiatingmilestoneindex,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_name,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_status,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_stagename1,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_stagename2,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_stagename3,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_stagename4,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_stagename5,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_duration1,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_duration2,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_duration3,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_duration4,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_duration5,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_actualend1,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_actualend2,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_actualend3,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_actualend4,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_actualend5"
    		+ "&$filter=ddsm_projectId eq guid'" + projId + "'"
    		+ "&$expand=ddsm_ddsm_project_ddsm_milestone,ddsm_ddsm_project_ddsm_financial", false);

    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();

    if (req.readyState == 4 && req.status == 200) {
        var proj = JSON.parse(req.responseText).d;
        if (proj.results[0] != null) {
            var Fncls = proj.results[0].ddsm_ddsm_project_ddsm_financial.results;
            var MSs = proj.results[0].ddsm_ddsm_project_ddsm_milestone.results;

            //console.log("Fncls...");
            //console.dir(Fncls);

            var initMS; //first this will be the starting index
            var initMSJson = -1; //This is the index of the Init MS in the JSON object
            var startDate = null;
            var thisStartDate = null;

       if (Fncls.length > 0) {
            for (var i in Fncls) {
                //alert("Fncl Name: " + Fncls[i].ddsm_name);
                initMS = Fncls[i].ddsm_initiatingmilestoneindex;
                var actStart = null;
                for (var j in MSs) {
                    //alert("MS Name: " + MSs[i].ddsm_name);
                    //console.log("initMS: " + initMS + "| MSs[" + j + "].ddsm_Index: " + MSs[j].ddsm_Index);
                    if (parseInt(initMS) == parseInt(MSs[j].ddsm_Index)) {
                        initMSJson = j;
                        //console.log(MSs[j].ddsm_ActualEnd);
                        if (MSs[j].ddsm_ActualEnd != null) {
                            //console.log(">>>> ddsm_ActualEnd");
                            //startDate = new Date(parseInt((MSs[j].ddsm_ActualEnd).substr(6)));
                            actStart = eval((MSs[j].ddsm_ActualEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                            startDate = eval((MSs[j].ddsm_ActualEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                            break;
                        } else if (MSs[j].ddsm_EstimatedEnd != null) {
                            //console.log(">>>> ddsm_EstimatedEnd");
                            //startDate = new Date(parseInt((MSs[j].ddsm_EstimatedEnd).substr(6)));
                            startDate = eval((MSs[j].ddsm_EstimatedEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                            break;
                        } else if (MSs[j].ddsm_TargetEnd != null) {
                            //console.log(">>>> ddsm_TargetEnd");
                            //startDate = new Date(parseInt((MSs[j].ddsm_TargetEnd).substr(6)));
                            startDate = eval((MSs[j].ddsm_TargetEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                            break;
                        } else {
                            alert("Error Updating Financial: " + Fncls[i].ddsm_name +
	        						"\nInitiating MS Target Date is missing");
                            return;
                        }
                    }
                }
                if (initMSJson == -1) {
                    alert("Error Updating Financial:" + Fncls[i].ddsm_name +
	        				"\nCannot find initiating milestone (index=" + initMS + ")");
                    continue;
                }

                if (typeof Fncls[i].ddsm_financialId != 'undefined' && parseInt(initMS) == CMS) {
                    startDate = new Date(startDate);
                    var fncl_obj = new Object();
                    if (Fncls[i].ddsm_stagename1 != null) {
                        if (Fncls[i].ddsm_initiatingmilestoneindex <= CMS && Fncls[i].ddsm_status.Value == 962080000 &&
                                actStart != null) { //only run if CMS is past Init MS AND Status == Not Started
                            fncl_obj.ddsm_status = new Object();
                            fncl_obj.ddsm_status.Value = 962080001; // setting status = Active
                            fncl_obj.ddsm_pendingstage = Fncls[i].ddsm_stagename1;
                            fncl_obj.ddsm_actualstart1 = actStart;
                        }
                        //console.log(startDate);
                        fncl_obj.ddsm_targetstart1 = new Date(startDate.getTime());
                        thisStartDate = new Date(startDate.getTime() + (Fncls[i].ddsm_duration1 * 86400000));
                        fncl_obj.ddsm_targetend1 = new Date(thisStartDate.getTime());
                    }
                    if (Fncls[i].ddsm_stagename2 != null) {
                        if (Fncls[i].ddsm_actualend1 != null) {
                            thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend1).substr(6)));
                            //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                        }
                        fncl_obj.ddsm_targetstart2 = new Date(thisStartDate.getTime());
                        thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration2 * 86400000));
                        fncl_obj.ddsm_targetend2 = new Date(thisStartDate.getTime());
                    }
                    if (Fncls[i].ddsm_stagename3 != null) {
                        if (Fncls[i].ddsm_actualend2 != null) {
                            thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend2).substr(6)));
                            //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                        }
                        fncl_obj.ddsm_targetstart3 = new Date(thisStartDate.getTime());
                        thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration3 * 86400000));
                        fncl_obj.ddsm_targetend3 = new Date(thisStartDate.getTime());
                    }
                    if (Fncls[i].ddsm_stagename4 != null) {
                        if (Fncls[i].ddsm_actualend3 != null) {
                            thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend3).substr(6)));
                            //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                        }
                        fncl_obj.ddsm_targetstart4 = new Date(thisStartDate.getTime());
                        thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration4 * 86400000));
                        fncl_obj.ddsm_targetend4 = new Date(thisStartDate.getTime());
                    }
                    if (Fncls[i].ddsm_stagename5 != null) {
                        if (Fncls[i].ddsm_actualend4 != null) {
                            thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend4).substr(6)));
                            //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                        }
                        fncl_obj.ddsm_targetstart5 = new Date(thisStartDate.getTime());
                        thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration5 * 86400000));
                        fncl_obj.ddsm_targetend5 = new Date(thisStartDate.getTime());
                    }
                    gen_H_updateEntitySync(Fncls[i].ddsm_financialId, fncl_obj, "ddsm_financialSet");
                    // Call function to update using ODATA call
                    //alert("It works!");
                }
            }
}
            if (CMS == parseInt(initMS)) {
                document.getElementById('WebResource_financial_grid').contentWindow.reloadGrid();
            }
        } else {
            alert("No results found (query successful)");
        }
    } else {
        alert("Error: Fncl_UpdateFncls failed query.\n" + req.responseText);
        return false;
    }
    return true;
}


function Fncl_UpdateFncls_onSite(projId, CMS) {
    //console.log("projId: " + projId);
    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    //Query if this ID is already in use
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projectSet?"
    		+ "$select=ddsm_name,"
    		+ "ddsm_ddsm_project_ddsm_milestone/ddsm_TargetEnd,"
    		+ "ddsm_ddsm_project_ddsm_milestone/ddsm_EstimatedEnd,"
    		+ "ddsm_ddsm_project_ddsm_milestone/ddsm_ActualEnd,"
    		+ "ddsm_ddsm_project_ddsm_milestone/ddsm_Index,"
    		+ "ddsm_ddsm_project_ddsm_milestone/ddsm_name,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_financialId,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_initiatingmilestoneindex,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_name,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_status,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_stagename1,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_stagename2,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_stagename3,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_stagename4,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_stagename5,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_duration1,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_duration2,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_duration3,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_duration4,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_duration5,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_actualend1,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_actualend2,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_actualend3,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_actualend4,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_actualend5"
    		+ "&$filter=ddsm_projectId eq guid'" + projId + "'"
    		+ "&$expand=ddsm_ddsm_project_ddsm_milestone,ddsm_ddsm_project_ddsm_financial", false);

    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();

    if (req.readyState == 4 && req.status == 200) {
        var proj = JSON.parse(req.responseText).d;
        if (proj.results[0] != null) {
            var Fncls = proj.results[0].ddsm_ddsm_project_ddsm_financial.results;
            var MSs = proj.results[0].ddsm_ddsm_project_ddsm_milestone.results;
            //                console.dir(Fncls);
            //                console.dir(MSs);

            var initMS; //first this will be the starting index
            var initMSJson = -1; //This is the index of the Init MS in the JSON object
            var startDate = null;
            var thisStartDate = null;

       if (Fncls.length > 0) {
            for (var i in Fncls) {
                //alert("Fncl Name: " + Fncls[i].ddsm_name);
                initMS = Fncls[i].ddsm_initiatingmilestoneindex;
                var actStart = null;
                for (var j in MSs) {
                    //alert("MS Name: " + MSs[i].ddsm_name);
                    //console.log("initMS: " + initMS + "| MSs[" + j + "].ddsm_Index: " + MSs[j].ddsm_Index);
                    if (parseInt(initMS) == parseInt(MSs[j].ddsm_Index)) {
                        initMSJson = j;
                        //console.log(MSs[j].ddsm_ActualEnd);
                        if (MSs[j].ddsm_ActualEnd != null) {
                            //console.log(">>>> ddsm_ActualEnd");
                            //startDate = new Date(parseInt((MSs[j].ddsm_ActualEnd).substr(6)));
                            actStart = eval((MSs[j].ddsm_ActualEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                            startDate = eval((MSs[j].ddsm_ActualEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                            break;
                        } else if (MSs[j].ddsm_EstimatedEnd != null) {
                            //console.log(">>>> ddsm_EstimatedEnd");
                            //startDate = new Date(parseInt((MSs[j].ddsm_EstimatedEnd).substr(6)));
                            startDate = eval((MSs[j].ddsm_EstimatedEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                            break;
                        } else if (MSs[j].ddsm_TargetEnd != null) {
                            //console.log(">>>> ddsm_TargetEnd");
                            //startDate = new Date(parseInt((MSs[j].ddsm_TargetEnd).substr(6)));
                            startDate = eval((MSs[j].ddsm_TargetEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                            break;
                        } else {
                            alert("Error Updating Financial: " + Fncls[i].ddsm_name +
	        						"\nInitiating MS Target Date is missing");
                            return;
                        }
                    }
                }
                if (initMSJson == -1) {
                    alert("Error Updating Financial:" + Fncls[i].ddsm_name +
	        				"\nCannot find initiating milestone (index=" + initMS + ")");
                    continue;
                }
                if (typeof Fncls[i].ddsm_financialId != 'undefined') {
                    var fncl_obj = new Object();

                    if (Fncls[i].ddsm_stagename1 != null) {
                        if (Fncls[i].ddsm_initiatingmilestoneindex <= CMS && Fncls[i].ddsm_status.Value == 962080000 &&
                                actStart != null) { //only run if CMS is past Init MS AND Status == Not Started
                            fncl_obj.ddsm_status = new Object();
                            fncl_obj.ddsm_status.Value = 962080001; // setting status = Active
                            fncl_obj.ddsm_pendingstage = Fncls[i].ddsm_stagename1;
                            fncl_obj.ddsm_actualstart1 = actStart;
                        }
                        //console.log(startDate);
                        fncl_obj.ddsm_targetstart1 = new Date(startDate.getTime());
                        thisStartDate = new Date(startDate.getTime() + (Fncls[i].ddsm_duration1 * 86400000));
                        fncl_obj.ddsm_targetend1 = new Date(thisStartDate.getTime());
                    }
                    if (Fncls[i].ddsm_stagename2 != null) {
                        if (Fncls[i].ddsm_actualend1 != null) {
                            thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend1).substr(6)));
                            //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                        }
                        fncl_obj.ddsm_targetstart2 = new Date(thisStartDate.getTime());
                        thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration2 * 86400000));
                        fncl_obj.ddsm_targetend2 = new Date(thisStartDate.getTime());
                    }
                    if (Fncls[i].ddsm_stagename3 != null) {
                        if (Fncls[i].ddsm_actualend2 != null) {
                            thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend2).substr(6)));
                            //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                        }
                        fncl_obj.ddsm_targetstart3 = new Date(thisStartDate.getTime());
                        thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration3 * 86400000));
                        fncl_obj.ddsm_targetend3 = new Date(thisStartDate.getTime());
                    }
                    if (Fncls[i].ddsm_stagename4 != null) {
                        if (Fncls[i].ddsm_actualend3 != null) {
                            thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend3).substr(6)));
                            //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                        }
                        fncl_obj.ddsm_targetstart4 = new Date(thisStartDate.getTime());
                        thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration4 * 86400000));
                        fncl_obj.ddsm_targetend4 = new Date(thisStartDate.getTime());
                    }
                    if (Fncls[i].ddsm_stagename5 != null) {
                        if (Fncls[i].ddsm_actualend4 != null) {
                            thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend4).substr(6)));
                            //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                        }
                        fncl_obj.ddsm_targetstart5 = new Date(thisStartDate.getTime());
                        thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration5 * 86400000));
                        fncl_obj.ddsm_targetend5 = new Date(thisStartDate.getTime());
                    }
                    gen_H_updateEntitySync(Fncls[i].ddsm_financialId, fncl_obj, "ddsm_financialSet");
                    // Call function to update using ODATA call
                    //alert("It works!");
                }
            }
}
        } else {
            alert("No results found (query successful)");
        }
    } else {
        alert("Error: Fncl_UpdateFncls failed query.\n" + req.responseText);
        return false;
    }
    return true;
}


function Fncl_UpdateProjectCompletable(projID, date) {
    //console.log('Fncl_UpdateProjectCompletable().....');
    //console.log('Date: ' + date);
    //console.log('projID: ' + projID);
    if (date != null) {
        var req = new XMLHttpRequest();
        var serverUrl = Xrm.Page.context.getClientUrl();
        //Query if this ID is already in use
        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projectSet?"
                + "$select=ddsm_name,"
                + "ddsm_projectId,"
                + "ddsm_EndDate,"
                + "ddsm_ProjectStatus,"
                + "ddsm_Phase,"
                + "ddsm_ProjectCompletable,"
                + "ddsm_ddsm_projecttemplate_ddsm_project/ddsm_CompletePhase,"
                + "ddsm_ddsm_projecttemplate_ddsm_project/ddsm_CompleteStatus"
                + "&$expand=ddsm_ddsm_projecttemplate_ddsm_project"
                + "&$filter=ddsm_projectId eq guid'" + projID + "'", true);

        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.onreadystatechange = function () {
            completeProj_reqCallback(this, projID, date);
        }
        req.send();
    }
}



function completeProj_reqCallback(req, projID, date) {
    var projCmpltDate = null;
    if (req.readyState == 4) {
        if (req.status == 200) {
            var proj = JSON.parse(req.responseText).d;
            if (proj.results[0] != null) {
                var newObj = new Object();
                newObj.ddsm_EndDate = new Date(date.getTime());
                newObj.ddsm_ProjectStatus = proj.results[0].ddsm_ddsm_projecttemplate_ddsm_project.ddsm_CompleteStatus;
                newObj.ddsm_Phase = proj.results[0].ddsm_ddsm_projecttemplate_ddsm_project.ddsm_CompletePhase;
                //updateRecord(projID, newObj, "ddsm_projectSet", RefreshCompletedProject(projID), null);
                updateRecord(projID, newObj, "ddsm_projectSet", null, null);
            } else {
                alert("Error: Update Project Complete failed to parent project. Please contact your database administrator");
            }
        } else {
            alert("Error: " + req.responseText);
            return;
        }
    }
}


function Fncl_UpdateProjectCompletable_onGrid(projID, date) {
    //console.log('Fncl_UpdateProjectCompletable().....');
    //console.log('Date: ' + date);
    //console.log('projID: ' + projID);
    if (date != null) {
        var req = new XMLHttpRequest();
        var serverUrl = Xrm.Page.context.getClientUrl();
        //Query if this ID is already in use
        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projectSet?"
                + "$select=ddsm_name,"
                + "ddsm_projectId,"
                + "ddsm_EndDate,"
                + "ddsm_ProjectStatus,"
                + "ddsm_Phase,"
                + "ddsm_ProjectCompletable,"
                + "ddsm_ddsm_projecttemplate_ddsm_project/ddsm_CompletePhase,"
                + "ddsm_ddsm_projecttemplate_ddsm_project/ddsm_CompleteStatus"
                + "&$expand=ddsm_ddsm_projecttemplate_ddsm_project"
                + "&$filter=ddsm_projectId eq guid'" + projID + "'", true);

        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.onreadystatechange = function () {
            completeProj_reqCallback_onGrid(this, projID, date);
        }
        req.send();
    }
}


function completeProj_reqCallback_onGrid(req, projID, date) {
    var projCmpltDate = null;
    if (req.readyState == 4) {
        if (req.status == 200) {
            var proj = JSON.parse(req.responseText).d;
            if (proj.results[0] != null) {
                Xrm.Page.getAttribute("ddsm_enddate").setValue(new Date(date.getTime()));
                Xrm.Page.getAttribute("ddsm_projectstatus").setValue(proj.results[0].ddsm_ddsm_projecttemplate_ddsm_project.ddsm_CompleteStatus);
                Xrm.Page.getAttribute("ddsm_phase").setValue(proj.results[0].ddsm_ddsm_projecttemplate_ddsm_project.ddsm_CompletePhase);

                progYear = date.getFullYear();

                switch (progYear) {
                    case 2009:
                        Xrm.Page.getAttribute("ddsm_programyear").setValue(962080000);
                        break;
                    case 2010:
                        Xrm.Page.getAttribute("ddsm_programyear").setValue(962080001);
                        break;
                    case 2011:
                        Xrm.Page.getAttribute("ddsm_programyear").setValue(962080002);
                        break;
                    case 2012:
                        Xrm.Page.getAttribute("ddsm_programyear").setValue(962080003);
                        break;
                    case 2013:
                        Xrm.Page.getAttribute("ddsm_programyear").setValue(962080004);
                        break;
                    case 2014:
                        Xrm.Page.getAttribute("ddsm_programyear").setValue(962080005);
                        break;
                    case 2015:
                        Xrm.Page.getAttribute("ddsm_programyear").setValue(962080006);
                        break;
                }
                Xrm.Page.data.save();
                alert("This project is now complete.");

                //var newObj = new Object();
                //newObj.ddsm_EndDate = new Date(date.getTime());
                //newObj.ddsm_ProjectStatus = proj.results[0].ddsm_ddsm_projecttemplate_ddsm_project.ddsm_CompleteStatus;
                //newObj.ddsm_Phase = proj.results[0].ddsm_ddsm_projecttemplate_ddsm_project.ddsm_CompletePhase;
                //updateRecord(projID, newObj, "ddsm_projectSet", proj_checkComplete(), null);

            } else {
                alert("Error: Update Project Complete failed to parent project. Please contact your database administrator");
            }
        } else {
            alert("Error: " + req.responseText);
            return;
        }
    }
}



function RefreshCompletedProject(id) {
    try {
        Xrm.Page.data.save().then(function () {
            if (typeof Xrm.Page.getAttribute("ddsm_projecttemplateid") !== 'undefined' && Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue() != null) {
                Xrm.Utility.openEntityForm("ddsm_projectSet", id);
            }
        }, function () {
            alert('Save failed! Please refresh the form.');
        });
    } catch (err) { alert(err); }
}



//Make the whole line visible or not
function Fncl_HideEmptyStages(OnOff) {
    if (Xrm.Page.getAttribute("ddsm_stagename1").getValue() == null) {
        Xrm.Page.ui.controls.get("ddsm_stagename1").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_targetstart1").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_actualstart1").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_targetend1").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_actualend1").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_duration1").setVisible(OnOff);
    }
    if (Xrm.Page.getAttribute("ddsm_stagename2").getValue() == null) {
        Xrm.Page.ui.controls.get("ddsm_stagename2").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_targetstart2").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_actualstart2").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_targetend2").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_actualend2").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_duration2").setVisible(OnOff);
    }
    if (Xrm.Page.getAttribute("ddsm_stagename3").getValue() == null) {
        Xrm.Page.ui.controls.get("ddsm_stagename3").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_targetstart3").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_actualstart3").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_targetend3").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_actualend3").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_duration3").setVisible(OnOff);
    }
    if (Xrm.Page.getAttribute("ddsm_stagename4").getValue() == null) {
        Xrm.Page.ui.controls.get("ddsm_stagename4").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_targetstart4").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_actualstart4").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_targetend4").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_actualend4").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_duration4").setVisible(OnOff);
    }
    if (Xrm.Page.getAttribute("ddsm_stagename5").getValue() == null) {
        Xrm.Page.ui.controls.get("ddsm_stagename5").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_targetstart5").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_actualstart5").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_targetend5").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_actualend5").setVisible(OnOff);
        Xrm.Page.ui.controls.get("ddsm_duration5").setVisible(OnOff);
    }
}





function payeeQuery() {
    if (Xrm.Page.getAttribute("ddsm_payeeid").getValue() != null) {  // Payee (TA)
        var payeeID = Xrm.Page.getAttribute("ddsm_payeeid").getValue()[0].id;
        var req = new XMLHttpRequest();
        var serverUrl = Xrm.Page.context.getClientUrl();
        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/AccountSet?"
	    	+ "$select=" //ddsm_BusinessType,"
	    	+ "ddsm_TaxID,"
	    	+ "Address1_Name,"
	    	+ "Address1_Line1,"
	    	+ "Address1_Line2,"
	    	+ "Address1_Line3,"
	    	+ "Address1_City,"
	    	+ "Address1_Country,"
	    	+ "Address1_Telephone1,"
	    	+ "Address1_PostalCode,"
	    	+ "Address1_StateOrProvince"
	    	+ "&$filter=AccountId eq guid'" + payeeID + "'", false);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.send();

        if (req.readyState == 4 && req.status == 200) {
            var payeeInfo = JSON.parse(req.responseText).d;
            if (payeeInfo.results[0] != null) {
                //var busType = payeeInfo.results[0].ddsm_BusinessType.Value;
                var taxId = payeeInfo.results[0].ddsm_TaxID;
                var payeeName = payeeInfo.results[0].Address1_Name;
                var payeeAdr1 = payeeInfo.results[0].Address1_Line1;
                var payeeAdr2 = payeeInfo.results[0].Address1_Line2;
                var payeeAdr3 = payeeInfo.results[0].Address1_Line3;
                var payeeCity = payeeInfo.results[0].Address1_City;
                var payeeCountry = payeeInfo.results[0].Address1_Country;
                var payeePhone = payeeInfo.results[0].Address1_Telephone1;
                var payeeZip = payeeInfo.results[0].Address1_PostalCode;
                var payeeState = payeeInfo.results[0].Address1_StateOrProvince;

                //Xrm.Page.getAttribute("ddsm_businesstype").setValue(busType);
                Xrm.Page.getAttribute("ddsm_taxid").setValue(taxId);
                Xrm.Page.getAttribute("ddsm_payeeaddress1").setValue(payeeAdr1);
                Xrm.Page.getAttribute("ddsm_payeeaddress2").setValue(payeeAdr2);
                Xrm.Page.getAttribute("ddsm_payeeaddress3").setValue(payeeAdr3);
                Xrm.Page.getAttribute("ddsm_payeeaddressname").setValue(payeeName);
                Xrm.Page.getAttribute("ddsm_payeecity").setValue(payeeCity);
                Xrm.Page.getAttribute("ddsm_payeecountry").setValue(payeeCountry);
                Xrm.Page.getAttribute("ddsm_payeephone").setValue(payeePhone);
                Xrm.Page.getAttribute("ddsm_payeezipcode").setValue(payeeZip);
                Xrm.Page.getAttribute("ddsm_payeestate").setValue(payeeState);
            } else {
                alert("Error: Could not find associated Payee Account.");
            }
        }
    }
}



function financialTemplateImport() {
    financialAcntSiteImport();

    var actStartDate = new Date();
    var req = new XMLHttpRequest();
    var fnclTplID = Xrm.Page.getAttribute("ddsm_financialtemplate").getValue()[0].id;
    var serverUrl = Xrm.Page.context.getClientUrl();
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_financialtemplateSet?"
		+ "$select="
			+ "ddsm_name,"
			+ "ddsm_FinancialType,"
			+ "ddsm_PaymentType,"
			+ "ddsm_InitiatingMilestoneIndex,"
			+ "ddsm_StageName1,"
			+ "ddsm_StageName2,"
			+ "ddsm_StageName3,"
			+ "ddsm_StageName4,"
			+ "ddsm_StageName5,"
			+ "ddsm_Duration1,"
			+ "ddsm_Duration2,"
			+ "ddsm_Duration3,"
			+ "ddsm_Duration4,"
			+ "ddsm_Duration5,"
			+ "ddsm_CalculationType,"
			+ "ddsm_ProjectCompletableIndex"
		+ "&$filter=ddsm_financialtemplateId eq guid'" + fnclTplID + "' and statecode/Value eq 0", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();
    if (req.readyState == 4 && req.status == 200) {
        //From the project template - will also contain milestone information
        var _tplData = JSON.parse(req.responseText).d;
        if (_tplData.results[0] != null) {
            var projID = Xrm.Page.getAttribute("ddsm_projecttofinancialid").getValue()[0].name;
            //console.log(projID);
            if (_tplData.results[0].ddsm_CalculationType.Value != null) {
                var calcType = ((_tplData.results[0].ddsm_CalculationType.Value).toString()).substring(7, 9);
            } else {
                var calcType = "";
            }
            //var checkNo = (projID + calcType);
            Xrm.Page.getAttribute("ddsm_name").setValue(_tplData.results[0].ddsm_name);
            Xrm.Page.getAttribute("ddsm_financialtype").setValue(_tplData.results[0].ddsm_FinancialType.Value);
            //Xrm.Page.getAttribute("ddsm_paymenttype").setValue(_tplData.results[0].ddsm_PaymentType.Value);
            Xrm.Page.getAttribute("ddsm_initiatingmilestoneindex").setValue(_tplData.results[0].ddsm_InitiatingMilestoneIndex);
            Xrm.Page.getAttribute("ddsm_stagename1").setValue(_tplData.results[0].ddsm_StageName1);
            Xrm.Page.getAttribute("ddsm_stagename2").setValue(_tplData.results[0].ddsm_StageName2);
            Xrm.Page.getAttribute("ddsm_stagename3").setValue(_tplData.results[0].ddsm_StageName3);
            Xrm.Page.getAttribute("ddsm_stagename4").setValue(_tplData.results[0].ddsm_StageName4);
            Xrm.Page.getAttribute("ddsm_stagename5").setValue(_tplData.results[0].ddsm_StageName5);
            Xrm.Page.getAttribute("ddsm_duration1").setValue(_tplData.results[0].ddsm_Duration1);
            Xrm.Page.getAttribute("ddsm_duration2").setValue(_tplData.results[0].ddsm_Duration2);
            Xrm.Page.getAttribute("ddsm_duration3").setValue(_tplData.results[0].ddsm_Duration3);
            Xrm.Page.getAttribute("ddsm_duration4").setValue(_tplData.results[0].ddsm_Duration4);
            Xrm.Page.getAttribute("ddsm_duration5").setValue(_tplData.results[0].ddsm_Duration5);
            Xrm.Page.getAttribute("ddsm_calculationtype").setValue(_tplData.results[0].ddsm_CalculationType.Value);
            Xrm.Page.getAttribute("ddsm_projectcompletableindex").setValue(_tplData.results[0].ddsm_ProjectCompletableIndex);
            Xrm.Page.getAttribute("ddsm_actualstart1").setValue(actStartDate);
            Xrm.Page.getAttribute("ddsm_status").setValue(962080001);
            //Xrm.Page.getAttribute("ddsm_checknumber").setValue(checkNo);
        }

        setTimeout(function () {
            Xrm.Page.data.save().then(function () {
                var id = Xrm.Page.data.entity.getId();
                var entityLogicalName = Xrm.Page.data.entity.getEntityName();
                Xrm.Utility.openEntityForm(entityLogicalName, id);
            }, function () {
                alert('Save failed! Please refresh the form.');
            });
        }, 2000);

    } else {
        alert("Error: " + req.responseText);
    }

}



function financialAcntSiteImport() {
    var req = new XMLHttpRequest();
    var projID = Xrm.Page.getAttribute("ddsm_projecttofinancialid").getValue()[0].id;
    var serverUrl = Xrm.Page.context.getClientUrl();
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projectSet?"
		+ "$select="
			+ "ddsm_AccountId,"
			+ "ddsm_ParentSiteId,"
			+ "ddsm_AccountNumber"
		+ "&$filter=ddsm_projectId eq guid'" + projID + "' and statecode/Value eq 0", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();
    if (req.readyState == 4 && req.status == 200) {
        //From the project template - will also contain milestone information
        var _tplData = JSON.parse(req.responseText).d;
        if (_tplData.results[0] != null) {
            //console.dir(_tplData.results);
            var setLookUp = new Array();
            setLookUp[0] = new Object();
            setLookUp[0].id = _tplData.results[0].ddsm_AccountId.Id;
            setLookUp[0].name = _tplData.results[0].ddsm_AccountId.Name;
            setLookUp[0].entityType = _tplData.results[0].ddsm_AccountId.LogicalName;
            Xrm.Page.ui.controls.get("ddsm_accountid").setDisabled(false);
            Xrm.Page.getAttribute("ddsm_accountid").setValue(setLookUp);
            Xrm.Page.ui.controls.get("ddsm_accountid").setDisabled(true);
            setLookUp[0] = new Object();
            setLookUp[0].id = _tplData.results[0].ddsm_ParentSiteId.Id;
            setLookUp[0].name = _tplData.results[0].ddsm_ParentSiteId.Name;
            setLookUp[0].entityType = _tplData.results[0].ddsm_ParentSiteId.LogicalName;
            Xrm.Page.ui.controls.get("ddsm_siteid").setDisabled(false);
            Xrm.Page.getAttribute("ddsm_siteid").setValue(setLookUp);
            Xrm.Page.ui.controls.get("ddsm_siteid").setDisabled(true);
            Xrm.Page.getAttribute("ddsm_accountnumber").setValue(_tplData.results[0].ddsm_AccountNumber);
        }
    } else {
        alert("Error: " + req.responseText);
    }
}



function fnclLockdown() {
    try {
        var checkRole = userRoleLock();
        if (checkRole) {
            if (Xrm.Page.getAttribute("ddsm_financialtemplate").getValue() == null) {
                Xrm.Page.ui.controls.get("ddsm_financialtemplate").setDisabled(false);
            }
        }
    } catch (err) {
        alert("Error: " + err + ".");
    }
}

function whoGetsPaid_OnChange() {

    var whoGetsPaid = Xrm.Page.getAttribute("ddsm_whogetspaid").getValue();
    var result;
    if (!whoGetsPaid){
      Alert.show("Error!", "Payee Preference cannot be blank.", null, "ERROR", 500, 200);
      return;
    }
    switch (whoGetsPaid) {
        //Account
        case 962080000:

            Xrm.Page.ui.controls.get("ddsm_payeeid").setVisible(false);
            Xrm.Page.ui.controls.get("ddsm_payeesiteid").setVisible(false);
            //Xrm.Page.ui.controls.get("ddsm_payeelpcid").setVisible(false);
            Xrm.Page.ui.controls.get("ddsm_payeeaccount").setVisible(false);
            Xrm.Page.getAttribute("ddsm_payeeid").setValue(null);

            Xrm.Page.getAttribute("ddsm_payeeaddress1").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeeaddress2").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeecity").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeestate").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeezipcode").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeeattnto").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeecompany").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeename").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeenumber").setSubmitMode("always");

            Xrm.Page.getAttribute("ddsm_payeeaddress1").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeeaddress2").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeecity").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeestate").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeezipcode").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeeattnto").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeecompany").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeename").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeenumber").setValue(null);

            var proj = Xrm.Page.getAttribute("ddsm_projecttofinancialid").getValue();

            if (!!proj) {
                var projId = proj[0].id;
                AGS.REST.retrieveRecord(
                    projId,
                    'ddsm_project',
                    'ddsm_AccountId',
                    null,
                    function(result) {

                        onChangePayeePartner(result.ddsm_AccountId, false);

                    },
                    function (err) {
                        console.log(err);
                    },
                    false
                );
            }

            break;
        //Partner
        case 962080001:
            Xrm.Page.ui.controls.get("ddsm_payeesiteid").setVisible(false);
            Xrm.Page.ui.controls.get("ddsm_payeeid").setVisible(false);
            //Xrm.Page.ui.controls.get("ddsm_payeelpcid").setVisible(false);
            Xrm.Page.ui.controls.get("ddsm_payeeaccount").setVisible(false);
            Xrm.Page.getAttribute("ddsm_payeeaccount").setValue(null);

            Xrm.Page.getAttribute("ddsm_payeeaddress1").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeeaddress2").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeecity").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeestate").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeezipcode").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeeattnto").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeecompany").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeename").setSubmitMode("always");
            Xrm.Page.getAttribute("ddsm_payeenumber").setSubmitMode("always");


            Xrm.Page.getAttribute("ddsm_payeeaddress1").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeeaddress2").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeecity").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeestate").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeezipcode").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeeattnto").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeecompany").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeename").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeenumber").setValue(null);

            var proj = Xrm.Page.getAttribute("ddsm_projecttofinancialid").getValue();

            if (!!proj) {
                var projId = proj[0].id;
                AGS.REST.retrieveRecord(
                    projId,
                    'ddsm_project',
                    'ddsm_TradeAlly',
                    null,
                    function(result) {
                        onChangePayeePartner(result.ddsm_TradeAlly, true);
                    },
                    function (err) {
                        console.log(err);
                    },
                    false
                );
            }

            break;
        //Other
        case 962080002:
            Xrm.Page.ui.controls.get("ddsm_payeesiteid").setVisible(false);
            Xrm.Page.ui.controls.get("ddsm_payeeid").setVisible(false);
            //Xrm.Page.ui.controls.get("ddsm_payeelpcid").setVisible(true);
            Xrm.Page.ui.controls.get("ddsm_payeeaccount").setVisible(false);
            Xrm.Page.getAttribute("ddsm_payeeid").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeesiteid").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeeaccount").setValue(null);

            Xrm.Page.getAttribute("ddsm_payeeaddress1").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeeaddress2").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeecity").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeestate").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeezipcode").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeeattnto").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeecompany").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeename").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeenumber").setValue(null);
            onChangePayeePartner(null, false);

            break;
        //LM
        default:
            Xrm.Page.ui.controls.get("ddsm_payeesiteid").setVisible(false);
            Xrm.Page.ui.controls.get("ddsm_payeeid").setVisible(false);
            //Xrm.Page.ui.controls.get("ddsm_payeelpcid").setVisible(true);
            Xrm.Page.ui.controls.get("ddsm_payeeaccount").setVisible(false);
            Xrm.Page.getAttribute("ddsm_payeeid").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeesiteid").setValue(null);

            Xrm.Page.getAttribute("ddsm_payeeaddress1").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeeaddress2").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeecity").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeestate").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeezipcode").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeeattnto").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeecompany").setValue(null);
            Xrm.Page.getAttribute("ddsm_payeename").setValue(null);

            onChangePayeePartner(null, false);

            break;
    }
}

function onChangePayeePartner(result, parthner) {

    var ft = Xrm.Page.getAttribute("ddsm_financialtemplate").getValue();
    var ftName;
    if (!!ft) {
        ftName = ft[0].name;
    }

    if (!!result && parthner) {

        var value = new Array();
        value[0] = new Object();
        value[0].id = result.Id;
        value[0].name = result.Name;
        value[0].entityType = result.LogicalName;
        Xrm.Page.getAttribute("ddsm_payeeid").setValue(value);
        Xrm.Page.getAttribute("ddsm_payeeid").setSubmitMode("always");

        if (ftName.includes("GH")) {
            Xrm.Page.getAttribute("ddsm_cc1_vendor").setValue(value);
            Xrm.Page.getAttribute("ddsm_cc2_vendor").setValue(value);
            Xrm.Page.getAttribute("ddsm_cc3_vendor").setValue(value);
        }

        var saveOptions = {UseSchedulingEngine: true};
        Xrm.Page.data.save(saveOptions).then(
            function () {
                Xrm.Page.data.refresh(true);
            },

            function (msg) {
                console.log(msg);
            }
        );
    }
    else if (!!result && !parthner) {
        var value = new Array();
        value[0] = new Object();
        value[0].id = result.Id;
        value[0].name = result.Name;
        value[0].entityType = result.LogicalName;
        Xrm.Page.getAttribute("ddsm_payeeaccount").setValue(value);
        Xrm.Page.getAttribute("ddsm_payeeaccount").setSubmitMode("always");

        if (ftName.includes("GH")) {
            Xrm.Page.getAttribute("ddsm_cc1_vendor").setValue(value);
            Xrm.Page.getAttribute("ddsm_cc2_vendor").setValue(value);
            Xrm.Page.getAttribute("ddsm_cc3_vendor").setValue(value);
        }

        var saveOptions = {UseSchedulingEngine: true};
        Xrm.Page.data.save(saveOptions).then(
            function () {
                Xrm.Page.data.refresh(true);
            },

            function (msg) {
                console.log(msg);
            }
        );
    }
    else {
        var value = new Array();
        value[0] = new Object();
        value[0].id = null;
        value[0].name = null;
        value[0].entityType = null;

        Xrm.Page.getAttribute("ddsm_payeeid").setSubmitMode("always");
        Xrm.Page.getAttribute("ddsm_payeeid").setValue(value);
        Xrm.Page.getAttribute("ddsm_payeeaccount").setSubmitMode("always");
        Xrm.Page.getAttribute("ddsm_payeeaccount").setValue(value);

        if (ftName.includes("GH")) {
            Xrm.Page.getAttribute("ddsm_cc1_vendor").setValue(null);
            Xrm.Page.getAttribute("ddsm_cc2_vendor").setValue(null);
            Xrm.Page.getAttribute("ddsm_cc3_vendor").setValue(null);
        }

        var saveOptions = {UseSchedulingEngine: true};
        Xrm.Page.data.save(saveOptions).then(
            function () {
                Xrm.Page.data.refresh(true);
            },

            function (msg) {
                console.log(msg);
            }
        );
    }
}

function onChangePayeeSelector() {
    var whoGetsPaid = Xrm.Page.getAttribute("ddsm_whogetspaid").getValue();
    Xrm.Page.ui.controls.get("ddsm_payeeaccount").setVisible(false);
    Xrm.Page.ui.controls.get("ddsm_payeeid").setVisible(false);
    if (whoGetsPaid != 962080002) {
        clrPayeeFields();
    }
    var pSelector;
    if(whoGetsPaid == 962080001) {
        pSelector = Xrm.Page.getAttribute("ddsm_payeeid").getValue();
        AGS.Form.setValue("ddsm_payeeaccount", null);
    }
    else if (whoGetsPaid == 962080000)
    {
        pSelector = Xrm.Page.getAttribute("ddsm_payeeaccount").getValue();
        AGS.Form.setValue("ddsm_payeetaid",null);
    }
    /*else{
        var pSelector = Xrm.Page.getAttribute("ddsm_payeeaccount").getValue();
        AGS.Form.setValue("ddsm_payeetaid",null);
    }*/


    if (!!pSelector) {
        var accId = pSelector[0].id;
        AGS.REST.retrieveRecord(
            accId,
            'Account',
            'ddsm_PayeeName, ' +
            'ddsm_PayeeAddress1, ' +
            'ddsm_PayeeAddress2, ' +
            'ddsm_PayeeCity, ' +
            'ddsm_PayeeState, ' +
            'ddsm_PayeeZipCode, ' +
            'ddsm_PayeeAttnTo, ' +
            'ddsm_PayeeNumber, ' +
            'Name, ' +
            'ddsm_PayeeCompany',
            // 'ddsm_PayeePhone, ' +
            // 'ddsm_PayeeEmail, ' +
            // 'ddsm_PayeeTaxIDNumber',
            false,
            function (result) {
                if (result.ddsm_PayeeAddress1){Xrm.Page.getAttribute("ddsm_payeeaddress1").setValue(result.ddsm_PayeeAddress1);}
                if (result.ddsm_PayeeAddress2){Xrm.Page.getAttribute("ddsm_payeeaddress2").setValue(result.ddsm_PayeeAddress2);}
                    if (result.ddsm_PayeeCity){Xrm.Page.getAttribute("ddsm_payeecity").setValue(result.ddsm_PayeeCity);}
                        if (result.ddsm_PayeeState){Xrm.Page.getAttribute("ddsm_payeestate").setValue(result.ddsm_PayeeState);}
                            if (result.ddsm_PayeeZipCode){Xrm.Page.getAttribute("ddsm_payeezipcode").setValue(result.ddsm_PayeeZipCode);}
                                if (result.ddsm_PayeeAttnTo){Xrm.Page.getAttribute("ddsm_payeeattnto").setValue(result.ddsm_PayeeAttnTo);}
                                    if (result.ddsm_PayeeNumber){Xrm.Page.getAttribute("ddsm_payeenumber").setValue(result.ddsm_PayeeNumber);}
                if (result.Name){
                    Xrm.Page.getAttribute("ddsm_payeecompany").setValue(result.ddsm_PayeeCompany);
                }
                if (result.ddsm_PayeeName){Xrm.Page.getAttribute("ddsm_payeename").setValue(result.ddsm_PayeeName);}
                // Xrm.Page.getAttribute("ddsm_payeecompany").setValue();
                // Xrm.Page.getAttribute("ddsm_payeephone").setValue();
                // Xrm.Page.getAttribute("ddsm_payeeemail").setValue();
                // Xrm.Page.getAttribute("ddsm_taxid").setValue();
            },
            function (err) {
                console.log(err);
            },
            false
        );
    }
}

function doesControlHaveAttribute(control) {
    var controlType = control.getControlType();
    return controlType != "iframe" && controlType != "webresource" && controlType != "subgrid";
}



function disableFormFields(onOff) {
    Xrm.Page.ui.controls.forEach(function (control, index) {
        if (doesControlHaveAttribute(control)) {
            control.setDisabled(onOff);
        }
    });
}



function ddsm_fncl_parentProjOnly() {
    if (Xrm.Page.ui.getFormType() == 1 && Xrm.Page.getAttribute("ddsm_projecttofinancialid").getValue() == null) {
        //console.log("Form Type: " + Xrm.Page.ui.getFormType());
        //console.log("ddsm_projecttofinancialid: " + Xrm.Page.getAttribute("ddsm_projecttofinancialid").getValue());
        Xrm.Page.ui.controls.get("ddsm_financialtemplate").setDisabled(true);
        alert("Financials must be created from a project.");
        var attributes = Xrm.Page.data.entity.attributes.get();
        for (var j in attributes) {
            if (attributes[j].getName() != "statecode") {
                attributes[j].setSubmitMode("always");
            }
        }
        Xrm.Page.ui.close()
    }
}




function userRoleLock() {

    var currentRoles = Xrm.Page.context.getUserRoles();

    var powerUser = "DE1EE80F-6BE2-E311-ACAB-6C3BE5BDA978";
    var limitedUser = "03C93C23-6BE2-E311-ACAB-6C3BE5BDA978";
    var powerFinancialUser = "C53A2C4B-6BE2-E311-ACAB-6C3BE5BDA978";
    var dataAdmin = "D390C85C-6BE2-E311-ACAB-6C3BE5BDA978";
    var browseOnly = "3A904A65-6BE2-E311-ACAB-6C3BE5BDA978";

    for (x in currentRoles) {
        if (currentRoles[x] == limitedUser || currentRoles[x] == browseOnly || currentRoles[x] == powerUser) {
            disableFormFields(true);
            return false;
        } return true;
    }
}





function fncl_fillPayeeInfo(obj, id, entitySet, entityId) {
    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    var strStateCode = "statecode";

    try {

        if (entitySet == "ddsm_siteSet") {
            req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/" + entitySet + "?"
                + "$select="
                    + "ddsm_payeeaddress1,"
                    + "ddsm_payeeaddress2,"
                    + "ddsm_payeeattnto,"
                    + "ddsm_payeecity,"
                    + "ddsm_payeecompany,"
                    + "ddsm_payeephone,"
                    + "ddsm_payeestate,"
                    + "ddsm_payeezipcode,"
                    + "ddsm_taxentity,"
                    + "ddsm_taxid,"
                    //+ "ddsm_payeeaccountnumber,"
                   // + "ddsm_payeeroutingnumber,"
                    + "ddsm_payeeemail"
                + "&$filter=" + entityId + " eq guid'" + id + "' and " + strStateCode + "/Value eq 0", false);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.send();
            if (req.readyState == 4 && req.status == 200) {
                //From the project template - will also contain milestone information
                var data = JSON.parse(req.responseText).d;
                if (data.results[0] != null) {
                    obj.ddsm_payeeaddress1 = data.results[0].ddsm_payeeaddress1;
                    obj.ddsm_payeeaddress2 = data.results[0].ddsm_payeeaddress2;
                    obj.ddsm_payeeattnto = data.results[0].ddsm_payeeattnto;
                    obj.ddsm_payeecity = data.results[0].ddsm_payeecity;
                    obj.ddsm_payeecompany = data.results[0].ddsm_payeecompany;
                    obj.ddsm_payeephone = data.results[0].ddsm_payeephone;
                    obj.ddsm_payeestate = data.results[0].ddsm_payeestate;
                    obj.ddsm_payeezipcode = data.results[0].ddsm_payeezipcode;
                    obj.ddsm_taxid = data.results[0].ddsm_taxid;
                    obj.ddsm_taxentity = new Object();
                    obj.ddsm_taxentity.Value = data.results[0].ddsm_taxentity.Value;
                    // obj.ddsm_payeeaccountnumber = data.results[0].ddsm_payeeaccountnumber;
                    //  obj.ddsm_payeeroutingnumber = data.results[0].ddsm_payeeroutingnumber;
                    obj.ddsm_payeeemail = data.results[0].ddsm_payeeemail;
                    //console.log('Data from ' + entitySet + ', fncl_fillPayeeInfo()....');
                    //console.dir(obj);
                }
            } else {
                alert("Error fill Payee Info from Site: " + req.responseText);
            }
        }

        else if (entitySet == "AccountSet") {
            strStateCode = "StateCode";

            req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/" + entitySet + "?"
                + "$select="
                    + "ddsm_PayeeAddress1,"
                    + "ddsm_PayeeAddress2,"
                    + "ddsm_PayeeAttnTo,"
                    + "ddsm_PayeeCity,"
                    + "ddsm_PayeeCompany,"
                    + "ddsm_PayeePhone,"
                    + "ddsm_PayeeState,"
                    + "ddsm_PayeeZipCode,"
                    + "ddsm_TaxEntity,"
                    + "ddsm_TaxID,"
                   // + "ddsm_PayeeAccountNumber,"
                   // + "ddsm_PayeeRoutingNumber,"
                    + "ddsm_PayeeEmail"
                + "&$filter=" + entityId + " eq guid'" + id + "' and " + strStateCode + "/Value eq 0", false);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.send();
            if (req.readyState == 4 && req.status == 200) {
                //From the project template - will also contain milestone information
                var data = JSON.parse(req.responseText).d;
                if (data.results[0] != null) {
                    obj.ddsm_payeeaddress1 = data.results[0].ddsm_PayeeAddress1;
                    obj.ddsm_payeeaddress2 = data.results[0].ddsm_PayeeAddress2;
                    obj.ddsm_payeeattnto = data.results[0].ddsm_PayeeAttnTo;
                    obj.ddsm_payeecity = data.results[0].ddsm_PayeeCity;
                    obj.ddsm_payeecompany = data.results[0].ddsm_PayeeCompany;
                    obj.ddsm_payeephone = data.results[0].ddsm_PayeePhone;
                    obj.ddsm_payeestate = data.results[0].ddsm_PayeeState;
                    obj.ddsm_payeezipcode = data.results[0].ddsm_PayeeZipCode;
                    obj.ddsm_taxid = data.results[0].ddsm_TaxID;
                    obj.ddsm_taxentity = new Object();
                    obj.ddsm_taxentity.Value = data.results[0].ddsm_TaxEntity.Value;
                    //  obj.ddsm_payeeaccountnumber = data.results[0].ddsm_PayeeAccountNumber;
                    //  obj.ddsm_payeeroutingnumber = data.results[0].ddsm_PayeeRoutingNumber;
                    obj.ddsm_payeeemail = data.results[0].ddsm_PayeeEmail;
                    //console.log('Data from ' + entitySet + ', fncl_fillPayeeInfo()....');
                    //console.dir(obj);
                }
            } else {
                alert("Error fill Payee Info from Company: " + req.responseText);
            }
        }

        else if (entitySet == "ddsm_localpowercompanySet") {
            req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/" + entitySet + "?"
                + "$select="
                    + "ddsm_payeeaddress1,"
                    + "ddsm_payeeaddress2,"
                    + "ddsm_payeeattnto,"
                    + "ddsm_payeecity,"
                    + "ddsm_payeecompany,"
                    + "ddsm_PayeePhone,"
                    + "ddsm_payeestate,"
                    + "ddsm_payeezipcode,"
                    + "ddsm_taxentity,"
                    + "ddsm_taxid,"
                   // + "ddsm_payeeaccountnumber,"
                  //  + "ddsm_payeeroutingnumber,"
                    + "ddsm_payeeemail"
                + "&$filter=" + entityId + " eq guid'" + id + "' and " + strStateCode + "/Value eq 0", false);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.send();
            if (req.readyState == 4 && req.status == 200) {
                //From the project template - will also contain milestone information
                var data = JSON.parse(req.responseText).d;
                if (data.results[0] != null) {
                    obj.ddsm_payeeaddress1 = data.results[0].ddsm_payeeaddress1;
                    obj.ddsm_payeeaddress2 = data.results[0].ddsm_payeeaddress2;
                    obj.ddsm_payeeattnto = data.results[0].ddsm_payeeattnto;
                    obj.ddsm_payeecity = data.results[0].ddsm_payeecity;
                    obj.ddsm_payeecompany = data.results[0].ddsm_payeecompany;
                    obj.ddsm_payeephone = data.results[0].ddsm_PayeePhone;
                    obj.ddsm_payeestate = data.results[0].ddsm_payeestate;
                    obj.ddsm_payeezipcode = data.results[0].ddsm_payeezipcode;
                    obj.ddsm_taxid = data.results[0].ddsm_taxid;
                    obj.ddsm_taxentity = new Object();
                    obj.ddsm_taxentity.Value = data.results[0].ddsm_taxentity.Value;
                    //obj.ddsm_payeeaccountnumber = data.results[0].ddsm_payeeaccountnumber;
                    // obj.ddsm_payeeroutingnumber = data.results[0].ddsm_payeeroutingnumber;
                    obj.ddsm_payeeemail = data.results[0].ddsm_payeeemail;
                    //console.log('Data from ' + entitySet + ', fncl_fillPayeeInfo()....');
                    //console.dir(obj);
                }
            } else {
                alert("Error fill Payee Info from Local Power Company: " + req.responseText);
            }
        }

    } catch (err) { alert(err); }
}





function fncl_PPNOverrideTrue(projId) {
    var PYMTPREF_SITE = 962080000;
    var PYMTPREF_PPN = 962080001;
    var PYMTPREF_LPC = 962080003;
    var PYMTPREF_LPC_SITE = null;

    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projectSet?"
    		+ "$select="
			+ "ddsm_ddsm_project_ddsm_financial/ddsm_financialId,"
    		+ "ddsm_ddsm_project_ddsm_financial/ddsm_name"
    		+ "&$filter=ddsm_projectId eq guid'" + projId + "'"
    		+ "&$expand=ddsm_ddsm_project_ddsm_financial", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();

    if (req.readyState == 4 && req.status == 200) {
        var proj = JSON.parse(req.responseText).d;
        if (proj.results[0] != null) {
            var Fncls = proj.results[0].ddsm_ddsm_project_ddsm_financial.results;
            var obj;
            for (var i in Fncls) {
                obj = new Object();
                obj.ddsm_whogetspaid = new Object();
                obj.ddsm_whogetspaid.Value = PYMTPREF_PPN;
                updateRecord(Fncls[i].ddsm_financialId, obj, "ddsm_financialSet", null, null);
            }
        }
    }
}



function dateToMDY(date) {
    if (date != null) {
        var d = date.getUTCDate();
        var m = date.getUTCMonth() + 1;
        var y = date.getUTCFullYear();
        return (m <= 9 ? '0' + m : m) + '/' + (d <= 9 ? '0' + d : d) + '/' + y;
    }
}


function fnclOnSave() {
    window.parent.document.getElementById('WebResource_financial_grid').contentWindow.reloadGrid();
}

//added 6/17/2014 21:00
function UTCToLocalTime(d) {

    //var timeOffset = -((new Date()).getTimezoneOffset() / 60);
    //d.setHours(d.getHours() + timeOffset);
    return d;
}

function iesoSettlemt_Calc() {
	var actualInc = Xrm.Page.getAttribute("ddsm_actualamount").getValue();
	actualInc = (actualInc == null) ? 0: (actualInc * 0.9);

	Xrm.Page.getAttribute("ddsm_iesocalcsettlemt").setValue(actualInc);
}

/**
 * Populate numeric fiels (number, decimal, money)
 * @param obj {Object} - source/field mapping  {source_field: "form_field"}
 * @returns {boolean} - true if any value is set
 */
function setNumField(obj) {
    var changed = false;
    for (var k in obj) {
        val = obj[k];
        if (Xrm.Page.getAttribute(k).getValue() !== val) {
            Xrm.Page.getAttribute(k).setValue(+val || 0);
            changed = true;
        }
    }
    return changed;
}

/**
 * Populate custom rollups
 */
function getRollups() {
    var projID = Xrm.Page.getAttribute('ddsm_projecttofinancialid');
    if (!projID || !projID.getValue()) {
        return;
    }
    projID = projID.getValue()[0].id;

    setTimeout(function () {//start function async
        var fieldMap = {
            ddsm_incentivepaymentnetdsm: 'ddsm_incentivescustomerrebatesdsmamount',
            ddsm_incentivepaymentnetpns: 'ddsm_incentivescustomerrebatespnsamount',
            ddsm_incentivecostfinancing: 'ddsm_incentivesfinancingcostsamount'
        };

        var curID = Xrm.Page.data.entity.getId();
        if (!curID) {
            return;
        }
        var filter = [[
            ["ddsm_projecttomeasureid", 'eq', projID.slice(1, -1)],
            ["statecode", 'eq', '0'],
        ]];

        //fields for query
        var measFields = Object.keys(fieldMap).map(function (el) {
            return [el, 'sum', el]
        });
        var DataAggr = AGS.Fetch.aggregateFXML('ddsm_measure', measFields, filter);

        //map field names with fetch result
        var fieldValMap = {};

        for (var k in fieldMap) {
            var el = fieldMap[k];
            fieldValMap[el] = (!!DataAggr.attributes[k] ? DataAggr.attributes[k].value : 0);
        }

        if (!DataAggr || !DataAggr.attributes) {
            return;
        }
        var isDirty = setNumField(fieldValMap);
        if (isDirty) {
            Xrm.Page.data.save();
        }

    }, 1);
}

/**
 * Populate program fields in order to parent project -> Program Offering
 */
function setCorrectPrograms() {
    var projID = Xrm.Page.getAttribute('ddsm_projecttofinancialid');
    if (!projID || !projID.getValue()) {
        return;
    }
    projID = projID.getValue()[0].id;
    var sel = "ddsm_ddsm_programoffering_ddsm_project_ProgramOffering/ddsm_ShortName";
    var ProjExpand = "ddsm_ddsm_project_ddsm_milestone,ddsm_ddsm_project_ddsm_financial,ddsm_ddsm_project_ddsm_measure,ddsm_ddsm_project_ddsm_uploadeddocumens_ProjectId,ddsm_ddsm_project_ddsm_documentconvention,ddsm_ddsm_projecttemplate_ddsm_project";
    var selExpand = "ddsm_ddsm_programoffering_ddsm_project_ProgramOffering";

    //success
    var _successProgOffData = function (res) {
        var IncentivesCustomerRebatesDSMProgram, IncentivesCustomerRebatesPNSProgram, IncentivesFinancingCostsProgram;

        switch (res[selExpand].ddsm_ShortName.Value) {
            case (962080000 || 962080001 || 962080004):
                IncentivesCustomerRebatesDSMProgram = 962080008;
                IncentivesFinancingCostsProgram = 962080008;
                break;
            case (962080002):
                IncentivesCustomerRebatesDSMProgram = 962080009;
                IncentivesFinancingCostsProgram = 962080009;
                break;
            case (962080006):
                IncentivesCustomerRebatesDSMProgram = 962080007;
                IncentivesFinancingCostsProgram = 962080007;
                break;
            case (962080008):
                IncentivesCustomerRebatesDSMProgram = 962080006;
                break;
            case (962080009):
                IncentivesCustomerRebatesDSMProgram = 962080002;
                IncentivesFinancingCostsProgram = 962080002;
                break;
            case (962080011):
                IncentivesCustomerRebatesDSMProgram = 962080000;
                IncentivesFinancingCostsProgram = 962080000;
                IncentivesCustomerRebatesPNSProgram = 962080010;
                break;
            case (962080014):
                IncentivesCustomerRebatesDSMProgram = 962080003;
                IncentivesCustomerRebatesPNSProgram = 962080013;
                break;
            default:
                break;
        }

        if (!IncentivesCustomerRebatesDSMProgram && !IncentivesCustomerRebatesPNSProgram && !IncentivesFinancingCostsProgram) {
            return;
        }

        var isDirty = false;
        if (!!IncentivesCustomerRebatesDSMProgram && IncentivesCustomerRebatesDSMProgram !== Xrm.Page.getAttribute('ddsm_incentivescustomerrebatesdsmprogram').getValue()) {
            Xrm.Page.getAttribute('ddsm_incentivescustomerrebatesdsmprogram').setValue(IncentivesCustomerRebatesDSMProgram);
            isDirty = true;
        }

        if (IncentivesCustomerRebatesPNSProgram && IncentivesCustomerRebatesPNSProgram !== Xrm.Page.getAttribute('ddsm_incentivescustomerrebatespnsprogram').getValue()) {
            Xrm.Page.getAttribute('ddsm_incentivescustomerrebatespnsprogram').setValue(IncentivesCustomerRebatesPNSProgram);
            isDirty = true;
        }

        if (IncentivesFinancingCostsProgram && IncentivesFinancingCostsProgram !== Xrm.Page.getAttribute('ddsm_incentivesfinancingcostsprogram').getValue()) {
            Xrm.Page.getAttribute('ddsm_incentivesfinancingcostsprogram').setValue(IncentivesFinancingCostsProgram);
            isDirty = true;
        }

        if (!isDirty) {
            return;
        }
        Xrm.Page.data.save();

    };
    AccentGold.REST.retrieveRecord(projID, "ddsm_project", sel, selExpand, _successProgOffData, null, true);


}

function getFnclStatus() {
    if(!!Xrm.Page.getAttribute("ddsm_status") && Xrm.Page.getAttribute("ddsm_status").getValue() == 962080002)
        AGS.Form.disableFormFields(true);
    else
        AccentGold.REST.retrieveRecord(Xrm.Page.data.entity.getId(), "ddsm_financial", "ddsm_status", "",
            function(obj){
                if(!!obj.ddsm_status.Value && parseInt(obj.ddsm_status.Value) == 962080002)
                    AGS.Form.disableFormFields(true);
            }, null, true);
}


function SavePayeeNumber () {
    Xrm.Page.data.save();
}

//OMAHA DEMO
function HideFields(){
    Xrm.Page.ui.controls.get("ddsm_payeeaccount").setVisible(false);
    Xrm.Page.ui.controls.get("ddsm_payeeid").setVisible(false);
}
//OMAHA DEMO
// Last updated: 02/20/2014 16:38 - BTL - mts_InvoiceDate onChange updates parent project mts_InvoiceDate
// Last updated: 02/28/2014 15:14 - BTL - commented out the LM option for WhoGetsPaid
// 12/20/2015  : cleared off the code usage of removed fields of ddsm_project entity
// 03/07/2016: added functions setNumField(),setCorrectPrograms(), getRollups()
