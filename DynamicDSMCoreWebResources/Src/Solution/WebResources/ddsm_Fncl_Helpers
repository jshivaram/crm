//************************************************************************/
// Filename: ddsm_Fncl_Helpers.js
// Usage: Contains scripts associated with Fncl Helper Functions
// Created by TAB on 12/5/12 10:38
// Last Updated: 1/15/14|12:11|TAB: Created payeeLPCQuery function
// Last updated: 05/13/2014 - function editions. Added PopulatingValues().
//************************************************************************/ 


function PopulatingValues(payeeCo, payeeAttnTo, payeePhone, payeeAdr1, payeeAdr2, payeeCity, payeeState, payeeZip, taxId, taxEnt, payeeEmail/*, payeePayRef, payeeRoutingNum, payeeAccountNum*/) {
    Xrm.Page.getAttribute("ddsm_payeecompany").setValue(payeeCo);
    Xrm.Page.getAttribute("ddsm_payeeattnto").setValue(payeeAttnTo);
    Xrm.Page.getAttribute("ddsm_payeephone").setValue(payeePhone);
    Xrm.Page.getAttribute("ddsm_payeeaddress1").setValue(payeeAdr1);
    Xrm.Page.getAttribute("ddsm_payeeaddress2").setValue(payeeAdr2);
    Xrm.Page.getAttribute("ddsm_payeecity").setValue(payeeCity);
    Xrm.Page.getAttribute("ddsm_payeestate").setValue(payeeState);
    Xrm.Page.getAttribute("ddsm_payeezipcode").setValue(payeeZip);
    Xrm.Page.getAttribute("ddsm_taxid").setValue(taxId);
    Xrm.Page.getAttribute("ddsm_taxentity").setValue(taxEnt);
    Xrm.Page.getAttribute("ddsm_payeeemail").setValue(payeeEmail);
    //Xrm.Page.getAttribute("ddsm_payeepaymentpreference").setValue(payeePayRef);
    //Xrm.Page.getAttribute("ddsm_payeeroutingnumber").setValue(payeeRoutingNum);
    // Xrm.Page.getAttribute("ddsm_payeeaccountnumber").setValue(payeeAccountNum);
}


function clrPayeeFields() {

    var payeeFields = ["ddsm_payeeaddress1", "ddsm_payeeaddress2", "ddsm_payeecity", "ddsm_payeestate", "ddsm_payeezipcode", "ddsm_payeephone", "ddsm_payeeattnto", "ddsm_payeecompany", "ddsm_taxid", "ddsm_taxentity", "ddsm_payeeemail","ddsm_payeenumber","ddsm_payeename", "ddsm_payeenumber"];
    for (let i = 0; i < payeeFields.length; i++) {
        let el = payeeFields[i];
        AGS.Form.setValue(el, null);
    }
}


function payeeTAQuery() {

    clrPayeeFields();
    var whoGetsPaid = Xrm.Page.getAttribute("ddsm_whogetspaid").getValue();
    if (whoGetsPaid == 962080001) {
        var pSelector = Xrm.Page.getAttribute("ddsm_payeeid").getValue();
        AGS.Form.setValue("ddsm_payeeaccount", null);
    } else {
        var pSelector = Xrm.Page.getAttribute("ddsm_payeeaccount").getValue();
        AGS.Form.setValue("ddsm_payeeid", null);
    }

    if (!pSelector) {
        return;
    }

    var acctMap = {
        ddsm_PayeeCompany: "ddsm_payeecompany",
        ddsm_PayeeAttnTo: "ddsm_payeeattnto",
        ddsm_PayeePhone: "ddsm_payeephone",
        ddsm_PayeeAddress1: "ddsm_payeeaddress1",
        ddsm_PayeeAddress2: "ddsm_payeeaddress2",
        ddsm_PayeeCity: "ddsm_payeecity",
        ddsm_PayeeState: "ddsm_payeestate",
        ddsm_PayeeZipCode: "ddsm_payeezipcode",
        ddsm_TaxEntity: "ddsm_taxentity",
        ddsm_PayeeTaxIDNumber: "ddsm_taxid",
        ddsm_PayeeEmail: "ddsm_payeeemail"
    };

    var selfields = Object.keys(acctMap);

    if (!!pSelector) {
        var accId = pSelector[0].id;
        AGS.REST.retrieveRecord(
            accId,
            'Account',
            selfields.join(','),
            null,
            function (result) {
                if (!result) {
                    return;
                }

                for (let i = 0; i < selfields.length; i++) {
                    let k = selfields[i];

                    if (!result.hasOwnProperty(k)) {
                        continue;
                    }
                    AGS.Form.setValue(acctMap[k], result[k]);
                }
            },
            function (err) {
                console.log(err);
            },
            false
        );
    }
}


function payeeSiteQuery() {

    clrPayeeFields();
    AGS.Form.setValue("ddsm_payeeaccount", null);
    AGS.Form.setValue("ddsm_payeeid", null);

    var payeeSite = Xrm.Page.getAttribute("ddsm_payeesiteid").getValue();
    if (!payeeSite) {
        return;
    }

    var payeeID = payeeSite[0].id;

    var siteMap = {
        ddsm_payeecompany: "ddsm_payeecompany",
        ddsm_payeeattnto: "ddsm_payeeattnto",
        ddsm_payeephone: "ddsm_payeephone",
        ddsm_payeeaddress1: "ddsm_payeeaddress1",
        ddsm_payeeaddress2: "ddsm_payeeaddress2",
        ddsm_payeecity: "ddsm_payeecity",
        ddsm_payeestate: "ddsm_payeestate",
        ddsm_payeezipcode: "ddsm_payeezipcode",
        ddsm_taxentity: "ddsm_taxentity",
        ddsm_taxid: "ddsm_taxid",
        ddsm_payeeemail: "ddsm_payeeemail"
    };

    var selfields = Object.keys(siteMap);

    AGS.REST.retrieveRecord(
        payeeID,
        'ddsm_site',
        selfields.join(','),
        null,
        function (result) {

            if (!result) {
                alert("Error: Could not find associated Payee Site.");
                return;
            }

            for (let i = 0; i < selfields.length; i++) {
                let k = selfields[i];

                if (!result.hasOwnProperty(k)) {
                    continue;
                }
                AGS.Form.setValue(siteMap[k], result[k]);
            }

        },
        function (err) {
            console.log(err);
            alert(err);
        },
        false
    );
}


function payeeLPCQuery() {
    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    var payeeID = Xrm.Page.getAttribute("ddsm_payeelpcid").getValue()[0].id;
    try {
        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_localpowercompanySet?"
            + "$select="
            + "ddsm_payeecompany,"
            + "ddsm_payeeattnto,"
            + "ddsm_PayeePhone,"
            + "ddsm_payeeaddress1,"
            + "ddsm_payeeaddress2,"
            + "ddsm_payeecity,"
            + "ddsm_payeestate,"
            + "ddsm_payeezipcode,"
            + "ddsm_taxentity,"
            + "ddsm_taxid,"
            + "ddsm_payeeemail,"
            + "ddsm_payeepaymentpreference,"
                // + "ddsm_payeeroutingnumber,"
                // + "ddsm_payeeaccountnumber"
            + "&$filter=ddsm_localpowercompanyId eq guid'" + payeeID + "'", false);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.send();

        if (req.readyState == 4 && req.status == 200) {
            var payeeInfo = JSON.parse(req.responseText).d;
            if (payeeInfo.results[0] != null) {
                var payeeCo = payeeInfo.results[0].ddsm_payeecompany;
                var payeeAttnTo = payeeInfo.results[0].ddsm_payeeattnto;
                var payeePhone = payeeInfo.results[0].ddsm_PayeePhone;
                var payeeAdr1 = payeeInfo.results[0].ddsm_payeeaddress1;
                var payeeAdr2 = payeeInfo.results[0].ddsm_payeeaddress2;
                var payeeCity = payeeInfo.results[0].ddsm_payeecity;
                var payeeState = payeeInfo.results[0].ddsm_payeestate;
                var payeeZip = payeeInfo.results[0].ddsm_payeezipcode;
                var taxId = payeeInfo.results[0].ddsm_taxid;
                var taxEnt = payeeInfo.results[0].ddsm_taxentity.Value;
                var payeeEmail = payeeInfo.results[0].ddsm_payeeemail;
                //var payeePayRef = payeeInfo.results[0].ddsm_payeepaymentpreference.Value;
                // var payeeRoutingNum = payeeInfo.results[0].ddsm_payeeroutingnumber;
                // var payeeAccountNum = payeeInfo.results[0].ddsm_payeeaccountnumber;

                PopulatingValues(payeeCo, payeeAttnTo, payeePhone, payeeAdr1, payeeAdr2, payeeCity, payeeState, payeeZip, taxId, taxEnt, payeeEmail/*, payeePayRef, payeeRoutingNum, payeeAccountNum*/);
            } else {
                alert("Error: Could not find associated Payee Site.");
            }
        } else {
            alert("Error: " + req.responseText);
        }
    }
    catch (err) {
        alert("Error: " + err + ".");
    }
}


// Created by TAB on 12/5/12 10:38
// Last Updated: 1/15/14|12:11|TAB: Created payeeLPCQuery function
// Last Updated: 1/15/14|12:11|TAB: Created payeeLPCQuery function
// Last updated: 05/13/2014 - function editions. Added PopulatingValues()