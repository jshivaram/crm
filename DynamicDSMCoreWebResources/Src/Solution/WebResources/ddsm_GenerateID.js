//************************************************************************/
// Filename: ddsm_GenerateID.js
// Usage: Contains the scripts associated with Fncls
// Created: 10/3/16
//************************************************************************

function GenerateID (entityIDCounter, ID_length, cb) {
    AGS.REST.retrieveMultipleRecords(
        'ddsm_admindata',
        "$select=" + entityIDCounter + ",ddsm_admindataId&$filter=ddsm_name eq 'Admin Data'",
        null,
        function (e) {
            console.log('Error: ' + e);
            cb(e);
        },
        function (adminData) {
            var currID = gen_h_pad(adminData[0][entityIDCounter], ID_length);
            var adminObj = {};
            adminObj[entityIDCounter] = adminData[0][entityIDCounter] + 1;
            AGS.REST.updateRecord(adminData[0].ddsm_admindataId, adminObj, "ddsm_admindata", null, errCallbackUpdateAdminData, true);
            cb(null, currID);
        },
        true,
        []
    );
}

function gen_h_pad(n, len) {
    var s = String(n);
    if (s.length < len) {
        s = ('000000' + s).slice(-len);
    }
    return s;
}