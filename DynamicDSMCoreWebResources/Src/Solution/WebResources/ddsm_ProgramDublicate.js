var spinnerForm;
function cloneProgram_grid (guids){
    $("body").attr({"id": "bodyTop"});
    var _target = document.getElementById("bodyTop");

    var httpRequest = null, httpRequest1 = null, guid = null, progObj = {};
    try {
        if (window.XMLHttpRequest) {
            httpRequest = new XMLHttpRequest(); httpRequest1 = new XMLHttpRequest();
        } else {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var ctxt;
        if (typeof GetGlobalContext != "undefined") {
            ctxt = GetGlobalContext();
        }
        else {
            if (typeof Xrm != "undefined") {
                ctxt = Xrm.Page.context;
            }
        }
        var sUrl = ctxt.getClientUrl();
        if (sUrl.match(/\/$/)) {
            sUrl = sUrl.substring(0, sUrl.length - 1);
        }

        //console.log(sUrl);
        httpRequest.open("GET", sUrl + "/WebResources/accentgold_/Script/ags.core.js", false);
        httpRequest.send(null);
        eval(httpRequest.responseText);

        httpRequest1.open("GET", sUrl + "/WebResources/accentgold_/Script/clab.spinner.js", false);
        httpRequest1.send(null);
        eval(httpRequest1.responseText);

        spinnerForm = CreaLab.Spinner.spin(_target);

        guid = guids[0];
        guid = guid.replace(/\{|\}/g, '');
        guid = guid.toLowerCase();

        AGS.REST.retrieveRecord(guid, "ddsm_programcycle", null, null, function(data){
            var new_data = deleteExtraData(data, "ddsm_programcycleId", true);
            new_data.ddsm_name = new_data.ddsm_name + "-Duplicate";
            new_data.statecode = {Value: 0};
            var newRec = AGS.REST.createRecord(new_data, "ddsm_programcycle", null, null, false)
            // alert("This record cloned successfully.");
            progObj.Id = newRec.ddsm_programcycleId;
            progObj.Name = newRec.ddsm_name;
            progObj.LogicalName = "ddsm_programcycle";
            cloneMeasTpl(guid, progObj);
            setTimeout(function(){
                spinnerForm.stop();
                //var parameters = {};
                //parameters["formid"] = newRec.ddsm_programcycleId;
                //Xrm.Utility.openEntityForm("ddsm_programcycle", newRec.ddsm_programcycleId);
                document.getElementById("crmGrid").control.refresh();
            }, 2000);
        }, function(msg){alert(msg);}, false)
    }
    catch (e) {
        alert(e);
    }
}

    function cloneProgram_form() {
    $("body").attr({"id": "bodyTop"});
    var _target = document.getElementById("bodyTop");

    var httpRequest = null, httpRequest1 = null, guid = null, progObj = {};
    try {
        if (window.XMLHttpRequest) {
            httpRequest = new XMLHttpRequest(); httpRequest1 = new XMLHttpRequest();
        } else {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var ctxt;
        if (typeof GetGlobalContext != "undefined") {
            ctxt = GetGlobalContext();
        }
        else {
            if (typeof Xrm != "undefined") {
                ctxt = Xrm.Page.context;
            }
        }
        var sUrl = ctxt.getClientUrl();
        if (sUrl.match(/\/$/)) {
            sUrl = sUrl.substring(0, sUrl.length - 1);
        }

        //console.log(sUrl);
        httpRequest.open("GET", sUrl + "/WebResources/accentgold_/Script/ags.core.js", false);
        httpRequest.send(null);
        eval(httpRequest.responseText);

        httpRequest1.open("GET", sUrl + "/WebResources/accentgold_/Script/clab.spinner.js", false);
        httpRequest1.send(null);
        eval(httpRequest1.responseText);

        spinnerForm = CreaLab.Spinner.spin(_target);

        guid = Xrm.Page.data.entity.getId();
        guid = guid.replace(/\{|\}/g, '');
        guid = guid.toLowerCase();

        AGS.REST.retrieveRecord(guid, "ddsm_programcycle", null, null, function(data){
            var new_data = deleteExtraData(data, "ddsm_programcycleId", true);
            new_data.ddsm_name = new_data.ddsm_name + "-Duplicate";
            new_data.statecode = {Value: 0};
            var newRec = AGS.REST.createRecord(new_data, "ddsm_programcycle", null, null, false)
           // alert("This record cloned successfully.");
            progObj.Id = newRec.ddsm_programcycleId;
            progObj.Name = newRec.ddsm_name;
            progObj.LogicalName = "ddsm_programcycle";
            cloneMeasTpl(guid, progObj);
            setTimeout(function(){
                spinnerForm.stop();
                //var parameters = {};
                //parameters["formid"] = newRec.ddsm_programcycleId;
                Xrm.Utility.openEntityForm("ddsm_programcycle", newRec.ddsm_programcycleId);
            }, 2000);
        }, function(msg){alert(msg);}, false)
    }
    catch (e) {
        alert(e);
    }
}

function cloneMeasTpl(oldProgramGUID, newProgramObj) {
    var queryReq = "$filter=ddsm_ProgramCycle/Id eq guid'" + oldProgramGUID + "'",
        dataReq = [];
    AccentGold.REST.retrieveMultipleRecords("ddsm_measuretemplate", queryReq, null, null, function(data){dataReq = data;}, false, null);
    for(var i = 0; i < dataReq.length; i++){
        var oldMeasGUID = dataReq[i].ddsm_measuretemplateId,
            measObj = {};
        var new_data = deleteExtraData(dataReq[i], "ddsm_measuretemplateId", null);
        new_data.ddsm_name = new_data.ddsm_name + "-Duplicate";
        new_data.statecode = {Value: 0};
        new_data.ddsm_ProgramCycle = newProgramObj;
        var newRec = AGS.REST.createRecord(new_data, "ddsm_measuretemplate", null, function(msg){alert(msg);}, false);
        measObj.Id = newRec.ddsm_measuretemplateId;
        measObj.Name = newRec.ddsm_name;
        measObj.LogicalName = "ddsm_measuretemplate";
        cloneMeasReqDocs(oldMeasGUID, measObj);
    }
    cloneProgramDocs(oldProgramGUID, newProgramObj);
}

function cloneMeasReqDocs(oldMeasGUID, measObj){
    var queryReq = "$filter=ddsm_MeasureTemplateId/Id eq guid'" + oldMeasGUID + "'",
        dataReq = [];
    AccentGold.REST.retrieveMultipleRecords("ddsm_documentconvention", queryReq, null, null, function(data){dataReq = data;}, false, null);
    for(var i = 0; i < dataReq.length; i++) {
        var new_data = deleteExtraData(dataReq[i], "ddsm_documentconventionId", null);
        new_data.ddsm_MeasureTemplateId = measObj;
        AGS.REST.createRecord(new_data, "ddsm_documentconvention", null, function(msg){alert(msg);}, true);
    }
}

function cloneProgramDocs(oldProgramGUID, newProgramObj){
    var queryReq = "$filter=ddsm_programcycleId/Id eq guid'" + oldProgramGUID + "'",
        dataReq = [];
    AccentGold.REST.retrieveMultipleRecords("ddsm_uploadeddocumens", queryReq, null, null, function(data){dataReq = data;}, false, null);
    for(var i = 0; i < dataReq.length; i++) {
        var oldDocGUID = dataReq[i].ddsm_uploadeddocumensId, queryReq1 = "$filter=ObjectId/Id eq guid'" + oldDocGUID + "'", dataReq1 = [];
        var new_data = deleteExtraData(dataReq[i], "ddsm_uploadeddocumensId", true);
        new_data.ddsm_programcycleId = newProgramObj;
        var newRec = AGS.REST.createRecord(new_data, "ddsm_uploadeddocumens", null, function(msg){alert(msg);}, false);
        AccentGold.REST.retrieveMultipleRecords("Annotation", queryReq1, null, null, function(data){dataReq1 = data;}, false, null);
        var new_data1 = deleteExtraData(dataReq1[0], "AnnotationId", null);
        new_data1.ObjectId.Id = newRec.ddsm_uploadeddocumensId;
        AGS.REST.createRecord(new_data1, "Annotation", null, function(msg){alert(msg);}, false);
    }
}

function deleteExtraData(retrievedObject, delKey, delDeferred) {
    if(!!delDeferred) {delDeferred = true;} else {delDeferred = false;}
    delete retrievedObject.__metadata;
    delete retrievedObject[delKey];
    for (var key in retrievedObject) {
        if (retrievedObject.hasOwnProperty(key)) {
            if (retrievedObject[key] == null) {
            } else {
                if (typeof (retrievedObject[key]) == "object") {
                    delete retrievedObject[key].__metadata;
                    if (delDeferred) {
                        if (typeof (retrievedObject[key].__deferred) == "object") {
                           delete retrievedObject[key];
                        }
                    }
                }
            }
        }
    }
    return retrievedObject;
}
