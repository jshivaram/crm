/*
 * Update 12/18/2015
 */
var StateCode = 0,
    milestoneID = null,
    ProjectTaskCount = 0,
    newValTo = new Array(),
    newValFrom = new Array(),
    URI = "",
    req = "";

function onSavePage(){
    window.parent.location.reload(true);
}
function onLoadPage(){
    Xrm.Page.ui.controls.get("subject").setDisabled(true);
    if(Xrm.Page.getAttribute("ddsm_project").getValue() != null) {Xrm.Page.ui.controls.get("ddsm_project").setDisabled(true);}
    if(Xrm.Page.ui.getFormType() != 1) {
        $(document).ready(function() {
            $("ul.ms-crm-CommandBar-Menu", window.parent.document).find("LI[command='ddsm_projecttask|NoRelationship|Form|Mscrm.SavePrimaryActivityAsComplete']").attr({"onclick": "completeStatus('Project Task Completed');"});
            $("ul.ms-crm-CommandBar-Menu", window.parent.document).find("LI[command='ddsm_projecttask|NoRelationship|Form|Mscrm.Form.CloseActivity']").attr({"onclick": "closeStatusBtn();"});
            //$("ul.ms-crm-CommandBar-Menu", window.parent.document).find("[command='ddsm_projecttask|NoRelationship|Form|Mscrm.Form.CloseActivity']").attr({"onclick": "setTimeout(closeStatusBtn(), 200);"});
            //$("ul.ms-crm-CommandBar-Menu", window.parent.document).find("li[id='ddsm_projecttask|NoRelationship|Form|Mscrm.Form.ddsm_projecttask.SaveAsComplete']").find("a").attr({"onclick": "completeStatus();return false;"});
        });
    }
    /*
     $.getScript("/WebResources/accentgold_/EditableGrid/js/XrmServiceToolkit.min.js")
     .done(function() {
     }).fail(function() {
     });
     */
    window.parent.$.ajax({
        url: "/WebResources/accentgold_/EditableGrid/js/XrmServiceToolkit.min.js",
        dataType: "script",
        cache: true
    }).done(function() {});

    StateCode = Xrm.Page.getAttribute("statecode").getValue();
    var Regarding = Xrm.Page.getAttribute("regardingobjectid").getValue();
    if(Regarding != null && Regarding[0].typename == "ddsm_project") {
        Xrm.Page.getAttribute("ddsm_project").setValue(Regarding);
    }

    if(Xrm.Page.getAttribute("ddsm_project").getValue() != null) {
        var projLkp = Xrm.Page.getAttribute("ddsm_project").getValue();
        Xrm.Page.getAttribute("regardingobjectid").setValue(projLkp);
        URI = Xrm.Page.context.getClientUrl() + "/XRMServices/2011/OrganizationData.svc/ddsm_projectSet?"
            + "$select=ddsm_AccountId"
            + "&$filter=ddsm_projectId eq guid'" + projLkp[0].id + "'";
        //console.log(URI);
        req = new XMLHttpRequest();
        req.open("GET", URI, false);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.onreadystatechange = function () {
            if (req.readyState == 4 && req.status == 200) {
                var responseData = JSON.parse(this.responseText).d;
                //console.dir(responseData.results);
                if (responseData.results.length > 0) {
                    var tmpArr = [];
                    tmpArr[0] = {};
                    tmpArr[0].id = responseData.results[0]["ddsm_AccountId"].Id;
                    tmpArr[0].name = responseData.results[0]["ddsm_AccountId"].Name;
                    tmpArr[0].typename = responseData.results[0]["ddsm_AccountId"].LogicalName;
                    Xrm.Page.getAttribute("ddsm_account").setValue(tmpArr);
                }
            }
        }
        req.send();

        if (Xrm.Page.getAttribute("ddsm_milestone").getValue() == null) {
            Xrm.Page.getAttribute("regardingobjectid").setValue(projLkp);
            URI = Xrm.Page.context.getClientUrl() + "/XRMServices/2011/OrganizationData.svc/ddsm_milestoneSet?"
                + "$select=ddsm_milestoneId, ddsm_Index,ddsm_ProjectTaskCount,"
                + "ddsm_name"
                + "&$filter=ddsm_ProjectToMilestoneId/Id eq guid'" + projLkp[0].id + "' and ddsm_Status/Value eq 962080001";
            //console.log(URI);
            req = new XMLHttpRequest();
            req.open("GET", URI, false);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.onreadystatechange = function () {
                if (req.readyState == 4 && req.status == 200) {
                    var responseData = JSON.parse(this.responseText).d;
                    //console.dir(responseData.results);
                    if (responseData.results.length > 0) {
                        var tmpArr = [];
                        tmpArr[0] = {};
                        tmpArr[0].id = responseData.results[0]["ddsm_milestoneId"];
                        tmpArr[0].name = responseData.results[0]["ddsm_name"];
                        tmpArr[0].typename = "ddsm_milestone";
                        Xrm.Page.getAttribute("ddsm_milestone").setValue(tmpArr);
                        Xrm.Page.getAttribute("ddsm_milestoneindex").setValue(parseInt(responseData.results[0]["ddsm_Index"]));
                        Xrm.Page.getAttribute("subject").setValue("Project Task - " + responseData.results[0]["ddsm_name"]);

                        var ProjectTaskCount = (!!(responseData.results[0]["ddsm_ProjectTaskCount"])) ? parseInt(responseData.results[0]["ddsm_ProjectTaskCount"]) : 0;
                        var objMs = {};
                        objMs.ddsm_ProjectTaskCount = ProjectTaskCount + 1;
                        gen_H_updateEntitySync(responseData.results[0]["ddsm_milestoneId"], objMs, "ddsm_milestoneSet");

                        Xrm.Page.data.save().then(
                            function () {
                                setTimeout(onLoadPage, 1000);
                            },
                            function () {
                            }
                        );
                    } else {
                        Xrm.Page.ui.controls.get("ddsm_project").setDisabled(false);
                    }

                }
            }
            req.send();

        }
        var Milestone = Xrm.Page.getAttribute("ddsm_milestone").getValue();
        milestoneID = (Milestone[0].id).replace(/\{|\}/g, '');
        window.parent.milestoneID = milestoneID;
        window.parent.ProjectTaskCount = ProjectTaskCount;
        newValTo[0] = new Object();
        newValFrom[0] = new Object();

        newValTo[0].id = Xrm.Page.getAttribute("ownerid").getValue()[0].id.replace(/\{|\}/g, '');
        newValTo[0].name = Xrm.Page.getAttribute("ownerid").getValue()[0].name;
        newValTo[0].typename = Xrm.Page.getAttribute("ownerid").getValue()[0].typename;

        newValFrom[0].id = (Xrm.Page.context.getUserId()).replace(/\{|\}/g, '');
        newValFrom[0].name = Xrm.Page.context.getUserName();
        newValFrom[0].typename = "systemuser";
        /*
         console.log("milestoneID: " + milestoneID);
         console.dir(newValTo);
         console.dir(newValFrom);
         */
        window.parent.newValTo = newValTo;
        window.parent.newValFrom = newValFrom;
        window.parent.gen_H_updateEntitySync = gen_H_updateEntitySync;
        //window.parent.XrmServiceToolkit = XrmServiceToolkit;

        if(Milestone != null && StateCode == 0) {
            var URI = Xrm.Page.context.getClientUrl() + "/XRMServices/2011/OrganizationData.svc/ddsm_milestoneSet?"
                + "$select="
                + "ddsm_ProjectTaskCount"
                + "&$filter=ddsm_milestoneId eq guid'" + Milestone[0].id + "'";
            var req = new XMLHttpRequest();
            req.open("GET", URI, true);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.onreadystatechange = function () {
                if (this.readyState == 4) {
                    req.onreadystatechange = null;
                    if (this.status == 200) {
                        var responseData = JSON.parse(this.responseText).d;
                        ProjectTaskCount = parseInt(responseData.results[0]["ddsm_ProjectTaskCount"]);
                        console.log("ProjectTaskCount: " + ProjectTaskCount);
                    }
                }
            }
            req.send();
        }

    }
}

window.parent.completeStatus = function(headerEmail){
    $.ajax({
        url: "/WebResources/accentgold_/EditableGrid/js/XrmServiceToolkit.min.js",
        dataType: "script",
        cache: true
    }).done(function() {

        if(milestoneID != null && ProjectTaskCount != 0) {
            /*
             console.log(ProjectTaskCount);
             console.log(milestoneID);
             console.log("-------------------------------------");
             console.dir(newValTo[0].id);
             console.dir(newValFrom[0].typename);
             */
            ProjectTaskCount--;
            if(ProjectTaskCount < 0){ProjectTaskCount = 0;}
            gen_H_updateEntitySync(milestoneID, {"ddsm_ProjectTaskCount": ProjectTaskCount}, "ddsm_milestoneSet");

            var createEmail = new XrmServiceToolkit.Soap.BusinessEntity("email");
            var from = [
                { id: (newValFrom[0].id).replace(/\{|\}/g, ''), logicalName: newValFrom[0].typename, type: "EntityReference" }
            ];
            createEmail.attributes["from"] = { value: from, type: "EntityCollection" };
            var to = [
                { id: (newValTo[0].id).replace(/\{|\}/g, ''), logicalName: newValTo[0].typename, type: "EntityReference" }
            ];
            createEmail.attributes["to"] = { value: to, type: "EntityCollection" };

            createEmail.attributes["subject"] = "Project Task: "  + Xrm.Page.getAttribute("subject").getValue();
            //createEmail.attributes["description"] = "Hi " + newValTo[0].name + ",<br/><br/>" + "Project Task Completed.<br/><br/>";
            var textEmail = "Project Task: " + Xrm.Page.getAttribute("subject").getValue() + "<br/>";
            textEmail += "Project: " + Xrm.Page.getAttribute("ddsm_project").getValue()[0].name + "<br/>";
            if(Xrm.Page.getAttribute("description").getValue() != null){
                textEmail += "Details: " + Xrm.Page.getAttribute("description").getValue();
            }
            createEmail.attributes["description"] = createEmailBody(null, headerEmail, newValTo[0].name, newValFrom[0].name, textEmail);
            createEmail.attributes["directioncode"] = true;

            //console.dir(createEmail);

            var emailId = XrmServiceToolkit.Soap.Create(createEmail);
            //console.log(emailId);
            alert(headerEmail + ". Notification will be sent to <" + newValTo[0].name + ">");


            var URL = Xrm.Page.context.getClientUrl() +"/XRMServices/2011/Organization.svc/web";
            var requestMain = ""
            requestMain += "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">";
            requestMain += "  <s:Body>";
            requestMain += "    <Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">";
            requestMain += "      <request i:type=\"b:SendEmailRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\" xmlns:b=\"http://schemas.microsoft.com/crm/2011/Contracts\">";
            requestMain += "        <a:Parameters xmlns:c=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">";
            requestMain += "          <a:KeyValuePairOfstringanyType>";
            requestMain += "            <c:key>EmailId</c:key>";
            requestMain += "            <c:value i:type=\"d:guid\" xmlns:d=\"http://schemas.microsoft.com/2003/10/Serialization/\">" + emailId + "</c:value>";
            requestMain += "          </a:KeyValuePairOfstringanyType>";
            requestMain += "          <a:KeyValuePairOfstringanyType>";
            requestMain += "            <c:key>IssueSend</c:key>";
            requestMain += "            <c:value i:type=\"d:boolean\" xmlns:d=\"http://www.w3.org/2001/XMLSchema\">true</c:value>";
            requestMain += "          </a:KeyValuePairOfstringanyType>";
            requestMain += "          <a:KeyValuePairOfstringanyType>";
            requestMain += "            <c:key>TrackingToken</c:key>";
            requestMain += "            <c:value i:type=\"d:string\" xmlns:d=\"http://www.w3.org/2001/XMLSchema\" />";
            requestMain += "          </a:KeyValuePairOfstringanyType>";
            requestMain += "        </a:Parameters>";
            requestMain += "        <a:RequestId i:nil=\"true\" />";
            requestMain += "        <a:RequestName>SendEmail</a:RequestName>";
            requestMain += "      </request>";
            requestMain += "    </Execute>";
            requestMain += "  </s:Body>";
            requestMain += "</s:Envelope>";
            var req = new XMLHttpRequest();
            req.open("POST", URL, false);
            req.setRequestHeader("Accept", "application/xml, text/xml, */*");
            req.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
            req.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute");
            req.send(requestMain);

        }

    });
};

window.parent.closeStatusBtn = function(){
    //console.log($("#InlineDialog_Iframe", window.parent.document).contents());

    if($("#InlineDialog_Iframe", window.parent.document).contents().length == 1) {
        $("#InlineDialog_Iframe", window.parent.document).bind("load", function () {
            $(this).contents().find("#butBegin").attr({"onclick": "if(!Mscrm.Utilities.resetValidationFailedElement()){if(document.getElementById('statusCode').options[document.getElementById('statusCode').selectedIndex].text.toLowerCase() == 'completed'){window.parent.completeStatus('Project Task Completed');}else{window.parent.completeStatus('Project Task Canceled');}applychanges();}"});
        });
    } else {
        setTimeout(window.parent.closeStatusBtn(), 100);
    }
};