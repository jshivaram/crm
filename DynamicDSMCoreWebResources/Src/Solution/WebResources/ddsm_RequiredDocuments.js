//Config
var RequiredDocuments_ViewName = "RequiredDocuments_View", reqDocRelationName = {}, reqDocParentField = {};
//Relationship
reqDocRelationName["ddsm_project"] = "ddsm_ddsm_project_ddsm_requireddocument_Project";

//Paren field
reqDocParentField["ddsm_project"] = "ddsm_project";

//Lib
var clickObjectTr = null, clickObjectTrClass = null;

function InitRequiredDocuments() {
    if(typeof GlobalJs != "object") {
        setTimeout(InitRequiredDocuments, 500);
        return;
    }
    if(typeof GlobalJs.ContentPanel != "object") {
        setTimeout(InitRequiredDocuments, 500);
        return;
    }
    if(typeof GlobalJs.ContentPanel.onLoad != "function") {
        setTimeout(InitRequiredDocuments, 500);
        return;
    }
    GlobalJs.ContentPanel.onLoad(function(){

        var style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = "#RequiredDocuments_View_divDataArea #gridBodyTable tr[uloaded='cancel'] { background-color: #AAAAAA;}" +
            "#RequiredDocuments_View_divDataArea #gridBodyTable tr[uloaded='true'] { background-color: #AAEEAA;}" +
            "#RequiredDocuments_View_divDataArea #gridBodyTable tr[uloaded='false'] { background-color: #EEAAAA;}" +
            "#RequiredDocuments_View_divDataArea #gridBodyTable tr[selected='true'], " +
            "#RequiredDocuments_View_divDataArea #gridBodyTable tr[uloaded='cancel'][selected='true']," +
            "#RequiredDocuments_View_divDataArea #gridBodyTable tr[uloaded='true'][selected='true']," +
            "#RequiredDocuments_View_divDataArea #gridBodyTable tr[uloaded='false'][selected='true'] {background-color: #3EA99F;}";
        document.getElementsByTagName('head')[0].appendChild(style);

        hideContorls_ReqDocsView();
        customFilterToRequiredAttachDocs();
    });
}

function hideContorls_ReqDocsView() {
    console.log("hideContorls_ReqDocsView");
    var viewName = Xrm.Page.getControl(RequiredDocuments_ViewName);
    if(!viewName) {
        setTimeout(function() {
            hideContorls_ReqDocsView();
        }, 500);
    } else {
        $("#" + "titleContainer_" + RequiredDocuments_ViewName).hide();
        Xrm.Page.getControl(RequiredDocuments_ViewName).addOnLoad(function (e){
                setTimeout(function(){
                    $("#" + "titleContainer_" + e._element.id).hide();
                    $("#" + e._element.id + "_ccDiv .ms-crm-grid-databodycontainer-Ex table tbody tr [id^=gridBodyTable_delete]").css({'display': 'none'});
                    BackGroundColorGrid(e);
                }, 250);
            }
        );
    }
}

function customFilterToRequiredAttachDocs(){

    console.log(">>> customFilterToRequiredAttachDocs");

    if(typeof DocumentsJs == "undefined" || typeof DocumentsJs.Attachments == "undefined" || typeof DocumentsJs.Attachments._getViewName != "function")
    {
        setTimeout(customFilterToRequiredAttachDocs,1000);
        return;
    }
    var attachDocsView = DocumentsJs.Attachments._getViewName();

    var ctrl = Xrm.Page.getControl(attachDocsView);
    if(!ctrl){
        setTimeout(customFilterToRequiredAttachDocs,1000);
        return;
    }

    Xrm.Page.getControl(attachDocsView).addOnLoad(function(e){
        Xrm.Page.getControl(RequiredDocuments_ViewName).refresh();
    });

    return;

    var isGridLoaded = null;
    try {isGridLoaded = ctrl.getEntityName();}
    catch(err)
    {
        return;
    }
    if (!!isGridLoaded)
    {
        if(!(typeof reqDocParentField[Xrm.Page.data.entity.getEntityName()] == "string" && !!reqDocParentField[Xrm.Page.data.entity.getEntityName()])) {
            return;
        }

        var GUIDvalue = Xrm.Page.data.entity.getId().replace(/[{}]/g,'');

        var FetchXml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false' >\
            <entity name='ddsm_attachmentsdocument' >\
            <attribute name='subject' />\
            <attribute name='description' />\
            <attribute name='createdon' />\
            <order attribute='createdon' descending='true' />\
            <filter type='or' >\
            <filter type='and' >\
            <condition attribute='regardingobjectid' operator='eq' value='" + GUIDvalue + "' />\
            <condition attribute='statecode' operator='eq' value='0' />\
            </filter>\
            <filter type='and' >\
            <condition entityname='ddsm_requireddocument' attribute='" + reqDocParentField[Xrm.Page.data.entity.getEntityName()] + "' operator='eq' value='" + GUIDvalue + "' />\
            <condition attribute='statecode' operator='eq' value='0' />\
            </filter>\
            </filter>\
            <link-entity name='ddsm_requireddocument' from='ddsm_requireddocumentid' to='regardingobjectid' link-type='outer' />\
            </entity>\
            </fetch>";

        var Grid = document.getElementById(attachDocsView);
        if (!!Grid.control) {
            Grid.control.SetParameter("fetchXml", FetchXml);
            Grid.control.refresh();
        } else {
            setTimeout(customFilterToRequiredAttachDocs, 500);
            return;
        }

    }

}

function GetRequiredDocumentList() {
    var reqDocsList = [];
    if(!(typeof reqDocRelationName[Xrm.Page.data.entity.getEntityName()] == "string" && !!reqDocRelationName[Xrm.Page.data.entity.getEntityName()])) {
        return reqDocsList;
    }

    var req = new XMLHttpRequest();
    req.open("GET", encodeURI(Xrm.Page.context.getClientUrl() + "/api/data/v8.1/" + Xrm.Page.data.entity.getEntityName() + "s(" + (Xrm.Page.data.entity.getId()).replace(/\{|\}/g, '') + ")/" + reqDocRelationName[Xrm.Page.data.entity.getEntityName()]), false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.setRequestHeader("OData-MaxVersion", "4.0");
    req.setRequestHeader("OData-Version", "4.0");
    req.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
    req.onreadystatechange = function () {
        if (this.readyState == 4 /* complete */) {
            req.onreadystatechange = null;
            if (this.status == 200) {
                var results = JSON.parse(this.response);
                reqDocsList = results.value;
                console.dir(reqDocsList);
            }
            else {
                var err = JSON.parse(this.response).error;
                console.log(err);
            }
        }
    };
    req.send();

    return reqDocsList;
}

function SelectedRequiredDocument(fileList) {
    var fileList = fileList;

    var entityReferenceCollection = [], entityReference = {};
    entityReference.id = Xrm.Page.data.entity.getId();
    entityReference.typename = Xrm.Page.data.entity.getEntityName();
    entityReference.type = Xrm.Internal.getEntityCode(Xrm.Page.data.entity.getEntityName());
    entityReferenceCollection.push(entityReference);

    if(Xrm.Page.getControl(RequiredDocuments_ViewName).getGrid().getSelectedRows().getLength() == 1) {
        //entityReferenceCollection = [];
        entityReference = {};
        entityReference.id = Xrm.Page.getControl(RequiredDocuments_ViewName).getGrid().getSelectedRows().get(0).getData().getEntity().getEntityReference().id;
        entityReference.typename = Xrm.Page.getControl(RequiredDocuments_ViewName).getGrid().getSelectedRows().get(0).getData().getEntity().getEntityReference().entityType;
        entityReference.type = Xrm.Internal.getEntityCode(entityReference.typename);
        entityReferenceCollection.push(entityReference);
        DocumentsJs.Attachments._processFiles(fileList, entityReferenceCollection);
        return;
    }

    if(!(typeof reqDocParentField[Xrm.Page.data.entity.getEntityName()] == "string" && !!reqDocParentField[Xrm.Page.data.entity.getEntityName()])) {
        return;
    }


    Alert.show(null, "The document you are attaching is not among the list of required documents. Please select an option below.", [{
        label: "Override",
        title: "Select the required documents that are no longer required as a result of this document attachment.",
        callback: function () {
            GenLookupView(fileList);
        }
    }, {
        label: "Do Not Override",
        title: "Attach this document without overriding an existing document requirement.",
        callback: function () {
            DocumentsJs.Attachments._processFiles(fileList, entityReferenceCollection);
        }
    }, {
        label: "Cancel",
        title: "Cancel Attachment of Document.",
        callback: function () {
            return;
        }
    }], "QUESTION", 500, 200);

}

function GenLookupView(fileList) {
    var fileList = fileList;


    var objectCode = Xrm.Internal.getEntityCode("ddsm_requireddocument");

    var fetchxml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
        "<entity name='ddsm_requireddocument'>" +
        "<attribute name='ddsm_name' />" +
        "<attribute name='ddsm_requiredbystatus' />" +
        "<order attribute='ddsm_requiredbystatus' descending='false' />" +
        "<order attribute='ddsm_name' descending='false' />" +
        "<filter type='and'>" +
        "<condition attribute='" + reqDocParentField[Xrm.Page.data.entity.getEntityName()] + "' operator='eq' value='" + Xrm.Page.data.entity.getId() + "' />" +
        "<condition attribute='ddsm_uploaded' operator='eq' value='false' />" +
        "<condition attribute='ddsm_canceled' operator='eq' value='false' />" +
        "</filter>" +
        "</entity>" +
        "</fetch>";

    var layout = "<grid name='resultset' object='1' jump='fullname' select='1' preview='0' icon='1'>" +
        "<row name='result' id='ddsm_requireddocumentid'>" +
        "<cell name='ddsm_requiredbystatus' width='100' />" +
        "<cell name='ddsm_name' width='300' />" +
        "</row>" +
        "</grid>";

    var viewId = "{00000000-0000-0000-00AA-000010001002}";

    var customView = {
        fetchXml: fetchxml,
        id: viewId,
        layoutXml: layout,
        name: "Required Document List",
        recordType: objectCode,
        Type: 0
    };

    Mscrm.Utilities.returnLookupItems = function (lookupItems, lookupField, bPopulateLookup, callbackReference) {
        var entityReferenceCollection = [], entityReference = {};
        entityReference.id = Xrm.Page.data.entity.getId();
        entityReference.typename = Xrm.Page.data.entity.getEntityName();
        entityReference.type = Xrm.Internal.getEntityCode(Xrm.Page.data.entity.getEntityName());
        entityReferenceCollection.push(entityReference);

        if (lookupItems != null) {
            if (lookupItems.items.length > 0) {
                console.dir(lookupItems);
                entityReferenceCollection = [];
                for(var i = 0; i < lookupItems.items.length; i++) {
                    entityReference = {};
                    entityReference.typename = lookupItems.items[i].typename;
                    entityReference.id = lookupItems.items[i].id;
                    entityReference.type = lookupItems.items[i].type;
                    entityReferenceCollection.push(entityReference);
                }

                entityReference = {};
                entityReference.id = Xrm.Page.data.entity.getId();
                entityReference.typename = Xrm.Page.data.entity.getEntityName();
                entityReference.type = Xrm.Internal.getEntityCode(Xrm.Page.data.entity.getEntityName());
                entityReferenceCollection.push(entityReference);

                DocumentsJs.Attachments._processFiles(fileList, entityReferenceCollection);
            }
        } else {
            DocumentsJs.Attachments._processFiles(fileList, entityReferenceCollection);
        }

    };
    var callbackFunctionObject = Mscrm.Utilities.createCallbackFunctionObject('returnLookupItems', Mscrm.Utilities, [null, null], false);
    LookupObjectsWithCallback(callbackFunctionObject, null, "multi", objectCode, 0, null, "", "0", null, null, null, null, null, viewId, [customView]);
}


function BackGroundColorGrid(grid) {
    try {
        setTimeout(function(){

            var items = Xrm.Page.getControl(grid._element.id).getGrid().getRows().getAll(),
                itemsCount = Xrm.Page.getControl(grid._element.id).getGrid().getRows().getLength(),
                pendingMsIndex = -1;
            if(!!Xrm.Page.getAttribute("ddsm_pendingmilestoneindex")) {
                pendingMsIndex = Xrm.Page.getAttribute("ddsm_pendingmilestoneindex").getValue();
            }
            if (items) {

                var indexMsIdx = $("#" + grid._element.id + " #gridBodyTable").find("col[name=ddsm_requiredbystatus]").index();
                var indexUploaded = $("#" + grid._element.id + " #gridBodyTable").find("col[name=ddsm_uploaded]").index();
                var indexCanceled = $("#" + grid._element.id + " #gridBodyTable").find("col[name=ddsm_canceled]").index();

                for (var i = 0; i < itemsCount; i++) {
                    var id = items[i].getKey();
                    $(grid._element).find("tr[oid='" + id + "']").each(function () {
                        var theTr = $(this);

                        if (indexCanceled != -1 && (theTr.find("td").slice(indexCanceled, indexCanceled + 1).text()).toLowerCase() == "yes") {
                            theTr.attr("uloaded", "cancel")
                        }

                        if (indexUploaded != -1 && (theTr.find("td").slice(indexUploaded, indexUploaded + 1).text()).toLowerCase() == "yes") {
                            theTr.attr("uloaded", "true")
                        }

                        if (indexMsIdx != -1 && indexUploaded != -1 && indexCanceled != -1 && (theTr.find("td").slice(indexUploaded, indexUploaded + 1).text()).toLowerCase() == "no" && parseInt(theTr.find("td").slice(indexMsIdx, indexMsIdx + 1).text()) <= pendingMsIndex && (theTr.find("td").slice(indexCanceled, indexCanceled + 1).text()).toLowerCase() == "no") {
                            theTr.attr("uloaded", "false")
                        }

                    });
                }

            }

        }, 1000);
    }
    catch (e) {
        console.log(e.description);
    }
}






