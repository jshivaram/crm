//Update 06/09/2016

$("body").attr({"id": "bodyTop1"});
var spinnerForm;


function onLoadMeasureButtons() {
        ConvertToButton('ddsm_showmadfieldsbtn', 'Show MAD Fields', '125px', showMAD, 'Show MAD Fields');
}

// Open HTML WebResources in the new window
// openWebResourcePage("example.html")
function openWebResourcePage(pageName) {
    var ctxt;
    if (typeof GetGlobalContext != "undefined") {
        ctxt = GetGlobalContext();
    }
    else {
        if (typeof Xrm != "undefined") {
            ctxt = Xrm.Page.context;
        }
    }
    var sUrl = ctxt.getClientUrl();
    if (sUrl.match(/\/$/)) {
        sUrl = sUrl.substring(0, sUrl.length - 1);
    }
    var c_url = sUrl + '/WebResources/accentgold_/' + pageName;
    var w = 1036, h = 600;
    if (typeof (openStdWin) == "undefined") {
        return window.open(c_url, '', "width=" + w + ",height=" + h + "," + "resizable=1");
    } else {
        Xrm.Utility.openWebResource('accentgold_/' + pageName, null, w, h);
    }

}

// Open UI to edit JSON data
function editMSD() {
    openWebResourcePage('EspMeasTplEditJSON.html');
}


function showPreloader(show) {
    if (show) {
// show overlay
        spinnerForm = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Loading data");
    }
    else {
        spinnerForm.stop();
    }
}

// Open HTML WebResources in the new window
// openWebResourcePage("example.html",queryString)
function openWebResourcePage(pageName,w,h) {
    var ctxt;
    if (typeof GetGlobalContext != "undefined") {
        ctxt = GetGlobalContext();
    }
    else {
        if (typeof Xrm != "undefined") {
            ctxt = Xrm.Page.context;
        }
    }
    var sUrl = ctxt.getClientUrl();
    if (sUrl.match(/\/$/)) {
        sUrl = sUrl.substring(0, sUrl.length - 1);
    }
    var c_url = sUrl + '/WebResources/accentgold_/' + pageName;
    //var w = 1036, h = 600;
    if (!w) {
        w = 1036;
    }
    if (!h) {
        h = 600;
    }
    if (typeof (openStdWin) == "undefined") {
        return window.open(c_url, '', "width=" + w + ",height=" + h + "," + "resizable=1");
    } else {
        Xrm.Utility.openWebResource('accentgold_/' + pageName, null, w, h);
    }

}


function showMAD(){
    //var p_addr = 'EspMadGrid.html';
    window.ddsm_madfields = AGS.Form.getValue('ddsm_madfields') || "";
    var p_addr = 'EspMadMapping_MeasTpl.html';
    var q = "?source_f=ddsm_madfields";
    //TODO: send query string on opening
    //openWebResourcePage(p_addr, 700);
    openWebResourcePage(p_addr, 1160);
}

function ConvertToButton(fieldname, buttontext, buttonwidth, clickevent, title) {
    //check if object exists; else return
    if ($("#" + fieldname)[0] == null) { return; }
    var functiontocall = clickevent;
    $("#" + fieldname)[0].textContent = buttontext;
    $("#" + fieldname)[0].setAttribute("readonly", true);
    $("#" + fieldname).css({
        "borderRight": "#3366cc 1px solid", "paddingRight": "5px", "borderTop": "#3366cc 1px solid",
        "paddingLeft": "5px", "fontSize": "11px", "backgroundImage": "url(/_imgs/btn_rest.gif)",
        "borderLeft": "#3366cc 1px solid", "width": buttonwidth, "cursor": "hand",
        "lineHeight": "18px", "borderBottom": "#3366cc 1px solid", "backgroundRepeat": "repeat-x",
        "fontFamily": "Tahoma", "height": "20px", "backgroundColor": "#cee7ff", "textAlign": "center",
        "overflow": "hidden"
    });
    if (window.attachEvent) {
        $("#" + fieldname)[0].attachEvent("onmousedown", MouseDown);
        $("#" + fieldname)[0].attachEvent("onmouseup", MouseUp);
        $("#" + fieldname)[0].attachEvent("onclick", functiontocall);
    } else {
        $("#" + fieldname)[0].addEventListener("mousedown", MouseDown, false);
        $("#" + fieldname)[0].addEventListener("mouseup", MouseUp, false);
        $("#" + fieldname)[0].addEventListener("click", functiontocall, false);
    }
    $("#" + fieldname)[0].title = title;
    window.focus();

    function MouseDown(evt) {
        var eventer;
        if (evt.srcElement) {
            eventer = evt.srcElement;   // event.srcElement in IE
        } else {
            eventer = evt.target;   // event.target in most other browsers
        }
        $("#" + eventer).css({
            "borderWidth": "2px", "borderStyle": "groove ridge ridge groove",
            "borderColor": "#3366cc #4080f0 #4080f0 #3366cc"
        });
    }

    function MouseUp(evt) {
        var eventer;
        if (evt.srcElement) {
            eventer = evt.srcElement;
        } else {
            eventer = evt.target;
        }
        $("#" + eventer).css({ "border": "1px solid #3366cc" });
    }
}
