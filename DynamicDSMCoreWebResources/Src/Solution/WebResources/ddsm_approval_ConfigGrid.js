/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 2.1.2 DSM
 * Last updated on 03/31/2015.
 * Milestones Grid
 */
///// START CONFIG GRID
var configJson = {
    entitySchemaName: "ddsm_projectgroupapproval",
    dateFormat: "m/d/Y",
    sorters: { property: 'ddsm_Index', direction: 'ASC' },
    modelGrid: "milestoneGrid",
    idGrid: "mtsGridId",
    border: true,
    height: 304,
    title: '',
    entityConcat: "",
    fields: [
        {
            header: '#',
            name: 'ddsm_Index',
            type: 'number',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 30,
            hidden: false
        }, {
            header: 'Name',
            name: 'ddsm_name',
            type: 'string',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 170,
            hidden: false
        }, {
            header: 'Approval Date',
            name: 'ddsm_ApprovalDate',
            type: 'date',
            defaultValue: null,
            sortable: false,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 75,
            saved: true,
            hidden: false
        }, {
            header: 'Skip',
            name: 'ddsm_Skip',
            type: 'checkcolumn',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 50,
            saved: true,
            hidden: false
        }, {
            header: 'Responsible',
            name: 'ddsm_Responsible',
            type: 'string',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 50,
            hidden: false
        }, {
            header: 'ApprCust',
            name: 'ddsm_ApprovalThresholdCust',
            type: 'currency',
            defaultValue: 0,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }
    ]
};
///// END CONFIG GRID

var testSatusData = [
    [ "962080000", "Not Started" ],
    [ "962080001", "Active" ],
    [ "962080002", "Completed" ],
    [ "962080003", "Skipped" ]
];
//// START RENDERER CURRENCY COLUMN
var rendererCurrency = function (value, meta) {
    return Ext.util.Format.usMoney(value);
};
//// END RENDERER CURRENCY COLUMN



///// START LOCAL COMBOBOX CONFIG
function _getOptionSetDataArray(entityName, optionSetName, customStore){
    SDK.Metadata.RetrieveAttribute(entityName, optionSetName, "00000000-0000-0000-0000-000000000000", true,
        function (result) {
            var Data = [];
            for (var i = 0; i < result.OptionSet.Options.length; i++) {
                var dataSet = [];
                var text = result.OptionSet.Options[i].Label.LocalizedLabels[0].Label;
                var value = result.OptionSet.Options[i].Value;
                dataSet = [value.toString(), text.toString()];
                Data.push(dataSet);
            }
            if(customStore !== null) {
                eval(customStore).add(Data);
            } else {
                eval(optionSetName + "Combo").add(Data);
            }
        },
        function (error) { }
    );
}
//------------------------------------------------------
var ddsm_StatusCombo =  new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_milestone", "ddsm_Status", null);

var ddsm_StatusColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    store: ddsm_StatusCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_StatusColumnRenderer = function (value) {
    var index = ddsm_StatusCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_StatusCombo.getAt(index).data;
        return rs.display;
    }
};
//------------------------------------------------------
var ddsm_CategoryComboFilter = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
ddsm_CategoryComboFilter.add({value:"000000000", display:"All"});
_getOptionSetDataArray("ddsm_milestone", "ddsm_Category", "ddsm_CategoryComboFilter");
//------------------------------------------------------
var ddsm_CategoryCombo = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_milestone", "ddsm_Category", null);
var ddsm_CategoryColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    store: ddsm_CategoryCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_CategoryColumnRenderer = function (value) {
    var index = ddsm_CategoryCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_CategoryCombo.getAt(index).data;
        return rs.display;
    }
};
///// END LOCAL COMBOBOX CONFIG
