/**
 * Created by Yurii on 08/03/16.
 */

$("body").attr({"id": "bodyTop1"});



function deleteSelected(grid) {

    var callbackDialog = function(dialogOutputParam)
    {
        debugger;
        showPreloader(true);
        console.log(JSON.stringify(grid));
        Process.callAction("ddsm_CreateDeletionRequestTask",
            [{
                key: "Target",
                type: Process.Type.EntityReference,
                value: {id: grid[0].Id, entityType: grid[0].TypeName}
            },
                {
                    key: "SelectedItemsJson",
                    type: Process.Type.String,
                    value: JSON.stringify(grid)
                },
                {
                key: "Description",
                type: Process.Type.String,
                value: dialogOutputParam.comment
                },
                {
                    key: "ReqUser",
                    type: Process.Type.String,
                    value: dialogOutputParam.reqUser
                }
            ],
            function (params) {
                debugger;
                var _result = "";
                var _complete = "";
                // Success
                showPreloader(false);
                for (var i = 0; i < params.length; i++) {

                    if (params[i].key == "Complete") {
                        _complete = params[i].value;
                    }
                    if (params[i].key == "Result") {
                        _result = params[i].value;
                    }
                }
                if (_complete !== "true") {
                    Alert.show(_result, _result, null, "WARNING", 500, 200);
                }
                else {
                    let msg = "Deletion Request Creation is complete";
                    Alert.show(msg, null, null, "SUCCESS", 500, 200);
                }
            },null

                )};

    var callback = function () {
        ShowDREForm(callbackDialog);
    };

    /*
    var callback = function () {


        let msg  = "Would you like delete selected records?";
        Alert.show(msg, null,
            [{
                label: "Yes",
                callback: function () {
                    showPreloader(true);
                    console.log(JSON.stringify(grid));
                    Process.callAction("ddsm_CreateDeletionRequestTask",
                        [{
                            key: "Target",
                            type: Process.Type.EntityReference,
                            value: {id: grid[0].Id, entityType: grid[0].TypeName}
                        },
                            {
                                key: "SelectedItemsJson",
                                type: Process.Type.String,
                                value: JSON.stringify(grid)
                            }
                        ],
                        function (params) {
                            debugger;
                            var _result = "";
                            var _complete="";
                            // Success
                            showPreloader(false);
                            for (var i = 0; i < params.length; i++) {

                                if(params[i].key=="Complete")
                                {
                                    _complete=params[i].value;
                                }
                                if(params[i].key=="Result")
                                {
                                    _result=params[i].value;
                                }
                            }
                            if (_complete !== "true") {
                                Alert.show(_result,null, null, "WARNING", 500, 200);
                            }
                            else {
                                let msg = "Deletion Request Creation is complete";
                                Alert.show(msg,  null, null, "SUCCESS", 500, 200);
                            }
                        },
                        function (e) {
                            // Error
                            console.log(e);
                            showPreloader(false);
                            Alert.show("Error",e, null, "ERROR", 500, 200);
                            alert("Error: " + e);
                        }
                    );
                }
            },
                {
                    label: "No"
                }], "QUESTION", 500, 200);
    };
    */

    var processJs = function () {
        LoadScript("../WebResources/mag_/js/process.js", "js", callback);
    };
    var alert_load = function(){
        LoadScript("../WebResources/mag_/js/alert.js", "js", processJs);
    };
    LoadScript("../WebResources/accentgold_/Script/clab.spinner.js", "js", alert_load);
}


function showPreloader(show) {
    if (show) {
// show overlay
        spinnerForm = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Loading data");
    }
    else {
        spinnerForm.stop();
    }
}

function LoadScript(url, type, callback) {
    // Adding the script tag to the head as suggested before
    var head = document.getElementsByTagName('head')[0];
    if(type == "js") {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;

// Then bind the event to the callback function.
// There are several events for cross browser compatibility.
        script.onreadystatechange = callback;
        script.onload = callback;

// Fire the loading
        head.appendChild(script);
        console.log|(url);
    }

    if(type == "css") {
        var css = document.createElement('link');
        css.type = 'text/css';
        css.rel = 'stylesheet';
        css.href = url;

        css.onreadystatechange = callback;
        css.onload = callback;

        head.appendChild(css);
        console.log|(url);
    }
}



function ShowDREForm(callbackDialog)
{
    var DialogOption = new Xrm.DialogOptions;
    DialogOption.width = 500;
    DialogOption.height = 450;
    Xrm.Internal.openDialog("/WebResources/ddsm_dreDialog",DialogOption,null,null,callbackDialog);

}

function ParseQueryString(query) {
    var result = {};

    if (typeof query == "undefined" || query == null) {
        return result;
    }

    var queryparts = query.split("&");
    for (var i = 0; i < queryparts.length; i++) {
        var params = queryparts[i].split("=");
        result[params[0]] = params.length > 1 ? params[1] : null;
    }
    return result;
}


