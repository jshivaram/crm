/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 1.0.0
 * Last updated on 06/18/2015.
 * Project Group Financials Grid
 */
///// START CONFIG GRID
var configJson = {
    entitySchemaName: "ddsm_projectgroupfinancials",
    dateFormat: "m/d/Y",
    sorters: { property: 'ddsm_name', direction: 'ASC' },
    modelGrid: "financialGrid",
    idGrid: "mtsGridId",
    border: true,
    height: 288,
    title: '',
    fields: [
        {
            header: 'Name',
            name: 'ddsm_name',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                if (value != null) {
                    return Ext.String.format(
                        '<a href="/main.aspx?etn={2}&extraqs=&histKey={3}&id={1}&newWindow=true&pagetype=entityrecord" target="_blank">{0}</a>',
                        value,
                        "{" + record.data["ddsm_projectgroupfinancialsId"] + "}",
                        "ddsm_projectgroupfinancials",
                        randomnumber);
                } else { return ''; }
            },
            hidden: false
        }, {
            groupHeader: "Incentive Amount",
            group: [{
                header: 'Calculated',
                name: 'ddsm_calculatedamount',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 100,
                hidden: false
            }, {
                header: 'Actual',
                name: 'ddsm_actualamount',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 100,
                hidden: false
            }]
        }, {
            header: 'Pending Stage',
            name: 'ddsm_pendingstage',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 150,
            hidden: false
        }, {
            groupHeader: "1. Check Requested",
            group: [
                {
                    header: 'Target Start',
                    name: 'ddsm_targetstart1',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }, {
                    header: 'Actual Start',
                    name: 'ddsm_actualstart1',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }, {
                    header: 'Target End',
                    name: 'ddsm_targetend1',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }, {
                    header: 'Actual End',
                    name: 'ddsm_actualend1',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }]
        }, {
            groupHeader: "2. Check Cut",
            group: [
                {
                    header: 'Target Start',
                    name: 'ddsm_targetstart2',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }, {
                    header: 'Actual Start',
                    name: 'ddsm_actualstart2',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }, {
                    header: 'Target End',
                    name: 'ddsm_targetend2',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }, {
                    header: 'Actual End',
                    name: 'ddsm_actualend2',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }]
        }, {
            groupHeader: "3. Check Delivered",
            group: [
                {
                    header: 'Target Start',
                    name: 'ddsm_targetstart3',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }, {
                    header: 'Actual Start',
                    name: 'ddsm_actualstart3',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }, {
                    header: 'Target End',
                    name: 'ddsm_targetend3',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }, {
                    header: 'Actual End',
                    name: 'ddsm_actualend3',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }]
        }, {
            groupHeader: "4. Check Cashed",
            group: [
                {
                    header: 'Target Start',
                    name: 'ddsm_targetstart4',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }, {
                    header: 'Actual Start',
                    name: 'ddsm_actualstart4',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }, {
                    header: 'Target End',
                    name: 'ddsm_targetend4',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }, {
                    header: 'Actual End',
                    name: 'ddsm_actualend4',
                    type: 'date',
                    defaultValue: null,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 100,
                    hidden: false
                }]
        }, {
            header: 'Invoice Date',
            name: 'ddsm_invoicedate',
            type: 'date',
            defaultValue: null,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            groupHeader: "Payee Info",
            group: [{
                header: 'Company',
                name: 'ddsm_payeecompany',
                type: 'string',
                defaultValue: '',
                sortable: true,
                filterable: false,
                readOnly: true,
                allowBlank: true,
                width: 150,
                hidden: false
            }, {
                header: 'Attn To',
                name: 'ddsm_payeeattnto',
                type: 'string',
                defaultValue: '',
                sortable: true,
                filterable: false,
                readOnly: true,
                allowBlank: true,
                width: 150,
                hidden: false
            }, {
                header: 'Address 1',
                name: 'ddsm_payeeaddress1',
                type: 'string',
                defaultValue: '',
                sortable: true,
                filterable: false,
                readOnly: true,
                allowBlank: true,
                width: 150,
                hidden: false
            }, {
                header: 'Address 2',
                name: 'ddsm_payeeaddress2',
                type: 'string',
                defaultValue: '',
                sortable: true,
                filterable: false,
                readOnly: true,
                allowBlank: true,
                width: 150,
                hidden: false
            }, {
                header: 'City',
                name: 'ddsm_payeecity',
                type: 'string',
                defaultValue: '',
                sortable: true,
                filterable: false,
                readOnly: true,
                allowBlank: true,
                width: 150,
                hidden: false
            }, {
                header: 'State',
                name: 'ddsm_payeestate',
                type: 'string',
                defaultValue: '',
                sortable: true,
                filterable: false,
                readOnly: true,
                allowBlank: true,
                width: 150,
                hidden: false
            }, {
                header: 'Zip Code',
                name: 'ddsm_payeezipcode',
                type: 'string',
                defaultValue: '',
                sortable: true,
                filterable: false,
                readOnly: true,
                allowBlank: true,
                width: 150,
                hidden: false
            }, {
                header: 'Phone',
                name: 'ddsm_payeephone',
                type: 'string',
                defaultValue: '',
                sortable: true,
                filterable: false,
                readOnly: true,
                allowBlank: true,
                width: 150,
                hidden: false
            }, {
                header: 'E-mail',
                name: 'ddsm_payeeemail',
                type: 'string',
                defaultValue: '',
                sortable: true,
                filterable: false,
                readOnly: true,
                allowBlank: true,
                width: 150,
                hidden: false
            }]
        }, {
            header: 'Check Number',
            name: 'ddsm_checknumber',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: false
        }, {
            header: 'Status',
            name: 'ddsm_status',
            type: 'combobox',
            defaultValue: null,
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: false,
            editor: 'ddsm_StatusColumnEditor',
            renderer: 'ddsm_StatusColumnRenderer'
        }, {
            header: 'ddsm_stagename1',
            name: 'ddsm_stagename1',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'ddsm_stagename2',
            name: 'ddsm_stagename2',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'ddsm_stagename3',
            name: 'ddsm_stagename3',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'ddsm_stagename4',
            name: 'ddsm_stagename4',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'ddsm_stagename5',
            name: 'ddsm_stagename5',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'ddsm_duration1',
            name: 'ddsm_duration1',
            type: 'number',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'ddsm_duration2',
            name: 'ddsm_duration2',
            type: 'number',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'ddsm_duration3',
            name: 'ddsm_duration3',
            type: 'number',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'ddsm_duration4',
            name: 'ddsm_duration4',
            type: 'number',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'ddsm_duration5',
            name: 'ddsm_duration5',
            type: 'number',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }
    ]
};
///// END CONFIG GRID

//// START RENDERER CURRENCY COLUMN
var rendererCurrency = function (value, meta) {
    return Ext.util.Format.usMoney(value);
};
///// START LOCAL COMBOBOX CONFIG
var ddsm_StatusCombo = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: [
        ["962080000", "Not Started"],
        ["962080001", "Active"],
        ["962080002", "Completed"]
    ]
});
var ddsm_StatusColumnEditor = {
    mode: 'local',
    xtype: 'combo',
    store: ddsm_StatusCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_StatusColumnRenderer = function (value) {
    var index = ddsm_StatusCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_StatusCombo.getAt(index).data;
        return rs.display;
    }
};
///// END LOCAL COMBOBOX CONFIG