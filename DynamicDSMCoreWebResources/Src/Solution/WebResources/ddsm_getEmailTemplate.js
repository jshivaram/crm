function createEmailBody(TemplateId, headerEmail, toUserName, fromUserName, textEmail){
    if(TemplateId == null || TemplateId == "") {TemplateId = "801786D8-CEEC-E411-A256-6C3BE5A8A238";} //Custom Email Template (Dynamic DSM)
    var objectId = Xrm.Page.data.entity.getId();
    var objectTypeCode = Xrm.Internal.getEntityCode(Xrm.Page.data.entity.getEntityName());

    var BodyEmail = getEmailTemplate(TemplateId, objectId, objectTypeCode);
    BodyEmail = BodyEmail.replace("{headerEmail}", headerEmail);
    BodyEmail = BodyEmail.replace("{toUser}", toUserName);
    BodyEmail = BodyEmail.replace("{fromUser}", fromUserName);
    BodyEmail = BodyEmail.replace("{textEmail}", textEmail);
    //console.dir(BodyEmail);
    return BodyEmail;
}

function getEmailTemplate(TemplateId, objectId, objectTypeCode){
    var command = new RemoteCommand("EmailTemplateService", "GetInstantiatedEmailTemplate");
    command.SetParameter("templateId", TemplateId);
    command.SetParameter("objectId", objectId);
    command.SetParameter("objectTypeCode", objectTypeCode);
    var result = command.Execute();

    if (result.Success) {
        if (typeof (result.ReturnValue) == "string") {
            var oXml = CreateXmlDocument(result.ReturnValue);
            return oXml.lastChild.lastElementChild.textContent;
        }
    }
}

function CreateXmlDocument(signatureXmlStr) {
    var parseXml;
   if (window.DOMParser) {
        parseXml = function (xmlStr) {
            return (new window.DOMParser()).parseFromString(xmlStr, "text/xml");
        };
    }
    else if (typeof window.ActiveXObject != "undefined" && new window.ActiveXObject("Microsoft.XMLDOM")) {
        parseXml = function (xmlStr) {
            var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = "false";
            xmlDoc.loadXML(xmlStr);

            return xmlDoc;
        };
    }
    else {
        parseXml = function () { return null; }
    }
    var xml = parseXml(signatureXmlStr);
    if (xml) {
        return xml;
    }
}