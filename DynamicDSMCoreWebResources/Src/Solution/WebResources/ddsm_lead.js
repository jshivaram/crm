function ContactTA_OnChange() {
    var field_val = Xrm.Page.getAttribute("ddsm_tradeallycontact").getValue();

    if(!field_val){
        Xrm.Page.getAttribute("ddsm_tradeally").setValue(null);
        return;
    }

    AGS.REST.retrieveRecord(field_val[0].id, 'Contact', 'ParentCustomerId', null, function (res) {
        var value = [
            {
                entityType: res.ParentCustomerId.LogicalName,
                id: res.ParentCustomerId.Id,
                name: res.ParentCustomerId.Name
            }
        ];
        Xrm.Page.getAttribute("ddsm_tradeally").setValue(value);
    }, null, true);
}

function OnLoad() {
    if( !Xrm.Page.getControl("SiteMeasure")) //.getGrid
    {
        console.log("-SiteMeasure is not loaded");
        setTimeout(OnLoad,3000);
        return;
    }

    console.log("-Stage1.After SiteMeasure loaded");
    if(Xrm.Page.getAttribute("ddsm_convertedtoproject").getValue() !==true ) {
        /// version 2 . Create Project from Lead using QuickCreate form
        console.log("-Stage2.Lead is not Converted");
        var stage = Xrm.Page.data.process.getActiveStage();

///write to top current Lead Id
        window.parent.top.leadId = Xrm.Page.data.entity.getId();

        var parentAcc = Xrm.Page.getAttribute("parentaccountid").getValue();
        var isConverted = Xrm.Page.getAttribute("ddsm_convertedtoproject").getValue();
        if (stage && stage.getName()=="Convert"  && parentAcc[0] && !isConverted) {
            console.log("-Stage3.Current step is Convert");
            // ** Create an object including the accountId. Used to call the
            // ** quick create form in the context of the account
            var parentAccount = {
                entityType: "account",
                id: parentAcc[0].id
            };

            var array = [];
// *** NOTE: Here we aren't setting any parameters! (But we could!)
            var parameters = {};
           // debugger;


            //get Site MEasures
            AGS.REST.retrieveMultipleRecords(
                'ddsm_sitemeasure',
                "$filter=ddsm_InitialLead/Id eq (guid'"+ Xrm.Page.data.entity.getId() +"')",
                function() {},
                errorCallback,
                function(data) {

                    if (data.length) {
                        var len;
                        if (data.length > 5) {
                            len = 5;
                        } else {
                            len = data.length;
                        }
                        for (var i = 0; i < len; i++) {
                            if (data[i].ddsm_MeasureSelector.Id) {
                                var property = 'ddsm_measuretemplate' + (i + 1);
                                var propertyname = property + 'name';
                                parameters[property] = data[i].ddsm_MeasureSelector.Id;
                                parameters[propertyname] = data[i].ddsm_MeasureSelector.Name;
                            }
                            if (data[i].ddsm_phase1units) {
                                var quantity = 'ddsm_measurequantity' + (i + 1);
                                parameters[quantity] = data[i].ddsm_phase1units;
                            }
                        }
                      //  debugger;
/// add list of site measures to QuickCreate project form
                            window.parent.top.leadSiteMeasureList =[];
                        data.forEach(function(sm){
                                window.parent.top.leadSiteMeasureList.push(sm.ddsm_sitemeasureId);
                            });


                    }
                },
                false,
                array);


            console.log("Pring parameters for QC form:");
            console.log(parameters);

            // *** Call the Xrm.Utility needed to add a contact.
            Xrm.Utility.openQuickCreate("ddsm_project", parentAccount, parameters).then(
                function (lookup) {
                    successCallback(lookup);
                }, function (error) {
                    errorCallback(error);
                });
            // *** Function called on success.
            function successCallback(lookup) {
                console.dir(lookup);
                Xrm.Page.getAttribute("ddsm_convertedtoproject").setValue(true);
                //   console.log("lookup: " + lookup.savedEntityReference.id);
                //    console.log("lookup: " + lookup.savedEntityReference.name);
            }
            // **** Function called on error.
            function errorCallback(e) {
                // *** No new contact created, which is an error we can ignore!
                alert("Error: " + e.errorCode + " " + e.message);
            }
        }
    }

    /// code for plugin
    /*
     var formType = Xrm.Page.ui.getFormType();
     if(formType)
     {
     var stage =Xrm.Page.data.process.getActiveStage();
     if(stage)
     {
     if(stage.getName() == "Converted")
     {
     Process.callDialog("130E4543-2412-4754-BF87-C7568768E3ED", "lead",
     Xrm.Page.data.entity.getId(),
     function () {
     Xrm.Page.data.refresh();
     });
     }
     }
     }
     else
     {
     console.log("FormType: " + formType);
     }
     */
}