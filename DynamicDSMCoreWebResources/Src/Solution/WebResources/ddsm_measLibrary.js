$("body").attr({"id": "bodyTop1"});

function setSubmitMode(fields, mode) {
    if(!Array.isArray(fields)){
        return;
    }
    if (!mode) {
        mode = "always";
    }
    try{
        for (let i = 0; i< fields.length; i++) {
            let attr = Xrm.Page.getAttribute(fields[i].toLowerCase());
            if(!attr){
                continue;
            }
            attr.setSubmitMode(mode);
        }
    }catch (e){
        console.log("submit mode is not changed",e);
    }
}

function showPreloader(show) {
    if (show) {
// show overlay
        spinnerForm = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Loading data");
    }
    else {
        if (!!spinnerForm) {
            spinnerForm.stop();
        }
    }
}

function ConvertButtons() {
    ConvertToButton('ddsm_newmtversion_btn', 'New MT Version', '125px', createMTVersion, 'New  MT Version');
    ConvertToButton('ddsm_newmodelnumberversion_btn', 'New  Model Number Version', '125px', createModelNumberVersion, 'New  Model Number Version');
    ConvertToButton('ddsm_newskuversion_btn', 'New  SKU Version', '125px', createSKUVersion, 'New  SKU Version');
}

function createMTVersion(){
    createNewVersion("ddsm_measuretemplateid", "Measure Template")
}

function createModelNumberVersion(){
    createNewVersion("ddsm_modelnumber", "Model Number")
}

function createSKUVersion(){
    createNewVersion("ddsm_sku", "SKU")
}

function createNewVersion(fieldName, customFieldName) {

    if (!Xrm.Page.getAttribute(fieldName) || !Xrm.Page.getAttribute(fieldName).getValue()) {
        Alert.show("Error", AGS.String.format("Field '{0}' is not populated", customFieldName || fieldName), null, "ERROR", 500, 200);
        return;
    }

    var fieldVal = Xrm.Page.getAttribute(fieldName).getValue();

    if (!Array.isArray(fieldVal) || !fieldVal[0]) {
        Alert.show("Error", AGS.String.format("Field '{0}' is not a lookup", customFieldName || fieldName), null, "ERROR", 500, 200);
        return;
    }

    if (!fieldVal[0].id) {
        Alert.show("Error", AGS.String.format("Wrong data ({0}) ", customFieldName || fieldName), null, "ERROR", 500, 200);
        return;
    }

    showPreloader(true);
    Alert.show("The system is going to create a new version of the record. The current one would be deactivated. Continue?", null,
        [
            {
                label: "Yes",
                callback: function () {

                    let pluginName;

                    let paramList = [
                        //{
                        //    key: "Target",
                        //    type: Process.Type.EntityReference,
                        //    value: {id: fieldVal[0].id, entityType: fieldVal[0].entityType}
                        //},
                        {
                            key: "Guids",
                            type: Process.Type.String,
                            //value: JSON.stringify(listOfIds)
                            value: fieldVal[0].id.replace(/([{}])+/g,'')
                        },
                        {
                            key: "TargetEntity",
                            type: Process.Type.String,
                            value: AGS.String.format("{0}|{1}", fieldVal[0].entityType,fieldVal[0].id)
                        },
                        {
                            key: "RelateField",
                            type: Process.Type.String,
                            value: AGS.String.format("{0}|{1}|{2}", 'ddsm_measurelibrary',Xrm.Page.data.entity.getId().replace(/([{}])+/g,'') ,fieldName)
                        },
                        {
                            key: "DeactivateCurrent",
                            type: Process.Type.Bool,
                            value: true
                        }
                    ];

                    if(fieldVal[0].entityType == 'ddsm_measuretemplate'){
                        pluginName = "ddsm_DDSMCloneMeasureTemplateRecords";
                        paramList.push(
                            {
                                key: "Target",
                                type: Process.Type.EntityReference,
                                value: {id: fieldVal[0].id, entityType: fieldVal[0].entityType}
                            }
                        );
                    }else{
                        pluginName = "ddsm_DDSMCloneRecord";
                    }

                    Process.callAction(pluginName,
                        paramList,
                        function (params) {
                            // Success
                            showPreloader(false);
                            for (var i = 0; i < params.length; i++) {
                                console.log(params[i].key + "=" + params[i].value);
                            }

                            //if(fieldVal[0].entityType == 'ddsm_measuretemplate'){
                            //    setFromMeasTpl();
                            //}

                            Alert.show("New version is created.", null,[
                                {
                                    label:"OK",
                                    callback: function(){
                                        Xrm.Page.data.refresh();
                                    }
                                }
                            ], "SUCCESS", 500, 200);
                        },
                        function (e) {
                            // Error
                            console.log(e);
                            showPreloader(false);
                            Alert.show("Error", e, null, "ERROR", 500, 200);
                            //alert("Error: " + e);
                        }
                    );

                }
            },
            {
                label: "No",
                callback: function () {
                    showPreloader(false);
                }
            }
        ], "QUESTION", 500, 200);
}

function setFieldValue(obj, dataObj) {
    var numTypes = ["decimal", "money", "integer", "bigint"];
    var ch = false;
    if (!dataObj) {
        dataObj = {};
    }
    for (var k in obj) {
        var val = null;
        var el = obj[k].toLowerCase();
        if (!Xrm.Page.getAttribute(el)) {
            continue;
        }

        if (numTypes.indexOf(Xrm.Page.getAttribute(el).getAttributeType()) < 0) {
            val = (!dataObj[k] || !dataObj[k].hasOwnProperty('Value')) ? (dataObj[k] || null ) : (dataObj[k].Value || null );
            if (!!val && val.hasOwnProperty('LogicalName')) {
                var valOld = val;
                val = [{id: valOld.Id, name: valOld.Name, typename: valOld.LogicalName}];//override lookup object
            }
        } else {
            val = (!dataObj[k] || !dataObj[k].hasOwnProperty('Value')) ? (+dataObj[k] || 0) : (+dataObj[k].Value || 0);
        }

        if (Xrm.Page.getAttribute(el).getValue() != val) {
            Xrm.Page.getAttribute(el).setValue(val);
            ch = true;
        }
    }
    return ch;
}

//TODO: Move to AGS core
/**
 * Get array of object values
 * @param obj
 * @returns {Array}
 */
function getObjectValues(obj){
   if(!obj || !(obj instanceof(Object)) ) {
       return [];
   }
    return Object.keys(obj).map(function (cur) {
        return obj[cur];
    });
}

function setFromMeasTpl(){
    var field_val = Xrm.Page.getAttribute("ddsm_measuretemplateid").getValue();
    var measTplMap = {
        ddsm_MeasureID: 'ddsm_Measure_ID',
        ddsm_MeasureCode: 'ddsm_MeasureCode'
    };
    var fieldsArr = Object.keys(measTplMap);
    var formFieldsArr = getObjectValues(measTplMap);
    setSubmitMode(formFieldsArr);

    if (!field_val || !field_val.length) {
        for (var k in measTplMap) {
            var el = measTplMap[k];
            Xrm.Page.getAttribute(el.toLowerCase()).setValue(null);
        }
        return true;
    }
    AGS.REST.retrieveRecord(
        field_val[0].id,
        'ddsm_measuretemplate',
        fieldsArr.join(','),
        null,
        function (res) {
            if(!res || !Object.keys(res).length){
                return;
            }
            setFieldValue(measTplMap, res);
        },
        function (e) { console.log(e); },
        true
    );
    return true;
}
