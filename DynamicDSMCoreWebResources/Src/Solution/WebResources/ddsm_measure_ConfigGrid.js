/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 2.2.7
 * Last updated on 04/25/2016.
 * Measures Grid
 */
///// START CONFIG GRID

var configJson = {
    entitySchemaName: "ddsm_measure",
    dateFormat: "m/d/Y",
    sorters: { property: 'ddsm_ProjectPhaseName', direction: 'DESC' },
    modelGrid: "measureGrid",
    idGrid: "mtsGridId",
    border: true,
    height: 260,
    title: '',
    fields: [
        {
            header: 'Measure Name',
            name: 'ddsm_name',
            type: 'string',
            defaultValue: '',
            sortable: true,
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                if (value != null) {
                    return Ext.String.format(
                        '<a href="/main.aspx?etn={2}&extraqs=&histKey={3}&id={1}&newWindow=true&pagetype=entityrecord" target="_blank">{0}</a>',
                        value,
                        "{" + record.data["ddsm_measureId"] + "}",
                        "ddsm_measure",
                        randomnumber);
                } else { return ''; }
            },
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 160,
            hidden: false
        }, {
            header: 'Measure Template',
            name: 'ddsm_MeasureSelector',
            type: 'lookup',
            entityNameLookup: 'ddsm_measuretemplate',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 160,
            hidden: false
        }, {
            header: 'Phase',
            name: 'ddsm_ProjectPhaseName',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Initial Phase Date',
            name: 'ddsm_InitialPhaseDate',
            type: 'date',
            defaultValue: null,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Project Phase Number',
            name: 'ddsm_UpToDatePhase',
            type: 'int',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: true
        },
        /*{
         groupHeader: "&nbsp;",
         group: [{
         header: 'Savings kWh',
         name: 'ddsm_SavingskWh',       //ddsm_CurrentSavingskWh
         type: 'decimal',
         defaultValue: 0,
         sortable: true,
         filterable: false,
         readOnly: true,
         allowBlank: true,
         width: 75,
         hidden: false
         }, {
         header: 'Savings KW',
         name: 'ddsm_SavingsKW',       //ddsm_CurrentSavingsKW
         type: 'decimal',
         defaultValue: 0,
         sortable: true,
         filterable: false,
         readOnly: true,
         allowBlank: true,
         width: 75,
         hidden: false
         }, {
         header: 'Savings thm',
         name: 'ddsm_Savingsthm',       //ddsm_CurrentSavingsthm
         type: 'decimal',
         defaultValue: 0,
         sortable: true,
         filterable: false,
         readOnly: true,
         allowBlank: true,
         width: 75,
         hidden: false
         }]
         },*/
        {
            groupHeader: "Current Phase",
            group: [{
                header: 'Units',
                name: 'ddsm_Units',       //ddsm_CurrentUnits
                type: 'decimal',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                width: 40,
                hidden: false
            },{
            //    header: 'Net to Gross Ratio',
            //    name: 'ddsm_NettoGrossRatio',
            //    type: 'decimal',
            //    defaultValue: 0,
            //    sortable: true,
            //    filterable: false,
            //    readOnly: true,
            //    allowBlank: true,
            //    width: 125,
            //    hidden: false
            //},{
            //    header: 'Tank Volume (gal)',
            //    name: 'ddsm_TankVolumegal',
            //    type: 'decimal',
            //    defaultValue: 0,
            //    sortable: true,
            //    filterable: false,
            //    readOnly: true,
            //    allowBlank: true,
            //    width: 125,
            //    hidden: false
            //},{
            //    header: 'New Water Heater efficiency (0.00 -1.00)',
            //    name: 'ddsm_NewWaterHeaterefficiency000100',
            //    type: 'decimal',
            //    defaultValue: 0,
            //    sortable: true,
            //    filterable: false,
            //    readOnly: true,
            //    allowBlank: true,
            //    width: 125,
            //    hidden: false
            //},{
            //    header: 'Water Heater Type',
            //    name: 'ddsm_WaterHeaterType',
            //    type: 'combobox',
            //    defaultValue: null,
            //    sortable: true,
            //    filterable: false,
            //    readOnly: false,
            //    allowBlank: true,
            //    width: 100,
            //    hidden: false,
            //    editor: 'ddsm_WaterHeaterTypeColumnEditor',
            //    renderer: 'ddsm_WaterHeaterTypeColumnRenderer'
            //},
            //    {
            //        header: 'Building Type',
            //        name: 'ddsm_BuildingType',
            //        type: 'combobox',
            //        defaultValue: null,
            //        sortable: true,
            //        filterable: false,
            //        readOnly: false,
            //        allowBlank: true,
            //        width: 100,
            //        hidden: false,
            //        editor: 'ddsm_BuildingTypeColumnEditor',
            //        renderer: 'ddsm_BuildingTypeColumnRenderer'
            //    },
            //    {
            //        header: 'Climate Zone',
            //        name: 'ddsm_climatezone',
            //        type: 'combobox',
            //        defaultValue: null,
            //        sortable: true,
            //        filterable: false,
            //        readOnly: false,
            //        allowBlank: true,
            //        width: 100,
            //        hidden: false,
            //        editor: 'ddsm_climatezoneColumnEditor',
            //        renderer: 'ddsm_climatezoneColumnRenderer'
            //    },
            //    {
            //        header: 'Unit Participant Incremental Cost',
            //        name: 'ddsm_UnitParticipantIncrementalCost',
            //        type: 'currency',
            //        defaultValue: 0,
            //        sortable: true,
            //        filterable: false,
            //        readOnly: true,
            //        allowBlank: true,
            //        renderer: 'rendererCurrency',
            //        width: 125,
            //        hidden: false
            //    },{
                    header: 'Incentive Payment-Net',
                    name: 'ddsm_IncentivePaymentNet',  //ddsm_phase1incentivetotal   ddsm_CurrentIncTotal
                    type: 'currency',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    renderer: 'rendererCurrency',
                    width: 125,
                    hidden: false
                },{
                    header: 'Incentive Payment Total Gross',
                    name: 'ddsm_IncentivePaymentTotalGross',  //ddsm_phase1incentivetotal   ddsm_CurrentIncTotal
                    type: 'currency',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: true,
                    allowBlank: true,
                    renderer: 'rendererCurrency',
                    width: 125,
                    hidden: true
                }, {
                    header: 'Incentive Payment Gross',
                    name: 'ddsm_IncentivePaymentGross', //ddsm_phase1incentiveunits
                    type: 'currency',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    renderer: 'rendererCurrency',
                    width: 125,
                    hidden: true
                }, {
                    header: 'Incentive Payment Electric Gross', // ddsm_CurrentIncElectric
                    name: 'ddsm_IncentivePaymentElectricGross',
                    type: 'currency',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    renderer: 'rendererCurrency',
                    width: 125,
                    hidden: true
                }, {
                    header: 'Incentive Payment Gas Gross', // ddsm_CurrentIncGas
                    name: 'ddsm_IncentivePaymentGasGross',
                    type: 'currency',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    renderer: 'rendererCurrency',
                    width: 125,
                    hidden: true
                }, {
                    header: 'kWh Gross Savings at Meter',
                    name: 'ddsm_kWhGrossSavingsatMeter',    //ddsm_phase1savingskwh
                    type: 'decimal',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 125,
                    hidden: false
                }, {
                    header: 'kW Gross Savings at Meter',
                    name: 'ddsm_kWGrossSavingsatMeter',   //ddsm_phase1savingskw
                    type: 'decimal',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 125,
                    hidden: false
                },
                //Comment because field status "ddsm_phase1savingsthm" = deleted in E1 Schema
                {
                    header: 'M3 Savings',
                    name: 'ddsm_M3Savings',
                    type: 'decimal',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 125,
                    hidden: false
                },
                {
                    header: 'Water Savings',
                    name: 'ddsm_WaterSavings',
                    type: 'decimal',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 125,
                    hidden: false
                },
                {
                    header: 'GJ Savings at Meter',
                    name: 'ddsm_GJSavingsatMeter',
                    type: 'decimal',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 125,
                    hidden: false
                },
                {
                    header: 'Incentive Payment-Gross Per Unit',
                    name: 'ddsm_IncentivePaymentGrossPerUnit',
                    type: 'currency',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    renderer: 'rendererCurrency',
                    width: 100,
                    hidden: false
                }, {
                    header: 'kWh Gross Savings at Meter per unit',
                    name: 'ddsm_kWhGrossSavingsatMeterperunit',  //ddsm_phase1perunitkwh
                    type: 'decimal',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 75,
                    hidden: false
                }, {
                    header: 'kW Gross Savings at Meter per unit',
                    name: 'ddsm_kWGrossSavingsatMeterperunit',   //ddsm_phase1perunitkw
                    type: 'decimal',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 75,
                    hidden: false
                },
                //Comment because field status "ddsm_phase1perunitthm" = deleted in E1 Schema
                {
                    header: 'M3 Savings per Unit',
                    name: 'ddsm_M3SavingsperUnit',
                    type: 'decimal',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 75,
                    hidden: false
                },
                {
                    header: 'Water Savings per Unit',
                    name: 'ddsm_WaterSavingsperUnit',
                    type: 'decimal',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 75,
                    hidden: false
                },
                {
                    header: 'GJ Savings at Meter per unit',
                    name: 'ddsm_GJSavingsatMeterperunit',
                    type: 'decimal',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 75,
                    hidden: false
                },
                {
                    header: 'Per Unit CCF',
                    name: 'ddsm_Phase1PerUnitCCF',
                    type: 'decimal',
                    defaultValue: 0,
                    sortable: true,
                    filterable: false,
                    readOnly: false,
                    allowBlank: true,
                    width: 75,
                    hidden: true
                }]
        }, {
            groupHeader: "Cost",
            group: [{
                header: 'OEM Recovery',
                name: 'ddsm_OEMRecoveryCost',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 75,
                hidden: false
            },{
                header: 'Equipment',
                name: 'ddsm_CostEquipment',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 75,
                hidden: false
            }, {
                header: 'Labor',
                name: 'ddsm_CostLabor',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 75,
                hidden: false
            }, {
                header: 'Incremental Cost',
                name: 'ddsm_IncrementalCost',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 100,
                hidden: true
            }, {
                header: 'Other',
                name: 'ddsm_CostOther',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 75,
                hidden: false
            }, {
                header: 'Total',
                name: 'ddsm_CostTotal',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 75,
                hidden: false
            }]
        }, {
            header: 'Implementation Site',
            name: 'ddsm_ImplementationSite',
            type: 'lookup',
            entityNameLookup: 'ddsm_site',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 150,
            hidden: false
        }, {
            header: 'Program',
            name: 'ddsm_ProgramId',
            type: 'lookup',
            entityNameLookup: 'ddsm_program',
            defaultValue: null,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            header: 'Program Offering',
            name: 'ddsm_ProgramOfferingsId',
            type: 'lookup',
            entityNameLookup: 'ddsm_programoffering',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 150,
            hidden: false
        }, {
            header: 'Parent Project',
            name: 'ddsm_ProjectToMeasureId',
            type: 'lookup',
            entityNameLookup: 'ddsm_project',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: false
        }, {
            header: 'Parent Site',
            name: 'ddsm_parentsite',
            type: 'lookup',
            entityNameLookup: 'ddsm_site',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: false
        }, {
            header: 'Parent Company',
            name: 'ddsm_AccountId',
            type: 'lookup',
            entityNameLookup: 'account',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: false
        }, {
            header: 'Delivery Agent',
            name: 'ddsm_TradeAllyId',
            type: 'lookup',
            entityNameLookup: 'account',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 150,
            hidden: false
        }, {
            header: 'Equipment Description',
            name: 'ddsm_EquipmentDescription',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'Eff Equip Descr',
            name: 'ddsm_EffEquipDescr',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'Measure Code',
            name: 'ddsm_MeasureCode',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            header: 'Measure Number',
            name: 'ddsm_MeasureNumber',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            header: 'Energy Type',
            name: 'ddsm_EnergyType',
            type: 'combobox',
            defaultValue: null,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false,
            editor: 'ddsm_EnergyTypeColumnEditor',
            renderer: 'ddsm_EnergyTypeColumnRenderer'
        }, {
            header: 'Category',
            name: 'ddsm_Category',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            header: 'Measure Group',
            name: 'ddsm_MeasureGroup',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            header: 'Measure Subgroup',
            name: 'ddsm_MeasureSubgroup',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            header: 'Measure ID',
            name: 'ddsm_Measure_ID',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        },
        //Comment because field status "ddsm_projectstatus" = deleted in E1 Schema
        /* {
         header: 'Savings Calculation Type',
         name: 'ddsm_SavingsCalculationType',
         type: 'lookup',
         entityNameLookup: 'ddsm_calculationtype',
         defaultValue: '',
         sortable: true,
         filterable: false,
         readOnly: false,
         allowBlank: true,
         width: 150,
         hidden: false
         },*/ {
            header: 'Funding Source Calculation Type',
            name: 'ddsm_FundingSourceCalculationType',
            type: 'lookup',
            entityNameLookup: 'ddsm_sourcecalculationtype',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 150,
            hidden: false
        }, {
            header: 'Measure Relevance',
            name: 'ddsm_MeasureRelevance',
            type: 'combobox',
            defaultValue: null,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false,
            editor: 'ddsm_MeasureRelevanceColumnEditor',
            renderer: 'ddsm_MeasureRelevanceColumnRenderer'
        }, {
            header: 'Measure Modeling Type',
            name: 'ddsm_MeasureModelingType',
            type: 'combobox',
            defaultValue: null,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false,
            editor: 'ddsm_MeasureModelingTypeColumnEditor',
            renderer: 'ddsm_MeasureModelingTypeColumnRenderer'
        }, {
            header: 'Quantity of Major Measures',
            name: 'ddsm_QuantityofMajorMeasures',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            header: 'Incentive Payment Gross-Level 1 PerUnit',
            name: 'ddsm_IncentivePaymentGrossLevel1PerUnit',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 75,
            hidden: false
        }, {
            header: 'Incentive Payment Gross-Level 2 PerUnit',
            name: 'ddsm_IncentivePaymentGrossLevel2PerUnit',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 75,
            hidden: false
        }, {
            header: 'Incentive Payment Gross-Level 3 PerUnit',
            name: 'ddsm_IncentivePaymentGrossLevel3PerUnit',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 75,
            hidden: false
        }, {
            header: 'OEM Recovery Cost',
            name: 'ddsm_OEMRecoveryCost',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 75,
            hidden: false
        }, {
            header: 'Incentive Payment-Net',
            name: 'ddsm_IncentivePaymentNet',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 75,
            hidden: false
        }, {
            header: 'ddsm_Size',
            name: 'ddsm_Size',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_EUL',
            name: 'ddsm_EUL',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            header: 'ddsm_lifecyclesavingskw',
            name: 'ddsm_lifecyclesavingskw',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_lifecyclesavingskwh',
            name: 'ddsm_lifecyclesavingskwh',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_lifecyclesavingsthm',
            name: 'ddsm_lifecyclesavingsthm',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Base Equip Descr',
            name: 'ddsm_BaseEquipDescr',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'End Use',
            name: 'ddsm_EndUse',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            header: 'Measure Notes',
            name: 'ddsm_MeasureNotes',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            header: 'ddsm_Source',
            name: 'ddsm_Source',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            header: 'Unit',
            name: 'ddsm_Unit',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            header: 'Study Measure',
            name: 'ddsm_StudyMeasure',
            type: 'boolean',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 50,
            hidden: true
        }, {
            header: 'Measure Type',
            name: 'ddsm_MeasureType',
            type: 'combobox',
            defaultValue: null,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false,
            editor: 'ddsm_MeasureTypeColumnEditor',
            renderer: 'ddsm_MeasureTypeColumnRenderer'
        }, {
            header: 'Annual Hours Before',
            name: 'ddsm_AnnualHoursBefore',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Annual Hours After',
            name: 'ddsm_AnnualHoursAfter',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        },
        //Comment field status "ddsm_SummerDemandReductionKW" = deleted in E1 Schema from schema
        /*{
         header: 'ddsm_SummerDemandReductionKW',
         name: 'ddsm_SummerDemandReductionKW',
         type: 'decimal',
         defaultValue: 0,
         sortable: true,
         filterable: false,
         readOnly: false,
         allowBlank: true,
         width: 100,
         hidden: true
         },*/ {
            header: 'ddsm_WinterDemandReductionKW',
            name: 'ddsm_WinterDemandReductionKW',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Watts Base',
            name: 'ddsm_WattsBase',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Watts Eff',
            name: 'ddsm_WattsEff',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Motor HP',
            name: 'ddsm_MotorHP',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            groupHeader: "Measure Source",
            group: [{
                header: 'Converted from Potential',
                name: 'ddsm_MeasureLeadSource',
                type: 'lookup',
                entityNameLookup: 'ddsm_sitemeasure',
                defaultValue: '',
                sortable: true,
                filterable: false,
                readOnly: true,
                allowBlank: true,
                width: 150,
                hidden: false
            },{
                header: 'Project Template',
                name: 'ddsm_MeasureLeadSourceProjTpl',
                type: 'lookup',
                entityNameLookup: 'ddsm_projecttemplatemeasure',
                defaultValue: '',
                sortable: true,
                filterable: false,
                readOnly: true,
                allowBlank: true,
                width: 150,
                hidden: false
            }]
        }, {
            header: 'Measure Subgroup 2',
            name: 'ddsm_measuresubgroup2',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false
        }, {
            header: 'ddsm_IncRateUnit',
            name: 'ddsm_IncRateUnit',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_IncRateKW',
            name: 'ddsm_IncRateKW',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_IncRatekWh',
            name: 'ddsm_IncRatekWh',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_IncRatethm',
            name: 'ddsm_IncRatethm',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'Measure Life',
            name: 'ddsm_MeasureLife',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_AnnualFixedCosts',
            name: 'ddsm_AnnualFixedCosts',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'Project Status (Parent Project)',
            name: 'ddsm_ProjectStatus',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'Calculation Type',
            name: 'ddsm_CalculationType',
            type: 'lookup',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_CalculationTypeValues',
            name: 'ddsm_CalculationTypeValues',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Includedin Financial',
            name: 'ddsm_IncludedinFinancial',
            type: 'boolean',
            defaultValue: false,
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: false
        },{
            header: 'Disable Recalculation',
            name: 'ddsm_DisableRecalculation',
            type: 'boolean',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 50,
            hidden: true
        }
    ]
};
///// END CONFIG GRID

//// START RENDERER CURRENCY COLUMN
var rendererCurrency = function (value, meta) {
    return Ext.util.Format.usMoney(value);
};

function _getOptionSetDataArray(entityName, optionSetName, customStore){
    SDK.Metadata.RetrieveAttribute(entityName, optionSetName, "00000000-0000-0000-0000-000000000000", true,
        function (result) {
            var Data = [];
            for (var i = 0; i < result.OptionSet.Options.length; i++) {
                var dataSet = [];
                var text = result.OptionSet.Options[i].Label.LocalizedLabels[0].Label;
                var value = result.OptionSet.Options[i].Value;
                dataSet = [value.toString(), text.toString()];
                Data.push(dataSet);
            }
            if(customStore !== null) {
                eval(customStore).add(Data);
            } else {
                eval(optionSetName + "Combo").add(Data);
            }
        },
        function (error) { }
    );
}
///// START LOCAL COMBOBOX CONFIG

//------------------------------------------------------
var ddsm_MeasureTypeCombo = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_measure", "ddsm_MeasureType", null);
var ddsm_MeasureTypeColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    store: ddsm_MeasureTypeCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_MeasureTypeColumnRenderer = function (value) {
    var index = ddsm_MeasureTypeCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_MeasureTypeCombo.getAt(index).data;
        return rs.display;
    }
};
//------------------------------------------------------
var ddsm_MotorTypeCombo = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_measure", "ddsm_MotorType", null);
var ddsm_MotorTypeColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    id: "motorType",
    store: ddsm_MotorTypeCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
        /*        ,blur: function (editor, e) {
         rowEditing.completeEdit();
         }
         */    }
};
var ddsm_MotorTypeColumnRenderer = function (value) {
    var index = ddsm_MotorTypeCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_MotorTypeCombo.getAt(index).data;
        return rs.display;
    }
};
//------------------------------------------------------
var ddsm_EnergyTypeCombo = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_measure", "ddsm_EnergyType", null);
var ddsm_EnergyTypeColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    store: ddsm_EnergyTypeCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_EnergyTypeColumnRenderer = function (value) {
    var index = ddsm_EnergyTypeCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_EnergyTypeCombo.getAt(index).data;
        return rs.display;
    }
};
//------------------------------------------------------
var ddsm_BulbTypeCombo = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_measure", "ddsm_BulbType", null);
var ddsm_BulbTypeColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    store: ddsm_BulbTypeCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_BulbTypeColumnRenderer = function (value) {
    var index = ddsm_BulbTypeCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_BulbTypeCombo.getAt(index).data;
        return rs.display;
    }
};
//------------------------------------------------------
var ddsm_MeasureRelevanceCombo = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_measure", "ddsm_MeasureRelevance", null);
var ddsm_MeasureRelevanceColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    store: ddsm_MeasureRelevanceCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_MeasureRelevanceColumnRenderer = function (value) {
    var index = ddsm_MeasureRelevanceCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_MeasureRelevanceCombo.getAt(index).data;
        return rs.display;
    }
};
//------------------------------------------------------
var ddsm_MeasureModelingTypeCombo = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_measure", "ddsm_MeasureModelingType", null);
var ddsm_MeasureModelingTypeColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    store: ddsm_MeasureModelingTypeCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_MeasureModelingTypeColumnRenderer = function (value) {
    var index = ddsm_MeasureModelingTypeCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_MeasureModelingTypeCombo.getAt(index).data;
        return rs.display;
    }
};

////For New ESP Combo box
////------------------------------------------------------
//var ddsm_WaterHeaterTypeCombo = new Ext.data.SimpleStore({
//    fields: ["value", "display"],
//    data: []
//});
//_getOptionSetDataArray("ddsm_measure", "ddsm_WaterHeaterType", null);
//var ddsm_WaterHeaterTypeColumnEditor = {
//    queryMode: 'local',
//    xtype: 'combobox',
//    store: ddsm_WaterHeaterTypeCombo,
//    displayField: 'display',
//    valueField: 'value',
//    listeners: {
//        focus: function (editor, e) { lookupActive = ""; }
//    }
//};
//var ddsm_WaterHeaterTypeColumnRenderer = function (value) {
//    var index = ddsm_WaterHeaterTypeCombo.findExact('value', value);
//    if (index != -1) {
//        var rs = ddsm_WaterHeaterTypeCombo.getAt(index).data;
//        return rs.display;
//    }
//};
////------------------------------------------------------
////------------------------------------------------------
//var ddsm_BuildingTypeCombo = new Ext.data.SimpleStore({
//    fields: ["value", "display"],
//    data: []
//});
//_getOptionSetDataArray("ddsm_measure", "ddsm_BuildingType", null);
//var ddsm_BuildingTypeColumnEditor = {
//    queryMode: 'local',
//    xtype: 'combobox',
//    store: ddsm_BuildingTypeCombo,
//    displayField: 'display',
//    valueField: 'value',
//    listeners: {
//        focus: function (editor, e) { lookupActive = ""; }
//    }
//};
//var ddsm_BuildingTypeColumnRenderer = function (value) {
//    var index = ddsm_BuildingTypeCombo.findExact('value', value);
//    if (index != -1) {
//        var rs = ddsm_BuildingTypeCombo.getAt(index).data;
//        return rs.display;
//    }
//};
////------------------------------------------------------
////------------------------------------------------------
//var ddsm_climatezoneCombo = new Ext.data.SimpleStore({
//    fields: ["value", "display"],
//    data: []
//});
//_getOptionSetDataArray("ddsm_measure", "ddsm_climatezone", null);
//var ddsm_climatezoneColumnEditor = {
//    queryMode: 'local',
//    xtype: 'combobox',
//    store: ddsm_climatezoneCombo,
//    displayField: 'display',
//    valueField: 'value',
//    listeners: {
//        focus: function (editor, e) { lookupActive = ""; }
//    }
//};
//var ddsm_climatezoneColumnRenderer = function (value) {
//    var index = ddsm_climatezoneCombo.findExact('value', value);
//    if (index != -1) {
//        var rs = ddsm_climatezoneCombo.getAt(index).data;
//        return rs.display;
//    }
//};
//------------------------------------------------------
///// END LOCAL COMBOBOX CONFIG