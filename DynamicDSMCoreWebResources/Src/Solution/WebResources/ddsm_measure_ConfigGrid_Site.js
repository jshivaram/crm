/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 2.0.7
 * Last updated on 04/25/2016
 * Measures Grid
 *////// START CONFIG GRID

var configJson = {
    entitySchemaName: "ddsm_sitemeasure",
    dateFormat: "m/d/Y",
    sorters: { property: 'ddsm_name', direction: 'ASC' },
    modelGrid: "measureGrid",
    idGrid: "mtsGridId",
    border: true,
    height: 346,
    title: '',
    fields: [
        {
            header: 'Measure Name',
            name: 'ddsm_name',
            type: 'string',
            defaultValue: '',
            sortable: true,
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                if (value != null) {
                    return Ext.String.format(
                        '<a href="/main.aspx?etn={2}&extraqs=&histKey={3}&id={1}&newWindow=true&pagetype=entityrecord" target="_blank">{0}</a>',
                        value,
                        "{" + record.data["ddsm_sitemeasureId"] + "}",
                        "ddsm_sitemeasure",
                        randomnumber);
                } else { return ''; }
            },
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 160,
            hidden: false
        }, {
            header: 'Standard Selector',
            name: 'ddsm_MeasureSelector',
            type: 'lookup',
            entityNameLookup: 'ddsm_measuretemplate',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 160,
            hidden: false
        }, {
            header: 'Measure Status',
            name: 'ddsm_MeasureStatus',
            type: 'combobox',
            defaultValue: null,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: false,
            editor: 'ddsm_MeasureStatusColumnEditor',
            renderer: 'ddsm_MeasureStatusColumnRenderer'
        }, {
            groupHeader: "&nbsp;",
            groupName: "phase1",
            group: [{
                header: 'Start Date',
                name: 'ddsm_StartDate',
                type: 'date',
                defaultValue: null,
                sortable: false,
                filterable: false,
                readOnly: true,
                allowBlank: true,
                width: 75,
                saved: true,
                hidden: false
            },{
                header: 'Expiration Date',
                name: 'ddsm_ExpirationDate',
                type: 'date',
                defaultValue: null,
                sortable: false,
                filterable: false,
                readOnly: true,
                allowBlank: true,
                width: 75,
                saved: true,
                hidden: false
            }]
        }, {
            groupHeader: "&nbsp;",
            groupName: "phase1",
            group: [{
                header: 'Units',
                name: 'ddsm_phase1units',
                type: 'decimal',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                width: 40,
                hidden: false
            }, {
                header: 'Savings kWh',
                name: 'ddsm_phase1savingskwh',
                type: 'decimal',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                width: 75,
                hidden: false
            }, {
                header: 'Savings Gas M3',
                name: 'ddsm_phase1savingskw',
                type: 'decimal',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                width: 100,
                hidden: false
            }, {
                header: 'Savings Water (Liters)',
                name: 'ddsm_phase1savingsthm',
                type: 'decimal',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                width: 100,
                hidden: false
            }, {
                header: 'Savings CCF',
                name: 'ddsm_Phase1SavingsCCF',
                type: 'decimal',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                width: 75,
                hidden: true
            }, {
                header: 'Incentive Total',
                name: 'ddsm_phase1incentivetotal',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 100,
                hidden: false
            }, {
                header: 'Per Unit kWh',
                name: 'ddsm_phase1perunitkwh',
                type: 'decimal',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                width: 75,
                hidden: false
            }, {
                header: 'Per Unit Gas M3',
                name: 'ddsm_phase1perunitkw',
                type: 'decimal',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                width: 100,
                hidden: false
            }, {
                header: 'Per Unit Water (Liters)',
                name: 'ddsm_phase1perunitthm',
                type: 'decimal',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                width: 100,
                hidden: false
            }, {
                header: 'Per Unit CCF',
                name: 'ddsm_Phase1PerUnitCCF',
                type: 'decimal',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                width: 75,
                hidden: true
            }, {
                header: 'Incentive Per Unit',
                name: 'ddsm_phase1incentiveunits',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 100,
                hidden: false
            }]
        }
/*        , {
            groupHeader: "Cost",
            groupName: "cost",
            group: [{
                header: 'Equipment',
                name: 'ddsm_CostEquipment',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 75,
                hidden: false
            }, {
                header: 'Labor',
                name: 'ddsm_CostLabor',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 75,
                hidden: false
            }, {
                header: 'Other',
                name: 'ddsm_CostOther',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 75,
                hidden: true
            }, {
                header: 'Total',
                name: 'ddsm_CostTotal',
                type: 'currency',
                defaultValue: 0,
                sortable: true,
                filterable: false,
                readOnly: false,
                allowBlank: true,
                renderer: 'rendererCurrency',
                width: 75,
                hidden: false
            }]
        }
        */
        , {
            header: 'Parent Company',
            name: 'ddsm_AccountId',
            type: 'lookup',
            entityNameLookup: 'account',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: false
        }, {
            header: 'Parent Site',
            name: 'ddsm_parentsite',
            type: 'lookup',
            entityNameLookup: 'ddsm_site',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: false
        }, {
            header: 'Lead',
            name: 'ddsm_InitialLead',
            type: 'lookup',
            entityNameLookup: 'lead',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'Parent Project',
            name: 'ddsm_ProjectToMeasureId',
            type: 'lookup',
            entityNameLookup: 'ddsm_project',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: false
        }, {
            header: 'Equipment Description',
            name: 'ddsm_EquipmentDescription',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'Eff Equip Descr',
            name: 'ddsm_EffEquipDescr',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'Measure Code',
            name: 'ddsm_MeasureCode',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Measure Number',
            name: 'ddsm_MeasureNumber',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Energy Type',
            name: 'ddsm_EnergyType',
            type: 'combobox',
            defaultValue: null,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true,
            editor: 'ddsm_EnergyTypeColumnEditor',
            renderer: 'ddsm_EnergyTypeColumnRenderer'
        }, {
            header: 'Measure Group',
            name: 'ddsm_MeasureGroup',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Measure Subgroup',
            name: 'ddsm_MeasureSubgroup',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_Size',
            name: 'ddsm_Size',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_EUL',
            name: 'ddsm_EUL',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_lifecyclesavingskw',
            name: 'ddsm_lifecyclesavingskw',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_lifecyclesavingskwh',
            name: 'ddsm_lifecyclesavingskwh',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_lifecyclesavingsthm',
            name: 'ddsm_lifecyclesavingsthm',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Base Equip Descr',
            name: 'ddsm_BaseEquipDescr',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'End Use',
            name: 'ddsm_EndUse',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_LMLightingorNon',
            name: 'ddsm_LMLightingorNon',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Measure Notes',
            name: 'ddsm_MeasureNotes',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_Source',
            name: 'ddsm_Source',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Unit',
            name: 'ddsm_Unit',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Study Measure',
            name: 'ddsm_StudyMeasure',
            type: 'boolean',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 50,
            hidden: true
        }, {
            header: 'Measure Type',
            name: 'ddsm_MeasureType',
            type: 'combobox',
            defaultValue: null,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true,
            editor: 'ddsm_MeasureTypeColumnEditor',
            renderer: 'ddsm_MeasureTypeColumnRenderer'
        }, {
            header: 'Annual Hours Before',
            name: 'ddsm_AnnualHoursBefore',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Annual Hours After',
            name: 'ddsm_AnnualHoursAfter',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_SummerDemandReductionKW',
            name: 'ddsm_SummerDemandReductionKW',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_WinterDemandReductionKW',
            name: 'ddsm_WinterDemandReductionKW',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Watts Base',
            name: 'ddsm_WattsBase',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Watts Eff',
            name: 'ddsm_WattsEff',
            type: 'number',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_clientpeakkwsavings',
            name: 'ddsm_clientpeakkwsavings',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Motor Type',
            name: 'ddsm_MotorType',
            type: 'combobox',
            defaultValue: null,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true,
            editor: 'ddsm_MotorTypeColumnEditor',
            renderer: 'ddsm_MotorTypeColumnRenderer'
        }, {
            header: 'Motor HP',
            name: 'ddsm_MotorHP',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Measure Subgroup 2',
            name: 'ddsm_measuresubgroup2',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_IncRateUnit',
            name: 'ddsm_IncRateUnit',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_IncRateKW',
            name: 'ddsm_IncRateKW',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_IncRatekWh',
            name: 'ddsm_IncRatekWh',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_IncRatethm',
            name: 'ddsm_IncRatethm',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_IncrementalCost',
            name: 'ddsm_IncrementalCost',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'Measure Life',
            name: 'ddsm_MeasureLife',
            type: 'decimal',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_AnnualFixedCosts',
            name: 'ddsm_AnnualFixedCosts',
            type: 'currency',
            defaultValue: 0,
            sortable: true,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_CalculationType',
            name: 'ddsm_CalculationType',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_CalculationTypeValues',
            name: 'ddsm_CalculationTypeValues',
            type: 'string',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Placeholder Measure',
            name: 'ddsm_PlaceholderMeasure',
            type: 'boolean',
            defaultValue: '',
            sortable: true,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: true
        }
    ]
};
///// END CONFIG GRID

//// START RENDERER CURRENCY COLUMN
var rendererCurrency = function (value, meta) {
    return Ext.util.Format.usMoney(value);
};

function _getOptionSetDataArray(entityName, optionSetName, customStore){
    SDK.Metadata.RetrieveAttribute(entityName, optionSetName, "00000000-0000-0000-0000-000000000000", true,
        function (result) {
            var Data = [];
            for (var i = 0; i < result.OptionSet.Options.length; i++) {
                var dataSet = [];
                var text = result.OptionSet.Options[i].Label.LocalizedLabels[0].Label;
                var value = result.OptionSet.Options[i].Value;
                dataSet = [value.toString(), text.toString()];
                Data.push(dataSet);
            }
            if(customStore !== null) {
                eval(customStore).add(Data);
            } else {
                eval(optionSetName + "Combo").add(Data);
            }
        },
        function (error) { }
    );
}
///// START LOCAL COMBOBOX CONFIG
//var ddsm_ProgramStore = [];

var ddsm_MeasureStatusCombo = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_sitemeasure", "ddsm_MeasureStatus", null);
var ddsm_MeasureStatusColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    store: ddsm_MeasureStatusCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_MeasureStatusColumnRenderer = function (value) {
    var index = ddsm_MeasureStatusCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_MeasureStatusCombo.getAt(index).data;
        return rs.display;
    }
};

//------------------------------------------------------
var ddsm_MeasureTypeCombo = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_sitemeasure", "ddsm_MeasureType", null);
var ddsm_MeasureTypeColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    store: ddsm_MeasureTypeCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_MeasureTypeColumnRenderer = function (value) {
    var index = ddsm_MeasureTypeCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_MeasureTypeCombo.getAt(index).data;
        return rs.display;
    }
};
//------------------------------------------------------
var ddsm_MotorTypeCombo = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_sitemeasure", "ddsm_MotorType", null);
var ddsm_MotorTypeColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    id: "motorType",
    store: ddsm_MotorTypeCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_MotorTypeColumnRenderer = function (value) {
    var index = ddsm_MotorTypeCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_MotorTypeCombo.getAt(index).data;
        return rs.display;
    }
};
//------------------------------------------------------
var ddsm_EnergyTypeCombo = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_sitemeasure", "ddsm_EnergyType", null);
var ddsm_EnergyTypeColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    store: ddsm_EnergyTypeCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_EnergyTypeColumnRenderer = function (value) {
    var index = ddsm_EnergyTypeCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_EnergyTypeCombo.getAt(index).data;
        return rs.display;
    }
};
///// END LOCAL COMBOBOX CONFIG
