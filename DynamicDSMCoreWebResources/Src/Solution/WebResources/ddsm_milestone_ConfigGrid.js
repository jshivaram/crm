/*
 * Created by Sergey Dergunov
 * E-mail: sergey.dergunov@accentgold.com
 * Version 2.1.2 DSM
 * Last updated on 03/31/2015.
 * Milestones Grid
 */
///// START CONFIG GRID
var configJson = {
    entitySchemaName: "ddsm_milestone",
    dateFormat: "m/d/Y",
    sorters: { property: 'ddsm_Index', direction: 'ASC' },
    modelGrid: "milestoneGrid",
    idGrid: "mtsGridId",
    border: true,
    height: 304,
    title: '',
    entityConcat: "",
    fieldConcat: "ddsm_MsTpltoMs",
    fields: [
        {
            header: '#',
            name: 'ddsm_Index',
            type: 'number',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 30,
            hidden: false
        }, {
            header: 'Category',
            name: 'ddsm_Category',
            type: 'combobox',
            defaultValue: null,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 60,
            hidden: true,
            saved: false,
            editor: 'ddsm_CategoryColumnEditor',
            renderer: 'ddsm_CategoryColumnRenderer'
        }, {
            header: 'Name',
            name: 'ddsm_name',
            type: 'string',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 170,
            hidden: false
        }, {
            header: 'Status',
            name: 'ddsm_Status',
            type: 'combobox',
            defaultValue: null,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: true,
            saved: true,
            editor: 'ddsm_StatusColumnEditor',
            renderer: 'ddsm_StatusColumnRenderer'
        }, {
            header: 'Active by Category',
            name: 'ddsm_ActiveByCategory',
            type: 'combobox',
            defaultValue: null,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: true,
            saved: true,
            editor: 'ddsm_StatusColumnEditor',
            renderer: 'ddsm_StatusColumnRenderer'
        }, {
            header: 'Project Status',
            name: 'ddsm_ProjectStatus',
            type: 'string',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            saved: false,
            hidden: false
        }, {
            header: 'Project Phase',
            name: 'ddsm_ProjectPhaseName',
            type: 'string',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'Target Start',
            name: 'ddsm_TargetStart',
            type: 'date',
            defaultValue: null,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 70,
            saved: true,
            hidden: false
        }, {
            header: 'Actual Start',
            name: 'ddsm_ActualStart',
            type: 'date',
            defaultValue: null,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 70,
            saved: true,
            hidden: false
        }, {
            header: 'Target End',
            name: 'ddsm_TargetEnd',
            type: 'date',
            defaultValue: null,
            renderer: function (value, meta, record) {
                var nowDate = new Date();
                var ddsm_TargetEnd = new Date(record.get('ddsm_TargetEnd'));
                var ddsm_CurrEscalOverdueLength = record.get('ddsm_CurrEscalOverdueLength');
                if(ddsm_CurrEscalOverdueLength === null || ddsm_CurrEscalOverdueLength == ""){ ddsm_CurrEscalOverdueLength = 0;}
                var ddsm_EstimatedEnd = (record.get('ddsm_EstimatedEnd') !== null) ? new Date(record.get('ddsm_EstimatedEnd')) : null;
                if ((record.get('ddsm_Status') === '962080000' || record.get('ddsm_Status') === '962080001') && ((ddsm_TargetEnd.getTime() + parseInt(ddsm_CurrEscalOverdueLength) * 86400000) < nowDate.getTime())) {
                    meta.tdCls = 'expiredMScell';
                }
                if (ddsm_EstimatedEnd !== null) {
                    if ((record.get('ddsm_Status') === '962080000' || record.get('ddsm_Status') === '962080001') && ((ddsm_EstimatedEnd.getTime() - parseInt(ddsm_CurrEscalOverdueLength) * 86400000) > nowDate.getTime())) {
                        meta.tdCls = 'expiredMScell';
                    }
                }
                return Ext.util.Format.date(value, 'm/d/Y');
            },
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 70,
            saved: true,
            hidden: false
        }, {
            header: 'Est End',
            name: 'ddsm_EstimatedEnd',
            type: 'date',
            defaultValue: null,
            sortable: false,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 70,
            saved: true,
            hidden: false
        }, {
            header: 'Actual End',
            name: 'ddsm_ActualEnd',
            type: 'date',
            defaultValue: null,
            sortable: false,
            filterable: false,
            readOnly: false,
            allowBlank: true,
            width: 70,
            saved: true,
            hidden: false
        }, {
            header: 'Waive',
            name: 'ddsm_SkipMilestone',
            type: 'checkcolumn',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 50,
            saved: true,
            hidden: false
        }, {
            header: 'Note',
            name: 'ddsm_MSSpecialNote',
            type: 'string',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 50,
            hidden: true
        }, {
            header: 'Duration',
            name: 'ddsm_Duration',
            type: 'number',
            defaultValue: 1,
            format: '0',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            saved: true,
            width: 50,
            hidden: true
        }, {
            header: 'Actual Duration',
            name: 'ddsm_ActualDuration',
            type: 'number',
            defaultValue: 1,
            format: '0',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            saved: true,
            width: 50,
            hidden: true
        }, {
            header: 'Responsible',
            name: 'ddsm_Responsible',
            type: 'string',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 50,
            hidden: true
        }, {
            header: 'Exclude SecRole',
            name: 'ddsm_SecurityExcludeGUID',
            type: 'string',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'Include Only SecRole',
            name: 'ddsm_SecurityIncludeOnlyGUID',
            type: 'string',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 150,
            hidden: true
        }, {
            header: 'ApprCust',
            name: 'ddsm_ApprovalThresholdCust',
            type: 'currency',
            defaultValue: 0,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'ApprStd',
            name: 'ddsm_ApprovalThresholdStd',
            type: 'currency',
            defaultValue: 0,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'ApprNc',
            name: 'ddsm_ApprovalThresholdNC',
            type: 'currency',
            defaultValue: 0,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'ApprRCx',
            name: 'ddsm_ApprovalThresholdRCx',
            type: 'currency',
            defaultValue: 0,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            renderer: 'rendererCurrency',
            width: 100,
            hidden: true
        }, {
            header: 'Initial Offered',
            name: 'ddsm_InitialOffered',
            type: 'boolean',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 50,
            hidden: true
        }, {
            header: 'Use Validation',
            name: 'ddsm_UseValidation',
            type: 'boolean',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 50,
            hidden: true
        }, {
            header: 'Requires Valid Measure',
            name: 'ddsm_RequiresValidMeasure',
            type: 'boolean',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 50,
            hidden: true
        }, {
            header: 'Requires Empty Measure',
            name: 'ddsm_RequiresEmptyMeasure',
            type: 'boolean',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 50,
            hidden: true
        }, {
            header: 'Insert MS Set',
            name: 'ddsm_insertset',
            type: 'boolean',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 50,
            hidden: true
        }, {
            header: 'Index Offset',
            name: 'ddsm_indexoffset',
            type: 'number',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 30,
            hidden: true
        }, {
            header: 'Modified On',
            name: 'ModifiedOn',
            type: 'date',
            defaultValue: null,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            saved: true,
            hidden: true
        }, {
            header: 'Modified By',
            name: 'ModifiedBy',
            type: 'lookup',
            entityNameLookup: 'Contact',
            defaultValue: null,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            saved: true,
            hidden: true
        }, {
            header: 'InsertedSetsID',
            name: 'ddsm_InsertedSetsID',
            type: 'string',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 50,
            hidden: true
        }, {
            header: 'Current Escalate Overdue',
            name: 'ddsm_CurrEscalOverdueLength',
            type: 'number',
            defaultValue: 0,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            hidden: true
        }, {
            header: 'ddsm_TradeAllyTemplateGUID',
            name: 'ddsm_TradeAllyTemplateGUID',
            type: 'string',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            saved: true,
            width: 200,
            hidden: true
        }, {
            header: 'ddsm_CustomerTemplateGUID',
            name: 'ddsm_CustomerTemplateGUID',
            type: 'string',
            defaultValue: '',
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            saved: true,
            width: 200,
            hidden: true
        }, {
            header: 'ddsm_MsTpltoMs',
            name: 'ddsm_MsTpltoMs',
            type: 'lookup',
            entityNameLookup: 'ddsm_milestonetemplate',
            defaultValue: null,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 100,
            saved: true,
            hidden: true
        },{
            header: 'Project Task Count',
            name: 'ddsm_ProjectTaskCount',
            type: 'number',
            defaultValue: 0,
            sortable: false,
            filterable: false,
            readOnly: true,
            allowBlank: true,
            width: 30,
            hidden: false
        }
    ]
};
///// END CONFIG GRID

var testSatusData = [
    [ "962080000", "Not Started" ],
    [ "962080001", "Active" ],
    [ "962080002", "Completed" ],
    [ "962080003", "Skipped" ]
];
//// START RENDERER CURRENCY COLUMN
var rendererCurrency = function (value, meta) {
    return Ext.util.Format.usMoney(value);
};
//// END RENDERER CURRENCY COLUMN



///// START LOCAL COMBOBOX CONFIG
function _getOptionSetDataArray(entityName, optionSetName, customStore){
    SDK.Metadata.RetrieveAttribute(entityName, optionSetName, "00000000-0000-0000-0000-000000000000", true,
        function (result) {
            var Data = [];
            for (var i = 0; i < result.OptionSet.Options.length; i++) {
                var dataSet = [];
                var text = result.OptionSet.Options[i].Label.LocalizedLabels[0].Label;
                var value = result.OptionSet.Options[i].Value;
                dataSet = [value.toString(), text.toString()];
                Data.push(dataSet);
            }
            if(customStore !== null) {
                eval(customStore).add(Data);
            } else {
                eval(optionSetName + "Combo").add(Data);
            }
        },
        function (error) { }
    );
}
//------------------------------------------------------
var ddsm_StatusCombo =  new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_milestone", "ddsm_Status", null);

var ddsm_StatusColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    store: ddsm_StatusCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_StatusColumnRenderer = function (value) {
    var index = ddsm_StatusCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_StatusCombo.getAt(index).data;
        return rs.display;
    }
};
//------------------------------------------------------
var ddsm_CategoryComboFilter = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
ddsm_CategoryComboFilter.add({value:"000000000", display:"All"});
_getOptionSetDataArray("ddsm_milestone", "ddsm_Category", "ddsm_CategoryComboFilter");
//------------------------------------------------------
var ddsm_CategoryCombo = new Ext.data.SimpleStore({
    fields: ["value", "display"],
    data: []
});
_getOptionSetDataArray("ddsm_milestone", "ddsm_Category", null);
var ddsm_CategoryColumnEditor = {
    queryMode: 'local',
    xtype: 'combobox',
    store: ddsm_CategoryCombo,
    displayField: 'display',
    valueField: 'value',
    listeners: {
        focus: function (editor, e) { lookupActive = ""; }
    }
};
var ddsm_CategoryColumnRenderer = function (value) {
    var index = ddsm_CategoryCombo.findExact('value', value);
    if (index != -1) {
        var rs = ddsm_CategoryCombo.getAt(index).data;
        return rs.display;
    }
};
///// END LOCAL COMBOBOX CONFIG
