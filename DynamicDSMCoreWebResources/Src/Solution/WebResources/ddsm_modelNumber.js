//Update 02/14/2017

//region Global variables
function crmWindow() {
    return Xrm.Internal.isTurboForm() ? partent.window : window;
}
HandlerIsAdded = false;
var modelNumberFilter = "";
//endregion

//Model Number Form
function onLoadModelNumberQuickCreateForm(){
    approvedByUser("ddsm_approvedby", false);
}
function onLoadModelNumberForm(){

    if(!!Xrm.Page.getAttribute("ddsm_approvedby")) {
        Xrm.Page.getAttribute("ddsm_approvedby").setSubmitMode("always");
    }
    approvedByUser("ddsm_approvedby", false);
    updateSKUView();
    updateCertLibVieew();
    onLoadMNDialog();
}
function onChangeApproved(){
    approvedByUser("ddsm_approvedby", true);
}

function updateSKUView() {
    setTimeout(function(){
        if(Xrm.Page.ui.getFormType() != 1) {

            if(($('#SKU_view_contextualButtonsContainer .ms-crm-contextButton')).length > 0)
            {
                $('#SKU_view_contextualButtonsContainer .ms-crm-contextButton').remove();
                var span = document.createElement('span');
                span.innerHTML = ' <div class="ms-crm-contextButton"><a href="#" id="SKU_addImageButton" onclick="return false;" style="display: block; cursor: pointer;" class="ms-crm-ImageStrip-addButton" title="Add SKU record." tabindex="1240"><img src="/_imgs/imagestrips/transparent_spacer.gif?ver=-730783382" id="btnSKUAddItem" alt="Add SKU record." title="Add SKU record." class="ms-crm-add-button-icon"></a></div>';
                document.getElementById('SKU_view_contextualButtonsContainer').appendChild(span);

                document.getElementById("btnSKUAddItem").onclick = function () {


                    //Set Call Quick Create Form
                    /*
                     Xrm.Utility.openQuickCreate("ddsm_sku",
                     {
                     entityType: "ddsm_modelnumber",
                     id: Xrm.Page.data.entity.getId()
                     }, {
                     }).then(function (lookup) {
                     console.log("Created new SKU: " + lookup.savedEntityReference.name + " and id: " + lookup.savedEntityReference.id);
                     setTimeout(function () {
                     Xrm.Page.ui.controls.get("SKU_view").refresh();
                     }, 250);
                     }, function (e) {
                     console.log("Error: " + e.errorCode + " " + e.message);
                     });
                     */

                };
            }

        }
    },500);
}

function updateCertLibVieew() {
    setTimeout(function(){
        if(Xrm.Page.ui.getFormType() != 1) {
            if(($('#CertLib_contextualButtonsContainer .ms-crm-contextButton')).length > 0)
            {
                $('#CertLib_contextualButtonsContainer .ms-crm-contextButton').remove();
                var span = document.createElement('span');
                span.innerHTML = ' <div class="ms-crm-contextButton"><a href="#" id="CertLib_addImageButton" onclick="return false;" style="display: block; cursor: pointer;" class="ms-crm-ImageStrip-addButton" title="Add Certification Library record." tabindex="1240"><img src="/_imgs/imagestrips/transparent_spacer.gif?ver=-730783382" id="btnCertLibAddItem" alt="Add Certification Library record." title="Add Certification Library record." class="ms-crm-add-button-icon"></a></div>';
                document.getElementById('CertLib_contextualButtonsContainer').appendChild(span);

                document.getElementById("btnCertLibAddItem").onclick = function () {
                    var filters = [
                        //[
                        [
                            [
                                ['ddsm_model_number', 'eq', Xrm.Page.getAttribute("ddsm_name").getValue()]
                                ,['statecode', 'eq', 0]
                                ,['statuscode', 'eq', 1]
                            ]
                            , 'and'
                        ]
                        //]
                    ];
                    var additionalAttributes = ["ddsm_model_number"];

                    AGS.Form.SubGrid.LookUp.addExistingFromSubGrid("CertLib", filters, "multi", null, function(items){console.dir(items);});
                    /*
                     var conditions = [
                     ['ddsm_model_number', 'eq', 'MN-00001']
                     ,['statecode', 'eq', '0']
                     , 'and'
                     ];
                     var filterFXML = getFilterFXML(conditions);
                     */
                    //addExistingFromSubGrid("CertLib_view", "ddsm_certlib", "ddsm_model_number",  Xrm.Page.getAttribute("ddsm_name").getValue());
                    //addExistingFromSubGrid("CertLib_view", "ddsm_certlib", null, null, "ddsm_model_number");
                };
            }
        }
    },500);
}

function MeasureTemplateViewAddOnLoad() {

}

function SKUViewAddOnLoad() {

}

//
//Model Number Approval Form
function onLoadModelNumberApprovalQuickCreateForm(){
    approvedByUser("ddsm_approvedby", false);
}
function onLoadModelNumberApprovalForm(){
    approvedByUser("ddsm_approvedby", false);

}

function onChangeModelNumber(){
    if(!!Xrm.Page.getAttribute("ddsm_modelnumberid").getValue())
        getDataModelNumber(Xrm.Page.getAttribute("ddsm_modelnumberid").getValue()[0].id)
    //Xrm.Page.getAttribute("ddsm_name").setValue(Xrm.Page.getAttribute("ddsm_modelnumberid").getValue()[0].name);
    else {
        Xrm.Page.getAttribute("ddsm_name").setValue(null);
        Xrm.Page.getAttribute("ddsm_approvalstatus").setValue(null);
        Xrm.Page.getAttribute("ddsm_source").setValue(null);
        Xrm.Page.getAttribute("ddsm_type").setValue(null);
    }
    CreateModelNumberApprovalName();
}

function onChangeApproved1(){
    approvedByUser("ddsm_approvedby", true);
}

function onChangeMeasTpl () {
    if(!!Xrm.Page.getAttribute("ddsm_measuretemplateid").getValue())
    {
        Xrm.Page.getControl("ddsm_modelnumberid").removePreSearch(preSearchLookupHandler);
        setModelNumberFilterLookup(Xrm.Page.getAttribute("ddsm_measuretemplateid").getValue()[0].id);
    }
    CreateModelNumberApprovalName();
}

function setModelNumberFilterLookup(measTplId) {

    let FetchXML = '<fetch>'
        +'<entity name="ddsm_measuretemplate" >'
        +'<filter type="and" >'
        +'<condition attribute="ddsm_measuretemplateid" operator="eq" value="' + measTplId.replace(/\{|\}/g, '') + '" />'
        +'</filter>'
        +'<link-entity name="ddsm_modelnumberapproval" from="ddsm_measuretemplateid" to="ddsm_measuretemplateid" >'
        +'<attribute name="ddsm_modelnumberid" alias="ddsm_modelnumberid" />'
        +'</link-entity>'
        +'</entity>'
        +'</fetch>';

    let resultFetch = XrmServiceToolkit.Soap.Fetch(FetchXML);
    if(!!resultFetch && resultFetch.length > 0) {

        //console.dir(resultFetch);

        modelNumberFilter = '<filter type="and">';
        for(let i = 0; i < resultFetch.length; i++)
        {
            //console.dir(resultFetch[i]["attributes"]["ddsm_modelnumberid"]["id"]);
            modelNumberFilter += '<condition attribute="ddsm_modelnumberid" operator="neq" value="' + resultFetch[i]["attributes"]["ddsm_modelnumberid"]["id"] + '" />';
        }
        modelNumberFilter += '</filter>';
        //console.log(modelNumberFilter);
        Xrm.Page.getControl("ddsm_modelnumberid").addPreSearch(preSearchLookupHandler);
    }
}
function preSearchLookupHandler(){
    Xrm.Page.getControl("ddsm_modelnumberid").addCustomFilter(modelNumberFilter);
}


function CreateModelNumberApprovalName()
{
    let nameMNApproval = (!!Xrm.Page.getAttribute("ddsm_modelnumberid").getValue())?Xrm.Page.getAttribute("ddsm_modelnumberid").getValue()[0].name:"";
    let nameMT = (!!Xrm.Page.getAttribute("ddsm_measuretemplateid").getValue())?Xrm.Page.getAttribute("ddsm_measuretemplateid").getValue()[0].name:"";
    if(nameMNApproval != "" && nameMT != "") {
        nameMNApproval = nameMNApproval + "-" + nameMT;
    } else if(nameMNApproval == "" && nameMT != "") {
        nameMNApproval = nameMT;
    } else if(nameMNApproval == "" && nameMT == ""){
        nameMNApproval = null;
    }
    Xrm.Page.getAttribute("ddsm_name").setValue(nameMNApproval);
}

//
//SKU Approval Form
function onLoadSkuApprovalQuickCreateForm(){
    approvedByUser("ddsm_approvedby", false);
}
function onLoadSkuApprovalForm(){
    approvedByUser("ddsm_approvedby", false);
}
function onChangeApprovedSkuBy(){
    approvedByUser("ddsm_approvedby", true);
}
function onChangeApprovedSku(){
    if(!!Xrm.Page.getAttribute("ddsm_skuid").getValue())
        Xrm.Page.getAttribute("ddsm_name").setValue(Xrm.Page.getAttribute("ddsm_skuid").getValue()[0].name);
    else
    {
        Xrm.Page.getAttribute("ddsm_name").setValue(null);
        Xrm.Page.getAttribute("ddsm_approvalstatus").setValue(null);
    }
    CreateSKUApprovalName();
}
function onChangeSKUMeasTpl () {
    if(!!Xrm.Page.getAttribute("ddsm_measuretemplateid").getValue())
    {
        Xrm.Page.getControl("ddsm_skuid").removePreSearch(preSearchSkuLookupHandler);
        setSkuApprovalFilterLookup(Xrm.Page.getAttribute("ddsm_measuretemplateid").getValue()[0].id);
    }
    CreateSKUApprovalName();
}
function CreateSKUApprovalName()
{
    let nameSKUApproval = (!!Xrm.Page.getAttribute("ddsm_skuid").getValue())?Xrm.Page.getAttribute("ddsm_skuid").getValue()[0].name:"";
    let nameMT = (!!Xrm.Page.getAttribute("ddsm_measuretemplateid").getValue())?Xrm.Page.getAttribute("ddsm_measuretemplateid").getValue()[0].name:"";
    if(nameSKUApproval != "" && nameMT != "") {
        nameSKUApproval = nameSKUApproval + "-" + nameMT;
    } else if(nameSKUApproval == "" && nameMT != "") {
        nameSKUApproval = nameMT;
    } else if(nameSKUApproval == "" && nameMT == ""){
        nameSKUApproval = null;
    }
    Xrm.Page.getAttribute("ddsm_name").setValue(nameSKUApproval);
}

function setSkuApprovalFilterLookup(measTplId) {
    console.log(measTplId);
    let skuArray = [];
    //let skuApprovedArray = [];

    let FetchXML = '<fetch>'
        +'<entity name="ddsm_measuretemplate" >'
        +'<filter type="and" >'
        +'<condition attribute="ddsm_measuretemplateid" operator="eq" value="' + measTplId.replace(/\{|\}/g, '') + '" />'
        +'</filter>'
        +'<link-entity name="ddsm_modelnumberapproval" from="ddsm_measuretemplateid" to="ddsm_measuretemplateid" >'
        +'<link-entity name="ddsm_sku" from="ddsm_modelnumber" to="ddsm_modelnumberid" >'
        +'<attribute name="ddsm_skuid" alias="ddsm_skuid" />'
        +'</link-entity>'
        +'</link-entity>'
        +'</entity>'
        +'</fetch>';

    let FetchXMLApproved = '<fetch>'
        +'<entity name="ddsm_measuretemplate" >'
        +'<filter type="and" >'
        +'<condition attribute="ddsm_measuretemplateid" operator="eq" value="' + measTplId.replace(/\{|\}/g, '') + '" />'
        +'</filter>'
        +'<link-entity name="ddsm_skuapproval" from="ddsm_measuretemplateid" to="ddsm_measuretemplateid" >'
        +'<attribute name="ddsm_skuid" alias="ddsm_skuid" />'
        +'</link-entity>'
        +'</entity>'
        +'</fetch>';

    let resultFetch = XrmServiceToolkit.Soap.Fetch(FetchXML);
    let resultFetchApproved = XrmServiceToolkit.Soap.Fetch(FetchXMLApproved);

    console.dir(resultFetch);
    console.dir(resultFetchApproved);

    if(!!resultFetch && resultFetch.length > 0) {
        for(let i = 0; i < resultFetch.length; i++)
        {
            //console.log("sku: " + resultFetch[i]["attributes"]["ddsm_skuid"]["value"]);
            //console.log("skuapproval: " + resultFetch[i]["attributes"]["ddsm_skuapprovalid"]["id"]);
            skuArray.push(resultFetch[i]["attributes"]["ddsm_skuid"]["value"]);
        }

        if(!!resultFetchApproved && resultFetchApproved.length > 0) {
            for(let i = 0; i < resultFetchApproved.length; i++)
            {
                let idx = skuArray.indexOf(resultFetchApproved[i]["attributes"]["ddsm_skuid"]["id"]);
                if (idx >= 0) {
                    skuArray.splice( idx, 1 );
                }
            }
        }


        console.dir(skuArray);
        modelNumberFilter = '<filter type="or">';
        for(let i = 0; i < skuArray.length; i++) {
            modelNumberFilter += '<condition attribute="ddsm_skuid" operator="eq" value="' + skuArray[i] + '" />';
        }
        modelNumberFilter += '</filter>';
        //console.log(modelNumberFilter);
        Xrm.Page.getControl("ddsm_skuid").addPreSearch(preSearchSkuLookupHandler);
        Xrm.Page.ui.controls.get("ddsm_skuid").setDisabled(false);
    }
    else {
        Xrm.Page.getAttribute("ddsm_skuid").setValue(null);
        //Xrm.Page.ui.controls.get("ddsm_skuid").setDisabled(true);
        alert("Error: For the selected Measure Template not found records 'Model Number Approval'");
    }

}
function preSearchSkuLookupHandler(){
    Xrm.Page.getControl("ddsm_skuid").addCustomFilter(modelNumberFilter);
}


// Additionaly functions
function approvedByUser(fieldName, notNewRecord) {
    var setUservalue = new Array();
    setUservalue[0] = new Object();
    setUservalue[0].id = Xrm.Page.context.getUserId();
    setUservalue[0].entityType = 'systemuser';
    setUservalue[0].name = Xrm.Page.context.getUserName();

    if(Xrm.Page.ui.getFormType() == 1 && !!Xrm.Page.getAttribute(fieldName)) {
        Xrm.Page.ui.controls.get(fieldName).setDisabled(false);
        Xrm.Page.getAttribute(fieldName).setValue(setUservalue);
        Xrm.Page.ui.controls.get(fieldName).setDisabled(true);
    }

    if(notNewRecord && Xrm.Page.ui.getFormType() != 1 && !!Xrm.Page.getAttribute(fieldName)) {
        Xrm.Page.ui.controls.get(fieldName).setDisabled(false);
        Xrm.Page.getAttribute(fieldName).setValue(setUservalue);
        Xrm.Page.ui.controls.get(fieldName).setDisabled(true);
    }
}

function getDataModelNumber(modelNumberId) {

    let customMap = AGS.Entity.getTargetMapping(962080008, 'ddsm_modelnumberapproval', ['ddsm_modelnumber']);
    console.dir(customMap);
    if (customMap.hasOwnProperty('ddsm_modelnumber') && !!Object.keys(customMap['ddsm_modelnumber'])) {
        AGS.REST.retrieveRecord(modelNumberId, "ddsm_modelnumber", Object.keys(customMap['ddsm_modelnumber']).join(','), null, function (data) {
            //console.dir(data);
            let modelMap = customMap['ddsm_modelnumber'];
            for (let k in modelMap) {
                if (!!data[k]) {
                    let nk = k;
                    if(nk == "ddsm_ModelNumberApproved") nk = "ddsm_ApprovalStatus";

                    if(!!Xrm.Page.getAttribute(nk.toLowerCase())){
                        if(typeof data[k] == "object") {
                            if(!!data[k].Value) {
                                Xrm.Page.getAttribute(nk.toLowerCase()).setValue(data[k].Value);
                            }
                            else if(!!data[k].Id) {
                                let lookupData = new Array();
                                lookupData[0] = new Object();
                                lookupData[0].id = data[k].Id;
                                lookupData[0].entityType = data[k].LogicalName;
                                lookupData[0].name = data[k].Name;
                                Xrm.Page.getAttribute(nk.toLowerCase()).setValue(lookupData);
                            }
                        }
                        else {
                            Xrm.Page.getAttribute(nk.toLowerCase()).setValue(data[k]);
                        }
                    }
                }
            }
            //if(!!Xrm.Page.getAttribute("ddsm_approvalstatus").getValue())
            Xrm.Page.ui.controls.get("ddsm_approvalstatus").setDisabled(false);

        }, null, false);
    };

}

// N:N Subgrid LookUp
function addExistingFromSubGridCustom(params) {

    var relName = params.gridControl.GetParameter("relName"),
        roleOrd = params.gridControl.GetParameter("roleOrd"),
        viewId = "{00000000-0000-0000-0000-000000000001}";

    var customView = {
        fetchXml: params.fetchXml,
        id: viewId,
        layoutXml: params.layoutXml,
        name: "Filtered Lookup View",
        recordType: params.gridTypeCode,
        Type: 0
    };

    var parentObj = params.crmWindow.GetParentObject(null, 0);
    var parameters = [params.gridTypeCode, "", relName, roleOrd, parentObj];

//    var callbackRef = params.crmWindow.Mscrm.Utilities.createCallbackFunctionObject("locAssocObjAction", params.crmWindow, parameters, false);
    var callbackRef = {
        callback: function (lookupItems) {
            if (lookupItems && lookupItems.items.length > 0) {
                var parent = params.crmWindow.GetParentObject(),
                    parentId = parent.id,
                    parentTypeCode = parent.objectTypeCode;

                params.crmWindow.AssociateObjects(parentTypeCode, parentId, params.gridTypeCode, lookupItems, IsNull(roleOrd) || roleOrd == 2, "", relName);
            }
        }
    };
    params.crmWindow.LookupObjectsWithCallback(callbackRef, null, "multi", params.gridTypeCode, 0, null, "", null, null, null, null, null, null, viewId, [customView]);

}

function addExistingFromSubGrid(gridName, entityName, filteredAttrName, filteredAttrValue, secondAttrName) {

    if(!gridName) return;
    if(!entityName) return;

    var crmWindow = Xrm.Internal.isTurboForm() ? partent.window : window
        ,entityName = entityName.toLowerCase()
        ,filteredAttrName = (!!filteredAttrName) ? filteredAttrName.toLowerCase() : null
        ,secondAttrName = (!!secondAttrName) ? secondAttrName.toLowerCase() : null;

    var fetchXml = "<fetch version='1.0' " +
        "output-format='xml-platform' " +
        "mapping='logical'>" +
        "<entity name='" + entityName + "'>" +
        ((!!(entityName.indexOf("ddsm_") + 1))? "<attribute name='ddsm_name' />" : "<attribute name='name' />") +
        ((!!secondAttrName) ? "<attribute name='" +secondAttrName + "' />" : "") +

        ((!!(entityName.indexOf("ddsm_") + 1))? "<order attribute='ddsm_name' descending='false' />" : "<order attribute='name' descending='false' />") +

        "<filter type='and'>" +
        "<condition attribute='statecode' operator='eq' value='0' />" +
        ((!!filteredAttrName) ? "<condition attribute='" +filteredAttrName + "' operator='eq' value='" + filteredAttrValue + "' />" : "") +
        "</filter>" +

        "</entity>" +
        "</fetch>";

    var layoutXml = "<grid name='resultset'  object='1'  jump='name'  select='1' icon='1'  preview='1'>" +
        "<row name='result' id='" + (entityName + "id") + "'>" +
        ((!!(entityName.indexOf("ddsm_") + 1))? "<cell name='ddsm_name'  width='150' />" : "<cell name='name'  width='150' />") +
        ((!!filteredAttrName) ? "<cell name='" + filteredAttrName + "'  width='100' />" : "") +
        ((!!secondAttrName) ? "<cell name='" + secondAttrName + "'  width='100' />" : "") +
        "</row>" +
        "</grid>";

    addExistingFromSubGridCustom({
        gridTypeCode: crmWindow.Xrm.Internal.getEntityCode(entityName),
        gridControl: crmWindow.document.getElementById(gridName).control,
        crmWindow: crmWindow,
        fetchXml: fetchXml,

        layoutXml: layoutXml

    });
}

function onLoadMNDialog(){
    if (Xrm.Page.ui.getFormType() == 1)
    {
        //ShowModelNumberDialog();
    }

}

function ShowModelNumberDialog(){
    var formURI = '/WebResources/ddsm_ModelNumberDialog.html';
    var DialogOptions = new Xrm.DialogOptions();
    DialogOptions.width = 550;
    DialogOptions.height = 200;
    crmWindow().Xrm.Internal.openDialog(formURI, DialogOptions, null, null, returnDialogResponse);
    function returnDialogResponse(response) {
        if (response) {
            console.log(response);
            if (response.hasOwnProperty('reasonInput')) {
                var objMNApproval = {};
                objMNApproval.ddsm_CreateApproval = false;
                if (response.hasOwnProperty('reasonType')) {
                    if (response.reasonType == true) {
                        console.log("11111");
                        console.log(response.hasOwnProperty('reasonType'));
                        crmWindow().Xrm.Page.getAttribute("ddsm_createapproval").setValue(true);

                        ShowLookupMT();

                    }
                    else {crmWindow().Xrm.Page.getAttribute("ddsm_createapproval").setValue(false);}

                }
            }

        }
    }
}

function ShowLookupMT(){
    var fetchXmlMT = "<fetch version='1.0' " +
        "output-format='xml-platform' " +
        "mapping='logical'>" +
        "<entity name='ddsm_measuretemplate'>" +
        "<attribute name='ddsm_measuretemplateid' />" +
        "<attribute name='ddsm_measurenumber' />" +
        "<attribute name='ddsm_name' />" +
       /* "<filter type='and'>" +
        "<condition attribute='statecode' operator='eq' value='0' />" +
        ((!!filteredAttrName) ? "<condition attribute='" +filteredAttrName + "' operator='eq' value='" + filteredAttrValue + "' />" : "") +
        "</filter>" +*/

        "</entity>" +
        "</fetch>";

    var layoutXmlMT = "<grid name='resultset'  object='1'  jump='name'  select='1' icon='1'  preview='1'>" +
        "<row name='result' id='ddsm_measuretemplateid'>" +
        "<cell name='ddsm_name'  width='150' />" +
        "<cell name='ddsm_measurenumber'  width='150' />"+
        /*((!!filteredAttrName) ? "<cell name='" + filteredAttrName + "'  width='100' />" : "") +*/
        "</row>" +
        "</grid>";


    addExistingFromSubGridCustomMT({
        gridTypeCode: crmWindow().Xrm.Internal.getEntityCode("ddsm_measuretemplate"),
        //gridControl: crmWindow.document.getElementById(gridName).control,
        crmWindow: crmWindow(),
        fetchXml: fetchXmlMT,

        layoutXml: layoutXmlMT

    });


}

function addExistingFromSubGridCustomMT(params) {

    var viewId = "{00000000-0000-0000-0000-000000000001}";

    var customView = {
        fetchXml: params.fetchXml,
        id: viewId,
        layoutXml: params.layoutXml,
        name: "Filtered Lookup View",
        recordType: params.gridTypeCode,
        Type: 0
    };

    var parentObj = params.crmWindow.GetParentObject(null, 0);
    var parameters = [params.gridTypeCode, "", relName, roleOrd, parentObj];

//    var callbackRef = params.crmWindow.Mscrm.Utilities.createCallbackFunctionObject("locAssocObjAction", params.crmWindow, parameters, false);
    var callbackRef = {
        callback: function (lookupItems) {
            if (lookupItems && lookupItems.items.length > 0) {
                var parent = lookupItems.items;

                console.dir(parent[0]);

                var MeasureNumber = JSON.parse(parent[0].values)[1].value;
                crmWindow().Xrm.Page.getAttribute("ddsm_measurenumber").setValue(MeasureNumber);
                debugger;

            }
        }
    };
    params.crmWindow.LookupObjectsWithCallback(callbackRef, null, "single", params.gridTypeCode, 0, null, "", null, null, null, null, null, null, viewId, [customView]);

}
///////////////////////////




