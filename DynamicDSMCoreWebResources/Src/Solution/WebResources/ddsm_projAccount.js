//create new project to the account entity
// create 04/28/2015 Sergey
// update 05/15/2015
// 12/20/2015  : cleared off the code usage of removed fields of ddsm_project entity
// 12/25/2015  : cleared off the code usage of removed fields of ddsm_project entity

var IndexOffsetDocList = [];
var ddsm_name_NewProject = "";
var ddsm_StartDate_NewProject = new Date();
var objMeasSiteGlobal = null, objMeasSiteGlobal_length = 0, objMeasSiteGlobal_length2 = 0, objMeasSiteGlobal_count = 0;
var accountProject = {}, siteProject = {};

function getInitialLeadSite(ID) {
    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_siteSet?"
    + "$select=ddsm_InitialLead"
    + "&$filter=ddsm_siteId eq guid'" + ID + "'", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();
    if (req.readyState == 4 && req.status == 200) {
        var _tplData = JSON.parse(req.responseText).d;
        //console.dir(_tplData.results[0].ddsm_InitialLead);
        return _tplData.results[0].ddsm_InitialLead;
    } else return null;
}

function createDocList_ptojTpl(proj_ID, proj_Name, projTpl_ID){
    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
    + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
    + "&$filter=ddsm_ProjectTemplateId/Id eq guid'" + projTpl_ID + "'", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();
    if (req.readyState == 4 && req.status == 200) {
        var _tplData = JSON.parse(req.responseText).d;
        if (_tplData.results[0] != null) {
            for (var x = 0; x < _tplData.results.length; x++) {
                var proj_obj = {};
                if (_tplData.results[x].ddsm_RequiredByStatus != null) {
                    proj_obj["ddsm_RequiredByStatus"] = _tplData.results[x].ddsm_RequiredByStatus;
                }
                if (_tplData.results[x].ddsm_name != null) {
                    proj_obj["ddsm_name"] = _tplData.results[x].ddsm_name;
                }
                if (_tplData.results[x].ddsm_Uploaded != null) {
                    proj_obj["ddsm_Uploaded"] = _tplData.results[x].ddsm_Uploaded;
                }

                proj_obj.ddsm_ProjectId = new Object();
                proj_obj.ddsm_ProjectId.Id = proj_ID;
                proj_obj.ddsm_ProjectId.Name = proj_Name;
                proj_obj.ddsm_ProjectId.LogicalName = "ddsm_project";

                //console.dir(proj_obj);

                gen_H_createEntitySync(proj_obj, "ddsm_documentconventionSet");
            }
        }
    }
}
function createDocList_measTpl(proj_ID, proj_Name, measTpl_ID){

    var docProjList = null;
    var _req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    _req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
    + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
    + "&$filter=ddsm_ProjectId/Id eq guid'" + proj_ID + "'", false);
    _req.setRequestHeader("Accept", "application/json");
    _req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    _req.send();
    if (_req.readyState == 4 && _req.status == 200) {
        var _tplData = JSON.parse(_req.responseText).d;
        if (_tplData.results[0] != null) {
            docProjList = _tplData.results;
        }
    }

    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
    + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
    + "&$filter=ddsm_MeasureTemplateId/Id eq guid'" + measTpl_ID + "'", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();
    if (req.readyState == 4 && req.status == 200) {
        var _tplData = JSON.parse(req.responseText).d;
        if (_tplData.results[0] != null) {
            for (var x = 0; x < _tplData.results.length; x++) {
                var addDocConv = true;
                if(docProjList !== null){
                    for(var i =0; i < docProjList.length; i++){
                        if(_tplData.results[x].ddsm_name == docProjList[i].ddsm_name) {addDocConv = false;break;}
                    }
                }
                if(addDocConv){
                    var proj_obj = {};
                    if (_tplData.results[x].ddsm_RequiredByStatus != null) {
                        proj_obj["ddsm_RequiredByStatus"] = _tplData.results[x].ddsm_RequiredByStatus;
                    }
                    if (_tplData.results[x].ddsm_name != null) {
                        proj_obj["ddsm_name"] = _tplData.results[x].ddsm_name;
                    }
                    if (_tplData.results[x].ddsm_Uploaded != null) {
                        proj_obj["ddsm_Uploaded"] = _tplData.results[x].ddsm_Uploaded;
                    }

                    proj_obj.ddsm_ProjectId = new Object();
                    proj_obj.ddsm_ProjectId.Id = proj_ID;
                    proj_obj.ddsm_ProjectId.Name = proj_Name;
                    proj_obj.ddsm_ProjectId.LogicalName = "ddsm_project";

                    //console.dir(proj_obj);

                    gen_H_createEntitySync(proj_obj, "ddsm_documentconventionSet");
                }
            }
        }
    }
}

function createProjMeas_ProjTplMeas(projTpl_ID, proj_Obj, site_Obj, account_Obj){

    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    try {
        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projecttemplatemeasureSet?"
        + "$select="
        + "ddsm_projecttemplatemeasureId,"
        + "ddsm_name,"
        + "ddsm_MeasureSelector,"
        + "ddsm_phase1units,"
        + "ddsm_CostEquipment,"
        + "ddsm_CostLabor,"
        + "ddsm_CostOther,"
        + "ddsm_CostTotal,"
        + "ddsm_ProjectTemplate,"

        + "&$filter=ddsm_ProjectTemplate/Id eq guid'" + projTpl_ID + "'", false);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.send();
        if (req.readyState == 4 && req.status == 200) {
            var _tplData = JSON.parse(req.responseText).d;
            if (_tplData.results[0] != null) {
                for (var i = 0; i < _tplData.results.length; i++) {
                    createProjOneMeas(_tplData.results[i], proj_Obj, site_Obj, account_Obj);
                }
            }
        }
    } catch (err) {
        alert("Error generate Measure with Measure Template & Measure Calculation Template for New Project. " + err);
    }
}

function createProjOneMeas(objOneMeas, proj_Obj, site_Obj, account_Obj){

    createDocList_measTpl(proj_Obj.Id, proj_Obj.Name, objOneMeas.ddsm_MeasureSelector.Id);

    var req = new XMLHttpRequest();
    var now = new Date();
    var serverUrl = Xrm.Page.context.getClientUrl();
    var measname = (proj_Obj.Name).substr(0, 6) + "-" + objOneMeas.ddsm_MeasureSelector.Name + "_" + now.getFullYear() + now.getMonth() + now.getDate() + "-" + now.getHours() + now.getMinutes() + now.getSeconds();
    var objMeas = {};
    var InitialLead = getInitialLeadSite(siteProject.Id);

    objMeas.ddsm_name = measname;
    objMeas.ddsm_MeasureSelector = objOneMeas.ddsm_MeasureSelector;
    objMeas.ddsm_AccountId = account_Obj;
    objMeas.ddsm_parentsite = site_Obj;
    objMeas.ddsm_ProjectToMeasureId = proj_Obj;
    objMeas.ddsm_MeasureLeadSourceProjTpl = {};
    objMeas.ddsm_MeasureLeadSourceProjTpl.Id = objOneMeas.ddsm_projecttemplatemeasureId;
    objMeas.ddsm_MeasureLeadSourceProjTpl.Name = objOneMeas.ddsm_name;
    objMeas.ddsm_MeasureLeadSourceProjTpl.LogicalName = "ddsm_projecttemplatemeasure";
    if(InitialLead != null) {objMeas.ddsm_InitialLead = InitialLead;}


    try {
        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_measuretemplateSet?"
        + "$select="
        + "ddsm_IncRateUnit,"
        + "ddsm_IncRateKW,"
        + "ddsm_IncRatekWh,"
        + "ddsm_IncRatethm,"
        + "ddsm_SavingsperUnitKW,"
        + "ddsm_SavingsperUnitkWh,"
        + "ddsm_SavingsperUnitthm,"
        + "ddsm_Program,"
        + "ddsm_MeasureCode,"
        + "ddsm_MeasureNumber,"
        + "ddsm_EnergyType,"
        + "ddsm_Category,"
        + "ddsm_MeasureGroup,"
        + "ddsm_MeasureSubgroup,"
        + "ddsm_Size,"
        + "ddsm_EUL,"
        + "ddsm_BaseEquipDescr,"
        + "ddsm_EffEquipDescr,"
        + "ddsm_AnnualFixedCosts,"
        + "ddsm_ClientDescription,"
        + "ddsm_EndUse,"
        + "ddsm_EquipmentDescription,"
        + "ddsm_IncrementalCost,"
        + "ddsm_LMLightingorNon,"
        + "ddsm_MeasureLife,"
        + "ddsm_MeasureNotes,"
        + "ddsm_Source,"
        + "ddsm_Unit,"
        + "ddsm_StudyMeasure,"

        + "ddsm_MeasureType,"
        + "ddsm_AnnualHoursBefore,"
        + "ddsm_AnnualHoursAfter,"
        + "ddsm_SummerDemandReductionKW,"
        + "ddsm_WinterDemandReductionKW,"
        + "ddsm_WattsBase,"
        + "ddsm_WattsEff,"
        + "ddsm_ClientPeakKWSavings,"
        + "ddsm_MotorType,"
        + "ddsm_MotorHP,"
        + "ddsm_measuresubgroup2,"
        + "ddsm_SavingsperUnitCCF,"
        + "ddsm_ProgramCycle,"

        + "ddsm_BPAEffectiveEndDate,"
        + "ddsm_BPAEffectiveStartDate,"
        + "ddsm_BPAIncentiveperUnit,"
        + "ddsm_BPAkWhSavingsperUnitTarget,"
        + "ddsm_BPAReimbursableperUnitTarget,"
        + "ddsm_BPAMeasureNumber,"
        + "ddsm_BPAMeasureVersion,"
        + "ddsm_MaxLoanperUnit,"

        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_name,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateSavings,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateIncentive,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_PerUnitLock,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_SavingsLock,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentiveRatesLock,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentivesLock"//+ "&$orderby=ddsm_projTplToMilestoneTpl/ddsm_Index asc"
        + "&$expand=ddsm_ddsm_measurecalculationtemplate_ddsm_measur"
        + "&$filter=ddsm_measuretemplateId eq guid'" + objOneMeas.ddsm_MeasureSelector.Id + "' and statecode/Value eq 0", false);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.send();
        if (req.readyState == 4 && req.status == 200) {
            //From the project template - will also contain milestone information
            var _tplData = JSON.parse(req.responseText).d;
            if (_tplData.results[0] != null) {

                //console.dir(_tplData.results);

                var measCalcTpl_Data = _tplData.results[0].ddsm_ddsm_measurecalculationtemplate_ddsm_measur;
                var measCalcTpl_Name = measCalcTpl_Data.ddsm_name;
                var measCalcTpl_CalcSavings = measCalcTpl_Data.ddsm_CalculateSavings;
                var measCalcTpl_CalcIncentives = measCalcTpl_Data.ddsm_CalculateIncentive;
                var measCalcTpl_PerUnitLock = measCalcTpl_Data.ddsm_PerUnitLock;
                var measCalcTpl_SavingsLock = measCalcTpl_Data.ddsm_SavingsLock;
                var measCalcTpl_IncentiveRatesLock = measCalcTpl_Data.ddsm_IncentiveRatesLock;
                var measCalcTpl_IncentivesLock = measCalcTpl_Data.ddsm_IncentivesLock;
                var measCalcTpl_CalcTypeValues = "";

                objMeas.ddsm_Program = _tplData.results[0].ddsm_Program;
                objMeas.ddsm_MeasureCode = _tplData.results[0].ddsm_MeasureCode;
                objMeas.ddsm_MeasureNumber = _tplData.results[0].ddsm_MeasureNumber;
                objMeas.ddsm_EnergyType = _tplData.results[0].ddsm_EnergyType;
                objMeas.ddsm_Category = _tplData.results[0].ddsm_Category;
                objMeas.ddsm_MeasureGroup = _tplData.results[0].ddsm_MeasureGroup;
                objMeas.ddsm_MeasureSubgroup = _tplData.results[0].ddsm_MeasureSubgroup;
                objMeas.ddsm_Size = _tplData.results[0].ddsm_Size;
                objMeas.ddsm_EUL = _tplData.results[0].ddsm_EUL;
                objMeas.ddsm_BaseEquipDescr = _tplData.results[0].ddsm_BaseEquipDescr;
                objMeas.ddsm_EffEquipDescr = _tplData.results[0].ddsm_EffEquipDescr;
                objMeas.ddsm_ClientDescription = _tplData.results[0].ddsm_ClientDescription;
                objMeas.ddsm_EndUse = _tplData.results[0].ddsm_EndUse;
                objMeas.ddsm_EquipmentDescription = _tplData.results[0].ddsm_EquipmentDescription;
                objMeas.ddsm_LMLightingorNon = _tplData.results[0].ddsm_LMLightingorNon;
                objMeas.ddsm_MeasureNotes = _tplData.results[0].ddsm_MeasureNotes;
                objMeas.ddsm_Source = _tplData.results[0].ddsm_Source;
                objMeas.ddsm_Unit = _tplData.results[0].ddsm_Unit;
                objMeas.ddsm_StudyMeasure = _tplData.results[0].ddsm_StudyMeasure;
                objMeas.ddsm_MeasureType = _tplData.results[0].ddsm_MeasureType;
                objMeas.ddsm_AnnualHoursBefore = _tplData.results[0].ddsm_AnnualHoursBefore;
                objMeas.ddsm_AnnualHoursAfter = _tplData.results[0].ddsm_AnnualHoursAfter;
                objMeas.ddsm_SummerDemandReductionKW = _tplData.results[0].ddsm_SummerDemandReductionKW;
                objMeas.ddsm_WinterDemandReductionKW = _tplData.results[0].ddsm_WinterDemandReductionKW;
                objMeas.ddsm_WattsBase = _tplData.results[0].ddsm_WattsBase;
                objMeas.ddsm_WattsEff = _tplData.results[0].ddsm_WattsEff;
                objMeas.ddsm_clientpeakkwsavings = _tplData.results[0].ddsm_ClientPeakKWSavings;
                objMeas.ddsm_MotorType = _tplData.results[0].ddsm_MotorType;
                objMeas.ddsm_MotorHP = _tplData.results[0].ddsm_MotorHP;
                objMeas.ddsm_measuresubgroup2 = _tplData.results[0].ddsm_measuresubgroup2;
                objMeas.ddsm_IncRateUnit = _tplData.results[0].ddsm_IncRateUnit;
                objMeas.ddsm_IncRateKW = _tplData.results[0].ddsm_IncRateKW;
                objMeas.ddsm_IncRatekWh = _tplData.results[0].ddsm_IncRatekWh;
                objMeas.ddsm_IncRatethm = _tplData.results[0].ddsm_IncRatethm;
                objMeas.ddsm_IncrementalCost = _tplData.results[0].ddsm_IncrementalCost;
                objMeas.ddsm_MeasureLife = _tplData.results[0].ddsm_MeasureLife;
                objMeas.ddsm_AnnualFixedCosts = _tplData.results[0].ddsm_AnnualFixedCosts;
                objMeas.ddsm_ProgramCycleId = _tplData.results[0].ddsm_ProgramCycle;
                //BPA
                objMeas.ddsm_BPAEffectiveEndDate = _tplData.results[0].ddsm_BPAEffectiveEndDate;
                objMeas.ddsm_BPAEffectiveStartDate = _tplData.results[0].ddsm_BPAEffectiveStartDate;
                objMeas.ddsm_BPAIncentivePerUnitTarget = _tplData.results[0].ddsm_BPAIncentiveperUnit;
                objMeas.ddsm_BPAkWhSavingsperUnitTarget = _tplData.results[0].ddsm_BPAkWhSavingsperUnitTarget;
                objMeas.ddsm_BPAReimbursableperUnitTarget = _tplData.results[0].ddsm_BPAReimbursableperUnitTarget;
                objMeas.ddsm_BPAMeasureNumber = _tplData.results[0].ddsm_BPAMeasureNumber;
                objMeas.ddsm_BPAMeasureVersion = _tplData.results[0].ddsm_BPAMeasureVersion;
                objMeas.ddsm_MaxLoanperUnit = _tplData.results[0].ddsm_MaxLoanperUnit;


                //Measure Calc Template Data
                if (measCalcTpl_CalcSavings == true) {
                    measCalcTpl_CalcTypeValues += "Calculate Savings; ";
                }
                if (measCalcTpl_CalcIncentives == true) {
                    measCalcTpl_CalcTypeValues += "Calculate Incentives; ";
                }
                if (measCalcTpl_PerUnitLock == true) {
                    measCalcTpl_CalcTypeValues += "Lock Per Units; ";
                }
                if (measCalcTpl_SavingsLock == true) {
                    measCalcTpl_CalcTypeValues += "Lock Savings; ";
                }
                if (measCalcTpl_IncentiveRatesLock == true) {
                    measCalcTpl_CalcTypeValues += "Lock Incentive Rates; ";
                }
                if (measCalcTpl_IncentivesLock == true) {
                    measCalcTpl_CalcTypeValues += "Lock Incentives; ";
                }

                if (measCalcTpl_Name != null) {
                    objMeas.ddsm_CalculationType = measCalcTpl_Name;
                }
                if (measCalcTpl_CalcTypeValues != null) {
                    objMeas.ddsm_CalculationTypeValues = measCalcTpl_CalcTypeValues;
                }

                //copies savings
                //Copy to Phase 1
                objMeas.ddsm_CurrentUnits = objOneMeas.ddsm_phase1units;

                objMeas.ddsm_CurrentSavingskWh = objOneMeas.ddsm_phase1savingskwh;
                objMeas.ddsm_currentsavingskw = objOneMeas.ddsm_phase1savingskw;
                objMeas.ddsm_CurrentSavingsthm = objOneMeas.ddsm_phase1savingsthm;
                objMeas.ddsm_CurrentIncTotal = objOneMeas.ddsm_phase1incentivetotal;

                objMeas.ddsm_CurrentSavingsCCF = objOneMeas.ddsm_Phase1SavingsCCF;

                objMeas.ddsm_phase1units = objOneMeas.ddsm_phase1units;
                objMeas.ddsm_phase1savingskwh = objOneMeas.ddsm_phase1savingskwh;
                objMeas.ddsm_phase1savingskw = objOneMeas.ddsm_phase1savingskw;
                objMeas.ddsm_phase1savingsthm = objOneMeas.ddsm_phase1savingsthm;
                objMeas.ddsm_Phase1SavingsCCF = objOneMeas.ddsm_Phase1SavingsCCF;
                objMeas.ddsm_phase1perunitkwh = objOneMeas.ddsm_phase1perunitkwh;
                objMeas.ddsm_phase1perunitkw = objOneMeas.ddsm_phase1perunitkw;
                objMeas.ddsm_phase1perunitthm = objOneMeas.ddsm_phase1perunitthm;
                objMeas.ddsm_phase1incentivetotal = objOneMeas.ddsm_phase1incentivetotal;
                objMeas.ddsm_phase1incentiveunits = objOneMeas.ddsm_phase1incentiveunits;
                objMeas.ddsm_CostEquipment = objOneMeas.ddsm_CostEquipment;
                objMeas.ddsm_CostLabor = objOneMeas.ddsm_CostLabor;
                objMeas.ddsm_CostOther = objOneMeas.ddsm_CostOther;
                objMeas.ddsm_CostTotal = objOneMeas.ddsm_CostTotal;

                gen_H_createEntitySync(objMeas, "ddsm_measureSet");

            }
        }
    } catch (err) {
        alert("Error generate Measure with Measure Template & Measure Calculation Template for New Project. " + err);
    }
}

function getMSSets(projTpl_ID){
    var serverUrl = Xrm.Page.context.getClientUrl();
    var objNewMSSet = [];
    IndexOffsetDocList = [];
    var reqMSSet = new XMLHttpRequest();
    reqMSSet.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_milestonesetSet?"
    + "$select="
    + "ddsm_milestonesetId,"
    + "ddsm_name,"
    + "ddsm_insertbeforeindex,"
    + "ddsm_numberofsets,"
    + "ddsm_Probability"
    + "&$orderby=ddsm_insertbeforeindex asc"
    + "&$filter=ddsm_ProjectTemplate/Id eq guid'" + projTpl_ID + "' and ddsm_Probability gt 0 and statecode/Value eq 0", false);
    reqMSSet.setRequestHeader("Accept", "application/json");
    reqMSSet.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    reqMSSet.send();

    if (reqMSSet.readyState == 4 && reqMSSet.status == 200) {
        try {
            var obj = JSON.parse(reqMSSet.responseText).d;
            var objMSSet = obj.results;

            if (objMSSet != null) {
                //console.dir(objMSSet);
                for (var i = 0; i < objMSSet.length; i++) {
                    var obj ={}, obj1 = {};
                    if (objMSSet[i].ddsm_Probability != 0) {
                        if(objMSSet[i].ddsm_Probability != 1){
                            var randomNumb = Math.random();
                            //console.log(randomNumb);
                            if (randomNumb < objMSSet[i].ddsm_Probability) {
                                if(objMSSet[i].ddsm_numberofsets != 0) {
                                    obj1.ddsm_insertbeforeindex = objMSSet[i].ddsm_insertbeforeindex;
                                    obj1.ddsm_numberofsets = objMSSet[i].ddsm_numberofsets;
                                    obj1.setId = objMSSet[i].ddsm_milestonesetId;
                                    IndexOffsetDocList.push(obj1);

                                    obj.ddsm_insertbeforeindex = objMSSet[i].ddsm_insertbeforeindex;
                                    obj.ddsm_numberofsets = objMSSet[i].ddsm_numberofsets;
                                    obj._ddsm_steps = getMSSetSteps(objMSSet[i].ddsm_milestonesetId);
                                    objNewMSSet.push(obj);
                                }
                            }
                        } else {
                            if(objMSSet[i].ddsm_numberofsets != 0) {
                                obj1.ddsm_insertbeforeindex = objMSSet[i].ddsm_insertbeforeindex;
                                obj1.ddsm_numberofsets = objMSSet[i].ddsm_numberofsets;
                                obj1.setId = objMSSet[i].ddsm_milestonesetId;
                                IndexOffsetDocList.push(obj1);

                                obj.ddsm_insertbeforeindex = objMSSet[i].ddsm_insertbeforeindex;
                                obj.ddsm_numberofsets = objMSSet[i].ddsm_numberofsets;
                                obj._ddsm_steps = getMSSetSteps(objMSSet[i].ddsm_milestonesetId);
                                objNewMSSet.push(obj);
                            }
                        }
                    }
                }
            }

        } catch (err) { alert(err); }
    } else {
        alert("Error: " + reqMSSet.responseText);
    }
    //console.log('objNewMSSet..');
    //console.dir(objNewMSSet);
    return objNewMSSet;
}

function getMSSetSteps(MSSet_ID) {
    var serverUrl = Xrm.Page.context.getClientUrl();

    var req = new XMLHttpRequest();
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_milestonesetstepSet?"
    + "$select="
    + "ddsm_milestonesetstepId,"
    + "ddsm_name,"
    + "ddsm_ProjectTemplToMsSetStep,"
    + "ddsm_Category,"
    + "ddsm_duration,"
    + "ddsm_index,"
    + "ddsm_pendingphase,"
    + "ddsm_resultingphase,"
    + "ddsm_msspecialnote,"
    + "ddsm_responsible,"
    + "ddsm_requiresemptymeasure,"
    + "ddsm_requiresvalidmeasure,"
    + "ddsm_approvalthresholdcust,"
    + "ddsm_approvalthresholdstd,"
    + "ddsm_approvalthresholdnc,"
    + "ddsm_approvalthresholdrcx,"
    + "ddsm_projectphasename,"
    + "ddsm_projectstatus,"
    + "ddsm_initialoffered,"
    + "ddsm_securityincludeonlyguid,"
    + "ddsm_securityexcludeguid,"
    + "ddsm_insertset,"
    + "ddsm_CurrEscalOverdueLength,"
    + "ddsm_TradeAllyTemplateGUID,"
    + "ddsm_CustomerTemplateGUID,"
    + "ddsm_UserTemplateGUID,"
    + "ddsm_PartnerTemplateGUID,"
    + "ddsm_PushNotifications"
    + "&$orderby=ddsm_index asc"
    + "&$filter=ddsm_milestonetemplatesetnameid/Id  eq guid'" + MSSet_ID + "'", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();

    if (req.readyState == 4 && req.status == 200) {
        try {
            var obj = JSON.parse(req.responseText).d;
            return(obj.results);

        } catch (err) { alert(err); }
    } else {
        alert("Error: " + req.responseText);
    }

}

function createProjMSonAccount(proj_ID, projTpl_ID, StartDate, proj_Name, objMeasSite) {
    var PYMTPREF_SITE = 962080000;
    var PYMTPREF_PPN = 962080001;
    var PYMTPREF_LPC = 962080003;
    var PYMTPREF_LPC_SITE = null;
    var PPN_OVERRIDE_NO = 962080000;
    var PPN_OVERRIDE_YES = 962080001;

    var IdxOffSet = 0;

    var parentSite = siteProject.Name;

    var serverUrl = Xrm.Page.context.getClientUrl();

    var MSSetObject = getMSSets(projTpl_ID);

    var req = new XMLHttpRequest();
    //console.log("proj_ID: " + proj_ID);
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projecttemplateSet?"
/*    + "$select=ddsm_InitialStatus,ddsm_InitialReportingStatus,"
    + "ddsm_ProgramCode,"
    + "ddsm_LastMilestoneIndex,"
    + "ddsm_requirepayeeinfoindex,"
    + "ddsm_completeonlastmilestone,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_milestonetemplateId,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_name,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_Category,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_Duration,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_Index,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_PendingPhase,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ResultingPhase,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_MSSpecialNote,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_Responsible,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_RequiresEmptyMeasure,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_RequiresValidMeasure,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ApprovalThresholdCust,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ApprovalThresholdStd,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ApprovalThresholdNC,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ApprovalThresholdRCx,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ProjectPhaseName,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ProjectStatus,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_InitialOffered,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_SecurityIncludeOnlyGUID,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_SecurityExcludeGUID,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_insertset,"

    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_TradeAllyTemplateGUID,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_CustomerTemplateGUID,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_UserTemplateGUID,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_PartnerTemplateGUID,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_PushNotifications,"

    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_EscalOverdueLength" //1.29.2015
*/
    + "$expand=ddsm_ddsm_projecttemplate_ddsm_milestonetemplate"
    + "&$filter=ddsm_projecttemplateId eq guid'" + projTpl_ID + "' and statecode/Value eq 0", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();
    if (req.readyState == 4 && req.status == 200) {
        //From the project template - will also contain milestone information
        var _tplData = JSON.parse(req.responseText).d;
        if (_tplData.results[0] != null) {
            //Loop the Result Set
            var proj_obj = {};
            //for (var x in _tplData.results) { //BTL 8/26/2014 I commented this line and added the next line.
            for (var x = 0; x < _tplData.results.length; x++) {
                if (_tplData.results[x].ddsm_InitialStatus != null) {
                    proj_obj["ddsm_ProjectStatus"] = _tplData.results[x].ddsm_InitialStatus;
                }
                if (_tplData.results[x].ddsm_InitialReportingStatus != null) {
                    proj_obj["ddsm_Phase"] = _tplData.results[x].ddsm_InitialReportingStatus;
                }
                if (_tplData.results[x].ddsm_requirepayeeinfoindex != null) {
                    proj_obj["ddsm_requirepayeeinfoindex"] = _tplData.results[x].ddsm_requirepayeeinfoindex;
                }
                if (_tplData.results[x].ddsm_completeonlastmilestone != null) {
                    proj_obj["ddsm_completeonlastmilestone"] = _tplData.results[x].ddsm_completeonlastmilestone;
                }

                // Rename the Project as 000000-CODE-SiteName

                if (parentSite != null) {
                    proj_Name = proj_Name + "-" + _tplData.results[x].ddsm_ProgramCode + "-" + parentSite.substr(0, 20);
                }
                proj_obj["ddsm_name"] = proj_Name;

                proj_obj["ddsm_CompletedStatus"] = _tplData.results[x].ddsm_InitialStatus;

                if ((_tplData.results[x].ddsm_ddsm_projecttemplate_ddsm_milestonetemplate != undefined) &&
                    (_tplData.results[x].ddsm_ddsm_projecttemplate_ddsm_milestonetemplate.results != undefined)) {
                    var ms_array = _tplData.results[x].ddsm_ddsm_projecttemplate_ddsm_milestonetemplate.results;

                    var tempStartDate = new Date();

                    tempStartDate = StartDate;

                    var j = new Array();
                    var y = 0;
                    for (var i in ms_array) {
                        j[ms_array[i].ddsm_Index - 1] = y;
                        y++;
                    }
                    for (var k = 0; k < _tplData.results[x].ddsm_LastMilestoneIndex; k++) {

                        var ms_obj = new Object();
// random insert set steps
                        var currIndex = ms_array[j[k]].ddsm_Index;
                        if(MSSetObject != null && MSSetObject.length > 0){
                            for(var l = 0; l< MSSetObject.length; l++){
                                if(currIndex == MSSetObject[l].ddsm_insertbeforeindex){
                                    for(var p = 0; p < MSSetObject[l]._ddsm_steps.length;p++){
                                        ms_obj = new Object();
                                        ms_obj.ddsm_TargetStart = new Date(tempStartDate.getTime());
                                        if ((currIndex + MSSetObject[l]._ddsm_steps[p].ddsm_index - 1 + IdxOffSet) == 1) {
                                            ms_obj.ddsm_ActualStart = new Date(tempStartDate.getTime());
                                        }
                                        tempStartDate = new Date(tempStartDate.getTime() + (MSSetObject[l]._ddsm_steps[p].ddsm_duration) * 86400000);
                                        ms_obj.ddsm_TargetEnd = new Date(tempStartDate.getTime());
                                        ms_obj.ddsm_name = MSSetObject[l]._ddsm_steps[p].ddsm_name;
                                        ms_obj.ddsm_Category = MSSetObject[l]._ddsm_steps[p].ddsm_Category;
                                        ms_obj.ddsm_Index = currIndex + MSSetObject[l]._ddsm_steps[p].ddsm_index - 1 + IdxOffSet;
                                        ms_obj.ddsm_PendingPhase = MSSetObject[l]._ddsm_steps[p].ddsm_pendingphase;
                                        ms_obj.ddsm_ResultingPhase = MSSetObject[l]._ddsm_steps[p].ddsm_resultingphase;
                                        ms_obj.ddsm_MSSpecialNote = MSSetObject[l]._ddsm_steps[p].ddsm_msspecialnote;
                                        ms_obj.ddsm_Responsible = MSSetObject[l]._ddsm_steps[p].ddsm_responsible;
                                        ms_obj.ddsm_Duration = MSSetObject[l]._ddsm_steps[p].ddsm_duration;
                                        ms_obj.ddsm_RequiresEmptyMeasure = MSSetObject[l]._ddsm_steps[p].ddsm_requiresemptymeasure;
                                        ms_obj.ddsm_RequiresValidMeasure = MSSetObject[l]._ddsm_steps[p].ddsm_requiresvalidmeasure;
                                        ms_obj.ddsm_ApprovalThresholdCust = new Object();
                                        ms_obj.ddsm_ApprovalThresholdCust.Value = MSSetObject[l]._ddsm_steps[p].ddsm_approvalthresholdcust.Value;
                                        ms_obj.ddsm_ApprovalThresholdStd = new Object();
                                        ms_obj.ddsm_ApprovalThresholdStd.Value = MSSetObject[l]._ddsm_steps[p].ddsm_approvalthresholdstd.Value;
                                        ms_obj.ddsm_ApprovalThresholdNC = new Object();
                                        ms_obj.ddsm_ApprovalThresholdNC.Value = MSSetObject[l]._ddsm_steps[p].ddsm_approvalthresholdnc.Value;
                                        ms_obj.ddsm_ApprovalThresholdRCx = new Object();
                                        ms_obj.ddsm_ApprovalThresholdRCx.Value = MSSetObject[l]._ddsm_steps[p].ddsm_approvalthresholdrcx.Value;
                                        ms_obj.ddsm_ProjectPhaseName = MSSetObject[l]._ddsm_steps[p].ddsm_projectphasename;
                                        ms_obj.ddsm_ProjectStatus = MSSetObject[l]._ddsm_steps[p].ddsm_projectstatus;
                                        ms_obj.ddsm_InitialOffered = MSSetObject[l]._ddsm_steps[p].ddsm_initialoffered;
                                        ms_obj.ddsm_SecurityIncludeOnlyGUID = MSSetObject[l]._ddsm_steps[p].ddsm_securityincludeonlyguid;
                                        ms_obj.ddsm_SecurityExcludeGUID = MSSetObject[l]._ddsm_steps[p].ddsm_securityexcludeguid;
                                        ms_obj.ddsm_insertset = MSSetObject[l]._ddsm_steps[p].ddsm_insertset;
                                        ms_obj.ddsm_indexoffset = 0;
                                        ms_obj.ddsm_CurrEscalOverdueLength = MSSetObject[l]._ddsm_steps[p].ddsm_CurrEscalOverdueLength; // 1.29.2015

                                        ms_obj.ddsm_TradeAllyTemplateGUID = MSSetObject[l]._ddsm_steps[p].ddsm_TradeAllyTemplateGUID;
                                        ms_obj.ddsm_CustomerTemplateGUID = MSSetObject[l]._ddsm_steps[p].ddsm_CustomerTemplateGUID;
                                        ms_obj.ddsm_PushNotifications = MSSetObject[l]._ddsm_steps[p].ddsm_PushNotifications;
                                        ms_obj.ddsm_UserTemplateGUID = MSSetObject[l]._ddsm_steps[p].ddsm_UserTemplateGUID;
                                        ms_obj.ddsm_PartnerTemplateGUID = MSSetObject[l]._ddsm_steps[p].ddsm_PartnerTemplateGUID;

                                        ms_obj.ddsm_Status = new Object();
                                        if (ms_obj.ddsm_Index == 1) {
                                            ms_obj.ddsm_Status.Value = 962080001;
                                            proj_obj["ddsm_Responsible"] = MSSetObject[l]._ddsm_steps[p].ddsm_responsible;
                                        } else {
                                            ms_obj.ddsm_Status.Value = 962080000;
                                        }
                                        ms_obj.ddsm_ProjectToMilestoneId = new Object();
                                        ms_obj.ddsm_ProjectToMilestoneId.Id = proj_ID;
                                        ms_obj.ddsm_ProjectToMilestoneId.Name = proj_obj["ddsm_name"];
                                        ms_obj.ddsm_ProjectToMilestoneId.LogicalName = "ddsm_project";
                                        ms_obj.ddsm_ProjectTaskCount = 0;

                                        //console.dir(ms_obj);
                                        gen_H_createEntitySync(ms_obj, "ddsm_milestoneSet");
                                    }
                                    IdxOffSet += MSSetObject[l].ddsm_numberofsets;
                                }
                            }
                        }

                        if (ms_array[j[k]].ddsm_name != null) {
                            ms_obj = new Object();
                            ms_obj.ddsm_TargetStart = new Date(tempStartDate.getTime());
                            if ((ms_array[j[k]].ddsm_Index + IdxOffSet) == 1) {
                                ms_obj.ddsm_ActualStart = new Date(tempStartDate.getTime());
                                //console.log("ms_obj.ddsm_ActualStart: " + ms_obj.ddsm_ActualStart);
                            }
                            tempStartDate = new Date(tempStartDate.getTime() + (ms_array[j[k]].ddsm_Duration) * 86400000);
                            ms_obj.ddsm_TargetEnd = new Date(tempStartDate.getTime());
                            ms_obj.ddsm_name = ms_array[j[k]].ddsm_name;
                            ms_obj.ddsm_Category = ms_array[j[k]].ddsm_Category;
                            ms_obj.ddsm_Index = ms_array[j[k]].ddsm_Index + IdxOffSet;
                            ms_obj.ddsm_PendingPhase = ms_array[j[k]].ddsm_PendingPhase;
                            ms_obj.ddsm_ResultingPhase = ms_array[j[k]].ddsm_ResultingPhase;
                            ms_obj.ddsm_MSSpecialNote = ms_array[j[k]].ddsm_MSSpecialNote;

                            ms_obj.ddsm_Responsible = ms_array[j[k]].ddsm_Responsible;
                            ms_obj.ddsm_Duration = ms_array[j[k]].ddsm_Duration;
                            ms_obj.ddsm_RequiresEmptyMeasure = ms_array[j[k]].ddsm_RequiresEmptyMeasure;
                            ms_obj.ddsm_RequiresValidMeasure = ms_array[j[k]].ddsm_RequiresValidMeasure;
                            ms_obj.ddsm_ApprovalThresholdCust = new Object();
                            ms_obj.ddsm_ApprovalThresholdCust.Value = ms_array[j[k]].ddsm_ApprovalThresholdCust.Value;
                            ms_obj.ddsm_ApprovalThresholdStd = new Object();
                            ms_obj.ddsm_ApprovalThresholdStd.Value = ms_array[j[k]].ddsm_ApprovalThresholdStd.Value;
                            ms_obj.ddsm_ApprovalThresholdNC = new Object();
                            ms_obj.ddsm_ApprovalThresholdNC.Value = ms_array[j[k]].ddsm_ApprovalThresholdNC.Value;
                            ms_obj.ddsm_ApprovalThresholdRCx = new Object();
                            ms_obj.ddsm_ApprovalThresholdRCx.Value = ms_array[j[k]].ddsm_ApprovalThresholdRCx.Value;
                            ms_obj.ddsm_ProjectPhaseName = ms_array[j[k]].ddsm_ProjectPhaseName;
                            ms_obj.ddsm_ProjectStatus = ms_array[j[k]].ddsm_ProjectStatus;
                            ms_obj.ddsm_InitialOffered = ms_array[j[k]].ddsm_InitialOffered;
                            ms_obj.ddsm_SecurityIncludeOnlyGUID = ms_array[j[k]].ddsm_SecurityIncludeOnlyGUID;
                            ms_obj.ddsm_SecurityExcludeGUID = ms_array[j[k]].ddsm_SecurityExcludeGUID;
                            ms_obj.ddsm_insertset = ms_array[j[k]].ddsm_insertset;
                            ms_obj.ddsm_indexoffset = 0;

                            ms_obj.ddsm_TradeAllyTemplateGUID = ms_array[j[k]].ddsm_TradeAllyTemplateGUID;
                            ms_obj.ddsm_CustomerTemplateGUID = ms_array[j[k]].ddsm_CustomerTemplateGUID;
                            ms_obj.ddsm_PushNotifications = ms_array[j[k]].ddsm_PushNotifications;
                            ms_obj.ddsm_UserTemplateGUID = ms_array[j[k]].ddsm_UserTemplateGUID;
                            ms_obj.ddsm_PartnerTemplateGUID = ms_array[j[k]].ddsm_PartnerTemplateGUID;

                            ms_obj.ddsm_Status = new Object();
                            if (ms_obj.ddsm_Index == 1) {
                                ms_obj.ddsm_Status.Value = 962080001;
                                proj_obj["ddsm_Responsible"] = ms_array[j[k]].ddsm_Responsible;
                            } else {
                                ms_obj.ddsm_Status.Value = 962080000;
                            }
                            ms_obj.ddsm_ProjectToMilestoneId = new Object();
                            ms_obj.ddsm_ProjectToMilestoneId.Id = proj_ID;
                            ms_obj.ddsm_ProjectToMilestoneId.Name = proj_obj["ddsm_name"];
                            ms_obj.ddsm_ProjectToMilestoneId.LogicalName = "ddsm_project";

                            ms_obj.ddsm_MsTpltoMs = new Object();
                            ms_obj.ddsm_MsTpltoMs.Id = ms_array[j[k]].ddsm_milestonetemplateId;
                            ms_obj.ddsm_MsTpltoMs.Name = ms_array[j[k]].ddsm_name;
                            ms_obj.ddsm_MsTpltoMs.LogicalName = "ddsm_milestonetemplate";

                            ms_obj.ddsm_CurrEscalOverdueLength = (ms_array[j[k]].ddsm_EscalOverdueLength != null) ? ms_array[j[k]].ddsm_EscalOverdueLength : 0; //1.29.2015
                            ms_obj.ddsm_ProjectTaskCount = 0;

                            gen_H_createEntitySync(ms_obj, "ddsm_milestoneSet");
                        }
                    }
                    proj_obj["ddsm_EstimatedProjectComplete"] = tempStartDate;

                    gen_H_updateEntitySync(proj_ID, proj_obj, "ddsm_projectSet");

                    var msReq = new XMLHttpRequest();
                    var serverUrl = Xrm.Page.context.getClientUrl();
                    var URL = serverUrl + "/XRMServices/2011/OrganizationData.svc/ddsm_milestoneSet?"
                        + "$select="
                        + "ddsm_name,"
                        + "ddsm_ActualStart,"
                        + "ddsm_Status,"
                        + "ddsm_Index,"
                        + "ddsm_TargetEnd"
                        + "&$filter=ddsm_ProjectToMilestoneId/Id eq guid'" + proj_ID + "' and statecode/Value eq 0";

                    //console.log(URL);
                    msReq.open("GET", URL, false);
                    msReq.setRequestHeader("Accept", "application/json");
                    msReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    msReq.send();
                    if (msReq.readyState == 4 && msReq.status == 200) {
                        var responseData = JSON.parse(msReq.responseText).d;
                        var modeResponse = responseData.results;
                        //console.log(modeResponse);
                        if (modeResponse.length > 0) {
                            var proj_obj = {};
                            for (var i = 0; i < modeResponse.length; i++) {
                                if (parseInt(modeResponse[i].ddsm_Status.Value) == 962080001) {
                                    //console.log("ActualStart: " + modeResponse[i].ddsm_ActualStart);
                                    //console.log("TargetEnd: " + modeResponse[i].ddsm_TargetEnd);
                                    var msActStart = eval((modeResponse[i].ddsm_ActualStart).replace(/\/Date\((\d+)\)\//gi, 'new Date($1)'));
                                    var msTargetStart = eval((modeResponse[i].ddsm_TargetEnd).replace(/\/Date\((\d+)\)\//gi, 'new Date($1)'));

                                    proj_obj["ddsm_PendingMilestone"] = modeResponse[i].ddsm_name;
                                    proj_obj["ddsm_PendingMilestoneIndex"] = modeResponse[i].ddsm_Index;
                                    proj_obj["ddsm_PendingActualStart"] = msActStart;
                                    proj_obj["ddsm_PendingTargetEnd"] = msTargetStart;
                                }
                            }
                            var InitialLead = getInitialLeadSite(siteProject.Id);
                            if(InitialLead != null) {proj_obj.ddsm_InitialLead = InitialLead;}
                            gen_H_updateEntitySync(proj_ID, proj_obj, "ddsm_projectSet");
                        }
                    }
                }
            }
        }

    } else {
        alert("Error: " + req.responseText);
    }

    //  Financial Template
    req = new XMLHttpRequest();
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projecttemplateSet?"
    + "$select="
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_name,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_FinancialType,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_PaymentType,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_InitiatingMilestoneIndex,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_StageName1,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_StageName2,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_StageName3,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_StageName4,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_StageName5,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_Duration1,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_Duration2,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_Duration3,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_Duration4,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_Duration5,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_CalculationType,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_ProjectCompletableIndex,"
    + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_financialtemplateId"
    + "&$expand=ddsm_ddsm_projecttemplate_ddsm_financialtemplate"
    + "&$filter=ddsm_projecttemplateId eq guid'" + projTpl_ID + "' and statecode/Value eq 0", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();

    if (req.readyState == 4 && req.status == 200) {
        //From the project template - will also contain milestone information
        var _tplData = JSON.parse(req.responseText).d;
        if (_tplData.results[0] != null) {
            //Loop the Result Set
            for (var x = 0; x < _tplData.results.length; x++) {

                if ((_tplData.results[x].ddsm_ddsm_projecttemplate_ddsm_financialtemplate != undefined) &&
                    (_tplData.results[x].ddsm_ddsm_projecttemplate_ddsm_financialtemplate.results != undefined)) {

                    var fncl_array = _tplData.results[x].ddsm_ddsm_projecttemplate_ddsm_financialtemplate.results;

                    for (var i in fncl_array) {
                        if (fncl_array[i].ddsm_name != null) {
                            var fncl_obj = new Object();
                            fncl_obj.ddsm_name = fncl_array[i].ddsm_name;
                            //fncl_obj.ddsm_whogetspaid = new Object();
                            //fncl_obj.ddsm_whogetspaid.Value = pymtPref;
                            fncl_obj.ddsm_projecttofinancialid = new Object();
                            fncl_obj.ddsm_projecttofinancialid.Id = proj_ID;
                            fncl_obj.ddsm_projecttofinancialid.Name = proj_Name;
                            fncl_obj.ddsm_projecttofinancialid.LogicalName = "ddsm_project";

                            fncl_obj.ddsm_financialtype = new Object();
                            fncl_obj.ddsm_financialtype.Value = fncl_array[i].ddsm_FinancialType.Value;

                            fncl_obj.ddsm_status = new Object();
                            fncl_obj.ddsm_status.Value = 962080000;

                            fncl_obj.ddsm_initiatingmilestoneindex = fncl_array[i].ddsm_InitiatingMilestoneIndex + IdxOffSet;
                            fncl_obj.ddsm_stagename1 = fncl_array[i].ddsm_StageName1;
                            fncl_obj.ddsm_stagename2 = fncl_array[i].ddsm_StageName2;
                            fncl_obj.ddsm_stagename3 = fncl_array[i].ddsm_StageName3;
                            fncl_obj.ddsm_stagename4 = fncl_array[i].ddsm_StageName4;
                            fncl_obj.ddsm_stagename5 = fncl_array[i].ddsm_StageName5;
                            fncl_obj.ddsm_duration1 = fncl_array[i].ddsm_Duration1;
                            fncl_obj.ddsm_duration2 = fncl_array[i].ddsm_Duration2;
                            fncl_obj.ddsm_duration3 = fncl_array[i].ddsm_Duration3;
                            fncl_obj.ddsm_duration4 = fncl_array[i].ddsm_Duration4;
                            fncl_obj.ddsm_duration5 = fncl_array[i].ddsm_Duration5;
                            fncl_obj.ddsm_calculationtype = fncl_array[i].ddsm_CalculationType;
                            fncl_obj.ddsm_projectcompletableindex = fncl_array[i].ddsm_ProjectCompletableIndex;
                            fncl_obj.ddsm_financialtemplate = new Object();
                            fncl_obj.ddsm_financialtemplate.Id = fncl_array[i].ddsm_financialtemplateId;
                            fncl_obj.ddsm_financialtemplate.Name = fncl_array[i].ddsm_name;
                            fncl_obj.ddsm_financialtemplate.LogicalName = "ddsm_financialtemplate";

                            fncl_obj.ddsm_siteid = siteProject;
                            fncl_obj.ddsm_accountid = accountProject;

                            gen_H_createEntitySync(fncl_obj, "ddsm_financialSet");
                        }
                    }
                }
            }
            Fncl_UpdateFncls_onAccount(proj_ID, 0);
        }
    }

    createDocList_ptojTpl(proj_ID, proj_Name, projTpl_ID);

    var proj_Obj = {};
    proj_Obj.Id = proj_ID;
    proj_Obj.Name = proj_Name;
    proj_Obj.LogicalName = "ddsm_project";

    createProjMeas_ProjTplMeas(projTpl_ID, proj_Obj, siteProject, accountProject);

    if(objMeasSite === null){
        if(IndexOffsetDocList != null && IndexOffsetDocList.length > 0){updateMSIndexDocList(proj_ID);createDocList_msSet(proj_ID, proj_Name);}
        setTimeout(function () {
            var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
            var features = "location=yes,menubar=no,status=yes,toolbar=yes,resizable=yes";
            var url = "/main.aspx?etn=ddsm_project&extraqs=&histKey=" + randomnumber + "&id=" + proj_ID + "&newWindow=true&pagetype=entityrecord";
            window.open(url, "_blank");
            //Xrm.Utility.openEntityForm("ddsm_project", proj_ID);
        }, 200);
    } else {
        var objProj= {};
        objProj.Id = proj_ID;
        objProj.Name = proj_Name;
        objProj.LogicalName = "ddsm_project";

        verifyMeasToProj(objMeasSiteGlobal[objMeasSiteGlobal_count], objProj);
    }
}

function updateMSIndexDocList(proj_ID){
    var serverUrl = Xrm.Page.context.getClientUrl();
    var req = new XMLHttpRequest();
    var URL = serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
        + "$select="
        + "ddsm_documentconventionId,"
        + "ddsm_RequiredByStatus"
        + "&$orderby=ddsm_RequiredByStatus asc"
        + "&$filter=ddsm_ProjectId/Id eq guid'" + proj_ID + "'";
    req.open("GET", URL, false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();

    if (req.readyState == 4 && req.status == 200) {
        try {
            var obj = (JSON.parse(req.responseText).d).results;
            if(obj != null && obj.length > 0){
                for(var i = 0; i < IndexOffsetDocList.length; i++){
                    for(var j = 0; j < obj.length; j++){
                        if(IndexOffsetDocList[i].ddsm_insertbeforeindex == obj[j].ddsm_RequiredByStatus){
                            for(var l = j; l < obj.length; l++){
                                obj[l].ddsm_RequiredByStatus = obj[l].ddsm_RequiredByStatus + IndexOffsetDocList[i].ddsm_numberofsets;
                            }
                            break;
                        }
                    }
                }
                for(var j = 0; j < obj.length; j++){
                    gen_H_updateEntitySync(obj[j].ddsm_documentconventionId, {ddsm_RequiredByStatus: obj[j].ddsm_RequiredByStatus}, "ddsm_documentconventionSet");
                }
            }

        } catch (err) { alert(err); }
    } else {
        alert("Error: " + req.responseText);
    }
}

function createDocList_msSet(proj_ID, proj_Name){

    var docProjList = null;
    var _req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    _req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
    + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
    + "&$filter=ddsm_ProjectId/Id eq guid'" + proj_ID + "'", false);
    _req.setRequestHeader("Accept", "application/json");
    _req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    _req.send();
    if (_req.readyState == 4 && _req.status == 200) {
        var _tplData = JSON.parse(_req.responseText).d;
        if (_tplData.results[0] != null) {
            docProjList = _tplData.results;
        }
    }
    for(var j = 0; j < IndexOffsetDocList.length; j++) {
        var req = new XMLHttpRequest();
        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
        + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
        + "&$filter=ddsm_MilestoneSet/Id eq guid'" + IndexOffsetDocList[j].setId + "'", false);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.send();
        if (req.readyState == 4 && req.status == 200) {
            var _tplData = JSON.parse(req.responseText).d;
            if (_tplData.results[0] != null) {
                for (var x = 0; x < _tplData.results.length; x++) {
                    var addDocConv = true;
                    if (docProjList !== null) {
                        for (var i = 0; i < docProjList.length; i++) {
                            if (_tplData.results[x].ddsm_name == docProjList[i].ddsm_name) {
                                addDocConv = false;
                                break;
                            }
                        }
                    }
                    if (addDocConv) {
                        var proj_obj = {};
                        if (_tplData.results[x].ddsm_RequiredByStatus != null) {
                            proj_obj["ddsm_RequiredByStatus"] = parseInt(_tplData.results[x].ddsm_RequiredByStatus) + IndexOffsetDocList[j].ddsm_insertbeforeindex - 1;
                        }
                        if (_tplData.results[x].ddsm_name != null) {
                            proj_obj["ddsm_name"] = _tplData.results[x].ddsm_name;
                        }
                        if (_tplData.results[x].ddsm_Uploaded != null) {
                            proj_obj["ddsm_Uploaded"] = _tplData.results[x].ddsm_Uploaded;
                        }

                        proj_obj.ddsm_ProjectId = new Object();
                        proj_obj.ddsm_ProjectId.Id = proj_ID;
                        proj_obj.ddsm_ProjectId.Name = proj_Name;
                        proj_obj.ddsm_ProjectId.LogicalName = "ddsm_project";

                        //console.dir(proj_obj);

                        gen_H_createEntitySync(proj_obj, "ddsm_documentconventionSet");
                    }
                }
            }
        }
    }
}

function verifyMeasTpl(ID){
    var req = new XMLHttpRequest();
    try {
        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_measuretemplateSet?"
        + "$select="
        + "statecode"
        + "&$filter=ddsm_measuretemplateId eq guid'" + ID + "'", false);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.send();
        if (req.readyState == 4 && req.status == 200) {
            var _tplData = JSON.parse(req.responseText).d;
            if(_tplData.results[0].statecode.Value == 0) {return true;} else {return false;}
        } else {return false;}
    } catch (err) {
        alert("Error verify Measure Template. " + err);
        return false;
    }

}
function verifyMeasToProj(objOneMeasSite, objProj){

    if(verifyMeasTpl(objOneMeasSite.ddsm_MeasureSelector.Id) && !objOneMeasSite.ddsm_PlaceholderMeasure) {
        createMeasForTpl(objOneMeasSite.ddsm_MeasureSelector, objOneMeasSite, objProj);
    } else if(verifyMeasTpl(objOneMeasSite.ddsm_MeasureSelector.Id) && objOneMeasSite.ddsm_PlaceholderMeasure) {
        createMeasToProj(objOneMeasSite, objProj, " is a generic measure, please select the specific measure to be used.");
    } else {
        createMeasToProj(objOneMeasSite, objProj, " has an Inactive Measure Template, please select a new Measure Template to be used.");
    }

}

function createMeasToProj(objOneMeasSite, objProj, message){
    var _objOneMeasSite = objOneMeasSite;
    var _objProj = objProj;

    if(confirm("This measure: " + objOneMeasSite.ddsm_MeasureLeadSource.Name + ", Units: " + objOneMeasSite.ddsm_phase1units + ", KW Savings: " + objOneMeasSite.ddsm_phase1savingskw + ", kWh Savings: " + objOneMeasSite.ddsm_phase1savingskwh + ", Incentive Total: " + objOneMeasSite.ddsm_phase1incentivetotal.Value + message)) {

        var objecttypes = Xrm.Internal.getEntityCode("ddsm_measuretemplate");
        var viewId = "{00000000-0000-0000-0000-000000000001}";

        var fetchXml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
            "<entity name='ddsm_measuretemplate'>" +
            "<attribute name='ddsm_measuretemplateid' />" +
            "<attribute name='statecode' />" +
            "<attribute name='ddsm_name' />" +
            "<attribute name='createdon' />" +
            "<attribute name='ddsm_placeholdermeasure' />" +
            "<order attribute='ddsm_name' descending='false' />" +
            "<filter type='and'>" +
            "<condition attribute='ddsm_placeholdermeasure' operator='eq' value='false' />" +
            "</filter>" +
            "<filter type='and'>" +
            "<condition attribute='statecode' operator='eq' value='0' />" +
            "</filter>" +
            "</entity>" +
            "</fetch>";

        var layoutXml = "" +
            "<grid name='' object='1' jump='name' select='1' icon='1' preview='1'>" +
            "<row name='result' id='ddsm_measuretemplateid'>" +
            "<cell name='ddsm_name' width='300' />" +
            "<cell name='createdon' width='100' />" +
            "</row>" +
            "</grid>";

        var customView = {
            fetchXml: fetchXml,
            id: viewId,
            layoutXml: layoutXml,
            name: "Measure Tpl LookupView (PlaceholderMeasure=False)",
            recordType: objecttypes,
            Type: 0
        };

        var callbackReference = {
            callback: function (lkp_popup) {
                try {
                    if (lkp_popup && lkp_popup.items) {
                        var meastpl = {};
                        meastpl.Id = lkp_popup.items[0].id;
                        meastpl.Name = lkp_popup.items[0].name;
                        meastpl.LogicalName = lkp_popup.items[0].typename;
                        createMeasForTpl(meastpl, _objOneMeasSite, _objProj);
                    } else {
                        objMeasSiteGlobal_length--;
                        objMeasSiteGlobal_count++;
                        if(objMeasSiteGlobal_count < objMeasSiteGlobal_length2 && objMeasSiteGlobal_length != 0) {
                            createMeasToProj(objMeasSiteGlobal[objMeasSiteGlobal_count], objProj);
                        } else {
                            if(IndexOffsetDocList != null && IndexOffsetDocList.length > 0){updateMSIndexDocList(_objProj.Id);}
                            setTimeout(function () {
                                document.getElementById('WebResource_measure_grid_site').contentWindow.reloadGrid();
                                var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                                var features = "location=yes,menubar=no,status=yes,toolbar=yes,resizable=yes";
                                var url = "/main.aspx?etn=ddsm_project&extraqs=&histKey=" + randomnumber + "&id=" + objProj.Id + "&newWindow=true&pagetype=entityrecord";
                                window.open(url, "_blank");
                                //Xrm.Utility.openEntityForm("ddsm_project", proj_ID);
                            }, 200);
                        }
                    }
                } catch (err) {
                    alert("Error select measure template: " + err);
                }
            }
        }

        LookupObjectsWithCallback(callbackReference, null, "single", objecttypes, 0, null, "", null, null, null, null, null, null, viewId, [customView]);


    } else {

        objMeasSiteGlobal_length--;
        objMeasSiteGlobal_count++;
        if(objMeasSiteGlobal_count < objMeasSiteGlobal_length2 && objMeasSiteGlobal_length != 0) {
            createMeasToProj(objMeasSiteGlobal[objMeasSiteGlobal_count], objProj);
        } else {
            setTimeout(function () {
                document.getElementById('WebResource_measure_grid_site').contentWindow.reloadGrid();
                var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                var features = "location=yes,menubar=no,status=yes,toolbar=yes,resizable=yes";
                var url = "/main.aspx?etn=ddsm_project&extraqs=&histKey=" + randomnumber + "&id=" + objProj.Id + "&newWindow=true&pagetype=entityrecord";
                window.open(url, "_blank");
                //Xrm.Utility.openEntityForm("ddsm_project", proj_ID);
            }, 200);
        }
    }
}

function createMeasForTpl(objMeasTpl, objOneMeasSite, objProj){

    createDocList_measTpl(objProj.Id, objProj.Name, objMeasTpl.Id);

    var req = new XMLHttpRequest();
    var measTpl_ID = objMeasTpl.Id;
    var now = new Date();
    var serverUrl = Xrm.Page.context.getClientUrl();
    var measname = (objProj.Name).substr(0, 6) + "-" + objMeasTpl.Name + "_" + now.getFullYear() + now.getMonth() + now.getDate() + "-" + now.getHours() + now.getMinutes() + now.getSeconds();
    var objMeas = {};

    objMeas.ddsm_name = measname;
    objMeas.ddsm_MeasureSelector = objMeasTpl;
    objMeas.ddsm_AccountId = objOneMeasSite.ddsm_AccountId;
    objMeas.ddsm_parentsite = objOneMeasSite.ddsm_parentsite;
    objMeas.ddsm_ProjectToMeasureId = objProj;
    objMeas.ddsm_MeasureLeadSource = objOneMeasSite.ddsm_MeasureLeadSource;
    var InitialLead = getInitialLeadSite(siteProject.Id);
    if(InitialLead != null) {objMeas.ddsm_InitialLead = InitialLead;}

    try {
        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_measuretemplateSet?"
        + "$select="
        + "ddsm_IncRateUnit,"
        + "ddsm_IncRateKW,"
        + "ddsm_IncRatekWh,"
        + "ddsm_IncRatethm,"
        + "ddsm_SavingsperUnitKW,"
        + "ddsm_SavingsperUnitkWh,"
        + "ddsm_SavingsperUnitthm,"
        + "ddsm_Program,"
        + "ddsm_MeasureCode,"
        + "ddsm_MeasureNumber,"
        + "ddsm_EnergyType,"
        + "ddsm_Category,"
        + "ddsm_MeasureGroup,"
        + "ddsm_MeasureSubgroup,"
        + "ddsm_Size,"
        + "ddsm_EUL,"
        + "ddsm_BaseEquipDescr,"
        + "ddsm_EffEquipDescr,"
        + "ddsm_AnnualFixedCosts,"
        + "ddsm_ClientDescription,"
        + "ddsm_EndUse,"
        + "ddsm_EquipmentDescription,"
        + "ddsm_IncrementalCost,"
        + "ddsm_LMLightingorNon,"
        + "ddsm_MeasureLife,"
        + "ddsm_MeasureNotes,"
        + "ddsm_Source,"
        + "ddsm_Unit,"
        + "ddsm_StudyMeasure,"

        + "ddsm_MeasureType,"
        + "ddsm_AnnualHoursBefore,"
        + "ddsm_AnnualHoursAfter,"
        + "ddsm_SummerDemandReductionKW,"
        + "ddsm_WinterDemandReductionKW,"
        + "ddsm_WattsBase,"
        + "ddsm_WattsEff,"
        + "ddsm_ClientPeakKWSavings,"
        + "ddsm_MotorType,"
        + "ddsm_MotorHP,"
        + "ddsm_measuresubgroup2,"
        + "ddsm_SavingsperUnitCCF,"
        + "ddsm_ProgramCycle,"

        + "ddsm_BPAEffectiveEndDate,"
        + "ddsm_BPAEffectiveStartDate,"
        + "ddsm_BPAIncentiveperUnit,"
        + "ddsm_BPAkWhSavingsperUnitTarget,"
        + "ddsm_BPAReimbursableperUnitTarget,"
        + "ddsm_BPAMeasureNumber,"
        + "ddsm_BPAMeasureVersion,"
        + "ddsm_MaxLoanperUnit,"

        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_name,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateSavings,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateIncentive,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_PerUnitLock,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_SavingsLock,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentiveRatesLock,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentivesLock"//+ "&$orderby=ddsm_projTplToMilestoneTpl/ddsm_Index asc"
        + "&$expand=ddsm_ddsm_measurecalculationtemplate_ddsm_measur"
        + "&$filter=ddsm_measuretemplateId eq guid'" + measTpl_ID + "' and statecode/Value eq 0", false);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.send();
        if (req.readyState == 4 && req.status == 200) {
            //From the project template - will also contain milestone information
            var _tplData = JSON.parse(req.responseText).d;
            if (_tplData.results[0] != null) {

                //console.dir(_tplData.results);
                var measCalcTpl_Data = _tplData.results[0].ddsm_ddsm_measurecalculationtemplate_ddsm_measur;
                var measCalcTpl_Name = measCalcTpl_Data.ddsm_name;
                var measCalcTpl_CalcSavings = measCalcTpl_Data.ddsm_CalculateSavings;
                var measCalcTpl_CalcIncentives = measCalcTpl_Data.ddsm_CalculateIncentive;
                var measCalcTpl_PerUnitLock = measCalcTpl_Data.ddsm_PerUnitLock;
                var measCalcTpl_SavingsLock = measCalcTpl_Data.ddsm_SavingsLock;
                var measCalcTpl_IncentiveRatesLock = measCalcTpl_Data.ddsm_IncentiveRatesLock;
                var measCalcTpl_IncentivesLock = measCalcTpl_Data.ddsm_IncentivesLock;
                var measCalcTpl_CalcTypeValues = "";

                objMeas.ddsm_Program = _tplData.results[0].ddsm_Program;
                objMeas.ddsm_MeasureCode = _tplData.results[0].ddsm_MeasureCode;
                objMeas.ddsm_MeasureNumber = _tplData.results[0].ddsm_MeasureNumber;
                objMeas.ddsm_EnergyType = _tplData.results[0].ddsm_EnergyType;
                objMeas.ddsm_Category = _tplData.results[0].ddsm_Category;
                objMeas.ddsm_MeasureGroup = _tplData.results[0].ddsm_MeasureGroup;
                objMeas.ddsm_MeasureSubgroup = _tplData.results[0].ddsm_MeasureSubgroup;
                objMeas.ddsm_Size = _tplData.results[0].ddsm_Size;
                objMeas.ddsm_EUL = _tplData.results[0].ddsm_EUL;
                objMeas.ddsm_BaseEquipDescr = _tplData.results[0].ddsm_BaseEquipDescr;
                objMeas.ddsm_EffEquipDescr = _tplData.results[0].ddsm_EffEquipDescr;
                objMeas.ddsm_ClientDescription = _tplData.results[0].ddsm_ClientDescription;
                objMeas.ddsm_EndUse = _tplData.results[0].ddsm_EndUse;
                objMeas.ddsm_EquipmentDescription = _tplData.results[0].ddsm_EquipmentDescription;
                objMeas.ddsm_LMLightingorNon = _tplData.results[0].ddsm_LMLightingorNon;
                objMeas.ddsm_MeasureNotes = _tplData.results[0].ddsm_MeasureNotes;
                objMeas.ddsm_Source = _tplData.results[0].ddsm_Source;
                objMeas.ddsm_Unit = _tplData.results[0].ddsm_Unit;
                objMeas.ddsm_StudyMeasure = _tplData.results[0].ddsm_StudyMeasure;
                objMeas.ddsm_MeasureType = _tplData.results[0].ddsm_MeasureType;
                objMeas.ddsm_AnnualHoursBefore = _tplData.results[0].ddsm_AnnualHoursBefore;
                objMeas.ddsm_AnnualHoursAfter = _tplData.results[0].ddsm_AnnualHoursAfter;
                objMeas.ddsm_SummerDemandReductionKW = _tplData.results[0].ddsm_SummerDemandReductionKW;
                objMeas.ddsm_WinterDemandReductionKW = _tplData.results[0].ddsm_WinterDemandReductionKW;
                objMeas.ddsm_WattsBase = _tplData.results[0].ddsm_WattsBase;
                objMeas.ddsm_WattsEff = _tplData.results[0].ddsm_WattsEff;
                objMeas.ddsm_clientpeakkwsavings = _tplData.results[0].ddsm_ClientPeakKWSavings;
                objMeas.ddsm_MotorType = _tplData.results[0].ddsm_MotorType;
                objMeas.ddsm_MotorHP = _tplData.results[0].ddsm_MotorHP;
                objMeas.ddsm_measuresubgroup2 = _tplData.results[0].ddsm_measuresubgroup2;
                objMeas.ddsm_IncRateUnit = _tplData.results[0].ddsm_IncRateUnit;
                objMeas.ddsm_IncRateKW = _tplData.results[0].ddsm_IncRateKW;
                objMeas.ddsm_IncRatekWh = _tplData.results[0].ddsm_IncRatekWh;
                objMeas.ddsm_IncRatethm = _tplData.results[0].ddsm_IncRatethm;
                objMeas.ddsm_IncrementalCost = _tplData.results[0].ddsm_IncrementalCost;
                objMeas.ddsm_MeasureLife = _tplData.results[0].ddsm_MeasureLife;
                objMeas.ddsm_AnnualFixedCosts = _tplData.results[0].ddsm_AnnualFixedCosts;
                objMeas.ddsm_ProgramCycleId = _tplData.results[0].ddsm_ProgramCycle;
                //BPA
                objMeas.ddsm_BPAEffectiveEndDate = _tplData.results[0].ddsm_BPAEffectiveEndDate;
                objMeas.ddsm_BPAEffectiveStartDate = _tplData.results[0].ddsm_BPAEffectiveStartDate;
                objMeas.ddsm_BPAIncentivePerUnitTarget = _tplData.results[0].ddsm_BPAIncentiveperUnit;
                objMeas.ddsm_BPAkWhSavingsperUnitTarget = _tplData.results[0].ddsm_BPAkWhSavingsperUnitTarget;
                objMeas.ddsm_BPAReimbursableperUnitTarget = _tplData.results[0].ddsm_BPAReimbursableperUnitTarget;
                objMeas.ddsm_BPAMeasureNumber = _tplData.results[0].ddsm_BPAMeasureNumber;
                objMeas.ddsm_BPAMeasureVersion = _tplData.results[0].ddsm_BPAMeasureVersion;
                objMeas.ddsm_MaxLoanperUnit = _tplData.results[0].ddsm_MaxLoanperUnit;

                //Measure Calc Template Data
                if (measCalcTpl_CalcSavings == true) {
                    measCalcTpl_CalcTypeValues += "Calculate Savings; ";
                }
                if (measCalcTpl_CalcIncentives == true) {
                    measCalcTpl_CalcTypeValues += "Calculate Incentives; ";
                }
                if (measCalcTpl_PerUnitLock == true) {
                    measCalcTpl_CalcTypeValues += "Lock Per Units; ";
                }
                if (measCalcTpl_SavingsLock == true) {
                    measCalcTpl_CalcTypeValues += "Lock Savings; ";
                }
                if (measCalcTpl_IncentiveRatesLock == true) {
                    measCalcTpl_CalcTypeValues += "Lock Incentive Rates; ";
                }
                if (measCalcTpl_IncentivesLock == true) {
                    measCalcTpl_CalcTypeValues += "Lock Incentives; ";
                }

                if (measCalcTpl_Name != null) {
                    objMeas.ddsm_CalculationType = measCalcTpl_Name;
                }
                if (measCalcTpl_CalcTypeValues != null) {
                    objMeas.ddsm_CalculationTypeValues = measCalcTpl_CalcTypeValues;
                }

                //copies savings
                //Copy to Phase 1

                objMeas.ddsm_CurrentUnits = objOneMeasSite.ddsm_phase1units;
                objMeas.ddsm_CurrentSavingskWh = objOneMeasSite.ddsm_phase1savingskwh;
                objMeas.ddsm_currentsavingskw = objOneMeasSite.ddsm_phase1savingskw;
                objMeas.ddsm_CurrentSavingsthm = objOneMeasSite.ddsm_phase1savingsthm;
                objMeas.ddsm_CurrentIncTotal = objOneMeasSite.ddsm_phase1incentivetotal;

                objMeas.ddsm_CurrentSavingsCCF = objOneMeasSite.ddsm_Phase1SavingsCCF;

                objMeas.ddsm_phase1units = objOneMeasSite.ddsm_phase1units;
                objMeas.ddsm_phase1savingskwh = objOneMeasSite.ddsm_phase1savingskwh;
                objMeas.ddsm_phase1savingskw = objOneMeasSite.ddsm_phase1savingskw;
                objMeas.ddsm_phase1savingsthm = objOneMeasSite.ddsm_phase1savingsthm;
                objMeas.ddsm_Phase1SavingsCCF = objOneMeasSite.ddsm_Phase1SavingsCCF;
                objMeas.ddsm_phase1perunitkwh = objOneMeasSite.ddsm_phase1perunitkwh;
                objMeas.ddsm_phase1perunitkw = objOneMeasSite.ddsm_phase1perunitkw;
                objMeas.ddsm_phase1perunitthm = objOneMeasSite.ddsm_phase1perunitthm;
                objMeas.ddsm_phase1incentivetotal = objOneMeasSite.ddsm_phase1incentivetotal;
                objMeas.ddsm_phase1incentiveunits = objOneMeasSite.ddsm_phase1incentiveunits;
                objMeas.ddsm_CostEquipment = objOneMeasSite.ddsm_CostEquipment;
                objMeas.ddsm_CostLabor = objOneMeasSite.ddsm_CostLabor;
                objMeas.ddsm_CostOther = objOneMeasSite.ddsm_CostOther;
                objMeas.ddsm_CostTotal = objOneMeasSite.ddsm_CostTotal;

                gen_H_createEntitySync(objMeas, "ddsm_measureSet");

                var objUpdMeasSite = {};
                objUpdMeasSite.ddsm_MeasureStatus = { Value: '962080001'};
                objUpdMeasSite.ddsm_ProjectToMeasureId = objProj;
                var measSiteId = objOneMeasSite.ddsm_MeasureLeadSource.Id;
                gen_H_updateEntitySync(measSiteId, objUpdMeasSite, 'ddsm_sitemeasureSet');

                objMeasSiteGlobal_length--;
                objMeasSiteGlobal_count++;
                if(objMeasSiteGlobal_count < objMeasSiteGlobal_length2 && objMeasSiteGlobal_length != 0) {
                    verifyMeasToProj(objMeasSiteGlobal[objMeasSiteGlobal_count], objProj);
                } else {
                    setTimeout(function () {
                        document.getElementById('WebResource_measure_grid_site').contentWindow.reloadGrid();
                        var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                        var features = "location=yes,menubar=no,status=yes,toolbar=yes,resizable=yes";
                        var url = "/main.aspx?etn=ddsm_project&extraqs=&histKey=" + randomnumber + "&id=" + objProj.Id + "&newWindow=true&pagetype=entityrecord";
                        window.open(url, "_blank");
                        //Xrm.Utility.openEntityForm("ddsm_project", proj_ID);
                    }, 200);
                }

            }
        }
    } catch (err) {
        alert("Error generate Measure with Measure Template & Measure Calculation Template for New Project. " + err);
    }
}

function changeProjectPlus(objMeasSite, objSite) {
    if (Xrm.Page.ui.getFormType() == 1) { alert("Error: Please save this Account!"); } else {

        if(objSite == null) {
            try {
                var req = new XMLHttpRequest();
                req.open("GET", Xrm.Page.context.getClientUrl() + "/xrmservices/2011/OrganizationData.svc/ddsm_siteSet?"
                + "$select="
                + "ddsm_name,"
                + "ddsm_siteId"
                + "&$filter=ddsm_parentaccount/Id eq guid'" + Xrm.Page.data.entity.getId() + "' and statecode/Value eq 0", false);
                req.setRequestHeader("Accept", "application/json");
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.send();
                if (req.readyState == 4 && req.status == 200) {
                    var _tplData = JSON.parse(req.responseText).d;
                    if (_tplData.results != null && _tplData.results.length > 0) {
                        var siteList = _tplData.results;
                        if (siteList.length > 1) {

                            siteProject.Id = siteList[0].ddsm_siteId;
                            siteProject.Name = siteList[0].ddsm_name;
                            siteProject.LogicalName = "ddsm_site";

                            var popupHTML = '<div id="overlay" style="display:none; position:fixed; z-index:998; opacity:0.5; filter:alpha(opacity=50); top:0; right:0; left:0; bottom:0; background:#000000 url(/WebResources/ddsm_loading.gif) 50% 50% no-repeat;"></div>';
                            popupHTML += '<div id="popup" style="display:none; position:fixed; border:3px solid #999; background:#fff; width:auto; height:auto; top:50%; left:50%; margin:-100px 0 0 -300px; z-index:1000;  border-radius:10px; padding:30px;">';
                            popupHTML += '<h2>Select Site Account:</h2>';
                            popupHTML += '<table><tr>';
                            popupHTML += '<td style="padding-left:10px;"><strong>Site&nbsp;List:&nbsp;</td><td id="tdSelect">';
                            popupHTML += '</td><td style="padding-left:10px;width:100px;"><input type="button" value="Next" id="nextBtn" name="nextBtn" style="margin:0 5px 0 80px;width:50px;"></td><td>';
                            popupHTML += '</td><td style="padding-lefty10px;"><input type="button" value="Cancel" id="cancelBtn" name="cancelBtn"></td><td></tr></table>';
                            popupHTML += '</div>';


                            var SiteNameList = $("<select>").attr("id", "SiteNameList").attr("name", "SiteNameList");
                            for (var i = 0; i < siteList.length; i++) {
                                SiteNameList.append($('<option>', {
                                    value: siteList[i].ddsm_siteId,
                                    text: siteList[i].ddsm_name
                                }));
                                //console.dir(siteList[i]);
                            }
                            $('body').append(popupHTML);
                            SiteNameList.change(function () {
                                siteProject.Id = $(this).val();
                                siteProject.Name = $(this)[0][$(this)[0].selectedIndex].text;
                                siteProject.LogicalName = "ddsm_site";
                                //console.log($(this)[0][$(this)[0].selectedIndex].text);
                                //console.log($(this).val());
                            }).trigger('change');
                            $('#tdSelect').append(SiteNameList);

                            $('#nextBtn').on('click', function () {
                                $('#overlay, #popup').hide();
                                $('#overlay').remove();
                                $('#popup').remove();
                                setDataProjTpl(objMeasSite);
                            });

                            $('#cancelBtn').on('click', function () {
                                siteProject = {};
                                $('#overlay, #popup').hide();
                                $('#overlay').remove();
                                $('#popup').remove();
                            });

                            $('#popup').show();
                            $('#overlay').show();

                        } else {
                            siteProject.Id = siteList[0].ddsm_siteId;
                            siteProject.Name = siteList[0].ddsm_name;
                            siteProject.LogicalName = "ddsm_site";
                            setDataProjTpl(objMeasSite);
                        }
                    } else {
                        siteProject = {};
                        setDataProjTpl(objMeasSite);
                    }
                }
            } catch (err) {
                alert("Error . " + err);
            }
        } else {
            siteProject = objSite;
            setDataProjTpl(objMeasSite);
        }

    }
}

function setDataProjTpl(objMeasSite){
    if(typeof siteProject.Id == 'undefined'){alert("No Site exists. Please create a Site before initiating a project.");return;}
    //console.dir(siteProject);

    var proj_number = Proj_GenerateProjID();
    objMeasSiteGlobal = objMeasSite;
    if(objMeasSite !== null) {objMeasSiteGlobal_length = objMeasSite.length; objMeasSiteGlobal_length2 = objMeasSite.length; objMeasSiteGlobal_count = 0;} else {objMeasSiteGlobal_length = 0;objMeasSiteGlobal_length2 = 0;objMeasSiteGlobal_count = 0;}

    var popupHTML = '<div id="overlay" style="display:none; position:fixed; z-index:998; opacity:0.5; filter:alpha(opacity=50); top:0; right:0; left:0; bottom:0; background:#000000 url(/WebResources/ddsm_loading.gif) 50% 50% no-repeat;"></div>';
    popupHTML += '<div id="loading" style="display:none; position:fixed; width:320px; height:320px; top:50%; left:50%; margin:-160px 0 0 -160px; z-index:999;"><img src="/WebResources/ddsm_loading.gif" alt="loading"/></div>';
    popupHTML += '<div id="popup" style="display:none; position:fixed; border:3px solid #999; background:#fff; width:auto; height:auto; top:50%; left:50%; margin:-100px 0 0 -300px; z-index:1000;  border-radius:10px; padding:30px;">';
    popupHTML += '<h2>Create New Project</h2>';
    popupHTML += '<table><tr><td><strong>Project&nbsp;Number:&nbsp;</td><td id="proj_number">';
    popupHTML += proj_number;
    popupHTML += '</td><td style="padding-left:10px;"><strong>Start&nbsp;Date:&nbsp;</td><td>';
    popupHTML += '<input type="text" value="" id="sProjectDate" name="sProjectDate"/>';
    popupHTML += '</td><td style="padding-left:10px;width:100px;"><input type="button" value="Next" id="createProject" name="createProject" style="margin:0 5px 0 80px;width:50px;"></td><td>';
    popupHTML += '</td><td style="padding-lefty10px;"><input type="button" value="Cancel" id="cancelProject" name="cancelProject"></td><td></tr></table>';
    popupHTML += '</div>';

    $('body').append(popupHTML);
    $('#popup').show();
    $('#overlay').show();

    //ng.ready(function () {
    var my_cal = new ng.Calendar({
        input: 'sProjectDate',
        start_day: '0',
        date_format: 'm/d/Y',
        weekend: [0, 6],
        allow_weekend_selection: true,
        start_date: 'year - 5',
        end_date: 'year + 10',
        display_date: new Date()
    });

    $(".ng_cal_input_field").attr("size", "7");

    $('#createProject').on('click', function () {
        if ($('.ng_cal_input_field ').val() != "") {
            ddsm_StartDate_NewProject = UTCToLocalTime(new Date($('.ng_cal_input_field ').val()));
            ddsm_name_NewProject = $('#proj_number').text();
            $('#popup').remove();
            $('#loading').show();

            var objecttypes = Xrm.Internal.getEntityCode("ddsm_projecttemplate");
            var lookupURI = "/_controls/lookup/lookupinfo.aspx";
            lookupURI += "?LookupStyle=single";
            lookupURI += "&objecttypes=" + objecttypes;
            lookupURI += "&ShowNewButton=0";
            lookupURI += "&ShowPropButton=1";
            lookupURI += "&browse=false";
            lookupURI += "&AllowFilterOff=0";
            lookupURI += "&DefaultType=" + objecttypes;
            lookupURI += "&DisableQuickFind=0";
            lookupURI += "&DisableViewPicker=0";
            var DialogOption = new Xrm.DialogOptions;
            DialogOption.width = 550; DialogOption.height = 550;
            Xrm.Internal.openDialog(lookupURI, DialogOption, null, null, Callback_lkp_popup);

        } else (alert("Error: Select Sart Date new Project!"))
    });

    $('#cancelProject').on('click', function () {
        $('#overlay, #popup, #loading').hide();
        $('#overlay').remove();
        $('#popup').remove();
        $('#loading').remove();
    });
}
function Callback_lkp_popup(lkp_popup) {
    try {
        if (lkp_popup) {
            if (lkp_popup.items) {
                var proj_obj = {};
                proj_obj['ddsm_name'] = ddsm_name_NewProject;

                var projecttemplate = {};
                projecttemplate.Id = lkp_popup.items[0].id;
                projecttemplate.Name = lkp_popup.items[0].name;
                projecttemplate.LogicalName = lkp_popup.items[0].typename;
                proj_obj['ddsm_ProjectTemplateId'] = projecttemplate;

                proj_obj['ddsm_StartDate'] = ddsm_StartDate_NewProject;

                proj_obj['ddsm_ParentSiteId'] = siteProject;

                var msReq = new XMLHttpRequest();
                var serverUrl = Xrm.Page.context.getClientUrl();
                var URL = serverUrl + "/xrmservices/2011/OrganizationData.svc/AccountSet?"
                    + "$select="
                    + "TransactionCurrencyId"
                    + "&$filter=AccountId eq guid'" + accountProject.Id + "'";
                msReq.open("GET", URL, false);
                msReq.setRequestHeader("Accept", "application/json");
                msReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                msReq.send();
                if (msReq.readyState == 4 && msReq.status == 200) {
                    var _msData = JSON.parse(msReq.responseText).d;
                    if (_msData.results[0] != null) {
                        proj_obj['TransactionCurrencyId'] = _msData.results[0].TransactionCurrencyId;
                    }
                }

                proj_obj['ddsm_AccountId'] = accountProject;

                URL = serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_siteSet?"
                    + "$select="
                    + "ddsm_account"
                    + "&$filter=ddsm_siteId eq guid'" + siteProject.Id + "'";
                msReq.open("GET", URL, false);
                msReq.setRequestHeader("Accept", "application/json");
                msReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                msReq.send();
                if (msReq.readyState == 4 && msReq.status == 200) {
                    var _msData = JSON.parse(msReq.responseText).d;
                    if (_msData.results[0] != null) {
                        if(_msData.results[0].ddsm_account != null) { proj_obj['ddsm_AccountNumber'] = _msData.results[0].ddsm_account;}
                    }
                }

                if (Xrm.Page.getAttribute("ddsm_bdlead").getValue() != null) {
                    var ddsm_dbleadid = {};
                    ddsm_dbleadid.Id = Xrm.Page.getAttribute("ddsm_bdlead").getValue()[0].id;
                    ddsm_dbleadid.Name = Xrm.Page.getAttribute("ddsm_bdlead").getValue()[0].name;
                    ddsm_dbleadid.LogicalName = Xrm.Page.getAttribute("ddsm_bdlead").getValue()[0].typename;
                    proj_obj['ddsm_dbleadid'] = ddsm_dbleadid;
                }

                //console.dir(proj_obj);

                var projCreate = null;
                projCreate = gen_H_createEntitySync(proj_obj, "ddsm_projectSet");
                //console.dir(projCreate);
                if (projCreate != null) {
                    createProjMSonAccount(projCreate.ddsm_projectId, projCreate.ddsm_ProjectTemplateId.Id, eval((projCreate.ddsm_StartDate).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))')), projCreate.ddsm_name, objMeasSiteGlobal);
                } else {
                    setTimeout(function () {
                        createProjMSonAccount(projCreate.ddsm_projectId, projCreate.ddsm_ProjectTemplateId.Id, eval((projCreate.ddsm_StartDate).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))')), projCreate.ddsm_name, objMeasSiteGlobal);
                    }, 500);
                }
                $('#overlay, #popup, #loading').hide();
                $('#overlay').remove();
                $('#popup').remove();
                $('#loading').remove();
            } else {
                $('#overlay, #popup, #loading').hide();
                $('#overlay').remove();
                $('#popup').remove();
                $('#loading').remove();
            }
        } else {
            $('#overlay, #popup, #loading').hide();
            $('#overlay').remove();
            $('#popup').remove();
            $('#loading').remove();
        }
    }
    catch (err) {
        alert("Error project template: " + err);
    }
}


function Fncl_UpdateFncls_onAccount(projId, CMS) {
    //console.log("projId: " + projId);
    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    //Query if this ID is already in use
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projectSet?"
    + "$select=ddsm_name,"
    + "ddsm_ddsm_project_ddsm_milestone/ddsm_TargetEnd,"
    + "ddsm_ddsm_project_ddsm_milestone/ddsm_EstimatedEnd,"
    + "ddsm_ddsm_project_ddsm_milestone/ddsm_ActualEnd,"
    + "ddsm_ddsm_project_ddsm_milestone/ddsm_Index,"
    + "ddsm_ddsm_project_ddsm_milestone/ddsm_name,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_financialId,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_initiatingmilestoneindex,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_name,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_status,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_stagename1,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_stagename2,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_stagename3,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_stagename4,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_stagename5,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_duration1,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_duration2,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_duration3,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_duration4,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_duration5,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_actualend1,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_actualend2,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_actualend3,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_actualend4,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_actualend5"
    + "&$filter=ddsm_projectId eq guid'" + projId + "'"
    + "&$expand=ddsm_ddsm_project_ddsm_milestone,ddsm_ddsm_project_ddsm_financial", false);

    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();

    if (req.readyState == 4 && req.status == 200) {
        var proj = JSON.parse(req.responseText).d;
        if (proj.results[0] != null) {
            var Fncls = proj.results[0].ddsm_ddsm_project_ddsm_financial.results;
            var MSs = proj.results[0].ddsm_ddsm_project_ddsm_milestone.results;
            //                console.dir(Fncls);
            //                console.dir(MSs);

            var initMS; //first this will be the starting index
            var initMSJson = -1; //This is the index of the Init MS in the JSON object
            var startDate = null;
            var thisStartDate = null;

            if (Fncls.length > 0) {
                for (var i in Fncls) {
                    //alert("Fncl Name: " + Fncls[i].ddsm_name);
                    initMS = Fncls[i].ddsm_initiatingmilestoneindex;
                    var actStart = null;
                    for (var j in MSs) {
                        //alert("MS Name: " + MSs[i].ddsm_name);
                        //console.log("initMS: " + initMS + "| MSs[" + j + "].ddsm_Index: " + MSs[j].ddsm_Index);
                        if (parseInt(initMS) == parseInt(MSs[j].ddsm_Index)) {
                            initMSJson = j;
                            //console.log(MSs[j].ddsm_ActualEnd);
                            if (MSs[j].ddsm_ActualEnd != null) {
                                //console.log(">>>> ddsm_ActualEnd");
                                //startDate = new Date(parseInt((MSs[j].ddsm_ActualEnd).substr(6)));
                                actStart = eval((MSs[j].ddsm_ActualEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                startDate = eval((MSs[j].ddsm_ActualEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                break;
                            } else if (MSs[j].ddsm_EstimatedEnd != null) {
                                //console.log(">>>> ddsm_EstimatedEnd");
                                //startDate = new Date(parseInt((MSs[j].ddsm_EstimatedEnd).substr(6)));
                                startDate = eval((MSs[j].ddsm_EstimatedEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                break;
                            } else if (MSs[j].ddsm_TargetEnd != null) {
                                //console.log(">>>> ddsm_TargetEnd");
                                //startDate = new Date(parseInt((MSs[j].ddsm_TargetEnd).substr(6)));
                                startDate = eval((MSs[j].ddsm_TargetEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                break;
                            } else {
                                alert("Error Updating Financial: " + Fncls[i].ddsm_name +
                                "\nInitiating MS Target Date is missing");
                                return;
                            }
                        }
                    }
                    if (initMSJson == -1) {
                        alert("Error Updating Financial:" + Fncls[i].ddsm_name +
                        "\nCannot find initiating milestone (index=" + initMS + ")");
                        continue;
                    }
                    if (typeof Fncls[i].ddsm_financialId != 'undefined') {
                        var fncl_obj = new Object();

                        if (Fncls[i].ddsm_stagename1 != null) {
                            if (Fncls[i].ddsm_initiatingmilestoneindex <= CMS && Fncls[i].ddsm_status.Value == 962080000 &&
                                actStart != null) { //only run if CMS is past Init MS AND Status == Not Started
                                fncl_obj.ddsm_status = new Object();
                                fncl_obj.ddsm_status.Value = 962080001; // setting status = Active
                                fncl_obj.ddsm_pendingstage = Fncls[i].ddsm_stagename1;
                                fncl_obj.ddsm_actualstart1 = actStart;
                            }
                            //console.log(startDate);
                            fncl_obj.ddsm_targetstart1 = new Date(startDate.getTime());
                            thisStartDate = new Date(startDate.getTime() + (Fncls[i].ddsm_duration1 * 86400000));
                            fncl_obj.ddsm_targetend1 = new Date(thisStartDate.getTime());
                        }
                        if (Fncls[i].ddsm_stagename2 != null) {
                            if (Fncls[i].ddsm_actualend1 != null) {
                                thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend1).substr(6)));
                                //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                            }
                            fncl_obj.ddsm_targetstart2 = new Date(thisStartDate.getTime());
                            thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration2 * 86400000));
                            fncl_obj.ddsm_targetend2 = new Date(thisStartDate.getTime());
                        }
                        if (Fncls[i].ddsm_stagename3 != null) {
                            if (Fncls[i].ddsm_actualend2 != null) {
                                thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend2).substr(6)));
                                //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                            }
                            fncl_obj.ddsm_targetstart3 = new Date(thisStartDate.getTime());
                            thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration3 * 86400000));
                            fncl_obj.ddsm_targetend3 = new Date(thisStartDate.getTime());
                        }
                        if (Fncls[i].ddsm_stagename4 != null) {
                            if (Fncls[i].ddsm_actualend3 != null) {
                                thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend3).substr(6)));
                                //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                            }
                            fncl_obj.ddsm_targetstart4 = new Date(thisStartDate.getTime());
                            thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration4 * 86400000));
                            fncl_obj.ddsm_targetend4 = new Date(thisStartDate.getTime());
                        }
                        if (Fncls[i].ddsm_stagename5 != null) {
                            if (Fncls[i].ddsm_actualend4 != null) {
                                thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend4).substr(6)));
                                //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                            }
                            fncl_obj.ddsm_targetstart5 = new Date(thisStartDate.getTime());
                            thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration5 * 86400000));
                            fncl_obj.ddsm_targetend5 = new Date(thisStartDate.getTime());
                        }
                        gen_H_updateEntitySync(Fncls[i].ddsm_financialId, fncl_obj, "ddsm_financialSet");
                        // Call function to update using ODATA call
                        //alert("It works!");
                    }
                }
            }
        } else {
            alert("No results found (query successful)");
        }
    } else {
        alert("Error: Fncl_UpdateFncls failed query.\n" + req.responseText);
        return false;
    }
    return true;
}

function UTCToLocalTime(d) {
    //var timeOffset = -((new Date()).getTimezoneOffset() / 60);
    //d.setHours(d.getHours() + timeOffset);
    return d;
}

function onLoadAccount_createNewProj(){
    /*
    var newPlusBtn = '<div class="ms-crm-Grid-ContextualButtonsContainer" id="Cycle_Projects_contextualButtonsContainer" style="padding-right: 36px;"><div class="ms-crm-contextButton"><a href="#" onclick="changeProjectPlus(null, null);" id="Cycle_Projects_addImageButton" style="display: block;" class="ms-crm-ImageStrip-addButton" title="Add Project record." tabindex="1920"><img src="/_imgs/imagestrips/transparent_spacer.gif" id="Cycle_Projects_addImageButtonImage" alt="Add Project record." title="Add Project record." class="ms-crm-add-button-icon"></a></div></div>';
    $("#Cycle_Projects_contextualButtonsContainer").hide();
    $("#Cycle_Projects_contextualButtonsContainer").parent().append(newPlusBtn);
    */
    accountProject.Id = Xrm.Page.data.entity.getId();
    accountProject.Name = Xrm.Page.getAttribute("name").getValue();
    accountProject.LogicalName = Xrm.Page.data.entity.getEntityName();

}