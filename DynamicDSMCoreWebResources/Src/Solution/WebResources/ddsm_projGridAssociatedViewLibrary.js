$("body").attr({"id": "bodyTop1"});

function OnClick(guids) {
    var ctxt;
    if (typeof GetGlobalContext != "undefined") {
        ctxt = GetGlobalContext();
    }
    else {
        if (typeof Xrm != "undefined") {
            ctxt = Xrm.Page.context;
        }
    }
    var sUrl = ctxt.getClientUrl();
    if (sUrl.match(/\/$/)) {
        sUrl = sUrl.substring(0, sUrl.length - 1);
    }
    var c_url = sUrl + '/WebResources/accentgold_/nextStepMilestone.html?id=' + guids;
    var w = 600, h = 400;
    if (typeof (openStdWin) == "undefined") {
        return window.open(c_url, '', "width=" + w + ",height=" + h + "," + "resizable=1");
    } else {
        Xrm.Utility.openWebResource('accentgold_/nextStepMilestone.html?id=' + guids, null, w, h);
    }
}



/**
 * script that provide opportunity to recalculate selected measure using ESP API
 * Created by Yurii on 03/21/16.
 */


function recalculateSelectedMeasure(grid) {

    var listOfIds = [];

    for (var i = 0; i < grid.length; i++) {
        listOfIds.push(grid[i].Id);
    }

    var callback = function () {
        Alert.show("Would you like to recalculate selected projects?", null,
            [{
                label: "Yes",
                callback: function () {
                    showPreloader(true);


                    console.log(JSON.stringify(listOfIds));

                    Process.callAction("ddsm_ESPRecalculateSmartMeasureFromProject",
                        [{
                            key: "Target",
                            type: Process.Type.EntityReference,
                            value: {id: grid[0], entityType: "ddsm_project"}
                        },
                            {
                                key: "UserInput",
                                type: Process.Type.String,
                                value: JSON.stringify(grid)
                            }

                        ],
                        function (params) {
                            // Success
                            showPreloader(false);
                            for (var i = 0; i < params.length; i++) {
                                console.log(params[i].key + "=" + params[i].value);
                            }
                            if (params[0].key === "Complete" && params[0].value !== "true") {

                                let resultobj = JSON.parse(params[1].value);
                                if (resultobj.hasOwnProperty("ErrorMsg")){
                                    Alert.show("Calculation is not complete",resultobj.ErrorMsg, null, "WARNING", 500, 200);
                                }
                            }
                            else {
                                //Alert.show("Calculation is complete",  null, null, "SUCCESS", 500, 200);
                                let msg = "Calculation is complete";
                                let resultobj = JSON.parse(params[1].value);
                                if(!!resultobj.completecount){
                                    msg += AGS.String.format(".\n{0} records have been updated", resultobj.completecount);
                                }
                                Alert.show(msg,  null, null, "SUCCESS", 500, 200);

                            }
                        },
                        function (e) {
                            // Error
                            showPreloader(false);
                            console.log(e);
                            Alert.show("Error",e, null, "ERROR", 500, 200);
                            alert("Error: " + e);
                        }
                    );
                }
            },
                {
                    label: "No"
                }], "QUESTION", 500, 200);

    };

    //var mainCallBack =  function(){
    //
    //    var processJs = function () {
    //        LoadScript("../WebResources/mag_/js/process.js", callback);
    //    };
    //    LoadScript("../WebResources/mag_/js/alert.js", processJs);
    //}
    //
    //LoadScript("../WebResources/accentgold_/Script/clab.spinner.js", mainCallBack);

    var getTargetDates = function () {
        var dateMin, dateMax;

        var filter = [[
            ["ddsm_projecttomeasureid", 'in', listOfIds.join(',').replace(/[{}]/g,'')],
        ]];

        //fields for query
        var measFields = [
            ['ddsm_esptargetdate', 'min', 'dateMin'],
            ['ddsm_esptargetdate', 'max', 'dateMax'],
        ];
        var DataAggr = AGS.Fetch.aggregateFXML('ddsm_measure', measFields, filter);
        if(!!DataAggr){

            if(!!DataAggr.attributes["ddsm_esptargetdate"] && !!DataAggr.attributes["ddsm_esptargetdate"].value){
                dateMin = AGS.Date.getShortDateFormat(DataAggr.attributes["ddsm_esptargetdate"].value)
            }
            if(!!DataAggr.attributes["dateMax"] && !!DataAggr.attributes["dateMax"].value){
                dateMax = AGS.Date.getShortDateFormat(DataAggr.attributes["dateMax"].value)
            }

        }
        callback(dateMin, dateMax);
    };
    var processJs = function () {
        LoadScript("../WebResources/mag_/js/process.js", getTargetDates);
    };
    var alert_load = function(){
        LoadScript("../WebResources/mag_/js/alert.js", processJs);
    };
    var agsFetch_load = function () {
        LoadScript("../WebResources/accentgold_/Script/ags.fetch.js", alert_load);
    };
    var serviceToolkit_load = function(){
        LoadScript("../WebResources/accentgold_/EditableGrid/js/XrmServiceToolkit.min.js", agsFetch_load);
    };
    var agsCore_load =  function(){
        showPreloader(true);
        LoadScript("../WebResources/accentgold_/Script/ags.core.js", serviceToolkit_load);
    };

    LoadScript("../WebResources/accentgold_/Script/clab.spinner.js", agsCore_load);


}

function showPreloader(show) {
    if (show) {
// show overlay
        spinnerForm = CreaLab.Spinner.spin(document.getElementById("bodyTop1"), "Loading data");
    }
    else {
        spinnerForm.stop();
    }
}

function LoadScript(url, callback) {
    // Adding the script tag to the head as suggested before
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

// Then bind the event to the callback function.
// There are several events for cross browser compatibility.
    script.onreadystatechange = callback;
    script.onload = callback;

// Fire the loading
    head.appendChild(script);
}
