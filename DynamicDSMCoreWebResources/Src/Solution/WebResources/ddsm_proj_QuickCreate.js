//create new project (Quick Create Form)
// create 05/04/2015 Sergey
// update 05/25/2016

var IndexOffsetDocList = [];
var ddsm_name_NewProject = "";
var ddsm_StartDate_NewProject = new Date();
var objMeasQuickForm = [], objMeasQuickForm_length = 0, objMeasQuickForm_length2 = 0, objMeasQuickForm_count = 0;
var accountProject = {}, siteProject = {}, taProject = {},  taContactProject = {}, _targetSpinner, spinnerForm, isOpportunity = false, OpptyProjTplList = [];
window.createdMesId = [];
$(document).ready(function () {
    $("body").attr({"id": "bodyTop1"});
    _targetSpinner = document.getElementById("bodyTop1");
});

function onLoad_QuickForm(){

    //Disabled Button "New" lookup modal form
    Xrm.Page.getControl("ddsm_parentsiteid").setParameter("ShowNewButton", "0");

    var qsp = Xrm.Page.context.getQueryStringParameters();
    isOpportunity = !!qsp.is_opportunity && Boolean.parse(qsp.is_opportunity);

    Xrm.Page.getAttribute("ddsm_startdate").setValue(new Date(dateToMDY(new Date())));

    Xrm.Page.getAttribute("ddsm_tradeallycontact").setValue(null);
    Xrm.Page.getAttribute("ddsm_tradeally").setValue(null);

    if(window.parent.top.leadId)
    {
        Xrm.Page.getAttribute("ddsm_tradeally").setValue(null);
    }

//
    if(($("#globalquickcreate_save_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document)).length == 1) {
        $("#globalquickcreate_save_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document).remove();
        var parentConteyner = $("#globalquickcreate_cancel_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document).parent();
        var cancelBtn = $("#globalquickcreate_cancel_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document);
        $("#globalquickcreate_cancel_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document).remove();
        $(parentConteyner).append('<button id="project_quickcreate_button" class="mscrm-globalqc-actionbutton" onclick="document.getElementById(\'Cycle_Projects_gridcontrol_quickcreate\').contentWindow.createNewProject();">Create</button>');
        $(parentConteyner).append(cancelBtn);
        $("#project_quickcreate_button", window.parent.document).attr('disabled', 'disabled');
    } else if(($("#globalquickcreate_save_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document)).length == 1) {
        $("#globalquickcreate_save_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document).remove();
        var parentConteyner = $("#globalquickcreate_cancel_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document).parent();
        var cancelBtn = $("#globalquickcreate_cancel_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document);
        $("#globalquickcreate_cancel_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document).remove();
        $(parentConteyner).append('<button id="project_quickcreate_button" class="mscrm-globalqc-actionbutton" onclick="document.getElementById(\'crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate\').contentWindow.createNewProject();">Create</button>');
        $(parentConteyner).append(cancelBtn);
        $("#project_quickcreate_button", window.parent.document).attr('disabled', 'disabled');
    } else if(($("#globalquickcreate_save_button_NavBarGloablQuickCreate", window.parent.document)).length == 1) {
        $("#globalquickcreate_save_button_NavBarGloablQuickCreate", window.parent.document).remove();
        var parentConteyner = $("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document).parent();
        var cancelBtn = $("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document);
        $("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document).remove();
        $(parentConteyner).append('<button id="project_quickcreate_button" class="mscrm-globalqc-actionbutton" onclick="document.getElementById(\'NavBarGloablQuickCreate\').contentWindow.createNewProject();">Create</button>');
        $(parentConteyner).append(cancelBtn);
        $("#project_quickcreate_button", window.parent.document).attr('disabled', 'disabled');
    }

    if(isOpportunity){
        $('.mscrm-globalqc-entityname').text('Opportunity');
        setProjTplFilterByOppty();
    }

    if (Xrm.Page.getAttribute("ddsm_accountid").getValue() == null) {
        if (Xrm.Page.getAttribute("ddsm_parentsiteid").getValue() == null) {
            accountProject = {};
            $("#project_quickcreate_button", window.parent.document).attr('disabled', 'disabled');
        } else {
            var siteId = Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id;
            var serverUrl = Xrm.Page.context.getClientUrl();
            var req = new XMLHttpRequest();
            req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_siteSet?"
            + "$select="
            + "ddsm_parentaccount"
            + "&$filter=ddsm_siteId eq guid'" + siteId + "'", false);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.send();

            if (req.readyState == 4 && req.status == 200) {
                var _data = JSON.parse(req.responseText).d;
                if (_data.results[0] != null) {
                    var value = new Array();
                    value[0] = new Object();
                    value[0].entityType = _data.results[0].ddsm_parentaccount.LogicalName;
                    value[0].id = _data.results[0].ddsm_parentaccount.Id;
                    value[0].name = _data.results[0].ddsm_parentaccount.Name;
                    Xrm.Page.getAttribute("ddsm_accountid").setValue(value);
                    accountProject.Id = _data.results[0].ddsm_parentaccount.Id;
                    accountProject.LogicalName = _data.results[0].ddsm_parentaccount.LogicalName;
                    accountProject.Name = _data.results[0].ddsm_parentaccount.Name;
                    enableCreateButton();
                }
            }
        }
    } else {
        accountProject.Id = Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id;
        accountProject.LogicalName = Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].entityType;
        accountProject.Name = Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].name;
        enableCreateButton();
    }
    if (Xrm.Page.getAttribute("ddsm_parentsiteid").getValue() == null) {
        if (Xrm.Page.getAttribute("ddsm_accountid").getValue() == null) {
            siteProject = {};
            $("#project_quickcreate_button", window.parent.document).attr('disabled','disabled');
        } else {
            var accId = Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id;
            var serverUrl = Xrm.Page.context.getClientUrl();
            var req = new XMLHttpRequest();
            req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_siteSet?"
            + "$select="
            + "ddsm_siteId,"
            + "ddsm_name"
            + "&$filter=ddsm_parentaccount/Id eq guid'" + accId + "'", false);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.send();

            if (req.readyState == 4 && req.status == 200) {
                var _data = JSON.parse(req.responseText).d;
                if (_data.results != null && _data.results.length == 1) {
                    var value = new Array();
                    value[0] = new Object();
                    value[0].entityType = "ddsm_site";
                    value[0].id = _data.results[0].ddsm_siteId;
                    value[0].name = _data.results[0].ddsm_name;
                    Xrm.Page.getAttribute("ddsm_parentsiteid").setValue(value);
                    siteProject.Id = _data.results[0].ddsm_siteId;
                    siteProject.LogicalName = "ddsm_site";
                    siteProject.Name = _data.results[0].ddsm_name;
                    enableCreateButton();

                }
            }
        }
    } else {
        siteProject.Id = Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id;
        siteProject.LogicalName = Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].entityType;
        siteProject.Name = Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].name;
        enableCreateButton();
    }
}


function proj_TradeAllyContact_onChange() {
    if (Xrm.Page.getAttribute("ddsm_tradeallycontact").getValue() == null) {
        Xrm.Page.getAttribute("ddsm_tradeally").setValue(null);
        taContactProject = {};
        taProject = {};
        return;
    } else {
        taContactProject.Id = Xrm.Page.getAttribute("ddsm_tradeallycontact").getValue()[0].id;
        taContactProject.LogicalName = Xrm.Page.getAttribute("ddsm_tradeallycontact").getValue()[0].entityType;
        taContactProject.Name = Xrm.Page.getAttribute("ddsm_tradeallycontact").getValue()[0].name;
    }
    var contactId = Xrm.Page.getAttribute("ddsm_tradeallycontact").getValue()[0].id;
    //console.log("contactId: " + contactId);
    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();

    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ContactSet?"
    + "$select="
    + "ParentCustomerId"
    + "&$filter=ContactId eq guid'" + contactId + "' and StateCode/Value eq 0", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();

    if (req.readyState == 4 && req.status == 200) {
        //From the project template - will also contain milestone information
        var _data = JSON.parse(req.responseText).d;
        if (_data.results[0] != null) {
            var contactData = _data.results[0];
            var value = new Array();
            value[0] = new Object();
            value[0].entityType = contactData.ParentCustomerId.LogicalName;
            value[0].id = contactData.ParentCustomerId.Id;
            value[0].name = contactData.ParentCustomerId.Name;
            Xrm.Page.getAttribute("ddsm_tradeally").setValue(value);

            taProject.Id = contactData.ParentCustomerId.Id;
            taProject.LogicalName = contactData.ParentCustomerId.LogicalName;
            taProject.Name = contactData.ParentCustomerId.Name;

        }
    }
}

function proj_startDateValidChk() {
    var curDate = new Date();
    var wday = curDate.getTime();
    if (Xrm.Page.getAttribute("ddsm_startdate").getValue() == null) {
        return;
    }
    var slctDate = Xrm.Page.getAttribute("ddsm_startdate").getValue().getTime();
    if (slctDate > wday) {
        alert("Invalid date. Date must not be greater than today");
        Xrm.Page.getAttribute("ddsm_startdate").setValue(null);
        //Xrm.Page.ui.controls.get("ddsm_startdate").setDisabled(false);
    } else {
        Xrm.Page.ui.controls.get("ddsm_startdate").setDisabled(true);
        //Xrm.Page.ui.controls.get("ddsm_projecttemplateid").setDisabled(false);

        StartDateOnChange();
    }
}

function StartDateOnChange() {
    if (Xrm.Page.getAttribute("ddsm_startdate").getValue() != null) {
        var objecttypes = Xrm.Internal.getEntityCode("ddsm_projecttemplate");
        var lookupURI = "/_controls/lookup/lookupinfo.aspx";
        lookupURI += "?LookupStyle=single";
        lookupURI += "&objecttypes=" + objecttypes;
        lookupURI += "&ShowNewButton=0";
        lookupURI += "&ShowPropButton=1";
        lookupURI += "&browse=false";
        lookupURI += "&AllowFilterOff=0";
        lookupURI += "&DefaultType=" + objecttypes;
        lookupURI += "&DisableQuickFind=0";
        lookupURI += "&DisableViewPicker=0";
        var DialogOption = new Xrm.DialogOptions;
        DialogOption.width = 550; DialogOption.height = 550;
        Xrm.Internal.openDialog(lookupURI, DialogOption, null, null, Callback_lkp_popup_ProjTpl);
    }
}
function Callback_lkp_popup_ProjTpl(lkp_popup) {
    try {
        if (lkp_popup) {
            if (lkp_popup.items) {
                var lookupValue = new Array();
                lookupValue[0] = new Object();
                lookupValue[0].id = lkp_popup.items[0].id;
                lookupValue[0].name = lkp_popup.items[0].name;
                lookupValue[0].entityType = lkp_popup.items[0].typename;
                Xrm.Page.getAttribute('ddsm_projecttemplateid').setValue(lookupValue);
                enableCreateButton();
            }
        }
    }
    catch (err) {
        alert(err);
    }
}
function proj_parentSite_onChange() {
    if (Xrm.Page.getAttribute("ddsm_parentsiteid").getValue() == null) {
        siteProject = {};
        $("#project_quickcreate_button", window.parent.document).attr('disabled','disabled');
    } else {
        siteProject.Id = Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id;
        siteProject.LogicalName = Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].entityType;
        siteProject.Name = Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].name;
        enableCreateButton();
    }
}
function proj_parentAccount_onChange() {
    if (Xrm.Page.getAttribute("ddsm_accountid").getValue() == null) {
        accountProject = {};
        $("#project_quickcreate_button", window.parent.document).attr('disabled','disabled');
    } else {
        accountProject.Id = Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id;
        accountProject.LogicalName = Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].entityType;
        accountProject.Name = Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].name;
        enableCreateButton();
    }
}
function enableCreateButton(){
    if(Xrm.Page.getAttribute("ddsm_accountid").getValue() != null && Xrm.Page.getAttribute("ddsm_parentsiteid").getValue() != null && Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue() != null) {
        $("#project_quickcreate_button", window.parent.document).removeAttr('disabled');
    }

}

function EligibilityCreatingProject(_account, _site, _programOffering){

    var output = false;

    let params = [{
        key: "Account",
        type: Process.Type.EntityReference,
        value: {
            id: _account.id,
            entityType: _account.entityType
        }
    },{
        key: "Site",
        type: Process.Type.EntityReference,
        value: {
            id: _site.id,
            entityType: _site.entityType
        }
    },{
        key: "ProgramOffering",
        type: Process.Type.EntityReference,
        value: {
            id: _programOffering.id,
            entityType: _programOffering.entityType
        }
    }
    ];

    Process.callAction("ddsm_EligibilityCreatingProject", params,
        function(params) {
            let rs = JSON.parse(params[0].value);
            if(rs)
                output = true;
            else
                output = false;
        },
        function(e) {
            console.log("error: " + e);
            output = true;
        },
        false
    );

    return output;
}

function ManuallyCreatingProject(ParentAccount, ParentSite, ProgramOffering, ProjectTemplate, StartDate, PartnerContact, PartnerCompany) {
    console.log("start ManuallyCreatingProject");
    var startDateUTC = new Date(StartDate);
    startDateUTC = dateToMDYLocal(startDateUTC);
debugger;
    let params = [{
        key: "ParentAccount",
        type: Process.Type.String,
        value: ParentAccount
    },
        {
            key: "ParentSite",
            type: Process.Type.String,
            value: ParentSite
        },
        {
            key: "ProgramOffering",
            type: Process.Type.String,
            value: ProgramOffering
        },
        {
            key: "ProjectTemplate",
            type: Process.Type.String,
            value: ProjectTemplate
        },
        {
            key: "StartDate",
            type: Process.Type.String,
            value: startDateUTC
        },
        {
            key: "PartnerContact",
            type: Process.Type.String,
            value: PartnerContact
        },
        {
            key: "PartnerCompany",
            type: Process.Type.String,
            value: PartnerCompany
        }
    ];

    console.dir(params);
    Process.callAction("ddsm_DDSMManuallyCreatingProject", params,
        function(params) {
            let _Comlete = false;
            let _ProjectId = '{00000000-0000-0000-0000-000000000000}';
            debugger;
            console.dir(params);
            for (let i = 0; i < params.length; i++) {
                if (params[i].key == "Complete") _Comlete = params[i].value;
                if (params[i].key == "ProjectId") _ProjectId = params[i].value;
            }

            if (_Comlete !="false")
            {

                setTimeout(function(){
                    spinnerForm.stop();

                    var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                    var features = "location=yes,menubar=no,status=yes,toolbar=yes,resizable=yes";
                    var url = AGS.getServerUrl() + "/main.aspx?etn=ddsm_project&extraqs=&histKey=" + randomnumber + "&id=" + _ProjectId + "&newWindow=true&pagetype=entityrecord";
                    window.open(url, "_blank");

                    if(($("#globalquickcreate_cancel_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document)).length == 1) {
                        $("#globalquickcreate_cancel_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document).click();
                    }
                    if(($("#globalquickcreate_cancel_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document)).length == 1) {
                        $("#globalquickcreate_cancel_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document).click();
                    }
                    if(($("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document)).length == 1) {
                        $("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document).click();
                    }
                }, 3000);

            }
            else
            {
                spinnerForm.stop();
                Alert.show("The project has not been created.",
                    null,
                    [{
                        label: "OK",
                        callback: function () {

                            if(($("#globalquickcreate_cancel_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document)).length == 1) {
                                $("#globalquickcreate_cancel_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document).click();
                            }
                            if(($("#globalquickcreate_cancel_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document)).length == 1) {
                                $("#globalquickcreate_cancel_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document).click();
                            }
                            if(($("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document)).length == 1) {
                                $("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document).click();
                            }

                        }
                    }],
                    "ERROR", 500, 200);

            }
        },
        function(e) {
            console.log("error: " + e);
            spinnerForm.stop();
            Alert.show(e,
                null,
                [{
                    label: "OK",
                    callback: function () {

                        if(($("#globalquickcreate_cancel_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document)).length == 1) {
                            $("#globalquickcreate_cancel_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document).click();
                        }
                        if(($("#globalquickcreate_cancel_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document)).length == 1) {
                            $("#globalquickcreate_cancel_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document).click();
                        }
                        if(($("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document)).length == 1) {
                            $("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document).click();
                        }

                    }
                }],
                "ERROR", 500, 200);
        }
    );

    return true;

}

function createNewProject(){
    //1) Check on Parent Site fields "Rate Class" or the Parent Site field "Rate Code". It should be not blank
    //2) Check on Parent Site fields "Rate Code Name"(lookup)
    var ParentSite = Xrm.Page.getAttribute("ddsm_parentsiteid").getValue();
    if (ParentSite)
    {
        var fsParentSite= 'ddsm_RateCode, ddsm_RateClass, ddsm_RateCodeNameId';
        var ParentSiteSel = AGS.REST.retrieveRecord(ParentSite[0].id, 'ddsm_site', fsParentSite, null, null, null, false);
    }
    //Lokup Name of "Rate Code Name"
    //ParentSiteSel.ddsm_RateCodeNameId.Name
    //if Commercial Unknown  - create without measure and popp after creating

    /*
    if(!EligibilityCreatingProject(Xrm.Page.getAttribute("ddsm_accountid").getValue()[0], Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0], Xrm.Page.getAttribute("ddsm_programoffering").getValue()[0])) {
        Alert.show("This project in not eligible for the project offering", null, [{
            label: "Ok",
            callback: function () {
            }
        }], "WARNING", 500, 200);

        return;
    }
    */

    if (ParentSiteSel.ddsm_RateClass.Value && ParentSiteSel.ddsm_RateCode.Value && ParentSiteSel.ddsm_RateCodeNameId.Name) {
//    var popupHTML = '<div id="overlay" style="display:none; position:fixed; z-index:998; opacity:0.5; filter:alpha(opacity=50); top:0; right:0; left:0; bottom:0; background:#000000 url(/WebResources/ddsm_loading.gif) 50% 50% no-repeat;"></div>';
        //var popupHTML = '<div id="overlay" style="display:none; position:fixed; z-index:998; opacity:0.5; filter:alpha(opacity=50); top:0; right:0; left:0; bottom:0;background:#000000;"></div>';
        //$('body').append(popupHTML);
        //$('#overlay').show();
        spinnerForm = window.top.CreaLab.Spinner.spin(_targetSpinner);

        debugger;

        ManuallyCreatingProject(
            (Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id).replace(/\{|\}/g, ''),
            (Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id).replace(/\{|\}/g, ''),
            (Xrm.Page.getAttribute("ddsm_programoffering").getValue()[0].id).replace(/\{|\}/g, ''),
            (Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue()[0].id).replace(/\{|\}/g, ''),
            Xrm.Page.getAttribute("ddsm_startdate").getValue(),
            ((!!Xrm.Page.getAttribute("ddsm_tradeallycontact") && !!Xrm.Page.getAttribute("ddsm_tradeallycontact").getValue() && !!Xrm.Page.getAttribute("ddsm_tradeallycontact").getValue()[0].id)?(Xrm.Page.getAttribute("ddsm_tradeallycontact").getValue()[0].id).replace(/\{|\}/g, ''):""),
            ((!!Xrm.Page.getAttribute("ddsm_tradeally") && !!Xrm.Page.getAttribute("ddsm_tradeally").getValue() && !!Xrm.Page.getAttribute("ddsm_tradeally").getValue()[0].id)?(Xrm.Page.getAttribute("ddsm_tradeally").getValue()[0].id).replace(/\{|\}/g, ''):""))


        /*
                setTimeout(function () {
                    ddsm_StartDate_NewProject = Xrm.Page.getAttribute("ddsm_startdate").getValue();

                    //ddsm_name_NewProject = Proj_GenerateProjID();

                    setMeasFieldsFromSite(siteProject.Id);

                    //Add Measure
                    //Check Lokkup RateCodeName on Site for creating Project with or without Measures.
                    if (ParentSiteSel.ddsm_RateCodeNameId.Name.indexOf("Commercial Unknown") < 0) {  //if (ParentSiteSel.ddsm_RateCodeNameId.Name != "Commercial Unknown")
                        objMeasQuickForm = [];

                        for (var i = 1; i <= 5; i++) {
                            if (Xrm.Page.getAttribute("ddsm_measuretemplate" + i).getValue() != null) {
                                var obj = {};
                                obj.MeasTpl = {
                                    Id: Xrm.Page.getAttribute("ddsm_measuretemplate" + i).getValue()[0].id,
                                    Name: Xrm.Page.getAttribute("ddsm_measuretemplate" + i).getValue()[0].name,
                                    LogicalName: Xrm.Page.getAttribute("ddsm_measuretemplate" + i).getValue()[0].entityType
                                };
                                if (Xrm.Page.getAttribute("ddsm_measurequantity" + i).getValue() != null && Xrm.Page.getAttribute("ddsm_measurequantity" + i).getValue() != "") {
                                    obj.Quantity = Xrm.Page.getAttribute("ddsm_measurequantity" + i).getValue();
                                } else {
                                    obj.Quantity = 0.0;
                                }
                                objMeasQuickForm.push(obj);
                            }
                        }

                        /// added by Nazarenko Y. 05.10.2016
                        //get measure tempaltes from
                        if(Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue() )
                        var projTpl = Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue()[0].id.slice(1,-1);
                        getMTRelations(projTpl,objMeasQuickForm);

                        console.dir(objMeasQuickForm);
                        if (objMeasQuickForm.length != 0) {
                            objMeasQuickForm_length = objMeasQuickForm.length;
                            objMeasQuickForm_length2 = objMeasQuickForm.length;
                            objMeasQuickForm_count = 0;
                        } else {
                            objMeasQuickForm_length = 0;
                            objMeasQuickForm_length2 = 0;
                            objMeasQuickForm_count = 0;
                        }
                    }


                    var proj_obj = {};
                    proj_obj['ddsm_CreatorRecordType'] = {Value: 962080000};

                    var serverUrl = Xrm.Page.context.getClientUrl();
                    var req = new XMLHttpRequest();
                    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projecttemplateSet?"
                             + "$select="
                             + "ddsm_ProgramCode"
                        + "&$filter=ddsm_projecttemplateId eq guid'" + Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue()[0].id + "' and statecode/Value eq 0", false);
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.send();
                    if (req.readyState == 4 && req.status == 200) {
                        var _tplData = JSON.parse(req.responseText).d;
                        if (_tplData.results[0] != null) {
                            console.dir(_tplData.results[0]);
                            for (var x = 0; x < _tplData.results.length; x++) {
                                proj_obj['ddsm_name'] = _tplData.results[x].ddsm_ProgramCode + "-" + accountProject.Name;
                            }
                        }
                    }
        //                        proj_obj['ddsm_name'] = ddsm_name_NewProject;

                    var programoffering = {};
                    programoffering.Id = Xrm.Page.getAttribute("ddsm_programoffering").getValue()[0].id;
                    programoffering.Name = Xrm.Page.getAttribute("ddsm_programoffering").getValue()[0].name;
                    programoffering.LogicalName = Xrm.Page.getAttribute("ddsm_programoffering").getValue()[0].entityType;
                    proj_obj['ddsm_ProgramOffering'] = programoffering;

                    //add program
                    AGS.REST.retrieveRecord(programoffering.Id, "ddsm_programoffering", "ddsm_ProgramId,ddsm_ProgramManagerId", null, function(data){
                        proj_obj["ddsm_ProgramId"] = data.ddsm_ProgramId;
                        proj_obj["ddsm_ProgramManagerId"] = data.ddsm_ProgramManagerId;
                    }, function(msg){alert(msg);}, false);

                    var projecttemplate = {};
                    projecttemplate.Id = Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue()[0].id;
                    projecttemplate.Name = Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue()[0].name;
                    projecttemplate.LogicalName = Xrm.Page.getAttribute("ddsm_projecttemplateid").getValue()[0].entityType;
                    proj_obj['ddsm_ProjectTemplateId'] = projecttemplate;

                    proj_obj['ddsm_StartDate'] = ddsm_StartDate_NewProject;

                    proj_obj['ddsm_ParentSiteId'] = siteProject;

                    if (typeof taProject.Id !== 'undefined') {
                        proj_obj['ddsm_TradeAlly'] = taProject;
                    }
                    if (typeof taContactProject.Id !== 'undefined') {
                        proj_obj['ddsm_TradeAllyContact'] = taContactProject;
                    }
                    if (!!Xrm.Page.getAttribute("ddsm_recordtype")) {
                        console.dir(Xrm.Page.getAttribute("ddsm_recordtype"));
                    proj_obj['ddsm_RecordType'] = {Value: Xrm.Page.getAttribute("ddsm_recordtype").getValue()};
                }
                    if(isOpportunity){
                        proj_obj['ddsm_RecordType'] = {Value: 962080000};
                    }

                    var msReq = new XMLHttpRequest();
                    var serverUrl = Xrm.Page.context.getClientUrl();
                    var URL = serverUrl + "/xrmservices/2011/OrganizationData.svc/AccountSet?"
                        + "$select="
                        + "TransactionCurrencyId,"
                        + "ddsm_bdlead"
                        + "&$filter=AccountId eq guid'" + accountProject.Id + "'";
                    msReq.open("GET", URL, false);
                    msReq.setRequestHeader("Accept", "application/json");
                    msReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    msReq.send();
                    if (msReq.readyState == 4 && msReq.status == 200) {
                        var _msData = JSON.parse(msReq.responseText).d;
                        if (_msData.results[0] != null) {
                            proj_obj['TransactionCurrencyId'] = _msData.results[0].TransactionCurrencyId;
                            proj_obj['ddsm_BDLeadId'] = _msData.results[0].ddsm_bdlead;
                        }
                    }

                    proj_obj['ddsm_AccountId'] = accountProject;

                    URL = serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_siteSet?"
                        + "$select="
                        + "ddsm_account"
                        + "&$filter=ddsm_siteId eq guid'" + siteProject.Id + "'";
                    msReq.open("GET", URL, false);
                    msReq.setRequestHeader("Accept", "application/json");
                    msReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    msReq.send();
                    if (msReq.readyState == 4 && msReq.status == 200) {
                        var _msData = JSON.parse(msReq.responseText).d;
                        if (_msData.results[0] != null) {
                            if (_msData.results[0].ddsm_account != null) {
                                proj_obj['ddsm_AccountNumber'] = _msData.results[0].ddsm_account;
                            }
                        }
                    }

                    var projCreate = null;
                    projCreate = gen_H_createEntitySync(proj_obj, "ddsm_projectSet");
                    if (!!projCreate) {
                        if(projCreate.hasOwnProperty('error')){
                            Alert.show(projCreate.error.message.value,
                                null,
                                [{
                                    label: "OK",
                                    callback: function () {
                                        $('#overlay').remove();
                                        $("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document).click();
                                    }
                                }],
                                "WARNING", 500, 200);
                            return;
                        }
                        createProjMS_QuickForm(projCreate.ddsm_projectId, projCreate.ddsm_ProjectTemplateId.Id, eval((projCreate.ddsm_StartDate).replace(/\/Date\((\d+)\)\//gi, 'proj_TradeAllyContact_onChange(new Date($1))')), projCreate.ddsm_name);
                    } else {
                        setTimeout(function () {
                            createProjMS_QuickForm(projCreate.ddsm_projectId, projCreate.ddsm_ProjectTemplateId.Id, eval((projCreate.ddsm_StartDate).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))')), projCreate.ddsm_name);
                        }, 500);
                    }
                }, 500);
                */

        //Check Lokkup RateCodeName on Site for alert about Project without Measures.
        if (ParentSiteSel.ddsm_RateCodeNameId.Name.indexOf("Commercial Unknown") >= 0) {  //if (ParentSiteSel.ddsm_RateCodeNameId.Name == "Commercial Unknown")
            alert('The Project was created but Measure(s) were not able to be created due to unknown Rate Code. You must first update the Rate Code on the Site prior to adding a Measure.');
        }
    }
    else{alert('Project Creation Failed. Unfortunately the Rate Class or Rate Code are missing on the Parent Site. In order to create a project, these fields must be populated.');}
}


function getMTRelations(tplId,objMeasQuickForm){
    var measureTplMap = {};
    var sel = "?$select=ddsm_MeasureTemplateId,ddsm_Units"
        + "&$filter=ddsm_ProjectTemplateId/Id eq guid'" + tplId + "'";

    //ddsm_measuretemplateid,ddsm_MeasureTemplateId
    //ddsm_projecttemplateid,ddsm_ProjectTemplateId
    //ddsm_units,ddsm_Units

    AGS.REST.retrieveMultipleRecords("ddsm_measuretemplaterelation", sel, function(data){
        for (var i = 0; i < data.length; i++) {
            //var el = data[i];
            var obj = {};
            obj.MeasTpl = {
                Id: '{'+data[i].ddsm_MeasureTemplateId.Id +'}',
                Name: data[i].ddsm_MeasureTemplateId.Name,
                LogicalName: data[i].ddsm_MeasureTemplateId.LogicalName
            };
            obj.Quantity = data[i].ddsm_Units;

            objMeasQuickForm.push(obj);
          //  measureTplMap[data[i].ddsm_SecondaryFieldLogicalName] = data[i].ddsm_PrimaryFieldLogicalName;
        }
    }, null, null, false, null) ;
}



function getInitialLeadSite(ID) {
    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_siteSet?"
    + "$select=ddsm_InitialLead"
    + "&$filter=ddsm_siteId eq guid'" + ID + "'", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();
    if (req.readyState == 4 && req.status == 200) {
        var _tplData = JSON.parse(req.responseText).d;
        //console.dir(_tplData.results[0].ddsm_InitialLead);
        return _tplData.results[0].ddsm_InitialLead;
    } else return null;
}

function createDocList_ptojTpl(proj_ID, proj_Name, projTpl_ID){
    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
    + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
    + "&$filter=ddsm_ProjectTemplateId/Id eq guid'" + projTpl_ID + "'", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();
    if (req.readyState == 4 && req.status == 200) {
        var _tplData = JSON.parse(req.responseText).d;
        if (_tplData.results[0] != null) {
            for (var x = 0; x < _tplData.results.length; x++) {
                var proj_obj = {};
                if (_tplData.results[x].ddsm_RequiredByStatus != null) {
                    proj_obj["ddsm_RequiredByStatus"] = _tplData.results[x].ddsm_RequiredByStatus;
                }
                if (_tplData.results[x].ddsm_name != null) {
                    proj_obj["ddsm_name"] = _tplData.results[x].ddsm_name;
                }
                if (_tplData.results[x].ddsm_Uploaded != null) {
                    proj_obj["ddsm_Uploaded"] = _tplData.results[x].ddsm_Uploaded;
                }

                proj_obj.ddsm_ProjectId = new Object();
                proj_obj.ddsm_ProjectId.Id = proj_ID;
                proj_obj.ddsm_ProjectId.Name = proj_Name;
                proj_obj.ddsm_ProjectId.LogicalName = "ddsm_project";

                //console.dir(proj_obj);

                gen_H_createEntitySync(proj_obj, "ddsm_documentconventionSet");
            }
        }
    }
}
function createDocList_measTpl(proj_ID, proj_Name, measTpl_ID){
console.log(measTpl_ID);
    var docProjList = null, measTpl_ID = measTpl_ID;
    var _req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    _req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
    + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
    + "&$filter=ddsm_ProjectId/Id eq guid'" + proj_ID + "'", false);
    _req.setRequestHeader("Accept", "application/json");
    _req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    _req.send();
    if (_req.readyState == 4 && _req.status == 200) {
        var _tplData = JSON.parse(_req.responseText).d;
        if (_tplData.results[0] != null) {
            docProjList = _tplData.results;
        }
    }

    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
    + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
    + "&$filter=ddsm_MeasureTemplateId/Id eq guid'" + measTpl_ID + "'", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();
    if (req.readyState == 4 && req.status == 200) {
        var _tplData = JSON.parse(req.responseText).d;
        if (_tplData.results[0] != null) {
            for (var x = 0; x < _tplData.results.length; x++) {
                var addDocConv = true;
                if(docProjList !== null){
                    for(var i =0; i < docProjList.length; i++){
                        if(_tplData.results[x].ddsm_name == docProjList[i].ddsm_name) {addDocConv = false;break;}
                    }
                }
                if(addDocConv){
                    var proj_obj = {};
                    if (_tplData.results[x].ddsm_RequiredByStatus != null) {
                        proj_obj["ddsm_RequiredByStatus"] = _tplData.results[x].ddsm_RequiredByStatus;
                    }
                    if (_tplData.results[x].ddsm_name != null) {
                        proj_obj["ddsm_name"] = _tplData.results[x].ddsm_name;
                    }
                    if (_tplData.results[x].ddsm_Uploaded != null) {
                        proj_obj["ddsm_Uploaded"] = _tplData.results[x].ddsm_Uploaded;
                    }

                    proj_obj.ddsm_ProjectId = new Object();
                    proj_obj.ddsm_ProjectId.Id = proj_ID;
                    proj_obj.ddsm_ProjectId.Name = proj_Name;
                    proj_obj.ddsm_ProjectId.LogicalName = "ddsm_project";

                    //console.dir(proj_obj);

                    gen_H_createEntitySync(proj_obj, "ddsm_documentconventionSet");
                }
            }
        }
    }
}

function createProjMeas_ProjTplMeas(projTpl_ID, proj_Obj){

    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    try {
        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projecttemplatemeasureSet?"
        + "$select="
            + "ddsm_projecttemplatemeasureId,"
            + "ddsm_name,"
            + "ddsm_MeasureSelector,"
            + "ddsm_phase1units,"
            + "ddsm_phase1savingskwh,"
            + "ddsm_phase1savingskw,"
                //+ "ddsm_phase1savingsthm,"
            + "ddsm_Phase1SavingsCCF,"
            + "ddsm_phase1incentivetotal,"
            + "ddsm_phase1perunitkwh,"
            + "ddsm_phase1perunitkw,"
                //+ "ddsm_phase1perunitthm,"
            + "ddsm_phase1incentiveunits,"
            + "ddsm_CostEquipment,"
            + "ddsm_CostLabor,"
            + "ddsm_CostOther,"
            + "ddsm_CostTotal,"
            + "ddsm_ProjectTemplate,"

        + "&$filter=ddsm_ProjectTemplate/Id eq guid'" + projTpl_ID + "'", false);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.send();
        if (req.readyState == 4 && req.status == 200) {
            var _tplData = JSON.parse(req.responseText).d;
            if (_tplData.results[0] != null) {
                for (var i = 0; i < _tplData.results.length; i++) {
                    createProjOneMeas(_tplData.results[i], proj_Obj);
                }
            }
        }
    } catch (err) {
        alert("Error generate Measure with Measure Template & Measure Calculation Template for New Project. " + err);
    }
}

function createProjOneMeas(objOneMeas, proj_Obj){
var objOneMeas = objOneMeas;
    //createDocList_measTpl(proj_Obj.Id, proj_Obj.Name, objOneMeas.ddsm_MeasureSelector.Id);

    var req = new XMLHttpRequest();
    var now = new Date();
    var serverUrl = Xrm.Page.context.getClientUrl();
    var measname = (proj_Obj.Name).substr(0, 6) + "-" + objOneMeas.ddsm_MeasureSelector.Name + "_" + now.getFullYear() + now.getMonth() + now.getDate() + "-" + now.getHours() + now.getMinutes() + now.getSeconds();
    var objMeas = {};
    var InitialLead = getInitialLeadSite(siteProject.Id);

    objMeas.ddsm_name = measname;
    objMeas.ddsm_MeasureSelector = objOneMeas.ddsm_MeasureSelector;
    objMeas.ddsm_AccountId = accountProject;
    objMeas.ddsm_parentsite = siteProject;
    objMeas.ddsm_ImplementationSite = siteProject;
    objMeas.ddsm_ProjectToMeasureId = proj_Obj;
    objMeas.ddsm_MeasureLeadSourceProjTpl = {};
    objMeas.ddsm_MeasureLeadSourceProjTpl.Id = objOneMeas.ddsm_projecttemplatemeasureId;
    objMeas.ddsm_MeasureLeadSourceProjTpl.Name = objOneMeas.ddsm_name;
    objMeas.ddsm_MeasureLeadSourceProjTpl.LogicalName = "ddsm_projecttemplatemeasure";
    if(typeof taProject.Id !== 'undefined') {
        objMeas.ddsm_TradeAllyId = taProject;
    }
    if(InitialLead != null) {objMeas.ddsm_InitialLead = InitialLead;}


    try {
        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_measuretemplateSet?"
        + "$select="
        + "ddsm_IncRateUnit,"
        + "ddsm_IncRateKW,"
        + "ddsm_IncRatekWh,"
        + "ddsm_IncRatethm,"
        + "ddsm_SavingsperUnitKW,"
        + "ddsm_SavingsperUnitkWh,"
        + "ddsm_SavingsperUnitthm,"
        + "ddsm_Program,"
        + "ddsm_MeasureCode,"
        + "ddsm_MeasureNumber,"
        + "ddsm_EnergyType,"
        + "ddsm_Category,"
        + "ddsm_MeasureGroup,"
        + "ddsm_MeasureSubgroup,"
        + "ddsm_Size,"
        + "ddsm_EUL,"
        + "ddsm_BaseEquipDescr,"
        + "ddsm_EffEquipDescr,"
        + "ddsm_AnnualFixedCosts,"
        + "ddsm_ClientDescription,"
        + "ddsm_EndUse,"
        + "ddsm_EquipmentDescription,"
        + "ddsm_IncrementalCost,"
        + "ddsm_LMLightingorNon,"
        + "ddsm_MeasureLife,"
        + "ddsm_MeasureNotes,"
        + "ddsm_Source,"
        + "ddsm_Unit,"
        + "ddsm_StudyMeasure,"
        + "ddsm_ProgramCycle,"

        + "ddsm_MeasureType,"
        + "ddsm_AnnualHoursBefore,"
        + "ddsm_AnnualHoursAfter,"
        + "ddsm_SummerDemandReductionKW,"
        + "ddsm_WinterDemandReductionKW,"
        + "ddsm_WattsBase,"
        + "ddsm_WattsEff,"
        + "ddsm_ClientPeakKWSavings,"
        + "ddsm_MotorType,"
        + "ddsm_MotorHP,"
        + "ddsm_measuresubgroup2,"
        + "ddsm_SavingsperUnitCCF,"

        + "ddsm_BPAEffectiveEndDate,"
        + "ddsm_BPAEffectiveStartDate,"
        + "ddsm_BPAIncentiveperUnit,"
        + "ddsm_BPAkWhSavingsperUnitTarget,"
        + "ddsm_BPAReimbursableperUnitTarget,"
        + "ddsm_BPAMeasureNumber,"
        + "ddsm_BPAMeasureVersion,"
        + "ddsm_MaxLoanperUnit,"

        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_name,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateSavings,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_CalculateIncentive,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_PerUnitLock,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_SavingsLock,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentiveRatesLock,"
        + "ddsm_ddsm_measurecalculationtemplate_ddsm_measur/ddsm_IncentivesLock"//+ "&$orderby=ddsm_projTplToMilestoneTpl/ddsm_Index asc"
        + "&$expand=ddsm_ddsm_measurecalculationtemplate_ddsm_measur"
        + "&$filter=ddsm_measuretemplateId eq guid'" + objOneMeas.ddsm_MeasureSelector.Id + "' and statecode/Value eq 0", false);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.send();
        if (req.readyState == 4 && req.status == 200) {
            //From the project template - will also contain milestone information
            var _tplData = JSON.parse(req.responseText).d;
            if (_tplData.results[0] != null) {

                //console.dir(_tplData.results);

                var measCalcTpl_Data = _tplData.results[0].ddsm_ddsm_measurecalculationtemplate_ddsm_measur;
                if (!!measCalcTpl_Data) {
                    var measCalcTpl_Name = measCalcTpl_Data.ddsm_name;
                    var measCalcTpl_CalcSavings = measCalcTpl_Data.ddsm_CalculateSavings;
                    var measCalcTpl_CalcIncentives = measCalcTpl_Data.ddsm_CalculateIncentive;
                    var measCalcTpl_PerUnitLock = measCalcTpl_Data.ddsm_PerUnitLock;
                    var measCalcTpl_SavingsLock = measCalcTpl_Data.ddsm_SavingsLock;
                    var measCalcTpl_IncentiveRatesLock = measCalcTpl_Data.ddsm_IncentiveRatesLock;
                    var measCalcTpl_IncentivesLock = measCalcTpl_Data.ddsm_IncentivesLock;
                    var measCalcTpl_CalcTypeValues = "";
                }

                objMeas.ddsm_Program = _tplData.results[0].ddsm_Program;
                objMeas.ddsm_MeasureCode = _tplData.results[0].ddsm_MeasureCode;
                objMeas.ddsm_MeasureNumber = _tplData.results[0].ddsm_MeasureNumber;
                objMeas.ddsm_EnergyType = _tplData.results[0].ddsm_EnergyType;
                objMeas.ddsm_Category = _tplData.results[0].ddsm_Category;
                objMeas.ddsm_MeasureGroup = _tplData.results[0].ddsm_MeasureGroup;
                objMeas.ddsm_MeasureSubgroup = _tplData.results[0].ddsm_MeasureSubgroup;
                objMeas.ddsm_Size = _tplData.results[0].ddsm_Size;
                objMeas.ddsm_EUL = _tplData.results[0].ddsm_EUL;
                objMeas.ddsm_BaseEquipDescr = _tplData.results[0].ddsm_BaseEquipDescr;
                objMeas.ddsm_EffEquipDescr = _tplData.results[0].ddsm_EffEquipDescr;
                objMeas.ddsm_ClientDescription = _tplData.results[0].ddsm_ClientDescription;
                objMeas.ddsm_EndUse = _tplData.results[0].ddsm_EndUse;
                objMeas.ddsm_EquipmentDescription = _tplData.results[0].ddsm_EquipmentDescription;
                objMeas.ddsm_LMLightingorNon = _tplData.results[0].ddsm_LMLightingorNon;
                objMeas.ddsm_MeasureNotes = _tplData.results[0].ddsm_MeasureNotes;
                objMeas.ddsm_Source = _tplData.results[0].ddsm_Source;
                objMeas.ddsm_Unit = _tplData.results[0].ddsm_Unit;
                objMeas.ddsm_StudyMeasure = _tplData.results[0].ddsm_StudyMeasure;
                objMeas.ddsm_MeasureType = _tplData.results[0].ddsm_MeasureType;
                objMeas.ddsm_AnnualHoursBefore = _tplData.results[0].ddsm_AnnualHoursBefore;
                objMeas.ddsm_AnnualHoursAfter = _tplData.results[0].ddsm_AnnualHoursAfter;
                objMeas.ddsm_SummerDemandReductionKW = _tplData.results[0].ddsm_SummerDemandReductionKW;
                objMeas.ddsm_WinterDemandReductionKW = _tplData.results[0].ddsm_WinterDemandReductionKW;
                objMeas.ddsm_WattsBase = _tplData.results[0].ddsm_WattsBase;
                objMeas.ddsm_WattsEff = _tplData.results[0].ddsm_WattsEff;
                objMeas.ddsm_clientpeakkwsavings = _tplData.results[0].ddsm_ClientPeakKWSavings;
                objMeas.ddsm_MotorType = _tplData.results[0].ddsm_MotorType;
                objMeas.ddsm_MotorHP = _tplData.results[0].ddsm_MotorHP;
                objMeas.ddsm_measuresubgroup2 = _tplData.results[0].ddsm_measuresubgroup2;
                objMeas.ddsm_IncRateUnit = _tplData.results[0].ddsm_IncRateUnit;
                objMeas.ddsm_IncRateKW = _tplData.results[0].ddsm_IncRateKW;
                objMeas.ddsm_IncRatekWh = _tplData.results[0].ddsm_IncRatekWh;
                objMeas.ddsm_IncRatethm = _tplData.results[0].ddsm_IncRatethm;
                objMeas.ddsm_IncrementalCost = _tplData.results[0].ddsm_IncrementalCost;
                objMeas.ddsm_MeasureLife = _tplData.results[0].ddsm_MeasureLife;
                objMeas.ddsm_AnnualFixedCosts = _tplData.results[0].ddsm_AnnualFixedCosts;
                objMeas.ddsm_ProgramOfferingsId = _tplData.results[0].ddsm_ProgramOffering;
                //objMeas.ddsm_ProgramCycleId = _tplData.results[0].ddsm_ProgramCycle;
                //BPA
                objMeas.ddsm_BPAEffectiveEndDate = _tplData.results[0].ddsm_BPAEffectiveEndDate;
                objMeas.ddsm_BPAEffectiveStartDate = _tplData.results[0].ddsm_BPAEffectiveStartDate;
                objMeas.ddsm_BPAIncentivePerUnitTarget = _tplData.results[0].ddsm_BPAIncentiveperUnit;
                objMeas.ddsm_BPAkWhSavingsperUnitTarget = _tplData.results[0].ddsm_BPAkWhSavingsperUnitTarget;
                objMeas.ddsm_BPAReimbursableperUnitTarget = _tplData.results[0].ddsm_BPAReimbursableperUnitTarget;
                objMeas.ddsm_BPAMeasureNumber = _tplData.results[0].ddsm_BPAMeasureNumber;
                objMeas.ddsm_BPAMeasureVersion = _tplData.results[0].ddsm_BPAMeasureVersion;
                objMeas.ddsm_MaxLoanperUnit = _tplData.results[0].ddsm_MaxLoanperUnit;


                //Measure Calc Template Data
                if (measCalcTpl_CalcSavings == true) {
                    measCalcTpl_CalcTypeValues += "Calculate Savings; ";
                }
                if (measCalcTpl_CalcIncentives == true) {
                    measCalcTpl_CalcTypeValues += "Calculate Incentives; ";
                }
                if (measCalcTpl_PerUnitLock == true) {
                    measCalcTpl_CalcTypeValues += "Lock Per Units; ";
                }
                if (measCalcTpl_SavingsLock == true) {
                    measCalcTpl_CalcTypeValues += "Lock Savings; ";
                }
                if (measCalcTpl_IncentiveRatesLock == true) {
                    measCalcTpl_CalcTypeValues += "Lock Incentive Rates; ";
                }
                if (measCalcTpl_IncentivesLock == true) {
                    measCalcTpl_CalcTypeValues += "Lock Incentives; ";
                }

                if (measCalcTpl_Name != null) {
                    objMeas.ddsm_CalculationType = measCalcTpl_Name;
                }
                if (measCalcTpl_CalcTypeValues != null) {
                    objMeas.ddsm_CalculationTypeValues = measCalcTpl_CalcTypeValues;
                }

                //copies savings
                //Copy to Phase 1

                objMeas.ddsm_CurrentUnits = objOneMeas.ddsm_phase1units;

                objMeas.ddsm_CurrentSavingskWh = objOneMeas.ddsm_phase1savingskwh;
                objMeas.ddsm_currentsavingskw = objOneMeas.ddsm_phase1savingskw;
                //objMeas.ddsm_CurrentSavingsthm = objOneMeas.ddsm_phase1savingsthm;
                objMeas.ddsm_CurrentSavingsCCF = objOneMeas.ddsm_Phase1SavingsCCF;
                objMeas.ddsm_CurrentIncTotal = objOneMeas.ddsm_phase1incentivetotal;

                objMeas.ddsm_phase1units = objOneMeas.ddsm_phase1units;
                objMeas.ddsm_phase1savingskwh = objOneMeas.ddsm_phase1savingskwh;
                objMeas.ddsm_phase1savingskw = objOneMeas.ddsm_phase1savingskw;
                //objMeas.ddsm_phase1savingsthm = objOneMeas.ddsm_phase1savingsthm;
                objMeas.ddsm_Phase1SavingsCCF = objOneMeas.ddsm_Phase1SavingsCCF;
                objMeas.ddsm_phase1perunitkwh = objOneMeas.ddsm_phase1perunitkwh;
                objMeas.ddsm_phase1perunitkw = objOneMeas.ddsm_phase1perunitkw;
                //objMeas.ddsm_phase1perunitthm = objOneMeas.ddsm_phase1perunitthm;
                objMeas.ddsm_phase1incentivetotal = objOneMeas.ddsm_phase1incentivetotal;
                objMeas.ddsm_phase1incentiveunits = objOneMeas.ddsm_phase1incentiveunits;
                objMeas.ddsm_CostEquipment = objOneMeas.ddsm_CostEquipment;
                objMeas.ddsm_CostLabor = objOneMeas.ddsm_CostLabor;
                objMeas.ddsm_CostOther = objOneMeas.ddsm_CostOther;
                objMeas.ddsm_CostTotal = objOneMeas.ddsm_CostTotal;

                //Populate some measure fields from the ImplementationSite or from the ParentSite if the first is empty
                if(window.hasOwnProperty('objMeasSiteFields')){
                    objMeas = setFieldsFromObj(objMeas,window.objMeasSiteFields);
                }
                //Populate some measure fields from the RateClassReference
                if(window.hasOwnProperty('objMeasRCRFields')){
                    objMeas = setFieldsFromObj(objMeas,window.objMeasRCRFields);
                }
                gen_H_createEntitySync(objMeas, "ddsm_measureSet");

            }
        }
    } catch (err) {
        alert("Error generate Measure with Measure Template & Measure Calculation Template for New Project. " + err);
    }
}

function getMSSets(projTpl_ID){
    var serverUrl = Xrm.Page.context.getClientUrl();
    var objNewMSSet = [];
    IndexOffsetDocList = [];
    var reqMSSet = new XMLHttpRequest();
    reqMSSet.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_milestonesetSet?"
    + "$select="
    + "ddsm_milestonesetId,"
    + "ddsm_name,"
    + "ddsm_insertbeforeindex,"
    + "ddsm_numberofsets,"
    + "ddsm_Probability"
    + "&$orderby=ddsm_insertbeforeindex asc"
    + "&$filter=ddsm_ProjectTemplate/Id eq guid'" + projTpl_ID + "' and ddsm_Probability gt 0 and statecode/Value eq 0", false);
    reqMSSet.setRequestHeader("Accept", "application/json");
    reqMSSet.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    reqMSSet.send();

    if (reqMSSet.readyState == 4 && reqMSSet.status == 200) {
        try {
            var obj = JSON.parse(reqMSSet.responseText).d;
            var objMSSet = obj.results;

            if (objMSSet != null) {
                //console.dir(objMSSet);
                for (var i = 0; i < objMSSet.length; i++) {
                    var obj ={}, obj1 = {};
                    if (objMSSet[i].ddsm_Probability != 0) {
                        if(objMSSet[i].ddsm_Probability != 1){
                            var randomNumb = Math.random();
                            //console.log(randomNumb);
                            if (randomNumb < objMSSet[i].ddsm_Probability) {
                                if(objMSSet[i].ddsm_numberofsets != 0) {
                                    obj1.ddsm_insertbeforeindex = objMSSet[i].ddsm_insertbeforeindex;
                                    obj1.ddsm_numberofsets = objMSSet[i].ddsm_numberofsets;
                                    obj1.setId = objMSSet[i].ddsm_milestonesetId;
                                    IndexOffsetDocList.push(obj1);

                                    obj.ddsm_insertbeforeindex = objMSSet[i].ddsm_insertbeforeindex;
                                    obj.ddsm_numberofsets = objMSSet[i].ddsm_numberofsets;
                                    obj._ddsm_steps = getMSSetSteps(objMSSet[i].ddsm_milestonesetId);
                                    objNewMSSet.push(obj);
                                }
                            }
                        } else {
                            if(objMSSet[i].ddsm_numberofsets != 0) {
                                obj1.ddsm_insertbeforeindex = objMSSet[i].ddsm_insertbeforeindex;
                                obj1.ddsm_numberofsets = objMSSet[i].ddsm_numberofsets;
                                obj1.setId = objMSSet[i].ddsm_milestonesetId;
                                IndexOffsetDocList.push(obj1);

                                obj.ddsm_insertbeforeindex = objMSSet[i].ddsm_insertbeforeindex;
                                obj.ddsm_numberofsets = objMSSet[i].ddsm_numberofsets;
                                obj._ddsm_steps = getMSSetSteps(objMSSet[i].ddsm_milestonesetId);
                                objNewMSSet.push(obj);
                            }
                        }
                    }
                }
            }

        } catch (err) { alert(err); }
    } else {
        alert("Error: " + reqMSSet.responseText);
    }
    //console.log('objNewMSSet..');
    //console.dir(objNewMSSet);
    return objNewMSSet;
}

function getMSSetSteps(MSSet_ID) {
    var serverUrl = Xrm.Page.context.getClientUrl();

    var req = new XMLHttpRequest();
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_milestonesetstepSet?"
    + "$select="
    + "ddsm_milestonesetstepId,"
    + "ddsm_name,"
    + "ddsm_ProjectTemplToMsSetStep,"
    + "ddsm_Category,"
    + "ddsm_duration,"
    + "ddsm_index,"
    + "ddsm_pendingphase,"
    + "ddsm_resultingphase,"
    + "ddsm_msspecialnote,"
    + "ddsm_responsible,"
    + "ddsm_requiresemptymeasure,"
    + "ddsm_requiresvalidmeasure,"
    + "ddsm_approvalthresholdcust,"
    + "ddsm_approvalthresholdstd,"
    + "ddsm_approvalthresholdnc,"
    + "ddsm_approvalthresholdrcx,"
    + "ddsm_projectphasename,"
    + "ddsm_projectstatus,"
    + "ddsm_initialoffered,"
    + "ddsm_securityincludeonlyguid,"
    + "ddsm_securityexcludeguid,"
    + "ddsm_insertset,"
    + "ddsm_CurrEscalOverdueLength,"
    + "ddsm_TradeAllyTemplateGUID,"
    + "ddsm_CustomerTemplateGUID,"
    + "ddsm_UserTemplateGUID,"
    + "ddsm_PartnerTemplateGUID,"
    + "ddsm_PushNotifications"
    + "&$orderby=ddsm_index asc"
    + "&$filter=ddsm_milestonetemplatesetnameid/Id  eq guid'" + MSSet_ID + "'", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();

    if (req.readyState == 4 && req.status == 200) {
        try {
            var obj = JSON.parse(req.responseText).d;
            return(obj.results);

        } catch (err) { alert(err); }
    } else {
        alert("Error: " + req.responseText);
    }

}

function createProjMS_QuickForm(proj_ID, projTpl_ID, StartDate, proj_Name) {
    var PYMTPREF_SITE = 962080000;
    var PYMTPREF_PPN = 962080001;
    var PYMTPREF_LPC = 962080003;
    var PYMTPREF_LPC_SITE = null;
    var PPN_OVERRIDE_NO = 962080000;
    var PPN_OVERRIDE_YES = 962080001;

    var IdxOffSet = 0, projTplName, PhaseNumber = 0, projPhase = "", InitialPhaseDate = new Date();
    InitialPhaseDate = StartDate;

    var parentSite = siteProject.Name;

    var serverUrl = Xrm.Page.context.getClientUrl();

    var MSSetObject = getMSSets(projTpl_ID);

    var req = new XMLHttpRequest();
    //console.log("proj_ID: " + proj_ID);
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projecttemplateSet?"
/*    + "$select="
    + "ddsm_InitialStatus,ddsm_InitialReportingStatus,"
    + "ddsm_ProgramCode,"
    + "ddsm_LastMilestoneIndex,"
    + "ddsm_requirepayeeinfoindex,"
    + "ddsm_completeonlastmilestone,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_milestonetemplateId,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_name,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_Category,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_Duration,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_Index,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_PendingPhase,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ResultingPhase,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_MSSpecialNote,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_Responsible,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_RequiresEmptyMeasure,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_RequiresValidMeasure,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ApprovalThresholdCust,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ApprovalThresholdStd,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ApprovalThresholdNC,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ApprovalThresholdRCx,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ProjectPhaseName,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_ProjectStatus,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_InitialOffered,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_SecurityIncludeOnlyGUID,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_SecurityExcludeGUID,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_insertset,"

    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_TradeAllyTemplateGUID,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_CustomerTemplateGUID,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_UserTemplateGUID,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_PartnerTemplateGUID,"
    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_PushNotifications,"

    + "ddsm_ddsm_projecttemplate_ddsm_milestonetemplate/ddsm_EscalOverdueLength" //1.29.2015
*/
    + "$expand=ddsm_ddsm_projecttemplate_ddsm_milestonetemplate"
    + "&$filter=ddsm_projecttemplateId eq guid'" + projTpl_ID + "' and statecode/Value eq 0", false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();
    if (req.readyState == 4 && req.status == 200) {
        //From the project template - will also contain milestone information
        var _tplData = JSON.parse(req.responseText).d;
        if (_tplData.results[0] != null) {
            //Loop the Result Set
            var proj_obj = {};
            //for (var x in _tplData.results) { //BTL 8/26/2014 I commented this line and added the next line.
            for (var x = 0; x < _tplData.results.length; x++) {
                projTplName = _tplData.results[x].ddsm_name;
                if (_tplData.results[x].ddsm_InitialStatus != null) {
                    proj_obj["ddsm_ProjectStatus"] = _tplData.results[x].ddsm_InitialStatus;
                }
                if (_tplData.results[x].ddsm_InitialReportingStatus != null) {
                    projPhase = proj_obj["ddsm_Phase"] = _tplData.results[x].ddsm_InitialReportingStatus;

                }
                if (_tplData.results[x].ddsm_requirepayeeinfoindex != null) {
                    proj_obj["ddsm_requirepayeeinfoindex"] = _tplData.results[x].ddsm_requirepayeeinfoindex;
                }
                if (_tplData.results[x].ddsm_completeonlastmilestone != null) {
                    proj_obj["ddsm_completeonlastmilestone"] = _tplData.results[x].ddsm_completeonlastmilestone;
                }
                proj_obj["ddsm_PendingMilestoneIndex"] = _tplData.results[x].ddsm_InitialMilestoneIndex
                PhaseNumber = proj_obj["ddsm_PhaseNumber"] = 1; // Start (First) Index project phase. Number of the active phase.

                proj_obj["ddsm_CalculationRecordStatus"] = {Value: 962080001};
                proj_obj["ddsm_CreatorRecordType"] = {Value: 962080000};

                // Bussiness Process ID (dynamicdsmdemo2015)
                //proj_obj["processid"] = "3c5193be-4bb3-4425-b574-56ce91051733";
                //proj_obj["stageid"] = "5430925c-1245-4533-80e2-dff366288b5b";

                // Rename the Project as 000000-CODE-SiteName
/*
                if (parentSite != null) {
                    proj_Name = proj_Name + "-" + _tplData.results[x].ddsm_ProgramCode + "-" + parentSite.substr(0, 20);
                }
                proj_obj["ddsm_name"] = proj_Name;
*/
                proj_obj["ddsm_CompletedStatus"] = _tplData.results[x].ddsm_InitialStatus;

                proj_obj["ddsm_FundingSourceCalculationRequired"] = _tplData.results[x].ddsm_FundingSourceCalculationRequired;

                if ((_tplData.results[x].ddsm_ddsm_projecttemplate_ddsm_milestonetemplate != undefined) &&
                    (_tplData.results[x].ddsm_ddsm_projecttemplate_ddsm_milestonetemplate.results != undefined)) {
                    var ms_array = _tplData.results[x].ddsm_ddsm_projecttemplate_ddsm_milestonetemplate.results;

                    var tempStartDate = new Date();

                    tempStartDate = StartDate;

                    var j = new Array();
                    var y = 0;
                    for (var i in ms_array) {
                        j[ms_array[i].ddsm_Index - 1] = y;
                        y++;
                    }
                    var Act = 0;
                    //---------------------------------------------------------------------------------------------------
                    var ProjPhaseNameCompare;
                    var ProjPhaseIndex = 1;
                    //---------------------------------------------------------------------------------------------------
                    for (var k = 0; k < _tplData.results[x].ddsm_LastMilestoneIndex; k++) {

                        var ms_obj = new Object();
// random insert set steps
                        var currIndex = ms_array[j[k]].ddsm_Index;
                        if(MSSetObject != null && MSSetObject.length > 0){
                            for(var l = 0; l< MSSetObject.length; l++){
                                if(currIndex == MSSetObject[l].ddsm_insertbeforeindex){
                                    for(var p = 0; p < MSSetObject[l]._ddsm_steps.length;p++){
                                        ms_obj = new Object();
                                        ms_obj.ddsm_TargetStart = new Date(tempStartDate.getTime());
                                        if ((currIndex + MSSetObject[l]._ddsm_steps[p].ddsm_index - 1 + IdxOffSet) == 1) {
                                            ms_obj.ddsm_ActualStart = new Date(tempStartDate.getTime());
                                        }
                                        tempStartDate = new Date(tempStartDate.getTime() + (MSSetObject[l]._ddsm_steps[p].ddsm_duration) * 86400000);
                                        ms_obj.ddsm_TargetEnd = new Date(tempStartDate.getTime());
                                        ms_obj.ddsm_name = MSSetObject[l]._ddsm_steps[p].ddsm_name;
                                        ms_obj.ddsm_Category = MSSetObject[l]._ddsm_steps[p].ddsm_Category;
                                        ms_obj.ddsm_Index = currIndex + MSSetObject[l]._ddsm_steps[p].ddsm_index - 1 + IdxOffSet;
                                        ms_obj.ddsm_PendingPhase = MSSetObject[l]._ddsm_steps[p].ddsm_pendingphase;
                                        ms_obj.ddsm_ResultingPhase = MSSetObject[l]._ddsm_steps[p].ddsm_resultingphase;
                                        ms_obj.ddsm_MSSpecialNote = MSSetObject[l]._ddsm_steps[p].ddsm_msspecialnote;
                                        ms_obj.ddsm_Responsible = MSSetObject[l]._ddsm_steps[p].ddsm_responsible;
                                        ms_obj.ddsm_Duration = MSSetObject[l]._ddsm_steps[p].ddsm_duration;
                                        ms_obj.ddsm_RequiresEmptyMeasure = MSSetObject[l]._ddsm_steps[p].ddsm_requiresemptymeasure;
                                        ms_obj.ddsm_RequiresValidMeasure = MSSetObject[l]._ddsm_steps[p].ddsm_requiresvalidmeasure;
                                        ms_obj.ddsm_ApprovalThresholdCust = new Object();
                                        ms_obj.ddsm_ApprovalThresholdCust.Value = MSSetObject[l]._ddsm_steps[p].ddsm_approvalthresholdcust.Value;
                                        ms_obj.ddsm_ApprovalThresholdStd = new Object();
                                        ms_obj.ddsm_ApprovalThresholdStd.Value = MSSetObject[l]._ddsm_steps[p].ddsm_approvalthresholdstd.Value;
                                        ms_obj.ddsm_ApprovalThresholdNC = new Object();
                                        ms_obj.ddsm_ApprovalThresholdNC.Value = MSSetObject[l]._ddsm_steps[p].ddsm_approvalthresholdnc.Value;
                                        ms_obj.ddsm_ApprovalThresholdRCx = new Object();
                                        ms_obj.ddsm_ApprovalThresholdRCx.Value = MSSetObject[l]._ddsm_steps[p].ddsm_approvalthresholdrcx.Value;
                                        ms_obj.ddsm_ProjectPhaseName = MSSetObject[l]._ddsm_steps[p].ddsm_projectphasename;
                                        ms_obj.ddsm_ProjectStatus = MSSetObject[l]._ddsm_steps[p].ddsm_projectstatus;
                                        ms_obj.ddsm_InitialOffered = MSSetObject[l]._ddsm_steps[p].ddsm_initialoffered;
                                        ms_obj.ddsm_SecurityIncludeOnlyGUID = MSSetObject[l]._ddsm_steps[p].ddsm_securityincludeonlyguid;
                                        ms_obj.ddsm_SecurityExcludeGUID = MSSetObject[l]._ddsm_steps[p].ddsm_securityexcludeguid;
                                        ms_obj.ddsm_insertset = MSSetObject[l]._ddsm_steps[p].ddsm_insertset;
                                        ms_obj.ddsm_indexoffset = 0;
                                        ms_obj.ddsm_CurrEscalOverdueLength = MSSetObject[l]._ddsm_steps[p].ddsm_CurrEscalOverdueLength; // 1.29.2015

                                        ms_obj.ddsm_TradeAllyTemplateGUID = MSSetObject[l]._ddsm_steps[p].ddsm_TradeAllyTemplateGUID;
                                        ms_obj.ddsm_CustomerTemplateGUID = MSSetObject[l]._ddsm_steps[p].ddsm_CustomerTemplateGUID;
                                        ms_obj.ddsm_PushNotifications = MSSetObject[l]._ddsm_steps[p].ddsm_PushNotifications;
                                        ms_obj.ddsm_UserTemplateGUID = MSSetObject[l]._ddsm_steps[p].ddsm_UserTemplateGUID;
                                        ms_obj.ddsm_PartnerTemplateGUID = MSSetObject[l]._ddsm_steps[p].ddsm_PartnerTemplateGUID;

                                        ms_obj.ddsm_Status = new Object();
                                        //Check for Project Status and Project Phase Name it should be same as proj templ Initial Status and Initial Reporting Status
                                        /*if (ms_obj.ddsm_ProjectPhaseName == proj_obj.ddsm_Phase && ms_obj.ddsm_ProjectStatus == proj_obj.ddsm_ProjectStatus) {
                                            ms_obj.ddsm_Status.Value = 962080001;
                                            proj_obj["ddsm_Responsible"] = MSSetObject[l]._ddsm_steps[p].ddsm_responsible;
                                        } else {
                                            ms_obj.ddsm_Status.Value = 962080000;
                                        }*/
                                        if (ms_obj.ddsm_Index == 1) {
                                            ms_obj.ddsm_Status.Value = 962080001;
                                            proj_obj["ddsm_Responsible"] = MSSetObject[l]._ddsm_steps[p].ddsm_responsible;
                                        } else {
                                            ms_obj.ddsm_Status.Value = 962080000;
                                        }
                                        ms_obj.ddsm_ProjectToMilestoneId = new Object();
                                        ms_obj.ddsm_ProjectToMilestoneId.Id = proj_ID;
                                        ms_obj.ddsm_ProjectToMilestoneId.Name = proj_obj["ddsm_name"];
                                        ms_obj.ddsm_ProjectToMilestoneId.LogicalName = "ddsm_project";
                                        ms_obj.ddsm_ProjectTaskCount = 0;
                                        gen_H_createEntitySync(ms_obj, "ddsm_milestoneSet");

                                    }
                                    IdxOffSet += MSSetObject[l].ddsm_numberofsets;
                                }
                            }
                        }

                        if (ms_array[j[k]].ddsm_name != null) {
                            ms_obj = new Object();
                            ms_obj.ddsm_TargetStart = new Date(tempStartDate.getTime());
                            if ((ms_array[j[k]].ddsm_Index + IdxOffSet) == 1) {
                                ms_obj.ddsm_ActualStart = new Date(tempStartDate.getTime());
                                //console.log("ms_obj.ddsm_ActualStart: " + ms_obj.ddsm_ActualStart);
                            }
                            tempStartDate = new Date(tempStartDate.getTime() + (ms_array[j[k]].ddsm_Duration) * 86400000);
                            ms_obj.ddsm_TargetEnd = new Date(tempStartDate.getTime());
                            ms_obj.ddsm_name = ms_array[j[k]].ddsm_name;
                            ms_obj.ddsm_Category = ms_array[j[k]].ddsm_Category;
                            ms_obj.ddsm_Index = ms_array[j[k]].ddsm_Index + IdxOffSet;
                            ms_obj.ddsm_PendingPhase = ms_array[j[k]].ddsm_PendingPhase;
                            ms_obj.ddsm_ResultingPhase = ms_array[j[k]].ddsm_ResultingPhase;
                            ms_obj.ddsm_MSSpecialNote = ms_array[j[k]].ddsm_MSSpecialNote;

                            ms_obj.ddsm_Responsible = ms_array[j[k]].ddsm_Responsible;
                            ms_obj.ddsm_Duration = ms_array[j[k]].ddsm_Duration;
                            ms_obj.ddsm_RequiresEmptyMeasure = ms_array[j[k]].ddsm_RequiresEmptyMeasure;
                            ms_obj.ddsm_RequiresValidMeasure = ms_array[j[k]].ddsm_RequiresValidMeasure;
                            ms_obj.ddsm_ApprovalThresholdCust = new Object();
                            ms_obj.ddsm_ApprovalThresholdCust.Value = ms_array[j[k]].ddsm_ApprovalThresholdCust.Value;
                            ms_obj.ddsm_ApprovalThresholdStd = new Object();
                            ms_obj.ddsm_ApprovalThresholdStd.Value = ms_array[j[k]].ddsm_ApprovalThresholdStd.Value;
                            ms_obj.ddsm_ApprovalThresholdNC = new Object();
                            ms_obj.ddsm_ApprovalThresholdNC.Value = ms_array[j[k]].ddsm_ApprovalThresholdNC.Value;
                            ms_obj.ddsm_ApprovalThresholdRCx = new Object();
                            ms_obj.ddsm_ApprovalThresholdRCx.Value = ms_array[j[k]].ddsm_ApprovalThresholdRCx.Value;
                            ms_obj.ddsm_ProjectPhaseName = ms_array[j[k]].ddsm_ProjectPhaseName;
                            ms_obj.ddsm_ProjectStatus = ms_array[j[k]].ddsm_ProjectStatus;
                            ms_obj.ddsm_InitialOffered = ms_array[j[k]].ddsm_InitialOffered;
                            ms_obj.ddsm_SecurityIncludeOnlyGUID = ms_array[j[k]].ddsm_SecurityIncludeOnlyGUID;
                            ms_obj.ddsm_SecurityExcludeGUID = ms_array[j[k]].ddsm_SecurityExcludeGUID;
                            ms_obj.ddsm_insertset = ms_array[j[k]].ddsm_insertset;
                            ms_obj.ddsm_indexoffset = 0;

                            ms_obj.ddsm_TradeAllyTemplateGUID = ms_array[j[k]].ddsm_TradeAllyTemplateGUID;
                            ms_obj.ddsm_CustomerTemplateGUID = ms_array[j[k]].ddsm_CustomerTemplateGUID;
                            ms_obj.ddsm_PushNotifications = ms_array[j[k]].ddsm_PushNotifications;
                            ms_obj.ddsm_UserTemplateGUID = ms_array[j[k]].ddsm_UserTemplateGUID;
                            ms_obj.ddsm_PartnerTemplateGUID = ms_array[j[k]].ddsm_PartnerTemplateGUID;

                            ms_obj.ddsm_Status = new Object();

                            //var Act = 0;
                            //Check for Project Status and Project Phase Name it should be same as proj templ Initial Status and Initial Reporting Status
                            //------------------------------------------------------------------------------------
                            //var ProjPhaseIndex = 1;
                            //var ProjPhaseNameCompare;
                            var ProjPhaseNameCurrent = ms_array[j[k]].ddsm_ProjectPhaseName;
                            if(k==0){
                                ProjPhaseNameCompare = ms_array[j[k]].ddsm_ProjectPhaseName;
                            }
                            //Set ProjectPhase
                            if (ProjPhaseNameCurrent != ProjPhaseNameCompare && Act == 0)
                            {
                                ProjPhaseIndex = ProjPhaseIndex + 1;
                                ProjPhaseNameCompare = ProjPhaseNameCurrent;
                                PhaseNumber = proj_obj["ddsm_PhaseNumber"] = ProjPhaseIndex;
                            }
                            //------------------------------------------------------------------------------------
                            if (ms_obj.ddsm_ProjectPhaseName == proj_obj.ddsm_Phase && ms_obj.ddsm_ProjectStatus == proj_obj.ddsm_ProjectStatus && Act == 0 && ms_obj.ddsm_Index == proj_obj["ddsm_PendingMilestoneIndex"]) {
                                ms_obj.ddsm_ActualStart = ms_obj.ddsm_TargetStart;
                                ms_obj.ddsm_Status.Value = 962080001;
                                proj_obj["ddsm_Responsible"] = ms_array[j[k]].ddsm_Responsible;
                                //PhaseNumber = proj_obj["ddsm_PhaseNumber"] = ms_obj.ddsm_Index;
                                Act = 1; //When set Active
                                //Check Phase (proj_obj.ddsm_Phase) and if it changed Set ProjPhaseIndex +1


                            } else {
                                if (Act == 0)
                                {
                                    ms_obj.ddsm_Status.Value = 962080002;
                                    ms_obj.ddsm_ActualStart = ms_obj.ddsm_TargetStart;
                                    ms_obj.ddsm_ActualEnd = ms_obj.ddsm_TargetStart;
                                } else {
                                    ms_obj.ddsm_Status.Value = 962080000;
                                }
                            }



                            /*if (ms_obj.ddsm_Index == 1) {
                                ms_obj.ddsm_Status.Value = 962080001;
                                proj_obj["ddsm_Responsible"] = ms_array[j[k]].ddsm_Responsible;
                            } else {
                                ms_obj.ddsm_Status.Value = 962080000;
                            }*/
                            ms_obj.ddsm_ProjectToMilestoneId = new Object();
                            ms_obj.ddsm_ProjectToMilestoneId.Id = proj_ID;
                            ms_obj.ddsm_ProjectToMilestoneId.Name = proj_obj["ddsm_name"];
                            ms_obj.ddsm_ProjectToMilestoneId.LogicalName = "ddsm_project";

                            ms_obj.ddsm_MsTpltoMs = new Object();
                            ms_obj.ddsm_MsTpltoMs.Id = ms_array[j[k]].ddsm_milestonetemplateId;
                            ms_obj.ddsm_MsTpltoMs.Name = ms_array[j[k]].ddsm_name;
                            ms_obj.ddsm_MsTpltoMs.LogicalName = "ddsm_milestonetemplate";

                            ms_obj.ddsm_CurrEscalOverdueLength = (ms_array[j[k]].ddsm_EscalOverdueLength != null) ? ms_array[j[k]].ddsm_EscalOverdueLength : 0; //1.29.2015
                            ms_obj.ddsm_ProjectTaskCount = 0;

                            gen_H_createEntitySync(ms_obj, "ddsm_milestoneSet");
                        }
                    }
                    proj_obj["ddsm_EstimatedProjectComplete"] = tempStartDate;
                    proj_obj["ddsm_ProjectCompletionDateUpdatedEstimate"] = tempStartDate;
                    //console.dir(proj_obj);
                    gen_H_updateEntitySync(proj_ID, proj_obj, "ddsm_projectSet");

                    var msReq = new XMLHttpRequest();
                    var serverUrl = Xrm.Page.context.getClientUrl();
                    var msArray = [];
                    var URL = serverUrl + "/XRMServices/2011/OrganizationData.svc/ddsm_milestoneSet?"
                        + "$select="
                        + "ddsm_name,"
                        + "ddsm_ActualStart,"
                        + "ddsm_Status,"
                        + "ddsm_Index,"
                        + "ddsm_TargetEnd"
                        + "&$orderby=ddsm_Index asc"
                        + "&$filter=ddsm_ProjectToMilestoneId/Id eq guid'" + proj_ID + "' and statecode/Value eq 0";

                    //console.log(URL);
                    msReq.open("GET", URL, false);
                    msReq.setRequestHeader("Accept", "application/json");
                    msReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    msReq.send();
                    if (msReq.readyState == 4 && msReq.status == 200) {
                        var responseData = JSON.parse(msReq.responseText).d;
                        var modeResponse = responseData.results;
                        //console.log(modeResponse);
                        if (modeResponse.length > 0) {
                            msArray = modeResponse;
                            var proj_obj = {};
                            for (var i = 0; i < modeResponse.length; i++) {
                                if (parseInt(modeResponse[i].ddsm_Status.Value) == 962080001) {
                                    //console.log("ActualStart: " + modeResponse[i].ddsm_ActualStart);
                                    //console.log("TargetEnd: " + modeResponse[i].ddsm_TargetEnd);
                                    var msActStart = eval((modeResponse[i].ddsm_ActualStart).replace(/\/Date\((\d+)\)\//gi, 'new Date($1)'));
                                    var msTargetStart = eval((modeResponse[i].ddsm_TargetEnd).replace(/\/Date\((\d+)\)\//gi, 'new Date($1)'));

                                    proj_obj["ddsm_PendingMilestone"] = modeResponse[i].ddsm_name;
                                    proj_obj["ddsm_PendingMilestoneIndex"] = modeResponse[i].ddsm_Index;
                                    proj_obj["ddsm_PendingActualStart"] = msActStart;
                                    proj_obj["ddsm_PendingTargetEnd"] = msTargetStart;
                                }
                            }
                            var InitialLead = getInitialLeadSite(siteProject.Id);
                            if(InitialLead != null) {proj_obj.ddsm_InitialLead = InitialLead;}
                            gen_H_updateEntitySync(proj_ID, proj_obj, "ddsm_projectSet");
                        }
                    }
                    if(typeof proj_obj["ddsm_PendingMilestone"] != "undefined" && proj_obj["ddsm_PendingMilestone"] != null) {
                        var BProcessData = [];
                        AGS.REST.retrieveMultipleRecords("Workflow", "$filter=Name eq '" + encodeURIComponent(projTplName) +"'", null, null, function(data){BProcessData = data;}, false, null);
                        if(BProcessData.length > 0) {
                            var processstageURI = BProcessData[0].process_processstage.__deferred.uri;

                                var bpReq = new XMLHttpRequest();
                                bpReq.open("GET", processstageURI, false);
                                bpReq.setRequestHeader("Accept", "application/json");
                                bpReq.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                                bpReq.send();
                                if (bpReq.readyState == 4 && bpReq.status == 200) {
                                    var _bpData = JSON.parse(bpReq.responseText).d;
                                    if (_bpData.results.length > 0) {
                                        var traversedpath = "";

                                        for(var i = 0; i < msArray.length; i++) {
                                            for(var j = 0; j < _bpData.results.length; j++) {
                                                if(_bpData.results[j].StageName == msArray[i].ddsm_name){
                                                    traversedpath = traversedpath + _bpData.results[j].ProcessStageId + ",";
                                                    break;
                                                }
                                            }
                                            if(msArray[i].ddsm_name == proj_obj["ddsm_PendingMilestone"]) {break;}
                                        }
                                        traversedpath = traversedpath.replace(/,\s*$/, "");

                                        //console.log(traversedpath);
                                        for(var i = 0; i < _bpData.results.length; i++){
                                            if(_bpData.results[i].StageName == proj_obj["ddsm_PendingMilestone"]){
                                                var objProj = {};
                                                objProj["processid"] = BProcessData[0].WorkflowId;
                                                objProj["stageid"] = _bpData.results[i].ProcessStageId;
                                                objProj["traversedpath"] = traversedpath;
                                                    gen_H_updateEntitySync(proj_ID, objProj, "ddsm_projectSet");
                                                break;
                                            }
                                        }
                                    }
                                }

                        }
                    }
                }
            }
        }

    } else {
        alert("Error: " + req.responseText);
    }



    //createDocList_ptojTpl(proj_ID, proj_Name, projTpl_ID);

    var proj_Obj = {};
    proj_Obj.Id = proj_ID;
    proj_Obj.Name = proj_Name;
    proj_Obj.LogicalName = "ddsm_project";


//    createProjMeas_ProjTplMeas(projTpl_ID, proj_Obj, siteProject, accountProject);

    if(objMeasQuickForm.length == 0){
        if(IndexOffsetDocList != null && IndexOffsetDocList.length > 0){updateMSIndexDocList(proj_ID);createDocList_msSet(proj_ID, proj_Name);}
        createFinancial(projTpl_ID, proj_ID, proj_Name, PhaseNumber);
        setTimeout(function () {
            var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
            var features = "location=yes,menubar=no,status=yes,toolbar=yes,resizable=yes";
            var url = "../../main.aspx?etn=ddsm_project&extraqs=&histKey=" + randomnumber + "&id=" + proj_ID + "&newWindow=true&pagetype=entityrecord";
            window.open(url, "_blank");
            $('#overlay').remove();
            if(($("#globalquickcreate_cancel_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document)).length == 1) {
                $("#globalquickcreate_cancel_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document).click();
            }
            if(($("#globalquickcreate_cancel_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document)).length == 1) {
                $("#globalquickcreate_cancel_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document).click();
            }
            if(($("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document)).length == 1) {
                $("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document).click();
            }



            //Xrm.Utility.openEntityForm("ddsm_project", proj_ID);
        }, 1000);
    } else {
        var objProj= {};
        objProj.Id = proj_ID;
        objProj.Name = proj_Name;
        objProj.LogicalName = "ddsm_project";

        createMeasToProj(objMeasQuickForm[objMeasQuickForm_count], objProj, PhaseNumber, projPhase, InitialPhaseDate, projTpl_ID);
    }
    //console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaa');
    //window.parent.Xrm.Page.getControl("WebResource_milestone_grid").getObject().contentWindow.window.nextMilestone("04/01/2016");



    updateLead(proj_ID);
}

// Create Financial to project
function createFinancial(projTpl_ID, proj_ID, proj_Name, PhaseNumber){
    console.log("Create New Financials");

    AGS.REST.retrieveMultipleRecords("ddsm_financialtemplate", "$select=ddsm_financialtemplateId,ddsm_name&$filter=ddsm_ProjectTemplateToFinancialTemplId/Id eq guid'" + projTpl_ID + "' and statecode/Value eq 0", null, function(msg){alert(msg);}, function(data){
        console.dir(data);
        if(data.length > 0){
            for(let i = 0; i < data.length; i++)
            {
                let thisFinancialData = {}, fnclId = data[i].ddsm_financialtemplateId;
                let customMap = AGS.Entity.getTargetMapping(962080003, 'ddsm_financial', ['ddsm_financialtemplate','ddsm_project', 'Account']);
                //console.dir(customMap);
                if (customMap.hasOwnProperty('ddsm_financialtemplate') && !!Object.keys(customMap['ddsm_financialtemplate'])) {
                    AGS.REST.retrieveRecord(fnclId, "ddsm_financialtemplate", Object.keys(customMap['ddsm_financialtemplate']).join(','), null, function (data) {
                        //console.dir(data);
                        let financialTplMap = customMap['ddsm_financialtemplate'];
                        for (let k in financialTplMap) {
                            if (!!data[k]) {
                                thisFinancialData[financialTplMap[k]] = data[k];
                            }
                        }
                    }, null, false);
                };

                if (customMap.hasOwnProperty('Account') && !!Object.keys(customMap['Account'])) {
                    AGS.REST.retrieveRecord(accountProject.Id, "Account", Object.keys(customMap['Account']).join(','), null, function (data) {
                        //console.dir(data);
                        let accountMap = customMap['Account'];
                        for (let k in accountMap) {
                            if (!!data[k]) {
                                thisFinancialData[accountMap[k]] = data[k];
                            }
                        }
                    }, null, false);
                };

                if (customMap.hasOwnProperty('ddsm_project') && !!Object.keys(customMap['ddsm_project'])) {
                    AGS.REST.retrieveRecord(proj_ID, "ddsm_project", Object.keys(customMap['ddsm_project']).join(','), null, function (data) {
                        //console.dir(data);
                        let projectMap = customMap['ddsm_project'];
                        for (let k in projectMap) {
                            if (!!data[k]) {
                                thisFinancialData[projectMap[k]] = data[k];
                            }
                        }
                    }, null, false);
                };

                thisFinancialData["ddsm_projecttofinancialid"] = {};
                thisFinancialData["ddsm_projecttofinancialid"].Id = proj_ID;
                thisFinancialData["ddsm_projecttofinancialid"].Name = proj_Name;
                thisFinancialData["ddsm_projecttofinancialid"].LogicalName = "ddsm_project";

                thisFinancialData["ddsm_financialtemplate"] = {};
                thisFinancialData["ddsm_financialtemplate"].Id = fnclId;
                thisFinancialData["ddsm_financialtemplate"].Name = data[i].ddsm_name;
                thisFinancialData["ddsm_financialtemplate"].LogicalName = "ddsm_financialtemplate";

                thisFinancialData["ddsm_siteid"] = siteProject;
                thisFinancialData["ddsm_accountid"] = accountProject;
                thisFinancialData["ddsm_status"] = {Value: 962080000};

                if(!thisFinancialData["ddsm_duration1"]) thisFinancialData["ddsm_duration1"] = 0;
                if(!thisFinancialData["ddsm_duration2"]) thisFinancialData["ddsm_duration2"] = 0;
                if(!thisFinancialData["ddsm_duration3"]) thisFinancialData["ddsm_duration3"] = 0;
                if(!thisFinancialData["ddsm_duration4"]) thisFinancialData["ddsm_duration4"] = 0;
                if(!thisFinancialData["ddsm_duration5"]) thisFinancialData["ddsm_duration5"] = 0;

                if(!!thisFinancialData["ddsm_initiatingmilestoneindex"]) {
                    var Status = null, TargetStart = null, ActualStart = null, ActualEnd = null;
                    AGS.REST.retrieveMultipleRecords("ddsm_milestone", "$select=ddsm_Status,ddsm_TargetStart,ddsm_ActualStart,ddsm_ActualEnd&$filter=ddsm_ProjectToMilestoneId/Id eq guid'" + proj_ID + "' and ddsm_Index eq " + parseInt(thisFinancialData["ddsm_initiatingmilestoneindex"]) + " and statecode/Value eq 0", null, function(msg){alert(msg);}, function(dataMSs){
                        //console.dir(dataMSs);
                        if(dataMSs.length > 0) {
                            Status = dataMSs[0].ddsm_Status;
                            TargetStart = (!!dataMSs[0].ddsm_TargetStart)?new Date((dataMSs[0].ddsm_TargetStart)).toUTCString():null;
                            ActualStart = (!!dataMSs[0].ddsm_ActualStart)?new Date((dataMSs[0].ddsm_ActualStart).toUTCString()):null;
                            ActualEnd = (!!dataMSs[0].ddsm_ActualEnd)?new Date((dataMSs[0].ddsm_ActualEnd).toUTCString()):null;

                        }
                    }, false, []);
                    if(!!Status && !!Status.Value){
                        var thisStartDate = new Date();
                        thisStartDate = new Date(TargetStart);

                            thisFinancialData["ddsm_targetstart1"] = new Date(thisStartDate.getTime());
                            thisStartDate = new Date(thisStartDate.getTime() + (thisFinancialData["ddsm_duration1"] * 86400000));
                            thisFinancialData["ddsm_targetend1"] = new Date(thisStartDate.getTime());
                        /*
                            if(parseInt(Status.Value) != 962080000 && thisFinancialData["ddsm_status"].Value  != 962080001) {
                                thisFinancialData["ddsm_status"] = {Value: 962080001};
                                if(!!ActualStart) {
                                    thisStartDate = ActualStart;
                                    thisFinancialData["ddsm_actualstart1"] = new Date(thisStartDate.getTime());
                                    if(!ActualEnd){
                                        ActualEnd = new Date();
                                    }
                                    thisStartDate = thisFinancialData["ddsm_actualend1"] = new Date(ActualEnd.getTime());
                                    thisFinancialData["ddsm_actualstart2"] = new Date(thisStartDate.getTime());
                                } else
                                    thisStartDate = new Date(thisStartDate.getTime() + (thisFinancialData["ddsm_duration1"] * 86400000));
                            } else
                                thisStartDate = new Date(thisStartDate.getTime() + (thisFinancialData["ddsm_duration1"] * 86400000));
*/
                        thisFinancialData["ddsm_targetstart2"] = new Date(thisStartDate.getTime());
                        thisStartDate = new Date(thisStartDate.getTime() + (thisFinancialData["ddsm_duration2"] * 86400000));
                        thisFinancialData["ddsm_targetend2"] = new Date(thisStartDate.getTime());

                        thisFinancialData["ddsm_targetstart3"] = new Date(thisStartDate.getTime());
                        thisStartDate = new Date(thisStartDate.getTime() + (thisFinancialData["ddsm_duration3"] * 86400000));
                        thisFinancialData["ddsm_targetend3"] = new Date(thisStartDate.getTime());

                        thisFinancialData["ddsm_targetstart4"] = new Date(thisStartDate.getTime());
                        thisStartDate = new Date(thisStartDate.getTime() + (thisFinancialData["ddsm_duration4"] * 86400000));
                        thisFinancialData["ddsm_targetend4"] = new Date(thisStartDate.getTime());

                        thisFinancialData["ddsm_targetstart5"] = new Date(thisStartDate.getTime());
                        thisStartDate = new Date(thisStartDate.getTime() + (thisFinancialData["ddsm_duration5"] * 86400000));
                        thisFinancialData["ddsm_targetend5"] = new Date(thisStartDate.getTime());

                        var newFnclRecord = AGS.REST.createRecord(thisFinancialData, "ddsm_financial", null, function(msg){alert(msg);}, false);
                        console.dir(newFnclRecord);
                        //debugger;
                        //if(parseInt(Status.Value) != 962080000 && thisFinancialData["ddsm_status"].Value  == 962080001)
                            //calcFinancial(proj_ID, newFnclRecord.ddsm_financialId, PhaseNumber, ActualEnd, thisFinancialData["ddsm_calculationtype"].Value);
                    } else
                    {
                        var newFnclRecord = AGS.REST.createRecord(thisFinancialData, "ddsm_financial", null, function(msg){alert(msg);}, false);
                        //console.dir(newFnclRecord);
                        //debugger;
                    }
                } else
                {
                    var newFnclRecord = AGS.REST.createRecord(thisFinancialData, "ddsm_financial", null, function(msg){alert(msg);}, false);
                    //console.dir(newFnclRecord);
                    //debugger;
                }

            }
        }
    }, false, []);
console.dir(window.createdMesId);
    debugger;
    if(window.createdMesId.length > 0){
        let date = new Date();
        let TaskQueue = {};
        TaskQueue.ddsm_ProcessedItems0 = '{"SmartMeasures":["'+ window.createdMesId.join('","') + '"],"DataFields":null}';
        TaskQueue.ddsm_TaskEntity = {Value:962080000};
        TaskQueue.ddsm_EntityRecordType = {Value:962080000};
        TaskQueue.ddsm_name = "Measures Task Queue - " + ('0' + (date.getUTCMonth() + 1)).slice(-2) + '/' + ('0' + date.getUTCDate()).slice(-2) + '/' + date.getUTCFullYear() + " " + date.getUTCHours() + ":" + date.getUTCMinutes() + ":" + date.getUTCSeconds();
        console.dir(TaskQueue);
        AGS.REST.createRecord(TaskQueue, "ddsm_taskqueue", null, function (err) {console.log(err);}, false);
        window.createdMesId = [];
    }

    /*

        //  Financial Template
        var serverUrl = Xrm.Page.context.getClientUrl();
        req = new XMLHttpRequest();
        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projecttemplateSet?"
            + "$select="
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_name,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_FinancialType,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_PaymentType,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_InitiatingMilestoneIndex,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_StageName1,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_StageName2,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_StageName3,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_StageName4,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_StageName5,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_Duration1,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_Duration2,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_Duration3,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_Duration4,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_Duration5,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_CalculationType,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_ProjectCompletableIndex,"
            + "ddsm_ddsm_projecttemplate_ddsm_financialtemplate/ddsm_financialtemplateId"
            + "&$expand=ddsm_ddsm_projecttemplate_ddsm_financialtemplate"
            + "&$filter=ddsm_projecttemplateId eq guid'" + projTpl_ID + "' and statecode/Value eq 0", false);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.send();

        if (req.readyState == 4 && req.status == 200) {
            //From the project template - will also contain milestone information
            var _tplData = JSON.parse(req.responseText).d;
            if (_tplData.results[0] != null) {
                //Loop the Result Set
                for (var x = 0; x < _tplData.results.length; x++) {

                    if ((_tplData.results[x].ddsm_ddsm_projecttemplate_ddsm_financialtemplate != undefined) &&
                        (_tplData.results[x].ddsm_ddsm_projecttemplate_ddsm_financialtemplate.results != undefined)) {

                        var fncl_array = _tplData.results[x].ddsm_ddsm_projecttemplate_ddsm_financialtemplate.results;

                        for (var i in fncl_array) {
                            if (fncl_array[i].ddsm_name != null) {
                                var fncl_obj = new Object();
                                fncl_obj.ddsm_name = fncl_array[i].ddsm_name;
                                //fncl_obj.ddsm_whogetspaid = new Object();
                                //fncl_obj.ddsm_whogetspaid.Value = pymtPref;
                                fncl_obj.ddsm_projecttofinancialid = new Object();
                                fncl_obj.ddsm_projecttofinancialid.Id = proj_ID;
                                fncl_obj.ddsm_projecttofinancialid.Name = proj_Name;
                                fncl_obj.ddsm_projecttofinancialid.LogicalName = "ddsm_project";

                                fncl_obj.ddsm_financialtype = new Object();
                                fncl_obj.ddsm_financialtype.Value = fncl_array[i].ddsm_FinancialType.Value;

                                fncl_obj.ddsm_status = new Object();
                                fncl_obj.ddsm_status.Value = 962080000;

                                fncl_obj.ddsm_initiatingmilestoneindex = fncl_array[i].ddsm_InitiatingMilestoneIndex + IdxOffSet;
                                fncl_obj.ddsm_stagename1 = fncl_array[i].ddsm_StageName1;
                                fncl_obj.ddsm_stagename2 = fncl_array[i].ddsm_StageName2;
                                fncl_obj.ddsm_stagename3 = fncl_array[i].ddsm_StageName3;
                                fncl_obj.ddsm_stagename4 = fncl_array[i].ddsm_StageName4;
                                fncl_obj.ddsm_stagename5 = fncl_array[i].ddsm_StageName5;
                                fncl_obj.ddsm_duration1 = fncl_array[i].ddsm_Duration1;
                                fncl_obj.ddsm_duration2 = fncl_array[i].ddsm_Duration2;
                                fncl_obj.ddsm_duration3 = fncl_array[i].ddsm_Duration3;
                                fncl_obj.ddsm_duration4 = fncl_array[i].ddsm_Duration4;
                                fncl_obj.ddsm_duration5 = fncl_array[i].ddsm_Duration5;
                                fncl_obj.ddsm_calculationtype = fncl_array[i].ddsm_CalculationType;
                                fncl_obj.ddsm_projectcompletableindex = fncl_array[i].ddsm_ProjectCompletableIndex;

                                fncl_obj.ddsm_financialtemplate = new Object();
                                fncl_obj.ddsm_financialtemplate.Id = fncl_array[i].ddsm_financialtemplateId;
                                fncl_obj.ddsm_financialtemplate.Name = fncl_array[i].ddsm_name;
                                fncl_obj.ddsm_financialtemplate.LogicalName = "ddsm_financialtemplate";

                                fncl_obj.ddsm_siteid = siteProject;
                                fncl_obj.ddsm_accountid = accountProject;

                                gen_H_createEntitySync(fncl_obj, "ddsm_financialSet");
                            }
                        }
                    }
                }
                Fncl_UpdateFncls_onAccount(proj_ID, 0);
            }
        }
        */
}

function calcFinancial(proj_ID, fncl_ID, PhaseNumber, ActualEnd, CalcType) {
    let thisFinancialData = {};
    let customMap = AGS.Entity.getTargetMapping(962080003, 'ddsm_financial', ['ddsm_project']);
    if (customMap.hasOwnProperty('ddsm_project') && !!Object.keys(customMap['ddsm_project'])) {
        AGS.REST.retrieveRecord(proj_ID, "ddsm_project", Object.keys(customMap['ddsm_project']).join(','), null, function (data) {
            //console.dir(data);
            let projectMap = customMap['ddsm_project'];
            for (let k in projectMap) {
                if (!!data[k]) {
                    thisFinancialData[projectMap[k]] = data[k];
                }
            }
        }, null, false);
    };
//debugger;
    if(CalcType == 962080009) {
    let fetch = "";
    fetch += '<fetch aggregate="true" >';
    fetch += '<entity name="ddsm_measure" >';
    fetch += '<attribute name="ddsm_incentivepaymentnet" alias="incPaymentNet" aggregate="sum" />';
    fetch += '<attribute name="ddsm_incentivepaymentnetdsm" alias="incPaymentNetDSM" aggregate="sum" />';
    fetch += '<filter type="and" >';
    fetch += '<condition attribute="ddsm_projecttomeasureid" operator="eq" value="' + proj_ID + '" />';
    fetch += '<condition attribute="ddsm_uptodatephase" operator="eq" value="' + PhaseNumber + '" />';
    fetch += '</filter>';
    fetch += '</entity>';
    fetch += '</fetch>';

    var fetchData = XrmServiceToolkit.Soap.Fetch(fetch);
    if (fetchData != null) {

        if (!!fetchData[0].attributes.incPaymentNet.value) {
            thisFinancialData["ddsm_actualamount"] = {};
            thisFinancialData["ddsm_actualamount"].Value = parseFloat(fetchData[0].attributes.incPaymentNet.value).toFixed(6);
            thisFinancialData["ddsm_calculatedamount"] = {};
            thisFinancialData["ddsm_calculatedamount"].Value = parseFloat(fetchData[0].attributes.incPaymentNet.value).toFixed(6);
        }
        if (!!fetchData[0].attributes.incPaymentNetDSM.value) {
            thisFinancialData["ddsm_IncentivesCustomerRebatesDSMAmount"] = {};
            thisFinancialData["ddsm_IncentivesCustomerRebatesDSMAmount"].Value = parseFloat(fetchData[0].attributes.incPaymentNetDSM.value).toFixed(6);
            thisFinancialData["ddsm_CC1_Amount"] = {};
            thisFinancialData["ddsm_CC1_Amount"].Value = parseFloat(fetchData[0].attributes.incPaymentNetDSM.value).toFixed(6);
        }

    }

    thisFinancialData["ddsm_IncentivesCustomerRebatesDSMProgram"] = {};
    thisFinancialData["ddsm_IncentivesCustomerRebatesDSMProgram"].Value = 962080006;
    thisFinancialData["ddsm_CC1_CostDate"] = ActualEnd;
        thisFinancialData["ddsm_CC1_ProgramCode"] = {};
        thisFinancialData["ddsm_CC1_ProgramCode"].Value = 962080006;
        thisFinancialData["ddsm_CC1_ProgramCodeDescription"] = {};
        thisFinancialData["ddsm_CC1_ProgramCodeDescription"].Value = 962080006;
}
    AGS.REST.updateRecord(fncl_ID, thisFinancialData, "ddsm_financial", null, null, false);
}

function updateLead(proj_ID) {
    var leadId = window.parent.top.leadId;
    var siteMeasures = window.parent.top.leadSiteMeasureList;

    var error_handler = function (error){
        console.log(error);
    };

    if(leadId){
        // Update Site Measures (Opportunity), set Parent Company, Parent Site, Parent Project
        if(siteMeasures && siteMeasures.length>0){
            siteMeasures.forEach(function(sm){
                var smObj = {
                    ddsm_parentsite: {
                        Id: Xrm.Page.getAttribute("ddsm_parentsiteid").getValue()[0].id,
                        LogicalName:"ddsm_site"
                    },
                    ddsm_AccountId: {
                        Id: Xrm.Page.getAttribute("ddsm_accountid").getValue()[0].id,
                        LogicalName:"account"
                    },
                    ddsm_ProjectToMeasureId: {
                        Id: proj_ID,
                        LogicalName:"ddsm_project"
                    },
                    ddsm_MeasureStatus:{
                        Value: 962080001
                    }
                };
                AGS.REST.updateRecord(sm,smObj,'ddsm_sitemeasure',null,error_handler,true );
            });
        }



        // Update Lead, set ddsm_convertedtoproject = true
        var leadobj = {ddsm_ConvertedToProject: "true"};

        AGS.REST.updateRecord(leadId,leadobj,'Lead',null,error_handler,true );

    }

}

function updateMSIndexDocList(proj_ID){
    var serverUrl = Xrm.Page.context.getClientUrl();
    var req = new XMLHttpRequest();
    var URL = serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
        + "$select="
        + "ddsm_documentconventionId,"
        + "ddsm_RequiredByStatus"
        + "&$orderby=ddsm_RequiredByStatus asc"
        + "&$filter=ddsm_ProjectId/Id eq guid'" + proj_ID + "'";
    req.open("GET", URL, false);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();

    if (req.readyState == 4 && req.status == 200) {
        try {
            var obj = (JSON.parse(req.responseText).d).results;
            if(obj != null && obj.length > 0){
                for(var i = 0; i < IndexOffsetDocList.length; i++){
                    for(var j = 0; j < obj.length; j++){
                        if(IndexOffsetDocList[i].ddsm_insertbeforeindex == obj[j].ddsm_RequiredByStatus){
                            for(var l = j; l < obj.length; l++){
                                obj[l].ddsm_RequiredByStatus = obj[l].ddsm_RequiredByStatus + IndexOffsetDocList[i].ddsm_numberofsets;
                            }
                            break;
                        }
                    }
                }
                for(var j = 0; j < obj.length; j++){
                    gen_H_updateEntitySync(obj[j].ddsm_documentconventionId, {ddsm_RequiredByStatus: obj[j].ddsm_RequiredByStatus}, "ddsm_documentconventionSet");
                }
            }

        } catch (err) { alert(err); }
    } else {
        alert("Error: " + req.responseText);
    }
}

function createDocList_msSet(proj_ID, proj_Name){

    var docProjList = null;
    var _req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    _req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
    + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
    + "&$filter=ddsm_ProjectId/Id eq guid'" + proj_ID + "'", false);
    _req.setRequestHeader("Accept", "application/json");
    _req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    _req.send();
    if (_req.readyState == 4 && _req.status == 200) {
        var _tplData = JSON.parse(_req.responseText).d;
        if (_tplData.results[0] != null) {
            docProjList = _tplData.results;
        }
    }
    for(var j = 0; j < IndexOffsetDocList.length; j++) {
        var req = new XMLHttpRequest();
        req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_documentconventionSet?"
        + "$select=ddsm_RequiredByStatus,ddsm_name,ddsm_Uploaded"
        + "&$filter=ddsm_MilestoneSet/Id eq guid'" + IndexOffsetDocList[j].setId + "'", false);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.send();
        if (req.readyState == 4 && req.status == 200) {
            var _tplData = JSON.parse(req.responseText).d;
            if (_tplData.results[0] != null) {
                for (var x = 0; x < _tplData.results.length; x++) {
                    var addDocConv = true;
                    if (docProjList !== null) {
                        for (var i = 0; i < docProjList.length; i++) {
                            if (_tplData.results[x].ddsm_name == docProjList[i].ddsm_name) {
                                addDocConv = false;
                                break;
                            }
                        }
                    }
                    if (addDocConv) {
                        var proj_obj = {};
                        if (_tplData.results[x].ddsm_RequiredByStatus != null) {
                            proj_obj["ddsm_RequiredByStatus"] = parseInt(_tplData.results[x].ddsm_RequiredByStatus) + IndexOffsetDocList[j].ddsm_insertbeforeindex - 1;
                        }
                        if (_tplData.results[x].ddsm_name != null) {
                            proj_obj["ddsm_name"] = _tplData.results[x].ddsm_name;
                        }
                        if (_tplData.results[x].ddsm_Uploaded != null) {
                            proj_obj["ddsm_Uploaded"] = _tplData.results[x].ddsm_Uploaded;
                        }

                        proj_obj.ddsm_ProjectId = new Object();
                        proj_obj.ddsm_ProjectId.Id = proj_ID;
                        proj_obj.ddsm_ProjectId.Name = proj_Name;
                        proj_obj.ddsm_ProjectId.LogicalName = "ddsm_project";

                        //console.dir(proj_obj);

                        gen_H_createEntitySync(proj_obj, "ddsm_documentconventionSet");
                    }
                }
            }
        }
    }
}

function createMeasToProj(objOneMeasQuickForm, objProj, PhaseNumber, PtojectPhaseName, InitialPhaseDate, projTpl_ID){
    //console.dir(objOneMeasQuickForm);
    //console.dir(objProj);
    //console.dir(PhaseNumber);
    //console.dir(PtojectPhaseName);
    //console.dir(InitialPhaseDate);
    createMeasForTpl(objOneMeasQuickForm.MeasTpl, objOneMeasQuickForm.Quantity, objProj, PhaseNumber, PtojectPhaseName, InitialPhaseDate, projTpl_ID);
}

function createMeasForTpl(objMeasTpl, units, objProj, PhaseNumber, PtojectPhaseName, InitialPhaseDate, projTpl_ID){

    //createDocList_measTpl(objProj.Id, objProj.Name, objMeasTpl.Id);

    var measTpl_ID = objMeasTpl.Id;
    var now = new Date();
    try {

        var thisMeasureData = {};
        let customMap = AGS.Entity.getTargetMapping(null, null, ['ddsm_measuretemplate','Account','ddsm_site','ddsm_project','ddsm_rateclassreference']);
        //console.dir(customMap);
        if (customMap.hasOwnProperty('ddsm_measuretemplate') && !!Object.keys(customMap['ddsm_measuretemplate'])) {
            AGS.REST.retrieveRecord(objMeasTpl.Id, "ddsm_measuretemplate", Object.keys(customMap['ddsm_measuretemplate']).join(','), null, function (data) {
                //console.dir(data);
                let measureTplMap = customMap['ddsm_measuretemplate'];
                for (let k in measureTplMap) {
                    if (!!data[k]) {
                        thisMeasureData[measureTplMap[k]] = data[k];
                    }
                }
            }, null, false);
        }
        //console.dir(thisMeasureData);
        if (customMap.hasOwnProperty('Account') && !!Object.keys(customMap['Account'])) {
            AGS.REST.retrieveRecord(accountProject.Id, "Account", Object.keys(customMap['Account']).join(','), null, function (data) {
                //console.dir(data);
                let AccountMap = customMap['Account'];
                for (let k in AccountMap) {
                    if (!!data[k]) {
                        thisMeasureData[AccountMap[k]] = data[k];
                    }
                }
            }, null, false);
        }
        //console.dir(thisMeasureData);
        if (customMap.hasOwnProperty('ddsm_site') && !!Object.keys(customMap['ddsm_site'])) {
            AGS.REST.retrieveRecord(siteProject.Id, "ddsm_site", Object.keys(customMap['ddsm_site']).join(','), null, function (data) {
                //console.dir(data);
                let siteMap = customMap['ddsm_site'];
                for (let k in siteMap) {
                    if (!!data[k]) {
                        thisMeasureData[siteMap[k]] = data[k];
                    }
                }
            }, null, false);
        }
        //console.dir(thisMeasureData);
        if (customMap.hasOwnProperty('ddsm_project') && !!Object.keys(customMap['ddsm_project'])) {
            AGS.REST.retrieveRecord(objProj.Id, "ddsm_project", Object.keys(customMap['ddsm_project']).join(','), null, function (data) {
                //console.dir(data);
                let projectMap = customMap['ddsm_project'];
                for (let k in projectMap) {
                    if (!!data[k]) {
                        thisMeasureData[projectMap[k]] = data[k];
                    }
                }
            }, null, false);
        }
        //console.dir(thisMeasureData);

        thisMeasureData["ddsm_Units"] = parseFloat(units).toFixed(6);
        thisMeasureData["ddsm_MeasureSelector"] = objMeasTpl;
        thisMeasureData["ddsm_AccountId"] = accountProject;
        thisMeasureData["ddsm_parentsite"] = siteProject;
        thisMeasureData["ddsm_ProjectToMeasureId"] = objProj;

        if(typeof taProject.Id !== 'undefined') {
            thisMeasureData["ddsm_TradeAllyId"] = taProject;
        }
        var InitialLead = getInitialLeadSite(siteProject.Id);
        if(InitialLead != null) {thisMeasureData["ddsm_InitialLead"] = InitialLead;}


        var now = new Date();
        now = InitialPhaseDate;
        //thisMeasureData["ddsm_name"] = (objProj.Name).substr(0, 6) + "-" + objMeasTpl.Name + "_" + now.getFullYear() + now.getMonth() + now.getDate() + "-" + now.getHours() + now.getMinutes() + now.getSeconds();
        thisMeasureData["ddsm_name"] = objMeasTpl.Name;
        //thisMeasureData["ddsm_name"] = objMeasTpl.Name + "-" + now.getFullYear() + now.getMonth() + now.getDate() + "-" + now.getHours() + now.getMinutes() + now.getSeconds();
        thisMeasureData["ddsm_InitialPhaseDate"] = now;
        console.dir(thisMeasureData);

        AGS.REST.retrieveMultipleRecords("ddsm_measuretemplate", "$select=ddsm_ddsm_measurecalculationtemplate_ddsm_measur&$expand=ddsm_ddsm_measurecalculationtemplate_ddsm_measur&$filter=ddsm_measuretemplateId eq guid'" + objMeasTpl.Id + "' and statecode/Value eq 0", null, null, function(data) {
            let measCalcTpl_Data = data[0].ddsm_ddsm_measurecalculationtemplate_ddsm_measur;
            console.dir(measCalcTpl_Data);
            let measCalcTpl_CalcTypeValues = "";
            if (!!measCalcTpl_Data) {
            if (measCalcTpl_Data.ddsm_CalculateSavings) measCalcTpl_CalcTypeValues += "Calculate Savings; ";
            if (measCalcTpl_Data.ddsm_CalculateIncentive) measCalcTpl_CalcTypeValues += "Calculate Incentives; ";
            if (measCalcTpl_Data.ddsm_PerUnitLock) measCalcTpl_CalcTypeValues += "Lock Per Units; ";
            if (measCalcTpl_Data.ddsm_SavingsLock) measCalcTpl_CalcTypeValues += "Lock Savings; ";
            if (measCalcTpl_Data.ddsm_IncentiveRatesLock) measCalcTpl_CalcTypeValues += "Lock Incentive Rates; ";
            if (measCalcTpl_Data.ddsm_IncentivesLock) measCalcTpl_CalcTypeValues += "Lock Incentives; ";
        }
            thisMeasureData["ddsm_CalculationTypeValues"] = measCalcTpl_CalcTypeValues;
        }, false, []);

        //Sets fields to 0 values
        thisMeasureData["ddsm_IncentivePaymentGross"] = {Value: parseFloat("0.0").toFixed(6)};
        thisMeasureData["ddsm_IncentivePaymentElectricGross"] = {Value: parseFloat("0.0").toFixed(6)};
        thisMeasureData["ddsm_IncentivePaymentGasGross"] = {Value: parseFloat("0.0").toFixed(6)};
        thisMeasureData["ddsm_IncentivePaymentTotalGross"] = {Value: parseFloat("0.0").toFixed(6)};


        var calcSavings = false;
        var calcIncentives = false;

        if (thisMeasureData["ddsm_CalculationTypeValues"] != null) {
            if ((thisMeasureData["ddsm_CalculationTypeValues"].indexOf("Calculate Savings")) != -1) {
                calcSavings = true;
            }
            if ((thisMeasureData["ddsm_CalculationTypeValues"].indexOf("Calculate Incentives")) != -1) {
                calcIncentives = true;
            }
        }
        if (!units) units = 0;
        //Calculates savings if calculation type is set to do so.
        if (calcSavings) {
            var perUnitkWh = thisMeasureData["ddsm_kWhGrossSavingsatMeterperunit"];
            var perUnitKW = thisMeasureData["ddsm_kWGrossSavingsatMeterperunit"];
            var perUnitthm = thisMeasureData["ddsm_M3SavingsperUnit"];
            /*Demo Enbr*/
            var perUnitwater= thisMeasureData["ddsm_WaterSavingsperUnit"];
            var perUnitgj= thisMeasureData["ddsm_GJSavingsatMeterperunit"];
            var perUnitIncCostMeas= thisMeasureData["ddsm_IncentiveCostMeasurePerUnit"].Value;
            /*Demo Enbr*/

            if (perUnitkWh == null) {
                perUnitkWh = 0;
            }
            if (perUnitKW == null) {
                perUnitKW = 0;
            }
            if (perUnitthm == null) {
                perUnitthm = 0;
            }
            /*Demo Enbr*/
            if (perUnitwater == null) {
                perUnitwater = 0;
            }
            if (perUnitgj == null) {
                perUnitgj = 0;
            }
            if (perUnitIncCostMeas == null) {
                perUnitIncCostMeas = 0;
            }
            /*Demo Enbr*/
            var savingskWh;
            var savingsKW;
            var savingsthm;
            /*Demo Enbr*/
            var savingswater;
            var savingsgj;
            var inccostmeas;
            /*Demo Enbr*/

            //Savings calculations
            savingskWh = (units * perUnitkWh);
            savingsKW = (units * perUnitKW);
            savingsthm = (units * perUnitthm);
            /*Demo Enbr*/
            savingswater = (units * perUnitwater);
            savingsgj = (units * perUnitgj);
            inccostmeas = (units * perUnitIncCostMeas);
            /*Demo Enbr*/

            //Sets fields to savings values
            thisMeasureData["ddsm_kWhGrossSavingsatMeter"] = parseFloat(savingskWh).toFixed(6);
            thisMeasureData["ddsm_kWGrossSavingsatMeter"] = parseFloat(savingsKW).toFixed(6);
            thisMeasureData["ddsm_M3Savings"] = parseFloat(savingsthm).toFixed(6);
            /*Demo Enbr*/
            thisMeasureData["ddsm_WaterSavings"] = parseFloat(savingswater).toFixed(6);
            thisMeasureData["ddsm_GJSavingsatMeter"] = parseFloat(savingsgj).toFixed(6);
            thisMeasureData["ddsm_IncentiveCostMeasure"] = {Value: parseFloat(inccostmeas).toFixed(6)};
            /*Demo Enbr*/

        }

        //Calculates incentives if calculation type is set to do so.
        if (calcIncentives) {
            var incRateUnits = thisMeasureData["ddsm_IncentivePaymentGrossPerUnit"].Value;
            var incRatekWh = thisMeasureData["ddsm_IncRatekWh"].Value;
            var incRateKW = thisMeasureData["ddsm_IncRateKW"].Value;
            var incRatethm = thisMeasureData["ddsm_IncRatethm"].Value;

            if (incRateUnits == null) {
                incRateUnits = 0;
            }
            if (incRatekWh == null) {
                incRatekWh = 0;
            }
            if (incRateKW == null) {
                incRateKW = 0;
            }
            if (incRatethm == null) {
                incRatethm = 0;
            }
            var savingskWh = thisMeasureData["ddsm_kWhGrossSavingsatMeter"];
            var savingsKW = thisMeasureData["ddsm_kWGrossSavingsatMeter"];

            //Comment because field status "ddsm_phase1savingsthm" = deleted in E1 Schema
            var savingsthm = thisMeasureData["ddsm_M3Savings"];

            if (savingskWh == null) {
                savingskWh = 0;
            }
            if (savingsKW == null) {
                savingsKW = 0;
            }
            if (savingsthm == null) {
                savingsthm = 0;
            }
            var incUnits;
            var inckWh;
            var incKW;
            var incthm;
            var incElectric;
            var incGas;
            var incTotal;

            //Incentive calculations
            incUnits = (units * incRateUnits);
            inckWh = (savingskWh * incRatekWh);
            incKW = (savingsKW * incRateKW);
            incthm = (savingsthm * incRatethm);
            incElectric = (inckWh + incKW);
            incGas = incthm;
            incTotal = (incElectric + incGas + incUnits);

            //Sets fields to incentive values
            thisMeasureData["ddsm_IncentivePaymentGross"] = {Value: parseFloat(incUnits).toFixed(6)};

            thisMeasureData["ddsm_IncentivePaymentElectricGross"] = {Value: parseFloat(incElectric).toFixed(6)};

            thisMeasureData["ddsm_IncentivePaymentGasGross"] = {Value: parseFloat(incGas).toFixed(6)};

            thisMeasureData["ddsm_IncentivePaymentTotalGross"] = {Value: parseFloat(incTotal).toFixed(6)};
        }

        var incTotal = thisMeasureData["ddsm_IncentivePaymentElectricGross"].Value + thisMeasureData["ddsm_IncentivePaymentGasGross"].Value + thisMeasureData["ddsm_IncentivePaymentGross"].Value;
        thisMeasureData["ddsm_IncentivePaymentTotalGross"] = {Value: parseFloat(incTotal).toFixed(6)};

        //Sends phase values to current fields

        console.dir(thisMeasureData);
        var phase1units = units;
        var phase1savingskWh = thisMeasureData["ddsm_kWhGrossSavingsatMeter"];
        var phase1savingsKW = thisMeasureData["ddsm_kWGrossSavingsatMeter"];

        var phase1savingsthm = thisMeasureData["ddsm_M3Savings"];
        //Comment because field status "ddsm_phase1savingsthm" = deleted in E1 Schema
        //var phase1savingsthm = record.get("ddsm_phase1savingsthm");
        var phase1incTotal = thisMeasureData["ddsm_IncentivePaymentTotalGross"].Value;

        var eul = thisMeasureData["ddsm_EUL"];
        var lifecycleKW = 0.0;
        var lifecyclekWh = 0.0;
        var lifecyclethm = 0.0;

        if (phase1savingskWh == null) {
            phase1savingskWh = 0;
        }
        if (phase1savingsKW == null) {
            phase1savingsKW = 0;
        }
        if (phase1savingsthm == null) {
            phase1savingsthm = 0;
        }
        if (eul == null) {
            eul = 0;
        }
        lifecycleKW = (phase1savingsKW * eul);
        lifecyclekWh = (phase1savingskWh * eul);
        lifecyclethm = (phase1savingsthm * eul);


        thisMeasureData["ddsm_lifecyclesavingskw"] = parseFloat(lifecycleKW).toFixed(6);
        thisMeasureData["ddsm_lifecyclesavingskwh"] = parseFloat(lifecyclekWh).toFixed(6);
        thisMeasureData["ddsm_lifecyclesavingsthm"] = parseFloat(lifecyclethm).toFixed(6);

        console.dir(thisMeasureData);
        var newGuid = AGS.REST.createRecord(thisMeasureData, "ddsm_measure", null, function(msg){console.log(msg);}, false);
        window.createdMesId.push(newGuid.ddsm_measureId);

                objMeasQuickForm_length--;
                objMeasQuickForm_count++;
                if(objMeasQuickForm_count < objMeasQuickForm_length2 && objMeasQuickForm_length != 0) {
                    createMeasToProj(objMeasQuickForm[objMeasQuickForm_count], objProj, PhaseNumber, PtojectPhaseName, InitialPhaseDate, projTpl_ID);
                } else {
                    createFinancial(projTpl_ID, objProj.Id, objProj.Name, PhaseNumber);
                    setTimeout(function () {
                        //document.getElementById('WebResource_measure_grid_site').contentWindow.reloadGrid();
                        var randomnumber = 100000000 + Math.floor(Math.random() * 900000000);
                        var features = "location=yes,menubar=no,status=yes,toolbar=yes,resizable=yes";
                        var url = "../../main.aspx?etn=ddsm_project&extraqs=&histKey=" + randomnumber + "&id=" + objProj.Id + "&newWindow=true&pagetype=entityrecord";
                        window.open(url, "_blank");
                        $('#overlay').remove();
                        if(($("#globalquickcreate_cancel_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document)).length == 1) {
                            $("#globalquickcreate_cancel_button_Cycle_Projects_gridcontrol_quickcreate", window.parent.document).click();
                        }
                        if(($("#globalquickcreate_cancel_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document)).length == 1) {
                            $("#globalquickcreate_cancel_button_crmGrid_ddsm_account_ddsm_project_Partner_gridcontrol_quickcreate", window.parent.document).click();
                        }
                        if(($("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document)).length == 1) {
                            $("#globalquickcreate_cancel_button_NavBarGloablQuickCreate", window.parent.document).click();
                        }

                        //Xrm.Utility.openEntityForm("ddsm_project", proj_ID);
                    }, 1000);
                }



    } catch (err) {
        console.log(err);
        alert("Error generate Measure with Measure Template & Measure Calculation Template for New Project. " + err);
    }
}

function Fncl_UpdateFncls_onAccount(projId, CMS) {
    //console.log("projId: " + projId);
    var req = new XMLHttpRequest();
    var serverUrl = Xrm.Page.context.getClientUrl();
    //Query if this ID is already in use
    req.open("GET", serverUrl + "/xrmservices/2011/OrganizationData.svc/ddsm_projectSet?"
    + "$select=ddsm_name,"
    + "ddsm_ddsm_project_ddsm_milestone/ddsm_TargetEnd,"
    + "ddsm_ddsm_project_ddsm_milestone/ddsm_EstimatedEnd,"
    + "ddsm_ddsm_project_ddsm_milestone/ddsm_ActualEnd,"
    + "ddsm_ddsm_project_ddsm_milestone/ddsm_Index,"
    + "ddsm_ddsm_project_ddsm_milestone/ddsm_name,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_financialId,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_initiatingmilestoneindex,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_name,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_status,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_stagename1,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_stagename2,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_stagename3,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_stagename4,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_stagename5,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_duration1,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_duration2,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_duration3,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_duration4,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_duration5,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_actualend1,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_actualend2,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_actualend3,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_actualend4,"
    + "ddsm_ddsm_project_ddsm_financial/ddsm_actualend5"
    + "&$filter=ddsm_projectId eq guid'" + projId + "'"
    + "&$expand=ddsm_ddsm_project_ddsm_milestone,ddsm_ddsm_project_ddsm_financial", false);

    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.send();

    if (req.readyState == 4 && req.status == 200) {
        var proj = JSON.parse(req.responseText).d;
        if (proj.results[0] != null) {
            var Fncls = proj.results[0].ddsm_ddsm_project_ddsm_financial.results;
            var MSs = proj.results[0].ddsm_ddsm_project_ddsm_milestone.results;
            //                console.dir(Fncls);
            //                console.dir(MSs);

            var initMS; //first this will be the starting index
            var initMSJson = -1; //This is the index of the Init MS in the JSON object
            var startDate = null;
            var thisStartDate = null;

            if (Fncls.length > 0) {
                for (var i in Fncls) {
                    //alert("Fncl Name: " + Fncls[i].ddsm_name);
                    initMS = Fncls[i].ddsm_initiatingmilestoneindex;
                    var actStart = null;
                    for (var j in MSs) {
                        //alert("MS Name: " + MSs[i].ddsm_name);
                        //console.log("initMS: " + initMS + "| MSs[" + j + "].ddsm_Index: " + MSs[j].ddsm_Index);
                        if (parseInt(initMS) == parseInt(MSs[j].ddsm_Index)) {
                            initMSJson = j;
                            //console.log(MSs[j].ddsm_ActualEnd);
                            if (MSs[j].ddsm_ActualEnd != null) {
                                //console.log(">>>> ddsm_ActualEnd");
                                //startDate = new Date(parseInt((MSs[j].ddsm_ActualEnd).substr(6)));
                                actStart = eval((MSs[j].ddsm_ActualEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                startDate = eval((MSs[j].ddsm_ActualEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                break;
                            } else if (MSs[j].ddsm_EstimatedEnd != null) {
                                //console.log(">>>> ddsm_EstimatedEnd");
                                //startDate = new Date(parseInt((MSs[j].ddsm_EstimatedEnd).substr(6)));
                                startDate = eval((MSs[j].ddsm_EstimatedEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                break;
                            } else if (MSs[j].ddsm_TargetEnd != null) {
                                //console.log(">>>> ddsm_TargetEnd");
                                //startDate = new Date(parseInt((MSs[j].ddsm_TargetEnd).substr(6)));
                                startDate = eval((MSs[j].ddsm_TargetEnd).replace(/\/Date\((\d+)\)\//gi, 'UTCToLocalTime(new Date($1))'));
                                break;
                            } else {
                                alert("Error Updating Financial: " + Fncls[i].ddsm_name +
                                "\nInitiating MS Target Date is missing");
                                return;
                            }
                        }
                    }
                    if (initMSJson == -1) {
                        alert("Error Updating Financial:" + Fncls[i].ddsm_name +
                        "\nCannot find initiating milestone (index=" + initMS + ")");
                        continue;
                    }
                    if (typeof Fncls[i].ddsm_financialId != 'undefined') {
                        var fncl_obj = new Object();

                        if (Fncls[i].ddsm_stagename1 != null) {
                            if (Fncls[i].ddsm_initiatingmilestoneindex <= CMS && Fncls[i].ddsm_status.Value == 962080000 &&
                                actStart != null) { //only run if CMS is past Init MS AND Status == Not Started
                                fncl_obj.ddsm_status = new Object();
                                fncl_obj.ddsm_status.Value = 962080001; // setting status = Active
                                fncl_obj.ddsm_pendingstage = Fncls[i].ddsm_stagename1;
                                fncl_obj.ddsm_actualstart1 = actStart;
                            }
                            //console.log(startDate);
                            fncl_obj.ddsm_targetstart1 = new Date(startDate.getTime());
                            thisStartDate = new Date(startDate.getTime() + (Fncls[i].ddsm_duration1 * 86400000));
                            fncl_obj.ddsm_targetend1 = new Date(thisStartDate.getTime());
                        }
                        if (Fncls[i].ddsm_stagename2 != null) {
                            if (Fncls[i].ddsm_actualend1 != null) {
                                thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend1).substr(6)));
                                //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                            }
                            fncl_obj.ddsm_targetstart2 = new Date(thisStartDate.getTime());
                            thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration2 * 86400000));
                            fncl_obj.ddsm_targetend2 = new Date(thisStartDate.getTime());
                        }
                        if (Fncls[i].ddsm_stagename3 != null) {
                            if (Fncls[i].ddsm_actualend2 != null) {
                                thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend2).substr(6)));
                                //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                            }
                            fncl_obj.ddsm_targetstart3 = new Date(thisStartDate.getTime());
                            thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration3 * 86400000));
                            fncl_obj.ddsm_targetend3 = new Date(thisStartDate.getTime());
                        }
                        if (Fncls[i].ddsm_stagename4 != null) {
                            if (Fncls[i].ddsm_actualend3 != null) {
                                thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend3).substr(6)));
                                //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                            }
                            fncl_obj.ddsm_targetstart4 = new Date(thisStartDate.getTime());
                            thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration4 * 86400000));
                            fncl_obj.ddsm_targetend4 = new Date(thisStartDate.getTime());
                        }
                        if (Fncls[i].ddsm_stagename5 != null) {
                            if (Fncls[i].ddsm_actualend4 != null) {
                                thisStartDate = new Date(parseInt((Fncls[i].ddsm_actualend4).substr(6)));
                                //thisStartDate = new Date(thisStartDate.getTime() + (thisStartDate.getTimezoneOffset() * 60000));
                            }
                            fncl_obj.ddsm_targetstart5 = new Date(thisStartDate.getTime());
                            thisStartDate = new Date(thisStartDate.getTime() + (Fncls[i].ddsm_duration5 * 86400000));
                            fncl_obj.ddsm_targetend5 = new Date(thisStartDate.getTime());
                        }
                        gen_H_updateEntitySync(Fncls[i].ddsm_financialId, fncl_obj, "ddsm_financialSet");
                        // Call function to update using ODATA call
                        //alert("It works!");
                    }
                }
            }
        } else {
            alert("No results found (query successful)");
        }
    } else {
        alert("Error: Fncl_UpdateFncls failed query.\n" + req.responseText);
        return false;
    }
    return true;
}

function UTCToLocalTime(d) {
    //var timeOffset = -((new Date()).getTimezoneOffset() / 60);
    //d.setHours(d.getHours() + timeOffset);
    return d;
}
function dateToMDY(date) {
    var d = date.getUTCDate();
    var m = date.getUTCMonth() + 1;
    var y = date.getUTCFullYear();
    return (m <= 9 ? '0' + m : m) + '/' + (d <= 9 ? '0' + d : d) + '/' + y;
}

function dateToMDYLocal(date) {
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    return (m <= 9 ? '0' + m : m) + '/' + (d <= 9 ? '0' + d : d) + '/' + y;
}

function setMeasFieldsFromSite(siteId){
    var fsFromImplSite = 'ddsm_RateClass,ddsm_sqft,ddsm_climatezone,ddsm_BuildingType,ddsm_RateCodeNameId,ddsm_RateCode,ddsm_DemandUnit,ddsm_Utility'; //var fsFromImplSite = 'ddsm_RateClass,ddsm_sqft,ddsm_climatezone,ddsm_buildingtype';
    window.objMeasSiteFields = AGS.REST.retrieveRecord(siteId,'ddsm_site',fsFromImplSite,null,null,null,false);
    setMeasFieldsFromRateClassReference(window.objMeasSiteFields.ddsm_RateCodeNameId.Id);
}

function setFieldsFromObj(obj,structIn) {
    if(!obj) obj = {};
    var structOutNames = {ddsm_buildingtype:'ddsm_BuildingType'};

    for(var el in structIn){
        if(el.slice(0,2)=='__'){ continue;}
        var fName = structOutNames.hasOwnProperty(el) ? structOutNames[el] : el;
        var val = structIn[el];
        obj[fName] = (typeof(val)=='number' && val==val)?  String(val) : val;
    }
    return obj;
}

function OnChangeProgramOffering() {
    if(!!Xrm.Page.getAttribute("ddsm_programoffering") && !!Xrm.Page.getAttribute("ddsm_programoffering").getValue())
    {
        var ProgramOffering = Xrm.Page.getAttribute('ddsm_programoffering').getValue();
        var progOffering_Name = ProgramOffering[0].name;

        if(!(progOffering_Name.indexOf("Green Heat") + 1))
        {
            Xrm.Page.ui.tabs.getByName('tab_1').getSections().getByName('sect_MeasureTemplate').setVisible(true);
            Xrm.Page.ui.tabs.getByName('tab_1').getSections().getByName('sect_PayeeFields').setVisible(true);
        } else {
            Xrm.Page.ui.tabs.getByName('tab_1').getSections().getByName('sect_MeasureTemplate').setVisible(false);
            Xrm.Page.ui.tabs.getByName('tab_1').getSections().getByName('sect_PayeeFields').setVisible(false);
        }
    } else {
        Xrm.Page.ui.tabs.getByName('tab_1').getSections().getByName('sect_MeasureTemplate').setVisible(true);
        Xrm.Page.ui.tabs.getByName('tab_1').getSections().getByName('sect_PayeeFields').setVisible(true);
    }

}

function OnChangeProjectTempalte() {
    //Add custom filter for Program Offering (Name match ReportingName)
    //Get value of Selectable Program Offering from selected Project Template
    var SelProgOff;
    var ProjectTemplate = Xrm.Page.getAttribute('ddsm_projecttemplateid').getValue();
    if (ProjectTemplate) {
        var fsFromProjtempl = 'ddsm_SelectableProgramOffering';
        var ShName = AGS.REST.retrieveRecord(ProjectTemplate[0].id, 'ddsm_projecttemplate', fsFromProjtempl, null, null, null, false);
        if (ShName) {
            SelProgOff = ShName.ddsm_SelectableProgramOffering.Value;
        }
    }
    else {
    //Set null to Lookup of MT
        Xrm.Page.getAttribute("ddsm_measuretemplate1").setValue();
    }
    //add custom view and filtering Measure Template
    if (SelProgOff) {
        setCustomViewMeasureTempalte(SelProgOff);
    }
}

function setCustomViewMeasureTempalte(projectTemplateId) {
    var functionName = "setCustomViewMeasureTempalte";
    var defview = Xrm.Page.getControl("ddsm_measuretemplate1").getDefaultView();
    try {
        //Check whether the field exist or not
        if (Xrm.Page.getControl("ddsm_measuretemplate1")) {
        // add the randomly generated GUID for the view id
            var viewId = '{00000000-0000-0000-0000-000000000001}';
                           // '{F453A0C6-A9E6-E511-80C0-323166616638}';
                           //  {F453A0C6-A9E6-E511-80C0-323166616637}
        //add the entity name
            var entityName = 'ddsm_measuretemplate';
        // add the view display name
            var viewDisplayName = 'Measure Template for Quick Create Proj (cust)';
        // fetch xml for filtered Project Template Item records
            var fetchXml =  "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> " +
                            "<entity name='ddsm_measuretemplate'>" +
                            "<attribute name='ddsm_name' />" +
                            "<filter type='and'>" +
                            "<condition attribute='statecode' operator='eq' value='0' />"+
                            "<condition attribute='ddsm_selectableprogramoffering' operator='eq' value='" + projectTemplateId + "' />" +
                            "</filter>" +
                            "<filter type='or' >"+
                            "<condition attribute='ddsm_selectablestartdate' operator='today' />"+
                            "<condition attribute='ddsm_selectablestartdate' operator='last-x-days' value='500' />"+
                            "</filter>"+
                            "<filter type='or' >"+
                            "<condition attribute='ddsm_selectableenddate' operator='today' />"+
                            "<condition attribute='ddsm_selectableenddate' operator='next-x-days' value='500' />" +
                            "</filter>"+
                            "</entity>" +
                            "</fetch>";
        //Grid Layout for filtered records
            //var layoutXml = jQueryXrmCustomFilterView.config[customFilterView].layoutXml;
            var layoutXml = "<grid name='resultset' object='1' jump='ddsm_name' select='1' icon='1' preview='1'>" +
                            "<row name='result' id='ddsm_measuretemplate1'>" +
                            "<cell name='Name' width='150' />" +
                            "</row>" +
                            "</grid>";
            // add the custom view for the lookup field
            Xrm.Page.getControl("ddsm_measuretemplate1").addCustomView(viewId, entityName, viewDisplayName, fetchXml, layoutXml, false);
            //Xrm.Page.getControl("ddsm_measuretemplate1").setDefaultView(defview);
        }
    } catch (e) {
        throwError(e, functionName);
    }
}

function setMeasFieldsFromRateClassReference(RateClassReferenceId){ //function setMeasFieldsFromSite(siteId){
    var fsFromRCR = 'ddsm_LossFactor,ddsm_EnergyCharge,ddsm_DemandCharge';
    window.objMeasRCRFields = AGS.REST.retrieveRecord(RateClassReferenceId,'ddsm_rateclassreference',fsFromRCR,null,null,null,false);
}

function CustomMappingMeasure(){
    var measureTplMap = {};
    var sel = "?$select=ddsm_PrimaryFieldLogicalName,ddsm_SecondaryFieldLogicalName"
        + "&$filter= ddsm_PrimaryEntityLogicalName eq 'ddsm_measure' and ddsm_SecondaryEntityLogicalName eq 'ddsm_measuretemplate' and ddsm_RelatedEntitiesGroup/Value eq 962080002";

    AGS.REST.retrieveMultipleRecords("ddsm_mappingfields", sel, function(data){

        for (var i = 0; i < data.length; i++) {
            var el = data[i];
            measureTplMap[data[i].ddsm_SecondaryFieldLogicalName] = data[i].ddsm_PrimaryFieldLogicalName;
        }
    }, null, null, false, null) ;
}



function setProjTplFilterByOppty(){

    var expand = 'ddsm_ddsm_projecttemplate_ddsm_milestonetemplate';
    var sel = AGS.String.format("?$select=ddsm_Index,ddsm_ProjectTemplateToMilestoneTemplId,{0}/ddsm_InitialMilestoneIndex" +
        "&$expand={0}" +
        "&$filter=statecode/Value eq 0" +
        "and substringof('opportunity',ddsm_ProjectPhaseName)",expand);

    AGS.REST.retrieveMultipleRecords('ddsm_milestonetemplate',sel,
        function(res){
        //success

            if(Array.isArray(res)){
                for (var i = 0; i < res.length; i++) {
                    var el = res[i];
                    if(el.ddsm_Index !== el[expand].ddsm_InitialMilestoneIndex ){
                        continue;
                    }
                    OpptyProjTplList.push(el['ddsm_ProjectTemplateToMilestoneTemplId'].Id)
                }
            }

            var fetchXml = AGS.String.format("<filter type='and'><condition attribute='ddsm_projecttemplateid' operator='in' value='{0}' /></filter>",OpptyProjTplList);
            Xrm.Page.getControl("ddsm_projecttemplateid").addCustomFilter(fetchXml);
        },function(res){
            //error
        },null,true,null);
}