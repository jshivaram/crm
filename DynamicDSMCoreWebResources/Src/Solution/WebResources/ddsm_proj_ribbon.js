//Old version
//New version: accentgold_/RibbonJs/project.js

function SendEmail() {
    var version = detectIE();
    var SendMail = {};
    var showPreloader = {};
    var hidePreloader = {};
    var showMsg = {};

    if (version === false || version >= 12) {
        SendMail = function(recip, subj, body, serialized) {
            var id = getURLParameters(serialized, "id");
            //Set the Parent Customer field value to “Contoso”.
            var extraqs = "id={" + id + "}";
            //Set features for how the window will appear.
            var features = "location=no,menubar=no,status=no,toolbar=no";
            // Open the window.
            window.open(Xrm.Page.context.getClientUrl() + "/main.aspx?etn=email&pagetype=entityrecord&extraqs=" + encodeURIComponent(extraqs), "_blank", features, false);
        };

        showPreloader = function(show, target, msg) {
            Alert.show(msg, null, null, "LOADING", 460, 115, null, true);
        };

        hidePreloader = function() {
            Alert.hide();
        };

        showMsg = function(caption, text, icon) {
            Alert.show(caption, text, null, icon, 460, 250, null, true);
        };

    } else {
        var showSpinner = function(show, target, msg) {
            if (show) {
                spinnerGlobal = CreaLab.Spinner.spin(target || null, msg || "Loading data ...");
            } else {
                spinnerGlobal.stop();
            }
        }

        showPreloader = function(show, target, msg) {
            showSpinner(show, target, msg);
        };

        hidePreloader = function() {
            showSpinner(false);
        };

        showMsg = function(caption, text, icon) {
            alert(caption + " " + text);
        };

        SendMail = function(to, subject, msg, serialized) {
            var theApp; //Reference to Outlook.Application
            var theMailItem; //Outlook.mailItem
            //Attach Files to the email, Construct the Email including
            //To(address),subject,body     
            //Create a object of Outlook.Application
            try {
                var theApp = new ActiveXObject("Outlook.Application");
                var theMailItem = theApp.CreateItem(0); // value 0 = MailItem
                //Bind the variables with the email
                theMailItem.to = to;
                theMailItem.Subject = (subject);
                theMailItem.HTMLBody = (msg);
                //theMailItem.Body = (msg);
                //Show the mail before sending for review purpose
                //You can directly use the theMailItem.send() function
                //if you do not want to show the message.
                theMailItem.display();
            } catch (err) {
                console.log("Error on send Email using Outlook ActiveX: " + err);
                showMsg("Error on send Email using Outlook ActiveX", err, "ERROR");
            }
        };
    }
    Alert.showWebResource("/accentgold_/ProjectChooseEmailTemplate.html", 400, 250, "Choose Email Template", [
        new Alert.Button("Send Email", function() {
                debugger;
                if (!window.parent.selectedEmailTemplate) {
                    Alert.show("Error", "Please select templete for continue", null, "ERROR",500,200);
                    return;
                }
                showPreloader(true, null, "Generating Email Template...");
                Process.callAction("ddsm_DDSMComposeCustomEmailfromProject", [{
                        key: "Target",
                        type: Process.Type.EntityReference,
                        value: { id: Xrm.Page.data.entity.getId(), entityType: Xrm.Page.data.entity.getEntityName() }
                    }, {
                        key: "EmailTemplate",
                        type: Process.Type.String,
                        value: window.parent.selectedEmailTemplate

                    }, ],
                    function(params) {
                        console.log(params);
                        var subj = "";
                        var recip = "";
                        var body = "";
                        var serialized = {};

                        for (var i = 0; i < params.length; i++) {
                            //debugger;
                            var item = params[i];
                            if (item.key == "Body") {
                                body = item.value;
                            }
                            if (item.key == "Subject") {
                                subj = item.value;
                            }
                            if (item.key == "Recipient") {
                                recip = item.value;
                            }
                            if (item.key == "Serialized") {
                                serialized = item.value;
                            }
                        }
                        debugger;
                        SendMail(recip, subj, body, serialized);
                        hidePreloader();
                    },
                    function(e) {
                        // Error
                        hidePreloader();
                        showMsg("Preparing email templete ERROR", e, "ERROR");
                        console.log(e);
                    }
                ); window.parent.selectedEmailTemplate = null;
            },
            true),
        new Alert.Button("Сancel")
    ], null, null, 20);



}

function getURLParameters(url, paramName) {
    var sURL = url; //window.document.URL.toString();
    if (sURL.indexOf("?") > 0) {
        var arrParams = sURL.split("?");
        var arrURLParams = arrParams[1].split("&");
        var arrParamNames = new Array(arrURLParams.length);
        var arrParamValues = new Array(arrURLParams.length);

        var i = 0;
        for (i = 0; i < arrURLParams.length; i++) {
            var sParam = arrURLParams[i].split("=");
            arrParamNames[i] = sParam[0];
            if (sParam[1] != "")
                arrParamValues[i] = unescape(sParam[1]);
            else
                arrParamValues[i] = "No Value";
        }

        for (i = 0; i < arrURLParams.length; i++) {
            if (arrParamNames[i] == paramName) {
                //alert("Parameter:" + arrParamValues[i]);
                return arrParamValues[i];
            }
        }
        return "No Parameters Found";
    }
}

function detectIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }
    // other browser
    return false;
}