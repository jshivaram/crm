function onLoadRateClass(){
    var attrs = Xrm.Page.data.entity.getAttributes();

    Xrm.Page.data.entity.attributes.forEach(
        function(attr,index){attr.addOnChange(setModified);}
    )
}

function setModified() {
    if(Xrm.Page.getAttribute("ddsm_manuallymodified").getValue() !== 962080000){
        Xrm.Page.getAttribute("ddsm_manuallymodified").setValue(962080000);
    }
}
