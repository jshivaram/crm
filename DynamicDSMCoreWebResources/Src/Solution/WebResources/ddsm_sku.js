function roleBasedVisibilityOnLoad(){

    let roleNames = AGS.Users.UserRole.getName().map( function(el){return el.toLowerCase()});
    if(roleNames.indexOf('delivery agent') <0 && roleNames.indexOf('system administrator') <0){
        Xrm.Page.ui.controls.get("ddsm_modelnumber").setDisabled(true);
    }
}
