/* Version 2.6
 * Update 08/19/2015
 */
Xrm = window.Xrm || { __namespace: true };
Xrm.QuickNavigation = Xrm.QuickNavigation || { __namespace: true };
Xrm.QuickNavigation.ShowTab = true;
Xrm.QuickNavigation.ShowRelated = false;
Xrm.QuickNavigation.ShowTabs = [];
Xrm.QuickNavigation.Relations = [];
Xrm.QuickNavigation.RelatedLinks = [];
Xrm.QuickNavigation.TabControl = [];
Xrm.QuickNavigation.TabLinks = [];
Xrm.QuickNavigation.TabControlName = [];
Xrm.QuickNavigation.ShowRelatedInterval = null;
Xrm.QuickNavigation.ShowTabInterval = null;
Xrm.QuickNavigation.prevTab = null;
Xrm.QuickNavigation.prevRelated = null;
Xrm.QuickNavigation.clickAll = false;
Xrm.QuickNavigation.ClickDocsTab;
Xrm.QuickNavigation.DefineRecordType;

$(function () {

    var tmpArr_c = [];
    $(window.parent.document).find("#crmFormHeader").find("tr").each(function(){
        if(typeof $(this).find("td.ms-crm-Field-Data-Print").attr("colspan") != 'undefined'){
            tmpArr_c.push(parseInt($(this).find("td.ms-crm-Field-Data-Print").attr("colspan")));
        }

    });
    tmpArr_c.sort(function (a, b) {
        if (a < b) {
            return 1;
        }
        if (a > b) {
            return -1;
        }
        return 0;
    });
    $(window.parent.document).find("#crmFormHeader").find("tr:first").find("td.ms-crm-Field-Data-Print").attr({"colspan": tmpArr_c[0]});
    $(window.parent.document).find("#crmFormHeader").find("tr:first").find("td.ms-crm-Field-Data-Print").find("div.ms-crm-Field-Data-Print").css({"float":"right","width":"auto"});
    $(window.parent.document).find("#crmFormHeader").find("tr:first").find("td.ms-crm-Field-Data-Print").append("<div style='clear:both;'></div>");

// Returns:
// true if Company Record Type is TA || Bus/TA
// false if Company Record Type is Bus || Blank
// 962080000 (Bus), 962080001 (Bus/TA), 962080002 (TA)
    Xrm.QuickNavigation.DefineRecordType = function () {
        var isTA = false;
        var type = window.parent.Xrm.Page.getAttribute("ddsm_recordtype").getValue();

        if ( type != null) {
            switch (type) {
                case 962080000:
                    isTA = false;
                    break;
                case 962080001:
                    isTA = true;
                    break;
                case 962080002:
                    isTA = true;
                    break;
                case 962080003:
                    isTA = true;
                    break;
            }
        }
        return isTA;
    }

    Xrm.QuickNavigation.GetDataParam();

    //If displaying relations - retrieve and display the contents
    if (Xrm.QuickNavigation.ShowRelated) {
        $("#RelatedLabel").show();
        $("#Related").show();
        Xrm.QuickNavigation.GetRelationships();

        Xrm.QuickNavigation.ShowRelatedInterval = setInterval(Xrm.QuickNavigation.GetRelationships, 5000);
    }

    //If displaying tabs - retrieve and display the contents
    if (Xrm.QuickNavigation.ShowTab) {
        $("#TabsLabel").show();
        $("#Tabs").show();
        Xrm.QuickNavigation.GetTabs();
        Xrm.QuickNavigation.ShowTabInterval = setInterval(Xrm.QuickNavigation.GetTabs, 5000);
    }
});

//Handles retrieving and displaying the links to the navigation items
Xrm.QuickNavigation.GetRelationships = function () {
    var relItems = [];

    //Add the link back to base record
    relItems.push("<li><a href='#' id='Home' title='Return to form'>Main</a></li>");

    //Get the navigation items
    window.parent.Xrm.Page.ui.navigation.items.forEach(
        function (control) {
            //Ignore if hidden
            if (control.getVisible()) {
                //Show all navigation items if no specifc items are specified in the configuration, otherwise only show the allowed items
                if (Xrm.QuickNavigation.Relations.length === 0 || $.inArray(control.getId().toUpperCase(), Xrm.QuickNavigation.Relations) !== -1) {
                    relItems.push("<li><a href='#' id='" + control.getId() + "' class='relLink'>" + control.getLabel() + "</a></li>");
                }
            }
        }
    );

    //If there is a difference (navigation item was shown or hidden through another mechanism) rebuild the page content
    if (relItems.join("") !== Xrm.QuickNavigation.RelatedLinks.join("")) {
        $("#Related").html(relItems.join(""));
        Xrm.QuickNavigation.RelatedLinks = relItems;
    }

    //Set the click event of the link to setFocus on the navigation item
    $(".relLink").click(function (e) {
        e.preventDefault();
        clearInterval(Xrm.QuickNavigation.ShowRelatedInterval);
        if(Xrm.QuickNavigation.prevRelated != null) {$(Xrm.QuickNavigation.prevRelated).removeClass("Active");}
        var id = $(this).attr("id");
        Xrm.QuickNavigation.prevRelated = this;
        $(Xrm.QuickNavigation.prevRelated).addClass("Active");
        window.parent.Xrm.Page.ui.navigation.items.forEach(
            function (control) {
                if (control.getId() === id) {
                    //Hide the tab navigation if navigating to a list of related entities
                    if (Xrm.QuickNavigation.ShowTab) {
                        $("#TabsLabel").css('visibility', 'hidden');
                        $("#TabsLabel").css('display', 'none');
                        $("#Tabs").css('visibility', 'hidden');
                        $("#Tabs").css('display', 'none');
                    }
                    //control.setFocus();
                }
            }
        );
        return false;
    });

    //Set the click event for the link back to the base record
    $("#Home").click(function () {
        window.parent.window.location.replace(window.parent.document.URL);
    });

};

//Handles retrieving and displaying the links to the tabs
Xrm.QuickNavigation.GetTabs = function () {
    var tabItems = [], TabControlName = [];
    var i = 0;

    //Get the tabs
    window.parent.Xrm.Page.ui.tabs.forEach(
        function (control) {
            //Ignore if hidden
            //if (control.getVisible()) {
            //Show all tabs if no specifc tabs are specified in the configuration, otherwise only show the allowed tabs
            if (Xrm.QuickNavigation.ShowTabs.length === 0 || $.inArray(control.getName().toUpperCase(), Xrm.QuickNavigation.ShowTabs) !== -1) {
                //Tabs that are configured to not shwo the label do not return a value from getLabel() so a value is being created to show something
                TabControlName.push(control.getName());
                Xrm.QuickNavigation.TabControl[control.getName()] = control;
                if(control.getName() !== "Tab_MeasureSummary"){
                    tabItems.push("<li><a href='#' id='" + control.getName() + "' class='tabLink' title='" + ((control.getLabel() === "") ? "Tab Label Not Displayed" : "") + "'>" +
                    ((control.getLabel() !== "") ? control.getLabel() : ("Tab " + i)) + "</a></li>");
                }
            }
            //}
        }
    );
    //tabItems.push("<li style='padding-left: 20px;'><a href='#' id='showAllTabs' class='tabShowAll' title='Show All Tabs'>Show All</a></li>");
    //tabItems.push("<li><a href='#' id='expandedAll' class='tabExpandedAll' title='Show All Tabs'>Expand All</a></li>");
    //tabItems.push("<li><a href='#' id='collapsedAll' class='tabCollapsedAll' title='Show All Tabs'>Collapse All</a></li>");
    Xrm.QuickNavigation.TabControlName = TabControlName;
    //If there is a difference (tab was shown or hidden through another mechanism) rebuild the page content
    if (tabItems.join("") !== Xrm.QuickNavigation.TabLinks.join("")) {
        $("#Tabs").html(tabItems.join(""));
        Xrm.QuickNavigation.TabLinks = tabItems;
    }

    Xrm.QuickNavigation.prevTab = $("a#" + TabControlName[0]);
    $(Xrm.QuickNavigation.prevTab).addClass("Active");

    var widthConteyner = 0;
    $("#Tabs").find("LI").each(function(){
        widthConteyner += 10 + $(this).width();
    });

    $(window.parent.document).find("#crmFormHeader").find("tr:first").find("td.ms-crm-Field-Data-Print").find("div.ms-crm-Field-Data-Print").css({"width": widthConteyner + 10});
    $(window.parent.document).find("#crmFormHeader").find("tr:first").find("td.ms-crm-Field-Data-Print").find("div.ms-crm-Field-Data-Print").find("iframe").css({"width": widthConteyner + 10});

    //Set the click event of the link to hide All tabs and show & expanded & setFocus on the tab
    $(".tabLink").click(function (e) {
        e.preventDefault();
        clearInterval(Xrm.QuickNavigation.ShowTabInterval);
        if(Xrm.QuickNavigation.prevTab != null) {
            $(Xrm.QuickNavigation.prevTab).removeClass("Active");
            Xrm.QuickNavigation.TabControl[$(Xrm.QuickNavigation.prevTab).attr("id")].setDisplayState('collapsed');
            Xrm.QuickNavigation.TabControl[$(Xrm.QuickNavigation.prevTab).attr("id")].setVisible(false);
            if($(Xrm.QuickNavigation.prevTab).attr("id") === "Tab_Measures"){
                Xrm.QuickNavigation.TabControl["Tab_MeasureSummary"].setDisplayState('collapsed');
                Xrm.QuickNavigation.TabControl["Tab_MeasureSummary"].setVisible(false);
            }
        } else if(Xrm.QuickNavigation.clickAll) {
            for(i = 0; i < Xrm.QuickNavigation.TabControlName.length; i++){
                Xrm.QuickNavigation.TabControl[Xrm.QuickNavigation.TabControlName[i]].setDisplayState('collapsed');
                Xrm.QuickNavigation.TabControl[Xrm.QuickNavigation.TabControlName[i]].setVisible(false);
            }
        }
        Xrm.QuickNavigation.clickAll = false;
        Xrm.QuickNavigation.prevTab = this;
        $(Xrm.QuickNavigation.prevTab).addClass("Active");

        Xrm.QuickNavigation.TabControl[$(Xrm.QuickNavigation.prevTab).attr("id")].setVisible(true);
        Xrm.QuickNavigation.TabControl[$(Xrm.QuickNavigation.prevTab).attr("id")].setDisplayState('expanded');
        if($(Xrm.QuickNavigation.prevTab).attr("id") === "Tab_Measures"){
            Xrm.QuickNavigation.TabControl["Tab_MeasureSummary"].setVisible(true);
            Xrm.QuickNavigation.TabControl["Tab_MeasureSummary"].setDisplayState('expanded');
        }
        //Xrm.QuickNavigation.TabControl[$(Xrm.QuickNavigation.prevTab).attr("id")].setFocus();

        if($(Xrm.QuickNavigation.prevTab).attr("id") === "Tab_Notes"){
            window.parent.document.getElementById('WebResource_docConvention').contentWindow.refreshGrid();
            window.parent.document.getElementById('WebResource_uploadDocs').contentWindow.refreshGrid();
        }
        if($(Xrm.QuickNavigation.prevTab).attr("id") === "Tab_ProjectSummary"){
            window.parent.document.getElementById('WebResource_milestone_grid').contentWindow.refreshGrid();
        }
        if($(Xrm.QuickNavigation.prevTab).attr("id") === "Tab_Measures"){
            window.parent.document.getElementById('WebResource_measure_grid').contentWindow.refreshGrid();
        }
        if($(Xrm.QuickNavigation.prevTab).attr("id") === "Tab_Financials"){
            window.parent.document.getElementById('WebResource_financial_grid').contentWindow.refreshGrid();
        }
//site
        if($(Xrm.QuickNavigation.prevTab).attr("id") === "tab_opportunities"){
            window.parent.document.getElementById('WebResource_measure_grid_site').contentWindow.refreshGrid();
        }
        if($(Xrm.QuickNavigation.prevTab).attr("id") === "tab_Notes"){
            window.parent.document.getElementById('WebResource_uploaddocs').contentWindow.refreshGrid();
        }

        return false;
    });

    //Set the click event of the link to show all tabs
    $(".tabShowAll").click(function (e) {
        e.preventDefault();
        clearInterval(Xrm.QuickNavigation.ShowTabInterval);
        if(Xrm.QuickNavigation.prevTab != null) {$(Xrm.QuickNavigation.prevTab).removeClass("Active");}
        Xrm.QuickNavigation.prevTab = null;
        Xrm.QuickNavigation.clickAll = true;
                for(i = 0; i < Xrm.QuickNavigation.TabControlName.length; i++){
                    Xrm.QuickNavigation.TabControl[Xrm.QuickNavigation.TabControlName[i]].setVisible(true);
                    if(Xrm.QuickNavigation.TabControlName[i] === "Tab_Notes"){
                        window.parent.document.getElementById('WebResource_docConvention').contentWindow.refreshGrid();
                        window.parent.document.getElementById('WebResource_uploadDocs').contentWindow.refreshGrid();
                    }
                    if(Xrm.QuickNavigation.TabControlName[i] === "Tab_ProjectSummary"){
                        window.parent.document.getElementById('WebResource_milestone_grid').contentWindow.refreshGrid();
                    }
                    if(Xrm.QuickNavigation.TabControlName[i] === "Tab_Measures"){
                        window.parent.document.getElementById('WebResource_measure_grid').contentWindow.refreshGrid();
                    }
                    if(Xrm.QuickNavigation.TabControlName[i] === "Tab_Financials"){
                        window.parent.document.getElementById('WebResource_financial_grid').contentWindow.refreshGrid();
                    }
                }
        return false;
    });

    //Set the click event of the link to expanded all tabs
    $(".tabExpandedAll").click(function (e) {
        e.preventDefault();
        clearInterval(Xrm.QuickNavigation.ShowTabInterval);
        if(Xrm.QuickNavigation.prevTab != null) {$(Xrm.QuickNavigation.prevTab).removeClass("Active");}
        Xrm.QuickNavigation.prevTab = null;
        Xrm.QuickNavigation.clickAll = true;
                for(i = 0; i < Xrm.QuickNavigation.TabControlName.length; i++){
                    Xrm.QuickNavigation.TabControl[Xrm.QuickNavigation.TabControlName[i]].setVisible(true);
                    Xrm.QuickNavigation.TabControl[Xrm.QuickNavigation.TabControlName[i]].setDisplayState('expanded');
                    if(Xrm.QuickNavigation.TabControlName[i] === "Tab_Notes"){
                        window.parent.document.getElementById('WebResource_docConvention').contentWindow.refreshGrid();
                        window.parent.document.getElementById('WebResource_uploadDocs').contentWindow.refreshGrid();
                    }
                    if(Xrm.QuickNavigation.TabControlName[i] === "Tab_ProjectSummary"){
                        window.parent.document.getElementById('WebResource_milestone_grid').contentWindow.refreshGrid();
                    }
                    if(Xrm.QuickNavigation.TabControlName[i] === "Tab_Measures"){
                        window.parent.document.getElementById('WebResource_measure_grid').contentWindow.refreshGrid();
                    }
                    if(Xrm.QuickNavigation.TabControlName[i] === "Tab_Financials"){
                        window.parent.document.getElementById('WebResource_financial_grid').contentWindow.refreshGrid();
                    }
                }
        return false;
    });

    //Set the click event of the link to collapsed all tabs
    $(".tabCollapsedAll").click(function (e) {
        e.preventDefault();
        clearInterval(Xrm.QuickNavigation.ShowTabInterval);
        if(Xrm.QuickNavigation.prevTab != null) {$(Xrm.QuickNavigation.prevTab).removeClass("Active");}
        Xrm.QuickNavigation.prevTab = null;
        Xrm.QuickNavigation.clickAll = true;
                for(i = 0; i < Xrm.QuickNavigation.TabControlName.length; i++){
                    Xrm.QuickNavigation.TabControl[Xrm.QuickNavigation.TabControlName[i]].setVisible(true);
                    Xrm.QuickNavigation.TabControl[Xrm.QuickNavigation.TabControlName[i]].setDisplayState('collapsed');
                }
        return false;
    });
};
Xrm.QuickNavigation.ClickDocsTab = function() {
    $("#Tab_Notes").click();
}
//Get the incoming data parameter
Xrm.QuickNavigation.GetDataParam = function () {
    var vals = [];
    if (location.search != "") {
        vals = location.search.substr(1).split("&");
        for (var i = 0; i < vals.length; i++) {
            vals[i] = vals[i].replace(/\+/g, " ").split("=");
        }
        for (var j = 0; j < vals.length; j++) {
            if (vals[j][0].toLowerCase() == "data") {
                Xrm.QuickNavigation.ParseData(vals[j][1]);
                break;
            }
        }
    }
};

//Parse the various parts of the incoming data parameter
//ShowTab=true
//ShowRelated=true
//Tabs=tabName1,tabName2
//Relations=navId1,navId2
Xrm.QuickNavigation.ParseData = function (data) {
    var vals = [];
    vals = decodeURIComponent(data).split("&");
    for (var i = 0; i < vals.length; i++) {
        vals[i] = vals[i].replace(/\+/g, " ").split("=");
    }

    for (var x = 0; x < vals.length; x++) {
        switch (vals[x][0].toUpperCase()) {
            case "SHOWTAB":
                Xrm.QuickNavigation.ShowTab = (vals[x][1].toUpperCase() === "TRUE") ? true : false;
                break;
            case "SHOWRELATED":
                Xrm.QuickNavigation.ShowRelated = (vals[x][1].toUpperCase() === "TRUE") ? true : false;
                break;
            case "TABS":
                Xrm.QuickNavigation.ShowTabs = vals[x][1].toUpperCase().split(",");
                if(window.parent.Xrm.Page.data.entity.getEntityName() === "account"){
                    if(!Xrm.QuickNavigation.DefineRecordType()){
                        Xrm.QuickNavigation.ShowTabs.splice( Xrm.QuickNavigation.ShowTabs.indexOf( "TAB_TRADEALLY" ), 1 );
                    }
                }
                break;
            case "RELATIONS":
                Xrm.QuickNavigation.Relations = vals[x][1].toUpperCase().split(",");
                break;
        }
    }
};