function EnableButton() {
	debugger;
    return true;
}

function ExecuteDC(entityName) {
    var currentWindow = parent.Xrm.Internal.isTurboForm() ? parent.window : window;
    kendo = currentWindow.kendo;

    if (typeof ($) === 'undefined') {
        $ = currentWindow.$;
        jQuery = currentWindow.jQuery;
    }
	

    injectWindow();
    try {
    var dialogContent = '<div>     <div id="instructions">         <p>             Please specify a date range of no more than 4 months for Hourly consumption data and no more than 4 years for Daily or Monthly consumption data.         </p>     </div>     <div id="inputs">         <div>             <label for="intreval">Interval</label>             <input id="interval" name="interval"/>         </div>         <div>             <label for="start-date">Start Date</label>             <input id="start-date" name="start-date" data-role="datepicker" data-bind="value: StartDate"/>         </div>         <div>             <label for="end-date">End Date</label>             <input id="end-date" name="end-date" data-role="datepicker" data-bind="value: EndDate"/>         </div>         <div id="filter-container">             <label for="filter">Filter results for current site only</label>             <input type="checkbox" id="filter" name="filter" data-bind="checked: Filter"/>         </div>     </div> </div>';

    if (!$("#action_dialog", currentWindow.document).data("kendoDialog")){
        $("#action_dialog", currentWindow.document).kendoDialog({
            width: "500px",
            title: 'Detailed Consumption',
            closable: true,
            modal: true,
            content: dialogContent,
            close: function () {
                $("#interval", currentWindow.document).data("kendoDropDownList").select(-1);
            },
            actions: [
                { text: 'Cancel', action: function () { $("#action_dialog", currentWindow.document).data("kendoDialog").close(); } },
                { text: '<span class="exc-mark">!</span>Results', primary: true, action: function(e){
                    if (!ValidateData())
                        return false;
                    
                    OkAction();
                } }
            ],
        });
    } else {
        $("#action_dialog", currentWindow.document).data("kendoDialog").open();
    }

    CreateIntervalsDropdown();

    currentWindow.viewModel = kendo.observable({
        SelectedInterval: null,
        StartDate: null,
        EndDate: null,
        Filter: false
    });
    kendo.bind($("#inputs", currentWindow.document), currentWindow.viewModel);

    if (entityName == "ddsm_premise")
        $("#filter-container", currentWindow.document).hide();

    } catch (e) {
        console.log(e);
    }

    function OkAction(){

        currentWindow.parameters = {
            SEARCH_INTERVAL: currentWindow.viewModel.SelectedInterval,
            START_DATETIME: {},
            END_DATETIME: {},
            RECORD_TYPE: {},
            RECORD_ID: {},
            METER_TYPECODE: {},
            BILLING_TYPECODE: {}
        }

        currentWindow.parameters.START_DATETIME = FormatDate(currentWindow.viewModel.StartDate, false);
        currentWindow.parameters.END_DATETIME = FormatDate(currentWindow.viewModel.EndDate, true);

        var id = Xrm.Page.data.entity.getId();
        
        try {
            
        } catch(e) {
            console.log(e);
        }
    }

    function FormatDate(date, isEndDate){
        var yyyy = date.getFullYear();

        var mo = "0" + (date.getMonth() + 1);
        if (mo.length > 2)
            mo = mo.slice(1,3);

        var dd = "0" + date.getDate();
        if (dd.length > 2)
            dd = dd.slice(1,3);
        
        var hh = "0" + date.getHours();
        if (hh.length > 2)
            hh = hh.slice(1,3);

        var mm = "0" + date.getMinutes();
        if (mm.length > 2)
            mm = mm.slice(1,3);

        var ss = "0" + date.getSeconds();
        if (ss.length > 2)
            ss = ss.slice(1,3);

        if (currentWindow.viewModel.SelectedInterval == "Monthly"){
            if(isEndDate)
                dd = "31";
            else 
                dd= "01"
        }

        if (currentWindow.viewModel.SelectedInterval == "Monthly" || currentWindow.viewModel.SelectedInterval == "Daily"){
            hh = "00";
            mm = "00";
            ss = "00";
        }   

        return "" + yyyy + mo + dd + hh + ":" + mm + ":" + ss
    }

    function ValidateData(){
        if (currentWindow.viewModel.SelectedInterval === null || currentWindow.viewModel.SelectedInterval === "--"){
            currentWindow.Alert.show("Please select an Interval", null, [{ label: "OK" }], "WARNING");
            return false;
        }

        if (currentWindow.viewModel.StartDate === null){
            currentWindow.Alert.show("Please select a Start Date", null, [{ label: "OK" }], "WARNING");
            return false;
        }
        
        if (currentWindow.viewModel.EndDate === null){
            currentWindow.Alert.show("Please select an End Date", null, [{ label: "OK" }], "WARNING");
            return false;
        }
        
        if (currentWindow.viewModel.SelectedInterval == "Hourly"){
            var dateDiff = currentWindow.viewModel.EndDate.getTime() - currentWindow.viewModel.StartDate.getTime();

            if (dateDiff < 0){
                currentWindow.Alert.show("The End Date cannot be set before the Start Date. Please reenter the End Date.", null, [{ label: "OK" }], "WARNING");
                $("#end-date").data('kendoDatePicker').value("");
                return false;
            }

            var dayDiff = dateDiff / (1000 * 60 * 60 * 24);
            if (Math.floor(dayDiff) > 122){
                currentWindow.Alert.show("The End Date cannot be more than 2 months after the Start Date. Please reenter the End Date.", null, [{ label: "OK" }], "WARNING");
                $("#end-date").data('kendoDatePicker').value("");
                return false;
            } 
        }

        if (currentWindow.viewModel.SelectedInterval == "Daily" || currentWindow.viewModel.SelectedInterval == "Monthly"){
            var dateDiff = currentWindow.viewModel.EndDate.getTime() - currentWindow.viewModel.StartDate.getTime();

            if (dateDiff < 0){
                currentWindow.Alert.show("The End Date cannot be set before the Start Date. Please reenter the End Date.", null, [{ label: "OK" }], "WARNING");
                $("#end-date").data('kendoDatePicker').value("");
                return false;
            }
            debugger;
            var yearDiff = dateDiff / (1000 * 60 * 60 * 24 * 365);
            if (Math.ceil(yearDiff) > 4){
                currentWindow.Alert.show("The End Date cannot be more than 48 months after the Start Date. Please reenter the End Date.", null, [{ label: "OK" }], "WARNING");
                $("#end-date").data('kendoDatePicker').value("");
                return false;
            }
        }

        return true;
    }
    
    function onIntervalSelect(e){
        var dataItem = this.dataItem(e.item);
        currentWindow.viewModel.SelectedInterval = dataItem.Label;
    }

    function CreateIntervalsDropdown(){
        $("#interval", currentWindow.document).kendoDropDownList({
            dataTextField: "Label",
            dataValueField: "Id",
            optionLabel: "--",
            select: onIntervalSelect,
            dataSource: {data: [
                {Label: "Monthly", Id: 0}, 
                {Label: "Daily", Id: 1}, 
                {Label: "Hourly", Id: 2}
            ]}
        });
    }

    function create(htmlStr, htmlTag) {
        if (!htmlTag) {
            htmlTag = 'div';
        }
        var frag = currentWindow.document.createDocumentFragment(),
            temp = currentWindow.document.createElement(htmlTag);
        temp.innerHTML = htmlStr;
        while (temp.firstChild) {
            frag.appendChild(temp.firstChild);
        }
        return frag;
    }

    function injectWindow(){
        if ($('#action_dialog', currentWindow.document).length == 0){
            var dialogTemplate = '<div id="action_dialog"></div>';
            var dialogFragment = create(dialogTemplate);
            currentWindow.document.body.insertBefore(dialogFragment, currentWindow.document.body.childNodes[0]);
        }
    }
}