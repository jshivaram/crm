
GlobalJs.resourcesList["ddsm_project"] = [
    {
        url: "accentgold_/settings/js/settingsHelper.js",
        id: "settingsHelper",
        type: "js",
        onloadFunc: []
    },
     /*  {
        url:"mapping_mod_/js/helper.js",
        id: "crmHelper1",
        type: "js",
        onloadFunc: [] 
    },*/
     {
        url: "accentgold_/ESP_mod/css/style.css",
        id: "DialogStyle2",
        type: "css",
        onloadFunc: []
    },
    {
        url: "ddsm_discontinuingProject",
        id: "DiscontinuingProject",
        type: "js",
        onloadFunc: ["LockFieldsOnDiscontinue"]
    },
    {
        url: "ddsm_onHoldProject",
        id: "OnHoldProject",
        type: "js",
        onloadFunc: ["onLoad"]
    }
];