function Init(){
    var targetEntityControl = $("#ddsm_targetentity");

    var data = {
        "EntityLogicalName": "ddsm_indicator"
    };
    
    GlobalJs.WebAPI.ExecuteAction('ddsm_DDSMGetOneToManyRelationships', data).then(
        function(result){
            var results = JSON.parse(result.Result);
            debugger;
            targetEntityControl.data("kendoDropDownList").setDataSource(new kendo.data.DataSource({
                data: results.filter(function(e) {
                    return e.IsCustomRelationship || e.LogicalName == "ddsm_process" || e.LogicalName == "ddsm_processtemplate";
                }),
                sort: { field: "Label", dir: "asc" }
            }));
            var targetEntity = Xrm.Page.getAttribute("ddsm_targetentity").getValue(); 

            if (targetEntity)
                targetEntityControl.data("kendoDropDownList").select(function(dataItem) {
                    return dataItem.LogicalName === targetEntity;
                });
        }
        ,function(error){
            console.log(error);
        });
    
    targetEntityControl.kendoDropDownList({
        dataTextField: "Label",
        dataValueField: "LogicalName",
        optionLabel: "--",
        select: OnEntitySelect,
        dataSource: new kendo.data.DataSource({}),
    });

    function OnEntitySelect(e){
        var logicalName = e.dataItem.LogicalName;
        if (logicalName != "") {
            Xrm.Page.getAttribute("ddsm_targetentity").setValue(logicalName);
            Xrm.Page.getAttribute("ddsm_targetfield").setValue(e.dataItem.ReferencingAttribute);
        } else {
            Xrm.Page.getAttribute("ddsm_targetentity").setValue(null);
            Xrm.Page.getAttribute("ddsm_targetfield").setValue(null);
        }
            
    }
}