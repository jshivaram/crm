    var currentWindow = parent.Xrm.Internal.isTurboForm() ? parent.window : window;
    GlobalJs = currentWindow.GlobalJs;

    if (typeof ($) === 'undefined') {
        $ = currentWindow.$;
        jQuery = currentWindow.jQuery;
    }

    function addButton(){
        console.log("-- in addButton");

        injectWindow();

        $("#ddsm_msdbutton_d", currentWindow.document).empty();
        $("#ddsm_msdbutton_d", currentWindow.document).append(
            $('<button>').attr("id", "ddsm_msdbutton").text("Edit MSD Fields").height("auto").width("auto")
        );

        $("#ddsm_msdbutton", currentWindow.document).kendoButton({
            click: onMsdButtonClick
        });
    }

    function onMsdButtonClick(e){

        var windowContent = '<div id="grid"></div>';

        $('#msd_window', currentWindow.document).kendoWindow({
            title: "Edit MSD Fields",
            width: 900,
            visible: false,
            actions: [
                "Close"
            ]
        });


        var kendoWindow = $("#msd_window", currentWindow.document).data("kendoWindow");
        kendoWindow.content(windowContent);
        try{
            RenderGrid();
        } catch(e){
            console.log(e);
        }
        
        kendoWindow.center().open();
    }

    function RenderGrid(){

        var msdfields = currentWindow.Xrm.Page.getAttribute("ddsm_msdfields");
        var fields = currentWindow.JSON.parse(msdfields.getValue());

        var checkedIds = {};

        var dataSource = new kendo.data.DataSource({
            data: fields,
            autoSync: false,
            schema: {
                model: {
                    fields: {
                        AttrName: { editable: true }, //logical name
                        Entity: { editable: false }, //ddsm_measure
                        FieldType: { editable: false }, //ddsm_measure
                        IsReadOnly: { type: "boolean" },
                        IsRequired: { type: "boolean" },
                        DispName: { editable: false }, // CRMFieldName
                        DispNameOver: { editable: true }, //
                    }
                }
            }
        });

        var grid = $("#grid").kendoGrid({
            dataSource: dataSource,
            height: 430,
            saveChanges: onSave,
            toolbar: ["create", "save"],
            columns: [
                //{ field: "DispName", title: "Display Name"  },
                { field: "AttrName", title:"Attribute Name", width: "280px", editor: fieldNameDropDownEditor, template: '#=DispName#'},
                { field: "DispNameOver", title:"Display Name Override", width: "150px" },
                { title:"Required", template: '<input type="checkbox" #= IsRequired ? \'checked="checked"\' : "" # class="chkbx required-chkbx" />' },
                { title:"Read-only", template: '<input type="checkbox" #= IsReadOnly ? \'checked="checked"\' : "" # class="chkbx readonly-chkbx" />'},
                { field: "Entity", title:"Entity" },
                { field: "FieldType", title:"Field Type" }, 
                {
                    command: ["destroy"],
                    title: "&nbsp;",
                    width: "172px"
                }
            ],
            editable: true
        }).data("kendoGrid");

        $("#grid .k-grid-content").on("change", "input.required-chkbx", function(e) {
            var grid = $("#grid").data("kendoGrid"),
                dataItem = grid.dataItem($(e.target).closest("tr"));

            dataItem.set("IsRequired", this.checked);
        });

        $("#grid .k-grid-content").on("change", "input.readonly-chkbx", function(e) {
            var grid = $("#grid").data("kendoGrid"),
                dataItem = grid.dataItem($(e.target).closest("tr"));

            dataItem.set("IsReadOnly", this.checked);
        });

        function onSave(e){
            var data = e.sender.dataItems();
            var msdfields = currentWindow.Xrm.Page.getAttribute("ddsm_msdfields");
            msdfields.setValue(currentWindow.JSON.stringify(data));
        }
        
    }

    function fieldNameDropDownEditor(container, options) {
   // debugger;
    var val =options.model.AttrName;
        $('<input required name="' + options.field + '" data-bind="value:'+ options.model.AttrName+'"/>')
            .appendTo(container).kendoDropDownList({
                autoBind: false,
                dataTextField: "AttrDisplayName",
                dataValueField: "AttrLogicalName",
                optionLabel: "--",
                filter: "startswith",
                dataSource: currentWindow.KendoHelper.CRM.getEntityMetadata("ddsm_measure", false, null,"AttrType"),
                select: OnDispNameSelect,
              /*  dataBound:function(e){
                	this.value(val);
                }*/
            });

        function OnDispNameSelect(e){
           // debugger;
            options.model.DispName = e.dataItem.AttrDisplayName;
            options.model.AttrName = e.dataItem.AttrLogicalName;
            options.model.FieldType = e.dataItem.AttrType;
        }
    }

    function create(htmlStr, htmlTag) {
        if (!htmlTag) {
            htmlTag = 'div';
        }
        var frag = GlobalJs.document.createDocumentFragment(),
            temp = GlobalJs.document.createElement(htmlTag);
        temp.innerHTML = htmlStr;
        while (temp.firstChild) {
            frag.appendChild(temp.firstChild);
        }
        return frag;
    }

    function injectWindow(){
        if ($('#msd_window', currentWindow.document).length == 0){
            var dialogTemplate = '<div id="msd_window"></div>';
            var dialogFragment = create(dialogTemplate);
            currentWindow.document.body.insertBefore(dialogFragment, currentWindow.document.body.childNodes[0]);
        }
    }