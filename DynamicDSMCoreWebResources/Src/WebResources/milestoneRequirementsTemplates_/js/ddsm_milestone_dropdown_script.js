var milestoneLookup;
var targetField;
var entity;
var entityField;
var indicator;

var GlobalJs = parent.GlobalJs;

$(document).ready(function () {
    var milestoneEntity =
        {
            EntityLogicalName: "ddsm_milestone"
        };
    milestoneLookup = $("#milestoneLookup").kendoDropDownList(
        {
            optionLabel: "Select milestone lookup...",
            dataTextField: "DisplayName",
            dataValue: "LogicalName",
            change: onChange,
            filter: "contains",
            dataSource: {
                transport: {
                    read: function (options) {
                        GlobalJs.WebAPI.ExecuteAction('ddsm_GetMilestoneLookupFields', milestoneEntity).then(
                            function (result) {
                                var resultParse = JSON.parse(result.Result);
                                options.success(resultParse);
                                if (GlobalJs.crmContentPanel._window.Xrm.Page.getAttribute("ddsm_dropdowndatajson") &&
                                    GlobalJs.crmContentPanel._window.Xrm.Page.getAttribute("ddsm_dropdowndatajson").getValue() !== "" &&
                                    GlobalJs.crmContentPanel._window.Xrm.Page.getAttribute("ddsm_dropdowndatajson").getValue()) {
                                    reestablishMilestoneLookup();
                                }
                            }
                            , function (error) {
                            });
                    }
                },
            }
        }).data().kendoDropDownList;
    targetField = $("#targetField").kendoDropDownList(
        {
            optionLabel: "Select field...",
            dataTextField: "DisplayName",
            dataValue: "LogicalName",
            change: onChangeTargetField,
            filter: "contains",
            enable: false,
        }).data().kendoDropDownList;

    entity = $("#entity").kendoDropDownList(
        {
            optionLabel: "Select entity...",
            dataTextField: "DisplayName",
            dataValue: "LogicalName",
            change: onChangeEntity,
            filter: "contains",
            enable: false,
        }).data().kendoDropDownList;

    entityField = $("#entityField").kendoDropDownList(
        {
            optionLabel: "Select field...",
            dataTextField: "DisplayName",
            dataValue: "LogicalName",
            change: onChangeIndicator,
            filter: "contains",
            enable: false,
        }).data().kendoDropDownList;

    indicator = $("#indicator").kendoDropDownList(
        {
            optionLabel: "Select...",
            dataTextField: "ddsm_name",
            dataValue: "ddsm_indicatorid",
            enable: false,
            change: onChangeIndicator,
            filter: "contains",
        }).data().kendoDropDownList;

    function onChange(e) {
        entityField.enable(false);
        indicator.enable(false);
        entityField.value("Select field...");
        indicator.value("Select...");
        targetField.enable(false);
        entity.enable(false);

        var dataItem;
        if (e.sender) {
            dataItem = this.dataItem();
        }
        else {
            dataItem = e.dataItem();
        }

        GlobalJs.crmContentPanel._window.Xrm.Page.getAttribute("ddsm_milestonelookup").setValue(dataItem.LogicalName);
        if (dataItem.DisplayName != "Select milestone lookup...") {

            var res = GlobalJs.WebAPI.ExecuteAction('ddsm_GetFields', { EntityLogicalName: dataItem.TargetEntity.LogicalName }).then(
                function (result) {
                    var resultParse = JSON.parse(result.Result);
                    res = resultParse
                    targetField.setDataSource(new kendo.data.DataSource({ data: res }));
                    targetField.enable(true);
                    if (!e.sender) {
                        reestablishTargetField();
                    }
                }
                , function (error) {
                });

            var targetEntity =
                {
                    TargetEntity: dataItem.TargetEntity.LogicalName
                };

            var resOfRelationship = GlobalJs.WebAPI.ExecuteAction('ddsm_GetEntityWithRelationship', targetEntity).then(
                function (result) {
                    var resultParse = JSON.parse(result.ResultJson);
                    resOfRelationship = resultParse;
                    entity.setDataSource(new kendo.data.DataSource({ data: resOfRelationship }));

                    if (data == undefined || data.ChildTargetEntity.LogicalName != "") {
                        entity.enable(true);
                    }
                    if (!e.sender) {
                        reestablishEntity();
                    }
                }
                , function (error) {
                });
        }
        else {
            targetField.enable(false);
            targetField.value("Select field...");
            entity.enable(false);
            entity.value("Select entity...");
            entityField.enable(false);
            entityField.value("Select field...");
            indicator.enable(false);
            indicator.value("Select...");

            GlobalJs.crmContentPanel._window.Xrm.Page.getAttribute("ddsm_dropdowndatajson").setValue(null);
            GlobalJs.crmContentPanel._window.Xrm.Page.data.save().then(
                function () {
                    console.log("Saved");
                },
                function () {
                    console.log("Error when save");
                });
        }
    }

    function onChangeTargetField(e) {
        var valueOfTargetField = this.dataItem();

        if (valueOfTargetField.DisplayName != "Select field...") {
            entity.enable(false);
            entityField.enable(false);
            indicator.enable(false);

            var milestoneLookupDataItem = milestoneLookup.dataItem();
            var targetFieldDataItem = targetField.dataItem();

            if (targetFieldDataItem.AttributeType == "Picklist") {
                var data = {
                    MilestoneLookup: {
                        LogicalName: milestoneLookupDataItem.LogicalName,
                        DisplayName: milestoneLookupDataItem.DisplayName,
                        TargetEntity:
                        {
                            LogicalName: milestoneLookupDataItem.TargetEntity.LogicalName,
                            DisplayName: milestoneLookupDataItem.TargetEntity.DisplayName
                        }
                    },
                    TargetField: {
                        LogicalName: targetFieldDataItem.LogicalName,
                        DisplayName: targetFieldDataItem.DisplayName,
                        AttributeType: targetFieldDataItem.AttributeType,
                        Options: targetFieldDataItem.Options.map(function (element) {
                            return { Label: element.Label, Value: element.Value }
                        })
                    }
                }
            }

            else if (targetFieldDataItem.AttributeType == "Lookup") {
                var data = {
                    MilestoneLookup: {
                        LogicalName: milestoneLookupDataItem.LogicalName,
                        DisplayName: milestoneLookupDataItem.DisplayName,
                        TargetEntity:
                        {
                            LogicalName: milestoneLookupDataItem.TargetEntity.LogicalName,
                            DisplayName: milestoneLookupDataItem.TargetEntity.DisplayName
                        }
                    },
                    TargetField: {
                        LogicalName: targetFieldDataItem.LogicalName,
                        DisplayName: targetFieldDataItem.DisplayName,
                        AttributeType: targetFieldDataItem.AttributeType,
                        Target: targetFieldDataItem.Target
                    }
                }
            }
            else {
                var data = {
                    MilestoneLookup: {
                        LogicalName: milestoneLookupDataItem.LogicalName,
                        DisplayName: milestoneLookupDataItem.DisplayName,
                        TargetEntity:
                        {
                            LogicalName: milestoneLookupDataItem.TargetEntity.LogicalName,
                            DisplayName: milestoneLookupDataItem.TargetEntity.DisplayName
                        }
                    },
                    TargetField: {
                        LogicalName: targetFieldDataItem.LogicalName,
                        DisplayName: targetFieldDataItem.DisplayName,
                        AttributeType: targetFieldDataItem.AttributeType
                    }
                }
            }
            var resJson = JSON.stringify(data);
            GlobalJs.crmContentPanel._window.Xrm.Page.getAttribute("ddsm_dropdowndatajson").setValue(resJson);
            GlobalJs.crmContentPanel._window.Xrm.Page.data.save().then(
                function () {
                    console.log("Saved");
                },
                function () {
                    console.log("Error when save");
                });
        }
        else {
            entity.enable(true);
            GlobalJs.crmContentPanel._window.Xrm.Page.getAttribute("ddsm_dropdowndatajson").setValue(null);
            GlobalJs.crmContentPanel._window.Xrm.Page.data.save().then(
                function () {
                    console.log("Saved");
                },
                function () {
                    console.log("Error when save");
                });
        }
    }

    function onChangeEntity(e) {
        var field;
        if (e.sender) {
            field = this.dataItem();
        }
        else {
            field = e.dataItem();
        }
        entityField.enable(false);
        indicator.enable(false);
        if (field.DisplayName != "Select entity...") {
            targetField.enable(false);
            entityField.enable(true);
            var resentityFields = GlobalJs.WebAPI.ExecuteAction('ddsm_GetFields', { EntityLogicalName: field.LogicalName }).then(
                function (result) {
                    var resultParse = JSON.parse(result.Result);
                    resentityFields = resultParse;
                    entityField.setDataSource(new kendo.data.DataSource({ data: resentityFields }));
                    entityField.enable(true);
                    if (!e.sender) {
                        reestablishEntityField();
                    }
                }
                , function (error) {

                });

            indicator.enable(true);
            GlobalJs.WebAPI.GetList("ddsm_indicators",
                {
                    FormattedValues: true,
                    OrderBy: ['ddsm_name'],
                    Select: ['ddsm_name']
                }).then(
                function (result) {
                    var variable = result.List.map(function (element) {
                        return { ddsm_name: element.ddsm_name, ddsm_indicatorid: element.ddsm_indicatorid };
                    })
                    indicator.setDataSource(new kendo.data.DataSource({ data: variable }));
                    indicator.enable(true);
                    if (!e.sender) {
                        reestablishIndicator();
                    }
                }
                , function (error) {
                });

        }
        else {
            targetField.enable(true);
            entityField.enable(false);
            indicator.enable(false);
            entityField.value("Select field...");
            indicator.value("Select...");

            GlobalJs.crmContentPanel._window.Xrm.Page.getAttribute("ddsm_dropdowndatajson").setValue(null);
            GlobalJs.crmContentPanel._window.Xrm.Page.data.save().then(
                function () {
                    console.log("Saved");
                },
                function () {
                    console.log("Error when save");
                });
        }
    }

    function onChangeIndicator(e) {
        var indicatorFielddDataItem = indicator.dataItem();

        if (indicatorFielddDataItem.ddsm_name != "Select...") {
            var milestoneLookupDataItem = milestoneLookup.dataItem();
            var entityDataItem = entity.dataItem();
            var entityFielddDataItem = entityField.dataItem();


            if (entityFielddDataItem.AttributeType == "Picklist") {
                var data = {
                    MilestoneLookup: {
                        LogicalName: milestoneLookupDataItem.LogicalName,
                        DisplayName: milestoneLookupDataItem.DisplayName,
                        TargetEntity:
                        {
                            LogicalName: milestoneLookupDataItem.TargetEntity.LogicalName,
                            DisplayName: milestoneLookupDataItem.TargetEntity.DisplayName
                        }
                    },
                    ChildTargetEntity: {
                        LogicalName: entityDataItem.LogicalName,
                        DisplayName: entityDataItem.DisplayName,
                    },
                    ChildTargetField: {
                        LogicalName: entityFielddDataItem.LogicalName,
                        DisplayName: entityFielddDataItem.DisplayName,
                        AttributeType: entityFielddDataItem.AttributeType,
                        Options: entityFielddDataItem.Options.map(function (element) {
                            return { Label: element.Label, Value: element.Value }
                        })
                    },
                    Indicator: {
                        Id: indicatorFielddDataItem.ddsm_indicatorid,
                        Name: indicatorFielddDataItem.ddsm_name
                    }
                }
            }

            else if (entityFielddDataItem.AttributeType == "Lookup") {
                var data = {
                    MilestoneLookup: {
                        LogicalName: milestoneLookupDataItem.LogicalName,
                        DisplayName: milestoneLookupDataItem.DisplayName,
                        TargetEntity:
                        {
                            LogicalName: milestoneLookupDataItem.TargetEntity.LogicalName,
                            DisplayName: milestoneLookupDataItem.TargetEntity.DisplayName
                        }
                    },
                    ChildTargetEntity: {
                        LogicalName: entityDataItem.LogicalName,
                        DisplayName: entityDataItem.DisplayName,
                    },
                    ChildTargetField: {
                        LogicalName: entityFielddDataItem.LogicalName,
                        DisplayName: entityFielddDataItem.DisplayName,
                        AttributeType: entityFielddDataItem.AttributeType,
                        Target: entityFielddDataItem.Target
                    },
                    Indicator: {
                        Id: indicatorFielddDataItem.ddsm_indicatorid,
                        Name: indicatorFielddDataItem.ddsm_name
                    }
                }
            }
            else {
                var data = {
                    MilestoneLookup: {
                        LogicalName: milestoneLookupDataItem.LogicalName,
                        DisplayName: milestoneLookupDataItem.DisplayName,
                        TargetEntity:
                        {
                            LogicalName: milestoneLookupDataItem.TargetEntity.LogicalName,
                            DisplayName: milestoneLookupDataItem.TargetEntity.DisplayName
                        }
                    },
                    ChildTargetEntity: {
                        LogicalName: entityDataItem.LogicalName,
                        DisplayName: entityDataItem.DisplayName,
                    },
                    ChildTargetField: {
                        LogicalName: entityFielddDataItem.LogicalName,
                        DisplayName: entityFielddDataItem.DisplayName,
                        AttributeType: entityFielddDataItem.AttributeType
                    },
                    Indicator: {
                        Id: indicatorFielddDataItem.ddsm_indicatorid,
                        Name: indicatorFielddDataItem.ddsm_name
                    }
                }
            }
            var resJson = JSON.stringify(data);
            GlobalJs.crmContentPanel._window.Xrm.Page.getAttribute("ddsm_dropdowndatajson").setValue(resJson);
            GlobalJs.crmContentPanel._window.Xrm.Page.data.save().then(
                function () {
                    console.log("Saved");
                },
                function () {
                    console.log("Error when save");
                });
        }
    }
});
var data;
function reestablishMilestoneLookup() {
    data = JSON.parse(GlobalJs.crmContentPanel._window.Xrm.Page.getAttribute("ddsm_dropdowndatajson").getValue());

    if (!data) {
        return;
    }

    milestoneLookup.value(milestoneLookup.dataSource.data().find(function (element, index) {
        return element.LogicalName === data.MilestoneLookup.LogicalName;
    }));

    milestoneLookup.options.change(milestoneLookup);
}

function reestablishTargetField() {
    if (!data || typeof (data.TargetField) === "undefined" || !data.TargetField) {
        return;
    }
    targetField.value(targetField.dataSource.data().find(function (element, index) {
        return element.LogicalName === data.TargetField.LogicalName;
    }));

    entity.enable(false);
    entityField.enable(false);
    indicator.enable(false);
}

function reestablishEntity() {
    if (!data || typeof (data.ChildTargetEntity) === "undefined" || !data.ChildTargetEntity) {
        return;
    }
    entity.value(entity.dataSource.data().find(function (element, index) {
        return element.LogicalName === data.ChildTargetEntity.LogicalName;
    }));

    targetField.enable(false);
    entityField.enable(true);
    indicator.enable(true);

    entity.options.change(entity);
}

function reestablishEntityField() {

    if (!data || typeof (data.ChildTargetField) === "undefined" || !data.ChildTargetField) {
        return;
    }
    entityField.value(entityField.dataSource.data().find(function (element, index) {
        return element.LogicalName === data.ChildTargetField.LogicalName;
    }));
}

function reestablishIndicator() {

    if (!data || typeof (data.Indicator) === "undefined" || !data.Indicator) {
        return;
    }
    indicator.value(indicator.dataSource.data().find(function (element, index) {
        return element.ddsm_indicatorid === data.Indicator.Id;
    }));
}