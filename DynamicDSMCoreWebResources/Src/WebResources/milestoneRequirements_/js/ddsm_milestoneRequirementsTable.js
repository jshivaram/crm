$(document).ready(function () {
    insertDataInTable();
    var getData;
    function insertDataInTable() {
        getData = GlobalJs.crmContentPanel._Xrm.Page.getAttribute("ddsm_config").getValue();
        if (getData == null || getData == undefined) {
            getData = GlobalJs.crmContentPanel._Xrm.Page.getAttribute("ddsm_dropdowndatajson").getValue();

        }
        parseData = JSON.parse(getData);
        if (parseData.MilestoneLookup.DisplayName != "" && parseData.MilestoneLookup.DisplayName != undefined) {
            $('#ParentEntity').text(parseData.MilestoneLookup.DisplayName);
        }
        if (parseData.TargetField != "" && parseData.TargetField != undefined) {
            $('#FieldOfParentEntity').text(parseData.TargetField.DisplayName);
        }
        if (parseData.ChildTargetEntity != "" && parseData.ChildTargetEntity != undefined) {
            $('#ChildEntity').text(parseData.ChildTargetEntity.DisplayName);
        }
        if (parseData.ChildTargetField != "" && parseData.ChildTargetField != undefined) {
            $('#FieldOfSelectedEntity').text(parseData.ChildTargetField.DisplayName);
        }
        if (parseData.Indicator != "" && parseData.Indicator != undefined) {
            $('#Indicator').text(parseData.Indicator.Name);
        }
    }
});
