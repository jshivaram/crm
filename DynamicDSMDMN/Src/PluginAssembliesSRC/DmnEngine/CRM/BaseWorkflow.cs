﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using DmnEngineApp.Service;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;

namespace DmnEngine.CRM
{
    public abstract class BaseWorkflow : CodeActivity
    {
        /// <summary>
        ///     IsBackgroundCalculation
        /// </summary>
        [Input("Background Calculation")]
        [Default("True")]
        public InArgument<bool> IsBackgroundCalculation { get; set; }

        [Input("CallBack Workflow Reference")]
        [ReferenceTarget("workflow")]
        public InArgument<EntityReference> CallBackWorkflowReference { get; set; }

        [Input("DmnJob Name")]
        public InArgument<string> DmnJobName { get; set; }

        /// <summary>
        ///     Only for Schedular
        /// </summary>
        [Input("CallBack Entity Logical Name")]
        public InArgument<string> InEntityLogicalName { get; set; }

        /// <summary>
        ///     Only for Schedular
        /// </summary>
        [Input("CallBack Object Id")]
        public InArgument<string> InEntityId { get; set; }

        [Output("OutputValue")]
        public OutArgument<string> OutputValue { get; set; }

        [Output("OutputMessage")]
        public OutArgument<string> OutputMessage { get; set; }

        [Input("Business Rule Reference №1")]
        [ReferenceTarget("ddsm_businessrule")]
        public InArgument<EntityReference> BusinessRuleReference { get; set; }

        [Input("Business Rule Reference №2")]
        [ReferenceTarget("ddsm_businessrule")]
        public InArgument<EntityReference> BusinessRuleReference02 { get; set; }

        [Input("Business Rule Reference №3")]
        [ReferenceTarget("ddsm_businessrule")]
        public InArgument<EntityReference> BusinessRuleReference03 { get; set; }

        [Input("Business Rule Reference №4")]
        [ReferenceTarget("ddsm_businessrule")]
        public InArgument<EntityReference> BusinessRuleReference04 { get; set; }

        [Input("Business Rule Reference №5")]
        [ReferenceTarget("ddsm_businessrule")]
        public InArgument<EntityReference> BusinessRuleReference05 { get; set; }

        [Input("Business Rule Reference №6")]
        [ReferenceTarget("ddsm_businessrule")]
        public InArgument<EntityReference> BusinessRuleReference06 { get; set; }

        [Input("Business Rule Reference №7")]
        [ReferenceTarget("ddsm_businessrule")]
        public InArgument<EntityReference> BusinessRuleReference07 { get; set; }

        [Input("Business Rule Reference №8")]
        [ReferenceTarget("ddsm_businessrule")]
        public InArgument<EntityReference> BusinessRuleReference08 { get; set; }

        [Input("Business Rule Reference №9")]
        [ReferenceTarget("ddsm_businessrule")]
        public InArgument<EntityReference> BusinessRuleReference09 { get; set; }

        [Input("Business Rule Reference №10")]
        [ReferenceTarget("ddsm_businessrule")]
        public InArgument<EntityReference> BusinessRuleReference10 { get; set; }


        /// <summary>
        ///     Actions that provide common implementation
        /// </summary>
        protected override void Execute(CodeActivityContext executionContext)
        {
            var rs = new List<KeyValuePair<string, string>>();
            var tracer = executionContext.GetExtension<ITracingService>();
            var sw = Stopwatch.StartNew();
            try
            {
                // The InputParameters collection contains all the data passed in the message request.
                if (!IsContextFull(executionContext, tracer))
                    return;

                // Obtain the execution context from the service provider.
                var context = executionContext.GetExtension<IWorkflowContext>();


                // Obtain the organization service reference.
                var factory = executionContext.GetExtension<IOrganizationServiceFactory>();
                var service = factory.CreateOrganizationService(context.UserId);
                service = GetSystemOrgService(service, factory, true);
                object target = null;
                // Obtain the target entity from the input parameters.
                if (context.InputParameters.Contains("Target") &&
                    context.InputParameters["Target"] != null
                )
                    target = context.InputParameters["Target"];
                else
                    target = GetEntityIdFromContext(context);


                var inEntityLogicalName = InEntityLogicalName.Get(executionContext);
                var inEntityId = InEntityId.Get(executionContext);


                //only for DMN JOBS
                if (!string.IsNullOrEmpty(inEntityLogicalName) && !string.IsNullOrEmpty(inEntityId))
                    target = new EntityReference(inEntityLogicalName, new Guid(inEntityId));

                var businessRule = BusinessRuleReference.Get(executionContext);


                var dmnDto = new DmnDto(executionContext, context, service, tracer);
                var dataService = new DataService(dmnDto);
                dmnDto.DataService = dataService;


                var inputArgsDto = new InputArgsDto
                {
                    CallBackWorkflowReference = CallBackWorkflowReference.Get(executionContext),
                    TargetEntity = GetTargetData(target, dmnDto)
                };
                inputArgsDto.BusinessRuleIds.Add(businessRule.Id);
                InitAllBusinessRules(executionContext, inputArgsDto);

                dmnDto.InputArgs = inputArgsDto;


                Main(dmnDto, businessRule);
                //tracer.Trace("--- Performance info ---");
                //tracer.Trace("Execution time: " + sw.Elapsed);

                rs.Add(new KeyValuePair<string, string>("SUCCESS", "Calculated successfully!"));
                var json = JsonConvert.SerializeObject(rs);
                OutputMessage.Set(executionContext, json);
            }
            catch (Exception e)
            {
                tracer.Trace($"*** The action {GetType().FullName} was stopped due to an error ***");
                rs.Add(new KeyValuePair<string, string>("ERROR", e.Message));
                tracer.Trace($"Error mesage: {e.Message}");
                var json = JsonConvert.SerializeObject(rs);
                OutputMessage.Set(executionContext, json);
                tracer.Trace("Execution time: " + sw.Elapsed);
            }
        }

        /// <summary>
        ///     Get target entity
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private Entity GetTargetData(object entity, DmnDto dmnDto)
        {
            try
            {
                if (entity == null)
                    throw new Exception("Can't get target from Context! entity is null");

                var target = entity;
                if (target is EntityReference)
                {
                    var sourceEntity = (EntityReference) entity;
                    return dmnDto.DataService.RetreiveEntity(sourceEntity.LogicalName, sourceEntity.Id);
                }

                if (target is Entity)
                {
                    var sourceEntity = (Entity) entity;
                    return dmnDto.DataService.RetreiveEntity(sourceEntity.LogicalName, sourceEntity.Id);
                }

                throw new Exception("Can't get target from Context! entity is null");
            }
            catch (Exception)
            {
                throw new Exception("Can't get target from Context");
            }
        }

        private void InitAllBusinessRules(CodeActivityContext executionContex, InputArgsDto inputArgsDto)
        {
            var businessRuleReference02 = BusinessRuleReference02.Get(executionContex);
            var businessRuleReference03 = BusinessRuleReference03.Get(executionContex);
            var businessRuleReference04 = BusinessRuleReference04.Get(executionContex);
            var businessRuleReference05 = BusinessRuleReference05.Get(executionContex);
            var businessRuleReference06 = BusinessRuleReference06.Get(executionContex);
            var businessRuleReference07 = BusinessRuleReference07.Get(executionContex);
            var businessRuleReference08 = BusinessRuleReference08.Get(executionContex);
            var businessRuleReference09 = BusinessRuleReference09.Get(executionContex);
            var businessRuleReference10 = BusinessRuleReference10.Get(executionContex);

            if (businessRuleReference02 != null)
                inputArgsDto.BusinessRuleIds.Add(businessRuleReference02.Id);
            if (businessRuleReference03 != null)
                inputArgsDto.BusinessRuleIds.Add(businessRuleReference03.Id);
            if (businessRuleReference04 != null)
                inputArgsDto.BusinessRuleIds.Add(businessRuleReference04.Id);
            if (businessRuleReference05 != null)
                inputArgsDto.BusinessRuleIds.Add(businessRuleReference05.Id);
            if (businessRuleReference06 != null)
                inputArgsDto.BusinessRuleIds.Add(businessRuleReference06.Id);
            if (businessRuleReference07 != null)
                inputArgsDto.BusinessRuleIds.Add(businessRuleReference07.Id);
            if (businessRuleReference08 != null)
                inputArgsDto.BusinessRuleIds.Add(businessRuleReference08.Id);
            if (businessRuleReference09 != null)
                inputArgsDto.BusinessRuleIds.Add(businessRuleReference09.Id);
            if (businessRuleReference10 != null)
                inputArgsDto.BusinessRuleIds.Add(businessRuleReference10.Id);
        }

        private object GetEntityIdFromContext(IWorkflowContext context)
        {
            if (context.InputParameters.Contains("EntityId") && context.InputParameters["EntityId"] != null)
            {
                var primaryEntityName = context.PrimaryEntityName;
                var primaryEntityId = context.PrimaryEntityId;
                return new EntityReference(primaryEntityName, primaryEntityId);
            }
            return null;
        }

        /// <summary>
        ///     Call action from system user
        /// </summary>
        /// <param name="Service"></param>
        /// <param name="ServiceFactory"></param>
        /// <param name="useSystem"></param>
        /// <returns></returns>
        public IOrganizationService GetSystemOrgService(IOrganizationService Service,
            IOrganizationServiceFactory ServiceFactory, bool useSystem = false)
        {
            return useSystem ? ServiceFactory.CreateOrganizationService(GetSystemUserId("SYSTEM", Service)) : Service;
        }

        public Guid GetSystemUserId(string name, IOrganizationService service)
        {
            var queryUsers = new QueryByAttribute
            {
                EntityName = "systemuser",
                ColumnSet = new ColumnSet("systemuserid")
            };
            queryUsers.AddAttributeValue("fullname", name);
            var retrievedUsers = service.RetrieveMultiple(queryUsers);
            var systemUserId = retrievedUsers.Entities[0].Id;
            return systemUserId;
        }


        /// <summary>
        ///     Start Executing a Action
        /// </summary>
        private void Main(DmnDto dmnDto, EntityReference businessRule)
        {
            if (isRemoteCalculation(dmnDto))
            {
                RunRemoteDmnCalcAsync(dmnDto);
                return;
            }


            Run(dmnDto);

            //dmnDto.Tracer.Trace("*** Action " + GetType().FullName + " is successfully executed ***");
        }

        /// <summary>
        /// </summary>
        /// <param name="dmnDto"></param>
        private void RunRemoteDmnCalcAsync(DmnDto dmnDto)
        {
            try
            {
                var remoteCalcDto = new RemoteCalcDto
                {
                    TargetEntityName = dmnDto.InputArgs.TargetEntity.LogicalName,
                    IsDataUploader = IsCreatoRecodTypeDu(dmnDto)
                };
                remoteCalcDto.RecordIds.Add(dmnDto.InputArgs.TargetEntity.Id);

                foreach (var item in dmnDto.InputArgs.BusinessRuleIds)
                    remoteCalcDto.BusinessRuleIds.Add(item.ToString());

                if (dmnDto.InputArgs.CallBackWorkflowReference != null)
                    remoteCalcDto.CallBackWorkflowId = dmnDto.InputArgs.CallBackWorkflowReference.Id.ToString();

                remoteCalcDto.UserId = dmnDto.Context.InitiatingUserId;
                var json = JsonConvert.SerializeObject(remoteCalcDto);
                var url = dmnDto.RemoteCalcSettings.Attributes["ddsm_dmnremotecalcapiurl"].ToString();
                var client = new HttpClient();
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                if (!IsBackgroundCalculation.Get(dmnDto.ExecutionContext))
                {
                    url = dmnDto.RemoteCalcSettings.Attributes["ddsm_syncdmnremotecalculationapiurl"].ToString();
                    var webClient = new WebClient {Headers = {[HttpRequestHeader.ContentType] = "application/json"}};
                    var response = webClient.UploadString(url, "POST", json);
                    var result = response.Replace("\"", "");
                    OutputValue.Set(dmnDto.ExecutionContext, result);
                    return;
                }

                client.PostAsync(url, content);
            }
            catch (Exception e)
            {
                dmnDto.Tracer.Trace($"RunRemoteDmnCalc Exception{e.Message}");
            }
        }

        private static bool IsCreatoRecodTypeDu(DmnDto dmnDto)
        {
            object creatorrecordtype = null;
            if (dmnDto.InputArgs.TargetEntity.Attributes.TryGetValue("ddsm_creatorrecordtype", out creatorrecordtype))
                return (CreatorRecordType) ((OptionSetValue) creatorrecordtype).Value == CreatorRecordType.DataUploader;
            return false;
        }

        private bool isRemoteCalculation(DmnDto dmnDto)
        {
            var isRemoteCalc = false;
            var columns = new ColumnSet("ddsm_dmnremotecalculation", "ddsm_dmnremotecalcapiurl",
                "ddsm_syncdmnremotecalculationapiurl");
            // Create query expression.
            var query = new QueryExpression
            {
                ColumnSet = columns,
                EntityName = "ddsm_admindata",
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_name", ConditionOperator.Equal, "Admin Data")
                    }
                }
            };

            var adminData = dmnDto.Service.RetrieveMultiple(query).Entities[0];

            object isRemoteCalcTmp = null;
            if (adminData.Attributes.TryGetValue("ddsm_dmnremotecalculation", out isRemoteCalcTmp))
                isRemoteCalc = (bool) isRemoteCalcTmp;


            object urlApiTmp = null;
            object urlApiTmpSync = null;
            if (adminData.Attributes.TryGetValue("ddsm_dmnremotecalcapiurl", out urlApiTmp) &&
                adminData.Attributes.TryGetValue("ddsm_syncdmnremotecalculationapiurl", out urlApiTmpSync))
                if (isRemoteCalc)
                {
                    dmnDto.RemoteCalcSettings = adminData;
                    return true;
                }


            return false;
        }

        /// <summary>
        ///     Abstract method should be implemented in a child class
        /// </summary>
        protected abstract void Run(DmnDto dmnDto);

        /// <summary>
        ///     Obtain the execution context from the service provider.
        /// </summary>
        /// <param name="executionContext"></param>
        /// <returns></returns>
        private bool IsContextFull(CodeActivityContext executionContext, ITracingService Tracer)
        {
            if (executionContext == null) throw new ArgumentNullException(nameof(executionContext));
            var rs = true;
            try
            {
                if (executionContext.GetExtension<IWorkflowContext>() == null)
                    rs = false;
            }
            catch (Exception ex)
            {
                Tracer.Trace("IsContextFull:" + ex.Message);
                return false;
            }
            return rs;
        }
    }

    public enum CreatorRecordType
    {
        UnassignedValue = -2147483648,
        Front = 962080000,
        DataUploader = 962080001,
        Portal = 962080002
    }
}