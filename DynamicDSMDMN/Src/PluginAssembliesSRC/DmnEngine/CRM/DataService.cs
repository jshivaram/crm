﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;

namespace DmnEngineApp.Service
{
    public class DataService
    {
        private readonly DmnDto _dmnDto;
        private readonly IOrganizationService _orgService;
        public ConcurrentDictionary<string, object> _sourceValuesCache;

        public DataService()
        {
        }

        public DataService(DmnDto dmnDto)
        {
            _dmnDto = dmnDto;
            _orgService = dmnDto.Service;
            _sourceValuesCache = new ConcurrentDictionary<string, object>();
        }

        /// <summary>
        ///     Get target entity reference
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public EntityReference GetEntityReferenceById(IOrganizationService orgService, string logicalName, string id)
        {
            if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(logicalName))
                return null;

            var entity = RetreiveEntity(logicalName, id, new ColumnSet());
            return entity?.ToEntityReference();
        }

        /// <summary>
        ///     Retreive entity
        /// </summary>
        /// <param name="orgService"></param>
        /// <param name="logicalName"></param>
        /// <param name="recordId"></param>
        /// <param name="columnSet"></param>
        /// <returns></returns>
        public Entity RetreiveEntity(string logicalName, string recordId, ColumnSet columnSet = null)
        {
            if (string.IsNullOrEmpty(recordId))
                return null;

            if (columnSet == null || columnSet.Columns.Count == 0)
                columnSet = new ColumnSet();

            return RetreiveEntity(logicalName, recordId);
        }

        /// <summary>
        ///     Retreive Entity
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <param name="recordId"></param>
        /// <returns></returns>
        public Entity RetreiveEntity(string entityLogicalName, Guid recordId)
        {
            try
            {
                var entityCache = _dmnDto.EntityCache;

                if (entityCache.ContainsKey(recordId))
                {
                    var tmp = new Entity();
                    if (entityCache.TryGetValue(recordId, out tmp))
                        return tmp;
                }


                var entity = _orgService.Retrieve(entityLogicalName, recordId, new ColumnSet(true));

                entityCache.TryAdd(recordId, entity);
                return entity;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        ///     Retreive Entity
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <param name="recordId"></param>
        /// <returns></returns>
        public Entity RetreiveEntity(string entityLogicalName, string recordId)
        {
            return RetreiveEntity(entityLogicalName, new Guid(recordId));
        }

        /// <summary>
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <param name="attName"></param>
        /// <returns></returns>
        public IEnumerable<AttributeMetadata> GetAttributeMetadata(string entityLogicalName, string attName)
        {
            try
            {
                var entityMetadata = GetEntityMetadata(entityLogicalName);

                return entityMetadata?.Attributes.Where(att => att.LogicalName.ToLower() == attName.ToLower());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception(e.Message);
            }
        }

        public string GetEntityDisplayName(string entityLogicalName)
        {
            try
            {
                var entityMetadata = GetEntityMetadata(entityLogicalName);

                return entityMetadata.DisplayName.UserLocalizedLabel.Label;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception(e.Message);
            }
        }

        public void RetreiveBatchEntities(List<Guid> entitiesGuids, string entityLogicalName, string fieldName)
        {
            //var entityCache = DmnDto.EntityCache;
            var entityCache = _dmnDto.EntityCache;
            var query = new QueryExpression
            {
                EntityName = entityLogicalName,
                ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression(fieldName, ConditionOperator.In, entitiesGuids)
                    }
                }
            };
            var rs = _orgService.RetrieveMultiple(query).Entities;
            foreach (var item in rs)
                entityCache.TryAdd(item.Id, item);
        }

        /// <summary>
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <returns></returns>
        private EntityMetadata GetEntityMetadata(string entityLogicalName)
        {
            try
            {
                var currentEntityMetadata = new EntityMetadata();
                var entityMetadata = _dmnDto.EntityMetadata;
                if (entityMetadata.ContainsKey(entityLogicalName))
                {
                    var tmp = new EntityMetadata();
                    if (entityMetadata.TryGetValue(entityLogicalName, out tmp))
                        currentEntityMetadata = tmp;
                }
                else
                {
                    var request = new RetrieveEntityRequest
                    {
                        EntityFilters = EntityFilters.Attributes,
                        LogicalName = entityLogicalName,
                        RetrieveAsIfPublished = true
                    };
                    var response = (RetrieveEntityResponse) _orgService.Execute(request);
                    currentEntityMetadata = response.EntityMetadata;
                    entityMetadata.TryAdd(entityLogicalName, currentEntityMetadata);
                    _dmnDto.EntityDisplayName.TryAdd(entityLogicalName,
                        response.EntityMetadata.DisplayName.UserLocalizedLabel.Label);
                }
                return currentEntityMetadata;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception(e.Message);
            }
        }


        /// <summary>
        ///     Convert list of ids  to list  of Guid
        /// </summary>
        /// <param name="listIds"></param>
        /// <returns></returns>
        public List<Guid> ConvertListIdToListGuid(List<string> listIds)
        {
            var rs = new List<Guid>();
            foreach (var id in listIds)
                rs.Add(new Guid(id));
            return rs;
        }
    }
}