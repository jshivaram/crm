﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using DmnEngine.Core;
using DmnEngine.Core.CalculationHelpers;
using DmnEngine.Core.CustomException;
using DmnEngine.Core.DTO.Rule;
using DmnEngine.Core.Executor;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

namespace DmnEngine.CRM
{
    public class DmnDecisionService : BaseWorkflow
    {
        private const string BusinessRuleEntity = "ddsm_businessrule";


        private DefaultDmnEngine _defaultDmnEngine;
        private DmnDto _dmnDto;
        private DmnService _dmnService;


        /// <summary>
        ///     Start
        /// </summary>
        protected override void Run(DmnDto dmnDto)
        {
            // Obtain the target entity from the input parameters.
            _dmnDto = dmnDto;
            InitTargetEntityReference();
            PrepareBusinessRules();
        }

        /// <summary>
        ///     Init targer entity
        /// </summary>
        private void InitTargetEntityReference()
        {
            var inputTarget = _dmnDto.InputArgs.TargetEntity;
            var targetEntity = _dmnDto.DataService.RetreiveEntity(inputTarget.LogicalName, inputTarget.Id);
            if (targetEntity == null)
                return;
            _dmnDto.TargetEntity = targetEntity;
        }


        /// <summary>
        ///     Get lisf of business rules and start execution decisions
        /// </summary>
        private void PrepareBusinessRules()
        {
            _dmnService = new DmnService(_dmnDto);


            var listBusinessRulesGuid = _dmnDto.InputArgs.BusinessRuleIds;
            //get business rules
            var businessRules = _dmnService.GetDmnSchema(listBusinessRulesGuid);
            if (businessRules.Entities.Count == 0)
                return;
            _dmnDto.BusinessRules = businessRules;
            //execute business rules
            ExecuteBusinessRules();
        }

        /// <summary>
        /// </summary>
        /// <param name="businessRules"></param>
        private void ExecuteBusinessRules()
        {
            var businessRuleName = string.Empty;
            try
            {
                var businessRules = _dmnDto.BusinessRules;
                if (businessRules == null || businessRules.Entities.Count == 0)
                    throw new Exception($"Doesn't exist DMN rules for this entity!");
                foreach (var businessRule in businessRules.Entities)
                {
                    var sw = Stopwatch.StartNew();
                    businessRuleName = businessRule.Attributes["ddsm_name"].ToString();
                    _dmnDto.Tracer.Trace($"--------start------businessRuleName={businessRuleName}");
                    var decisions = _dmnService.GetDecisions(businessRule);

                    if (decisions == null)
                        continue;
                    ExecuteDecision(decisions);

                    if (!_dmnDto.InputArgs.IsExecuteRule)
                        UpdateCrmDb();

                    _dmnDto.Tracer.Trace($"******end***businessRuleName={businessRuleName}, time={sw.Elapsed}");
                }

                ExecuteCallBackWorkflow();
            }
            catch (ExecutorException)
            {
                throw new Exception(
                    $"Decision cannot be executed. Please check the following decision:<br/>Business rule name: {businessRuleName}");
            }
        }

        private void ExecuteCallBackWorkflow()
        {
            var callBackRecordId = _dmnDto.InputArgs.CallBackRecordId;
            if (callBackRecordId == Guid.Empty)
                callBackRecordId = _dmnDto.TargetEntity.Id;
            if (_dmnDto.InputArgs.CallBackWorkflowReference == null ||
                _dmnDto.InputArgs.CallBackWorkflowReference.Id == Guid.Empty)
                return;
            _dmnDto.Tracer.Trace(
                $"WorkflowId={_dmnDto.InputArgs.CallBackWorkflowReference.Id}, EntityId = {callBackRecordId}");
            var request = new ExecuteWorkflowRequest
            {
                WorkflowId = _dmnDto.InputArgs.CallBackWorkflowReference.Id,
                EntityId = callBackRecordId
            };
            // Execute the workflow.
            _dmnDto.Service.Execute(request);
        }

        private void UpdateCrmDb()
        {
            var outputEntities = _dmnDto.OutputEntities;
            var start = 0;
            var offset = 500;
            var counter = 1;
            if (outputEntities.Count > offset)
            {
                counter = outputEntities.Count / offset;
                var remainder = outputEntities.Count % offset;
                if (remainder > 0)
                    counter++;
            }
            for (var i = 0; i < counter; i++)
            {
                var requestWithNoResults = new ExecuteMultipleRequest
                {
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = false,
                        ReturnResponses = false
                    },
                    Requests = new OrganizationRequestCollection()
                };
                var btachEntities = outputEntities.Skip(start).Take(offset);
                foreach (var entity in btachEntities)
                {
                    var updateRequest = new UpdateRequest {Target = entity.Value};

                    _dmnDto.Tracer.Trace($"-------LogicalName={entity.Value.LogicalName}, Id={entity.Value.Id}");
                    foreach (var item in entity.Value.Attributes)
                    {
                        var value = item.Value;
                        if (value is Money)
                            value = ((Money) value).Value;

                        _dmnDto.Tracer.Trace($"attr={item.Key}, value={value}");
                    }
                    _dmnDto.Tracer.Trace($"========");

                    requestWithNoResults.Requests.Add(updateRequest);
                }

                _dmnDto.Service.Execute(requestWithNoResults);
                //Log.Debug($"all:{_dmnDto.OutputEntities.Count}. updated {start + offset} time:{sw.Elapsed}");
                start += offset;
            }
            _dmnDto.OutputEntities.Clear();
        }

        /// <summary>
        ///     Execute decision
        /// </summary>
        /// <param name="decision"></param>
        /// <param name="decisions"></param>
        /// <param name="decisionNumber"></param>
        private void ExecuteDecision(List<OutputEntryDto> decisions)
        {
            try
            {
                var outputEntities = _dmnDto.OutputEntities;


                //run execute decision
                foreach (var decision in decisions)
                {
                    if (decision == null) continue;
                    RunDecision(outputEntities, decision);
                }
            }
            catch (Exception e)
            {
                _dmnDto.Tracer.Trace(e.Message);
            }
        }


        /// <summary>
        ///     Execute decision
        /// </summary>
        /// <param name="decision"></param>
        /// <param name="decisions"></param>
        /// <param name="decisionNumber"></param>
        public void RunDecision(ConcurrentDictionary<Guid, Entity> outputEntities, OutputEntryDto decision)
        {
            try
            {
                var dmnDecisionExecutor = new DmnDecisionExecutorBase(_dmnDto, decision.DataType,
                    decision.OutputFullPath);

                var outputEntity = dmnDecisionExecutor._outputEntity;
                var result = string.Empty;
                if (DmnAction.SetValue.ToString().ToLower() ==
                    GetExprFunction(decision.Value.TrimStart('{').TrimEnd('}')))
                {
                    result = ExecuteDecision(decision.Value.TrimStart('{').TrimEnd('}'), _dmnDto);
                }
                else if (DmnAction.ExecuteDmn.ToString().ToLower() ==
                         GetExprFunction(decision.Value.TrimStart('{').TrimEnd('}')))
                {
                    var linkName = GetExprArgs(decision.Value.TrimStart('{').TrimEnd('}'), _dmnDto);
                    CreateDmnJobs(linkName);
                    return;
                }
                else if (DmnAction.OutputValue.ToString().ToLower() ==
                         GetExprFunction(decision.Value.TrimStart('{').TrimEnd('}')))
                {
                    var expression = decision.Value.TrimStart('{').TrimEnd('}');
                    var formula = expression;
                    if (expression.IndexOf('^') != -1)
                    {
                        var args = expression.Split('^');
                        formula = "{{" + args[1] + "^" + args[2] + "}}";
                    }

                    var formulaCalculator = new FormulaCalculator(dmnDecisionExecutor);
                    result = formulaCalculator.Calculate(formula, decision.DataType, _dmnDto);
                    OutputValue.Set(_dmnDto.ExecutionContext, result);
                    _dmnDto.InputArgs.IsExecuteRule = true;
                    return;
                }
                else
                {
                    var formulaCalculator = new FormulaCalculator(dmnDecisionExecutor);
                    result = formulaCalculator.Calculate(decision.Value, decision.DataType, _dmnDto);
                }
                var entity = new Entity(outputEntity.LogicalName, outputEntity.Id);
                if (!outputEntities.ContainsKey(entity.Id))
                    outputEntities.TryAdd(outputEntity.Id, entity);

                var tmpOut = new Entity();

                if (!outputEntities.TryGetValue(outputEntity.Id, out tmpOut))
                    return;

                UpdateEntity(tmpOut, dmnDecisionExecutor._outputField, result);
                UpdateCacheData(entity, dmnDecisionExecutor._outputField, result);
            }
            catch (Exception e)
            {
                _dmnDto.Tracer.Trace(e.Message);
            }
        }

        public void UpdateCacheData(Entity entity, string attr, string value)
        {
            if (!_dmnDto.EntityCache.ContainsKey(entity.Id))
                _dmnDto.EntityCache.TryAdd(entity.Id, entity);

            var tmpOut = new Entity();

            if (!_dmnDto.EntityCache.TryGetValue(entity.Id, out tmpOut))
                return;

            UpdateEntity(tmpOut, attr, value);
        }

        private void UpdateEntity(Entity entity, string attr, string value)
        {
            if (entity == null || string.IsNullOrEmpty(attr))
                return;
            var attMetadata = _dmnDto.DataService.GetAttributeMetadata(entity.LogicalName, attr).FirstOrDefault();
            if (attMetadata == null)
                return;
            var attrType = attMetadata.AttributeType;

            if (attrType == null)
                return;

            switch (attrType)
            {
                case AttributeTypeCode.Integer:
                    entity.Attributes[attr] = (int) Math.Round(double.Parse(value));
                    break;
                case AttributeTypeCode.Decimal:
                    decimal amount;
                    decimal.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out amount);
                    entity.Attributes[attr] = amount;
                    break;
                case AttributeTypeCode.Double:
                    entity.Attributes[attr] = double.Parse(value);
                    break;
                case AttributeTypeCode.Money:
                    entity.Attributes[attr] = new Money(decimal.Parse(value));
                    break;
                case AttributeTypeCode.String:
                    entity.Attributes[attr] = value.GetType() == typeof(string) ? value : value;
                    break;
                case AttributeTypeCode.Boolean:
                    entity.Attributes[attr] = bool.Parse(value);
                    break;
                case AttributeTypeCode.DateTime:
                    DateTime? date = null;
                    if (value == "''" || string.IsNullOrEmpty(value))
                    {
                        entity.Attributes[attr] = date;
                        break;
                    }
                    date = DateTime.Parse(value);
                    entity.Attributes[attr] = date;
                    break;
                case AttributeTypeCode.Picklist:
                    if (value == "''")
                    {
                        entity.Attributes[attr] = null;
                        break;
                    }
                    var key = GetPickListKeyByValue(value, attMetadata);
                    var option = new OptionSetValue(key);
                    entity.Attributes[attr] = option;
                    break;
                case AttributeTypeCode.Memo:
                    entity.Attributes[attr] = value.GetType() == typeof(string) ? value : value;
                    break;
            }
        }


        private int GetPickListKeyByValue(string value, AttributeMetadata fieldMetadata)
        {
            try
            {
                var attMetadata = (EnumAttributeMetadata) fieldMetadata;
                var optionMetadata = attMetadata.OptionSet.Options.FirstOrDefault();
                var key = optionMetadata.Value;

                var firstOrDefault = attMetadata.OptionSet.Options
                    .FirstOrDefault(x => x.Label.UserLocalizedLabel.Label == value);

                if (firstOrDefault != null)
                    key = firstOrDefault.Value;

                return (int) key;
            }
            catch (Exception)
            {
                throw new ExecutorException();
            }
        }

        /// <summary>
        ///     Execute decision
        /// </summary>
        /// <param name="decision"></param>
        /// <param name="dmnDecisionExecutor"></param>
        private string ExecuteDecision(string decision, DmnDto dmnDto)
        {
            return GetExprArgs(decision, dmnDto);
        }

        /// <summary>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static string[] ConvertArgsToArray(string args)
        {
            return args.IndexOf(',') != -1 ? args.Split(',') : new[] {args};
        }

        /// <summary>
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private static string GetExprFunction(string expression)
        {
            return expression.IndexOf('^') != -1 ? expression.Split('^')[0].ToLower() : expression.ToLower();
        }

        /// <summary>
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public string GetExprArgs(string expression, DmnDto dmnDto)
        {
            return expression.IndexOf('^') != -1
                ? GetSourceValue(expression.Split('^')[1], dmnDto)
                : expression.Split('^')[1];
            //return expression.IndexOf('^') != -1 ? expression.Split('^')[1] : expression;
        }


        public string GetSourceValue(string fullFieldName, DmnDto dmnDto)
        {
            var targetEntity = dmnDto.TargetEntity;
            var fieldParts = fullFieldName.Split('.');

            if (fieldParts.Length == 1)
                return fieldParts.FirstOrDefault();

            if (fieldParts.First() != targetEntity.LogicalName)
                return null;

            //remove base entity name from array
            fieldParts = fieldParts.Skip(1).ToArray();

            var baseEntity = RetreiveEntity(dmnDto, targetEntity.LogicalName, targetEntity.Id);
            var attMetadata =
                _dmnDto.DataService.GetAttributeMetadata(baseEntity.LogicalName, fieldParts.FirstOrDefault())
                    .FirstOrDefault();
            //get first field
            object firstFieldValue = null;
            baseEntity.Attributes.TryGetValue(fieldParts.FirstOrDefault(), out firstFieldValue);
            if (firstFieldValue == null)
                return GetEmptyResultByType(attMetadata);

            //if field is not lookup
            if (firstFieldValue.GetType() != typeof(EntityReference))
                return GetRS(firstFieldValue);
            //if field is lookup
            object lookupValue = null;
            baseEntity.Attributes.TryGetValue(fieldParts.First(), out lookupValue);
            var resultValue = GetLookupInnerValue(dmnDto, lookupValue as EntityReference, fieldParts.Skip(1).ToArray());
            return resultValue.ToString();
        }

        private string GetRS(object value)
        {
            if (value == null)
                return "0";
            if (value.GetType() == typeof(Money))
                return (value as Money).Value.ToString();
            return value.ToString();
        }

        private object GetLookupInnerValue(DmnDto dmnDto, EntityReference lookup, string[] fields)
        {
            if (lookup == null)
                return null;

            object value = null;

            var entity = RetreiveEntity(dmnDto, lookup.LogicalName, lookup.Id);
            entity.Attributes.TryGetValue(fields.First(), out value);

            if (value is EntityReference)
                value = GetLookupInnerValue(dmnDto, value as EntityReference, fields.Skip(1).ToArray());

            return value;
        }

        private string GetEmptyResultByType(AttributeMetadata attrType)
        {
            var defaultNumericRs = "0";
            switch (attrType.AttributeType)
            {
                case AttributeTypeCode.Integer:
                    return defaultNumericRs;
                case AttributeTypeCode.Decimal:
                    return defaultNumericRs;
                case AttributeTypeCode.Double:
                    return defaultNumericRs;
                case AttributeTypeCode.Money:
                    return defaultNumericRs;
                case AttributeTypeCode.String:
                    return string.Empty;
                case AttributeTypeCode.Boolean:
                    return "false";
                case AttributeTypeCode.DateTime:
                    return string.Empty;
                case AttributeTypeCode.Picklist:
                    return string.Empty;
                case AttributeTypeCode.Memo:
                    return string.Empty;
            }
            return string.Empty;
        }

        private Entity RetreiveEntity(DmnDto dmnDto, string entityLogicalName, Guid targetEntityId)
        {
            return dmnDto.DataService.RetreiveEntity(entityLogicalName, targetEntityId);
        }

        private void CreateDmnJobs(string linkName)
        {
            var args = ConvertArgsToArray(linkName);
            var relService = new RelationshipService(_dmnDto.Service, _dmnDto.TargetEntity, args[1]);
            var o2M = relService.GetOneToManyRelationshipsMetadata();
            var callBackRecordId = Guid.Empty;
            var m2O = relService.GetManyToOneRelationshipsMetadata();
            var businessRuleId = args[0];

            var o2MId = new List<Guid>();
            var entityLogicalName = string.Empty;
            var entityIdFieldName = string.Empty;
            if (o2M != null)
                foreach (var entity in o2M)
                {
                    o2MId.Add(entity.Id);
                    entityLogicalName = entity.LogicalName;
                    entityIdFieldName = $"{entityLogicalName}id";
                    callBackRecordId = entity.Id;
                }

            if (o2M != null)
                _dmnDto.DataService.RetreiveBatchEntities(o2MId, entityLogicalName, entityIdFieldName);
            foreach (var entity in o2M)
            {
                var inputArg = CreateInputArgDto(entity, businessRuleId);
                var dmnDto = new DmnDto(_dmnDto.ExecutionContext, _dmnDto.Context, _dmnDto.Service, _dmnDto.Tracer)
                {
                    EntityCache = _dmnDto.EntityCache,
                    EntityMetadata = _dmnDto.EntityMetadata,
                    EntityDisplayName = _dmnDto.EntityDisplayName,
                    OutputEntities = _dmnDto.OutputEntities,
                    InputArgs = inputArg,
                    DataService = _dmnDto.DataService
                };

                var dmnDecisionService = new DmnDecisionService();
                dmnDecisionService.Run(dmnDto);
            }

            if (m2O != null)
            {
                var inputArg = CreateInputArgDto(m2O, businessRuleId);
                var dmnDto = new DmnDto(_dmnDto.ExecutionContext, _dmnDto.Context, _dmnDto.Service, _dmnDto.Tracer)
                {
                    EntityCache = _dmnDto.EntityCache,
                    EntityMetadata = _dmnDto.EntityMetadata,
                    EntityDisplayName = _dmnDto.EntityDisplayName,
                    OutputEntities = _dmnDto.OutputEntities,
                    InputArgs = inputArg,
                    DataService = _dmnDto.DataService
                };

                var dmnDecisionService = new DmnDecisionService();
                dmnDecisionService.Run(dmnDto);
            }
            _dmnDto.InputArgs.CallBackRecordId = callBackRecordId;
        }

        private InputArgsDto CreateInputArgDto(Entity entity, string businessRuleId)
        {
            var inputParams = new InputArgsDto();
            inputParams.TargetEntity = entity;
            inputParams.BusinessRuleIds.Add(new Guid(businessRuleId));
            inputParams.IsExecuteRule = true;
            return inputParams;
        }

        //private EntityReference _targetEntity;

        /// <summary>
        ///     Main  DMN Actions
        /// </summary>
        private enum DmnAction
        {
            SetValue,
            ExecuteDmn,
            OutputValue
        }
    }
}