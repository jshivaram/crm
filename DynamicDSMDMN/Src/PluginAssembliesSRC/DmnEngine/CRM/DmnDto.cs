﻿using System;
using System.Activities;
using System.Collections.Concurrent;
using DmnEngineApp.Service;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Workflow;

public class DmnDto
{
    public EntityCollection BusinessRules;
    //cache
    public ConcurrentDictionary<Guid, Entity> EntityCache;

    public ConcurrentDictionary<string, string> EntityDisplayName;

    public ConcurrentDictionary<string, EntityMetadata> EntityMetadata;


    public ConcurrentDictionary<Guid, Entity> OutputEntities;


    public DmnDto(CodeActivityContext executionContext, IWorkflowContext context, IOrganizationService service,
        ITracingService tracer)
    {
        Context = context;
        Service = service;
        Tracer = tracer;
        ExecutionContext = executionContext;
        BusinessRules = new EntityCollection();

        EntityMetadata = new ConcurrentDictionary<string, EntityMetadata>();
        EntityCache = new ConcurrentDictionary<Guid, Entity>();
        EntityDisplayName = new ConcurrentDictionary<string, string>();
        OutputEntities = new ConcurrentDictionary<Guid, Entity>();
        DataService = new DataService();
    }

    //Execution Context
    public IWorkflowContext Context { get; private set; }

    //Oganization Service
    public IOrganizationService Service { get; private set; }

    //Target Entity
    public Entity TargetEntity { get; set; }

    //Tracing Service
    public ITracingService Tracer { get; private set; }
    public CodeActivityContext ExecutionContext { get; private set; }

    public Entity RemoteCalcSettings { get; set; }

    public DataService DataService { get; set; }
    public InputArgsDto InputArgs { get; set; }
}