﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

public class InputArgsDto
{
    public InputArgsDto()
    {
        BusinessRuleIds = new List<Guid>();
        IsExecuteRule = false;

        CallBackWorkflowReference = new EntityReference();
        TargetEntity = new Entity();
    }
    
    public List<Guid> BusinessRuleIds { get; set; }
    public Entity TargetEntity { get; set; }

    //for callback
    public EntityReference CallBackWorkflowReference { get; set; }
    public Guid CallBackRecordId { get; set; }
    public bool IsExecuteRule { get; set; }
}