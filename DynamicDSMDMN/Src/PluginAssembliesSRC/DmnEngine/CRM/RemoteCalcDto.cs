﻿using System;
using System.Collections.Generic;

public class RemoteCalcDto
{
    public RemoteCalcDto()
    {
        BusinessRuleIds = new List<string>();
        RecordIds = new List<Guid>();
        CallBackRecordId = Guid.Empty;
        UserId = Guid.Empty;
        IsDataUploader = false;
    }

    public string TargetEntityName { get; set; }
    public List<Guid> RecordIds { get; set; }
    public List<string> BusinessRuleIds { get; set; }

    public string CallBackWorkflowId { get; set; }
    public string CallBackEntityLogicalName { get; set; }
    public Guid CallBackRecordId { get; set; }
    public Guid UserId { get; set; }
    public bool IsDataUploader { get; set; }
}