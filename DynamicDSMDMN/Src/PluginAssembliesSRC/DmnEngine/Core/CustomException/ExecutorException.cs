﻿using System;

namespace DmnEngine.Core.CustomException
{
    public class ExecutorException : Exception
    {
        public ExecutorException()
        {
        }

        public ExecutorException(string message) : base(message)
        {
        }

        public ExecutorException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}