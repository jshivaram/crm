﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace DmnEngine.Core
{
    internal class DMNActivities
    {
        public DMNActivities(IOrganizationService orgService, Entity outputEntity, Entity businessRule, string output)
        {
            _orgService = orgService;
            _outputEntity = outputEntity;
            _businessRule = businessRule;
            _output = output;
        }

        public DMNActivities()
        {
        }

        public DMNActivities(IOrganizationService orgService, Entity businessRule)
        {
            _orgService = orgService;
            _businessRule = businessRule;
        }

        private IOrganizationService _orgService { get; }
        private Entity _outputEntity { get; }
        private Entity _businessRule { get; }
        private Entity _dmnActivity { get; set; }
        private string _output { get; }


        public void CreateActivity()
        {
            /* var isExistActivity = IsExistActivity();
             if (isExistActivity)
             {
                 StartActivity();
                 return;
             }*/
            Add();
        }


        public void FinishActivity(string entityName)
        {
            var activities = GetActiveActivitiesByEntity(entityName);

            var requestWithResults = new ExecuteMultipleRequest
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            // Add a CreateRequest for each entity to the request collection.
            foreach (var activity in activities.Entities)
            {
                activity["ddsm_activitystatus"] = new OptionSetValue((int) DmnActivityStatus.Finished);
                var updateRequest = new UpdateRequest {Target = activity};
                requestWithResults.Requests.Add(updateRequest);
            }

            // Execute all the requests in the request collection using a single web method call.
            //var responseWithResults =(ExecuteMultipleResponse) _orgService.Execute(requestWithResults);
            foreach (UpdateRequest item in requestWithResults.Requests)
                _orgService.Update(item.Target);
        }

        public EntityCollection GetActiveActivitiesByEntity(string entityName)
        {
            // Build a query expression that we will turn into FetchXML.
            /* var filters = new FilterExpression
             {
                 FilterOperator = LogicalOperator.And,
                 Conditions =
                 {
                     new ConditionExpression("ddsm_entityname", ConditionOperator.Equal,
                         entityName)
                 }
             };*/

            // Construct query            
            var condition = new ConditionExpression
            {
                AttributeName = "ddsm_businessrule",
                Operator = ConditionOperator.Equal
            };
            condition.Values.Add(_businessRule.Id.ToString());

            //Create a column set.
            var columns = new ColumnSet("ddsm_activitystatus");
            // Create query expression.
            var query = new QueryExpression
            {
                ColumnSet = columns,
                EntityName = "ddsm_dmnactivities"
            };
            query.Criteria.AddCondition(condition);
            //query.Criteria.AddFilter(filters);
            return _orgService.RetrieveMultiple(query);
        }

        private void StartActivity()
        {
            _dmnActivity["ddsm_activitystatus"] = new OptionSetValue((int) DmnActivityStatus.In_Progress);
            _orgService.Update(_dmnActivity);
        }

        private void Add()
        {
            var dmnactivities = new Entity("ddsm_dmnactivities");

            if (_outputEntity == null)
                return;


            dmnactivities["ddsm_name"] = _output;
            dmnactivities["ddsm_entityid"] = _outputEntity.Id.ToString();
            dmnactivities["ddsm_entityname"] = _outputEntity.LogicalName;

            dmnactivities["ddsm_activitystatus"] = new OptionSetValue((int) DmnActivityStatus.In_Progress);
            dmnactivities["ddsm_businessrule"] =
                new EntityReference(_businessRule.LogicalName, _businessRule.Id);

            var id = _orgService.Create(dmnactivities);
            _dmnActivity = _orgService.Retrieve("ddsm_dmnactivities", id, new ColumnSet("ddsm_activitystatus"));
        }

        private bool IsExistActivity()
        {
            // Build a query expression that we will turn into FetchXML.
            var filters = new FilterExpression
            {
                FilterOperator = LogicalOperator.And,
                Conditions =
                {
                    new ConditionExpression("ddsm_entityid", ConditionOperator.Equal,
                        _outputEntity.Id.ToString()),
                    new ConditionExpression("ddsm_entityname", ConditionOperator.Equal,
                        _outputEntity.LogicalName)
                }
            };

            // Construct query            
            var condition = new ConditionExpression
            {
                AttributeName = "ddsm_businessrule",
                Operator = ConditionOperator.Equal
            };
            condition.Values.Add(_businessRule.Id.ToString());


            //Create a column set.
            var columns = new ColumnSet("ddsm_entityid", "ddsm_entityname", "ddsm_businessrule", "ddsm_activitystatus");
            // Create query expression.
            var query = new QueryExpression
            {
                ColumnSet = columns,
                EntityName = "ddsm_dmnactivities"
            };
            query.Criteria.AddCondition(condition);
            query.Criteria.AddFilter(filters);
            var result = _orgService.RetrieveMultiple(query);
            if (result.Entities.Count == 0)
                return false;
            _dmnActivity = result.Entities[0];
            return true;
        }


        private enum DmnActivityStatus
        {
            Not_Started = 962080000,
            In_Progress = 962080001,
            Finished = 962080002
        }
    }
}