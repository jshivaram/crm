﻿namespace DmnEngine.Core.DTO
{
    public class DmnFunctionDTO
    {
        public string Base { get; set; }
        public int Position { get; set; }
    }
}