﻿namespace DmnEngine.Core.DTO
{
    public class DmnFunctionDTOResult
    {
        public DmnFunctionDTO DmnFunction { get; set; }
        public string Result { get; set; }
    }
}