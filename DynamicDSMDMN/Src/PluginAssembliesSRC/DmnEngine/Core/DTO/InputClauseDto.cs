﻿namespace DmnEngine.Core.DTO
{
    public class InputClauseDto
    {
        public InputClauseDto()
        {
            InputExpression = new InputExpressionDto();
        }

        public string Id { get; set; }

        public string Field { get; set; }

        public InputExpressionDto InputExpression { get; set; }
    }
}