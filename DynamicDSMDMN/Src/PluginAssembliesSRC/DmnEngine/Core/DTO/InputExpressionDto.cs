﻿namespace DmnEngine.Core.DTO
{
    public class InputExpressionDto
    {
        public string Id { get; set; }

        public string TypeRef { get; set; }

        public string Value { get; set; }
    }
}