﻿using System.Collections.Generic;

namespace DmnEngine.Core.DTO.Rule
{
    public class DecisionRuleDto
    {
        public DecisionRuleDto()
        {
            InputEntries = new List<InputEntryDto>();
            OutputEntries = new List<OutputEntryDto>();
        }

        public string Id { get; set; }

        public List<InputEntryDto> InputEntries { get; set; }

        public List<OutputEntryDto> OutputEntries { get; set; }
    }
}