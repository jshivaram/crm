﻿namespace DmnEngine.Core.DTO.Rule
{
    public class InputEntryDto
    {
        public string Id { get; set; }

        public string Value { get; set; }

        public string DataType { get; set; }

        public string Field { get; set; }
    }
}