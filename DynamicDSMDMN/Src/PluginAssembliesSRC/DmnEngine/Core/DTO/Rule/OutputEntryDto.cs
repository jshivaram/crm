﻿namespace DmnEngine.Core.DTO.Rule
{
    public class OutputEntryDto
    {
        public string Id { get; set; }

        public string Value { get; set; }

        public string DataType { get; set; }

        public string OutputFullPath { get; set; }
    }
}