﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using DmnEngine.Core.DmnExpression;
using DmnEngine.Core.DTO;
using DmnEngine.Core.DTO.Rule;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;

namespace DmnEngine.Core
{
    internal class DmnDecisionResult
    {
        private readonly DecisionTableDto _decisionTableDto;
        private readonly DmnDto _dmnDto;
        private readonly ConcurrentDictionary<string, object> _sourceValuesCache;

        private readonly Entity _targetEntity;
        private List<OutputEntryDto> _output;

        public DmnDecisionResult(DmnDto dmnDto,
            DecisionTableDto decisionTableDto)
        {
            _decisionTableDto = decisionTableDto;

            _targetEntity = dmnDto.TargetEntity;
            _dmnDto = dmnDto;
            _sourceValuesCache = dmnDto.DataService._sourceValuesCache;
        }

        public int RuleNumber { get; private set; }


        /// <summary>
        /// </summary>
        /// <returns></returns>
        public List<OutputEntryDto> GetResult()
        {
            CheckRules();
            return _output;
        }

        /// <summary>
        /// </summary>
        private void CheckRules()
        {
            foreach (var decisionRules in _decisionTableDto.DecisionRule)
            {
                RuleNumber++;
                if (!IsValidateInputEntries(decisionRules))
                    continue;
                _output = decisionRules.OutputEntries;
            }
        }

        private bool IsValidateInputEntries(DecisionRuleDto decisionRules)
        {
            foreach (var inputEntry in decisionRules.InputEntries)
            {
                var expr = inputEntry.Value;
                if (string.IsNullOrEmpty(expr)) continue;
                var exprOperator = expr.Split('^')[0];
                var exprValue = GetExpressionValue(inputEntry.DataType, expr.Split('^')[1].Split(','));
                var sourceValue = GetSourceValue(inputEntry.Field);
                var key = $"{_targetEntity.Id}^{inputEntry.Field}";
                _sourceValuesCache.TryAdd(key, sourceValue);
                var expMng = new ExpressionManager(exprOperator, inputEntry.DataType, exprValue, sourceValue);
                var isInputEntry = expMng.GetResult();
                if (!isInputEntry)
                    return false;
            }
            return true;
        }

        private string[] GetExpressionValue(string inputType, string[] exprValue)
        {
            var tmpList = new List<string>();
            foreach (var item in exprValue)
            {
                if (item.IndexOf('.') != -1)
                {
                    var val = GetSourceValue(item)?.ToString();
                    tmpList.Add(val);
                    continue;
                }
                tmpList.Add(item);
            }
            return tmpList.ToArray();
        }

        /// <summary>
        ///     key consist of _targetEntity.Id^fullFieldName
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object TryGetValueFromCache(string key)
        {
            object value = null;
            if (!_sourceValuesCache.ContainsKey(key)) return null;
            return _sourceValuesCache.TryGetValue(key, out value) ? value : null;
        }

        public object GetSourceValue(string fullFieldName)
        {
            var key = $"{_targetEntity.Id}^{fullFieldName}";
            var valueFromCache = TryGetValueFromCache(key);
            if (valueFromCache != null)
                return valueFromCache;

            var fieldParts = fullFieldName.Split('.');

            if (fieldParts.First() != _targetEntity.LogicalName)
                return null;

            //remove base entity name from array
            fieldParts = fieldParts.Skip(1).ToArray();

            var attName = fieldParts.FirstOrDefault();

            var baseEntity = _dmnDto.DataService.RetreiveEntity(_targetEntity.LogicalName, _targetEntity.Id);

            //get first field
            object firstFieldValue = null;
            baseEntity.Attributes.TryGetValue(attName, out firstFieldValue);
            if (firstFieldValue == null)
                return null;

            if (firstFieldValue is OptionSetValue)
                return GetOptionSetValue(_dmnDto, attName, firstFieldValue);

            //if field is not lookup
            if (firstFieldValue.GetType() != typeof(EntityReference))
                return firstFieldValue;

            //if field is lookup
            object lookupValue = null;
            baseEntity.Attributes.TryGetValue(attName, out lookupValue);
            var resultValue = GetLookupInnerValue(lookupValue as EntityReference, fieldParts.Skip(1).ToArray());
            return resultValue;
        }

        private string GetOptionSetValue(DmnDto dmnDto, string attName, object fieldValue)
        {
            var attMetadata =
                dmnDto.DataService.GetAttributeMetadata(_targetEntity.LogicalName, attName).FirstOrDefault();
            var optionSetKey = (fieldValue as OptionSetValue).Value;
            return GetOptionSetValueLabel(optionSetKey, attMetadata);
        }

        private static string GetOptionSetValueLabel(int optionSetKey, AttributeMetadata fieldMetadata)
        {
            var attMetadata = (EnumAttributeMetadata) fieldMetadata;

            if (attMetadata == null)
                return optionSetKey.ToString();


            return
                attMetadata.OptionSet.Options.Where(x => x.Value == optionSetKey)
                    .FirstOrDefault()
                    .Label.UserLocalizedLabel.Label;
        }

        private object GetLookupInnerValue(EntityReference lookup, string[] fields)
        {
            if (lookup == null)
                return null;

            object value = null;

            var entity = _dmnDto.DataService.RetreiveEntity(lookup.LogicalName, lookup.Id);
            if (!entity.Attributes.TryGetValue(fields.First(), out value))
                return null;

            if (value is EntityReference)
                value = GetLookupInnerValue(value as EntityReference, fields.Skip(1).ToArray());

            if (value is OptionSetValue)
            {
                var attMetadata =
                    _dmnDto.DataService.GetAttributeMetadata(entity.LogicalName, fields.First()).FirstOrDefault();
                var optionSetKey = (value as OptionSetValue).Value;
                value = GetOptionSetValueLabel(optionSetKey, attMetadata);
            }

            return value;
        }
    }
}