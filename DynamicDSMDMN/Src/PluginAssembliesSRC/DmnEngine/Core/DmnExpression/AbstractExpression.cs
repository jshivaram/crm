﻿namespace DmnEngine.Core.DmnExpression
{
    public abstract class AbstractExpression
    {
        private int Infinity { get; } = -1;

        protected abstract void InitOperator();

        public abstract bool GetResult();


        /// <summary>
        ///     Returns the exact number of parameters needed
        ///     which is set as infinite by default.
        /// </summary>
        /// <returns></returns>
        protected virtual int GetParamCount()
        {
            return Infinity;
        }
    }
}