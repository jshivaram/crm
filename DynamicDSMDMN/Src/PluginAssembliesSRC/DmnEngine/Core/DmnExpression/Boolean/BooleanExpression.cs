﻿namespace DmnEngine.Core.DmnExpression.Boolean
{
    internal class BooleanExpression : AbstractExpression
    {
        private BooleanExpression _booleanExpression;
        protected bool[] _exprValue;
        protected string _operator;
        protected bool _sourceValue;

        public BooleanExpression(string op, bool sourceValue, bool[] exprValue)
        {
            _sourceValue = sourceValue;
            _exprValue = exprValue;
            _operator = op;
        }

        public override bool GetResult()
        {
            InitOperator();
            return Evaluate();
        }

        protected override void InitOperator()
        {
            switch (_operator)
            {
                case "$true":
                    _booleanExpression = new IsTrueFalse(_operator, _sourceValue, new[] {true});
                    break;
                case "$false":
                    _booleanExpression = new IsTrueFalse(_operator, _sourceValue, new[] {false});
                    break;
            }
        }

        protected virtual bool Evaluate()
        {
            if (_booleanExpression == null)
                return false;
            _booleanExpression.ValidateOperator();
            _booleanExpression.ValidateParameters();
            return _booleanExpression.Evaluate();
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        /// <returns></returns>
        protected virtual void ValidateParameters()
        {
        }


        protected virtual void ValidateOperator()
        {
        }
    }
}