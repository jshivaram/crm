﻿using System;

namespace DmnEngine.Core.DmnExpression.Date
{
    internal class DateExpression : AbstractExpression
    {
        private DateExpression _dateExpression;
        protected string[] _exprValue;
        protected string _operator;
        protected DateTime _sourceValue;

        public DateExpression(string op, DateTime sourceValue, string[] exprValue)
        {
            _sourceValue = sourceValue;
            _exprValue = exprValue;
            _operator = op;
        }

        public override bool GetResult()
        {
            InitOperator();
            return Evaluate();
        }

        protected override void InitOperator()
        {
            switch (_operator)
            {
                case "$equals":
                    _dateExpression = new Equal(_operator, _sourceValue, _exprValue);
                    break;
                case "$not_equals":
                    _dateExpression = new NotEqual(_operator, _sourceValue, _exprValue);
                    break;
                case "$gt":
                    _dateExpression = new GreaterThan(_operator, _sourceValue, _exprValue);
                    break;
                case "$gte":
                    _dateExpression = new GreaterThanOrEqual(_operator, _sourceValue, _exprValue);
                    break;
                case "$lt":
                    _dateExpression = new LessThan(_operator, _sourceValue, _exprValue);
                    break;
                case "$lte":
                    _dateExpression = new LessThan(_operator, _sourceValue, _exprValue);
                    break;

                case "$between":
                    _dateExpression = new Between(_operator, _sourceValue, _exprValue);
                    break;
                case "$in":
                    _dateExpression = new In(_operator, _sourceValue, _exprValue);
                    break;
            }
        }

        protected virtual bool Evaluate()
        {
            if (_dateExpression == null)
                return false;
            _dateExpression.ValidateOperator();
            _dateExpression.ValidateParameters();
            return _dateExpression.Evaluate();
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        /// <returns></returns>
        protected virtual void ValidateParameters()
        {
        }


        protected virtual void ValidateOperator()
        {
        }
    }
}