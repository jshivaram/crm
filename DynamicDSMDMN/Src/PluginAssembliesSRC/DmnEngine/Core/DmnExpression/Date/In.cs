﻿using System;
using System.Linq;

namespace DmnEngine.Core.DmnExpression.Date
{
    internal class In : DateExpression
    {
        public In(string op, DateTime sourceValue, string[] exprValue)
            : base(op, sourceValue, exprValue)
        {
        }

        protected override bool Evaluate()
        {
            var datesList = _exprValue.Select(d => DateTime.Parse(d)).ToList();
            var result = datesList.Contains(_sourceValue);
            return result;
        }
    }
}