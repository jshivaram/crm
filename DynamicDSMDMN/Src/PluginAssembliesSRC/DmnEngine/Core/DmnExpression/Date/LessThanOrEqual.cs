﻿using System;
using System.Linq.Expressions;

namespace DmnEngine.Core.DmnExpression.Date
{
    internal class LessThanOrEqual : DateExpression
    {
        public LessThanOrEqual(string op, DateTime sourceValue, string[] exprValue) : base(op, sourceValue, exprValue)
        {
        }

        /// <summary>
        ///     Returns itself when evaluating.
        /// </summary>
        /// <returns></returns>
        protected override bool Evaluate()
        {
            var sourceValue = _sourceValue;
            var exprValue = _exprValue[0];
            Expression lessThanOrEqual = Expression.LessThanOrEqual(
                Expression.Constant(sourceValue),
                Expression.Constant(exprValue)
            );
            return Expression.Lambda<Func<bool>>(lessThanOrEqual).Compile()();
        }

        protected override int GetParamCount()
        {
            return 1;
        }


        protected override void ValidateOperator()
        {
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        protected override void ValidateParameters()
        {
        }
    }
}