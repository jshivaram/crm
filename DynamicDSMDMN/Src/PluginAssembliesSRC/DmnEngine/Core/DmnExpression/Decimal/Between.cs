﻿using System;
using System.Linq.Expressions;
using Microsoft.Xrm.Sdk;

namespace DmnEngine.Core.DmnExpression.Decimal
{
    internal class Between : DecimalExpression
    {
        public Between(string op, decimal sourceValue, decimal[] exprValue) : base(op, sourceValue, exprValue)
        {
        }

        /// <summary>
        ///     Returns itself when evaluating.
        /// </summary>
        /// <returns></returns>
        protected override bool Evaluate()
        {
            var sourceValue = _sourceValue;
            return GreaterThan(sourceValue, _exprValue[0]) && LessThan(sourceValue, _exprValue[1]);
        }

        private bool GreaterThan(decimal input1, decimal input2)
        {
            var sourceValue = _sourceValue;
            var exprValue = _exprValue[0];
            Expression greaterThan = Expression.GreaterThan(
                Expression.Constant(input1),
                Expression.Constant(input2)
            );
            return Expression.Lambda<Func<bool>>(greaterThan).Compile()();
        }

        private bool LessThan(decimal input1, decimal input2)
        {
            Expression lessThanExpr = Expression.LessThan(
                Expression.Constant(input1),
                Expression.Constant(input2)
            );
            return Expression.Lambda<Func<bool>>(lessThanExpr).Compile()();
        }

        protected override int GetParamCount()
        {
            return 2;
        }


        protected override void ValidateOperator()
        {
            try
            {
                if (_operator == string.Empty)
                    throw new InvalidPluginExecutionException("An error occurred in workflow. Invalid operator");
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("An error occurred in workflow", ex);
            }
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        protected override void ValidateParameters()
        {
            try
            {
                foreach (var item in _exprValue)
                    if (item == int.MinValue)
                        throw new InvalidPluginExecutionException("An error occurred in workflow. Invalid arguments");
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("An error occurred in workflow", ex);
            }
        }
    }
}