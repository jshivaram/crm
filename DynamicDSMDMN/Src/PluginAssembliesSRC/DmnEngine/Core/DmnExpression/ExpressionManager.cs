﻿using System;
using DmnEngine.Core.DmnExpression.Boolean;
using DmnEngine.Core.DmnExpression.Date;
using DmnEngine.Core.DmnExpression.Decimal;
using DmnEngine.Core.DmnExpression.Integer;
using DmnEngine.Core.DmnExpression.Memo;
using DmnEngine.Core.DmnExpression.Money;
using DmnEngine.Core.DmnExpression.Picklist;
using DmnEngine.Core.DmnExpression.String;
using Microsoft.Xrm.Sdk;

namespace DmnEngine.Core.DmnExpression
{
    internal class ExpressionManager : IExpressionManager
    {
        private readonly string[] _args;
        private readonly string _dataType;
        private readonly string _operator;
        private readonly object _sourceValue;


        public ExpressionManager(string op, string dataType, string[] args, object sourceValue)
        {
            _dataType = dataType;
            _operator = op;
            _sourceValue = sourceValue;
            _args = args;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public bool GetResult()
        {
            if (_operator == "$not_empty")
            {
                var result = _sourceValue == null || string.IsNullOrEmpty(_sourceValue.ToString());
                return !result;
            }
            if (_operator == "$empty")
                return IsEmpty(_sourceValue, _dataType.ToLower());
            if (_operator == "$isnull")
                return _sourceValue == null || IsNull(_sourceValue, _dataType.ToLower());
            switch (_dataType.ToLower())
            {
                case "integer":
                case "int32":
                    var args = Array.ConvertAll(_args, int.Parse);
                    var exprInt = new IntegerExpression(_operator, Convert.ToInt32(_sourceValue), args);
                    return exprInt.GetResult();
                case "decimal":
                case "double":
                    var argsDecimal = Array.ConvertAll(_args, decimal.Parse);
                    var exprDecimal = new DecimalExpression(_operator, Convert.ToDecimal(_sourceValue), argsDecimal);
                    return exprDecimal.GetResult();
                case "string":
                    if (_sourceValue == null)
                        return false;
                    var exprString = new StringExpression(_operator, _sourceValue.ToString(), _args);
                    return exprString.GetResult();
                case "datetime":
                    var exprDateTime = new DateExpression(_operator, Convert.ToDateTime(_sourceValue), _args);
                    return exprDateTime.GetResult();
                case "boolean":
                    var exprBool = new BooleanExpression(_operator, Convert.ToBoolean(_sourceValue), new bool[] {});
                    return exprBool.GetResult();
                case "money":
                    if (_sourceValue == null) return false;
                    var argsMoney = Array.ConvertAll(_args, decimal.Parse);
                    var exprMoney = new MoneyExpression(_operator, (Microsoft.Xrm.Sdk.Money) _sourceValue, argsMoney);
                    return exprMoney.GetResult();
                case "guid":
                    var exprGuid = new StringExpression(_operator, _sourceValue.ToString(), _args);
                    return exprGuid.GetResult();
                case "optionsetvalue":
                    var exprOS = new StringExpression(_operator, ((OptionSetValue) _sourceValue).Value.ToString(), _args);
                    return exprOS.GetResult();
                case "picklist":
                    if (_sourceValue == null)
                        return false;
                    var exprPl = new PicklistExpression(_operator, _sourceValue.ToString(), _args);
                    return exprPl.GetResult();
                case "memo":
                    var exprMemo = new MemoExpression(_operator, _sourceValue.ToString(), _args);
                    return exprMemo.GetResult();
            }
            return false;
        }

        private bool IsEmpty(object value, string dataType)
        {
            switch (dataType)
            {
                case "string":
                    return value == null || string.IsNullOrEmpty(value.ToString());
                case "guid":
                    return value == null || string.IsNullOrEmpty(value.ToString());
            }
            return false;
        }

        private bool IsNull(object value, string dataType)
        {
            switch (dataType)
            {
                case "integer":
                case "int32":
                    return value == null || Convert.ToInt32(value).Equals(null);
                case "decimal":
                case "double":
                    return value == null || Convert.ToDecimal(value).Equals(null);

                case "datetime":
                    return value == null || Convert.ToDateTime(value).Equals(null);
                case "boolean":
                    return value == null || Convert.ToBoolean(value).Equals(null);
                case "money":
                    var val = (Microsoft.Xrm.Sdk.Money) value;
                    return value == null ||
                           new Microsoft.Xrm.Sdk.Money(val.Value).Equals(default(Microsoft.Xrm.Sdk.Money));
            }
            return false;
        }
    }
}