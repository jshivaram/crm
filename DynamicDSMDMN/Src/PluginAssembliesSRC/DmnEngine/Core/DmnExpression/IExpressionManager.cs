﻿namespace DmnEngine.Core.DmnExpression
{
    internal interface IExpressionManager
    {
        bool GetResult();
    }
}