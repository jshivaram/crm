﻿using System;
using System.Linq.Expressions;
using Microsoft.Xrm.Sdk;

namespace DmnEngine.Core.DmnExpression.Integer
{
    internal class GreaterThanOrEqual : IntegerExpression
    {
        public GreaterThanOrEqual(string op, int sourceValue, int[] exprValue) : base(op, sourceValue, exprValue)
        {
        }

        /// <summary>
        ///     Returns itself when evaluating.
        /// </summary>
        /// <returns></returns>
        protected override bool Evaluate()
        {
            var sourceValue = _sourceValue;
            var exprValue = _exprValue[0];
            Expression greaterThanOrEqual = Expression
                .GreaterThanOrEqual(
                    Expression.Constant(sourceValue),
                    Expression.Constant(exprValue)
                );
            return Expression.Lambda<Func<bool>>(greaterThanOrEqual).Compile()();
        }

        protected override int GetParamCount()
        {
            return 1;
        }


        protected override void ValidateOperator()
        {
            try
            {
                if (_operator == string.Empty)
                    throw new InvalidPluginExecutionException("An error occurred in workflow. Invalid operator");
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("An error occurred in workflow", ex);
            }
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        protected override void ValidateParameters()
        {
        }
    }
}