﻿using System;
using Microsoft.Xrm.Sdk;

namespace DmnEngine.Core.DmnExpression.Integer
{
    internal class In : IntegerExpression
    {
        public In(string op, int sourceValue, int[] exprValue) : base(op, sourceValue, exprValue)
        {
        }

        /// <summary>
        ///     Returns itself when evaluating.
        /// </summary>
        /// <returns></returns>
        protected override bool Evaluate()
        {
            var sourceValue = _sourceValue;
            var exprValue = _exprValue;
            return Array.FindIndex(exprValue, x => x == sourceValue) != -1;
        }

        protected override int GetParamCount()
        {
            return 1000;
        }


        protected override void ValidateOperator()
        {
            try
            {
                if (_operator == string.Empty)
                    throw new InvalidPluginExecutionException("An error occurred in workflow. Invalid operator");
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("An error occurred in workflow", ex);
            }
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        protected override void ValidateParameters()
        {
            try
            {
                foreach (var item in _exprValue)
                    if (item == int.MinValue)
                        throw new InvalidPluginExecutionException("An error occurred in workflow. Invalid arguments");
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("An error occurred in workflow", ex);
            }
        }
    }
}