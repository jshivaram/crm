﻿namespace DmnEngine.Core.DmnExpression.Memo
{
    internal class MemoExpression : AbstractExpression
    {
        protected string[] _exprValue;

        private MemoExpression _memoExpression;
        protected string _operator;
        protected string _sourceValue;

        public MemoExpression(string op, string sourceValue, string[] exprValue)
        {
            _sourceValue = sourceValue;
            _exprValue = exprValue;
            _operator = op;
        }

        public override bool GetResult()
        {
            InitOperator();
            return Evaluate();
        }

        protected override void InitOperator()
        {
            switch (_operator)
            {
                case "$equals":
                    _memoExpression = new Equal(_operator, _sourceValue, _exprValue);
                    break;
                case "$not_equals":
                    _memoExpression = new NotEqual(_operator, _sourceValue, _exprValue);
                    break;
                case "$contains":
                    _memoExpression = new Contains(_operator, _sourceValue, _exprValue);
                    break;
                case "$not_contains":
                    _memoExpression = new NotContains(_operator, _sourceValue, _exprValue);
                    break;
            }
        }

        protected virtual bool Evaluate()
        {
            if (_memoExpression == null)
                return false;
            _memoExpression.ValidateOperator();
            _memoExpression.ValidateParameters();
            return _memoExpression.Evaluate();
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        /// <returns></returns>
        protected virtual void ValidateParameters()
        {
        }


        protected virtual void ValidateOperator()
        {
        }
    }
}