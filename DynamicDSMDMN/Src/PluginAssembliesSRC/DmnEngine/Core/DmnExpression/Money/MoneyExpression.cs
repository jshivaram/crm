﻿namespace DmnEngine.Core.DmnExpression.Money
{
    internal class MoneyExpression : AbstractExpression
    {
        protected decimal[] _exprValue;

        private MoneyExpression _moneyExpression;
        protected string _operator;
        protected Microsoft.Xrm.Sdk.Money _sourceValue;

        public MoneyExpression(string op, Microsoft.Xrm.Sdk.Money sourceValue, decimal[] exprValue)
        {
            _sourceValue = sourceValue;
            _exprValue = exprValue;
            _operator = op;
        }

        public override bool GetResult()
        {
            InitOperator();
            return Evaluate();
        }

        protected override void InitOperator()
        {
            switch (_operator)
            {
                case "$equals":
                    _moneyExpression = new Equal(_operator, _sourceValue, _exprValue);
                    break;
                case "$not_equals":
                    _moneyExpression = new NotEqual(_operator, _sourceValue, _exprValue);
                    break;
                case "$gt":
                    _moneyExpression = new GreaterThan(_operator, _sourceValue, _exprValue);
                    break;
                case "$gte":
                    _moneyExpression = new GreaterThanOrEqual(_operator, _sourceValue, _exprValue);
                    break;
                case "$lt":
                    _moneyExpression = new LessThan(_operator, _sourceValue, _exprValue);
                    break;
                case "$lte":
                    _moneyExpression = new LessThan(_operator, _sourceValue, _exprValue);
                    break;
                case "$in":
                    _moneyExpression = new In(_operator, _sourceValue, _exprValue);
                    break;
                case "$between":
                    _moneyExpression = new Between(_operator, _sourceValue, _exprValue);
                    break;
            }
        }

        protected virtual bool Evaluate()
        {
            if (_moneyExpression == null)
                return false;
            _moneyExpression.ValidateOperator();
            _moneyExpression.ValidateParameters();
            return _moneyExpression.Evaluate();
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        /// <returns></returns>
        protected virtual void ValidateParameters()
        {
        }


        protected virtual void ValidateOperator()
        {
        }
    }
}