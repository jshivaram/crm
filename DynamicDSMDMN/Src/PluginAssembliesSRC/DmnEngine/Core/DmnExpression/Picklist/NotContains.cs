﻿using System;
using System.Linq.Expressions;

namespace DmnEngine.Core.DmnExpression.Picklist
{
    internal class NotContains : PicklistExpression
    {
        public NotContains(string op, string sourceValue, string[] exprValue) : base(op, sourceValue, exprValue)
        {
        }

        protected override bool Evaluate()
        {
            var parameterExp = Expression.Parameter(typeof(string), "str");
            var method = typeof(string).GetMethod("Contains", new[] {typeof(string)});
            var subStr = Expression.Constant(_exprValue[0], typeof(string));
            var containsMethodExp = Expression.Call(parameterExp, method, subStr);

            var lambdaExpression = Expression.Lambda(containsMethodExp, parameterExp);
            var newLambda = (Func<string, bool>) lambdaExpression.Compile();
            return !newLambda(_sourceValue);
        }
    }
}