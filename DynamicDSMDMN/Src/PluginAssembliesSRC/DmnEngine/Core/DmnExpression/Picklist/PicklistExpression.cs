﻿namespace DmnEngine.Core.DmnExpression.Picklist
{
    internal class PicklistExpression : AbstractExpression
    {
        protected string[] _exprValue;
        protected string _operator;

        private PicklistExpression _picklistExpression;
        protected string _sourceValue;

        public PicklistExpression(string op, string sourceValue, string[] exprValue)
        {
            _sourceValue = sourceValue;
            _exprValue = exprValue;
            _operator = op;
        }

        public override bool GetResult()
        {
            InitOperator();
            return Evaluate();
        }

        protected override void InitOperator()
        {
            switch (_operator)
            {
                case "$equals":
                    _picklistExpression = new Equal(_operator, _sourceValue, _exprValue);
                    break;
                case "$not_equals":
                    _picklistExpression = new NotEqual(_operator, _sourceValue, _exprValue);
                    break;
                case "$contains":
                    _picklistExpression = new Contains(_operator, _sourceValue, _exprValue);
                    break;
                case "$not_contains":
                    _picklistExpression = new NotContains(_operator, _sourceValue, _exprValue);
                    break;
                case "$in":
                    _picklistExpression = new In(_operator, _sourceValue, _exprValue);
                    break;
            }
        }

        protected virtual bool Evaluate()
        {
            if (_picklistExpression == null)
                return false;
            _picklistExpression.ValidateOperator();
            _picklistExpression.ValidateParameters();
            return _picklistExpression.Evaluate();
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        /// <returns></returns>
        protected virtual void ValidateParameters()
        {
        }


        protected virtual void ValidateOperator()
        {
        }
    }
}