﻿using System.Linq;

namespace DmnEngine.Core.DmnExpression.String
{
    internal class In : StringExpression
    {
        public In(string op, string sourceValue, string[] exprValue)
            : base(op, sourceValue, exprValue)
        {
        }

        protected override bool Evaluate()
        {
            var result = _exprValue.Contains(_sourceValue);
            return result;
        }
    }
}