﻿namespace DmnEngine.Core.DmnExpression.String
{
    internal class StringExpression : AbstractExpression
    {
        protected string[] _exprValue;
        protected string _operator;
        protected string _sourceValue;

        private StringExpression _stringExpression;

        public StringExpression(string op, string sourceValue, string[] exprValue)
        {
            _sourceValue = sourceValue;
            _exprValue = exprValue;
            _operator = op;
        }

        public override bool GetResult()
        {
            InitOperator();
            return Evaluate();
        }

        protected override void InitOperator()
        {
            switch (_operator)
            {
                case "$equals":
                    _stringExpression = new Equal(_operator, _sourceValue, _exprValue);
                    break;
                case "$not_equals":
                    _stringExpression = new NotEqual(_operator, _sourceValue, _exprValue);
                    break;
                case "$contains":
                    _stringExpression = new Contains(_operator, _sourceValue, _exprValue);
                    break;
                case "$not_contains":
                    _stringExpression = new NotContains(_operator, _sourceValue, _exprValue);
                    break;
                case "$in":
                    _stringExpression = new In(_operator, _sourceValue, _exprValue);
                    break;
            }
        }

        protected virtual bool Evaluate()
        {
            if (_stringExpression == null)
                return false;
            _stringExpression.ValidateOperator();
            _stringExpression.ValidateParameters();
            return _stringExpression.Evaluate();
        }

        /// <summary>
        ///     Validates the parameters and throws an Exception if invalid.
        /// </summary>
        /// <returns></returns>
        protected virtual void ValidateParameters()
        {
        }


        protected virtual void ValidateOperator()
        {
        }
    }
}