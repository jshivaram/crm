﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DmnEngine.Core.DmnQueryBuilder
{
    public class FetchXMLCondition
    {
        public FetchXMLCondition()
        {
            Values = new List<string>();
        }

        [XmlAttribute("attribute")]
        public string Attribute { get; set; }

        [XmlAttribute("operator")]
        public string Op { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }

        [XmlAttribute("value")]
        public List<string> Values { get; set; }
    }
}