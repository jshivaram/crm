﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DmnEngine.Core.DmnQueryBuilder
{
    public class MainEntity
    {
        public List<LinkEntity> FetchLinkEntity;

        public MainEntity(string name)
        {
            Name = name;
            LinkEntity = new Dictionary<string, LinkEntity>();
            FetchLinkEntity = new List<LinkEntity>();
            Filters = new List<Filter>();
            Attributes = new List<FetchXMLAttribute>();
        }

        public MainEntity()
        {
            LinkEntity = new Dictionary<string, LinkEntity>();
            FetchLinkEntity = new List<LinkEntity>();
            Filters = new List<Filter>();
            Attributes = new List<FetchXMLAttribute>();
        }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "attribute")]
        public List<FetchXMLAttribute> Attributes { get; }

        public List<Filter> Filters { get; set; }

        [XmlIgnore]
        public Dictionary<string, LinkEntity> LinkEntity { get; set; }

        //public List<KeyValuePair<string, LinkEntity>> LinkEntity { get; set; }
    }
}