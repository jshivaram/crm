﻿using System.Xml.Serialization;

namespace DmnEngine.Core.DmnQueryBuilder
{
    [XmlRoot("fetch")]
    public class MainFetch
    {
        [XmlElement(ElementName = "entity")] public MainEntity Entity;

        public MainFetch()
        {
            Mapping = "logical";
            Aggregate = true;
        }

        [XmlAttribute("mapping")]
        public string Mapping { get; set; }

        [XmlAttribute("aggregate")]
        public bool Aggregate { get; set; }
    }
}