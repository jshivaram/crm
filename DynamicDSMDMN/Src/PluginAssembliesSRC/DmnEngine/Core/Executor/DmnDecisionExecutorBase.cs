﻿using System.Collections.Generic;
using System.Linq;
using DmnEngine.Core.Executor.Function;
using DmnEngine.Core.Interfaces;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;

namespace DmnEngine.Core.Executor
{
    public class DmnDecisionExecutorBase : IDmnDecisionExecutorBase
    {
        private readonly DmnDto _dmnDto;


        private IExecutor _executor;

        /// <summary>
        ///     ctor
        /// </summary>
        /// <param name="targetFullFieldName"></param>
        public DmnDecisionExecutorBase(DmnDto dmnDto, string dataType, string outputFullPath)
        {
            var targetEntity = dmnDto.TargetEntity;
            _dmnDto = dmnDto;

            _targetEntity = dmnDto.DataService.RetreiveEntity(targetEntity.LogicalName, targetEntity.Id);

            InitOutput(outputFullPath);
            switch (dataType.ToLower())
            {
                case "integer":
                    _executor = new Function.Integer.Executor();
                    break;
                case "decimal":
                    _executor = new Function.Decimal.Executor();
                    break;
                case "string":
                    _executor = new Function.String.Executor();
                    break;
                case "money":
                    _executor = new Function.Money.Executor();
                    break;
                case "datetime":
                    _executor = new Function.DateTime.Executor();
                    break;
                case "picklist":
                    _executor = new Function.Picklist.Executor();
                    break;
                case "memo":
                    _executor = new Function.Memo.Executor();
                    break;
            }
        }

        /// <summary>
        /// </summary>
        public Entity _outputEntity { get; set; }

        /// <summary>
        /// </summary>
        public string _outputField { get; set; }

        /// <summary>
        /// </summary>
        //private  Entity _targetEntity { get; set; }
        private Entity _targetEntity { get; }


        //private static IOrganizationService _orgService { get; set; }

        public string Run(string expression)
        {
            var expFunction = GetExprFunction(expression);
            var expArgs = GetExprArgs(expression);
            if (expFunction == string.Empty) return expArgs;
            var arg = ConvertArgsToArray(expArgs);

            return Run(expFunction, arg);
        }

        /// <summary>
        /// </summary>
        /// <param name="function"></param>
        /// <param name="args"></param>
        public string Run(string function, string[] args)
        {
            //if we are working with relationships
            CheckRelationFunction(function);
            var dmnFunction = new DmnFunction(_dmnDto);
            var rs = _executor.PerformOperation(function, args, dmnFunction);
            return rs;
        }

        private void CheckRelationFunction(string function)
        {
            var relFunctions = new List<string>
            {
                "count",
                "rel_max",
                "rel_min",
                "rel_avg",
                "sum"
            };
            if (!relFunctions.Contains(function.ToLower()))
                return;
            _executor = new Function.Relationship.Executor();
        }

        private void InitOutput(string outputFullPath)
        {
            //the path is empty
            if (string.Empty == outputFullPath)
            {
                _outputField = string.Empty;
                _outputEntity = null;
                return;
            }
            //only field
            if (outputFullPath.IndexOf('.') == -1)
            {
                _outputField = outputFullPath;
            }
            else
            {
                var pathArr = outputFullPath.Split('.').ToArray();
                _outputField = pathArr.LastOrDefault();
                GetRelatedEntity(_dmnDto, _targetEntity, pathArr.Skip(1).ToArray());
            }
        }

        private void GetRelatedEntity(DmnDto dmnDto, Entity targetEntity, string[] entityPath, int entityIndex = 0
        )
        {
            if (entityPath.Length <= entityIndex + 1)
            {
                _outputEntity = targetEntity;
                return;
            }
            var link = entityPath[entityIndex];
            entityIndex++;
            object lookUp = null;
            if (!targetEntity.Attributes.TryGetValue(link, out lookUp))
                GetRelatedEntity(dmnDto, targetEntity, entityPath, entityIndex);
            var relatedEntity = (EntityReference) lookUp;
            var output = _dmnDto.DataService.RetreiveEntity(relatedEntity.LogicalName, relatedEntity.Id);

            GetRelatedEntity(dmnDto, output, entityPath, entityIndex);
        }


        /// <summary>
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private string[] ConvertArgsToArray(string args)
        {
            return args.IndexOf(',') != -1 ? args.Split(',') : new[] {args};
        }

        /// <summary>
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private string GetExprFunction(string expression)
        {
            return expression.IndexOf('^') != -1 ? expression.Split('^')[0] : string.Empty;
        }

        /// <summary>
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private string GetExprArgs(string expression)
        {
            return expression.IndexOf('^') != -1 ? expression.Split('^')[1] : GetSourceValue(expression);
        }


        private string GetSourceValue(string fullFieldName)
        {
            var fieldParts = fullFieldName.Split('.');

            if (fieldParts.First() != _targetEntity.LogicalName)
                return null;

            //remove base entity name from array
            fieldParts = fieldParts.Skip(1).ToArray();

            var attName = fieldParts.FirstOrDefault();

            var baseEntity = _dmnDto.DataService.RetreiveEntity(_targetEntity.LogicalName, _targetEntity.Id);

            //get first field
            object firstFieldValue = null;

            var attMetadata = GetAttributeMetadata(_dmnDto, baseEntity.LogicalName, attName).FirstOrDefault();

            baseEntity.Attributes.TryGetValue(attName, out firstFieldValue);
            if (firstFieldValue == null)
                return GetEmptyResultByType(attMetadata);

            if (firstFieldValue is OptionSetValue)
                return GetOptionSetValue(_dmnDto, attName, firstFieldValue);


            if (firstFieldValue.GetType() == typeof(Money))
                return (firstFieldValue as Money).Value.ToString();


            //if field is not lookup
            if (firstFieldValue.GetType() != typeof(EntityReference))
                return firstFieldValue.ToString();

            //if field is lookup
            object lookupValue = null;
            baseEntity.Attributes.TryGetValue(attName, out lookupValue);
            var resultValue = GetLookupInnerValue(lookupValue as EntityReference, fieldParts.Skip(1).ToArray());
            return GetRS(resultValue);
        }

        private string GetRS(object value)
        {
            if (value == null)
                return "0";
            if (value.GetType() == typeof(Money))
                return (value as Money).Value.ToString();
            return value.ToString();
        }

        private string GetEmptyResultByType(AttributeMetadata attrType)
        {
            var defaultNumericRs = "0";
            switch (attrType.AttributeType)
            {
                case AttributeTypeCode.Integer:
                    return defaultNumericRs;
                case AttributeTypeCode.Decimal:
                    return defaultNumericRs;
                case AttributeTypeCode.Double:
                    return defaultNumericRs;
                case AttributeTypeCode.Money:
                    return defaultNumericRs;
                case AttributeTypeCode.String:
                    return string.Empty;
                case AttributeTypeCode.Boolean:
                    return "false";
                case AttributeTypeCode.DateTime:
                    return string.Empty;
                case AttributeTypeCode.Picklist:
                    return string.Empty;
                case AttributeTypeCode.Memo:
                    return string.Empty;
            }
            return string.Empty;
        }

        private string GetOptionSetValue(DmnDto dmtDto, string attName, object fieldValue)
        {
            var attMetadata = GetAttributeMetadata(dmtDto, _targetEntity.LogicalName, attName).FirstOrDefault();
            var optionSetKey = (fieldValue as OptionSetValue).Value;
            return GetOptionSetValueLabel(optionSetKey, attMetadata);
        }

        public IEnumerable<AttributeMetadata> GetAttributeMetadata(DmnDto dmtDto, string entityName, string attName)
        {
            return dmtDto.DataService.GetAttributeMetadata(entityName, attName);
        }

        private static string GetOptionSetValueLabel(int optionSetKey, AttributeMetadata fieldMetadata)
        {
            var attMetadata = (EnumAttributeMetadata) fieldMetadata;

            if (attMetadata == null)
                return optionSetKey.ToString();


            return
                attMetadata.OptionSet.Options.Where(x => x.Value == optionSetKey)
                    .FirstOrDefault()
                    .Label.UserLocalizedLabel.Label;
        }

        private object GetLookupInnerValue(EntityReference lookup, string[] fields)
        {
            if (lookup == null)
                return null;

            object value = null;

            var entity = _dmnDto.DataService.RetreiveEntity(lookup.LogicalName, lookup.Id);
            if (!entity.Attributes.TryGetValue(fields.First(), out value))
                return null;

            if (value is EntityReference)
                value = GetLookupInnerValue(value as EntityReference, fields.Skip(1).ToArray());

            return value;
        }
    }
}