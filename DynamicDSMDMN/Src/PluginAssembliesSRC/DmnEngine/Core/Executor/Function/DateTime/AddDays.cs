﻿using System;
using System.Collections.Generic;

namespace DmnEngine.Core.Executor.Function.DateTime
{
    internal class AddDays : DmnFunction
    {
        public AddDays(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            args = GetValue(_dmnFunction, args);
            var date = System.DateTime.Parse(args[0]);
            var days = Convert.ToDouble(args[1]);
            date = date.AddDays(days);
            return date.ToString("MM/dd/yyyy");
        }

        private string[] GetValue(DmnFunction _dmnFunction, string[] args)
        {
            var tmpList = new List<string>();
            foreach (var item in args)
            {
                if (item.IndexOf('.') != -1)
                {
                    var val = _dmnFunction.GetSourceValue(item)?.ToString() ?? item;
                    tmpList.Add(val);
                    continue;
                }
                if (item.ToLower() == "now")
                {
                    var now = System.DateTime.Now;
                    var nowStr = now.ToString("MM/dd/yyyy");
                    tmpList.Add(nowStr);
                    continue;
                }
                tmpList.Add(item);
            }
            return tmpList.ToArray();
        }
    }
}