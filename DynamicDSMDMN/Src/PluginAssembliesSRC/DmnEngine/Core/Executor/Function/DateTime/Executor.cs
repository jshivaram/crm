﻿using System;

namespace DmnEngine.Core.Executor.Function.DateTime
{
    internal class Executor : IExecutor
    {
        public string PerformOperation(string op, string[] args, DmnFunction dmnFunction)
        {
            return Execute(op, args, dmnFunction);
        }


        private string Execute(string op, string[] args, DmnFunction dmnFunction)
        {
            try
            {
                switch (op)
                {
                    case "now":
                        var now = new Now(dmnFunction._dmnDto);
                        return now.Evaluate(args);
                    case "addDays":
                        var addDays = new AddDays(dmnFunction._dmnDto);
                        return addDays.Evaluate(args);
                    case "addMonths":
                        var addMonths = new AddMonths(dmnFunction._dmnDto);
                        return addMonths.Evaluate(args);
                    case "addYears":
                        var addYears = new AddYears(dmnFunction._dmnDto);
                        return addYears.Evaluate(args);
                }
            }

            catch (Exception e)
            {
                dmnFunction._dmnDto.Tracer.Trace(e.Message);
            }
            return string.Empty;
        }
    }
}