﻿namespace DmnEngine.Core.Executor.Function.DateTime
{
    internal class Now : DmnFunction
    {
        public Now(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            var now = System.DateTime.Now;
            return now.ToString("MM/dd/yyyy");
        }
    }
}