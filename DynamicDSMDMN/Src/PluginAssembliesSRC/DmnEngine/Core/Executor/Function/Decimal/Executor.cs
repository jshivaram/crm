﻿using System;

namespace DmnEngine.Core.Executor.Function.Decimal
{
    internal class Executor : IExecutor
    {
        public string PerformOperation(string op, string[] args, DmnFunction dmnFunction)
        {
            return Execute(op, args, dmnFunction);
        }


        private string Execute(string op, string[] args, DmnFunction dmnFunction)
        {
            try
            {
                switch (op)
                {
                    case "addition":
                        var sum = new Sum(dmnFunction._dmnDto);
                        return sum.Evaluate(args);
                    case "subtraction":
                        var subtraction = new Subtraction(dmnFunction._dmnDto);
                        return subtraction.Evaluate(args);
                    case "multiply":
                        var multiply = new Multiply(dmnFunction._dmnDto);
                        return multiply.Evaluate(args);
                    case "divide":
                        var divide = new Divide(dmnFunction._dmnDto);
                        return divide.Evaluate(args);
                    case "subtractionDates":
                        var subtractionDates = new SubtractionDates(dmnFunction._dmnDto);
                        return subtractionDates.Evaluate(args);
                }
            }

            catch (Exception e)
            {
                dmnFunction._dmnDto.Tracer.Trace(e.Message);
            }
            return string.Empty;
        }
    }
}