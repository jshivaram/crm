﻿using System.Linq;

namespace DmnEngine.Core.Executor.Function.Decimal
{
    internal class Multiply : DmnFunction
    {
        public Multiply(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            args = _dmnFunction.GetExpressionValue(args);
            decimal res = 1;

            for (var i = 0; i < args.Count(); i++)
                res *= decimal.Parse(args[i]);
            return res.ToString();
        }
    }
}