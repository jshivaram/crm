﻿using System.Collections.Generic;

namespace DmnEngine.Core.Executor.Function.Decimal
{
    internal class SubtractionDates : DmnFunction
    {
        public SubtractionDates(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            args = GetValue(_dmnFunction, args);


            //NOTE!!!!
            var x = new System.DateTime();
            var y = new System.DateTime();

            switch (args.Length)
            {
                case 1:
                    x = System.DateTime.Parse(args[0]);
                    break;
                case 2:
                    x = System.DateTime.Parse(args[0]);
                    y = System.DateTime.Parse(args[1]);
                    break;
            }
            if (y != System.DateTime.MinValue)
            {
                var b = x.Subtract(y).Days;
                return b.ToString();
            }
            return string.Empty;
        }

        private string[] GetValue(DmnFunction _dmnFunction, string[] args)
        {
            var tmpList = new List<string>();
            foreach (var item in args)
            {
                if (item.IndexOf('.') != -1)
                {
                    var val = _dmnFunction.GetSourceValue(item)?.ToString() ?? item;
                    tmpList.Add(val);
                    continue;
                }
                if (item.ToLower() == "now")
                {
                    var now = System.DateTime.Now;
                    var nowStr = now.ToString("MM/dd/yyyy");
                    tmpList.Add(nowStr);
                    continue;
                }
                tmpList.Add(item);
            }
            return tmpList.ToArray();
        }
    }
}