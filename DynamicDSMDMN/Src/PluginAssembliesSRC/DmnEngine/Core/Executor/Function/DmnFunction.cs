﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using DmnEngine.Core.CustomException;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;

namespace DmnEngine.Core.Executor.Function
{
    public class DmnFunction
    {
        private readonly ConcurrentDictionary<string, object> _sourceValuesCache;

        public DmnFunction(DmnDto dmnDto)
        {
            _dmnDto = dmnDto;
            _targetEntity = dmnDto.TargetEntity;
            _dmnFunction = this;

            _sourceValuesCache = dmnDto.DataService._sourceValuesCache;
        }

        public DmnDto _dmnDto { get; set; }

        public string Result { get; set; }

        /// <summary>
        /// </summary>
        public Entity _outputEntity { get; set; }

        /// <summary>
        /// </summary>
        public string _outputField { get; set; }

        /// <summary>
        /// </summary>
        public Entity _targetEntity { get; set; }

        public DmnFunction _dmnFunction { get; set; }
        //protected static IOrganizationService _orgService { get; private set; }

        public string GetResult()
        {
            return Result;
        }

        protected bool CheckParams(string[] args)
        {
            return args.Length != 0;
        }

        protected void UpdateCurrentTargetEntity(IOrganizationService orgService)
        {
            orgService.Update(_outputEntity);
        }

        protected void SetResult(string value)
        {
            Result = value;
        }


        protected Entity GetLastRelatedEntity(Entity entity, string[] attributesArr,
            IOrganizationService orgService)
        {
            if (!_outputEntity.Attributes.ContainsKey(attributesArr[0]))
                return null;

            var attributeValue = entity.Attributes[attributesArr[0]] as EntityReference;

            if (attributeValue == null)
                return null;

            attributesArr = attributesArr.Skip(1).ToArray();
            var currentEntity = orgService.Retrieve(attributeValue.LogicalName, attributeValue.Id, new ColumnSet());

            if (attributesArr.Count() == 1)
                return currentEntity;

            return GetLastRelatedEntity(currentEntity, attributesArr, orgService);
        }

        protected void SetValueToEntity(Entity entity, string attr, string value)
        {
            if (entity == null || string.IsNullOrEmpty(attr))
                return;

            var attMetadata = _dmnDto.DataService.GetAttributeMetadata(entity.LogicalName, attr).FirstOrDefault();

            var attrType = attMetadata.AttributeType;

            if (attrType == null)
                return;

            switch (attrType)
            {
                case AttributeTypeCode.Integer:
                    entity.Attributes[attr] = (int) Math.Round(double.Parse(value));
                    break;
                case AttributeTypeCode.Decimal:
                    entity.Attributes[attr] = decimal.Parse(value);
                    break;
                case AttributeTypeCode.Double:
                    entity.Attributes[attr] = double.Parse(value);
                    break;
                case AttributeTypeCode.Money:
                    entity.Attributes[attr] = new Microsoft.Xrm.Sdk.Money(decimal.Parse(value));
                    break;
                case AttributeTypeCode.String:
                    entity.Attributes[attr] = value.GetType() == typeof(string) ? value : value;
                    break;
                case AttributeTypeCode.Boolean:
                    entity.Attributes[attr] = bool.Parse(value);
                    break;
                case AttributeTypeCode.DateTime:
                    var date = System.DateTime.Parse(value);
                    entity.Attributes[attr] = date;
                    break;
                case AttributeTypeCode.Picklist:
                    if (value == "''")
                    {
                        entity.Attributes[attr] = null;
                        break;
                    }
                    var key = GetPickListKeyByValue(value, attMetadata);
                    var option = new OptionSetValue(key);
                    entity.Attributes[attr] = option;
                    break;
            }
            //var target = orgService.Retrieve(entity.LogicalName, entity.Id, new ColumnSet(attr));
            //target.Attributes[attr] = entity.Attributes[attr];
            //orgService.Update(target);
        }

        public IEnumerable<AttributeMetadata> GetAttributeMetadata(DmnDto dmnDto, string entityName, string attName)
        {
            return dmnDto.DataService.GetAttributeMetadata(entityName, attName);
        }

        private static int GetPickListKeyByValue(string value, AttributeMetadata fieldMetadata)
        {
            try
            {
                var attMetadata = (EnumAttributeMetadata) fieldMetadata;
                var optionMetadata = attMetadata.OptionSet.Options.FirstOrDefault();
                var key = optionMetadata.Value;

                var firstOrDefault = attMetadata.OptionSet.Options
                    .FirstOrDefault(x => x.Label.UserLocalizedLabel.Label == value);

                if (firstOrDefault != null)
                    key = firstOrDefault.Value;

                return (int) key;
            }
            catch (Exception)
            {
                throw new ExecutorException();
            }
        }

        protected AttributeTypeCode? GetAttributeType(string attr, IOrganizationService orgService,
            Entity entity = null)
        {
            if (entity == null)
                entity = _targetEntity;

            if (string.IsNullOrEmpty(attr))
                return null;

            var attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entity.LogicalName,
                LogicalName = attr
            };

            try
            {
                var response = (RetrieveAttributeResponse) orgService.Execute(attributeRequest);

                if (response.Results.Count == 0)
                    return null;

                return response.AttributeMetadata.AttributeType;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string[] GetExpressionValue(string[] exprValue)
        {
            var tmpList = new List<string>();
            foreach (var item in exprValue)
            {
                if (item.IndexOf('.') != -1)
                {
                    var val = GetSourceValue(item)?.ToString() ?? item;
                    tmpList.Add(val);
                    continue;
                }
                tmpList.Add(item);
            }
            return tmpList.ToArray();
        }

        private string GetEmptyResultByType(AttributeMetadata attrType)
        {
            var defaultNumericRs = "0";
            switch (attrType.AttributeType)
            {
                case AttributeTypeCode.Integer:
                    return defaultNumericRs;
                case AttributeTypeCode.Decimal:
                    return defaultNumericRs;
                case AttributeTypeCode.Double:
                    return defaultNumericRs;
                case AttributeTypeCode.Money:
                    return defaultNumericRs;
                case AttributeTypeCode.String:
                    return string.Empty;
                case AttributeTypeCode.Boolean:
                    return "false";
                case AttributeTypeCode.DateTime:
                    return string.Empty;
                case AttributeTypeCode.Picklist:
                    return string.Empty;
                case AttributeTypeCode.Memo:
                    return string.Empty;
            }
            return string.Empty;
        }

        /// <summary>
        ///     key consist of _targetEntity.Id^fullFieldName
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object TryGetValueFromCache(string key)
        {
            object value = null;
            if (!_sourceValuesCache.ContainsKey(key)) return null;
            return _sourceValuesCache.TryGetValue(key, out value) ? value : null;
        }

        /// <summary>
        /// </summary>
        /// <param name="fullFieldName"></param>
        /// <returns></returns>
        /*private object GetSourceValue(string fullFieldName)
        {
            object value = null;
            var entityLogicalName = fullFieldName.Split('.')[0];
            var fieldName = fullFieldName.Split('.')[1];
            var sourcEntity = RetreiveEntity(entityLogicalName, _targetEntity.Id);
            return sourcEntity.Attributes.TryGetValue(fieldName, out value) ? value : null;
        }*/
        public object GetSourceValue(string fullFieldName)
        {
            var key = $"{_targetEntity.Id}^{fullFieldName}";
            var valueFromCache = TryGetValueFromCache(key);
            if (valueFromCache != null)
                return valueFromCache;
            var fieldParts = fullFieldName.Split('.');

            if (fieldParts.First() != _targetEntity.LogicalName)
                return null;

            //remove base entity name from array
            fieldParts = fieldParts.Skip(1).ToArray();

            var baseEntity = _dmnDto.DataService.RetreiveEntity(_targetEntity.LogicalName, _targetEntity.Id);
            var attMetadata =
                _dmnDto.DataService.GetAttributeMetadata(baseEntity.LogicalName, fieldParts.FirstOrDefault())
                    .FirstOrDefault();
            object firstFieldValue = null;
            baseEntity.Attributes.TryGetValue(fieldParts.FirstOrDefault(), out firstFieldValue);
            if (firstFieldValue == null)
                return GetEmptyResultByType(attMetadata);

            //if field is not lookup
            if (firstFieldValue.GetType() != typeof(EntityReference))
                return GetRS(firstFieldValue);
            //if field is lookup
            object lookupValue = null;
            baseEntity.Attributes.TryGetValue(fieldParts.First(), out lookupValue);
            var resultValue = GetLookupInnerValue(lookupValue as EntityReference, fieldParts.Skip(1).ToArray());
            return GetRS(resultValue);
        }

        public string GetRS(object value)
        {
            if (value == null)
                return "0";
            if (value.GetType() == typeof(Microsoft.Xrm.Sdk.Money))
                return (value as Microsoft.Xrm.Sdk.Money).Value.ToString();
            return value.ToString();
        }

        private object GetLookupInnerValue(EntityReference lookup, string[] fields)
        {
            if (lookup == null)
                return null;

            object value = null;

            var entity = _dmnDto.DataService.RetreiveEntity(lookup.LogicalName, lookup.Id);
            entity.Attributes.TryGetValue(fields.First(), out value);

            if (value is EntityReference)
                value = GetLookupInnerValue(value as EntityReference, fields.Skip(1).ToArray());

            return value;
        }
    }
}