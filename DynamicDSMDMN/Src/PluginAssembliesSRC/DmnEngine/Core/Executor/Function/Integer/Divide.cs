﻿using System;

namespace DmnEngine.Core.Executor.Function.Integer
{
    internal class Divide : DmnFunction
    {
        public Divide(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            args = _dmnFunction.GetExpressionValue(args);
            var rs = Convert.ToInt32(args[0]) / Convert.ToInt32(args[1]);
            return rs.ToString();
        }
    }
}