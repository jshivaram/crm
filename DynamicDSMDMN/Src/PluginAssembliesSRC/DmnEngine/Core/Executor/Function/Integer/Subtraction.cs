﻿using System;
using System.Linq.Expressions;

namespace DmnEngine.Core.Executor.Function.Integer
{
    internal class Subtraction : DmnFunction
    {
        public Subtraction(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            args = _dmnFunction.GetExpressionValue(args);


            //NOTE!!!!
            var x = 0;
            var y = 0;

            switch (args.Length)
            {
                case 1:
                    x = Convert.ToInt32(args[0]);
                    break;
                case 2:
                    x = Convert.ToInt32(args[0]);
                    y = Convert.ToInt32(args[1]);
                    break;
            }

            var xParam = Expression.Parameter(typeof(int), "x");
            var yParam = Expression.Parameter(typeof(int), "y");
            Expression negative = Expression.Negate(yParam);
            Expression sum = Expression.Add(xParam, negative);
            var lambdaExpression = Expression.Lambda(sum, xParam, yParam);
            var newLambda = (Func<int, int, int>) lambdaExpression.Compile();

            return newLambda(x, y).ToString();
        }
    }
}