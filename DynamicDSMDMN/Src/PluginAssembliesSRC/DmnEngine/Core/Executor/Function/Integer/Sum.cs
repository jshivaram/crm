﻿using System;
using System.Linq;

namespace DmnEngine.Core.Executor.Function.Integer
{
    internal class Sum : DmnFunction
    {
        public Sum(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            args = _dmnFunction.GetExpressionValue(args);
            var sum = args.Sum(item => Convert.ToInt32(item));
            return sum.ToString();
        }
    }
}