﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using DmnEngine.Core.DmnQueryBuilder;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

namespace DmnEngine.Core.Executor.Function.Relationship
{
    public abstract class AggregateFunction : DmnFunction
    {
        public MainEntity _mainEntity;
        public ITracingService _tracer;


        public AggregateFunction(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public void AddMainAtt(XElement entity)
        {
            foreach (var att in _mainEntity.Attributes)
            {
                var attElement = new XElement("attribute");
                var attName = new XAttribute("name", att.Name);
                var attAlias = new XAttribute("alias", att.Alias);
                var attAggregate = new XAttribute("aggregate", att.Aggregate);
                attElement.Add(attName);
                attElement.Add(attAlias);
                attElement.Add(attAggregate);
                entity.Add(attElement);
            }
        }

        public void AddFilter(XElement filterElement, Filter filters)
        {
            foreach (var filter in filters.Conditions)
            {
                var conditionElement = new XElement("condition");
                var conditionAtt = new XAttribute("attribute", filter.Attribute);
                var conditionOp = new XAttribute("operator", filter.Op);

                conditionElement.Add(conditionAtt);
                conditionElement.Add(conditionOp);

                if (filter.Op == "in" || filter.Op == "between")
                {
                    foreach (var val in filter.Values)
                    {
                        var conditionVal = new XElement("value", val);
                        conditionElement.Add(conditionVal);
                    }
                }
                else
                {
                    var conditionVal = new XAttribute("value", filter.Value);
                    conditionElement.Add(conditionVal);
                }

                filterElement.Add(conditionElement);
            }
        }

        public string CreateFetchXML()
        {
            var xdoc = new XDocument();

            // создаем первый элемент
            var entity = new XElement("entity");

            // создаем атрибут
            var entityName = new XAttribute("name", _mainEntity.Name);
            entity.Add(entityName);

            AddMainAtt(entity);

            foreach (var fltr in _mainEntity.Filters)
            {
                var filterElement = new XElement("filter");
                var filterType = new XAttribute("type", fltr.Type);
                filterElement.Add(filterType);

                AddFilter(filterElement, fltr);

                if (filterElement.HasElements)
                    entity.Add(filterElement);
            }


            AddLinkEntity(entity, _mainEntity.LinkEntity);

            // создаем корневой элемент
            var mainFetch = new MainFetch
            {
                Entity = _mainEntity
            };


            var fetch = new XElement("fetch");
            var fetchMapping = new XAttribute("mapping", mainFetch.Mapping);
            var fetchAggregate = new XAttribute("aggregate", mainFetch.Aggregate);
            fetch.Add(fetchMapping);
            fetch.Add(fetchAggregate);

            fetch.Add(entity);
            xdoc.Add(fetch);
            return xdoc.ToString();
        }

        private void AddLinkEntity(XElement entity, Dictionary<string, LinkEntity> linkEntity)
        {
            foreach (var key in linkEntity.Keys)
            {
                var linkedEntity = linkEntity[key];
                var linkEntityEl = new XElement("link-entity");
                var linkEntityAttName = new XAttribute("name", linkedEntity.Name);
                var linkEntityFrom = new XAttribute("from", linkedEntity.From);
                var linkEntityAttTo = new XAttribute("to", linkedEntity.To);
                var linkEntityAttType = new XAttribute("link-type", linkedEntity.LinkType);

                linkEntityEl.Add(linkEntityAttName);
                linkEntityEl.Add(linkEntityFrom);
                linkEntityEl.Add(linkEntityAttTo);
                linkEntityEl.Add(linkEntityAttType);


                foreach (var fltr in linkedEntity.Filters)
                {
                    var filter = new XElement("filter");
                    var filterType = new XAttribute("type", fltr.Type);
                    filter.Add(filterType);

                    AddFilter(filter, fltr);
                    if (filter.HasElements)
                        linkEntityEl.Add(filter);
                }


                AddLinkEntity(linkEntityEl, linkedEntity.RelatedEntity);

                entity.Add(linkEntityEl);
            }
        }

        public void AddWhere(string whereConditions, string filterOperator = "and")
        {
            var conditions = whereConditions.Split(';');
            if (conditions.Length < 3)
                return;
            var leftArgs = conditions.First().Split('.');
            var rightArgs = new List<string>();

            for (var i = 2; i < conditions.Length; i++)
                rightArgs.Add(conditions[i]);

            var conditionOperator = conditions.Skip(1).FirstOrDefault();

            var linkedEntity = new LinkEntity();
            var tmpLinkedEntity = new LinkEntity();
            var entityName = leftArgs[0];
            var tmpLinkedEntityName = entityName;
            var fieldName = string.Empty;
            for (var k = 0; k < leftArgs.Length - 1; k++)
            {
                var fieldMetadata = new AttributeMetadata();

                if (leftArgs.Length > k + 1)
                {
                    fieldName = leftArgs[k + 1];
                    fieldMetadata =
                        _dmnDto.DataService.GetAttributeMetadata(tmpLinkedEntityName, fieldName).FirstOrDefault();
                }
                if (fieldMetadata.AttributeType != null && IsAttributeLookup(fieldMetadata))
                {
                    var linkMetadata =
                        GetManyToOneRelationshipsMetadata(entityName, fieldName, _dmnDto.Service).FirstOrDefault();

                    if (k > 0)
                    {
                        var subLinkEntity = new LinkEntity();
                        FillLinkedEntity(subLinkEntity, linkMetadata);

                        tmpLinkedEntity.RelatedEntity.Add(subLinkEntity.Name, subLinkEntity);
                        tmpLinkedEntity = subLinkEntity;
                        tmpLinkedEntityName = tmpLinkedEntity.Name.ToLower();
                        continue;
                    }

                    FillLinkedEntity(linkedEntity, linkMetadata);

                    tmpLinkedEntity = linkedEntity;
                    tmpLinkedEntityName = tmpLinkedEntity.Name.ToLower();
                    entityName = linkMetadata.ReferencedEntity;
                }
                else
                {
                    if (IsAttributePicklist(fieldMetadata))
                        rightArgs = UpdateRightArgs(rightArgs, fieldMetadata);
                    AddJoinFilters(tmpLinkedEntity, rightArgs, fieldName, conditionOperator, filterOperator);
                }
            }

            if (linkedEntity.Name != null)
            {
                if (_mainEntity.LinkEntity.ContainsKey(entityName))
                {
                    var link = _mainEntity.LinkEntity[entityName];
                    foreach (var fltr in linkedEntity.Filters)
                        link.Filters.Add(fltr);
                }

                if (!_mainEntity.LinkEntity.ContainsKey(entityName))
                    _mainEntity.LinkEntity.Add(entityName, linkedEntity);
            }
        }

        private List<string> UpdateRightArgs(List<string> rightArgs, AttributeMetadata fieldMetadata)
        {
            var tmpList = new List<string>();

            var attMetadata = (EnumAttributeMetadata) fieldMetadata;
            if (attMetadata == null)
                return rightArgs;

            foreach (var item in rightArgs.ToArray())
            {
                var search = item;
                var key =
                    attMetadata.OptionSet.Options
                        .FirstOrDefault(x => x.Label.UserLocalizedLabel.Label == search).Value.ToString();
                tmpList.Add(key);
            }
            return tmpList;
        }

        private void FillLinkedEntity(LinkEntity linkEntity, OneToManyRelationshipMetadata linkMetadata)
        {
            linkEntity.Name = linkMetadata.ReferencedEntity;
            linkEntity.From = linkMetadata.ReferencedAttribute;
            linkEntity.To = linkMetadata.ReferencingAttribute;
        }

        private void AddJoinFilters(LinkEntity linkedEntity, List<string> rightArgs, string fieldName,
            string conditionOperator, string filterOperator = "and")
        {
            var targetEntity = _dmnDto.TargetEntity;
            var value = new List<string>();
            foreach (var t in rightArgs)
            {
                var arg = t.Split('.');
                if (arg.Length == 1)
                {
                    value.Add(arg.FirstOrDefault());
                    continue;
                }

                if (arg[0] != _mainEntity.Name.ToLower() && arg[0] != targetEntity.LogicalName.ToLower())
                {
                    value.Add(t);
                    continue;
                }
                if (arg[0].ToLower() == targetEntity.LogicalName.ToLower())
                    value.Add(CalcArgumentsValue(t, _dmnDto.Service, targetEntity));
            }
            AddFilterWhere(linkedEntity, fieldName, value, conditionOperator, filterOperator);
        }

        private void AddFilterWhere(LinkEntity linkedEntity, string fieldName, List<string> value,
            string conditionOperator, string filterOperator = "and")
        {
            var filter = new Filter(filterOperator);


            var condition = new FetchXMLCondition
            {
                Attribute = fieldName,
                Op = GetOperator(conditionOperator)
            };

            if (condition.Op == "in" || condition.Op == "between")
                foreach (var val in value)
                    condition.Values.Add(val);
            else
                condition.Value = value.FirstOrDefault();

            if (linkedEntity.Name != null)
            {
                filter.Conditions.Add(condition);
                linkedEntity.Filters.Add(filter);
                return;
            }

            filter.Conditions.Add(condition);
            _mainEntity.Filters.Add(filter);
        }

        private string GetOperator(string op)
        {
            switch (op.ToLower())
            {
                case "equals":
                    return "eq";
                case "in":
                    return "in";
                case "between":
                    return "between";
                case "gt":
                    return "gt";
                case "lt":
                    return "lt";
                case "not_equals":
                    return "neq";
                default:
                    return "eq";
            }
        }


        /// <summary>
        /// </summary>
        /// <param name="conditions"></param>
        public void BuildConditions(string[] conditions)
        {
            if (conditions.Count() == 0)
                throw new InvalidPluginExecutionException("Conditions is not correct. The WHERE doesn't exist");
            foreach (var condition in conditions)
            {
                var args = condition.Split('$');
                AddConditions(args);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="conditions"></param>
        public void AddConditions(string[] conditions)
        {
            var op = conditions.FirstOrDefault();
            switch (op.ToLower())
            {
                case "where":
                    AddWhere(conditions.LastOrDefault());
                    break;
                case "and":
                    AddWhere(conditions.LastOrDefault());
                    break;
                case "or":
                    AddWhere(conditions.LastOrDefault(), "or");
                    break;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <param name="referedEntityLogicalName"></param>
        /// <returns></returns>
        protected IEnumerable<OneToManyRelationshipMetadata> GetOneToManyRelationshipsMetadata
            (string entityLogicalName, string referedEntityLogicalName, IOrganizationService orgService)
        {
            var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = entityLogicalName
            };

            var retrieveBankAccountEntityResponse =
                (RetrieveEntityResponse) orgService.Execute(retrieveBankAccountEntityRequest);

            var oneToNRelationships = retrieveBankAccountEntityResponse.EntityMetadata.OneToManyRelationships;
            return oneToNRelationships.Where(r => r.ReferencingEntity == referedEntityLogicalName).ToList();
        }

        /// <summary>
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <param name="referedEntityLogicalName"></param>
        /// <returns></returns>
        protected IEnumerable<OneToManyRelationshipMetadata> GetManyToOneRelationshipsMetadata
            (string entityLogicalName, string referedEntityLogicalName, IOrganizationService orgService)
        {
            var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = entityLogicalName
            };

            var retrieveBankAccountEntityResponse =
                (RetrieveEntityResponse) orgService.Execute(retrieveBankAccountEntityRequest);

            var oneToNRelationships = retrieveBankAccountEntityResponse.EntityMetadata.ManyToOneRelationships;
            return oneToNRelationships.Where(r => r.ReferencingAttribute == referedEntityLogicalName).ToList();
        }

        protected bool IsAttributeLookup(AttributeMetadata attributeMetadata)
        {
            return attributeMetadata.AttributeType == AttributeTypeCode.Lookup;
        }

        public bool IsAttributePicklist(AttributeMetadata attributeMetadata)
        {
            return attributeMetadata.AttributeType == AttributeTypeCode.Picklist;
        }


        private string CalcArgumentsValue(string rightArguments, IOrganizationService orgService, Entity targetEntity)
        {
            try
            {
                if (string.IsNullOrEmpty(rightArguments)) return string.Empty;
                var tmp = string.Empty;
                var rightArgumentsArr = rightArguments.Split('.');

                var condition = rightArguments.Split('.');
                var firstOrDefault = condition.FirstOrDefault();
                if (firstOrDefault != null && firstOrDefault.ToLower() != targetEntity.LogicalName.ToLower())
                    return string.Empty;
                tmp = GetRelatedEntity(rightArguments, targetEntity, condition, orgService);

                return !string.IsNullOrEmpty(tmp) ? tmp : string.Empty;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private string GetRelatedEntity(string defaultValue, Entity targetEntity, string[] entityPath,
            IOrganizationService orgService, int entityIndex = 1)
        {
            if (entityPath.Length <= entityIndex + 1)
            {
                var fieldName = entityPath.LastOrDefault();
                object rs = null;
                return targetEntity.Attributes.TryGetValue(fieldName, out rs) ? rs.ToString() : defaultValue;
            }
            var link = entityPath[entityIndex];
            entityIndex++;
            object lookUp = null;
            if (!targetEntity.Attributes.TryGetValue(link, out lookUp))
                GetRelatedEntity(defaultValue, targetEntity, entityPath, orgService, entityIndex);

            var relatedEntity = (EntityReference) lookUp;
            var output = _dmnDto.DataService.RetreiveEntity(relatedEntity.LogicalName, relatedEntity.Id);

            return GetRelatedEntity(defaultValue, output, entityPath, orgService, entityIndex);
        }

        public string GetRS(object value)
        {
            if (value == null)
                return "0";
            if (value.GetType() == typeof(Microsoft.Xrm.Sdk.Money))
                return (value as Microsoft.Xrm.Sdk.Money).Value.ToString();
            return value.ToString();
        }
    }
}