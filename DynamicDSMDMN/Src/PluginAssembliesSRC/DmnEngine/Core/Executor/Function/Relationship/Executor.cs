﻿using System;

namespace DmnEngine.Core.Executor.Function.Relationship
{
    internal class Executor : IExecutor
    {
        public string PerformOperation(string op, string[] args, DmnFunction dmnFunction)
        {
            return Execute(op, args, dmnFunction);
        }


        private string Execute(string op, string[] args, DmnFunction dmnFunction)
        {
            try
            {
                switch (op)
                {
                    case "count":
                        var count = new Count(dmnFunction._dmnDto);
                        return count.Evaluate(args);
                    case "rel_max":
                        var max = new Max(dmnFunction._dmnDto);
                        return max.Evaluate(args);
                    case "rel_min":
                        var min = new Min(dmnFunction._dmnDto);
                        return min.Evaluate(args);
                    case "rel_avg":
                        var avg = new Avg(dmnFunction._dmnDto);
                        return avg.Evaluate(args);
                    case "sum":
                        var sum = new Sum(dmnFunction._dmnDto);
                        return sum.Evaluate(args);
                }
            }

            catch (Exception e)
            {
                dmnFunction._dmnDto.Tracer.Trace(e.Message);
            }
            return string.Empty;
        }
    }
}