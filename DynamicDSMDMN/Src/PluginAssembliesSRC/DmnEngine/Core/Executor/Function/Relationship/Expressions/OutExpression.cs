﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace DmnEngine.Core.Executor.Function.Relationship.Expressions
{
    public class OutExpression
    {
        private readonly IOrganizationService _orgService;
        private readonly Entity _targetEntity;

        public OutExpression(Entity targetEntity, IOrganizationService orgService)
        {
            _targetEntity = targetEntity;
            _orgService = orgService;
            RightArguments = new List<string>();
        }

        public string Name { get; set; }
        public string LeftArgument { get; set; }
        public string Operator { get; set; }
        public List<string> RightArguments { get; set; }

        public void CalculateConditions()
        {
            CalcLeftArgument();
            CalcRightArguments();
        }

        private void CalcLeftArgument()
        {
            if (LeftArgument == string.Empty) return;
            var leftArgument = LeftArgument.Split('.');
            var firstOrDefault = leftArgument.FirstOrDefault();
            if (firstOrDefault != null && firstOrDefault.ToLower() != _targetEntity.LogicalName.ToLower())
                return;
            LeftArgument = GetRelatedEntity(LeftArgument, _targetEntity, leftArgument);
        }

        private void CalcRightArguments()
        {
            if (RightArguments.Count == 0) return;
            var tmp = new List<string>();
            foreach (var args in RightArguments)
            {
                var condition = args.Split('.');
                var firstOrDefault = condition.FirstOrDefault();
                if (firstOrDefault != null && firstOrDefault.ToLower() != _targetEntity.LogicalName.ToLower())
                    continue;
                tmp.Add(GetRelatedEntity(args, _targetEntity, condition));
            }
            if (tmp.Count != 0)
                RightArguments = tmp;
        }

        private string GetRelatedEntity(string defaultValue, Entity targetEntity, string[] entityPath,
            int entityIndex = 1)
        {
            if (entityPath.Length <= entityIndex + 1)
            {
                var fieldName = entityPath.LastOrDefault();
                object rs = null;
                return targetEntity.Attributes.TryGetValue(fieldName, out rs) ? rs.ToString() : defaultValue;
            }
            var link = entityPath[entityIndex];
            entityIndex++;
            object lookUp = null;
            if (!targetEntity.Attributes.TryGetValue(link, out lookUp))
                GetRelatedEntity(defaultValue, targetEntity, entityPath, entityIndex);
            var relatedEntity = (EntityReference) lookUp;
            var output = _orgService.Retrieve(relatedEntity.LogicalName, relatedEntity.Id, new ColumnSet(true));

            return GetRelatedEntity(defaultValue, output, entityPath, entityIndex);
        }
    }
}