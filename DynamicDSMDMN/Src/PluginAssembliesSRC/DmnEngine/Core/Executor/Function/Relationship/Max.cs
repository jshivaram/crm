﻿using System.Linq;
using DmnEngine.Core.DmnQueryBuilder;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace DmnEngine.Core.Executor.Function.Relationship
{
    internal class Max : AggregateFunction
    {
        public Max(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            if (args.Count() == 0 || args.FirstOrDefault() == null)
                return string.Empty;

            var arg = args.First();
            var argsArr = arg.Split('\\');

            var select = argsArr.FirstOrDefault();
            if (select != null)
            {
                var selectArr = select.Split('.');
                AddMainEntity(selectArr);
            }

            if (argsArr.Length > 1)
                BuildConditions(argsArr.Skip(1).ToArray());
            var fetchXML = CreateFetchXML();

            var query = new FetchExpression(fetchXML);

            var totals = _dmnDto.Service.RetrieveMultiple(query);
            if (totals.Entities.Count == 0)
            {
                SetResult("");
                return string.Empty;
            }

            var val = (totals.Entities[0].Attributes["rs"] as AliasedValue).Value;
            return GetRS(val);
        }


        public void AddMainEntity(string[] select)
        {
            if (select == null || select.Length < 2)
                throw new InvalidPluginExecutionException(
                    "Sum cannot be calculated. Please check the arguments of the functions");
            var initEntity = select.FirstOrDefault();
            var att = select.LastOrDefault();
            for (var i = 1; i < select.Length; i++)
            {
                var field = select[i];
                var fieldMetadata = _dmnDto.DataService.GetAttributeMetadata(initEntity, field);
                if (IsAttributeLookup(fieldMetadata.FirstOrDefault()))
                    continue;
            }
            _mainEntity = new MainEntity(initEntity);

            var fetchAtt = new FetchXMLAttribute();
            fetchAtt.Name = att;
            fetchAtt.Aggregate = "max";
            fetchAtt.Alias = "rs";

            _mainEntity.Attributes.Add(fetchAtt);
        }
    }
}