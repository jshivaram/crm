﻿namespace DmnEngine.Core.Executor.Function.String
{
    internal class Concat : DmnFunction
    {
        public Concat(DmnDto dmnDto) : base(dmnDto)
        {
        }

        public string Evaluate(string[] args)
        {
            args = _dmnFunction.GetExpressionValue(args);
            var res = string.Join("", args);
            return res;
        }
    }
}