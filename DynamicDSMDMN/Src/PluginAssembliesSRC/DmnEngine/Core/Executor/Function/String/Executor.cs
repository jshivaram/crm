﻿using System;

namespace DmnEngine.Core.Executor.Function.String
{
    internal class Executor : IExecutor
    {
        /*private static readonly Dictionary<string, OperationDelegate> _intOperations =
            new Dictionary<string, OperationDelegate>
            {
                {"setValue", DmnFunction.SetValue},
                {"concat", Concat.Evaluate}
            };

    */

        public string PerformOperation(string op, string[] args, DmnFunction dmnFunction)
        {
            return Execute(op, args, dmnFunction);
        }


        private string Execute(string op, string[] args, DmnFunction dmnFunction)
        {
            try
            {
                switch (op)
                {
                    case "concat":
                        var concat = new Concat(dmnFunction._dmnDto);
                        return concat.Evaluate(args);
                }
            }

            catch (Exception e)
            {
                dmnFunction._dmnDto.Tracer.Trace(e.Message);
            }
            return string.Empty;
        }
    }
}