﻿using DmnEngine.Core.Executor.Function;

namespace DmnEngine.Core.Executor
{
    //public delegate void OperationDelegate(string[] args, IOrganizationService orgService);

    public interface IExecutor
    {
        string PerformOperation(string op, string[] args, DmnFunction dmnFunction);
    }
}