﻿namespace DmnEngine.Core.Interfaces
{
    public interface ICoreCalculator
    {
        string Calculate(string func);
    }
}