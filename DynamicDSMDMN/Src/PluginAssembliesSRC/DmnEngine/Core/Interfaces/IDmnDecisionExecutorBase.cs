﻿namespace DmnEngine.Core.Interfaces
{
    public interface IDmnDecisionExecutorBase
    {
        string Run(string expression);
    }
}