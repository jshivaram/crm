﻿namespace DmnEngine.Core.Interfaces
{
    public interface IFormulaCalculator
    {
        string Calculate(string baseStr);
    }
}