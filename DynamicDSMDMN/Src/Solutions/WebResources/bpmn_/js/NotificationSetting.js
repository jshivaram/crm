function getRelatedEntity2Notification(entityName) {
    //assoiciate entity and related entities    
    var config = [{
        "entityName": "ddsm_project",
        "relationships": ["ddsm_measure", "ddsm_financial"],
        "isCheckDMNJobs": true

    }];
    return search(entityName, config);
}

function search(nameKey, config) {
    for (var i = 0; i < config.length; i++) {
        if (config[i].entityName === nameKey) {
            return config[i];
        }
    }
}

