/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 86);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

var config = {
    'N:1': {
        'showEntityBox': false,
        'memo': {
            'SetValue': {
                displayName: 'SetValue',
                argsType: 'number',
                argsNumber: 'setValue',
                template: 'SetValue({{args}})'
            }
        },
        'string': {
            'concat': {
                displayName: 'Concat',
                argsType: 'number',
                argsNumber: 'concat',
                template: 'Concat({{args}})'
            },
            'SetValue': {
                displayName: 'SetValue',
                argsType: 'number',
                argsNumber: 'setValue',
                template: 'SetValue({{args}})'
            }
        },
        'picklist': {
            'SetValue': {
                displayName: 'SetValue',
                argsType: 'number',
                argsNumber: 'setValue',
                template: 'SetValue({{args}})'
            }
        },
        'integer': {
            'addition': {
                displayName: 'Addition',
                argsNumber: 'N',
                template: 'Addition({{args}})'
            },
            'subtraction': {
                displayName: 'Subtraction',
                argsNumber: '2',
                template: 'Subtraction({{arg1}},{{arg2}})'
            },
            'multiply': {
                displayName: 'Multiply',
                argsNumber: 'N',
                template: 'Multiply({{args}})',
            },
            'divide': {
                displayName: 'Divide',
                argsNumber: 2,
                template: 'Divide({{arg1}},{{arg2}})'
            },
            'SetValue': {
                displayName: 'SetValue',
                argsNumber: 1,
                template: 'setValue({{arg1}})'
            }
        },
        'decimal': {
            'addition': {
                displayName: 'Addition',
                argsNumber: 'N',
                template: 'Addition({{args}})',
            },
            'subtraction': {
                displayName: 'Subtraction',
                argsNumber: '2',
                template: 'Subtraction({{arg1}},{{arg2}})'
            },
            'multiply': {
                displayName: 'Multiply',
                argsNumber: 'N',
                template: 'Multiply({{args}})',
            },
            'divide': {
                displayName: 'Divide',
                argsNumber: 2,
                template: 'Divide({{arg1}},{{arg2}})'
            },
            'SetValue': {
                displayName: 'SetValue',
                argsNumber: 1,
                template: 'setValue({{arg1}})'
            }
        },
        'double': {
            'addition': {
                displayName: 'Addition',
                argsNumber: 'N',
                template: 'Addition({{args}})'
            },
            'subtraction': {
                displayName: 'Subtraction',
                argsNumber: '2',
                template: 'Subtraction({{arg1}},{{arg2}})'
            },
            'multiply': {
                displayName: 'Multiply',
                argsNumber: 'N',
                template: 'Multiply({{args}})',
            },
            'divide': {
                displayName: 'Divide',
                argsNumber: 2,
                template: 'Divide({{arg1}},{{arg2}})'
            },
            'SetValue': {
                displayName: 'SetValue',
                argsNumber: 1,
                template: 'setValue({{arg1}})'
            }
        },
        'money': {
            'addition': {
                displayName: 'Addition',
                argsNumber: 'N',
                template: 'Addition({{args}})',
            },
            'subtraction': {
                displayName: 'Subtraction',
                argsNumber: '2',
                template: 'Subtraction({{arg1}},{{arg2}})'
            },
            'multiply': {
                displayName: 'Multiply',
                argsNumber: 'N',
                template: 'Multiply({{args}})',
            },
            'divide': {
                displayName: 'Divide',
                argsNumber: 2,
                template: 'Divide({{arg1}},{{arg2}})'
            },
            'SetValue': {
                displayName: 'SetValue',
                argsNumber: 1,
                template: 'setValue({{arg1}})'
            }
        },
        'boolean': {
            'isTrue': {
                displayName: 'IsTrue',
                argsNumber: 1,
                template: 'IsTrue({{arg1}})'
            },
            'isFalse': {
                displayName: 'isFalse',
                argsNumber: 1,
                template: 'IsFalse({{arg1}})'
            },
            'SetValue': {
                displayName: 'SetValue',
                argsNumber: 1,
                template: 'setValue({{arg1}})'
            }
        },
        'datetime': {
            'SetValue': {
                displayName: 'SetValue',
                argsNumber: 1,
                template: 'setValue({{arg1}})'
            },
            'now': {
                displayName: 'Now',
                argsNumber: 0,
                template: 'Now'
            },
            'addDays': {
                displayName: 'AddDays',
                argsNumber: 1,
                template: 'AddDays({{arg1}},{{arg2}})'
            },
            'addMonths': {
                displayName: 'AddMonths',
                argsNumber: 1,
                template: 'AddMonths({{arg1}},{{arg2}})'
            },
            'addYears': {
                displayName: 'AddYears',
                argsNumber: 1,
                template: 'AddYears({{arg1}},{{arg2}})'
            },
        },
        'outputvalue':{
            'concat': {
                displayName: 'Concat',
                argsType: 'number',
                argsNumber: 'concat',
                template: 'Concat({{args}})'
            },
            'addition': {
                displayName: 'Addition',
                argsNumber: 'N',
                template: 'Addition({{args}})'
            },
            'subtraction': {
                displayName: 'Subtraction',
                argsNumber: '2',
                template: 'Subtraction({{arg1}},{{arg2}})'
            },
            'multiply': {
                displayName: 'Multiply',
                argsNumber: 'N',
                template: 'Multiply({{args}})',
            },
            'divide': {
                displayName: 'Divide',
                argsNumber: 2,
                template: 'Divide({{arg1}},{{arg2}})'
            },
            'isTrue': {
                displayName: 'IsTrue',
                argsNumber: 1,
                template: 'IsTrue({{arg1}})'
            },
            'isFalse': {
                displayName: 'isFalse',
                argsNumber: 1,
                template: 'IsFalse({{arg1}})'
            },
            'now': {
                displayName: 'Now',
                argsNumber: 0,
                template: 'Now'
            },
            'addDays': {
                displayName: 'AddDays',
                argsNumber: 1,
                template: 'AddDays({{arg1}},{{arg2}})'
            },
            'addMonths': {
                displayName: 'AddMonths',
                argsNumber: 1,
                template: 'AddMonths({{arg1}},{{arg2}})'
            },
            'addYears': {
                displayName: 'AddYears',
                argsNumber: 1,
                template: 'AddYears({{arg1}},{{arg2}})'
            }
        }
    },
    '1:N': {
        'showEntityBox': true,
        'count': {
            isAgregate: true,
            displayName: 'Count',
            argsNumber: 1,
            template: 'Count({{arg1}})'
        },
        'where': {
            isHaveOperators: true,
            separator: '.',
            displayName: 'Where',
            argsNumber: 2,
            template: 'Where({{arg1}}{{operator}}{{arg2}})',
            operators: ['=', '!=', '<', '>', 'in'],
            operationTranslate: {
                '=': 'equals',
                '!=': 'not_equals',
                '>': 'gt',
                '<': 'lt',
                'in': 'in'

            }
        },
        'and': {
            isHaveOperators: true,
            separator: '.',
            displayName: 'And',
            argsNumber: 2,
            template: 'And({{arg1}}{{operator}}{{arg2}})',
            operators: ['=', '!=', '<', '>', 'in', 'between'],
            operationTranslate: {
                '=': 'equals',
                '!=': 'not_equals',
                '>': 'gt',
                '<': 'lt',
                'in': 'in',
                'between': 'between'

            }
        },
        'or': {
            isHaveOperators: true,
            separator: '.',
            displayName: 'Or',
            argsNumber: 2,
            template: 'Or({{arg1}}{{operator}}{{arg2}})',
            operators: ['=', '!=', '<', '>', 'in'],
            operationTranslate: {
                '=': 'equals',
                '!=': 'not_equals',
                '>': 'gt',
                '<': 'lt',
                'in': 'in'
            }
        },
        'sum': {
            isAgregate: true,
            displayName: 'Sum',
            argsNumber: 1,
            template: 'Addition({{arg1}})'
        },
        'rel_max': {
            isAgregate: true,
            displayName: 'MaxAgr',
            argsNumber: 1,
            template: 'MaxAgr({{arg1}})'
        },
        'rel_min': {
            isAgregate: true,
            displayName: 'MinAgr',
            argsNumber: 1,
            template: 'MinAgr({{arg1}})'
        },
        'rel_avg': {
            isAgregate: true,
            displayName: 'AvgAgr',
            argsNumber: 1,
            template: 'AvgAgr({{arg1}})'
        },


    },
    'GlobalFunctions': {
        'Abs': {},
        'Acos': {},
        'Asin': {},
        'Atan': {},
        'Ceiling': {},
        'Cos': {},
        'Exp': {},
        'Floor': {},
        'IEEERemainder': {},
        'Log': {},
        'Log10': {},
        'Max': {},
        'Min': {},
        'Pow': {},
        'Round': {},
        'Sign': {},
        'Sqrt': {},
        'Tan': {},
        'Truncate': {}

    },
    ExecuteDmn: {
        getMtoO: {
            displayName: 'ManyToOne'
        },
        getOtoM: {
            displayName: 'OneToMany'
        }
    },
    //Template, how executeDmn rule should be dropped in xml file.
    //example:{functionName}{functionSeparator}{arg1}{argsSeparator}{arg2}
    executeDmnXmlTemplate: {
        'functionName': 'ExecuteDMN',
        'functionSeparator': '^',
        'argsSeparator': ','
    }
}

module.exports = config;


/***/ }),
/* 1 */
/***/ (function(module, exports) {


let popupConstants = {
    arrIds: {
        businessRulesId: 'business-rules-dropdown',
        relationTypesId: 'relation-types-dropdown',
        relationsId: 'relations-dropdown',
        btnSubmitId: 'btn-submit',
        formulaEditorId: 'formula-editor',
        relationsDropdownId:'relations-dropdown-id'
    },
    relationTypes: {
        ManyToOne: 'getMtoO',
        OneToMany: 'getOtoM'
    }
}

module.exports = popupConstants;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

let crmDataConverter = (function () {
    let convertSingleElement = function (element, entityName) {
        let title;
        try{
            title = element.DisplayName.LocalizedLabels[0].Label;
        }catch(e){
            title = undefined;
        }

        let resultElement = {
            fieldName : element.LogicalName,
            title : title ,
            type: element.AttributeType,
            entityName: entityName,
            targets: element.Targets
        };
        return resultElement;
    };

    let convertCrmDataArray = function (crmDataArray, entityName, isRequriedExecuteDmn = false) {
        let additionalData = [{
                fieldName : 'ExecuteDmn',
                title : 'ExecuteDMN' ,
                type: 'executeDmn',
                entityName: entityName
            },{
                fieldName : 'OutputValue',
                title : 'OutputValue' ,
                type: 'outputValue',
                entityName: entityName
            }
        ]

        let resultArray = crmDataArray.map(item => convertSingleElement(item, entityName))
                                    .filter(item => item.type!== 'Virtual' && 
                                                    !item.fieldName.includes('_base') &&
                                                    item.title !== undefined);

        
        resultArray.sort(function(a,b){
            if(a.title < b.title){
                return -1;
            }
            if(a.title > b.title){
                return 1;
            }

            return 0;
        });

        if(isRequriedExecuteDmn){
            resultArray = additionalData.concat(resultArray);
        }

        return resultArray;
    };

    return {
        convertCrmDataArray : convertCrmDataArray
    }
})();

module.exports = crmDataConverter;


/***/ }),
/* 3 */
/***/ (function(module, exports) {


let obj = {
    rowNumberColumn: 0,
    destroyColumn: 1,
    inputColumn: 2,
    outputColumn: 3,
    annotationColumn: 4
}

module.exports = obj;

/***/ }),
/* 4 */
/***/ (function(module, exports) {


let obj = {
    NToOne: 'N:1',
    OneToN: '1:N',
    NoRelation: 'NoRelation'
};

module.exports = obj;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

function insertToTextarea($textarea, insertedString) {
    let start = $textarea.prop("selectionStart");
    let value = $textarea.val();

    let output = insert(value, insertedString, start);

    $textarea.val(output);
}

function insert(baseString, insertedString, position) {
    let output = [
        baseString.slice(0, position),
        insertedString,
        baseString.slice(position)
    ].join('');

    return output;
}

let tools = {
    insertToTextarea: insertToTextarea
};

module.exports = tools;


/***/ }),
/* 6 */
/***/ (function(module, exports) {

let bracketConfig = {
    logical:{
        openBracket:'<#',
        closeBracket:'#>'
    },
    title:{
        openBracket:'</br index=',
        closeBracket:'>'
    }
}

module.exports = bracketConfig;

/***/ }),
/* 7 */
/***/ (function(module, exports) {

let alertFunction = function (message) {
    if(parent.Alert){
        parent.Alert.show(message, null, null, "ERROR", 500, 200);
    }else{
        alert(message);
    }
};

module.exports = alertFunction;

/***/ }),
/* 8 */
/***/ (function(module, exports) {

function insertToTextarea($textarea, insertedString) {
    let start = $textarea.prop("selectionStart");
    let value = $textarea.val();

    let output = insert(value, insertedString, start);

    $textarea.val(output);
}

function insert(baseString, insertedString, position) {
    let output = [
        baseString.slice(0, position),
        insertedString,
        baseString.slice(position)
    ].join('');

    return output;
}

module.exports = insertToTextarea;


/***/ }),
/* 9 */
/***/ (function(module, exports) {

let config = {
    treeViewSelectorId:'#treeview'
};

module.exports = config;

/***/ }),
/* 10 */
/***/ (function(module, exports) {

 function getDisplayName(item) {
    let label = item.DisplayName.UserLocalizedLabel ?
        item.DisplayName.UserLocalizedLabel.Label :
        item.DisplayName.LocalizedLabels ? item.DisplayName.LocalizedLabels.Label : item.LogicalName;

    return label;
}

module.exports = getDisplayName;

/***/ }),
/* 11 */
/***/ (function(module, exports) {

let config = {
    pluralEntityName:'ddsm_dmnconfigentities',
    treeviewIdSelector:'#treeview',
    entityConfigFormName:'ddsm_entityconfigtype',
    entityDropdownCase: [
            //{ title: "Custom Entity", value: 'custom' },
            { title: "Entity", value: 'entity' }
        ],
    formTargetEntityName:'ddsm_targetentityname'
};

module.exports = config;

/***/ }),
/* 12 */
/***/ (function(module, exports) {

//We are making dropped object in object, that validated in the grid dataSource 
let createGridObject = function (droppedObject) {
    let newFieldName;
    let newTitle;
    if (droppedObject.logicalParent) {
        newFieldName = droppedObject.logicalParent.replace(/\./g, '>') + '>' + droppedObject.logicalName;
    } else {
        newFieldName = droppedObject.logicalName;
    }
    if (droppedObject.displayParent) {
        newTitle = droppedObject.displayParent + '.' + droppedObject.displayName;
    } else {
        newTitle = droppedObject.displayName;
    }
    let entityName = droppedObject.logicalParent;
    let type = droppedObject.type;

    let newObject = {
        fieldName: newFieldName,
        title: newTitle,
        type,
        entityName
    };

    return newObject;
}
module.exports = createGridObject;

/***/ }),
/* 13 */
/***/ (function(module, exports) {


let stylize = function(selector){
    $(selector).css('white-space', 'normal')
                .css('vertical-align', 'top');
                
};

module.exports = stylize;

/***/ }),
/* 14 */
/***/ (function(module, exports) {

function filter(dataSource, query) {
    var hasVisibleChildren = false;
    var data = dataSource instanceof kendo.data.HierarchicalDataSource && dataSource.data();

    let visibleDataArray = [];

    for (var i = 0; i < data.length; i++) {
        var item = data[i];
        var text = item.text.toLowerCase();
        var itemVisible =
            query === true // parent already matches
            || query === "" // query is empty
            || text.indexOf(query) >= 0; // item text matches query

        var anyVisibleChildren = filter(item.children, itemVisible || query); // pass true if parent matches

        hasVisibleChildren = hasVisibleChildren || anyVisibleChildren || itemVisible;

        item.hidden = !itemVisible && !anyVisibleChildren;
        if (!item.hidden) {
            let elementFromArray = visibleDataArray.find(el => el === item.logicalName);
            if (elementFromArray) {
                item.hidden = true;
            } else {
                visibleDataArray.push(item.logicalName);
            }
        }
    }

    if (data) {
        // re-apply filter on children
        dataSource.filter({ field: "hidden", operator: "neq", value: true });
    }

    return hasVisibleChildren;
}

module.exports = filter;

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

let filter = __webpack_require__(14);

///Settings properties : text selector, treeview li selector, button selector, treeViewLiSelector, spinnerContainer, treeViewContainer,
///draggableRequried
let setSearchEvent = function (settings) {
    let _addDraggableToLiElements = function () {
        $('#treeview li:has(ul)').addClass('lookup-li');
        $('#treeview li').not('.lookup-li').kendoDraggable({
            hint: function (element) {
                return element.clone();
            },
            dragstart: function (e) {
                e.currentTarget.hide();
            },
            dragend: function (e) {
                e.currentTarget.show();
            }
        });
    };

    let setInputElementsDisabled = function (buttonId, textId, value) {
        $(buttonId).prop('disabled', value);
        $(textId).prop('disabled', value);
    };

    let searchEvent = function () {
        let inputText = $(settings.textSelector).val().toLowerCase();
        var dataSource = $("#treeview").data("kendoTreeView").dataSource;

        filter(dataSource, inputText);
        if(settings.draggableRequried){
            _addDraggableToLiElements();
        }
    };

    $(settings.buttonSelector).off().click(searchEvent);

    $(settings.textSelector).off().keypress(function (e) {
        if (e.which == 13) {
            searchEvent();

        }
    });
};

module.exports = setSearchEvent;


/***/ }),
/* 16 */
/***/ (function(module, exports) {

class BaseConverter{
  getContentBetweenBrackets(expression){
    let indexOfBracket = expression.indexOf('(');
    let indexOfClosedBracket = expression.indexOf(')', expression.length -2);
    let expressionClosingBracketPosition =indexOfClosedBracket=== -1?expression.length: expression.length - 1;

    let contentBetweenBrackets = expression.substring(indexOfBracket + 1, expressionClosingBracketPosition);
    return contentBetweenBrackets;
  }
}

module.exports  = BaseConverter;


/***/ }),
/* 17 */
/***/ (function(module, exports) {

class FunctionalSequence{
    constructor(expression, convertedExpression){
        this.functionExpression = expression;
        this.convertedFunctionExpression = convertedExpression? convertedExpression: '';
    }
}
module.exports = FunctionalSequence;

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

let FunctionalSequence = __webpack_require__(17);

class Searcher {
    constructor(methodArray) {
        if(methodArray){
            this.methodArray = methodArray;
        }else{
            this.methodArray = __webpack_require__(70);
        }
    }
    ///returns 
    ///proerty closestOperation
    ///property closestIndex
    _searchClosestMatch(startPosition, queryString) {
        let closestOperation = '';
        let closestIndex = queryString.length;

        this.methodArray.forEach(operation => {
            let currentIndex = queryString.indexOf(operation+'(', startPosition);

            if (currentIndex === -1 || closestIndex === 0  || currentIndex > closestIndex) {
                return;
            } else {               
                    closestIndex = currentIndex;
                    closestOperation = operation;             
            }
        });
        if (closestIndex === queryString.length) {
            return false;
        }
        let resultObject = {
            closestOperation,
            closestIndex
        };

        return resultObject;
    }

    ///returns 
    ///startExpressionPosition  
    ///endExpressionPosition
    _getSingleFunctionalSequence(startPosition, queryString) {
        let closestMatch = this._searchClosestMatch(startPosition, queryString);
        if (!closestMatch) {
            return;
        }
        //let closestClosedBracketIndex = queryString.indexOf(')', closestMatch.closestIndex);
        let closestClosedBracketIndex = this._getValidClosestBracket(closestMatch.closestIndex, queryString);

        let sequenceClosedBracketPosition = closestClosedBracketIndex;

        while (true) {
            let charAfterBracket = queryString.charAt(sequenceClosedBracketPosition + 1);
            if (charAfterBracket !== '.') {
                break;
            } else {
                //sequenceClosedBracketPosition = queryString.indexOf(')', sequenceClosedBracketPosition + 1);
                sequenceClosedBracketPosition = this._getValidClosestBracket(sequenceClosedBracketPosition + 1, queryString);
            }

        }
        return {
            startExpressionPosition: closestMatch.closestIndex,
            endExpressionPosition: sequenceClosedBracketPosition
        };
    }

    _getValidClosestBracket(startPosition,queryString){
        let stringLength = queryString.length;
        let bracketNumber = -1;
        let endPosition;

        for (let i = startPosition; i < stringLength; i++) {
            let char = queryString[i];
            if (char === '(') {
                bracketNumber++;
                continue;
            }
            if (char === ')' && bracketNumber > 0) {
                bracketNumber--;
                continue;
            }
            if (char === ')' && bracketNumber === 0) {
                endPosition = i;
                break;
            }
        }

        if(bracketNumber < 0){
            console.error('When searching closest bracket, imposible take the result below then 0');
            throw 'error';
        }

        if(!endPosition){
            console.error('The end bracket hasn\' t been found in the xml converting string');
            throw 'error';
        }

        return endPosition;
    }

    getFunctionalSequenceArray(queryString) {
        let startPosition = 0;
        let functionalExpressionArray = [];
        
        while (true) {
            let stringRange = this._getSingleFunctionalSequence(startPosition, queryString);
            if (!stringRange || stringRange.endExpressionPosition === -1) {
                return functionalExpressionArray;
            }

            let expression = queryString.substring(stringRange.startExpressionPosition, stringRange.endExpressionPosition + 1);
            startPosition = stringRange.endExpressionPosition;

            let functSeq = new FunctionalSequence(expression);
            functionalExpressionArray.push(functSeq);
        }
    }
}
module.exports = Searcher;

/***/ }),
/* 19 */
/***/ (function(module, exports) {

class Swapper{
    _swapSingleFunctionalSequence(queryString, functionalSequence){
        let targetString = functionalSequence.functionExpression;
        let sourceString = functionalSequence.convertedFunctionExpression;

        let swappedString = queryString.replace(targetString, sourceString);
        return swappedString;
    }

    swapFunctionalSequences(queryString, functionalSequenceArray){
        let result = queryString;
        functionalSequenceArray.forEach(item=>{
            result = this._swapSingleFunctionalSequence(result,item );
        });

        return result;
    }
}

module.exports = Swapper;

/***/ }),
/* 20 */
/***/ (function(module, exports) {

let config = {
    'uniqueidentifier': {
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        }

    },
    //numbers begin
    'integer': {
        gt: {
            displayName: 'GrateThan',
            argsNumber: 1,
            type: 'operator',
            template: '>{{arg1}}'
        },
        gte: {
            displayName: 'GrateThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '>={{arg1}}'
        },
        lt: {
            displayName: 'LessThan',
            argsNumber: 1,
            type: 'operator',
            template: '<{{arg1}}'
        },
        lte: {
            displayName: 'LessThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '<={{arg1}}'
        },
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        between: {
            displayName: 'between',
            argsNumber: 2,
            type: 'operator',
            template: '[{{arg1}}..{{arg2}}]'
        },
        in: {
            displayName: 'In',
            argsNumber: 'N',
            type: 'operator',
            template: '[{{args}}]',
            argsType: 'text'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    'decimal': {
        gt: {
            displayName: 'GrateThan',
            argsNumber: 1,
            type: 'operator',
            template: '>{{arg1}}'
        },
        gte: {
            displayName: 'GrateThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '>={{arg1}}'
        },
        lt: {
            displayName: 'LessThan',
            argsNumber: 1,
            type: 'operator',
            template: '<{{arg1}}'
        },
        lte: {
            displayName: 'LessThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '<={{arg1}}'
        },
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        between: {
            displayName: 'between',
            argsNumber: 2,
            type: 'operator',
            template: '[{{arg1}}..{{arg2}}]'
        },
        in: {
            displayName: 'In',
            argsNumber: 'N',
            type: 'operator',
            template: '[{{args}}]',
            argsType: 'text'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    'double': {
        gt: {
            displayName: 'GrateThan',
            argsNumber: 1,
            type: 'operator',
            template: '>{{arg1}}'
        },
        gte: {
            displayName: 'GrateThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '>={{arg1}}'
        },
        lt: {
            displayName: 'LessThan',
            argsNumber: 1,
            type: 'operator',
            template: '<{{arg1}}'
        },
        lte: {
            displayName: 'LessThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '<={{arg1}}'
        },
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        between: {
            displayName: 'between',
            argsNumber: 2,
            type: 'operator',
            template: '[{{arg1}}..{{arg2}}]'
        },
        in: {
            displayName: 'In',
            argsNumber: 'N',
            type: 'operator',
            template: '[{{args}}]',
            argsType: 'text'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    'money': {
        gt: {
            displayName: 'GrateThan',
            argsNumber: 1,
            type: 'operator',
            template: '>{{arg1}}'
        },
        gte: {
            displayName: 'GrateThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '>={{arg1}}'
        },
        lt: {
            displayName: 'LessThan',
            argsNumber: 1,
            type: 'operator',
            template: '<{{arg1}}'
        },
        lte: {
            displayName: 'LessThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '<={{arg1}}'
        },
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        between: {
            displayName: 'between',
            argsNumber: 2,
            type: 'operator',
            template: '[{{arg1}}..{{arg2}}]'
        },
        in: {
            displayName: 'In',
            argsNumber: 'N',
            type: 'operator',
            template: '[{{args}}]',
            argsType: 'text'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    //numbers end
    'string': {
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        length: {
            displayName: 'Length',
            argsNumber: 1,
            type: 'function',
            template: 'length({{arg1}})'
        },
        contains: {
            displayName: 'Contains',
            argsNumber: 1,
            type: 'function',
            template: 'contains({{arg1}})'
        },
        not_contains: {
            displayName: 'Not Contains',
            argsNumber: 1,
            type: 'function',
            template: 'not_contains({{arg1}})'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        empty: {
            displayName: 'IsEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'IsEmpty()'
        }
    },
    'boolean': {
        true: {
            displayName: 'true',
            argsNumber: 0,
            type: 'bool',
            template: 'true'
        },
        false: {
            displayName: 'false',
            argsNumber: 0,
            type: 'bool',
            template: 'false'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    'datetime': {
        'gt': {
            displayName: 'GrateThan',
            argsNumber: 1,
            type: 'operator',
            argsType: 'date',
            template: '>{{arg1}}'
        },
        'lt': {
            displayName: 'LessThan',
            argsNumber: 1,
            type: 'operator',
            argsType: 'date',
            template: '<{{arg1}}'
        },
        'equals': {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            argsType: 'date',
            template: 'Equal({{arg1}})'
        },
        'between': {
            displayName: 'between',
            type: 'operator',
            argsType: 'date',
            argsNumber: 2,
            template: '[{{arg1}}..{{arg2}}]'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    //numbers end
    'picklist': {
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        length: {
            displayName: 'Length',
            argsNumber: 1,
            type: 'function',
            template: 'length({{arg1}})'
        },
        contains: {
            displayName: 'Contains',
            argsNumber: 1,
            type: 'function',
            template: 'contains({{arg1}})'
        },
        not_contains: {
            displayName: 'Not Contains',
            argsNumber: 1,
            type: 'function',
            template: 'not_contains({{arg1}})'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    'memo': {
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },       
        contains: {
            displayName: 'Contains',
            argsNumber: 1,
            type: 'function',
            template: 'contains({{arg1}})'
        },
        not_contains: {
            displayName: 'Not Contains',
            argsNumber: 1,
            type: 'function',
            template: 'not_contains({{arg1}})'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        empty: {
            displayName: 'IsEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'IsEmpty()'
        }
    }
}

module.exports = config;


/***/ }),
/* 21 */
/***/ (function(module, exports) {

let conf = {
    custom: {
        '>': 'gt',
        '>=': 'gte',
        '<': 'lt',
        '<=': 'lte',
        '=': 'equals',
        '!=': 'not_equals',
        'in': 'in',
        'between': 'between',
        'contains': 'contains',
        'not_contains': 'not_contains',
    },
    integer: ['integer', '>', '>=', '<', '<=', '==', '!='],
    decimal: ['decimal', '>', '>=', '<', '<=', '==', '!='],
    double: ['double', '>', '>=', '<', '<=', '==', '!='],
    money: ['money', '>', '>=', '<', '<=', '==', '!='],
    string: ['string', '>', '>=', '<', '<=', '==', '!='],
    boolean: ['boolean', '==', '!='],
    datetime: ['datetime', '>', '>=', '<', '<=', '==', '!=']
}

module.exports = conf;

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

let configForIndexesOfColumns = __webpack_require__(3);

let createTable = function (id, entityDropdownData) {
    //ready begin
    kendo.ui.progress($('#main_container'), true);

    let array;
    let dataContainerSelector = '#data_container';

    let dataGetter = __webpack_require__(35);
    let dataConverter = __webpack_require__(2);
    let converter = __webpack_require__(31);
    let gridBuilder = __webpack_require__(60);
    __webpack_require__(61)('#tableKey');
    let treeViewService = __webpack_require__(85);

    function changeColumns(cols) {
        let newCols = $.extend(true, {}, cols);

        if (newCols.columnsArray.length === 4) {
            let rowNumberColumn = {
                title: "&nbsp;",
                template: "<span class='row-number'></span>",
                width: 30
            }

            newCols.columnsArray.unshift(rowNumberColumn);
        }

        newCols.columnsArray.forEach(item => {
            if (item.title && (item.title === 'Input' || item.title === 'Output')) {
                item.columns.forEach(col => {
                    col.width = 200;
                });
            }
            else if (item.field && item.field === 'Annotation') {
                item.width = 200;
            }
        });

        return newCols;
    }
    gridBuilder.resetGrid('#grid');
    dataGetter(id, function (item) {

        console.log(item);
        
        if (id) {
            let entityName = item.LogicalName;

            treeViewService.setId('#treeview');
            treeViewService.initTree();
            treeViewService.createInitTreeForLogicalName(entityName);
        }


        var columns = window.parent.Xrm.Page.getAttribute('ddsm_jsonheaderschema').getValue();
        var datarows = window.parent.Xrm.Page.getAttribute('ddsm_jsontabledata').getValue();
        if (columns) {
            let cols = window.parent.JSON.parse(columns);

            cols = changeColumns(cols);

            let data = window.parent.JSON.parse(datarows);
            gridBuilder.buildGridFromFiles('#grid', cols.columnsArray, data.dataSource, cols.objectState, data.dataContainer);
        } else {
            gridBuilder.createGrid('#grid');
        }
        (__webpack_require__(13))('#grid th');
        gridBuilder.setMetadata(array);

        let getConvertedObjectFromGrid = function () {
            let columns = gridBuilder.getColumnArray();
            let input = columns[configForIndexesOfColumns.inputColumn].columns;
            let output = columns[configForIndexesOfColumns.outputColumn].columns;

            let recordArray = $("#grid").data("kendoGrid").dataItems();
            let policyValue = $('#tableKey').data("kendoDropDownList").text();
            let containerArray = gridBuilder.getContainerData();
            let resultArray = converter(input, output, recordArray, containerArray, policyValue);


            let stringified = JSON.stringify(resultArray);
            return stringified;
        };
        window.top.getColumns = function () {

            return gridBuilder.getColumnsState();
        };
        window.top.getDataContainerAndDataSourceJSON = function () {

            let dataSource = $("#grid").data("kendoGrid").dataItems();
            let dataContainer = gridBuilder.getContainerData();

            let objectToJson = {
                dataContainer,
                dataSource
            };
            let json = JSON.stringify(objectToJson);
            return json;
        }

        window.top.gridBuilder = gridBuilder;

        window.top.getCurrentColumns = function () {
            let $grid = $('#grid').data('kendoGrid');
            let currentColumns = {
                inputs: $grid.columns[2].columns,
                outputs: $grid.columns[3].columns
            };

            return currentColumns;
        };


        let parserClickHandler = function () {
            let x2js = new X2JS();
            try {
                let obj = getConvertedObjectFromGrid();
                let xml = x2js.json2xml_str($.parseJSON(obj));
                xml = xml.replace(/&quot;/g,'"')
                    .replace(/&apos;/g, "'");
                parent.Xrm.Page.getAttribute("ddsm_dmnrule").setValue(xml);
            } catch (e) {
                console.error(e);
            }
        };

        window.top.parserClickHandler = parserClickHandler;

        
        //Create splitter ----------
        $('#search-div').css('visibility','visible');
        (__webpack_require__(63))('#splitter');
        //--------------------------

        //Remove upper-row hidding cause of splitter creating 
        $('.upper-row').css('padding-top','10px');
        entityDropdownData.enable(true);
    });
};
module.exports = createTable;


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

let readyFunc = function () {
    console.log('bundle');
    let createDropdown = __webpack_require__(40);
    var baseUrl = "/api/data/v8.1/";
    var Xrm = window.parent.Xrm;
    var clientUrl = Xrm.Page.context.getClientUrl();
    $tipeDropdown = $('#entity-type-dropdown');


    $('#search-div').css('visibility','hidden');
    window.top.lockdropDown = function (animateHide = false) {
        $("#entities").data("kendoDropDownList").enable(false);
        
        if(!animateHide){
            $tipeDropdown.css('visibility', 'hidden');
        }else{
            $tipeDropdown.fadeOut(500);
        }
    };
    var isFiltered = false;
    createDropdown('#custom-entities','#entities');
};

module.exports = readyFunc;


/***/ }),
/* 24 */
/***/ (function(module, exports) {

class XmlInputConverter{
    _createXmlString(operator, args){
        let xmlStringBegin = `$${operator}^`;
        let length = args.length;
        let lastElement = length - 1;

        for(let i = 0; i < length; i++){
            xmlStringBegin +=args[i];
            if(i !== lastElement){
                xmlStringBegin +=',';
            }
        }

        return xmlStringBegin;
    }

    convertObject(targetObject){
        let operator=  targetObject.operator;
        let convertedString = this._createXmlString(operator, targetObject.args);

        return convertedString;

    }
}

module.exports = XmlInputConverter;

/***/ }),
/* 25 */
/***/ (function(module, exports) {

let checkUnknowMethodNames = function(targetString, methodArray){
    let regExpr = /[\s\W](\w*?)\(/g;
    let match = regExpr.exec(targetString);

    while(match != null){
        let matchedGroup = match[1];
        if(matchedGroup===''){
            continue;
        }
        let contains = methodArray.includes(matchedGroup);

        if(!contains){
            throw "The method is unknown";
        }
        match = regExpr.exec(targetString);
    }


}

module.exports = checkUnknowMethodNames;

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

let executeDmnConfig = (__webpack_require__(0))['executeDmnXmlTemplate'];

let _convertExecuteDmn = function(object){
    let businessRuleId = object.businessRuleId;
    let relationName = object.relationName;

    let functionName = executeDmnConfig.functionName;
    let funcSeparator = executeDmnConfig.functionSeparator;
    let argsSeparator = executeDmnConfig.argsSeparator;



    return `${functionName}${funcSeparator}${businessRuleId}${argsSeparator}${relationName}`;
}



let convertExecuteDmn = function(object){
    try {
        return _convertExecuteDmn(object);
    } catch (error) {
        console.log('Error with execute Dmn converter');
        console.log(error);
        throw error;
    }
}


module.exports = convertExecuteDmn;

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

let functionConverter = (function () {
    let OneToN = __webpack_require__(71);
    let NToOneXmlConverter = __webpack_require__(64);
    let config = __webpack_require__(0);
    let SingleFieldsConverter = __webpack_require__(29);
    let checkUnknowMethods = __webpack_require__(25);
    let wrapOutputValue = __webpack_require__(28);

    let globalFunctionsNames = Object.keys(config.GlobalFunctions);
    let nToOneConverter;

    let currentEntity = parent.Xrm.Page.getAttribute("ddsm_targetentity").getValue();
    let oneToNXmlConverter = new OneToN(currentEntity);

    let _convert = function (object) {
        let objectType = object.datatype;
        let singleFieldsConverter =  SingleFieldsConverter(objectType);

        if (object.relation === 'N:1') {
            let nToOneCaseConfig = config['N:1'][objectType];

            nToOneConverter = new NToOneXmlConverter(nToOneCaseConfig);
            let convertedString = nToOneConverter.convertString(object.expression.value);
            let singleFieldsConverted = singleFieldsConverter(convertedString);

            if(objectType === "outputvalue"){   
                singleFieldsConverted = wrapOutputValue(singleFieldsConverted);
            }

            return singleFieldsConverted;

        } else if (object.relation === '1:N') {

            let convertedString = oneToNXmlConverter.convertStringToXml(object.expression.value);
            let singleFieldsConverted = singleFieldsConverter(convertedString);    

            if(objectType === "outputvalue"){   
                singleFieldsConverted = wrapOutputValue(singleFieldsConverted);
            }

            return singleFieldsConverted;

        } else if (object.relation === 'NoRelation') {
            let nToOneCaseConfig = config['N:1'][objectType];
            let oneToNCaseConfig = config['1:N'];

            nToOneConverter = new NToOneXmlConverter(nToOneCaseConfig);
            let convertedNToOneString = nToOneConverter.convertString(object.expression.value);

            let string = oneToNXmlConverter.convertStringToXml(convertedNToOneString);

            let singleFieldsConverted = singleFieldsConverter(string);  

            if(objectType === "outputvalue"){   
                singleFieldsConverted = wrapOutputValue(singleFieldsConverted);
            }
            
            return singleFieldsConverted;
        }
    };

    let convert = function (object) {
        try {
            return _convert(object);
        } catch (e) {
            console.log(e);
            console.log(e.message);

            throw 'errored message';
        }
    }

    return convert;
})();

module.exports = functionConverter;

/***/ }),
/* 28 */
/***/ (function(module, exports) {

module.exports = function (string) {
    const position = 2;
    let result = string.substr(0, position)
        + 'OutputValue^' 
        + string.substr(position);

    return result;
}

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

let SingleFieldConverter = (function(type) {

        let config = __webpack_require__(0);
        let nToOneCaseConfig = config['N:1'][type];
        let oneToNCaseConfig = config['1:N'];
        let globalFunctions = config['GlobalFunctions'];

    

    let _isNotContainsFromConfig = function(item) {
        
        let _isContainsOperation = function (configObject, expressionString) {
            let keyArray = Object.keys(configObject);
            let isContains = false;

            for (let i = 0, length = keyArray.length; i < length; i++) {

                let searchedSubString = keyArray[i];
                if (expressionString.indexOf(searchedSubString + '^') !== -1) {
                    isContains = true;
                    break;
                }
            }
            return isContains;
        }
        let nToOneContains = _isContainsOperation(oneToNCaseConfig, item);
        let OneToNContains;
        if (!nToOneContains) {
            OneToNContains = _isContainsOperation(nToOneCaseConfig, item);
        }

        return !(nToOneContains || OneToNContains);
    }
    let distinct = function(stringArray){
        let length = stringArray.length;
        let resultArray=  [];
        for(let i = 0; i < length; i++){
            let trimed = stringArray[i].trim();
            if(resultArray.includes(trimed)){
                continue; 
            }else{
                resultArray.push(trimed);
            }
        }

        return resultArray;
    }
    let _isNotNumber = function(item) {
        let converted = +(item.trim())[0];
        return isNaN(converted);
    }
    let _filterFromBrackets = function(item) {
        let globalFunctionKeysArray = Object.keys(globalFunctions);

        globalFunctionKeysArray.forEach(functionName=>{
            let methodPart = functionName + '(';
            let positionOfMethod = item.indexOf(methodPart);

            if(positionOfMethod !== -1){
                item = item.replace(methodPart, '');
            };
        });
        let result = item.replace(/\(|\)/g, '');
        return result.trim();
    }
    let _wrapByBrackets = function(stringArray, targetString) {
        
        let result = targetString;
        stringArray.forEach(item => {

            let wrappedString = `{{${item}}}`;

            let searchPosition = 0; 
            while(true){
                let position = result.indexOf(item, searchPosition);
                if(position === -1){
                    break;
                }
                let charBehind;
                let backing = 1;
                while(true){
                    charBehind = result.charAt(position - backing);
                    if(charBehind !== ' '){
                        break;
                    }
                    backing+= 1;
                }

                if(['{', '^', ','].includes(charBehind)){
                    searchPosition = position + item.length;
                    continue;
                }

                let beginString = result.substr(0, position);
                let endString = result.substr(position + item.length);

                let newString = `${beginString}{{${item}}}${endString}`;
                result = newString;
                searchPosition = position + item.length;
            }
        });

        return result;
    }
    let _getArrays = function(string) {
        let operationsArray = ['+', '-', '*', '/', ','];
        let resultArray = [];
        let localSplit = function (arrayOfStrings, operator) {
            let resultArray = [];
            arrayOfStrings.forEach(item => {
                let result = item.split(operator);
                resultArray = resultArray.concat(result);
            });

            return resultArray;
        };

        let firstArray = string.split(operationsArray[0]);

        let loopLength = operationsArray.length;
        for (var i = 1; i < loopLength; i++) {
            firstArray = localSplit(firstArray, operationsArray[i]);
        };

        return firstArray;

    }
    let convertSingle = function(target) {
        let arrayOfStrings = _getArrays(target);
        let indexOfSetValue = target.indexOf('{{setValue^');
        if(indexOfSetValue !== -1){
            return target;
        }

        let filteredArrayFromConvertedConfig = arrayOfStrings.filter(_isNotContainsFromConfig);
        let filteredArrayFromNumbers = filteredArrayFromConvertedConfig.filter(_isNotNumber);
        let filteredFromBrackets = filteredArrayFromNumbers.map(_filterFromBrackets);
        let distinctArray=  distinct(filteredFromBrackets);

        let resultString = _wrapByBrackets(distinctArray, target);
        return resultString;
    }

    return convertSingle;

});

module.exports = SingleFieldConverter;



/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

let converter = (function () {

    let XmlInputConverter = __webpack_require__(24);

    let functionConverter = __webpack_require__(27);

    let executeDmn = __webpack_require__(26);

    let converter = new XmlInputConverter();

    return function (object, type) {
        type = type.toLowerCase();

        try {
            if (type === 'function') {
                return functionConverter(object);
            } else if (type === 'executedmn'){
                return executeDmn(object);
            }else {
                return converter.convertObject(object);
            }

        } catch (e) {
            console.log(e.message);
            console.log('Wrong converting');

            throw 'error';
        }
    };
})();


module.exports = converter;   

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

let convertor = (function () {
    let inputConverter = __webpack_require__(32);
    let outputConverter = __webpack_require__(33);
    let rowsConverter = __webpack_require__(34);

    let _dataArray;

    return function (inputColumns, outputColumns, dataRows, dataArray, policyValue) {
        try{
 
            let inputs = inputConverter(inputColumns, dataArray);
            let outputs = outputConverter(outputColumns, dataArray);
            let rules = rowsConverter(inputColumns, outputColumns,dataRows, dataArray);

            let decisionTable = {
                input: inputs,
                output: outputs,
                rule: rules,
                _id: 'decisionTable',
                _hitPolicy: policyValue
            };

            let decision = {
                decisionTable,
                _id: 'decision',
                _name: 'test'
            };

            let definitions = {
                decision,
                _xmlns: "http://www.omg.org/spec/DMN/20151101/dmn.xsd",
                _id: 'definitions',
                _name: 'definitions',
                _namespace: "http://camunda.org/schema/1.0/dmn"
            };

            let result = {
                definitions
            };

            return result;

        }catch(e){
            console.log(e);
            console.error('bad dmn-convertor'); 
            throw false;
        }
    };
})();

module.exports = convertor;

/***/ }),
/* 32 */
/***/ (function(module, exports) {

let inputColumnCreator = (function () {
    let columns = [];
    let idCounter = 0;
    let inputExpressionID = 0;

    let _createId = function () {
        let inputIdValue = 'input' + idCounter++;
        return inputIdValue;
    };

    let _createLabel = function (columnLabel) {
        return columnLabel;
    };

    let _createInputExpression = function (type) {
        let inputExpression = {
            text : '',
            _id : 'inputExpression'+ inputExpressionID++,
            _typeRef : type
        };

        return inputExpression;
    };

    let _createSingleInputObject = function (column, dataArray) {
        let indexOfType = column.field.indexOf('_Input');

        if(indexOfType === -1){
            throw 'Field doesn\'t contain column input or output type';
        }

        let columnField = column.field.substring(0,indexOfType);
        let findedElement = dataArray.find(e => e.fieldName === columnField);
  
        let label = columnField.replace(/>/g, '.');

        let input = {
            _label: _createLabel(label),
            _id: _createId(),
            inputExpression: _createInputExpression(findedElement.type)
        };

        return input;
    };

    let createInputObjects = function(columns,dataArray){
        try {
            let inputArray = columns.map(c=>_createSingleInputObject(c, dataArray) );
            return inputArray;
        }catch(e){
            console.log(e);
            console.log('Error with inputColumn');
        }
    };

    return createInputObjects;
})();

module.exports = inputColumnCreator;

/***/ }),
/* 33 */
/***/ (function(module, exports) {

let outputColumnCreator = (function () {
    let columns = [];
    let idCounter = 0;
    let inputExpressionID = 0;

    let _createId = function () {
        let inputIdValue = 'output' + idCounter++;
        return inputIdValue;
    };

    let _createLabel = function (columnLabel) {
        return columnLabel;
    };

    let _createSingleOutputObject = function (column, dataArray) {
       
        let columnfield = column.field.replace(/_Output/g, '');

        let findedElement = dataArray.find(e => e.fieldName == columnfield);
        let type = findedElement.type;
        let label;
        if(type === 'executeDmn'){
            label = type;
        }else{
            label =  columnfield.replace(/>/g, '.');
        }            
        let output = {
            _label: _createLabel(label),
            _id: _createId(),
            _name: '',
            _typeRef: type
        };

        return output;
    };

    let createOutputObjects = function(columns, dataArray){
        try {
            let outputArray = columns.map(c=>_createSingleOutputObject(c, dataArray) );
            return outputArray;
        }catch(e){
            console.log(e);
            console.log('Error with outputColumn');
        }
    };

    return createOutputObjects;
})();

module.exports = outputColumnCreator;

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

let ruleCreator = (function () {
    let converter = __webpack_require__(30);
    let alertFunction = __webpack_require__(7);
    let inputColumns;
    let outputColumns;

    let data = [];

    let unaryTestCounter = 0;
    let rowCounter = 0;


    let _setInput = function (inputCols) {
        let filteredArray = inputCols.map(c => c.field);
        inputColumns = filteredArray;
    };
    let _setOutput = function (outputCols) {
        let filteredArray = outputCols.map(c => c.field);
        outputColumns = filteredArray;
    };

    let _createUnaryTestCounter = function () {
        let unaryTests = 'UnaryTests_' + unaryTestCounter++;
        return unaryTests;
    };
    let _createRowCounter = function () {
        let row = 'row-' + rowCounter++;
        return row;
    };

    let _createEntries = function (sourceColumns, dataRow, dataArray, isInput) {

        let entryArray = [];
        let fieldDescription = 'Unknow field';

        try {
            sourceColumns.forEach(inputField => {
                let inputFieldWithoutPrefix;

                if(isInput){
                    let indexOfType = inputField.indexOf('_Input');

                    if (indexOfType === -1) {
                        throw 'Field doesn\'t contain column input or output type';
                    }
                    inputFieldWithoutPrefix = inputField.substring(0, indexOfType);

                }else{
                    inputFieldWithoutPrefix = inputField.replace(/_Output/g, '');
                }
               
                let findedElement = dataArray.find(el => el.fieldName === inputFieldWithoutPrefix);


                let elementType = findedElement ? findedElement.type : 'string';
                fieldDescription = findedElement.title;

                if (!isInput && elementType !== 'executeDmn') {
                    elementType = 'function';
                }
                let property;
                if (isInput) {
                    property = dataRow[inputField];
                } else {
                    property = dataRow[inputField];
                }

                if (!property) {
                    let entry = {
                        _id: _createUnaryTestCounter(),
                        text: ""
                    };

                    entryArray.push(entry);
                } else {
                    let entry = {
                        _id: _createUnaryTestCounter(),
                        text: converter(property, elementType)
                    };

                    entryArray.push(entry);
                }
            });

            return entryArray;
        } catch (e) {
            console.error('Iteration rules error');
            throw { columnName: fieldDescription };
        }
    };

    let _createOutputEntries = function (dataRow, dataArray) {
        let array = _createEntries(outputColumns, dataRow, dataArray, false);
        return array;
    };

    let _createInputEntries = function (dataRow, dataArray) {
        let array = _createEntries(inputColumns, dataRow, dataArray, true);
        return array;
    };

    let _createRule = function (dataRow, dataArray, index) {
        let currentIndex = index + 1;
        try {
            let inputEnt = _createInputEntries(dataRow, dataArray);
            let outputEnt = _createOutputEntries(dataRow, dataArray);
            let rule;


            rule = {
                inputEntry: inputEnt,
                outputEntry: outputEnt,
                _id: _createRowCounter()
            };

            return rule;
        } catch (e) {
            console.error('Cant create rule Object');
            alertFunction(`Rule number:${currentIndex} Field: ${e.columnName} `);
            throw "Cant create rule Object";
        }

    };


    return function (inputColumns, outputColumns, dataRows, dataArray) {
        try {
            _setOutput(outputColumns);
            _setInput(inputColumns);

            let rules = dataRows.map((r, index) => _createRule(r, dataArray, index));

            return rules;


        } catch (e) {
            console.log(e.message);
            console.log('bad rule convert');
            throw e;
        }
    }

})();

module.exports = ruleCreator;

/***/ }),
/* 35 */
/***/ (function(module, exports) {

let crmGetter = (function () {
    let _baseUrl = parent.Xrm.Page.context.getClientUrl();
    let getData = function (id, callback) {
        if(!id){
            callback();
        }else{
            $.ajax({
            type: 'GET',
            url: _baseUrl + `/api/data/v8.1/EntityDefinitions(${id})?$select=LogicalName`,
            headers: {
                'Accept': 'application/json',
                "Content-Type": "application/json; charset=utf-8",
                'OData-MaxVersion': "4.0",
                "OData-Version": "4.0"
            }
        }).done(callback);
        }
        
    }

    return getData;

})();

module.exports = crmGetter;


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

let alertFunction = __webpack_require__(7);

const bagging = 'Please, add column';

let singleColumnMessage = (columnName) => `${columnName} has no column.`;

const messageCase = {
    inputOnly: `${singleColumnMessage('Input')} ${bagging}`,
    outputOnly: `${singleColumnMessage('Output')} ${bagging}`,
    both: `Input and output have no columns. Please, add columns.`
}


let unvalidColumnsMessage = function (columnsState) {
    inputIsUnvalid = columnsState.inputIsUnvalid;
    outputIsUnvalid = columnsState.outputIsUnvalid;

    if (inputIsUnvalid && outputIsUnvalid) {
        alertFunction(messageCase.both);
        return;
    }
    if (inputIsUnvalid && !outputIsUnvalid) {
        alertFunction(messageCase.inputOnly);
        return;
    }
    if (!inputIsUnvalid && outputIsUnvalid) {
        alertFunction(messageCase.outputOnly);
        return;
    }

    throw 'Columns are valid but it is still triggered as error';

}

module.exports = unvalidColumnsMessage;

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

let treeViewBuilder = (function () {

    let _converter = __webpack_require__(2);
    let searchEvent = __webpack_require__(15);
    let _baseUrl = parent.Xrm.Page.context.getClientUrl();

    let _addDraggableToLiElements = function () {
        $('#treeview li:has(ul)')
            .addClass('lookup-li')
            .off();

        $('#treeview').find('li').not('.lookup-li').not('[data-role=draggable]').kendoDraggable({
            hint: function (element) {
                return element.clone();
            },
            dragstart: function (e) {
                e.currentTarget.hide();
            },
            dragend: function (e) {
                e.currentTarget.show();
            }
        });
    };

    let appendTreeViewData = function (selector, dataArray) {
        let $treeView = $(selector).data('kendoTreeView');
        dataArray.forEach(attribute => {

            if (attribute.type === 'Lookup') {

                let html = $treeView.append({
                    text: attribute.title,
                    logicalName: attribute.fieldName,
                    displayName: attribute.title,
                    type: attribute.type,
                    target: attribute.target,
                    items: [
                        { text: 'Loading...' }
                    ]
                });
                $(html).closest('li').addClass('lookup-li');

            } else {
                let html = $treeView.append({
                    text: attribute.title,
                    logicalName: attribute.fieldName,
                    displayName: attribute.title,
                    type: attribute.type
                });

            }
        });

        searchEvent({
            buttonSelector: '#search-button',
            textSelector: '#search-text-field',
            treeViewLiSelector: '#treeview li',
            spinnerContainer: "#loader",
            treeViewContainer: "#treeview",
            draggableRequried: true

        });
        _addDraggableToLiElements();
        kendo.ui.progress($('#main_container'), false);

    }

    let initTree = function (selector, eventObj) {

        let expandFunction = function (e) {

            let node = e.node;
            let length = $(node).find('li').length;
            if (length <= 1) {
                let curentObject = $(selector).data('kendoTreeView').dataItem(node);
                let target = curentObject.target;
                let linkedField = curentObject.logicalName;
                (function (name, linkedField) {
                    $.ajax({
                        type: 'GET',
                        url: _baseUrl + `/api/data/v8.1/EntityDefinitions()?$select=LogicalName,DisplayName&$expand=Attributes&$filter= LogicalName eq '${name}'`,
                        headers: {
                            'Accept': 'application/json',
                            "Content-Type": "application/json; charset=utf-8",
                            'OData-MaxVersion': "4.0",
                            "OData-Version": "4.0"
                        }
                    }).done(item => {
                        let emptyDeleted = false;

                        try {
                            displayEntityName = item.value[0].DisplayName.LocalizedLabels[0].Label;
                        } catch (e) {
                            displayEntityName = logicalName;
                        }

                        let _treeView = $(selector).data("kendoTreeView");
                        let recievedAttributes = item.value[0].Attributes;
                        let logicalName = item.value[0].LogicalName;
                        let convertedArray = _converter.convertCrmDataArray(recievedAttributes, item.value[0].LogicalName);

                        convertedArray.forEach(attribute => {
                            let logicalParent, displayParent, entityName;
                            if (!curentObject.logicalParent && !curentObject.displayParent) {
                                logicalParent = linkedField;
                                displayParent = displayEntityName;

                            } else {
                                logicalParent = `${curentObject.logicalParent}.${linkedField}`;
                                displayParent = `${curentObject.displayParent}.${displayEntityName}`;
                            }

                            if (attribute.type === 'Lookup') {
                                let html = _treeView.append({
                                    text: attribute.title,
                                    entityName: attribute.entityName,
                                    logicalName: attribute.fieldName,
                                    displayName: attribute.title,
                                    type: attribute.type,
                                    logicalParent: logicalParent,
                                    displayParent: displayParent,
                                    target: attribute.targets[0],
                                    items: [
                                        { text: 'Loading...' }
                                    ]
                                }, $(node));
                                $(html).closest('li').addClass('lookup-li');

                                if (!emptyDeleted) {
                                    let loadingPlace = _treeView.findByText('Loading...');
                                    _treeView.remove(loadingPlace);
                                    emptyDeleted = true;
                                }
                            } else {
                                let html = _treeView.append({
                                    text: attribute.title,
                                    entityName: attribute.entityName,
                                    logicalName: attribute.fieldName,
                                    displayName: attribute.title,
                                    type: attribute.type,
                                    logicalParent: logicalParent,
                                    displayParent: displayParent,
                                }, $(node));

                                if (!emptyDeleted) {
                                    let loadingPlace = _treeView.findByText('Loading...');
                                    _treeView.remove(loadingPlace);
                                    emptyDeleted = true;
                                }
                            }
                        });
                        _addDraggableToLiElements();
                    });
                })(target, linkedField);
            }
        };

        let settings = {
            animation: {
                expand: {
                    duration: 200
                },
                collapse: {
                    duration: 200
                }
            },
            expand: expandFunction
        };
        let $treeView = $(selector).data('kendoTreeView');
        if (eventObj) {
            Object.keys(eventObj).forEach(key => settings[key] = eventObj[key]);
            if($treeView){
                $treeView.destroy();
            }
            $(selector).empty().kendoTreeView(settings);
        } else {
            if($treeView){
                $treeView.destroy();
            }
            $(selector).empty().kendoTreeView(settings);
        }
    };

    return {
        initTree,
        appendTreeViewData
    };
})();

module.exports = treeViewBuilder;


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

let createDropdownList = function (selector, entityName) {
    const config = __webpack_require__(11);
    let dataParse = __webpack_require__(42);
    let treeViewCreator = __webpack_require__(37);
    //$('.entity-dropdown-div').css('visibility','visible');
    $(selector).kendoDropDownList({
        filter: "startswith",
        ignoreCase: true,
        filtering: function (e) {
            if (!e.filter) {
                e.preventDefault();
            }
        },
        dataSource: {
            transport: {
                read: {
                    url: parent.Xrm.Page.context.getClientUrl() + `/api/data/v8.1/${config.pluralEntityName}?$select=ddsm_name,ddsm_json_data`,
                    dataType: "json",
                    beforeSend: function (req) {
                        req.setRequestHeader('Accept', 'application/json');
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader('OData-MaxVersion', "4.0");
                        req.setRequestHeader("OData-Version", "4.0");
                    }
                }
            },
            schema: {
                data: function (responce) {
                    let mappedArray = responce.value.map(item => {
                        let mappedObject = {
                            name: item.ddsm_name,
                            jsonData: JSON.parse(item.ddsm_json_data)
                        };

                        return mappedObject;
                    });

                    return mappedArray;
                }
            }
        },
        select: function (e) {
            let title = e.sender.dataItem(e.item)[e.sender.options.dataTextField];
            let data = e.sender.dataItem(e.item)[e.sender.options.dataValueField];
            let convertedData = dataParse(data);
            console.log(convertedData);

            treeViewCreator.initTree(config.treeviewIdSelector);
            treeViewCreator.appendTreeViewData(config.treeviewIdSelector, convertedData);

            parent.Xrm.Page.getAttribute("ddsm_targetentityname").setValue(title);
            parent.Xrm.Page.getAttribute("ddsm_targetentity").setValue(title);
            window.top.createTable();

        },
        dataBound: function () {
            $('.entity-list-div').show(500);
            if (entityName) {
                this.select(function (item) {
                    return item.name === entityName;
                });
                let data = this.dataItem().jsonData;
                let convertedData = dataParse(data);
                console.log(convertedData);

                treeViewCreator.initTree(config.treeviewIdSelector);
                treeViewCreator.appendTreeViewData(config.treeviewIdSelector, convertedData);
                window.top.createTable();
                window.top.lockdropDown();
            }
        },
        dataTextField: 'name',
        dataValueField: 'jsonData',
        optionLabel: '--',
        animation: false
    });
}

module.exports = createDropdownList;

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

let config = {
    custom: __webpack_require__(38),
    entity: __webpack_require__(41)
}

module.exports = config;

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

let createEntityTypeConfigDropdown = function (parentSelector, childSelector) {
    let dropdownChooseConfig = __webpack_require__(39);
    const constants = __webpack_require__(11);

    let formConfigType = parent.Xrm.Page.getAttribute(constants.entityConfigFormName);
    let formEntityType = parent.Xrm.Page.getAttribute(constants.formTargetEntityName);

    let data = new kendo.data.DataSource({
        data: constants.entityDropdownCase
    });

    let makeVisible = function (selector = '.entity-list-div') {
        $(selector).css('visibility', 'visible');
    };

    const defaultLabel = '--';

    $(parentSelector).kendoDropDownList({
        dataSource: data,
        dataTextField: 'title',
        dataValueField: 'value',
        optionLabel: defaultLabel,
        animation: false,
        select: function (e) {

            let value = e.sender.dataItem(e.item)[e.sender.options.dataValueField];
            let title = e.sender.dataItem(e.item)[e.sender.options.dataTextField];
            if (title === defaultLabel) {
                $('.entity-list-div').hide(500);
            } else {
                $('.entity-list-div').show(500);
                //formConfigType.setValue(title);
                //Config contains methods for creating choosen dropdown;
                if (!dropdownChooseConfig[value]) {
                    let message = 'Creating entity type config. Current value doesn\'t contained in config';
                    console.error(message);
                    throw message;
                }
                makeVisible();
                dropdownChooseConfig[value](childSelector);
            }


        },
        dataBound: function () {
            /*
            let formValue;
            let entityName = formEntityType.getValue();;
            let searchResult;
            if (formConfigType) {
                formValue = formConfigType.getValue();
                searchResult = constants.entityDropdownCase.find(item => item.title === formValue);
            }
            if (searchResult) {
                this.select(function (item) {
                    return item.title === formValue;
                });
                let value = this.value();

                makeVisible();
                dropdownChooseConfig[value](childSelector, entityName);
            } else if (entityName) {*/
                this.select(item=>{
                    return item.value === 'entity';
                });
                makeVisible();
                dropdownChooseConfig['entity'](childSelector);
                
                $('#entity-type-dropdown').hide();
            // } else {
            //     kendo.ui.progress($('#main_container'), false);
            // }

        }
    });


}

module.exports = createEntityTypeConfigDropdown;

/***/ }),
/* 41 */
/***/ (function(module, exports) {

let createDropdown = function (selector) {
    //$('.entity-dropdown-div').css('visibility','visible');
    kendo.ui.progress($('#main_container'), true);
    $(selector).kendoDropDownList({
        filter: "startswith",
        ignoreCase: true,
        filtering: function (e) {
            if (!e.filter) {
                e.preventDefault();
            }
        },
        dataSource: {
            serverFiltering: false,
            schema: {
                data: function (response) {
                    var value = response.value;
                    var model = value.map(function (el) {
                        var displayName = "";
                        var id = el.MetadataId;
                        var logicalName = el.SchemaName.toLowerCase();
                        if (el.DisplayName.LocalizedLabels[0] && el.DisplayName.LocalizedLabels[0].Label) {
                            displayName = el.DisplayName.LocalizedLabels[0].Label;
                        } else if (el.DisplayName.UserLocalizedLabel && el.DisplayName.UserLocalizedLabel.Label) {
                            displayName = el.DisplayName.UserLocalizedLabel.Label;
                        } else {
                            displayName = logicalName;
                        }
                        return {
                            label: displayName,
                            logicalName: logicalName,
                            id: id
                        };
                    })
                        .sort(function (a, b) {
                            if (a.label < b.label) {
                                return -1;
                            }
                            if (a.label > b.label) {
                                return 1;
                            }
                            return 0;
                        });
                    return model;
                }
            },
            transport: {
                read: {
                    url: parent.Xrm.Page.context.getClientUrl() + '/api/data/v8.1/EntityDefinitions?$select=DisplayName,SchemaName,MetadataId',
                    dataType: "json",
                    beforeSend: function (req) {
                        req.setRequestHeader('Accept', 'application/json');
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader('OData-MaxVersion', "4.0");
                        req.setRequestHeader("OData-Version", "4.0");
                    }
                }
            }
        },
        dataTextField: "label",
        dataValueField: "logicalName",
        optionLabel: 'Select an entity..',
        select: function (e) {
            kendo.ui.progress($('#main_container'), true);
            var dataItem = this.dataItem(e.item);
            this.enable(false);
            var $grid = $('#grid');
            var kendoData = $grid.data('kendoGrid');

            if(kendoData){
                kendoData.destroy();
                $grid.empty();
            }

            
            try {
                parent.Xrm.Page.getAttribute("ddsm_targetentityname").setValue(dataItem.label);
            } catch (error) {

            }

            var logicalName = dataItem.logicalName;
            var id = dataItem.id;
            parent.Xrm.Page.getAttribute("ddsm_targetentity").setValue(logicalName);
            window.top.createTable(id, this);


        },
        dataBound: function (e) {
            $('.entity-list-div').show(500);
            // fill value on load
            var dropdownlist = this;
            var selectedEntity = parent.Xrm.Page.getAttribute("ddsm_targetentity").getValue();
            if (selectedEntity) {
                dropdownlist.value(selectedEntity);
                let createTab = function () {
                    if (window.top.createTable) {
                        window.top.createTable(id);
                    } else {
                        setTimeout(createTab, 500);
                    }
                };
                // set url for nested dropdown
                var dataItem = this.dataItem(e.item);
                var id = dataItem ? dataItem.id : undefined;
                var label = dataItem ? dataItem.Label : parent.Xrm.Page.getAttribute("ddsm_targetentity").getValue();
                try {
                    parent.Xrm.Page.getAttribute("ddsm_targetentityname").setValue(dataItem.label);

                    window.top.lockdropDown();
                    createTab();

                } catch (e) {
                    kendo.ui.progress($('#main_container'), false);

                }

            } else {
                kendo.ui.progress($('#main_container'), false);
            }
        }
    });
}

module.exports = createDropdown;

/***/ }),
/* 42 */
/***/ (function(module, exports) {

let dataConverter = function(dataArray){
    let convertSingleElement = function (element) {

        let target;
        let type = element.type;

        if(type === 'entity'){
            type = 'Lookup';
            target = element.logicalName;
        }

        let resultElement = {
            fieldName : element.logicalName,
            title : element.displayName ,
            type,
            target
        };
        return resultElement;
    };

    let resultArray = dataArray.map(convertSingleElement)
        .sort(function(a,b){
            if(a.title < b.title){
                return -1;
            }
            if(a.title > b.title){
                return 1;
            }
            return 0;
        });

        return resultArray;
}

module.exports = dataConverter;

/***/ }),
/* 43 */
/***/ (function(module, exports) {


let getStringOrObject = function(stringOrObj, $popup){
        return $popup.find(stringOrObj).data('kendoDropDownList');
}

let onUpdateAndBoundDataGet = function(dmnDropdown, relationTypeDropdown, relationValueString, $popup){
    let valueDmn = getStringOrObject(dmnDropdown,$popup).text();
    let valueRelType = getStringOrObject(relationTypeDropdown,$popup).text();
    

    let resultString = `executeDMN(${valueDmn}, ${valueRelType}(${relationValueString}))`
    return resultString;
}

module.exports  = onUpdateAndBoundDataGet;

/***/ }),
/* 44 */
/***/ (function(module, exports) {


let getStringOrObject = function(stringOrObj, $popup){
        return $popup.find(stringOrObj).data('kendoDropDownList');
}

let businessRuleEventDataGet = function(businessRuleText, relationTypeDropdown, relationDropdown, $popup){
    let valueRelType = getStringOrObject(relationTypeDropdown,$popup).text();
    let valueRel = getStringOrObject(relationDropdown,$popup).value();
    

    let resultString = `executeDMN(${businessRuleText}, ${valueRelType}(${valueRel}))`;
    return resultString;
}

module.exports  = businessRuleEventDataGet;

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

let insertToTextarea = __webpack_require__(8);
let popupConstants = __webpack_require__(1);
let multipleDataGeter = __webpack_require__(44);


///Create dropDown with business rules array
///popup - the reference, where dropdown is situated
///selector - optionaly, id where dropdown will be initialized
///businessRuleName - the name  of current business rule, where user is working
let createDropDownList = function ($popup, selector, businessRuleName = '', defaultValue) {

    let $businessRules = $popup.find(selector);
    $businessRules.kendoDropDownList({
        //clear business rules array from the current rule
        dataSource: {
            transport: {
                read: {
                    url: parent.Xrm.Page.context.getClientUrl() + `/api/data/v8.1/ddsm_businessrules?$filter=ddsm_isactivate eq true&$select=ddsm_businessruleid,ddsm_name`,
                    dataType: "json",
                    beforeSend: function (req) {
                        req.setRequestHeader('Accept', 'application/json');
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader('OData-MaxVersion', "4.0");
                        req.setRequestHeader("OData-Version", "4.0");
                    }
                }
            },
            schema: {
                data: function (responce) {
                    return responce.value.filter(rule => rule.ddsm_name !== businessRuleName);
                }
            }
        },
        select: function (e) {
            let insertedString = e.item.text();
            let stringResult = multipleDataGeter(insertedString,
                '#' + popupConstants.arrIds.relationTypesId,
                '#' + popupConstants.arrIds.relationsDropdownId,
                $popup);
            let $textarea = $popup.find('#' + popupConstants.arrIds.formulaEditorId);
            $textarea.val(stringResult);
        },
        dataBound: function () {

            let dropDownListData = $businessRules.data('kendoDropDownList');

            if (defaultValue) {
                dropDownListData.value(defaultValue);
            } else {
                kendo.ui.progress($popup, false);
            }
        },
        dataTextField: 'ddsm_name',
        dataValueField: 'ddsm_businessruleid',
        optionLabel: '--',
        animation: false,
        popup: {
            appendTo: $popup
        }
    });
};

module.exports = createDropDownList;

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

let baseUrl = parent.Xrm.Page.context.getClientUrl();
let popupConstants = __webpack_require__(1);

let getEntities = function (relationType, entityLogicalName, callback) {
    
    if (relationType === popupConstants.relationTypes.ManyToOne) {
        $.ajax({
            type: 'GET',
            url: baseUrl + "/api/data/v8.1/EntityDefinitions?$select=LogicalName,DisplayName&$expand=Attributes&$filter=LogicalName eq '" + entityLogicalName + "'",
            headers: {
                'Accept': 'application/json',
                "Content-Type": "application/json; charset=utf-8",
                'OData-MaxVersion': "4.0",
                "OData-Version": "4.0"
            }
        }).done(result => {
            let attrs = result.value[0].Attributes;
            let lookups = attrs.filter(attr => attr.AttributeType === 'Lookup');
            let relationsNtoOne = lookups.map(item => {
                let displayName = item.EntityLogicalName + ' -> ' + item.Targets[0];

                let obj = {
                    text: displayName,
                    value: item.LogicalName
                };

                return obj;
            });

            callback(relationsNtoOne);
        });
    } else if (relationType === popupConstants.relationTypes.OneToMany) {
        $.ajax({
            type: 'GET',
            url: baseUrl + "/api/data/v8.1/RelationshipDefinitions/Microsoft.Dynamics.CRM.OneToManyRelationshipMetadata?$filter=ReferencedEntity eq '" + entityLogicalName + "'",
            headers: {
                'Accept': 'application/json',
                "Content-Type": "application/json; charset=utf-8",
                'OData-MaxVersion': "4.0",
                "OData-Version": "4.0"
            }
        }).done(result => {
            let relationsOneToN = result.value.map(item => {
                let text = item.ReferencedEntity + ' -> ' + item.ReferencingEntity;
                let value = item.ReferencedEntityNavigationPropertyName;

                return {
                    text: text,
                    value: value
                };
            });

            callback(relationsOneToN);
        });
    }
}

module.exports = getEntities;

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

//Config value
let config = (__webpack_require__(0))['ExecuteDmn'];
//
let relationsDropDownListCreator = __webpack_require__(48);
let insertToTextarea = __webpack_require__(8);
let popupConstants = __webpack_require__(1);

let propertiesArray = Object.keys(config);

let createObjectsArray = function () {
    let array = propertiesArray.map(item => {
        let displayName = config[item].displayName;
        let value = item;

        return { displayName, value }
    });

    return array;
}

let createDropDownList = function ($popup, selector, entitiesStorageSelector, entityLogicalName,
    defaultRelationType, defaultRelation) {
    let dataSourceArray = createObjectsArray();

    let $relationTypesDropDown = $popup.find(selector);
    $relationTypesDropDown.empty();

    $relationTypesDropDown.kendoDropDownList({
        //clear business rules array from the current rule
        dataSource: dataSourceArray,
        dataTextField: 'displayName',
        dataValueField: 'value',
        optionLabel: '--',
        animation: false,
        select: function(e){
            kendo.ui.progress($popup, true);
            let relationType = this.dataItem(e.item).value;
            // let insertedString = e.item.text() + '()';
            
            // let $textarea = $popup.find('#' + popupConstants.arrIds.formulaEditorId);
            // insertToTextarea($textarea, insertedString);

            //e.sender.dataItem(e.item)[e.sender.options.dataTextField]
            //e.sender.dataItem(e.item)[e.sender.options.dataValueField]
            //TO.DO get data array for relation type;
            //let resultArray = []
            relationsDropDownListCreator($popup, entitiesStorageSelector, relationType, entityLogicalName);
        },
        dataBound: function () {
            if (defaultRelationType) {

                this.value(defaultRelationType);

                relationsDropDownListCreator($popup, entitiesStorageSelector, defaultRelationType,
                    entityLogicalName, defaultRelation);
            }
        },
        popup: {
            appendTo: $popup
        }
    });
}

module.exports = createDropDownList;

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

let getEntities = __webpack_require__(46);
let insertToTextarea = __webpack_require__(8);
let popupConstants = __webpack_require__(1);

const getData = __webpack_require__(43);


let createDropDownList = function ($popup, selector, relationType, entityLogicalName, defaultValue) {  
    getEntities(relationType, entityLogicalName, 
        function (array) {
            let $relationsStorage = $popup.find(selector);

            $relationsStorage.empty();
            let $dropDownList = $('<div/>', {
                style: 'width: 100%',
                id: popupConstants.arrIds.relationsDropdownId
            });
            $relationsStorage.append($dropDownList);

            $dropDownList.kendoDropDownList({
                //clear business rules array from the current rule
                dataSource: array,
                //TO.DO set properyName for array
                dataTextField: 'text',
                dataValueField: 'value',
                optionLabel: '--',
                animation: false,
                popup: {
                    appendTo: $popup
                },
                select: function (e) {
                    let string = getData(`#${popupConstants.arrIds.businessRulesId}`,
                    `#${popupConstants.arrIds.relationTypesId}`,
                    this.dataItem(e.item).value,
                    $popup);

                    let $textarea = $popup.find('#' + popupConstants.arrIds.formulaEditorId);
                    $textarea.val(string);
                },
                dataBound: function () {
                    

                    if (defaultValue) {
                        this.value(defaultValue);
                        
                    }else{
                        kendo.ui.progress($popup, false);
                    }

                    let string = getData(`#${popupConstants.arrIds.businessRulesId}`,
                    `#${popupConstants.arrIds.relationTypesId}`,
                    this.value(),
                    $popup);

                    let $textarea = $popup.find('#' + popupConstants.arrIds.formulaEditorId);
                    $textarea.val(string);
                }
            });
            kendo.ui.progress($popup, false);
        }
        
    );
};

module.exports = createDropDownList;

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

let createBusinessRulesDropDownList = __webpack_require__(45);
let createRelationTypesDropDownList = __webpack_require__(47);
let renderPopupBody = __webpack_require__(51);
let addEventHandlerForSubmitButton = __webpack_require__(50);
let popupConstants = __webpack_require__(1);
let createPopup = __webpack_require__(52);

function createPopUpForOutputWindow(type, container, options, title, entityLogicalName) {
    let popup = createPopup(title);
    let $popup = $(popup.document.body);
    let currentBusinessRule = parent.Xrm.Page.getAttribute('ddsm_name').getValue();

    let cellState = options.model[options.field];
    if (!cellState) {
        cellState = {
            businessRuleId: undefined,
            relationName: undefined,
            relationType: undefined,
            view: undefined
        };
    }

    cellState.relationType = cellState.relationType ? cellState.relationType : popupConstants.relationTypes.OneToMany;

    renderPopupBody($popup, cellState.view);
    kendo.ui.progress($popup, true);
    addEventHandlerForSubmitButton(popup, options);

    createBusinessRulesDropDownList($popup, '#' + popupConstants.arrIds.businessRulesId, currentBusinessRule,
        cellState.businessRuleId);
    createRelationTypesDropDownList($popup, '#' + popupConstants.arrIds.relationTypesId, '#' + popupConstants.arrIds.relationsId,
        entityLogicalName, cellState.relationType, cellState.relationName);
}

module.exports = createPopUpForOutputWindow; 

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

let popupConstants = __webpack_require__(1);

function addEventHandlerForSubmitButton(popup, options) {

    let $popup = $(popup.document.body);

    let $submit = $popup.find('#' + popupConstants.arrIds.btnSubmitId);

    $submit.on('click', function () {
        let $businessRulesDropDown = $popup.find('#' + popupConstants.arrIds.businessRulesId).data("kendoDropDownList");
        let $relationTypesDropDown = $popup.find('#' + popupConstants.arrIds.relationTypesId).data("kendoDropDownList");
        let $relationDropDown = $popup.find('#' + popupConstants.arrIds.relationsId + ' div').data("kendoDropDownList");
        let $formulaEditor = $popup.find('#' + popupConstants.arrIds.formulaEditorId);

        let resultObject = {
            businessRuleId: $businessRulesDropDown.value(),
            relationName: $relationDropDown.value(),
            relationType: $relationTypesDropDown.value(),
            view: $formulaEditor.val()
        };

        console.log('resultObject');
        console.log(resultObject);

        options.model[options.field] = resultObject;

        refreshGrid(popup);
    });

    function refreshGrid(popup) {
        let gridData = $('#grid').data('kendoGrid');
        gridData.refresh();
        popup.close();
    }
}

module.exports = addEventHandlerForSubmitButton;

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {


let popupConstants = __webpack_require__(1);

function renderPopupBody($popup, view) {

    // --------------------------------- Create textarea for editing formula -----------------------------------
    view = view ? view : 'executeDMN()';
    let $textarea = $('<textarea/>', {
        id: popupConstants.arrIds.formulaEditorId,
        class: 'k-textbox',
        style: 'width: 100%;',
        rows: 10
    });
    $textarea.val(view);
    let $textareaCol = $('<div/>', {
        class: 'col-md-12'
    });
    $textareaCol.append($textarea);

    let $textareaRow = $('<div/>', {
        class: 'row pop-up-row'
    });
    $textareaRow.append($textareaCol);
    $popup.append($textareaRow);
    // ---------------------------------------------------------------------------------------------------------

    // ------------------- Create drop down lists: business rules, relation types, relations -------------------
    let $businessRules = createDivWithId(popupConstants.arrIds.businessRulesId);
    let $relationTypes = createDivWithId(popupConstants.arrIds.relationTypesId);
    let $relations = createDivWithId(popupConstants.arrIds.relationsId);

    let $dropDownListsRow = $('<div/>', {
        class: 'row pop-up-row'
    });
    $dropDownListsRow
        .append($businessRules)
        .append($relationTypes)
        .append($relations);

    $popup.append($dropDownListsRow);
    // ---------------------------------------------------------------------------------------------------------

    // ----------------------------------- Create blocks for submit button -------------------------------------
    let $submit = $('<input/>', {
        id: popupConstants.arrIds.btnSubmitId,
        type: 'button',
        value: 'Submit',
        class: 'k-button'
    });
    let $submitCol = $('<div/>', {
        class: 'col-md-12'
    });
    $submitCol.append($submit);

    let $submitRow = $('<div/>', {
        class: 'row pop-up-row'
    });
    $submitRow.append($submitCol);
    $popup.append($submitRow);
    // ---------------------------------------------------------------------------------------------------------
}

function createDivWithId(id) {
    let $div = $('<div/>', {
        id,
        style: 'width: 100%'
    });

    let $divWrapper = $('<div/>', {
        class: 'col-md-3'
    });
    $divWrapper.append($div);

    return $divWrapper;
}

module.exports = renderPopupBody;

/***/ }),
/* 52 */
/***/ (function(module, exports) {


function getHeadersForPopup() {
    let baseUrl = parent.Xrm.Page.context.getClientUrl() + '/WebResources';

    let headers = '';

    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.common.min.css" />';
    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.default.min.css" />';
    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.default.mobile.min.css" />';

    headers += '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />';
    headers += '<link rel="stylesheet" href="http://kendo.cdn.telerik.com/2016.2.714/styles/kendo.bootstrap.min.css" />';

    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.office365.min.css"" />';
    headers += '<link rel="stylesheet" href="' + baseUrl + '/bpmn_/css/style.css" />';

    return headers;
}

function createPopup(title) {
    let params = 'width=' + screen.width +
        ', height=' + screen.height;

    let popup = window.open('about:blank', '', params);

    // for test
    window.executeDmnPopup = popup;

    popup.moveTo(0, 0);
    popup.document.title = title;

    let headersHtml = getHeadersForPopup();

    let $popupHead = $(popup.document.head);
    let popupHeadHtml = $popupHead.html();

    popupHeadHtml += headersHtml;
    $popupHead.html(popupHeadHtml);

    return popup;
}

module.exports = createPopup;

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

let checkUnvalidColumns = __webpack_require__(54);
let configForIndexesOfColumns = __webpack_require__(3);


///Shows, does contain any grid column the unvalid state, like 'No Input' or 'No Output'
let checkGridColumns = function(columns){
    let inputColumn = columns[configForIndexesOfColumns.inputColumn];
    let outputColumn = columns[configForIndexesOfColumns.outputColumn];

    let inputIsUnvalid = checkUnvalidColumns(inputColumn, 'no_input');
    let outputIsUnvalid = checkUnvalidColumns(outputColumn, 'no_output');

    let result = {
        inputIsUnvalid,
        outputIsUnvalid,

        //if gridIsValid is false, you don't need to check any property
        gridIsValid : !(inputIsUnvalid || outputIsUnvalid) 
    };

    return result;
} 

module.exports = checkGridColumns;

/***/ }),
/* 54 */
/***/ (function(module, exports) {

let checkUnvalidColumn = function(column,expectedUnvalidName){
    let containedColumns = column.columns;
    let isContainsUnvalidColumn = containedColumns.find(c => c.field === expectedUnvalidName);

    return !!isContainsUnvalidColumn;
}

module.exports = checkUnvalidColumn;

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

let setDropEvents = function(container_data,addColumn ,columnBehavour,alertFunction, configForIndexesOfColumns){
    let setInputDropEvent = __webpack_require__(56);
    let setOutputDropEvent = __webpack_require__(57);
    
    setInputDropEvent(container_data,addColumn ,columnBehavour,alertFunction, configForIndexesOfColumns);
    setOutputDropEvent(container_data,addColumn ,columnBehavour,alertFunction, configForIndexesOfColumns);
}

module.exports = setDropEvents;

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

//Set input drop-event
let setInputDropEvent = function (container_data, addColumn, columnBehavour, alertFunction, configForIndexesOfColumns) {
    let $inputHeader = $('#grid thead>tr:first>th[data-title="Input"]');
    let createObjectForGrid = __webpack_require__(12);
    const constants = __webpack_require__(9);

    $inputHeader.kendoDropTarget({
        drop: function (e) {
             
            let droppedObject = $(constants.treeViewSelectorId).data('kendoTreeView').dataItem(e.draggable.element);                    

            let newObject = createObjectForGrid(droppedObject);

            if (!container_data) {
                container_data = [];
                console.log('On dropevent container_data is empty or undef');
            }

            let element = container_data.find(e => e.fieldName === newObject.fieldName);

            if (!element) {
                container_data.push(newObject);
                element = container_data.find(e => e.fieldName === newObject.fieldName);
            }

            if (element.type === 'executeDmn' 
            || element.type === 'outputValue') {
                $inputHeader.removeClass('dropHower');
                alertFunction(`${element.type} is valid only for output`);

            } else {
                let inputsArray = columnBehavour.getColumnArray()[configForIndexesOfColumns.inputColumn];
                let res = inputsArray.columns.find(i => i.field.indexOf(element.fieldName + '_Input') !== -1);
                if (!res) {

                    let fieldName = element.fieldName;

                    let title = element.title;
                    let type = element.type;
                    addColumn(fieldName, title, 'Input', type, 0);
                } else {
                    //this code executes when input has the same field 
                    let titleConfig = (__webpack_require__(6))['title'];
                    let index = 0;
                    let titleTemplate;
                    while (true) {
                        //we creating visualisation of data title 
                        titleTemplate = `<b>${element.title}</b>${titleConfig.openBracket}${index}${titleConfig.closeBracket}(${element.type})`;

                        //we searching the valid index number 
                        let titleIsContains = inputsArray.columns.find(i => i.title === titleTemplate);

                        if (titleIsContains) {
                            index++;
                        } else {
                            let fieldName = element.fieldName;

                            let title = element.title;
                            let type = element.type;


                            addColumn(fieldName, title, 'Input', type, index);
                            break;
                        }
                    }

                    $inputHeader.removeClass('dropHower');
                }
            }
        },        
        dragenter: function () {
            $inputHeader.addClass('dropHower').removeClass('animation');
        },
        dragleave: function () {
            $inputHeader.removeClass('dropHower').addClass('animation');
        }

    });
}

module.exports = setInputDropEvent;




/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

//Set output column drop event
let setOutputEvemt = function (container_data, addColumn, columnBehavour, alertFunction, configForIndexesOfColumns) {
    let $outputHeader = $('#grid thead>tr:first>th[data-title="Output"]');
    let createObjectForGrid = __webpack_require__(12);
    const constants = __webpack_require__(9);

    $outputHeader.kendoDropTarget({
        drop: function (e) {
            let droppedObject = $(constants.treeViewSelectorId).data('kendoTreeView').dataItem(e.draggable.element);

            let newObject = createObjectForGrid(droppedObject);
            let type = newObject.type;
            if(type === 'outputValue'){

                if (!container_data) {
                    container_data = [];
                    console.log('On dropevent container_data is empty or undef');
                }

                let outputArray = columnBehavour.getColumnArray()[configForIndexesOfColumns.outputColumn];
                let element = outputArray.columns.find(e => e.field === 'OutputValue_Output');
                
                let metadataElement = container_data.find(e => e.fieldName === 'OutputValue_Output');

                if(!metadataElement){
                    container_data.push({
                        fieldName: 'OutputValue',
                        title: 'OutputValue',
                        type: 'outputValue'
                    });
                }

                if(element){
                    alertFunction('Field already exists in output ');
                    $outputHeader.removeClass('dropHower');
                }else{
                    let fieldName = 'OutputValue';
                    let title = 'OutputValue';
                    let type = 'outputValue';
                    addColumn(fieldName, title, 'Output', type);
                }
            }else if (type === 'executeDmn') {
                let executeDmnCounter = 1;

                let outputArray = columnBehavour.getColumnArray()[configForIndexesOfColumns.outputColumn];

                let executeDmnTitle;
                //find executeDmn with index
                while (true) {
                    executeDmnTitle = `${droppedObject.displayName}#${executeDmnCounter}`;
                    let element = outputArray.columns.find(e => e.title.indexOf(executeDmnTitle + '</b>') !== -1);
                    if (!element) {
                        //if index is free, break loop and add element 
                        break;
                    }
                    executeDmnCounter++;
                }

                newObject.fieldName = `${droppedObject.logicalName}#${executeDmnCounter}`;
                newObject.title = executeDmnTitle;
                container_data.push(newObject);
                addColumn(newObject.fieldName, newObject.title, 'Output', type);
            } else {

                if (!container_data) {
                    container_data = [];
                    console.log('On dropevent container_data is empty or undef');
                }

                let element = container_data.find(e => e.fieldName === newObject.fieldName);

                if (!element) {
                    container_data.push(newObject);
                    element = container_data.find(e => e.fieldName === newObject.fieldName);
                }

                let outputArray = columnBehavour.getColumnArray()[configForIndexesOfColumns.outputColumn];
                let res = outputArray.columns.find(i => i.field.indexOf(element.fieldName+'_Output') !== -1);
                if (!res) {
                    let fieldName = element.fieldName;
                    let title = element.title;
                    let type = element.type;
                    addColumn(fieldName, title, 'Output', type);
                } else {
                    alertFunction('Field already exists in output ');
                    $outputHeader.removeClass('dropHower');
                }
            }
        },
        dragenter: function () {
            $outputHeader.addClass('dropHower').removeClass('animation');
        },
        dragleave: function () {
            $outputHeader.removeClass('dropHower').addClass('animation');
        }
    });
}

module.exports = setOutputEvemt;




/***/ }),
/* 58 */
/***/ (function(module, exports) {

class ToolTipEvemtCreator {
    createEvent(cssSelector) {
        let tooltipDov;

        $(cssSelector).kendoTooltip({
            content:function(e){
                return $(e.target).text();
            }
        })
    }
}

module.exports = ToolTipEvemtCreator;

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

let configForIndexesOfColumns = __webpack_require__(3);

let columns_behavour = (function () {

    let colsStatus = {
        inputIsDef: true,
        outputIsDef: true,
    };

    let editorConstructor = __webpack_require__(77);
    let editor;

    let inputColumn = {
        title: 'Input',
        columns: [{
            field: 'no_input',
            attributes: {
                style: 'background-color:rgb(255, 255, 183);'
            },
            title: 'No Input'
        }]
    };

    let outputColumn = {
        title: 'Output',
        columns: [{
            field: 'no_output',
            attributes: {
                style: 'background-color:rgb(214, 254, 184);'
            },
            title: 'No Output'
        }]
    };

    let annotationColumn = {
        width: 120,
        field: 'Annotation'
    };

    let destroyColumn = {
        command: {
            name: "Delete",
            text: '<span class="k-icon k-i-close" style="margin-right: 0px;"></span>'
        },
        width: 85       
    };

    let rowNumberColumn = {
        title: "&nbsp;",
        template: "<span class='row-number'></span>",
        width: 18
    };

    let columns = [rowNumberColumn, destroyColumn, inputColumn, outputColumn, annotationColumn];

    let addNewColumn = function (mainColumn, columnName, title, type) {

        let newField = {
            field: columnName.replace(/\s/g, '') + `_${mainColumn.title}`,
            editor: editor,

            title: type ? title + ` (${type})` : title,
            attributes: {
                style: mainColumn.title === 'Input' ?
                    'background-color:rgb(255, 255, 183);' : 'background-color:rgb(214, 254, 184);'
            }
        };
        mainColumn.columns.push(newField);
    };

    let removeColumn = function (targetColumn, columnName) {
        let newColumn = targetColumn.filter(c => c.field !== columnName);
        return newColumn;
    }; 

    let setColumns = function (columnsArray) {
        columnsArray[configForIndexesOfColumns.inputColumn].columns.forEach(function (el) {
            el.template = function (data) {

                let fieldName = el.field;

                if (!data || !data[fieldName] || !data[fieldName].view) {
                    return '';
                }

                return data[fieldName].view;
            }
        });

        columnsArray[configForIndexesOfColumns.outputColumn].columns.forEach(function (el) {
            el.template = function (data) {
                let fieldName = el.field;

                if (!data || !data[fieldName] || !data[fieldName].view) {
                    return '';
                }

                return data[fieldName].view;
            }
        });

        columnsArray[configForIndexesOfColumns.rowNumberColumn]=columns[configForIndexesOfColumns.rowNumberColumn];
        columnsArray[configForIndexesOfColumns.destroyColumn]=columns[configForIndexesOfColumns.destroyColumn];
        columnsArray[configForIndexesOfColumns.annotationColumn]=columns[configForIndexesOfColumns.annotationColumn];

        columns = columnsArray;
    };

    let removeInputColumn = function (columnName) {
        let inputColumn = removeColumn(columns[configForIndexesOfColumns.inputColumn].columns, columnName);
        if (inputColumn.length === 0) {
            columns[configForIndexesOfColumns.inputColumn].columns = [{
                field: 'no_input',
                attributes: {
                    style: 'background-color:rgb(255, 255, 183);'
                },
                title: 'No Input'
            }];
            colsStatus.inputIsDef = true;
        } else {
            columns[configForIndexesOfColumns.inputColumn].columns = inputColumn;
        }
    };

    let removeOutputColumn = function (columnName) {
        let outputColumn = removeColumn(columns[configForIndexesOfColumns.outputColumn].columns, columnName);
        if (outputColumn.length === 0) {
            columns[configForIndexesOfColumns.outputColumn].columns = [{
                field: 'no_output',
                attributes: {
                    style: 'background-color:rgb(214, 254, 184);'
                },
                title: 'No Output'
            }
            ];
            colsStatus.outputIsDef = true;
        } else {
            columns[configForIndexesOfColumns.outputColumn].columns = outputColumn;
        }
    };

    let reset = function () {
        colsStatus.inputIsDef = true;
        colsStatus.outputIsDef = true;

        columns[configForIndexesOfColumns.inputColumn] = {
            title: 'Input',
            columns: [{
                field: 'no_input',
                attributes: {
                    style: 'background-color:rgb(255, 255, 183);'
                },
                title: 'No Input'
            }]
        };

        columns[configForIndexesOfColumns.outputColumn] = {
            title: 'Output',
            columns: [{
                field: 'no_output',
                attributes: {
                    style: 'background-color:rgb(214, 254, 184);'
                },
                title: 'No Output'
            }]
        };
    };

    return {
        addInput: function (columnName, title, type, index = 0) {
            let indexationConfig = __webpack_require__(6);
            let logicalConfig = indexationConfig.logical;
            let titleConfig = indexationConfig.title;

            let logicalFieldName = columnName.replace(/\s/g, '') + `_${inputColumn.title}${logicalConfig.openBracket}${index}${logicalConfig.closeBracket}`;
            let newField = {
                width: 200,
                field: logicalFieldName,
                editor: editor,
                template: function (data) {

                    let fieldName = logicalFieldName;

                    if (!data || !data[fieldName] || !data[fieldName].view) {
                        return '';
                    }

                    return data[fieldName].view;
                },
                title: type ? `<b>${title}</b>${titleConfig.openBracket}${index}${titleConfig.closeBracket}(${type})` : title,
                attributes: {
                    style: 'background-color:rgb(255, 255, 183);'
                }
            };
            if (colsStatus.inputIsDef) {
                columns[configForIndexesOfColumns.inputColumn].columns = [newField];
                colsStatus.inputIsDef = false;
            } else {
                columns[configForIndexesOfColumns.inputColumn].columns.push(newField);
            }

        },
        addOutput: function (columnName, title, type) {

            let newField = {
                width: 200,
                field: columnName.replace(/\s/g, '') + `_${outputColumn.title}`,
                editor: editor,
                template: function (data) {
                    let fieldName = columnName + '_Output';

                    if (!data || !data[fieldName] || !data[fieldName].view) {
                        return '';
                    }

                    return data[fieldName].view;
                },
                title: type ? `<b>${title}</b>` + `</br>(${type})` : title,
                attributes: {
                    style: 'background-color:rgb(214, 254, 184);'
                }
            };
            if (colsStatus.outputIsDef) {
                columns[configForIndexesOfColumns.outputColumn].columns = [newField];
                colsStatus.outputIsDef = false;
            } else {
                columns[configForIndexesOfColumns.outputColumn].columns.push(newField);
            }
        },

        removeInputColumn: removeInputColumn,
        removeOutputColumn: removeOutputColumn,
        getColumnArray: function () {
            return columns;
        },
        getColumnsState: function () {
            return colsStatus;
        },
        setMetadata: function (metadata) {

            let addEditor = function (column, func) {
                column.columns.forEach(item => {
                    item.editor = func;
                });
            };

            editor = editorConstructor(metadata);
            addEditor(columns[configForIndexesOfColumns.inputColumn], editor);
            addEditor(columns[configForIndexesOfColumns.outputColumn], editor);
        },
        setColumns: setColumns,
        reset,
        setDataState: function (statusObject) {
            colsStatus = statusObject;
        },
        getInputColumns(){
            return columns[configForIndexesOfColumns.inputColumn].columns;
        },
        getOutputColumns(){
            return columns[configForIndexesOfColumns.outputColumn].columns;
        },
        setInputColumns(column){
            columns[configForIndexesOfColumns.inputColumn].columns = column;
        },
        setOutputColumns(column){
            columns[configForIndexesOfColumns.outputColumn].columns = column;
        }
    }   
})();

module.exports = columns_behavour;


/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

let configForIndexesOfColumns = __webpack_require__(3);
const constans = __webpack_require__(9);
const checkGridColumns = __webpack_require__(53);
const addRowWhenColumnsHaveNoValidValueMessage = __webpack_require__(36);
let gridBuilder = (function () {
    let id;
    let columnBehavour = __webpack_require__(59);
    let container_data = [];

    let dblClickHandlerIsSet = false;

    let dataBoundHandler = function () {

        let rows = this.items();
        $(rows).each(function () {
            let index = $(this).index() + 1;
            let rowLabel = $(this).find(".row-number");
            $(rowLabel).html(index);
        });

        if (!dblClickHandlerIsSet) {
            $("#grid").delegate("tbody td", "dblclick", function () {

                $("#grid").data("kendoGrid").editCell($(this));

            });

            dblClickHandlerIsSet = true;
        }

        function initEventClickByRemoveButton() {
            $('.k-grid-Delete').on('click', function (e) {
                let msg = "Are you sure?";
                parent.Alert.show(msg, null, [{
                    label: "Yes",
                    callback: function () {
                        e.preventDefault();
                        let grid = $("#grid").data("kendoGrid");
                        let tr = $(e.target).closest("tr");
                        grid.removeRow(tr);
                    }
                }, {
                    label: "No"
                }], "QUESTION", 500, 200);
            });
        }

        let evForBtn = $._data(document.getElementById('toolbar-btn-add'), 'events');
        if (!evForBtn) {
            $('#toolbar-btn-add').on('click', function () {
                let grid = $("#grid").data("kendoGrid");
                let columnsStatus = checkGridColumns(grid.columns);
                
                if(columnsStatus.gridIsValid){
                    grid.addRow();
                    $("#window").data("kendoWindow").close();
                }else{
                    addRowWhenColumnsHaveNoValidValueMessage(columnsStatus);
                }
            });
        }

        initEventClickByRemoveButton();
    };

    let setEvents = function () {

        let HeaderToolTipCreator = __webpack_require__(58);
        let headerCreator = new HeaderToolTipCreator();

        headerCreator.createEvent('#grid thead>tr:eq(1)>th');

        let thisDropEvent = function () {
            let alertFunction = function (text) {
                let alertFunc = __webpack_require__(7);
                alertFunc(text);
            };
            let createDropEvent = __webpack_require__(55);
            createDropEvent(container_data,addColumn ,columnBehavour,alertFunction, configForIndexesOfColumns);
        };

        let doubleClickEvent = function (event) {
            let isInput;
            let $curentTarget = $(event.currentTarget);

            let title = $curentTarget.attr('data-title');
            let logicalName = $curentTarget.attr('data-field');

            let data = $("#grid").data("kendoGrid").dataItems();

            let curentColumnsArray = columnBehavour.getColumnArray();
            let curentArray;

            if (logicalName.indexOf('_Input') !== -1) {
                curentArray = curentColumnsArray[configForIndexesOfColumns.inputColumn];
                isInput = true;

            } else if (logicalName.indexOf('_Output') !== -1) {
                curentArray = curentColumnsArray[configForIndexesOfColumns.outputColumn];
                isInput = false;
            } else {
                throw 'When delete column, it hasn\'t input or output identifier';
            }

            data.forEach(item => {
                delete item[logicalName];
            });
            if (isInput) {
                columnBehavour.removeInputColumn(logicalName);
            } else {
                columnBehavour.removeOutputColumn(logicalName);
            }
            createGrid(id);
            $("#grid").data("kendoGrid").dataSource.data(data);
        };

        thisDropEvent();
        $('thead').children('tr').eq(1).find('th').dblclick(doubleClickEvent);

    };

    let createGrid = function (idSelector, data) {

        if (!id) {
            id = idSelector;
        }

        $(id).empty();
        let columnArray = columnBehavour.getColumnArray();
        if (data) {
            $(id).kendoGrid({
                height: 650,
                reorderable: true,
                resizable: true,
                editable: false,
                columns: columnArray,
                dataSource: data,
                toolbar: '<div class="col-md-2">' +
                '<button id="toolbar-btn-add" type="button" class="k-button">' +
                '<span class="k-icon k-i-plus"></span>' +
                'Add new record' +
                '</button>' +
                '</div>',
                dataBound: dataBoundHandler
            });
        } else {
            $(id).kendoGrid({
                height: 650,
                reorderable: true,
                resizable: true,
                editable: false,
                columns: columnArray,
                toolbar: '<div class="col-md-2">' +
                '<button id="toolbar-btn-add" type="button" class="k-button">' +
                '<span class="k-icon k-i-plus"></span>' +
                'Add new record' +
                '</button>' +
                '</div>',
                dataBound: dataBoundHandler
            });
        }

        setEvents();

    };

    let addColumn = function (name, title, position = 'Input', type, index) {
        let createGridWithNewColumn = function (name, title, position, type, index) {
            let data = $("#grid").data("kendoGrid").dataItems();
            if (position === 'Input') {
                columnBehavour.addInput(name, title, type, index);
            }
            if (position === 'Output') {
                columnBehavour.addOutput(name, title, type);
            }
            createGrid(id);
            $("#grid").data("kendoGrid").dataSource.data(data);
        };

        createGridWithNewColumn(name, title, position, type, index);
        setEvents();
        (__webpack_require__(13))('#grid th');

    };

    let buildGridFromFiles = function (idSelector, columns, data, columnsState, dataCont) {

        if (!id) {
            id = idSelector;
        }
        container_data = dataCont;
        $(id).empty();
        columnBehavour.setColumns(columns);
        columnBehavour.setDataState(columnsState);
        columnBehavour.setMetadata(container_data);

        let columnArray = columns;
        if (data) {
            $(id).kendoGrid({
                height: 650,
                reorderable: true,
                resizable: true,
                editable: false,
                columns: columnArray,
                dataSource: data,
                toolbar: '<div class="col-md-2">' +
                '<button id="toolbar-btn-add" type="button" class="k-button">' +
                '<span class="k-icon k-i-plus"></span>' +
                'Add new record' +
                '</button>' +
                '</div>',
                dataBound: dataBoundHandler
            });
        } else {
            $(id).kendoGrid({
                height: 650,
                reorderable: true,
                resizable: true,
                editable: false,
                columns: columnArray,
                toolbar: '<div class="col-md-2">' +
                '<button id="toolbar-btn-add" type="button" class="k-button">' +
                '<span class="k-icon k-i-plus"></span>' +
                'Add new record' +
                '</button>' +
                '</div>',
                dataBound: dataBoundHandler
            });
        }

        setEvents();
    };

    return {
        createGrid,
        addColumn,
        getColumnsState: function () {
            let columnsArray = columnBehavour.getColumnArray();
            let objectState = columnBehavour.getColumnsState();
            return {
                columnsArray,
                objectState,
                container_data
            };
        },
        setMetadata: function () {
            columnBehavour.setMetadata(container_data);
        },
        setDataContainerSource: function (dataSourceArray) {
            container_data = dataSourceArray;
        },
        buildGridFromFiles,
        resetGrid: function (id) {
            container_data = [];
            columnBehavour.reset();
            createGrid(id);
        },
        getColumnArray: function () {
            return columnBehavour.getColumnArray();
        },
        getContainerData: function () {
            return container_data;
        },
        getInputColumns(){
            return columnBehavour.getInputColumns();
        },
        getOutputColumns(){
            return columnBehavour.getOutputColumns();
        },
        setInputColumns(column){
            columnBehavour.setInputColumns(column);
        },
        setOutputColumns(column){
            columnBehavour.setOutputColumns(column)
        }
    
    };
})();

module.exports = gridBuilder;

/***/ }),
/* 61 */
/***/ (function(module, exports) {

let hitPolicyBuilder = (function () {
    let data = [
        'UNIQUE',
        'FIRST',
        'PRIORITY',
        'ANY',
        'COLLECT',
        'RULE ORDER',
        'OUTPUT ORDER'
    ];

    return function (id) {
        $(id).kendoDropDownList({
            dataSource: {
                data: data
            },
            animation: {
                close: {
                    effects: "fadeOut zoom:out",
                    duration: 300
                },
                open: {
                    effects: "fadeIn zoom:in",
                    duration: 300
                }
            }
        });
    }
})();

module.exports = hitPolicyBuilder;


/***/ }),
/* 62 */
/***/ (function(module, exports) {

let createSplitter = function (idSelector) {
    $(idSelector).kendoSplitter({
        panes: [
            {
                collapsible: true
                , max: "40%"
                , size: '35%'
                //, scrollable: true
            },
            {
                collapsible: false
                , min: '60%'
               // , scrollable: true
            }
        ]
    })
};

module.exports = createSplitter;

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {


let initSplitter = function(selector){
    var $splitterData = $(selector).data('kendoSplitter');

    if(!$splitterData){
        (__webpack_require__(62))(selector);
        $(selector).data('kendoSplitter').collapse('.k-pane:first');
    }
}

module.exports = initSplitter;

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

class NToOneXmlConverter {
    constructor(configObject) {
        let Searcher = __webpack_require__(18);
        let Swapper = __webpack_require__(19);
        let methodArray = Object.keys(configObject);

        this.searcher = new Searcher(methodArray);
        this.swapper = new Swapper();
    }
    _getfunctionName(inputString) {
        let result = (/.*?\(/).exec(inputString)[0];
        let functionName = result.substr(0, result.length - 1);

        return functionName;

    };
    _getParameterArray(inputString) {
        let getParametersWithoutFunctionName = function (targetString) {
            let result = targetString.match(/\(.*\)/)[0];
            return result;
        };
        let getArrayOfParameters = function (targetArray) {
            let splitedArray = targetArray.split(',');
            let arrayLength = splitedArray.length;
            splitedArray[0] = splitedArray[0].replace('(', '');
            splitedArray[arrayLength - 1] = splitedArray[arrayLength - 1].replace(')', '');

            return splitedArray;
        };

        let parameters = getParametersWithoutFunctionName(inputString);
        let parametersArray = getArrayOfParameters(parameters);

        return parametersArray;

    };
    convertString(string) {
        let bareFunctionalSequenceArray = this.searcher.getFunctionalSequenceArray(string);
         
        bareFunctionalSequenceArray.forEach(item => {
            let funcExpr = item.functionExpression;

            let functionName = this._getfunctionName(funcExpr);

            //filter 'now' argument from brackets
            let parameterArray = this._getParameterArray(funcExpr)
                                        .map(item=>{
                                            if(item=== 'now()'){
                                                return 'now';
                                            }else{
                                                return item;
                                            }
                                        });
                                    
            let resultString = `${functionName}^`;
            parameterArray.forEach(item => {
                resultString += item + ',';
            });
            resultString = resultString.substring(0, resultString.length - 1);

            let wrappedResultString = `{{${resultString}}}`;

            item.convertedFunctionExpression = wrappedResultString;
        });

        let convertedString = this.swapper.swapFunctionalSequences(string, bareFunctionalSequenceArray);

        return convertedString;
    }
}

module.exports = NToOneXmlConverter;

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

let filter = __webpack_require__(14);

///Settings properties : text selector, treeview li selector, button selector, treeViewLiSelector, spinnerContainer, treeViewContainer,
///draggableRequried
let setSearchEvent = function (settings) {
    let $popup = $(window.popup.document.body);

    let searchEvent = function () {

        let inputText = $popup.find(settings.textSelector).val().toLowerCase();
        //let inputTex = window.document.getElementById('popup-search-input-field').value.toLowerCase();

        var dataSource = $popup.find(settings.treeViewId).data("kendoTreeView").dataSource;

        filter(dataSource, inputText);
    };

    $popup.find(settings.buttonSelector).off().click(searchEvent);

    $popup.find(settings.textSelector).off().keypress(function (e) {
        if (e.which == 13) {
            searchEvent();
        }
    });
};

module.exports = setSearchEvent;


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

let BaseConverter = __webpack_require__(16);
  
class AgregateOperationConverter extends BaseConverter{
  constructor(methodName){
    super();
    this.methodName = methodName;

  }
  _makeConvertedCountExpression(content){
    return `${this.methodName}^${content}`;
  }

  getConvertedExpression(countExpression){
    if(typeof countExpression !== 'string'){
      throw 'countExpression is not a string';
    }

    let contentBetweenBrackets = this.getContentBetweenBrackets(countExpression);
    let result = this._makeConvertedCountExpression(contentBetweenBrackets);
    return result;
  }
}

module.exports = AgregateOperationConverter;


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

let BaseConverter = __webpack_require__(16);

class LogicalOperationConverter extends BaseConverter{
  constructor(methodName, entityName, operationTranslate){
    super();

    this.operationTranslate = operationTranslate;
    this.entityName = entityName;
    this.methodName = methodName;
}
  _multipleReplace(targetString, sourceReplace, destinationReplace){
    while(targetString.includes(sourceReplace)){
      targetString = targetString.replace(sourceReplace, destinationReplace);
    }
    return targetString;
  }

///Returns 'false' if index is -1 (not exists) or returns number of index if exists
  _findOperatorInsideString(targetString, operatorString){
    operatorString = ` ${operatorString} `;
    let index = targetString.indexOf(operatorString);
    if (index === -1){
      return false;
    }else{

      let operatorObject ={
        index,
        length: operatorString.length,
        operator: operatorString.trim()
      };

      return operatorObject;
    }
  };

  _findCorrectPositionOfOperator(targetString){
    for(let key in this.operationTranslate){
      let index = this._findOperatorInsideString(targetString, key);

      if(index){
        return index;
      }
    }
    throw 'WhereConverter have not found the valid operator in content position';
  };


///valueObject.leftValue 
///valueObject.operator
///valueObject.rightValue
  _createConvertedExpression(valueObject){
    let string = `${this.methodName}$${valueObject.leftValue};${valueObject.operator};${valueObject.rightValue}`;

    //string = this._multipleReplace(string, ' ', '');
    return string;
  };
  _deleteUnnesesaryEntityLogicalName(targetString, logicalName){
    let deletedLogicalNameString = targetString.replace(logicalName+'.', '');
    let checkSameLogicalName = deletedLogicalNameString.indexOf(logicalName);
    if(checkSameLogicalName=== 0){
      return targetString;
    }else{
      return deletedLogicalNameString;
    }
  };

  getConvertedExpression(expressionString){
    let contentBetweenBrackets = this.getContentBetweenBrackets(expressionString);
    let objectOperator = this._findCorrectPositionOfOperator(contentBetweenBrackets);

    let leftOperand = contentBetweenBrackets.substring(0, objectOperator.index);
    //let parsedLeftOperand = this._deleteUnnesesaryEntityLogicalName(leftOperand, this.entityName);

    let rightOperand = contentBetweenBrackets.substring(objectOperator.index + objectOperator.length);

    rightOperand = this._multipleReplace(rightOperand, ',', ';');

    let convertedOperator = this.operationTranslate[objectOperator.operator];

    let result = this._createConvertedExpression({
      leftValue:leftOperand,
      rightValue:rightOperand,
      operator:convertedOperator
    });

    return result;

  }
}

module.exports = LogicalOperationConverter;


/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

let FunctionalSequence = __webpack_require__(17);

class FunctionalParser {
    constructor(currentEntity) {
        let MainConverter = __webpack_require__(69);
        
        this.converter = new MainConverter(currentEntity);
    }
    _getSingleConvertedFunctionalSequence(functionalSequence) {
        let stringExpression = functionalSequence.functionExpression;

        let functionsArray = stringExpression.split(').');

        let resultExpression = '';

        for (let i = 0, length = functionsArray.length, lastIndex = functionsArray.length - 1; i < length; i++) {
            let target = functionsArray[i];
            let convertedFunction = this.converter.convertExpression(target);
            resultExpression += convertedFunction;

            if (i !== lastIndex) {
                resultExpression += '\\';
            }
        }
        resultExpression = `{{${resultExpression}}}`;

        let result = new FunctionalSequence(stringExpression, resultExpression);
        return result;
    }

    getConvertedFunctionalSequenceArray(funcSequenceArray) {
        let convertedFuncSequenceArray = [];

        funcSequenceArray.forEach(item => {
            let singleSeq = this._getSingleConvertedFunctionalSequence(item);
            convertedFuncSequenceArray.push(singleSeq);
        });

        return convertedFuncSequenceArray;
    }
}
module.exports = FunctionalParser;

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

class MainConverter{
    constructor(currentEntity){
        this.LogicalConverter = __webpack_require__(67);
        this.AgregateConverter = __webpack_require__(66);
        this.configFile = (__webpack_require__(0))['1:N'];
        this.currentEntity = currentEntity;
    }

    _findMethodNameLowerCase(stringExpression){
        let indexOfOpenBracket = stringExpression.indexOf('(');
        let methodName =  stringExpression.substring(0,indexOfOpenBracket)
                                            .toLowerCase();
                                            
        return methodName;

    };
    convertExpression(stringExpression){
        let lowerCaseMethodName = this._findMethodNameLowerCase(stringExpression);
        let operationInfo = this.configFile[lowerCaseMethodName];
        
        if(!operationInfo){
            throw 'Config has no current operation';
        }
        let converter;

        if(operationInfo.isAgregate){
            converter = new this.AgregateConverter(lowerCaseMethodName);
        }else{
            let operationTranslate = (__webpack_require__(21)).custom;
            converter = new this.LogicalConverter(lowerCaseMethodName, this.currentEntity, operationTranslate);
        }


        let convertedExpression = converter.getConvertedExpression(stringExpression);
        return convertedExpression;
    }
}

module.exports =    MainConverter;

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

let configOneToN = (__webpack_require__(0))['1:N'];


let methodArray = Object.keys(configOneToN);



module.exports = methodArray;

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

class OneToNXmlConverter{
    constructor(currentEntity){
        let FunctionalParser = __webpack_require__(68);
        let Searcher = __webpack_require__(18);
        let Swapper = __webpack_require__(19);

        this.functionalParser = new FunctionalParser(currentEntity);
        this.searcher = new Searcher();
        this.swapper = new Swapper();
    }

    convertStringToXml(string){
        let bareFunctionalSequenceArray = this.searcher.getFunctionalSequenceArray(string);
        
         
        let convertedFunctionalSequenceArray = this.functionalParser.getConvertedFunctionalSequenceArray(bareFunctionalSequenceArray);
        let convertedString = this.swapper.swapFunctionalSequences(string, convertedFunctionalSequenceArray);
 
        return convertedString;
    }

}

module.exports = OneToNXmlConverter;

/***/ }),
/* 72 */
/***/ (function(module, exports) {

let setDropforPopupInput = function (inputSelector) {
    $(inputSelector).kendoDropTarget({
        drop: function (e) {

            let droppedObject = $('#treeview').data('kendoTreeView').dataItem(e.draggable.element);

            let value = `${droppedObject.logicalParent}.${droppedObject.logicalName}`;
            let displayValue = `${droppedObject.displayParent}.${droppedObject.displayName}`;
            $(inputSelector).val(displayValue);
            $(inputSelector).parent().find('input[type=hidden]').val(value);
        }
    })
};

module.exports = setDropforPopupInput;

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

let inputTypesOperationsConfig = __webpack_require__(20);
function refreshGrid() {
    let gridData = $('#grid').data('kendoGrid');
    gridData.refresh();
    $("#window").data("kendoWindow").close();
}

//this function return another function
//returned functionmust hand event - edit grid field
//:type - string argument, that contains one crm simpe type
//like sting, integer, decimal, etc.
//:operation - string argument, that contains operator look like <,=,!=
//or a function look like contains, substring

function createFormForPopup(type, operation, container, options) {
    let operationConfig = inputTypesOperationsConfig[type][operation];

    let $container = $('<div/>', {
        id: 'popup-form-content-container'
    });

    let $form = $('<form />', {
        id: 'pop-up-form'
    });

    let argsNumber = operationConfig.argsNumber;

    if (!argsNumber && argsNumber != 0) {
        throw 'Cant get argsNumber';
    }

    let dataObject = options.model[options.field];

    function canEdit() {
        return dataObject && dataObject.operator === operation;
    }

    if (argsNumber === 'N') {
        let $row = $('<div/>', {
            'class': 'pop-up-row'
        });

        $label = $('<label/>', {
            'class': 'pop-up-form-label'
        });
        $label.text('Arguments');

        let inputType = getInputType(type, operationConfig.argsType);

        let $input = $('<input />', {
            type: inputType,
            'class': 'k-textbox pop-up-form-input',
            name: 'arg'
        });

        let $hiddenInput = $('<input />', {
            type: 'hidden',
            'class': 'k-textbox pop-up-form-input-hidden',
            name: 'hidden'
        });

        if(canEdit()) {
            $input.val(dataObject.args);
        }

        $label.append($input);
        $label.append($hiddenInput);
        $row.append($label);
        $form.append($row);
    } else {
        for (let i = 0; i < argsNumber; i++) {
            let $row = $('<div/>', {
                'class': 'pop-up-row'
            });

            let $label = $('<label/>', {
                'class': 'pop-up-form-label'
            });
            $label.text('Argument' + (i + 1));

            let inputType = getInputType(type, operationConfig.argsType);

            let $input = $('<input />', {
                type: inputType,
                'class': 'k-textbox pop-up-form-input',
                name: 'arg'
            });
            let $hiddenInput = $('<input />', {
            type: 'hidden',
            'class': 'k-textbox pop-up-form-input-hidden',
            name: 'hidden'
        }); 

        $input.change(function(){
            $hiddenInput.val('');
        });

            if(canEdit()) {
                if (dataObject.displayView){
                    $input.val(dataObject.displayView[i]);
                    $hiddenInput.val(dataObject.args[i]);
                }else{
                    $input.val(dataObject.args[i]);
                }
            }

            $label.append($input);
            $label.append($hiddenInput);
            $row.append($label);
            $form.append($row);

             if(type === 'datetime') {
                $input.kendoDatePicker();
            }
        }
    }

    $submit = $('<input />', {
        type: 'submit',
        'class': 'k-button pop-up-submit'
    });
    $submit.val('Submit');
    $form.on('submit', function (options, operationConfig) {
        return function (ev) {
            ev.preventDefault();
            let getTitleTextForIndex = function(array, index, otherwiseElement){
                if(!array){
                    return otherwiseElement;
                }
                if(!array[index]){
                    return otherwiseElement;
                }

                return array[index];
            };
            let titleTextForDroppable;
            let serializedArray = $(ev.target).serializeArray();
            let hiddenInput = serializedArray.find(el=> el.name ==='hidden');
            serializedArray = serializedArray.filter(el => el.name !== 'hidden');

            if(hiddenInput && hiddenInput.value !== ''){
                titleTextForDroppable = [serializedArray[0].value];
                serializedArray[0].value = hiddenInput.value;
            }


            let argsValues = serializedArray.map(field => field.value);
            let result = '';

            let template = operationConfig.template;

            if (operationConfig.argsNumber === 0) {
                result = template;
            } else if (operationConfig.argsNumber === 'N') {
                result = template.replace('{{args}}', argsValues[0]);
            } else {
                let bufResult = template;
                argsValues.forEach((el, i) => {
                    bufResult = bufResult.replace('{{arg' + (i + 1) + '}}', getTitleTextForIndex(titleTextForDroppable,i,el) );
                });
                result = bufResult;
            }

            let resultObject = {
                datatype: type,
                operator: operation,

                args: argsValues,//argsValues.length <= 1 || operationConfig.argsNumber === 'N' ? argsValues[0]: argsValues,
                view: result,
                displayView: titleTextForDroppable ? titleTextForDroppable : null           
            };

            options.model[options.field] = resultObject;
            refreshGrid();
        }
    } (options, operationConfig));

    $submitRow = $('<div/>', {
        'class': 'pop-up-row'
    });

    $submitRow.append($submit);
    $form.append($submitRow);

    $container.append($form);


    return $container;
};


module.exports = createFormForPopup;

function getInputType(type, argsType) {
    let inputType = 'text';

    if (argsType) {
        return  argsType !== 'date' ? argsType : 'text';
    }

    if (type === 'integer') {
        inputType = 'number';
    }

    return inputType;
}


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

let outputPopupConfig = __webpack_require__(0);
let toolsForPopupOutput = __webpack_require__(5);
let relationTypes = __webpack_require__(4);

const NToOne = relationTypes.NToOne;
const OneToN = relationTypes.OneToN;
const NoRelation = relationTypes.NoRelation;

function createFunctionsColumn($functionsColumn, $functionDropDown, operations, getRelationType, getFunctionConfigForNoRelation,
    $popup, options, type, $resultInput) {

    $functionsColumn.append($functionDropDown);
    
    $functionDropDown.kendoDropDownList({
        dataSource: operations,
        height:400,
        dataTextField: "text",
        dataValueField: "value",
        optionLabel: 'Select function',
        select: function (e) {
            let dataItem = this.dataItem(e.item);
            let func = dataItem.text;

            let relationType = getRelationType($popup);
            let functionLogicalName = dataItem.value;

            let funcConf;
            if (relationType === NoRelation) {
                funcConf = getFunctionConfigForNoRelation(type, functionLogicalName);
            } else {
                funcConf = outputPopupConfig[relationType][functionLogicalName];
            }

            if (func) {
                let separator = funcConf && funcConf.separator ? funcConf.separator : "";
                let insertedString = separator + func + '()';
                let $textarea = $popup.find('.output-popup-result-input');
                toolsForPopupOutput.insertToTextarea($textarea, insertedString);
            }
        },
        dataBound: function () {

            let dropDownListData = $functionDropDown.data('kendoDropDownList');
            dropDownListData.value('');

            let dataObject = options.model[options.field];
            let view = dataObject ? dataObject.view : '';
            $resultInput.val(view);
        },
        popup: {
            appendTo: $popup
        }
    });
}

module.exports = createFunctionsColumn;

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

let toolsForPopupOutput = __webpack_require__(5);
let outputOperatorsConfig = __webpack_require__(21);
let relationTypes = __webpack_require__(4);

const NToOne = relationTypes.NToOne;

function createOperatorsDropDown($popup, $operatorsColumn, relationType) {

    if (relationType === NToOne) {
        return;
    }

    let operators = Object.keys(outputOperatorsConfig['custom']);

    if (!operators) {
        return;
    }

    let $operatorsDropDown = $('<select/>', {
        id: 'popup-operators-dropdown'
    });

    $operatorsColumn.append($operatorsDropDown);

    $operatorsDropDown.kendoDropDownList({
        dataSource: operators,
        optionLabel: 'Select operator',
        height: 400,
        select: function (e) {
            let operator = this.dataItem(e.item);

            let $textarea = $popup.find('.output-popup-result-input');
            toolsForPopupOutput.insertToTextarea($textarea, ' ' + operator + ' ');
        },
        popup: {
            appendTo: $popup
        }
    }).addClass('popup-dropdown-input');
}

module.exports = createOperatorsDropDown;

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

let outputPopupConfig = __webpack_require__(0);
let treeViewCreator = __webpack_require__(84);
let toolsForPopupOutput = __webpack_require__(5);
let createFunctionsColumn = __webpack_require__(74);
// let parseFormula = require('./parser/formulaParser.js');
let relationTypes = __webpack_require__(4);
let createOperatorsDropDown = __webpack_require__(75);
let clearMemory = __webpack_require__(78);


const NToOne = relationTypes.NToOne;
const OneToN = relationTypes.OneToN;
const NoRelation = relationTypes.NoRelation;

const baseOperations = ['/', '*', '+', '-'];

function refreshGrid(popup) {
    let gridData = $('#grid').data('kendoGrid');
    gridData.refresh();
    popup.close();
}

let $functionDropDown = $('<div/>', {
    id: 'popup-output-functions'
}).addClass('popup-dropdown-input');

let $resultInput = $('<textarea/>', {
    'class': 'output-popup-result-input k-textbox',
    rows: 10
});

function getRelationType($popup) {
    let $input = $popup.find('input[name="output-popup-relation-radio"]:checked');
    let selectedRelation = $input.val();

    return selectedRelation;
}

function renderFieldsTreeView($entityFieldsColumn, entityLogicalName, entityDisplayName, popup) {
    let appendSearch = __webpack_require__(79);

    let $popupEntityFieldsTree = $('<div/>', {
        id: 'popup-fields-tree-view',
        style: 'overflow:scroll; height: 49%; width: 100%;'
    });
    appendSearch($popupEntityFieldsTree,$entityFieldsColumn );
    //$entityFieldsColumn.append($popupEntityFieldsTree);

    treeViewCreator(
        '#popup-fields-tree-view',
        entityLogicalName,
        entityDisplayName,
        popup
    );
    let search = __webpack_require__(65);
    search({
        popup: popup,
        buttonSelector:'#popup-search-input-button',
        textSelector: '#popup-search-input-field',
        treeViewId: '#popup-fields-tree-view'
    });
}

function getHeadersForPopup() {
    let baseUrl = parent.Xrm.Page.context.getClientUrl() + '/WebResources';

    let headers = '';

    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.common.min.css" />';
    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.default.min.css" />';
    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.default.mobile.min.css" />';

    headers += '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />';
    headers += '<link rel="stylesheet" href="http://kendo.cdn.telerik.com/2016.2.714/styles/kendo.bootstrap.min.css" />';

    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.office365.min.css"" />';
    headers += '<link rel="stylesheet" href="' + baseUrl + '/bpmn_/css/style.css" />';
    return headers;
}

function createPopup(title) {
    let params = 'width=' + screen.width +
        ', height=' + screen.height;

    let popup = window.open('about:blank', '', params);

    popup.getRelationType = getRelationType;

    // for test
    window.popup = popup;

    popup.moveTo(0, 0);
    popup.document.title = title;

    let headersHtml = getHeadersForPopup();

    let $popupHead = $(popup.document.head);
    let popupHeadHtml = $popupHead.html();

    popupHeadHtml += headersHtml;
    $popupHead.html(popupHeadHtml);

    return popup;
}

function createPopUpForOutputWindow(type, container, options, title, entityLogicalName) {
    let popup = createPopup(title);
    kendo.ui.progress($(window.popup.document.body), true);
    let $popup = $(popup.document.body);

    let $dropdown = $('<div/>').attr('id', 'dropdown');
    $popup.append($dropdown);

    let $popupContainer = $('<div/>', {
        id: 'popup-container'
    });
    $popupContainer.appendTo($popup);

    let $headerPopupRow = createPopupRow();

    let $headerPopupColTitle = createPopupCol(4);
    $titleSpan = $('<span/>', {
        'class': 'pop-up-title'
    });
    $titleSpan.text(type ? type : 'Output');

    $headerPopupColTitle.append($titleSpan);

    $headerPopupRow.append($headerPopupColTitle);
    $popupContainer.append($headerPopupRow);

    let $functionsColumn = $('<div />', {
        'class': 'output-popup-functions col-md-5'
    });

    let entityDisplayName = title.split(".")[0];

    let $radioButtonsBlock = createRadioButtonsBlock($functionsColumn, type,
        options, entityLogicalName, entityDisplayName, popup);
    $headerPopupRow.append($radioButtonsBlock);

    let $resultRow = $('<div/>', {
        'class': 'output-popup-result-block row'
    });

    let $resultCol = $('<div/>', {
        'class': 'output-popup-result-col col-md-12'
    });

    $resultCol.append($resultInput);
    $resultRow.append($resultCol);
    $popupContainer.append($resultRow);

    let $outputPopupBody = $('<div/>', {
        'class': 'outpot-popup-body row'
    });

    let $entityFieldsColumn = $('<div/>', {
        'class': 'output-popup-entity-feilds col-md-5'
    });

    let relationType = getRelationType($popup);
    if (relationType === OneToN || relationType === NToOne) {
        setTimeout(function () {
            renderFieldsTreeView($entityFieldsColumn, entityLogicalName, entityDisplayName, popup);
        }, 500);
    }
    else if (relationType === NoRelation) {
        setTimeout(function () {
            renderFieldsTreeView($entityFieldsColumn, entityLogicalName, entityDisplayName, popup);
        }, 500);
    }

    $outputPopupBody.append($functionsColumn);
    $outputPopupBody.append($entityFieldsColumn);
    $popupContainer.append($outputPopupBody);

    let $submitRow = $('<div/>', {
        'class': 'output-popup-submit-row row'
    });

    let $submitCol = $('<div/>', {
        'class': 'output-submit-col col-md-8'
    });

    let $submit = $('<input/>', {
        type: 'submit',
        value: 'Submit',
        'class': 'k-button'
    });

    $submit.on('click', function () {

        let selectedRelation = getRelationType($popup);

        let view = $resultInput.val();
        let operations = $popup.find('#popup-output-functions').data('kendoDropDownList')
            .dataSource.options.data;

        let expression = replaceAllDisplayNames(operations, view);

        // let initExps = [];
        // let formula;
        // try {
        //     formula = parseFormula(view, baseOperations, initExps, selectedRelation, type);
        // }
        // catch (err) {
        //     console.log(err);
        //     /*popup.alert(err);

        //     throw err;*/
        // }

        let resultObject = {
            relation: selectedRelation,
            datatype: type,
            args: null,
            view: view,
            expression: {
                value: expression
            }
            // ,formula: formula
        };

        console.log('resultObject');
        console.log(resultObject);

        options.model[options.field] = resultObject;
        refreshGrid(popup);
    });

    $submitCol.append($submit);
    $submitRow.append($submitCol);
    $popupContainer.append($submitRow);
}

function createPopupRow() {
    return $('<div/>', {
        'class': 'pop-up-row row'
    });
}

function createPopupCol(colSize, colOffset) {
    let col = $('<div/>', {
        'class': 'pop-up-col col-md-' + colSize + (colOffset ? 'col-md-offset-' + colOffset : '')
    });
    return col;
}

function createRadioButtonsBlock($functionsColumn, type, options,
    entityLogicalName, entityDisplayName, popup) {

    let $radioButtonsBlock = $('<div/>', {
        'class': 'pop-up-radio-block col-md-4 col-md-offset-1'
    });

    $ul = $('<ul/>', {
        'class': 'pop-up-radio-buttons-list field-list'
    });

    let $nToOneListItem = $('<li/>', {
        'class': 'pop-up-list-item'
    });

    let $oneToNListItem = $('<li/>', {
        'class': 'pop-up-list-item'
    });

    let $noRelationListItem = $('<li/>', {
        'class': 'pop-up-list-item'
    });

    // NToOne is default
    let radioNToOne = true;
    let radioOneToN = false;
    let radioNoRelation = false;

    let relation = options.model[options.field] ?
        options.model[options.field].relation : undefined;

    if (relation === OneToN) {
        radioNToOne = false;
        radioOneToN = true;
        radioNoRelation = false;
    } else if (relation === NoRelation) {
        radioNToOne = false;
        radioOneToN = false;
        radioNoRelation = true;
    }

    let $nToOneRadioSpan = createRelationRadio(radioNToOne, NToOne, $functionsColumn, type, options,
        popup, 'Nto1Radio', entityLogicalName, entityDisplayName);
    let $oneToNRadioSpan = createRelationRadio(radioOneToN, OneToN, $functionsColumn, type, options,
        popup, 'OnetoNRadio', entityLogicalName, entityDisplayName);
    let $noRelationSpan = createRelationRadio(radioNoRelation, NoRelation, $functionsColumn, type, options,
        popup, 'NoRelationRadio', entityLogicalName, entityDisplayName);

    $nToOneListItem.append($nToOneRadioSpan);
    $ul.append($nToOneListItem);

    $oneToNListItem.append($oneToNRadioSpan);
    $ul.append($oneToNListItem);

    $noRelationListItem.append($noRelationSpan);
    $ul.append($noRelationListItem);

    $radioButtonsBlock.append($ul);
    return $radioButtonsBlock;
}

function createRelationRadio(selected, relationType, $functionsColumn, type, options,
    popup, radioId, entityLogicalName, entityDisplayName) {

    $label = $('<label/>', {
        'class': 'popup-radio-label k-radio-label',
        'for': radioId
    });
    $label.text(relationType);

    let $radio = $('<input/>', {
        id: radioId,
        type: 'radio',
        'class': 'pop-up-radio k-radio',
        name: 'output-popup-relation-radio',
        value: relationType
    });

    $radio.on('click', onRadioClickWrapper($functionsColumn, type, options, relationType,
        popup, entityLogicalName, entityDisplayName));

    if (selected) {
        $radio.attr('checked', 'checked');
        onRadioClickWrapper($functionsColumn, type, options, relationType,
            popup, entityLogicalName, entityDisplayName)();
    }

    $radioSpan = $('<span/>', {
        'class': 'popup-radio-span'
    });

    $radioSpan.append($radio);
    $radioSpan.append($label);

    return $radioSpan;
}

function onRadioClickWrapper($functionsColumn, type, options, relationType,
    popup, entityLogicalName, entityDisplayName) {

    let $popup = $(popup.document.body);

    return function () {
        let operationsConfig;

        clearMemory('#popup-output-functions','kendoDropDownList',$popup);
        clearMemory('#popup-fields-tree-view','kendoTreeView',$popup);
        clearMemory('#popup-operators-dropdown','kendoDropDownList',$popup);

        $popup.find('.k-list-container.k-popup.k-group.k-reset').remove();


        let $entityFieldsColumn = $popup.find('.output-popup-entity-feilds').eq(0);
        let $functionsDropdownSelector = $popup.find('.output-popup-functions.col-md-5');
        
        $functionsDropdownSelector.empty();
        $entityFieldsColumn.empty();

        $resultInput.val('');

        if (relationType === NToOne) {
            operationsConfig = outputPopupConfig[relationType][type];

            if ($entityFieldsColumn.length) {
                renderFieldsTreeView($entityFieldsColumn, entityLogicalName, entityDisplayName, popup);
            }
        } else if (relationType === OneToN) {
            operationsConfig = outputPopupConfig[relationType];
            Object.keys(operationsConfig)
                .forEach(key => {
                    if (typeof (operationsConfig[key]) !== 'object') {
                        delete operationsConfig[key];
                    }
                });

            if ($entityFieldsColumn.length) {
                renderFieldsTreeView($entityFieldsColumn, entityLogicalName, entityDisplayName, popup);
            }

        } else if (relationType === NoRelation) {
            operationsConfig = getOperationConfigForNoRelation(type);

            if ($entityFieldsColumn.length) {
                renderFieldsTreeView($entityFieldsColumn, entityLogicalName, entityDisplayName, popup);
            }
        }

        let operations = Object.keys(operationsConfig).map(key => {
            let text = operationsConfig[key].displayName ? operationsConfig[key].displayName : key;
            return {
                value: key,
                text: text
            };
        });

        let $row = $('<div/>', {
            class: 'row'
        });

        let $funcCol = $('<div/>', {
            class: 'col-md-6'
        });

        let $operCol = $('<div/>', {
            id: 'popup-operators-column',
            class: 'col-md-6'
        });

        $functionsColumn.html("");

        $functionsColumn.append($row);
        $row.append($funcCol);
        $row.append($operCol);

        createFunctionsColumn($funcCol, $functionDropDown, operations, getRelationType, getFunctionConfigForNoRelation,
            $popup, options, type, $resultInput);

        createOperatorsDropDown($popup, $operCol, relationType);

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}

function getFunctionConfigForNoRelation(type, funcLogicalName) {
    let funcConf = outputPopupConfig[NToOne][type][funcLogicalName];

    if (!funcConf) {
        funcConf = outputPopupConfig[OneToN][funcLogicalName];
    }

    return funcConf;
}

function getOperationConfigForNoRelation(type) {
    let result = {};

    let operationsConfigNToOne = outputPopupConfig[NToOne][type];
    Object.keys(operationsConfigNToOne)
        .forEach(key => {
            result[key] = operationsConfigNToOne[key];
        });

    let operationsConfigOneToN = outputPopupConfig[OneToN];
    Object.keys(operationsConfigOneToN)
        .forEach(key => {
            if (typeof (operationsConfigOneToN[key]) === 'object') {
                result[key] = operationsConfigOneToN[key];
            }
        });

    return result;
}

function replaceAllDisplayNames(operations, expression) {

    if (operations) {
        operations.forEach(item => {
            expression = expression.replace(
                new RegExp(item.text, 'g'),
                item.value
            );
        });
    }

    return expression;
}

module.exports = createPopUpForOutputWindow;


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

let editor = function (metadata) {
    let inputTypesOperationsConfig = __webpack_require__(20)
    let createFormForPopup = __webpack_require__(73);
    let createPopUpForOutputWindow = __webpack_require__(76);
    let dropEvent = __webpack_require__(72);

    //executeDmnPopup creator 
    let createExecuteDmnPopup = __webpack_require__(49);

    let data = metadata;
    let _windowId = '#window';

    let stringEdit = function (container, options) {
        let input = $('<input/>');
        input.attr("name", options.field);
        input.appendTo(container);
    };

    let _createDropdownForWindow = function (elementType, container, options, elementLabel = 'Editor') {
        let $dropdown = $('<div/>').attr('id', 'dropdown');
        $('#window').empty();

        $('#window').append($dropdown);
        $('<div/>', {
            id: 'popup-container'
        }).appendTo('#window');

        let typeConfig = inputTypesOperationsConfig[elementType];

        let operations = [];

        Object.keys(typeConfig).forEach((key) => {
            let text = typeConfig[key].displayName ?
                typeConfig[key].displayName :
                key;
            operations.push({
                value: key,
                text: text
            });
        });

        $dropdown.kendoDropDownList({
            dataSource: operations,
            dataTextField: "text",
            dataValueField: "value",
            optionLabel: 'Select operation',
            animation: {
                close: {
                    effects: "fadeOut zoom:out",
                    duration: 300
                },
                open: {
                    effects: "fadeIn zoom:in",
                    duration: 300
                },

            },
            select: function (e) {
                var dataItem = this.dataItem(e.item);
                let val = dataItem.value;
                let $container = createFormForPopup(elementType, val, container, options);
                $('#popup-container').empty()
                    .append($($container));
                dropEvent('#popup-container input.pop-up-form-input');
            },
            dataBound: function () {
                let fieldObj = options.model[options.field];

                if (fieldObj) {
                    let operator = fieldObj.operator;

                    if (operator) {
                        $dropdown.data('kendoDropDownList').value(operator);
                        let $container = createFormForPopup(elementType, operator, container, options);
                        $('#popup-container').empty()
                            .append($($container));
                        dropEvent('#popup-container input.pop-up-form-input');
                    }
                }
            }
        });

        let $window = $("#window").kendoWindow({
            width: "600px",
            visible: false,
            actions: [
                "Close"
            ],
            animation: false
        });
        let windowData = $window.data("kendoWindow");
        windowData.center().open();
        windowData.title(elementLabel); //fix bug with unchanged title
    };

    return function (container, options) {

        let field = options.field;
        //Output editor
        if (field.indexOf('_Output') !== -1) {
            let dataElement = data.find(d => d.fieldName + '_Output' === field);
            let type = dataElement.type.toLowerCase();
            if (type === 'executedmn') {
                createExecuteDmnPopup(type, container, options, dataElement.title, dataElement.entityName);
            } else {
                createPopUpForOutputWindow(type, container, options, dataElement.title, dataElement.entityName);
            }
            //----------------------------------------------------------------

            //Input editor
        } else {

            let logicalConfig = (__webpack_require__(6))['logical'];

            let isNewPropertyParse = field.indexOf(logicalConfig.openBracket) !== -1;

            if (isNewPropertyParse) {
                let multipleInputService = __webpack_require__(80);
                let inputIndexString = multipleInputService.getValueWithBrackets(logicalConfig.openBracket,
                    logicalConfig.closeBracket,
                    field);

                let dataElement = data.find(d => d.fieldName + `_Input${inputIndexString}` === field);
                let type = dataElement.type.toLowerCase();
                _createDropdownForWindow(type, container, options, dataElement.title, dataElement.entityName);
            } else {
                let dataElement = data.find(d => d.fieldName + `_Input` === field);
                let type = dataElement.type.toLowerCase();
                _createDropdownForWindow(type, container, options, dataElement.title, dataElement.entityName);
            }


        }

        let value = options.model[options.field];
        value = value && value.view ? value.view : '';

        $('<span>' + value + '</span>')
            .appendTo(container);
    }
};

module.exports = editor;


/***/ }),
/* 78 */
/***/ (function(module, exports) {

let clearMemory = function(selector, kendoDataType,popup ){
    let kendoElement = popup.find(selector).data(kendoDataType);

        if(kendoElement && kendoElement.destroy){
            kendoElement.destroy();
        }
}

module.exports = clearMemory;

/***/ }),
/* 79 */
/***/ (function(module, exports) {

let createInputsRow = function(){
    let inputField = $('<input/>', {
        id:'popup-search-input-field',
        class:'k-textbox',
        type:'text',
        placeholder:'Search...'
    }).css('width','100%');

    let inputButton = $('<input/>', {
        id:'popup-search-input-button',
        class:'k-button',
        value:'Search',
        type:'button'
    }).css('width','100%');

    let fieldColmd = $('<div/>',{
        class:'col-md-9'
    });

    let buttonColmd = $('<div/>',{
        class:'col-md-3'
    });

    let row = $('<div/>',{
        class:'row'
    });  

    fieldColmd.append(inputField);
    buttonColmd.append(inputButton);
    row.append(fieldColmd)
        .append(buttonColmd);

    return row;
        
}

let createSearch = function(popupTreeViewElement, parentDiv){

    let row = $('<div/>',{
        class:'row'
    }); 
    let searchDiv = createInputsRow();

    row.append(popupTreeViewElement);

    parentDiv.append(searchDiv)
            .append(row);
}

module.exports = createSearch;

/***/ }),
/* 80 */
/***/ (function(module, exports) {

let multipleInputCreateService = (function () {
    let getValueBetweenBrackets = function (leftBracketString, rightBracketString, targetString) {
        try {
            let leftBracketIndex = targetString.indexOf(leftBracketString);
            let startValueIndex = leftBracketIndex + leftBracketString.length;

            let rightBracketIndex = targetString.indexOf(rightBracketString, startValueIndex);

            if (rightBracketIndex === -1 || leftBracketIndex === -1) {
                throw 'string doesn\'t contain brackets';
            }

            let result = targetString.substring(startValueIndex, rightBracketIndex);
            return result;

        } catch (error) {
             
        }
    }

    let clearFromBrackets = function (leftBracketString, rightBracketString, targetString) {
        try {
            let leftBracketIndex = targetString.indexOf(leftBracketString);
            let rightBracketIndex = targetString.indexOf(rightBracketString);

            let rightDiapasone = rightBracketIndex + rightBracketString.length;

            let leftResultString = targetString.substring(0, leftBracketIndex);
            let rightResultString = targetString.substring(rightDiapasone);

            let result = leftResultString + rightResultString;
            return result;

        } catch (error) {
             
        }
    }

    let getValueWithBrackets = function (leftBracketString, rightBracketString, targetString) {
        let leftBracketIndex = targetString.indexOf(leftBracketString);
        let rightBracketIndex = targetString.indexOf(rightBracketString);

        let rightDiapasone = rightBracketIndex + rightBracketString.length;

        let result = targetString.substring(leftBracketIndex, rightDiapasone);

        return result;
    }

    return {
        getValueBetweenBrackets,
        clearFromBrackets,
        getValueWithBrackets
    }
})();

module.exports = multipleInputCreateService;

/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

let getDisplayName = __webpack_require__(10);

let appendTreeItems = function (popup, treeView, dataArray) {
    dataArray.forEach(el => {
        let LogicalName = el.LogicalName;
        let DisplayName = getDisplayName(el);

        if(!DisplayName){
            return;
        }

        let insertObject = {
            text: DisplayName,
            type: 'Lookup',
            title: DisplayName,
            fieldName: LogicalName,
            target: LogicalName,
            logicalParent: '',
            displayParent: '',
            items: [{
                text: 'Loading...'
            }]
        }

        let html = treeView.append(insertObject);
        popup.find(html).closest('li').addClass('lookup-li');
    });
}

module.exports = appendTreeItems;



/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

let getDisplayName = __webpack_require__(10);

let appendTreeItems = function (popup, treeView, dataArray, displayName) {
    dataArray.forEach(el => {
        let LogicalName = el.LogicalName;
        let DisplayName = getDisplayName(el);

        if(!DisplayName){
            return;
        }

        let insertObject = {
            text: DisplayName,
            type: el.AttributeType,
            title: DisplayName,
            fieldName: LogicalName,
            logicalParent: el.EntityLogicalName,
            displayParent: displayName           
        }

        if(el.AttributeType === 'Lookup'){
            insertObject.items =[{
                text: 'Loading...'
            }];
            insertObject.target = el.Targets[0];
        }

        let html = treeView.append(insertObject);
        if(el.AttributeType === 'Lookup'){
            popup.find(html).closest('li').addClass('lookup-li');
        };
    });
}

module.exports = appendTreeItems;

/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

let getDisplayName = __webpack_require__(10);

let appendTreeItems = function (popup, treeView, dataArray, displayParent, logicalParent) {
    dataArray.forEach(el => {
        let LogicalName = el.LogicalName;
        let DisplayName = getDisplayName(el);

        if(!DisplayName){
            return;
        }

        let insertObject = {
            text: DisplayName,
            type: 'Lookup',
            title: DisplayName,
            fieldName: LogicalName,
            logicalParent: logicalParent,
            displayParent: displayParent,
            target: LogicalName,
            items: [{
                text: 'Loading...'
            }]
        }



        let html = treeView.append(insertObject);
        popup.find(html).closest('li').addClass('lookup-li');
    });
}

module.exports = appendTreeItems;

/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

let treeViewForPopup = function (id, entityLogicalName, entityDisplayName, popup) {
    let _converter = __webpack_require__(2);
    let toolsForPopupOutput = __webpack_require__(5);
    let baseUrl = parent.Xrm.Page.context.getClientUrl();
    let relationTypes = __webpack_require__(4);
    let outputPopupConfig = __webpack_require__(0);

    const NToOne = relationTypes.NToOne;
    const OneToN = relationTypes.OneToN;
    const NoRelation = relationTypes.NoRelation;

    let $popup = $(popup.document.body);

    startProcessCreatingTreeView(entityLogicalName, entityDisplayName);

    function createKendoTreeView() {
        $popup.find(id).kendoTreeView({
            animation: false,
            expand: expandFunction(id),
            select: ev => {
                let dataSourceRowElement = $popup.find('#popup-fields-tree-view').data('kendoTreeView').dataItem(ev.node);

                let logicalName = dataSourceRowElement.fieldName ? dataSourceRowElement.fieldName : dataSourceRowElement.target;
                let logicalResult = dataSourceRowElement.logicalParent + '.' + logicalName;
                logicalResult = logicalResult.replace('undefined.', '');

                let displayResult = dataSourceRowElement.displayParent + '.' + dataSourceRowElement.text;
                displayResult = displayResult.replace('undefined.', '');

                if (logicalResult[0] === '.' && displayResult[0] === '.') {
                    logicalResult = logicalResult.substr(1);
                    displayResult = displayResult.substr(1);
                }

                let $textarea = $popup.find('.output-popup-result-input');
                toolsForPopupOutput.insertToTextarea($textarea, logicalResult);
            }
        });
    }

    function createTreeViewFromCache(cachedDataSource) {
        let _$treeView;
        $popup.find(id).empty();

        createKendoTreeView();
        _$treeView = $popup.find(id).data('kendoTreeView');

        cachedDataSource.forEach(item => _$treeView.append(item));

    };

    function createTreeView(data, entityLogicalName, displayLogicalName) {
        if (!window.top.cache) {
            window.top.cache = {};
        }
        console.log('Creating Tree View');
        console.log(data);
        createKendoTreeView();

        let treeView = $popup.find('#popup-fields-tree-view').data('kendoTreeView');

        let ulIsMarked = false;

        let relationType = popup.getRelationType($popup);

        if (treeView) {
            if (relationType === 'NoRelation') {
                let convert = __webpack_require__(81);
                convert($popup, treeView, data);
            } else if (relationType === 'N:1') {
                let convert = __webpack_require__(82);
                convert($popup, treeView, data, displayLogicalName);
            } else if (relationType === '1:N') {
                let convert = __webpack_require__(83);
                convert($popup, treeView, data, displayLogicalName, entityLogicalName);
            }
            window.top.cache[relationType] = treeView.dataSource.data();
        }
    }

    function appendWayInformation(html, logicalName, displayEntityName) {

        let kendoItem = $popup.find('#popup-fields-tree-view').data('kendoTreeView').dataItem(html);

        kendoItem.logicalName = logicalName;
        kendoItem.logicalParent = logicalName;
        kendoItem.displayParent = displayEntityName;


    }

    function expandFunction(_id) {
        return function (e) {

            let node = e.node;
            let length = $popup.find('#popup-fields-tree-view').data('kendoTreeView').dataItem(e.node).items.length;
            if (length <= 1) {

                let curentObject = $popup.find(_id).data("kendoTreeView").dataItem(node);
                let target = curentObject.target;
                let linkedField = curentObject.fieldName;
                (function (name, linkedField, _id) {
                    $.ajax({
                        type: 'GET',
                        //url: Xrm.Page.context.getClientUrl() +`/api/data/v8.1/EntityDefinitions(${id})?$select=LogicalName&$expand=Attributes($select=DisplayName,LogicalName,AttributeType, EntityLogicalName, )`,
                        url: parent.Xrm.Page.context.getClientUrl() + `/api/data/v8.1/EntityDefinitions()?$select=LogicalName,DisplayName&$expand=Attributes&$filter= LogicalName eq '${name}'`,
                        headers: {
                            'Accept': 'application/json',
                            "Content-Type": "application/json; charset=utf-8",
                            'OData-MaxVersion': "4.0",
                            "OData-Version": "4.0"
                        }
                    }).done(item => {

                        let emptyDeleted = false;
                        let displayEntityName;

                        try {
                            displayEntityName = item.value[0].DisplayName.LocalizedLabels[0].Label;
                        } catch (e) {
                            displayEntityName = logicalName;
                        }

                        let _treeView = $popup.find(_id).data("kendoTreeView");
                        let recievedAttributes = item.value[0].Attributes;
                        let logicalName = item.value[0].LogicalName;
                        let convertedArray = _converter.convertCrmDataArray(recievedAttributes, item.value[0].LogicalName);


                        convertedArray.forEach(attribute => {
                            let logicalParent = `${curentObject.logicalParent}.${linkedField}`;
                            let displayParent = `${curentObject.displayParent}.${displayEntityName}`;

                            if (attribute.type === 'Lookup') {
                                let html = _treeView.append({
                                    text: attribute.title,
                                    entityName: attribute.entityName,
                                    fieldName: attribute.fieldName,
                                    displayName: attribute.title,
                                    type: attribute.type,
                                    logicalParent: logicalParent,
                                    displayParent: displayParent,
                                    target: attribute.targets[0],
                                    items: [{
                                        text: 'Loading...'
                                    }]
                                }, $popup.find(node));
                                $popup.find(html).closest('li').addClass('lookup-li');

                                if (!emptyDeleted) {
                                    let loadingPlace = _treeView.findByText('Loading...');
                                    _treeView.remove(loadingPlace);
                                    emptyDeleted = true;
                                }
                            } else {
                                let html = _treeView.append({
                                    text: attribute.title,
                                    entityName: attribute.entityName,
                                    fieldName: attribute.fieldName,
                                    displayName: attribute.title,
                                    type: attribute.type,
                                    logicalParent: logicalParent,
                                    displayParent: displayParent,
                                }, $(node));

                                if (!emptyDeleted) {
                                    let loadingPlace = _treeView.findByText('Loading...');
                                    _treeView.remove(loadingPlace);
                                    emptyDeleted = true;
                                }
                            }
                        });

                        let relationType = popup.getRelationType($popup);
                        window.top.cache[relationType] = _treeView.dataSource.data();
                    });
                })(target, linkedField, _id);
            }
        }
    }

    function startProcessCreatingTreeView(entityLogicalName, displayLogicalName) {
        kendo.ui.progress($(window.popup.document.body), true);
        entityLogicalName = entityLogicalName ? entityLogicalName.split('.')[0] : entityLogicalName;

        let url = baseUrl + "/api/data/v8.1/RelationshipDefinitions" +
            "/Microsoft.Dynamics.CRM.OneToManyRelationshipMetadata" +
            "?$select=ReferencingEntity&$filter=ReferencedEntity eq '" + entityLogicalName + "'";

        let relationType = popup.getRelationType($popup);

        if (window.top.cache && window.top.cache[relationType]) {
            let cachedDataSource = window.top.cache[relationType];
            createTreeViewFromCache(cachedDataSource);
            kendo.ui.progress($(window.popup.document.body), false);
        } else {
            if (relationType === NToOne) {

                url = baseUrl + `/api/data/v8.1/EntityDefinitions()?$select=LogicalName,DisplayName&$expand=Attributes&$filter= LogicalName eq '${entityLogicalName}'`



            } else if (relationType === NoRelation) {
                url = baseUrl + '/api/data/v8.1/EntityDefinitions?$select=DisplayName,LogicalName';
            }

            $.ajax({
                type: 'GET',
                url: url,
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "application/json; charset=utf-8",
                    'OData-MaxVersion': "4.0",
                    "OData-Version": "4.0"
                }
            }).done(entities => {
                kendo.ui.progress($(window.popup.document.body), false);
                if (relationType === OneToN) {
                    if (!entities.value || !Array.isArray(entities.value) || !entities.value.length) {
                        throw "Received data is not valid (Get all related entities)";
                    }

                    let urls = [];
                    let urlsCount = 0;

                    urls[urlsCount] = baseUrl + "/api/data/v8.1/EntityDefinitions" +
                        "?$select=LogicalName,DisplayName" +
                        "&$filter=LogicalName eq '" + entities.value[0].ReferencingEntity + "'";

                    // It's limitation for CRM Web Api
                    const MAX_NUMBER_OF_FILTERS = 25;

                    for (let i = 1, numberOfFilters = 1; i < entities.value.length; i++ , numberOfFilters++) {
                        if (numberOfFilters === MAX_NUMBER_OF_FILTERS) {
                            numberOfFilters = 0;
                            urlsCount++;

                            urls[urlsCount] = baseUrl + "/api/data/v8.1/EntityDefinitions" +
                                "?$select=LogicalName,DisplayName" +
                                "&$filter=LogicalName eq '" + entities.value[i].ReferencingEntity + "'";

                            continue;
                        }

                        urls[urlsCount] += " or LogicalName eq '" + entities.value[i].ReferencingEntity + "'";
                    }

                    let deferreds = [];
                    let allEntities = [];

                    urls.forEach(url => {
                        deferreds.push($.ajax({
                            type: 'GET',
                            url: url,
                            headers: {
                                'Accept': 'application/json',
                                "Content-Type": "application/json; charset=utf-8",
                                'OData-MaxVersion': "4.0",
                                "OData-Version": "4.0"
                            }
                        }).success(entities => {
                            allEntities = allEntities.concat(entities.value);
                            console.log('Popup result: ');
                            console.log(allEntities);
                        }));
                    });

                    $.when.apply($, deferreds).done(function () {

                        allEntities.sort(compare);

                        createTreeView(allEntities, entityLogicalName, displayLogicalName);
                    });
                } else if (relationType === NoRelation) {
                    let allEntities = entities.value.filter(item=>item.DisplayName.LocalizedLabels[0]);
                    allEntities.sort(compare);

                    createTreeView(allEntities, entityLogicalName, displayLogicalName);
                } else if (relationType === NToOne) {
                    let allEntities = entities.value[0].Attributes.filter(item=>item.DisplayName.LocalizedLabels[0]);
                    allEntities.sort(compare);

                    createTreeView(allEntities, entityLogicalName, displayLogicalName);
                }

                function compare(first, second) {
                    let labelFirst = getDisplayName(first);
                    let labelSecond = getDisplayName(second);

                    if (labelFirst < labelSecond)
                        return -1;
                    if (labelFirst > labelSecond)
                        return 1;
                    return 0;
                }
            });
        }


    }

    function getDisplayName(item) {
        let label = item.DisplayName.LocalizedLabels[0].Label;
            

        return label;
    }
}

module.exports = treeViewForPopup;


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

let treeViewBuilder = (function () {
    let _id;
    let _converter = __webpack_require__(2);
    let searchEvent = __webpack_require__(15);
    let _baseUrl = parent.Xrm.Page.context.getClientUrl();
    let setId = function (id) {
        _id = id;
    };

    let _addDraggableToLiElements = function () {
        $('#treeview li:has(ul)')
            .addClass('lookup-li')
            .off();

        $(_id).find('li').not('.lookup-li').not('[data-role=draggable]').kendoDraggable({
            hint: function (element) {
                return element.clone();
            },
            dragstart: function (e) {
                e.currentTarget.hide();
                $('#grid thead>tr:first>th[data-title="Input"],#grid thead>tr:first>th[data-title="Output"]').addClass('animation');
            },
            dragend: function (e) {
                $('#grid thead>tr:first>th[data-title="Input"],#grid thead>tr:first>th[data-title="Output"]').removeClass('animation');
                e.currentTarget.show();
            }
        });
    };

    let createInitTreeForLogicalName = function (logicalName) {
        (function (name) {
            $.ajax({
                type: 'GET',
                url: _baseUrl + `/api/data/v8.1/EntityDefinitions()?$select=LogicalName,DisplayName&$expand=Attributes&$filter= LogicalName eq '${name}'`,
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "application/json; charset=utf-8",
                    'OData-MaxVersion': "4.0",
                    "OData-Version": "4.0"
                }
            }).done(item => {
                let displayEntityName;

                let _treeView = $(_id).data("kendoTreeView");
                let recievedAttributes = item.value[0].Attributes;
                let logicalName = item.value[0].LogicalName;
                try {
                    displayEntityName = item.value[0].DisplayName.LocalizedLabels[0].Label;
                } catch (e) {
                    displayEntityName = logicalName;
                }

                let convertedArray = _converter.convertCrmDataArray(recievedAttributes, logicalName, true);

                convertedArray.forEach(attribute => {

                    if (attribute.type === 'Lookup') {

                        let html = _treeView.append({
                            text: attribute.title,
                            entityName: attribute.entityName,
                            logicalName: attribute.fieldName,
                            displayName: attribute.title,
                            type: attribute.type,
                            logicalParent: logicalName,
                            displayParent: displayEntityName,
                            target: attribute.targets[0],
                            items: [
                                { text: 'Loading...' }
                            ]
                        });
                        $(html).closest('li').addClass('lookup-li');

                    } else {
                        let html = _treeView.append({
                            text: attribute.title,
                            entityName: attribute.entityName,
                            logicalName: attribute.fieldName,
                            displayName: attribute.title,
                            type: attribute.type,
                            logicalParent: logicalName,
                            displayParent: displayEntityName
                        });

                    }
                });
                
                searchEvent({
                    buttonSelector: '#search-button',
                    textSelector: '#search-text-field',
                    treeViewLiSelector: '#treeview li',
                    spinnerContainer: "#loader",
                    treeViewContainer: "#treeview",
                    draggableRequried:true

                });
                _addDraggableToLiElements(); 
                kendo.ui.progress($('#main_container'), false);
            });
        })(logicalName);
 
    }

    let initTree = function (eventObj) {

        let expandFunction = function (e) {
 
            let node = e.node;
            let length = $(node).find('li').length;
            if (length <= 1) {
                let curentObject = $(_id).data('kendoTreeView').dataItem(node);
                let target = curentObject.target;
                let linkedField = curentObject.logicalName;
                (function (name, linkedField) {
                    $.ajax({
                        type: 'GET',
                        url: _baseUrl + `/api/data/v8.1/EntityDefinitions()?$select=LogicalName,DisplayName&$expand=Attributes&$filter= LogicalName eq '${name}'`,
                        headers: {
                            'Accept': 'application/json',
                            "Content-Type": "application/json; charset=utf-8",
                            'OData-MaxVersion': "4.0",
                            "OData-Version": "4.0"
                        }
                    }).done(item => {
                        let emptyDeleted = false;

                        try {
                            displayEntityName = item.value[0].DisplayName.LocalizedLabels[0].Label;
                        } catch (e) {
                            displayEntityName = logicalName;
                        }

                        let _treeView = $(_id).data("kendoTreeView");
                        let recievedAttributes = item.value[0].Attributes;
                        let logicalName = item.value[0].LogicalName;
                        let convertedArray = _converter.convertCrmDataArray(recievedAttributes, item.value[0].LogicalName);

                        convertedArray.forEach(attribute => {
                            let logicalParent = `${curentObject.logicalParent}.${linkedField}`;
                            let displayParent = `${curentObject.displayParent}.${displayEntityName}`;

                            if (attribute.type === 'Lookup') {
                                let html = _treeView.append({
                                    text: attribute.title,
                                    entityName: attribute.entityName,
                                    logicalName: attribute.fieldName,
                                    displayName: attribute.title,
                                    type: attribute.type,
                                    logicalParent: logicalParent,
                                    displayParent: displayParent,
                                    target: attribute.targets[0],
                                    items: [
                                        { text: 'Loading...' }
                                    ]
                                }, $(node));
                                $(html).closest('li').addClass('lookup-li');

                                if (!emptyDeleted) {
                                    let loadingPlace = _treeView.findByText('Loading...');                                    
                                    _treeView.remove(loadingPlace);
                                    emptyDeleted = true;
                                }
                            } else {
                                let html = _treeView.append({
                                    text: attribute.title,
                                    entityName: attribute.entityName,
                                    logicalName: attribute.fieldName,
                                    displayName: attribute.title,
                                    type: attribute.type,
                                    logicalParent: logicalParent,
                                    displayParent: displayParent,
                                }, $(node));

                                if (!emptyDeleted) {
                                    let loadingPlace = _treeView.findByText('Loading...');
                                    _treeView.remove(loadingPlace);
                                    emptyDeleted = true;
                                }
                            }
                        });
                        _addDraggableToLiElements();
                    });
                })(target, linkedField);
            }
        };

        let settings = {
            animation: {
                expand: {
                    duration: 200
                },
                collapse: {
                    duration: 200
                }
            },
            expand: expandFunction
        };
        let $treeView = $(_id).data('kendoTreeView');

        if (eventObj) {
            Object.keys(eventObj).forEach(key => settings[key] = eventObj[key]);
            if($treeView){
                $treeView.destroy(); 
            }
            $(_id).empty().kendoTreeView(settings);
        } else {
            if($treeView){
                $treeView.destroy();
            }
            $(_id).empty().kendoTreeView(settings);
        }
    };
 
    return {
        setId,
        initTree,
        createInitTreeForLogicalName

    };
})();

module.exports = treeViewBuilder;


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

$(document).ready(function () {
  let createTable = __webpack_require__(22);
  let readyFunc = __webpack_require__(23);
  kendo.ui.progress($('#main_container'), true);
  window.top.createTable = createTable;
  readyFunc();
});


/***/ })
/******/ ]);
//# sourceMappingURL=bundle.js.map