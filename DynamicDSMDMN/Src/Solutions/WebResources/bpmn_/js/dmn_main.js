/*window.$.ajax({
    url: "/WebResources/accentgold_/SDK/SDK.MetaData.js",
    dataType: "script",
    cache: true
}).done(function() {
    newNamespace();
});

function newNamespace() {
    AccentGold = window.AccentGold;
    if (typeof(AccentGold) == "undefined") {
        setTimeout(newNamespace(), 100);
    } else {
        AccentGold.Entity.namespace("Mapping").extend((function() {

            var createEntitiesOptionSet = function(crmEntityFieldName, crmAttributeFieldName) {
                    var entities = AccentGold.Entity.getEntitiesList(),
                        entitiesList = [],
                        crmEntityFieldName = crmEntityFieldName,
                        crmAttributeFieldName = crmAttributeFieldName;

                    var EntityNameList = $("<select>").attr("id", "Select_" + crmEntityFieldName[0]).attr("name", "Select_" + crmEntityFieldName[0]).attr("class", "ms-crm-SelectBox ms-crm-Inline-OptionSet-AutoOpen");
                    EntityNameList.append($('<option>', {
                        value: "",
                        text: ""
                    }));

                    for (var i = 0; i < entities.length; i++) {
                        EntityNameList.append($('<option>', {
                            value: entities[i].SchemaName,
                            text: (!!entities[i].DisplayName.UserLocalizedLabel.Label) ? entities[i].DisplayName.UserLocalizedLabel.Label : entities[i].SchemaName
                        }));
                    }

                    EntityNameList.change(function() {
                        Xrm.Page.getAttribute(crmEntityFieldName[0]).setValue($(this)[0][$(this)[0].selectedIndex].text);
                        Xrm.Page.getAttribute(crmEntityFieldName[1]).setValue($(this)[0][$(this)[0].selectedIndex].value);

                        var entity = Xrm.Page.getAttribute(crmEntityFieldName[1]);

                        if (entity && entity.getValue()) {
                            window.top.entityAttrs = AccentGold.Entity.getAttributesList(entity.getValue());
                        }

                        //.$("#Select_" + crmAttributeFieldName[0]).remove();
                        //createAttributesOptionSet(Xrm.Page.getAttribute(crmEntityFieldName[0]).getValue(), crmAttributeFieldName);
                    }).trigger('change');

                    var containerName = "#" + crmEntityFieldName[0] + "_i";
                    var parentContainer = $(containerName).parent();
                    $(parentContainer).append(EntityNameList);

                },

                createAttributesOptionSet = function(entity, crmAttributeFieldFormObj) {

                    var entity = entity,
                        attributesList = [],
                        crmAttributeFieldFormObj = crmAttributeFieldFormObj;
                    if (entity !== "") {
                        var attributes = AccentGold.Entity.getAttributesList(entity);

                        console.dir(attributes);
                        var AttributeNameList = $("<select>").attr("id", "Select_" + crmAttributeFieldFormObj[0]).attr("name", "Select_" + crmAttributeFieldFormObj[0]).attr("class", "ms-crm-SelectBox ms-crm-Inline-OptionSet-AutoOpen");
                        AttributeNameList.append($('<option>', {
                            value: "",
                            text: ""
                        }));
                        for (var i = 0; i < attributes.length; i++) {
                            AttributeNameList.append($('<option>', {
                                value: (attributes[i].SchemaName + "|" + attributes[i].AttributeType),
                                text: (!!attributes[i].DisplayName.UserLocalizedLabel) ? attributes[i].DisplayName.UserLocalizedLabel.Label : attributes[i].SchemaName
                            }));
                        }
                        AttributeNameList.change(function() {
                            Xrm.Page.getAttribute(crmAttributeFieldFormObj[0]).setValue($(this)[0][$(this)[0].selectedIndex].text);
                            Xrm.Page.getAttribute(crmAttributeFieldFormObj[1]).setValue(($(this).val() != "") ? ($(this).val()).split("|")[1] : "");
                            Xrm.Page.getAttribute(crmAttributeFieldFormObj[2]).setValue(($(this).val() != "") ? ($(this).val()).split("|")[0] : "");

                            if (crmAttributeFieldFormObj[0] != "ddsm_targetfield" &&
                                Xrm.Page.getAttribute("ddsm_targetfieldtype").getValue() != Xrm.Page.getAttribute(crmAttributeFieldFormObj[1]).getValue()) {
                                Xrm.Page.getAttribute("ddsm_targetfieldtype").setValue("");
                                Xrm.Page.getAttribute("ddsm_targetfieldlogicalname").setValue("");
                                Xrm.Page.getAttribute("ddsm_targetfield").setValue("");
                            }
                        }).trigger('change');
                        var containerName = "#" + crmAttributeFieldFormObj[0] + "_i";
                        var parentContainer = $(containerName).parent();
                        $(parentContainer).append(AttributeNameList);
                    }
                };
            return {
                createEntitiesOptionSet: createEntitiesOptionSet,
                createAttributesOptionSet: createAttributesOptionSet
            };

        })());
    }
}
*/

function onLoadForm() {

}

function resortArrays(sourceArray, targetArray) {
    let tempArray = [];
    targetArray.forEach(item => {
        let fieldName = item.field;

        let element = sourceArray.find(e => e.field === fieldName);
        tempArray.push(element);
    });

    return tempArray;
}




function onSave(executionObj) {
    // from bundle.js for convert grid data to xml
    window.top.parserClickHandler();

    let gridBuilder = window.top.gridBuilder;

    let savedInput = gridBuilder.getInputColumns();
    let savedOutput = gridBuilder.getOutputColumns();

    let currentColumns = window.top.getCurrentColumns();

    let sortedInput = resortArrays(savedInput, currentColumns.inputs);
    let sortedOutput = resortArrays(savedOutput, currentColumns.outputs);

    gridBuilder.setInputColumns(sortedInput);
    gridBuilder.setOutputColumns(sortedOutput);

    //if true - it will hide typedropdwn animately
    window.top.lockdropDown(true);
    var modelerFrame = window.document.getElementById('WebResource_modeler').contentWindow;

    let jsonString = window.top.getDataContainerAndDataSourceJSON();

    Xrm.Page.getAttribute("ddsm_jsontabledata").setValue(jsonString);

    var columns = window.top.getColumns();
    var columnsStr = window.kendo.stringify(columns);
    Xrm.Page.getAttribute("ddsm_jsonheaderschema").setValue(columnsStr);

}