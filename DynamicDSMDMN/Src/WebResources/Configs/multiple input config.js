let bracketConfig = {
    logical:{
        openBracket:'<#',
        closeBracket:'#>'
    },
    title:{
        openBracket:'</br index=',
        closeBracket:'>'
    }
}

module.exports = bracketConfig;