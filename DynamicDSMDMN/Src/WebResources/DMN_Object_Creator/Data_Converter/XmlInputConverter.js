class XmlInputConverter{
    _createXmlString(operator, args){
        let xmlStringBegin = `$${operator}^`;
        let length = args.length;
        let lastElement = length - 1;

        for(let i = 0; i < length; i++){
            xmlStringBegin +=args[i];
            if(i !== lastElement){
                xmlStringBegin +=',';
            }
        }

        return xmlStringBegin;
    }

    convertObject(targetObject){
        let operator=  targetObject.operator;
        let convertedString = this._createXmlString(operator, targetObject.args);

        return convertedString;

    }
}

module.exports = XmlInputConverter;