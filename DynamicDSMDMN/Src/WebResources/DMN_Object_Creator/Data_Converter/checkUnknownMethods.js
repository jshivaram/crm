let checkUnknowMethodNames = function(targetString, methodArray){
    let regExpr = /[\s\W](\w*?)\(/g;
    let match = regExpr.exec(targetString);

    while(match != null){
        let matchedGroup = match[1];
        if(matchedGroup===''){
            continue;
        }
        let contains = methodArray.includes(matchedGroup);

        if(!contains){
            throw "The method is unknown";
        }
        match = regExpr.exec(targetString);
    }


}

module.exports = checkUnknowMethodNames;