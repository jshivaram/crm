function replaceAllFields(baseString, fields) {
    if (fields == undefined) { return baseString; }
    fields.forEach(item => {
        let arrDisplay = item.displayName.split(".");
        let arrLogical = item.logicalName.split(".");
        for (let i = 0; i < arrDisplay.length; i++) {
            baseString = baseString.replace(arrDisplay[i], arrLogical[i]);
        }
    });

    return baseString;
}

module.exports = replaceAllFields;
