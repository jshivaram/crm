let executeDmnConfig = (require('../../Row_edditor/outputTypesOperationsConfig'))['executeDmnXmlTemplate'];

let _convertExecuteDmn = function(object){
    let businessRuleId = object.businessRuleId;
    let relationName = object.relationName;

    let functionName = executeDmnConfig.functionName;
    let funcSeparator = executeDmnConfig.functionSeparator;
    let argsSeparator = executeDmnConfig.argsSeparator;



    return `${functionName}${funcSeparator}${businessRuleId}${argsSeparator}${relationName}`;
}



let convertExecuteDmn = function(object){
    try {
        return _convertExecuteDmn(object);
    } catch (error) {
        console.log('Error with execute Dmn converter');
        console.log(error);
        throw error;
    }
}


module.exports = convertExecuteDmn;