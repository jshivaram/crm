let functionConverter = (function () {
    let OneToN = require('../../OneToNXmlConverter/OneToNXmlConverter');
    let NToOneXmlConverter = require('../../NToOneXmlConverter/NToOneXmlConverter');
    let config = require('../../Row_edditor/outputTypesOperationsConfig');
    let SingleFieldsConverter = require('./singleFieldsConverter');
    let checkUnknowMethods = require('./checkUnknownMethods');
    let wrapOutputValue = require('./outputValueVrapper');

    let globalFunctionsNames = Object.keys(config.GlobalFunctions);
    let nToOneConverter;

    let currentEntity = parent.Xrm.Page.getAttribute("ddsm_targetentity").getValue();
    let oneToNXmlConverter = new OneToN(currentEntity);

    let _convert = function (object) {
        let objectType = object.datatype;
        let singleFieldsConverter =  SingleFieldsConverter(objectType);

        if (object.relation === 'N:1') {
            let nToOneCaseConfig = config['N:1'][objectType];

            nToOneConverter = new NToOneXmlConverter(nToOneCaseConfig);
            let convertedString = nToOneConverter.convertString(object.expression.value);
            let singleFieldsConverted = singleFieldsConverter(convertedString);

            if(objectType === "outputvalue"){   
                singleFieldsConverted = wrapOutputValue(singleFieldsConverted);
            }

            return singleFieldsConverted;

        } else if (object.relation === '1:N') {

            let convertedString = oneToNXmlConverter.convertStringToXml(object.expression.value);
            let singleFieldsConverted = singleFieldsConverter(convertedString);    

            if(objectType === "outputvalue"){   
                singleFieldsConverted = wrapOutputValue(singleFieldsConverted);
            }

            return singleFieldsConverted;

        } else if (object.relation === 'NoRelation') {
            let nToOneCaseConfig = config['N:1'][objectType];
            let oneToNCaseConfig = config['1:N'];

            nToOneConverter = new NToOneXmlConverter(nToOneCaseConfig);
            let convertedNToOneString = nToOneConverter.convertString(object.expression.value);

            let string = oneToNXmlConverter.convertStringToXml(convertedNToOneString);

            let singleFieldsConverted = singleFieldsConverter(string);  

            if(objectType === "outputvalue"){   
                singleFieldsConverted = wrapOutputValue(singleFieldsConverted);
            }
            
            return singleFieldsConverted;
        }
    };

    let convert = function (object) {
        try {
            return _convert(object);
        } catch (e) {
            console.log(e);
            console.log(e.message);

            throw 'errored message';
        }
    }

    return convert;
})();

module.exports = functionConverter;