let SingleFieldConverter = (function(type) {

        let config = require('../../Row_edditor/outputTypesOperationsConfig');
        let nToOneCaseConfig = config['N:1'][type];
        let oneToNCaseConfig = config['1:N'];
        let globalFunctions = config['GlobalFunctions'];

    

    let _isNotContainsFromConfig = function(item) {
        
        let _isContainsOperation = function (configObject, expressionString) {
            let keyArray = Object.keys(configObject);
            let isContains = false;

            for (let i = 0, length = keyArray.length; i < length; i++) {

                let searchedSubString = keyArray[i];
                if (expressionString.indexOf(searchedSubString + '^') !== -1) {
                    isContains = true;
                    break;
                }
            }
            return isContains;
        }
        let nToOneContains = _isContainsOperation(oneToNCaseConfig, item);
        let OneToNContains;
        if (!nToOneContains) {
            OneToNContains = _isContainsOperation(nToOneCaseConfig, item);
        }

        return !(nToOneContains || OneToNContains);
    }
    let distinct = function(stringArray){
        let length = stringArray.length;
        let resultArray=  [];
        for(let i = 0; i < length; i++){
            let trimed = stringArray[i].trim();
            if(resultArray.includes(trimed)){
                continue; 
            }else{
                resultArray.push(trimed);
            }
        }

        return resultArray;
    }
    let _isNotNumber = function(item) {
        let converted = +(item.trim())[0];
        return isNaN(converted);
    }
    let _filterFromBrackets = function(item) {
        let globalFunctionKeysArray = Object.keys(globalFunctions);

        globalFunctionKeysArray.forEach(functionName=>{
            let methodPart = functionName + '(';
            let positionOfMethod = item.indexOf(methodPart);

            if(positionOfMethod !== -1){
                item = item.replace(methodPart, '');
            };
        });
        let result = item.replace(/\(|\)/g, '');
        return result.trim();
    }
    let _wrapByBrackets = function(stringArray, targetString) {
        
        let result = targetString;
        stringArray.forEach(item => {

            let wrappedString = `{{${item}}}`;

            let searchPosition = 0; 
            while(true){
                let position = result.indexOf(item, searchPosition);
                if(position === -1){
                    break;
                }
                let charBehind;
                let backing = 1;
                while(true){
                    charBehind = result.charAt(position - backing);
                    if(charBehind !== ' '){
                        break;
                    }
                    backing+= 1;
                }

                if(['{', '^', ','].includes(charBehind)){
                    searchPosition = position + item.length;
                    continue;
                }

                let beginString = result.substr(0, position);
                let endString = result.substr(position + item.length);

                let newString = `${beginString}{{${item}}}${endString}`;
                result = newString;
                searchPosition = position + item.length;
            }
        });

        return result;
    }
    let _getArrays = function(string) {
        let operationsArray = ['+', '-', '*', '/', ','];
        let resultArray = [];
        let localSplit = function (arrayOfStrings, operator) {
            let resultArray = [];
            arrayOfStrings.forEach(item => {
                let result = item.split(operator);
                resultArray = resultArray.concat(result);
            });

            return resultArray;
        };

        let firstArray = string.split(operationsArray[0]);

        let loopLength = operationsArray.length;
        for (var i = 1; i < loopLength; i++) {
            firstArray = localSplit(firstArray, operationsArray[i]);
        };

        return firstArray;

    }
    let convertSingle = function(target) {
        let arrayOfStrings = _getArrays(target);
        let indexOfSetValue = target.indexOf('{{setValue^');
        if(indexOfSetValue !== -1){
            return target;
        }

        let filteredArrayFromConvertedConfig = arrayOfStrings.filter(_isNotContainsFromConfig);
        let filteredArrayFromNumbers = filteredArrayFromConvertedConfig.filter(_isNotNumber);
        let filteredFromBrackets = filteredArrayFromNumbers.map(_filterFromBrackets);
        let distinctArray=  distinct(filteredFromBrackets);

        let resultString = _wrapByBrackets(distinctArray, target);
        return resultString;
    }

    return convertSingle;

});

module.exports = SingleFieldConverter;

