let converter = (function () {

    let XmlInputConverter = require('./XmlInputConverter');

    let functionConverter = require('./functionConverter');

    let executeDmn = require('./executeDmnConverter');

    let converter = new XmlInputConverter();

    return function (object, type) {
        type = type.toLowerCase();

        try {
            if (type === 'function') {
                return functionConverter(object);
            } else if (type === 'executedmn'){
                return executeDmn(object);
            }else {
                return converter.convertObject(object);
            }

        } catch (e) {
            console.log(e.message);
            console.log('Wrong converting');

            throw 'error';
        }
    };
})();


module.exports = converter;   