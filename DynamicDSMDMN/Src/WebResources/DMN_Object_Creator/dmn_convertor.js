let convertor = (function () {
    let inputConverter = require('./inputCreator');
    let outputConverter = require('./outputCreator');
    let rowsConverter = require('./ruleCreator');

    let _dataArray;

    return function (inputColumns, outputColumns, dataRows, dataArray, policyValue) {
        try{
 
            let inputs = inputConverter(inputColumns, dataArray);
            let outputs = outputConverter(outputColumns, dataArray);
            let rules = rowsConverter(inputColumns, outputColumns,dataRows, dataArray);

            let decisionTable = {
                input: inputs,
                output: outputs,
                rule: rules,
                _id: 'decisionTable',
                _hitPolicy: policyValue
            };

            let decision = {
                decisionTable,
                _id: 'decision',
                _name: 'test'
            };

            let definitions = {
                decision,
                _xmlns: "http://www.omg.org/spec/DMN/20151101/dmn.xsd",
                _id: 'definitions',
                _name: 'definitions',
                _namespace: "http://camunda.org/schema/1.0/dmn"
            };

            let result = {
                definitions
            };

            return result;

        }catch(e){
            console.log(e);
            console.error('bad dmn-convertor'); 
            throw false;
        }
    };
})();

module.exports = convertor;