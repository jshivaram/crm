let inputColumnCreator = (function () {
    let columns = [];
    let idCounter = 0;
    let inputExpressionID = 0;

    let _createId = function () {
        let inputIdValue = 'input' + idCounter++;
        return inputIdValue;
    };

    let _createLabel = function (columnLabel) {
        return columnLabel;
    };

    let _createInputExpression = function (type) {
        let inputExpression = {
            text : '',
            _id : 'inputExpression'+ inputExpressionID++,
            _typeRef : type
        };

        return inputExpression;
    };

    let _createSingleInputObject = function (column, dataArray) {
        let indexOfType = column.field.indexOf('_Input');

        if(indexOfType === -1){
            throw 'Field doesn\'t contain column input or output type';
        }

        let columnField = column.field.substring(0,indexOfType);
        let findedElement = dataArray.find(e => e.fieldName === columnField);
  
        let label = columnField.replace(/>/g, '.');

        let input = {
            _label: _createLabel(label),
            _id: _createId(),
            inputExpression: _createInputExpression(findedElement.type)
        };

        return input;
    };

    let createInputObjects = function(columns,dataArray){
        try {
            let inputArray = columns.map(c=>_createSingleInputObject(c, dataArray) );
            return inputArray;
        }catch(e){
            console.log(e);
            console.log('Error with inputColumn');
        }
    };

    return createInputObjects;
})();

module.exports = inputColumnCreator;