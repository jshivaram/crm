let outputColumnCreator = (function () {
    let columns = [];
    let idCounter = 0;
    let inputExpressionID = 0;

    let _createId = function () {
        let inputIdValue = 'output' + idCounter++;
        return inputIdValue;
    };

    let _createLabel = function (columnLabel) {
        return columnLabel;
    };

    let _createSingleOutputObject = function (column, dataArray) {
       
        let columnfield = column.field.replace(/_Output/g, '');

        let findedElement = dataArray.find(e => e.fieldName == columnfield);
        let type = findedElement.type;
        let label;
        if(type === 'executeDmn'){
            label = type;
        }else{
            label =  columnfield.replace(/>/g, '.');
        }            
        let output = {
            _label: _createLabel(label),
            _id: _createId(),
            _name: '',
            _typeRef: type
        };

        return output;
    };

    let createOutputObjects = function(columns, dataArray){
        try {
            let outputArray = columns.map(c=>_createSingleOutputObject(c, dataArray) );
            return outputArray;
        }catch(e){
            console.log(e);
            console.log('Error with outputColumn');
        }
    };

    return createOutputObjects;
})();

module.exports = outputColumnCreator;