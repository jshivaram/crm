let ruleCreator = (function () {
    let converter = require('./Data_Converter/toXmlDataConverter');
    let alertFunction = require('../Dialogs/alertDialogFunction');
    let inputColumns;
    let outputColumns;

    let data = [];

    let unaryTestCounter = 0;
    let rowCounter = 0;


    let _setInput = function (inputCols) {
        let filteredArray = inputCols.map(c => c.field);
        inputColumns = filteredArray;
    };
    let _setOutput = function (outputCols) {
        let filteredArray = outputCols.map(c => c.field);
        outputColumns = filteredArray;
    };

    let _createUnaryTestCounter = function () {
        let unaryTests = 'UnaryTests_' + unaryTestCounter++;
        return unaryTests;
    };
    let _createRowCounter = function () {
        let row = 'row-' + rowCounter++;
        return row;
    };

    let _createEntries = function (sourceColumns, dataRow, dataArray, isInput) {

        let entryArray = [];
        let fieldDescription = 'Unknow field';

        try {
            sourceColumns.forEach(inputField => {
                let inputFieldWithoutPrefix;

                if(isInput){
                    let indexOfType = inputField.indexOf('_Input');

                    if (indexOfType === -1) {
                        throw 'Field doesn\'t contain column input or output type';
                    }
                    inputFieldWithoutPrefix = inputField.substring(0, indexOfType);

                }else{
                    inputFieldWithoutPrefix = inputField.replace(/_Output/g, '');
                }
               
                let findedElement = dataArray.find(el => el.fieldName === inputFieldWithoutPrefix);


                let elementType = findedElement ? findedElement.type : 'string';
                fieldDescription = findedElement.title;

                if (!isInput && elementType !== 'executeDmn') {
                    elementType = 'function';
                }
                let property;
                if (isInput) {
                    property = dataRow[inputField];
                } else {
                    property = dataRow[inputField];
                }

                if (!property) {
                    let entry = {
                        _id: _createUnaryTestCounter(),
                        text: ""
                    };

                    entryArray.push(entry);
                } else {
                    let entry = {
                        _id: _createUnaryTestCounter(),
                        text: converter(property, elementType)
                    };

                    entryArray.push(entry);
                }
            });

            return entryArray;
        } catch (e) {
            console.error('Iteration rules error');
            throw { columnName: fieldDescription };
        }
    };

    let _createOutputEntries = function (dataRow, dataArray) {
        let array = _createEntries(outputColumns, dataRow, dataArray, false);
        return array;
    };

    let _createInputEntries = function (dataRow, dataArray) {
        let array = _createEntries(inputColumns, dataRow, dataArray, true);
        return array;
    };

    let _createRule = function (dataRow, dataArray, index) {
        let currentIndex = index + 1;
        try {
            let inputEnt = _createInputEntries(dataRow, dataArray);
            let outputEnt = _createOutputEntries(dataRow, dataArray);
            let rule;


            rule = {
                inputEntry: inputEnt,
                outputEntry: outputEnt,
                _id: _createRowCounter()
            };

            return rule;
        } catch (e) {
            console.error('Cant create rule Object');
            alertFunction(`Rule number:${currentIndex} Field: ${e.columnName} `);
            throw "Cant create rule Object";
        }

    };


    return function (inputColumns, outputColumns, dataRows, dataArray) {
        try {
            _setOutput(outputColumns);
            _setInput(inputColumns);

            let rules = dataRows.map((r, index) => _createRule(r, dataArray, index));

            return rules;


        } catch (e) {
            console.log(e.message);
            console.log('bad rule convert');
            throw e;
        }
    }

})();

module.exports = ruleCreator;