let crmDataConverter = (function () {
    let convertSingleElement = function (element, entityName) {
        let title;
        try{
            title = element.DisplayName.LocalizedLabels[0].Label;
        }catch(e){
            title = undefined;
        }

        let resultElement = {
            fieldName : element.LogicalName,
            title : title ,
            type: element.AttributeType,
            entityName: entityName,
            targets: element.Targets
        };
        return resultElement;
    };

    let convertCrmDataArray = function (crmDataArray, entityName, isRequriedExecuteDmn = false) {
        let additionalData = [{
                fieldName : 'ExecuteDmn',
                title : 'ExecuteDMN' ,
                type: 'executeDmn',
                entityName: entityName
            },{
                fieldName : 'OutputValue',
                title : 'OutputValue' ,
                type: 'outputValue',
                entityName: entityName
            }
        ]

        let resultArray = crmDataArray.map(item => convertSingleElement(item, entityName))
                                    .filter(item => item.type!== 'Virtual' && 
                                                    !item.fieldName.includes('_base') &&
                                                    item.title !== undefined);

        
        resultArray.sort(function(a,b){
            if(a.title < b.title){
                return -1;
            }
            if(a.title > b.title){
                return 1;
            }

            return 0;
        });

        if(isRequriedExecuteDmn){
            resultArray = additionalData.concat(resultArray);
        }

        return resultArray;
    };

    return {
        convertCrmDataArray : convertCrmDataArray
    }
})();

module.exports = crmDataConverter;
