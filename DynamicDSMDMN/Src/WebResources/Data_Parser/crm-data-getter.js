let crmGetter = (function () {
    let _baseUrl = parent.Xrm.Page.context.getClientUrl();
    let getData = function (id, callback) {
        if(!id){
            callback();
        }else{
            $.ajax({
            type: 'GET',
            url: _baseUrl + `/api/data/v8.1/EntityDefinitions(${id})?$select=LogicalName`,
            headers: {
                'Accept': 'application/json',
                "Content-Type": "application/json; charset=utf-8",
                'OData-MaxVersion': "4.0",
                "OData-Version": "4.0"
            }
        }).done(callback);
        }
        
    }

    return getData;

})();

module.exports = crmGetter;
