let alertFunction = require('./alertDialogFunction');

const bagging = 'Please, add column';

let singleColumnMessage = (columnName) => `${columnName} has no column.`;

const messageCase = {
    inputOnly: `${singleColumnMessage('Input')} ${bagging}`,
    outputOnly: `${singleColumnMessage('Output')} ${bagging}`,
    both: `Input and output have no columns. Please, add columns.`
}


let unvalidColumnsMessage = function (columnsState) {
    inputIsUnvalid = columnsState.inputIsUnvalid;
    outputIsUnvalid = columnsState.outputIsUnvalid;

    if (inputIsUnvalid && outputIsUnvalid) {
        alertFunction(messageCase.both);
        return;
    }
    if (inputIsUnvalid && !outputIsUnvalid) {
        alertFunction(messageCase.inputOnly);
        return;
    }
    if (!inputIsUnvalid && outputIsUnvalid) {
        alertFunction(messageCase.outputOnly);
        return;
    }

    throw 'Columns are valid but it is still triggered as error';

}

module.exports = unvalidColumnsMessage;