let alertFunction = function (message) {
    if(parent.Alert){
        parent.Alert.show(message, null, null, "ERROR", 500, 200);
    }else{
        alert(message);
    }
};

module.exports = alertFunction;