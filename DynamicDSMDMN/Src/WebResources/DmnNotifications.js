function dmnNotification() {
    if (!Xrm.Page.data) return;
    var ddsm_entityid = Xrm.Page.data.entity.getId();
    if (ddsm_entityid.length == 0) { return };
    window.top.simplyLoader = "";
    setInterval("showNotification()", 4000);
}


function showNotification() {
    var entityName = Xrm.Page.data.entity.getEntityName()
    var relatedEntity = getRelatedEntity2Notification(entityName);

    if ((relatedEntity == undefined) || (relatedEntity.relationships.length == 0)) {
        initNotification(false)
        return;
    }
    var relationships = relatedEntity.relationships;
    var isCheckDMNJobs = relatedEntity.isCheckDMNJobs;
    if (isCheckDMNJobs == undefined) {
        isCheckDMNJobs = false;
    }

    var entitiesData = onGeneratedJson(relationships);
    var entities = { "EntityNames": entitiesData };
    var entitiesJson = JSON.stringify(entities);
    initNotification(true, isCheckDMNJobs, entitiesJson);
}
function initNotification(isCheckRelatedEntities, isCheckDMNJobs = false, listRelatedEntities = []) {
    if (window.top.simplyLoader.length > 80) {
        window.top.simplyLoader = "";
    }
    window.top.simplyLoader += "&#x2771;";
    var params = [];
    params.push({
        key: "Target",
        type: Process.Type.EntityReference,
        value: {
            id: Xrm.Page.data.entity.getId(),
            entityType: Xrm.Page.data.entity.getEntityName()
        }
    });
    params.push({
        key: "IsCheckRelatedEntities",
        type: Process.Type.Bool,
        value: isCheckRelatedEntities
    });
    params.push({
        key: "IsCheckDMNJobs",
        type: Process.Type.Bool,
        value: isCheckDMNJobs
    });
    if (isCheckRelatedEntities && listRelatedEntities.length > 0) {
        params.push({
            key: "ListRelatedEntities",
            type: Process.Type.String,
            value: '{"EntityNames":[ { "EntityName":"ddsm_measure"}]}'
        });
    }
    Process.callAction("ddsm_DDSMManualcheckofBackgroundJobs", params,
        function (params) {
            var rs = JSON.parse(params[0].value);
            if (rs.Result) {
                var msgs = rs.Message;
                var entities = "";
                var delimer = ", ";
                for (var i = 0; i < msgs.length; i++) {
                    if (entities.length == 0) {
                        entities += msgs[i].EntityName;
                    } else {
                        entities += delimer + msgs[i].EntityName;
                    }
                }
                Xrm.Page.ui.setFormHtmlNotification("Please note! Calculation is in progress for next entities: " + entities + ". Wait until this notice dissapears." + window.top.simplyLoader, "WARNING", "asyncjobsacivity");
            } else {
                Xrm.Page.ui.clearFormNotification("asyncjobsacivity");
            }
        },
        function (e) {
            // Error
            console.log(e);

        }
    );
}

function onGeneratedJson(entities) {
    var jsonData = [];
    for (var i = 0; i < entities.length; i++) {
        var tmp = {};
        tmp['EntityName'] = entities[i];
        jsonData.push(tmp);
    }
    return JSON.stringify(jsonData);
}
function showNotification123() {
    var ddsm_entityid = Xrm.Page.data.entity.getId();
    if (ddsm_entityid.length == 0) { return };
    var entityId = ddsm_entityid.replace(/[{}]/g, "");
    var baseUrl = "/api/data/v8.1/";
    var clientUrl = Xrm.Page.context.getClientUrl();
    var req = new XMLHttpRequest();
    var url = clientUrl + baseUrl + "asyncoperations?$select=*&$filter=_regardingobjectid_value eq " + entityId + " and (statecode eq 0 or statecode eq 2) and (statuscode eq 0 or statuscode eq 20)";
    req.open("GET", encodeURI(url), true);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.setRequestHeader("OData-MaxVersion", "4.0");
    req.setRequestHeader("OData-Version", "4.0");
    req.onreadystatechange = function () {
        if (this.readyState == 4) {
            req.onreadystatechange = null;
            if (this.status == 200) {
                var data = JSON.parse(this.response);
                var dat = data.value;
                if (dat.length > 0) {
                    Xrm.Page.ui.setFormNotification("Please note! There are background activities working with the record.", "WARNING", "asyncjobsacivity");
                } else {
                    Xrm.Page.ui.clearFormNotification("asyncjobsacivity");
                }
            }
        }
    };
    req.send();
}
dmnNotification();
