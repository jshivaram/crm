let config = {
    pluralEntityName:'ddsm_dmnconfigentities',
    treeviewIdSelector:'#treeview',
    entityConfigFormName:'ddsm_entityconfigtype',
    entityDropdownCase: [
            //{ title: "Custom Entity", value: 'custom' },
            { title: "Entity", value: 'entity' }
        ],
    formTargetEntityName:'ddsm_targetentityname'
};

module.exports = config;