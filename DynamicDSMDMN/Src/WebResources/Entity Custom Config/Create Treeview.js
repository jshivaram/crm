let treeViewBuilder = (function () {

    let _converter = require('../Data_Parser/crm-data-converter');
    let searchEvent = require('../Navigation/search');
    let _baseUrl = parent.Xrm.Page.context.getClientUrl();

    let _addDraggableToLiElements = function () {
        $('#treeview li:has(ul)')
            .addClass('lookup-li')
            .off();

        $('#treeview').find('li').not('.lookup-li').not('[data-role=draggable]').kendoDraggable({
            hint: function (element) {
                return element.clone();
            },
            dragstart: function (e) {
                e.currentTarget.hide();
            },
            dragend: function (e) {
                e.currentTarget.show();
            }
        });
    };

    let appendTreeViewData = function (selector, dataArray) {
        let $treeView = $(selector).data('kendoTreeView');
        dataArray.forEach(attribute => {

            if (attribute.type === 'Lookup') {

                let html = $treeView.append({
                    text: attribute.title,
                    logicalName: attribute.fieldName,
                    displayName: attribute.title,
                    type: attribute.type,
                    target: attribute.target,
                    items: [
                        { text: 'Loading...' }
                    ]
                });
                $(html).closest('li').addClass('lookup-li');

            } else {
                let html = $treeView.append({
                    text: attribute.title,
                    logicalName: attribute.fieldName,
                    displayName: attribute.title,
                    type: attribute.type
                });

            }
        });

        searchEvent({
            buttonSelector: '#search-button',
            textSelector: '#search-text-field',
            treeViewLiSelector: '#treeview li',
            spinnerContainer: "#loader",
            treeViewContainer: "#treeview",
            draggableRequried: true

        });
        _addDraggableToLiElements();
        kendo.ui.progress($('#main_container'), false);

    }

    let initTree = function (selector, eventObj) {

        let expandFunction = function (e) {

            let node = e.node;
            let length = $(node).find('li').length;
            if (length <= 1) {
                let curentObject = $(selector).data('kendoTreeView').dataItem(node);
                let target = curentObject.target;
                let linkedField = curentObject.logicalName;
                (function (name, linkedField) {
                    $.ajax({
                        type: 'GET',
                        url: _baseUrl + `/api/data/v8.1/EntityDefinitions()?$select=LogicalName,DisplayName&$expand=Attributes&$filter= LogicalName eq '${name}'`,
                        headers: {
                            'Accept': 'application/json',
                            "Content-Type": "application/json; charset=utf-8",
                            'OData-MaxVersion': "4.0",
                            "OData-Version": "4.0"
                        }
                    }).done(item => {
                        let emptyDeleted = false;

                        try {
                            displayEntityName = item.value[0].DisplayName.LocalizedLabels[0].Label;
                        } catch (e) {
                            displayEntityName = logicalName;
                        }

                        let _treeView = $(selector).data("kendoTreeView");
                        let recievedAttributes = item.value[0].Attributes;
                        let logicalName = item.value[0].LogicalName;
                        let convertedArray = _converter.convertCrmDataArray(recievedAttributes, item.value[0].LogicalName);

                        convertedArray.forEach(attribute => {
                            let logicalParent, displayParent, entityName;
                            if (!curentObject.logicalParent && !curentObject.displayParent) {
                                logicalParent = linkedField;
                                displayParent = displayEntityName;

                            } else {
                                logicalParent = `${curentObject.logicalParent}.${linkedField}`;
                                displayParent = `${curentObject.displayParent}.${displayEntityName}`;
                            }

                            if (attribute.type === 'Lookup') {
                                let html = _treeView.append({
                                    text: attribute.title,
                                    entityName: attribute.entityName,
                                    logicalName: attribute.fieldName,
                                    displayName: attribute.title,
                                    type: attribute.type,
                                    logicalParent: logicalParent,
                                    displayParent: displayParent,
                                    target: attribute.targets[0],
                                    items: [
                                        { text: 'Loading...' }
                                    ]
                                }, $(node));
                                $(html).closest('li').addClass('lookup-li');

                                if (!emptyDeleted) {
                                    let loadingPlace = _treeView.findByText('Loading...');
                                    _treeView.remove(loadingPlace);
                                    emptyDeleted = true;
                                }
                            } else {
                                let html = _treeView.append({
                                    text: attribute.title,
                                    entityName: attribute.entityName,
                                    logicalName: attribute.fieldName,
                                    displayName: attribute.title,
                                    type: attribute.type,
                                    logicalParent: logicalParent,
                                    displayParent: displayParent,
                                }, $(node));

                                if (!emptyDeleted) {
                                    let loadingPlace = _treeView.findByText('Loading...');
                                    _treeView.remove(loadingPlace);
                                    emptyDeleted = true;
                                }
                            }
                        });
                        _addDraggableToLiElements();
                    });
                })(target, linkedField);
            }
        };

        let settings = {
            animation: {
                expand: {
                    duration: 200
                },
                collapse: {
                    duration: 200
                }
            },
            expand: expandFunction
        };
        let $treeView = $(selector).data('kendoTreeView');
        if (eventObj) {
            Object.keys(eventObj).forEach(key => settings[key] = eventObj[key]);
            if($treeView){
                $treeView.destroy();
            }
            $(selector).empty().kendoTreeView(settings);
        } else {
            if($treeView){
                $treeView.destroy();
            }
            $(selector).empty().kendoTreeView(settings);
        }
    };

    return {
        initTree,
        appendTreeViewData
    };
})();

module.exports = treeViewBuilder;
