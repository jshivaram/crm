let createDropdownList = function (selector, entityName) {
    const config = require('./Constants');
    let dataParse = require('./dataParser');
    let treeViewCreator = require('./Create Treeview');
    //$('.entity-dropdown-div').css('visibility','visible');
    $(selector).kendoDropDownList({
        filter: "startswith",
        ignoreCase: true,
        filtering: function (e) {
            if (!e.filter) {
                e.preventDefault();
            }
        },
        dataSource: {
            transport: {
                read: {
                    url: parent.Xrm.Page.context.getClientUrl() + `/api/data/v8.1/${config.pluralEntityName}?$select=ddsm_name,ddsm_json_data`,
                    dataType: "json",
                    beforeSend: function (req) {
                        req.setRequestHeader('Accept', 'application/json');
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader('OData-MaxVersion', "4.0");
                        req.setRequestHeader("OData-Version", "4.0");
                    }
                }
            },
            schema: {
                data: function (responce) {
                    let mappedArray = responce.value.map(item => {
                        let mappedObject = {
                            name: item.ddsm_name,
                            jsonData: JSON.parse(item.ddsm_json_data)
                        };

                        return mappedObject;
                    });

                    return mappedArray;
                }
            }
        },
        select: function (e) {
            let title = e.sender.dataItem(e.item)[e.sender.options.dataTextField];
            let data = e.sender.dataItem(e.item)[e.sender.options.dataValueField];
            let convertedData = dataParse(data);
            console.log(convertedData);

            treeViewCreator.initTree(config.treeviewIdSelector);
            treeViewCreator.appendTreeViewData(config.treeviewIdSelector, convertedData);

            parent.Xrm.Page.getAttribute("ddsm_targetentityname").setValue(title);
            parent.Xrm.Page.getAttribute("ddsm_targetentity").setValue(title);
            window.top.createTable();

        },
        dataBound: function () {
            $('.entity-list-div').show(500);
            if (entityName) {
                this.select(function (item) {
                    return item.name === entityName;
                });
                let data = this.dataItem().jsonData;
                let convertedData = dataParse(data);
                console.log(convertedData);

                treeViewCreator.initTree(config.treeviewIdSelector);
                treeViewCreator.appendTreeViewData(config.treeviewIdSelector, convertedData);
                window.top.createTable();
                window.top.lockdropDown();
            }
        },
        dataTextField: 'name',
        dataValueField: 'jsonData',
        optionLabel: '--',
        animation: false
    });
}

module.exports = createDropdownList;