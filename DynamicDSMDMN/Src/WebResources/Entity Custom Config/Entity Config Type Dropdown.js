let createEntityTypeConfigDropdown = function (parentSelector, childSelector) {
    let dropdownChooseConfig = require('./Dropdown Choose Config');
    const constants = require('./Constants');

    let formConfigType = parent.Xrm.Page.getAttribute(constants.entityConfigFormName);
    let formEntityType = parent.Xrm.Page.getAttribute(constants.formTargetEntityName);

    let data = new kendo.data.DataSource({
        data: constants.entityDropdownCase
    });

    let makeVisible = function (selector = '.entity-list-div') {
        $(selector).css('visibility', 'visible');
    };

    const defaultLabel = '--';

    $(parentSelector).kendoDropDownList({
        dataSource: data,
        dataTextField: 'title',
        dataValueField: 'value',
        optionLabel: defaultLabel,
        animation: false,
        select: function (e) {

            let value = e.sender.dataItem(e.item)[e.sender.options.dataValueField];
            let title = e.sender.dataItem(e.item)[e.sender.options.dataTextField];
            if (title === defaultLabel) {
                $('.entity-list-div').hide(500);
            } else {
                $('.entity-list-div').show(500);
                //formConfigType.setValue(title);
                //Config contains methods for creating choosen dropdown;
                if (!dropdownChooseConfig[value]) {
                    let message = 'Creating entity type config. Current value doesn\'t contained in config';
                    console.error(message);
                    throw message;
                }
                makeVisible();
                dropdownChooseConfig[value](childSelector);
            }


        },
        dataBound: function () {
            /*
            let formValue;
            let entityName = formEntityType.getValue();;
            let searchResult;
            if (formConfigType) {
                formValue = formConfigType.getValue();
                searchResult = constants.entityDropdownCase.find(item => item.title === formValue);
            }
            if (searchResult) {
                this.select(function (item) {
                    return item.title === formValue;
                });
                let value = this.value();

                makeVisible();
                dropdownChooseConfig[value](childSelector, entityName);
            } else if (entityName) {*/
                this.select(item=>{
                    return item.value === 'entity';
                });
                makeVisible();
                dropdownChooseConfig['entity'](childSelector);
                
                $('#entity-type-dropdown').hide();
            // } else {
            //     kendo.ui.progress($('#main_container'), false);
            // }

        }
    });


}

module.exports = createEntityTypeConfigDropdown;