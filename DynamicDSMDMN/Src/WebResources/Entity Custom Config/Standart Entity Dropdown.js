let createDropdown = function (selector) {
    //$('.entity-dropdown-div').css('visibility','visible');
    kendo.ui.progress($('#main_container'), true);
    $(selector).kendoDropDownList({
        filter: "startswith",
        ignoreCase: true,
        filtering: function (e) {
            if (!e.filter) {
                e.preventDefault();
            }
        },
        dataSource: {
            serverFiltering: false,
            schema: {
                data: function (response) {
                    var value = response.value;
                    var model = value.map(function (el) {
                        var displayName = "";
                        var id = el.MetadataId;
                        var logicalName = el.SchemaName.toLowerCase();
                        if (el.DisplayName.LocalizedLabels[0] && el.DisplayName.LocalizedLabels[0].Label) {
                            displayName = el.DisplayName.LocalizedLabels[0].Label;
                        } else if (el.DisplayName.UserLocalizedLabel && el.DisplayName.UserLocalizedLabel.Label) {
                            displayName = el.DisplayName.UserLocalizedLabel.Label;
                        } else {
                            displayName = logicalName;
                        }
                        return {
                            label: displayName,
                            logicalName: logicalName,
                            id: id
                        };
                    })
                        .sort(function (a, b) {
                            if (a.label < b.label) {
                                return -1;
                            }
                            if (a.label > b.label) {
                                return 1;
                            }
                            return 0;
                        });
                    return model;
                }
            },
            transport: {
                read: {
                    url: parent.Xrm.Page.context.getClientUrl() + '/api/data/v8.1/EntityDefinitions?$select=DisplayName,SchemaName,MetadataId',
                    dataType: "json",
                    beforeSend: function (req) {
                        req.setRequestHeader('Accept', 'application/json');
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader('OData-MaxVersion', "4.0");
                        req.setRequestHeader("OData-Version", "4.0");
                    }
                }
            }
        },
        dataTextField: "label",
        dataValueField: "logicalName",
        optionLabel: 'Select an entity..',
        select: function (e) {
            kendo.ui.progress($('#main_container'), true);
            var dataItem = this.dataItem(e.item);
            this.enable(false);
            var $grid = $('#grid');
            var kendoData = $grid.data('kendoGrid');

            if(kendoData){
                kendoData.destroy();
                $grid.empty();
            }

            
            try {
                parent.Xrm.Page.getAttribute("ddsm_targetentityname").setValue(dataItem.label);
            } catch (error) {

            }

            var logicalName = dataItem.logicalName;
            var id = dataItem.id;
            parent.Xrm.Page.getAttribute("ddsm_targetentity").setValue(logicalName);
            window.top.createTable(id, this);


        },
        dataBound: function (e) {
            $('.entity-list-div').show(500);
            // fill value on load
            var dropdownlist = this;
            var selectedEntity = parent.Xrm.Page.getAttribute("ddsm_targetentity").getValue();
            if (selectedEntity) {
                dropdownlist.value(selectedEntity);
                let createTab = function () {
                    if (window.top.createTable) {
                        window.top.createTable(id);
                    } else {
                        setTimeout(createTab, 500);
                    }
                };
                // set url for nested dropdown
                var dataItem = this.dataItem(e.item);
                var id = dataItem ? dataItem.id : undefined;
                var label = dataItem ? dataItem.Label : parent.Xrm.Page.getAttribute("ddsm_targetentity").getValue();
                try {
                    parent.Xrm.Page.getAttribute("ddsm_targetentityname").setValue(dataItem.label);

                    window.top.lockdropDown();
                    createTab();

                } catch (e) {
                    kendo.ui.progress($('#main_container'), false);

                }

            } else {
                kendo.ui.progress($('#main_container'), false);
            }
        }
    });
}

module.exports = createDropdown;