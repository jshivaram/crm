let dataConverter = function(dataArray){
    let convertSingleElement = function (element) {

        let target;
        let type = element.type;

        if(type === 'entity'){
            type = 'Lookup';
            target = element.logicalName;
        }

        let resultElement = {
            fieldName : element.logicalName,
            title : element.displayName ,
            type,
            target
        };
        return resultElement;
    };

    let resultArray = dataArray.map(convertSingleElement)
        .sort(function(a,b){
            if(a.title < b.title){
                return -1;
            }
            if(a.title > b.title){
                return 1;
            }
            return 0;
        });

        return resultArray;
}

module.exports = dataConverter;