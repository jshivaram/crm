
let getStringOrObject = function(stringOrObj, $popup){
        return $popup.find(stringOrObj).data('kendoDropDownList');
}

let onUpdateAndBoundDataGet = function(dmnDropdown, relationTypeDropdown, relationValueString, $popup){
    let valueDmn = getStringOrObject(dmnDropdown,$popup).text();
    let valueRelType = getStringOrObject(relationTypeDropdown,$popup).text();
    

    let resultString = `executeDMN(${valueDmn}, ${valueRelType}(${relationValueString}))`
    return resultString;
}

module.exports  = onUpdateAndBoundDataGet;