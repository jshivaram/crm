
let getStringOrObject = function(stringOrObj, $popup){
        return $popup.find(stringOrObj).data('kendoDropDownList');
}

let businessRuleEventDataGet = function(businessRuleText, relationTypeDropdown, relationDropdown, $popup){
    let valueRelType = getStringOrObject(relationTypeDropdown,$popup).text();
    let valueRel = getStringOrObject(relationDropdown,$popup).value();
    

    let resultString = `executeDMN(${businessRuleText}, ${valueRelType}(${valueRel}))`;
    return resultString;
}

module.exports  = businessRuleEventDataGet;