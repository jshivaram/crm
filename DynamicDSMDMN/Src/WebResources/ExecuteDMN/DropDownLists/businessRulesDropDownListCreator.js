let insertToTextarea = require('./insertToTextarea');
let popupConstants = require('../Popup/popupConstants');
let multipleDataGeter = require('./TextAreaBind/UpdateOnBusinessRuleSelect');


///Create dropDown with business rules array
///popup - the reference, where dropdown is situated
///selector - optionaly, id where dropdown will be initialized
///businessRuleName - the name  of current business rule, where user is working
let createDropDownList = function ($popup, selector, businessRuleName = '', defaultValue) {

    let $businessRules = $popup.find(selector);
    $businessRules.kendoDropDownList({
        //clear business rules array from the current rule
        dataSource: {
            transport: {
                read: {
                    url: parent.Xrm.Page.context.getClientUrl() + `/api/data/v8.1/ddsm_businessrules?$filter=ddsm_isactivate eq true&$select=ddsm_businessruleid,ddsm_name`,
                    dataType: "json",
                    beforeSend: function (req) {
                        req.setRequestHeader('Accept', 'application/json');
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader('OData-MaxVersion', "4.0");
                        req.setRequestHeader("OData-Version", "4.0");
                    }
                }
            },
            schema: {
                data: function (responce) {
                    return responce.value.filter(rule => rule.ddsm_name !== businessRuleName);
                }
            }
        },
        select: function (e) {
            let insertedString = e.item.text();
            let stringResult = multipleDataGeter(insertedString,
                '#' + popupConstants.arrIds.relationTypesId,
                '#' + popupConstants.arrIds.relationsDropdownId,
                $popup);
            let $textarea = $popup.find('#' + popupConstants.arrIds.formulaEditorId);
            $textarea.val(stringResult);
        },
        dataBound: function () {

            let dropDownListData = $businessRules.data('kendoDropDownList');

            if (defaultValue) {
                dropDownListData.value(defaultValue);
            } else {
                kendo.ui.progress($popup, false);
            }
        },
        dataTextField: 'ddsm_name',
        dataValueField: 'ddsm_businessruleid',
        optionLabel: '--',
        animation: false,
        popup: {
            appendTo: $popup
        }
    });
};

module.exports = createDropDownList;