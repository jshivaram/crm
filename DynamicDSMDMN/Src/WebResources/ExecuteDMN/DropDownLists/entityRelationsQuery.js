let baseUrl = parent.Xrm.Page.context.getClientUrl();
let popupConstants = require('../Popup/popupConstants');

let getEntities = function (relationType, entityLogicalName, callback) {
    
    if (relationType === popupConstants.relationTypes.ManyToOne) {
        $.ajax({
            type: 'GET',
            url: baseUrl + "/api/data/v8.1/EntityDefinitions?$select=LogicalName,DisplayName&$expand=Attributes&$filter=LogicalName eq '" + entityLogicalName + "'",
            headers: {
                'Accept': 'application/json',
                "Content-Type": "application/json; charset=utf-8",
                'OData-MaxVersion': "4.0",
                "OData-Version": "4.0"
            }
        }).done(result => {
            let attrs = result.value[0].Attributes;
            let lookups = attrs.filter(attr => attr.AttributeType === 'Lookup');
            let relationsNtoOne = lookups.map(item => {
                let displayName = item.EntityLogicalName + ' -> ' + item.Targets[0];

                let obj = {
                    text: displayName,
                    value: item.LogicalName
                };

                return obj;
            });

            callback(relationsNtoOne);
        });
    } else if (relationType === popupConstants.relationTypes.OneToMany) {
        $.ajax({
            type: 'GET',
            url: baseUrl + "/api/data/v8.1/RelationshipDefinitions/Microsoft.Dynamics.CRM.OneToManyRelationshipMetadata?$filter=ReferencedEntity eq '" + entityLogicalName + "'",
            headers: {
                'Accept': 'application/json',
                "Content-Type": "application/json; charset=utf-8",
                'OData-MaxVersion': "4.0",
                "OData-Version": "4.0"
            }
        }).done(result => {
            let relationsOneToN = result.value.map(item => {
                let text = item.ReferencedEntity + ' -> ' + item.ReferencingEntity;
                let value = item.ReferencedEntityNavigationPropertyName;

                return {
                    text: text,
                    value: value
                };
            });

            callback(relationsOneToN);
        });
    }
}

module.exports = getEntities;