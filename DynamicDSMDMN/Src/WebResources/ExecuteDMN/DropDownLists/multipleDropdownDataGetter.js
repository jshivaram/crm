
let getStringOrObject = function(stringOrObj, $popup){
    if(typeof stringOrObj === 'string'){
        return $popup.find(stringOrObj).data('kendoDropDownList');
    }else{
        return stringOrObj.item;
    }
}

let dataGetter = function(dmnDropdown, relationTypeDropdown, relationDropdown, $popup){
    let valueRelType = getStringOrObject(relationTypeDropdown,$popup).text();
    let valueRel = getStringOrObject(relationDropdown,$popup).value();
    
    let valueDmn = getStringOrObject(dmnDropdown,$popup).text();

    let resultString = `executeDMN(${valueDmn}, ${valueRelType}(${valueRel}))`
    return resultString;
}

module.exports  = dataGetter;