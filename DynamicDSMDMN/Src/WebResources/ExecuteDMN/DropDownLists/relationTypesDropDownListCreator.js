//Config value
let config = (require('../../Row_edditor/outputTypesOperationsConfig'))['ExecuteDmn'];
//
let relationsDropDownListCreator = require('./relationsDropDownListCreator');
let insertToTextarea = require('./insertToTextarea');
let popupConstants = require('../Popup/popupConstants');

let propertiesArray = Object.keys(config);

let createObjectsArray = function () {
    let array = propertiesArray.map(item => {
        let displayName = config[item].displayName;
        let value = item;

        return { displayName, value }
    });

    return array;
}

let createDropDownList = function ($popup, selector, entitiesStorageSelector, entityLogicalName,
    defaultRelationType, defaultRelation) {
    let dataSourceArray = createObjectsArray();

    let $relationTypesDropDown = $popup.find(selector);
    $relationTypesDropDown.empty();

    $relationTypesDropDown.kendoDropDownList({
        //clear business rules array from the current rule
        dataSource: dataSourceArray,
        dataTextField: 'displayName',
        dataValueField: 'value',
        optionLabel: '--',
        animation: false,
        select: function(e){
            kendo.ui.progress($popup, true);
            let relationType = this.dataItem(e.item).value;
            // let insertedString = e.item.text() + '()';
            
            // let $textarea = $popup.find('#' + popupConstants.arrIds.formulaEditorId);
            // insertToTextarea($textarea, insertedString);

            //e.sender.dataItem(e.item)[e.sender.options.dataTextField]
            //e.sender.dataItem(e.item)[e.sender.options.dataValueField]
            //TO.DO get data array for relation type;
            //let resultArray = []
            relationsDropDownListCreator($popup, entitiesStorageSelector, relationType, entityLogicalName);
        },
        dataBound: function () {
            if (defaultRelationType) {

                this.value(defaultRelationType);

                relationsDropDownListCreator($popup, entitiesStorageSelector, defaultRelationType,
                    entityLogicalName, defaultRelation);
            }
        },
        popup: {
            appendTo: $popup
        }
    });
}

module.exports = createDropDownList;