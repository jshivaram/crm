let getEntities = require('./entityRelationsQuery.js');
let insertToTextarea = require('./insertToTextarea');
let popupConstants = require('../Popup/popupConstants');

const getData = require('./TextAreaBind/UpdateAndBoundOnRel');


let createDropDownList = function ($popup, selector, relationType, entityLogicalName, defaultValue) {  
    getEntities(relationType, entityLogicalName, 
        function (array) {
            let $relationsStorage = $popup.find(selector);

            $relationsStorage.empty();
            let $dropDownList = $('<div/>', {
                style: 'width: 100%',
                id: popupConstants.arrIds.relationsDropdownId
            });
            $relationsStorage.append($dropDownList);

            $dropDownList.kendoDropDownList({
                //clear business rules array from the current rule
                dataSource: array,
                //TO.DO set properyName for array
                dataTextField: 'text',
                dataValueField: 'value',
                optionLabel: '--',
                animation: false,
                popup: {
                    appendTo: $popup
                },
                select: function (e) {
                    let string = getData(`#${popupConstants.arrIds.businessRulesId}`,
                    `#${popupConstants.arrIds.relationTypesId}`,
                    this.dataItem(e.item).value,
                    $popup);

                    let $textarea = $popup.find('#' + popupConstants.arrIds.formulaEditorId);
                    $textarea.val(string);
                },
                dataBound: function () {
                    

                    if (defaultValue) {
                        this.value(defaultValue);
                        
                    }else{
                        kendo.ui.progress($popup, false);
                    }

                    let string = getData(`#${popupConstants.arrIds.businessRulesId}`,
                    `#${popupConstants.arrIds.relationTypesId}`,
                    this.value(),
                    $popup);

                    let $textarea = $popup.find('#' + popupConstants.arrIds.formulaEditorId);
                    $textarea.val(string);
                }
            });
            kendo.ui.progress($popup, false);
        }
        
    );
};

module.exports = createDropDownList;