
let popupConstants = {
    arrIds: {
        businessRulesId: 'business-rules-dropdown',
        relationTypesId: 'relation-types-dropdown',
        relationsId: 'relations-dropdown',
        btnSubmitId: 'btn-submit',
        formulaEditorId: 'formula-editor',
        relationsDropdownId:'relations-dropdown-id'
    },
    relationTypes: {
        ManyToOne: 'getMtoO',
        OneToMany: 'getOtoM'
    }
}

module.exports = popupConstants;