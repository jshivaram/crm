let createBusinessRulesDropDownList = require('../DropDownLists/businessRulesDropDownListCreator');
let createRelationTypesDropDownList = require('../DropDownLists/relationTypesDropDownListCreator');
let renderPopupBody = require('./renderPopupBody');
let addEventHandlerForSubmitButton = require('./popupSubmit');
let popupConstants = require('./popupConstants');
let createPopup = require('./toolsForPopupCreator');

function createPopUpForOutputWindow(type, container, options, title, entityLogicalName) {
    let popup = createPopup(title);
    let $popup = $(popup.document.body);
    let currentBusinessRule = parent.Xrm.Page.getAttribute('ddsm_name').getValue();

    let cellState = options.model[options.field];
    if (!cellState) {
        cellState = {
            businessRuleId: undefined,
            relationName: undefined,
            relationType: undefined,
            view: undefined
        };
    }

    cellState.relationType = cellState.relationType ? cellState.relationType : popupConstants.relationTypes.OneToMany;

    renderPopupBody($popup, cellState.view);
    kendo.ui.progress($popup, true);
    addEventHandlerForSubmitButton(popup, options);

    createBusinessRulesDropDownList($popup, '#' + popupConstants.arrIds.businessRulesId, currentBusinessRule,
        cellState.businessRuleId);
    createRelationTypesDropDownList($popup, '#' + popupConstants.arrIds.relationTypesId, '#' + popupConstants.arrIds.relationsId,
        entityLogicalName, cellState.relationType, cellState.relationName);
}

module.exports = createPopUpForOutputWindow; 