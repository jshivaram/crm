let popupConstants = require('./popupConstants');

function addEventHandlerForSubmitButton(popup, options) {

    let $popup = $(popup.document.body);

    let $submit = $popup.find('#' + popupConstants.arrIds.btnSubmitId);

    $submit.on('click', function () {
        let $businessRulesDropDown = $popup.find('#' + popupConstants.arrIds.businessRulesId).data("kendoDropDownList");
        let $relationTypesDropDown = $popup.find('#' + popupConstants.arrIds.relationTypesId).data("kendoDropDownList");
        let $relationDropDown = $popup.find('#' + popupConstants.arrIds.relationsId + ' div').data("kendoDropDownList");
        let $formulaEditor = $popup.find('#' + popupConstants.arrIds.formulaEditorId);

        let resultObject = {
            businessRuleId: $businessRulesDropDown.value(),
            relationName: $relationDropDown.value(),
            relationType: $relationTypesDropDown.value(),
            view: $formulaEditor.val()
        };

        console.log('resultObject');
        console.log(resultObject);

        options.model[options.field] = resultObject;

        refreshGrid(popup);
    });

    function refreshGrid(popup) {
        let gridData = $('#grid').data('kendoGrid');
        gridData.refresh();
        popup.close();
    }
}

module.exports = addEventHandlerForSubmitButton;