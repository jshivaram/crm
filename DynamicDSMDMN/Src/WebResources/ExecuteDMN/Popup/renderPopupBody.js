
let popupConstants = require('./popupConstants');

function renderPopupBody($popup, view) {

    // --------------------------------- Create textarea for editing formula -----------------------------------
    view = view ? view : 'executeDMN()';
    let $textarea = $('<textarea/>', {
        id: popupConstants.arrIds.formulaEditorId,
        class: 'k-textbox',
        style: 'width: 100%;',
        rows: 10
    });
    $textarea.val(view);
    let $textareaCol = $('<div/>', {
        class: 'col-md-12'
    });
    $textareaCol.append($textarea);

    let $textareaRow = $('<div/>', {
        class: 'row pop-up-row'
    });
    $textareaRow.append($textareaCol);
    $popup.append($textareaRow);
    // ---------------------------------------------------------------------------------------------------------

    // ------------------- Create drop down lists: business rules, relation types, relations -------------------
    let $businessRules = createDivWithId(popupConstants.arrIds.businessRulesId);
    let $relationTypes = createDivWithId(popupConstants.arrIds.relationTypesId);
    let $relations = createDivWithId(popupConstants.arrIds.relationsId);

    let $dropDownListsRow = $('<div/>', {
        class: 'row pop-up-row'
    });
    $dropDownListsRow
        .append($businessRules)
        .append($relationTypes)
        .append($relations);

    $popup.append($dropDownListsRow);
    // ---------------------------------------------------------------------------------------------------------

    // ----------------------------------- Create blocks for submit button -------------------------------------
    let $submit = $('<input/>', {
        id: popupConstants.arrIds.btnSubmitId,
        type: 'button',
        value: 'Submit',
        class: 'k-button'
    });
    let $submitCol = $('<div/>', {
        class: 'col-md-12'
    });
    $submitCol.append($submit);

    let $submitRow = $('<div/>', {
        class: 'row pop-up-row'
    });
    $submitRow.append($submitCol);
    $popup.append($submitRow);
    // ---------------------------------------------------------------------------------------------------------
}

function createDivWithId(id) {
    let $div = $('<div/>', {
        id,
        style: 'width: 100%'
    });

    let $divWrapper = $('<div/>', {
        class: 'col-md-3'
    });
    $divWrapper.append($div);

    return $divWrapper;
}

module.exports = renderPopupBody;