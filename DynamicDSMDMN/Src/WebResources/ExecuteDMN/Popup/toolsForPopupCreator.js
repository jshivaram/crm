
function getHeadersForPopup() {
    let baseUrl = parent.Xrm.Page.context.getClientUrl() + '/WebResources';

    let headers = '';

    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.common.min.css" />';
    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.default.min.css" />';
    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.default.mobile.min.css" />';

    headers += '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />';
    headers += '<link rel="stylesheet" href="http://kendo.cdn.telerik.com/2016.2.714/styles/kendo.bootstrap.min.css" />';

    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.office365.min.css"" />';
    headers += '<link rel="stylesheet" href="' + baseUrl + '/bpmn_/css/style.css" />';

    return headers;
}

function createPopup(title) {
    let params = 'width=' + screen.width +
        ', height=' + screen.height;

    let popup = window.open('about:blank', '', params);

    // for test
    window.executeDmnPopup = popup;

    popup.moveTo(0, 0);
    popup.document.title = title;

    let headersHtml = getHeadersForPopup();

    let $popupHead = $(popup.document.head);
    let popupHeadHtml = $popupHead.html();

    popupHeadHtml += headersHtml;
    $popupHead.html(popupHeadHtml);

    return popup;
}

module.exports = createPopup;