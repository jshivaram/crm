let checkUnvalidColumns = require('./checkUnvalidColumns');
let configForIndexesOfColumns = require('../configForIndexesOfColumns');


///Shows, does contain any grid column the unvalid state, like 'No Input' or 'No Output'
let checkGridColumns = function(columns){
    let inputColumn = columns[configForIndexesOfColumns.inputColumn];
    let outputColumn = columns[configForIndexesOfColumns.outputColumn];

    let inputIsUnvalid = checkUnvalidColumns(inputColumn, 'no_input');
    let outputIsUnvalid = checkUnvalidColumns(outputColumn, 'no_output');

    let result = {
        inputIsUnvalid,
        outputIsUnvalid,

        //if gridIsValid is false, you don't need to check any property
        gridIsValid : !(inputIsUnvalid || outputIsUnvalid) 
    };

    return result;
} 

module.exports = checkGridColumns;