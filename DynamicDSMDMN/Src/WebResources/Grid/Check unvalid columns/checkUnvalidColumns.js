let checkUnvalidColumn = function(column,expectedUnvalidName){
    let containedColumns = column.columns;
    let isContainsUnvalidColumn = containedColumns.find(c => c.field === expectedUnvalidName);

    return !!isContainsUnvalidColumn;
}

module.exports = checkUnvalidColumn;