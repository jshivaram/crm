let setDropEvents = function(container_data,addColumn ,columnBehavour,alertFunction, configForIndexesOfColumns){
    let setInputDropEvent = require('./dropEvents/inputDrop');
    let setOutputDropEvent = require('./dropEvents/outputDrop');
    
    setInputDropEvent(container_data,addColumn ,columnBehavour,alertFunction, configForIndexesOfColumns);
    setOutputDropEvent(container_data,addColumn ,columnBehavour,alertFunction, configForIndexesOfColumns);
}

module.exports = setDropEvents;