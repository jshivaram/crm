//We are making dropped object in object, that validated in the grid dataSource 
let createGridObject = function (droppedObject) {
    let newFieldName;
    let newTitle;
    if (droppedObject.logicalParent) {
        newFieldName = droppedObject.logicalParent.replace(/\./g, '>') + '>' + droppedObject.logicalName;
    } else {
        newFieldName = droppedObject.logicalName;
    }
    if (droppedObject.displayParent) {
        newTitle = droppedObject.displayParent + '.' + droppedObject.displayName;
    } else {
        newTitle = droppedObject.displayName;
    }
    let entityName = droppedObject.logicalParent;
    let type = droppedObject.type;

    let newObject = {
        fieldName: newFieldName,
        title: newTitle,
        type,
        entityName
    };

    return newObject;
}
module.exports = createGridObject;