//Set input drop-event
let setInputDropEvent = function (container_data, addColumn, columnBehavour, alertFunction, configForIndexesOfColumns) {
    let $inputHeader = $('#grid thead>tr:first>th[data-title="Input"]');
    let createObjectForGrid = require('./Create Object For Grid');
    const constants = require('../../Constants');

    $inputHeader.kendoDropTarget({
        drop: function (e) {
             
            let droppedObject = $(constants.treeViewSelectorId).data('kendoTreeView').dataItem(e.draggable.element);                    

            let newObject = createObjectForGrid(droppedObject);

            if (!container_data) {
                container_data = [];
                console.log('On dropevent container_data is empty or undef');
            }

            let element = container_data.find(e => e.fieldName === newObject.fieldName);

            if (!element) {
                container_data.push(newObject);
                element = container_data.find(e => e.fieldName === newObject.fieldName);
            }

            if (element.type === 'executeDmn' 
            || element.type === 'outputValue') {
                $inputHeader.removeClass('dropHower');
                alertFunction(`${element.type} is valid only for output`);

            } else {
                let inputsArray = columnBehavour.getColumnArray()[configForIndexesOfColumns.inputColumn];
                let res = inputsArray.columns.find(i => i.field.indexOf(element.fieldName + '_Input') !== -1);
                if (!res) {

                    let fieldName = element.fieldName;

                    let title = element.title;
                    let type = element.type;
                    addColumn(fieldName, title, 'Input', type, 0);
                } else {
                    //this code executes when input has the same field 
                    let titleConfig = (require('../../../Configs/multiple input config'))['title'];
                    let index = 0;
                    let titleTemplate;
                    while (true) {
                        //we creating visualisation of data title 
                        titleTemplate = `<b>${element.title}</b>${titleConfig.openBracket}${index}${titleConfig.closeBracket}(${element.type})`;

                        //we searching the valid index number 
                        let titleIsContains = inputsArray.columns.find(i => i.title === titleTemplate);

                        if (titleIsContains) {
                            index++;
                        } else {
                            let fieldName = element.fieldName;

                            let title = element.title;
                            let type = element.type;


                            addColumn(fieldName, title, 'Input', type, index);
                            break;
                        }
                    }

                    $inputHeader.removeClass('dropHower');
                }
            }
        },        
        dragenter: function () {
            $inputHeader.addClass('dropHower').removeClass('animation');
        },
        dragleave: function () {
            $inputHeader.removeClass('dropHower').addClass('animation');
        }

    });
}

module.exports = setInputDropEvent;


