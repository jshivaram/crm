//Set output column drop event
let setOutputEvemt = function (container_data, addColumn, columnBehavour, alertFunction, configForIndexesOfColumns) {
    let $outputHeader = $('#grid thead>tr:first>th[data-title="Output"]');
    let createObjectForGrid = require('./Create Object For Grid');
    const constants = require('../../Constants');

    $outputHeader.kendoDropTarget({
        drop: function (e) {
            let droppedObject = $(constants.treeViewSelectorId).data('kendoTreeView').dataItem(e.draggable.element);

            let newObject = createObjectForGrid(droppedObject);
            let type = newObject.type;
            if(type === 'outputValue'){

                if (!container_data) {
                    container_data = [];
                    console.log('On dropevent container_data is empty or undef');
                }

                let outputArray = columnBehavour.getColumnArray()[configForIndexesOfColumns.outputColumn];
                let element = outputArray.columns.find(e => e.field === 'OutputValue_Output');
                
                let metadataElement = container_data.find(e => e.fieldName === 'OutputValue_Output');

                if(!metadataElement){
                    container_data.push({
                        fieldName: 'OutputValue',
                        title: 'OutputValue',
                        type: 'outputValue'
                    });
                }

                if(element){
                    alertFunction('Field already exists in output ');
                    $outputHeader.removeClass('dropHower');
                }else{
                    let fieldName = 'OutputValue';
                    let title = 'OutputValue';
                    let type = 'outputValue';
                    addColumn(fieldName, title, 'Output', type);
                }
            }else if (type === 'executeDmn') {
                let executeDmnCounter = 1;

                let outputArray = columnBehavour.getColumnArray()[configForIndexesOfColumns.outputColumn];

                let executeDmnTitle;
                //find executeDmn with index
                while (true) {
                    executeDmnTitle = `${droppedObject.displayName}#${executeDmnCounter}`;
                    let element = outputArray.columns.find(e => e.title.indexOf(executeDmnTitle + '</b>') !== -1);
                    if (!element) {
                        //if index is free, break loop and add element 
                        break;
                    }
                    executeDmnCounter++;
                }

                newObject.fieldName = `${droppedObject.logicalName}#${executeDmnCounter}`;
                newObject.title = executeDmnTitle;
                container_data.push(newObject);
                addColumn(newObject.fieldName, newObject.title, 'Output', type);
            } else {

                if (!container_data) {
                    container_data = [];
                    console.log('On dropevent container_data is empty or undef');
                }

                let element = container_data.find(e => e.fieldName === newObject.fieldName);

                if (!element) {
                    container_data.push(newObject);
                    element = container_data.find(e => e.fieldName === newObject.fieldName);
                }

                let outputArray = columnBehavour.getColumnArray()[configForIndexesOfColumns.outputColumn];
                let res = outputArray.columns.find(i => i.field.indexOf(element.fieldName+'_Output') !== -1);
                if (!res) {
                    let fieldName = element.fieldName;
                    let title = element.title;
                    let type = element.type;
                    addColumn(fieldName, title, 'Output', type);
                } else {
                    alertFunction('Field already exists in output ');
                    $outputHeader.removeClass('dropHower');
                }
            }
        },
        dragenter: function () {
            $outputHeader.addClass('dropHower').removeClass('animation');
        },
        dragleave: function () {
            $outputHeader.removeClass('dropHower').addClass('animation');
        }
    });
}

module.exports = setOutputEvemt;


