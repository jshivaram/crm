
let stylize = function(selector){
    $(selector).css('white-space', 'normal')
                .css('vertical-align', 'top');
                
};

module.exports = stylize;