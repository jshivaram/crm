let configForIndexesOfColumns = require('./configForIndexesOfColumns.js');

let columns_behavour = (function () {

    let colsStatus = {
        inputIsDef: true,
        outputIsDef: true,
    };

    let editorConstructor = require('../Row_edditor/editor');
    let editor;

    let inputColumn = {
        title: 'Input',
        columns: [{
            field: 'no_input',
            attributes: {
                style: 'background-color:rgb(255, 255, 183);'
            },
            title: 'No Input'
        }]
    };

    let outputColumn = {
        title: 'Output',
        columns: [{
            field: 'no_output',
            attributes: {
                style: 'background-color:rgb(214, 254, 184);'
            },
            title: 'No Output'
        }]
    };

    let annotationColumn = {
        width: 120,
        field: 'Annotation'
    };

    let destroyColumn = {
        command: {
            name: "Delete",
            text: '<span class="k-icon k-i-close" style="margin-right: 0px;"></span>'
        },
        width: 85       
    };

    let rowNumberColumn = {
        title: "&nbsp;",
        template: "<span class='row-number'></span>",
        width: 18
    };

    let columns = [rowNumberColumn, destroyColumn, inputColumn, outputColumn, annotationColumn];

    let addNewColumn = function (mainColumn, columnName, title, type) {

        let newField = {
            field: columnName.replace(/\s/g, '') + `_${mainColumn.title}`,
            editor: editor,

            title: type ? title + ` (${type})` : title,
            attributes: {
                style: mainColumn.title === 'Input' ?
                    'background-color:rgb(255, 255, 183);' : 'background-color:rgb(214, 254, 184);'
            }
        };
        mainColumn.columns.push(newField);
    };

    let removeColumn = function (targetColumn, columnName) {
        let newColumn = targetColumn.filter(c => c.field !== columnName);
        return newColumn;
    }; 

    let setColumns = function (columnsArray) {
        columnsArray[configForIndexesOfColumns.inputColumn].columns.forEach(function (el) {
            el.template = function (data) {

                let fieldName = el.field;

                if (!data || !data[fieldName] || !data[fieldName].view) {
                    return '';
                }

                return data[fieldName].view;
            }
        });

        columnsArray[configForIndexesOfColumns.outputColumn].columns.forEach(function (el) {
            el.template = function (data) {
                let fieldName = el.field;

                if (!data || !data[fieldName] || !data[fieldName].view) {
                    return '';
                }

                return data[fieldName].view;
            }
        });

        columnsArray[configForIndexesOfColumns.rowNumberColumn]=columns[configForIndexesOfColumns.rowNumberColumn];
        columnsArray[configForIndexesOfColumns.destroyColumn]=columns[configForIndexesOfColumns.destroyColumn];
        columnsArray[configForIndexesOfColumns.annotationColumn]=columns[configForIndexesOfColumns.annotationColumn];

        columns = columnsArray;
    };

    let removeInputColumn = function (columnName) {
        let inputColumn = removeColumn(columns[configForIndexesOfColumns.inputColumn].columns, columnName);
        if (inputColumn.length === 0) {
            columns[configForIndexesOfColumns.inputColumn].columns = [{
                field: 'no_input',
                attributes: {
                    style: 'background-color:rgb(255, 255, 183);'
                },
                title: 'No Input'
            }];
            colsStatus.inputIsDef = true;
        } else {
            columns[configForIndexesOfColumns.inputColumn].columns = inputColumn;
        }
    };

    let removeOutputColumn = function (columnName) {
        let outputColumn = removeColumn(columns[configForIndexesOfColumns.outputColumn].columns, columnName);
        if (outputColumn.length === 0) {
            columns[configForIndexesOfColumns.outputColumn].columns = [{
                field: 'no_output',
                attributes: {
                    style: 'background-color:rgb(214, 254, 184);'
                },
                title: 'No Output'
            }
            ];
            colsStatus.outputIsDef = true;
        } else {
            columns[configForIndexesOfColumns.outputColumn].columns = outputColumn;
        }
    };

    let reset = function () {
        colsStatus.inputIsDef = true;
        colsStatus.outputIsDef = true;

        columns[configForIndexesOfColumns.inputColumn] = {
            title: 'Input',
            columns: [{
                field: 'no_input',
                attributes: {
                    style: 'background-color:rgb(255, 255, 183);'
                },
                title: 'No Input'
            }]
        };

        columns[configForIndexesOfColumns.outputColumn] = {
            title: 'Output',
            columns: [{
                field: 'no_output',
                attributes: {
                    style: 'background-color:rgb(214, 254, 184);'
                },
                title: 'No Output'
            }]
        };
    };

    return {
        addInput: function (columnName, title, type, index = 0) {
            let indexationConfig = require('../Configs/multiple input config');
            let logicalConfig = indexationConfig.logical;
            let titleConfig = indexationConfig.title;

            let logicalFieldName = columnName.replace(/\s/g, '') + `_${inputColumn.title}${logicalConfig.openBracket}${index}${logicalConfig.closeBracket}`;
            let newField = {
                width: 200,
                field: logicalFieldName,
                editor: editor,
                template: function (data) {

                    let fieldName = logicalFieldName;

                    if (!data || !data[fieldName] || !data[fieldName].view) {
                        return '';
                    }

                    return data[fieldName].view;
                },
                title: type ? `<b>${title}</b>${titleConfig.openBracket}${index}${titleConfig.closeBracket}(${type})` : title,
                attributes: {
                    style: 'background-color:rgb(255, 255, 183);'
                }
            };
            if (colsStatus.inputIsDef) {
                columns[configForIndexesOfColumns.inputColumn].columns = [newField];
                colsStatus.inputIsDef = false;
            } else {
                columns[configForIndexesOfColumns.inputColumn].columns.push(newField);
            }

        },
        addOutput: function (columnName, title, type) {

            let newField = {
                width: 200,
                field: columnName.replace(/\s/g, '') + `_${outputColumn.title}`,
                editor: editor,
                template: function (data) {
                    let fieldName = columnName + '_Output';

                    if (!data || !data[fieldName] || !data[fieldName].view) {
                        return '';
                    }

                    return data[fieldName].view;
                },
                title: type ? `<b>${title}</b>` + `</br>(${type})` : title,
                attributes: {
                    style: 'background-color:rgb(214, 254, 184);'
                }
            };
            if (colsStatus.outputIsDef) {
                columns[configForIndexesOfColumns.outputColumn].columns = [newField];
                colsStatus.outputIsDef = false;
            } else {
                columns[configForIndexesOfColumns.outputColumn].columns.push(newField);
            }
        },

        removeInputColumn: removeInputColumn,
        removeOutputColumn: removeOutputColumn,
        getColumnArray: function () {
            return columns;
        },
        getColumnsState: function () {
            return colsStatus;
        },
        setMetadata: function (metadata) {

            let addEditor = function (column, func) {
                column.columns.forEach(item => {
                    item.editor = func;
                });
            };

            editor = editorConstructor(metadata);
            addEditor(columns[configForIndexesOfColumns.inputColumn], editor);
            addEditor(columns[configForIndexesOfColumns.outputColumn], editor);
        },
        setColumns: setColumns,
        reset,
        setDataState: function (statusObject) {
            colsStatus = statusObject;
        },
        getInputColumns(){
            return columns[configForIndexesOfColumns.inputColumn].columns;
        },
        getOutputColumns(){
            return columns[configForIndexesOfColumns.outputColumn].columns;
        },
        setInputColumns(column){
            columns[configForIndexesOfColumns.inputColumn].columns = column;
        },
        setOutputColumns(column){
            columns[configForIndexesOfColumns.outputColumn].columns = column;
        }
    }   
})();

module.exports = columns_behavour;
