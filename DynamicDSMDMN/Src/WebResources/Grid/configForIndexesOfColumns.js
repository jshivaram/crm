
let obj = {
    rowNumberColumn: 0,
    destroyColumn: 1,
    inputColumn: 2,
    outputColumn: 3,
    annotationColumn: 4
}

module.exports = obj;