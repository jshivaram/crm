let configForIndexesOfColumns = require('./configForIndexesOfColumns.js');
const constans = require('./Constants');
const checkGridColumns = require('./Check unvalid columns/checkGridColumns');
const addRowWhenColumnsHaveNoValidValueMessage = require('../Dialogs/addRowWhenUnvalidColumn');
let gridBuilder = (function () {
    let id;
    let columnBehavour = require('./columns-behavour');
    let container_data = [];

    let dblClickHandlerIsSet = false;

    let dataBoundHandler = function () {

        let rows = this.items();
        $(rows).each(function () {
            let index = $(this).index() + 1;
            let rowLabel = $(this).find(".row-number");
            $(rowLabel).html(index);
        });

        if (!dblClickHandlerIsSet) {
            $("#grid").delegate("tbody td", "dblclick", function () {

                $("#grid").data("kendoGrid").editCell($(this));

            });

            dblClickHandlerIsSet = true;
        }

        function initEventClickByRemoveButton() {
            $('.k-grid-Delete').on('click', function (e) {
                let msg = "Are you sure?";
                parent.Alert.show(msg, null, [{
                    label: "Yes",
                    callback: function () {
                        e.preventDefault();
                        let grid = $("#grid").data("kendoGrid");
                        let tr = $(e.target).closest("tr");
                        grid.removeRow(tr);
                    }
                }, {
                    label: "No"
                }], "QUESTION", 500, 200);
            });
        }

        let evForBtn = $._data(document.getElementById('toolbar-btn-add'), 'events');
        if (!evForBtn) {
            $('#toolbar-btn-add').on('click', function () {
                let grid = $("#grid").data("kendoGrid");
                let columnsStatus = checkGridColumns(grid.columns);
                
                if(columnsStatus.gridIsValid){
                    grid.addRow();
                    $("#window").data("kendoWindow").close();
                }else{
                    addRowWhenColumnsHaveNoValidValueMessage(columnsStatus);
                }
            });
        }

        initEventClickByRemoveButton();
    };

    let setEvents = function () {

        let HeaderToolTipCreator = require('./Events/showToolTip');
        let headerCreator = new HeaderToolTipCreator();

        headerCreator.createEvent('#grid thead>tr:eq(1)>th');

        let thisDropEvent = function () {
            let alertFunction = function (text) {
                let alertFunc = require('../Dialogs/alertDialogFunction');
                alertFunc(text);
            };
            let createDropEvent = require('./Events/dropEvent');
            createDropEvent(container_data,addColumn ,columnBehavour,alertFunction, configForIndexesOfColumns);
        };

        let doubleClickEvent = function (event) {
            let isInput;
            let $curentTarget = $(event.currentTarget);

            let title = $curentTarget.attr('data-title');
            let logicalName = $curentTarget.attr('data-field');

            let data = $("#grid").data("kendoGrid").dataItems();

            let curentColumnsArray = columnBehavour.getColumnArray();
            let curentArray;

            if (logicalName.indexOf('_Input') !== -1) {
                curentArray = curentColumnsArray[configForIndexesOfColumns.inputColumn];
                isInput = true;

            } else if (logicalName.indexOf('_Output') !== -1) {
                curentArray = curentColumnsArray[configForIndexesOfColumns.outputColumn];
                isInput = false;
            } else {
                throw 'When delete column, it hasn\'t input or output identifier';
            }

            data.forEach(item => {
                delete item[logicalName];
            });
            if (isInput) {
                columnBehavour.removeInputColumn(logicalName);
            } else {
                columnBehavour.removeOutputColumn(logicalName);
            }
            createGrid(id);
            $("#grid").data("kendoGrid").dataSource.data(data);
        };

        thisDropEvent();
        $('thead').children('tr').eq(1).find('th').dblclick(doubleClickEvent);

    };

    let createGrid = function (idSelector, data) {

        if (!id) {
            id = idSelector;
        }

        $(id).empty();
        let columnArray = columnBehavour.getColumnArray();
        if (data) {
            $(id).kendoGrid({
                height: 650,
                reorderable: true,
                resizable: true,
                editable: false,
                columns: columnArray,
                dataSource: data,
                toolbar: '<div class="col-md-2">' +
                '<button id="toolbar-btn-add" type="button" class="k-button">' +
                '<span class="k-icon k-i-plus"></span>' +
                'Add new record' +
                '</button>' +
                '</div>',
                dataBound: dataBoundHandler
            });
        } else {
            $(id).kendoGrid({
                height: 650,
                reorderable: true,
                resizable: true,
                editable: false,
                columns: columnArray,
                toolbar: '<div class="col-md-2">' +
                '<button id="toolbar-btn-add" type="button" class="k-button">' +
                '<span class="k-icon k-i-plus"></span>' +
                'Add new record' +
                '</button>' +
                '</div>',
                dataBound: dataBoundHandler
            });
        }

        setEvents();

    };

    let addColumn = function (name, title, position = 'Input', type, index) {
        let createGridWithNewColumn = function (name, title, position, type, index) {
            let data = $("#grid").data("kendoGrid").dataItems();
            if (position === 'Input') {
                columnBehavour.addInput(name, title, type, index);
            }
            if (position === 'Output') {
                columnBehavour.addOutput(name, title, type);
            }
            createGrid(id);
            $("#grid").data("kendoGrid").dataSource.data(data);
        };

        createGridWithNewColumn(name, title, position, type, index);
        setEvents();
        (require('./Style Decoration/headerStyle'))('#grid th');

    };

    let buildGridFromFiles = function (idSelector, columns, data, columnsState, dataCont) {

        if (!id) {
            id = idSelector;
        }
        container_data = dataCont;
        $(id).empty();
        columnBehavour.setColumns(columns);
        columnBehavour.setDataState(columnsState);
        columnBehavour.setMetadata(container_data);

        let columnArray = columns;
        if (data) {
            $(id).kendoGrid({
                height: 650,
                reorderable: true,
                resizable: true,
                editable: false,
                columns: columnArray,
                dataSource: data,
                toolbar: '<div class="col-md-2">' +
                '<button id="toolbar-btn-add" type="button" class="k-button">' +
                '<span class="k-icon k-i-plus"></span>' +
                'Add new record' +
                '</button>' +
                '</div>',
                dataBound: dataBoundHandler
            });
        } else {
            $(id).kendoGrid({
                height: 650,
                reorderable: true,
                resizable: true,
                editable: false,
                columns: columnArray,
                toolbar: '<div class="col-md-2">' +
                '<button id="toolbar-btn-add" type="button" class="k-button">' +
                '<span class="k-icon k-i-plus"></span>' +
                'Add new record' +
                '</button>' +
                '</div>',
                dataBound: dataBoundHandler
            });
        }

        setEvents();
    };

    return {
        createGrid,
        addColumn,
        getColumnsState: function () {
            let columnsArray = columnBehavour.getColumnArray();
            let objectState = columnBehavour.getColumnsState();
            return {
                columnsArray,
                objectState,
                container_data
            };
        },
        setMetadata: function () {
            columnBehavour.setMetadata(container_data);
        },
        setDataContainerSource: function (dataSourceArray) {
            container_data = dataSourceArray;
        },
        buildGridFromFiles,
        resetGrid: function (id) {
            container_data = [];
            columnBehavour.reset();
            createGrid(id);
        },
        getColumnArray: function () {
            return columnBehavour.getColumnArray();
        },
        getContainerData: function () {
            return container_data;
        },
        getInputColumns(){
            return columnBehavour.getInputColumns();
        },
        getOutputColumns(){
            return columnBehavour.getOutputColumns();
        },
        setInputColumns(column){
            columnBehavour.setInputColumns(column);
        },
        setOutputColumns(column){
            columnBehavour.setOutputColumns(column)
        }
    
    };
})();

module.exports = gridBuilder;