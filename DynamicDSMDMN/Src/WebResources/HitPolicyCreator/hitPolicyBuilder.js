let hitPolicyBuilder = (function () {
    let data = [
        'UNIQUE',
        'FIRST',
        'PRIORITY',
        'ANY',
        'COLLECT',
        'RULE ORDER',
        'OUTPUT ORDER'
    ];

    return function (id) {
        $(id).kendoDropDownList({
            dataSource: {
                data: data
            },
            animation: {
                close: {
                    effects: "fadeOut zoom:out",
                    duration: 300
                },
                open: {
                    effects: "fadeIn zoom:in",
                    duration: 300
                }
            }
        });
    }
})();

module.exports = hitPolicyBuilder;
