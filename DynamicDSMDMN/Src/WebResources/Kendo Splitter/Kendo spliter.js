let createSplitter = function (idSelector) {
    $(idSelector).kendoSplitter({
        panes: [
            {
                collapsible: true
                , max: "40%"
                , size: '35%'
                //, scrollable: true
            },
            {
                collapsible: false
                , min: '60%'
               // , scrollable: true
            }
        ]
    })
};

module.exports = createSplitter;