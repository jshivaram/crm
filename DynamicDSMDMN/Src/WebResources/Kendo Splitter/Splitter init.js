
let initSplitter = function(selector){
    var $splitterData = $(selector).data('kendoSplitter');

    if(!$splitterData){
        (require('./Kendo spliter'))(selector);
        $(selector).data('kendoSplitter').collapse('.k-pane:first');
    }
}

module.exports = initSplitter;