class NToOneXmlConverter {
    constructor(configObject) {
        let Searcher = require('../OneToNXmlConverter/Searcher/Searcher');
        let Swapper = require('../OneToNXmlConverter/Swapper/Swapper');
        let methodArray = Object.keys(configObject);

        this.searcher = new Searcher(methodArray);
        this.swapper = new Swapper();
    }
    _getfunctionName(inputString) {
        let result = (/.*?\(/).exec(inputString)[0];
        let functionName = result.substr(0, result.length - 1);

        return functionName;

    };
    _getParameterArray(inputString) {
        let getParametersWithoutFunctionName = function (targetString) {
            let result = targetString.match(/\(.*\)/)[0];
            return result;
        };
        let getArrayOfParameters = function (targetArray) {
            let splitedArray = targetArray.split(',');
            let arrayLength = splitedArray.length;
            splitedArray[0] = splitedArray[0].replace('(', '');
            splitedArray[arrayLength - 1] = splitedArray[arrayLength - 1].replace(')', '');

            return splitedArray;
        };

        let parameters = getParametersWithoutFunctionName(inputString);
        let parametersArray = getArrayOfParameters(parameters);

        return parametersArray;

    };
    convertString(string) {
        let bareFunctionalSequenceArray = this.searcher.getFunctionalSequenceArray(string);
         
        bareFunctionalSequenceArray.forEach(item => {
            let funcExpr = item.functionExpression;

            let functionName = this._getfunctionName(funcExpr);

            //filter 'now' argument from brackets
            let parameterArray = this._getParameterArray(funcExpr)
                                        .map(item=>{
                                            if(item=== 'now()'){
                                                return 'now';
                                            }else{
                                                return item;
                                            }
                                        });
                                    
            let resultString = `${functionName}^`;
            parameterArray.forEach(item => {
                resultString += item + ',';
            });
            resultString = resultString.substring(0, resultString.length - 1);

            let wrappedResultString = `{{${resultString}}}`;

            item.convertedFunctionExpression = wrappedResultString;
        });

        let convertedString = this.swapper.swapFunctionalSequences(string, bareFunctionalSequenceArray);

        return convertedString;
    }
}

module.exports = NToOneXmlConverter;