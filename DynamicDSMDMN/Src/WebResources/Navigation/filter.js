function filter(dataSource, query) {
    var hasVisibleChildren = false;
    var data = dataSource instanceof kendo.data.HierarchicalDataSource && dataSource.data();

    let visibleDataArray = [];

    for (var i = 0; i < data.length; i++) {
        var item = data[i];
        var text = item.text.toLowerCase();
        var itemVisible =
            query === true // parent already matches
            || query === "" // query is empty
            || text.indexOf(query) >= 0; // item text matches query

        var anyVisibleChildren = filter(item.children, itemVisible || query); // pass true if parent matches

        hasVisibleChildren = hasVisibleChildren || anyVisibleChildren || itemVisible;

        item.hidden = !itemVisible && !anyVisibleChildren;
        if (!item.hidden) {
            let elementFromArray = visibleDataArray.find(el => el === item.logicalName);
            if (elementFromArray) {
                item.hidden = true;
            } else {
                visibleDataArray.push(item.logicalName);
            }
        }
    }

    if (data) {
        // re-apply filter on children
        dataSource.filter({ field: "hidden", operator: "neq", value: true });
    }

    return hasVisibleChildren;
}

module.exports = filter;