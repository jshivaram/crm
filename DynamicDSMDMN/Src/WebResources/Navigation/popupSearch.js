let filter = require('./filter');

///Settings properties : text selector, treeview li selector, button selector, treeViewLiSelector, spinnerContainer, treeViewContainer,
///draggableRequried
let setSearchEvent = function (settings) {
    let $popup = $(window.popup.document.body);

    let searchEvent = function () {

        let inputText = $popup.find(settings.textSelector).val().toLowerCase();
        //let inputTex = window.document.getElementById('popup-search-input-field').value.toLowerCase();

        var dataSource = $popup.find(settings.treeViewId).data("kendoTreeView").dataSource;

        filter(dataSource, inputText);
    };

    $popup.find(settings.buttonSelector).off().click(searchEvent);

    $popup.find(settings.textSelector).off().keypress(function (e) {
        if (e.which == 13) {
            searchEvent();
        }
    });
};

module.exports = setSearchEvent;
