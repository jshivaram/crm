let filter = require('./filter');

///Settings properties : text selector, treeview li selector, button selector, treeViewLiSelector, spinnerContainer, treeViewContainer,
///draggableRequried
let setSearchEvent = function (settings) {
    let _addDraggableToLiElements = function () {
        $('#treeview li:has(ul)').addClass('lookup-li');
        $('#treeview li').not('.lookup-li').kendoDraggable({
            hint: function (element) {
                return element.clone();
            },
            dragstart: function (e) {
                e.currentTarget.hide();
            },
            dragend: function (e) {
                e.currentTarget.show();
            }
        });
    };

    let setInputElementsDisabled = function (buttonId, textId, value) {
        $(buttonId).prop('disabled', value);
        $(textId).prop('disabled', value);
    };

    let searchEvent = function () {
        let inputText = $(settings.textSelector).val().toLowerCase();
        var dataSource = $("#treeview").data("kendoTreeView").dataSource;

        filter(dataSource, inputText);
        if(settings.draggableRequried){
            _addDraggableToLiElements();
        }
    };

    $(settings.buttonSelector).off().click(searchEvent);

    $(settings.textSelector).off().keypress(function (e) {
        if (e.which == 13) {
            searchEvent();

        }
    });
};

module.exports = setSearchEvent;
