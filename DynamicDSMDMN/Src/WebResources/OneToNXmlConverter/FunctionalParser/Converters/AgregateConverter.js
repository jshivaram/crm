let BaseConverter = require('./BaseConverter');
  
class AgregateOperationConverter extends BaseConverter{
  constructor(methodName){
    super();
    this.methodName = methodName;

  }
  _makeConvertedCountExpression(content){
    return `${this.methodName}^${content}`;
  }

  getConvertedExpression(countExpression){
    if(typeof countExpression !== 'string'){
      throw 'countExpression is not a string';
    }

    let contentBetweenBrackets = this.getContentBetweenBrackets(countExpression);
    let result = this._makeConvertedCountExpression(contentBetweenBrackets);
    return result;
  }
}

module.exports = AgregateOperationConverter;
