class BaseConverter{
  getContentBetweenBrackets(expression){
    let indexOfBracket = expression.indexOf('(');
    let indexOfClosedBracket = expression.indexOf(')', expression.length -2);
    let expressionClosingBracketPosition =indexOfClosedBracket=== -1?expression.length: expression.length - 1;

    let contentBetweenBrackets = expression.substring(indexOfBracket + 1, expressionClosingBracketPosition);
    return contentBetweenBrackets;
  }
}

module.exports  = BaseConverter;
