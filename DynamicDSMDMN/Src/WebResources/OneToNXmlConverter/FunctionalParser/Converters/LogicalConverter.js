let BaseConverter = require('./BaseConverter');

class LogicalOperationConverter extends BaseConverter{
  constructor(methodName, entityName, operationTranslate){
    super();

    this.operationTranslate = operationTranslate;
    this.entityName = entityName;
    this.methodName = methodName;
}
  _multipleReplace(targetString, sourceReplace, destinationReplace){
    while(targetString.includes(sourceReplace)){
      targetString = targetString.replace(sourceReplace, destinationReplace);
    }
    return targetString;
  }

///Returns 'false' if index is -1 (not exists) or returns number of index if exists
  _findOperatorInsideString(targetString, operatorString){
    operatorString = ` ${operatorString} `;
    let index = targetString.indexOf(operatorString);
    if (index === -1){
      return false;
    }else{

      let operatorObject ={
        index,
        length: operatorString.length,
        operator: operatorString.trim()
      };

      return operatorObject;
    }
  };

  _findCorrectPositionOfOperator(targetString){
    for(let key in this.operationTranslate){
      let index = this._findOperatorInsideString(targetString, key);

      if(index){
        return index;
      }
    }
    throw 'WhereConverter have not found the valid operator in content position';
  };


///valueObject.leftValue 
///valueObject.operator
///valueObject.rightValue
  _createConvertedExpression(valueObject){
    let string = `${this.methodName}$${valueObject.leftValue};${valueObject.operator};${valueObject.rightValue}`;

    //string = this._multipleReplace(string, ' ', '');
    return string;
  };
  _deleteUnnesesaryEntityLogicalName(targetString, logicalName){
    let deletedLogicalNameString = targetString.replace(logicalName+'.', '');
    let checkSameLogicalName = deletedLogicalNameString.indexOf(logicalName);
    if(checkSameLogicalName=== 0){
      return targetString;
    }else{
      return deletedLogicalNameString;
    }
  };

  getConvertedExpression(expressionString){
    let contentBetweenBrackets = this.getContentBetweenBrackets(expressionString);
    let objectOperator = this._findCorrectPositionOfOperator(contentBetweenBrackets);

    let leftOperand = contentBetweenBrackets.substring(0, objectOperator.index);
    //let parsedLeftOperand = this._deleteUnnesesaryEntityLogicalName(leftOperand, this.entityName);

    let rightOperand = contentBetweenBrackets.substring(objectOperator.index + objectOperator.length);

    rightOperand = this._multipleReplace(rightOperand, ',', ';');

    let convertedOperator = this.operationTranslate[objectOperator.operator];

    let result = this._createConvertedExpression({
      leftValue:leftOperand,
      rightValue:rightOperand,
      operator:convertedOperator
    });

    return result;

  }
}

module.exports = LogicalOperationConverter;
