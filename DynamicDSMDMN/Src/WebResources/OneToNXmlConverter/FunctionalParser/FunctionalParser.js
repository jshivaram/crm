let FunctionalSequence = require('../Searcher/FunctionalSequence');

class FunctionalParser {
    constructor(currentEntity) {
        let MainConverter = require('./MainConverter');
        
        this.converter = new MainConverter(currentEntity);
    }
    _getSingleConvertedFunctionalSequence(functionalSequence) {
        let stringExpression = functionalSequence.functionExpression;

        let functionsArray = stringExpression.split(').');

        let resultExpression = '';

        for (let i = 0, length = functionsArray.length, lastIndex = functionsArray.length - 1; i < length; i++) {
            let target = functionsArray[i];
            let convertedFunction = this.converter.convertExpression(target);
            resultExpression += convertedFunction;

            if (i !== lastIndex) {
                resultExpression += '\\';
            }
        }
        resultExpression = `{{${resultExpression}}}`;

        let result = new FunctionalSequence(stringExpression, resultExpression);
        return result;
    }

    getConvertedFunctionalSequenceArray(funcSequenceArray) {
        let convertedFuncSequenceArray = [];

        funcSequenceArray.forEach(item => {
            let singleSeq = this._getSingleConvertedFunctionalSequence(item);
            convertedFuncSequenceArray.push(singleSeq);
        });

        return convertedFuncSequenceArray;
    }
}
module.exports = FunctionalParser;