class MainConverter{
    constructor(currentEntity){
        this.LogicalConverter = require('./Converters/LogicalConverter');
        this.AgregateConverter = require('./Converters/AgregateConverter');
        this.configFile = (require('../../Row_edditor/outputTypesOperationsConfig'))['1:N'];
        this.currentEntity = currentEntity;
    }

    _findMethodNameLowerCase(stringExpression){
        let indexOfOpenBracket = stringExpression.indexOf('(');
        let methodName =  stringExpression.substring(0,indexOfOpenBracket)
                                            .toLowerCase();
                                            
        return methodName;

    };
    convertExpression(stringExpression){
        let lowerCaseMethodName = this._findMethodNameLowerCase(stringExpression);
        let operationInfo = this.configFile[lowerCaseMethodName];
        
        if(!operationInfo){
            throw 'Config has no current operation';
        }
        let converter;

        if(operationInfo.isAgregate){
            converter = new this.AgregateConverter(lowerCaseMethodName);
        }else{
            let operationTranslate = (require('../../Row_edditor/outputOperatorsConfig')).custom;
            converter = new this.LogicalConverter(lowerCaseMethodName, this.currentEntity, operationTranslate);
        }


        let convertedExpression = converter.getConvertedExpression(stringExpression);
        return convertedExpression;
    }
}

module.exports =    MainConverter;