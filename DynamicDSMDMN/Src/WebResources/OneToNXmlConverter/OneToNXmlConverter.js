class OneToNXmlConverter{
    constructor(currentEntity){
        let FunctionalParser = require('./FunctionalParser/FunctionalParser');
        let Searcher = require('./Searcher/Searcher');
        let Swapper = require('./Swapper/Swapper');

        this.functionalParser = new FunctionalParser(currentEntity);
        this.searcher = new Searcher();
        this.swapper = new Swapper();
    }

    convertStringToXml(string){
        let bareFunctionalSequenceArray = this.searcher.getFunctionalSequenceArray(string);
        
         
        let convertedFunctionalSequenceArray = this.functionalParser.getConvertedFunctionalSequenceArray(bareFunctionalSequenceArray);
        let convertedString = this.swapper.swapFunctionalSequences(string, convertedFunctionalSequenceArray);
 
        return convertedString;
    }

}

module.exports = OneToNXmlConverter;