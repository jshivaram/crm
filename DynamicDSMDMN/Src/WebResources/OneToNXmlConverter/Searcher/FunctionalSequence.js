class FunctionalSequence{
    constructor(expression, convertedExpression){
        this.functionExpression = expression;
        this.convertedFunctionExpression = convertedExpression? convertedExpression: '';
    }
}
module.exports = FunctionalSequence;