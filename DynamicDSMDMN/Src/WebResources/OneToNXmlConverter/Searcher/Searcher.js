let FunctionalSequence = require('./FunctionalSequence');

class Searcher {
    constructor(methodArray) {
        if(methodArray){
            this.methodArray = methodArray;
        }else{
            this.methodArray = require('../MethodArray');
        }
    }
    ///returns 
    ///proerty closestOperation
    ///property closestIndex
    _searchClosestMatch(startPosition, queryString) {
        let closestOperation = '';
        let closestIndex = queryString.length;

        this.methodArray.forEach(operation => {
            let currentIndex = queryString.indexOf(operation+'(', startPosition);

            if (currentIndex === -1 || closestIndex === 0  || currentIndex > closestIndex) {
                return;
            } else {               
                    closestIndex = currentIndex;
                    closestOperation = operation;             
            }
        });
        if (closestIndex === queryString.length) {
            return false;
        }
        let resultObject = {
            closestOperation,
            closestIndex
        };

        return resultObject;
    }

    ///returns 
    ///startExpressionPosition  
    ///endExpressionPosition
    _getSingleFunctionalSequence(startPosition, queryString) {
        let closestMatch = this._searchClosestMatch(startPosition, queryString);
        if (!closestMatch) {
            return;
        }
        //let closestClosedBracketIndex = queryString.indexOf(')', closestMatch.closestIndex);
        let closestClosedBracketIndex = this._getValidClosestBracket(closestMatch.closestIndex, queryString);

        let sequenceClosedBracketPosition = closestClosedBracketIndex;

        while (true) {
            let charAfterBracket = queryString.charAt(sequenceClosedBracketPosition + 1);
            if (charAfterBracket !== '.') {
                break;
            } else {
                //sequenceClosedBracketPosition = queryString.indexOf(')', sequenceClosedBracketPosition + 1);
                sequenceClosedBracketPosition = this._getValidClosestBracket(sequenceClosedBracketPosition + 1, queryString);
            }

        }
        return {
            startExpressionPosition: closestMatch.closestIndex,
            endExpressionPosition: sequenceClosedBracketPosition
        };
    }

    _getValidClosestBracket(startPosition,queryString){
        let stringLength = queryString.length;
        let bracketNumber = -1;
        let endPosition;

        for (let i = startPosition; i < stringLength; i++) {
            let char = queryString[i];
            if (char === '(') {
                bracketNumber++;
                continue;
            }
            if (char === ')' && bracketNumber > 0) {
                bracketNumber--;
                continue;
            }
            if (char === ')' && bracketNumber === 0) {
                endPosition = i;
                break;
            }
        }

        if(bracketNumber < 0){
            console.error('When searching closest bracket, imposible take the result below then 0');
            throw 'error';
        }

        if(!endPosition){
            console.error('The end bracket hasn\' t been found in the xml converting string');
            throw 'error';
        }

        return endPosition;
    }

    getFunctionalSequenceArray(queryString) {
        let startPosition = 0;
        let functionalExpressionArray = [];
        
        while (true) {
            let stringRange = this._getSingleFunctionalSequence(startPosition, queryString);
            if (!stringRange || stringRange.endExpressionPosition === -1) {
                return functionalExpressionArray;
            }

            let expression = queryString.substring(stringRange.startExpressionPosition, stringRange.endExpressionPosition + 1);
            startPosition = stringRange.endExpressionPosition;

            let functSeq = new FunctionalSequence(expression);
            functionalExpressionArray.push(functSeq);
        }
    }
}
module.exports = Searcher;