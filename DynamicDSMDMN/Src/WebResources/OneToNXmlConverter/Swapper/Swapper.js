class Swapper{
    _swapSingleFunctionalSequence(queryString, functionalSequence){
        let targetString = functionalSequence.functionExpression;
        let sourceString = functionalSequence.convertedFunctionExpression;

        let swappedString = queryString.replace(targetString, sourceString);
        return swappedString;
    }

    swapFunctionalSequences(queryString, functionalSequenceArray){
        let result = queryString;
        functionalSequenceArray.forEach(item=>{
            result = this._swapSingleFunctionalSequence(result,item );
        });

        return result;
    }
}

module.exports = Swapper;