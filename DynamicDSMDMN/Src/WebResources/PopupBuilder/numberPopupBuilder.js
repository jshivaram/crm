let numberPopupBuilder = (function(){
    let operationArray = [
        '>',
        '<',
        '>=',
        '<=',
        '!=',
        '=',
        'in',
        'between'
    ];

    let singleNumericCase = [
        '>',
        '>=',
        '<',
        '<=',
        '==',
        '!='
    ];

    let betweenCase = 'between';
    let inCase = 'in';


    let createNumericTextBox = function(id){
        
        $('<input/>').attr('id', 'numeric')
                    .attr('type', 'number')
                    .appendTo(id);                   
    };

    let createOperationsBox = function(id, windowDiv){
        $(id).kendoDropDownList({
            dataSource: {
                data: operationArray
            },
            animation: {
                close: {
                    effects: "fadeOut zoom:out",
                    duration: 300
                },
                open: {
                    effects: "fadeIn zoom:in",
                    duration: 300
                }
            },

            select:function(e){
                
            }
        });
    }
     


    return function(id){
        let numberDiv = $('<div/>').attr('id', 'numberDiv');
        let operationDiv = $('<div/>').attr('id', 'operation');

        $(id).append(numberDiv)
            .append(operationDiv);


    }
})();