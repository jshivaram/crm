let setDropforPopupInput = function (inputSelector) {
    $(inputSelector).kendoDropTarget({
        drop: function (e) {

            let droppedObject = $('#treeview').data('kendoTreeView').dataItem(e.draggable.element);

            let value = `${droppedObject.logicalParent}.${droppedObject.logicalName}`;
            let displayValue = `${droppedObject.displayParent}.${droppedObject.displayName}`;
            $(inputSelector).val(displayValue);
            $(inputSelector).parent().find('input[type=hidden]').val(value);
        }
    })
};

module.exports = setDropforPopupInput;