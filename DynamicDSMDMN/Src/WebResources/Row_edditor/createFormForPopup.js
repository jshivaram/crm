let inputTypesOperationsConfig = require('./inputTypesOperationsConfig.js');
function refreshGrid() {
    let gridData = $('#grid').data('kendoGrid');
    gridData.refresh();
    $("#window").data("kendoWindow").close();
}

//this function return another function
//returned functionmust hand event - edit grid field
//:type - string argument, that contains one crm simpe type
//like sting, integer, decimal, etc.
//:operation - string argument, that contains operator look like <,=,!=
//or a function look like contains, substring

function createFormForPopup(type, operation, container, options) {
    let operationConfig = inputTypesOperationsConfig[type][operation];

    let $container = $('<div/>', {
        id: 'popup-form-content-container'
    });

    let $form = $('<form />', {
        id: 'pop-up-form'
    });

    let argsNumber = operationConfig.argsNumber;

    if (!argsNumber && argsNumber != 0) {
        throw 'Cant get argsNumber';
    }

    let dataObject = options.model[options.field];

    function canEdit() {
        return dataObject && dataObject.operator === operation;
    }

    if (argsNumber === 'N') {
        let $row = $('<div/>', {
            'class': 'pop-up-row'
        });

        $label = $('<label/>', {
            'class': 'pop-up-form-label'
        });
        $label.text('Arguments');

        let inputType = getInputType(type, operationConfig.argsType);

        let $input = $('<input />', {
            type: inputType,
            'class': 'k-textbox pop-up-form-input',
            name: 'arg'
        });

        let $hiddenInput = $('<input />', {
            type: 'hidden',
            'class': 'k-textbox pop-up-form-input-hidden',
            name: 'hidden'
        });

        if(canEdit()) {
            $input.val(dataObject.args);
        }

        $label.append($input);
        $label.append($hiddenInput);
        $row.append($label);
        $form.append($row);
    } else {
        for (let i = 0; i < argsNumber; i++) {
            let $row = $('<div/>', {
                'class': 'pop-up-row'
            });

            let $label = $('<label/>', {
                'class': 'pop-up-form-label'
            });
            $label.text('Argument' + (i + 1));

            let inputType = getInputType(type, operationConfig.argsType);

            let $input = $('<input />', {
                type: inputType,
                'class': 'k-textbox pop-up-form-input',
                name: 'arg'
            });
            let $hiddenInput = $('<input />', {
            type: 'hidden',
            'class': 'k-textbox pop-up-form-input-hidden',
            name: 'hidden'
        }); 

        $input.change(function(){
            $hiddenInput.val('');
        });

            if(canEdit()) {
                if (dataObject.displayView){
                    $input.val(dataObject.displayView[i]);
                    $hiddenInput.val(dataObject.args[i]);
                }else{
                    $input.val(dataObject.args[i]);
                }
            }

            $label.append($input);
            $label.append($hiddenInput);
            $row.append($label);
            $form.append($row);

             if(type === 'datetime') {
                $input.kendoDatePicker();
            }
        }
    }

    $submit = $('<input />', {
        type: 'submit',
        'class': 'k-button pop-up-submit'
    });
    $submit.val('Submit');
    $form.on('submit', function (options, operationConfig) {
        return function (ev) {
            ev.preventDefault();
            let getTitleTextForIndex = function(array, index, otherwiseElement){
                if(!array){
                    return otherwiseElement;
                }
                if(!array[index]){
                    return otherwiseElement;
                }

                return array[index];
            };
            let titleTextForDroppable;
            let serializedArray = $(ev.target).serializeArray();
            let hiddenInput = serializedArray.find(el=> el.name ==='hidden');
            serializedArray = serializedArray.filter(el => el.name !== 'hidden');

            if(hiddenInput && hiddenInput.value !== ''){
                titleTextForDroppable = [serializedArray[0].value];
                serializedArray[0].value = hiddenInput.value;
            }


            let argsValues = serializedArray.map(field => field.value);
            let result = '';

            let template = operationConfig.template;

            if (operationConfig.argsNumber === 0) {
                result = template;
            } else if (operationConfig.argsNumber === 'N') {
                result = template.replace('{{args}}', argsValues[0]);
            } else {
                let bufResult = template;
                argsValues.forEach((el, i) => {
                    bufResult = bufResult.replace('{{arg' + (i + 1) + '}}', getTitleTextForIndex(titleTextForDroppable,i,el) );
                });
                result = bufResult;
            }

            let resultObject = {
                datatype: type,
                operator: operation,

                args: argsValues,//argsValues.length <= 1 || operationConfig.argsNumber === 'N' ? argsValues[0]: argsValues,
                view: result,
                displayView: titleTextForDroppable ? titleTextForDroppable : null           
            };

            options.model[options.field] = resultObject;
            refreshGrid();
        }
    } (options, operationConfig));

    $submitRow = $('<div/>', {
        'class': 'pop-up-row'
    });

    $submitRow.append($submit);
    $form.append($submitRow);

    $container.append($form);


    return $container;
};


module.exports = createFormForPopup;

function getInputType(type, argsType) {
    let inputType = 'text';

    if (argsType) {
        return  argsType !== 'date' ? argsType : 'text';
    }

    if (type === 'integer') {
        inputType = 'number';
    }

    return inputType;
}
