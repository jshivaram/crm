let outputPopupConfig = require('./outputTypesOperationsConfig.js');
let toolsForPopupOutput = require('./toolsForPopupOutput');
let relationTypes = require('./relationTypes.js');

const NToOne = relationTypes.NToOne;
const OneToN = relationTypes.OneToN;
const NoRelation = relationTypes.NoRelation;

function createFunctionsColumn($functionsColumn, $functionDropDown, operations, getRelationType, getFunctionConfigForNoRelation,
    $popup, options, type, $resultInput) {

    $functionsColumn.append($functionDropDown);
    
    $functionDropDown.kendoDropDownList({
        dataSource: operations,
        height:400,
        dataTextField: "text",
        dataValueField: "value",
        optionLabel: 'Select function',
        select: function (e) {
            let dataItem = this.dataItem(e.item);
            let func = dataItem.text;

            let relationType = getRelationType($popup);
            let functionLogicalName = dataItem.value;

            let funcConf;
            if (relationType === NoRelation) {
                funcConf = getFunctionConfigForNoRelation(type, functionLogicalName);
            } else {
                funcConf = outputPopupConfig[relationType][functionLogicalName];
            }

            if (func) {
                let separator = funcConf && funcConf.separator ? funcConf.separator : "";
                let insertedString = separator + func + '()';
                let $textarea = $popup.find('.output-popup-result-input');
                toolsForPopupOutput.insertToTextarea($textarea, insertedString);
            }
        },
        dataBound: function () {

            let dropDownListData = $functionDropDown.data('kendoDropDownList');
            dropDownListData.value('');

            let dataObject = options.model[options.field];
            let view = dataObject ? dataObject.view : '';
            $resultInput.val(view);
        },
        popup: {
            appendTo: $popup
        }
    });
}

module.exports = createFunctionsColumn;