let toolsForPopupOutput = require('./toolsForPopupOutput.js');
let outputOperatorsConfig = require('../Row_edditor/outputOperatorsConfig.js');
let relationTypes = require('./relationTypes.js');

const NToOne = relationTypes.NToOne;

function createOperatorsDropDown($popup, $operatorsColumn, relationType) {

    if (relationType === NToOne) {
        return;
    }

    let operators = Object.keys(outputOperatorsConfig['custom']);

    if (!operators) {
        return;
    }

    let $operatorsDropDown = $('<select/>', {
        id: 'popup-operators-dropdown'
    });

    $operatorsColumn.append($operatorsDropDown);

    $operatorsDropDown.kendoDropDownList({
        dataSource: operators,
        optionLabel: 'Select operator',
        height: 400,
        select: function (e) {
            let operator = this.dataItem(e.item);

            let $textarea = $popup.find('.output-popup-result-input');
            toolsForPopupOutput.insertToTextarea($textarea, ' ' + operator + ' ');
        },
        popup: {
            appendTo: $popup
        }
    }).addClass('popup-dropdown-input');
}

module.exports = createOperatorsDropDown;