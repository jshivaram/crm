let outputPopupConfig = require('./outputTypesOperationsConfig.js');
let treeViewCreator = require('../TreeViewService/treeViewCreatorForPopup.js');
let toolsForPopupOutput = require('./toolsForPopupOutput');
let createFunctionsColumn = require('./createFunctionsColumn.js');
// let parseFormula = require('./parser/formulaParser.js');
let relationTypes = require('./relationTypes.js');
let createOperatorsDropDown = require('../Row_edditor/createOperatorsDropDown.js');
let clearMemory = require('./garbageCollector');


const NToOne = relationTypes.NToOne;
const OneToN = relationTypes.OneToN;
const NoRelation = relationTypes.NoRelation;

const baseOperations = ['/', '*', '+', '-'];

function refreshGrid(popup) {
    let gridData = $('#grid').data('kendoGrid');
    gridData.refresh();
    popup.close();
}

let $functionDropDown = $('<div/>', {
    id: 'popup-output-functions'
}).addClass('popup-dropdown-input');

let $resultInput = $('<textarea/>', {
    'class': 'output-popup-result-input k-textbox',
    rows: 10
});

function getRelationType($popup) {
    let $input = $popup.find('input[name="output-popup-relation-radio"]:checked');
    let selectedRelation = $input.val();

    return selectedRelation;
}

function renderFieldsTreeView($entityFieldsColumn, entityLogicalName, entityDisplayName, popup) {
    let appendSearch = require('./popupSearchSystem');

    let $popupEntityFieldsTree = $('<div/>', {
        id: 'popup-fields-tree-view',
        style: 'overflow:scroll; height: 49%; width: 100%;'
    });
    appendSearch($popupEntityFieldsTree,$entityFieldsColumn );
    //$entityFieldsColumn.append($popupEntityFieldsTree);

    treeViewCreator(
        '#popup-fields-tree-view',
        entityLogicalName,
        entityDisplayName,
        popup
    );
    let search = require('../Navigation/popupSearch');
    search({
        popup: popup,
        buttonSelector:'#popup-search-input-button',
        textSelector: '#popup-search-input-field',
        treeViewId: '#popup-fields-tree-view'
    });
}

function getHeadersForPopup() {
    let baseUrl = parent.Xrm.Page.context.getClientUrl() + '/WebResources';

    let headers = '';

    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.common.min.css" />';
    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.default.min.css" />';
    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.default.mobile.min.css" />';

    headers += '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />';
    headers += '<link rel="stylesheet" href="http://kendo.cdn.telerik.com/2016.2.714/styles/kendo.bootstrap.min.css" />';

    headers += '<link rel="stylesheet" href="' + baseUrl + '/kendoui_/kendo.office365.min.css"" />';
    headers += '<link rel="stylesheet" href="' + baseUrl + '/bpmn_/css/style.css" />';
    return headers;
}

function createPopup(title) {
    let params = 'width=' + screen.width +
        ', height=' + screen.height;

    let popup = window.open('about:blank', '', params);

    popup.getRelationType = getRelationType;

    // for test
    window.popup = popup;

    popup.moveTo(0, 0);
    popup.document.title = title;

    let headersHtml = getHeadersForPopup();

    let $popupHead = $(popup.document.head);
    let popupHeadHtml = $popupHead.html();

    popupHeadHtml += headersHtml;
    $popupHead.html(popupHeadHtml);

    return popup;
}

function createPopUpForOutputWindow(type, container, options, title, entityLogicalName) {
    let popup = createPopup(title);
    kendo.ui.progress($(window.popup.document.body), true);
    let $popup = $(popup.document.body);

    let $dropdown = $('<div/>').attr('id', 'dropdown');
    $popup.append($dropdown);

    let $popupContainer = $('<div/>', {
        id: 'popup-container'
    });
    $popupContainer.appendTo($popup);

    let $headerPopupRow = createPopupRow();

    let $headerPopupColTitle = createPopupCol(4);
    $titleSpan = $('<span/>', {
        'class': 'pop-up-title'
    });
    $titleSpan.text(type ? type : 'Output');

    $headerPopupColTitle.append($titleSpan);

    $headerPopupRow.append($headerPopupColTitle);
    $popupContainer.append($headerPopupRow);

    let $functionsColumn = $('<div />', {
        'class': 'output-popup-functions col-md-5'
    });

    let entityDisplayName = title.split(".")[0];

    let $radioButtonsBlock = createRadioButtonsBlock($functionsColumn, type,
        options, entityLogicalName, entityDisplayName, popup);
    $headerPopupRow.append($radioButtonsBlock);

    let $resultRow = $('<div/>', {
        'class': 'output-popup-result-block row'
    });

    let $resultCol = $('<div/>', {
        'class': 'output-popup-result-col col-md-12'
    });

    $resultCol.append($resultInput);
    $resultRow.append($resultCol);
    $popupContainer.append($resultRow);

    let $outputPopupBody = $('<div/>', {
        'class': 'outpot-popup-body row'
    });

    let $entityFieldsColumn = $('<div/>', {
        'class': 'output-popup-entity-feilds col-md-5'
    });

    let relationType = getRelationType($popup);
    if (relationType === OneToN || relationType === NToOne) {
        setTimeout(function () {
            renderFieldsTreeView($entityFieldsColumn, entityLogicalName, entityDisplayName, popup);
        }, 500);
    }
    else if (relationType === NoRelation) {
        setTimeout(function () {
            renderFieldsTreeView($entityFieldsColumn, entityLogicalName, entityDisplayName, popup);
        }, 500);
    }

    $outputPopupBody.append($functionsColumn);
    $outputPopupBody.append($entityFieldsColumn);
    $popupContainer.append($outputPopupBody);

    let $submitRow = $('<div/>', {
        'class': 'output-popup-submit-row row'
    });

    let $submitCol = $('<div/>', {
        'class': 'output-submit-col col-md-8'
    });

    let $submit = $('<input/>', {
        type: 'submit',
        value: 'Submit',
        'class': 'k-button'
    });

    $submit.on('click', function () {

        let selectedRelation = getRelationType($popup);

        let view = $resultInput.val();
        let operations = $popup.find('#popup-output-functions').data('kendoDropDownList')
            .dataSource.options.data;

        let expression = replaceAllDisplayNames(operations, view);

        // let initExps = [];
        // let formula;
        // try {
        //     formula = parseFormula(view, baseOperations, initExps, selectedRelation, type);
        // }
        // catch (err) {
        //     console.log(err);
        //     /*popup.alert(err);

        //     throw err;*/
        // }

        let resultObject = {
            relation: selectedRelation,
            datatype: type,
            args: null,
            view: view,
            expression: {
                value: expression
            }
            // ,formula: formula
        };

        console.log('resultObject');
        console.log(resultObject);

        options.model[options.field] = resultObject;
        refreshGrid(popup);
    });

    $submitCol.append($submit);
    $submitRow.append($submitCol);
    $popupContainer.append($submitRow);
}

function createPopupRow() {
    return $('<div/>', {
        'class': 'pop-up-row row'
    });
}

function createPopupCol(colSize, colOffset) {
    let col = $('<div/>', {
        'class': 'pop-up-col col-md-' + colSize + (colOffset ? 'col-md-offset-' + colOffset : '')
    });
    return col;
}

function createRadioButtonsBlock($functionsColumn, type, options,
    entityLogicalName, entityDisplayName, popup) {

    let $radioButtonsBlock = $('<div/>', {
        'class': 'pop-up-radio-block col-md-4 col-md-offset-1'
    });

    $ul = $('<ul/>', {
        'class': 'pop-up-radio-buttons-list field-list'
    });

    let $nToOneListItem = $('<li/>', {
        'class': 'pop-up-list-item'
    });

    let $oneToNListItem = $('<li/>', {
        'class': 'pop-up-list-item'
    });

    let $noRelationListItem = $('<li/>', {
        'class': 'pop-up-list-item'
    });

    // NToOne is default
    let radioNToOne = true;
    let radioOneToN = false;
    let radioNoRelation = false;

    let relation = options.model[options.field] ?
        options.model[options.field].relation : undefined;

    if (relation === OneToN) {
        radioNToOne = false;
        radioOneToN = true;
        radioNoRelation = false;
    } else if (relation === NoRelation) {
        radioNToOne = false;
        radioOneToN = false;
        radioNoRelation = true;
    }

    let $nToOneRadioSpan = createRelationRadio(radioNToOne, NToOne, $functionsColumn, type, options,
        popup, 'Nto1Radio', entityLogicalName, entityDisplayName);
    let $oneToNRadioSpan = createRelationRadio(radioOneToN, OneToN, $functionsColumn, type, options,
        popup, 'OnetoNRadio', entityLogicalName, entityDisplayName);
    let $noRelationSpan = createRelationRadio(radioNoRelation, NoRelation, $functionsColumn, type, options,
        popup, 'NoRelationRadio', entityLogicalName, entityDisplayName);

    $nToOneListItem.append($nToOneRadioSpan);
    $ul.append($nToOneListItem);

    $oneToNListItem.append($oneToNRadioSpan);
    $ul.append($oneToNListItem);

    $noRelationListItem.append($noRelationSpan);
    $ul.append($noRelationListItem);

    $radioButtonsBlock.append($ul);
    return $radioButtonsBlock;
}

function createRelationRadio(selected, relationType, $functionsColumn, type, options,
    popup, radioId, entityLogicalName, entityDisplayName) {

    $label = $('<label/>', {
        'class': 'popup-radio-label k-radio-label',
        'for': radioId
    });
    $label.text(relationType);

    let $radio = $('<input/>', {
        id: radioId,
        type: 'radio',
        'class': 'pop-up-radio k-radio',
        name: 'output-popup-relation-radio',
        value: relationType
    });

    $radio.on('click', onRadioClickWrapper($functionsColumn, type, options, relationType,
        popup, entityLogicalName, entityDisplayName));

    if (selected) {
        $radio.attr('checked', 'checked');
        onRadioClickWrapper($functionsColumn, type, options, relationType,
            popup, entityLogicalName, entityDisplayName)();
    }

    $radioSpan = $('<span/>', {
        'class': 'popup-radio-span'
    });

    $radioSpan.append($radio);
    $radioSpan.append($label);

    return $radioSpan;
}

function onRadioClickWrapper($functionsColumn, type, options, relationType,
    popup, entityLogicalName, entityDisplayName) {

    let $popup = $(popup.document.body);

    return function () {
        let operationsConfig;

        clearMemory('#popup-output-functions','kendoDropDownList',$popup);
        clearMemory('#popup-fields-tree-view','kendoTreeView',$popup);
        clearMemory('#popup-operators-dropdown','kendoDropDownList',$popup);

        $popup.find('.k-list-container.k-popup.k-group.k-reset').remove();


        let $entityFieldsColumn = $popup.find('.output-popup-entity-feilds').eq(0);
        let $functionsDropdownSelector = $popup.find('.output-popup-functions.col-md-5');
        
        $functionsDropdownSelector.empty();
        $entityFieldsColumn.empty();

        $resultInput.val('');

        if (relationType === NToOne) {
            operationsConfig = outputPopupConfig[relationType][type];

            if ($entityFieldsColumn.length) {
                renderFieldsTreeView($entityFieldsColumn, entityLogicalName, entityDisplayName, popup);
            }
        } else if (relationType === OneToN) {
            operationsConfig = outputPopupConfig[relationType];
            Object.keys(operationsConfig)
                .forEach(key => {
                    if (typeof (operationsConfig[key]) !== 'object') {
                        delete operationsConfig[key];
                    }
                });

            if ($entityFieldsColumn.length) {
                renderFieldsTreeView($entityFieldsColumn, entityLogicalName, entityDisplayName, popup);
            }

        } else if (relationType === NoRelation) {
            operationsConfig = getOperationConfigForNoRelation(type);

            if ($entityFieldsColumn.length) {
                renderFieldsTreeView($entityFieldsColumn, entityLogicalName, entityDisplayName, popup);
            }
        }

        let operations = Object.keys(operationsConfig).map(key => {
            let text = operationsConfig[key].displayName ? operationsConfig[key].displayName : key;
            return {
                value: key,
                text: text
            };
        });

        let $row = $('<div/>', {
            class: 'row'
        });

        let $funcCol = $('<div/>', {
            class: 'col-md-6'
        });

        let $operCol = $('<div/>', {
            id: 'popup-operators-column',
            class: 'col-md-6'
        });

        $functionsColumn.html("");

        $functionsColumn.append($row);
        $row.append($funcCol);
        $row.append($operCol);

        createFunctionsColumn($funcCol, $functionDropDown, operations, getRelationType, getFunctionConfigForNoRelation,
            $popup, options, type, $resultInput);

        createOperatorsDropDown($popup, $operCol, relationType);

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}

function getFunctionConfigForNoRelation(type, funcLogicalName) {
    let funcConf = outputPopupConfig[NToOne][type][funcLogicalName];

    if (!funcConf) {
        funcConf = outputPopupConfig[OneToN][funcLogicalName];
    }

    return funcConf;
}

function getOperationConfigForNoRelation(type) {
    let result = {};

    let operationsConfigNToOne = outputPopupConfig[NToOne][type];
    Object.keys(operationsConfigNToOne)
        .forEach(key => {
            result[key] = operationsConfigNToOne[key];
        });

    let operationsConfigOneToN = outputPopupConfig[OneToN];
    Object.keys(operationsConfigOneToN)
        .forEach(key => {
            if (typeof (operationsConfigOneToN[key]) === 'object') {
                result[key] = operationsConfigOneToN[key];
            }
        });

    return result;
}

function replaceAllDisplayNames(operations, expression) {

    if (operations) {
        operations.forEach(item => {
            expression = expression.replace(
                new RegExp(item.text, 'g'),
                item.value
            );
        });
    }

    return expression;
}

module.exports = createPopUpForOutputWindow;
