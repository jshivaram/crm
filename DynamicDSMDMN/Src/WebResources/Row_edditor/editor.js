let editor = function (metadata) {
    let inputTypesOperationsConfig = require('./inputTypesOperationsConfig.js')
    let createFormForPopup = require('./createFormForPopup.js');
    let createPopUpForOutputWindow = require('./createPopUpForOutputWindow.js');
    let dropEvent = require('../PopupBuilder/popupDrop');

    //executeDmnPopup creator 
    let createExecuteDmnPopup = require('../ExecuteDMN/Popup/popupCreator');

    let data = metadata;
    let _windowId = '#window';

    let stringEdit = function (container, options) {
        let input = $('<input/>');
        input.attr("name", options.field);
        input.appendTo(container);
    };

    let _createDropdownForWindow = function (elementType, container, options, elementLabel = 'Editor') {
        let $dropdown = $('<div/>').attr('id', 'dropdown');
        $('#window').empty();

        $('#window').append($dropdown);
        $('<div/>', {
            id: 'popup-container'
        }).appendTo('#window');

        let typeConfig = inputTypesOperationsConfig[elementType];

        let operations = [];

        Object.keys(typeConfig).forEach((key) => {
            let text = typeConfig[key].displayName ?
                typeConfig[key].displayName :
                key;
            operations.push({
                value: key,
                text: text
            });
        });

        $dropdown.kendoDropDownList({
            dataSource: operations,
            dataTextField: "text",
            dataValueField: "value",
            optionLabel: 'Select operation',
            animation: {
                close: {
                    effects: "fadeOut zoom:out",
                    duration: 300
                },
                open: {
                    effects: "fadeIn zoom:in",
                    duration: 300
                },

            },
            select: function (e) {
                var dataItem = this.dataItem(e.item);
                let val = dataItem.value;
                let $container = createFormForPopup(elementType, val, container, options);
                $('#popup-container').empty()
                    .append($($container));
                dropEvent('#popup-container input.pop-up-form-input');
            },
            dataBound: function () {
                let fieldObj = options.model[options.field];

                if (fieldObj) {
                    let operator = fieldObj.operator;

                    if (operator) {
                        $dropdown.data('kendoDropDownList').value(operator);
                        let $container = createFormForPopup(elementType, operator, container, options);
                        $('#popup-container').empty()
                            .append($($container));
                        dropEvent('#popup-container input.pop-up-form-input');
                    }
                }
            }
        });

        let $window = $("#window").kendoWindow({
            width: "600px",
            visible: false,
            actions: [
                "Close"
            ],
            animation: false
        });
        let windowData = $window.data("kendoWindow");
        windowData.center().open();
        windowData.title(elementLabel); //fix bug with unchanged title
    };

    return function (container, options) {

        let field = options.field;
        //Output editor
        if (field.indexOf('_Output') !== -1) {
            let dataElement = data.find(d => d.fieldName + '_Output' === field);
            let type = dataElement.type.toLowerCase();
            if (type === 'executedmn') {
                createExecuteDmnPopup(type, container, options, dataElement.title, dataElement.entityName);
            } else {
                createPopUpForOutputWindow(type, container, options, dataElement.title, dataElement.entityName);
            }
            //----------------------------------------------------------------

            //Input editor
        } else {

            let logicalConfig = (require('../Configs/multiple input config'))['logical'];

            let isNewPropertyParse = field.indexOf(logicalConfig.openBracket) !== -1;

            if (isNewPropertyParse) {
                let multipleInputService = require('../Tools/multiple input tool');
                let inputIndexString = multipleInputService.getValueWithBrackets(logicalConfig.openBracket,
                    logicalConfig.closeBracket,
                    field);

                let dataElement = data.find(d => d.fieldName + `_Input${inputIndexString}` === field);
                let type = dataElement.type.toLowerCase();
                _createDropdownForWindow(type, container, options, dataElement.title, dataElement.entityName);
            } else {
                let dataElement = data.find(d => d.fieldName + `_Input` === field);
                let type = dataElement.type.toLowerCase();
                _createDropdownForWindow(type, container, options, dataElement.title, dataElement.entityName);
            }


        }

        let value = options.model[options.field];
        value = value && value.view ? value.view : '';

        $('<span>' + value + '</span>')
            .appendTo(container);
    }
};

module.exports = editor;
