let clearMemory = function(selector, kendoDataType,popup ){
    let kendoElement = popup.find(selector).data(kendoDataType);

        if(kendoElement && kendoElement.destroy){
            kendoElement.destroy();
        }
}

module.exports = clearMemory;