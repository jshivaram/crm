let config = {
    'uniqueidentifier': {
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        }

    },
    //numbers begin
    'integer': {
        gt: {
            displayName: 'GrateThan',
            argsNumber: 1,
            type: 'operator',
            template: '>{{arg1}}'
        },
        gte: {
            displayName: 'GrateThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '>={{arg1}}'
        },
        lt: {
            displayName: 'LessThan',
            argsNumber: 1,
            type: 'operator',
            template: '<{{arg1}}'
        },
        lte: {
            displayName: 'LessThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '<={{arg1}}'
        },
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        between: {
            displayName: 'between',
            argsNumber: 2,
            type: 'operator',
            template: '[{{arg1}}..{{arg2}}]'
        },
        in: {
            displayName: 'In',
            argsNumber: 'N',
            type: 'operator',
            template: '[{{args}}]',
            argsType: 'text'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    'decimal': {
        gt: {
            displayName: 'GrateThan',
            argsNumber: 1,
            type: 'operator',
            template: '>{{arg1}}'
        },
        gte: {
            displayName: 'GrateThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '>={{arg1}}'
        },
        lt: {
            displayName: 'LessThan',
            argsNumber: 1,
            type: 'operator',
            template: '<{{arg1}}'
        },
        lte: {
            displayName: 'LessThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '<={{arg1}}'
        },
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        between: {
            displayName: 'between',
            argsNumber: 2,
            type: 'operator',
            template: '[{{arg1}}..{{arg2}}]'
        },
        in: {
            displayName: 'In',
            argsNumber: 'N',
            type: 'operator',
            template: '[{{args}}]',
            argsType: 'text'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    'double': {
        gt: {
            displayName: 'GrateThan',
            argsNumber: 1,
            type: 'operator',
            template: '>{{arg1}}'
        },
        gte: {
            displayName: 'GrateThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '>={{arg1}}'
        },
        lt: {
            displayName: 'LessThan',
            argsNumber: 1,
            type: 'operator',
            template: '<{{arg1}}'
        },
        lte: {
            displayName: 'LessThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '<={{arg1}}'
        },
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        between: {
            displayName: 'between',
            argsNumber: 2,
            type: 'operator',
            template: '[{{arg1}}..{{arg2}}]'
        },
        in: {
            displayName: 'In',
            argsNumber: 'N',
            type: 'operator',
            template: '[{{args}}]',
            argsType: 'text'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    'money': {
        gt: {
            displayName: 'GrateThan',
            argsNumber: 1,
            type: 'operator',
            template: '>{{arg1}}'
        },
        gte: {
            displayName: 'GrateThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '>={{arg1}}'
        },
        lt: {
            displayName: 'LessThan',
            argsNumber: 1,
            type: 'operator',
            template: '<{{arg1}}'
        },
        lte: {
            displayName: 'LessThanOrEqual',
            argsNumber: 1,
            type: 'operator',
            template: '<={{arg1}}'
        },
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        between: {
            displayName: 'between',
            argsNumber: 2,
            type: 'operator',
            template: '[{{arg1}}..{{arg2}}]'
        },
        in: {
            displayName: 'In',
            argsNumber: 'N',
            type: 'operator',
            template: '[{{args}}]',
            argsType: 'text'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    //numbers end
    'string': {
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        length: {
            displayName: 'Length',
            argsNumber: 1,
            type: 'function',
            template: 'length({{arg1}})'
        },
        contains: {
            displayName: 'Contains',
            argsNumber: 1,
            type: 'function',
            template: 'contains({{arg1}})'
        },
        not_contains: {
            displayName: 'Not Contains',
            argsNumber: 1,
            type: 'function',
            template: 'not_contains({{arg1}})'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        empty: {
            displayName: 'IsEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'IsEmpty()'
        }
    },
    'boolean': {
        true: {
            displayName: 'true',
            argsNumber: 0,
            type: 'bool',
            template: 'true'
        },
        false: {
            displayName: 'false',
            argsNumber: 0,
            type: 'bool',
            template: 'false'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    'datetime': {
        'gt': {
            displayName: 'GrateThan',
            argsNumber: 1,
            type: 'operator',
            argsType: 'date',
            template: '>{{arg1}}'
        },
        'lt': {
            displayName: 'LessThan',
            argsNumber: 1,
            type: 'operator',
            argsType: 'date',
            template: '<{{arg1}}'
        },
        'equals': {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            argsType: 'date',
            template: 'Equal({{arg1}})'
        },
        'between': {
            displayName: 'between',
            type: 'operator',
            argsType: 'date',
            argsNumber: 2,
            template: '[{{arg1}}..{{arg2}}]'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    //numbers end
    'picklist': {
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },
        length: {
            displayName: 'Length',
            argsNumber: 1,
            type: 'function',
            template: 'length({{arg1}})'
        },
        contains: {
            displayName: 'Contains',
            argsNumber: 1,
            type: 'function',
            template: 'contains({{arg1}})'
        },
        not_contains: {
            displayName: 'Not Contains',
            argsNumber: 1,
            type: 'function',
            template: 'not_contains({{arg1}})'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        isnull: {
            displayName: 'IsNull',
            argsNumber: 0,
            type: 'operator',
            template: 'IsNull()'
        }
    },
    'memo': {
        equals: {
            displayName: 'Equal',
            argsNumber: 1,
            type: 'operator',
            template: 'Equal({{arg1}})'
        },
        not_equals: {
            displayName: 'NotEqual',
            argsNumber: 1,
            type: 'operator',
            template: 'NotEqual({{arg1}})'
        },       
        contains: {
            displayName: 'Contains',
            argsNumber: 1,
            type: 'function',
            template: 'contains({{arg1}})'
        },
        not_contains: {
            displayName: 'Not Contains',
            argsNumber: 1,
            type: 'function',
            template: 'not_contains({{arg1}})'
        },
        not_empty: {
            displayName: 'NotEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'NotEmpty()'
        },
        empty: {
            displayName: 'IsEmpty',
            argsNumber: 0,
            type: 'operator',
            template: 'IsEmpty()'
        }
    }
}

module.exports = config;
