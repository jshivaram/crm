let conf = {
    custom: {
        '>': 'gt',
        '>=': 'gte',
        '<': 'lt',
        '<=': 'lte',
        '=': 'equals',
        '!=': 'not_equals',
        'in': 'in',
        'between': 'between',
        'contains': 'contains',
        'not_contains': 'not_contains',
    },
    integer: ['integer', '>', '>=', '<', '<=', '==', '!='],
    decimal: ['decimal', '>', '>=', '<', '<=', '==', '!='],
    double: ['double', '>', '>=', '<', '<=', '==', '!='],
    money: ['money', '>', '>=', '<', '<=', '==', '!='],
    string: ['string', '>', '>=', '<', '<=', '==', '!='],
    boolean: ['boolean', '==', '!='],
    datetime: ['datetime', '>', '>=', '<', '<=', '==', '!=']
}

module.exports = conf;