var config = {
    'N:1': {
        'showEntityBox': false,
        'memo': {
            'setValue': {
                displayName: 'SetValue',
                argsType: 'number',
                argsNumber: 'setValue',
                template: 'SetValue({{args}})'
            }
        },
        'string': {
            'concat': {
                displayName: 'Concat',
                argsType: 'number',
                argsNumber: 'concat',
                template: 'Concat({{args}})'
            },
            'setValue': {
                displayName: 'SetValue',
                argsType: 'number',
                argsNumber: 'setValue',
                template: 'SetValue({{args}})'
            }
        },
        'picklist': {
            'setValue': {
                displayName: 'SetValue',
                argsType: 'number',
                argsNumber: 'setValue',
                template: 'SetValue({{args}})'
            }
        },
        'integer': {
            'addition': {
                displayName: 'Addition',
                argsNumber: 'N',
                template: 'Addition({{args}})'
            },
            'subtraction': {
                displayName: 'Subtraction',
                argsNumber: '2',
                template: 'Subtraction({{arg1}},{{arg2}})'
            },
            'multiply': {
                displayName: 'Multiply',
                argsNumber: 'N',
                template: 'Multiply({{args}})',
            },
            'divide': {
                displayName: 'Divide',
                argsNumber: 2,
                template: 'Divide({{arg1}},{{arg2}})'
            },
            'setValue': {
                displayName: 'SetValue',
                argsNumber: 1,
                template: 'setValue({{arg1}})'
            }
        },
        'decimal': {
            'addition': {
                displayName: 'Addition',
                argsNumber: 'N',
                template: 'Addition({{args}})',
            },
            'subtraction': {
                displayName: 'Subtraction',
                argsNumber: '2',
                template: 'Subtraction({{arg1}},{{arg2}})'
            },
            'multiply': {
                displayName: 'Multiply',
                argsNumber: 'N',
                template: 'Multiply({{args}})',
            },
            'divide': {
                displayName: 'Divide',
                argsNumber: 2,
                template: 'Divide({{arg1}},{{arg2}})'
            },
            'setValue': {
                displayName: 'SetValue',
                argsNumber: 1,
                template: 'setValue({{arg1}})'
            }
        },
        'double': {
            'addition': {
                displayName: 'Addition',
                argsNumber: 'N',
                template: 'Addition({{args}})'
            },
            'subtraction': {
                displayName: 'Subtraction',
                argsNumber: '2',
                template: 'Subtraction({{arg1}},{{arg2}})'
            },
            'multiply': {
                displayName: 'Multiply',
                argsNumber: 'N',
                template: 'Multiply({{args}})',
            },
            'divide': {
                displayName: 'Divide',
                argsNumber: 2,
                template: 'Divide({{arg1}},{{arg2}})'
            },
            'setValue': {
                displayName: 'SetValue',
                argsNumber: 1,
                template: 'setValue({{arg1}})'
            }
        },
        'money': {
            'addition': {
                displayName: 'Addition',
                argsNumber: 'N',
                template: 'Addition({{args}})',
            },
            'subtraction': {
                displayName: 'Subtraction',
                argsNumber: '2',
                template: 'Subtraction({{arg1}},{{arg2}})'
            },
            'multiply': {
                displayName: 'Multiply',
                argsNumber: 'N',
                template: 'Multiply({{args}})',
            },
            'divide': {
                displayName: 'Divide',
                argsNumber: 2,
                template: 'Divide({{arg1}},{{arg2}})'
            },
            'setValue': {
                displayName: 'SetValue',
                argsNumber: 1,
                template: 'setValue({{arg1}})'
            }
        },
        'boolean': {
            'isTrue': {
                displayName: 'IsTrue',
                argsNumber: 1,
                template: 'IsTrue({{arg1}})'
            },
            'isFalse': {
                displayName: 'isFalse',
                argsNumber: 1,
                template: 'IsFalse({{arg1}})'
            },
            'setValue': {
                displayName: 'SetValue',
                argsNumber: 1,
                template: 'setValue({{arg1}})'
            }
        },
        'datetime': {
            'setValue': {
                displayName: 'SetValue',
                argsNumber: 1,
                template: 'setValue({{arg1}})'
            },
            'now': {
                displayName: 'Now',
                argsNumber: 0,
                template: 'Now'
            },
            'addDays': {
                displayName: 'AddDays',
                argsNumber: 1,
                template: 'AddDays({{arg1}},{{arg2}})'
            },
            'addMonths': {
                displayName: 'AddMonths',
                argsNumber: 1,
                template: 'AddMonths({{arg1}},{{arg2}})'
            },
            'addYears': {
                displayName: 'AddYears',
                argsNumber: 1,
                template: 'AddYears({{arg1}},{{arg2}})'
            },
        },
        'outputvalue':{
            'concat': {
                displayName: 'Concat',
                argsType: 'number',
                argsNumber: 'concat',
                template: 'Concat({{args}})'
            },
            'addition': {
                displayName: 'Addition',
                argsNumber: 'N',
                template: 'Addition({{args}})'
            },
            'subtraction': {
                displayName: 'Subtraction',
                argsNumber: '2',
                template: 'Subtraction({{arg1}},{{arg2}})'
            },
            'multiply': {
                displayName: 'Multiply',
                argsNumber: 'N',
                template: 'Multiply({{args}})',
            },
            'divide': {
                displayName: 'Divide',
                argsNumber: 2,
                template: 'Divide({{arg1}},{{arg2}})'
            },
            'isTrue': {
                displayName: 'IsTrue',
                argsNumber: 1,
                template: 'IsTrue({{arg1}})'
            },
            'isFalse': {
                displayName: 'isFalse',
                argsNumber: 1,
                template: 'IsFalse({{arg1}})'
            },
            'now': {
                displayName: 'Now',
                argsNumber: 0,
                template: 'Now'
            },
            'addDays': {
                displayName: 'AddDays',
                argsNumber: 1,
                template: 'AddDays({{arg1}},{{arg2}})'
            },
            'addMonths': {
                displayName: 'AddMonths',
                argsNumber: 1,
                template: 'AddMonths({{arg1}},{{arg2}})'
            },
            'addYears': {
                displayName: 'AddYears',
                argsNumber: 1,
                template: 'AddYears({{arg1}},{{arg2}})'
            }
        }
    },
    '1:N': {
        'showEntityBox': true,
        'count': {
            isAgregate: true,
            displayName: 'Count',
            argsNumber: 1,
            template: 'Count({{arg1}})'
        },
        'where': {
            isHaveOperators: true,
            separator: '.',
            displayName: 'Where',
            argsNumber: 2,
            template: 'Where({{arg1}}{{operator}}{{arg2}})',
            operators: ['=', '!=', '<', '>', 'in'],
            operationTranslate: {
                '=': 'equals',
                '!=': 'not_equals',
                '>': 'gt',
                '<': 'lt',
                'in': 'in'

            }
        },
        'and': {
            isHaveOperators: true,
            separator: '.',
            displayName: 'And',
            argsNumber: 2,
            template: 'And({{arg1}}{{operator}}{{arg2}})',
            operators: ['=', '!=', '<', '>', 'in', 'between'],
            operationTranslate: {
                '=': 'equals',
                '!=': 'not_equals',
                '>': 'gt',
                '<': 'lt',
                'in': 'in',
                'between': 'between'

            }
        },
        'or': {
            isHaveOperators: true,
            separator: '.',
            displayName: 'Or',
            argsNumber: 2,
            template: 'Or({{arg1}}{{operator}}{{arg2}})',
            operators: ['=', '!=', '<', '>', 'in'],
            operationTranslate: {
                '=': 'equals',
                '!=': 'not_equals',
                '>': 'gt',
                '<': 'lt',
                'in': 'in'
            }
        },
        'sum': {
            isAgregate: true,
            displayName: 'Sum',
            argsNumber: 1,
            template: 'Addition({{arg1}})'
        },
        'rel_max': {
            isAgregate: true,
            displayName: 'MaxAgr',
            argsNumber: 1,
            template: 'MaxAgr({{arg1}})'
        },
        'rel_min': {
            isAgregate: true,
            displayName: 'MinAgr',
            argsNumber: 1,
            template: 'MinAgr({{arg1}})'
        },
        'rel_avg': {
            isAgregate: true,
            displayName: 'AvgAgr',
            argsNumber: 1,
            template: 'AvgAgr({{arg1}})'
        },


    },
    'GlobalFunctions': {
        'Abs': {},
        'Acos': {},
        'Asin': {},
        'Atan': {},
        'Ceiling': {},
        'Cos': {},
        'Exp': {},
        'Floor': {},
        'IEEERemainder': {},
        'Log': {},
        'Log10': {},
        'Max': {},
        'Min': {},
        'Pow': {},
        'Round': {},
        'Sign': {},
        'Sqrt': {},
        'Tan': {},
        'Truncate': {}

    },
    ExecuteDmn: {
        getMtoO: {
            displayName: 'ManyToOne'
        },
        getOtoM: {
            displayName: 'OneToMany'
        }
    },
    //Template, how executeDmn rule should be dropped in xml file.
    //example:{functionName}{functionSeparator}{arg1}{argsSeparator}{arg2}
    executeDmnXmlTemplate: {
        'functionName': 'ExecuteDMN',
        'functionSeparator': '^',
        'argsSeparator': ','
    }
}

module.exports = config;
