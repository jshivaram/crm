const outputPopupConfig = require('../outputTypesOperationsConfig.js');

const NToOne = 'N:1';
const OneToN = '1:N';
const NoRelation = 'NoRelation';

function getFunctionDisplayName(partOfExpression) {
    let result = partOfExpression.substr(0, partOfExpression.indexOf("("));

    return result;
}

function getFunctionArguments(partOfExpression) {
    let result = partOfExpression.substr(partOfExpression.indexOf("("), partOfExpression.length);

    result = result.replace("(", "").replace(")", "");

    return result;
}

function getFunctionConf(funcDisplayName, relationType, type) {
    let objFuncs;

    if (relationType === NToOne) {
        objFuncs = outputPopupConfig[relationType][type];
    } else if (relationType === OneToN) {
        objFuncs = outputPopupConfig[relationType];
    } else if (relationType === NoRelation) {
        objFuncs = {};

        let nToOneConfig = outputPopupConfig[NToOne][type];
        let oneToNConfig = outputPopupConfig[OneToN];

        let keysNToOne = Object.keys(nToOneConfig);
        keysNToOne.forEach(key => {
            if (typeof nToOneConfig[key] === 'object') {
                objFuncs[key] = nToOneConfig[key];
            }
        });

        let keysOneToN = Object.keys(oneToNConfig);
        keysOneToN.forEach(key => {
            if (typeof oneToNConfig[key] === 'object') {
                objFuncs[key] = oneToNConfig[key];
            }
        });
    }

    if (objFuncs) {
        let arrProps = Object.keys(objFuncs);

        for (let i = 0; i < arrProps.length; i++) {
            let prop = arrProps[i];

            if (objFuncs[prop].displayName === funcDisplayName) {

                let separator = objFuncs[prop].separator ? objFuncs[prop].separator : "";

                let result = {
                    logicalName: prop,
                    separator: separator
                };

                return result;
            }
        }

        throw "Error, function " + funcDisplayName + " is not correct for relation "
            + relationType + " and type " + type;
    }
}

function parseExpression(expression, relationType, type) {
    let partsOfExpression = expression.replace(/\)\./g, ")).").split(").");

    let result = [];
    partsOfExpression.forEach(partOfExpression => {

        let funcDisplayName = getFunctionDisplayName(partOfExpression);
        let funcConf = getFunctionConf(funcDisplayName, relationType, type);
        let funcArguments = getFunctionArguments(partOfExpression);

        result.push({
            separator: funcConf.separator,
            funcLogicalName: funcConf.logicalName,
            funcArguments: funcArguments
        });
    });

    return result;
}

module.exports = parseExpression;