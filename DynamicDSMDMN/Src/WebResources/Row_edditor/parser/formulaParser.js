let parseExpression = require('./expressionParser.js');

function getOperation(formula, operations) {

    var ops = operations.join("");

    var result = formula.match(new RegExp("[" + ops + "]"));

    if (result) {
        return result[0];
    }
}

function parseFormula(formula, operations, expressions, relationType, type) {
    var op = getOperation(formula, operations);

    if (op) {
        var firstPart = formula.slice(0, formula.indexOf(op)).trim();
        var secondPart = formula.slice(formula.indexOf(op) + 1, formula.length).trim();

        if (!firstPart || !secondPart) {
            throw "Error, formula is not correct";
        }

        expressions = addExpression(expressions, firstPart, op, relationType, type);

        return parseFormula(secondPart, operations, expressions, relationType, type)
    } else {
        formula = formula.trim();

        expressions = addExpression(expressions, formula, "", relationType, type);

        return expressions;
    }
}

function addExpression(expressions, exp, operation, relationType, type) {

    let expression;

    if (isNumeric(exp)) {
        expression = exp;
    } else {
        expression = parseExpression(exp, relationType, type);
    }

    expressions.push({
        expression: expression,
        operation: operation
    });

    return expressions;
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

module.exports = parseFormula;