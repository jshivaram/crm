let createInputsRow = function(){
    let inputField = $('<input/>', {
        id:'popup-search-input-field',
        class:'k-textbox',
        type:'text',
        placeholder:'Search...'
    }).css('width','100%');

    let inputButton = $('<input/>', {
        id:'popup-search-input-button',
        class:'k-button',
        value:'Search',
        type:'button'
    }).css('width','100%');

    let fieldColmd = $('<div/>',{
        class:'col-md-9'
    });

    let buttonColmd = $('<div/>',{
        class:'col-md-3'
    });

    let row = $('<div/>',{
        class:'row'
    });  

    fieldColmd.append(inputField);
    buttonColmd.append(inputButton);
    row.append(fieldColmd)
        .append(buttonColmd);

    return row;
        
}

let createSearch = function(popupTreeViewElement, parentDiv){

    let row = $('<div/>',{
        class:'row'
    }); 
    let searchDiv = createInputsRow();

    row.append(popupTreeViewElement);

    parentDiv.append(searchDiv)
            .append(row);
}

module.exports = createSearch;