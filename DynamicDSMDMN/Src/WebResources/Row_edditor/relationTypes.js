
let obj = {
    NToOne: 'N:1',
    OneToN: '1:N',
    NoRelation: 'NoRelation'
};

module.exports = obj;