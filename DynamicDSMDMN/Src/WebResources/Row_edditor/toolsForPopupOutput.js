function insertToTextarea($textarea, insertedString) {
    let start = $textarea.prop("selectionStart");
    let value = $textarea.val();

    let output = insert(value, insertedString, start);

    $textarea.val(output);
}

function insert(baseString, insertedString, position) {
    let output = [
        baseString.slice(0, position),
        insertedString,
        baseString.slice(position)
    ].join('');

    return output;
}

let tools = {
    insertToTextarea: insertToTextarea
};

module.exports = tools;
