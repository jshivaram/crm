let multipleInputCreateService = (function () {
    let getValueBetweenBrackets = function (leftBracketString, rightBracketString, targetString) {
        try {
            let leftBracketIndex = targetString.indexOf(leftBracketString);
            let startValueIndex = leftBracketIndex + leftBracketString.length;

            let rightBracketIndex = targetString.indexOf(rightBracketString, startValueIndex);

            if (rightBracketIndex === -1 || leftBracketIndex === -1) {
                throw 'string doesn\'t contain brackets';
            }

            let result = targetString.substring(startValueIndex, rightBracketIndex);
            return result;

        } catch (error) {
             
        }
    }

    let clearFromBrackets = function (leftBracketString, rightBracketString, targetString) {
        try {
            let leftBracketIndex = targetString.indexOf(leftBracketString);
            let rightBracketIndex = targetString.indexOf(rightBracketString);

            let rightDiapasone = rightBracketIndex + rightBracketString.length;

            let leftResultString = targetString.substring(0, leftBracketIndex);
            let rightResultString = targetString.substring(rightDiapasone);

            let result = leftResultString + rightResultString;
            return result;

        } catch (error) {
             
        }
    }

    let getValueWithBrackets = function (leftBracketString, rightBracketString, targetString) {
        let leftBracketIndex = targetString.indexOf(leftBracketString);
        let rightBracketIndex = targetString.indexOf(rightBracketString);

        let rightDiapasone = rightBracketIndex + rightBracketString.length;

        let result = targetString.substring(leftBracketIndex, rightDiapasone);

        return result;
    }

    return {
        getValueBetweenBrackets,
        clearFromBrackets,
        getValueWithBrackets
    }
})();

module.exports = multipleInputCreateService;