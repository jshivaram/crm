let getDisplayName = require('./getDisplayName');

let appendTreeItems = function (popup, treeView, dataArray) {
    dataArray.forEach(el => {
        let LogicalName = el.LogicalName;
        let DisplayName = getDisplayName(el);

        if(!DisplayName){
            return;
        }

        let insertObject = {
            text: DisplayName,
            type: 'Lookup',
            title: DisplayName,
            fieldName: LogicalName,
            target: LogicalName,
            logicalParent: '',
            displayParent: '',
            items: [{
                text: 'Loading...'
            }]
        }

        let html = treeView.append(insertObject);
        popup.find(html).closest('li').addClass('lookup-li');
    });
}

module.exports = appendTreeItems;

