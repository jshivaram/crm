 function getDisplayName(item) {
    let label = item.DisplayName.UserLocalizedLabel ?
        item.DisplayName.UserLocalizedLabel.Label :
        item.DisplayName.LocalizedLabels ? item.DisplayName.LocalizedLabels.Label : item.LogicalName;

    return label;
}

module.exports = getDisplayName;