let getDisplayName = require('./getDisplayName');

let appendTreeItems = function (popup, treeView, dataArray, displayName) {
    dataArray.forEach(el => {
        let LogicalName = el.LogicalName;
        let DisplayName = getDisplayName(el);

        if(!DisplayName){
            return;
        }

        let insertObject = {
            text: DisplayName,
            type: el.AttributeType,
            title: DisplayName,
            fieldName: LogicalName,
            logicalParent: el.EntityLogicalName,
            displayParent: displayName           
        }

        if(el.AttributeType === 'Lookup'){
            insertObject.items =[{
                text: 'Loading...'
            }];
            insertObject.target = el.Targets[0];
        }

        let html = treeView.append(insertObject);
        if(el.AttributeType === 'Lookup'){
            popup.find(html).closest('li').addClass('lookup-li');
        };
    });
}

module.exports = appendTreeItems;