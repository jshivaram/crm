let getDisplayName = require('./getDisplayName');

let appendTreeItems = function (popup, treeView, dataArray, displayParent, logicalParent) {
    dataArray.forEach(el => {
        let LogicalName = el.LogicalName;
        let DisplayName = getDisplayName(el);

        if(!DisplayName){
            return;
        }

        let insertObject = {
            text: DisplayName,
            type: 'Lookup',
            title: DisplayName,
            fieldName: LogicalName,
            logicalParent: logicalParent,
            displayParent: displayParent,
            target: LogicalName,
            items: [{
                text: 'Loading...'
            }]
        }



        let html = treeView.append(insertObject);
        popup.find(html).closest('li').addClass('lookup-li');
    });
}

module.exports = appendTreeItems;