let createTreeForId = (function () {
    let _treeViewService = require('./treeViewService');
    let createTree = function (id, logicalName, events) {
        _treeViewService.setId(id);
        _treeViewService.initTree(events);
        _treeViewService.createInitTreeForLogicalName(logicalName);

    }
    return createTree;
})();
module.exports = createTreeForId;
