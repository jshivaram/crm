let treeViewForPopup = function (id, entityLogicalName, entityDisplayName, popup) {
    let _converter = require('../Data_Parser/crm-data-converter');
    let toolsForPopupOutput = require('../Row_edditor/toolsForPopupOutput');
    let baseUrl = parent.Xrm.Page.context.getClientUrl();
    let relationTypes = require('../Row_edditor/relationTypes.js');
    let outputPopupConfig = require('../Row_edditor/outputTypesOperationsConfig.js');

    const NToOne = relationTypes.NToOne;
    const OneToN = relationTypes.OneToN;
    const NoRelation = relationTypes.NoRelation;

    let $popup = $(popup.document.body);

    startProcessCreatingTreeView(entityLogicalName, entityDisplayName);

    function createKendoTreeView() {
        $popup.find(id).kendoTreeView({
            animation: false,
            expand: expandFunction(id),
            select: ev => {
                let dataSourceRowElement = $popup.find('#popup-fields-tree-view').data('kendoTreeView').dataItem(ev.node);

                let logicalName = dataSourceRowElement.fieldName ? dataSourceRowElement.fieldName : dataSourceRowElement.target;
                let logicalResult = dataSourceRowElement.logicalParent + '.' + logicalName;
                logicalResult = logicalResult.replace('undefined.', '');

                let displayResult = dataSourceRowElement.displayParent + '.' + dataSourceRowElement.text;
                displayResult = displayResult.replace('undefined.', '');

                if (logicalResult[0] === '.' && displayResult[0] === '.') {
                    logicalResult = logicalResult.substr(1);
                    displayResult = displayResult.substr(1);
                }

                let $textarea = $popup.find('.output-popup-result-input');
                toolsForPopupOutput.insertToTextarea($textarea, logicalResult);
            }
        });
    }

    function createTreeViewFromCache(cachedDataSource) {
        let _$treeView;
        $popup.find(id).empty();

        createKendoTreeView();
        _$treeView = $popup.find(id).data('kendoTreeView');

        cachedDataSource.forEach(item => _$treeView.append(item));

    };

    function createTreeView(data, entityLogicalName, displayLogicalName) {
        if (!window.top.cache) {
            window.top.cache = {};
        }
        console.log('Creating Tree View');
        console.log(data);
        createKendoTreeView();

        let treeView = $popup.find('#popup-fields-tree-view').data('kendoTreeView');

        let ulIsMarked = false;

        let relationType = popup.getRelationType($popup);

        if (treeView) {
            if (relationType === 'NoRelation') {
                let convert = require('./ItemsAppend/NoRelation');
                convert($popup, treeView, data);
            } else if (relationType === 'N:1') {
                let convert = require('./ItemsAppend/nToOne');
                convert($popup, treeView, data, displayLogicalName);
            } else if (relationType === '1:N') {
                let convert = require('./ItemsAppend/oneToN');
                convert($popup, treeView, data, displayLogicalName, entityLogicalName);
            }
            window.top.cache[relationType] = treeView.dataSource.data();
        }
    }

    function appendWayInformation(html, logicalName, displayEntityName) {

        let kendoItem = $popup.find('#popup-fields-tree-view').data('kendoTreeView').dataItem(html);

        kendoItem.logicalName = logicalName;
        kendoItem.logicalParent = logicalName;
        kendoItem.displayParent = displayEntityName;


    }

    function expandFunction(_id) {
        return function (e) {

            let node = e.node;
            let length = $popup.find('#popup-fields-tree-view').data('kendoTreeView').dataItem(e.node).items.length;
            if (length <= 1) {

                let curentObject = $popup.find(_id).data("kendoTreeView").dataItem(node);
                let target = curentObject.target;
                let linkedField = curentObject.fieldName;
                (function (name, linkedField, _id) {
                    $.ajax({
                        type: 'GET',
                        //url: Xrm.Page.context.getClientUrl() +`/api/data/v8.1/EntityDefinitions(${id})?$select=LogicalName&$expand=Attributes($select=DisplayName,LogicalName,AttributeType, EntityLogicalName, )`,
                        url: parent.Xrm.Page.context.getClientUrl() + `/api/data/v8.1/EntityDefinitions()?$select=LogicalName,DisplayName&$expand=Attributes&$filter= LogicalName eq '${name}'`,
                        headers: {
                            'Accept': 'application/json',
                            "Content-Type": "application/json; charset=utf-8",
                            'OData-MaxVersion': "4.0",
                            "OData-Version": "4.0"
                        }
                    }).done(item => {

                        let emptyDeleted = false;
                        let displayEntityName;

                        try {
                            displayEntityName = item.value[0].DisplayName.LocalizedLabels[0].Label;
                        } catch (e) {
                            displayEntityName = logicalName;
                        }

                        let _treeView = $popup.find(_id).data("kendoTreeView");
                        let recievedAttributes = item.value[0].Attributes;
                        let logicalName = item.value[0].LogicalName;
                        let convertedArray = _converter.convertCrmDataArray(recievedAttributes, item.value[0].LogicalName);


                        convertedArray.forEach(attribute => {
                            let logicalParent = `${curentObject.logicalParent}.${linkedField}`;
                            let displayParent = `${curentObject.displayParent}.${displayEntityName}`;

                            if (attribute.type === 'Lookup') {
                                let html = _treeView.append({
                                    text: attribute.title,
                                    entityName: attribute.entityName,
                                    fieldName: attribute.fieldName,
                                    displayName: attribute.title,
                                    type: attribute.type,
                                    logicalParent: logicalParent,
                                    displayParent: displayParent,
                                    target: attribute.targets[0],
                                    items: [{
                                        text: 'Loading...'
                                    }]
                                }, $popup.find(node));
                                $popup.find(html).closest('li').addClass('lookup-li');

                                if (!emptyDeleted) {
                                    let loadingPlace = _treeView.findByText('Loading...');
                                    _treeView.remove(loadingPlace);
                                    emptyDeleted = true;
                                }
                            } else {
                                let html = _treeView.append({
                                    text: attribute.title,
                                    entityName: attribute.entityName,
                                    fieldName: attribute.fieldName,
                                    displayName: attribute.title,
                                    type: attribute.type,
                                    logicalParent: logicalParent,
                                    displayParent: displayParent,
                                }, $(node));

                                if (!emptyDeleted) {
                                    let loadingPlace = _treeView.findByText('Loading...');
                                    _treeView.remove(loadingPlace);
                                    emptyDeleted = true;
                                }
                            }
                        });

                        let relationType = popup.getRelationType($popup);
                        window.top.cache[relationType] = _treeView.dataSource.data();
                    });
                })(target, linkedField, _id);
            }
        }
    }

    function startProcessCreatingTreeView(entityLogicalName, displayLogicalName) {
        kendo.ui.progress($(window.popup.document.body), true);
        entityLogicalName = entityLogicalName ? entityLogicalName.split('.')[0] : entityLogicalName;

        let url = baseUrl + "/api/data/v8.1/RelationshipDefinitions" +
            "/Microsoft.Dynamics.CRM.OneToManyRelationshipMetadata" +
            "?$select=ReferencingEntity&$filter=ReferencedEntity eq '" + entityLogicalName + "'";

        let relationType = popup.getRelationType($popup);

        if (window.top.cache && window.top.cache[relationType]) {
            let cachedDataSource = window.top.cache[relationType];
            createTreeViewFromCache(cachedDataSource);
            kendo.ui.progress($(window.popup.document.body), false);
        } else {
            if (relationType === NToOne) {

                url = baseUrl + `/api/data/v8.1/EntityDefinitions()?$select=LogicalName,DisplayName&$expand=Attributes&$filter= LogicalName eq '${entityLogicalName}'`



            } else if (relationType === NoRelation) {
                url = baseUrl + '/api/data/v8.1/EntityDefinitions?$select=DisplayName,LogicalName';
            }

            $.ajax({
                type: 'GET',
                url: url,
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "application/json; charset=utf-8",
                    'OData-MaxVersion': "4.0",
                    "OData-Version": "4.0"
                }
            }).done(entities => {
                kendo.ui.progress($(window.popup.document.body), false);
                if (relationType === OneToN) {
                    if (!entities.value || !Array.isArray(entities.value) || !entities.value.length) {
                        throw "Received data is not valid (Get all related entities)";
                    }

                    let urls = [];
                    let urlsCount = 0;

                    urls[urlsCount] = baseUrl + "/api/data/v8.1/EntityDefinitions" +
                        "?$select=LogicalName,DisplayName" +
                        "&$filter=LogicalName eq '" + entities.value[0].ReferencingEntity + "'";

                    // It's limitation for CRM Web Api
                    const MAX_NUMBER_OF_FILTERS = 25;

                    for (let i = 1, numberOfFilters = 1; i < entities.value.length; i++ , numberOfFilters++) {
                        if (numberOfFilters === MAX_NUMBER_OF_FILTERS) {
                            numberOfFilters = 0;
                            urlsCount++;

                            urls[urlsCount] = baseUrl + "/api/data/v8.1/EntityDefinitions" +
                                "?$select=LogicalName,DisplayName" +
                                "&$filter=LogicalName eq '" + entities.value[i].ReferencingEntity + "'";

                            continue;
                        }

                        urls[urlsCount] += " or LogicalName eq '" + entities.value[i].ReferencingEntity + "'";
                    }

                    let deferreds = [];
                    let allEntities = [];

                    urls.forEach(url => {
                        deferreds.push($.ajax({
                            type: 'GET',
                            url: url,
                            headers: {
                                'Accept': 'application/json',
                                "Content-Type": "application/json; charset=utf-8",
                                'OData-MaxVersion': "4.0",
                                "OData-Version": "4.0"
                            }
                        }).success(entities => {
                            allEntities = allEntities.concat(entities.value);
                            console.log('Popup result: ');
                            console.log(allEntities);
                        }));
                    });

                    $.when.apply($, deferreds).done(function () {

                        allEntities.sort(compare);

                        createTreeView(allEntities, entityLogicalName, displayLogicalName);
                    });
                } else if (relationType === NoRelation) {
                    let allEntities = entities.value.filter(item=>item.DisplayName.LocalizedLabels[0]);
                    allEntities.sort(compare);

                    createTreeView(allEntities, entityLogicalName, displayLogicalName);
                } else if (relationType === NToOne) {
                    let allEntities = entities.value[0].Attributes.filter(item=>item.DisplayName.LocalizedLabels[0]);
                    allEntities.sort(compare);

                    createTreeView(allEntities, entityLogicalName, displayLogicalName);
                }

                function compare(first, second) {
                    let labelFirst = getDisplayName(first);
                    let labelSecond = getDisplayName(second);

                    if (labelFirst < labelSecond)
                        return -1;
                    if (labelFirst > labelSecond)
                        return 1;
                    return 0;
                }
            });
        }


    }

    function getDisplayName(item) {
        let label = item.DisplayName.LocalizedLabels[0].Label;
            

        return label;
    }
}

module.exports = treeViewForPopup;
