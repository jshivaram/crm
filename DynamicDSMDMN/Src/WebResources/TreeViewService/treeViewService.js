let treeViewBuilder = (function () {
    let _id;
    let _converter = require('../Data_Parser/crm-data-converter');
    let searchEvent = require('../Navigation/search');
    let _baseUrl = parent.Xrm.Page.context.getClientUrl();
    let setId = function (id) {
        _id = id;
    };

    let _addDraggableToLiElements = function () {
        $('#treeview li:has(ul)')
            .addClass('lookup-li')
            .off();

        $(_id).find('li').not('.lookup-li').not('[data-role=draggable]').kendoDraggable({
            hint: function (element) {
                return element.clone();
            },
            dragstart: function (e) {
                e.currentTarget.hide();
                $('#grid thead>tr:first>th[data-title="Input"],#grid thead>tr:first>th[data-title="Output"]').addClass('animation');
            },
            dragend: function (e) {
                $('#grid thead>tr:first>th[data-title="Input"],#grid thead>tr:first>th[data-title="Output"]').removeClass('animation');
                e.currentTarget.show();
            }
        });
    };

    let createInitTreeForLogicalName = function (logicalName) {
        (function (name) {
            $.ajax({
                type: 'GET',
                url: _baseUrl + `/api/data/v8.1/EntityDefinitions()?$select=LogicalName,DisplayName&$expand=Attributes&$filter= LogicalName eq '${name}'`,
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "application/json; charset=utf-8",
                    'OData-MaxVersion': "4.0",
                    "OData-Version": "4.0"
                }
            }).done(item => {
                let displayEntityName;

                let _treeView = $(_id).data("kendoTreeView");
                let recievedAttributes = item.value[0].Attributes;
                let logicalName = item.value[0].LogicalName;
                try {
                    displayEntityName = item.value[0].DisplayName.LocalizedLabels[0].Label;
                } catch (e) {
                    displayEntityName = logicalName;
                }

                let convertedArray = _converter.convertCrmDataArray(recievedAttributes, logicalName, true);

                convertedArray.forEach(attribute => {

                    if (attribute.type === 'Lookup') {

                        let html = _treeView.append({
                            text: attribute.title,
                            entityName: attribute.entityName,
                            logicalName: attribute.fieldName,
                            displayName: attribute.title,
                            type: attribute.type,
                            logicalParent: logicalName,
                            displayParent: displayEntityName,
                            target: attribute.targets[0],
                            items: [
                                { text: 'Loading...' }
                            ]
                        });
                        $(html).closest('li').addClass('lookup-li');

                    } else {
                        let html = _treeView.append({
                            text: attribute.title,
                            entityName: attribute.entityName,
                            logicalName: attribute.fieldName,
                            displayName: attribute.title,
                            type: attribute.type,
                            logicalParent: logicalName,
                            displayParent: displayEntityName
                        });

                    }
                });
                
                searchEvent({
                    buttonSelector: '#search-button',
                    textSelector: '#search-text-field',
                    treeViewLiSelector: '#treeview li',
                    spinnerContainer: "#loader",
                    treeViewContainer: "#treeview",
                    draggableRequried:true

                });
                _addDraggableToLiElements(); 
                kendo.ui.progress($('#main_container'), false);
            });
        })(logicalName);
 
    }

    let initTree = function (eventObj) {

        let expandFunction = function (e) {
 
            let node = e.node;
            let length = $(node).find('li').length;
            if (length <= 1) {
                let curentObject = $(_id).data('kendoTreeView').dataItem(node);
                let target = curentObject.target;
                let linkedField = curentObject.logicalName;
                (function (name, linkedField) {
                    $.ajax({
                        type: 'GET',
                        url: _baseUrl + `/api/data/v8.1/EntityDefinitions()?$select=LogicalName,DisplayName&$expand=Attributes&$filter= LogicalName eq '${name}'`,
                        headers: {
                            'Accept': 'application/json',
                            "Content-Type": "application/json; charset=utf-8",
                            'OData-MaxVersion': "4.0",
                            "OData-Version": "4.0"
                        }
                    }).done(item => {
                        let emptyDeleted = false;

                        try {
                            displayEntityName = item.value[0].DisplayName.LocalizedLabels[0].Label;
                        } catch (e) {
                            displayEntityName = logicalName;
                        }

                        let _treeView = $(_id).data("kendoTreeView");
                        let recievedAttributes = item.value[0].Attributes;
                        let logicalName = item.value[0].LogicalName;
                        let convertedArray = _converter.convertCrmDataArray(recievedAttributes, item.value[0].LogicalName);

                        convertedArray.forEach(attribute => {
                            let logicalParent = `${curentObject.logicalParent}.${linkedField}`;
                            let displayParent = `${curentObject.displayParent}.${displayEntityName}`;

                            if (attribute.type === 'Lookup') {
                                let html = _treeView.append({
                                    text: attribute.title,
                                    entityName: attribute.entityName,
                                    logicalName: attribute.fieldName,
                                    displayName: attribute.title,
                                    type: attribute.type,
                                    logicalParent: logicalParent,
                                    displayParent: displayParent,
                                    target: attribute.targets[0],
                                    items: [
                                        { text: 'Loading...' }
                                    ]
                                }, $(node));
                                $(html).closest('li').addClass('lookup-li');

                                if (!emptyDeleted) {
                                    let loadingPlace = _treeView.findByText('Loading...');                                    
                                    _treeView.remove(loadingPlace);
                                    emptyDeleted = true;
                                }
                            } else {
                                let html = _treeView.append({
                                    text: attribute.title,
                                    entityName: attribute.entityName,
                                    logicalName: attribute.fieldName,
                                    displayName: attribute.title,
                                    type: attribute.type,
                                    logicalParent: logicalParent,
                                    displayParent: displayParent,
                                }, $(node));

                                if (!emptyDeleted) {
                                    let loadingPlace = _treeView.findByText('Loading...');
                                    _treeView.remove(loadingPlace);
                                    emptyDeleted = true;
                                }
                            }
                        });
                        _addDraggableToLiElements();
                    });
                })(target, linkedField);
            }
        };

        let settings = {
            animation: {
                expand: {
                    duration: 200
                },
                collapse: {
                    duration: 200
                }
            },
            expand: expandFunction
        };
        let $treeView = $(_id).data('kendoTreeView');

        if (eventObj) {
            Object.keys(eventObj).forEach(key => settings[key] = eventObj[key]);
            if($treeView){
                $treeView.destroy(); 
            }
            $(_id).empty().kendoTreeView(settings);
        } else {
            if($treeView){
                $treeView.destroy();
            }
            $(_id).empty().kendoTreeView(settings);
        }
    };
 
    return {
        setId,
        initTree,
        createInitTreeForLogicalName

    };
})();

module.exports = treeViewBuilder;
