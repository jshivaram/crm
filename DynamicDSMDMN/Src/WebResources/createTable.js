let configForIndexesOfColumns = require('./Grid/configForIndexesOfColumns.js');

let createTable = function (id, entityDropdownData) {
    //ready begin
    kendo.ui.progress($('#main_container'), true);

    let array;
    let dataContainerSelector = '#data_container';

    let dataGetter = require('./Data_Parser/crm-data-getter');
    let dataConverter = require('./Data_Parser/crm-data-converter');
    let converter = require('./DMN_Object_Creator/dmn_convertor');
    let gridBuilder = require('./Grid/grid_builder');
    require('./HitPolicyCreator/hitPolicyBuilder')('#tableKey');
    let treeViewService = require('./TreeViewService/treeViewService');

    function changeColumns(cols) {
        let newCols = $.extend(true, {}, cols);

        if (newCols.columnsArray.length === 4) {
            let rowNumberColumn = {
                title: "&nbsp;",
                template: "<span class='row-number'></span>",
                width: 30
            }

            newCols.columnsArray.unshift(rowNumberColumn);
        }

        newCols.columnsArray.forEach(item => {
            if (item.title && (item.title === 'Input' || item.title === 'Output')) {
                item.columns.forEach(col => {
                    col.width = 200;
                });
            }
            else if (item.field && item.field === 'Annotation') {
                item.width = 200;
            }
        });

        return newCols;
    }
    gridBuilder.resetGrid('#grid');
    dataGetter(id, function (item) {

        console.log(item);
        
        if (id) {
            let entityName = item.LogicalName;

            treeViewService.setId('#treeview');
            treeViewService.initTree();
            treeViewService.createInitTreeForLogicalName(entityName);
        }


        var columns = window.parent.Xrm.Page.getAttribute('ddsm_jsonheaderschema').getValue();
        var datarows = window.parent.Xrm.Page.getAttribute('ddsm_jsontabledata').getValue();
        if (columns) {
            let cols = window.parent.JSON.parse(columns);

            cols = changeColumns(cols);

            let data = window.parent.JSON.parse(datarows);
            gridBuilder.buildGridFromFiles('#grid', cols.columnsArray, data.dataSource, cols.objectState, data.dataContainer);
        } else {
            gridBuilder.createGrid('#grid');
        }
        (require('./Grid/Style Decoration/headerStyle'))('#grid th');
        gridBuilder.setMetadata(array);

        let getConvertedObjectFromGrid = function () {
            let columns = gridBuilder.getColumnArray();
            let input = columns[configForIndexesOfColumns.inputColumn].columns;
            let output = columns[configForIndexesOfColumns.outputColumn].columns;

            let recordArray = $("#grid").data("kendoGrid").dataItems();
            let policyValue = $('#tableKey').data("kendoDropDownList").text();
            let containerArray = gridBuilder.getContainerData();
            let resultArray = converter(input, output, recordArray, containerArray, policyValue);


            let stringified = JSON.stringify(resultArray);
            return stringified;
        };
        window.top.getColumns = function () {

            return gridBuilder.getColumnsState();
        };
        window.top.getDataContainerAndDataSourceJSON = function () {

            let dataSource = $("#grid").data("kendoGrid").dataItems();
            let dataContainer = gridBuilder.getContainerData();

            let objectToJson = {
                dataContainer,
                dataSource
            };
            let json = JSON.stringify(objectToJson);
            return json;
        }

        window.top.gridBuilder = gridBuilder;

        window.top.getCurrentColumns = function () {
            let $grid = $('#grid').data('kendoGrid');
            let currentColumns = {
                inputs: $grid.columns[2].columns,
                outputs: $grid.columns[3].columns
            };

            return currentColumns;
        };


        let parserClickHandler = function () {
            let x2js = new X2JS();
            try {
                let obj = getConvertedObjectFromGrid();
                let xml = x2js.json2xml_str($.parseJSON(obj));
                xml = xml.replace(/&quot;/g,'"')
                    .replace(/&apos;/g, "'");
                parent.Xrm.Page.getAttribute("ddsm_dmnrule").setValue(xml);
            } catch (e) {
                console.error(e);
            }
        };

        window.top.parserClickHandler = parserClickHandler;

        
        //Create splitter ----------
        $('#search-div').css('visibility','visible');
        (require('./Kendo Splitter/Splitter init'))('#splitter');
        //--------------------------

        //Remove upper-row hidding cause of splitter creating 
        $('.upper-row').css('padding-top','10px');
        entityDropdownData.enable(true);
    });
};
module.exports = createTable;
