/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	$(function () {
	    const settings = __webpack_require__(5);

	    let createGrid = __webpack_require__(1);
	    let prevValue = parent.Xrm.Page.getAttribute("ddsm_json_data").getValue();
	    let dataSource = JSON.parse(prevValue);

	    createGrid(settings.gridId, dataSource);
	});

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	let createGrid = function (gridSelector, dataSource) {
	    let createFieldEditor = __webpack_require__(2);
	    let createTypeEditor = __webpack_require__(3);

	    $(gridSelector).kendoGrid({
	        columns: [
	            {
	                field: 'logicalName',
	                title: 'Logical Name',
	                editor: createFieldEditor(gridSelector)
	            },
	            {
	                field: 'displayName',
	                title: 'Display Name',
	                editor: createFieldEditor(gridSelector)
	            },
	            {
	                field: 'type',
	                title: 'Type',
	                editor: createTypeEditor()
	            },
	            { command: "destroy" }
	        ],
	        editable: {
	            createAt: "bottom"
	        },
	        dataSource: dataSource,
	        toolbar: ["create"]
	    });
	};

	module.exports = createGrid;

/***/ },
/* 2 */
/***/ function(module, exports) {

	let editor = function (selector) {

	    return function (container, options) {

	        let typeName = options.model['type'];
	        if (typeName === 'entity') {
	            $('<input required name="' + options.field + '"/>')
	                .appendTo(container)
	                .kendoDropDownList({
	                    autoBind: false,
	                    dataTextField: "DisplayName",
	                    dataValueField: "LogicalName",
	                    dataSource: {
	                        transport: {
	                            read: {
	                                url: parent.Xrm.Page.context.getClientUrl() + "/api/data/v8.1/EntityDefinitions?$select=LogicalName,DisplayName",
	                                dataType: "json",
	                                beforeSend: function (req) {
	                                    req.setRequestHeader('Accept', 'application/json');
	                                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
	                                    req.setRequestHeader('OData-MaxVersion', "4.0");
	                                    req.setRequestHeader("OData-Version", "4.0");
	                                }
	                            }
	                        },
	                        schema: {
	                            data: function (result) {
	                                return result.value
	                                    .filter(item => item.DisplayName.LocalizedLabels.length > 0)
	                                    .map(item => {
	                                        return {
	                                            LogicalName: item.LogicalName,
	                                            DisplayName: item.DisplayName.LocalizedLabels[0].Label
	                                        }
	                                    });
	                            }
	                        }
	                    },
	                    select: function (e) {
	                        let element = this.dataItem(e.item);
	                        options.model['logicalName'] = element.LogicalName;
	                        options.model['displayName'] = element.DisplayName;

	                        $(selector).data('kendoGrid').refresh();
	                    }
	                });
	        } else {
	            $('<input name="' + options.field + '"/>')
	                .appendTo(container)
	                .kendoAutoComplete({
	                    minLength: 0
	                });
	        }

	    }
	};
	module.exports = editor;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	let createTypeDropdownEditor = function(){
	    let typeList = __webpack_require__(4);
	    typeList = Object.keys(typeList);

	    typeList.push('entity');

	    return function(container, options){
	        $('<input required name="' + options.field + '"/>')
	                .appendTo(container)
	                .kendoDropDownList({
	                    autoBind: false,
	                    dataSource: typeList
	                });
	    }
	}   

	module.exports = createTypeDropdownEditor;

/***/ },
/* 4 */
/***/ function(module, exports) {

	let config = {
	    'uniqueidentifier': {
	        equals: {
	            displayName: 'Equal',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'Equal({{arg1}})'
	        },
	        not_equals: {
	            displayName: 'NotEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEqual({{arg1}})'
	        },
	        not_empty: {
	            displayName: 'NotEmpty',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEmpty({{arg1}})'
	        }

	    },
	    //numbers begin
	    'integer': {
	        gt: {
	            displayName: 'GrateThan',
	            argsNumber: 1,
	            type: 'operator',
	            template: '>{{arg1}}'
	        },
	        gte: {
	            displayName: 'GrateThanOrEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: '>={{arg1}}'
	        },
	        lt: {
	            displayName: 'LessThan',
	            argsNumber: 1,
	            type: 'operator',
	            template: '<{{arg1}}'
	        },
	        lte: {
	            displayName: 'LessThanOrEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: '<={{arg1}}'
	        },
	        equals: {
	            displayName: 'Equal',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'Equal({{arg1}})'
	        },
	        not_equals: {
	            displayName: 'NotEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEqual({{arg1}})'
	        },
	        between: {
	            displayName: 'between',
	            argsNumber: 2,
	            type: 'operator',
	            template: '[{{arg1}}..{{arg2}}]'
	        },
	        in: {
	            displayName: 'In',
	            argsNumber: 'N',
	            type: 'operator',
	            template: '[{{args}}]',
	            argsType: 'text'
	        },
	        not_empty: {
	            displayName: 'NotEmpty',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEmpty({{arg1}})'
	        }
	    },
	    'decimal': {
	        gt: {
	            displayName: 'GrateThan',
	            argsNumber: 1,
	            type: 'operator',
	            template: '>{{arg1}}'
	        },
	        gte: {
	            displayName: 'GrateThanOrEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: '>={{arg1}}'
	        },
	        lt: {
	            displayName: 'LessThan',
	            argsNumber: 1,
	            type: 'operator',
	            template: '<{{arg1}}'
	        },
	        lte: {
	            displayName: 'LessThanOrEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: '<={{arg1}}'
	        },
	        equals: {
	            displayName: 'Equal',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'Equal({{arg1}})'
	        },
	        not_equals: {
	            displayName: 'NotEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEqual({{arg1}})'
	        },
	        between: {
	            displayName: 'between',
	            argsNumber: 2,
	            type: 'operator',
	            template: '[{{arg1}}..{{arg2}}]'
	        },
	        in: {
	            displayName: 'In',
	            argsNumber: 'N',
	            type: 'operator',
	            template: '[{{args}}]',
	            argsType: 'text'
	        },
	        not_empty: {
	            displayName: 'NotEmpty',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEmpty({{arg1}})'
	        },
	        empty: {
	            displayName: 'Empty',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'Empty({{arg1}})'
	        }
	    },
	    'double': {
	        gt: {
	            displayName: 'GrateThan',
	            argsNumber: 1,
	            type: 'operator',
	            template: '>{{arg1}}'
	        },
	        gte: {
	            displayName: 'GrateThanOrEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: '>={{arg1}}'
	        },
	        lt: {
	            displayName: 'LessThan',
	            argsNumber: 1,
	            type: 'operator',
	            template: '<{{arg1}}'
	        },
	        lte: {
	            displayName: 'LessThanOrEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: '<={{arg1}}'
	        },
	        equals: {
	            displayName: 'Equal',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'Equal({{arg1}})'
	        },
	        not_equals: {
	            displayName: 'NotEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEqual({{arg1}})'
	        },
	        between: {
	            displayName: 'between',
	            argsNumber: 2,
	            type: 'operator',
	            template: '[{{arg1}}..{{arg2}}]'
	        },
	        in: {
	            displayName: 'In',
	            argsNumber: 'N',
	            type: 'operator',
	            template: '[{{args}}]',
	            argsType: 'text'
	        },
	        not_empty: {
	            displayName: 'NotEmpty',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEmpty({{arg1}})'
	        }
	    },
	    'money': {
	        gt: {
	            displayName: 'GrateThan',
	            argsNumber: 1,
	            type: 'operator',
	            template: '>{{arg1}}'
	        },
	        gte: {
	            displayName: 'GrateThanOrEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: '>={{arg1}}'
	        },
	        lt: {
	            displayName: 'LessThan',
	            argsNumber: 1,
	            type: 'operator',
	            template: '<{{arg1}}'
	        },
	        lte: {
	            displayName: 'LessThanOrEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: '<={{arg1}}'
	        },
	        equals: {
	            displayName: 'Equal',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'Equal({{arg1}})'
	        },
	        not_equals: {
	            displayName: 'NotEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEqual({{arg1}})'
	        },
	        between: {
	            displayName: 'between',
	            argsNumber: 2,
	            type: 'operator',
	            template: '[{{arg1}}..{{arg2}}]'
	        },
	        in: {
	            displayName: 'In',
	            argsNumber: 'N',
	            type: 'operator',
	            template: '[{{args}}]',
	            argsType: 'text'
	        },
	        not_empty: {
	            displayName: 'NotEmpty',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEmpty({{arg1}})'
	        }
	    },
	    //numbers end
	    'string': {
	        equals: {
	            displayName: 'Equal',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'Equal({{arg1}})'
	        },
	        not_equals: {
	            displayName: 'NotEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEqual({{arg1}})'
	        },
	        length: {
	            displayName: 'Length',
	            argsNumber: 1,
	            type: 'function',
	            template: 'length({{arg1}})'
	        },
	        contains: {
	            displayName: 'Contains',
	            argsNumber: 1,
	            type: 'function',
	            template: 'contains({{arg1}})'
	        },
	        not_contains: {
	            displayName: 'Not Contains',
	            argsNumber: 1,
	            type: 'function',
	            template: 'not_contains({{arg1}})'
	        },
	        not_empty: {
	            displayName: 'NotEmpty',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEmpty({{arg1}})'
	        }
	        /*substring: {
	            displayName: 'Substring',
	            argsNumber: 2,
	            argsType: 'number',
	            type: 'function',
	            template: 'substring({{arg1}},{{arg2}})'
	        }*/
	    },
	    'boolean': {
	        true: {
	            displayName: 'true',
	            argsNumber: 0,
	            type: 'bool',
	            template: 'true'
	        },
	        false: {
	            displayName: 'false',
	            argsNumber: 0,
	            type: 'bool',
	            template: 'false'
	        },
	        not_empty: {
	            displayName: 'NotEmpty',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEmpty({{arg1}})'
	        }
	    },
	    'datetime': {
	        'gt': {
	            displayName: 'GrateThan',
	            argsNumber: 1,
	            type: 'operator',
	            argsType: 'date',
	            template: '>{{arg1}}'
	        },
	        'lt': {
	            displayName: 'LessThan',
	            argsNumber: 1,
	            type: 'operator',
	            argsType: 'date',
	            template: '<{{arg1}}'
	        },
	        'equals': {
	            displayName: 'Equal',
	            argsNumber: 1,
	            type: 'operator',
	            argsType: 'date',
	            template: 'Equal({{arg1}})'
	        },
	        'between': {
	            displayName: 'between',
	            type: 'operator',
	            argsType: 'date',
	            argsNumber: 2,
	            template: '[{{arg1}}..{{arg2}}]'
	        },
	        not_empty: {
	            displayName: 'NotEmpty',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEmpty({{arg1}})'
	        }
	    },
	    //numbers end
	    'picklist': {
	        equals: {
	            displayName: 'Equal',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'Equal({{arg1}})'
	        },
	        not_equals: {
	            displayName: 'NotEqual',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEqual({{arg1}})'
	        },
	        length: {
	            displayName: 'Length',
	            argsNumber: 1,
	            type: 'function',
	            template: 'length({{arg1}})'
	        },
	        contains: {
	            displayName: 'Contains',
	            argsNumber: 1,
	            type: 'function',
	            template: 'contains({{arg1}})'
	        },
	        not_contains: {
	            displayName: 'Not Contains',
	            argsNumber: 1,
	            type: 'function',
	            template: 'not_contains({{arg1}})'
	        },
	        not_empty: {
	            displayName: 'NotEmpty',
	            argsNumber: 1,
	            type: 'operator',
	            template: 'NotEmpty({{arg1}})'
	        }
	    },
	}

	module.exports = config;


/***/ },
/* 5 */
/***/ function(module, exports) {

	const settings = {
	    gridId:'#grid'
	}

	module.exports = settings;

/***/ }
/******/ ]);