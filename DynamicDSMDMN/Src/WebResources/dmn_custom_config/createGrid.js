let createGrid = function (gridSelector, dataSource) {
    let createFieldEditor = require('./editor');
    let createTypeEditor = require('./typeDropdownlist');

    $(gridSelector).kendoGrid({
        columns: [
            {
                field: 'type',
                title: 'Type',
                editor: createTypeEditor()
            },
            {
                field: 'logicalName',
                title: 'Logical Name',
                editor: createFieldEditor(gridSelector)
            },
            {
                field: 'displayName',
                title: 'Display Name',
                editor: createFieldEditor(gridSelector)
            },
            { command: "destroy" }
        ],
        editable: {
            createAt: "bottom"
        },
        dataSource: dataSource,
        toolbar: ["create"]
    });
};

module.exports = createGrid;