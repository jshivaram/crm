let editor = function (selector) {

    return function (container, options) {

        let typeName = options.model['type'];
         
        if (typeName === 'entity') {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataTextField: "DisplayName",
                    dataValueField: "LogicalName",
                    dataSource: {
                        transport: {
                            read: {
                                url: parent.Xrm.Page.context.getClientUrl() + "/api/data/v8.1/EntityDefinitions?$select=LogicalName,DisplayName",
                                dataType: "json",
                                beforeSend: function (req) {
                                    req.setRequestHeader('Accept', 'application/json');
                                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                                    req.setRequestHeader('OData-MaxVersion', "4.0");
                                    req.setRequestHeader("OData-Version", "4.0");
                                }
                            }
                        },
                        schema: {
                            data: function (result) {
                                return result.value
                                    .filter(item => item.DisplayName.LocalizedLabels.length > 0)
                                    .map(item => {
                                        return {
                                            LogicalName: item.LogicalName,
                                            DisplayName: item.DisplayName.LocalizedLabels[0].Label
                                        }
                                    });
                            }
                        }
                    },
                    select: function (e) {
                        let element = this.dataItem(e.item);
                        options.model['logicalName'] = element.LogicalName;
                        options.model['displayName'] = element.DisplayName;

                        $(selector).data('kendoGrid').refresh();
                    }
                });
        } else {
            $('<input name="' + options.field + '"/>')
                .appendTo(container)
                .kendoAutoComplete({
                    minLength: 0
                });
        }

    }
};
module.exports = editor;