$(function () {
    const settings = require('./constants');

    let createGrid = require('./createGrid');
    let prevValue = parent.Xrm.Page.getAttribute("ddsm_json_data").getValue();
    let dataSource = JSON.parse(prevValue);

    createGrid(settings.gridId, dataSource);
});