function onSave(){
    let way = window.document.getElementById('WebResource_dmn_customization_config').contentWindow;
    let $grid = way.$('#grid').data('kendoGrid');

    let dataItems = $grid.dataItems();
    let jsonString  = JSON.stringify(dataItems);

    Xrm.Page.getAttribute("ddsm_json_data").setValue(jsonString);
}