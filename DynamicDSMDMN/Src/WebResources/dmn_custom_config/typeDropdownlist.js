let createTypeDropdownEditor = function(){
    let typeList = require('../Row_edditor/inputTypesOperationsConfig');
    typeList = Object.keys(typeList);

    typeList.push('entity');

    return function(container, options){
        $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataSource: typeList
                });
    }
}   

module.exports = createTypeDropdownEditor;