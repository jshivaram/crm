let readyFunc = function () {
    console.log('bundle');
    let createDropdown = require('./Entity Custom Config/Entity Config Type Dropdown');
    var baseUrl = "/api/data/v8.1/";
    var Xrm = window.parent.Xrm;
    var clientUrl = Xrm.Page.context.getClientUrl();
    $tipeDropdown = $('#entity-type-dropdown');


    $('#search-div').css('visibility','hidden');
    window.top.lockdropDown = function (animateHide = false) {
        $("#entities").data("kendoDropDownList").enable(false);
        
        if(!animateHide){
            $tipeDropdown.css('visibility', 'hidden');
        }else{
            $tipeDropdown.fadeOut(500);
        }
    };
    var isFiltered = false;
    createDropdown('#custom-entities','#entities');
};

module.exports = readyFunc;
