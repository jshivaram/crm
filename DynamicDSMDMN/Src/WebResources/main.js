$(document).ready(function () {
  let createTable = require('./createTable');
  let readyFunc = require('./docReady');
  kendo.ui.progress($('#main_container'), true);
  window.top.createTable = createTable;
  readyFunc();
});
