/**
 * Created by Vlad Poliukhovych on 1/11/2016.
 * Dependencies: ags.core.js,  EditableGrid/js/XrmServiceToolkit.min.js
 *
 * Fetch XML operations
 *
 * Version: 0.1.06.08016
 */

function newNamespace() {
    AccentGold = window.AccentGold;
    if (typeof (AccentGold) == "undefined") {
        setTimeout(newNamespace, 100);
    } else {
        AccentGold.namespace("Fetch").extend((function () {

            var queryLimit = 50, //max record count per request

            //TODO:
                validateQuates = function (input) {
                    if (!input) {
                        return '';
                    }
                },

                /**
                 * Return condition string for FetchXML filter
                 * @param cond {Array} ['name':string, 'opr':string,'value':string ]
                 * @returns {string}
                 */
                getFiltCondFXML = function (cond) {
                   if(cond[1]=='null' || cond[1]=='not-null') {
                       return AGS.String.format('<condition attribute="{0}" operator="{1}"/>', cond[0], cond[1]);
                   }
                   if (cond.length < 3) {
                        return "";
                    }

                   if(cond[1]=='in' || cond[1]=='not-in') {
                       let valStr = "";
                        let valList = cond[2].split(',');
                        for (let i = 0; i < valList.length; i++) {
                            valStr += AGS.String.format("<value>{0}</value>",valList[i]);
                        }
                       return AGS.String.format('<condition attribute="{0}" operator="{1}"> {2} </condition>', cond[0], cond[1], valStr);
                   }
                   return AGS.String.format('<condition attribute="{0}" operator="{1}" value="{2}" />', cond[0], cond[1], cond[2]);
                },

                getFilterFXML = function (conditions, filter_type) {
                    if (!Array.isArray(conditions)) {
                        conditions = [conditions];
                    }
                    var result = AGS.String.format('<filter type="{0}" >', (!!filter_type ? filter_type : 'and'));

                    conditions.forEach(function (item) {
                        result += getFiltCondFXML(item);
                    });
                    result += '</filter>';

                    return result;
                },

                /**
                 * Prepare query attributes
                 * @param fields {Array} [string,string,string]
                 * @returns {Array} {name:string, alias:string, aggr_type:string, groupby:bool}
                 */
                getAttrObjs = function (fields) {

                    if (!Array.isArray(fields)) {
                        fields = [fields];
                    }

                    var result = [];

                    fields.forEach(function (item) {
                        var obj = {
                            name: item[0],
                            aggr_type: (!!item[1] ? item[1] : null),
                            alias: (!!item[2] ? item[2] : null),
                            groupby: (!!item[3] ? item[3] : null)
                        };
                        result.push(obj);
                    });
                    return result;
                },

                /**
                 * Get string for 1 attribute
                 * @param item {string} || Object {name:string,alias: string, aggr_type:string, groupby: bool}
                 * @param alias '{string}
                 * @param aggr_type {string}
                 * @param groupby {bool}
                 * @returns {string}
                 */
                getAttributeFXML = function (item, aggr_type, alias, groupby) {

                    var attr_str = '', alias_pref = '_c_', type_str = '', alias_str = '',groupby_str = '';
                    if (item.hasOwnProperty('name')) {
                        type_str = !!item.aggr_type ? AGS.String.format('aggregate="{0}"', item.aggr_type) : '';
                        alias_str = (!!type_str || item.alias) ? AGS.String.format('alias="{0}" ', (!!item.alias ? item.alias : (alias_pref + item.name))) : '';
                        groupby_str = (!!item.groupby) ? AGS.String.format('groupby="{0}" ', Boolean(item.groupby)) : '';
                        attr_str += AGS.String.format('<attribute name="{0}" {1} {2} {3}/>', item.name, alias_str, type_str, groupby_str);
                    } else {
                        type_str = !!aggr_type ? AGS.String.format('aggregate="{0}"', aggr_type) : '';
                        alias_str = (!!type_str || !!alias) ? AGS.String.format('alias="{0}" ', (!!alias ? alias : (alias_pref + item.name))) : '';
                        groupby_str = (!!groupby) ? AGS.String.format('groupby="{0}" ', Boolean(groupby)) : '';
                        attr_str += AGS.String.format('<attribute name="{0}" {1} {2} />', item, alias_str, type_str, groupby_str);
                    }

                    return attr_str;
                },

                /**
                 * Get string of attributes
                 * @param fields {string} Array of Object {'name':string,'alias':string,aggr_type':string}
                 * @returns {string}
                 */
                getAttributesFXML = function (fields) {
                    var attr_str = '';

                    getAttrObjs(fields).forEach(function (item) {
                        attr_str += getAttributeFXML(item);
                    });

                    return attr_str;
                },

                /**
                 * Get string of order attributes
                 * @param items
                 * @returns {string}
                 */
                getOrderFXML = function (items) {
                    var attr_str = '';

                    items.forEach(function (item) {
                        var desc = !!item[1] ? 'descending="true"' : '';
                        attr_str += AGS.String.format('<order attribute="{0}" {1} />', item[0], desc);
                    });

                    return attr_str;
                },

                /**
                 * Execute fetchXML query
                 * @param q String - query string
                 * @returns {null} - Object with fetch result or null if it fails
                 */
                    //TODO: take functionality of XrmServiceToolkit.Soap.Fetch
                executeFXML = function(q){
                    if(!q){ return null; }
                    try{
                        var fetchResult = XrmServiceToolkit.Soap.Fetch(q);
                        return fetchResult;
                    }catch (e){
                        console.error("Fetch XML Failed: ", e.message);
                        return null;
                    }
                },

                /**
                 * Return array of records
                 * @param entity {string}
                 * @param fields String || Array{string} [name,aggr_type,alias]
                 * @param filters Array{string} [name,operator,value]
                 * @param order Array{string,bool} [name,descending]
                 * @param count - limit of records per query
                 * @param page - page of result
                 * @param idName - name of ID field of the entity if it is different to entity name
                 * @returns {Array} businessEntity
                 * not required params: order, count, page, idName
                 */
                selectFXML = function (entity, fields, filters, order, count, page, idName) {

                    //set defaults
                    if (!idName) {
                        idName = entity + 'id';
                    }
                    if (!page) {
                        page = 1;
                    }
                    if (!count) {
                        count = queryLimit;
                    }
                    if (!order) {
                        order = [[idName, true]];
                    }

                    // fetch xml body
                    var fetch = AGS.String.format('<fetch count="{0}" aggregate="false" page="1" >', count);//'<fetch count="1" aggregate="false" page="1" >';


                    fetch += AGS.String.format('<entity name="{0}" >', entity);
                    fetch += getAttributesFXML(fields);

                    if (!!filters) {
                        if (typeof(filters) == 'string') {
                            //prepared filter block
                            fetch += filters;
                        } else {
                            fetch += getFilterFXML(filters[0], filters[1]);
                        }
                    }
                    fetch += getOrderFXML(order);

                    fetch += '</entity>';
                    fetch += '</fetch>';

                    var result = executeFXML(fetch);
                    if (!result) {
                        return [];
                    }

                    return result;
                },

                selectOneFXML = function (entity, id, fields, idName) {

                    if (!idName) {
                        idName = entity + 'id';
                    }
                    var filter = [[[idName, 'eq', id]], 'and'];
                    var data = selectFXML(entity, fields, filter, null, null, null, idName);
                    return !data.length ? null : data[0];
                },

                /**
                 * Return the only record of aggregated data
                 * @param entity
                 * @param fields String || Array{string} [name,aggr_type,alias]
                 * @param filters Array{Array{string,string,string},string} [[name,operator,value],filter_type]
                 * @param recordCount {number} - count of result records
                 * @returns {Object} businessEntity
                 */
                aggregateFXML = function (entity, fields, filters, recordCount) {

                    if(!recordCount){ recordCount= 1;}
                    // fetch xml body
                    var fetch = AGS.String.format('<fetch count="{0}" aggregate="true" page="1" >',recordCount);

                    fetch += AGS.String.format('<entity name="{0}" >', entity);
                    fetch += getAttributesFXML(fields);

                    if (!!filters) {
                        if (typeof(filters) == 'string') {
                            //prepared filter block
                            fetch += filters;
                        } else {
                            fetch += getFilterFXML(filters[0], filters[1]);
                        }
                    }

                    fetch += '</entity>';
                    fetch += '</fetch>';

                    var result = executeFXML(fetch);
                    if (!result || !result.length) {
                        return null;
                    }

                    //return result[0];
                    return returnInitNamesData(result, getAttrObjs(fields));
                },

                /**
                 * Remove auto prefix from data.attributes
                 * @param data {Array[Object]} businessEntity Collection
                 * @param items {Object} {name:string,alias:string,aggr_type:string}
                 * @param pref {string}- field prefix
                 * @returns {Object} businessEntity
                 *
                 */
                returnInitNamesData = function (data, items, pref) {
                    if (!data ||!data[0] || !items || !items.length) {
                        return null;
                    }
                    var data_res = [];
                    if (!pref) {
                        pref = '_c_';
                    }
                    for(var i= 0;i<data.length;i++) {
                        var data_row = data[i];
                        if (!data_row ) { continue; }

                        items.forEach(function (item) {
                            var alias = !!item.alias ? item.alias : (pref + item.name);
                            var clFieldName = alias.slice(0, 2) != pref ? alias : item.name;

                            if (data_row.attributes.hasOwnProperty(alias) && !data_row.attributes.hasOwnProperty(clFieldName)) {
                                data_row.attributes[clFieldName] = data_row.attributes[alias];
                                delete data_row.attributes[alias];
                            }
                        });
                        data_res.push(data_row);
                    }

                    //LEGACY
                    if(data_res.length==1){
                        return data_res[0];
                    }

                    return data_res;
                };

            return {
                executeFXML: executeFXML,
                selectFXML: selectFXML,
                selectOneFXML: selectOneFXML,
                aggregateFXML: aggregateFXML,
                getAttributesFXML: getAttributesFXML,
                getOrderFXML: getOrderFXML,
                getFilterFXML: getFilterFXML
            };

        })());
    }
}

//Initialize creation of a namespace
newNamespace();


///**
// * EXAMPLE OF USAGE
// */

//    //---- Fetch XML ---
//    var fetchData = AGS.Fetch.executeFXML(<query_xml>);

//    //---- Fetch records ---
//
//    var meas_fields = [
//        [ "<fields_name_1>", null , "<alias_1>"],
//        [ "<fields_name_2>"],
//        [ "<fields_name_3>"],
//        [ "<fields_name_4>"],
//    ];
//
//    var conditions = [
//        ['<field_name1>','eq','<value_1>'],
//        ['<field_name2>','ne','<value_2>'],
//        ['<field_name3>','gt','<value_3>'],
//        ['<field_name4>','lt','<value_4>'],
//        ['<field_name5>','like','<value_5>'],
//        ['<field_name6>','not-like','<value_6>'],
//        ['<field_name7>','in','<value_7>'],
//        ['<field_name8>','not-in','<value_8>'],
//        ['<field_name9>','null'], //Just 2 items !!!
//        ['<field_name10>','not-null'], //Just 2 items !!!
//    ];
//
//    var filter  = [conditions,'and']; // second element is not necessary
//    var fetchData =  AGS.Fetch.selectFXML("<entity_name>",meas_fields,filter);
//    //if you have more than 1 filter block, you need to prepare fetch filter string before:
//    var filter =  AGS.Fetch.getFilterFXML([conditions,'and']);
//    filter += AGS.Fetch.getFilterFXML([conditions2,'or']);
//    filter += AGS.Fetch.getFilterFXML([conditions3,'or']);
//    var fetchData =  AGS.Fetch.selectFXML("<entity_name>",meas_fields,filter);
//    //----
//
//    //---- Fetch data for 1 record ---
//    var fetchData =  AGS.Fetch.selectOneFXML("<entity_name>","<record_id>",meas_fields,filter);
//
//
//    //---- Fetch aggregates---
//    var meas_fields = [
//        [ "<fields_name_1>", "sum" , "<alias_1>"],
//        [ "<fields_name_2>", "sum"],
//        [ "<fields_name_3>", "count"],
//        [ "<fields_name_4>", "min"],
//        [ "<groper_fields_name_5>", null, <alias_5>, true],
//        [ "<groper_fields_name_5>", null, null, true],
//    ];
//
//    var fetchData =  AGS.Fetch.aggregateFXML("<entity_name>",[meas_fields],filters); //filter rules the same to the fetch record example above
//    setNumValueFxmlData(fetchData,meas_fields);
////-----

