var ExcelController = function (_interface) {
    var _this = this;
    this._interface = _interface;
    this.EntitiesDataSource = new kendo.data.DataSource({
        schema: KendoHelper.CRM.defEntityMetadataSchema(),
        sort: { field: "Label", dir: "asc" },
        transport: KendoHelper.CRM.transport(clientUrl + baseUrl + 'EntityDefinitions?$select=DisplayName,SchemaName,MetadataId&$filter=IsPrivate eq false and IsBPFEntity eq false'),
    });
};
ExcelController.prototype.get = function (name){
    if(!name) {
        return this._interface;
    }
    if(!!this[name]) {
        return this[name];
    } else {
        return [];
    }
};
ExcelController.prototype.init = function (){
    var _econtroller = this;
    this.EntityMetadata = {};
    this.ignoreFieldDisplayNamePart = ["(Base)", "Z_"];
    this.ignoreFieldTypeArray = ["Virtual", "Uniqueidentifier", "EntityName", "PartyList"];
    this.ignoreFieldNameArray = ["ddsm_applicationfieldsmappingconfigid",
        "createdby",
        "createdonbehalfby",
        "createdon",
        "transactioncurrencyid",
        "exchangerate",
        "importsequencenumber",
        "modifiedby",
        "modifiedonbehalfby",
        "modifiedon",
        "overriddencreatedon",
        "ddsm_sourceentity",
        "ddsm_status",
        "statecode",
        "statuscode",
        "timezoneruleversionnumber",
        "traversedpath",
        "utcconversiontimezonecode",
        "owningteam",
        "owninguser",
        "owningbusinessunit",
        "ownerid"
    ];
    this.fieldFilter = function () {
        var filter = {
            logic: "and",
            filters: []
        };

        filter.filters.push({
            field: "AttributeOf",
            operator: "isnull",
            value: null
        });

        _econtroller.get("ignoreFieldNameArray").forEach(function(entry) {
            filter.filters.push({
                field: "AttrLogicalName",
                operator: "neq",
                value: entry
            });
        }, null);


        _econtroller.get("ignoreFieldTypeArray").forEach(function(entry2) {
            filter.filters.push({
                field: "AttrType",
                operator: "neq",
                value: entry2
            });
        }, null);


        _econtroller.get("ignoreFieldDisplayNamePart").forEach(function(entry3) {
            filter.filters.push({
                field: "AttrDisplayName",
                operator: "neq",
                value: entry3
            });
        }, null);

        return filter;
    };
};

ExcelController.prototype.getSourceEntityMetadata = function (entityname, callback) {
    var _econtroller = this;

    if(!DataUploader.Controller.EntityMetadata[entityname]) {
        var fields = KendoHelper.CRM.getEntityMetadata(entityname, true, "", "Type", _econtroller.fieldFilter());

        fields.fetch(function() {
            fields.filter(_econtroller.fieldFilter());

            if (!_econtroller.EntityMetadata) {
                _econtroller.EntityMetadata = {};
            }
            //fields.view();
            _econtroller.EntityMetadata[entityname] = fields.data();
            if (callback) {
                callback(entityname);
            }
        });
    } else {
        if (callback) {
            callback(entityname);
        }
    }

};


ExcelController.prototype.getGridColumns = function () {
    var _econtroller = this;
    var columns = [{
        field: "ExcelColumn",
        title: "Column",
        width: "50px"
    }, {
        field: "ColumnName",
        title: "Excel Field Name",
        width: "30%"
    }, {
        field: "AttrDisplayName",
        title: "CRM Field Name",
        editor: this.categoryDropDownEditor,
        //width: "300px"
    }, {
        field: "AttrLogicalName",
        title: "CRM Field Logical Name",
        hidden: true
    }, {
        field: "AttrTargetEntity",
        title: "CRM Lookup Target Logical Name",
        hidden: true
    }, {
        field: "AttrType",
        title: "CRM Field Type",
        width: "100px",
        //hidden: true
    }, {
        field: "TypeField",
        title: "Type",
        width: "100px",
        //hidden: true
    }, {
        field: "IName",
        title: "Indicator Name",
        width: "300px",
        hidden: true
    }, {
        field: "IFieldLogicalName",
        title: "IFieldLogicalName",
        width: "200px",
        hidden: true
    }, {
        field: "IEntity",
        title: "Indicator Entity Value",
        width: "300px",
        hidden: true
    }, {
        field: "IRelationName",
        title: "IRelationName",
        width: "300px",
        hidden: true
    }, {
        field: "IFieldTargetEntity",
        title: "IFieldTargetEntity",
        width: "100px",
        hidden: true
    }, {
        field: "IndicatorId",
        title: "IndicatorId",
        width: "100px",
        hidden: true
    }, {
        field: "IAdditionalAttr",
        title: "IAdditionalAttr",
        width: "100px",
        hidden: true
    }
    ];
    return columns;
};

ExcelController.prototype.categoryDropDownEditor = function (container, options) {

    var existedValue = options.model.AttrLogicalName;
    var existedText = options.model.AttrDisplayName;
    var selectedTabLogicalName = DataUploader.Internal("SheetsName").Value()[DataUploader.Internal("SelectedTab").Value()].Entity.LogicalName;

    $('<input />')
        .appendTo(container)
        .kendoDropDownList({
            value: existedValue != "" ? existedValue : null,
            filter: "startswith",
            optionLabel: "--",
            autoBind: true,
            //suggest: true,
            //valuePrimitive: false,
            dataTextField: "AttrDisplayName",
            dataValueField: "AttrLogicalName",
            //template: '#: data.AttrDisplayName # #: data.IAdditionalAttr != "" ? data.IAdditionalAttr : "" #',

            dataSource: new kendo.data.DataSource({
                sort: {
                    field: "AttrDisplayName",
                    dir: "asc"
                },
                data: DataUploader.Controller.EntityMetadata[selectedTabLogicalName]
                , filter: DataUploader.Controller.fieldFilter()
                , group: { field: "TypeField" }
            }),
            //placeholder: "Select field...",//
            dataBound: function (e) {
                //e.sender.value(options.model.AttrDisplayName);

                //debugger;
                var dropDown = this;
                if (!!existedValue && existedText != null && existedText != "") {
                    var data = this.dataSource.data();

                    var counter = 0;
                    for (var currentValue of data) {
                        counter++;
                        if (currentValue.AttrDisplayName && currentValue.AttrDisplayName.trim() == existedText.trim()) {
                            dropDown.select(counter);
                            break;
                        }
                    }
                } else {
                    dropDown.text("")
                }

            },
            change: function (e) {
                var data = e.sender.dataItem();
                if (!!data && data.AttrDisplayName && data.AttrDisplayName != "--") {
                    options.model.AttrDisplayName = data.AttrDisplayName;
                    options.model.AttrLogicalName = data.AttrLogicalName;
                    options.model.AttrType = data.AttrType;
                    options.model.TypeField = data.TypeField;
                    options.model.AttrTargetEntity = data.AttrTargetEntity;

                    options.model.IndicatorId = data.IndicatorId;
                    options.model.IEntity = data.IEntity;
                    options.model.IRelationName = data.IRelationName;
                    options.model.IName = data.IName;
                    options.model.IFieldTargetEntity = data.IFieldTargetEntity;
                    options.model.IFieldLogicalName = data.IFieldLogicalName;
                    options.model.IAdditionalAttr = data.IAdditionalAttr;
                } else {
                    options.model.AttrDisplayName = null;
                    options.model.AttrLogicalName = null;
                    options.model.AttrType = null;
                    options.model.TypeField = null;
                    options.model.AttrTargetEntity = null;

                    options.model.IndicatorId = null;
                    options.model.IEntity = null;
                    options.model.IRelationName = null;
                    options.model.IName = null;
                    options.model.IFieldTargetEntity = null;
                    options.model.IFieldLogicalName = null;
                    options.model.IAdditionalAttr = null;
                }

                var grid = $("#grid").data("kendoGrid");
                if (!DataUploader.Internal("GridDataSource").Value()) {
                    DataUploader.Internal("GridDataSource").Value({});
                }
                if (selectedTabLogicalName != null) {
                    DataUploader.Internal().GridDataSource[DataUploader.Internal("SelectedTab").Value()].LogicalName = selectedTabLogicalName;
                    DataUploader.Internal().GridDataSource[DataUploader.Internal("SelectedTab").Value()].Attributes = grid.dataItems();
                } else {
                    DataUploader.Internal().GridDataSource[DataUploader.Internal("SelectedTab").Value()].LogicalName = "";
                    DataUploader.Internal().GridDataSource[DataUploader.Internal("SelectedTab").Value()].Attributes = [];
                }
                grid.refresh();
                DataUploader.UpdateFormData();
            }
        });

};

ExcelController.prototype.isEditable = function (fieldName, model) {
    return fieldName === "AttrDisplayName";
};

ExcelController.prototype.actualizeTargetEntityWrapper = function(targetControl) {
    if(!targetControl) {
        targetControl = "targetEntityList";
    }
    var _econtroller = this;
    $('#' + targetControl).empty().kendoDropDownList({
        filter: "contains",
        ignoreCase: true,
        optionLabel: "Select source entity...",
        filtering: function () {
            isFiltered = true;
        },
        change: function (e) {
            if (isFiltered) {
                e.preventDefault();
                return;
            }
        },
        dataSource: _econtroller.get("EntitiesDataSource"),
        dataTextField: "Label",
        dataValueField: "LogicalName",
        template: ' #: Label # (#:LogicalName #)',
        select: function (e) {
            var dataItem = getDataItem(this, e);
            var _entity =  {
                Label: (!!dataItem.Label)?dataItem.Label:"",
                LogicalName: (!!dataItem.LogicalName)?dataItem.LogicalName:"",
                Id:(!!dataItem.Id)?dataItem.Id:""
            };
            if(!!_entity.LogicalName){
                _entity.Label = "";
            }

            if(!!_econtroller.get().Internal("SelectedTab").Value() && !!_econtroller.get().Internal("SheetsName").Value()[_econtroller.get().Internal("SelectedTab").Value()]){

                _econtroller.get().Internal().SheetsName[_econtroller.get().Internal("SelectedTab").Value()].Entity = _entity;

                _econtroller.get().Internal().GridDataSource[_econtroller.get().Internal("SelectedTab").Value()].LogicalName = _entity.LogicalName;
                _econtroller.get().Internal().GridDataSource[_econtroller.get().Internal("SelectedTab").Value()].Attributes = [];
                if(!!_entity.LogicalName){
                    kendo.ui.progress($("#grid"), true);
                    _econtroller.get().Controller.getSourceEntityMetadata(dataItem.LogicalName, function(entityname) {
                        _econtroller.get().Controller.actualizeGridWrapper("grid");
                    });

                } else {
                    _econtroller.get().Controller.actualizeGridWrapper("grid");
                }
            }

            function getDataItem(s, d) {
                var dataItem = s.dataItem(d.item);
                return dataItem;
            }
        },
        dataBound: function (e) {
            if (isFiltered) {
                return;
            }
        }
    });
};

ExcelController.prototype.actualizeGrid = function (selectedLogicalName, selectedTab) {
    var _econtroller = this;
    if (!selectedLogicalName && !selectedTab && !_econtroller.get().Internal("SelectedTab").Value()) {
        Alert.show("Error","Current mapping record is broken. Remove it and create again." ,null,"ERROR");
        return;
    } else if(!!_econtroller.get().Internal("SelectedTab").Value()) {
        selectedTab = _econtroller.get().Internal("SelectedTab").Value();
    }

    var selectedLogicalName  = selectedLogicalName;
    var selectedTab  = selectedTab;

    if(!!selectedLogicalName && !selectedTab) {
        for(var key in _econtroller.get().Internal("SheetsName").Value()) {
            if(_econtroller.get().Internal("SheetsName").Value()[key].Entity.LogicalName == selectedLogicalName) {
                selectedTab = key;
                break;
            }
        }
    }

    if(!selectedLogicalName && !!selectedTab) {
        selectedLogicalName = _econtroller.get().Internal("SheetsName").Value()[selectedTab].Entity.LogicalName;
    }

    console.log("selectedLogicalName = " + selectedLogicalName);
    console.log("selectedTab = " + selectedTab);
    var ds = {};
    //if (selectedLogicalName) {
    if (_econtroller.get().Internal("GridDataSource").Value()[selectedTab].Attributes.length == 0) {
        ds = new kendo.data.DataSource({
            sort: {
                field: "ExcelColumn",
                dir: "asc"
            },
            schema: {
                data: function (response) {

                    var value = _econtroller.get().Internal("SheetsName").Value()[selectedTab].Data;
                    var model = value.map(function (el) {
                        var targetField = [];

                        if (!!selectedLogicalName) {
                            targetField = _econtroller.EntityMetadata[selectedLogicalName].filter(function (el1) {
                                return el1.AttrDisplayName.toLowerCase() == el.ColumnName.toLowerCase();
                                //return ((el1.AttrDisplayName.toLowerCase().indexOf(el.ColumnName.toLowerCase()) != -1));
                            });
                        }

                        return {
                            ColumnName: el.ColumnName,
                            ExcelColumn: el.ExcelColumn,

                            AttrDisplayName: targetField.length > 0 ? targetField[0].AttrDisplayName : "",
                            AttrLogicalName: targetField.length > 0 ? targetField[0].AttrLogicalName : "",
                            AttrType: targetField.length > 0 ? targetField[0].AttrType : "",
                            AttrTargetEntity: targetField.length > 0 ? targetField[0].AttrTargetEntity : "",
                            TypeField: targetField.length > 0 ? targetField[0].TypeField : "",

                            IndicatorId: targetField.length > 0 ? targetField[0].IndicatorId : "",
                            IEntity: targetField.length > 0 ? targetField[0].IEntity : "",
                            IRelationName: targetField.length > 0 ? targetField[0].IRelationName : "",
                            IName: targetField.length > 0 ? targetField[0].IName : "",
                            IFieldTargetEntity: targetField.length > 0 ? targetField[0].IFieldTargetEntity : "",
                            IFieldLogicalName: targetField.length > 0 ? targetField[0].IFieldLogicalName : "",
                            IAdditionalAttr: targetField.length > 0 ? targetField[0].IAdditionalAttr : ""

                        };
                    });
                    return model;
                }, filter: _econtroller.fieldFilter()
            }
        });
    } else {
        ds = new kendo.data.DataSource({
            data: _econtroller.get().Internal("GridDataSource").Value()[selectedTab].Attributes, filter: _econtroller.fieldFilter()
        });
    }

    ds.filter(_econtroller.fieldFilter());

    ds.view();

    _econtroller.get().Internal().GridDataSource[selectedTab].LogicalName = selectedLogicalName;
    _econtroller.get().Internal().GridDataSource[selectedTab].Attributes = ds.data();
    /*
     } else {
     ds = new kendo.data.DataSource();
     }
     */
    /******* END Clear Virtual and Uniqueidentifier fileds ***********/
    kendo.ui.progress($("#grid"), false);
    return ds;
}
;
ExcelController.prototype.actualizeGridWrapper = function(targetControl) {
    if(!targetControl) {
        targetControl = "grid";
    }
    var _econtroller = this;
    $("#" + targetControl).empty().kendoGrid({
        pageable: true,
        dataSource: _econtroller.get().Controller.actualizeGrid(),
        //rowTemplate: '<tr class="#:SourceField  !==""? \"green\" : \"customClass2\"#" data-uid="#= uid #"><td>#: SourceField  #</td><td>#:SourceFieldType  #</td><td>#:TargetField#</td></tr>',
        height: window.parent.$("IFRAME#WebResource_config").context.body.offsetHeight - 315 + 'px',
        filterable: {
            extra: true,
            operators: {
                string: {
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    isnotempty: "Is not empty",
                    isempty: "Is empty",
                    contains: "Contains"
                }
            }
        },
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        columns: _econtroller.get().Controller.getGridColumns(),
        width: "200px",
        title: "Clear mapping",
        editable: true,
        edit: function (e) {
            if(!!(((e.model.get("ColumnName")).toLowerCase()).indexOf((_econtroller.get().Internal("SelectedTab").Value()).toLowerCase() + " guid")+1)){
                e.preventDefault();
                this.closeCell();
            }
            var columnIndex = this.cellIndex(e.container);
            var fieldName = this.thead.find("th").eq(columnIndex).data("field");
            if (!_econtroller.get().Controller.isEditable(fieldName, e.model)) {
                e.preventDefault();
                this.closeCell();
            }
        },
        dataBound: function(e) {
            var dataItems = e.sender.dataSource.view();
            for (var j = 0; j < dataItems.length; j++) {
                var _attrDisplayName = dataItems[j].get("AttrDisplayName");
                var _columnName = (dataItems[j].get("ColumnName")).toLowerCase();

                var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
                if(!_attrDisplayName && !(_columnName.indexOf((_econtroller.get().Internal("SelectedTab").Value()).toLowerCase() + " guid")+1)){
                    row.addClass("unmapped");
                }
                if(!!(_columnName.indexOf((_econtroller.get().Internal("SelectedTab").Value()).toLowerCase() + " guid")+1)){
                    row.addClass("unique");
                }
            }
            _econtroller.get().UpdateFormData()
        }
    });

};