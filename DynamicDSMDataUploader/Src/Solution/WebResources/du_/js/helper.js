var KendoHelper = KendoHelper || {};

KendoHelper.CRM = {
    transport: function (url) {
        var result = {};
        result.read = {
            url: encodeURI(url),
            dataType: "json",
            beforeSend: function (req) {
                req.setRequestHeader('Accept', 'application/json');
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader('OData-MaxVersion', "4.0");
                req.setRequestHeader("OData-Version", "4.0");
            }
        }
        return result;
    },
    defEntityMetadataSchema: function () {
        var result = {};
        result.data = function (response) {
            var value = response.value;
            var model = {};
            if (value) {
                model = value.map(function (el) {
                    var displayName = "";
                    var id = el.MetadataId;
                    var logicalName = el.SchemaName.toLowerCase();
                    if (el.DisplayName.LocalizedLabels[0] && el.DisplayName.LocalizedLabels[0].Label) {
                        displayName = el.DisplayName.LocalizedLabels[0].Label;
                    } else if (el.DisplayName.UserLocalizedLabel && el.DisplayName.UserLocalizedLabel.Label) {
                        displayName = el.DisplayName.UserLocalizedLabel.Label;
                    } else {
                        displayName = logicalName;
                    }
                    return {
                        Label: displayName,
                        LogicalName: logicalName,
                        Id: id
                    };
                });
            }
            else {
                console.log("---defEntityMetadataSchem->Value ==null  ");
            }

            return model;
        }
        return result;
    },
    getEntityMetadata: function (entName, withIndicators, justFieldType, groupBy, filters) {
        var url = clientUrl + baseUrl;
        var query = "ddsm_DDSMGetMetadataForExcel";
        return new kendo.data.DataSource({
            schema: {
                data: function (response) {
                    //console.dir(JSON.parse(response.Result));
                    return JSON.parse(response.Result);
                },
                filter: filters
            },
            group: {field: groupBy},
            transport: {
                read: {
                    type: "POST",
                    url: url + query,
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json'
                },
                parameterMap: function (options, operation) {
                    var postData = {
                        EntityName: entName, WithIndicators: withIndicators,
                        JustFieldType: justFieldType
                    };
                    // note that you may need to merge that postData with the options send from the DataSource
                    return JSON.stringify(postData);
                }
            },
        });
    }
};