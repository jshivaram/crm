if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
    };
}

var DataUploader = window.DataUploader || { __namespace: true };

(function(){
    var _this = this;
    this.crmForm = window.parent;
    this.Xrm = this.crmForm.Xrm;
    this.Page = this.Xrm.Page;
    this.getControl = this.Page.getControl;
    this.getAttribute = this.Page.getAttribute;

    this.formType = this.Page.ui.getFormType();
    this.clientUrl = this.Page.context.getClientUrl();
    this.baseUrl = "/api/data/v8.2/";
    this.Controller = {};
    this.ColorUI = [
        "#f44336","#E91E63","#9C27B0","#673AB7","#3F51B5","#2196F3","#00BCD4","#009688","#4CAF50","#8BC34A","#CDDC39","#FFEB3B","#FFC107","#FF9800","#FF5722","#795548","#9E9E9E","#607D8B"
        ,"#d32f2f","#C2185B","#7B1FA2","#512DA8","#303F9F","#1976D2","#0097A7","#00796B","#388E3C","#689F38","#AFB42B","#FBC02D","#FFA000","#F57C00","#E64A19","#5D4037","#616161","#455A64"
        ,"#b71c1c","#880E4F","#4A148C","#311B92","#1A237E","#0D47A1","#006064","#004D40","#1B5E20","#33691E","#827717","#F57F17","#FF6F00","#E65100","#BF360C","#3E2723","#212121","#263238"

    ]
    var __private = {};
    __private.items = {};

    this.showSpinner = function (show, msg) {

        if (show) {

            if (typeof parent.CreaLab != "undefined" && typeof parent.CreaLab.Spinner != "undefined" && typeof parent.CreaLab.Spinner.spin == "function")
                window.spinnerUploader = parent.CreaLab.Spinner.spin(parent.GlobalJs.crmContentPanel._document.getElementById("formBodyContainer"), msg || "Loading data ...");
        }
        else {
            if (!!window.spinnerUploader)
                window.spinnerUploader.stop();
        }
    },
    this.Internal = function(parameterName){
        var __local = __private;
        var parameterName = parameterName;
        var Value = function(val) {
            if(typeof parameterName !== "undefined" && !!parameterName) {
                if(typeof val !== "undefined") {
                    __local.items[parameterName] = val;
                } else {
                    return __local.items[parameterName];
                }
            } else {
                return __local.items;
            }
        };

        if(!(typeof parameterName !== "undefined" && !!parameterName)){
            return __local.items;
        } else {
            return {
                Value: Value
            };
        }
    },

        this.Init = function() {
            _hidePageControls();
            //Set base values
            _setParameter({
                isConfigForm: false,
                isUploadForm: false,
                baseDUConfig: JSON.parse('{"DUConfig":{"DU_ExcelIsHeader":true,"DU_RequestsCount":100,"DU_PageRecordsCount":1000,"DU_CallExecuteMultiple":true,"DU_DuplicateDetection":true,"DU_InitiateUploadData":true,"DU_AllowSimultaneous":962080001,"DU_DeduplicationRules":962080000,"DU_RemoteCalculation":false,"DU_ParserRemoteCalculationApiUrl":"","DU_CreatorRemoteCalculationApiUrl":"","DU_OrderEntitiesList":["ddsm_programoffering","contact","account","ddsm_site","lead","ddsm_projectgroup","ddsm_project","ddsm_measure","activitypointer","annotation"],"DU_MappingImplementation":[]}}')["DUConfig"],
                DUConfig : {},
                newAnnotation: {},
                intervalWorker1: null,
                intervalWorker2: null,
                xlsxParsing: false,
                X: {},
                fileName: "",
                xlsxObj: {},
                SheetsName: {},
                GridDataSource:{},
                TabsName: [],
                SelectedTab: "",
                HeaderRow: 0,
                EntitiesList: [],
                objEntities: {},
                UserSettings: {
                    UserId: _this.Page.context.getUserId().replace(/\{|\}/g, ''),
                    TransactionCurrencyId: "",
                    TimezoneCode: -2147483648
                }
            });

            if(typeof configJSON !== "undefined" && typeof configJSON === "string") {
                var _configJSON = {}, _DUConfig = {};
                try {
                    _configJSON = JSON.parse(configJSON);
                    if(typeof _configJSON === "object" && typeof _configJSON["DUConfig"] === "object") {
                        for(var key in _configJSON["DUConfig"]) {
                            if(key == "DU_OrderEntitiesList") {
                                var oEntityList = [];
                                for(var i = 0; i < _configJSON["DUConfig"][key].length; i++) {
                                    oEntityList.push(_configJSON["DUConfig"][key][i].LogicalName);
                                }
                                _DUConfig[key] = oEntityList;
                            } else if(key == "DU_MappingImplementation"){
                                var oEntityList = [];
                                for(var i = 0; i < _configJSON["DUConfig"][key].length; i++) {
                                    var objKey = _configJSON["DUConfig"][key][i].EntityLogicalName.LogicalName;
                                    var objVal = _configJSON["DUConfig"][key][i].MappingImplementation.Id;
                                    oEntityList.push(objKey+"|"+objVal);

                                }
                                _DUConfig[key] = oEntityList;
                            } else {
                                _DUConfig[key] = _configJSON["DUConfig"][key];
                            }
                        }
                    } else {
                        _DUConfig = _this.Internal("baseDUConfig").Value();
                    }
                }
                catch(e) {
                    _configJSON = {};
                    _DUConfig = _this.Internal("baseDUConfig").Value();
                }
                _setParameter({DUConfig:_DUConfig});
            } else {
                _setParameter({DUConfig:_this.Internal("baseDUConfig").Value()});
            }
            _getUserSettings();

        },
        this.LoadFile = function (files) {
            console.log(">>> LoadFile");

            $("#filePanel").hide();
            _this.showSpinner(true, "Parsing Excel File");

            var _f = files[0];
            var f = null;
            if(typeof _f.rawFile !== "undefined") {
                f = _f.rawFile;
            } else {
                f = _f;
            }

            if (f.type === "application/vnd.ms-excel" || f.type === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || (f.type === "" && (f.name.endsWith("xls") || f.name.endsWith("xlsx")))) {
            } else {
                _this.showSpinner(false);
                $("#filePanel").show();
                Alert.show(null, "The format of the file you have selected is not supported. Please select a valid Excel file ('.xls, *.xlsx').", [{
                    label: "Ok",
                    callback: function () {
                        try {
                            var uploadControl = $("#excelFile").data("kendoUpload");
                            uploadControl.clearAllFiles();
                        } catch(e){}
                        return;
                    }
                }], "ERROR", 500, 200);
                return;
            }

            if(_this.formType == 1) {
                _this.Page.ui.tabs.getByName("General").sections.getByName("General_Parameter").setVisible(false);
            }

            _this.Internal("fileName").Value(f.name);

            //Gen Note record
            var readerBase64 = new FileReader();
            readerBase64.onload = function (e) {
                var u = e.target.result, s = u.indexOf("base64,") + 7;

                _this.Internal("newAnnotation").Value({
                    isdocument:!0
                    ,filename: _this.Internal("fileName").Value()
                    ,documentbody: u.toString().substring(s)
                    ,subject: _this.Internal("fileName").Value()
                });
                _this.Internal("xlsxParsing").Value(true);
            };
            readerBase64.onerror = function (e) {
                console.dir(e);
            };

            //Parse Excel File
            var reader = new FileReader();

            _this.getControl("ddsm_name").setDisabled(false);
            //Configuration form
            if(_this.Internal("isConfigForm").Value()){
                _this.getAttribute("ddsm_name").setValue(_this.getAttribute("ddsm_shortname").getText());
            }
            //Uploading form
            if(_this.Internal("isUploadForm").Value()){
                _this.getAttribute("ddsm_name").setValue(_this.Internal("fileName").Value());
            }
            _this.getControl("ddsm_name").setDisabled(true);

            reader.onloadstart = function(){};
            reader.onloadend = function(){
                readerBase64.readAsDataURL(f);
            };

            reader.onload = function(e) {
                //Configuration form

                var data = e.target.result;
                var workbook;

                var arr = _fixData(data);

                workbook = _this.Internal().X.read(btoa(arr), {type: 'base64'});
                var result = {};
                workbook.SheetNames.forEach(function(sheetName) {
                    //var formulae = this.Internal().X.utils.get_formulae(workbook.Sheets[sheetName]);
                    var formulae = _this.Internal().X.utils.sheet_to_custom(workbook.Sheets[sheetName]);
                    if(formulae.length > 0){
                        if(typeof formulae[1] !== 'undefined') result[sheetName] = formulae[_this.Internal("HeaderRow").Value()];
                    }
                });

                _this.Internal("xlsxObj").Value(result);

            };

            reader.readAsArrayBuffer(f);
            _this.Internal("intervalWorker1").Value(setInterval(_verifyParsing, 1000));
        },
        this.UpdateFormData = function () {
            console.log(">>> UpdateFormData");

            _this.getControl("ddsm_jsonexcel").setDisabled(false);
            _this.getControl("ddsm_jsondata").setDisabled(false);
            _this.getAttribute("ddsm_jsonexcel").setValue(JSON.stringify(_this.Internal("SheetsName").Value()));
            _this.getAttribute("ddsm_jsondata").setValue(JSON.stringify(_this.Internal("GridDataSource").Value()));
            _this.getControl("ddsm_jsonexcel").setDisabled(true);
            _this.getControl("ddsm_jsondata").setDisabled(true);

            if(_this.formType == 1) {

                setTimeout(function() {

                    _this.Page.data.save().then(function(){
                        setTimeout(function(){
                            var _confId = "";
                            try {
                                _confId = _this.Page.data.entity.getId();
                            }
                            catch(e){
                                _confId = ""
                            }
                            if(!!_confId) {
                                _createAnnotation(
                                    _this.Internal().newAnnotation,
                                    _attachAnnotation,
                                    function(e){console.dir(e);}
                                );
                            }
                        }, 100);
                    })
                }, 100);

            } else {
                setTimeout(function(){
                    _this.Xrm.Page.data.save();
                }, 100);
            }
        },
        this.LoadData = function() {
            console.log(">>> LoadData");

            //Configuration form
            if(_this.Internal("isConfigForm").Value()){
                _this.showSpinner(true, "Creating the <br>Configuration Object Model");
                _createEntitiesList(_getFormData);
            } else
            //Uploading form
            if(_this.Internal("isUploadForm").Value()){
                if(!!_this.getAttribute("ddsm_jsondata") && !!_this.getAttribute("ddsm_jsondata").getValue()) {
                    _this.Internal("GridDataSource").Value(JSON.parse(_this.getAttribute("ddsm_jsondata").getValue()));
                }
                if(!!_this.getAttribute("ddsm_settings") && !!_this.getAttribute("ddsm_settings").getValue()) {
                    _this.Internal("DUConfig").Value(JSON.parse(_this.getAttribute("ddsm_settings").getValue()));
                }
                if(!!_this.getAttribute("ddsm_usersettings") && !!_this.getAttribute("ddsm_usersettings").getValue()) {
                    _this.Internal("UserSettings").Value(JSON.parse(_this.getAttribute("ddsm_usersettings").getValue()));
                }
                var _gridDataSource = _this.Internal("GridDataSource").Value();
                for(var key in _gridDataSource){
                    _this.Internal().objEntities[key] = {
                        NotProcessed: -1,
                        Processed: 0
                    };
                }
                _createChart();
            } else {}

            //Create link to download file
            _getFileId(function (id) {
                _createLinkToFile(id);
            });
        },
        this.InitDragandDrop = function(){
            if(window.FileReader && Modernizr.draganddrop) {
                var _dropPanel = window.parent.document.getElementsByTagName("body")[0];
                _dropPanel.addEventListener("drop", _handleDrop, !1);
                _dropPanel.addEventListener("dragover",_handleDragOver, !1)
                var _dropPanel2 = document.getElementsByTagName("body")[0];
                _dropPanel2.addEventListener("drop", _handleDrop, !1);
                _dropPanel2.addEventListener("dragover",_handleDragOver, !1)
            } else console.log("Drag and drop api not supported");

        },
        this.getAdditionalConfigParameter = function(id, callback) {
            console.log(">>> getAdditionalConfigParameter");

            var urlRecord = _this.clientUrl + _this.baseUrl + "ddsm_datauploaderconfigurations(" + ((id).replace(/\{|\}/g, '')).toUpperCase() + ")?$select=ddsm_shortname,ddsm_recalculationtype,ddsm_jsondata";

            var req = new XMLHttpRequest();
            req.open("GET", encodeURI(urlRecord), false);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.onreadystatechange = function () {
                if (this.readyState == 4) {
                    req.onreadystatechange = null;
                    var respData = {};
                    respData["ddsm_jsondata"] = null;
                    respData["ddsm_shortname"] = null;
                    respData["ddsm_recalculationtype"] = null;
                    if (this.status == 200) {
                        var respObj = JSON.parse(req.responseText);
                        respData["ddsm_jsondata"] = (typeof respObj["ddsm_jsondata"])?respObj["ddsm_jsondata"]:null;
                        respData["ddsm_shortname"] = (typeof respObj["ddsm_shortname"])?respObj["ddsm_shortname"]:null;
                        respData["ddsm_recalculationtype"] = (typeof respObj["ddsm_recalculationtype"])?respObj["ddsm_recalculationtype"]:null;
                        if(!!callback) {
                            callback(respData);
                        }
                    }
                    else {
                        console.log(this.response);
                        if(!!callback) {
                            callback(respData);
                        }
                    }
                }
            };
            req.send(null);

        },
        this.transformGridToJson = function(gridData){
            console.log(">>> transformGridToJson");

            var _gridData = JSON.parse(gridData);
            var _jsonData = {};
            for(var key in _gridData){
                _jsonData[key] = {};
                _jsonData[key].LogicalName = _gridData[key].LogicalName;
                _jsonData[key].Attributes = {};
                for(var i = 0; i < _gridData[key].Attributes.length; i++){
                    _jsonData[key].Attributes[_gridData[key].Attributes[i].ExcelColumn] = {
                        ExcelColumn: _gridData[key].Attributes[i].ExcelColumn,
                        ColumnName: _gridData[key].Attributes[i].ColumnName,
                        AttrDisplayName: _gridData[key].Attributes[i].AttrDisplayName,
                        AttrLogicalName: _gridData[key].Attributes[i].AttrLogicalName,
                        AttrType: _gridData[key].Attributes[i].AttrType,
                        AttrTargetEntity: _gridData[key].Attributes[i].AttrTargetEntity,
                        TypeField: _gridData[key].Attributes[i].TypeField,

                        IndicatorId: _gridData[key].Attributes[i].IndicatorId,
                        IEntity: _gridData[key].Attributes[i].IEntity,
                        IRelationName: _gridData[key].Attributes[i].IRelationName,
                        IName: _gridData[key].Attributes[i].IName,
                        IFieldTargetEntity: _gridData[key].Attributes[i].IFieldTargetEntity,
                        IFieldLogicalName: _gridData[key].Attributes[i].IFieldLogicalName,
                        IAdditionalAttr: _gridData[key].Attributes[i].IAdditionalAttr
                    }
                }
            }
            console.dir(_jsonData);
            return JSON.stringify(_jsonData);
        };

//Additional private functions

    function _setParameter(parameterObject){
        if(typeof parameterObject !== "undefined" && !!parameterObject && typeof parameterObject === "object"){
            for(var key in parameterObject) {
                _this.Internal(key).Value(parameterObject[key])
            }
        }
    }
    function _getFileId(successCallback, errorCallback) {
        console.log(">>> _getFileId");

        var req = new XMLHttpRequest();
        req.open("GET", encodeURI(_this.clientUrl + _this.baseUrl + "annotations?$select=filename,annotationid&$filter=_objectid_value eq " + (_this.Page.data.entity.getId()).replace(/\{|\}/g, '') + " and objecttypecode eq '" + _this.Page.data.entity.getEntityName() + "'"), true);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Prefer", "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"");
        req.setRequestHeader("Prefer", "odata.maxpagesize=1");
        req.onreadystatechange = function () {
            if (this.readyState == 4 /* complete */) {
                req.onreadystatechange = null;
                if (this.status == 200) {
                    var results = JSON.parse(this.response);
                    var fileId = null;
                    if (results.value.length == 1)
                        fileId = results.value[0].annotationid;
                    _this.Internal("fileName").Value(results.value[0].filename);
                    if (successCallback)
                        successCallback(fileId);
                }
                else {
                    if (errorCallback)
                        errorCallback(DocumentsJs.Attachments._errorHandler(this.response));
                }
            }
        };
        req.send();
    }
    function _createLinkToFile(id) {
        console.log(">>> _createLinkToFile");

        var noteUrl = _this.clientUrl + "/notes/edit.aspx?id=" + id;
        var notePage = null;
        var req = {};
        req.type = "GET";
        req.url = noteUrl;
        req.async = false;
        req.dataType = "html";
        req.success = function (data, textStatus, XmlHttpRequest) {
            notePage = data;
        };
        $.ajax(req);
        if (!!notePage) {
            var tage = 'span';
            var regexp = new RegExp('<span[^>]+WRPCTokenUrl[^>]+>');
            var SpanTages = notePage.match(regexp);
            if (!!SpanTages) {
                var TokenTage = SpanTages[0] + '</span>';
                var div = $('<div/>').html(TokenTage);
                var WRPCToken = $(tage, div).attr('WRPCTokenUrl');
                if (!!WRPCToken) {
                    //console.dir(WRPCToken);
                    var fileUrl = _this.clientUrl + "/Activities/Attachment/download.aspx?AttachmentType=5&AttachmentId=" + id + WRPCToken;
                    _this.crmForm.$("label[for='ddsm_name_label']").html('<div class="ms-crm-div-NotVisible">' + _this.getAttribute("ddsm_name").getValue() + '</div>' + '<a href="' + fileUrl + '" target="_blank" style="display:block;">' + _this.getAttribute("ddsm_name").getValue() + '</a><div class="ms-crm-Inline-GradientMask"></div>')
                }
            }
        }
    }
    function _getFormData(){
        console.log(">>> _getFormData");
        if((!!_this.getAttribute("ddsm_jsonexcel") && !!_this.getAttribute("ddsm_jsonexcel").getValue())
            && (!!_this.getAttribute("ddsm_jsondata") && !!_this.getAttribute("ddsm_jsondata").getValue())) {
            _this.Internal("SheetsName").Value(JSON.parse(_this.getAttribute("ddsm_jsonexcel").getValue()));
            _this.Internal("GridDataSource").Value(JSON.parse(_this.getAttribute("ddsm_jsondata").getValue()));

            var _entityNameList = [];
            for(var key in _this.Internal().SheetsName) {
                _this.Internal().TabsName.push({
                    SheetName: key
                });
                if(!!_this.Internal().SheetsName[key].Entity.LogicalName) {
                    _entityNameList.push(_this.Internal().SheetsName[key].Entity.LogicalName)
                }

            }
            /*
             for(var i = 0; i < _entityNameList.length; i++) {
             _this.Controller.getSourceEntityMetadata(_entityNameList[i], function(entityname){
             _this.Controller.actualizeGrid(entityname);
             });
             }
             */
            _createTabs(_this.Internal().TabsName);

        }
    }
    function _fixData(data) {
        var o = "", l = 0, w = 10240;
        for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint8Array(data.slice(l*w,l*w+w)));
        o+=String.fromCharCode.apply(null, new Uint8Array(data.slice(l*w)));
        return o;
    }
    function _verifyParsing() {
        console.log(">>> _verifyParsing");

        if(_this.Internal("xlsxParsing").Value()) {
            clearInterval(_this.Internal().intervalWorker1);
            setTimeout(function(){
                //Configuration form
                if(_this.Internal("isConfigForm").Value()){
                    kendo.ui.progress($("#grid"), true);
                    _this.showSpinner(true, "Creating the <br>Configuration Object Model");
                    _createEntitiesList(_generateForm);
                } else
                //Uploading form
                if(_this.Internal("isUploadForm").Value()){
                    _this.showSpinner(true, "Validation of the<br>uploaded file");
                    _verifyUploadedFile();
                } else {}
            }, 1000);
        }
    }
    function _verifyUploadedFile(){
        console.log(">>> _verifyUploadedFile");

        _this.getAdditionalConfigParameter(_this.getAttribute("ddsm_configuration").getValue()[0].id, function (resp){
            if(_this.getAttribute("ddsm_griddata")) {
                _this.getControl("ddsm_griddata").setDisabled(false);
                _this.getAttribute("ddsm_griddata").setValue(resp["ddsm_jsondata"]);
                _this.getControl("ddsm_griddata").setDisabled(true);
            }
            if(_this.getAttribute("ddsm_jsondata")) {
                _this.getControl("ddsm_jsondata").setDisabled(false);
                _this.getAttribute("ddsm_jsondata").setValue(_this.transformGridToJson(resp["ddsm_jsondata"]));
                _this.getControl("ddsm_jsondata").setDisabled(true);
            }
        });

        _this.getControl("ddsm_settings").setDisabled(false);
        _this.getControl("ddsm_usersettings").setDisabled(false);
        _this.getAttribute("ddsm_settings").setValue(JSON.stringify(_this.Internal("DUConfig").Value()));
        _this.getAttribute("ddsm_usersettings").setValue(JSON.stringify(_this.Internal("UserSettings").Value()));
        _this.getControl("ddsm_settings").setDisabled(true);
        _this.getControl("ddsm_usersettings").setDisabled(true);

        if(!!_this.getAttribute("ddsm_griddata") && !!_this.getAttribute("ddsm_griddata").getValue()) {
            _this.Internal("GridDataSource").Value(JSON.parse(_this.getAttribute("ddsm_griddata").getValue()));
        } else {
            $("#filePanel").show();
            _this.showSpinner(false);
            Alert.show(null, "The selected configuration is not valid.", [{
                label: "Ok",
                callback: function () {
                    try {
                        var uploadControl = $("#excelFile").data("kendoUpload");
                        uploadControl.clearAllFiles();
                    } catch(e){}
                    return;
                }
            }], "ERROR", 500, 200);
            return;
        }

        var _gridDataSource = _this.Internal("GridDataSource").Value(),
            _sheetsName = {}, _fileSheetLength = 0, _configSheetLength = 0, _isNotValid = false, _unmappedSheets = [];

        for(var key in _gridDataSource) {
            _configSheetLength++;
            if(!_gridDataSource[key].LogicalName){
                _unmappedSheets.push(key);
            }
        }
        if(_unmappedSheets.length > 0) {
            $("#filePanel").show();
            _this.showSpinner(false);
            Alert.show(null, "The selected configuration is not valid. Sheets " + _unmappedSheets.join(",") + " are not associated with the database.", [{
                label: "Ok",
                callback: function () {
                    try {
                        var uploadControl = $("#excelFile").data("kendoUpload");
                        uploadControl.clearAllFiles();
                    } catch(e){}
                    return;
                }
            }], "ERROR", 500, 200);
            return;
        }
        for(var key in _this.Internal().xlsxObj) {
            _fileSheetLength++;
            _sheetsName[key] = {
                Data: []
            };
            for(var key2 in _this.Internal().xlsxObj[key]) {
                _sheetsName[key].Data.push({
                    ExcelColumn: key2,
                    ColumnName: _this.Internal().xlsxObj[key][key2]
                });
            }
        }
        if(_configSheetLength != _fileSheetLength) {
            $("#filePanel").show();
            _this.showSpinner(false);
            Alert.show(null, "The uploaded file doesn't match the selected configuration.", [{
                label: "Ok",
                callback: function () {
                    try {
                        var uploadControl = $("#excelFile").data("kendoUpload");
                        uploadControl.clearAllFiles();
                    } catch(e){}
                    return;
                }
            }], "ERROR", 500, 200);
            return;
        }

        for(var key in _sheetsName){
            if(!_gridDataSource[key]) {
                _isNotValid = true;
                break;
            }
            if(_gridDataSource[key].Attributes.length != _sheetsName[key].Data.length){
                _isNotValid = true;
                break;
            }
            for(var i = 0; i < _sheetsName[key].Data.length; i++){
                if(!((_sheetsName[key].Data[i].ColumnName.toLowerCase()).indexOf(_gridDataSource[key].Attributes[i].ColumnName.toLowerCase())+1)){
                    if(!((_gridDataSource[key].Attributes[i].ColumnName.toLowerCase()).indexOf(_sheetsName[key].Data[i].ColumnName.toLowerCase())+1)){
                        _isNotValid = true;
                        break;
                    }
                }
            }
        }
        if(_isNotValid){
            $("#filePanel").show();
            _this.showSpinner(false);
            Alert.show(null, "The uploaded file doesn't match the selected configuration.", [{
                label: "Ok",
                callback: function () {
                    try {
                        var uploadControl = $("#excelFile").data("kendoUpload");
                        uploadControl.clearAllFiles();
                    } catch(e){}
                    return;
                }
            }], "ERROR", 500, 200);
            return;
        }

        for(var key in _gridDataSource){
            _this.Internal().objEntities[key] = {
                NotProcessed: -1,
                Processed: 0
            };
        }

        _this.Page.data.save().then(function(){
            setTimeout(function(){
                _this.formType = 2;

                var _confId = "";
                try {
                    _confId = _this.Page.data.entity.getId();
                }
                catch(e){
                    _confId = ""
                }
                if(!!_confId) {
                    _createAnnotation(
                        _this.Internal().newAnnotation,
                        _attachAnnotation,
                        function(e){console.dir(e);}
                    );
                }
            }, 100);
        })

    }
    function _generateForm() {
        console.log(">>> _generateForm");

        var _entityNameList = [];
        for(var key in _this.Internal().xlsxObj) {
            _this.Internal().TabsName.push({
                SheetName: key
            });
            var _entity =  {
                Label: "",
                LogicalName: "",
                Id:""
            };
            if(_this.Internal().EntitiesList.length > 0) {
                var result = $.grep(_this.Internal().EntitiesList, function(e){ return e.Label.toLowerCase() == key.toLowerCase(); });
                if(result.length == 1) {
                    _entity = result[0];
                }
            }
            if(!!_entity.LogicalName) {
                _entityNameList.push(_entity.LogicalName);
            }
            _this.Internal().SheetsName[key] = {
                Entity: _entity,
                Data: []
            };
            _this.Internal().GridDataSource[key] = {};
            _this.Internal().GridDataSource[key].LogicalName = _entity.LogicalName;
            _this.Internal().GridDataSource[key].Attributes = [];

            _this.Internal().SheetsName[key].Data = [];

            for(var key2 in _this.Internal().xlsxObj[key]) {
                _this.Internal().SheetsName[key].Data.push({
                    ExcelColumn: key2,
                    ColumnName: _this.Internal().xlsxObj[key][key2]
                });
                if(!_this.Internal().GridDataSource[key].LogicalName){
                    _this.Internal().GridDataSource[key].Attributes.push({
                        ExcelColumn: key2,
                        ColumnName: _this.Internal().xlsxObj[key][key2].trim(),
                        AttrDisplayName: "",
                        AttrLogicalName: "",
                        AttrType: "",
                        AttrTargetEntity: "",
                        TypeField: "",

                        IndicatorId: "",
                        IEntity: "",
                        IRelationName: "",
                        IName: "",
                        IFieldTargetEntity: "",
                        IFieldLogicalName: "",
                        IAdditionalAttr: ""


                    });
                }
            }
        }
        console.dir(_entityNameList);
        _this.showSpinner(true, "Get Entities Metadata");
        for(var i = 0; i < _entityNameList.length; i++) {
            _this.Controller.getSourceEntityMetadata(_entityNameList[i], function(entityname){
                _this.Controller.actualizeGrid(entityname);
            });
        }
        _createTabs(_this.Internal().TabsName);
    }
    function _createEntitiesList(callback) {
        console.log(">>> _createEntitiesList");

        var _eds = _this.Controller.EntitiesDataSource;
        _eds.read().done(function(){
            var _edsPre = _eds.data();
            _this.Internal().EntitiesList = _edsPre.toJSON();
            if(!!callback) {
                callback();
            }
        }).fail(function(){
            _this.Internal().EntitiesList = [];
            if(!!callback) {
                callback();
            }
        });
    }

    function _actualizeGrid(){
        console.log(">>> _actualizeGrid");

        var logicalName = "";
        if(!!_this.Internal("SelectedTab").Value() && !!_this.Internal("SheetsName").Value()[_this.Internal("SelectedTab").Value()]){
            logicalName = _this.Internal("SheetsName").Value()[_this.Internal("SelectedTab").Value()].Entity.LogicalName;
        }
        kendo.ui.progress($("#grid"), true);
        if(!!logicalName) {
            $("#targetEntityList").data("kendoDropDownList").value(logicalName);
            _this.Controller.getSourceEntityMetadata(logicalName, function(entityname) {
                _this.Controller.actualizeGridWrapper("grid");
            });
        } else {
            $("#targetEntityList").data("kendoDropDownList").value("");
            _this.Controller.actualizeGridWrapper("grid");
        }
    }
    function _createTabs(dataSource) {
        console.log(">>> _createTabs");

        window.parent.window.addEventListener('resize', function(){
            resizeIframe();
            DataUploader.Controller.actualizeGridWrapper("grid");
        });

        var tabs = $("#tabstrip").empty().kendoTabStrip({
            dataSource: dataSource,
            dataTextField: "SheetName",
            dataValueField: "SheetName",
            select: function (e) {
                kendo.ui.progress($("#grid"), true);
                _this.Internal().SelectedTab = e.item.textContent;
                _actualizeGrid();
            }
        });
        _activateFirstTab();
    }
    function _activateFirstTab() {
        console.log(">>> _activateFirstTab");

        var _gridDataSource = _this.Internal().GridDataSource;
        var _isGenDataGird = true;
        for(var i = 0; i < _this.Internal().TabsName.length; i++) {
            if(_gridDataSource[_this.Internal().TabsName[i].SheetName].Attributes.length == 0 && _this.Internal().SheetsName[_this.Internal().TabsName[i].SheetName].Entity.LogicalName != ""){
                _isGenDataGird = false;
                break;
            }
        }
        if(!_isGenDataGird) {
            setTimeout(_activateFirstTab, 250);
            return;
        }
        _this.Controller.actualizeTargetEntityWrapper("targetEntityList");
        _this.Internal("SelectedTab").Value(_this.Internal().TabsName[0].SheetName);
        $("#tabstrip").kendoTabStrip().data("kendoTabStrip").select(0);
        _this.showSpinner(false);
        $("#tablePanel").show();
        _actualizeGrid();
    }

    function _createAnnotation(entity, successCallback, errorCallback) {
        console.log(">>> _createAnnotation");

        if(_this.Internal("isUploadForm").Value()){
            _this.getControl("ddsm_status").setDisabled(false);
            _this.getAttribute("ddsm_status").setValue(962080000);
            _this.getControl("ddsm_status").setDisabled(true);
        }

        var req = new XMLHttpRequest();
        req.open("POST", encodeURI(_this.clientUrl + _this.baseUrl + "annotations"), true);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.onreadystatechange = function () {
            if (this.readyState == 4) {
                req.onreadystatechange = null;
                if (this.status == 204) {
                    if (successCallback)
                        successCallback(this.getResponseHeader("OData-EntityId"));
                }
                else {
                    if(_this.Internal("isUploadForm").Value()){
                        _this.getControl("ddsm_status").setDisabled(false);
                        _this.getAttribute("ddsm_status").setValue(962080002);
                        _this.getControl("ddsm_status").setDisabled(true);
                    }
                    if (errorCallback)
                        errorCallback(this.response);
                }
            }
        };
        req.send(JSON.stringify(entity));
    }
    function _attachAnnotation(oData){
        console.log(">>> _attachAnnotation");

        var _confId = _this.Page.data.entity.getId();
        var _note = oData.split(/[()]/);
        var thisRecord = {};
        var urlRecord = _this.clientUrl + _this.baseUrl + _this.Page.data.entity.getEntityName() + "s(" + ((_confId).replace(/\{|\}/g, '')).toUpperCase() + ")/" + _this.Page.data.entity.getEntityName() + "_Annotations/$ref";
        thisRecord["@odata.id"] = _this.clientUrl + _this.baseUrl + "annotations(" + (_note[1]).toUpperCase() + ")";

        var req = new XMLHttpRequest();
        req.open("POST", encodeURI(urlRecord), true);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.onreadystatechange = function () {
            if (this.readyState == 4) {
                req.onreadystatechange = null;
                if (this.status == 204) {
                    _this.formType = 2;
                    //Create link to download file
                    _getFileId(function (id) {
                        _createLinkToFile(id);
                    });
                    //Uploading form
                    if(_this.Internal("isUploadForm").Value()){
                        _this.showSpinner(false);
                        _this.getControl("ddsm_status").setDisabled(false);
                        _this.getAttribute("ddsm_status").setValue(962080001);
                        _this.getControl("ddsm_status").setDisabled(true);
                        _this.Page.data.save().then(function(){
                            _createChart();
                        });

                        if(_this.Internal("DUConfig").Value().DU_RemoteCalculation && (!!_this.Internal("DUConfig").Value().DU_ParserRemoteCalculationApiUrl || !!_this.Internal("DUConfig").Value().DU_CreatorRemoteCalculationApiUrl)) {
                            var _remoteApiURL = (!!_this.Internal("DUConfig").Value().DU_ParserRemoteCalculationApiUrl)?_this.Internal("DUConfig").Value().DU_ParserRemoteCalculationApiUrl:_this.Internal("DUConfig").Value().DU_CreatorRemoteCalculationApiUrl;

                            $.post( _remoteApiURL + "RunDataUploaderParserWithAppArgs", { TargetGuid: _this.Page.data.entity.getId().replace(/\{|\}/g, '') })
                                .done(function() {
                                    Alert.show("Excel File Upload Completed. Start remote parsing excel file.", null, [{
                                        label: "Ok",
                                        callback: function () {
                                        }
                                    }], "SUCCESS", 500, 200);
                                })
                                .fail(function(e) {
                                    console.log(e.message);
                                });
                        }
                        _hidePageControls();
                    }
                }
                else {
                    console.log(this.response);
                    _this.showSpinner(false);
                    if(_this.Internal("isUploadForm").Value()){
                        _this.getControl("ddsm_status").setDisabled(false);
                        _this.getAttribute("ddsm_status").setValue(962080002);
                        _this.getControl("ddsm_status").setDisabled(true);
                        _this.Page.data.save();
                    }
                }
            }
        };
        req.send(JSON.stringify(thisRecord));
    }
    //DragnDrop
    function _handleDragOver(n) {
        n.stopPropagation();
        n.preventDefault()
    }
    function _handleDrop(n) {
        n.stopPropagation();
        n.preventDefault();
        _processFiles(n.dataTransfer.files)
    }
    function _processFiles(n) {
        if (n && n.length && n.length == 1) {
            _this.LoadFile(n);
        } else {
            Alert.show(null, "Error", [{
                label: "Ok",
                callback: function () {
                    return;
                }
            }], "WARNING", 500, 200);
        }
    }

    //Chart
    function _createChart(){
        console.log(">>> _createChart");
        _this.showSpinner(false);

        var jsonData = _this.Internal("GridDataSource").Value();
        var chartStyle = '', i = 0, j = 1, _colors = 54;
        for(var key in jsonData) {
            i++;
            var _trunc = Math.trunc(i/_colors);
            if(_trunc == 0) {j=1}else{j = _trunc*_colors};
            chartStyle = chartStyle + '.entities-collection > li:nth-child(' + i + ') .entity-inner {height: 0%;bottom: 0;}';
            chartStyle = chartStyle + '.entities-collection li:nth-child(' + i + ') .entity-inner {background-color: ' + _this.ColorUI[i - j] +  ';opacity: 0.9;filter: alpha(Opacity=90);}';
            chartStyle = chartStyle + '.entities-collection li:nth-child(' + i + ') .entity-inner:before {background-color: ' + _this.ColorUI[i - j] +  ';opacity: 0.7;filter: alpha(Opacity=70);}';
            chartStyle = chartStyle + '.entities-collection li:nth-child(' + i + ') .entity-inner:after {background-color: ' + _this.ColorUI[i - j] +  ';opacity: 0.8;filter: alpha(Opacity=80);}';
        }
        $(".main").css({'width': i*143 + 'px'});
        var style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = chartStyle;
        document.getElementsByTagName('head')[0].appendChild(style);

        var htmlFirst = '<li><span>',
            htmlSecond = '<br/><b>0/0</b></span>'
                + '<div class="entity-wrapper">'
                + '<div class="entity-container">'
                + '<div class="entity-background"></div>'
                + '<div class="entity-inner">50</div>'
                + '<div class="entity-foreground"></div>'
                + '</div></div></li>',
            htmlLast = '<li>'
                + '<ul class="y-marker-container">'
                + '<li style="bottom:25%;"><span>25%</span></li>'
                + '<li style="bottom:50%;"><span>50%</span></li>'
                + '<li style="bottom:75%;"><span>75%</span></li>'
                + '<li style="bottom:100%;"><span>100%</span></li>'
                + '</ul>'
                + '</li>';
        for(var key in jsonData) {
            i++;
            $(".main ul.entities-collection").append(htmlFirst + key + htmlSecond);
        }
        $(".main ul.entities-collection").append(htmlLast);
        $("#tablePanel").show();

        if(parseInt(_this.getAttribute("ddsm_status").getValue()) != 962080002
        && parseInt(_this.getAttribute("ddsm_status").getValue()) != 962080005
            && parseInt(_this.getAttribute("ddsm_status").getValue()) != 962080007
            && parseInt(_this.getAttribute("ddsm_status").getValue()) != 962080008)
        {
            _this.Internal("intervalWorker1").Value(setInterval(_refreshPageData, 30000));
            _this.Internal("intervalWorker2").Value(setInterval(_updateCounter, 10000));
        } else {
            _updateCounter();
        }

    }

    function _refreshPageData(){
        _this.Page.data.refresh().then(function(){
            _hidePageControls();
            if(parseInt(_this.getAttribute("ddsm_status").getValue()) == 962080002) window.top.location.reload();
            if(parseInt(_this.getAttribute("ddsm_status").getValue()) == 962080005) window.top.location.reload();
            if(parseInt(_this.getAttribute("ddsm_status").getValue()) == 962080007) window.top.location.reload();
            if(parseInt(_this.getAttribute("ddsm_status").getValue()) == 962080008) window.top.location.reload();
        });

    }
    function _updateCounter(){
        console.log(">>> _updateCounter");

        var i = -1;
        for(var key in _this.Internal().objEntities){
            i++;
            var sel_fields = [['subject', 'count', 'NotProcessed']];
            var conditions = [['ddsm_datauploader', 'eq', (_this.Page.data.entity.getId()).replace(/\{|\}/g, '')],
                ['ddsm_processed', 'eq', false],
                ['subject', 'eq', key]];
            var filter = [conditions];
            var fetchData = AGS.Fetch.aggregateFXML("ddsm_exceldata", sel_fields, filter);
            _this.Internal().objEntities[key].NotProcessed = parseInt(fetchData.attributes["NotProcessed"].value);

            //Processed Count
            var sel_fields1 = [['subject', 'count', 'Processed']];
            var conditions1 = [['ddsm_datauploader', 'eq', (_this.Page.data.entity.getId()).replace(/\{|\}/g, '')],
                ['ddsm_processed', 'eq', true],
                ['subject', 'eq', key]];
            var filter1 = [conditions1];
            var fetchData1 = AGS.Fetch.aggregateFXML("ddsm_exceldata", sel_fields1, filter1);
            //if(!!fetchData.attributes["Processed"] && !!fetchData.attributes["Processed"].value)
            _this.Internal().objEntities[key].Processed = parseInt(fetchData1.attributes["Processed"].value);

            if((_this.Internal().objEntities[key].Processed + _this.Internal().objEntities[key].NotProcessed) != 0)
            {
                $(".entities-collection > li:nth-child(" + (i+1) + ") .entity-inner").css("height",Math.round(_this.Internal().objEntities[key].Processed * 100 / (_this.Internal().objEntities[key].Processed + _this.Internal().objEntities[key].NotProcessed)) + "%");
                $(".entities-collection > li:nth-child(" + (i+1) + ") span").html(key + "<br/><b>" + _this.Internal().objEntities[key].Processed +"/" + (_this.Internal().objEntities[key].Processed + _this.Internal().objEntities[key].NotProcessed + "</b>"));
            }
            else
            {
                if(parseInt(_this.getAttribute("ddsm_status").getValue()) > 962080006)
                {
                    $(".entities-collection > li:nth-child(" + (i+1) + ") .entity-inner").css("height","0%");
                    $(".entities-collection > li:nth-child(" + (i+1) + ") span").html(key + "<br/>" + "<b>0</b>");
                    $(".entities-collection > li:nth-child(" + (i+1) + ") span").css("color", "#ccc");
                }
            }

        }

        if(parseInt(_this.getAttribute("ddsm_status").getValue()) >= 962080001 && parseInt(_this.getAttribute("ddsm_status").getValue()) <= 962080006 && parseInt(_this.getAttribute("ddsm_status").getValue()) != 962080002 && parseInt(_this.getAttribute("ddsm_status").getValue()) != 962080005) {
            //if(parseInt(_this.getAttribute("ddsm_status").getValue()) == 962080004) window.top.location.reload();
            //if(parseInt(_this.getAttribute("ddsm_status").getValue()) == 962080006) window.top.location.reload();
        } else {
            if(!!_this.Internal().intervalWorker1)
            {
                clearInterval(_this.Internal().intervalWorker1);
                clearInterval(_this.Internal().intervalWorker2);
            }
        }

    };

    function _getUserSettings(){
        console.log(">>> _getUserSettings");

        var _userId = _this.Page.context.getUserId().replace(/\{|\}/g, '');
        var urlRecord = _this.clientUrl + _this.baseUrl + "usersettingscollection("+_userId.toUpperCase()+")?$select=timezonecode,_transactioncurrencyid_value";

        var req = new XMLHttpRequest();
        req.open("GET", encodeURI(urlRecord), false);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.onreadystatechange = function () {
            if (this.readyState == 4) {
                req.onreadystatechange = null;
                if (this.status == 200) {
                    var respObj = JSON.parse(req.responseText);
                    var _userObj = {
                        UserId: _userId,
                        TransactionCurrencyId: (!!respObj["_transactioncurrencyid_value"])?respObj["_transactioncurrencyid_value"]:"",
                        TimezoneCode: (!!respObj["timezonecode"])?respObj["timezonecode"]:92
                    };
                    _this.Internal("UserSettings").Value(_userObj);



                }
                else {
                    console.log(this.response);
                }
            }
        };
        req.send(null);
    }

    function _hidePageControls(){
        $("#processControlContainer .collapsibleArea", DataUploader.crmForm.document).hide();
        $("#processControlContainer .processWarningBar", DataUploader.crmForm.document).hide();
        $("#processControlContainer *", DataUploader.crmForm.document).each(function(){
            $(this, DataUploader.crmForm.document).unbind("click");
            $(this, DataUploader.crmForm.document).click(function (e){
                e.preventDefault();
                $("#processControlContainer .collapsibleArea", DataUploader.crmForm.document).hide();
                $("#processControlContainer .processWarningBar", DataUploader.crmForm.document).hide();
                return false;
            });
            $(this, DataUploader.crmForm.document).unbind('mouseenter mouseleave')
        })
    }
}).call(DataUploader);



