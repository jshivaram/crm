﻿using System;
using System.Collections.Generic;
using DDSM.CommonProvider.Model;
using Microsoft.Xrm.Sdk;

public class ESPConfig : IModuleConfig
{
    public class CustomLookup
    {
        public string Label { get; set; }
        public string LogicalName { get; set; }
    }

    public ESPConfig(Entity config)
    {
        if (config != null)
        {
            Url = config.GetAttributeValue<string>("ddsm_espurl");
            Login = config.GetAttributeValue<string>("ddsm_esplogin");
            Pass = config.GetAttributeValue<string>("ddsm_esppassword");
            DataPortion = config.GetAttributeValue<decimal>("ddsm_espdataportion");
            LastToken = Convert.FromBase64String(config.GetAttributeValue<string>("ddsm_esplasttoken"));
            RequestReceivedDate = config.GetAttributeValue<DateTime>("ddsm_requestreceiveddate");
            EspMapingData = config.GetAttributeValue<string>("ddsm_espmapingdata");
            EspUniqueQdd = config.GetAttributeValue<string>("ddsm_espuniqueqdd");
            EspQddMappingData = config.GetAttributeValue<string>("ddsm_espqddmappingdata");
            EspRemoteCalculation = config.GetAttributeValue<bool>("ddsm_espremotecalculation");
            EspRemoteCalculationApiUrl = config.GetAttributeValue<string>("ddsm_espremotecalculationapiurl");
            ConfigId = config.Id;
            ESPAdministratorsJson = config.GetAttributeValue<string>("ddsm_espadministrators");

        }
    }

    public string ConfigName => nameof(ESPConfig);
    public string Url { get; set; }
    public string Login { get; set; }
    public string Pass { get; set; }
    public decimal DataPortion { get; set; }
    
    public byte[] LastToken { get; set; }
    public CustomLookup DefaultLibrary { get; set; }
    public bool AllMadAreRequired { get; set; }
 
    public DateTime? RequestReceivedDate { get; set; }
    // mapping data
    public string EspMapingData { get; set; }
    public string EspUniqueQdd { get; set; }
    public string EspQddMappingData { get; set; }
    //remote calculation
    public bool EspRemoteCalculation { get; set; }
    public string EspRemoteCalculationApiUrl { get; set; }
    public Guid ConfigId { get; set; }
    public Dictionary<string, string> Esp2CrmTypes { get; set; }
    public string ESPAdministratorsJson { get; set; }
}

