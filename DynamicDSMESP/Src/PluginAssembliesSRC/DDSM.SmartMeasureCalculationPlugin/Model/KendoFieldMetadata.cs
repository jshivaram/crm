﻿using System.Collections.Generic;

namespace DDSM.SmartMeasureCalculationPlugin.Model
{
    public class KendoFieldMetadata
    {

        public string Name { get; set; }

        public string Role { get; set; }

        public object Value { get; set; }

        public string Label { get; set; }

        public List<OptionSetVal> DataSet { get; set; }

        public string Type { get; set; }

        public string FieldName { get; set; }

        public bool Required { get; set; }
    }
}
