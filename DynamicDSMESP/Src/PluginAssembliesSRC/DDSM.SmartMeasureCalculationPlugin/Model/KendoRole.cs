    namespace DDSM.SmartMeasureCalculationPlugin.Model
    {
        public enum KendoRole
        {
            kendoMaskedTextBox = 0, kendoDatePicker = 1, kendoDropDownList = 2, kendoNumericTextBox = 3
        }
    }
