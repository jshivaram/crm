﻿using System.Runtime.Serialization;
namespace DDSM.SmartMeasureCalculationPlugin.Model
{
   [DataContract]
    public class MeasureHeader
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}
