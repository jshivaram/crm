namespace DDSM.SmartMeasureCalculationPlugin.Model
{
    public class OptionSetVal
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }
}