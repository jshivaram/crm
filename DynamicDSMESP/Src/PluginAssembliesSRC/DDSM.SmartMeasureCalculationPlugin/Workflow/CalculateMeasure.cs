﻿using System;
using System.Activities;
using Microsoft.Xrm.Sdk.Workflow;
using DDSM.CommonProvider.src;
using DDSM.CommonProvider.Utils;
using Microsoft.Xrm.Sdk;

public class CalculateMeasure : BaseCodeActivity
{

    [Input("Measure Id")]
    [RequiredArgument]
    public InArgument<string> MeasureId { get; set; }

    public override string OperationEndpoint { get { return "CalculateMeasure"; } }

    [Input("Target Date in String")]
    public  InArgument<string> TargetDateString { get; set; }


    protected override void ExecuteActivity(CodeActivityContext context)
    {
        var espConfig = SettingsProvider.GetConfig<ESPConfig>(_objCommon.GetOrgService(systemCall: true));

        var measureId = MeasureId.Get(context);
        var measureRef = new EntityReference("ddsm_measure", Guid.Parse(measureId));
        var targetDateString = TargetDateString.Get(context);
        var targetDate = TargetDate.Get(context);
        var userInput = UserInput.Get(context);

        DateTime calcDate = DateTime.UtcNow;
        //if (string.IsNullOrEmpty(targetDateString) && DateTime.MinValue.Equals(targetDate))
        //{
        //    calcDate =
        //}
        if (!string.IsNullOrEmpty(targetDateString))
        {
            calcDate = DateTime.Parse(targetDateString);
        }
        if (!DateTime.MinValue.Equals(targetDate))
        {
            calcDate = targetDate;
        }

        //&& ? : 
        if (espConfig.EspRemoteCalculation)
        {
            RunRemoteEspCalc(espConfig.EspRemoteCalculationApiUrl, _objCommon?.Context?.InitiatingUserId ?? Guid.Empty, calcDate, userInput, measureRef);
        }
    }


}
