﻿using System.Activities;
using DDSM.CommonProvider.src;
using DDSM.CommonProvider.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;


/// <summary>
/// Provide an opportunity to Load all Mrtadata From ESP (SM Library,SM,MAD,QDD) 
/// </summary>
public class GetAllMadInfo : BaseCodeActivity
{
    #region internal fields

    public override string OperationEndpoint { get { return "SyncMetadata"; } }

    #endregion


    protected override void ExecuteActivity(CodeActivityContext context)
    {
        var target = getTarget(_objCommon.Context);

        var targetDate = TargetDate.Get(context);

        var espConfig = SettingsProvider.GetConfig<ESPConfig>(_objCommon.GetOrgService(systemCall: true));
        if (espConfig.EspRemoteCalculation)
        {
            RunRemoteEspCalc(espConfig.EspRemoteCalculationApiUrl, _objCommon.Context.InitiatingUserId, targetDate, "", target);
            Result.Set(context, "");
            Complete.Set(context, true);
            return;
        }
        Result.Set(context, "");
        Complete.Set(context, true);
    }

    private EntityReference getTarget(IWorkflowContext context)
    {
        if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
        {
            return ((Entity)context.InputParameters["Target"]).ToEntityReference();
        }
        else if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is EntityReference)
        {
            return (EntityReference)context.InputParameters["Target"];
        }
        else
        {
            return new EntityReference(context.PrimaryEntityName, context.PrimaryEntityId);
        }
    }



}
