﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider.JSON;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.src;
using DDSM.SmartMeasureCalculationPlugin.Model;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using DDSM.CommonProvider.Utils;

public class GetLatestMtData : BaseCodeActivity
{

    [Input("Target Records")]
    public InArgument<string> TargetRecords { get; set; }

    string GetKendoRole(string fieldType)
    {
        var result = "";
        switch (fieldType?.ToLower())
        {
            case "datetime":
                result = KendoRole.kendoDatePicker.ToString();
                break;

            case "optionset":
            case "picklist":
            case "lookup":
                result = KendoRole.kendoDropDownList.ToString();
                break;

            case "integer":
            case "double":
            case "money":
            case "decimal":
                result = KendoRole.kendoNumericTextBox.ToString();
                break;
        }

        return result;
    }

    private ESPConfig _config;

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        try
        {
            var targetRecordsJson = TargetRecords.Get(executionContext);
            var targetRecords = JsonConvert.DeserializeObject<Dictionary<string, Guid>>(targetRecordsJson);

            var targetMeasureTempl = targetRecords.FirstOrDefault(x => x.Key.ToLower().Equals("ddsm_measuretemplate"));
            var measureTemplate = new EntityReference(targetMeasureTempl.Key, targetMeasureTempl.Value);

            var targetDate = TargetDate.Get(executionContext);
            _config = SettingsProvider.GetConfig<ESPConfig>(_objCommon.GetOrgService(true));

            if (DateTime.MinValue == targetDate)
            {
                targetDate = DateTime.UtcNow;
            }

            if (targetDate > DateTime.UtcNow.AddSeconds(5))
            {
                throw new Exception("Target Date can't be more then Today");
            }

            var versions = CrmHelper?.GetSMVerions(measureTemplate);
            var latestVer = versions.GetLatestVersion(targetDate);
            var mappingData = GetMadDefDataMapping(latestVer);
            var data = GenerateFormMetadata(mappingData, measureTemplate, targetRecords);
            var result = new
            {
                fields = data
            };

            var resultJson = JsonConvert.SerializeObject(result);

            Result.Set(executionContext, resultJson);
            Complete.Set(executionContext, true);
        }
        catch (Exception e)
        {
            Result.Set(executionContext, e.Message);
            Complete.Set(executionContext, false);
            throw;
        }
    }

    Dictionary<string, List<MappingItem2>> GetMadDefDataMapping(DDSM.CommonProvider.Model.Version latestVer)
    {
        if (latestVer == null)
        {
            return null;
            //throw new Exception("Latest Sm version not found.");
        }
        //Contract.Requires<ArgumentNullException>(latestVer != null);
        Dictionary<string, List<MappingItem2>> result = null;

        //var madsJson = latestVer?.Record?.GetAttributeValue<string>("ddsm_madfields");
        //if (string.IsNullOrEmpty(madsJson))
        //{
        //    throw new Exception("Can't get MAD fields from SmartMesure. SM Id: " + latestVer?.Record?.Id);
        //}

        //var madMappingRef = latestVer?.Record?.GetAttributeValue<EntityReference>("ddsm_madmappingid");
        var maDefDataMappingRef = latestVer?.Record?.GetAttributeValue<EntityReference>("ddsm_maddefdatamapping");

        if (maDefDataMappingRef == null)
        {
            throw new Exception("Can't get MAD Def Data Mapping from SmartMesure. SM Id: " + latestVer?.Record?.Id);
        }
        var mappingRecord = _objCommon.GetOrgService().Retrieve(maDefDataMappingRef.LogicalName, maDefDataMappingRef.Id, new ColumnSet("ddsm_jsondata"));
        var mappingJson = mappingRecord.GetAttributeValue<string>("ddsm_jsondata");

        if (!string.IsNullOrEmpty(mappingJson))
        {
            result = JsonConvert.DeserializeObject<Dictionary<string, List<MappingItem2>>>(mappingJson);
        }
        return result;
    }

    private List<KendoFieldMetadata> GenerateFormMetadata(Dictionary<string, List<MappingItem2>> mappingData, EntityReference measureTemplate, Dictionary<string, Guid> targetRecords)
    {
        var result = new List<KendoFieldMetadata>();

        if (mappingData?.Count > 0)
        {
            var existingTarget = PrepareTaragetData(targetRecords, mappingData);
            foreach (var entFields in mappingData)
            {
                var currentEntFields = entFields;
                var entityName = currentEntFields.Key;
                var entityFields = currentEntFields.Value;

                var targetRef = targetRecords.FirstOrDefault(x => x.Key.ToLower().Equals(entityName.ToLower()));

                var targetData = existingTarget.FirstOrDefault(x => x.LogicalName.Equals(targetRef.Key) && x.Id.Equals(targetRef.Value));// ?? _objCommon.GetOrgService(systemCall: true).Retrieve(targetRef.Key, targetRef.Value, new ColumnSet(true));

                //existingTarget.Add(targetData);

                var indicatorsSet = GetIndicatorSetTemplatesData(measureTemplate);
                foreach (var _field in entityFields)
                {
                    var field = _field;
                    KendoFieldMetadata item;
                    // Entity field
                    item = !string.IsNullOrEmpty(field.AttrLogicalName) ? GetAttributeMetadata(targetData, field, entityName) : GetIndicatorMetadata(indicatorsSet, field);
                    //add to result list
                    if (item != null)
                    {
                        result.Add(item);
                    }
                }
            }
        }


        var msdFieldsMetadata = GetMsdMetadata(measureTemplate);
        if (msdFieldsMetadata?.Count > 0)
        {
            result.AddRange(msdFieldsMetadata);
        }

        return result;
    }

    private List<Entity> PrepareTaragetData(Dictionary<string, Guid> targetRecords, Dictionary<string, List<MappingItem2>> mappingData)
    {
        var result = new List<Entity>();

        if (targetRecords == null || targetRecords.Count == 0)
        {
            return result;
        }

        // get fields for every entity
        var targetEntities = from ent in mappingData
                             select new KeyValuePair<string, string[]>(ent.Key, ent.Value.Where(a => !string.IsNullOrEmpty(a.AttrLogicalName)).Select(x => x.AttrLogicalName?.ToLower()).Where(x => !string.IsNullOrEmpty(x)).ToArray());

        //retriev data for every target entity
        foreach (var ent in targetEntities.ToList())
        {
            var entityWithFields = ent;
            var entLogicalName = entityWithFields.Key;
            var entRef = targetRecords.FirstOrDefault(x => x.Key.Equals(entLogicalName));

            if (string.IsNullOrEmpty(entRef.Key) || Guid.Empty.Equals(entRef.Value) || entityWithFields.Value?.Length == 0) continue;
            try
            {
                var data = _objCommon.GetOrgService(systemCall: true).Retrieve(entRef.Key, entRef.Value, new ColumnSet(entityWithFields.Value));
                result.Add(data);
            }
            catch (Exception e)
            {
                _objCommon.TracingService.Trace("Error in PrepareTaragetData" + e.Message + "Can't get target data from entity: " + entRef.Key + "columns: " + string.Join(", ", entityWithFields.Value));
            }
        }
        return result;
    }

    private KendoFieldMetadata GetMSDFieldMetadata(MSDField msd, Entity mtData)
    {
        var msdLogicalName = msd?.AttrName?.ToLower();

        var item = new KendoFieldMetadata
        {
            Name = msd.AttrName, //field logical name
            FieldName = msd.AttrName,
            Value = (mtData != null && mtData.Attributes.ContainsKey(msdLogicalName)) ? mtData[msdLogicalName] : "",
            DataSet = null, //for optionset
            Label = !string.IsNullOrEmpty(msd.DispNameOver) ? msd.DispNameOver : msd.DispName, // label on form
            Type = msd.FieldType,
            Role = GetKendoRole(msd.FieldType), //type of control
        };
        return item;
    }

    private List<KendoFieldMetadata> GetMsdMetadata(EntityReference mtRef)
    {
        var mtData = _objCommon.GetOrgService().Retrieve(mtRef.LogicalName, mtRef.Id, new ColumnSet("ddsm_msdfields")); //existingTargetData.FirstOrDefault(x => x.LogicalName.Equals("ddsm_measuretemplate"));

        var result = new List<KendoFieldMetadata>();

        var msdFieldsJson = mtData?.GetAttributeValue<string>("ddsm_msdfields");
        List<MSDField> msdFields = null;

        if (!string.IsNullOrEmpty(msdFieldsJson))
        {
            msdFields = JsonConvert.DeserializeObject<List<MSDField>>(msdFieldsJson);
        }
        if (!(msdFields?.Count > 0)) return result;

        foreach (var msd in msdFields)
        {
            var item = GetMSDFieldMetadata(msd, mtData);

            if (_config != null && _config.AllMadAreRequired)
            {
                item.Required = true;
            }
            else
            {
                item.Required = msd.IsRequired; //is required
            }


            result.Add(item);
        }
        return result;
    }

    private KendoFieldMetadata GetIndicatorMetadata(EntityCollection data, MappingItem2 field)
    {
        var entity = data.Entities.FirstOrDefault(x => x.GetAttributeValue<string>("ddsm_name").Trim().ToLower().Equals(field.AttrDisplayName.Trim().ToLower()));

        var attrType = field?.AttrType;

        var result = new KendoFieldMetadata
        {
            Name = field?.FieldDisplayName?.Trim().ToLower().Replace(" ", "_").Replace("(", "_").Replace(")", "_"),
            Label = !string.IsNullOrEmpty(field?.AttrDisplayName) ? field?.AttrDisplayName?.Trim() : field?.FieldDisplayName?.Trim(),
            Role = KendoRole.kendoNumericTextBox.ToString(),
            FieldName = field?.FieldDisplayName,
            Type = attrType
        };

        switch (attrType)
        {
            case "Decimal":
                result.Value = entity?.GetAttributeValue<decimal>("ddsm_value");
                break;
            case "Currency":
            case "Money":
                result.Value = entity?.GetAttributeValue<Money>("ddsm_amount")?.Value;
                break;
        }

        if (_config != null && _config.AllMadAreRequired)
        {
            result.Required = true;
        }

        //if (string.IsNullOrEmpty(field?.AttrDisplayName?.Trim().ToLower()))
        //{
        //    result.Name = field?.FieldDisplayName?.Trim().ToLower().Replace(" ", "_");
        //    result.Label = field?.FieldDisplayName;
        //}
        return result;
    }

    private KendoFieldMetadata GetAttributeMetadata(Entity mtData, MappingItem2 field, string entityName)
    {
        //var result = new KendoFieldMetadata();

        var data = mtData;
        var fieldLogicalname = field.AttrLogicalName.ToLower();
        var fieldType = field.AttrType;

        var result = new KendoFieldMetadata
        {
            Name = field?.FieldDisplayName?.Trim().ToLower().Replace(" ", "_"),
            Label = !string.IsNullOrEmpty(field?.AttrDisplayName) ? field?.AttrDisplayName?.Trim() : field?.FieldDisplayName?.Trim(),
            FieldName = field.FieldDisplayName,
            Type = fieldType,
        };
        if (_config != null && _config.AllMadAreRequired)
        {
            result.Required = true;
        }

        switch (fieldType)
        {
            case "String":
                result.Value = data.GetAttributeValue<string>(fieldLogicalname);
                result.Role = KendoRole.kendoMaskedTextBox.ToString();
                break;
            case "Decimal":
                result.Value = data.GetAttributeValue<decimal>(fieldLogicalname);
                result.Role = KendoRole.kendoNumericTextBox.ToString();
                break;
            case "Money":
                result.Value = data.GetAttributeValue<Money>(fieldLogicalname)?.Value;
                result.Role = KendoRole.kendoNumericTextBox.ToString();
                break;
            case "Picklist":
                result.Value = data.GetAttributeValue<OptionSetValue>(fieldLogicalname)?.Value;
                result.DataSet = GetOptionSetMetadata(entityName, field.AttrLogicalName);
                result.Role = KendoRole.kendoDropDownList.ToString();
                break;
        }
        return result;
    }

    private List<OptionSetVal> GetOptionSetMetadata(string entityName, string fieldName)
    {
        if (string.IsNullOrEmpty(entityName) || string.IsNullOrEmpty(fieldName))
        {
            return null;
        }

        var request = new RetrieveAttributeRequest { EntityLogicalName = entityName, LogicalName = fieldName };

        var data = (RetrieveAttributeResponse)_objCommon.GetOrgService().Execute(request);

        var result = new List<OptionSetVal>();

        if (data != null)
        {
            foreach (var val in ((PicklistAttributeMetadata)data.AttributeMetadata).OptionSet.Options)
            {
                var d = new OptionSetVal
                {
                    Name = val.Label.LocalizedLabels[0].Label,
                };

                if (val.Value != null)
                {
                    d.Value = (int)val.Value;
                }
                result.Add(d);
            }

        }

        return result;
    }

    private EntityCollection GetIndicatorSetTemplatesData(EntityReference measureTemplate)
    {
        var query = new QueryExpression
        {
            EntityName = "ddsm_indicatorsettemplate",
            ColumnSet = new ColumnSet("ddsm_name", "ddsm_value", "ddsm_amount"),
            Criteria = new FilterExpression(LogicalOperator.And) { Conditions = { new ConditionExpression("ddsm_measuretemplate", ConditionOperator.Equal, measureTemplate.Id) } }
        };

        return _objCommon.GetOrgService().RetrieveMultiple(query);
    }
}
