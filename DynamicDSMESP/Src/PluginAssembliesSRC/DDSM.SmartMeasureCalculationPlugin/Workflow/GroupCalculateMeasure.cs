using System;
using System.Activities;
using DDSM.CommonProvider.src;
using DDSM.CommonProvider.Utils;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

public class GroupCalculateMeasure : BaseCodeActivity
{
    #region "Parameter Definition"
    [Output("Processed Projects")]
    public OutArgument<string> ProcessedProject { get; set; }

    [Input("Project")]
    //[RequiredArgument]
    [ArgumentEntity("ddsm_project")]
    [ReferenceTarget("ddsm_project")]
    public InArgument<EntityReference> Project { get; set; }

    #endregion

    public override string OperationEndpoint { get { return "GroupCalculateMeasures"; } }

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        ESPConfig espConfig = SettingsProvider.GetConfig<ESPConfig>(_objCommon.GetOrgService(systemCall: true));

        EntityReference projectRef = Project.Get(executionContext);
        if (espConfig.EspRemoteCalculation)
        {
            RunRemoteEspCalc(espConfig.EspRemoteCalculationApiUrl, _objCommon?.Context?.InitiatingUserId ?? Guid.Empty, TargetDate.Get(_objCommon?.ExecutionContext), "", projectRef);
        }
    }

    //private void RunRemoteEspCalc(ESPConfig espConfig, EntityReference projectRef)
    //{
    //    var espRemoteCalcURL = espConfig.EspRemoteCalculationApiUrl;
    //    var client = new HttpClient { BaseAddress = new Uri(espRemoteCalcURL) };
    //    var config = new
    //    {
    //        UserId = 
    //        Target = projectRef,
    //        TargetDate = TargetDate.Get(_objCommon.ExecutionContext)
    //    };

    //    var data = JsonConvert.SerializeObject(config);
    //    var content = new StringContent(data, Encoding.UTF8, "application/json");
    //    client.PostAsync("", content);
    //}

}

