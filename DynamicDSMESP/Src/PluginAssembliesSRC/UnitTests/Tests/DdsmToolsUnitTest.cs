﻿using System;
using System.Activities;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Workflow;
using Moq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace UnitTests.Tests
{
    [TestClass]
    public class DdsmToolsUnitTest
    {
        #region Class Constructor
        private string _namespaceClassAssembly;

        public DdsmToolsUnitTest()
        {
            _namespaceClassAssembly = "GetAllMadInfo" + ", " + "DDSM.SmartMeasureCalculationPlugin";
        }
        #endregion
        #region Test Initialization and Cleanup
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext) { }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void ClassCleanup() { }

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void TestMethodInitialize() { }

        // Use TestCleanup to run code after each test has run
        [TestCleanup]
        public void TestMethodCleanup() { }
        #endregion

        [TestMethod]
        public void ClaimAndPayMeasures()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "ClaimAndPayMeasures" + ", " + "DDSM.Tools";
           
            var targetEntity = new EntityReference
                {
                    LogicalName = "ddsm_project",
                    Id = Guid.Parse("4FF90CC8-8498-E711-80D8-663933383038")
                };
                
            //Input parameters
            var inputs = new Dictionary<string, object>
                {
                    { "UserInput", "{'measuresId':['DB76A39A-3C99-E711-80DA-663933383038'],'message':1,'financialtemplate':'A7FF8C9C-D08C-E711-80D6-663933383038'}" }
                };
            //"{'SmartMeasures':['10aced81-1e70-e611-80db-366637386334'],'DataFields':{}}"
            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void GetDataForMeasureClaim()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "GetDataForMeasureClaim" + ", " + "DDSM.Tools";
            var targetEntity = new EntityReference
                {
                    LogicalName = "ddsm_project",
                    Id = Guid.Parse("691188AD-7D98-E711-80D8-663933383038")
                };

            //Input parameters
            var inputs = new Dictionary<string, object>
                {
                    { "ParentProjectId", "691188AD-7D98-E711-80D8-663933383038" },
                    { "ViewName", "Claim and Pay Measures View" }

                };
            //"{'SmartMeasures':['10aced81-1e70-e611-80db-366637386334'],'DataFields':{}}"
            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void GetEntityIdByTwoAttributes()
        {
            //Set Plugin name for Invoke 
            //[Namespace.class name, assembly name]
            _namespaceClassAssembly = "GetEntityIdByTwoAttributes" + ", " + "DDSM.Tools";
            var targetEntity = new EntityReference
                {
                    LogicalName = "ddsm_project",
                    Id = Guid.Parse("691188AD-7D98-E711-80D8-663933383038")
                };

            //Input parameters
            var inputs = new Dictionary<string, object>
                {
                    { "EntityLogicalName", "ddsm_indicatorvalue" },
                    { "FirstAttributeName", "ddsm_name" },
                    { "FirstAttributeValue", "Art Project" }
                };
            //"{'SmartMeasures':['10aced81-1e70-e611-80db-366637386334'],'DataFields':{}}"
            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        /// <summary>
        /// Modify to mock CRM Organization Service actions
        /// </summary>
        /// <param name="serviceMock">The Organization Service to mock</param>
        /// <returns>Configured Organization Service</returns>
        private static Mock<IOrganizationService> TestMethod1Setup(Mock<IOrganizationService> serviceMock)
        {
            EntityCollection queryResult = new EntityCollection();
            ////Add created items to EntityCollection

            serviceMock.Setup(t =>
                t.RetrieveMultiple(It.IsAny<QueryExpression>()))
                .ReturnsInOrder(queryResult);

            return serviceMock;
        }

        /// <summary>
        /// Invokes the workflow.
        /// </summary>
        /// <param name="name">Namespace.Class, Assembly</param>
        /// <param name="target">The target entity</param>
        /// <param name="inputs">The workflow input parameters</param>
        /// <param name="configuredServiceMock">The function to configure the Organization Service</param>
        /// <returns>The workflow output parameters</returns>
        private static IDictionary<string, object> InvokeWorkflow(string name, ref EntityReference target, Dictionary<string, object> inputs,
            Func<Mock<IOrganizationService>, Mock<IOrganizationService>> configuredServiceMock)
        {
            var testClass = Activator.CreateInstance(Type.GetType(name)) as CodeActivity;

            var serviceMock = new Mock<IOrganizationService>();
            var factoryMock = new Mock<IOrganizationServiceFactory>();
            var tracingServiceMock = new Mock<ITracingService>();
            var workflowContextMock = new Mock<IWorkflowContext>();

            //Apply configured Organization Service Mock
            if (configuredServiceMock != null)
                serviceMock = configuredServiceMock(serviceMock);

            CrmConnection connection = CrmConnection.Parse("Url=https://crm212.ddsm.online/AGSDEV;Username=alex.shkidin; Password=nX9KcwtgPL; Timeout=00:10:00;");
            //CrmConnection connection = CrmConnection.Parse("Url=http://192.168.1.12/RLS/;Username=Administrator; Password=Ghjuhfvth89!; Timeout=00:10:00;");
            //CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.191/EfficiencyOneDDSM/;Username=Administrator; Password=Rjnecz1219694; Timeout=00:10:00;");
            //CrmConnection connection = CrmConnection.Parse("Url=http://ddsm-build-e1.ddsm.online/RLS/;Username=sergeyd; Password=GhjuhfvtH!; Timeout=00:10:00;");
             //CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS090916/; Username=Administrator; Password=c#102612; Timeout=00:10:00;");
            //  CrmConnection connection = CrmConnection.Parse("Url=https://egddemo1.crm.dynamics.com/; ; Username=admin@EGDdemo1.onmicrosoft.com; Password=P@ssword1; Timeout=00:10:00;");
            // CrmConnection connection = CrmConnection.Parse("Url=http://e1-dev1.dynamicdsm.com/RLS/; Domain=e1-dev1.dynamicdsm.com; Username=dev2; Password=Development2pass!; Timeout=00:10:00;");
            //  CrmConnection connection = CrmConnection.Parse("Url=http://e1-dev1.dynamicdsm.com/RLS/; Domain=e1-dev1.dynamicdsm.com; Username=pgatestuser5; Password=Pgatest!User5; Timeout=00:10:00;");
            //  CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS; Domain=RLS; Username=dev2; Password=Development2pass!; Timeout=00:10:00;");
            //   CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS310816/; Domain=RLS; Username=dev2; Password=Development2pass!; Timeout=00:10:00;");
            //CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS; Domain=RLS; Username=Administrator; Password=Rjnecz121969; Timeout=00:10:00;");
            IOrganizationService service = new OrganizationService(connection);
            //  IOrganizationService service = serviceMock.Object;


            //Mock workflow Context
            var workflowUserId = Guid.Parse("{3D4AB943-A3D2-E611-80F9-666665393634}");
            var workflowCorrelationId = Guid.NewGuid();
            var workflowInitiatingUserId = Guid.Parse("{3D4AB943-A3D2-E611-80F9-666665393634}");

            //Workflow Context Mock
            workflowContextMock.Setup(t => t.InitiatingUserId).Returns(workflowInitiatingUserId);
            workflowContextMock.Setup(t => t.CorrelationId).Returns(workflowCorrelationId);
            workflowContextMock.Setup(t => t.UserId).Returns(workflowUserId);
            var workflowContext = workflowContextMock.Object;

            //Organization Service Factory Mock
            factoryMock.Setup(t => t.CreateOrganizationService(It.IsAny<Guid>())).Returns(service);
            var factory = factoryMock.Object;

            //Tracing Service - Content written appears in output
            tracingServiceMock.Setup(t => t.Trace(It.IsAny<string>(), It.IsAny<object[]>())).Callback<string, object[]>(MoqExtensions.WriteTrace);
            var tracingService = tracingServiceMock.Object;

            //Parameter Collection
            ParameterCollection inputParameters = new ParameterCollection { { "Target", target } };
            workflowContextMock.Setup(t => t.InputParameters).Returns(inputParameters);

            //Workflow Invoker
            var invoker = new WorkflowInvoker(testClass);
            invoker.Extensions.Add(() => tracingService);
            invoker.Extensions.Add(() => workflowContext);
            invoker.Extensions.Add(() => factory);

            return invoker.Invoke(inputs);
        }
    }
}
