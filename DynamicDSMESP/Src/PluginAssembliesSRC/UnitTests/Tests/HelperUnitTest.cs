﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Moq;

namespace UnitTests.Tests
{
    [TestClass]
    public class HelperUnitTest
    {
        #region Class Constructor
        private string _namespaceClassAssembly;

        public HelperUnitTest()
        {
            //[Namespace.class name, assembly name] for the class/assembly being tested
            //Namespace and class name can be found on the class file being tested
            //Assembly name can be found under the project properties on the Application tab
            _namespaceClassAssembly = "GetRCR" + ", " + "DDSM.Helper";

            //"DDSM.Helper.Class.CreateProject"
        }
        #endregion
        #region Test Initialization and Cleanup
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void ClassInitialize(TestContext testContext) { }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup()]
        public static void ClassCleanup() { }

        // Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void TestMethodInitialize() { }

        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void TestMethodCleanup() { }
        #endregion

        [TestMethod]
        public void TestGetLatestMtData()
        {
            _namespaceClassAssembly = "GetLatestMtData" + ", " + "DDSM.SmartMeasureCalculationPlugin";
            //Target
            Entity targetEntity = new Entity { LogicalName = "ddsm_measuretemplate", Id = Guid.Parse("6ad976ea-64e3-e611-810c-666665393634") };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                //{ "UserInput", Convert.ToBase64String(Encoding.UTF8.GetBytes("{'accountid':'{5934E493-31F4-E611-812B-666665393634}','parentsiteid':'{EEA58626-6444-E711-80CA-326539636430}','tradeally':'','projectId':'{DFA5FCC9-2C57-E711-80DE-08002746373B}','projectName':'152814-GH-Alden Landry1','projectstatus':'1-Initiated','phase':'1. Proposed','phasenumber':1,'initialphasedate':'06/30/2017','measureId':'{00000000-0000-0000-0000-000000000000}','measureTplId':'5bffff1c-56d8-e611-80fa-666665393634','calculationType':'5bffff1c-56d8-e611-80fa-666665393634','espSmartMeasureId':'a881c31b-db25-40f2-b3b2-0734c1fc5604','dataFields':{'kWh Gross Savings at Meter per unit':'10443','Units':'4','kW Gross Savings at Meter per unit':'4','Incentive Cost-Measure Per Unit':'4','Incentive Payment-Gross Per Unit':'500','Line Loss Factor':'0.1','Net to Gross Ratio-Standard':'0.64','ddsm_purchasepricefinancing':'4','ddsm_purchasepriceactual':'4'}}"))},
                ////                                                            8f783dbc-3505-e611-80c6-323166616637
                // { "ParentSite", new EntityReference("ddsm_site", Guid.Parse("8F783DBC-3505-E611-80C6-323166616637"))},
                // { "StartDate", DateTime.UtcNow},
                // { "ProgramOffering", new EntityReference("ddsm_projectoffreing", Guid.Parse("7CF4AC3A-25FB-E511-80C0-323166616637"))},
                // { "Workflow", new EntityReference("ddsm_projecttemplate", Guid.Parse("6A9D524C-29FB-E511-80C0-323166616637"))},
                // { "SiteMeasure", new EntityReference("ddsm_sitemeasure", Guid.Parse("FAD453D3-CD07-E611-80C6-323166616637"))}
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        [TestMethod]
        public void TestGetDefaultSenderEmail()
        {
            _namespaceClassAssembly = "GetDefaultSenderEmail" + ", " + "DDSM.Helper";
            //Target
            Entity targetEntity = new Entity { LogicalName = "ddsm_project", Id = Guid.Parse("F6370382-169C-E611-80E0-366637386334") };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                //{ "UserInput", Convert.ToBase64String(Encoding.UTF8.GetBytes("{'accountid':'{5934E493-31F4-E611-812B-666665393634}','parentsiteid':'{EEA58626-6444-E711-80CA-326539636430}','tradeally':'','projectId':'{DFA5FCC9-2C57-E711-80DE-08002746373B}','projectName':'152814-GH-Alden Landry1','projectstatus':'1-Initiated','phase':'1. Proposed','phasenumber':1,'initialphasedate':'06/30/2017','measureId':'{00000000-0000-0000-0000-000000000000}','measureTplId':'5bffff1c-56d8-e611-80fa-666665393634','calculationType':'5bffff1c-56d8-e611-80fa-666665393634','espSmartMeasureId':'a881c31b-db25-40f2-b3b2-0734c1fc5604','dataFields':{'kWh Gross Savings at Meter per unit':'10443','Units':'4','kW Gross Savings at Meter per unit':'4','Incentive Cost-Measure Per Unit':'4','Incentive Payment-Gross Per Unit':'500','Line Loss Factor':'0.1','Net to Gross Ratio-Standard':'0.64','ddsm_purchasepricefinancing':'4','ddsm_purchasepriceactual':'4'}}"))},
                ////                                                            8f783dbc-3505-e611-80c6-323166616637
                // { "ParentSite", new EntityReference("ddsm_site", Guid.Parse("8F783DBC-3505-E611-80C6-323166616637"))},
                // { "StartDate", DateTime.UtcNow},
                // { "ProgramOffering", new EntityReference("ddsm_projectoffreing", Guid.Parse("7CF4AC3A-25FB-E511-80C0-323166616637"))},
                // { "Workflow", new EntityReference("ddsm_projecttemplate", Guid.Parse("6A9D524C-29FB-E511-80C0-323166616637"))},
                // { "SiteMeasure", new EntityReference("ddsm_sitemeasure", Guid.Parse("FAD453D3-CD07-E611-80C6-323166616637"))}
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }




        //
        [TestMethod]
        public void CreateMeasure()
        {
            _namespaceClassAssembly = "CreateMeasure" + ", " + "DDSM.Helper";
            //Target
            Entity project = new Entity { LogicalName = "ddsm_project", Id = Guid.Parse("{959B8159-886C-E711-80CF-663933383038}") };
            Entity targetMT = new Entity { LogicalName = "ddsm_measuretemplate", Id = Guid.Parse("{68842E8A-7C6C-E711-80CF-663933383038}") };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                { "UserInput","{'kWh Gross Savings at Meter per unit':23423,'Units':0,'kW Gross Savings at Meter per unit':20,'Incentive Cost-Measure Per Unit':30,'Incentive Payment-Gross Per Unit':40,'Line Loss Factor':2342342,'Net to Gross Ratio-Standard':50}" },
                { "Project", project.ToEntityReference() }
               
                //Convert.ToBase64String(Encoding.UTF8.GetBytes("{'accountid':'{5934E493-31F4-E611-812B-666665393634}','parentsiteid':'{978C6118-6544-E711-80CA-326539636430}','tradeally':'','projectId':'{981C24CE-0B46-E711-80CA-326539636430}','projectName':'149856-GH-Alden Landry1','projectstatus':'1-Initiated','phase':'1. Proposed','phasenumber':1,'initialphasedate':'07/12/2017','measureId':'{00000000-0000-0000-0000-000000000000}','measureTplId':'6ad976ea-64e3-e611-810c-666665393634','calculationType':'6ad976ea-64e3-e611-810c-666665393634','espSmartMeasureId':null,'dataFields':{'Units':'1','Incentive Cost-Measure':'1','Line Loss Factor':'0.1','Net to Gross Ratio-Standard':'0.76','Interactive Effects-Energy':'0.96','Interactive Effects-Demand':'0.944','Inservice Rate ':'0.85','Coincidence Factor':'0.77','Measure Lifetime ':'1','kWh Unitary Savings at Meter per unit':'484.2098','kW Unitary Savings at Meter per unit':'0.113','Net to Gross-Demand':'2'}}"))},
                ////8f783dbc-3505-e611-80c6-323166616637
                // { "ParentSite", new EntityReference("ddsm_site", Guid.Parse("8F783DBC-3505-E611-80C6-323166616637"))},
                // { "StartDate", DateTime.UtcNow},
                // { "ProgramOffering", new EntityReference("ddsm_projectoffreing", Guid.Parse("7CF4AC3A-25FB-E511-80C0-323166616637"))},
                // { "Workflow", new EntityReference("ddsm_projecttemplate", Guid.Parse("6A9D524C-29FB-E511-80C0-323166616637"))},
                // { "SiteMeasure", new EntityReference("ddsm_sitemeasure", Guid.Parse("FAD453D3-CD07-E611-80C6-323166616637"))}
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetMT, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }


        [TestMethod]
        public void TestMethod1()
        {
            //Target
            Entity targetEntity = new Entity { LogicalName = "lead", Id = Guid.NewGuid() };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
               { "UserInput", "EE370382-169C-E611-80E0-366637386334"},

            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }


        [TestMethod]
        public void CheckActivePGFTaskByPG()
        {
            _namespaceClassAssembly = "CheckActivePGFTaskByPG" + ", " + "DDSM.Helper";
            //Target
            Entity targetEntity = new Entity { LogicalName = "ddsm_projectgroupfinancials", Id = Guid.Parse("F6370382-169C-E611-80E0-366637386334") };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                            { "UserInput", "EE370382-169C-E611-80E0-366637386334"},
               ////                                                            8f783dbc-3505-e611-80c6-323166616637
               // { "ParentSite", new EntityReference("ddsm_site", Guid.Parse("8F783DBC-3505-E611-80C6-323166616637"))},
               // { "StartDate", DateTime.UtcNow},
               // { "ProgramOffering", new EntityReference("ddsm_projectoffreing", Guid.Parse("7CF4AC3A-25FB-E511-80C0-323166616637"))},
               // { "Workflow", new EntityReference("ddsm_projecttemplate", Guid.Parse("6A9D524C-29FB-E511-80C0-323166616637"))},
               // { "SiteMeasure", new EntityReference("ddsm_sitemeasure", Guid.Parse("FAD453D3-CD07-E611-80C6-323166616637"))}
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }
        [TestMethod]
        public void UpdatePGCalculationStatus()
        {
            _namespaceClassAssembly = "UpdatePGCalculationStatus" + ", " + "DDSM.Helper";
            //Target
            Entity targetEntity = new Entity { LogicalName = "ddsm_projectgroupfinancials", Id = Guid.Parse("{A1E4B811-9F05-E711-8135-666665393634}") };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                            { "UserInput", "{'SmartMeasures':['dfd6e86b-9e05-e711-8135-666665393634'],'DataFields':{}}"},
               ////                                                            8f783dbc-3505-e611-80c6-323166616637
               // { "ParentSite", new EntityReference("ddsm_site", Guid.Parse("8F783DBC-3505-E611-80C6-323166616637"))},
               // { "StartDate", DateTime.UtcNow},
               // { "ProgramOffering", new EntityReference("ddsm_projectoffreing", Guid.Parse("7CF4AC3A-25FB-E511-80C0-323166616637"))},
               // { "Workflow", new EntityReference("ddsm_projecttemplate", Guid.Parse("6A9D524C-29FB-E511-80C0-323166616637"))},
               // { "SiteMeasure", new EntityReference("ddsm_sitemeasure", Guid.Parse("FAD453D3-CD07-E611-80C6-323166616637"))}
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }


        [TestMethod]
        public void GetRCR()
        {
            //Target
            Entity targetEntity = new Entity { LogicalName = "ddsm_measure", Id = Guid.Parse("8238B9AA-F541-E611-80E5-323166616637") };

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
               { "RateCode",new OptionSetValue (100000027)},
                 { "StartProjectDate", DateTime.Now},
               ////                                                            8f783dbc-3505-e611-80c6-323166616637
               // { "ParentSite", new EntityReference("ddsm_site", Guid.Parse("8F783DBC-3505-E611-80C6-323166616637"))},
               // { "StartDate", DateTime.UtcNow},
               // { "ProgramOffering", new EntityReference("ddsm_projectoffreing", Guid.Parse("7CF4AC3A-25FB-E511-80C0-323166616637"))},
               // { "Workflow", new EntityReference("ddsm_projecttemplate", Guid.Parse("6A9D524C-29FB-E511-80C0-323166616637"))},
               // { "SiteMeasure", new EntityReference("ddsm_sitemeasure", Guid.Parse("FAD453D3-CD07-E611-80C6-323166616637"))}
            };

            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        /// <summary>
        /// Modify to mock CRM Organization Service actions
        /// </summary>
        /// <param name="serviceMock">The Organization Service to mock</param>
        /// <returns>Configured Organization Service</returns>
        private static Mock<IOrganizationService> TestMethod1Setup(Mock<IOrganizationService> serviceMock)
        {
            EntityCollection queryResult = new EntityCollection();
            ////Add created items to EntityCollection

            serviceMock.Setup(t =>
                t.RetrieveMultiple(It.IsAny<QueryExpression>()))
                .ReturnsInOrder(queryResult);

            return serviceMock;
        }

        /// <summary>
        /// Invokes the workflow.
        /// </summary>
        /// <param name="name">Namespace.Class, Assembly</param>
        /// <param name="target">The target entity</param>
        /// <param name="inputs">The workflow input parameters</param>
        /// <param name="configuredServiceMock">The function to configure the Organization Service</param>
        /// <returns>The workflow output parameters</returns>
        private static IDictionary<string, object> InvokeWorkflow(string name, ref Entity target, Dictionary<string, object> inputs,
            Func<Mock<IOrganizationService>, Mock<IOrganizationService>> configuredServiceMock)
        {
            var testClass = Activator.CreateInstance(Type.GetType(name)) as CodeActivity;

            var serviceMock = new Mock<IOrganizationService>();
            var factoryMock = new Mock<IOrganizationServiceFactory>();
            var tracingServiceMock = new Mock<ITracingService>();
            var workflowContextMock = new Mock<IWorkflowContext>();

            //Apply configured Organization Service Mock
            if (configuredServiceMock != null)
                serviceMock = configuredServiceMock(serviceMock);
            //CrmConnection connection = CrmConnection.Parse("Url=http://ddsm-build-e1.dynamicdsm.com/RLS; Username=yuriyn; Password=AgAJfH6T4p");
            CrmConnection connection = CrmConnection.Parse("Url=http://192.168.1.202/AGS; Username=Administrator; Password=Cjytxrf12041969");
            //  CrmConnection connection = CrmConnection.Parse("Url=http://195.88.73.189/RLS191016/; Username=Administrator; Password=c#102612; Timeout=00:10:00;");
            IOrganizationService service = new OrganizationService(connection);
            //  IOrganizationService service = serviceMock.Object;


            //Mock workflow Context
            var workflowUserId = Guid.NewGuid();
            var workflowCorrelationId = Guid.NewGuid();
            var workflowInitiatingUserId = Guid.NewGuid();

            //Workflow Context Mock
            workflowContextMock.Setup(t => t.InitiatingUserId).Returns(workflowInitiatingUserId);
            workflowContextMock.Setup(t => t.CorrelationId).Returns(workflowCorrelationId);
            workflowContextMock.Setup(t => t.UserId).Returns(workflowUserId);
            var workflowContext = workflowContextMock.Object;

            //Organization Service Factory Mock
            factoryMock.Setup(t => t.CreateOrganizationService(It.IsAny<Guid>())).Returns(service);
            var factory = factoryMock.Object;

            //Tracing Service - Content written appears in output
            tracingServiceMock.Setup(t => t.Trace(It.IsAny<string>(), It.IsAny<object[]>())).Callback<string, object[]>(MoqExtensions.WriteTrace);
            var tracingService = tracingServiceMock.Object;

            //Parameter Collection
            ParameterCollection inputParameters = new ParameterCollection { { "Target", target } };
            workflowContextMock.Setup(t => t.InputParameters).Returns(inputParameters);

            //Workflow Invoker
            var invoker = new WorkflowInvoker(testClass);
            invoker.Extensions.Add(() => tracingService);
            invoker.Extensions.Add(() => workflowContext);
            invoker.Extensions.Add(() => factory);

            return invoker.Invoke(inputs);
        }
    }
}
