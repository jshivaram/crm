//var configRecordName = "Admin Data";
var baseUrl = "/api/data/v8.2/";
var Xrm = window.parent.Xrm;
var clientUrl = Xrm.Page.context.getClientUrl();

$(document).ready(function() {
    // kendo.ui.progress($("#main"), true);
    $("#targetDate").kendoDatePicker({       
        autoBind: false,
        change: function(e) {     
        //debugger;
            window.parent.selectedTargetDate = this.value();
        }      
    });
    var defValue = new Date(); //kendo.toString(kendo.parseDate(new Date()), 'MM/dd/yyyy');
       window.parent.selectedTargetDate =defValue;
       $("#targetDate").data("kendoDatePicker").value(defValue); 
});