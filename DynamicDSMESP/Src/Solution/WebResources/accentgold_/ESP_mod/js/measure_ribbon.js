var currentWindow = parent.Xrm.Internal.isTurboForm() ? parent.window : window;
GlobalJs = currentWindow.GlobalJs; // || window.top.GlobalJs;
var defaultMeasureLibrary = "";
var formFilters = [];


var filter2fieldName = {
    962080000: "deliveryAgent",
    962080001: "measureLibrary",
};
var control2CrmFiled = {
    "deliveryAgent": "ddsm_deliveryagent_value",
    "measureLibrary": "_ddsm_espmeasurelibraryid_value",
};

var filterConditions = ['measureTemplate'];

var firstTemplate = [
    '    <ul class="fieldlist">',
    '            <li id="measureLibraryDiv" style="display:none;">',
    '                <label for="measureLibrary">Measure Library</label>',
    '                <input data-role="dropdownlist" id="measureLibrary" data-bind="value: mtValue, visible:false" type="text" validationMessage="Choose Measure Library" style="width: 100%;" />',
    '            </li>',
    '             <li id="deliveryAgentDiv" style="display:none;>',
    '                <label for="deliveryAgent">Delivery Agent</label>',
    '                <input data-role="dropdownlist" id="deliveryAgent" type="text" validationMessage="Choose Delivery Agent" style="width: 100%;" />',
    '            </li>',
    '             <li id="measureTemplateDiv">',
    '                <label for="measureTemplate">Measure Template</label>',
    '                <input data-role="dropdownlist" id="measureTemplate" type="text" validationMessage="Choose Measure Template" style="width: 100%;" />',
    '            </li>',
    '              </ul>',
].join("");


if (typeof ($) === 'undefined') {
    $ = currentWindow.$;
    jQuery = currentWindow.jQuery;
}

function customCreate() {
    //console.log("---in custom create");
    if (GlobalJs && GlobalJs.DDSM && GlobalJs.DDSM.Settings) {
        GlobalJs.DDSM.Settings.GetEspSettings(function (x) {
          //  console.log("ESP Config:");
           // console.log(x);
            defaultMeasureLibrary = x.DefaultLibrary.LogicalName;
            formFilters = x.CreateMeasureFormFilters;
            //formFilters.sort();
        }, null);
    }


    if (!$("#dialog", currentWindow.document).data("kendoDialog")) {
        $("#dialog", currentWindow.document).kendoDialog({
            width: "400px",
            //height: "240px",
            //visible: false,
            title: "Create New Measure",
            closable: true,
            modal: true,
            content: firstTemplate,
            close: function () {
                //$("#dialog", currentWindow.document).data("kendoDialog").destroy();
            },
            actions: [{
                    text: 'Cancel',
                    action: function () {
                        $("#dialog", currentWindow.document).data("kendoDialog").close();
                    }
                },
                {
                    text: 'OK',
                    primary: true,
                    action: actionOK
                }
            ],
            //initOpen: initOpen,
            open: dialogOpen
        });
    } else {
        $("#dialog", currentWindow.document).data("kendoDialog").open();
    }
}

function dialogOpen(e) {


    formFilters.forEach(function (element) {

        var controlName = filter2fieldName[element];
        filterConditions.push(controlName);

        switch (element) {
            case 962080000:
                $("#" + controlName, currentWindow.document).kendoDropDownList({
                    dataTextField: "Label",
                    dataValueField: "LogicalName",
                    optionLabel: "--",
                    change: filterChange,
                    dataSource: new currentWindow.kendo.data.DataSource({
                        schema: {
                            data: function (response) {
                                var value = response.value;
                                var model = value.map(function (el) {
                                    return {
                                        Label: el.name,
                                        LogicalName: el.accountid,
                                    }
                                });
                                return model;
                            }
                        },
                        sort: {
                            field: "Label",
                            dir: "asc"
                        },
                        transport: currentWindow.KendoHelper.CRM.transport(GlobalJs.clientUrl + GlobalJs.baseUrl + 'accounts?$select=name'),
                    }),
                    //index: 0,
                });
                $("#" + controlName + "Div").show();
                break;
            case 962080001:
                $("#" + controlName, currentWindow.document).kendoDropDownList({
                    dataTextField: "Label",
                    dataValueField: "LogicalName",
                    optionLabel: "--",
                    visible: false,
                    value: defaultMeasureLibrary,
                    required: true,
                    change: filterChange,
                    dataBound: function (e) {
                        //var handler = e;
                        var control = e.sender;
                        GlobalJs.DDSM.Settings.GetEspSettings(function (data) {
                            if (data["DefaultLibrary"] && data.DefaultLibrary["LogicalName"]) {
                                control.value(data.DefaultLibrary.LogicalName);
                                var dropdownlist = $("#" + controlName).data("kendoDropDownList");
                                dropdownlist.trigger("change");
                            }
                        }, null);
                    },
                    dataSource: new currentWindow.kendo.data.DataSource({
                        schema: {
                            data: function (response) {
                                var value = response.value;
                                var model = value.map(function (el) {
                                    return {
                                        Label: el.ddsm_name,
                                        LogicalName: el.ddsm_espmeasurelibraryid,
                                    }
                                });
                                return model;
                            }
                        },
                        sort: {
                            field: "Label",
                            dir: "asc"
                        },
                        transport: currentWindow.KendoHelper.CRM.transport(GlobalJs.clientUrl + GlobalJs.baseUrl + 'ddsm_espmeasurelibraries?$select=ddsm_name'), //&$orderby=ddsm_name asc
                    }),
                    // index: 0,
                });
                $("#" + controlName + "Div").show();
                break;
        }
    }, this);


    /* */
    function filterChange(e) {
        var mtControl = $("#" + "measureTemplate").data("kendoDropDownList");
        mtControl.setDataSource(measureTemplateDs());

        /* var $libraryId = e.sender.dataItem().LogicalName;
         var $template = $("#measureTemplate", currentWindow.document).data("kendoDropDownList");
         $template.setDataSource(new currentWindow.kendo.data.DataSource({
             schema: {
                 data: function (response) {
                     var value = response.value;
                     var model = value.map(function (el) {
                         return {
                             Label: el.ddsm_name,
                             LogicalName: el.ddsm_measuretemplateid,
                         }
                     });
                     return model;
                 }
             },
             sort: {
                 field: "Label",
                 dir: "asc"
             },
             transport: currentWindow.KendoHelper.CRM.transport(GlobalJs.clientUrl + GlobalJs.baseUrl + 'ddsm_measuretemplates?$select=ddsm_name&$filter=_ddsm_espmeasurelibraryid_value eq ' + $libraryId),
         }));*/

    }

    $("#measureTemplate", currentWindow.document).kendoDropDownList({
        dataTextField: "Label",
        dataValueField: "LogicalName",
        filter: "contains",
        optionLabel: "--",
        valuePrimitive: false,
        dataSource: measureTemplateDs(),
    });
}

function measureTemplateDs() {
    var filtersArray = [];

    var progOffControl = currentWindow.Xrm.Page.getAttribute("ddsm_programofferingid");
    if (progOffControl) {
        var progOffValues = progOffControl.getValue();
        if (progOffValues && progOffValues.length > 0) {
            var progOffValue = progOffValues[0].id.slice(1, -1);
            filtersArray.push("_ddsm_programoffering_value eq " + progOffValue);
        }
    }

    filterConditions.slice(1).forEach(function (element) {
        var crmField = control2CrmFiled[element];
        var controlValue = $("#" + element, currentWindow.document).data("kendoDropDownList").value();
        if (controlValue) {
            filtersArray.push(crmField + " eq " + controlValue);
        }
    }, this);

    var filter = "&$filter=" + filtersArray.join(" and ");

    //clear filter if conditions are empty
    if (filter && filter.length == 9) {
        filter = "";
    }
    //debugger;
    return new currentWindow.kendo.data.DataSource({
        schema: {
            data: function (response) {
                var value = response.value;
                var model = value.map(function (el) {
                    // debugger;
                    return {
                        Label: el.ddsm_name,
                        LogicalName: el.ddsm_measuretemplateid,
                        Type: el['ddsm_calctype@OData.Community.Display.V1.FormattedValue'],
                    }
                });
                return model;
            }
        },
        sort: {
            field: "Label",
            dir: "asc"
        },
        transport: currentWindow.KendoHelper.CRM.transport(GlobalJs.clientUrl + GlobalJs.baseUrl + 'ddsm_measuretemplates?$select=ddsm_name,ddsm_calctype' + filter, true),
        group: {
            field: "Type"
        },
        //_ddsm_espmeasurelibraryid_value eq ' + $libraryId
    });
}

function actionOK(e) {
      var validator = $("#dialog").kendoValidator({
          rules: {
              customRule1: function (input) {
                  var id = input[0].id;
                   if(filterConditions.indexOf(id) !=-1){
                      //  console.log("Validate: "+ id);
                        return input[0].value.length > 0 && input[0].value !="--";
                    }
                  /*if (id == "measureLibrary") {

                  }
                  else if (id == "deliveryAgent"){
                      return input[0].value.length > 0;
                  }
                  else if (id == "measureTemplate") {
                      return input[0].value.length > 0;
                  }*/
                  return true;
              }
          }
      }).data('kendoValidator');

      if (!validator.validate()) {
          //console.log("Person was not selected");
          return false;
      }

    var mt = $("#measureTemplate", currentWindow.document).data("kendoDropDownList");

    currentWindow.$mtId = mt.value();
    var mtName = mt.text();

    if (!$("#dialog2", currentWindow.document).data("kendoDialog")) {
        $("#dialog2", currentWindow.document).kendoDialog({
            width: "800px",
            //height: "300px",
            visible: false,
            title: mtName,
            closable: true,
            modal: true,
            content: html1,
            /*  close: function () {
                  //$("#dialog2", currentWindow.document).data("kendoDialog").destroy();
              },*/
            actions: [{
                    text: 'Cancel',
                    action: function () {
                        $("#dialog2", currentWindow.document).data("kendoDialog").close();
                    }
                },
                {
                    text: 'OK',
                    primary: true,
                    action: action2OK
                }
            ],
        });
    }
    showSpinner(true);
    try {
        var successCallback = function (data) {
            currentWindow.$model = data.fields;
            if (currentWindow.$model && currentWindow.$model.length > 0) {
                renderFields();
                showSpinner(false);
                $("#dialog2", currentWindow.document).data("kendoDialog").open();
            } else {
                action2OK();
                showSpinner(false);
            }
        };
        var errorCallback = function (errorMsg) {
            showSpinner(false);
            $("#dialog2", currentWindow.document).data("kendoDialog").close();
            currentWindow.Alert.show("Error", errorMsg, null, "ERROR");
        };

        var targetRecords = getTargetRecords();

        currentWindow.DDSM.WebApi.CallActionGlobal("ddsm_DDSMGetLatestMTData", {
            TargetRecords: currentWindow.JSON.stringify(targetRecords),
            // TargetDate: new Date().toISOString()
        }, successCallback, errorCallback);
    } catch (e) {
        console.log(e);
    }
}

function getTargetRecords() {
    var targetRecords = {};
    targetRecords["ddsm_measuretemplate"] = currentWindow.$mtId;

    var projId = currentWindow.Xrm.Page.data.entity.getId();
    if (projId) {
        targetRecords["ddsm_project"] = projId.slice(1, -1);
    }

    var parentAccControl = currentWindow.Xrm.Page.getAttribute("ddsm_accountid");
    if (parentAccControl) {
        let values = parentAccControl.getValue();
        if (values && values.length > 0) {
            targetRecords["account"] = values[0].id.slice(1, -1);
        }
    }

    var parentSiteControl = currentWindow.Xrm.Page.getAttribute("ddsm_parentsite");
    if (parentSiteControl) {
        let values = parentSiteControl.getValue();
        if (values && values.length > 0) {
            targetRecords["ddsm_site"] = values[0].id.slice(1, -1);
        }
    }

   // console.log("Get latest MT data: " + currentWindow.JSON.stringify(targetRecords));
    return targetRecords;
}

function showSpinner(isShowing) {
    GlobalJs.showSpinner(isShowing);
    // var dialog2 = $("#dialog2", currentWindow.document).data("kendoDialog");
    // currentWindow.kendo.ui.progress(dialog2.element, isShowing);
}

function renderFields() {
    var $container = $("#fields-container", currentWindow.document);
    $container.empty();
    $.each(currentWindow.$model, function (i, elem) {
        var req = elem.Required ? "*" : "";
        $container.append(
            $('<li>').append(
                $('<label>').attr({
                    for: elem.Name,
                    text: elem.Label
                }).text(elem.Label)).append(
                $('<span>').text(req).css("color", "red").css("font-size", "1.5em")).append(
                $('<input>').attr({
                    id: elem.Name,
                    name: elem.Name
                }).width('100%').prop('required', elem.Required)));
        var $control = $("#" + elem.Name, currentWindow.document);
        try {
            switch (elem.Role) {
                case "kendoDropDownList":
                    $control.kendoDropDownList({
                        dataTextField: "Name",
                        dataValueField: "Value",
                        optionLabel: "--",
                        dataSource: elem.DataSet,
                        value: elem.Value,
                        index: -1
                    });
                    break;
                case "kendoNumericTextBox":
                    var data = {};
                    if (elem.Value !== null) {
                        data.value = elem.Value;
                    }
                    data.placeholder = "--";
                    if (elem.Type == "Money") {
                        data.format = "c";
                    }
                    $control.kendoNumericTextBox(data);
                    break;
                case "kendoDatePicker":
                    $control.kendoDatePicker();
                    break;
                case "kendoMaskedTextBox":
                    $control.kendoMaskedTextBox({});
                    break;
            }
        } catch (e) {
            console.log(e);
        }
    });
    $dialogWindow = $container.parent();
    var positionTop = ($(window).height() / 2) - ($dialogWindow.height() / 2);
    $dialogWindow.position({
        top: positionTop
    });
}

function action2OK(e) {
    var validator = $("#fieldsForm").kendoValidator().data("kendoValidator");
    if (!validator.validate()) return false;
    var formData = {};

    var measureData = [{
        "TargetEntityLogicalName": "ddsm_measure",
        "TemplateEntityLogicalName": "ddsm_measuretemplate",
        "TemplatelId": currentWindow.$mtId,
        "Attributes": [{
            "LogicalName": "ddsm_parentproject",
            "Value": currentWindow.Xrm.Page.data.entity.getId().slice(1, -1),
        }]
    }];

    $.each(currentWindow.$model, function (i, elem) {
        var $value;
        if (elem.Role == "kendoDropDownList") {
            $value = $("#" + elem.Name, currentWindow.document).data(elem.Role).text();
        } else {
            $value = $("#" + elem.Name, currentWindow.document).data(elem.Role).value();
        }
        //formData.push({ FieldName: elem.FieldName, Value: $value });
        formData[elem.FieldName] = $value;
        measureData[0].Attributes.push({
            LogicalName: elem.FieldName,
            Value: $value
        });
    });


    currentWindow.DDSM.WebApi.CallAction("ddsm_DDSMCreateMeasure", "ddsm_measuretemplates", currentWindow.$mtId, {
        UserInput: currentWindow.JSON.stringify(formData),
        Data: currentWindow.JSON.stringify(measureData),
    }, function () {
        showSpinner(false);
        OnLoad();
    }, function (errorMsg) {
        showSpinner(false);
        currentWindow.Alert.show("Error", errorMsg, null, "ERROR");
    });
    showSpinner(true);
    $("#dialog2", currentWindow.document).data("kendoDialog").close();
}

var html1 = ['<div id="example">',
    '       <form id="fieldsForm">',
    '           <ul id="fields-container"></ul>',
    '       </form>',
    '   </div>'
].join("");

function customCreateShow() {
    // console.log("---in custom create Show");
    return true;
}

function create(htmlStr, htmlTag) {
    if (!htmlTag) {
        htmlTag = 'div';
    }
    var frag = GlobalJs.document.createDocumentFragment(),
        temp = GlobalJs.document.createElement(htmlTag);
    temp.innerHTML = htmlStr;
    while (temp.firstChild) {
        frag.appendChild(temp.firstChild);
    }
    return frag;
}

function injectDialogs() {
   // console.log("-injectDialogs");
    if ($('#dialog', currentWindow.document).length == 0) {
        var dialogTemplate = '<div id="dialog"></div>';
        var dialogFragment = create(dialogTemplate);
        currentWindow.document.body.insertBefore(dialogFragment, currentWindow.document.body.childNodes[0]);
    }
    if ($('#dialog2', currentWindow.document).length == 0) {
        var dialog2Template = '<div id="dialog2"></div>';
        var dialog2Fragment = create(dialog2Template);
        currentWindow.document.body.insertBefore(dialog2Fragment, currentWindow.document.body.childNodes[0]);
    }
}