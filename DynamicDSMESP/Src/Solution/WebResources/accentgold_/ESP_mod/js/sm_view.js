function ValidateMADMapping(rowData, userLCID) {
    var str = JSON.parse(rowData);

    var resultarray = [];
    if (str["ddsm_madmappingid"]) {
        resultarray = getValid();
    }
    else {
        resultarray = getNotValid();
    }
    return resultarray;
}

function getValid() {
    var imgName = "accentgold_/ESP_mod/css/green.jpeg";
    var tooltip = "Valid";
    var resultarray = [imgName, tooltip];
    return resultarray;
}

function getNotValid() {
    var imgName = "accentgold_/ESP_mod/css/red.jpeg";
    var tooltip = "Not Valid";
    var resultarray = [imgName, tooltip];
    return resultarray;
}

function ValidateQDDMapping(rowData, userLCID) {
    var str = JSON.parse(rowData);
    if (str["ddsm_qddmappingid"]) {
        resultarray = getValid();
    }
    else {
        resultarray = getNotValid();
    }
    return resultarray;
}
function ValidateQDDField(rowData, userLCID) {
    var str = JSON.parse(rowData);
    if (str["ddsm_qddfields"]) {
        resultarray = getValid();
    }
    else {
        resultarray = getNotValid();
    }
    return resultarray;
}
function ValidateMADField(rowData, userLCID) {
    var str = JSON.parse(rowData);
    if (str["ddsm_madfields"]) {
        resultarray = getValid();
    }
    else {
        resultarray = getNotValid();
    }
    return resultarray;
}

function ValidateMADDefDataMapping(rowData, userLCID){
var str = JSON.parse(rowData);
    if (str["ddsm_maddefdatamapping"]) {
        resultarray = getValid();
    }
    else {
        resultarray = getNotValid();
    }
    return resultarray;
}
