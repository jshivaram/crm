/*
Basic Query
------------------------------------------------------------------------------------------------------------------------

    Query Options

    1. Select : Specify the array of fields you want returned - should use to improve performance
    2. FormattedValues: Indicate you want formatted values also returned with the results
    3. OrderBy : Array of fields to order by
    4. Filter : Standard OData filter syntax http://bit.ly/1Q6KEBh
    5. IncludeCount : Set to true if you want the count included in your results
    6. Top : Set the number of records to return e.g. I just want 500 would be Top:500
    7. Skip : Currently not supported by CRM and will result in an error if specified
    8. SystemQuery : allows you to specify a saved query ID to run e.g. SystemQuery:guid
    9. UserQuery : allows you to specify a user saved query ID to run e.g. UserQuery:guid
    10. FetchXml : specify unencoded fetch xml , the API will encode it for you
    11. Expand : specify an array of properties to expand - see example below
    12. TrackChanges : request change tracking on query, will result in TrackChangesLink being populated in results
    13. TrackChangesLink : query is a delta only query - should not be populated on first query
 ------------------------------------------------------------------------------------------------------------------------
*/

/*
I. Basic Function
------------------------------------------------------------------------------------------------------------------------

1. JavaScript Example:

 var queryOptions = {
    Top:10
    ,FormattedValues:true
    ,Select:['field1','field2']
    ,Filter:'field eq <value>'
    ,OrderBy:['name']
 };

 GlobalJs.WebAPI.GetList("accounts",queryOptions).then(
    function (response){
        PopulateOutput(response);
     },
    function(error){});


2. Example using a CRM Web API Query Function:

    In this example we use a filter on BirthDate and are using the Between function to look for people born between the specified dates:

 var queryOptions = {
    ,Top:10
    ,FormattedValues:true
    ,Select:['field1','field2']
    ,Filter:'Microsoft.Dynamics.CRM.Between(PropertyName='birthdate',PropertyValues=["1979-01-01","1989-12-31"])'
    ,OrderBy:['name']
 };


3. Example using the Expand query option:

    In this example we use a Expand to retrieve the top 5 users of a business unit, selecting only specific properties from the user:

 var queryOptionsBU = {
    FormattedValues:true
    ,Expand:[{
        Property:'business_unit_system_users'
        ,Select:['systemuserid','fullname'],
        ,Filter:'systemuserid ne ' + UserId
        ,OrderBy:['createdon asc']
        ,Top:5
    }];

 GlobalJs.WebAPI.Get("businessunits", businessunitid, queryOptionsBU).then(
        function (result){
            console.log("retrieved business unit");
        });


Create Examples
------------------------------------------------------------------------------------------------------------------------

 4. JavaScript example of creating a task and associating it with an Opportunity:

 var newTask = {
    "subject":"Follow-up Task"
    ,"scheduledstart":"2016-05-26T06:00:00.000Z"
    ,"prioritycode":2
    ,"regardingobjectid_opportunity@odata.bind":"/opportunities(BE0D0283-5BF2-E311-945F-6C3BE5A8DD64)"
    };

 GlobalJs.WebAPI.Create("tasks", newTask).then(
    function(result) {
    },
    function(error){
        console.log(error);
    });

5. JavaScript example of creating a note with an image:

 var note = {
    subject:'test'
    ,filename:'qrcode.jpg'
    ,documentbody:'base64 image data here'
    ,mimetype:"image/jepg"
 };

 GlobalJs.WebAPI.Create("annotations",note).then(
    function(result) {
        console.log(result)
    },
    function(error){
        console.log(error)
    });


Action Examples
------------------------------------------------------------------------------------------------------------------------

6. JavaScript example of qualifying a lead

 GlobalJs.WebAPI.Create("leads", {
    subject:'test lead'
    ,firstname:'John'
    ,lastname:'Smith'
 }).then(
    function(leadID) {
        console.log('Lead Id is ' + leadID);
        var data = {
            "CreateAccount": true
            ,"CreateContact": true
            ,"CreateOpportunity": true
            ,"Status":3
        };

        GlobalJs.WebAPI.ExecuteAction('Microsoft.Dynamics.CRM.QualifyLead', data, 'leads' , leadID).then(
            function(result){
                console.log('lead qualified ' + result);
            }
            ,function(error){
                console.log(error);
            })
    }
    ,function(error){
        console.log(error)
    }
 );


 7. JavaScript example of execute action

 var data = {
    "CreateAccount": true
    ,"CreateContact": true
    ,"CreateOpportunity": true
    ,"Status":3
 };

 GlobalJs.WebAPI.ExecuteAction('Microsoft.Dynamics.CRM.QualifyLead', data, 'leads' , leadID).then(
    function(result){
        console.log('lead qualified ' + result);
    }
    ,function(error){
        console.log(error);
    });


 8. JavaScript example of execute workflow

 var actionParams = {
    'EntityId':'1BF8108F-FF95-E421-80DA-00155D28E90E'
 };

 GlobalJs.WebAPI.ExecuteAction('Microsoft.Dynamics.CRM.ExecuteWorkflow', actionParams,"workflows","3F841295-5EBE-4230-9EAB-B562E726B188").then(
        function(results){console.log(results); }
        ,function(error){console.log(error)});

 */


/*

II. Metadata Function

1. JavaScript example retrieve OptionSet attribute (Label/Value)

 GlobalJs.WebAPI.GetOptionSetUserLabels("ddsm_accounttype").then(
    function(r){
        console.dir(r);
    },
    function(e){
        console.log(e);
 })

 2. JavaScript example retrieve entities list

 GlobalJs.WebAPI.GetEntityDisplayNameList(1033).then(function(result)
 {
    console.dir(result);
 });


 2. JavaScript example retrieve Relationships list

    0:"OneToManyRelationships",1:"ManyToManyRelationships",2:"ManyToOneRelationships"

 GlobalJs.WebAPI.GetRelationshipsDisplayNameList("{667dacd8-cbb8-4906-93bc-c0db6f52e662}", 0,
     {
        Filter:'IsCustomRelationship eq true',
        Select:['MetadataId','ReferencingEntity']
    }).then(function(result)
 {
    console.dir(result);
 });

 GlobalJs.WebAPI.GetRelationshipsDisplayNameList("ddsm_indicator", 1).then(function(result)
    {
        console.dir(result);
    });



 */