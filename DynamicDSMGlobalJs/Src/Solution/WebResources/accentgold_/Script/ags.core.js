/**
 * Created by Sergey  Dergunov (sergey.dergunov@accentgold.com) on 07/01/2015.
 * Last Update by
 * Sergey Dergunov (sergey.dergunov@accentgold.com) on 09/27/2016.
 *
 * Version: 0.1.27.09016
 */
//JSON
if(typeof JSON!=="object"){JSON={}}(function(){function f(n){return n<10?"0"+n:n}if(typeof Date.prototype.toJSON!=="function"){Date.prototype.toJSON=function(key){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null};String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(key){return this.valueOf()}}var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={"\b":"\\b","\t":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},rep;function quote(string){escapable.lastIndex=0;return escapable.test(string)?'"'+string.replace(escapable,function(a){var c=meta[a];return typeof c==="string"?c:"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+string+'"'}function str(key,holder){var i,k,v,length,mind=gap,partial,value=holder[key];if(value&&typeof value==="object"&&typeof value.toJSON==="function"){value=value.toJSON(key)}if(typeof rep==="function"){value=rep.call(holder,key,value)}switch(typeof value){case"string":return quote(value);case"number":return isFinite(value)?String(value):"null";case"boolean":case"null":return String(value);case"object":if(!value){return"null"}gap+=indent;partial=[];if(Object.prototype.toString.apply(value)==="[object Array]"){length=value.length;for(i=0;i<length;i+=1){partial[i]=str(i,value)||"null"}v=partial.length===0?"[]":gap?"[\n"+gap+partial.join(",\n"+gap)+"\n"+mind+"]":"["+partial.join(",")+"]";gap=mind;return v}if(rep&&typeof rep==="object"){length=rep.length;for(i=0;i<length;i+=1){if(typeof rep[i]==="string"){k=rep[i];v=str(k,value);if(v){partial.push(quote(k)+(gap?": ":":")+v)}}}}else{for(k in value){if(Object.prototype.hasOwnProperty.call(value,k)){v=str(k,value);if(v){partial.push(quote(k)+(gap?": ":":")+v)}}}}v=partial.length===0?"{}":gap?"{\n"+gap+partial.join(",\n"+gap)+"\n"+mind+"}":"{"+partial.join(",")+"}";gap=mind;return v}}if(typeof JSON.stringify!=="function"){JSON.stringify=function(value,replacer,space){var i;gap="";indent="";if(typeof space==="number"){for(i=0;i<space;i+=1){indent+=" "}}else{if(typeof space==="string"){indent=space}}rep=replacer;if(replacer&&typeof replacer!=="function"&&(typeof replacer!=="object"||typeof replacer.length!=="number")){throw new Error("JSON.stringify")}return str("",{"":value})}}if(typeof JSON.parse!=="function"){JSON.parse=function(text,reviver){var j;function walk(holder,key){var k,v,value=holder[key];if(value&&typeof value==="object"){for(k in value){if(Object.prototype.hasOwnProperty.call(value,k)){v=walk(value,k);if(v!==undefined){value[k]=v}else{delete value[k]}}}}return reviver.call(holder,key,value)}text=String(text);cx.lastIndex=0;if(cx.test(text)){text=text.replace(cx,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})}if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,""))){j=eval("("+text+")");return typeof reviver==="function"?walk({"":j},""):j}throw new SyntaxError("JSON.parse")}}}());

//AccentGold Core CRM
(function (undefined) {

    var configDDSM = {
        emailTemplateId: "801786D8-CEEC-E411-A256-6C3BE5A8A238", //Custom Email Template (Dynamic DSM)
        financialTemplateId: "e71126c8-45f0-e411-80ef-fc15b428ddb0" //Financial Template for Auto/Manual Incentive
    };

    var trimRE = /^\s+|\s+$/g,
        digitRE = /\d+/,
        formatDigitRE = /{\d+}/g,
        serverUrlRE = /^(?:http)(?:s)?\:\/\/([^\/]+)/i,
        jsPathRE = /accentgold_\/Script\/ags.core.js/,
        curlyBracesRE = /%7b.*%7d/,
        webResourcesRE = /[^{]*/,
        endsWithSlashRE = /\/$/,
        slice = [].slice,
        toString = Object.prototype.toString,
        _odataEndpoint = "/XRMServices/2011/OrganizationData.svc",
        systemAdmin = {};
    systemAdmin.roles = [{Name: "System Administrator", Id: "528b26fa-abe1-e411-80eb-fc15b4287e18"}];
    //systemAdmin.teams = [];

    function _getHttpRequestObject()
    {
        if (window.XMLHttpRequest) { return new XMLHttpRequest(); } else { try { return new ActiveXObject("MSXML2.XMLHTTP.3.0"); } catch (ex) { return null; } }
    }

    function Namespace(name) {
        if (this instanceof arguments.callee === false) {
            return new Namespace(name);
        }
        this.name = name;
        return this;
    }
    Namespace.prototype.__namespace = true;
    Namespace.prototype.namespace = function (name) {
        return this[name] || (
                this[name] = new Namespace((this.name ? this.name + "." : "") + name)
            );
    };
    Namespace.prototype.extend = function (options) {
        for (var i in options) {
            if (options.hasOwnProperty(i)) {
                this[i] = options[i];
            }
        }
        return this;
    };
    Namespace.prototype.toString = function () { return this.name; }

    Namespace.prototype.namespace.call(this, "AccentGold").extend((function () {
        var classToTypeTable = {};

        var types = "Boolean Number String Function Array Date RegExp Object".split(" ");
        for (var i = 0; i < types.length; i++) {
            classToTypeTable["[object " + types[i] + "]"] = types[i].toLowerCase();
        }

        function type(obj) {
            return obj == null ?
                String(obj) :
            classToTypeTable[toString.call(obj)] || "object";
        }

        return {
            type: type
        };

    })());

    AccentGold.namespace("String").extend((function () {
        function format(s, args) {
            if (!s) return "";

            args = slice.call(arguments, 1);
            var matches = s.match(formatDigitRE);
            if (matches) {
                for (var i = 0; i < matches.length; i++) {
                    var index = parseInt(matches[i].match(digitRE), 10);
                    if (args.length > index) {
                        s = s.replace(matches[i], args[index]);
                    }
                }
            }

            return s;
        }

        function trim(s) {
            return s.replace(trimRE, "");
        }

        function concat() {
            var args = slice.call(arguments, 0);
            return args.join("");
        }

        function isNullOrEmpty(s) {

            if(typeof(s)=='string')s=s.trim();
            return !s;
            //return s === "" || s === undefined || s === null;
        }

        function padLeft(s, padChar, padCount) {
            var resultArray = [];

            while (padCount-- > 0) {
                resultArray.push(padChar);
            }

            resultArray.push(s);
            return resultArray.join("");
        }

        /**
         * Adds leading zeros before the input number
         * @param {number} val
         * @param {number} len
         * @returns {string}
         */
        function addLeadZeros(val,len){
            var pref = '';
            var cnt = len-(pref+val).length;
            for (var i = 0; i < cnt; i++) {
                pref+='0' ;
            }
            return pref + val;
        }

        return {
            format: format,
            trim: trim,
            concat: concat,
            isNullOrEmpty: isNullOrEmpty,
            padLeft: padLeft,
            addLeadZeros: addLeadZeros
        };
    })());

    AccentGold.namespace("Date").extend((function () {
        var oneday = 1000 * 60 * 60 * 24;
        var dateMetadata = {
            ShortDatePattern: "M/d/yyyy",
            ShortTimePattern: "h:mm tt",
            AMDesignator: "AM",
            PMDesignator: "PM"
        };

        try {
            // Try to get the date metadata info from CRM.
            dateMetadata = Sys.CultureInfo.CurrentCulture.dateTimeFormat;
        }
        catch (e) { }

        function _formatDate(d, pattern) {
            var fd = d.toString();

            fd = pattern.replace(/yyyy/g, d.getFullYear());
            fd = fd.replace(/yy/g, (d.getFullYear() + "").substring(2));

            var month = d.getMonth();
            fd = fd.replace(/MM/g, month + 1 < 10 ? "0" + (month + 1) : month + 1);
            fd = fd.replace(/(\\)?M/g, function ($0, $1) { return $1 ? $0 : month + 1; });

            var day = d.getDate();
            fd = fd.replace(/dd/g, day < 10 ? "0" + day : day);
            fd = fd.replace(/(\\)?d/g, function ($0, $1) { return $1 ? $0 : day; });

            var militaryHour = d.getHours();
            var shortHour = militaryHour > 12 ? militaryHour - 12 : militaryHour;

            fd = fd.replace(/HH/g, militaryHour < 10 ? "0" + militaryHour : militaryHour);
            fd = fd.replace(/(\\)?H/g, function ($0, $1) { return $1 ? $0 : militaryHour; });

            fd = fd.replace(/hh/g, militaryHour < 10 ? "0" + shortHour : shortHour);
            fd = fd.replace(/(\\)?h/g, function ($0, $1) { return $1 ? $0 : shortHour; });

            var minutes = d.getMinutes();
            fd = fd.replace(/mm/g, minutes < 10 ? "0" + minutes : minutes);
            fd = fd.replace(/(\\)?m/g, function ($0, $1) { return $1 ? $0 : minutes; });

            var seconds = d.getSeconds();
            fd = fd.replace(/ss/g, seconds < 10 ? "0" + seconds : seconds);
            fd = fd.replace(/(\\)?s/g, function ($0, $1) { return $1 ? $0 : seconds; });

            fd = fd.replace(/fff/g, d.getMilliseconds());

            fd = fd.replace(/tt/g, d.getHours() > 12 || d.getHours() == 0 ? dateMetadata.PMDesignator : dateMetadata.AMDesignator);

            return fd.replace(/\\/g, "");
        }

        function parse(d) {
            var timestamp = Date.parse(d),
                minutesOffset = 0,
                struct;

            if (isNaN(timestamp) && (struct = /(\d{4})-?(\d{2})-?(\d{2})(?:[T ](\d{2}):?(\d{2}):?(\d{2})?(?:\.(\d{3,}))?(?:(Z)|([+\-])(\d{2})(?::?(\d{2}))?))/.exec(d))) {
                if (struct[8] !== 'Z') {
                    minutesOffset = +struct[10] * 60 + (+struct[11]);

                    if (struct[9] === '+') {
                        minutesOffset = 0 - minutesOffset;
                    }
                }

                timestamp = Date.UTC(+struct[1], +struct[2] - 1, +struct[3], +struct[4], +struct[5] + minutesOffset, +struct[6], +struct[7].substr(0, 3));
            }

            return timestamp;
        }

        function toISOString(d) {
            var month = d.getUTCMonth() + 1;
            if (month.toString().length == 1)
                month = AccentGold.String.padLeft(month, '0', 1);
            var date = d.getUTCDate();
            if (date.toString().length == 1)
                date = AccentGold.String.padLeft(date, '0', 1);
            var hours = d.getUTCHours();
            if (hours.toString().length == 1)
                hours = AccentGold.String.padLeft(hours, '0', 1);
            var min = d.getUTCMinutes();
            if (min.toString().length == 1)
                min = AccentGold.String.padLeft(min, '0', 1);
            var sec = d.getUTCSeconds();
            if (sec.toString().length == 1)
                sec = AccentGold.String.padLeft(sec, '0', 1);
            var milli = d.getUTCMilliseconds();
            if (milli.toString().length == 1)
                milli = AccentGold.String.padLeft(milli, '0', 2);
            else if (milli.toString().length == 2)
                milli = AccentGold.String.padLeft(milli, '0', 1);

            return d.getUTCFullYear() + '-' + month + '-' + date + 'T' +
                hours + ':' + min + ':' + sec + '.' + milli + 'Z';
        }

        function convert(d) {
            switch (AccentGold.type(d)) {
                case "date":
                    return d;
                case "array":
                    return new Date(d[0], d[1], d[2]);
                case "number":
                case "string":
                    return new Date(d);
                case "object":
                    if (d.year !== undefined && d.month !== undefined && d.date !== undefined) {
                        return new Date(d.year, d.month, d.date);
                    }
            }

            return NaN;
        }

        function inRange(d, start, end) {
            return (
                isFinite(d = convert(d).valueOf()) &&
                isFinite(start = convert(start).valueOf()) &&
                isFinite(end = convert(end).valueOf()) ?
                start <= d && d <= end :
                    NaN);
        }

        function zeroTime(d) {
            d.setHours(0);
            d.setMinutes(0);
            d.setSeconds(0);
            d.setMilliseconds(0);

            return d;
        }

        function getShortDateFormat(d) {
            return _formatDate(d, dateMetadata.ShortDatePattern);
        }

        function getShortDateTimeFormat(d) {
            return _formatDate(d, dateMetadata.ShortTimePattern);
        }

        function getODataUTCDateFilter(d) {
            var monthString;
            var rawMonth = (d.getUTCMonth() + 1).toString();
            if (rawMonth.length == 1) {
                monthString = "0" + rawMonth;
            }
            else { monthString = rawMonth; }

            var dateString;
            var rawDate = d.getUTCDate().toString();
            if (rawDate.length == 1) {
                dateString = "0" + rawDate;
            }
            else { dateString = rawDate; }

            var hourString = d.getUTCHours().toString();
            if (hourString.length == 1)
                hourString = "0" + hourString;

            var minuteString = d.getUTCMinutes().toString();
            if (minuteString.length == 1)
                minuteString = "0" + minuteString;

            var secondString = d.getUTCSeconds().toString();
            if (secondString.length == 1)
                secondString = "0" + secondString;

            var DateFilter = "datetime'";
            DateFilter += d.getUTCFullYear() + "-";
            DateFilter += monthString + "-";
            DateFilter += dateString;
            DateFilter += "T" + hourString + ":";
            DateFilter += minuteString + ":";
            DateFilter += secondString + "Z'";
            return DateFilter;
        }

        function addDays(d, days) {
            return new Date((convert(d)).getTime() + parseInt(days) * oneday);
        }

        function differenceDates (d1, d2) {
            d1 = convert(d1);
            d2 = convert(d2);

            function Days () {
                return parseInt((d1.getTime() - d2.getTime()) / oneday);
            }
            function Hours () {
                return parseInt((d1.getTime() - d2.getTime()) * 24 / oneday);
            }

            return {
                Days: Days,
                Hours: Hours
            }
        }

        return {
            parse: parse,
            toISOString: toISOString,
            convert: convert,
            inRange: inRange,
            zeroTime: zeroTime,
            getShortDateFormat: getShortDateFormat,
            getShortDateTimeFormat: getShortDateTimeFormat,
            getODataUTCDateFilter: getODataUTCDateFilter,
            addDays: addDays,
            differenceDates: differenceDates
        };

    })());

    AccentGold.extend((function () {
        if (window.AGS !== undefined) {
            _AGS = AGS;
        }

        if (window.top.AGS !== undefined) {
            window.top._AGS = window.top.AGS;
        }

        this.default = configDDSM;

        /*-- Private --*/
        function _context () {
            var oContext;
            if (typeof window.GetGlobalContext != "undefined") {
                oContext = window.GetGlobalContext();
            }
            else if (typeof GetGlobalContext != "undefined") {
                oContext = GetGlobalContext();
            }
            else {
                if (typeof Xrm != "undefined") {
                    oContext = Xrm.Page.context;
                }
                else if (typeof window.parent.Xrm != "undefined") {
                    oContext = window.parent.Xrm.Page.context;
                }
                else {
                    throw new Error("Context is not available.");
                }
            }
            return oContext;
        };

        /*-- Public --*/
        var getServerUrl = function() {
            var correctHost = window.location.host,
                xrmServerUrl = "",
                globalContext,
                hasBadHost = true,
                hasBadProtocol = false;

            if (window.Xrm && Xrm.Page && Xrm.Page.context) {
                xrmServerUrl = typeof _context().getClientUrl !== "undefined" ? _context().getClientUrl() : _context().getServerUrl();
                if (xrmServerUrl.match(/\/$/)) {
                    xrmServerUrl = xrmServerUrl.substring(0, xrmServerUrl.length - 1);
                }
            }
            else {
                var windowUrl = unescape(window.location.href).toLowerCase();
                if (windowUrl.indexOf("/webresources") !== -1) {
                    var leftOfWebResource = windowUrl.split("/webresources")[0];
                    xrmServerUrl = leftOfWebResource.match(webResourcesRE)[0];

                    hasBadHost = false;
                }
            }

            if (!xrmServerUrl) {
                alert("Unable to determine server url using AccentGold.getServerUrl.  Please include ClientGlobalContext.js.aspx.");
                return;
            }

            hasBadProtocol = xrmServerUrl.indexOf(window.location.protocol) === -1;

            if (hasBadHost) {
                var badHost = xrmServerUrl.match(serverUrlRE)[1];
                xrmServerUrl = xrmServerUrl.replace(badHost, correctHost);
            }

            if (hasBadProtocol) {
                xrmServerUrl = window.location.protocol + xrmServerUrl.substring(xrmServerUrl.indexOf(":") + 1)
            }

            if (xrmServerUrl.match(endsWithSlashRE))
                xrmServerUrl = xrmServerUrl.substring(0, xrmServerUrl.length - 1);

            return xrmServerUrl;
        };

        var oDataPath = function() {
            return AccentGold.getServerUrl() + _odataEndpoint;
        };

        var getODataEndpoint = function(entitySchemaName) {
            return AccentGold.String.format("{0}{1}/{2}Set", AccentGold.getServerUrl(), _odataEndpoint, entitySchemaName);
        };

        var baseScriptUrl = (function() {
            var baseUrl = getServerUrl(),
                scripts = document.getElementsByTagName("script"),
                src = "",
                timestamp = "";

            for (var i = 0, len = scripts.length; i < len; i++) {
                if (src = scripts[i].src) {
                    src = src.toLowerCase();
                    if (src.match(jsPathRE) && (timestamp = src.match(curlyBracesRE))) {
                        baseUrl += "/" + timestamp[0];
                        break;
                    }
                }
            }

            return baseUrl + "/WebResources/accentgold_/Script/";
        })();

        var getObjectTypeCode = function(entitySchemaName) {
            if (typeof Xrm != "undefined") {
                return Xrm.Internal.getEntityCode(entitySchemaName);
            }
            else if (typeof window.parent.Xrm != "undefined") {
                return window.parent.Xrm.Internal.getEntityCode(entitySchemaName);
            }
            else {
                throw new Error("Xrm is not available.");
            }
        };

        var paramCheck = function (parameter, message) {
            if ((typeof parameter === "undefined") || parameter === null) {
                throw new Error(message);
            }
        };
        var stringParamCheck = function (parameter, message) {
            if (typeof parameter != "string") {
                throw new Error(message);
            }
        };
        var callbackParamCheck = function (callbackParameter, message) {
            if (typeof callbackParameter != "function") {
                throw new Error(message);
            }
        };
        var booleanParamCheck = function (parameter, message) {
            if (typeof parameter != "boolean") {
                throw new Error(message);
            }
        };
        var objectParamCheck = function (parameter, message) {
            if (typeof parameter != "object") {
                throw new Error(message);
            }
        };
        var guidParamCheck = function (parameter, message) {
            stringParamCheck(parameter, message);
            var guid = "[A-z0-9]{8}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{12}",
                Guid = parameter.match(guid);
            if (!!Guid) {
                return true;
            } else {
                throw new Error(message);
                return false;
            }
        };

        // END --------

        // --------
        var enableField = function(fieldName) {
            if (typeof Xrm != "undefined") {
                Xrm.Page.getControl(fieldName).setDisabled(false);
            }
            else if (typeof window.parent.Xrm != "undefined") {
                window.parent.Xrm.Page.getControl(fieldName).setDisabled(false);
            }
            else {
                throw new Error("Xrm is not available.");
            }
        };

        var disableField = function(fieldName) {
            if (typeof Xrm != "undefined") {
                Xrm.Page.getControl(fieldName).setDisabled(true);
            }
            else if (typeof window.parent.Xrm != "undefined") {
                window.parent.Xrm.Page.getControl(fieldName).setDisabled(true);
            }
            else {
                throw new Error("Xrm is not available.");
            }
        };

        var showField = function(fieldName) {
            if (typeof Xrm != "undefined") {
                Xrm.Page.getControl(fieldName).setVisible(true);
            }
            else if (typeof window.parent.Xrm != "undefined") {
                window.parent.Xrm.Page.getControl(fieldName).setVisible(true);
            }
            else {
                throw new Error("Xrm is not available.");
            }
        };

        var hideField = function(fieldName) {
            if (typeof Xrm != "undefined") {
                Xrm.Page.getControl(fieldName).setVisible(false);
            }
            else if (typeof window.parent.Xrm != "undefined") {
                window.parent.Xrm.Page.getControl(fieldName).setVisible(false);
            }
            else {
                throw new Error("Xrm is not available.");
            }
        };

        // END --------

        function noConflict() {
            if (window._AGS !== undefined) {
                AGS = _AGS;
            }

            if (window.top._AGS !== undefined) {
                window.top.AGS = window.top._AGS;
            }
        };


        return {
            getServerUrl: getServerUrl,
            oDataPath: oDataPath,
            getODataEndpoint: getODataEndpoint,
            baseScriptUrl: baseScriptUrl,
            paramCheck: paramCheck,
            stringParamCheck: stringParamCheck,
            callbackParamCheck: callbackParamCheck,
            booleanParamCheck: booleanParamCheck,
            objectParamCheck: objectParamCheck,
            guidParamCheck: guidParamCheck,
            enableField: enableField,
            disableField: disableField,
            showField: showField,
            hideField: hideField,
            getObjectTypeCode: getObjectTypeCode,
            default: this.default,
            noConflict: noConflict,
            version: "0.1.24.02016"
        };

    })());

//AccentGold.Users
    AccentGold.namespace("Users").extend((function () {
        var UserGUID = null,
            UserNAME = null;
        if (typeof Xrm != "undefined") {
            UserGUID = Xrm.Page.context.getUserId();
            UserNAME = Xrm.Page.context.getUserName();
        }
        else if (typeof window.parent.Xrm != "undefined") {
            UserGUID = window.parent.Xrm.Page.context.getUserId();
            UserNAME = window.parent.Xrm.Page.context.getUserName();
        }
        else {
            throw new Error("Xrm is not available.");
            return null;
        }

        var getUserId = function() {
            return UserGUID;
        }
        var getUserName = function() {
            return UserNAME;
        }

        return {
            getUserId: getUserId,
            getUserName: getUserName
        }
    })());

//AccentGold.Users.UserRole
    AccentGold.Users.namespace("UserRole").extend((function () {
        var UserGUID = AccentGold.Users.getUserId(),
            role = [];

        if(!!UserGUID) {
            var oDataEndpointUrl = AccentGold.getODataEndpoint('SystemUser'),
                service = _getHttpRequestObject();
            oDataEndpointUrl += "?$select=systemuserroles_association/Name,systemuserroles_association/RoleId&$expand=systemuserroles_association&$filter=SystemUserId eq guid'" + UserGUID + "'";
            service.open("GET", oDataEndpointUrl, false);
            service.setRequestHeader("X-Requested-Width", "XMLHttpRequest");
            service.setRequestHeader("Accept", "application/json, text/javascript, */*");
            service.send(null);
            var requestResults = JSON.parse(service.responseText).d;
            role = requestResults.results[0].systemuserroles_association.results;
        }
        var isAdmin = function() {
            var check = false;
            if(systemAdmin.roles.length > 0){
                for(var i = 0; i < role.length; i++) {
                    for (var j = 0; j < systemAdmin.roles.length; j++) {
                        if (role[i].Name == systemAdmin.roles[j].Name) {check = true;break;}
                    }
                }
            }
            return check;
        };
        var getName = function() {
            var roleName = [];
            for(var i = 0; i < role.length; i++) {roleName.push(role[i].Name);}
            return roleName;
        };
        var getGuid = function() {
            var roleId = [];
            for(var i = 0; i < role.length; i++) {roleId.push(role[i].RoleId);}
            return roleId;
        };
        var verify = function(GuidOrName) {
            var check = false;
            if(AccentGold.String.isNullOrEmpty(GuidOrName)){return check;}
            var guid = "[A-z0-9]{8}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{12}";
            var Guid = GuidOrName.match(guid);
            if(Guid !== null) {
                for(var i = 0; i < role.length; i++) {
                    if(role[i].RoleId == Guid) {check =  true;break;}
                }
                return check;
            } else {
                for(var i = 0; i < role.length; i++) {
                    if(role[i].Name == GuidOrName) {check =  true;break;}
                }
                return check;
            }
        };
        var getPairNameId = function(NameId) {
            if(AccentGold.String.isNullOrEmpty(NameId)){return [];}
            var guid = "[A-z0-9]{8}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{12}",
                Guid = NameId.match(guid),
                pairArr = [];

            if(Guid !== null) {
                for(var i = 0; i < role.length; i++) {
                    if(role[i].RoleId == Guid) {pairArr.push(role[i]);}
                }
                return pairArr;
            } else {
                for(var i = 0; i < role.length; i++) {
                    if(role[i].Name == NameId) {pairArr.push(role[i]);}
                }
                return pairArr;
            }
        };
        var getRole = function() {
            return role;
        };

        return {
            isAdmin: isAdmin,
            getName: getName,
            getGuid: getGuid,
            getRole: getRole,
            getPairNameId: getPairNameId,
            verify: verify
        };
    })());

//AccentGold.Users.UserTeam
    AccentGold.Users.namespace("UserTeam").extend((function () {
        var UserGUID = AccentGold.Users.getUserId(),
            team = [];

        if(!!UserGUID) {
            var oDataEndpointUrl = AccentGold.getODataEndpoint('SystemUser'),
                service = _getHttpRequestObject();
            oDataEndpointUrl += "?$select=teammembership_association/Name,teammembership_association/TeamId&$expand=teammembership_association&$filter=SystemUserId eq guid'" + UserGUID + "'";
            service.open("GET", oDataEndpointUrl, false);
            service.setRequestHeader("X-Requested-Width", "XMLHttpRequest");
            service.setRequestHeader("Accept", "application/json, text/javascript, */*");
            service.send(null);
            var requestResults = JSON.parse(service.responseText).d;
            team = requestResults.results[0].teammembership_association.results;
        }

        var getName = function() {
            var teamName = [];
            for(var i = 0; i < team.length; i++) {teamName.push(team[i].Name);}
            return teamName;
        };
        var getGuid = function() {
            var teamId = [];
            for(var i = 0; i < team.length; i++) {teamId.push(team[i].TeamId);}
            return teamId;
        };
        var verify = function(GuidOrName) {
            var check = false;
            if(AccentGold.String.isNullOrEmpty(GuidOrName)){return check;}
            var guid = "[A-z0-9]{8}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{12}";
            var Guid = GuidOrName.match(guid);
            if(Guid !== null) {
                for(var i = 0; i < team.length; i++) {
                    if(team[i].TeamId == Guid) {check =  true;break;}
                }
                return check;
            } else {
                for(var i = 0; i < team.length; i++) {
                    if(team[i].Name == GuidOrName) {check =  true;break;}
                }
                return check;
            }
        };
        var getPairNameId = function(NameId) {
            if(AccentGold.String.isNullOrEmpty(NameId)){return [];}
            var guid = "[A-z0-9]{8}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{12}",
                Guid = NameId.match(guid),
                pairArr = [];

            if(Guid !== null) {
                for(var i = 0; i < team.length; i++) {
                    if(team[i].TeamId == Guid) {pairArr.push(team[i]);}
                }
                return pairArr;
            } else {
                for(var i = 0; i < team.length; i++) {
                    if(team[i].Name == NameId) {pairArr.push(team[i]);}
                }
                return pairArr;
            }
        };
        var getTeam = function() {
            return team;
        };

        return {
            getName: getName,
            getGuid: getGuid,
            getTeam: getTeam,
            getPairNameId: getPairNameId,
            verify: verify
        };
    })());

//AccentGold.REST
    AccentGold.namespace("REST").extend((function () {
        var _converToDate = function (key, value) {
            var tmp;
            if (typeof value === 'string') {
                tmp = /Date\(([-+]?\d+)\)/.exec(value);
                if (tmp) {
                    return AccentGold.Date.zeroTime(AccentGold.Date.convert(parseInt(value.replace("/Date(", "").replace(")/", ""), 10)));
                }
            }
            return value;
        };
        var _errorHandler = function (req) {
            return new Error(
                req.status + ": " +
                req.statusText + ": " +
                JSON.parse(req.responseText).error.message.value);
        };
        var createRecord = function (object, entitySchemaName, successCallback, errorCallback, async) {
            AccentGold.objectParamCheck(object, "Requires the 'Object' parameter.");
            AccentGold.stringParamCheck(entitySchemaName, "Requires the 'EntitySchemaName' parameter is a string.");
            if(!!successCallback) {
                AccentGold.callbackParamCheck(successCallback, "Requires the 'SuccessCallback' is a function.");
            }
            if(!!errorCallback) {
                AccentGold.callbackParamCheck(errorCallback, "Requires the 'ErrorCallback' is a function.");
            }
            AccentGold.booleanParamCheck(async, "Requires the 'Async' is a boolean.");

            var req = _getHttpRequestObject();
            req.open("POST", AccentGold.getODataEndpoint(entitySchemaName), async);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");

            if (async) {
                req.onreadystatechange = function () {
                    if (this.readyState === 4 /* complete */) {
                        req.onreadystatechange = null;
                        if (req.status === 201) {
                            if(!!successCallback) {
                                successCallback(JSON.parse(req.responseText, _converToDate).d);
                            } else {return JSON.parse(req.responseText, _converToDate).d;}
                        }
                        else {
                            if(!!errorCallback) {
                                errorCallback(_errorHandler(req));
                            }
                        }
                    }
                };
                req.send(JSON.stringify(object));
            } else {
                req.send(JSON.stringify(object));
                if (req.status === 201) {
                    if(!!successCallback) {
                        successCallback(JSON.parse(req.responseText, _converToDate).d);
                    } else {return JSON.parse(req.responseText, _converToDate).d;}
                }
                else {
                    if(!!errorCallback) {
                        errorCallback(_errorHandler(req));
                    }
                }
            }

        };

        var retrieveRecord = function (id, entitySchemaName, select, expand, successCallback, errorCallback, async) {
            AccentGold.stringParamCheck(id, "Requires the 'ID' parameter is a string.");
            AccentGold.stringParamCheck(entitySchemaName, "RetrieveRecord requires the 'EntitySchemaName' parameter is a string.");
            if (!!select)
                AccentGold.stringParamCheck(select, "RetrieveRecord requires the 'SELECT' parameter is a string.");
            if (!!expand)
                AccentGold.stringParamCheck(expand, "RetrieveRecord requires the 'EXPAND' parameter is a string.");
            if(!!successCallback) {
                AccentGold.callbackParamCheck(successCallback, "RetrieveRecord requires the 'SuccessCallback' parameter is a function.");
            }
            if(!!errorCallback) {
                AccentGold.callbackParamCheck(errorCallback, "Requires the 'ErrorCallback' is a function.");
            }
            AccentGold.booleanParamCheck(async, "RetrieveRecord requires the 'Async' parameter is a boolean.");

            id = id.replace(/([{}])+/g,''); //remove brackets from id string

            var systemQueryOptions = "";

            if (!!select || !!expand) {
                systemQueryOptions = "?";
                if (!!select) {
                    var selectString = "$select=" + select;
                    if (!!expand) {
                        selectString = selectString + "," + expand;
                    }
                    systemQueryOptions += selectString;
                } else if(!!expand) {
                    selectString = "$select=" + expand;
                }
                if (!!expand) {
                    systemQueryOptions = systemQueryOptions + "&$expand=" + expand;
                }
            }

            var req = _getHttpRequestObject();
            req.open("GET", AccentGold.getODataEndpoint(entitySchemaName) + "(guid'" + id + "')" + systemQueryOptions, async);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");

            if (async) {
                req.onreadystatechange = function () {
                    if (req.readyState === 4 /* complete */) {
                        if (req.status === 200) {
                            if(!!successCallback) {
                                successCallback(JSON.parse(req.responseText, _converToDate).d);
                            } else {return JSON.parse(req.responseText, _converToDate).d;}
                        }
                        else {
                            if(!!errorCallback) {
                                errorCallback(_errorHandler(req));
                            }
                        }
                    }
                };
                req.send();
            } else {
                req.send();
                if (req.status === 200) {
                    if(!!successCallback) {
                        successCallback(JSON.parse(req.responseText, _converToDate).d);
                    } else {return JSON.parse(req.responseText, _converToDate).d;}
                }
                else {
                    if(!!errorCallback) {
                        errorCallback(_errorHandler(req));
                    }
                }
            }
        };
        var updateRecord = function (id, object, entitySchemaName, successCallback, errorCallback, async) {
            AccentGold.objectParamCheck(object, "Requires the 'Object' parameter.");
            AccentGold.stringParamCheck(id, "Requires the 'ID' parameter is a string.");
            AccentGold.stringParamCheck(entitySchemaName, "Requires the 'EntitySchemaName' parameter is a string.");
            if(!!successCallback) {
                AccentGold.callbackParamCheck(successCallback, "Requires the 'SuccessCallback' parameter is a function.");
            }
            if(!!errorCallback) {
                AccentGold.callbackParamCheck(errorCallback, "Requires the 'ErrorCallback' is a function.");
            }
            AccentGold.booleanParamCheck(async, "Requires the 'Async' parameter is a boolean.");

            id = id.replace(/([{}])+/g,''); //remove brackets from id string

            var req = _getHttpRequestObject();
            req.open("POST", AccentGold.getODataEndpoint(entitySchemaName) + "(guid'" + id + "')", async);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("X-HTTP-Method", "MERGE");
            if (async) {
                req.onreadystatechange = function () {
                    if (req.readyState === 4 /* complete */) {
                        if (req.status === 204 || req.status === 1223) {
                            if(!!successCallback) {
                                successCallback();
                            }
                        }
                        else {
                            if(!!errorCallback) {
                                errorCallback(_errorHandler(req));
                            }
                        }
                    }
                };
                req.send(JSON.stringify(object));
            } else {
                req.send(JSON.stringify(object));
                if (req.status === 204 || req.status === 1223) {
                    if(!!successCallback) {
                        successCallback();
                    }
                }
                else {
                    if(!!errorCallback) {
                        errorCallback(_errorHandler(req));
                    }
                }
            }
        };
        var deleteRecord = function (id, entitySchemaName, successCallback, errorCallback, async) {
            AccentGold.stringParamCheck(id, "Requires the 'ID' parameter is a string.");
            AccentGold.stringParamCheck(entitySchemaName, "Requires the 'EntitySchemaName' parameter is a string.");
            AccentGold.callbackParamCheck(successCallback, "Requires the 'SuccessCallback' parameter is a function.");
            if(!!errorCallback) {
                AccentGold.callbackParamCheck(errorCallback, "Requires the 'ErrorCallback' is a function.");
            }
            AccentGold.booleanParamCheck(async, "Requires the 'Async' parameter is a boolean.");

            id = id.replace(/([{}])+/g,''); //remove brackets from id string

            var req = _getHttpRequestObject();
            req.open("POST", AccentGold.getODataEndpoint(entitySchemaName) + "(guid'" + id + "')", async);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("X-HTTP-Method", "DELETE");

            if (async) {
                req.onreadystatechange = function () {
                    if (req.readyState === 4 /* complete */) {
                        if (req.status === 204 || req.status === 1223) {
                            successCallback();
                        }
                        else {
                            if(!!errorCallback) {
                                errorCallback(_errorHandler(req));
                            }
                        }
                    }
                };
                req.send();
            } else {
                req.send();
                if (req.status === 204 || req.status === 1223) {
                    successCallback();
                }
                else {
                    if(!!errorCallback) {
                        errorCallback(_errorHandler(req));
                    }
                }
            }
        };
        var retrieveMultipleRecords = function (entitySchemaName, options, successCallback, errorCallback, onComplete, async, returnedAllRecords) {

            var returnedAllRecords = returnedAllRecords;
            if (!!returnedAllRecords) {}else{returnedAllRecords = [];}
            AccentGold.stringParamCheck(entitySchemaName, "Requires the 'EntitySchemaName' parameter is a string.");
            if (!!options)
                AccentGold.stringParamCheck(options, "Requires the 'Options' parameter is a string.");
            if(!!successCallback) {
                AccentGold.callbackParamCheck(successCallback, "Requires the 'SuccessCallback' parameter is a function.");
            }
            if(!!errorCallback) {
                AccentGold.callbackParamCheck(errorCallback, "Requires the 'ErrorCallback' is a function.");
            }
            if(!!onComplete) {
                AccentGold.callbackParamCheck(onComplete, "Requires the 'OnComplete' parameter is a function.");
            }
            AccentGold.booleanParamCheck(async, "Requires the 'Async' parameter is a boolean.");

            var optionsString = '';
            if (!!options) {
                if (options.charAt(0) != "?") {
                    optionsString = "?" + options;
                }
                else {
                    optionsString = options;
                }
            }

            var req = _getHttpRequestObject();
            req.open("GET", AccentGold.getODataEndpoint(entitySchemaName) + optionsString, async);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");

            if (async) {
                req.onreadystatechange = function () {
                    if (req.readyState === 4 /* complete */) {
                        if (req.status === 200) {
                            var returned = JSON.parse(req.responseText, _converToDate).d;
                            returnedAllRecords = returnedAllRecords.concat(returned.results);
                            if(!!successCallback) {
                                successCallback(returned.results);
                            }
                            if (returned.__next == null) {
                                if(!!onComplete) {
                                    onComplete(returnedAllRecords);
                                } //else {return returnedAllRecords;}
                            } else {
                                var queryOptions = returned.__next.substring((AccentGold.getODataEndpoint(entitySchemaName)).length);
                                retrieveMultipleRecords(entitySchemaName, queryOptions, successCallback, errorCallback, onComplete, async, returnedAllRecords);
                            }
                        }
                        else {
                            if(!!errorCallback) {
                                errorCallback(_errorHandler(req));
                            }
                        }
                    }
                };
                req.send();
            } else {
                req.send();
                if (req.status === 200) {
                    var returned = JSON.parse(req.responseText, _converToDate).d;
                    returnedAllRecords = returnedAllRecords.concat(returned.results);
                    if(!!successCallback) {
                        successCallback(returned.results);
                    }
                    if (returned.__next == null) {
                        if(!!onComplete) {
                            onComplete(returnedAllRecords);
                        } //else {return returnedAllRecords;}
                    } else {
                        var queryOptions = returned.__next.substring((AccentGold.getODataEndpoint(entitySchemaName)).length);
                        retrieveMultipleRecords(entitySchemaName, queryOptions, successCallback, errorCallback, onComplete, async, returnedAllRecords);
                    }
                }
                else {
                    if(!!errorCallback) {
                        errorCallback(_errorHandler(req));
                    }
                }
            }
        };

        return {
            createRecord: createRecord,
            retrieveRecord: retrieveRecord,
            updateRecord: updateRecord,
            deleteRecord: deleteRecord,
            retrieveMultipleRecords: retrieveMultipleRecords
        };
    })());

    AccentGold.namespace("Soap").extend((function () {

        var xrmEntityCollection = function (items) {
            this.value = items;
            this.type = 'EntityCollection';
        };

        var xrmEntityReference = function (gId, sLogicalName, sName) {
            this.id = gId;
            this.logicalName = sLogicalName;
            this.name = sName;
            this.type = 'EntityReference';
        };
        var businessEntity = function (logicalName, id) {
            this.id = (!id) ? "00000000-0000-0000-0000-000000000000" : id;
            this.logicalName = logicalName;
            this.attributes = new Object();
        };

        return {
            xrmEntityCollection: xrmEntityCollection,
            xrmEntityReference: xrmEntityReference,
            businessEntity: businessEntity
        }
    })());

    AccentGold.namespace("Email").extend((function () {
        var EmailTemplateId = AccentGold.default.emailTemplateId;

        function _createXmlDocument(signatureXmlStr) {
            var parseXml;
            if (window.DOMParser) {
                parseXml = function (xmlStr) {
                    return (new window.DOMParser()).parseFromString(xmlStr, "text/xml");
                };
            }
            else if (typeof window.ActiveXObject != "undefined" && new window.ActiveXObject("Microsoft.XMLDOM")) {
                parseXml = function (xmlStr) {
                    var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = "false";
                    xmlDoc.loadXML(xmlStr);

                    return xmlDoc;
                };
            }
            else {
                parseXml = function () { return null; }
            }
            var xml = parseXml(signatureXmlStr);
            if (xml) {
                return xml;
            }
        }

        var getEmailTemplate = function (TemplateId, objectId, objectTypeCode) {
            var command = new RemoteCommand("EmailTemplateService", "GetInstantiatedEmailTemplate");
            command.SetParameter("templateId", TemplateId);
            command.SetParameter("objectId", objectId);
            command.SetParameter("objectTypeCode", objectTypeCode);
            var result = command.Execute();

            if (result.Success) {
                if (typeof (result.ReturnValue) == "string") {
                    var oXml = _createXmlDocument(result.ReturnValue);
                    return oXml.lastChild.lastElementChild.textContent;
                }
            }
        };

        var createEmailBody = function (TemplateId, headerEmail, toUserName, fromUserName, textEmail, objectId, entityName){
            var objectTypeCode = null, BodyEmail;
            if (typeof Xrm != "undefined") {
                objectTypeCode = Xrm.Internal.getEntityCode(entityName);
            }
            else if (typeof window.parent.Xrm != "undefined") {
                objectTypeCode = window.parent.Xrm.Internal.getEntityCode(entityName);
            }
            else {
                throw new Error("Xrm is not available.");
                return null;
            }
            if(!!TemplateId) {TemplateId = EmailTemplateId;}

            if(!!objectId && !!objectTypeCode) {
                BodyEmail = getEmailTemplate(TemplateId, objectId, objectTypeCode);
                BodyEmail = BodyEmail.replace("{headerEmail}", headerEmail);
            } else {
                BodyEmail = "Hi {toUser},\n\n"
                    +"{textEmail}\n\n"
                    +"Best Regards,\n"
                    +"{fromUser}";
            }
            BodyEmail = BodyEmail.replace("{toUser}", toUserName);
            BodyEmail = BodyEmail.replace("{fromUser}", fromUserName);
            BodyEmail = BodyEmail.replace("{textEmail}", textEmail);

            return BodyEmail;
        };

        var sendEmailRequest = function (emailId) {
            var URL = AccentGold.getServerUrl() +"/XRMServices/2011/Organization.svc/web";
            var requestMain = ""
            requestMain += "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">";
            requestMain += "  <s:Body>";
            requestMain += "    <Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">";
            requestMain += "      <request i:type=\"b:SendEmailRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\" xmlns:b=\"http://schemas.microsoft.com/crm/2011/Contracts\">";
            requestMain += "        <a:Parameters xmlns:c=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">";
            requestMain += "          <a:KeyValuePairOfstringanyType>";
            requestMain += "            <c:key>EmailId</c:key>";
            requestMain += "            <c:value i:type=\"d:guid\" xmlns:d=\"http://schemas.microsoft.com/2003/10/Serialization/\">" + emailId + "</c:value>";
            requestMain += "          </a:KeyValuePairOfstringanyType>";
            requestMain += "          <a:KeyValuePairOfstringanyType>";
            requestMain += "            <c:key>IssueSend</c:key>";
            requestMain += "            <c:value i:type=\"d:boolean\" xmlns:d=\"http://www.w3.org/2001/XMLSchema\">true</c:value>";
            requestMain += "          </a:KeyValuePairOfstringanyType>";
            requestMain += "          <a:KeyValuePairOfstringanyType>";
            requestMain += "            <c:key>TrackingToken</c:key>";
            requestMain += "            <c:value i:type=\"d:string\" xmlns:d=\"http://www.w3.org/2001/XMLSchema\" />";
            requestMain += "          </a:KeyValuePairOfstringanyType>";
            requestMain += "        </a:Parameters>";
            requestMain += "        <a:RequestId i:nil=\"true\" />";
            requestMain += "        <a:RequestName>SendEmail</a:RequestName>";
            requestMain += "      </request>";
            requestMain += "    </Execute>";
            requestMain += "  </s:Body>";
            requestMain += "</s:Envelope>";
            var req = new XMLHttpRequest();
            req.open("POST", URL, false);
            req.setRequestHeader("Accept", "application/xml, text/xml, */*");
            req.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
            req.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute");
            req.send(requestMain);
            //var strResponse = req.responseXML.xml;
            //alert(strResponse.toString());
        };

        return {
            getEmailTemplate: getEmailTemplate,
            createEmailBody: createEmailBody,
            sendEmailRequest: sendEmailRequest
        }
    })());

    AccentGold.namespace("Entity").extend((function () {

        /**
         * Return the list of entities
         * @returns {Array}
         */
        var getEntitiesList = function () {
                var entitiesList = [];

                var entityMetadataCollection = SDK.Metadata.RetrieveAllEntitiesOver(SDK.Metadata.EntityFilters.Entity, false, null, errorRetrieve, null, false);

                if (entityMetadataCollection != null) {

                    entityMetadataCollection.sort(function (a, b) {
                        var a1 = a.DisplayName.UserLocalizedLabel != null ? a.DisplayName.UserLocalizedLabel.Label : a.LogicalName,
                            b1 = b.DisplayName.UserLocalizedLabel != null ? b.DisplayName.UserLocalizedLabel.Label : b.LogicalName;
                        if (a1.toLowerCase() < b1.toLowerCase()) {
                            return -1
                        }
                        if (a1.toLowerCase() > b1.toLowerCase()) {
                            return 1
                        }
                        return 0;
                    });

                    for (var i = 0; i < entityMetadataCollection.length; i++) {
                        var item = entityMetadataCollection[i];
                        if(!!item.DisplayName.UserLocalizedLabel) {
                            if (item.CanCreateForms.Value && item.CanCreateAttributes.Value && item.IsCustomizable.Value && item.LogicalName != "ddsm_mappingfields") {
                                entitiesList.push(entityMetadataCollection[i]);
                            }
                        }
                    }
                }
                return entitiesList;
            },

            /**
             * Return the list of attributes
             * @param EntitySchemaName {string}
             * @returns {Array}
             */
            getAttributesList = function (EntitySchemaName) {
                var attributesList = [];
                var entityMetadata = SDK.Metadata.RetrieveEntityOver(SDK.Metadata.EntityFilters.Attributes,
                    EntitySchemaName,
                    null,
                    false,
                    null,
                    errorRetrieve, null, false);

                entityMetadata.Attributes.sort(function (a, b) {
                    var a1 = a.DisplayName.UserLocalizedLabel != null ? a.DisplayName.UserLocalizedLabel.Label : a.LogicalName,
                        b1 = b.DisplayName.UserLocalizedLabel != null ? b.DisplayName.UserLocalizedLabel.Label : b.LogicalName;
                    if (a1.toLowerCase() < b1.toLowerCase()) {
                        return -1
                    }
                    if (a1.toLowerCase() > b1.toLowerCase()) {
                        return 1
                    }
                    return 0;
                });

                for (var i = 0; i < entityMetadata.Attributes.length; i++) {
                    if(!!entityMetadata.Attributes[i].DisplayName.UserLocalizedLabel) {
                        /*
                         if(entityMetadata.Attributes[i].AttributeType == "Customer") {
                         console.log("----------------------")
                         console.log(EntitySchemaName);
                         console.dir(entityMetadata.Attributes[i]);
                         console.log("----------------------")

                         }
                         */
                        if (verifyAttributeType(entityMetadata.Attributes[i])) {
                            if (!(((entityMetadata.Attributes[i].SchemaName.toLowerCase()).substring(entityMetadata.Attributes[i].SchemaName.length - 5, entityMetadata.Attributes[i].SchemaName.length)).indexOf("_base") + 1)) {
                                attributesList.push(entityMetadata.Attributes[i]);
                            } else if (entityMetadata.Attributes[i].AttributeType != "Money") {
                                attributesList.push(entityMetadata.Attributes[i]);
                            }
                        }
                    }
                }

                return attributesList;

            },
            /**
             * Return records from ddsm_mappingfields
             * @param relationGroup  {Int/String}/ null - optionSet value; default: 962080002
             * @param target {String} - schema name of the target entity;  default: ddsm_measure
             * @param sources {Array} [String] / null - array of schema names of sources
             * @param targetIsKey {Boolean} - will set ddsm_PrimaryFieldLogicalName  as a key if TRUE, or  ddsm_SecondaryFieldLogicalName if not
             * @returns {Object} - {ent_name1: {field1,field2,field3..},ent_name2: {field1,field2,field3..}
         */
            getTargetMapping = function (relationGroup,target, sources, targetIsKey){

                targetIsKey = !!targetIsKey;
                if(!relationGroup){ relationGroup = 962080002;}
                if(!target){ target = 'ddsm_measure';}
                if(!sources){ sources = [];}
                var fields = ['ddsm_PrimaryFieldLogicalName','ddsm_SecondaryFieldLogicalName','ddsm_SecondaryEntityLogicalName'];

                var result  = {};

                var source_str = "";
                for(let i=0; i<sources.length;i++){
                    source_str += (i==0?"" :  " or ")  + AGS.String.format("ddsm_SecondaryEntityLogicalName eq '{0}'",sources[i]);
                    result[sources[i]]  = {};
                }

                var sel = AGS.String.format("?$select={0}",fields.join(','));
                sel += AGS.String.format("&$filter= ddsm_PrimaryEntityLogicalName eq '{0}' and ddsm_RelatedEntitiesGroup/Value eq {1}", target, relationGroup);
                sel += (!sources)? '' : AGS.String.format(" and ({0})",source_str);

                AGS.REST.retrieveMultipleRecords("ddsm_mappingfields", sel, function(data){

                    for (let i = 0; i < data.length; i++) {
                        let el = data[i];
                        if(targetIsKey){
                            result[el.ddsm_SecondaryEntityLogicalName][el.ddsm_PrimaryFieldLogicalName] = el.ddsm_SecondaryFieldLogicalName;
                        }else{
                            result[el.ddsm_SecondaryEntityLogicalName][el.ddsm_SecondaryFieldLogicalName] = el.ddsm_PrimaryFieldLogicalName;
                        }
                    }
                }, function(){ return result}, null, false, null) ;

                //console.dir(result);
                //debugger;

                return result;
            },
            getTargetNewMapping = function (mappingType,target, sources, targetIsKey){

                targetIsKey = !!targetIsKey;
                if(!mappingType){ mappingType = 962080000;} //Mapping Type = Entity
                if(!target){ target = 'ddsm_measure';}
                if(!sources){ sources = [];}
                var fields = ['ddsm_SourceEntity','ddsm_JSONData'];

                var result  = {};

                var source_str = "";
                for(let i=0; i<sources.length;i++){
                    source_str += (i==0?"" :  " or ")  + AGS.String.format("ddsm_SourceEntity eq '{0}'",sources[i]);
                    result[sources[i]]  = {};
                }

                var sel = AGS.String.format("?$select={0}",fields.join(','));
                sel += AGS.String.format("&$filter= substringof('{0}',ddsm_Entities) and ddsm_MappingType/Value eq {1} and ddsm_SourceEntity ne null and statecode/Value eq 0", target, mappingType);
                sel += (!source_str)? '' : AGS.String.format(" and ({0})",source_str);

                AGS.REST.retrieveMultipleRecords("ddsm_mapping", sel, function(data){

                    for (let i = 0; i < data.length; i++) {
                        let obj = JSON.parse(data[i].ddsm_JSONData);
                        if(!!obj[target])
                        {
                            let arrayObj = obj[target];
                            for(let j = 0; j < arrayObj.length; j++){
                                if(!!arrayObj[j].SourceFieldLogicalName && !!arrayObj[j].TargetFieldLogicalName)
                                {

                                    if(!result[data[i].ddsm_SourceEntity])
                                    {
                                        result[data[i].ddsm_SourceEntity] = {};
                                    }

                                    if(targetIsKey){
                                        result[data[i].ddsm_SourceEntity][arrayObj[j].TargetFieldLogicalName] = arrayObj[j].SourceFieldLogicalName;
                                    }else{
                                        result[data[i].ddsm_SourceEntity][arrayObj[j].SourceFieldLogicalName] = arrayObj[j].TargetFieldLogicalName;
                                    }
                                }
                            }
                        }
                    }
                }, function(){ return result}, null, false, null) ;

                //console.dir(result);
                //debugger;
                return result;
            },
            errorRetrieve = function(error) {
                //console.log("Error: " + error.message);
                throw new Error(error.message);
            },
            verifyAttributeType = function(item) {
                switch (item.AttributeType) {
                    case "BigInt":
                        return true;
                    case "Boolean":
                        return true;
                    case "CalendarRules":
                        return false;
                    case "Customer":
                        return true;
                    case "DateTime":
                        return true;
                    case "Decimal":
                        return true;
                    case "Double":
                        return true;
                    case "EntityName":
                        return false;
                    case "Integer":
                        return true;
                    case "Lookup":
                        return true;
                    case "ManagedProperty":
                        return false;
                    case "Memo":
                        return true;
                    case "Money":
                        return true;
                    case "Owner":
                        return true;
                    case "PartyList":
                        return false;
                    case "Picklist":
                        return true;
                    case "State":
                        return true;
                    case "Status":
                        return true;
                    case "String":
                        return true;
                    case "Uniqueidentifier":
                        return false;
                    case "Virtual":
                        return false;
                    default:
                        return false;
                }
            };

        return {
            getEntitiesList: getEntitiesList,
            getAttributesList: getAttributesList,
            getTargetMapping: getTargetMapping,
            getTargetNewMapping: getTargetNewMapping
        }

    })());

//AccentGold.Form
    AccentGold.namespace("Form").extend((function () {
        /**
         * Wrapper for Xrm.Page.getAttribute(field).setValue(value)
         * @param field {string}
         * @param value {string} | {object} | null; default: null
         * @returns {boolean}
         */
        var setValue = function (field, value) {
                var val = value;


                if(!field){
                    return false;
                }
                field = String(field).toLowerCase();
                if(!Xrm.Page.getAttribute(field)){
                    return false;
                }

                /**
                 * Clear field if value is empty
                 */
                if(!val){
                    Xrm.Page.getAttribute(field).setValue(null);
                    return true;
                }

                /**
                 * OPTIIONSET
                 * Acceptable values:
                 * {object} : { Value }
                 * {number} : <96208000>
                 *
                 */
                if(Xrm.Page.getAttribute(field).getAttributeType()=='optionset'){
                    val =  null;
                    if((value instanceof Object) && value.hasOwnProperty('Value')){
                        val =  value.Value;
                    }
                    if(+value > 0){
                        val = value;
                    }
                    /**
                     * LOOKUP
                     * {array[object]} | object
                     * Acceptable object as input value :
                     * {
                 * id | Id,
                 * name | Name,
                 * typename | TypeName | Typename | SchemaName | schemaname
                 * }
                     * Example:
                     * {Id,name,SchemaName}
                     */
                }else if(Xrm.Page.getAttribute(field).getAttributeType()=='lookup'){

                    if(!Array.isArray(value)){
                        value = [value];
                    }
                    val = [];

                    for(let i=0;i<value.length;i++){
                        let el = value[i];
                        if(! (el instanceof Object)){
                            continue;
                        }

                        let hasLogicalName = el.hasOwnProperty("LogicalName") || el.hasOwnProperty("logicalname") || el.hasOwnProperty("typename") || el.hasOwnProperty("Typename") || el.hasOwnProperty("TypeName") || el.hasOwnProperty("SchemaName") || el.hasOwnProperty("schemaname");
                        let hasId  = el.hasOwnProperty("id") || el.hasOwnProperty("Id");
                        let hasName = el.hasOwnProperty("name") || el.hasOwnProperty("name");

                        if(!(hasLogicalName || hasId || hasName)){
                            continue;
                        }
                        let obj ={
                            id: el.Id || el.id,
                            name: el.Name || el.name,
                            typename: el.LogicalName || el.logicalname || el.typename || el.TypeName || el.Typename || el.SchemaName || el.schemaname
                        };
                        val.push(obj);
                    }

                    if(!val.length){
                        console.log(AGS.String.format("Wrong format for field {0}. Value not set",field));
                        return false;
                    }
                }

                Xrm.Page.getAttribute(field).setValue(val);
                return true;
            },
            /**
             * Wrapper for Xrm.Page.getAttribute(field).getValue(value)
             * @param field {string}
             * @returns {object} / {null} - field value
             */
            getValue = function (field) {
                if(!field){
                    return null;
                }
                field = String(field).toLowerCase();
                if(!Xrm.Page.getAttribute(field)){
                    return null;
                }

                return Xrm.Page.getAttribute(field).getValue();
            },
            /**
             * Wrapper for Xrm.Page.getAttribute(field).setSubmitMode(value)
             * @param fields {Array[string]} | {string} - one attr name or array of attr names
             * @param mode {string}
             * @returns {boolean} True if there is no bad parameter
             */
            setSubmitMode = function(fields,mode){
                if(!fields){
                    return false;
                }
                var modes = ['always','dirty','none'];
                if(modes.indexOf(mode)<0){
                    mode = modes[0];
                }
                if(!Array.isArray(fields)){
                    fields = [fields];
                }
                for (let i = 0; i < fields.length; i++) {
                    let el = fields[i];
                    if(!el || typeof(el)!='string' || !Xrm.Page.getAttribute(el.toLowerCase())){
                        continue;
                    }
                    Xrm.Page.getAttribute(el.toLowerCase()).setSubmitMode(mode);
                }
                return true;
            },
            _doesControlHaveAttribute = function(control) {
                var controlType = control.getControlType();
                return controlType != "iframe" && controlType != "webresource" && controlType != "subgrid";
            },
            disableFormFields = function(onOff) {
                Xrm.Page.ui.controls.forEach(function (control, index) {
                    if (_doesControlHaveAttribute(control)) {
                        control.setDisabled(onOff);
                    }
                });
            };
        return{
            setValue: setValue,
            getValue: getValue,
            setSubmitMode: setSubmitMode,
            disableFormFields: disableFormFields
        }
    })());

    AccentGold.Form.namespace("SubGrid").extend((function () {
        var _getConditionsFXML = function (cond) {
                debugger;
                if(cond[1]=='null' || cond[1]=='not-null') {
                    return AGS.String.format('<condition attribute="{0}" operator="{1}"/>', cond[0], cond[1]);
                }
                if (cond.length < 3) {
                    return "";
                }

                if(cond[1]=='in' || cond[1]=='not-in') {
                    let valStr = "";
                    let valList = cond[2].split(',');
                    for (let i = 0; i < valList.length; i++) {
                        valStr += AGS.String.format("<value>{0}</value>",valList[i]);
                    }
                    return AGS.String.format('<condition attribute="{0}" operator="{1}"> {2} </condition>', cond[0], cond[1], valStr);
                }
                return AGS.String.format('<condition attribute="{0}" operator="{1}" value="{2}" />', cond[0], cond[1], cond[2]);
            },
            getFiltersFXML = function (filters) {
                if(!filters) return;
                var result = "";
                if (!Array.isArray(filters)) {
                    filters = [filters];
                }
                filters.forEach(function (item) {
                    if(!!item) {

                        if (!Array.isArray(item)) {
                            item = [item];
                        }
                        conditions = item[0];
                        filter_type  = item[1];

                        if (!Array.isArray(conditions)) {
                            conditions = [conditions];
                        }
                        result += AGS.String.format('<filter type="{0}" >', (!!filter_type ? filter_type : 'and'));
                        conditions.forEach(function (cond) {
                            result += _getConditionsFXML(cond);
                        });
                        result += '</filter>';
                    }
                });
                return result;
            };

        return{
            getFiltersFXML: getFiltersFXML
        };

///**
// * EXAMPLE OF USAGE
// */
//        var filters = [
//Filter 1
//            [
//Conditions
//                [
//                    ['ddsm_model_number', 'eq', 'MN-00001']
//                    ,['statecode', 'eq', '0']
//                ]
//Filter Type
//                , 'and'
//            ],
//
//Filter 2
//            [
//                [
//                    ['ddsm_model_number', 'eq', 'MN-00001']
//                    ,['statecode', 'eq', '0']
//                ]
//                , 'and'
//            ]
//
//        ];
//
//Conditions
//        ['<field_name1>','eq','<value_1>'],
//        ['<field_name2>','ne','<value_2>'],
//        ['<field_name3>','gt','<value_3>'],
//        ['<field_name4>','lt','<value_4>'],
//        ['<field_name5>','like','<value_5>'],
//        ['<field_name6>','not-like','<value_6>'],
//        ['<field_name7>','in','<value_7>'],
//        ['<field_name8>','not-in','<value_8>'],
//        ['<field_name9>','null'], //Just 2 items !!!
//        ['<field_name10>','not-null'], //Just 2 items !!!
//
//Get Filters Fetch XML
//        var FiltersFXML = getFiltersFXML(filters);

    })());

    AccentGold.Form.SubGrid.namespace("LookUp").extend((function () {

        var _getCustomLookUp =  function(params, lookupStyle, callbackForForm) {

                var relName = params.gridControl.GetParameter("relName"),
                    roleOrd = params.gridControl.GetParameter("roleOrd"),
                    viewId = "{00000000-0000-0000-0000-000000000001}";

                var customView = {
                    fetchXml: params.fetchXml,
                    id: viewId,
                    layoutXml: params.layoutXml,
                    name: "Filtered Lookup View",
                    recordType: params.gridTypeCode,
                    Type: 0
                };

                var parentObj = params.crmWindow.GetParentObject(null, 0);
                var parameters = [params.gridTypeCode, "", relName, roleOrd, parentObj];

//    var callbackRef = params.crmWindow.Mscrm.Utilities.createCallbackFunctionObject("locAssocObjAction", params.crmWindow, parameters, false);
                var callbackRef = {
                    callback: function (lookupItems) {
                        if (lookupItems && lookupItems.items.length > 0) {
                            var parent = params.crmWindow.GetParentObject(),
                                parentId = parent.id,
                                parentTypeCode = parent.objectTypeCode;

                            params.crmWindow.AssociateObjects(parentTypeCode, parentId, params.gridTypeCode, lookupItems, IsNull(roleOrd) || roleOrd == 2, "", relName);

                            //Custome callback for form
                            //if(callbackForForm instanceof Function) callbackForForm(lookupItems.items);
                        }
                    }
                };
                params.crmWindow.LookupObjectsWithCallback(callbackRef, null, lookupStyle, params.gridTypeCode, 0, null, "", null, null, null, null, null, null, viewId, [customView]);

            },

// lookupStyle = 'single' or 'multi'
//
            addExistingFromSubGrid = function(gridName, filters, lookupStyle, additionalAttributes, callbackForForm) {

                if(!gridName) return;

                var callbackForForm = (callbackForForm instanceof Function) ? callbackForForm : null;

                var crmWindow = Xrm.Internal.isTurboForm() ? parent.window : window;
                var gridControl = crmWindow.document.getElementById(gridName).control;
                var entityName = gridControl.getEntityName();

                var attributesList = "", layoutList = "";
                if(!!additionalAttributes) {
                    if (!Array.isArray(additionalAttributes)) {
                        additionalAttributes = [additionalAttributes];
                    }
                    additionalAttributes.forEach(function (item) {
                        layoutList += "<cell name='" + item + "'  width='100' />";
                        attributesList += "<attribute name='" + item + "' />";
                    });

                }

                var fetchXml = "<fetch version='1.0' " +
                    "output-format='xml-platform' " +
                    "mapping='logical'>" +
                    "<entity name='" + entityName + "'>" +
                    ((!!(entityName.indexOf("ddsm_") + 1))? "<attribute name='ddsm_name' />" : "<attribute name='name' />") +
                    attributesList +

                    ((!!(entityName.indexOf("ddsm_") + 1))? "<order attribute='ddsm_name' descending='false' />" : "<order attribute='name' descending='false' />") +

                    ((!!filters) ? AccentGold.Form.SubGrid.getFiltersFXML(filters) : "") +

                    "</entity>" +
                    "</fetch>";

                var layoutXml = "<grid name='resultset'  object='1'  jump='name'  select='1' icon='1'  preview='1'>" +
                    "<row name='result' id='" + (entityName + "id") + "'>" +
                    ((!!(entityName.indexOf("ddsm_") + 1))? "<cell name='ddsm_name'  width='150' />" : "<cell name='name'  width='150' />") +
                    layoutList +
                    "</row>" +
                    "</grid>";

                _getCustomLookUp({
                        gridTypeCode: crmWindow.Xrm.Internal.getEntityCode(entityName),
                        gridControl: gridControl,
                        crmWindow: crmWindow,
                        fetchXml: fetchXml,

                        layoutXml: layoutXml

                    },
                    lookupStyle, callbackForForm);
            };

        return{
            addExistingFromSubGrid: addExistingFromSubGrid
        };

///**
// * EXAMPLE OF USAGE
// */
//        var filters = [
//Filter 1
//            [
//Conditions
//                [
//                    ['ddsm_model_number', 'eq', 'MN-00001']
//                    ,['statecode', 'eq', '0']
//                ]
//Filter Type
//                , 'and'
//            ],
//
//Filter 2
//            [
//                [
//                    ['ddsm_model_number', 'eq', 'MN-00001']
//                    ,['statecode', 'eq', '0']
//                ]
//                , 'and'
//            ]
//
//        ];
//
//Conditions
//        ['<field_name1>','eq','<value_1>'],
//        ['<field_name2>','ne','<value_2>'],
//        ['<field_name3>','gt','<value_3>'],
//        ['<field_name4>','lt','<value_4>'],
//        ['<field_name5>','like','<value_5>'],
//        ['<field_name6>','not-like','<value_6>'],
//        ['<field_name7>','in','<value_7>'],
//        ['<field_name8>','not-in','<value_8>'],
//        ['<field_name9>','null'], //Just 2 items !!!
//        ['<field_name10>','not-null'], //Just 2 items !!!
//
//Get Filters Fetch XML
//        addExistingFromSubGrid("CertLib_view", filters, "multi", ["ddsm_modelnumber"], function(items){console.dir(items);});


    })());

    //region Initialize the namespace

    window.top.AccentGold = AccentGold;
    window.top.AGS = AccentGold;
    AGS = AccentGold;
    AccentGold = AccentGold;

    //endregion
})();

