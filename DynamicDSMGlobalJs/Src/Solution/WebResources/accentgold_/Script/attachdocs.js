;window.Modernizr=function(a,b,c){function u(a){i.cssText=a}function v(a,b){return u(prefixes.join(a+";")+(b||""))}function w(a,b){return typeof a===b}function x(a,b){return!!~(""+a).indexOf(b)}function y(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:w(f,"function")?f.bind(d||b):f}return!1}var d="2.7.0",e={},f=b.documentElement,g="modernizr",h=b.createElement(g),i=h.style,j,k={}.toString,l={},m={},n={},o=[],p=o.slice,q,r=function(){function d(d,e){e=e||b.createElement(a[d]||"div"),d="on"+d;var f=d in e;return f||(e.setAttribute||(e=b.createElement("div")),e.setAttribute&&e.removeAttribute&&(e.setAttribute(d,""),f=w(e[d],"function"),w(e[d],"undefined")||(e[d]=c),e.removeAttribute(d))),e=null,f}var a={select:"input",change:"input",submit:"form",reset:"form",error:"img",load:"img",abort:"img"};return d}(),s={}.hasOwnProperty,t;!w(s,"undefined")&&!w(s.call,"undefined")?t=function(a,b){return s.call(a,b)}:t=function(a,b){return b in a&&w(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=p.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(p.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(p.call(arguments)))};return e}),l.draganddrop=function(){var a=b.createElement("div");return"draggable"in a||"ondragstart"in a&&"ondrop"in a};for(var z in l)t(l,z)&&(q=z.toLowerCase(),e[q]=l[z](),o.push((e[q]?"":"no-")+q));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)t(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof enableClasses!="undefined"&&enableClasses&&(f.className+=" "+(b?"":"no-")+a),e[a]=b}return e},u(""),h=j=null,e._version=d,e.hasEvent=r,e}(this,this.document),Modernizr.addTest("filereader",function(){return!!(window.File&&window.FileList&&window.FileReader)});

"use strict";
if (typeof (GlobalJs.DocsJs) == "undefined") {
    GlobalJs.DocsJs = {__namespace: true};
    (function () {
        var _this= this;
        this._document = GlobalJs.crmContentPanel._document;
        this._window = GlobalJs.crmContentPanel._window;
        this._Xrm = GlobalJs.crmContentPanel._Xrm;

        this.Init = function(){

            _this._document = GlobalJs.crmContentPanel._document;
            _this._window = GlobalJs.crmContentPanel._window;
            _this._Xrm = GlobalJs.crmContentPanel._Xrm || null;

            _this.Attachments._removeDragAndDrop();

            if(!!_this._Xrm && !!_this._Xrm.Page && !!_this._Xrm.Page.data && !!_this._Xrm.Page.data.entity && !!_this._Xrm.Page.ui)
            {
                if (_this._Xrm.Page.ui.getFormType() == 1) {
                    console.log("GlobalJs.DocsJs not initialized.");
                    return;
                }
            } else {
                console.log("GlobalJs.DocsJs not initialized.");
                return;
            }

            console.log("GlobalJs.DocsJs Initialized.");

            _loadSettings(function(){
                _this.Attachments.viewName = null;
                _this.Attachments._entityRelation = null;

                _this.Recuired.viewName = null;
                _this.Recuired._lookupFieldName = null;
                _this.Recuired._isRelationship = false;
                _this.Recuired._rDocCount = 0;
                $(_this._document).ready(function() {
                    //Attachments
                    _isManyToManyRelationship("ddsm_attachmentdocument", null, function(isRelation, schemaName){

                        if(isRelation)
                        {
                            _this.Attachments._entityRelation = schemaName;

                            var viewName = _getViewName(_this.Attachments.Settings);
                            if(!!viewName){
                                _this.Attachments.viewName = viewName;
                                _addImageButton(viewName);
                            }
                            _this.Attachments.Init(viewName, _this);
                        }
                    });

                    //Required
                    _isManyToOneRelationship("ddsm_requireddocument", function(r){
                        var viewName = _getViewName(_this.Recuired.Settings);
                        if(!!viewName){
                            _this.Recuired.viewName = viewName;
                            _addImageButton(viewName);
                        }
                        var _queryCountOptions = {
                            Filter: '((_' + r.ReferencingAttribute + '_value eq ' + _this._Xrm.Page.data.entity.getId().replace(/[{}]/g,'') +') and (statecode eq 0) and (ddsm_canceled ne true) and (ddsm_uploaded ne true))'
                            ,Select:["ddsm_requireddocumentid"]
                        };
                        GlobalJs.WebAPI.GetList("ddsm_requireddocuments", _queryCountOptions).then(function(r2){
                            _this.Recuired._rDocCount = r2.List.length;
                        });

                        _this.Recuired.Init(viewName, r.ReferencingAttribute, true, _this);
                    });

                    //Attachments to Required
                    _isManyToManyRelationship("ddsm_attachmentdocument", "ddsm_requireddocument", function(isRelation, schemaName){
                        if(isRelation)
                        {
                            _this.Recuired._attachRelation = schemaName;
                        } else {
                            _this.Recuired._attachRelation = null;
                        }
                    });

                });

            });

        };

        function _getViewName(_settings) {
            var viewName = null;
            /*
             for(var i = 0; i < (DocumentsJs.Attachments.Settings["ViewName"]).length; i++) {
             if(!!Xrm.Page.getControl(DocumentsJs.Attachments.Settings["ViewName"][i])) {
             viewName = DocumentsJs.Attachments.Settings["ViewName"][i];
             break;
             }
             }
             if(!viewName && !!Xrm.Page.getControl( DocumentsJs.Attachments.Settings["ObjectName"] + "_View")) {
             viewName = DocumentsJs.Attachments.Settings["ObjectName"] + "_View";
             }
             */

            for(var i = 0; i < (_settings["ViewName"]).length; i++) {
                if(!!_this._document.getElementById(_settings["ViewName"][i])) {
                    viewName = _settings["ViewName"][i];
                    break;
                }
            }
            if(!viewName && !!_this._document.getElementById( _settings["ObjectName"] + "_View")) {
                viewName = _settings["ObjectName"] + "_View";
            }


            return viewName;
        }

        function _loadSettings(callback) {
            try{
                GlobalJs.Process.callAction("ddsm_AttachmentDocumentSettings", [/* {key: "Target",type: Process.Type.EntityReference,value: {id: Xrm.Page.data.entity.getId(),entityType: Xrm.Page.data.entity.getEntityName()}} */],
                    function(params) {
                        var rs = params;
                        for(var i = 0; i < rs.length; i++)
                        {
                            var key = rs[i].key;
                            var val = rs[i].value;

                            if(key == "DragControlGlobal" && !!val) {
                                _this.Attachments.Settings[key] = val;
                            } else if(!!(key.toLowerCase().indexOf("attach") + 1)) {
                                var _property = key;
                                if(!!(key.toLowerCase().indexOf("viewname") + 1)) {
                                    _property = key.substr(key.toLowerCase().indexOf("viewname"), key.length);
                                }
                                if(_this.Attachments.Settings.hasOwnProperty(_property) && !!val) {
                                    _this.Attachments.Settings[_property] = [];
                                    var valArr = val.split(",");
                                    for(var j = 0; j < valArr.length; j++) {
                                        if(!!valArr[j]) {
                                            (_this.Attachments.Settings[_property]).push(valArr[j]);
                                        }
                                    }
                                }
                            } else if(!!(key.toLowerCase().indexOf("required") + 1)) {
                                var _property = key;
                                if(!!(key.toLowerCase().indexOf("viewname") + 1)) {
                                    _property = key.substr(key.toLowerCase().indexOf("viewname"), key.length);
                                }
                                if(_this.Recuired.Settings.hasOwnProperty(_property) && !!val) {
                                    _this.Recuired.Settings[_property] = [];
                                    var valArr = val.split(",");
                                    for(var j = 0; j < valArr.length; j++) {
                                        if(!!valArr[j]) {
                                            (_this.Recuired.Settings[_property]).push(valArr[j]);
                                        }
                                    }
                                }
                            }


                        }

                        if (callback)
                            callback();
                    },
                    function(e) {
                        console.error(e);
                        if (callback)
                            callback();
                    }
                );
            } catch(e){
                console.error(e);
                if (callback)
                    callback();
            }
        }

        function _addImageButton(viewName) {
            var viewName  = viewName;
            if (Xrm.Page.data && Xrm.Page.data.entity.getEntityName() == "ddsm_attachmentdocument") {
                viewName = _this.Attachments.Settings["ObjectName"] + "_View";
            }

            if(!viewName) {
                return;
            } else if (!_this._Xrm.Page.getControl(viewName)) {
                setTimeout(function(){
                    _addImageButton(viewName);
                }, 250);
            } else {

                setTimeout(function(){
                    var parentContainer = $("#" + viewName + "_contextualButtonsContainer", _this._document).parent();
                    $("#" + viewName + "_contextualButtonsContainer").hide();
                    $("#" + viewName + "_openAssociatedGridViewImageButton").hide();
                    $("#" + viewName + "_ccDiv .ms-crm-grid-databodycontainer-Ex table tbody tr [id^=gridBodyTable_delete]").css({'display': 'none'});

                    if (_this._Xrm.Page.getAttribute("statecode").getValue() == 0) {
                        var btnContainer = '<div class="ms-crm-Grid-ContextualButtonsContainer" id="' + viewName + '_contextualButtonsContainer"><div class="ms-crm-contextButton"><a href="#" id="' + viewName + '_addImageButtonCustom" style="display: block; cursor: pointer;" class="ms-crm-ImageStrip-addButton" title="Add New Document."><img src="/_imgs/imagestrips/transparent_spacer.gif?ver=-293175106" id="' + viewName + '_addImageButtonCustomImage" alt="Add New Document." title="Add New Document." class="ms-crm-add-button-icon"></a><input id="attachedFiles" type="file" multiple="multiple" name="file[]" style="display:none;visibility:hidden;" onchange="GlobalJs.DocsJs.Attachments._preProcessFiles(this.files);" /></div></div>';
                        $(parentContainer).append(btnContainer);
                        $("#" + viewName + "_addImageButtonCustom").click(function () {
                            $('#attachedFiles').trigger('click');
                        });
                    }
                }, 250);

                _this._Xrm.Page.getControl(viewName).addOnLoad(function(e){
                    setTimeout(function(){
                        $("#" + e._element.id + "_contextualButtonsContainer").hide();
                        $("#" + e._element.id + "_openAssociatedGridViewImageButton").hide();
                        $("#" + e._element.id + "_ccDiv .ms-crm-grid-databodycontainer-Ex table tbody tr [id^=gridBodyTable_delete]").css({'display': 'none'});
                    }, 250);
                });

                if(_this._Xrm.Page.data.entity.getEntityName() == "ddsm_attachmentdocument") {

                    setTimeout(function(){
                        $("#All_" + viewName + "_contextualButtonsContainer").hide();
                        $("#All_" + viewName + "_openAssociatedGridViewImageButton").hide();
                        $("#All_" + viewName + "_ccDiv .ms-crm-grid-databodycontainer-Ex table tbody tr [id^=gridBodyTable_delete]").css({'display': 'none'});
                    }, 250);
                    /*
                     Xrm.Page.getControl("All_" + viewName).addOnLoad(function(e){
                     setTimeout(function(){
                     $("#" + e._element.id + "_contextualButtonsContainer").hide();
                     $("#" + e._element.id + "_openAssociatedGridViewImageButton").hide();
                     $("#" + e._element.id + "_ccDiv .ms-crm-grid-databodycontainer-Ex table tbody tr [id^=gridBodyTable_delete]").css({'display': 'none'});
                     }, 250);
                     });
                     */
                    //Create link to download file
                    /*
                     DocumentsJs.Attachments._getFileId(function (id) {
                     DocumentsJs.Attachments._createLinkToFile(id);
                     });
                     */
                }

            }

        }

        function _isManyToManyRelationship(entityName1, entityName2, callback){
            if(!entityName1 && !entityName2) {
                if (!!callback) {callback(false, null);}
                return;
            } else  if(!!entityName1 && !entityName2) {
                entityName2 = _this._Xrm.Page.data.entity.getEntityName();
            } else  if(!entityName1 && !!entityName2) {
                entityName1 = _this._Xrm.Page.data.entity.getEntityName();
            }

            var queryOptions = {
                Filter:'IsCustomRelationship eq true',
                Select:['MetadataId','SchemaName']
            };
            GlobalJs.WebAPI.GetRelationshipsDisplayNameList(entityName1, 1, queryOptions).then(function(entityResult1)
                {
                    GlobalJs.WebAPI.GetRelationshipsDisplayNameList(entityName2, 1, queryOptions).then(function(entityResult2)
                        {
                            var arr1 = [], arr2 = [], isRelation = false, schemaName = null;
                            if(entityResult1.length == 0 || entityResult2.length == 0)
                            {
                                if (!!callback) {callback(false, null);}
                                return;
                            }
                            if(entityResult1.length < entityResult2.length) {
                                arr1 = entityResult1;
                                arr2 = entityResult2;
                            } else {
                                arr1 = entityResult2;
                                arr2 = entityResult1;
                            }
                            for(var i = 0; i < arr1.length; i++){
                                for(var j = 0; j < arr2.length; j++){
                                    if(arr1[i].MetadataId == arr2[j].MetadataId)
                                    {
                                        isRelation = true;
                                        schemaName = arr1[i].SchemaName;
                                        break;
                                    }
                                }
                            }

                            if (!!callback) {callback(isRelation, schemaName);}

                        },
                        function(e){
                            if (!!callback) {callback(false, null);}
                        });
                },
                function(e){
                    if (!!callback) {callback(false, null);}
                });

        }

        function _isManyToOneRelationship(entityName, callback){
            var queryOptions = {
                Filter:"IsCustomRelationship eq true and ReferencedEntity eq '" + _this._Xrm.Page.data.entity.getEntityName() + "'"
                ,Select:["MetadataId","ReferencingAttribute"]
            };
            GlobalJs.WebAPI.GetRelationshipsDisplayNameList(entityName, 2, queryOptions).then(function(r) {
                if(r.length == 1)
                {
                    if (!!callback) {callback(r[0]);}
                }

            });
        }


    }).call(GlobalJs.DocsJs);
}


if (typeof (GlobalJs.DocsJs.Attachments) == "undefined") {
    GlobalJs.DocsJs.Attachments = {__namespace: true};
    (function () {
        var _this = this;
        this.Settings = {};
        this._Xrm = null;
        this._window = null;
        this._document = null;
        this.RecuiredObj = null;

        this.initAttachments = false,
            this._parentRelation = "ddsm_attachmentdocument_ddsm_attachmentdocument_Parent",
            this._entityRelation = null,
            this.viewName = null,
            this.Settings["DragControlGlobal"] = true,
            this.Settings["ObjectName"] = "AttachmentDocument",
            this.Settings["ViewName"] = [this.Settings["ObjectName"] + "_View"],
            this.Settings["FilesCount"] = 0,
            this.Settings["RenderTo"] = null;


        this.Init = function(viewName, docsJs) {
            _this._Xrm = docsJs._Xrm;
            _this._window = docsJs._window;
            _this._document = docsJs._document;
            _this.viewName = viewName;
            _this.RecuiredObj = docsJs.Recuired;

            if (!!_this.viewName)
            {
                console.log("GlobalJs.DocsJs.Attachments Initialized.");
                _this._initDragAndDrop();
            }
        },
            this._initDragAndDrop = function() {

                if (window.FileReader && Modernizr.draganddrop /* && Xrm.Page.getAttribute("statecode").getValue() == 0 */) {
                    _this.Settings["RenderTo"] = null;
                    if (_this.Settings["DragControlGlobal"]) {
                        _this.Settings["RenderTo"] = _this._document.getElementsByTagName("body")[0];
                    } else if(!!_this.viewName) {
                        _this.Settings["RenderTo"] = _this._document.getElementById(_this.viewName + "_d");
                    } else {
                        _this.Settings["RenderTo"] = _this._document.getElementsByTagName("body")[0];
                    }
                    _this.Settings["RenderTo"].addEventListener("drop", _this._handleDrop, !1);
                    _this.Settings["RenderTo"].addEventListener("dragover", _this._handleDragOver, !1);
                } else console.error("Drag and drop api not supported");
            },

            this._removeDragAndDrop = function() {
                if(!!_this.Settings["RenderTo"])
                {
                    _this.Settings["RenderTo"].removeEventListener("drop", _this._handleDrop, !1);
                    _this.Settings["RenderTo"].removeEventListener("dragover", _this._handleDragOver, !1);
                }
            },

            this._handleDragOver = function (n) {
                n.stopPropagation();
                n.preventDefault()
            },

            this._handleDrop = function (n) {
                n.stopPropagation();
                n.preventDefault();
                var t = _this._Xrm.Page.data.entity.getId();
                if (typeof t == "undefined" || t == null || t.length < 36) {
                    GlobalJs.Alert.show(null, "The file was unable to be uploaded. Please save the file and reload the page.", [{
                        label: "Ok",
                        callback: function () {
                            return;
                        }
                    }], "ERROR", 500, 200);
                    return;
                }
                _this._preProcessFiles(n.dataTransfer.files)
            },

            this._preProcessFiles = function (n) {

                if (_this._Xrm.Page.ui.getFormType() != 2) {
                    GlobalJs.Alert.show(null, "Record Read only or inactive", [{
                        label: "Ok",
                        callback: function () {
                            return;
                        }
                    }], "WARNING", 500, 200);
                } else {

                    var n = n;
                    var entityReferenceCollection = [], entityReference = {};
                    entityReference.id = _this._Xrm.Page.data.entity.getId();
                    entityReference.typename = _this._Xrm.Page.data.entity.getEntityName();
                    entityReference.type = _this._Xrm.Internal.getEntityCode(_this._Xrm.Page.data.entity.getEntityName());
                    entityReferenceCollection.push(entityReference);

                    //Verify Required Documents
                    if(_this.RecuiredObj._isRelationship && _this.RecuiredObj._rDocCount > 0) {
                        _selectedRequiredDocument(n);

                    } else {
                        _processFiles(n, entityReferenceCollection);
                    }
                }
            };

        function _processFiles(n, entityRef) {
            console.dir(entityRef);
            GlobalJs.showSpinner(true, null, "Uploading files ...")
            if (n && n.length) {
                _this.Settings["FilesCount"] = n.length;
                for (var t = 0; t < n.length; t++) _uploadSingle(n[t], entityRef);
            }
        }

        function _uploadSingle (n, entityRef) {

            var t = new FileReader;
            t.onloadstart = function () {

            };
            t.onload = function (t) {

                var u = t.target.result, s = u.indexOf("base64,") + 7;

                var newAttachmentDocument = {}, newAnnotation = {};
                newAttachmentDocument.ddsm_name = n.name;
                newAttachmentDocument["ddsm_attachmentdocument_Annotations"] = [];
                newAnnotation.isdocument = !0;
                newAnnotation.filename = n.name;
                newAnnotation.documentbody = u.toString().substring(s);
                newAttachmentDocument["ddsm_attachmentdocument_Annotations"].push(newAnnotation);

                var updateAttachmentDocument = {}, urlActual = "", urlParent = "";

                GlobalJs.WebAPI.Create("ddsm_attachmentdocuments", newAttachmentDocument).then(function(docId) {
                        console.log('AttachDoc Id is ' + docId);
                        //New Version
                        if (_this._Xrm.Page.data.entity.getEntityName() == "ddsm_attachmentdocument") {

                            updateAttachmentDocument["@odata.id"] = _getWebAPIPath() + _this._Xrm.Page.data.entity.getEntityName() + "s(" + ((_this._Xrm.Page.data.entity.getId()).replace(/\{|\}/g, '')).toUpperCase() + ")";
                            urlParent = _getWebAPIPath() + _this._Xrm.Page.data.entity.getEntityName() + "s(" + ((docId).replace(/\{|\}/g, '')).toUpperCase() + ")/" + _this._parentRelation + "/$ref";
                            _update("PUT", urlParent, updateAttachmentDocument, function(){

                                setTimeout(function(){
                                    _this._Xrm.Utility.openEntityForm(_this._Xrm.Page.data.entity.getEntityName(), docId);
                                }, 500);
                            });

                        } else {

                            if(entityRef.length > 0) {

                                for(var i = 0; i < entityRef.length; i++){
                                    var lookupItems = {};
                                    lookupItems.items = [];
                                    lookupItems.items.push(entityRef[i]);

                                    //Update Required Document records
                                    if(entityRef[i].typename == "ddsm_requireddocument") {
                                        _this._window.AssociateObjects(_this._Xrm.Internal.getEntityCode("ddsm_attachmentdocument"), docId, lookupItems.items[0].type, lookupItems, true, "", _this.RecuiredObj._attachRelation);

                                        GlobalJs.WebAPI.Update(entityRef[i].typename + "s",(entityRef[i].id).replace(/\{|\}/g, ''),{'ddsm_uploaded':true},false);

                                    } else {
                                        _this._window.AssociateObjects(_this._Xrm.Internal.getEntityCode("ddsm_attachmentdocument"), docId, lookupItems.items[0].type, lookupItems, true, "", _this._entityRelation);
                                    }
                                }
                            }

                            _this.Settings["FilesCount"]--;
                            if (_this.Settings["FilesCount"] == 0) {
                                setTimeout(function(){
                                    GlobalJs.showSpinner(false);
                                    if(_this._Xrm.Page.data.entity.getEntityName() =="ddsm_requireddocument") {
                                        _this._Xrm.Page.data.refresh();
                                    } else {
                                        if(!!_this.viewName)
                                        {
                                            _this._Xrm.Page.getControl(_this.viewName).refresh();
                                        }
                                    }

                                    if(!!_this._Xrm.Page.getControl(_this.RecuiredObj.viewName)) {
                                        _this._Xrm.Page.getControl(_this.RecuiredObj.viewName).refresh();
                                    }

                                }, 500);
                            }
                        }

                    }
                    ,function(e){
                        console.error(e);
                        _this.Settings["FilesCount"]--;
                        if (_this.Settings["FilesCount"] == 0) {
                            setTimeout(function(){
                                GlobalJs.showSpinner(false);
                                if(_this._Xrm.Page.data.entity.getEntityName() =="ddsm_requireddocument") {
                                    _this._Xrm.Page.data.refresh();
                                } else {
                                    if(!!_this.viewName)
                                    {
                                        _this._Xrm.Page.getControl(_this.viewName).refresh();
                                    }
                                }

                                if(!!_this._Xrm.Page.getControl(_this.RecuiredObj.viewName)) {
                                    _this._Xrm.Page.getControl(_this.RecuiredObj.viewName).refresh();
                                }

                            }, 500);
                        }
                    });

            };

            t.onerror = function (n) {
                GlobalJs.Alert.show(null, "File could not be read! Code " + n.target.error.code, [{
                    label: "Ok",
                    callback: function () {
                        return;
                    }
                }], "ERROR", 500, 200);
                _this.Settings["FilesCount"]--;
                if (_this.Settings["FilesCount"] == 0) {
                    setTimeout(function(){
                        GlobalJs.showSpinner(false);
                        if(_this._Xrm.Page.data.entity.getEntityName() =="ddsm_requireddocument") {
                            _this._Xrm.Page.data.refresh();
                        } else {
                            if(!!_this.viewName)
                            {
                                _this._Xrm.Page.getControl(_this.viewName).refresh();
                            }
                        }

                        if(!!_this._Xrm.Page.getControl(_this.RecuiredObj.viewName)) {
                            _this._Xrm.Page.getControl(_this.RecuiredObj.viewName).refresh();
                        }

                    }, 500);
                }
            };
            t.readAsDataURL(n);
        }

        function _create(entitySetName, entity, successCallback, errorCallback) {
            var req = new XMLHttpRequest();
            req.open("POST", encodeURI(_getWebAPIPath() + entitySetName), true);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.onreadystatechange = function () {
                if (this.readyState == 4) {
                    req.onreadystatechange = null;
                    if (this.status == 204) {
                        if (successCallback)
                            successCallback(this.getResponseHeader("OData-EntityId"));
                    }
                    else {
                        if (errorCallback)
                            errorCallback(_errorHandler(this.response));
                    }
                }
            };
            req.send(JSON.stringify(entity));
        }

        function _update(_type, url, entity, callback) {
            var req = new XMLHttpRequest();
            req.open(_type, encodeURI(url), true);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.onreadystatechange = function () {
                if (this.readyState == 4) {
                    req.onreadystatechange = null;
                    if (this.status == 204) {
                        if (callback)
                            callback();
                    }
                    else {
                        console.log(_errorHandler(this.response));
                        if (callback)
                            callback();
                    }
                }
            };

            req.send(JSON.stringify(entity));

        }

        function _selectedRequiredDocument(fileList) {
            var fileList = fileList;

            var entityReferenceCollection = [], entityReference = {};
            entityReference.id = _this._Xrm.Page.data.entity.getId();
            entityReference.typename = _this._Xrm.Page.data.entity.getEntityName();
            entityReference.type = _this._Xrm.Internal.getEntityCode(_this._Xrm.Page.data.entity.getEntityName());
            entityReferenceCollection.push(entityReference);

            if(!!_this.RecuiredObj.viewName && _this._Xrm.Page.getControl(_this.RecuiredObj.viewName).getGrid().getSelectedRows().getLength() == 1) {
                //entityReferenceCollection = [];
                entityReference = {};
                entityReference.id = _this._Xrm.Page.getControl(_this.RecuiredObj.viewName).getGrid().getSelectedRows().get(0).getData().getEntity().getEntityReference().id;
                entityReference.typename = _this._Xrm.Page.getControl(_this.RecuiredObj.viewName).getGrid().getSelectedRows().get(0).getData().getEntity().getEntityReference().entityType;
                entityReference.type = _this._Xrm.Internal.getEntityCode(entityReference.typename);
                entityReferenceCollection.push(entityReference);
                _processFiles(fileList, entityReferenceCollection);
                return;
            }

            GlobalJs.Alert.show(null, "The document you are attaching is not among the list of required documents. Please select an option below.", [{
                label: "Override",
                title: "Select the required documents that are no longer required as a result of this document attachment.",
                callback: function () {
                    _genLookupView(fileList);
                }
            }, {
                label: "Do Not Override",
                title: "Attach this document without overriding an existing document requirement.",
                callback: function () {
                    _processFiles(fileList, entityReferenceCollection);
                }
            }, {
                label: "Cancel",
                title: "Cancel Attachment of Document.",
                callback: function () {
                    _this.Settings["FilesCount"] = 0;
                    return;
                }
            }], "QUESTION", 500, 200);

        }

        function _genLookupView(fileList) {
            var fileList = fileList;
            var objectCode = _this._Xrm.Internal.getEntityCode("ddsm_requireddocument");

            var fetchxml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                "<entity name='ddsm_requireddocument'>" +
                "<attribute name='ddsm_name' />" +
                "<attribute name='ddsm_requiredbystatus' />" +
                "<order attribute='ddsm_requiredbystatus' descending='false' />" +
                "<order attribute='ddsm_name' descending='false' />" +
                "<filter type='and'>" +
                "<condition attribute='" + _this.RecuiredObj._lookupFieldName + "' operator='eq' value='" + _this._Xrm.Page.data.entity.getId() + "' />" +
                "<condition attribute='ddsm_uploaded' operator='eq' value='false' />" +
                "<condition attribute='ddsm_canceled' operator='eq' value='false' />" +
                "</filter>" +
                "</entity>" +
                "</fetch>";

            var layout = "<grid name='resultset' object='1' jump='fullname' select='1' preview='0' icon='1'>" +
                "<row name='result' id='ddsm_requireddocumentid'>" +
                "<cell name='ddsm_requiredbystatus' width='100' />" +
                "<cell name='ddsm_name' width='300' />" +
                "</row>" +
                "</grid>";

            var viewId = "{00000000-0000-0000-00AA-000010001002}";

            var customView = {
                fetchXml: fetchxml,
                id: viewId,
                layoutXml: layout,
                name: "Required Document List",
                recordType: objectCode,
                Type: 0
            };

            _this._window.Mscrm.Utilities.returnLookupItems = function (lookupItems, lookupField, bPopulateLookup, callbackReference) {
                var entityReferenceCollection = [], entityReference = {};
                entityReference.id = _this._Xrm.Page.data.entity.getId();
                entityReference.typename = _this._Xrm.Page.data.entity.getEntityName();
                entityReference.type = _this._Xrm.Internal.getEntityCode(_this._Xrm.Page.data.entity.getEntityName());
                entityReferenceCollection.push(entityReference);

                if (lookupItems != null) {
                    if (lookupItems.items.length > 0) {
                        console.dir(lookupItems);
                        for(var i = 0; i < lookupItems.items.length; i++) {
                            entityReference = {};
                            entityReference.typename = lookupItems.items[i].typename;
                            entityReference.id = lookupItems.items[i].id;
                            entityReference.type = lookupItems.items[i].type;
                            entityReferenceCollection.push(entityReference);
                        }
                    }
                }
                _processFiles(fileList, entityReferenceCollection);


            };
            var callbackFunctionObject =  _this._window.Mscrm.Utilities.createCallbackFunctionObject('returnLookupItems',  _this._window.Mscrm.Utilities, [null, null], false);
            _this._window.LookupObjectsWithCallback(callbackFunctionObject, null, "multi", objectCode, 0, null, "", "0", null, null, null, null, null, viewId, [customView]);
        }

        function _getWebAPIPath(){
            return GlobalJs.clientUrl + GlobalJs.baseUrl;
        }

    }).call(GlobalJs.DocsJs.Attachments);
}

if (typeof (GlobalJs.DocsJs.Recuired) == "undefined") {
    GlobalJs.DocsJs.Recuired = {__namespace: true};
    (function () {
        var _this = this;
        this.Settings = {};
        this._Xrm = null;
        this._window = null;
        this._document = null;

        this.initRecuired = false,
            this.AttachmentsObj = null,
            this._attachRelation = null,
            this.viewName = null,
            this.Settings["ObjectName"] = "RequiredDocument",
            this.Settings["ViewName"] = [this.Settings["ObjectName"] + "_View"],
            this._lookupFieldName = null,
            this._isRelationship = false,
            this._rDocCount = 0;

        this.Init = function(viewName, lookupFieldName, relationship, docsJs) {
            _this._Xrm = docsJs._Xrm;
            _this._window = docsJs._window;
            _this._document = docsJs._document;
            _this.AttachmentsObj = docsJs.Attachments;
            _this.viewName = viewName;
            _this._lookupFieldName = lookupFieldName;
            _this._isRelationship = relationship;


            console.log("GlobalJs.DocsJs.Recuired Initialized.");
            if (!!_this.viewName)
            {
                _appendStyle();
            }
        };

        function _appendStyle() {
            var style = _this._document.createElement('style');
            style.type = 'text/css';
            style.innerHTML = "#" + _this.viewName + "_divDataArea #gridBodyTable tr[uloaded='cancel'] { background-color: #AAAAAA;}" +
                "#" + _this.viewName + "_divDataArea #gridBodyTable tr[uloaded='true'] { background-color: #AAEEAA;}" +
                "#" + _this.viewName + "_divDataArea #gridBodyTable tr[uloaded='false'] { background-color: #EEAAAA;}" +
                "#" + _this.viewName + "_divDataArea #gridBodyTable tr[selected='true'], " +
                "#" + _this.viewName + "_divDataArea #gridBodyTable tr[uloaded='cancel'][selected='true']," +
                "#" + _this.viewName + "_divDataArea #gridBodyTable tr[uloaded='true'][selected='true']," +
                "#" + _this.viewName + "_divDataArea #gridBodyTable tr[uloaded='false'][selected='true'] {background-color: #3EA99F;}";
            _this._document.getElementsByTagName('head')[0].appendChild(style);

            _hideContorls();
        }

        function _hideContorls() {
            var viewName = _this._Xrm.Page.getControl(_this.viewName);
            if(!viewName) {
                setTimeout(function() {
                    _hideContorls();
                }, 500);
            } else {
                $("#" + "titleContainer_" + _this.viewName).hide();
                _this._Xrm.Page.getControl(_this.viewName).addOnLoad(function (e){
                        setTimeout(function(){
                            $("#" + "titleContainer_" + e._element.id, _this._document).hide();
                            $("#" + e._element.id + "_ccDiv .ms-crm-grid-databodycontainer-Ex table tbody tr [id^=gridBodyTable_delete]", _this._document).css({'display': 'none'});
                            _backGroundColorGrid(e);
                        }, 250);
                    }
                );
            }
        }

        function _backGroundColorGrid(grid) {
            try {
                setTimeout(function(){

                    var items = _this._Xrm.Page.getControl(grid._element.id).getGrid().getRows().getAll(),
                        itemsCount = _this._Xrm.Page.getControl(grid._element.id).getGrid().getRows().getLength(),
                        pendingMsIndex = -1;
                    if(!!_this._Xrm.Page.getAttribute("ddsm_pendingmilestoneindex")) {
                        pendingMsIndex = _this._Xrm.Page.getAttribute("ddsm_pendingmilestoneindex").getValue();
                    }
                    if (items) {

                        var indexMsIdx = $("#" + grid._element.id + " #gridBodyTable", _this._document).find("col[name=ddsm_requiredbystatus]").index();
                        var indexUploaded = $("#" + grid._element.id + " #gridBodyTable", _this._document).find("col[name=ddsm_uploaded]").index();
                        var indexCanceled = $("#" + grid._element.id + " #gridBodyTable", _this._document).find("col[name=ddsm_canceled]").index();

                        for (var i = 0; i < itemsCount; i++) {
                            var id = items[i].getKey();
                            $(grid._element).find("tr[oid='" + id + "']").each(function () {
                                var theTr = $(this);

                                if (indexCanceled != -1 && (theTr.find("td").slice(indexCanceled, indexCanceled + 1).text()).toLowerCase() == "yes") {
                                    theTr.attr("uloaded", "cancel")
                                }

                                if (indexUploaded != -1 && (theTr.find("td").slice(indexUploaded, indexUploaded + 1).text()).toLowerCase() == "yes") {
                                    theTr.attr("uloaded", "true")
                                }

                                if (indexMsIdx != -1 && indexUploaded != -1 && indexCanceled != -1 && (theTr.find("td").slice(indexUploaded, indexUploaded + 1).text()).toLowerCase() == "no" && parseInt(theTr.find("td").slice(indexMsIdx, indexMsIdx + 1).text()) <= pendingMsIndex && (theTr.find("td").slice(indexCanceled, indexCanceled + 1).text()).toLowerCase() == "no") {
                                    theTr.attr("uloaded", "false")
                                }

                            });
                        }

                    }

                }, 1000);
            }
            catch (e) {
                console.log(e.description);
            }
        }

    }).call(GlobalJs.DocsJs.Recuired);
}

