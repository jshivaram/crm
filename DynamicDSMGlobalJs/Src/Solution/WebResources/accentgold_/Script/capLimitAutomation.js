function Init() {
        var $xrm = GlobalJs.window.Xrm;

        if ($xrm.Page.data === null || $xrm.Page.data.entity === null)
            return;
    
        if (!$xrm.Page.data.entity.getId())
            return;
        
        try {
            var entityInfo = {};
    
            entityInfo.TargetEntityLogicalName = $xrm.Page.data.entity.getEntityName();
            if (entityInfo.TargetEntityLogicalName == "ddsm_project")
                entityInfo.TargetRecordId = $xrm.Page.data.entity.getId();
            else if (entityInfo.TargetEntityLogicalName == "ddsm_financial")
                entityInfo.TargetRecordId = $xrm.Page.getAttribute('ddsm_projecttofinancialid').getValue()[0].id;
            else 
                return;
            
            entityInfo.UserId = $xrm.Page.context.getUserId();
             
            var request = {
                "InputData": JSON.stringify([entityInfo]) 
            }
    
            GlobalJs.WebAPI.ExecuteAction('ddsm_DDSMCapCalculation', request).then(
                function(params) {
                    if (!params || !params.Reply)
                        return;
                        
                    var response = JSON.parse(params.Reply);
        
                        var message = "";
                        var info = "";
                        if (response.IsLimitReached) {
                            message = "The Customer Cap should be reviewed prior to moving this project forward to the next stage";
                        }
                        if (response.IsCustomerLimitReached) {
                            message += "Customer Cap is reached";
                            info += "<p>Customer Cap for Parent Account's Total Incentive Amount is reached</p>";
                        }
                        if (response.IsSiteLimitReached) {
                            message += "<p>Site Cap is reached</p>";
                            info += "<p>Site Cap for Parent Account's Total Incentive Amount is reached</p>";
                        }
                        if (info != "") {
                            info += "<p>Please manually review to determine the payment amount</p>";
                        }
    
                        if (message.length > 0)
                            GlobalJs.window.Alert.show(message, info, [{ label: "OK" }], "WARNING");   
        
                    
                }
            ,function(error){
                console.log(error);
            });  
        } catch(e) {
            console.log(e);
        }
        
    }