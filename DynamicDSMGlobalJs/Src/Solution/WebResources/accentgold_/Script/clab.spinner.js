/**
 * CreaLab.in Spinner
 *
 * Created by Sergey  Dergunov (s.dergunov@gmail.com) on 07/11/2015.
 * Last Update by Sergey Dergunov (s.dergunov@gmail.com) on 07/25/2017.
 *
 * Version: 3.2.0.2
 */

/**
 * EXAMPLE OF USAGE
 * ---------------------

 * DEFAULT SPINNER SETTINGS
 *
 CreaLab.Spinner.defaults = {
 lines: 13,             // Count of lines
 length: 0,             // Line length
 width: 7,              // Line width
 radius: 23,            // Circle radius
 rotate: 45,            // Rotation displacement (0..90 degrees)
 corners: 1,            // Corner radius of lines (0..1)
 color: '#FFFFFF',      // #rgb or #rrggbb color of text and lines
 color_o: '#000000',    // #rgb Ð¸Ð»Ð¸ #rrggbb Ñ†Ð²ÐµÑ‚ Ð¾Ð²ÐµÑ€Ð»ÐµÑ? (Ñ„Ð¾Ð½Ð°)
 direction: 1,          // 1: clockwise, -1: counterclock-wise
 speed: 0.5,            // Count of spins per second
 trail: 75,             // Percent of the rest glow
 opacity: 0,            // Line opacity
 opacity_o: 0.25,       // Overlay opacity (background)
 fps: 20,               // frames per second - setTimeout()
fontFamily: 'Segoe UI Light, Segoe UI, Tahoma, Arial',    // Font styles
fontWeight: 'lighter',
fontSize: '14px'

 };


 * INITIATION OF THE SPINNER
 *
 * Window Top
 * spinnerForm = CreaLab.Spinner.spin();
 * or
 * spinnerForm = CreaLab.Spinner.spin(null, "Loading ...");
 *
 * Current Window
 spinnerForm = CreaLab.Spinner.spin(document.getElementById("Attribute ID"));
 * or
 spinnerForm = CreaLab.Spinner.spin(document.getElementById("Attribute ID"), "Loading ...");

 * Setting a new text message into initiated spinner
 spinnerForm.setText("100%");

 * REMOVE THE SPINNER
 spinnerForm.stop();
 **/

(this,function(){function t(e){return this instanceof arguments.callee==!1?new t(e):(this.name=e,this)}t.prototype.__namespace=!0,t.prototype.namespace=function(e){return this[e]||(this[e]=new t((this.name?this.name+".":"")+e))},t.prototype.extend=function(t){for(var e in t)t.hasOwnProperty(e)&&(this[e]=t[e]);return this},t.prototype.toString=function(){return this.name},window.CreaLab||t.prototype.namespace.call(this,"CreaLab").extend(function(){function t(t){return null==t?String(t):e[toString.call(t)]||"object"}for(var e={},i="Boolean Number String Function Array Date RegExp Object".split(" "),o=0;o<i.length;o++)e["[object "+i[o]+"]"]=i[o].toLowerCase();return{type:t}}()),CreaLab.namespace("Spinner").extend(function(){void 0!==window.CreaLab&&(_CreaLab=CreaLab),void 0!==window.top.CreaLab&&(window.top._CreaLab=window.top.CreaLab);var t,e=["webkit","Moz","ms","O"],i={},o=document,n=function(){return CreaLab.Spinner.defaults&&"object"==typeof CreaLab.Spinner.defaults?CreaLab.Spinner.defaults:{}},r=function(t,e){var i,n=o.createElement(t||"div");for(i in e)n[i]=e[i];return n},a=function(t){for(var e=1,i=arguments.length;i>e;e++)t.appendChild(arguments[e]);return t},s=function(){var t=r("style",{type:"text/css"});return a(o.getElementsByTagName("head")[0],t),t.sheet||t.styleSheet}(),l=function(e,o,n,r){var a=["opacity",o,~~(100*e),n,r].join("-"),l=.01+n/r*100,p=Math.max(1-(1-e)/o*(100-l),e),c=t.substring(0,t.indexOf("Animation")).toLowerCase(),d=c&&"-"+c+"-"||"";return i[a]||(s.insertRule("@"+d+"keyframes "+a+"{0%{opacity:"+p+"}"+l+"%{opacity:"+e+"}"+(l+.01)+"%{opacity:1}"+(l+o)%100+"%{opacity:"+e+"}100%{opacity:"+p+"}}",s.cssRules.length),i[a]=1),a},p=function(t,i){var o,n,r=t.style;for(i=i.charAt(0).toUpperCase()+i.slice(1),n=0;n<e.length;n++)if(o=e[n]+i,void 0!==r[o])return o;return void 0!==r[i]?i:void 0},c=function(t,e){for(var i in e)t.style[p(t,i)||i]=e[i];return t},d=function(t){for(var e={x:t.offsetLeft,y:t.offsetTop};t=t.offsetParent;)e.x+=t.offsetLeft,e.y+=t.offsetTop;return e},h=function(t,e){return"string"==typeof t?t:t[e%t.length]},f=function(){function t(t,e){return r("<"+t+' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">',e)}s.addRule(".spin-vml","behavior:url(#default#VML)"),this.lines=function(e,i){function o(){return c(t("group",{coordsize:l+" "+l,coordorigin:-s+" "+-s}),{width:l,height:l})}function n(e,n,r){a(d,a(c(o(),{rotation:360/i.lines*e+"deg",left:~~n}),a(c(t("roundrect",{arcsize:i.corners}),{width:s,height:i.width,left:i.radius,top:-i.width>>1,filter:r}),t("fill",{color:h(i.color,e),opacity:i.opacity}),t("stroke",{opacity:0}))))}var r,s=i.length+i.width,l=2*s,p=2*-(i.width+i.length)+"px",d=c(o(),{position:"absolute",top:p,left:p});if(i.shadow)for(r=1;r<=i.lines;r++)n(r,-2,"progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)");for(r=1;r<=i.lines;r++)n(r);return a(e,d)},this.opacity=function(t,e,i,o){var n=t.firstChild;o=o.shadow&&o.lines||0,n&&e+o<n.childNodes.length&&(n=n.childNodes[e+o],n=n&&n.firstChild,n=n&&n.firstChild,n&&(n.opacity=i))}},u=function(){var e=c(r("group"),{behavior:"url(#default#VML)"});!p(e,"transform")&&e.adj?f():t=p(e,"animation")},m=function(t,e){return new g(t,e)},g=function(e,i){this._doc=o=e?window.document:window.top.document,e=e?e:o.getElementsByTagName("body")[0];for(var s=e.querySelectorAll('div[role="spin_overlay"]'),p=e.querySelectorAll('div[role="spin_textbox"]'),f=e.querySelectorAll('div[role="spin_progressbar"]'),u=0;u<f.length;u++)e.removeChild(f[u]),e.removeChild(p[u]),e.removeChild(s[u]);this.lines=function(e,i){function o(t,e){return c(r(),{position:"absolute",width:i.length+i.width+"px",height:i.width+"px",background:t,boxShadow:e,transformOrigin:"left",transform:"rotate("+~~(360/i.lines*s+i.rotate)+"deg) translate("+i.radius+"px,0)",borderRadius:(i.corners*i.width>>1)+"px"})}for(var n,s=0,p=(i.lines-1)*(1-i.direction)/2;s<i.lines;s++)n=c(r(),{position:"absolute",top:1+~(i.width/2)+"px",transform:i.hwaccel?"translate3d(0,0,0)":"",opacity:i.opacity,animation:t&&l(i.opacity,i.trail,p+s*i.direction,i.lines)+" "+1/i.speed+"s linear infinite"}),i.shadow&&a(n,css(o("#000","0 0 4px #000"),{top:"2px"})),a(e,a(n,o(h(i.color,s),"0 0 1px rgba(0,0,0,.1)")));return e},this.opacity=function(t,e,i){e<t.childNodes.length&&(t.childNodes[e].style.opacity=i)},this.stop=w,this.setText=y,this._onresize=v,this.opts={lines:13,length:0,width:7,radius:23,rotate:45,corners:1,color:"#FFFFFF",color_o:"#000000",direction:1,speed:.5,trail:75,opacity:0,opacity_o:.25,fps:20,zIndex:2e9,className:"spinner",top:"auto",left:"auto",position:"absolute",fontFamily:"Segoe UI Light, Segoe UI, Tahoma, Arial",fontWeight:"lighter",fontSize:"14px"};var m=n();for(var g in m)this.opts[g]=m[g];var b,x,C,_=this,L=_.opts,N=_.el_o=c(r(0,{className:"overlay"}),{position:"fixed",top:0,right:0,left:0,bottom:0,opacity:L.opacity_o,background:L.color_o,zIndex:L.zIndex-2}),I=_.el_t=c(r(0,{className:"textloader"}),{position:L.position,display:"inline-block","font-family":L.fontFamily,"font-weight":L.fontWeight,"font-size":L.fontSize,color:L.color,"text-align":"center",width:"auto",zIndex:L.zIndex}),S=_.el=c(r(0,{className:L.className}),{position:L.position,width:0,zIndex:L.zIndex-1}),z=L.radius+L.length+L.width;if(_._conteyner=e,_._msg=i?i:"",e&&(e.insertBefore(N,e.firstChild||null),e.insertBefore(I,e.firstChild||null),e.insertBefore(S,e.firstChild||null),x=d(e),b=d(S),C=d(I),N.setAttribute("role","spin_overlay"),N.setAttribute("id","spin_overlay"),I.setAttribute("role","spin_textbox"),I.innerHTML=_._msg,c(S,{left:"50%",top:"50%","margin-top":-parseInt((I.clientHeight+z)/2)+"px"}),c(I,{left:"50%",top:"50%","margin-left":-parseInt(I.clientWidth/2)+"px","margin-top":-parseInt((I.clientHeight-z)/2)+"px"})),S.setAttribute("role","spin_progressbar"),_.lines(S,_.opts),!t){var T,u=0,A=(L.lines-1)*(1-L.direction)/2,F=L.fps,M=F/L.speed,k=(1-L.opacity)/(M*L.trail/100),B=M/L.lines;!function H(){u++;for(var t=0;t<L.lines;t++)T=Math.max(1-(u+(L.lines-t)*B)%M*k,L.opacity),_.opacity(S,t*L.direction+A,T,L);_.timeout=_.el&&setTimeout(H,~~(1e3/F))}()}return _},y=function(t){var e,i=this.opts,o=this.el_t,n=d(this._conteyner);c(o,{left:"0",top:"0"}),o.innerHTML=this.msg=t?t:"",e=d(o),c(o,{left:("auto"==i.left?n.x-e.x+(this._conteyner.offsetWidth>>1)-parseInt(o.clientWidth/2):parseInt(i.left,10)+mid-parseInt(o.clientWidth/2))+"px",top:("auto"==i.top?n.y-e.y+(this._conteyner.offsetHeight>>1)+i.radius+i.length+o.clientHeight+10:parseInt(i.top,10)+mid+i.radius+i.length+o.clientHeight+10)+"px"})},w=function(){var t=this.el,e=this.el_o,i=this.el_t;return t&&(clearTimeout(this.timeout),t.parentNode&&t.parentNode.removeChild(t),this.el=void 0),e&&(e.parentNode&&e.parentNode.removeChild(e),this.el_o=void 0),i&&(i.parentNode&&i.parentNode.removeChild(i),this.el_t=void 0),this},v=function(t){console.dir(this)},b=function(){void 0!==window._CreaLab&&(CreaLab=_CreaLab),void 0!==window.top._CreaLab&&(window.top.CreaLab=window.top._CreaLab)};return{init:u,spin:m,stop:w,setText:y,onresize:v,noConflict:b,version:"2.2.0.0"}}())})(window.top.CreaLab=window.CreaLab||function(){},this.init);

(function () {
    CreaLab.Spinner.defaults = {
        lines: 11,             // Count of lines
        length: 0,             // Line length
        width: 3,              // Line width
        radius: 13,            // Circle radius
        rotate: 45,            // Rotation displacement (0..90 degrees)
        corners: 1,            // Corner radius of lines (0..1)
        color: '#000000',      // #rgb or #rrggbb color of text and lines
        color_o: '#FFFFFF',    // #rgb or #rrggbb Overlay background color
        direction: 1,          // 1: clockwise, -1: counterclock-wise
        speed: 0.5,            // Count of spins per second
        trail: 75,             // Percent of the rest glow
        opacity: 0,            // Line opacity
        opacity_o: 0.35,       // Overlay opacity (background)
        fps: 20,               // frames per second - setTimeout()
        fontFamily: 'Segoe UI Light, Segoe UI, Tahoma, Arial',    // Font styles
        fontWeight: 'lighter',
        fontSize: '9px'
    };

    CreaLab._spinnerSettings = {};
    CreaLab._spinnerSettings["OnLoadShow"] = false;
    CreaLab._spinnerSettings["Defaults"] = CreaLab.Spinner.defaults;

    CreaLab._loadSpinnerSettings = function(callback) {
        if (callback)
            callback();
        /*
         Process.callAction("ddsm_SpinnerSettings", [],
         function(params) {
         //var rs = JSON.parse(params[0].value);
         var rs = params;
         //console.dir(rs);
         for(let i = 0; i < rs.length; i++)
         {
         let key = rs[i].key;
         let val = JSON.parse(rs[i].value);
         if(GlobalJs._spinnerSettings.hasOwnProperty(key) && !!val) {
         GlobalJs._spinnerSettings[key] = val;
         if(key == "OnLoadShow") {
         window.top.OnLoadFormShowSpinner = val;
         }
         }
         }

         if (callback)
         callback();
         },
         function(e) {
         console.log("error: " + e);
         if (callback)
         callback();
         }
         );
         */
    };


    //Example:
    //Show: showSpinner(true) //window.top
    //showSpinner(true, null, "Text Message")
    //showSpinner(true, document.getElementsById("Element Id"), "Text Message") //for current element
    //Stop: showSpinner(false)
    GlobalJs.showSpinner = function (show, target, msg) {
        if (show) {

            if (typeof CreaLab != "undefined" && typeof CreaLab.Spinner != "undefined" && typeof CreaLab.Spinner.spin == "function")
                window.top.spinnerGlobal = CreaLab.Spinner.spin(target || null, msg || "Loading data ...");
        }
        else {
            if (!!window.top.spinnerGlobal)
                window.top.spinnerGlobal.stop();
        }
    };

    CreaLab._initLoadSpinnerJs = function () {

        if(typeof GlobalJs.Process === "undefined")
        {
            setTimeout(CreaLab._initLoadSpinnerJs, 200);
        } else {
            CreaLab._loadSpinnerSettings(function(){
                if(CreaLab._spinnerSettings["OnLoadShow"]) {
                    GlobalJs.showSpinner(true, null, "Loading components ...");
                } else {
                    //set 100% transparent color
                    CreaLab.Spinner.defaults = {
                        opacity: 1,
                        opacity_o: 1
                    };
                    GlobalJs.showSpinner(true, null, "");
                    GlobalJs.showSpinner(false);
                    //set defaults settings
                    CreaLab.Spinner.defaults = CreaLab._spinnerSettings["Defaults"];
                }

            });
        }

    };

    CreaLab._initLoadSpinnerJs();

})();
