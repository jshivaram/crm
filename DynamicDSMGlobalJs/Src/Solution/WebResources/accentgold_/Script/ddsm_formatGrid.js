function Format() {
	try {

        console.log("in Grid Format"); //debugger;
        var contFrame0visability = $(window.top.document.getElementById('contentIFrame0')).css('visibility');
        var entityType = "";
        var tblHeaders = "";
        var rows = "";
        if (contFrame0visability == "visible") {
            entityType = $(window.top.document.getElementById('contentIFrame0').contentWindow.document.getElementById('gridBodyTable').rows[1]).attr("otypename");
            tblHeaders = window.top.document.getElementById('contentIFrame0').contentWindow.document.getElementById('gridBodyTable').getElementsByTagName('TH');
            rows = window.top.document.getElementById('contentIFrame0').contentWindow.document.getElementById('gridBodyTable').rows;
        } else {
            entityType = $(window.top.document.getElementById('contentIFrame1').contentWindow.document.getElementById('gridBodyTable').rows[1]).attr("otypename");
            tblHeaders = window.top.document.getElementById('contentIFrame1').contentWindow.document.getElementById('gridBodyTable').getElementsByTagName('TH');
            rows = window.top.document.getElementById('contentIFrame1').contentWindow.document.getElementById('gridBodyTable').rows;
        }
        getCustomControlMetadata(entityType, function (config) {
            config.forEach(function (element) {
                var def = JSON.parse(element.ddsm_definition);
                //  console.log(def.selectedField);
                fillGrid(def.selectedFieldLabel);
            }, this);
        });
        var fillGrid = function (hdText) {
            //   console.log("Format grid for: " + entityType);
            var timerId = "";
            var hdIndex = 0;
            var i = 0;
            var chkKeyWord = "";
            // var hdText = "Log";
            //Loop through the headers to find the index of hdText
            for (i = 0; i < tblHeaders.length; i++) {
                //  console.log("TAB: " + tblHeaders[i].innerText);
                if (hdText == tblHeaders[i].innerText) {
                    //   console.log("column index is: " + i);
                    hdIndex = i;
                    break;
                }
            } //end of for
            //   console.log("after find column");
            // console.log("after rows");
            var ctr = 0;
            for (i = 1; i < rows.length; i++) {
                // console.log("in rows FOR");
                if ((rows[i].cells[hdIndex].innerText).substring(rows[i].cells[hdIndex].innerText.length - 1, rows[i].cells[hdIndex].innerText.length) == "\n")
                    chkKeyWord = (rows[i].cells[hdIndex].innerText).substring(0, rows[i].cells[hdIndex].innerText.length - 1);
                else
                    chkKeyWord = rows[i].cells[hdIndex].innerText;
                //debugger;
                var json = rows[i].cells[hdIndex].innerText;
                if (json.startsWith("[")) { //for array
                    var listOfVal = JSON.parse(json);
                    var list = listOfVal.map(function (val) {
                        if (val["Label"]) {
                            return val["Label"];
                        }
                    });
                    rows[i].cells[hdIndex].innerText = list.join(", ");
                }
                if (json.startsWith("{")) { //for one
                    var val = JSON.parse(json);
                    if (val.Label) {
                        rows[i].cells[hdIndex].innerText = val.Label;
                    }
                }
            }
        }
    } catch (e) {
        console.error("In Grid Format: " + e);
    }
}