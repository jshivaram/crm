function InitMyFunc() {
	try {
		var allGrids = $("div.ms-crm-Field-Data-Print");
		for (var i = 0; i < allGrids.length; i++) {
			var firstChildOfGrid = allGrids[i].children[0];
			if (firstChildOfGrid && firstChildOfGrid.children[0] && firstChildOfGrid.children[0].nodeName === "IFRAME") {
				continue;
			}
			var heightOfGrid = allGrids[i].style.height;
			if (!heightOfGrid || heightOfGrid === "") {
				continue;
			}
			heightOfGrid = heightOfGrid.replace("px", "");
			heightOfGrid = +heightOfGrid;
			var newHeightOfGrid = heightOfGrid - 45;
			newHeightOfGrid = "" + newHeightOfGrid + "px";
			allGrids[i].style.height = newHeightOfGrid;
		}
	} catch (e) {
		console.log(e);
	}
}