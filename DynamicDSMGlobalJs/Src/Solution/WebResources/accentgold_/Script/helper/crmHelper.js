var currentWindow = parent.Xrm.Internal.isTurboForm() ? parent.window : window;
GlobalJs = currentWindow.GlobalJs;

var DDSM = DDSM || {};
DDSM.WebApi = DDSM.WebApi || {};
var Xrm = Xrm || window.parent.Xrm;
//console.log("-- in DDSM.WebApi1");

(function () {
   // console.log("-- in DDSM.WebApi2");
    DDSM.WebApi = {
        CallActionGlobal: function (actionName, params, callback, errorCallback) {
            //console.log("In params DDSM.WebApi CallActionGlobal ");
           // console.log(window.parent.JSON.stringify(params));
            var query = actionName;
            var req = new XMLHttpRequest();
            req.open("POST", GlobalJs.clientUrl + GlobalJs.baseUrl + query, true);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.onreadystatechange = function () {
                console.log("req.onreadystatechange: " + this.readyState + " status: " + this.status);
                if (this.readyState == 4) {
                    req.onreadystatechange = null;
                  //  debugger;
                    if (this.status == 200) {
                        //debugger;
                        if (callback) {
                            var resp = JSON.parse(this.response);
                            if (resp["Result"] && resp["Result"] != "") {
                                callback(JSON.parse(resp.Result));
                            }
                            else {
                                callback();
                            }
                        }
                    } else {
                      //  debugger;
                        if (this.response != "") {
                            var error = JSON.parse(this.response).error;
                            if (errorCallback) {
                                errorCallback(error.message);
                            }
                            console.log(error.message);
                        }
                        else {
                            callback();
                        }
                    }
                }
            };
            req.send(JSON.stringify(params));
        },

        CallAction: function (actionName, entityLogicalName, recordId, params,callback, errorCallback) {
            //console.log("In params DDSM.WebApi CallAction");
           // console.log(window.parent.JSON.stringify(params));
            var query = entityLogicalName +"("+recordId+")/Microsoft.Dynamics.CRM."+actionName;
            var req = new XMLHttpRequest();
            req.open("POST", GlobalJs.clientUrl + GlobalJs.baseUrl + query, true);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.onreadystatechange = function () {
                console.log("req.onreadystatechange: " + this.readyState + " status: " + this.status);
                if (this.readyState == 4) {
                    req.onreadystatechange = null;
                  //  debugger;
                    if (this.status == 200) {
                       // debugger;
                        if (callback) {
                            var resp = JSON.parse(this.response);
                            if (resp["Result"] && resp["Result"] != "") {
                                callback(resp.Result);
                            }
                            else {
                                callback();
                            }
                        }
                    } else {
                      //  debugger;
                        if (this.response != "") {
                            var error = JSON.parse(this.response).error;
                            if (errorCallback) {
                                errorCallback(error.message);
                            }
                            console.log(error.message);
                        }
                        else {
                            callback();
                        }
                    }
                }
            };
            req.send(JSON.stringify(params));
        },


    };
 window.top.DDSM = DDSM;
}).call(this);




