/** @namespace Namespace for MYAPP classes and functions. */
var KendoHelper = KendoHelper || {};


var WebApiHelper = WebApiHelper || {};

/**
 * A maths utility
 * @namespace KendoHelper.CRM
 * @class KendoHelper 
 */



KendoHelper.CRM = {
    getBaseUrl: function () {
       return GlobalJs.clientUrl + GlobalJs.baseUrl;
       // return Xrm.Page.context.getClientUrl() + "/api/data/v8.2/";
    },
    transport: function (url,extended) {
        var result = {};
        result.read = {
            url: encodeURI(url),
            dataType: "json",
            beforeSend: function (req) {
                req.setRequestHeader('Accept', 'application/json');
                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                req.setRequestHeader('OData-MaxVersion', "4.0");
                req.setRequestHeader("OData-Version", "4.0");

                 if(extended){
                  req.setRequestHeader("Prefer", 'odata.include-annotations="OData.Community.Display.V1.FormattedValue"');
                 }
            }
        }



        return result;
    },
    defEntityMetadataSchema: function () {
        var result = {};
        result.data = function (response) {
            var value = response.value;
            var model = {};
            if (value) {
                model = value.map(function (el) {
                    var displayName = "";
                    var id = el.MetadataId;
                    var logicalName = el.SchemaName.toLowerCase();
                    if (el.DisplayName.LocalizedLabels[0] && el.DisplayName.LocalizedLabels[0].Label) {
                        displayName = el.DisplayName.LocalizedLabels[0].Label;
                    } else if (el.DisplayName.UserLocalizedLabel && el.DisplayName.UserLocalizedLabel.Label) {
                        displayName = el.DisplayName.UserLocalizedLabel.Label;
                    } else {
                        displayName = logicalName;
                    }
                    return {
                        Label: displayName,
                        LogicalName: logicalName,
                        Id: id
                    };
                });
            }
            else {
                console.log("---defEntityMetadataSchem->Value ==null  ");
            }

            return model;
        }
        return result;
    },
    defOptionsetMetadataSchema: function () {
        var result = {};
        result.data = function (response) {
            var value = response.value;
            var model = value.map(function (el) {
                var displayName = "";
                var id = el.MetadataId;
                var logicalName = el.Name.toLowerCase();
                return {
                    LogicalName: logicalName,
                    Id: id
                };
            });
            return model;
        }
        return result;
    },
    optionSetMetadata: function () {
        var result = {};
        result.data = function (response) {
            var value = response.Options;
            var model = value.map(function (el) {
                var displayName = el.Label.LocalizedLabels[0].Label;
                var value = el.Value;

                return {
                    DisplayName: displayName,
                    Value: value
                };
            });
            return model;
        }
        return result;
        var logicalName = model.TargetFieldLogicalName;
    },


    getEntityMetadata: function (entName, withIndicators, justFieldType, groupBy) {
   // debugger;
        var url = this.getBaseUrl() + "ddsm_DDSMGetMetadataWithIndicators";       
        return new kendo.data.DataSource({
            schema: {
                data: function (response) {
                    return JSON.parse(response.Result);
                }
            },
            group: { field: groupBy },
            transport: {
                read: {
                    type: "POST",
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json'
                },
                parameterMap: function (options, operation) {
                    var postData = {
                        EntityName: entName, WithIndicators: withIndicators,
                        JustFieldType: justFieldType
                    };
                    // note that you may need to merge that postData with the options send from the DataSource
                    return JSON.stringify(postData);
                }
            },
        });
    },


    // return esp metadata like mad's and qdd's
    getSmartMeasureMetadata: function (id, callback, targetEntity,errorCallback1) {
        var url = this.getBaseUrl()+ "ddsm_DDSMGetSmartMeasureMetadata";
        var errorCallback = function (error) {
            console.log(error);
            errorCallback1(error);
        };
        var postData = {
            SmartMeasureId: id,
            TargetEntity: targetEntity
        };
        var req = new XMLHttpRequest();
        req.open("POST", encodeURI(url), true);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.onreadystatechange = function () {
            if (this.readyState == 4) { // complete 
                req.onreadystatechange = null;
                if (this.status == 200) {
                    // debugger;
                    var data = JSON.parse(this.response);
                    var result = JSON.parse(data.Result);
                    if (callback) {
                        callback(result);
                    }
                } else {
                    if (this.status != 204) {
                        var error = JSON.parse(this.response).error;
                        if (errorCallback) {
                            errorCallback(error.message);
                        }

                    }
                    else {
                        if (errorCallback) {
                            errorCallback("Cen't get data from WEB API. Status: "+ this.status);
                        }
                    }

                }
            }
        };
        req.send(JSON.stringify(postData));

    },
    //return kendo dataSource
    getOptionSet: function (logicalName) {
         var url = this.getBaseUrl();
        return new kendo.data.DataSource({
            schema: KendoHelper.CRM.optionSetMetadata(),
            transport: KendoHelper.CRM.transport(url+ "GlobalOptionSetDefinitions(Name='" + logicalName + "')"),
        });
    }
};



WebApiHelper.Wrap = {
    retriew: function (url, callback, errorCallback) {
        var req = new XMLHttpRequest();
        req.open("GET", encodeURI(url), true);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.onreadystatechange = function () {
            if (this.readyState == 4/* complete */) {
                req.onreadystatechange = null;
                if (this.status == 200) {
                    debugger;
                    var data = JSON.parse(this.response);
                    var dat = data.value;

                    if (callback) {
                        callback(dat);
                    }

                } else {
                    var error = JSON.parse(this.response).error;
                    if (errorCallback) {
                        errorCallback(error.message);
                    }
                }
            }
        };
        req.send();
    }
}; 