var clientUrl = Xrm.Page.context.getClientUrl();
var LoadGlobalIncludes = function(url, id, type, obj, callbackFunc) {
    if (typeof  obj._window.GlobalJs == 'undefined') {
        var head = obj._document.getElementsByTagName("head")[0];
        if(type == "js") {
            var scriptTag = obj._document.createElement('script');
            scriptTag.id=id;
            scriptTag.type = "text/javascript";
            scriptTag.language="javascript";
            scriptTag.src = clientUrl + "/WebResources/" + url;
            scriptTag.async = true;
            scriptTag.onreadystatechange = scriptTag.onload = function () {
                if (!scriptTag.readyState || scriptTag.readyState === "loaded" || scriptTag.readyState === "complete") {
                    scriptTag.onreadystatechange = scriptTag.onload = null;
                    if(!!callbackFunc) {
                        callbackFunc();
                    }
                }
            };
            scriptTag.onerror = function (oError) {
                console.error("[GlobalJs]: The " + oError.target.src + " is not accessible.");
                if(!!callbackFunc) {
                    callbackFunc();
                }
            };
            head.appendChild(scriptTag);
        }
    } else {
        console.warn('['+obj._panelId+'][GlobalJs]: Repeated loading of the js script. Load skipped. The GlobalJs.GlobalInit method is called.');
        obj._window.GlobalJs.GlobalInit(obj._panelId);
        return;
    }
};

function GetObjectContentPanel() {
    var obj = {};
    obj._panelId = window.top.document.getElementById("crmContentPanel").getAttribute("currentcontentid");
    var _iframe = window.top.document.getElementById(obj._panelId);
    if(!!_iframe) {
        obj._window = _iframe.contentWindow || _iframe.contentDocument;
        if(!!obj._window.document) {
            obj._document = obj._window.document;
        }
    }
    return obj;
}

function globalCheck() {
    var obj =  GetObjectContentPanel();
    if(typeof obj._window == "undefined" || typeof obj._document == "undefined" ) {
        return;
    }
    //console.log('['+obj._panelId+'][GlobalJs]: Call function globalCheck;');
    if(typeof window.top.ActivePanel == "undefined") {
        window.top.ActivePanel = obj._panelId;
        LoadGlobalIncludes("ddsm_includes.js", "ddsm_includes", "js", obj, null);
    } else if (window.top.ActivePanel != obj._panelId) {
        window.top.ActivePanel = obj._panelId;
        LoadGlobalIncludes("ddsm_includes.js", "ddsm_includes", "js", obj, null);
    } else {
        if (typeof  obj._window.GlobalJs == 'undefined') {
            LoadGlobalIncludes("ddsm_includes.js", "ddsm_includes", "js", obj, null);
            return;
        }
        if(typeof obj._window.initStarted != "undefined" && obj._window.initStarted) {
            console.warn('['+obj._panelId+'][GlobalJs]: Call function globalCheck skipped.');
            return;
        } else {
            obj._window.initStarted = true;
            LoadGlobalIncludes("ddsm_includes.js", "ddsm_includes", "js", obj, null);
        }
    }
}
