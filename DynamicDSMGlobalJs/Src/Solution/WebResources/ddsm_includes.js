if (typeof (GlobalJs) == "undefined") {
    window.initStarted = false;

    GlobalJs = { __namespace: true };

    (function () {
        var _this = this;
        /// Private function to the context object.
        function _context() {
            if (typeof GetGlobalContext != "undefined") {
                return GetGlobalContext();
            } else if (typeof Xrm != "undefined") {
                return Xrm.Page.context;
            } else {
                throw new Error("Context is not available.");
            }
        }

        /// Private function to return the server URL from the context
        function _getClientUrl() {
            var clientUrl = _context().getClientUrl()
            return clientUrl;
        };

        function _getContentIFrame() {
            var crmContentPanel = { _document: null, _window: null, _containerId: null, _entity: null, _Xrm: null, _isForm: false };
            var activeIFrameId = null;
            if(!!_this._iframelId) {
                activeIFrameId = _this._iframelId
            } else if(typeof window.top.ActivePanel != "undefined") {
                activeIFrameId = _this._iframelId = window.top.ActivePanel;
            } else {
                activeIFrameId = _this._iframelId = window.top.document.getElementById("crmContentPanel").getAttribute("currentcontentid");
            }
            if (!!activeIFrameId) {
                crmContentPanel._containerId = activeIFrameId;
                var _iframe = window.top.document.getElementById(activeIFrameId);
                if (!!_iframe) {
                    crmContentPanel._window = _iframe.contentWindow || _iframe.contentDocument;
                    _this.window = crmContentPanel._window;
                    if (!!crmContentPanel._window.document) {
                        crmContentPanel._document = crmContentPanel._window.document;
                        _this.document = crmContentPanel._document;
                    }
                    if (!!crmContentPanel._window.Xrm) {
                        crmContentPanel._Xrm = crmContentPanel._window.Xrm;
                    }
                    if (!!crmContentPanel._Xrm) {
                        crmContentPanel._entity = crmContentPanel._Xrm.Internal.getEntityName(parseInt(crmContentPanel._Xrm.Page.context.getQueryStringParameters().etc));
                        if (typeof crmContentPanel._Xrm.Page.context.getQueryStringParameters().id != "undefined" && !!crmContentPanel._Xrm.Page.context.getQueryStringParameters().id) {
                            crmContentPanel._isForm = true;
                        } else if (typeof crmContentPanel._Xrm.Page.context.getQueryStringParameters().id == "undefined" && !!crmContentPanel._Xrm.Page.ui && !!crmContentPanel._Xrm.Page.ui.getFormType()) {
                            crmContentPanel._isForm = true;
                        }
                    }
                }
            }
            return crmContentPanel;
        };

        this.window = window,
            this.document = this.window.document,
            this.OrgLCID = _context().getOrgLcid(),
            this.LCID = _context().getUserLcid(),
            this.formType = (!!Xrm.Page.ui) ? Xrm.Page.ui.getFormType() : -1,
            this.head = this.document.getElementsByTagName("head")[0],
            this._iframelId = null,
            this.crmContentPanel = _getContentIFrame(),
            this.clientUrl = _getClientUrl(),
            this.baseUrl = "/api/data/v8.2/",
            this.resourcesList = new Object(),
            this.resourcesLoader = new Object(),
            this.typeEvent = {
                OnSave: "form",
                OnChange: "Attribute",  //The OnChange event usually occurs when the data in a form field has changed and focus is lost. The OnChange event does not occur if the field is changed programmatically using the setValue method. If you want event handlers for the OnChange event to run after you set the value you must use the Xrm.Page.data.entity attribute.fireOnChange method in your code.
                OnKeyPress: "Control",
                TabStateChange: "Tab",  //The TabStateChange event occurs when the DisplayState of the tab changes due to user interaction or when the setDisplayState method is applied in code
                OnReadyStateComplete: "IFrame", //The OnReadyStateComplete event indicates that the content of the IFRAME has loaded and can be accessed in code. Use this event when referencing IFRAME controls within your scripts.
                PreSearch: "LookUp" //Use this event with other Lookup control methods and events to change the results displayed in a lookup based on the form data current just before the lookup control shows search results for a user to choose from.
            },
            this.eventsList = new Object(),
            this.initFunction = {
                Global: null,
                Local: null
            },
            this.Loader = function (url, id, type, callbackFunc) {
                id = CrmEncodeDecode.CrmHtmlAttributeEncode(url);
                if (typeof GlobalJs.resourcesLoader[id] == 'undefined') {
                    GlobalJs.resourcesLoader[id] = null;
                }
                if (GlobalJs.resourcesLoader[id] == null) {
                    try {
                        var resourceUrl = GlobalJs.clientUrl + "/WebResources/" + url;
                        if (type == "js") {
                            var scriptTag = GlobalJs.document.createElement('script');
                            scriptTag.id = id;
                            scriptTag.type = "text/javascript";
                            scriptTag.language = "javascript";
                            scriptTag.async = true;
                            scriptTag.onreadystatechange = scriptTag.onload = function (e) {
                                if (!GlobalJs.resourcesLoader[id] && (!scriptTag.readyState || scriptTag.readyState === "loaded" || scriptTag.readyState === "complete")) {
                                    GlobalJs.resourcesLoader[id] = true;
                                    scriptTag.onreadystatechange = scriptTag.onload = null;
                                    if (!!callbackFunc) { callbackFunc(); }
                                }
                            };
                            scriptTag.onerror = function (oError) {
                                GlobalJs.resourcesLoader[id] = false;
                                //console.error('['+_this._iframelId+'][GlobalJs]: The ' + oError.target.src + " is not accessible.');
                                if (!!callbackFunc) { callbackFunc(); }
                            };
                            scriptTag.src = resourceUrl;
                            GlobalJs.head.appendChild(scriptTag);
                        }
                        if (type == "css") {
                            var styleTag = GlobalJs.document.createElement('link');
                            styleTag.id = id;
                            styleTag.type = "text/css";
                            styleTag.rel = 'stylesheet';
                            styleTag.async = true;
                            styleTag.onreadystatechange = styleTag.onload = function (e) {
                                if (!GlobalJs.resourcesLoader[id] && (!styleTag.readyState || styleTag.readyState === "loaded" || styleTag.readyState === "complete")) {
                                    GlobalJs.resourcesLoader[id] = true;
                                    styleTag.onreadystatechange = styleTag.onload = null;
                                    if (!!callbackFunc) { callbackFunc(); }
                                }
                            };
                            styleTag.onerror = function (oError) {
                                GlobalJs.resourcesLoader[id] = false;
                                //console.error('['+_this._iframelId+'][GlobalJs]: The ' + oError.target.href + " is not style.');
                                if (!!callbackFunc) { callbackFunc(); }
                            };
                            styleTag.href = resourceUrl;
                            GlobalJs.head.appendChild(styleTag);
                        }
                    } catch (err) {
                        console.error('['+_this._iframelId+'][GlobalJs]:' + err.message);
                        GlobalJs.resourcesLoader[id] = false;
                        if (!!callbackFunc) { callbackFunc(); }
                    }
                } else {
                    if (!!callbackFunc) { callbackFunc(); }
                }
            },
            this.Init = function () {
                if (_this.resourcesList["global"] != null && Array.isArray(_this.resourcesList["global"]) && _this.resourcesList["global"].length > 0) {
                    var objResources = _this.resourcesList["global"];
                    for (var i = 0; i < objResources.length; i++) {
                        _this.Loader(objResources[i].url, objResources[i].id, objResources[i].type, null);
                    }
                    var lastFn = "function () {$(GlobalJs.document).ready(function () {";
                    for (var i = 0; i < objResources.length; i++) {
                        if (Array.isArray(objResources[i].onloadFunc) && objResources[i].onloadFunc.length > 0) {
                            for (var j = 0; j < objResources[i].onloadFunc.length; j++) {
                                lastFn += "try {" + objResources[i].onloadFunc[j] + "();" + "} catch (err) {console.error('['+GlobalJs._iframelId+'][GlobalJs]: ' + err.message + '. (InitFunc: " + objResources[i].onloadFunc[j] + "; File: " + objResources[i].url + ")');}";
                            }
                        }
                    }
                    lastFn += "});};";
                    _this.initFunction['Global'] = new Function('return ' + lastFn)();
                } else {
                    console.warn('['+_this._iframelId+'][GlobalJs]: The Global scripts list is null.');
                }
                if (_this.resourcesList[_this.crmContentPanel._entity] != null && Array.isArray(_this.resourcesList[_this.crmContentPanel._entity]) && _this.resourcesList[_this.crmContentPanel._entity].length > 0) {
                    var objResources = _this.resourcesList[_this.crmContentPanel._entity];
                    for (var i = 0; i < objResources.length; i++) {
                        _this.Loader(objResources[i].url, objResources[i].id, objResources[i].type, null);
                    }
                    var lastFn = "function () {$(GlobalJs.document).ready(function () {";
                    for (var i = 0; i < objResources.length; i++) {
                        if (Array.isArray(objResources[i].onloadFunc) && objResources[i].onloadFunc.length > 0) {
                            for (var j = 0; j < objResources[i].onloadFunc.length; j++) {
                                lastFn += "try {" + objResources[i].onloadFunc[j] + "();" + "} catch (err) {console.error('['+GlobalJs._iframelId+'][GlobalJs]: ' + err.message + '. (InitFunc: " + objResources[i].onloadFunc[j] + "; File: " + objResources[i].url + ")');}";
                            }
                        }
                    }
                    lastFn += "});};";
                    _this.initFunction['Local'] = new Function('return ' + lastFn)();
                } else {
                    console.warn('['+_this._iframelId+'][GlobalJs]: The ' + _this.crmContentPanel._entity + ' scripts list is null.');
                }
                setTimeout(GlobalJs._InitMetods, 200);
            },
            this.GlobalInit = function (iframelId) {
                if(!!iframelId) {
                    _this._iframelId = iframelId;
                }
                window.initStarted = true;
                _this.crmContentPanel = _getContentIFrame();
                _this.formType = (!!Xrm.Page.ui) ? Xrm.Page.ui.getFormType() : -1;
                if (typeof (window.formType) != "undefined" && window.formType == 1 && _this.formType != 1) { }
                else {
                    window.formType = _this.formType;
                }
                //Load WebAPI JS and then all other JS libs
                _this.Loader("sdk_/sdk.webapi.js", "WebAPI", "js", function () {
                    if (typeof (GlobalJs.WebAPI) == "undefined") {
                        GlobalJs.WebAPI = { __namespace: true };
                        GlobalJs.WebAPI = new WebAPI({ APIUrl: GlobalJs.clientUrl + GlobalJs.baseUrl, AccessToken: null });
                    }
                    _this.Loader('accentgold_/Entity/' + _this.crmContentPanel._entity + '/ddsm_includes.js', + _this.crmContentPanel._entity + '_include', 'js', function () { GlobalJs.Init(); });
                });

            },
            this._InitMetods = function () {
                var isLoaded = true;
                for (var key in GlobalJs.resourcesLoader) {
                    if (GlobalJs.resourcesLoader[key] == null) {
                        isLoaded = false;
                        break;
                    }
                }
                if (!isLoaded) {
                    setTimeout(GlobalJs._InitMetods, 200);
                    return;
                } else {
                    //console.info('['+_this._iframelId+'][GlobalJs]: The js script was successfully loaded.');
                    _this.crmContentPanel = _getContentIFrame();
                    if ($("#mainContainer #formContainer #areaForm #formBodyContainer #crmFormTabContainer div.ms-crm-InlineTab-Read", _this.crmContentPanel._document).length == 0 && $("#homepageTableCell #crmGrid", _this.crmContentPanel._document).length == 0) {
                        setTimeout(GlobalJs._InitMetods, 200);
                        return;
                    }
                    var iJsMethod = GlobalJs.Attr("initGlobalJsMethod").Value();
                    if(typeof iJsMethod == 'undefined') {
                        window.initStarted = false;
                        return;
                    } else if(iJsMethod == null) {
                        iJsMethod = 0;
                    } else {
                        iJsMethod = parseInt(iJsMethod);
                    }
                    if (!iJsMethod) {
                        GlobalJs.Attr("initGlobalJsMethod").Value(1);
                    } else {
                        if(typeof (window.formType) != "undefined" && window.formType == 1 && _this.formType != 1) {
                            console.warn('['+_this._iframelId+'][GlobalJs]: Changed the Form type from 1 to 2');
                            window.formType = _this.formType;
                        } else {
                            console.warn('['+_this._iframelId+'][GlobalJs]: Repeated calling of the GlobalJs.GlobalInit method. Call skipped.');
                            window.initStarted = false;
                            _this.showSpinner(false);
                            return;
                        }
                    }
                    if (_this.initFunction['Global'] != null && typeof _this.initFunction['Global'] == 'function') {
                        //console.info('['+_this._iframelId+'][GlobalJs]: Global methods of the Content Panel were initiated.');
                        _this.initFunction['Global']();
                    }
                    if (_this.initFunction['Local'] != null && typeof _this.initFunction['Local'] == 'function') {
                        //console.info('['+_this._iframelId+'][GlobalJs]: Local methods of the Content Panel were initiated.');
                        _this.initFunction['Local']();
                    }
                    window.initStarted = false;
                    //console.info('['+_this._iframelId+'][GlobalJs]: All GlobalJs methods of the Content Panel were initiated.');
                    if(_this.crmContentPanel._isForm) {
                        _this._GenAttachEvents();
                    }
                    _this.showSpinner(false);
                }
            },
            this._InitAttachEvents = function (keyEvt, attrNames, funcName) {
                if (attrNames[0].trim() != "all") {
                    for (var i = 0; i < attrNames.length; i++) {
                        GlobalJs.AttachEvents(keyEvt, attrNames[i].trim(), new Function('return ' + funcName)());
                    }
                } else {
                    switch (keyEvt) {
                        case "OnSave":
                            GlobalJs.AttachEvents(keyEvt, "form", new Function('return ' + funcName)());
                            break;
                        case "OnChange":
                            var attr = Xrm.Page.data.entity.attributes.get();
                            for (var j in attr) {
                                GlobalJs.AttachEvents(keyEvt, attr[j].getName(), new Function('return ' + funcName)());
                            }
                            break;
                        case "OnKeyPress":
                        case "PreSearch":
                            var ctrl = Xrm.Page.ui.controls.get();
                            for (var j in ctrl) {
                                GlobalJs.AttachEvents(keyEvt, ctrl[j].getName(), new Function('return ' + funcName)());
                            }
                            break;
                        case "TabStateChange":
                            var tab = Xrm.Page.ui.tabs.get();
                            for (var j in tab) {
                                GlobalJs.AttachEvents(keyEvt, tab[j].getName(), new Function('return ' + funcName)());
                            }
                            break;
                        case "OnReadyStateComplete":
                            break;
                        default:
                            break;
                    }

                }
            },
            this._GenAttachEvents = function () {
                var globalEvent = (GlobalJs.eventsList["global"] != null) ? true : false;
                var localEvent = (!!_this.crmContentPanel._entity && GlobalJs.eventsList[_this.crmContentPanel._entity] != null) ? true : false;
                for (var key in GlobalJs.typeEvent) {
                    //Global Event
                    var funcName = "";
                    var eventObjArr = [];
                    if (!!globalEvent && !!GlobalJs.eventsList["global"][key]) {
                        eventObjArr = GlobalJs.eventsList["global"][key];
                    }
                    if (globalEvent && eventObjArr.length > 0) {
                        for (var l = 0; l < eventObjArr.length; l++) {
                            if (!!eventObjArr[l].logicalName && (!!eventObjArr[l].eventFunc && Array.isArray(eventObjArr[l].eventFunc) && eventObjArr[l].eventFunc.length > 0)) {
                                for (var i = 0; i < eventObjArr[l].eventFunc.length; i++) {
                                    funcName += "try {" + eventObjArr[l].eventFunc[i] + "();" + "} catch (err) {console.error(err.message);}";
                                }
                                if (funcName.length > 0) {
                                    funcName = "function () {" + funcName + "}";
                                    var attrNames = eventObjArr[l].logicalName.toLowerCase().split(",");
                                    GlobalJs._InitAttachEvents(key, attrNames, funcName);
                                }
                            }
                        }
                    }
                    //Local Event
                    funcName = "";
                    eventObjArr = [];
                    if (!!localEvent && !!GlobalJs.eventsList[_this.crmContentPanel._entity][key]) {
                        eventObjArr = GlobalJs.eventsList[_this.crmContentPanel._entity][key];
                    }
                    if (localEvent && eventObjArr.length > 0) {
                        for (var l = 0; l < eventObjArr.length; l++) {
                            if (!!eventObjArr[l].logicalName && (!!eventObjArr[l].eventFunc && Array.isArray(eventObjArr[l].eventFunc) && eventObjArr[l].eventFunc.length > 0)) {
                                for (var i = 0; i < eventObjArr[l].eventFunc.length; i++) {
                                    funcName += "try {" + eventObjArr[l].eventFunc[i] + "();" + "} catch (err) {console.error(err.message);}";
                                }
                                if (funcName.length > 0) {
                                    funcName = "function () {" + funcName + "}";
                                    var attrNames = eventObjArr[l].logicalName.toLowerCase().split(",");
                                    GlobalJs._InitAttachEvents(key, attrNames, funcName);
                                }
                            }
                        }
                    }
                }

            },
            this.AttachEvents = function (keyEvt, attrName, funcName) {
                if (!!Xrm && !!Xrm.Page) {
                    switch (keyEvt) {
                        case "OnSave":
                            if (!!Xrm.Page.data && !!Xrm.Page.data.entity) {
                                Xrm.Page.data.entity.addOnSave(funcName)
                            }
                            break;
                        case "OnChange":
                            if (!!Xrm.Page.getAttribute(attrName)) {
                                Xrm.Page.getAttribute(attrName).addOnChange(funcName);
                            }
                            break;
                        case "OnKeyPress":
                            if (!!Xrm.Page.getControl(attrName)) {
                                Xrm.Page.getControl(attrName).addOnKeyPress(funcName);
                            }
                            break;
                        case "TabStateChange":
                            if (!!Xrm.Page.ui.tabs.get(attrName)) {
                                Xrm.Page.ui.tabs.get(attrName).addTabStateChange(funcName);
                            }
                            break;
                        case "OnReadyStateComplete":
                            break;
                        case "PreSearch":
                            if (!!Xrm.Page.getControl(attrName)) {
                                Xrm.Page.getControl(attrName).addPreSearch(funcName);
                            }
                            break;
                        default:
                            break;
                    }
                }

            },
            this.CheckBackgroundProcess = function (relatedEntities, onStartCall, successCallback, errorCallback, loop, timeOut) {
                //Example:
                //CheckBackgroundProcess(null, true)
                //CheckBackgroundProcess('{"EntityNames":[ { "EntityName":"ddsm_measure"}, {"EntityName":"ddsm_financial"}]}', function() {GlobalJs.showSpinner(true, null, "Please note! <br>There are background activities <br>working with the record.");})
                // CheckBackgroundProcess('{"EntityNames":[ { "EntityName":"ddsm_measure"}, {"EntityName":"ddsm_financial"}]}', null, null, null, true, 3000)

                relatedEntities = (!!relatedEntities && typeof relatedEntities == 'string') ? relatedEntities : null;
                onStartCall = (!!onStartCall && typeof onStartCall == 'function') ? onStartCall : null;
                successCallback = (!!successCallback && typeof successCallback == 'function') ? successCallback : null;
                errorCallback = (!!errorCallback && typeof errorCallback == 'function') ? errorCallback : null;
                loop = (!!loop && typeof loop == 'boolean') ? loop : false;
                timeOut = (!!timeOut && typeof timeOut == 'number') ? loop : 5000;

                var params = [{
                    key: "Target",
                    type: Process.Type.EntityReference,
                    value: {
                        id: Xrm.Page.data.entity.getId(),
                        entityType: Xrm.Page.data.entity.getEntityName()
                    }
                }];
                if (!!relatedEntities) {
                    params.push({
                        key: "IsCheckRelatedEntities",
                        type: Process.Type.Bool,
                        value: true
                    });
                    params.push({
                        key: "ListRelatedEntities",
                        type: Process.Type.String,
                        value: relatedEntities
                    });
                }
                Process.callAction("ddsm_DDSMManualcheckofBackgroundJobs", params,
                    function (params) {
                        var rs = JSON.parse(params[0].value);
                        var isBackgrondJobs = rs.Result;
                        if (isBackgrondJobs) {
                            if (!!onStartCall && typeof onStartCall == "function") {
                                onStartCall();
                            }

                            if (loop) {
                                setTimeout(function () {
                                    CheckBackgroundProcess(relatedEntities, onStartCall, successCallback, errorCallback, loop, timeOut);
                                }, timeOut || timeOut_CheckBackgroundProcess);
                            } else {
                                if (!!successCallback && typeof successCallback == "function") {
                                    successCallback(true);
                                }
                            }
                        } else {
                            if (!!successCallback && typeof successCallback == "function") {
                                successCallback(false);
                            }
                        }
                    },
                    function (e) {
                        if (!!errorCallback && typeof errorCallback == "function") {
                            errorCallback(e);
                        } else {
                            console.error(e);
                        }
                    }
                );
            },
            this.Attr = function (attrName) {
                var attrConteyner = null;
                if (typeof $("#mainContainer #formContainer #areaForm #formBodyContainer #crmFormTabContainer", _this.crmContentPanel._document)[0] != "undefined") {
                    attrConteyner = $("#mainContainer #formContainer #areaForm #formBodyContainer #crmFormTabContainer", _this.crmContentPanel._document)[0];
                } else if (typeof $("#homepageTableCell #crmGrid", _this.crmContentPanel._document)[0] != "undefined") {
                    attrConteyner = $("#homepageTableCell #crmGrid", _this.crmContentPanel._document)[0];
                } else {
                    attrConteyner = null;
                }
                if (!attrConteyner) { return "undefined"; }

                var attrName = attrName;
                if (typeof attrName !== "undefined" && !!attrName) { attrName = attrName.toLowerCase(); }
                var Value = function (val) {
                    if (typeof attrName !== "undefined" && !!attrName) {
                        if (typeof val !== "undefined") {
                            $(attrConteyner).attr(attrName, val);
                        } else {
                            var attrVal = $(attrConteyner).attr(attrName);
                            if (typeof attrVal == 'undefined') {
                                return null;
                            } else {
                                return attrVal;
                            }
                        }
                    } else {
                        return null;
                    }
                };

                if (!(typeof attrName !== "undefined" && !!attrName)) {
                    var containerAttr = {};
                    $(attrConteyner).each(function () {
                        $.each(this.attributes, function () {
                            if (this.specified) {
                                containerAttr[this.name] = this.value;
                            }
                        });
                    });
                    return containerAttr;
                } else {
                    return {
                        Value: Value
                    };
                }
            };


        GlobalJs = this;
    }).call(GlobalJs);
}
//Global JS list

GlobalJs.resourcesList["global"] = [
    {
        url: "accentgold_/Css/RemoveEmptySpaceOnForms.css",
        id: "RemoveEmptySpaceOnFormsCss",
        type: "css",
        onloadFunc: []
    },
    {
        url: "accentgold_/Script/ddsm_removeEmptySpaceFromForms.js",
        id: "removeEmptySpaceFromFormsJs",
        type: "js",
        onloadFunc: ["InitMyFunc"]
    },
    {
        url: "accentgold_/Script/helper/crmHelper.js",
        id: "crmHelper",
        type: "js",
        onloadFunc: []
    },
    {
        url: "accentgold_/Script/ddsm_formatGrid.js",
        id: "ddsm_formatGrid",
        type: "js",
        onloadFunc: ["Format"]
    },
    {
        url: "accentgold_/settings/js/settingsHelper.js",
        id: "settingsHelper",
        type: "js",
        onloadFunc: []
    },
    {
        url: "accentgold_/Script/helper/kendoHelper.js",
        id: "kendoHelper",
        type: "js",
        onloadFunc: []
    },
    {
        url: "mag_/js/alert.js",
        id: "AlertJs",
        type: "js",
        onloadFunc: ["(function () {GlobalJs.Alert = window.GlobalJs.Alert || Alert;})"]
    }
    , {
        url: "mag_/js/process.js",
        id: "ProcessJs",
        type: "js",
        onloadFunc: ["(function () {GlobalJs.Process = window.GlobalJs.Process || Process;})"]
    }
    , {
        url: "accentgold_/Script/clab.spinner.js",
        id: "SpinnerJs",
        type: "js",
        onloadFunc: []
    }
    , {
        url: "kendoui_/kendo.all.min.js",
        id: "Kendo",
        type: "js",
        onloadFunc: ["(function () {GlobalJs.kendo = window.GlobalJs.kendo || kendo;})"]
    }
    , {
        url: "libs_/uibuilder/uibuilder.js",
        id: "uibuilderlib",
        type: "js",
        onloadFunc: []
    }
    , {
        url: "notification_/js/createMileStone.js",
        id: "createMileStone",
        type: "js",
        onloadFunc: ["GlobalJs.StartMilestone"]
    }
    , {
        url: "adatatypes_/js/ribbon.js",
        id: "RibbonJs",
        type: "js",
        onloadFunc: ["GlobalJs.StartADT"]
    }
    , {
        url: "accentgold_/Script/ags.core.js",
        id: "AgsCore",
        type: "js",
        onloadFunc: []
    }
    , {
        url: "bpmn_/js/NotificationSetting.js",
        id: "NotificationSetting",
        type: "js",
        onloadFunc: []
    }
    , {
        url: "accentgold_/Script/attachdocs.js",
        id: "AttachmentDocuments",
        type: "js",
        onloadFunc: ["GlobalJs.DocsJs.Init"]
    }
    , {
        url: "ddsm_customercontrolfix.js",
        id: "CustomerControlFix",
        type: "js",
        onloadFunc: []
    }
    , {
        url: "notification_/js/createNotification.js",
        id: "createNotification",
        type: "js",
        onloadFunc: ["GlobalJs.StartNotification"]
    },
    {
        url: "accentgold_/Script/capLimitAutomation.js",
        id: "capLimitAutomation",
        type: "js",
        onloadFunc: ["Init"]
    }
    , {
        url: "kendoui_/kendo.common.min.css",
        id: "KendoCommonCss",
        type: "css",
        onloadFunc: []
    }
    , {
        url: "kendoui_/kendo.default.min.css",
        id: "KendoDefaultCss",
        type: "css",
        onloadFunc: []
    }
    , {
        url: "kendoui_/kendo.office365.min.css",
        id: "KendoOffice365Css",
        type: "css",
        onloadFunc: []
    }
    , {
        url: "kendoui_/custom.kendo.office365.css",
        id: "customKendoOffice365Style",
        type: "css",
        onloadFunc: []
    }
    , {
        url: "notification_/css/notific.css",
        id: "NotificCss",
        type: "css",
        onloadFunc: []
    },
    {
        url: "accentgold_/DetailedConsumption/style.css",
        id: "DetailedConsumptionStyle",
        type: "css",
        onloadFunc: []
    }
];

GlobalJs.eventsList["global"] = {
    OnSave: [{
        logicalName: "page",
        eventFunc: []
    }],
    OnChange: [{
        logicalName: "all",
        eventFunc: []
    }],
    fireOnChange: [{
        logicalName: "all",
        eventFunc: []
    }],
    TabStateChange: [{
        logicalName: "all",
        eventFunc: []
    }],
    OnReadyStateComplete: [{
        logicalName: "all",
        eventFunc: []
    }],
    PreSearch: [{
        logicalName: "all",
        eventFunc: []
    }]

};

//Init GlobalJs
GlobalJs.GlobalInit();



//Old scripts

function getCustomControlMetadata(entityName, callback) {
    callAction(entityName, callback);
}
function callAction(entityName, callback) {
    if (!entityName || entityName == "") return;
    //var organizationUrl = Xrm.Page.context.getClientUrl();
    var data = {
        "EntityName": entityName,
    };
    //debugger;
    var query = GlobalJs.clientUrl + GlobalJs.baseUrl + "ddsm_DDSMGetCustomControlMetadata";
    var req = new XMLHttpRequest();
    req.open("POST", query, true);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.setRequestHeader("OData-MaxVersion", "4.0");
    req.setRequestHeader("OData-Version", "4.0");
    req.onreadystatechange = function () {
        if (this.readyState == 4) {
            req.onreadystatechange = null;
            if (this.status == 200) {
                var data = window.top.JSON.parse(this.response);
                var dat = window.top.JSON.parse(data.Result);
                if (callback) {
                    callback(dat);
                }

                console.log("Action called successfully");
            } else {
                console.error("callAction error:" + this.response);
            }
        }
    };
    req.send(window.JSON.stringify(data));
}
window.top.ADT = {};
window.top.ADT.getControlsMetadata = callAction;