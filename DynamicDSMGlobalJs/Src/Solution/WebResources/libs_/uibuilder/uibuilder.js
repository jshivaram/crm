; (function () {
    window.uibuilder = {
        buildControls: buildControls
    };

    function buildControls($container, config) {

        if (!$container || !$container.jquery || !$container.attr) {
            console.error("$container is wrong (It should be jquery selector)");
            return;
        }

        if (!$container.length) {
            console.error("$container is not found");
            return;
        }

        if (!config.Metadata || !Array.isArray(config.Metadata)) {
            console.error("metadata is wrong (it should be array)");
            return;
        }

        if (config.IsOrderBy) {
            config.Metadata.sort(function (controlMetadata1, controlMetadata2) {
                return controlMetadata1.Order - controlMetadata2.Order;
            });
        }

        var $table = $("<table/>", {
            width: "100%",
            class: "uibuilder-controls-container"
        });
        $table.appendTo($container);

        config.Metadata.forEach(function (controlMetadata) {
            var createControl = controlTypes[controlMetadata.Type];
            createControl($table, controlMetadata, config.Width);
        });
    }

    var controlTypes = {
        "text": createTextControl,
        "textarea": createTextareaControl,
        "checkbox": createCheckBoxControl,
        "number": createNumberControl,
        "datetime": createDateTimeControl,
        "combobox": createComboBoxControl,
        "dropdownlist": createDropDownList
    };

    function createTextControl($container, controlMetadata, width) {
        createBaseControl($container, controlMetadata, width, "input", {
            type: "text",
            class: "k-textbox",
            name: controlMetadata.Name,
            value: controlMetadata.DefaultValue ? controlMetadata.DefaultValue : ""
        });
    }

    function createTextareaControl($container, controlMetadata, width) {
        createBaseControl($container, controlMetadata, width, "textarea", {
            class: "k-textbox",
            name: controlMetadata.Name,
            value: controlMetadata.DefaultValue ? controlMetadata.DefaultValue : ""
        });
    }

    function createCheckBoxControl($container, controlMetadata, width) {
        var options = {
            type: "checkbox",
            // class: "k-checkbox",
            name: controlMetadata.Name
        };

        if (controlMetadata.DefaultValue) {
            options.checked = "checked";
        }

        createBaseControl($container, controlMetadata, width, "input", options);
    }

    function createNumberControl($container, controlMetadata, width) {
        var $control = createBaseControl($container, controlMetadata, width, "input", {
            type: "number",
            name: controlMetadata.Name
        });

        $control.kendoNumericTextBox({
            value: controlMetadata.DefaultValue
        });
    }

    function createDateTimeControl($container, controlMetadata, width) {
        var $control = createBaseControl($container, controlMetadata, width, "input", {
            type: "datetime-local",
            name: controlMetadata.Name
        });

        $control.kendoDateTimePicker({
            value: controlMetadata.DefaultValue
        });
    }

    function createComboBoxControl($container, controlMetadata, width) {
        var $control = createBaseControl($container, controlMetadata, width, "select", {
            name: controlMetadata.Name
        });

        $control.kendoDropDownList({
            optionLabel: "--",
            filter: "contains",
            dataSource: controlMetadata.LocalData,
            dataTextField: controlMetadata.FieldsConfig.TextField,
            dataValueField: controlMetadata.FieldsConfig.ValueField,
            value: controlMetadata.DefaultValue
        });
    }

    function createDropDownList($container, controlMetadata, width) {

        var $control = createBaseControl($container, controlMetadata, width, "select", {
            name: controlMetadata.Name
        });

        var dataSource = {
            schema: {
                data: function (response) {
                    var responsedData = deepFind(response, controlMetadata.RemoteData.ResponsePropertyPath);

                    if (controlMetadata.RemoteData.ConvertJsonToObject) {
                        responsedData = JSON.parse(responsedData);
                    }

                    return responsedData;
                }
            },
            transport: {
                read: function (options) {
                    GlobalJs.WebAPI.GetList(controlMetadata.RemoteData.Uri, controlMetadata.RemoteData.QueryOptions).then(
                        function (response) {
                            options.success(response);
                        },
                        function (error) {
                            options.error(error);
                        }
                    );
                }
            }
        };

        $control.kendoDropDownList({
            optionLabel: "--",
            filter: "contains",
            dataTextField: controlMetadata.FieldsConfig.TextField,
            dataValueField: controlMetadata.FieldsConfig.ValueField,
            dataSource: dataSource,
            dataBound: function () {
                this.value(controlMetadata.DefaultValue);
            }
        });
    }

    function createBaseControl($container, controlMetadata, width, tagName, options) {

        var $row = $("<tr/>", {
            class: "uibuilder-row"
        });
        $row.appendTo($container);

        if (!width) {
            width = {
                Label: "30%",
                Control: "70%"
            }
        }

        if (controlMetadata.Label) {
            var $labelWrapper = $("<td/>", {
                width: width.Label,
                class: "uibuilder-label"
            });
            $labelWrapper.appendTo($row);

            var $label = $("<label/>", {
                html: controlMetadata.Label
            });
            $label.appendTo($labelWrapper);
        }

        var $controlWrapper = $("<td/>", {
            width: width.Control,
            class: "uibuilder-control"
        });
        $controlWrapper.appendTo($row);

        if (controlMetadata.Required) {
            options.required = "required"
        }

        options.style = "width: 100%;";
        if (options.type === "checkbox") {
            options.style = "width: auto;";
        }

        options["additional-data"] = controlMetadata.AdditionalData;

        var $control = $("<" + tagName + "/>", options);
        $control.appendTo($controlWrapper);

        return $control;
    }

    function deepFind(obj, path) {

        if (!path) {
            return obj;
        }

        var paths = path.split('.')
            , current = obj
            , i;

        for (i = 0; i < paths.length; ++i) {
            if (current[paths[i]] == undefined) {
                return undefined;
            } else {
                current = current[paths[i]];
            }
        }
        return current;
    }

})();