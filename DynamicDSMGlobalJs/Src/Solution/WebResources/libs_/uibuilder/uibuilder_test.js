window.uibuildertest = {
    start: function () {
        var clientUrl = window.top.Xrm.Page.context.getClientUrl();

        $.ajax({
            type: "GET",
            url: clientUrl + "/WebResources/libs_/uibuilder/uibuilder_metadata.json",
            dataType: "json"
        }).done(function (response) {
            var $container = $("[name='tab_13_content']");
            uibuilder.buildControls($container, response);
        });

    }
}