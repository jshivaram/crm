/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 17);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = {
    warningClass: 'warining-message',
    informationClass: 'information-message',
    errorClass: 'error-message',
    successClass: 'success-message'
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _mainFunction = __webpack_require__(15);

var _mainFunction2 = _interopRequireDefault(_mainFunction);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

GlobalJs.StartNotification = _mainFunction2.default;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (callback) {
    if (!GlobalJs || !GlobalJs.DDSM || !GlobalJs.DDSM.Settings || !GlobalJs.DDSM.Settings.GetSettings) {
        errorMessage('Cannot get config object.');
    } else {
        GlobalJs.DDSM.Settings.GetSettings(callback);
    }
};

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _onMessageEvent = __webpack_require__(14);

var _onMessageEvent2 = _interopRequireDefault(_onMessageEvent);

var _onErrorEvent = __webpack_require__(13);

var _onErrorEvent2 = _interopRequireDefault(_onErrorEvent);

var _configRequest = __webpack_require__(2);

var _configRequest2 = _interopRequireDefault(_configRequest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*test method dependency*/
//import createNotification from '../NotificationConfiguration/createNotification'
/* */
var $ = window.$; // import onOpen from '../WebSocketConfiguration/onOpenEvent'
// import onClose from '../WebSocketConfiguration/onCloseEvent'


var dataRequest = function dataRequest() {
    (0, _configRequest2.default)(function (response) {
        try {
            var websocketUrl = response.DMNConfig.WebSocketURL;
            if (!window.ws) {
                if (!websocketUrl) {
                    // console.warn('Websocket module: Admin data field has empty or undefined websocket server url');
                    throw {};
                }
                var ws = new WebSocket(websocketUrl);
                window.ws = ws;

                // ws.onopen = onOpen;
                // ws.onclose = onClose;
                ws.onmessage = _onMessageEvent2.default;
                ws.onerror = _onErrorEvent2.default;
            }
        } catch (error) {
            // console.error('Websocket module: Failed to create websocket stream');
        }
    });
};

exports.default = dataRequest;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (notifications) {
    var length = notifications.length;
    var lastIndex = length - 1;

    $.each(notifications, function (index, item) {
        if (index !== lastIndex) {
            var isExceptNotification = checkForClassess(item, _notificationTemplateConfig2.default.errorClass, _notificationTemplateConfig2.default.warningClass);

            if (!isExceptNotification) {
                $(item).parent().remove();
            }
        }
    });
};

var _notificationTemplateConfig = __webpack_require__(0);

var _notificationTemplateConfig2 = _interopRequireDefault(_notificationTemplateConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = window.$;

function checkForClassess(target) {
    var isContains = false;

    for (var _len = arguments.length, classNames = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        classNames[_key - 1] = arguments[_key];
    }

    classNames.forEach(function (item) {
        if (isContains) {
            return;
        }

        isContains |= $(target).find('.' + item).length > 0;
    });

    return isContains;
};

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (notifications, config) {

    $.each(notifications, function (index, item) {
        var $div = $(item).parent();
        $div.css('top', config.baseTop + config.shift * index);
    });
};

var $ = window.$;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (object) {
    var $ = window.$;
    var height = 110;
    var config = {
        height: height,
        baseTop: 40,
        shift: height + 2,
        width: 330
    };

    var $doc = $(window.document);

    var $notification = $doc.find('#notification');
    if ($notification.length === 0) {
        $notification = $('<div />', {
            id: 'notification'
        });

        $doc.find('body').append($notification);
    }

    (0, _showNotification2.default)(object, $notification, config);
};

var _showNotification = __webpack_require__(7);

var _showNotification2 = _interopRequireDefault(_showNotification);

__webpack_require__(16);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _tryCreateNotification = __webpack_require__(8);

var _tryCreateNotification2 = _interopRequireDefault(_tryCreateNotification);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createNotification = function createNotification(data, $notification, config) {
    var $ = window.$;
    if (!$().kendoNotification) {
        var timeOutFunction = function timeOutFunction() {
            createNotification(data, $notification, config);
        };
        setTimeout(timeOutFunction, 1000);
    } else {
        var type = data.MessageType.toUpperCase();
        switch (type) {
            case 'WARNING':
                (0, _tryCreateNotification2.default)($notification, {
                    message: data.Message,
                    title: 'Warning'
                }, 'warning', config);
                break;

            case 'INFORMATION':
                (0, _tryCreateNotification2.default)($notification, {
                    message: data.Message,
                    title: 'In Progress'
                }, 'information', config);
                break;

            case 'SUCCESS':
                (0, _tryCreateNotification2.default)($notification, {
                    message: data.Message,
                    title: 'Completed'
                }, 'success', config);
                break;

            case 'ERROR':
                (0, _tryCreateNotification2.default)($notification, {
                    message: data.Message,
                    title: 'Error'
                }, 'error', config);
                break;
        }
    }
};
exports.default = createNotification;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _notificationTemplateConfig = __webpack_require__(0);

var _notificationTemplateConfig2 = _interopRequireDefault(_notificationTemplateConfig);

var _removeDivNotifications = __webpack_require__(4);

var _removeDivNotifications2 = _interopRequireDefault(_removeDivNotifications);

var _sortDivNotifications = __webpack_require__(5);

var _sortDivNotifications2 = _interopRequireDefault(_sortDivNotifications);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function tryCreateNotification(notification, dataObject, type, config) {
    try {
        var notificationElement = $(notification).data("kendoNotification");

        if (!notificationElement) {
            initNotification(notification, config);
        }
        showMessage(notification, dataObject, type);
    } catch (error) {
        // console.error('Something wrong when try to create notification component or show notification');
    }
}

function showMessage(notification, object, type) {
    var $ = window.$;
    $(notification).data("kendoNotification").show(object, type);
}

function initNotification(target, config) {
    var $ = window.$;
    $(target).kendoNotification({
        hide: onHide(config),
        show: onShow(config),
        stacking: "down",
        position: {
            pinned: true,
            top: config.baseTop //,
            //right: 20
        },
        autoHideAfter: 0,
        width: config.width,
        height: config.height,
        templates: [{
            type: "warning",
            template: '<div class="' + _notificationTemplateConfig2.default.warningClass + '"><h3 class="notification-header">#= title #</h3><p class="notification-message">#= message #</p></div>'
        }, {
            type: "information",
            template: '<div class="' + _notificationTemplateConfig2.default.informationClass + '"><h3 class="notification-header">#= title #</h3><p class="notification-message">#= message #</p></div>'
        }, {
            type: "error",
            template: '<div class="' + _notificationTemplateConfig2.default.errorClass + '"><h3 class="notification-header">#= title #</h3><p class="notification-message">#= message #</p></div>'
        }, {
            type: "success",
            template: '<div class="' + _notificationTemplateConfig2.default.successClass + '"><h3 class="notification-header">#= title #</h3><p class="notification-message">#= message #</p></div>'
        }]
    });
}

function onHide(config) {
    var $ = window.$;
    return function () {
        var elements = this.getNotifications();
        (0, _sortDivNotifications2.default)(elements, config);
    };
}

function onShow(config) {
    return function (e) {
        var elements = this.getNotifications();
        (0, _removeDivNotifications2.default)(elements);

        elements = this.getNotifications();
        (0, _sortDivNotifications2.default)(elements, config);
    };
}

exports.default = tryCreateNotification;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (notificationData) {
    try {
        var currentEntityName = void 0;
        try {
            currentEntityName = Xrm.Page.data.entity.getEntityName().toUpperCase();
        } catch (error) {
            // console.warn('Websocket module: Cannot get Xrm.Page.data.entity.getEntityName. Filter condition assigned as false.')
            return false;
        }

        var entityName = notificationData.PrimaryEntityName.toUpperCase();
        var isCurrentEntity = entityName === currentEntityName;

        if (!isCurrentEntity) {
            return false;
            ///we no need to check farther if this condition is false.
        }

        var notificationId = notificationData.RecordId.toUpperCase();

        var currentEntityId = void 0;
        try {
            currentEntityId = Xrm.Page.data.entity.getId();
        } catch (error) {
            // console.warn('Websocket module: Cannot get Xrm.Page.data.entity.getId. Filter condition assigned as false.')
            return false;
        }
        currentEntityId = currentEntityId.substring(1, currentEntityId.length - 1).toUpperCase();

        var isCurrentEntityId = notificationId === currentEntityId;

        return isCurrentEntity && isCurrentEntityId;
    } catch (error) {
        // console.error('Websocket module: Wrong entity record check. Condition assigned as false');
        return false;
    }
};

function tryGetAccountNameFromContentFrame(contentFrameId) {
    try {
        return window.parent.document.getElementById(contentFrameId).contentWindow.Xrm.Page.data.entity.getEntityName();
    } catch (error) {
        return null;
    }
}
// function tryGetAccountNameFromContentFrame(contentFrameId) {
//     try {
//         return window.parent.document.getElementById(contentFrameId).contentWindow.Xrm.Page.data.entity.getEntityName();
//     } catch (error) {
//         return null;
//     }
// }

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (notificationData) {
    try {
        var currentEntityName = void 0;
        try {
            currentEntityName = Xrm.Page.data.entity.getEntityName().toUpperCase();
        } catch (error) {
            // console.warn('Websocket module: Cannot get Xrm.Page.data.entity.getEntityName. Filter condition assigned as false.')
            return false;
        }
        return currentEntityName === 'DDSM_DATAUPLOADER';
    } catch (error) {
        // console.error('Websocket module: Wrong "ddsm datauploader check. Filter condition assigned as false');
        return false;
    }
};

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (dataObject) {
    if (!dataObject.Message) {
        // console.log('Websocket module: Empty message.');
        return false;
    }
    var isValidMessage = (0, _currentEntity2.default)(dataObject) || (0, _dataUploaderCheck2.default)(dataObject) || (0, _userIdCheck2.default)(dataObject);

    return isValidMessage;
};

var _userIdCheck = __webpack_require__(12);

var _userIdCheck2 = _interopRequireDefault(_userIdCheck);

var _currentEntity = __webpack_require__(9);

var _currentEntity2 = _interopRequireDefault(_currentEntity);

var _dataUploaderCheck = __webpack_require__(10);

var _dataUploaderCheck2 = _interopRequireDefault(_dataUploaderCheck);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (notificationData) {
    try {
        var id = void 0;
        try {
            id = Xrm.Page.context.getUserId();
        } catch (error) {
            // console.warn('Websocket module:  Xrm.Page.context.getUserId. Filter condition assigned as false.')
            return false;
        }
        var userId = id.substring(1, id.length - 1).toUpperCase();

        var messageUser = notificationData.UserId.toUpperCase();

        return messageUser === userId;
    } catch (error) {
        // console.warn('Websocket module: Error when check user id equality. Filter condition assigned as false');
        return false;
    }
};

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var event = function event(error) {
    // console.error('Websocket module: error ws connect');
};

exports.default = event;

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createNotification = __webpack_require__(6);

var _createNotification2 = _interopRequireDefault(_createNotification);

var _mainCondition = __webpack_require__(11);

var _mainCondition2 = _interopRequireDefault(_mainCondition);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//let appendToSideMenuNotification = require('../SideMenuFill/appendMessageToSideMenu');

//let events = [createNotification,appendToSideMenuNotification ];
var event = function event(e) {
    try {
        var jsonData = e.data;
        var parsedData = JSON.parse(jsonData);
        var isValidMessage = (0, _mainCondition2.default)(parsedData);

        if (isValidMessage) {
            (0, _createNotification2.default)(parsedData);
        }
    } catch (error) {
        // console.error("Websocket module: Error when callback websocket message");
    }
};

exports.default = event;

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _dataRequest = __webpack_require__(3);

var _dataRequest2 = _interopRequireDefault(_dataRequest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//var initMenu = require('./NotificationMenu/initMenu');

var mainFunction = function mainFunction() {
    try {
        (0, _dataRequest2.default)();
    } catch (error) {}
    // console.error('Websocket module: Error before sending request and create socket connection ');
    //console.log(error);

    // try {
    //     initMenu();
    // } catch (error) {
    //     console.log(error);
    // }
};

exports.default = mainFunction;

/***/ }),
/* 16 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ })
/******/ ]);
//# sourceMappingURL=CreateNotification_develop.js.map