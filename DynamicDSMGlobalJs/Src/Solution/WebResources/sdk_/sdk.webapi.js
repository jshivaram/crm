'use strict';
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['WebAPI'], factory);
    } else if (typeof exports === 'object') {
        // Node, CommonJS-like
        module.exports = factory(require('https'), require('url'));
    } else {
        // Browser globals (root is window)
        root.WebAPI = factory(root.WebAPI);
    }
}(this, function (https, url) {
    return (function () {
        function WebAPI(config) {
            this.config = config;
            if (typeof module !== 'undefined' && module.exports) {
                this.node = true;
                this.https = https;
                this.urllib = url;
                this._GetHttpRequest = this._GetHttpRequestHTTPS;
            } else {
                this.node = false;
                this._GetHttpRequest = this._GetHttpRequestXMLHTTPRequest;
            }
            return this;
        }

// Basic Methods
        WebAPI.prototype._reID = /^(([A-Za-z0-9]{8})-([A-Za-z0-9]{4})-([A-Za-z0-9]{4})-([A-Za-z0-9]{4})-([A-Za-z0-9]{12}))$/;
        WebAPI.prototype._relationType = {0:"OneToManyRelationships",1:"ManyToManyRelationships",2:"ManyToOneRelationships"};
        WebAPI.prototype._log = function (category, message,data) {

            var logger = function(category,message,data) { console.log(category +':' + message)};
            if ((this.config.Log != null) && (this.config.Log.Logger))
                logger = this.config.Log.Logger;

            if ((this.config.Log != null) && (this.config.Log[category] == true))
                logger(category,message,data);

        };
        WebAPI.prototype._restParam = function (func, startIndex) {
            startIndex = startIndex == null ? func.length - 1 : +startIndex;
            return function () {
                var length = Math.max(arguments.length - startIndex, 0);
                var rest = Array(length);
                for (var index = 0; index < length; index++) {
                    rest[index] = arguments[index + startIndex];
                }
                switch (startIndex) {
                    case 0:
                        return func.call(this, rest);
                    case 1:
                        return func.call(this, arguments[0], rest);
                }
            };
        };
        WebAPI.prototype.whilst = function (test, iterator, callback) {
            if (test()) {
                var next = this._restParam(function (err, args) {
                    if (err) {
                        callback(err);
                    } else if (test.apply(this, args)) {
                        iterator(next);
                    } else {
                        callback.apply(null, [null].concat(args));
                    }
                });
                iterator(next);
            } else {
                callback(null);
            }
        };
        WebAPI.prototype.GetList = function (uri, QueryOptions) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var url = self._BuildQueryURL(uri, QueryOptions, self.config);
                self._GetHttpRequest(self.config, "GET", url, {
                    'headers': self._BuildQueryHeaders(QueryOptions, self.config)
                }, function (err, res) {
                    if (err != false) {
                        self._log('Errors','GetList Error:',res);
                        reject(res);
                    } else {
                        var data = JSON.parse(res.response, WebAPI.prototype._DateReviver);
                        var nextLink = data['@odata.nextLink'];
                        var recordCount = data['@odata.count'];
                        var response = {
                            List: data.value,
                            Count: recordCount
                        };
                        if ((QueryOptions != null) && (QueryOptions.RecordAction != null))
                        {
                            response.List.forEach(function(record){
                                QueryOptions.RecordAction(record);
                            });
                            response.List = [];
                        }
                        if ((QueryOptions != null) && (QueryOptions.PageAction != null))
                        {
                            QueryOptions.PageAction(response.List);
                            response.List = [];
                        }
                        if (nextLink === 'undefined') {
                            resolve(response);
                        } else {
                            self.whilst(function () {
                                return (nextLink !== undefined);
                            }, function (callback) {
                                self._GetHttpRequest(self.config, "GET", nextLink, {
                                    'headers': self._BuildQueryHeaders(QueryOptions, self.config)
                                }, function (err, res) {
                                    if (err == false) {
                                        data = JSON.parse(res.response, WebAPI.prototype._DateReviver);
                                        nextLink = data['@odata.nextLink'];
                                        response.List = response.List.concat(data.value);
                                        if ((QueryOptions != null) && (QueryOptions.RecordAction != null))
                                        {
                                            response.List.forEach(function(record){
                                                QueryOptions.RecordAction(record);
                                            });
                                            response.List = [];
                                        }
                                        if ((QueryOptions != null) && (QueryOptions.PageAction != null))
                                        {
                                            QueryOptions.PageAction(response.List);
                                            response.List = [];
                                        }
                                        callback(null, response.List.length);
                                    } else {
                                        self._log('Errors','GetList Error2',res);
                                        callback('err', 0);
                                    }
                                });
                            }, function (err, n) {
                                resolve(response);
                            });
                        }
                    }
                });
            });
        };
        WebAPI.prototype.Get = function (entityCollection, entityID, QueryOptions) {
            /// <summary>
            /// Get a collection or an instance of given entity type
            /// </summary>
            /// <param name="entityCollection" type="type">Entity logical name to retrieve including plural suffix</param>
            /// <param name="entityID" type="type">ID of requested record, or null for collection based on QueryOptions.Filter</param>
            /// <param name="QueryOptions" type="type"></param>
            var self = this;
            return new Promise(function (resolve, reject) {
                var url = null;
                if (entityID == null)
                    url = self._BuildQueryURL(entityCollection , QueryOptions, self.config);
                else
                    url  = self._BuildQueryURL(entityCollection + "(" + entityID.toString().replace(/[{}]/g, "") + ")", QueryOptions, self.config);
                self._GetHttpRequest(self.config, "GET", url, {
                    'headers': self._BuildQueryHeaders(QueryOptions, self.config)
                }, function (err, res) {
                    if (err != false) {
                        self._log('Errors','Get Error',res);
                        reject(res);
                    } else {
                        var data = JSON.parse(res.response, WebAPI.prototype._DateReviver)
                        resolve(data);
                    }
                });
            });
        };
        WebAPI.prototype.GetCount = function (uri, QueryOptions) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var url = self._BuildQueryURL(uri + "/$count", QueryOptions, self.config);
                self._GetHttpRequest(self.config, "GET", url, {
                    'headers': self._BuildQueryHeaders(QueryOptions, self.config)
                }, function (err, res) {
                    if (err != false) {
                        self._log('Errors','GetCount Error',res);
                        reject(res);
                    } else {
                        var data = parseInt(res.response);
                        resolve(data);
                    }
                });
            });
        };
        WebAPI.prototype.Create = function (entityCollection, data) {
            /// <summary>
            /// Create a record
            /// </summary>
            /// <param name="entityCollection" type="type">Plural name of entity to create</param>
            /// <param name="data" type="type">JSON object with attributes for the record to create</param>
            var self = this;
            return new Promise(function (resolve, reject) {
                var url = self.config.APIUrl + entityCollection;
                self._log('ODataUrl',url);
                self._GetHttpRequest(self.config, "POST", url, {
                    'data': JSON.stringify(data)
                }, function (err, res) {
                    if (err != false) {
                        self._log('Errors','Create Error',res);
                        reject(res);
                    } else {
                        resolve(/\(([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})\)/g.exec(res.headers["odata-entityid"])[1]);
                    }
                });
            });
        };
        WebAPI.prototype.Update = function (entityCollection, key, data, Upsert) {
            /// <summary>
            /// Update an existing record or create a new if record does not exist (Upsert)
            /// </summary>
            /// <param name="entityCollection" type="type">Plural name of entity to update</param>
            /// <param name="key" type="type">Key to locate existing record</param>
            /// <param name="data" type="type">JSON object with attributes for the record to upddate</param>
            /// <param name="Upsert" type="type">Set to true to enable upsert functionality, which creates a new record if key is not found</param>
            var self = this;
            return new Promise(function (resolve, reject) {
                var url = self.config.APIUrl + entityCollection + '(' + key.replace(/[{}]/g, "") + ')';
                self._log('ODataUrl',url);
                var payload = {
                    "data": JSON.stringify(data),
                    "headers": {}
                };
                if (Upsert == false) payload["headers"]["If-Match"] = "*";
                self._GetHttpRequest(self.config, "PATCH", url, payload, function (err, res) {
                    if (err != false) {
                        self._log('Errors','Update Error',res);
                        reject(res);
                    } else {
                        var response = {};
                        var parseEntityID = /\(([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})\)/g.exec(res.headers["odata-entityid"]);

                        if (parseEntityID != null)
                            response.EntityID = parseEntityID[1];

                        resolve(response);
                    }
                });
            });
        };
        WebAPI.prototype.Delete = function (entityCollection, entityID) {
            /// <summary>
            /// Delete an existing record
            /// </summary>
            /// <param name="entityCollection" type="type">Plural name of entity to delete</param>
            /// <param name="entityID" type="type">ID of record to delete</param>
            var self = this;
            return new Promise(function (resolve, reject) {
                var url = self.config.APIUrl + entityCollection + '(' + entityID.replace(/[{}]/g, "") + ')';
                self._log('ODataUrl',url);
                self._GetHttpRequest(self.config, "DELETE", url, {}, function (err, res) {
                    if (err != false) {
                        self._log('Errors','Delete Error',res);
                        reject(res);
                    } else {
                        resolve(true);
                    }
                });
            });
        };
        WebAPI.prototype.Associate = function (fromEntitycollection, fromEntityID, navProperty, toEntityCollection, toEntityID) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var url = self.config.APIUrl + fromEntitycollection + '(' + fromEntityID.replace(/[{}]/g, "") + ')/' + navProperty + '/$ref';
                self._log('ODataUrl',url);
                var payload = {
                    'data': JSON.stringify({
                        '@odata.id': self.config.APIUrl + toEntityCollection + '(' + toEntityID.replace(/[{}]/g, "") + ')'
                    })
                };
                self._GetHttpRequest(self.config, 'POST', url, payload, function (err, res) {
                    if (err != false) {
                        self._log('Errors','Associate Error',res);
                        reject(res);
                    } else {
                        resolve(true);
                    }
                });
            });
        };
        WebAPI.prototype.DeleteAssociation = function (fromEntitycollection, fromEntityID, navProperty, toEntityCollection, toEntityID) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var url = self.config.APIUrl + fromEntitycollection + '(' + fromEntityID.replace(/[{}]/g, "") +
                    ')/' + navProperty + '/$ref';

                if (toEntityCollection != null && toEntityID != null)
                    url += '?$id=' + self.config.APIUrl + toEntityCollection + '(' + toEntityID.replace(/[{}]/g, "") + ')';

                self._log('ODataUrl',url);
                self._GetHtpRequest(self.config, 'DELETE', url, {}, function (err, res) {
                    if (err != false) {
                        self._log('Errors','DeleteAssociation Error',res);
                        reject(res);
                    } else {
                        resolve(true);
                    }
                });
            });
        };
        WebAPI.prototype.ExecuteFunction = function (functionName, parameters, entityCollection, entityID) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var parmvars = [];
                var parmvalues = [];
                var parmcount = 1;
                if (parameters != null) {
                    Object.keys(parameters).forEach(function (key) {
                        var val = parameters[key];
                        parmvars.push(key + "=" + "@p" + parmcount.toString());
                        if (typeof val === 'string' || val instanceof String) parmvalues.push("@p" + parmcount.toString() + "='" + val + "'");
                        else parmvalues.push("@p" + parmcount.toString() + "=" + val);
                        parmcount++;
                    });
                }
                var url = "";
                if (parameters != null) {
                    url = self.config.APIUrl + functionName + "(" + parmvars.join(",") + ")?" + parmvalues.join("&");
                    if (entityCollection != null) url = self.config.APIUrl + entityCollection + "(" + entityID.toString().replace(/[{}]/g, "") + ")" + functionName + "(" + parmvars.join(",") + ")?" + parmvalues.join("&");
                } else {
                    url = self.config.APIUrl + functionName + "()";
                    if (entityCollection != null) url = self.config.APIUrl + entityCollection + "(" + entityID.toString().replace(/[{}]/g, "") + ")" + functionName + "()";
                }
                self._log('ODataUrl',url);
                self._GetHttpRequest(self.config, "GET", url, {}, function (err, res) {
                    if (err != false) {
                        self._log('Errors','ExecuteFunction Error',res);
                        reject(res);
                    } else {
                        var data = JSON.parse(res.response, WebAPI.prototype._DateReviver)
                        resolve(data);
                    }
                });
            });
        };
        WebAPI.prototype.ExecuteAction = function (actionName, data, entityCollection, entityID) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var url = self.config.APIUrl + actionName;
                if (entityCollection != null) url = self.config.APIUrl + entityCollection + "(" + entityID.toString().replace(/[{}]/g, "") + ")/" + actionName;
                self._log('ODataUrl',url);
                self._GetHttpRequest(self.config, "POST", url, {
                    "data": JSON.stringify(data)
                }, function (err, res) {
                    if (err != false) {
                        self._log('Errors','ExecuteAction Error',res);
                        reject(res);
                    } else {
                        if (res.response == "") {
                            resolve(null);
                        } else {
                            var data = JSON.parse(res.response, WebAPI.prototype._DateReviver)
                            resolve(data);
                        }
                    }
                });
            });
        };
        WebAPI.prototype._BuildQueryURL = function (uri, queryOptions, config) {
            var fullurl = config.APIUrl + uri;
            var qs = [];
            if (queryOptions != null) {
                if (queryOptions.Select != null) qs.push("$select=" + encodeURI(queryOptions.Select.join(",")));
                if (queryOptions.OrderBy != null) qs.push("$orderby=" + encodeURI(queryOptions.OrderBy.join(",")));
                if (queryOptions.Filter != null) qs.push("$filter=" + encodeURI(queryOptions.Filter));
                if (queryOptions.Expand != null)
                {
                    var expands = [];
                    queryOptions.Expand.forEach(function (ex){
                        if ((ex.Select != null) || (ex.Filter != null) || (ex.OrderBy != null) || (ex.Top != null))
                        {
                            var qsExpand = [];
                            if (ex.Select != null) qsExpand.push("$select=" + ex.Select.join(","));
                            if (ex.OrderBy != null) qsExpand.push("$orderby=" + ex.OrderBy.join(","));
                            if (ex.Filter != null) qsExpand.push("$filter=" + ex.Filter);
                            if (ex.Top > 0) qsExpand.push("$top=" + ex.Top);
                            expands.push(ex.Property + "(" +qsExpand.join(";") + ")");
                        }
                        else
                            expands.push(ex.Property);
                    });
                    qs.push("$expand="+ encodeURI(expands.join(",")));
                }
                if (queryOptions.IncludeCount) qs.push("$count=true");
                if (queryOptions.Skip > 0) qs.push("skip=" + encodeURI(queryOptions.Skip));
                if (queryOptions.Top > 0) qs.push("$top=" + encodeURI(queryOptions.Top));
                if (queryOptions.SystemQuery != null) qs.push("savedQuery=" + encodeURI(queryOptions.SystemQuery));
                if (queryOptions.UserQuery != null) qs.push("userQuery=" + encodeURI(queryOptions.UserQuery));
                if (queryOptions.FetchXml != null) qs.push("fetchXml=" + encodeURI(queryOptions.FetchXml));
            }
            if (qs.length > 0) fullurl += "?" + qs.join("&")
            this._log('ODataUrl',fullurl);
            return fullurl;
        };
        WebAPI.prototype._BuildQueryHeaders = function (queryOptions, config) {
            var headers = {};
            if (queryOptions != null) {
                if (queryOptions.FormattedValues == true) headers['Prefer'] = 'odata.include-annotations="OData.Community.Display.V1.FormattedValue"';
            }
            return headers;
        };
        WebAPI.prototype.parseResponseHeaders = function (headerStr) {
            var headers = {};
            if (!headerStr) {
                return headers;
            }
            var headerPairs = headerStr.split('\u000d\u000a');
            for (var i = 0; i < headerPairs.length; i++) {
                var headerPair = headerPairs[i];
                // Can't use split() here because it does the wrong thing
                // if the header value has the string ": " in it.
                var index = headerPair.indexOf('\u003a\u0020');
                if (index > 0) {
                    var key = headerPair.substring(0, index);
                    var val = headerPair.substring(index + 2);
                    headers[key.toLowerCase()] = val;
                }
            }
            return headers;
        };
        WebAPI.prototype._GetHttpRequestXMLHTTPRequest = function (config, method, url, payload, callback) {
            var self = this;
            var req = new XMLHttpRequest();
            //req.open(method, encodeURI(url), true);
            req.open(method, url, true);
            if (config.AccessToken != null) req.setRequestHeader("Authorization", "Bearer " + config.AccessToken);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            if (config.callerId) req.setRequestHeader("MSCRMCallerID", config.callerId);
            if (config.CallerID) req.setRequestHeader("MSCRMCallerID", config.CallerID);
            if (['POST', 'PUT', 'PATCH'].indexOf(method) >= 0) {
                req.setRequestHeader("Content-Length", payload.data.length);
                req.setRequestHeader("Content-Type", "application/json");
            }
            if (payload.headers !== 'undefined') {
                for (var name in payload.headers) {
                    req.setRequestHeader(name, payload.headers[name]);
                }
            }
            req.onreadystatechange = function () {
                if (this.readyState == 4 /* complete */ ) {
                    req.onreadystatechange = null;
                    if ((this.status >= 200) && (this.status < 300)) {
                        callback(false, {
                            'response': this.response,
                            'headers': self.parseResponseHeaders(this.getAllResponseHeaders())
                        });
                    } else {
                        callback(true, this)
                    }
                }
            };
            if (['POST', 'PUT', 'PATCH'].indexOf(method) >= 0) {
                req.send(payload.data);
            } else {
                req.send();
            }
        };
        WebAPI.prototype._GetHttpRequestHTTPS = function (config, method, url, payload, callback) {
            var parsed_url = this.urllib.parse(url);
            var options = {
                hostname: parsed_url.hostname,
                port: 443,
                path: parsed_url.path,
                method: method,
                headers: {
                    "Accept": "application/json",
                    "OData-MaxVersion": "4.0",
                    "OData-Version": "4.0",
                }
            }
            if (['POST', 'PUT', 'PATCH'].indexOf(method) >= 0) {
                options.headers['Content-Length'] = payload.data.length;
                options.headers['Content-Type'] = 'application/json';
            }
            if (config.callerId) options.headers["MSCRMCallerID"] = config.callerId;
            if (config.AccessToken != null) options.headers["Authorization"] = "Bearer " + config.AccessToken;
            if (payload.headers != undefined) {
                for (var name in payload.headers) {
                    options.headers[name] = payload.headers[name];
                }
            }
            var req = this.https.request(options, function (res) {
                var body = '';
                res.setEncoding('utf8');
                res.on('data', function (chunk) {
                    body += chunk;
                })
                res.on('end', function () {
                    if ((res.statusCode >= 200) && (res.statusCode < 300)) {
                        callback(false, {
                            'response': body,
                            'headers': res.headers
                        });
                    } else {
                        callback(true, {
                            'response': body,
                            'headers': res.headers
                        });
                    }
                });
            });
            req.on('error', function (err) {
                callback(true, err);
            })
            if (['POST', 'PUT', 'PATCH'].indexOf(method) >= 0) {
                req.write(payload.data);
            }
            req.end();
        };
        WebAPI.prototype._DateReviver = function (key, value) {
            var a;
            if (typeof value === 'string') {
                a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                if (a) {
                    return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6]));
                }
            }
            return value;
        };

// Metadata Methods
        WebAPI.prototype._GetOptionSetByName = function (optionSetName) {
            var self = this;
            return new Promise(function (resolve, reject) {

                self.GetList('GlobalOptionSetDefinitions',
                    {Select:['Name']}).then(
                    function (r)
                    {
                        r.List.forEach(function(set)
                            {
                                if (set.Name == optionSetName)
                                {
                                    self.Get('GlobalOptionSetDefinitions',set.MetadataId).then(
                                        function(res){
                                            resolve(res);
                                        },function(err){console.log(err)}
                                    )
                                }
                            }
                        )
                    },
                    function(e){
                        console.log(e)
                        reject(e)
                    })
            });
        };
        WebAPI.prototype.GetOptionSetUserLabels = function (optionSetName) {
            var self = this;
            return new Promise(function (resolve, reject) {

                self._GetOptionSetByName(optionSetName).then(
                    function (result)
                    {
                        var displayList = new Array();
                        result.Options.forEach(function (option)
                        {
                            var displayOption = new Object;
                            displayOption.Value = option.Value;
                            displayOption.Label = option.Label.UserLocalizedLabel.Label;
                            displayList.push(displayOption);
                        });
                        resolve(displayList);
                    }
                    ,
                    function (err)
                    {
                        console.log(err)
                        reject(err);
                    }
                );
            });

        }
        WebAPI.prototype.GetEntityDisplayNameList = function (QueryOptions, LCID) {
            var self = this;
            return new Promise(function (resolve, reject) {

                if((QueryOptions != null)) {
                    if (!!QueryOptions.Filter) {
                        QueryOptions.Filter = '((IsPrivate eq false) and (' + QueryOptions.Filter + '))';
                    } else {
                        QueryOptions.Filter = 'IsPrivate eq false';
                    }

                    if(!QueryOptions.Select) {
                        QueryOptions.Select = ['MetadataId','EntitySetName','DisplayName',
                            'DisplayCollectionName','LogicalName','LogicalCollectionName','PrimaryIdAttribute'];
                    }

                } else {
                    QueryOptions = {Filter:'IsPrivate eq false',Select:['MetadataId','EntitySetName','DisplayName',
                        'DisplayCollectionName','LogicalName','LogicalCollectionName','PrimaryIdAttribute']};
                }

                self.GetList('EntityDefinitions', QueryOptions).then(
                    function (r)
                    {
                        var list = new Array();
                        r.List.forEach(function(entity) {
                            var edm = new Object();
                            if (!!entity.MetadataId)
                                edm.MetadataId = entity.MetadataId;
                            if (!!entity.EntitySetNameEntitySetName)
                                edm.EntitySetName = entity.EntitySetName;
                            if (!!entity.LogicalName)
                                edm.LogicalName = entity.LogicalName;
                            if (!!entity.LogicalCollectionName)
                                edm.LogicalCollectionName = entity.LogicalCollectionName;
                            if (!!entity.PrimaryIdAttribute)
                                edm.PrimaryIdAttribute = entity.PrimaryIdAttribute;
                            if (!!entity.DisplayName)
                            {
                                if ((entity.DisplayName.LocalizedLabels != null) && (entity.DisplayName.LocalizedLabels.length > 0)) {
                                    edm.DisplayName = entity.DisplayName.LocalizedLabels[0].Label;
                                    if (LCID != null)
                                        entity.DisplayName.LocalizedLabels.forEach(function (label) {
                                            if (label.LanguageCode == LCID) edm.DisplayName = label.Label
                                    });
                                }
                                else
                                    edm.DisplayName = edm.LogicalName;
                            }

                                if (!!entity.DisplayCollectionName) {
                                    if ((entity.DisplayCollectionName.LocalizedLabels != null) && (entity.DisplayCollectionName.LocalizedLabels.length > 0))
                                    {
                                        edm.DisplayCollectionName = entity.DisplayCollectionName.LocalizedLabels[0].Label;
                                        if (LCID != null)
                                            entity.DisplayCollectionName.LocalizedLabels.forEach(function (label) { if (label.LanguageCode == LCID) edm.DisplayCollectionName = label.Label});
                                    }
                                    else
                                        edm.DisplayCollectionName = entity.LogicalCollectionName;
                                }

                                if (!!edm.DisplayName && !!edm.LogicalName)
                                    edm.LogicalDisplayName = edm.DisplayName +'(' + edm.LogicalName + ')';

                                if (!!edm.DisplayCollectionName && !!edm.LogicalCollectionName)
                                    edm.LogicalDisplayCollectionName = edm.DisplayCollectionName +'(' + edm.LogicalCollectionName + ')';

                                list.push(edm);
                            }
                        )
                        resolve(list);
                    },
                    function(e){
                        console.log(e)
                        reject(e)
                    })
            });
        };
        WebAPI.prototype.GetAttributeDisplayNameList = function (entityID,LCID) {
            var self = this;
            return new Promise(function (resolve, reject) {
                var _entityID = (self._reID.test(entityID.toString().replace(/[{}]/g,'')))? entityID.toString().replace(/[{}]/g,''): 'LogicalName=\'' + entityID.toString() + '\'';
                self.GetList('EntityDefinitions('+ _entityID + ')/Attributes',
                    {Filter:'((IsValidForRead eq true) and (AttributeOf eq null))',Select:['MetadataId','DisplayName','LogicalName','SchemaName','AttributeType','IsPrimaryId']}).then(
                    function (r)
                    {
                        var list = new Array();
                        r.List.forEach(function(attrib)
                            {
                                var edm = new Object();
                                edm.MetadataId = attrib.MetadataId;
                                edm.LogicalName = attrib.LogicalName;
                                edm.SchemaName = attrib.SchemaName;
                                edm.IsPrimaryId = attrib.IsPrimaryId;
                                edm.AttributeType = attrib.AttributeType;
                                if (attrib.AttributeType === "Lookup" || attrib.AttributeType === "Customer" || attrib.AttributeType === "Owner")
                                    edm.ODataLogicalName = "_" + attrib.LogicalName + "_value";
                                else
                                    edm.ODataLogicalName = attrib.LogicalName;

                                if ((attrib.DisplayName.LocalizedLabels != null) && (attrib.DisplayName.LocalizedLabels.length > 0))
                                {
                                    edm.DisplayName = attrib.DisplayName.LocalizedLabels[0].Label;
                                    if (LCID != null)
                                        attrib.DisplayName.LocalizedLabels.forEach(function (label) { if (label.LanguageCode == LCID) edm.DisplayName = label.Label});
                                }
                                else
                                    edm.DisplayName = edm.LogicalName;
                                edm.LogicalDisplayName = edm.DisplayName +'(' + edm.LogicalName + ')'
                                list.push(edm);
                            }
                        )
                        resolve(list);
                    },
                    function(e){
                        console.log(e)
                        reject(e)
                    })
            });
        };
        WebAPI.prototype.CopyEntityAttribute = function (fromEntityID,toEntityID,fromAttributeID,attributeType,toNames) {
            var self = this;
            return new Promise(function (resolve, reject) {

                var ec ='EntityDefinitions('+ fromEntityID.toString() + ')/Attributes('+fromAttributeID+')';
                if (attributeType == "Boolean")
                    ec += '/Microsoft.Dynamics.CRM.BooleanAttributeMetadata?$expand=OptionSet';
                if (attributeType == "Picklist")
                    ec += '/Microsoft.Dynamics.CRM.PicklistAttributeMetadata?$expand=OptionSet,GlobalOptionSet';

                self.Get(ec,null,{}).then(
                    function (r)
                    {
                        console.log(JSON.stringify(r));
                        delete r.MetadataId;
                        delete r.EntityLogicalName;
                        if (attributeType == "Boolean" )
                        {
                            r['@odata.type'] = 'Microsoft.Dynamics.CRM.BooleanAttributeMetadata';
                            delete r['OptionSet@odata.context'];
                            if (r.OptionSet != null)
                            {
                                delete r.OptionSet.Name;
                                delete r.OptionSet.MetadataId;
                                r.OptionSet.IsCustomOptionSet=true;
                            }
                        }
                        if (attributeType == "Picklist" )
                        {
                            delete r.EntityLogicalName;
                            r['@odata.type'] = 'Microsoft.Dynamics.CRM.PicklistAttributeMetadata';

                            if (r.OptionSet != null)
                            {
                                delete r['OptionSet@odata.context'];
                                delete r.OptionSet.Name;
                                delete r.OptionSet.MetadataId;
                                r.OptionSet.IsCustomOptionSet=true;
                            }
                            else
                            {
                                delete r.OptionSet;
                                r['GlobalOptionSet@odata.bind'] = self.config.APIUrl +'GlobalOptionSetDefinitions('+r.GlobalOptionSet.MetadataId+')';
                                delete r['OptionSet@odata.context'];
                                delete r.GlobalOptionSet;

                            }
                        }
                        r.LogicalName = toNames.LogicalName;
                        r.SchemaName  = toNames.SchemaName;
                        console.log(JSON.stringify(r));

                        self.Create('EntityDefinitions('+ toEntityID.toString() + ')/Attributes',r).then(
                            function (createR)
                            {
                                resolve(createR);
                            },
                            function(errorR)
                            {
                                console.log(errorR)
                                reject(errorR)
                            }
                        );
                    },
                    function(e){
                        console.log(e)
                        reject(e)
                    })
            });
        };
        WebAPI.prototype.GetRelationshipsDisplayNameList = function (entityID, relationType, QueryOptions) {
            var self = this;
            return new Promise(function (resolve, reject) {

                if(entityID == null) {
                    var _uri = 'EntityDefinitions';
                } else {
                    if(relationType == null || isNaN(Number(relationType))) {
                        relationType = 0;
                    } if(Number(relationType) < 0 || Number(relationType) > 2){
                        relationType = 0;
                    } else {
                        relationType = Number(relationType);
                    }
                    var _uri = 'EntityDefinitions('+ ((self._reID.test(entityID.toString().replace(/[{}]/g,'')))? entityID.toString().replace(/[{}]/g,''): 'LogicalName=\'' + entityID.toString() + '\'') + ')/' + self._relationType[relationType];
                }
                self.GetList(_uri, QueryOptions).then(
                    function (r)
                    {
                        var list = new Array();
                        r.List.forEach(function(attrib)
                            {
                                var edm = new Object();
                                if(!!attrib.MetadataId)
                                    edm.MetadataId = attrib.MetadataId;
                                if(!!attrib.ReferencedAttribute)
                                    edm.ReferencedAttribute = attrib.ReferencedAttribute;
                                if(!!attrib.ReferencedEntity)
                                    edm.ReferencedEntity = attrib.ReferencedEntity;
                                if(!!attrib.ReferencedEntityNavigationPropertyName)
                                    edm.ReferencedEntityNavigationPropertyName = attrib.ReferencedEntityNavigationPropertyName;
                                if(!!attrib.ReferencingAttribute)
                                    edm.ReferencingAttribute = attrib.ReferencingAttribute;
                                if(!!attrib.ReferencingEntity)
                                    edm.ReferencingEntity = attrib.ReferencingEntity;
                                if(!!attrib.RelationshipType)
                                    edm.RelationshipType = attrib.RelationshipType;
                                if(!!attrib.SchemaName)
                                    edm.SchemaName = attrib.SchemaName;
                                if(!!attrib.IsCustomRelationship)
                                    edm.IsCustomRelationship = attrib.IsCustomRelationship
                                if(!!attrib.IsValidForAdvancedFind)
                                    edm.IsValidForAdvancedFind = attrib.IsValidForAdvancedFind

                                list.push(edm);
                            }
                        )
                        resolve(list);
                    },
                    function(e){
                        console.log(e)
                        reject(e)
                    })
            });
        };

// Additional Methods


        return WebAPI;
    })();
}));
