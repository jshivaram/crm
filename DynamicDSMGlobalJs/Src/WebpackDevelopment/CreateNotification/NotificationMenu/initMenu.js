import createButton from'./createButton'
import createMenu from'./createMenu'
import setEvents from'./setEvents'
import './sideMenu.css'

var initMenu = function(){
    var target = window.parent.document.body;
    
    createButton(target);
    createMenu(target);
    setEvents();
}

export default initMenu;