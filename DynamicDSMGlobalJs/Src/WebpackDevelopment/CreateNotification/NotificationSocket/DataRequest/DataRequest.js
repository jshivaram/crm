// import onOpen from '../WebSocketConfiguration/onOpenEvent'
// import onClose from '../WebSocketConfiguration/onCloseEvent'
import onMessage from '../WebSocketConfiguration/onMessageEvent'
import onError from '../WebSocketConfiguration/onErrorEvent'

import configRequest from './configRequest'

/*test method dependency*/
//import createNotification from '../NotificationConfiguration/createNotification'
/* */
const $ = window.$;

let dataRequest = function () {
    configRequest(function (response) {
        try {
            var websocketUrl = response.DMNConfig.WebSocketURL;
            if (!window.ws) {
                if (!websocketUrl) {
                    // console.warn('Websocket module: Admin data field has empty or undefined websocket server url');
                    throw {};
                }
                var ws = new WebSocket(websocketUrl);
                window.ws = ws;

                // ws.onopen = onOpen;
                // ws.onclose = onClose;
                ws.onmessage = onMessage;
                ws.onerror = onError;
            }
        } catch (error) {
            // console.error('Websocket module: Failed to create websocket stream');
        }
    })
}

export default dataRequest;