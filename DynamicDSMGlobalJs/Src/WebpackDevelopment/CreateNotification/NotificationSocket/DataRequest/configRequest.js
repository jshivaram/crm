export default function(callback){
    if(!GlobalJs || !GlobalJs.DDSM || !GlobalJs.DDSM.Settings || !GlobalJs.DDSM.Settings.GetSettings){
        errorMessage('Cannot get config object.');
    }else{
        GlobalJs.DDSM.Settings.GetSettings(callback);
    }
}