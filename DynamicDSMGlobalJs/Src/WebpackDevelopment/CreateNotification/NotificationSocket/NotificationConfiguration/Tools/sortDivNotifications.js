const $ = window.$;

export default function (notifications, config) {
    
    $.each(notifications, function (index, item) {
        let $div = $(item).parent();
        $div.css('top', config.baseTop + config.shift * index);
    });
}
