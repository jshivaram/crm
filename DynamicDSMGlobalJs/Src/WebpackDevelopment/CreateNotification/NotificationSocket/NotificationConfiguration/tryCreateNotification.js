import templateConfig from'./notificationTemplateConfig'
import removeDivNotifications from'./Tools/removeDivNotifications'
import sortDivNotifications from'./Tools/sortDivNotifications'

function tryCreateNotification(notification, dataObject, type, config) {
    try {
        let notificationElement = $(notification).data("kendoNotification");

        if (!notificationElement) {
            initNotification(notification, config);
        }
        showMessage(notification, dataObject, type);
    } catch (error) {
        // console.error('Something wrong when try to create notification component or show notification');
    }
}

function showMessage(notification, object, type) {
    const $ = window.$;
    $(notification).data("kendoNotification").show(object, type);
}

function initNotification(target, config) {
    const $ = window.$;
    $(target).kendoNotification({
        hide: onHide(config),
        show: onShow(config),
        stacking: "down",
        position: {
            pinned: true,
            top: config.baseTop//,
            //right: 20
        },
        autoHideAfter: 0,
        width: config.width,
        height: config.height,
        templates: [{
            type: "warning",
            template: `<div class="${templateConfig.warningClass}"><h3 class="notification-header">#= title #</h3><p class="notification-message">#= message #</p></div>`
        },
        {
            type: "information",
            template: `<div class="${templateConfig.informationClass}"><h3 class="notification-header">#= title #</h3><p class="notification-message">#= message #</p></div>`
        },
        {
            type: "error",
            template: `<div class="${templateConfig.errorClass}"><h3 class="notification-header">#= title #</h3><p class="notification-message">#= message #</p></div>`
        },
        {
            type: "success",
            template: `<div class="${templateConfig.successClass}"><h3 class="notification-header">#= title #</h3><p class="notification-message">#= message #</p></div>`
        }]
    });
}


function onHide(config) {
    const $ = window.$;
    return function () {
        var elements = this.getNotifications();
        sortDivNotifications(elements, config);
    }

}

function onShow(config) {    
    return function (e) {
        let elements = this.getNotifications();
        removeDivNotifications(elements);

        elements = this.getNotifications();
        sortDivNotifications(elements, config);
    }

}

export default tryCreateNotification;