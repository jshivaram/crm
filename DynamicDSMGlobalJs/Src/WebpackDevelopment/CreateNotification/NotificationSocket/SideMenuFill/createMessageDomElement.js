import cssConfig from './cssConfigByType'
const $ = window.$;

///messageData should contain properties: 'MessageType','Message'
let createElement = function (messageData) {
    try {
        const upperCaseType = messageData.MessageType.toUpperCase();
        const configObject = cssConfig[upperCaseType];


        let paragraph = $('<p />', {
            class: 'notification-message',
            text: messageData.Message
        });

        let header = $('<h3 />', {
            class: 'notification-header',
            text: configObject.title
        })

        let dataContainer = $('<div />', {
            class: configObject.mainDivCssClass
        });

        dataContainer
            .append(header)
            .append(paragraph);

        return dataContainer;

    } catch (error) {
        // console.error('Some troubles with creating element for side-menu');
        throw error;
    }

}

export default createElement;