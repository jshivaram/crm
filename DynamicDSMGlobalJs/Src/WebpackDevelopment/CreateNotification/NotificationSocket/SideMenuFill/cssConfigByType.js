function CreateData(mainDivCssClass, titleMessage) {
    this.title = titleMessage;
    this.mainDivCssClass = mainDivCssClass;
}

///Describes title and main container css-class to notification message
let getStyleCssByMessageType = {
    'WARNING': new CreateData('warining-message message-div', 'Warning'),
    'SUCCESS': new CreateData('success-message message-div', 'Success'),
    'INFORMATION': new CreateData('information-message message-div', 'Information'),
    'ERROR': new CreateData('error-message message-div', 'Error')
}

export default getStyleCssByMessageType;
