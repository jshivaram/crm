import checkUserId from './userIdCheck'
import checkCurrentEntity from './currentEntity'
import checkDataUploader from './dataUploaderCheck'

export default function (dataObject) {
    if (!dataObject.Message) {
        // console.log('Websocket module: Empty message.');
        return false;
    }
    const isValidMessage = checkCurrentEntity(dataObject)
        || checkDataUploader(dataObject)
        || checkUserId(dataObject);

    return isValidMessage;
}