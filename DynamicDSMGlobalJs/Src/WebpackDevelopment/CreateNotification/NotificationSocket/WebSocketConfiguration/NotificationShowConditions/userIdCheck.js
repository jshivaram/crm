export default function (notificationData) {
    try {
        let id;
        try {
            id = Xrm.Page.context.getUserId();
        } catch (error) {
            // console.warn('Websocket module:  Xrm.Page.context.getUserId. Filter condition assigned as false.')
            return false;
        }
        const userId = id.substring(1, id.length - 1).toUpperCase();

        let messageUser = notificationData.UserId.toUpperCase();

        return messageUser === userId;
    } catch (error) {
        // console.warn('Websocket module: Error when check user id equality. Filter condition assigned as false');
        return false;
    }

}