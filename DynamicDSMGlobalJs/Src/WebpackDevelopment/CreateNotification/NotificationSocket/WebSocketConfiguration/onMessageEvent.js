import createNotification from '../NotificationConfiguration/createNotification'
import checkAllConditions from './NotificationShowConditions/mainCondition';
//let appendToSideMenuNotification = require('../SideMenuFill/appendMessageToSideMenu');

//let events = [createNotification,appendToSideMenuNotification ];
let event = function (e) {
    try {
        var jsonData = e.data;
        var parsedData = JSON.parse(jsonData);
        const isValidMessage = checkAllConditions(parsedData);

        if (isValidMessage) {
            createNotification(parsedData);
        }
    } catch (error) {
        // console.error("Websocket module: Error when callback websocket message");
    }
}

export default event;