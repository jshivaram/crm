var path = require('path');
var webpack = require('webpack');
const ExtractText = require('extract-text-webpack-plugin');

module.exports = {
    entry: ['./entryPoint.js'],
    output: {
        path: path.resolve(__dirname, 'prod'),
        filename: 'CreateNotification.js'
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractText.extract({
                    fallback: 'style-loader',
                    use: {
                        loader: 'css-loader',
                        options: {
                            minimize: true
                        }
                    }
                })
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            }
        ]
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        }),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            mangle: {
                screw_ie8: true,
                keep_fnames: true
            },
            compress: {
                screw_ie8: true
            },
            comments: false
        }),
        new ExtractText('../css/notific.css')
    ]
};

// path: '../../Solution/WebResources/notification_/js',