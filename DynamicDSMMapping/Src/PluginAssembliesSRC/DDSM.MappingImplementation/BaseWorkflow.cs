﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Diagnostics;
using DDSM.MappingImplementation.Dto;
using DDSM.MappingImplementation.Service;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;

namespace DDSM.MappingImplementation
{
    public abstract class BaseWorkflow : CodeActivity
    {
        public BaseDto BaseDto;
        public List<BatchResultDto> BatchResultDtoList;

        [Input("Is Create from Target")]
        [Default("false")]
        public InArgument<bool> IsCreateFromTarget { get; set; }

        [Input("Mapping Implementation")]
        [ReferenceTarget("ddsm_mappingimplementation")]
        public InArgument<EntityReference> MappingImplementation { get; set; }

        [Input("Data")]
        public InArgument<string> Data { get; set; }

        [Output("Result")]
        public OutArgument<string> Result { get; set; }

        [Output("BatchResult")]
        public OutArgument<string> BatchResult { get; set; }

        /// <summary>
        ///     A plug-in that provide common implementation
        /// </summary>
        protected override void Execute(CodeActivityContext executionContext)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                // The InputParameters collection contains all the data passed in the message request.

                if (!IsContextFull(executionContext))
                    return;

                BatchResultDtoList = new List<BatchResultDto>();

                PopulateBaseDto(executionContext);

                //Execute plug-in
                Main();

                BaseDto.OrgService.Update(BaseDto.Target);
                BaseDto.Tracer.Trace("--- Performance info ---");
                BaseDto.Tracer.Trace("Execution time: " + sw.Elapsed);
            }
            catch (Exception ex)
            {
                BaseDto.Tracer.Trace(ex.Message);
            }
        }


        /// <summary>
        /// </summary>
        /// <param name="executionContext"></param>
        private void PopulateBaseDto(CodeActivityContext executionContext)
        {
            BaseDto = new BaseDto(executionContext);
            var dataService = new DataService(BaseDto);
            BaseDto.DataService = dataService;
            BaseDto.RelationshipService = new RelationshipService(BaseDto, dataService);

            var context = executionContext.GetExtension<IWorkflowContext>();
            var mappingimplementation = MappingImplementation.Get(executionContext);
            if (mappingimplementation != null)
                BaseDto.MappingImplementation = mappingimplementation.Id;

            object target = null;
            var isCreateFromTarget = IsCreateFromTarget.Get(executionContext);

            // Obtain the target entity from the input parameters.
            if (isCreateFromTarget && context.InputParameters.Contains("Target") &&
                context.InputParameters["Target"] != null
            )
            {
                target = context.InputParameters["Target"];

                var targetEntity = GetEntity(target);
                if (targetEntity != null)
                {
                    BaseDto.CreateDto =
                        new List<CreateDto> {new CreateDto {MappingImplementation = mappingimplementation.Id}};
                    BaseDto.Target = targetEntity;
                    return;
                }
            }
            var data = Data.Get(executionContext);
            if (string.IsNullOrEmpty(data))
                throw new Exception($"Please Fill in the field 'Data'. ");
            BaseDto.CreateDto = JsonConvert.DeserializeObject<List<CreateDto>>(data);
        }


        /// <summary>
        ///     Cast Target to Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private Entity GetEntity(object entity)
        {
            try
            {
                if (entity == null)
                    throw new Exception("Can't get target from Context! entity is null");

                var target = entity;
                if (target is EntityReference)
                {
                    var sourceEntity = (EntityReference) entity;
                    return new Entity(sourceEntity.LogicalName, sourceEntity.Id);
                }

                if (target is Entity)
                    return (Entity) target;

                throw new Exception("Can't get target from Context! entity is null");
            }
            catch (Exception)
            {
                throw new Exception("Can't get target from Context");
            }
        }

        /// <summary>
        ///     Start Executing a plug-in
        /// </summary>
        private void Main()
        {
            BaseDto.Tracer.Trace("*** Plug-in " + GetType().FullName + " is started ***");


            //Run the specific plugin
            Run();

            BaseDto.Tracer.Trace("*** Plug-in " + GetType().FullName + " is successfully executed ***");
        }


        /// <summary>
        ///     Abstract method should be implemented in a child class
        /// </summary>
        protected abstract void Run();


        /// <summary>
        ///     Obtain the execution context from the service provider.
        /// </summary>
        /// <param name="executionContext"></param>
        /// <returns></returns>
        private bool IsContextFull(CodeActivityContext executionContext)
        {
            var tracer = executionContext.GetExtension<ITracingService>();
            if (executionContext == null) throw new ArgumentNullException(nameof(executionContext));
            var rs = true;
            try
            {
                if (executionContext.GetExtension<IWorkflowContext>() == null)
                    rs = false;
            }
            catch (Exception ex)
            {
                tracer.Trace("IsContextFull:" + ex.Message);
                return false;
            }
            return rs;
        }
    }
}