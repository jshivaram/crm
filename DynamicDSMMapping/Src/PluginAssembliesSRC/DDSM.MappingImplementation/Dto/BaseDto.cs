﻿using System;
using System.Activities;
using System.Collections.Concurrent;
using System.Collections.Generic;
using DDSM.MappingImplementation.Service;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Workflow;

namespace DDSM.MappingImplementation.Dto
{
    public class BaseDto
    {
        public List<CreateDto> CreateDto;

        public Guid CurrentUserId;
        public ConcurrentDictionary<Guid, Entity> EntityCache = new ConcurrentDictionary<Guid, Entity>();

        public ConcurrentDictionary<string, string> EntityDisplayName =
            new ConcurrentDictionary<string, string>();

        public ConcurrentDictionary<string, EntityMetadata> EntityMetadata =
            new ConcurrentDictionary<string, EntityMetadata>();

        public BaseDto()
        {
            EntityMetadata = new ConcurrentDictionary<string, EntityMetadata>();
            EntityCache = new ConcurrentDictionary<Guid, Entity>();
            EntityDisplayName = new ConcurrentDictionary<string, string>();
        }

        public BaseDto(CodeActivityContext executionContext)
        {
            ExecutionContext = executionContext;
            // Obtain the execution context from the service provider.
            WorkflowContext = executionContext.GetExtension<IWorkflowContext>();
            CurrentUserId = WorkflowContext.UserId;
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            Tracer = executionContext.GetExtension<ITracingService>();

            // Obtain the organization service reference.
            var factory = executionContext.GetExtension<IOrganizationServiceFactory>();
            OrgService = factory.CreateOrganizationService(WorkflowContext.UserId);

            //target entity
            Target = new Entity();

            MappingImplementation = Guid.Empty;
            CreateDto = new List<CreateDto>();
        }

        public CodeActivityContext ExecutionContext { get; }

        //Execution Context
        public IWorkflowContext WorkflowContext { get; }

        //Oganization Service
        public IOrganizationService OrgService { get; set; }

        //Target Entity
        public Entity Target { get; set; }

        //Tracing Service
        public ITracingService Tracer { get; }

        public Guid MappingImplementation { get; set; }

        public DataService DataService { get; set; }
        public RelationshipService RelationshipService { get; set; }
    }
}