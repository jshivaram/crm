﻿using System;

namespace DDSM.MappingImplementation.Dto
{
    public class BatchResultDto
    {
        public string EntityLogicalName { get; set; }
        public Guid RecordId { get; set; }
    }
}