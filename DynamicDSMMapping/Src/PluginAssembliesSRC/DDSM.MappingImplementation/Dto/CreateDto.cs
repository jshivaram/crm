﻿using System;
using System.Collections.Generic;

namespace DDSM.MappingImplementation.Dto
{
    public class CreateDto
    {
        public List<Attribute> Attributes;


        public CreateDto()
        {
            MappingImplementation = Guid.Empty;
            Attributes = new List<Attribute>();
            TemplatelId = Guid.Empty;
            
        }

        public string TargetEntityLogicalName { get; set; }
        public Guid TemplatelId { get; set; }
        public string TemplateEntityLogicalName { get; set; }
        public Guid MappingImplementation { get; set; }

        public class Attribute
        {
            public string LogicalName { get; set; }
            public string Value { get; set; }
        }
    }

}