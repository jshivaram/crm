﻿using System;
using System.Collections.Generic;

namespace DDSM.MappingImplementation.Dto
{
    public class CreateRelationshipDto
    {
        public List<Child> Childs;

        public CreateRelationshipDto()
        {
            RecordId = Guid.Empty;
            Childs = new List<Child>();
        }

        public string EntityLogicalName { get; set; }
        public Guid RecordId { get; set; }

        public class Child
        {
            public Child()
            {
                RecordId = Guid.Empty;
            }

            public string EntityLogicalName { get; set; }
            public Guid RecordId { get; set; }
        }
    }
}