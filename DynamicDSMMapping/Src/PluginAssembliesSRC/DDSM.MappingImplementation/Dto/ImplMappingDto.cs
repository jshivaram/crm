﻿using System;
using System.Collections.Generic;

namespace DDSM.MappingImplementation.Dto
{
    public class ImplMappingDto
    {
        //Mapping Id
        public Mapping SelectedMapping;
        //Referenced entities
        public List<Tab> Tabs;

        public ImplMappingDto()
        {
            Tabs = new List<Tab>();
            SelectedMapping = new Mapping();
        }

        public class Mapping
        {
            public Guid Id;

            public Mapping()
            {
                Id = Guid.Empty;
            }
        }


        //Referenced entities
        public class Tab
        {
            //Relationships m2O from referenced entity
            public List<Relationship> Relationships;

            public Tab()
            {
                Relationships = new List<Relationship>();
                Lookup = new Lookup();
            }

            public Lookup Lookup { get; set; }

            public string Label { get; set; }
            public string LogicalName { get; set; }
        }

        //Relationships m2O from referenced entity
        public class Relationship
        {
            public MappingImplementation MappingImplementation;

            public Relationship()
            {
                MappingImplementation = new MappingImplementation();
                Implement = false;
            }

            public string Label { get; set; }
            public string SchemaName { get; set; }
            public string LogicalName { get; set; }
            public bool Implement { get; set; }
        }


        public class MappingImplementation
        {
            public MappingImplementation()
            {
                Id = Guid.Empty;
            }

            public Guid Id { get; set; }
        }

        public class Lookup
        {
            public string Label { get; set; }
            public string LogicalName { get; set; }
        }
    }
}