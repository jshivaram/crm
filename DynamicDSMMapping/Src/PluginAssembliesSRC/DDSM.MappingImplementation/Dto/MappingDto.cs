﻿namespace DDSM.MappingImplementation.Dto
{
    public class MappingDto
    {
        public string SourceField { get; set; }
        public string SourceFieldLogicalName { get; set; }
        public string SourceFieldType { get; set; }
        public string AttributeOf { get; set; }
        public string TargetField { get; set; }
        public string TargetFieldLogicalName { get; set; }
        public string TargetFieldType { get; set; }
    }
}