﻿using System;

namespace DDSM.MappingImplementation.Dto
{
    public class ResultDto
    {
        public bool Successful { get; set; }
        public string Message { get; set; }
        public Guid Id { get; set; }
    }
}