﻿using System;
using System.Collections.Generic;
using System.Linq;
using DDSM.MappingImplementation.Dto;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;

namespace DDSM.MappingImplementation.Service
{
    public class CreatorManager
    {
        private readonly BaseDto _baseDto;
        private readonly DataService _dataService;
        private readonly RelationshipService _relationshipService;


        public CreatorManager(BaseDto baseDto)
        {
            _baseDto = baseDto;
            _dataService = baseDto.DataService;
            _relationshipService = baseDto.RelationshipService;
        }

        /// <summary>
        /// </summary>
        /// <param name="implMappinId"></param>
        public void Run(Guid implMappinId)
        {
            try
            {
                //get mapping implementation
                var mappingImplementation = GetMappingImplementation(implMappinId);
                if (mappingImplementation == null)
                    throw new Exception($"ImplMapping does not exist for the entity {_baseDto.Target.LogicalName}");
                //get mapping
                var mapping = _dataService.GetMappingById(mappingImplementation.SelectedMapping.Id);
                var mappingDir = _dataService.GetMappingDirection(mappingImplementation.SelectedMapping.Id);

                //mapping entity
                if (mappingDir == (int)DataService.MappingDirection.m2O)
                {
                    InitTargetEntityByMappingImplManyToOne(_baseDto.Target, mappingImplementation, mapping);
                    return;
                }
                InitTargetEntityByMappingImplOneToMany(_baseDto.Target, mappingImplementation, mapping);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="implMappinId"></param>
        /// <returns></returns>
        private ImplMappingDto GetMappingImplementation(Guid implMappinId)
        {
            var mappedDataResult = new ImplMappingDto();

            var columns = new ColumnSet("ddsm_json");

            // Create query expression.
            var query = new QueryExpression
            {
                ColumnSet = columns,
                EntityName = "ddsm_mappingimplementation",
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_mappingimplementationid", ConditionOperator.Equal, implMappinId)
                    }
                }
            };
            var rs = _dataService.RetrieveMultiple(query);

            object implMappingDataObj = null;
            var implMappingData = string.Empty;
            if (rs.Entities[0].Attributes.TryGetValue("ddsm_json", out implMappingDataObj))
                implMappingData = implMappingDataObj.ToString();

            if (!string.IsNullOrEmpty(implMappingData))
                mappedDataResult = JsonConvert.DeserializeObject<ImplMappingDto>(implMappingData);


            return mappedDataResult;
        }


        /// <summary>
        ///     Populate target by mapping
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="mappingImplementation"></param>
        /// <param name="mapping"></param>
        /// <returns></returns>
        private Guid InitTargetEntityByMappingImplManyToOne(Entity targetEntity, ImplMappingDto mappingImplementation,
            Dictionary<string, List<MappingDto>> mapping)
        {
            if (mappingImplementation == null) throw new ArgumentNullException(nameof(mappingImplementation));

            var id = targetEntity.Id;
            
            if (id == Guid.Empty)
            {
                id = _baseDto.OrgService.Create(targetEntity);
                targetEntity.Id = id;
            }

            if (mapping.Count == 0)
                throw new Exception($"Mapping does not exist for the entity {targetEntity.LogicalName}");
            foreach (var tab in mappingImplementation.Tabs)
            {
                if (tab.Lookup.LogicalName == null)
                    throw new Exception($"Please select a certain instance for mapping");
                //get entity from need map
                var fromEntity = GetSourceEntityData(targetEntity, tab.Lookup.LogicalName);

                if (fromEntity == null) continue;
                                
                //map target entity
                _dataService.InitTargetEntityFromSource(targetEntity, fromEntity, mapping[tab.LogicalName]);

                //create childs
                CreateRelationship(targetEntity, fromEntity, tab.Relationships);
            }
            return id;
        }

        private Guid InitTargetEntityByMappingImplOneToMany(Entity sourceEntity, ImplMappingDto mappingImplementation,
            Dictionary<string, List<MappingDto>> mapping)
        {
            if (mappingImplementation == null) throw new ArgumentNullException(nameof(mappingImplementation));

            var id = sourceEntity.Id;

            if (id == Guid.Empty)
            {
                id = _baseDto.OrgService.Create(sourceEntity);
                sourceEntity.Id = id;
            }

            if (mapping.Count == 0)
                throw new Exception($"Mapping does not exist for the entity {sourceEntity.LogicalName}");
            foreach (var tab in mappingImplementation.Tabs)
            {
                if (tab.Lookup.LogicalName == null)
                    throw new Exception($"Please select a certain instance for mapping");

                //get entity from need map
                var targetEntity = GetSourceEntityData(sourceEntity, tab.Lookup.LogicalName);

                if (targetEntity == null) continue;

                //map target entity
                _dataService.InitTargetEntityFromSource(targetEntity, sourceEntity, mapping[tab.LogicalName]);

                //create childs
                //CreateRelationship(targetEntity, fromEntity, tab.Relationships);
            }
            return id;
        }

        /// <summary>
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="fromEntity"></param>
        /// <param name="relationships"></param>
        public void CreateRelationship(Entity targetEntity, Entity fromEntity,
            List<ImplMappingDto.Relationship> relationships)
        {
            if (relationships.Count == 0) return;
            foreach (var relationship in relationships)
            {
                if (!relationship.Implement)
                    continue;
                if (relationship.MappingImplementation == null) continue;
                var referencingEntites = _relationshipService.GetReferencingEntities(fromEntity, relationship.SchemaName);
                if (referencingEntites == null || referencingEntites.Entities.Count == 0) continue;
                var relationshipEntities = CreateRelationShipByMapping(relationship.MappingImplementation,
                    referencingEntites);
                AssociateTargetAndRelationShipEntity(targetEntity, relationshipEntities);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="relationshipEntities"></param>
        private void AssociateTargetAndRelationShipEntity(Entity targetEntity, List<Entity> relationshipEntities)
        {
            var referencingAtt = _relationshipService.GetReferencingMetadata(targetEntity.LogicalName,
                relationshipEntities[0].LogicalName);
            if (referencingAtt == null)
                throw new Exception(
                    $"Doesn't exist relationships between {targetEntity.LogicalName} and  {relationshipEntities[0].LogicalName}");
            foreach (var relationshipEntity in relationshipEntities)
            {
                relationshipEntity[referencingAtt.ReferencingAttribute] = new EntityReference(targetEntity.LogicalName,
                    targetEntity.Id);

                _baseDto.OrgService.Update(relationshipEntity);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="mappingImplementation"></param>
        /// <param name="referencingEntites"></param>
        /// <returns></returns>
        private List<Entity> CreateRelationShipByMapping(
            ImplMappingDto.MappingImplementation mappingImplementation, EntityCollection referencingEntites)
        {
            var rs = new List<Entity>();
            foreach (var referencingEntity in referencingEntites.Entities)
            {
                var mappingImpl = GetMappingImplementation(mappingImplementation.Id);
                var mapping = _dataService.GetMappingById(mappingImpl.SelectedMapping.Id);

                var referencingAttr =
                    mappingImpl.Tabs.FirstOrDefault(x => x.LogicalName == referencingEntity.LogicalName)
                        .Lookup.LogicalName;
                var sourceEntityLogicalName =
                    _dataService.GetMappingSourceEntityLogicalName(mappingImpl.SelectedMapping.Id);

                var sourceEntity = new Entity(sourceEntityLogicalName)
                {
                    [referencingAttr] = new EntityReference(referencingEntity.LogicalName, referencingEntity.Id)
                };

                sourceEntity.Id = InitTargetEntityByMappingImplManyToOne(sourceEntity, mappingImpl, mapping);
                rs.Add(sourceEntity);
            }
            return rs;
        }


        /// <summary>
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="sourceEntityLogicalName"></param>
        /// <param name="referencingAtt"></param>
        /// <returns></returns>
        private Entity GetSourceEntityData(Entity targetEntity, string referencingAtt)
        {
            object sourceEntityTmp = null;
            EntityReference sourceEntityRef = null;

            //if target entity already contains the referencing attr
            if (!targetEntity.Attributes.TryGetValue(referencingAtt, out sourceEntityTmp))
                return new Entity();

            sourceEntityRef = (EntityReference) sourceEntityTmp;

            if (sourceEntityRef == null)
                return null;

            //return source entity
            return _dataService.RetreiveEntity(sourceEntityRef.LogicalName, sourceEntityRef.Id);
        }
    }
}