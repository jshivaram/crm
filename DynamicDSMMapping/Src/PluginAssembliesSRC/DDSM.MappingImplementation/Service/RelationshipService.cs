﻿using System.Linq;
using DDSM.MappingImplementation.Dto;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;

namespace DDSM.MappingImplementation.Service
{
    public class RelationshipService
    {
        private readonly BaseDto _baseDto;
        private readonly DataService _dataService;

        private readonly IOrganizationService _orgService;

        public RelationshipService(BaseDto baseDto, DataService dataService)
        {
            _baseDto = baseDto;
            _orgService = baseDto.OrgService;
            _dataService = dataService;
        }


        /// <summary>
        ///     Get Refarencing Attribute from entity metadata
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="referencingEntity"></param>
        /// <returns></returns>
        public OneToManyRelationshipMetadata GetReferencingMetadata(string targetEntity, string referencingEntity)
        {
            var o2M = GetReferencingAttrOneToMany(targetEntity, referencingEntity);
            if (o2M != null)
                return o2M;
            var m2O = GetReferencingAttrManyToOne(targetEntity, referencingEntity);
            if (m2O != null)
                return m2O;
            return null;
        }

        public OneToManyRelationshipMetadata GetReferencingAttrOneToMany(string targetEntity, string referencingEntity)
        {
            var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = targetEntity
            };

            var retrieveBankEntityResponse =
                (RetrieveEntityResponse) _orgService.Execute(retrieveBankAccountEntityRequest);

            var oneToManyRelationships = retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships;
            var rs =
                oneToManyRelationships.Where(r => r.ReferencingEntity == referencingEntity).ToList();

            return rs.Count == 0? null : rs[0];
        }

        public OneToManyRelationshipMetadata GetReferencingAttrManyToOne(string targetEntity, string referencedEntity)
        {
            var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = targetEntity
            };

            var retrieveBankEntityResponse =
                (RetrieveEntityResponse) _orgService.Execute(retrieveBankAccountEntityRequest);

            var manyToOneRelationships = retrieveBankEntityResponse.EntityMetadata.ManyToOneRelationships;
            var rs =
                manyToOneRelationships.Where(r => r.ReferencedEntity == referencedEntity).ToList();

            return rs.Count == 0 ? null : rs[0];
        }

        public EntityCollection GetReferencingEntities(Entity referencedEntity, string relationshipName)
        {
            var metadata = GetOneToManyRelationshipMetadata(referencedEntity.LogicalName, relationshipName);
            if (metadata == null) return null;

            var query = new QueryExpression
            {
                ColumnSet = new ColumnSet(),
                EntityName = metadata.ReferencingEntity,
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression(metadata.ReferencingAttribute, ConditionOperator.Equal,
                            referencedEntity.Id)
                    }
                }
            };
            return _dataService.RetrieveMultiple(query);
        }

        private OneToManyRelationshipMetadata GetOneToManyRelationshipMetadata(string referencedEntity,
            string relationshipName)
        {
            var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = referencedEntity
            };

            var retrieveBankEntityResponse =
                (RetrieveEntityResponse) _orgService.Execute(retrieveBankAccountEntityRequest);

            var oneToManyRelationships = retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships;
            var rs =
                oneToManyRelationships.Where(r => r.SchemaName == relationshipName).ToList();
            if (rs.Count == 0)
                return null;
            return rs[0];
        }
    }
}