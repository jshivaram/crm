﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;

namespace DDSM.MappingImplementation.Service
{
    public static class TransactionCurrency
    {
        private static readonly ConcurrentDictionary<string, EntityReference> CurrencyCache =
            new ConcurrentDictionary<string, EntityReference>();

        private static readonly ConcurrentDictionary<string, bool> EntityIsCurrencyCache =
            new ConcurrentDictionary<string, bool>();

        public static EntityReference GetCurrency(IOrganizationService orgService, Guid userGuid = new Guid())
        {
            var currency = new EntityReference();


            var query = new QueryExpression("usersettings") {ColumnSet = new ColumnSet("transactioncurrencyid")};
            query.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, userGuid);

            var userSettings = orgService.RetrieveMultiple(query).Entities.FirstOrDefault();
            if (userSettings == null) return currency;
            if (userSettings.Attributes.ContainsKey("transactioncurrencyid"))
                currency = userSettings.GetAttributeValue<EntityReference>("transactioncurrencyid");

            //Get Org default currency
            if (currency.Id == Guid.Empty)
                currency = GetCurrencyOrg(orgService);

            CurrencyCache.AddOrUpdate(userGuid.ToString(), currency, (oldkey, oldvalue) => currency);

            return currency;
        }

        public static EntityReference GetCurrency(IOrganizationService orgService, Guid userGuid, string logicalName)
        {
            if (string.IsNullOrEmpty(logicalName))
                return new EntityReference();

            var isField = EntityIsCurrency(orgService, logicalName);

            if (isField)
                return GetCurrency(orgService, userGuid);

            return new EntityReference();
        }

        public static bool EntityIsCurrency(IOrganizationService orgService, string logicalName)
        {
            if (string.IsNullOrEmpty(logicalName))
                return false;

            if (EntityIsCurrencyCache.ContainsKey(logicalName))
                return EntityIsCurrencyCache[logicalName];

            var request = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Attributes,
                LogicalName = logicalName
            };
            var response = (RetrieveEntityResponse) orgService.Execute(request);
            var isField =
                response.EntityMetadata.Attributes.FirstOrDefault(
                    element => element.LogicalName == "transactioncurrencyid") != null;
            EntityIsCurrencyCache.AddOrUpdate(logicalName, isField, (oldkey, oldvalue) => isField);

            return isField;
        }

        private static EntityReference GetCurrencyOrg(IOrganizationService orgService)
        {
            var orgId = ((WhoAmIResponse) orgService.Execute(new WhoAmIRequest())).OrganizationId;
            var currency = new EntityReference();
            var query = new QueryExpression("organization");
            query.ColumnSet = new ColumnSet("basecurrencyid");
            query.Criteria.AddCondition("organizationid", ConditionOperator.Equal, orgId);
            var orgInfo = orgService.RetrieveMultiple(query).Entities.FirstOrDefault();
            if (orgInfo.Attributes.ContainsKey("basecurrencyid"))
                currency = orgInfo.GetAttributeValue<EntityReference>("basecurrencyid");
            return currency;
        }
    }
}