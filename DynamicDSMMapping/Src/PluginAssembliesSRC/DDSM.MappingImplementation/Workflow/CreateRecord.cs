﻿using System;
using DDSM.CommonProvider.JSON;
using DDSM.MappingImplementation;
using DDSM.MappingImplementation.Dto;
using DDSM.MappingImplementation.Service;
using Microsoft.Xrm.Sdk;

public class CreateRecord : BaseWorkflow
{
    /// <summary>
    /// </summary>
    protected override void Run()
    {
        try
        {
            foreach (var createData in BaseDto.CreateDto)
            {

                var implMappingId = createData.MappingImplementation == Guid.Empty
                    ? BaseDto.MappingImplementation : createData.MappingImplementation;


                PopulateTarget(createData);

                if (implMappingId == Guid.Empty)
                {
                    BaseDto.OrgService.Create(BaseDto.Target);
                }
                var creatorManager = new CreatorManager(BaseDto);
                //start creating record
                creatorManager.Run(implMappingId);
                BaseDto.OrgService.Update(BaseDto.Target);
                CreateResult();
            }
            InitResult();
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
    }

    /// <summary>
    /// </summary>
    private void InitResult()
    {
        if (BaseDto.CreateDto.Count > 1)
        {
            var rs = JsonConvert.SerializeObject(BatchResultDtoList);
            BatchResult.Set(BaseDto.ExecutionContext, rs);
            return;
        }
        Result.Set(BaseDto.ExecutionContext, BaseDto.Target.Id.ToString());
    }

    /// <summary>
    /// </summary>
    private void CreateResult()
    {
        if (BaseDto.CreateDto.Count > 1)
        {
            var rs = new BatchResultDto
            {
                EntityLogicalName = BaseDto.Target.LogicalName,
                RecordId = BaseDto.Target.Id
            };
            BatchResultDtoList.Add(rs);
        }
    }
    /// <summary>
    /// </summary>
    /// <param name="createDto"></param>
    private void PopulateTarget(CreateDto createDto)
    {
        var dataService = BaseDto.DataService;

        //if entity does't create from crm target entity is null
        if (!string.IsNullOrEmpty(BaseDto.Target.LogicalName))
            return;
        var target = new Entity();

        if (!string.IsNullOrEmpty(createDto.TargetEntityLogicalName))
            target = new Entity(createDto.TargetEntityLogicalName);
        foreach (var attr in createDto.Attributes)
            dataService.UpdateEntity(target, attr.LogicalName, attr.Value);

        InitRefAttrBetweenTargetAndSource(target, createDto);
        BaseDto.Target = target;
        
    }
    /// <summary>
    /// </summary>
    /// <param name="createDto"></param>
    /// <summary>
    /// </summary>
    /// <param name="target"></param>
    /// <param name="createDto"></param>
    private void InitRefAttrBetweenTargetAndSource(Entity target, CreateDto createDto)
    {

        if (!string.IsNullOrEmpty(createDto.TargetEntityLogicalName) && createDto.TemplatelId != Guid.Empty)
        {
        //get referencing field between target and source

        var tmplField =
            BaseDto.RelationshipService.GetReferencingMetadata(target.LogicalName,
                createDto.TemplateEntityLogicalName);
        if (tmplField == null)
            throw new Exception(
                $"Doesn't exist relationships between {target.LogicalName} and  {createDto.TemplateEntityLogicalName}");
        target[tmplField.ReferencingAttribute] =
            new EntityReference(createDto.TemplateEntityLogicalName, createDto.TemplatelId);
        }
    }
}