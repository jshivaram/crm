﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider.JSON;
using DDSM.MappingImplementation.Dto;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;

public class LinkEntities : CodeActivity
{
    private IOrganizationService _orgService;
    public BaseDto BaseDto;

    [Input("Data")]
    public InArgument<string> Data { get; set; }

    [Output("Result")]
    public OutArgument<string> Result { get; set; }

    /// <summary>
    ///     A plug-in that provide common implementation
    /// </summary>
    protected override void Execute(CodeActivityContext executionContext)
    {
        var tracer = executionContext.GetExtension<ITracingService>();
        try
        {
            // The InputParameters collection contains all the data passed in the message request.

            if (!IsContextFull(executionContext))
                return;

            // Obtain the execution context from the service provider.
            var context = executionContext.GetExtension<IWorkflowContext>();

            //Extract the tracing service for use in debugging sandboxed plug-ins.


            // Obtain the organization service reference.
            var factory = executionContext.GetExtension<IOrganizationServiceFactory>();
            _orgService = factory.CreateOrganizationService(context.UserId);

            AddRelationship(executionContext);

            var rs = new ResultDto
            {
                Successful = true
            };
            Result.Set(executionContext, JsonConvert.SerializeObject(rs));
        }
        catch (Exception ex)
        {
            tracer.Trace(ex.Message);
            var rs = new ResultDto
            {
                Successful = false,
                Message = ex.Message
            };
            Result.Set(executionContext, JsonConvert.SerializeObject(rs));
        }
    }

    private void AddRelationship(CodeActivityContext executionContext)
    {
        var data = Data.Get(executionContext);
        if (string.IsNullOrEmpty(data))
            throw new Exception($"Please Fill in the field 'Data'. ");

        var createRelationshipDtoList = JsonConvert.DeserializeObject<List<CreateRelationshipDto>>(data);
        if (createRelationshipDtoList == null)
            return;

        foreach (var createRelationshipDto in createRelationshipDtoList)
        {
            var parentEntityLogicalName = createRelationshipDto.EntityLogicalName;
            var parentRecordId = createRelationshipDto.RecordId;
            if (string.IsNullOrEmpty(parentEntityLogicalName) || parentRecordId == Guid.Empty)
                return;
            foreach (var child in createRelationshipDto.Childs)
            {
                var childEntityLogicalName = child.EntityLogicalName;
                var childRecordId = child.RecordId;
                if (string.IsNullOrEmpty(parentEntityLogicalName) || parentRecordId == Guid.Empty)
                    return;
                var referencingAtt = GetRefarencingMetadata(childEntityLogicalName, parentEntityLogicalName);

                var entity = _orgService.Retrieve(childEntityLogicalName, childRecordId, new ColumnSet());
                if (entity == null)
                    return;


                entity[referencingAtt.ReferencingAttribute] = new EntityReference(parentEntityLogicalName,
                    parentRecordId);

                _orgService.Update(entity);
            }
        }
    }

    public OneToManyRelationshipMetadata GetRefarencingMetadata(string targetEntity, string referencingEntity)
    {
        var o2M = GetRefarencingAttrOneToMany(targetEntity, referencingEntity);
        if (o2M != null)
            return o2M;
        var m2O = GetRefarencingAttrManyToOne(targetEntity, referencingEntity);
        if (m2O != null)
            return m2O;
        return null;
    }

    public OneToManyRelationshipMetadata GetRefarencingAttrOneToMany(string targetEntity, string referencingEntity)
    {
        var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
        {
            EntityFilters = EntityFilters.Relationships,
            LogicalName = targetEntity
        };

        var retrieveBankEntityResponse =
            (RetrieveEntityResponse) _orgService.Execute(retrieveBankAccountEntityRequest);

        var oneToManyRelationships = retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships;
        var rs =
            oneToManyRelationships.Where(r => r.ReferencingEntity == referencingEntity).ToList();

        return rs.Count == 0 ? null : rs[0];
    }

    public OneToManyRelationshipMetadata GetRefarencingAttrManyToOne(string targetEntity, string referencedEntity)
    {
        var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
        {
            EntityFilters = EntityFilters.Relationships,
            LogicalName = targetEntity
        };

        var retrieveBankEntityResponse =
            (RetrieveEntityResponse) _orgService.Execute(retrieveBankAccountEntityRequest);

        var manyToOneRelationships = retrieveBankEntityResponse.EntityMetadata.ManyToOneRelationships;
        var rs =
            manyToOneRelationships.Where(r => r.ReferencedEntity == referencedEntity).ToList();

        return rs.Count == 0 ? null : rs[0];
    }

    /// <summary>
    ///     Obtain the execution context from the service provider.
    /// </summary>
    /// <param name="executionContext"></param>
    /// <returns></returns>
    private bool IsContextFull(CodeActivityContext executionContext)
    {
        var tracer = executionContext.GetExtension<ITracingService>();
        if (executionContext == null) throw new ArgumentNullException(nameof(executionContext));
        var rs = true;
        try
        {
            if (executionContext.GetExtension<IWorkflowContext>() == null)
                rs = false;
        }
        catch (Exception ex)
        {
            tracer.Trace("IsContextFull:" + ex.Message);
            return false;
        }
        return rs;
    }
}