﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Workflow;
using DynamicDSMMapping;
using DynamicDSMMapping.Service;
using System.Collections.Concurrent;

public class DmnAttribute
{
    public string LogicalName { get; set; }
    public string Label { get; set; }
    public string AttributeType { get; set; }
    public List<Target> Targets { get; set; }
    public string TypeField { get; set; }
}

public class Target
{
    public string LogicalName { get; set; }
    public string Label { get; set; }
    public string TargetEntity { get; set; }
    public string ValueEntity { get; set; }
    public string RefAttribute { get; set; }
}

public class GetEntityDefinitionsAndIndicators : WorkFlowActivityBase
{
    [Input("Entity Name")]
    [RequiredArgument]
    public InArgument<string> EntityName { get; set; }

    public override void ExecuteCRMWorkFlowActivity(CodeActivityContext context, LocalWorkflowContext crmWorkflowContext)
    {
        var entityName = EntityName.Get(context);

        var request = new RetrieveEntityRequest
        {
            EntityFilters = EntityFilters.Attributes,
            LogicalName = entityName
        };


        var response = (RetrieveEntityResponse)crmWorkflowContext.OrganizationService.Execute(request);

        var result = new
        {
            Label = response.EntityMetadata.DisplayName.LocalizedLabels[0].Label,
            LogicalName = response.EntityMetadata.LogicalName,
            Attributes = new List<DmnAttribute>()
        };

        foreach (var attr in response.EntityMetadata.Attributes)
        {
            if (attr.DisplayName.LocalizedLabels.Count > 0 && string.IsNullOrEmpty(attr.AttributeOf))
            {


                string _attrTargetLogicalName = "";
                if (attr.AttributeType.ToString() == "Lookup")
                {
                    LookupAttributeMetadata attrLookup = attr as LookupAttributeMetadata;
                    if (attrLookup?.Targets?.Length > 0)
                    {
                        _attrTargetLogicalName = attrLookup.Targets[0].ToString();
                    }
                }

                var lookupAttribute = new DmnAttribute
                {
                    LogicalName = attr.LogicalName,
                    AttributeType = attr.AttributeType.ToString(),
                    Label = attr.DisplayName.LocalizedLabels[0]?.Label,
                    Targets = new List<Target>(new Target[] { new Target { LogicalName = _attrTargetLogicalName } }),
                    TypeField = AttrType.Field.ToString()
                };

                result.Attributes.Add(lookupAttribute);
            }
        }

        var indicators = IndicatorService.GetList(crmWorkflowContext.OrganizationService, entityName);

       

        try
        {

            ConcurrentDictionary<string, string> iRelationship = new ConcurrentDictionary<string, string>();

            foreach (var ind in indicators)
            {
                if (!iRelationship.ContainsKey(ind.ValueEntity))
                {
                    var rs = GetRelationship(crmWorkflowContext, ind.ValueEntity, entityName);
                    if (rs != null)
                    {
                        var iRelationName = rs.SchemaName;
                        iRelationship.AddOrUpdate(ind.ValueEntity, iRelationName + ":" + ind.ValueRefField, (oldkey, oldvalue) => iRelationName + ":" + ind.ValueRefField);
                    }
                    else
                    {
                        crmWorkflowContext.Trace("Indicator: " + ind.Name + "; Type: " + ind.Type + "; Category: " + ind.Category + "; Category Group: " + ind.CategoryGroup + "; Entities of the " + entityName + " and " + ind.ValueEntity + " do not have a connection 1:N");
                        continue;
                    }
                }
            }

            foreach (var ind in indicators )
            {

                if (ind.ValueFields.Count == 0)
                {
                    crmWorkflowContext.Trace("Indicator: " + ind.Name + "; Type: " + ind.Type + "; Category: " + ind.Category + "; Category Group: " + ind.CategoryGroup + "; Entity Values: " + ind.ValueEntity + "; Value fields not found");
                    continue;
                }
                if (!iRelationship.ContainsKey(ind.ValueEntity)) { continue; }

                if (ind.ValueFields.Count == 1)
                {
                    result.Attributes.Add(new DmnAttribute
                    {
                        Label = ind.Name,
                        AttributeType = ind.Type,
                        LogicalName = ind.Id.ToString("B"),
                        Targets = new List<Target>(new Target[] { new Target {
                            LogicalName = ind.ValueFields[0].LogicalName,
                            Label = ind.ValueFields[0].DisplayName,
                            TargetEntity = ind.ValueFields[0].TargetEntity,
                            ValueEntity = ind.ValueEntity,
                            RefAttribute = ind.ValueRefField
                        }}),
                        TypeField = AttrType.Indicator.ToString(),
                    });
                }
                else
                {
                    var dmnAttribute = new DmnAttribute
                    {
                        Label = ind.Name,
                        AttributeType = ind.Type,
                        LogicalName = ind.Id.ToString("B"),
                        Targets = new List<Target>(),
                        TypeField = AttrType.Indicator.ToString(),
                    };

                    for (var i = 0; i < ind.ValueFields.Count; i++)
                    {
                        dmnAttribute.Targets.Add(new Target {
                            LogicalName = ind.ValueFields[i].LogicalName,
                            Label = ind.ValueFields[i].DisplayName,
                            TargetEntity = ind.ValueFields[i].TargetEntity,
                            ValueEntity = ind.ValueEntity,
                            RefAttribute = ind.ValueRefField
                        });
                    }

                    result.Attributes.Add(dmnAttribute);
                }
            }
        }
        catch (Exception e)
        {
            crmWorkflowContext.Trace("Message: " + e.Message);
            //throw new Exception(e.Message);
        }
        //indicators.Dispose();

        var resultJson = DDSM.CommonProvider.JSON.JsonConvert.SerializeObject(result);
        Result.Set(context, resultJson);
        Complete.Set(context, true);

    }

    private OneToManyRelationshipMetadata GetRelationship(LocalWorkflowContext crmWorkflowContext, string targetEntity, string referencedEntity)
    {
        try
        {
            var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = targetEntity
            };

            var retrieveBankEntityResponse =
                (RetrieveEntityResponse)crmWorkflowContext.OrganizationService.Execute(retrieveBankAccountEntityRequest);

            var manyToOneRelationships = retrieveBankEntityResponse.EntityMetadata.ManyToOneRelationships;
            var rs =
                manyToOneRelationships.Where(r => r.ReferencedEntity == referencedEntity).ToList();

            return rs.Count == 0 ? null : rs[0];

        }
        catch (Exception e)
        {
            crmWorkflowContext.Trace("TargetEntity: " + targetEntity + "; ReferencedEntity: " + referencedEntity + "; Message: " + e.Message);
            return null;
            //throw new InvalidPluginExecutionException(e.Message);
        }
    }
}


