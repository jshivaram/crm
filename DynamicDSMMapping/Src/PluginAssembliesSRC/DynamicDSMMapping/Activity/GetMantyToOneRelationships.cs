﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Linq;
using DynamicDSMMapping;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

public class GetMantyToOneRelationships : WorkFlowActivityBase
{
    [Input("SourceEntity")]
    public InArgument<string> SourceEntity { get; set; }

    [Input("RelatedEntity")]
    public InArgument<string> RelatedEntity { get; set; }

    public override void ExecuteCRMWorkFlowActivity(CodeActivityContext context, LocalWorkflowContext crmWorkflowContext)
    {
        try
        {
            var sourceEntity = SourceEntity.Get<string>(context);
            var relatedEntity = RelatedEntity.Get<string>(context);

            if (string.IsNullOrEmpty(sourceEntity))
            {
                var json = "[]";
                Result.Set(context, json);
                Complete.Set(context, true);

                return;
            }

            var entityMetadataRequest = new RetrieveEntityRequest { EntityFilters = EntityFilters.Attributes, LogicalName = sourceEntity };
            var entityMetadataResponse = (RetrieveEntityResponse)crmWorkflowContext.OrganizationService.Execute(entityMetadataRequest);

            var entityMetadata = entityMetadataResponse.EntityMetadata;
            var lookups = entityMetadata.Attributes.Where(attr => attr.AttributeType == AttributeTypeCode.Lookup
                && ((LookupAttributeMetadata)attr).Targets.Contains(relatedEntity)).ToList();

            var mappedValues = lookups.Select(lookup => new
            {
                lookup.DisplayName.UserLocalizedLabel.Label,
                lookup.LogicalName
            }).ToList();

            var resultJson = DDSM.CommonProvider.JSON.JsonConvert.SerializeObject(mappedValues);
            Result.Set(context, resultJson);
            Complete.Set(context, true);
        }
        catch (Exception e)
        {
            throw new InvalidPluginExecutionException(e.Message);
        }
    }
}
