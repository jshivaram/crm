﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using DynamicDSMMapping;
using DynamicDSMMapping.Service;
using Microsoft.Xrm.Sdk.Metadata.Query;
using System.Collections.Concurrent;
using DynamicDSMMapping.Model;

public class GetMetadataForExcel : WorkFlowActivityBase
{
    [Input("Entity Name")]
    [RequiredArgument]
    public InArgument<string> EntityName { get; set; }

    [Input("Just Field Type")]
    public InArgument<string> JustFieldType { get; set; }

    [Input("With Indicators")]
    public InArgument<bool> WithIndicators { get; set; }



    public override void ExecuteCRMWorkFlowActivity(CodeActivityContext context, LocalWorkflowContext crmWorkflowContext)
    {
        var entityName = EntityName.Get(context);
        var justFieldType = JustFieldType.Get(context);
        var withIndicators = WithIndicators.Get(context);

        var result = new List<Attr>();


        var entityFilter = new MetadataFilterExpression(LogicalOperator.And)
        {
            Conditions = { new MetadataConditionExpression("LogicalName", MetadataConditionOperator.Equals, entityName) }
        };

        var entityProperties = new MetadataPropertiesExpression { AllProperties = false, PropertyNames = { "Attributes" } };

        var attributeFilter = new MetadataFilterExpression(LogicalOperator.And)
        {
            Conditions = { new MetadataConditionExpression("LogicalName", MetadataConditionOperator.NotIn, IndicatorService.ExcludedFields) }
        };

        var attributeProperties = new MetadataPropertiesExpression { AllProperties = false, PropertyNames = { "DisplayName", "LogicalName", "Targets", "AttributeType" } };


        var entityQueryExpression = new EntityQueryExpression
        {
            Criteria = entityFilter,
            Properties = entityProperties,
            AttributeQuery = new AttributeQueryExpression { Criteria = attributeFilter, Properties = attributeProperties }
        };

        var retrieveMetadataChangesRequest = new RetrieveMetadataChangesRequest { Query = entityQueryExpression };

        var response = (RetrieveMetadataChangesResponse)crmWorkflowContext.OrganizationService.Execute(retrieveMetadataChangesRequest);

        if (response?.EntityMetadata?.Count == 1)
        {
            foreach (var attr in response.EntityMetadata[0].Attributes)
            {
                if (attr.DisplayName.LocalizedLabels.Count > 0 && string.IsNullOrEmpty(attr.AttributeOf))
                {
                    var attrTargetLogicalName = "";
                    if (attr.AttributeType.ToString() == "Lookup")
                    {
                        var attrLookup = attr as LookupAttributeMetadata;
                        if (attrLookup?.Targets?.Length > 0)
                        {
                            attrTargetLogicalName = attrLookup.Targets[0];
                        }
                    }

                    if (attr.AttributeType == AttributeTypeCode.Money && attr.LogicalName?.IndexOf("_base") != -1) { continue; }
                    var lookupAttribute = new Attr
                    {
                        AttrLogicalName = attr.LogicalName,
                        AttrType = attr.AttributeType.ToString(),
                        AttrDisplayName = attr.DisplayName.LocalizedLabels[0]?.Label,
                        TypeField = AttrType.Field.ToString(),
                        AttrTargetEntity = attrTargetLogicalName,

                        IndicatorId = "",
                        IName = "",
                        IEntity = "",
                        IRelationName = "",
                        IFieldLogicalName = "",
                        IFieldTargetEntity = "",
                        IAdditionalAttr = ""
                    };

                    result.Add(lookupAttribute);
                }
            }
        }

        if (withIndicators)
        {
            var indicators = IndicatorService.GetList(crmWorkflowContext.OrganizationService, entityName);

            try
            {
                var iRelationship = new ConcurrentDictionary<string, string>();

                foreach (var indicator in indicators) // as IEnumerable<Indicator>
                {
                    if (!iRelationship.ContainsKey(indicator.ValueEntity))
                    {
                        var rs = GetRelationship(crmWorkflowContext, indicator.ValueEntity, entityName);
                        if (rs != null)
                        {
                            var iRelationName = rs.SchemaName;
                            //check for a field 'transactioncurrencyid' in the value entity
                            var isCurrencyField = DoesFieldExist(crmWorkflowContext.OrganizationService, indicator.ValueEntity, "transactioncurrencyid");

                            //check for a field 'ddsm_creatorrecordtype' in the value entity
                            var isCreatorField = DoesFieldExist(crmWorkflowContext.OrganizationService, indicator.ValueEntity, "ddsm_creatorrecordtype");

                            //primary field
                            var primaryEntityAttributeName = "";

                            var eFilter = new MetadataFilterExpression(LogicalOperator.And)
                            {
                                Conditions = { new MetadataConditionExpression("LogicalName", MetadataConditionOperator.Equals, indicator.ValueEntity) }
                            };

                            var eProperties = new MetadataPropertiesExpression { AllProperties = false, PropertyNames = { "PrimaryNameAttribute" } };

                            var eQuery = new EntityQueryExpression
                            {
                                Criteria = eFilter,
                                Properties = eProperties
                            };

                            var retriveMetadata = new RetrieveMetadataChangesRequest { Query = eQuery };

                            var resp = (RetrieveMetadataChangesResponse)crmWorkflowContext.OrganizationService.Execute(retriveMetadata);

                            if (resp?.EntityMetadata?.Count == 1)
                            {
                                primaryEntityAttributeName = resp.EntityMetadata[0].PrimaryNameAttribute;
                            }

                            var strVal = iRelationName + ":" + indicator.ValueRefField + ":" + primaryEntityAttributeName + ":" + isCurrencyField + ":" + isCreatorField;

                            iRelationship.AddOrUpdate(indicator.ValueEntity, strVal, (oldkey, oldvalue) => strVal);
                        }
                        else
                        {
                            crmWorkflowContext.Trace(indicator.ToString() + "; Entities of the " + entityName + " and " + indicator.ValueEntity + " do not have a connection 1:N");
                            continue;
                        }
                    }
                }

                foreach (var indicator in indicators)
                {
                    if (indicator.ValueFields.Count == 0)
                    {
                        crmWorkflowContext.Trace(indicator.ToString() + "; Value fields not found");
                        continue;
                    }
                    if (!iRelationship.ContainsKey(indicator.ValueEntity)) { continue; }

                    if (indicator.ValueFields.Count == 1)
                    {
                        result.Add(new Attr
                        {
                            AttrDisplayName = indicator.Name + " (Category Group: " + indicator.CategoryGroup + ")",
                            AttrLogicalName = indicator.Name + " (CategoryGroup:" + indicator.CategoryGroup + ")",
                            AttrType = indicator.Type,
                            TypeField = AttrType.Indicator.ToString(),
                            AttrTargetEntity = "",

                            IndicatorId = indicator.Id.ToString(),
                            IName = indicator.Name,
                            IEntity = indicator.ValueEntity,
                            IRelationName = iRelationship[indicator.ValueEntity],
                            IFieldLogicalName = indicator.ValueFields[0].LogicalName,
                            IFieldTargetEntity = indicator.ValueFields[0].TargetEntity,
                            IAdditionalAttr = "(Category Group: " + indicator.CategoryGroup + ")"
                        });
                    }
                    else
                    {
                        foreach (InternalAttr ind in indicator.ValueFields)
                        {
                            result.Add(new Attr
                            {
                                AttrDisplayName = indicator.Name + " " + ind.DisplayName + " (Category Group: " + indicator.CategoryGroup + ")",
                                AttrLogicalName = indicator.Name + " " + ind.DisplayName + " (CategoryGroup:" + indicator.CategoryGroup + ")",
                                AttrType = indicator.Type,
                                TypeField = AttrType.Indicator.ToString(),
                                AttrTargetEntity = "",

                                IndicatorId = indicator.Id.ToString(),
                                IName = indicator.Name,
                                IEntity = indicator.ValueEntity,
                                IRelationName = iRelationship[indicator.ValueEntity],
                                IFieldLogicalName = ind.LogicalName,
                                IFieldTargetEntity = ind.TargetEntity,
                                IAdditionalAttr = ind.DisplayName + " (Category Group: " + indicator.CategoryGroup + ")"
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                crmWorkflowContext.Trace("Message: " + e.Message);
                //throw new Exception(e.Message);
            }
            //  indicators.Dispose();
        }


        // filter by type
        if (!string.IsNullOrEmpty(justFieldType))
        {
            result = result.Where(x => x.AttrType.ToLower().Equals(justFieldType.ToLower())).ToList();
        }

        var resultJson = DDSM.CommonProvider.JSON.JsonConvert.SerializeObject(result);
        Result.Set(context, resultJson);
        Complete.Set(context, true);

    }

    private OneToManyRelationshipMetadata GetRelationship(LocalWorkflowContext crmWorkflowContext, string targetEntity, string referencedEntity)
    {
        try
        {
            var request = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = targetEntity
            };

            var retrieveBankEntityResponse = (RetrieveEntityResponse)crmWorkflowContext.OrganizationService.Execute(request);

            var manyToOneRelationships = retrieveBankEntityResponse.EntityMetadata.ManyToOneRelationships;
            var rs = manyToOneRelationships.Where(r => r.ReferencedEntity == referencedEntity).ToList();

            return rs.Count == 0 ? null : rs[0];
        }
        catch (Exception e)
        {
            crmWorkflowContext.Trace("TargetEntity: " + targetEntity + "; ReferencedEntity: " + referencedEntity + "; Message: " + e.Message);
            return null;
            //throw new InvalidPluginExecutionException(e.Message);
        }
    }

    public static bool DoesFieldExist(IOrganizationService orgService, string entityName, string fieldName)
    {
        var request = new RetrieveEntityRequest
        {
            EntityFilters = EntityFilters.Attributes,
            LogicalName = entityName
        };
        var response = (RetrieveEntityResponse)orgService.Execute(request);
        return response.EntityMetadata.Attributes.FirstOrDefault(element => element.LogicalName == fieldName) != null;
    }

}

