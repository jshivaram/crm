﻿using System.Activities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;
using DynamicDSMMapping;

public class Attribute
{
    public string AttrDisplayName { get; set; }
    public string AttrLogicalName { get; set; }
    public string AttrType { get; set; }
}

public enum IndicatorType
{
    Decimal = 962080000, Money = 962080001, Integer = 962080002
}

public class GetMetadataWithIndicators : WorkFlowActivityBase
{
    [Input("Entity Name")]
    [RequiredArgument]
    public InArgument<string> EntityName { get; set; }

    [Input("Just Field Type")]
    public InArgument<string> JustFieldType { get; set; }

    [Input("With Indicators")]
    public InArgument<bool> WithIndicators { get; set; }

    public override void ExecuteCRMWorkFlowActivity(CodeActivityContext context, LocalWorkflowContext crmWorkflowContext)
    {
        var entityName = EntityName.Get(context);
        var justFieldType = JustFieldType.Get(context);
        var withIndicators = WithIndicators.Get(context);

        var result = new List<Attribute>();
        var request = new RetrieveEntityRequest
        {
            EntityFilters = EntityFilters.Attributes,
            LogicalName = entityName
        };


        var response = (RetrieveEntityResponse)crmWorkflowContext.OrganizationService.Execute(request);



        foreach (var attr in response.EntityMetadata.Attributes)
        {
            if (attr.DisplayName.LocalizedLabels.Count > 0 && string.IsNullOrEmpty(attr.AttributeOf))
            {
                result.Add(new Attribute { AttrLogicalName = attr.LogicalName, AttrType = attr.AttributeType.ToString(), AttrDisplayName = attr.DisplayName.LocalizedLabels[0]?.Label });
            }
        }

        if (withIndicators)
        {
            var query = new QueryExpression
            {
                EntityName = "ddsm_indicator",
                ColumnSet = new ColumnSet("ddsm_name", "ddsm_type"),
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression("statecode",ConditionOperator.Equal,0)//only active
                    }
                }
            };
            var indicators = crmWorkflowContext.OrganizationService.RetrieveMultiple(query);

            foreach (var indicator in indicators.Entities)
            {

                var type = indicator.GetAttributeValue<OptionSetValue>("ddsm_type");
                if (type != null)
                {
                    result.Add(new Attribute
                    {
                        AttrDisplayName = indicator.GetAttributeValue<string>("ddsm_name"),
                        //AttrLogicalName = indicator.GetAttributeValue<string>("ddsm_name"),
                        AttrType = ((IndicatorType)type.Value).ToString(),

                    });
                }
            }
        }
        // filter by type
        if (!string.IsNullOrEmpty(justFieldType))
        {
            result = result.Where(x => x.AttrType.ToLower().Equals(justFieldType.ToLower())).ToList();
        }

        var resultJson = DDSM.CommonProvider.JSON.JsonConvert.SerializeObject(result);
        Result.Set(context, resultJson);
        Complete.Set(context, true);

    }
}

