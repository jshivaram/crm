﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Linq;
using DynamicDSMMapping;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

public class GetOneToManyRelationships : WorkFlowActivityBase
{
    [Input("EntityLogicalName")]
    public InArgument<string> EntityLogicalName { get; set; }

    [Output("Total")]
    public OutArgument<int> Total { get; set; }

    public override void ExecuteCRMWorkFlowActivity(CodeActivityContext context, LocalWorkflowContext crmWorkflowContext)
    {
        try
        {
            var entityLogicalName = EntityLogicalName.Get<string>(context);

            var retrieveBankEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = entityLogicalName
            };

            var retrieveBankEntityResponse =
                (RetrieveEntityResponse)crmWorkflowContext.OrganizationService.Execute(retrieveBankEntityRequest);

            OneToManyRelationshipMetadata[] oneToManyRelationships =
                retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships;
            var entities = oneToManyRelationships.Select(rel => rel.ReferencingEntity);
            entities = entities.Distinct().OrderBy(e => e);

            var metadataRequests = new OrganizationRequestCollection();

            foreach (string entity in entities)
            {
                metadataRequests.Add(new RetrieveEntityRequest
                {
                    LogicalName = entity,
                    EntityFilters = EntityFilters.Entity
                });
            }

            var entitiesDisplayNamesRequest = new ExecuteMultipleRequest
            {
                Requests = metadataRequests,
                Settings = new ExecuteMultipleSettings
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                }
            };

            var response = (ExecuteMultipleResponse)crmWorkflowContext.OrganizationService.Execute(entitiesDisplayNamesRequest);

            var mappedResult = response.Responses.Select(resp =>
            {
                var entityMetadata = resp.Response as RetrieveEntityResponse;
                var relation = oneToManyRelationships.FirstOrDefault(rel => rel.ReferencingEntity == entityMetadata?.EntityMetadata.LogicalName);
                var IsCustomRelationship = relation?.IsCustomRelationship;
                var SchemaName = relation?.SchemaName;

                var mappedValue = new
                {
                    entityMetadata?.EntityMetadata.LogicalName,
                    entityMetadata?.EntityMetadata.LogicalCollectionName,
                    entityMetadata?.EntityMetadata.DisplayName.UserLocalizedLabel.Label,
                    IsCustomRelationship,
                    SchemaName,
                    relation?.ReferencingAttribute,
                };

                return mappedValue;
            }).ToList();

            var resultJson = DDSM.CommonProvider.JSON.JsonConvert.SerializeObject(mappedResult);
            Result.Set(context, resultJson);
            Complete.Set(context, true);
            Total.Set(context, mappedResult.Count);
        }
        catch (Exception e)
        {
            throw new InvalidPluginExecutionException(e.Message);
        }
    }
}
