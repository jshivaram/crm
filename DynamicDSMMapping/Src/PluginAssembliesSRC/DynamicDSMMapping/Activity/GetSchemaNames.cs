﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using DynamicDSMMapping;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;

public class GetSchemaNames : WorkFlowActivityBase
{
    [Input("PrimaryEntity")]
    public InArgument<string> PrimaryEntity { get; set; }

    [Input("RelatedEntity")]
    public InArgument<string> RelatedEntity { get; set; }

    public override void ExecuteCRMWorkFlowActivity(CodeActivityContext context, LocalWorkflowContext crmWorkflowContext)
    {
        try
        {
            var primaryEntity = PrimaryEntity.Get<string>(context);
            var relatedEntity = RelatedEntity.Get<string>(context);

            var retrieveBankEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = primaryEntity
            };

            var retrieveBankEntityResponse =
                (RetrieveEntityResponse)crmWorkflowContext.OrganizationService.Execute(retrieveBankEntityRequest);

            OneToManyRelationshipMetadata[] oneToManyRelationships =
                retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships;

            IEnumerable<string> filteredRelations = oneToManyRelationships
                .Where(rel => rel.ReferencingEntity == relatedEntity)
                .Select(rel => rel.SchemaName).ToList();

            var resultJson = DDSM.CommonProvider.JSON.JsonConvert.SerializeObject(filteredRelations);
            Result.Set(context, resultJson);
            Complete.Set(context, true);
        }
        catch (Exception e)
        {
            throw new InvalidPluginExecutionException(e.Message);
        }
    }
}
