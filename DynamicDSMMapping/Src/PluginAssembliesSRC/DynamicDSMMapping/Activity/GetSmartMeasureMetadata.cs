﻿using System;
using System.Activities;
using System.Collections.Generic;
using DDSM.CommonProvider.Utils;
using DynamicDSMMapping;
using DynamicDSMMapping.Model;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Newtonsoft.Json;


public class GetSmartMeasureMetadata : WorkFlowActivityBase
{
    private Dictionary<string, string> External2Entity = new Dictionary<string, string>
        {
            {"site", "ddsm_site"} , //Implementation Site
            {"implementationsite", "ddsm_site"} , //Implementation Site
            {"measure", "ddsm_measure"} ,
            { "account", "account"}, //parent account
            {"project", "ddsm_project"} , //Parent Project  
            { "rateclass", "ddsm_rateclass"} //Rate Class Reference
        };

    [Input("Smart Measure Id")]
    [RequiredArgument]
    public InArgument<string> SmartMeasureId { get; set; }

    public override void ExecuteCRMWorkFlowActivity(CodeActivityContext context, LocalWorkflowContext crmWorkflowContext)
    {
        var smartMeasureId = SmartMeasureId.Get(context);

        var smData = crmWorkflowContext.OrganizationService.Retrieve("ddsm_smartmeasureversion",
            Guid.Parse(smartMeasureId), new ColumnSet("ddsm_madfields", "ddsm_qddfields"));


        var madsJson = smData.GetAttributeValue<string>("ddsm_madfields");
        var qddsJson = smData.GetAttributeValue<string>("ddsm_qddfields");

        var espConfig = SettingsProvider.GetConfig<ESPConfig>(crmWorkflowContext.OrganizationService);

        var esp2crmTypes = espConfig.Esp2CrmTypes;

        var mads = DDSM.CommonProvider.JSON.JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(madsJson);
        var qdds = DDSM.CommonProvider.JSON.JsonConvert.DeserializeObject<List<ESPWSVariableDefinitionResponse>>(qddsJson);

        var fixedMads = FixEspTypes(esp2crmTypes, mads);
        var fixedQdds = FixEspTypes(esp2crmTypes, qdds);

        var result = new
        {
            ddsm_madfields = fixedMads,
            ddsm_qddfields = fixedQdds
        };
        Result.Set(context, DDSM.CommonProvider.JSON.JsonConvert.SerializeObject(result));
    }

    private List<SMMetadataItem> FixEspTypes(Dictionary<string, string> esp2crmTypes, List<ESPWSVariableDefinitionResponse> data)
    {
        if (esp2crmTypes == null || data == null)
        {
            return null;
        }

        var result = new List<SMMetadataItem>();
        foreach (var item in data)
        {
            result.Add(new SMMetadataItem
            {
                Entity = External2Entity[item.External1 ?? "measure"],
                FieldType = esp2crmTypes[item.DisplayFormat],
                FieldDisplayName = item.Name,
                ReadOnly = item.IsReadOnly
            });
        }

        return result;

    }

}

