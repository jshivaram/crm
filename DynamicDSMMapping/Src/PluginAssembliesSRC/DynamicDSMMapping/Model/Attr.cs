namespace DynamicDSMMapping.Model
{
    public class Attr
    {
        public string AttrDisplayName { get; set; }
        public string AttrLogicalName { get; set; }
        public string AttrType { get; set; }
        public string AttrTargetEntity { get; set; }
        public string TypeField { get; set; }

        public string IndicatorId { get; set; }
        public string IName { get; set; }
        public string IEntity { get; set; }
        public string IRelationName { get; set; }
        public string IFieldLogicalName { get; set; }
        public string IFieldTargetEntity { get; set; }
        public string IAdditionalAttr { get; set; }
    }

}
