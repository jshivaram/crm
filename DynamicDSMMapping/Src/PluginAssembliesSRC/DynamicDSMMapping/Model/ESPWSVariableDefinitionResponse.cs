﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DynamicDSMMapping.Model
{

    public enum ESPWSVariableType
    {
        Double,
        Text,
        DoubleList,
        TextList
    }

    public struct DependentComboBoxIndexPair : IComparable
    {
        public int Index1;
        public int Index2;

        public DependentComboBoxIndexPair(int a_index1, int a_index2)
        {
            Index1 = a_index1;
            Index2 = a_index2;
        }

        public int CompareTo(object obj)
        {
            var iReturnValue = 0;

            if (obj is DependentComboBoxIndexPair)
            {
                var otherPair = (DependentComboBoxIndexPair)obj;

                iReturnValue = Index1.CompareTo(otherPair.Index1);
                if (iReturnValue == 0)
                {
                    iReturnValue = Index2.CompareTo(otherPair.Index2);
                }
            }
            return iReturnValue;
        }

        public override string ToString()
        {
            return Index1 + "; " + Index2;
        }
    }
    [DataContract]
    public partial class ESPWSVariableDefinitionResponse
    {
        #region Private variables

        private string _stringValue;
        private List<string> _availableComboBoxStringValues;
        private List<double?> _availableComboBoxDoubleValues;

        #endregion

        #region Constructor

        #endregion

        #region Public properties

        #region Data members

        [DataMember]
        public ESPWSVariableType ESPWSVariableType { get; set; }

        [DataMember]
        public Guid ID { get; set; }

        [DataMember]
        public Guid? QDDID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool IsReadOnly { get; set; }

        [DataMember]
        public string DisplayFormat { get; set; }

        /// <summary>
        /// Always 0 in current implementation. Kept for possible future use.
        /// </summary>
        [DataMember]
        public int DisplayLevel { get; set; }

        [DataMember]
        public int DisplayOrderRank { get; set; }

        /// <summary>
        /// Must be null if type is DoubleValue or DoubleList.
        /// </summary>
        [DataMember]
        public string StringValue
        {
            get
            {
                return _stringValue;
            }
            set
            {
                _stringValue = value;
            }
        }
        /// <summary>
        /// Must be null if type is StringValue or StringList.
        /// </summary>
        [DataMember]
        public double? DoubleValue { get; set; }

        /// <summary>
        /// Must be null if type is not a StringList.
        /// </summary>
        [DataMember]
        public List<string> ComboBoxStringValues { get; set; }

        /// <summary>
        /// Must be null if type is not a DoubleList.
        /// </summary>
        [DataMember]
        public List<double?> ComboBoxDoubleValues { get; set; }

        [DataMember]
        public Guid? ComboBoxSelectionDependencyVariableID { get; set; }

        [DataMember]
        public List<DependentComboBoxIndexPair> ValIDComboBoxIndexPairs { get; set; }

        [DataMember]
        public string External1 { get; set; }
        [DataMember]
        public string External2 { get; set; }

        #endregion

        public object Value
        {
            get
            {
                switch (ESPWSVariableType)
                {
                    case ESPWSVariableType.Double:
                        return DoubleValue;

                    case ESPWSVariableType.Text:
                    default:
                        return StringValue;
                }
            }
        }

        public List<string> AvailableComboBoxStringValues
        {
            get
            {
                if (_availableComboBoxStringValues != null)
                {
                    return _availableComboBoxStringValues;
                }
                return ComboBoxStringValues;
            }
        }

        public List<double?> AvailableComboBoxDoubleValues
        {
            get
            {
                if (_availableComboBoxDoubleValues != null)
                {
                    return _availableComboBoxDoubleValues;
                }
                return ComboBoxDoubleValues;
            }
        }

        #endregion

        public int GetDefaultSelectedIndex()
        {
            var iReturnValue = -1;

            switch (ESPWSVariableType)
            {
                case ESPWSVariableType.Double:
                    if (DoubleValue.HasValue && ComboBoxDoubleValues != null)
                    {
                        iReturnValue = ComboBoxDoubleValues.IndexOf(DoubleValue);
                    }
                    break;
                case ESPWSVariableType.Text:
                default:
                    if (StringValue != null && ComboBoxStringValues != null)
                    {
                        iReturnValue = ComboBoxStringValues.IndexOf(StringValue);
                    }
                    break;
            }
            return iReturnValue;
        }

        public void SetDependentComboBoxValues(int a_iSelectedDependencyIndex)
        {
            if (ValIDComboBoxIndexPairs != null && ValIDComboBoxIndexPairs.Count > 0)
            {
                switch (ESPWSVariableType)
                {
                    case ESPWSVariableType.Double:
                        _availableComboBoxDoubleValues = new List<double?>();
                        foreach (var pair in ValIDComboBoxIndexPairs)
                        {
                            if (pair.Index1 == a_iSelectedDependencyIndex)
                            {
                                _availableComboBoxDoubleValues.Add(ComboBoxDoubleValues[pair.Index2]);
                            }
                        }
                        if (!_availableComboBoxDoubleValues.Contains(DoubleValue))
                        {
                            if (_availableComboBoxDoubleValues.Count == 1)
                            {
                                DoubleValue = _availableComboBoxDoubleValues[0];
                            }
                            else
                            {
                                DoubleValue = null;
                            }
                        }
                        break;

                    case ESPWSVariableType.Text:
                    default:
                        _availableComboBoxStringValues = new List<string>();
                        foreach (var pair in ValIDComboBoxIndexPairs)
                        {
                            if (pair.Index1 == a_iSelectedDependencyIndex)
                            {
                                _availableComboBoxStringValues.Add(ComboBoxStringValues[pair.Index2]);
                            }
                        }
                        if (!_availableComboBoxStringValues.Contains(StringValue))
                        {
                            if (_availableComboBoxStringValues.Count == 1)
                            {
                                StringValue = _availableComboBoxStringValues[0];
                            }
                            else
                            {
                                StringValue = null;
                            }
                        }
                        break;
                }
            }
        }
    }
}
