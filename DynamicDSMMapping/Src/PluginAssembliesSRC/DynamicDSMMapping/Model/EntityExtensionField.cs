namespace DynamicDSMMapping.Service
{
    public class EntityExtensionField
    {
        //[JsonProperty("Label")]
        public string Label { get; set; }
        public string LogicalName { get; set; }
    }
}