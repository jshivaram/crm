namespace DynamicDSMMapping.Service
{
    public enum IType
    {
        Decimal = 962080000, Money = 962080001, Integer = 962080002, DateTime = 962080003, Picklist = 962080004, String = 962080005, Lookup = 962080006, Memo = 962080007
    }
}