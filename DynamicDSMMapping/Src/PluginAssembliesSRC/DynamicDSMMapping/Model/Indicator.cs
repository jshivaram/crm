﻿using System;
using System.Collections.Generic;

namespace DynamicDSMMapping.Model
{
    public class Indicator
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public string CategoryGroup { get; set; }
        public string ValueEntity { get; set; }
        public string ValueRefField { get; set; }
        public List<InternalAttr> ValueFields { get; set; }

        public override string ToString()
        {
            return "Indicator: " + Name +
                "; Type: " + Type +
                "; Category: " + Category +
                "; Category Group: " + CategoryGroup +
                "; Entity Values: " + ValueEntity;
        }
    }
}