namespace DynamicDSMMapping.Model
{
    public class InternalAttr
    {
        public string DisplayName { get; set; }
        public string LogicalName { get; set; }
        public string AttrType { get; set; }
        public string TargetEntity { get; set; }
    }
}