﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicDSMMapping.Model
{



  public  class SMMetadataItem
    {
        public string Entity { get; set; }
        public string FieldDisplayName { get; set; }
        public string FieldType { get; set; }
        public bool ReadOnly { get; set; }
    }
}
