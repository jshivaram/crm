﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk.Metadata.Query;
using Microsoft.Xrm.Sdk.Metadata;
using System.Collections.Concurrent;
using DDSM.CommonProvider.JSON;
using DynamicDSMMapping.Model;

namespace DynamicDSMMapping.Service
{
    public class IndicatorService //: IDisposable , IEnumerable<Indicator>
    {
        private static ConcurrentDictionary<string, List<InternalAttr>> _dictionaryValueField = new ConcurrentDictionary<string, List<InternalAttr>>();
        private static List<InternalAttr> _iEntityFields = new List<InternalAttr>();

        public static string[] ExcludedFields = {
        "createdby",
        "createdonbehalfby",
        "createdon",
        "transactioncurrencyid",
        "exchangerate",
        "importsequencenumber",
        "modifiedby",
        "modifiedonbehalfby",
        "modifiedon",
        "overriddencreatedon",
        "ddsm_status",
        "statecode",
        "statuscode",
        "timezoneruleversionnumber",
        "traversedpath",
        "utcconversiontimezonecode",
        "owningteam",
        "owninguser",
        "owningbusinessunit",
        "versionnumber"
};

        public IndicatorService()
        {

            _dictionaryValueField = new ConcurrentDictionary<string, List<InternalAttr>>();
            _iEntityFields = new List<InternalAttr>();
            //   _collection = new List<Indicator>();
        }

        public static List<Indicator> GetList(IOrganizationService service, string entityLogicalName, bool withValueEntity = true)
        {
            var collection = new List<Indicator>();
            var query = new QueryExpression
            {
                EntityName = "ddsm_entityextension",
                ColumnSet = new ColumnSet("ddsm_entityextensionid", "ddsm_entityname"),
                Criteria = new FilterExpression(LogicalOperator.And)
            };
            query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

            var respExtention = service.RetrieveMultiple(query);
            if (!(respExtention != null && respExtention.Entities.Count > 0)) return collection;

            var eGuids = respExtention.Entities.Where(
                entity => entity.Contains("ddsm_entityname") &&
                          entity.Contains("ddsm_entityextensionid") &&
                          entity.GetAttributeValue<string>("ddsm_entityname").StartsWith("{") &&

                JsonConvert.DeserializeObject<EntityExtensionField>(entity.GetAttributeValue<string>("ddsm_entityname")).LogicalName.ToLower().Equals(entityLogicalName.ToLower()))
                .Select(x => x.GetAttributeValue<Guid>("ddsm_entityextensionid")).ToList();



            if (eGuids?.Count > 0)
            {
                foreach (var guid in eGuids)
                {
                    var relationship = new Relationship { SchemaName = "ddsm_ddsm_entityextension_ddsm_indicator" };

                    query = new QueryExpression
                    {
                        EntityName = "ddsm_indicator",
                        ColumnSet = new ColumnSet("ddsm_indicatorid", "ddsm_category", "ddsm_categorygroup", "ddsm_name",
                            "ddsm_targetentity", "ddsm_targetfield", "ddsm_type"),
                        Criteria = new FilterExpression(LogicalOperator.And)
                        {
                            Conditions = { new ConditionExpression("statecode", ConditionOperator.Equal, "Active") }
                        }
                    };

                    var relatedEntity = new RelationshipQueryCollection { { relationship, query } };

                    var request = new RetrieveRequest
                    {
                        ColumnSet = new ColumnSet("ddsm_entityextensionid"),
                        Target = new EntityReference { Id = guid, LogicalName = "ddsm_entityextension" },
                        RelatedEntitiesQuery = relatedEntity
                    };

                    var response = (RetrieveResponse)service.Execute(request);

                    var relatedEntitis = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship("ddsm_ddsm_entityextension_ddsm_indicator")].Entities;

                    collection = relatedEntitis.Where(x => x.Attributes.ContainsKey("ddsm_name")).Select(resp =>
                    {
                        var iAttr = resp.Attributes;

                        var name = resp.GetAttributeValue<string>("ddsm_name") ?? "";
                        var id = (iAttr.ContainsKey("ddsm_indicatorid")) ? resp.GetAttributeValue<Guid>("ddsm_indicatorid") : new Guid();
                        var type = ((IType)resp.GetAttributeValue<OptionSetValue>("ddsm_type")?.Value).ToString() ?? "";
                        var category = (iAttr.ContainsKey("ddsm_category")) ? resp.GetAttributeValue<string>("ddsm_category") : "";
                        var categoryGroup = resp.FormattedValues["ddsm_categorygroup"].ToString() ?? "";  //(int)resp.GetAttributeValue<OptionSetValue>("ddsm_categorygroup").Value : -2147483648;
                        var valueEntity = resp.GetAttributeValue<string>("ddsm_targetentity") ?? "";
                        var valueRefField = resp.GetAttributeValue<string>("ddsm_targetfield") ?? "";

                        _iEntityFields = new List<InternalAttr>();

                        if (!string.IsNullOrEmpty(name) && withValueEntity && !string.IsNullOrEmpty(valueEntity))
                        {
                            if (_dictionaryValueField.ContainsKey(valueEntity + "__" + type))
                            {
                                _iEntityFields = _dictionaryValueField[valueEntity + "__" + type];
                            }
                            else
                            {
                                var entityFilter = new MetadataFilterExpression(LogicalOperator.And)
                                {
                                    Conditions =
                                    {
                                        new MetadataConditionExpression("LogicalName", MetadataConditionOperator.Equals, valueEntity)
                                    }
                                };

                                var entityProperties = new MetadataPropertiesExpression { AllProperties = false, PropertyNames = { "Attributes", "DisplayName" } };


                                var attributeFilter = new MetadataFilterExpression(LogicalOperator.And)
                                {
                                    Conditions =
                                    {
                                        new MetadataConditionExpression("LogicalName", MetadataConditionOperator.NotIn, ExcludedFields),
                                        new MetadataConditionExpression("LogicalName", MetadataConditionOperator.NotEquals, "ddsm_name"),
                                        AddConditionByType(type)
                                    }
                                };

                                var attributeProperties = new MetadataPropertiesExpression
                                {
                                    AllProperties = false,
                                    PropertyNames = { "DisplayName", "LogicalName", "Targets", "AttributeType" }
                                };


                                var entityQueryExpression = new EntityQueryExpression
                                {
                                    Criteria = entityFilter,
                                    Properties = entityProperties,
                                    AttributeQuery = new AttributeQueryExpression { Criteria = attributeFilter, Properties = attributeProperties }
                                };

                                var retrieveMetadataChangesRequest = new RetrieveMetadataChangesRequest { Query = entityQueryExpression };

                                var respValues = (RetrieveMetadataChangesResponse)service.Execute(retrieveMetadataChangesRequest);

                                if (respValues != null && respValues.EntityMetadata != null && respValues.EntityMetadata.Count == 1)
                                {
                                    foreach (var attrIndicator in respValues.EntityMetadata[0].Attributes)
                                    {
                                        if (attrIndicator.DisplayName.LocalizedLabels.Count > 0 && string.IsNullOrEmpty(attrIndicator.AttributeOf))
                                        {

                                            switch (type)
                                            {
                                                case "Money":
                                                    if (attrIndicator.LogicalName.IndexOf("_base") != -1) { continue; }
                                                    _iEntityFields.Add(new InternalAttr
                                                    {
                                                        LogicalName = attrIndicator.LogicalName,
                                                        DisplayName = attrIndicator.DisplayName.LocalizedLabels[0]?.Label,
                                                        TargetEntity = "",
                                                        AttrType = type
                                                    });

                                                    break;

                                                case "Picklist":
                                                    if (attrIndicator.AttributeType == AttributeTypeCode.Lookup)
                                                    {
                                                        var attrLookupP = attrIndicator as LookupAttributeMetadata;
                                                        if (attrLookupP.Targets[0].ToString() == "ddsm_optionsetvalue")
                                                        {
                                                            _iEntityFields.Add(new InternalAttr
                                                            {
                                                                LogicalName = attrIndicator.LogicalName,
                                                                DisplayName = attrIndicator.DisplayName.LocalizedLabels[0]?.Label,
                                                                TargetEntity = attrLookupP.Targets[0].ToString(),
                                                                AttrType = "Lookup"
                                                            });
                                                        }
                                                    }
                                                    else if (attrIndicator.AttributeType == AttributeTypeCode.Picklist)
                                                    {
                                                        _iEntityFields.Add(new InternalAttr
                                                        {
                                                            LogicalName = attrIndicator.LogicalName,
                                                            DisplayName = attrIndicator.DisplayName.LocalizedLabels[0]?.Label,
                                                            TargetEntity = "",
                                                            AttrType = type
                                                        });
                                                    }

                                                    break;

                                                case "Lookup":
                                                    var attrLookup = attrIndicator as LookupAttributeMetadata;

                                                    if (attrLookup.Targets[0].ToString() != "ddsm_indicator" && attrLookup.Targets[0].ToString() != "ddsm_optionsetvalue")
                                                    {
                                                        _iEntityFields.Add(new InternalAttr
                                                        {
                                                            LogicalName = attrIndicator.LogicalName,
                                                            DisplayName = attrIndicator.DisplayName.LocalizedLabels[0]?.Label,
                                                            TargetEntity = attrLookup.Targets[0].ToString(),
                                                            AttrType = type
                                                        });
                                                    }

                                                    break;

                                                case "Decimal":
                                                case "Integer":
                                                case "DateTime":
                                                case "String":
                                                case "Memo":
                                                    _iEntityFields.Add(new InternalAttr
                                                    {
                                                        LogicalName = attrIndicator.LogicalName,
                                                        DisplayName = attrIndicator.DisplayName.LocalizedLabels[0]?.Label,
                                                        TargetEntity = "",
                                                        AttrType = type
                                                    });
                                                    break;
                                            }
                                        }
                                    }
                                }
                                _dictionaryValueField.AddOrUpdate(valueEntity + "__" + type, _iEntityFields, (oldkey, oldvalue) => _iEntityFields);
                            }

                        }

                        var mappedValue = new Indicator
                        {
                            Name = name,
                            Id = id,
                            Type = type,
                            Category = category,
                            CategoryGroup = categoryGroup,
                            ValueEntity = valueEntity,
                            ValueRefField = valueRefField,
                            ValueFields = _iEntityFields
                        };
                        return mappedValue;

                    }).ToList();
                }
            }

            return collection;
        }

        private static MetadataConditionExpression AddConditionByType(string type)
        {
            MetadataConditionExpression result = null;
            switch (type)
            {
                case "Decimal":
                case "Integer":
                    result = new MetadataConditionExpression("AttributeType", MetadataConditionOperator.Equals, AttributeTypeCode.Decimal);
                    break;

                case "Money":
                    result = new MetadataConditionExpression("AttributeType", MetadataConditionOperator.Equals, AttributeTypeCode.Money);
                    break;

                case "DateTime":
                    result = new MetadataConditionExpression("AttributeType", MetadataConditionOperator.Equals, AttributeTypeCode.DateTime);
                    break;

                case "Picklist":
                    result = new MetadataConditionExpression("AttributeType", MetadataConditionOperator.Equals, AttributeTypeCode.Lookup);
                    //AttributeFilter.Conditions.Add(new MetadataConditionExpression("AttributeType", MetadataConditionOperator.Equals, AttributeTypeCode.Picklist));
                    break;

                case "String":
                    result = new MetadataConditionExpression("AttributeType", MetadataConditionOperator.Equals, AttributeTypeCode.String);
                    break;

                case "Lookup":
                    result = new MetadataConditionExpression("AttributeType", MetadataConditionOperator.Equals, AttributeTypeCode.Lookup);
                    break;

                case "Memo":
                    result = new MetadataConditionExpression("AttributeType", MetadataConditionOperator.Equals, AttributeTypeCode.Memo);
                    break;
            }

            return result;
        }


        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}

        //protected virtual void Dispose(bool disposing)
        //{
        //    if (!this._disposed)
        //    {
        //        if (disposing)
        //        {
        //            if (_dictionaryValueField != null)
        //            {
        //                _dictionaryValueField?.Clear();
        //            }
        //            if (_iEntityFields != null)
        //            {
        //                _iEntityFields?.Clear();
        //            }

        //        }
        //        this._disposed = true;
        //    }
        //}

        //public IEnumerator<Indicator> GetEnumerator()
        //{
        //    return _collection.GetEnumerator();
        //}

        //IEnumerator IEnumerable.GetEnumerator()
        //{
        //    return _collection.GetEnumerator();
        //}

        //private OneToManyRelationshipMetadata GetRelationship(IOrganizationService orgService, string targetEntity, string referencedEntity)
        //{
        //    try
        //    {
        //        var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
        //        {
        //            EntityFilters = EntityFilters.Relationships,
        //            LogicalName = targetEntity
        //        };

        //        var retrieveBankEntityResponse =
        //            (RetrieveEntityResponse)orgService.Execute(retrieveBankAccountEntityRequest);

        //        var manyToOneRelationships = retrieveBankEntityResponse.EntityMetadata.ManyToOneRelationships;
        //        var rs =
        //            manyToOneRelationships.Where(r => r.ReferencedEntity == referencedEntity).ToList();

        //        return rs.Count == 0 ? null : rs[0];

        //    }
        //    catch (Exception e)
        //    {
        //        return null;
        //    }
        //}

    }

}
