﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Moq;
using UnitTests;

namespace UnitTest.Test
{
    [TestClass]
    public class MappingImplementation
    {
        [TestMethod]
        public void BaseWorkflowTest()
        {
            //Target

            /*var targetEntity = new Entity("ddsm_measure")
            {
                ["ddsm_measureselector"] =
                new EntityReference("ddsm_measuretemplate", new Guid("f51e0511-796c-e711-80cf-663933383038"))
            };*/

            var targetEntity = new Entity("account", new Guid("70816501-edb9-4740-a16c-6a5efbc05d84"))
            {
                ["ddsm_siteid"] = new EntityReference("ddsm_site",
                    new Guid("6C7731A7-CA9D-E711-80E0-626430656337"))
            };
            //var targetEntity = new Entity("ddsm_project");

            //Input parameters
            var inputs = new Dictionary<string, object>
            {
                {
                    "MappingImplementation",
                    new EntityReference("ddsm_mappingimplementation", new Guid("2200FD2B-869C-E711-80DE-663933383038"))
                },
                {"IsCreateFromTarget", true}
            };


            //Expected value(s)
            const string expected = null;

            //Invoke the workflow
            var output = InvokeWorkflow(_namespaceClassAssembly, ref targetEntity, inputs, TestMethod1Setup);

            //Test(s)
            Assert.AreEqual(expected, null);
        }

        /// <summary>
        ///     Modify to mock CRM Organization Service actions
        /// </summary>
        /// <param name="serviceMock">The Organization Service to mock</param>
        /// <returns>Configured Organization Service</returns>
        private static Mock<IOrganizationService> TestMethod1Setup(Mock<IOrganizationService> serviceMock)
        {
            //EntityCollection queryResult = new EntityCollection();
            ////Add created items to EntityCollection

            //serviceMock.Setup(t =>
            //    t.RetrieveMultiple(It.IsAny<QueryExpression>()))
            //    .ReturnsInOrder(queryResult);

            return serviceMock;
        }

        /// <summary>
        ///     Invokes the workflow.
        /// </summary>
        /// <param name="name">Namespace.Class, Assembly</param>
        /// <param name="target">The target entity</param>
        /// <param name="inputs">The workflow input parameters</param>
        /// <param name="configuredServiceMock">The function to configure the Organization Service</param>
        /// <returns>The workflow output parameters</returns>
        private static IDictionary<string, object> InvokeWorkflow(string name, ref Entity target,
            Dictionary<string, object> inputs,
            Func<Mock<IOrganizationService>, Mock<IOrganizationService>> configuredServiceMock)
        {
            var testClass = Activator.CreateInstance(Type.GetType(name)) as CodeActivity;

            var serviceMock = new Mock<IOrganizationService>();
            var factoryMock = new Mock<IOrganizationServiceFactory>();
            var tracingServiceMock = new Mock<ITracingService>();
            var workflowContextMock = new Mock<IWorkflowContext>();

            //Apply configured Organization Service Mock
            if (configuredServiceMock != null)
                serviceMock = configuredServiceMock(serviceMock);

            var crmConnectionString = ConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString;
            var connection = CrmConnection.Parse(crmConnectionString);

            IOrganizationService service = new OrganizationService(connection);

            //IOrganizationService service = serviceMock.Object;

            //Mock workflow Context
            var workflowUserId = Guid.NewGuid();
            var workflowCorrelationId = Guid.NewGuid();
            var workflowInitiatingUserId = Guid.NewGuid();

            //Workflow Context Mock
            workflowContextMock.Setup(t => t.InitiatingUserId).Returns(workflowInitiatingUserId);
            workflowContextMock.Setup(t => t.CorrelationId).Returns(workflowCorrelationId);
            workflowContextMock.Setup(t => t.UserId).Returns(workflowUserId);
            var workflowContext = workflowContextMock.Object;

            //Organization Service Factory Mock
            factoryMock.Setup(t => t.CreateOrganizationService(It.IsAny<Guid>())).Returns(service);
            var factory = factoryMock.Object;

            //Tracing Service - Content written appears in output
            tracingServiceMock.Setup(t => t.Trace(It.IsAny<string>(), It.IsAny<object[]>()))
                .Callback<string, object[]>(MoqExtensions.WriteTrace);
            var tracingService = tracingServiceMock.Object;

            //Parameter Collection
            var inputParameters = new ParameterCollection { { "Target", target } };
            workflowContextMock.Setup(t => t.InputParameters).Returns(inputParameters);

            //Workflow Invoker
            var invoker = new WorkflowInvoker(testClass);
            invoker.Extensions.Add(() => tracingService);
            invoker.Extensions.Add(() => workflowContext);
            invoker.Extensions.Add(() => factory);

            return invoker.Invoke(inputs);
        }

        #region Class Constructor

        private readonly string _namespaceClassAssembly;

        public MappingImplementation()
        {
            //[Namespace.class name, assembly name] for the class/assembly being tested
            //Namespace and class name can be found on the class file being tested
            //Assembly name can be found under the project properties on the Application tab
            _namespaceClassAssembly = "CreateRecord" + ", " + "DDSM.MappingImplementation";
        }

        #endregion

        #region Test Initialization and Cleanup

        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
        }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void ClassCleanup()
        {
        }

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void TestMethodInitialize()
        {
        }

        // Use TestCleanup to run code after each test has run
        [TestCleanup]
        public void TestMethodCleanup()
        {
        }

        #endregion
    }
}