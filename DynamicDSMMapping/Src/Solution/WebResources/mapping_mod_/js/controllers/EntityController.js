/****START intrerface***/
var EntityController = function() { // implements BasicMember, GoldMember

}
var emptyString = "do not map";

EntityController.prototype.loadMetadata = function(callback) {
    //debugger;
    var jsonDataControl = Xrm.Page.getAttribute("ddsm_jsondata");
    // Load to top existed mapping data
    // var grid = $("#grid").data("kendoGrid");
    if (jsonDataControl && jsonDataControl.getValue()) {
        window.top.datasetDict = window.top.JSON.parse(jsonDataControl.getValue());
        window.top.showLoadMADMsg = false;
    }
    if (callback) {
        callback();
    }
}




EntityController.prototype.getSourceEntityMetadata = function(entname, target, callback) {
    getSourceEntityMetadata(entname, target, callback);
}

EntityController.prototype.getGridColumns = function() {
    var columns = [{
            field: "SourceField",
            title: "Source Field",
            width: "120px",
        }, {
            hidden: true,
            field: "SourceFieldLogicalName"
        }, {
            field: "SourceFieldType",
            title: "Source Field Type",
            width: "120px"
        }, {
            field: "TargetField",
            title: "Target Field",
            width: "120px",
            editor: this.categoryDropDownEditor,
        }, {
            field: "TargetFieldType",
            title: "Target Field Type",
            hidden: true,
            width: "120px",
        }, {
            field: "TargetFieldLogicalName",
            hidden: true
        },
        /* {
                command: [{
                    name: "Clear",
                    click: this.clearClick
                }],
                width: "200px",
                title: "Clear mapping",
        }*/
    ];
    return columns;
};


EntityController.prototype.getTargetEntities = function() {
    return new kendo.data.DataSource({
        schema: KendoHelper.CRM.defEntityMetadataSchema(),
        sort: {
            field: "Label",
            dir: "asc"
        },
        transport: KendoHelper.CRM.transport(encodeURI(clientUrl + baseUrl + 'EntityDefinitions?$select=DisplayName,SchemaName,MetadataId')),
    });
}


EntityController.prototype.clearClick = function(e) {
    e.preventDefault();
    var tr = $(e.target).closest("tr");
    var data = this.dataItem(tr);
    if (data) {
        data.TargetField = "";
        data.TargetFieldLogicalName = "";
        data.TargetFieldType = "";
    } else {
        console.log("Can't clear mapping. Data is null")
    }

    var grid = $("#grid").data("kendoGrid");
    grid.refresh();
    //console.log("in custom button click!!!");
}

EntityController.prototype.prepareUI = function(mappingType) {
    $("#second").show();
    $("#right").show();
    $("#certOrg").hide();
    var jsonDataControl = Xrm.Page.getAttribute("ddsm_jsondata");
    var currentFormType = Xrm.Page.ui.getFormType();
    if (currentFormType === 1) {
        jsonDataControl.setValue("");
    }

    var targetEntityList = $("#targetEntityList").data("kendoMultiSelect");
    if (targetEntityList) {
        targetEntityList.value('');
    }

    var control = Xrm.Page.getControl("ddsm_ignoretype");
    if (control) {
        control.setVisible(false);
    }

    var viewModel = kendo.observable({
        isDisabled: true
    });
    kendo.bind('body', viewModel);

    var currentFormType = Xrm.Page.ui.getFormType();
    if (currentFormType != 1) {
        var mappingType = $('#mappingType').data("kendoDropDownList");
        if (mappingType) {
            mappingType.enable(false);
        }
    }
}

EntityController.prototype.actualizeGrid = function(selectedLogicalName) {
    //debugger;
    var formType = Xrm.Page.ui.getFormType();
    if (!selectedLogicalName) {
        if (formType == 2 || formType == 4 && !window.top.$selected_tab) {
            selectedLogicalName = Object.keys(window.top.datasetDict)[0];
        } else {
            selectedLogicalName = window.top.$selected_tab; //window.top.datasetDict ? : window.top.$selected_tab;

        }
    }
    console.log("-- actualize grid for entity: " + selectedLogicalName);

    var ds = {};
    if (selectedLogicalName) {
        if (!window.top.datasetDict || !window.top.datasetDict[selectedLogicalName]) {
            console.log("no existing data in top. getting data for grid from CRM");
            // get target field
            ds = new kendo.data.DataSource({
                sort: {
                    field: "SourceField",
                    dir: "asc"
                },
                schema: {
                    data: function(response) {
                        var sourceEntity = Xrm.Page.getAttribute("ddsm_sourceentity").getValue();
                        var value = window.top.$sourceMetaData[sourceEntity];
                        //var value = window.top.$modelDataTarget[selectedLogicalName];
                        var model = value.map(function(el) {
                            var targetField = [];

                            var autoMapControl = Xrm.Page.getAttribute("ddsm_useautomapping");
                            window.top.needMapping = autoMapControl ? autoMapControl.getValue() : false;
                            if (window.top.needMapping && window.top.$modelDataTarget[selectedLogicalName]) {
                                targetField = window.top.$modelDataTarget[selectedLogicalName].filter(function(el1) {
                                    return el1.TargetField == el.SourceField && el1.TargetFieldType == el.SourceFieldType;
                                });
                            }

                            return {
                                SourceField: el.SourceField,
                                SourceFieldLogicalName: el.SourceFieldLogicalName,
                                SourceFieldType: el.SourceFieldType,

                                AttributeOf: el.AttributeOf,

                                TargetField: targetField.length > 0 ? targetField[0].TargetField : "",
                                TargetFieldLogicalName: targetField.length > 0 ? targetField[0].TargetFieldLogicalName : "",
                                TargetFieldType: targetField.length > 0 ? targetField[0].TargetFieldType : "",
                            };
                        });
                        return model;
                    }
                },
            });
        } else {
            console.log("Found existing data in top. Restore previous data for grid.");
            ds = new kendo.data.DataSource({
                data: window.top.datasetDict[selectedLogicalName]
            });
        }

        /******* START Clear Virtual and Uniqueidentifier fileds ***********/


        var filter = {
            logic: "and",
            filters: []
        };

        filter.filters.push({
            field: "AttributeOf",
            operator: "isnull",
            value: null
        });


        ignoreFieldNameArray.forEach(function(entry) {
            filter.filters.push({
                field: "SourceFieldLogicalName",
                operator: "neq",
                value: entry
            });
        }, null);
        ignoreFieldTypeArray.forEach(function(entry2) {
            filter.filters.push({
                field: "SourceFieldType",
                operator: "neq",
                value: entry2
            });
        }, null);

        ignoreFieldDisplayNamePart.forEach(function(entry3) {
            filter.filters.push({
                field: "SourceField",
                operator: "doesnotcontain",
                value: entry3
            });
        }, null);

        ds.filter(filter);


        ds.view();
        if (!window.top.datasetDict) {
            window.top.datasetDict = {};
        }
        window.top.datasetDict[selectedLogicalName] = ds.data();


    } else {
        console.log("fill grid with empty datasource");
        ds = new kendo.data.DataSource();
    }

    /******* END Clear Virtual and Uniqueidentifier fileds ***********/
    return ds;
}

EntityController.prototype.drowLines = function(e) {
    console.log("-- in drow Lines");
    var items = this._data;

    if (!window.top.$selected_tab) {
        var tabsList = window.top.JSON.parse(Xrm.Page.getAttribute("ddsm_entities").getValue());
        if (tabsList && tabsList.length > 0)
            window.top.$selected_tab = tabsList[0].LogicalName;
    }

    if (window.top.$selected_tab && items && items.length > 0) {
        if (!window.top.datasetDict) {
            window.top.datasetDict = {};
        }
        window.top.datasetDict[window.top.$selected_tab] = items;
        Xrm.Page.getAttribute("ddsm_jsondata").setValue(window.top.JSON.stringify(window.top.datasetDict));
    }
    var tableRows = $(this.table).find("tr");
    tableRows.each(function(index) {
        var row = $(this);
        var Item = items[index];
        /*
                if (Item.TargetField === "Select field..." || Item.TargetField === emptyString) {
                    Item.TargetField = "";
                }
        */
        if (Item.TargetField && Item.SourceField) {
            row.addClass("green");
        } else {
            row.removeClass("green");
        }
    });
}

EntityController.prototype.categoryDropDownEditor = function(container, options) {

    var sourceType = options.model.SourceFieldType;
    let selectedTabLogicalName = window.top.$selected_tab;
    if (!window.top.$modelDataTarget || !window.top.$modelDataTarget[selectedTabLogicalName]) {
        kendo.ui.progress($("#grid"), true);
        getSourceEntityMetadata(selectedTabLogicalName, "$modelDataTarget",
            function() {
                console.log("Actualize grid from on TAB change - selectedTabLogicalName");
                createDropdown();
                //  Controller.actualizeGrid(selectedLogicalName);
            });
    } else {
        createDropdown();
    }

    function createDropdown() {
        var filteredTargetFields = [];
        window.top.$modelDataTarget[selectedTabLogicalName].
        forEach(function(item) {
            if (item.TargetFieldType === sourceType) {
                filteredTargetFields.push(item);
            }
        });

        /*
                filteredTargetFields.unshift({
                    TargetField: emptyString
                });
        */

        kendo.ui.progress($("#grid"), false);
        var grid = $("#grid").data("kendoGrid");
        $('<input data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoComboBox({
                valuePrimitive: true,
                filter: "startswith",
                autoBind: true,
                suggest: true,
                dataSource: filteredTargetFields,
                placeholder: "Select product",
                //optionLabel: "Select field...",
                dataTextField: "TargetField", //DisplayName
                dataValueField: "TargetFieldLogicalName", //LogicalName            
                change: function(e) {
                    var model = options.model;
                    var source = model.SourceField;
                    var type = model.SourceFieldType;
                    var logicalName = model.SourceFieldLogicalName;
                    var dataValue = e.sender.dataItem();
                    /*  if (model.TargetField === emptyString || model.TargetField === "Select field...") {
                          model.TargetFieldLogicalName = null;
                          model.TargetFieldType = null;
                          model.TargetField = null;
                      } else {*/
                    if (dataValue.TargetFieldLogicalName && dataValue.TargetField) {
                        model.TargetFieldLogicalName = dataValue.TargetFieldLogicalName;
                        model.TargetFieldType = type;
                        model.TargetField = dataValue.TargetField;
                    } else {
                        model.TargetFieldLogicalName = null;
                        model.TargetFieldType = null;
                        model.TargetField = null;
                    }

                    grid.refresh();
                    dataItems = grid.dataItems();
                    if (!window.top.datasetDict) {
                        window.top.datasetDict = {};
                    }
                    window.top.datasetDict[selectedTabLogicalName] = dataItems;
                    //  Xrm.Page.getAttribute("ddsm_jsondata").setValue(window.top.JSON.stringify(window.top.datasetDict));
                }
            });
    }

}

EntityController.prototype.isEditable = function(fieldName, model) {
    return fieldName === "TargetField";
}

/****END intrerface***/