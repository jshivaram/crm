/****START intrerface***/
// hardcoded for current implementation
var targetEntityLogicalName = "ddsm_certlib";
var emptyString = "do not map";
// record name of main configuration
var configRecordName = "Admin Data";
var mappingSourceType = ""; //existing, new
window.top.activateFirstTab = true;
var EsController = function () { }

EsController.prototype.ESPTypesToCRMTypes = function () {
    return {
        //----Fro ES
        "html ": "String",
        "email": "String",
        "number": "Integer", //Decimal
        "percent": "Decimal",
        "phone": "String",
        "url": "String",
        "calendar_date": "DateTime",
        "text": "String",
        //----For DLC
        "varchar": "String",
        "float": "Decimal",
        "date": "DateTime",
    }
};

//TODO: fix list
EsController.prototype.getTargetEntities = function (idOrg) {
    if (!idOrg) {
        return;
    }
    var list2 = [];
    var ds2 = new kendo.data.DataSource({
        schema: {
            data: function (response) {
                var value = response.value;

                var model = value.map(function (el) {
                    return {
                        Label: el.ddsm_name,
                        LogicalName: el.ddsm_certifiedcategoryid,
                        MetadataId: el.ddsm_metadataid,
                    }
                });
                return model;
            }
        },
        sort: {
            field: "Label",
            dir: "asc"
        },
        transport: KendoHelper.CRM.transport(clientUrl + baseUrl + 'ddsm_certifiedcategories?$select=ddsm_name,ddsm_metadataid&$filter=_ddsm_certifyingorganizationid_value eq ' + idOrg + '&$orderby=ddsm_name asc'),
    });
    ds2.fetch(function () {
        // debugger;
        var targetList = $("#targetEntityList").data("kendoMultiSelect");
        targetList.value("");
        targetList.setDataSource(ds2);
    });
}

EsController.prototype.getSourceEntityMetadata = function (entityname, result, callback) {
    console.log("get source entitie Fields for: " + entityname + " to " + result);
    // window.top.prevEntityNameMetadata = entityname;
    if (entityname == undefined) {
        console.log("Return,entity name is null");
        return;
    }
    if (result !== "$sourceMetaData") {
        getSourceEntityMetadata("ddsm_certlib", result, callback);
    } else {
        var fields = new kendo.data.DataSource({
            schema: {
                data: function (response) {
                    var value = response.value;
                    var model = value.map(function (el) {
                        var displayName = el.ddsm_name;
                        var type = el.ddsm_datatype;
                        var logicalName = el.ddsm_logicalname; //.toLowerCase();
                        if (result === "$sourceMetaData") {
                            return {
                                SourceField: displayName,
                                SourceFieldLogicalName: logicalName,
                                SourceFieldType: Controller.ESPTypesToCRMTypes()[type],
                            };
                        }
                    });
                    return model;
                }
            },
            pageSize: 100,
            transport: KendoHelper.CRM.transport(clientUrl + baseUrl + "ddsm_certifieditemfields?$select=ddsm_name,ddsm_logicalname,ddsm_datatype,ddsm_metadataid&$filter=_ddsm_certifiedcategoryid_value eq " + entityname),
        });

        fields.fetch(function () {
            if (!window.top[result]) {
                window.top[result] = {};
            }
            window.top[result][entityname] = fields.data();
            if (callback) {
                callback(entityname);
            }
        });
    }
};

EsController.prototype.getGridColumns = function () {
    ////debugger;
    if (!window.top.$modelDataTarget || !window.top.$modelDataTarget[targetEntityLogicalName]) {
        Controller.getSourceEntityMetadata(targetEntityLogicalName, "$modelDataTarget", columns);
    } else {
        return columns();
    }

    function columns() {
        var columns = [{
            field: "FieldDisplayName",
            title: "Certifier API Field Name",
        }, {
            field: "FieldName",
            title: "Certifier API Field Logical Name",
            width: "200px",
        }, {
            field: "FieldType",
            title: "Certifier API Field Type",
            width: "200px"
        }, {
            field: "Attributes",
            title: "CRM Field Name",
            filterable: false,
            editor: Controller.multiSelectCRMFieldEditor,
            template: Controller.multiSelectArrayToString,
            values: window.top.$modelDataTarget[targetEntityLogicalName].map(function (target) {
                return {
                    "AttrDisplayName": target.TargetField,
                    "AttrLogicalName": target.TargetFieldLogicalName,
                    "AttrType": target.TargetFieldType
                };
            })
        }, {
            field: "AttrLogicalName",
            title: "CRM Field Name",
            hidden: true,
        }, {
            field: "AttrType",
            title: "CRM Field Type",
            hidden: true,
        },];
        //debugger;
        return columns;
    }
};

EsController.prototype.clearClick = function (e) {
    e.preventDefault();
    var tr = $(e.target).closest("tr");
    var data = this.dataItem(tr);
    // data.AttrDisplayName = "";
    //   data.AttrLogicalName = "";
    console.log("EsController .  data for clean: ");
    console.log(data);
    var grid = $("#grid").data("kendoGrid");
    if (grid) {
        grid.refresh();
    }
};

EsController.prototype.loadMetadata = function (callback) {
    var jsonDataControl = Xrm.Page.getAttribute("ddsm_jsondata");
    if (jsonDataControl && jsonDataControl.getValue()) {
        window.top.datasetDict = window.top.JSON.parse(jsonDataControl.getValue());
        window.top.showLoadMADMsg = false;
    }
    var list2 = []; //new mapping 
    var uniqTypes = {};

    getSourceEntityMetadata(targetEntityLogicalName, "$modelDataTarget", callback);

};

EsController.prototype.FillControls = function () {
    /*create targetEntities for using in Tabs ---START*/
    var entitiesControl = Xrm.Page.getAttribute("ddsm_entities"); //
    var entities = [];
    if (!entitiesControl.getValue() || window.parent.JSON.parse(entitiesControl.getValue()).length == 0) {
        if (!window.top.datasetDict) {
            console.log("in EsController.prototype.FillControls. window.top.datasetDict is null. exiting");
            return;
        }
        entities = Object.keys(window.top.datasetDict);
    } else {
        entities = window.parent.JSON.parse(entitiesControl.getValue());
    }
    var dataItems = [];
    Controller.ddd(dataItems, entities);
};

EsController.prototype.prepareUI = function (mappingType) {
    console.log("--in EsController prepareUI");
    $("#second").hide();
    $("#right").show();
    $("#certOrg").show();
    var jsonDataControl = Xrm.Page.getAttribute("ddsm_jsondata");
    var currentFormType = Xrm.Page.ui.getFormType();
    if (currentFormType === 1) {
        jsonDataControl.setValue("");
    }

    var control = Xrm.Page.getControl("ddsm_ignoretype");
    if (control) {
        control.setVisible(true);
    }
    enableTargetEntityDropdown(true);
    $("#right-pane").css({ 'width': '220px', 'float': 'right', 'padding-top': '15px' });
    $("#horizontal").css({ 'width': '100%', 'margin-left': '-225px' });
    $("#left-pane").css({ 'padding-left': '225px' });
    $("#right-pane").show();
    /*
    $("#horizontalSplitter").kendoSplitter({
        panes: [
            { collapsible: false },
            { collapsible: false, resizable: false, scrollable: false, size: "225px"}
        ]
    });
*/
    $("#copyMapping").kendoButton({
        icon: "ungroup",
        click: function (e) {
            console.dir(window.top.$selected_tab);
            if (!!window.top.datasetDict && Object.keys(window.top.datasetDict).length < 2) {
                Alert.show("Please, add more entities for mapping.", null, [{
                    label: "Ok",
                    callback: function () {
                        return;
                    }
                }], "INFO", 500, 200);
                return;
            }

            Alert.show("Mapping will be Copied/Reused on all entities.", null, [{
                label: "Ok",
                callback: function () {
                    kendo.ui.progress($("#grid"), true);
                    if (!!window.top.$selected_tab && !!window.top.datasetDict) {
                        console.dir(window.top.datasetDict[window.top.$selected_tab]);
                        let keyTab = Object.keys(window.top.datasetDict);
                        let sourceDataTable = window.top.datasetDict[window.top.$selected_tab];
                        for (let i = 0; i < keyTab.length; i++) {
                            if (keyTab[i] != window.top.$selected_tab) {
                                console.dir("keyTab: " + keyTab[i]);
                                for (let l = 0; l < sourceDataTable.length; l++) {
                                    for (let k = 0; k < window.top.datasetDict[keyTab[i]].length; k++) {
                                        if (sourceDataTable[l].FieldName.toLowerCase() == window.top.datasetDict[keyTab[i]][k].FieldName.toLowerCase() && sourceDataTable[l].FieldType.toLowerCase() == window.top.datasetDict[keyTab[i]][k].FieldType.toLowerCase()) {
                                            if (!!sourceDataTable[l].Attributes && sourceDataTable[l].Attributes.length > 0) {

                                                var newAttributes = [];
                                                for (let m = 0; m < sourceDataTable[l].Attributes.length; m++) {
                                                    let attrObj = {};
                                                    attrObj.AttrDisplayName = sourceDataTable[l].Attributes[m].AttrDisplayName;
                                                    attrObj.AttrLogicalName = sourceDataTable[l].Attributes[m].AttrLogicalName;
                                                    attrObj.AttrType = sourceDataTable[l].Attributes[m].AttrType;
                                                    newAttributes.push(attrObj);
                                                }

                                                window.top.datasetDict[keyTab[i]][k].Attributes = newAttributes;

                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        Xrm.Page.getAttribute("ddsm_jsondata").setValue(window.top.JSON.stringify(window.top.datasetDict));
                        Xrm.Page.data.save();
                    }
                    kendo.ui.progress($("#grid"), false);
                }
            }, {
                label: "Cancel",
                callback: function () {
                    return;
                }
            }], "WARNING", 500, 200);
        }
    });
};

EsController.prototype.actualizeGrid = function (selectedLogicalName) {
    //////debugger;
    var formType = Xrm.Page.ui.getFormType();
    window.top.mappingSourceType = "existing";
    var selectedLogicalName = window.top.$selected_tab;

    window.top.showLoadMADMsg = false;
    if (selectedLogicalName == "" && window.top.datasetDict) {
        selectedLogicalName = Object.keys(window.top.datasetDict)[0];
        window.top.activateFirstTab = true;
    } else {
        console.log("exit, window.top.datasetDict is null ");
    }
    console.log("-- actualize grid for entity: " + selectedLogicalName);
    var ds = {};
    if (selectedLogicalName) {
        if (!window.top.datasetDict || !window.top.datasetDict[selectedLogicalName]) {
            console.log("no existing data in top. getting data for grid from CRM");
            var mads = [];
            if (!window.top.$sourceMetaData || !window.top.$sourceMetaData[selectedLogicalName]) {
                Controller.getSourceEntityMetadata(selectedLogicalName, "$sourceMetaData",
                    function () {
                        console.log("Actualize grid from on TAB change - selectedLogicalName");
                        ddd();
                    });

            } else {
                ddd();
            }

            function ddd() {
                var mads = window.top.$sourceMetaData[selectedLogicalName];
                var fields = window.top.$modelDataTarget[targetEntityLogicalName]; //crm fields

                var autoMapControl = Xrm.Page.getAttribute("ddsm_useautomapping");
                window.top.needMapping = autoMapControl ? autoMapControl.getValue() : false;
                console.log("Actualize data for new Entity " + selectedLogicalName + "Using auto mapping: " + window.top.needMapping);

                var model = window.top.needMapping ? mads.map(
                    function (mad) {
                        //   ////debugger;
                        var targetField = [];
                        if (fields) {
                            targetField = fields.filter(function (el) {
                                return el.TargetField.toLowerCase() == mad.SourceField.toLowerCase() && el.TargetFieldType == mad.SourceFieldType;
                            });
                        }
                        return {
                            Entity: "",
                            FieldName: mad.SourceFieldLogicalName,
                            FieldDisplayName: mad.SourceField,
                            FieldType: mad.SourceFieldType,
                            Attributes: targetField.length > 0 && targetField[0].TargetField && targetField[0].TargetFieldLogicalName && targetField[0].TargetFieldType ? [{
                                AttrDisplayName: targetField[0].TargetField,
                                AttrLogicalName: targetField[0].TargetFieldLogicalName,
                                AttrType: targetField[0].TargetFieldType,
                            }] : null
                        };

                    }) : mads.map(
                        function (mad) {
                            //   ////debugger;
                            var targetField = [];
                            if (fields) {
                                targetField = fields.filter(function (el) {
                                    return el.TargetField.toLowerCase() == mad.SourceField.toLowerCase() && el.TargetFieldType == mad.SourceFieldType;
                                });
                            }
                            return {
                                Entity: "",
                                FieldName: mad.SourceFieldLogicalName,
                                FieldDisplayName: mad.SourceField,
                                FieldType: mad.SourceFieldType,
                                Attributes: null
                            };

                        });

                if (!window.top["datasetDict"]) {
                    window.top["datasetDict"] = {};
                }
                window.top.datasetDict[selectedLogicalName] = model;

                ds = new kendo.data.DataSource({
                    data: window.top.datasetDict[selectedLogicalName],
                    pageSize: 100,
                    schema: {
                        model: {
                            fields: {
                                'Entity': { type: 'string' },
                                'FieldName': { type: 'string' },
                                'FieldDisplayName': { type: 'string' },
                                'FieldType': { type: 'string' },
                                //array of crm fields                                
                                'Attributes': {},
                            }
                        }
                    },
                });
                var grid = $("#grid").data("kendoGrid");
                grid.setDataSource(ds);
            }
        } else {
            console.log("Found existing data in top. Restore previous data for grid.");
            ds = new kendo.data.DataSource({
                data: window.top.datasetDict[selectedLogicalName],
                pageSize: 100,
                schema: {
                    model: {
                        id: 'FieldName',
                        fields: {
                            'Entity': { type: 'string' },
                            'FieldName': { type: 'string' },
                            'FieldDisplayName': { type: 'string' },
                            'FieldType': { type: 'string' },
                            //array of crm fields                                
                            'Attributes': {},
                        }
                    }
                },
            });
        }
    } else {
        console.log("fill grid with empty datasource");
        ds = new kendo.data.DataSource();
    }
    return ds;
    /******* END Clear Virtual and Uniqueidentifier fileds ***********/
};

EsController.prototype.fillGlobalDsDictionary = function (ds, callback) {
    var entities = getUniqueValues(ds);
    window.top.$selected_tab = entities[0];
    window.top.datasetDict = {};

    for (var index in entities) {
        var entity = entities[index];
        window.top.datasetDict[entity] = ds;
    }

    var mappGlobalDsDictionary = function (callback) {
        var cnt = 0;
        var qnt = Object.keys(window.top.datasetDict).length;

        for (var entityName in window.top.datasetDict) {
            getSourceEntityMetadata(entityName, "$modelDataTarget", function (ent) {
                cnt++;
                console.log("mappGlobalDsDictionary (" + ent + ")");
                var mads = window.top.datasetDict[ent];
                window.top.datasetDict[ent] = mads.map(
                    function (mad) {
                        var targetField = [];
                        if (window.top.$modelDataTarget[ent]) {
                            targetField = window.top.$modelDataTarget[ent].filter(function (el) {
                                return el.TargetField.toLowerCase() == mad.FieldDisplayName.toLowerCase() && el.TargetFieldType == mad.FieldType;
                            });
                        }
                        return {
                            FieldDisplayName: mad.FieldDisplayName,
                            FieldName: mad.FieldName,
                            FieldType: mad.FieldType,
                            Attributes: targetField.length > 0 && targetField[0].TargetField && targetField[0].TargetFieldLogicalName && targetField[0].TargetFieldType ? [{
                                AttrDisplayName: targetField[0].TargetField,
                                AttrLogicalName: targetField[0].TargetFieldLogicalName,
                                AttrType: targetField[0].TargetFieldType,
                            }] : null
                        };
                    }
                );
                if (cnt == qnt) {
                    if (callback) {
                        callback();
                    }
                }
            });
        }
    };


    var autoMapControl = Xrm.Page.getAttribute("ddsm_useautomapping");
    window.top.needMapping = autoMapControl ? autoMapControl.getValue() : false;
    // console.log("Actualize data for new Entity " +  + "Using auto mapping: " + window.top.needMapping);
    if (window.top.needMapping) {
        mappGlobalDsDictionary(callback);
    } else {
        if (callback) {
            callback();
        }
    }

};

EsController.prototype.ddd = function (dataItems, entities) {
    if (window.top.activateFirstTab) {
        if (entities && entities.length > 0) {
            Xrm.Page.getAttribute("ddsm_entities").setValue(window.top.JSON.stringify(entities));
        }
        createTabs();
        activateFirstTab();
        window.top.activateFirstTab = false;
    }
};

EsController.prototype.drowLines = function (e) {
    Controller.FillControls();
    console.log("-- in drow Lines");
    var items = this._data;
    if (!window.top.$selected_tab) {
        var tabsList = window.top.JSON.parse(Xrm.Page.getAttribute("ddsm_entities").getValue());
        if (tabsList && tabsList.length > 0)
            window.top.$selected_tab = tabsList[0].LogicalName;
    }
    if (window.top.$selected_tab && items && items.length > 0) {
        if (!window.top.datasetDict) {
            window.top.datasetDict = {};
        }
        window.top.datasetDict[window.top.$selected_tab] = items;
        Xrm.Page.getAttribute("ddsm_jsondata").setValue(window.top.JSON.stringify(window.top.datasetDict));
    }
    var tableRows = $(this.table).find("tr");
    tableRows.each(function (index) {
        var row = $(this);
        var Item = items[index];
        if (Item.Attributes && Item.Attributes.length > 0) {
            row.addClass("green");
        } else {
            row.removeClass("green");
        }
    });
    kendo.ui.progress($("#grid"), false);
};

EsController.prototype.multiSelectCRMFieldEditor = function (container, options) {
    ////debugger;
    var sourceType = options.model.FieldType;
    let selectedTabLogicalName = window.top.$selected_tab;
    console.log("in make dropdown for target Entity: " + selectedTabLogicalName);
    createMultiSelect();

    function createMultiSelect() {
        //debugger;
        var filteredTargetFields = [];
        var ignoreType = Xrm.Page.getAttribute("ddsm_ignoretype").getValue();
        options.values.forEach(function (item) {

            if (!(item.AttrDisplayName.toLowerCase().indexOf("z_") != -1 && item.AttrDisplayName.toLowerCase().indexOf("z_") == 0)) {
                if (!ignoreType) {
                    if (item.AttrType === sourceType) {
                        filteredTargetFields.push(item);
                    }
                } else {
                    switch (sourceType.toLowerCase()) {
                        case 'string':
                            if (item.AttrType.toLowerCase() == 'integer' || item.AttrType.toLowerCase() == 'decimal' || item.AttrType.toLowerCase() == 'string' || item.AttrType.toLowerCase() == 'datetime') {
                                filteredTargetFields.push(item);
                            }
                            break;
                        case 'integer':
                            if (item.AttrType.toLowerCase() == 'integer' || item.AttrType.toLowerCase() == 'decimal' || item.AttrType.toLowerCase() == 'string') {
                                filteredTargetFields.push(item);
                            }
                            break;
                        case 'decimal':
                            if (item.AttrType.toLowerCase() == 'decimal' || item.AttrType.toLowerCase() == 'integer' || item.AttrType.toLowerCase() == 'string') {
                                filteredTargetFields.push(item);
                            }
                            break;
                        case 'datetime':
                            if (item.AttrType.toLowerCase() == 'datetime' || item.AttrType.toLowerCase() == 'string') {
                                filteredTargetFields.push(item);
                            }
                            break;
                        default:
                            if (item.AttrType.toLowerCase() == 'string') {
                                filteredTargetFields.push(item);
                            }
                            break;
                    }
                }
            }
        });
        var filteredTargetFieldsDs = new kendo.data.DataSource({
            data: filteredTargetFields,
            group: { field: "AttrType" }
        });
        ////debugger;
        kendo.ui.progress($("#grid"), false);

        $('<input data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoMultiSelect({
                dataSource: filteredTargetFieldsDs,
                filter: "contains",
                placeholder: "Select CRM Fields...",
                suggest: true,
                autoBind: false,
                valuePrimitive: false,
                footerTemplate: 'Total #: instance.dataSource.total() # items found',
                template: ' #= data.AttrDisplayName#  (#= data.AttrLogicalName#)',
                dataTextField: "AttrDisplayName",
                dataValueField: "AttrLogicalName",

                //  optionLabel: "Select field...",
                change: function (e) {
                    var grid = $("#grid").data("kendoGrid");
                    dataItems = grid.dataItems();
                    if (!window.top.datasetDict) {
                        window.top.datasetDict = {};
                    }
                    if (selectedTabLogicalName != null) {
                        window.top.datasetDict[selectedTabLogicalName] = dataItems;
                    }
                    grid.refresh();
                }
            });
    }
};

EsController.prototype.multiSelectArrayToString = function (item) {
    var result = [];
    if (item.Attributes) {
        item.Attributes.forEach(function (crmField) {
            result.push(crmField.AttrDisplayName);
        });
        return result.join(', ');
    } else {
        return "";
    }
    //item.Attributes.join(', ');
};

EsController.prototype.isEditable = function (fieldName, model) {
    return fieldName === "Attributes";
};