/****START intrerface***/


// record name of main configuration
var configRecordName = "Admin Data";
var mappingSourceType = ""; //existing, new
window.top.activateFirstTab = true;
var MadController = function () { }


//TODO: create entity associationing outer types to crm types 
MadController.prototype.ESPTypesToCRMTypes = function () {

    /* return {
         "TextboxDouble4Decimal": "Decimal",
         "TextboxDouble3Decimal": "Decimal",
         "TextboxDouble2Decimal": "Decimal",
         "TextboxDouble1Decimal": "Decimal",
         "TextboxDouble0Decimal": "Decimal",
         "TextboxDoubleDollars2Decimal": "Money",
         "ComboboxStringValue": "Picklist",
         "TextboxStringValue": "String"
     }*/

};

MadController.prototype.ESPExternal1ToCRMEntities = function () {
    return {
        "site": "ddsm_site", //Implementation Site
        "implementationsite": "ddsm_site", //Implementation Site
        "measure": "ddsm_measure",
        "account": "account", //parent account
        "project": "ddsm_project", //Parent Project  
        "rateclass": "ddsm_rateclass" //Rate Class Reference
    }
};

MadController.prototype.getTargetEntities = function () {
    return new kendo.data.DataSource({
        schema: KendoHelper.CRM.defEntityMetadataSchema(),
        sort: {
            field: "Label",
            dir: "asc"
        },
        transport: KendoHelper.CRM.transport(encodeURI(clientUrl + baseUrl + 'EntityDefinitions?$select=DisplayName,SchemaName,MetadataId')),
    });
}

MadController.prototype.getGridColumns = function () {
    var columns = [{
        field: "Entity",
        title: "Entity",
        hidden: true,
    }, {
        field: "FieldDisplayName",
        title: "MAD Field Name",
        //width: "200px",
    }, {
        field: "FieldType",
        title: "MAD Field Type",
        width: "200px"
    }, {
        field: "AttrDisplayName",
        title: "CRM Field Name",
        //    width: "200px",
        editor: this.categoryDropDownEditor,
    }, {
        field: "AttrLogicalName",
        title: "CRM Field Name",
        //  width: "200px",
        hidden: true,
    }, {
        field: "AttrType",
        title: "CRM Field Type",
        hidden: true,
        //    width: "200px",
    }, {
        field: "ReadOnly",
        title: "Read Only",
        //    width: "120px",
    },
    ];
    return columns;
};

MadController.prototype.clearClick = function (e) {
    e.preventDefault();
    //  //////debugger;
    var tr = $(e.target).closest("tr");
    var data = this.dataItem(tr);
    data.AttrDisplayName = "";
    data.AttrLogicalName = "";

    console.log("MadController .  data for clean: ");
    console.log(data);

    var grid = $("#grid").data("kendoGrid");
    if (grid) {
        grid.refresh();
    }
};

MadController.prototype.getSourceEntityMetadata = function (entname, target, callback) {

    getSourceEntityMetadata(entname, target, callback);
};


MadController.prototype.loadMetadata = function (callback) {
    //debugger;
    var jsonDataControl = Xrm.Page.getAttribute("ddsm_jsondata");
    // Load to top existed mapping data
    // var grid = $("#grid").data("kendoGrid");
    if (jsonDataControl && jsonDataControl.getValue()) {
        window.top.datasetDict = window.top.JSON.parse(jsonDataControl.getValue());
        window.top.showLoadMADMsg = false;
        var list2 = []; //new mapping 
        var url = clientUrl + baseUrl + "ddsm_admindatas?$select=ddsm_espuniquemad,ddsm_espmapingdata,ddsm_esptocrmtypes&$filter=ddsm_name eq '" + configRecordName + "'";
        var ds2 = new kendo.data.DataSource({
            schema: {
                data: function (response) {
                    //DataSource model
                    var espDispName = "";
                    var fieldType = "";
                    var entity = "";
                    var dispName = "";
                    var attrName = "";
                    if (response.value.length > 0) {

                        // create new mapping if not existing
                        if (response.value[0].ddsm_espuniquemad !== undefined) {

                            if (response.value[0].ddsm_esptocrmtypes !== null) {
                                Controller.ESPTypesToCRMTypes = function () {
                                    return window.top.JSON.parse(response.value[0].ddsm_esptocrmtypes);
                                };
                            } else {
                                Alert.show("No ESP To CRM Types data found.", "Please Admin Data and try again.", null, "WARNING", 500, 200);
                                return;
                            }


                            //save unique mad to top
                            window.top.$espuniquemad = window.top.JSON.parse(response.value[0].ddsm_espuniquemad);
                            console.log("Existing mapping not found. Will create empty mapping");
                            //////debugger;
                            var values = window.top.$espuniquemad;
                            for (var index in values) {
                                var targetField = [];
                                var el = values[index];
                                var sourceField = el.Name;
                                var sourceFieldType = Controller.ESPTypesToCRMTypes()[el.DisplayFormat];
                                var entity = (el.External1 === null || el.External1 === "") ? Controller.ESPExternal1ToCRMEntities()["measure"] : Controller.ESPExternal1ToCRMEntities()[el.External1];

                                list2.push({
                                    Entity: entity,
                                    FieldDisplayName: sourceField,
                                    FieldType: sourceFieldType,
                                    ReadOnly: el.IsReadOnly,

                                });

                            }
                            window.top.$espuniquemad_c = list2;
                        }
                    } else {
                        console.error("No MAD Mapping data found. please setting up ESP and try again.");
                        if (!window.top.errorMsgShow) {
                            window.top.errorMsgShow = true;
                            var targetEntityList = $("#targetEntityList").data("kendoMultiSelect");
                            if (targetEntityList) {
                                targetEntityList.enable(false);
                            }
                            Alert.show("No MAD Mapping data found.", "Please load ESP Metadata  and try again.", null, "WARNING", 500, 200);
                        }
                    }
                    return list2;
                }
            },
            transport: KendoHelper.CRM.transport(url),
            pageSize: 100
        });
        //////debugger;
        ds2.fetch(function () {
            if (callback) {
                callback();
            }
        });
    } else {
        var entities = [];
        var list = []; //existing mad mapping
        var list2 = []; //new mapping 
        var url = clientUrl + baseUrl + "ddsm_admindatas?$select=ddsm_espuniquemad,ddsm_espmapingdata,ddsm_esptocrmtypes&$filter=ddsm_name eq '" + configRecordName + "'";
        var ds = new kendo.data.DataSource({
            schema: {
                data: function (response) {
                    //DataSource model
                    var espDispName = "";
                    var fieldType = "";
                    var entity = "";
                    var dispName = "";
                    var attrName = "";
                    var attributeOf = "";
                    if (response.value.length > 0) {
                        // if exist MAD mapping data
                        if (response.value[0].ddsm_espmapingdata !== undefined) {


                            if (response.value[0].ddsm_esptocrmtypes !== null) {
                                Controller.ESPTypesToCRMTypes = function () {
                                    return window.top.JSON.parse(response.value[0].ddsm_esptocrmtypes);
                                };
                            } else {
                                Alert.show("No ESP To CRM Types data found.", "Please Admin Data and try again.", null, "WARNING", 500, 200);
                                return;
                            }


                            var values = window.top.JSON.parse(response.value[0].ddsm_espmapingdata);
                            for (var prop in values) {
                                espDispName = prop;
                                var value = values[espDispName];
                                fieldType = value.FieldType;
                                entity = value.Entity;
                                dispName = value.DispName;
                                attrName = value.AttrName;
                                attributeOf = value.attributeOf;

                                list.push({
                                    Entity: entity,
                                    FieldDisplayName: espDispName,
                                    FieldType: fieldType,
                                    AttrDisplayName: dispName,
                                    AttrLogicalName: attrName,
                                    AttrType: fieldType,
                                    AttributeOf: attributeOf

                                });
                            }
                        }
                        // create new mapping if not existing
                        if (response.value[0].ddsm_espuniquemad !== undefined) {

                            if (response.value[0].ddsm_esptocrmtypes !== null) {
                                Controller.ESPTypesToCRMTypes = function () {
                                    return window.top.JSON.parse(response.value[0].ddsm_esptocrmtypes);
                                };
                            } else {
                                Alert.show("No ESP To CRM Types data found.", "Please Admin Data and try again.", null, "WARNING", 500, 200);
                                return;
                            }



                            //save unique mad to top
                            window.top.$espuniquemad = window.top.JSON.parse(response.value[0].ddsm_espuniquemad);
                            console.log("Existing mapping not found. Will create empty mapping");
                            var values = window.top.$espuniquemad;
                            for (var index in values) {
                                var targetField = [];
                                var el = values[index];
                                var sourceField = el.Name;
                                var sourceFieldType = Controller.ESPTypesToCRMTypes()[el.DisplayFormat];
                                var entity = (el.External1 === null || el.External1 === "") ? Controller.ESPExternal1ToCRMEntities()["measure"] : Controller.ESPExternal1ToCRMEntities()[el.External1];

                                list2.push({
                                    Entity: entity,
                                    FieldDisplayName: sourceField,
                                    FieldType: sourceFieldType,
                                    ReadOnly: el.IsReadOnly
                                });
                                window.top.$espuniquemad_c = list2;
                            }
                        }
                    } else {
                        console.error("No MAD Mapping data found. please setting up ESP and try again.");
                        if (!window.top.errorMsgShow) {
                            window.top.errorMsgShow = true;
                            var targetEntityList = $("#targetEntityList").data("kendoMultiSelect");
                            if (targetEntityList) {
                                targetEntityList.enable(false);
                            }
                            Alert.show("No MAD Mapping data found.", "Please load ESP Metadata  and try again.", null, "WARNING", 500, 200);
                        }

                    }
                    return list;
                }
            },
            transport: KendoHelper.CRM.transport(url),
            pageSize: 100
        });
        //////debugger;
        ds.fetch(function () {
            //////debugger;
            window.top.showLoadMADMsg = true;
            if (list.length > 0) { //&& list2.length > 0
                if (window.top.showLoadMADMsg) {
                    // window.top.showLoadMADMsg = false;
                    window.top.$selected_tab = null;
                    Alert.show("Would you like to load existing MAD mapping?", "This will load data from Admin Data record", [{
                        label: "Load Existing",
                        callback: function () {
                            Controller.fillGlobalDsDictionary(list, callback);
                        }
                    }, {
                        label: "Create New Mapping",
                        callback: function () {
                            // debugger;
                            Controller.fillGlobalDsDictionary(list2, callback);
                        }
                    }], "QUESTION", 500, 200);
                }
            } else {
                Controller.fillGlobalDsDictionary(list2, callback);
            }
        });
    }
};

MadController.prototype.FillControls = function () {
    // //debugger;    
    /*create targetEntities for using in Tabs ---START*/
    var entitiesControl = Xrm.Page.getAttribute("ddsm_entities"); //
    var entities = [];
    if (!entitiesControl.getValue() || window.parent.JSON.parse(entitiesControl.getValue()).length == 0) {
        if (!window.top.datasetDict) {
            console.log("in MadController.prototype.FillControls. window.top.datasetDict is null. exiting");
            return;
        }
        entities = Object.keys(window.top.datasetDict);

    } else {
        entities = window.parent.JSON.parse(entitiesControl.getValue());
    }


    var dropdownlist = $("#targetEntityList").data("kendoMultiSelect");
    if (dropdownlist) {
        dropdownlist.value(entities);
    }

    var dataItems = [];
    if (!window.top.entitiesList) {
        //   //////debugger;
        var ds1 = new kendo.data.DataSource({
            schema: KendoHelper.CRM.defEntityMetadataSchema(),
            sort: {
                field: "Label",
                dir: "asc"
            },
            pageSize: 100,
            transport: KendoHelper.CRM.transport(encodeURI(clientUrl + baseUrl + 'EntityDefinitions?$select=DisplayName,SchemaName,MetadataId')),
        });
        ds1.fetch(function () {
            window.top.entitiesList = ds1.data();
            dataItems = window.top.entitiesList;
            Controller.ddd(dataItems, entities);

        });
    } else {
        dataItems = window.top.entitiesList;
        Controller.ddd(dataItems, entities);
    }
};

MadController.prototype.prepareUI = function (mappingType) {
    console.log("--in MadController prepareUI");
    $("#right").show();
    $("#second").hide();
    var jsonDataControl = Xrm.Page.getAttribute("ddsm_jsondata");
    var currentFormType = Xrm.Page.ui.getFormType();
    if (currentFormType === 1) {
        jsonDataControl.setValue("");
    }

    var control = Xrm.Page.getControl("ddsm_ignoretype");
    if (control) {
        control.setVisible(false);
    }

    enableTargetEntityDropdown(true);
    enableTypeDropdown(false);
};

MadController.prototype.actualizeGrid = function (selectedLogicalName) {
    ////debugger;
    var formType = Xrm.Page.ui.getFormType();
    window.top.mappingSourceType = "existing";
    var selectedLogicalName = window.top.$selected_tab;

    window.top.showLoadMADMsg = false;
    if (!selectedLogicalName) {
        selectedLogicalName = Object.keys(window.top.datasetDict)[0];
        window.top.activateFirstTab = true;
    }

    console.log("-- actualize grid for entity: " + selectedLogicalName);

    var ds = {};
    var autoMapControl = Xrm.Page.getAttribute("ddsm_useautomapping");
    window.top.needMapping = autoMapControl ? autoMapControl.getValue() : false;
    var mads = window.top.$espuniquemad_c;
    if (selectedLogicalName) {
        if (!window.top.datasetDict || !window.top.datasetDict[selectedLogicalName]) {
            console.log("no existing data in top. getting data for grid from CRM");


            var fields = window.top.$modelDataTarget[selectedLogicalName]; //crm fields

            console.log("Actualize data for new Entity " + selectedLogicalName + "Using auto mapping: " + window.top.needMapping);
            debugger;
            var model = [];

            if (window.top.needMapping) {
                mads.forEach(
                    function (mad) {
                        var targetField = [];
                        if (fields) {
                            targetField = fields.filter(function (el) {
                                return el.TargetField.toLowerCase() == mad.FieldDisplayName.toLowerCase() && el.TargetFieldType == mad.FieldType;
                            });
                        }
                        model.push({
                            Entity: "",
                            FieldDisplayName: mad.FieldDisplayName,
                            FieldType: mad.FieldType,
                            AttrDisplayName: targetField.length > 0 ? targetField[0].TargetField : "",
                            AttrLogicalName: targetField.length > 0 ? targetField[0].TargetFieldLogicalName : "",
                            AttrType: targetField.length > 0 ? targetField[0].TargetFieldType : "",
                            ReadOnly: mad.ReadOnly,
                            AttributeOf: mad.AttributeOf
                        });

                    });
            }
            else {
                mads.forEach(
                    function (mad) {
                        model.push({
                            Entity: "",
                            FieldDisplayName: mad.FieldDisplayName,
                            FieldType: mad.FieldType,                            
                            ReadOnly: mad.ReadOnly,
                            AttributeOf: mad.AttributeOf
                        });

                    });
            }

            window.top.datasetDict[selectedLogicalName] = model;

            ds = new kendo.data.DataSource({
                data: window.top.datasetDict[selectedLogicalName],
                pageSize: 100,
            });
        } else {
            debugger;
            console.log("Found existing data in top. Restore previous data for grid.");
            
            var mads = window.top.$espuniquemad_c; //original mads (not mapped)

            if (!window.top.$modelDataTarget || window.top.$modelDataTarget[selectedLogicalName]) {
                getSourceEntityMetadata(selectedLogicalName, "$modelDataTarget", function () { ttt(true); });
            } else {
                ttt();
            }

            function ttt(fillGrid) {
                var fields = window.top.$modelDataTarget[selectedLogicalName]; //crm fields
                var alreadyMapped = window.top.datasetDict[selectedLogicalName]; //mapped mads
                var model = [];
                if (window.top.needMapping) {
                    mads.forEach(
                        function (mad) {
                            var targetField = [];
                            if (fields) {
                                targetField = fields.filter(function (el) {
                                    return el.TargetField.toLowerCase() == mad.FieldDisplayName.toLowerCase() && el.TargetFieldType == mad.FieldType;
                                });
                            }
                            model.push({
                                Entity: "",
                                FieldDisplayName: mad.FieldDisplayName,
                                FieldType: mad.FieldType,
                                AttrDisplayName: targetField.length > 0 ? targetField[0].TargetField : "",
                                AttrLogicalName: targetField.length > 0 ? targetField[0].TargetFieldLogicalName : "",
                                AttrType: targetField.length > 0 ? targetField[0].TargetFieldType : "",
                                ReadOnly: mad.ReadOnly,
                                AttributeOf: mad.AttributeOf
                            });

                        });
                }
                else {
                    mads.forEach(
                        function (mad) {
                            model.push({
                                Entity: "",
                                FieldDisplayName: mad.FieldDisplayName,
                                FieldType: mad.FieldType,                              
                                ReadOnly: mad.ReadOnly,
                                AttributeOf: mad.AttributeOf
                            });

                        });
                }

                var existingMappedData = alreadyMapped;/*.filter(function (el) {
                    return el['AttrDisplayName'] && el['AttrType'];
                });*/

                model.forEach(function (exMapping) {
                    var record = existingMappedData.filter(function (x) {
                        return x.FieldDisplayName == exMapping.FieldDisplayName;
                    });
                    if (record.length > 0) {
                        var newData = record[0];
                        exMapping['AttrDisplayName'] = newData.AttrDisplayName;
                        exMapping['AttrType'] = newData.AttrType;
                        exMapping['AttrLogicalName'] = newData.AttrLogicalName;
                    }
                    else {
                        console.log("existingMappedData for " + exMapping.FieldDisplayName + " not found");
                    }
                });
                window.top.datasetDict[selectedLogicalName] = model;
                if (fillGrid) {
                    var grid = $("#grid").data("kendoGrid");
                    grid.setDataSource(new kendo.data.DataSource({
                        data: window.top.datasetDict[selectedLogicalName],
                        pageSize: 100,
                    }))
                }
                else {
                    ds = new kendo.data.DataSource({
                        data: window.top.datasetDict[selectedLogicalName],
                        pageSize: 100,
                    });
                }

            }            
        }
    } else {
        console.log("fill grid with empty datasource");
        ds = new kendo.data.DataSource();

    }
    return ds;
    /******* END Clear Virtual and Uniqueidentifier fileds ***********/

};

MadController.prototype.fillGlobalDsDictionary = function (ds, callback) {
    //debugger;
    var entities = getUniqueValues(ds);
    window.top.$selected_tab = entities[0];
    window.top.datasetDict = {};

    for (var index in entities) {
        var entity = entities[index];
        window.top.datasetDict[entity] = ds; //ds.filter(function (ent) { if (ent.Entity === entity) return ent; })
    }

    var mappGlobalDsDictionary = function (callback) {

        var cnt = 0;
        var qnt = Object.keys(window.top.datasetDict).length;

        for (var entityName in window.top.datasetDict) {
            getSourceEntityMetadata(entityName, "$modelDataTarget", function (ent) {
                cnt++;
                console.log("mappGlobalDsDictionary (" + ent + ")");
                var mads = window.top.datasetDict[ent];
                window.top.datasetDict[ent] = mads.map(
                    function (mad) {

                        var targetField = [];
                        if (window.top.$modelDataTarget[ent]) {
                            targetField = window.top.$modelDataTarget[ent].filter(function (el) {
                                return el.TargetField.toLowerCase() == mad.FieldDisplayName.toLowerCase() && el.TargetFieldType == mad.FieldType;
                            });
                        }

                        return {
                            FieldDisplayName: mad.FieldDisplayName,
                            FieldType: mad.FieldType,
                            AttrDisplayName: targetField.length > 0 ? targetField[0].TargetField : "",
                            AttrLogicalName: targetField.length > 0 ? targetField[0].TargetFieldLogicalName : "",
                            AttrType: targetField.length > 0 ? targetField[0].TargetFieldType : "",
                            ReadOnly: mad.ReadOnly,
                        };
                    });
                if (cnt == qnt) {
                    if (callback) {
                        callback();
                    }
                }
            });
        }
    };


    var autoMapControl = Xrm.Page.getAttribute("ddsm_useautomapping");
    window.top.needMapping = autoMapControl ? autoMapControl.getValue() : false;
    // console.log("Actualize data for new Entity " +  + "Using auto mapping: " + window.top.needMapping);
    if (window.top.needMapping) {
        mappGlobalDsDictionary(callback);
    } else {
        if (callback) {
            callback();
        }
    }

};

MadController.prototype.ddd = function (dataItems, entities) {
    //////debugger;
    if (window.top.activateFirstTab) {
        if (dataItems) {
            var tabs = entities.map(function (e) {
                return getEntityFromSourceEntity(dataItems, e);
            }).filter(function (t) {
                return !!t;
            });
            ////////debugger;
            if (tabs && tabs.length > 0) {
                Xrm.Page.getAttribute("ddsm_entities").setValue(window.top.JSON.stringify(tabs));
            }
            createTabs();
            activateFirstTab();
            window.top.activateFirstTab = false;
        }
    }
};

MadController.prototype.drowLines = function (e) {
    Controller.FillControls();
    console.log("-- in drow Lines");
    var items = this._data;

    if (!window.top.$selected_tab) {
        var tabsList = window.top.JSON.parse(Xrm.Page.getAttribute("ddsm_entities").getValue());
        if (tabsList && tabsList.length > 0)
            window.top.$selected_tab = tabsList[0].LogicalName;
    }

    if (window.top.$selected_tab && items && items.length > 0) {
        if (!window.top.datasetDict) {
            window.top.datasetDict = {};
        }
        window.top.datasetDict[window.top.$selected_tab] = items;

        //////debugger;
        Xrm.Page.getAttribute("ddsm_jsondata").setValue(window.top.JSON.stringify(window.top.datasetDict));
    }
    var tableRows = $(this.table).find("tr");
    tableRows.each(function (index) {
        var row = $(this);
        var Item = items[index];

        if (Item.AttrDisplayName === "Select field..." || Item.AttrDisplayName === emptyString) {
            Item.AttrDisplayName = "";
        }

        if (Item.AttrDisplayName && Item.FieldDisplayName) {
            row.addClass("green");
        } else {
            row.removeClass("green");
        }
    });
    kendo.ui.progress($("#grid"), false);
};

MadController.prototype.categoryDropDownEditor = function (container, options) {
    var emptyString = "do not map";
    var sourceType = options.model.FieldType;
    let selectedTabLogicalName = window.top.$selected_tab;
    console.log("in make dropdown for target Entity: " + selectedTabLogicalName);

    if (!window.top.$modelDataTarget || !window.top.$modelDataTarget[selectedTabLogicalName]) {
        kendo.ui.progress($("#grid"), true);
        getSourceEntityMetadata(selectedTabLogicalName, "$modelDataTarget",
            function () {
                console.log("Actualize grid from on TAB change - selectedLogicalName");
                createDropdown();
                //  Controller.actualizeGrid(selectedLogicalName);
            });

    } else {
        createDropdown();
    }

    function createDropdown() {
        var filteredTargetFields = [];
        window.top.$modelDataTarget[selectedTabLogicalName].forEach(function (item) {
            if (item.TargetFieldType === sourceType) {
                filteredTargetFields.push(item);
            }
        });
        filteredTargetFields.unshift({
            TargetField: emptyString
        });
        kendo.ui.progress($("#grid"), false);

        $('<input required name="' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                valuePrimitive: true,
                filter: "startswith",
                autoBind: false,
                dataSource: filteredTargetFields,
                optionLabel: "Select field...",
                dataTextField: "TargetField",
                dataValueField: "TargetFieldLogicalName",

                change: function (e) {
                    var model = options.model;
                    var source = model.SourceField;
                    var type = model.SourceFieldType;

                    var logicalName = model.Field;
                    if (model.AttrDisplayName === emptyString || model.AttrDisplayName === "Select field...") {
                        model.AttrDisplayName = null;
                        model.AttrLogicalName = null;
                        model.AttrType = null;
                    } else {
                        model.AttrLogicalName = e.sender.dataItem().TargetFieldLogicalName;
                        model.AttrDisplayName = e.sender.dataItem().TargetField;
                        model.AttrType = e.sender.dataItem().TargetFieldType;
                    }
                    var grid = $("#grid").data("kendoGrid");

                    dataItems = grid.dataItems();
                    if (!window.top.datasetDict) {
                        window.top.datasetDict = {};
                    }
                    if (selectedTabLogicalName != null) {
                        window.top.datasetDict[selectedTabLogicalName] = dataItems;
                    }
                    grid.refresh();
                    // Xrm.Page.getAttribute("ddsm_jsondata").setValue(window.top.JSON.stringify(window.top.datasetDict));
                }
            });
    }


};

MadController.prototype.isEditable = function (fieldName, model) {
    return fieldName === "AttrDisplayName";
};