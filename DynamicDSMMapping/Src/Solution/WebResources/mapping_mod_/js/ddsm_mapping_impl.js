var clientUrl = window.top.Xrm.Page.context.getClientUrl();
var baseUrl = "/api/data/v8.2/";
var form = window.parent;

var mappingType = {
    Entity: 962080000
};

var listOfRelationsManyToOneUrl = clientUrl + baseUrl + "ddsm_DDSMGetManyToOneRelationships";
var listOfRelationsOneToManyUrl = clientUrl + baseUrl + "ddsm_DDSMGetOneToManyRelationships";
var listOfMappingImplementationsUrl = clientUrl + baseUrl + "ddsm_mappingimplementations?$select=ddsm_mappingimplementationid,ddsm_name";
var listofSchemaNamesUrl = clientUrl + baseUrl + "ddsm_DDSMGetSchemaNames";

var savedJson = form.Xrm.Page.getAttribute("ddsm_json").getValue();
var savedData = savedJson ? JSON.parse(savedJson) : null;
var selectedMapping = savedData ? savedData.SelectedMapping : null;

var mainGridIsLoaded = false;

var ddsm_mapping = form.Xrm.Page.getAttribute("ddsm_mapping");

function getMappingUrl() {
    var mappingValue = ddsm_mapping.getValue();
    if (!mappingValue) {
        return;
    }

    var mappingId = mappingValue[0].id.slice(1, -1).toLowerCase();
    var mappingUrl = clientUrl + baseUrl + "ddsm_mappings?$select=ddsm_selectedtabs,ddsm_sourceentity&$filter=ddsm_mappingid eq " + mappingId + " and ddsm_mappingtype eq " + mappingType.Entity;

    return mappingUrl;
}

ddsm_mapping.addOnChange(function () {

    var mappingUrl = getMappingUrl();

    if (!mappingUrl) {
        var $grid = $("#grid-of-mapping-entities");
        var kendoGrid = $grid.data("kendoGrid");
        if (kendoGrid) {
            kendoGrid.destroy();
            $grid.empty();
        }
        form.Xrm.Page.getAttribute("ddsm_json").setValue("");
        return;
    }

    mappingHandler(mappingUrl);
});

function mappingHandler(mappingUrl) {
    $.get({ url: mappingUrl, dataType: "json" }).done(function (response) {
        var values = response.value;

        if (!values.length) {
            return;
        }

        var data = values[0];

        window.ddsm_sourceentity = data.ddsm_sourceentity;

        var $grid = $("#grid-of-mapping-entities");
        var kendoGrid = $grid.data("kendoGrid");

        var entities = JSON.parse(data.ddsm_selectedtabs);

        if (!kendoGrid) {
            createGrid(entities);
            return;
        }

        var dataSource = getGridDataSource(entities);
        kendoGrid.setDataSource(dataSource);
        Save();
    });
}

ddsm_mapping.fireOnChange();

function createGrid(data) {

    var mappedDatasource = mapData(data);

    var $grid = $("#grid-of-mapping-entities");
    $grid.kendoGrid({
        dataSource: mappedDatasource,
        height: 650,
        sortable: true,
        pageable: true,
        columns: [
            {
                field: "Label",
                title: "Entity Name",
                width: "150px"
            },
            {
                field: "Lookup",
                title: "Lookup",
                type: "object",
                editor: createRelatedEntityEditor,
                template: function (param) {
                    var lookup = param.Lookup ? param.Lookup.Label : "";

                    return lookup;
                },
                width: "200px"
            },
            {
                title: ""
            }
        ],
        editable: true,
        detailInit: detailInit,
        dataBound: function () {
            Save();
        }
    });
}

function getGridDataSource(data) {
    var dataSource = new kendo.data.DataSource({
        change: function () {
            if (!mainGridIsLoaded) {
                mainGridIsLoaded = true;
                return;
            }
            Save();
        },
        data: data,
        schema: {
            model: {
                fields: {
                    Label: {
                        type: "string",
                        editable: false
                    },
                    LogicalName: {
                        type: "string",
                        editable: false
                    },
                    Lookup: {
                        validation: {
                            required: true
                            // lookupvalidation: function (input) {
                            //     if (input.is("[name='Lookup']") && !input.val()) {
                            //         debugger;
                            //         input.attr("data-lookupvalidation-msg", "Lookup is empty");
                            //         return false;
                            //     }

                            //     return true;
                            // }
                        }
                    },
                    Relationships: {}
                }
            }
        },
        pageSize: 15
    });

    return dataSource;
}

function mapData(data) {

    if (!savedData) {
        var baseDatasource = getGridDataSource(data);
        return baseDatasource;
    }

    var mappedData = [];

    data.forEach(function (d) {
        var foundItem = savedData.Tabs.find(function (item) {
            return d.LogicalName === item.LogicalName;
        });

        if (foundItem) {
            d.Lookup = foundItem.Lookup;
            d.Relationships = foundItem.Relationships;
        }

        mappedData.push(d);
    });

    var mappedDatasource = getGridDataSource(mappedData);
    return mappedDatasource;
}

function createRelatedEntityEditor(container, options) {

    var sourceEntity = window.ddsm_sourceentity ? window.ddsm_sourceentity : "";
    var relatedEntity = options.model.LogicalName;

    $('<select name="' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            optionLabel: {
                Label: "--",
                LogicalName: ""
            },
            valuePrimitive: false,
            dataTextField: "Label",
            dataValueField: "LogicalName",
            filter: "contains",
            change: function(e) {
                var lookup = e.sender.dataItem();

                if (!lookup.LogicalName) {
                    options.model.Lookup = null;
                    return;
                }

                options.model.Lookup = lookup;
            },
            dataSource: {
                schema: {
                    data: function (response) {
                    //debugger;
                        return JSON.parse(response.Result);
                    }
                },
                transport: {
                    read: {
                        type: "POST",
                        url: listOfRelationsManyToOneUrl,
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json'
                    },
                    parameterMap: function (options, operation) {
                        var postData = {
                            SourceEntity: sourceEntity,
                            RelatedEntity: relatedEntity
                        };

                        // note that you may need to merge that postData with the options send from the DataSource
                        return JSON.stringify(postData);
                    }
                }
            }
        });
}

function detailInit(e) {
    var entityLogicalName = e.data.LogicalName;
    var parentGrid = e;
    var subgridIsLoaded = false;

    var $grid = $("<div/>");
    $grid.appendTo(e.detailCell).kendoGrid({
        dataSource: {
            change: function () {
                if (!subgridIsLoaded) {
                    subgridIsLoaded = true;
                    return;
                }

                var data = this.data();
                parentGrid.data.Relationships = data;
                Save();
            },
            schema: {
                data: function (response) {

                    if (!response || !response.Result) {
                        return [];
                    }

                    var data = JSON.parse(response.Result);
                    var mappedData = mapSubgridData(data, parentGrid);
                    return mappedData;
                },
                total: function (response) {

                    if (!response || !response.Total) {
                        return 0;
                    }

                    return response.Total;
                },
                model: {
                    fields: {
                        LogicalName: {
                            type: "string",
                            editable: false
                        },
                        Label: {
                            type: "string",
                            editable: false
                        },
                        SchemaName: {
                            editable: true
                        },
                        Implement: {
                            type: "boolean",
                            editable: true
                        },
                        MappingImplementation: {
                            editable: true
                        }
                    }
                }
            },
            transport: {
                read: {
                    type: "POST",
                    url: listOfRelationsOneToManyUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json'
                },
                parameterMap: function (options, operation) {
                    var postData = {
                        EntityLogicalName: entityLogicalName
                    };

                    // note that you may need to merge that postData with the options send from the DataSource
                    return JSON.stringify(postData);
                }
            },
            pageSize: 10
        },
        sortable: true,
        pageable: true,
        editable: true,
        filterable: true,
        columns: [
            {
                title: "Related Entity",
                field: "Label",
                width: "150px"
            },
            {
                width: "100px",
                title: "Implement",                
                template: '<input type="checkbox" #= Implement ? \'checked="checked"\' : "" # class="chkbx" />'
            },
            {
                title: "Schema Name",
                width: "200px",
                field: "SchemaName",
                editor: createSchemaNamesEditor(parentGrid)
            },
            {
                title: "Mapping Implementation",
                width: "200px",
                field: "MappingImplementation",
                editor: createMappingImplementationEditor,
                template: function (param) {
                    var name = param.MappingImplementation ? param.MappingImplementation.Name : "";

                    return name;
                },
                filterable: false
            },
            {
                title: ""
            }
        ]
    });

    $grid.find(".k-grid-content").on("change", "input.chkbx", function (e) {
        var kendoGrid = $grid.data("kendoGrid");
        var dataItem = kendoGrid.dataItem($(e.target).closest("tr"));

        dataItem.set("Implement", this.checked);
    });
}

function mapSubgridData(data, parentGrid) {
    if (!parentGrid.data.Relationships) {
        return data;
    }

    parentGrid.data.Relationships.forEach(function (relation) {
        var foundData = data.find(function (d) {
            var compareResult = d.LogicalName === relation.LogicalName;
            return compareResult;
        });

        if (foundData) {
            foundData.Implement = relation.Implement;
            foundData.MappingImplementation = relation.MappingImplementation;
            foundData.SchemaName = relation.SchemaName;
        }
    });

    return data;
}

function createMappingImplementationEditor(container, options) {

    $('<select name="' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            optionLabel: {
                Name: "--",
                Id: ""
            },
            valuePrimitive: false,
            dataTextField: "Name",
            dataValueField: "Id",
            filter: "contains",
            dataSource: {
                schema: {
                    data: function (response) {
                        var value = response.value.map(function (item) {
                            return {
                                Name: item.ddsm_name,
                                Id: item.ddsm_mappingimplementationid
                            };
                        });

                        return value;
                    }
                },
                transport: {
                    read: {
                        type: "GET",
                        url: listOfMappingImplementationsUrl,
                        dataType: 'json'
                    }
                }
            }
        });
}

function createSchemaNamesEditor(parentGrid) {
    return function (container, options) {
        var primatyEntity = parentGrid.data.LogicalName;
        var relatedEntity = options.model.LogicalName;

        $('<select name="' + options.field + '"/>')
            .appendTo(container)
            .kendoComboBox({
                placeholder: "--",
                valuePrimitive: true,
                filter: "contains",
                dataSource: {
                    schema: {
                        data: function (response) {
                            return JSON.parse(response.Result);
                        }
                    },
                    transport: {
                        read: {
                            type: "POST",
                            url: listofSchemaNamesUrl,
                            contentType: "application/json; charset=utf-8",
                            dataType: 'json'
                        },
                        parameterMap: function (options, operation) {
                            var postData = {
                                PrimaryEntity: primatyEntity,
                                RelatedEntity: relatedEntity
                            };

                            // note that you may need to merge that postData with the options send from the DataSource
                            return JSON.stringify(postData);
                        }
                    }
                }
            });
    }
}

function Save() {
    var grid = $("#grid-of-mapping-entities").data("kendoGrid");
    var mappingValue = ddsm_mapping.getValue()[0];

    var dropDownData = {
        Id: mappingValue.id.slice(1, -1).toLowerCase(),
        Name: mappingValue.name
    };
    var gridData = grid.dataItems();

    var result = {
        SelectedMapping: dropDownData,
        Tabs: gridData
    };

    var json = JSON.stringify(result);
    var oldJson = form.Xrm.Page.getAttribute("ddsm_json").getValue();

    if (json !== oldJson) {
        form.Xrm.Page.getAttribute("ddsm_json").setValue(json);
    }
}

form.Xrm.Page.data.entity.addOnSave(function (execContext) {

    if (!ddsm_mapping.getValue()) {
        return;
    }

    var validationResult = validateDatasource();
    if (!validationResult) {
        form.Alert.show("You should fill all lookups", null, null, "ERROR");
        execContext.getEventArgs().preventDefault();
    }
});

function validateDatasource() {
    var grid = $("#grid-of-mapping-entities").data("kendoGrid");
    var data = grid.dataItems();

    var result = data.find(function (item) {
        return !item.Lookup;
    });

    return !result;
}