function CloneRecord(selectedItems) {
    var recordId = "";
    if (Xrm && Xrm.Page && Xrm.Page.data && Xrm.Page.data.entity) {
        recordId = Xrm.Page.data.entity.getId();
    }
    else {
        recordId = selectedItems[0];
    }

    Alert.show("Would you like to Clone selected record?", "", [
        new Alert.Button("Yes", function () {
            var organizationUrl = Xrm.Page.context.getClientUrl();
            var query = "ddsm_DDSMCloneMappingrecord"; // Вызов Action new_TestAction и ередача GUID’а записи
            var req = new XMLHttpRequest();
            var data = { "MappingRef": { "ddsm_mappingid": recordId } };
            req.open("POST", organizationUrl + "/api/data/v8.1/" + query, true);
            req.setRequestHeader("Accept", "application/json");
            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            req.setRequestHeader("OData-MaxVersion", "4.0");
            req.setRequestHeader("OData-Version", "4.0");
            req.onreadystatechange = function () {
                debugger;
                if (this.readyState == 4) {
                    req.onreadystatechange = null;
                    if (this.status == 200 || this.status == 204) {
                        Alert.show("Record clonned successfully!", null, [new Alert.Button("Ok", function () { window.location.assign(document.URL); })], "SUCCESS", 500, 200);
                    } else {
                        var error = JSON.parse(this.response).error;
                        console.log(error);
                    }
                }
            };
            req.send(JSON.stringify(data)); // Передаем входящие параметры         

        }),
        new Alert.Button("No")
    ], "QUESTION", 500, 200);

}