var baseUrl = "/api/data/v8.1/";
var Xrm = window.parent.Xrm;
var clientUrl = Xrm.Page.context.getClientUrl();
var isFiltered = false;
window.top.$modelDataTarget = {};
window.top.$sourceMetaData = {};
var Controller = {};

//Fill Controller by config and init controls
function Init() {
    ////////debugger;
    var typeMapping = 0;
    var typeMappingControl = Xrm.Page.getAttribute("ddsm_mappingtype");
    if (typeMappingControl) {
        typeMapping = typeMappingControl.getValue();
    }
    window.top.datasetDict = null;
    window.top.entitiesList = null;
    window.top.$modelDataTarget = null;
    window.top.activateFirstTab = true;
    window.top.$selected_tab = null;
    window.top.prevEntityNameMetadata = null;
    window.top.showLoadMADMsg = false;
    window.top.errorMsgShow = false;

    switch (typeMapping) {
        default:
            case 962080003:
            case 962080000:
        //////////debugger;
            Controller = new EntityController();
        Controller.loadMetadata(initControls);
        console.log("--Selected EntityController");
        break;

        case 962080001:
                Controller = new MadController();
            Controller.loadMetadata(initControls);
            console.log("--Selected MadController");
            kendo.ui.progress($("#grid"), true);
            break;
        case 962080002:
                Controller = new QddController();
            Controller.loadMetadata(initControls);
            kendo.ui.progress($("#grid"), true);
            console.log("--Selected QddController");
            break;
            // Load EnergyStar controller implementation
        case 962080004:
                Controller = new EsController();
            initControls();
            Controller.loadMetadata(initControls);
            kendo.ui.progress($("#grid"), true);
            console.log("--Selected EsController");
            break;
    }
    /*Cleaning existing metadata when type in changed  */

}
Init();

$('#mappingType').empty().kendoDropDownList({
    dataTextField: "DisplayName",
    dataValueField: "Value",
    dataSource: getMappingTypeOptionSet(),
    select: function(e) {
        //clear multiselect
        var targetEntityList = $("#targetEntityList").data("kendoMultiSelect");
        targetEntityList.value('');

        //clear tabs
        var $tabStrip = $("#tabstrip");
        $tabStrip.empty();

        $("#grid").data("kendoGrid").setDataSource(new kendo.data.DataSource());

        console.log("--in mapping type select");
        var dataItem = this.dataItem(e.item);
        Xrm.Page.getAttribute("ddsm_mappingtype").setValue(dataItem.Value);
        Xrm.Page.getAttribute("ddsm_jsondata").setValue("");
        Xrm.Page.getAttribute("ddsm_entities").setValue("");
        Init();
    },
    dataBound: function(e) {
        console.log("--in mapping type databound");

        var dropdownlist = $("#mappingType").data("kendoDropDownList");
        var mappingType = Xrm.Page.getAttribute("ddsm_mappingtype");
        if (mappingType) {
            var valueOfMappingType = mappingType.getValue();
            dropdownlist.value(valueOfMappingType);
        }
    }
});

function actualizeGridWrapper(entName) {
    // window.top.$selected_tab = entName;
    $("#grid").empty().kendoGrid({
        pageable: true,
        dataSource: Controller.actualizeGrid(entName),
        //rowTemplate: '<tr class="#:SourceField  !==""? \"green\" : \"customClass2\"#" data-uid="#= uid #"><td>#: SourceField  #</td><td>#:SourceFieldType  #</td><td>#:TargetField#</td></tr>',
        height: 800,
        filterable: {
            extra: true,
            operators: {
                string: {
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    isnotempty: "Is not empty",
                    isempty: "Is empty",
                    contains: "Contains"
                }
            }
        },
        sortable: {
            mode: "single",
            allowUnsort: false
        },
        columns: Controller.getGridColumns(),
        width: "200px",
        title: "Clear mapping",
        editable: true,
        edit: function(e) {
            var columnIndex = this.cellIndex(e.container);
            var fieldName = this.thead.find("th").eq(columnIndex).data("field");
            if (!Controller.isEditable(fieldName, e.model)) {
                console.log("-----grid edit row disabling");
                e.preventDefault();
                this.closeCell();
            }
        },
        dataBound: Controller.drowLines,
    });
} 

// Init controls
function initControls() {
    /*----init Kendo controls START--------------*/
    // Grid of fields
    actualizeGridWrapper("");

    // dropdown of Source entities
    $('#sourceEntity').empty().kendoDropDownList({
        filter: "contains",
        ignoreCase: true,
        optionLabel: "Select source entity...",
        filtering: function() {
            isFiltered = true;
        },
        change: function(e) {
            if (isFiltered) {
                e.preventDefault();
                return;
            }
        },
        dataSource: new kendo.data.DataSource({
            schema: KendoHelper.CRM.defEntityMetadataSchema(),
            sort: { field: "Label", dir: "asc" },
            transport: KendoHelper.CRM.transport(encodeURI(clientUrl + baseUrl + 'EntityDefinitions?$select=DisplayName,SchemaName,MetadataId')),
        }),
        dataTextField: "Label",
        dataValueField: "LogicalName",
        template:' #: Label # (#:LogicalName #)',
        select: function(e) {
            //////////debugger;
            console.log("-- Change Source Entity");
            var logicalName = getLogicalName(this, e);
            logicalName != "" ? enableTypeDropdown(false) : enableTypeDropdown(true);
            logicalName != "" ? enableTargetEntityDropdown(true) : enableTargetEntityDropdown(false);

            Xrm.Page.getAttribute("ddsm_sourceentity").setValue(logicalName);
            Controller.getSourceEntityMetadata(logicalName, "$sourceMetaData");

            function getLogicalName(s, d) {
                var dataItem = s.dataItem(d.item);
                return dataItem.LogicalName;
            }
        },
        dataBound: function(e) {
            if (isFiltered) {
                return;
            }
            //////////debugger;
            // Restore value on load
            var dropdownlist = $("#sourceEntity").data("kendoDropDownList");
            var certOrgListControl = Xrm.Page.getAttribute("ddsm_sourceentity");
            if (certOrgListControl) {
                var sourceEntity = certOrgListControl.getValue();
                if (sourceEntity && sourceEntity != "") {
                    dropdownlist.value(sourceEntity);
                    Controller.getSourceEntityMetadata(sourceEntity, "$sourceMetaData");
                    sourceEntity != "" ? enableTypeDropdown(false) : enableTypeDropdown(true);
                }
            } else {
                console.error("Сan't get ddsm_sourceentity control. Please add it to the form, and then restart the page.");
            }

        }
    });
    createTabs();
    activateFirstTab();
    Controller.prepareUI();
}



// dropdown of Cert Org
$('#certOrgList').empty().kendoDropDownList({
    filter: "contains",
    ignoreCase: true,
    optionLabel: "Select Org...",
    filtering: function() {
        isFiltered = true;
    },
    change: function(e) {
        if (isFiltered) {
            e.preventDefault();
            return;
        }
    },
    dataSource: new kendo.data.DataSource({
        schema: {
            data: "value"
        },
        sort: { field: "ddsm_name", dir: "asc" },
        transport: KendoHelper.CRM.transport(clientUrl + baseUrl + 'ddsm_certifyingorganizations?$select=ddsm_name'),
    }),
    dataTextField: "ddsm_name",
    dataValueField: "ddsm_certifyingorganizationid",
    select: function(e) {
        if (e.item) {
            var dataItem = this.dataItem(e.item);
            Controller.getTargetEntities(dataItem.ddsm_certifyingorganizationid);

            //debugger;
            console.log("-- Change Source Entity");
            // var logicalName = getLogicalName(this, e);
            dataItem.ddsm_certifyingorganizationid != "" ? enableTypeDropdown(false) : enableTypeDropdown(true);
            dataItem.ddsm_certifyingorganizationid != "" ? enableTargetEntityDropdown(true) : enableTargetEntityDropdown(false);

            Xrm.Page.getAttribute("ddsm_certifyingorganizationid").setValue([{
                id: dataItem.ddsm_certifyingorganizationid,
                name: dataItem.ddsm_name,
                entityType: "ddsm_certifyingorganization"
            }]);
            // getcertOrgMetadata(logicalName, "$sourceMetaData");

            function getLogicalName(s, d) {
                var dataItem = s.dataItem(d.item);
                return dataItem.LogicalName;
            }
        }
    },
    dataBound: function(e) {
        if (isFiltered) {
            return;
        }
        //////////debugger;

        // Restore value on load
        var dropdownlist = $("#certOrgList").data("kendoDropDownList");
        var certOrgListControl = Xrm.Page.getAttribute("ddsm_certifyingorganizationid");
        if (certOrgListControl) {
            var certOrg = certOrgListControl.getValue();
            if (certOrg && certOrg.length > 0) {
                var idOrg = certOrg[0].id.replace(/[{}]/g, "").toLowerCase();
                dropdownlist.value(idOrg);
                Controller.getTargetEntities(idOrg);

                certOrg.length > 0 ? enableTypeDropdown(false) : enableTypeDropdown(true);
                certOrg.length > 0 ? enableTargetEntityDropdown(false) : enableTargetEntityDropdown(true);
            }
        } else {
            console.error("Сan't get ddsm_sourceentity control. Please add it to the form, and then restart the page.");
        }
    }
});

// multiselect of Target entities
$("#targetEntityList").empty().kendoMultiSelect({
    filter: "contains",
    ignoreCase: true,
    filtering: function() {
        isFiltered = true;
    },
    dataSource: Controller.getTargetEntities(),    
    dataTextField: "Label",
    dataValueField: "LogicalName",
    itemTemplate:' #: Label # (#:LogicalName #)',
    footerTemplate: 'Total #: instance.dataSource.total() # items found',   
    change: function(dropDown) {
        console.log("in onChange Entities");
        kendo.ui.progress($("#grid"), true);
        ////debugger;
        var valueOfMappingType = 0;
        var mappingType = Xrm.Page.getAttribute("ddsm_mappingtype");
        if (mappingType) {
            valueOfMappingType = mappingType.getValue();
        }


        var prevTabs = window.top.JSON.parse(Xrm.Page.getAttribute("ddsm_entities").getValue());
        var currentTabs = this.dataItems();
        var diff = [];

        if (prevTabs && prevTabs.length > 0 && currentTabs.length < prevTabs.length) {
            diff = filterByDifference(prevTabs, currentTabs, ["LogicalName"]);
        }
        if (diff.length > 0) {
            console.log('deleted')
        }
        //debugger;
        var entities = this.dataItems();
        Xrm.Page.getAttribute("ddsm_entities").setValue(window.top.JSON.stringify(entities));
        createTabs();
        //is remove record
        if (diff.length > 0) {
            delete window.top.datasetDict[diff[0].LogicalName];
            //clear selected tab
            window.top.$selected_tab = "";
            console.log('deleted from top: ' + diff[0]);

            //init grid on first opened tab
            if (entities && entities.length > 0) {
                actualizeGridWrapper(entities[0].LogicalName);
                activateFirstTab();
                enableSourceEntityDropdown(false);
                enableTypeDropdown(false);

            } else {
                actualizeGridWrapper("");
                enableSourceEntityDropdown(true);
                enableCertOrgListDropdown(true);
                enableTypeDropdown(true);
            }
        } else {
            if (entities && entities.length > 0) {
                let selectedlastLogicalName = entities[entities.length - 1].LogicalName;
                window.top.$selected_tab = selectedlastLogicalName;
                /* if (valueOfMappingType == 962080000) {
                     getSourceEntityMetadata(selectedlastLogicalName, "$sourceMetaData", function () {
                         actualizeGridWrapper(selectedlastLogicalName);
                     });
                     getSourceEntityMetadata(selectedlastLogicalName, "$modelDataTarget", function () {
                         //actualizeGridWrapper(selectedlastLogicalName);
                     });
                 } else {*/
                Controller.getSourceEntityMetadata(selectedlastLogicalName, "$modelDataTarget", function() {
                    actualizeGridWrapper(selectedlastLogicalName);
                });
                //  }
                activateLastTab();
                enableSourceEntityDropdown(false);
                enableTypeDropdown(false);
                enableCertOrgListDropdown(false);
            }

        }

        if (isFiltered) {
            dropDown.preventDefault();
            return;
        }
    },
    dataBound: function(e) {
        if (isFiltered) {
            return;
        }
        var selectedEntity = window.parent.JSON.parse(Xrm.Page.getAttribute("ddsm_entities").getValue());
        if (selectedEntity && window.top.$selected_tab == "" && selectedEntity.length > 0) {
            window.top.$selected_tab = selectedEntity[0].LogicalName;
        }
        // debugger;
        var dropdownlist = $("#targetEntityList").data("kendoMultiSelect");
        dropdownlist.value(selectedEntity);
        if (selectedEntity && selectedEntity.length > 0) {
            enableCertOrgListDropdown(false, true); //only for ES controller 
        }
        if (selectedEntity && selectedEntity.length > 0) {
            enableTypeDropdown(false);
        }
        enableSourceEntityDropdown(false, true);
        enableTargetEntityDropdown(true, true);
        // enableTypeDropdown(false, true);
        // 
    },
});


function enableSourceEntityDropdown(enable, ifHasValue) {
    //disabling source entity dropdown
    var sourceEntity = $('#sourceEntity').data("kendoDropDownList");
    if (ifHasValue && sourceEntity && sourceEntity.value()) {
        sourceEntity.enable(enable);
    } else if (sourceEntity && (!ifHasValue || ifHasValue == 'undefined')) {
        sourceEntity.enable(enable);
    }
}

function enableTargetEntityDropdown(enable, ifHasValue) {
    //disabling source entity dropdown
    var sourceEntity = $('#targetEntityList').data("kendoMultiSelect");
    if (ifHasValue && sourceEntity && sourceEntity.value()) {
        sourceEntity.enable(enable);
    } else if (sourceEntity && (!ifHasValue || ifHasValue == 'undefined')) {
        sourceEntity.enable(enable);
    }
}

function enableCertOrgListDropdown(enable, ifHasValue) {
    //disabling source entity dropdown
    var sourceEntity = $('#certOrgList').data("kendoDropDownList");
    if (ifHasValue && sourceEntity && sourceEntity.value()) {
        sourceEntity.enable(enable);
    } else if (sourceEntity && (!ifHasValue || ifHasValue == 'undefined')) {
        sourceEntity.enable(enable);
    }
}

function enableTypeDropdown(enable, ifHasValue) {
    //disabling source entity dropdown
    var sourceEntity = $('#mappingType').data("kendoDropDownList");
    if (ifHasValue && sourceEntity && sourceEntity.value()) {
        sourceEntity.enable(enable);
    } else if (sourceEntity && (!ifHasValue || ifHasValue == 'undefined')) {
        sourceEntity.enable(enable);
    }
}

function getMappingTypeOptionSet() {
    var name = "ddsm_mappingtype";
    var allData = new kendo.data.DataSource({
        schema: KendoHelper.CRM.defOptionsetMetadataSchema(),
        transport: KendoHelper.CRM.transport(encodeURI(clientUrl + baseUrl + 'GlobalOptionSetDefinitions?$select=Name')),
    });
    var currentOptsId = "";
    allData.fetch(function() {
        var data = allData.data();
        for (i = 0; i < data.length; i++) {
            if (data[i].LogicalName == name) {
                console.log(data[i].Id);
                var typeDS = new kendo.data.DataSource({
                    schema: KendoHelper.CRM.optionSetMetadata(),
                    transport: KendoHelper.CRM.transport(encodeURI(clientUrl + baseUrl + 'GlobalOptionSetDefinitions(' + data[i].Id + ')')),
                });
                typeDS.fetch(function() {
                    var mappingType = $('#mappingType').data("kendoDropDownList");
                    mappingType.setDataSource(typeDS); // return typeDS;
                });
                return typeDS;
            }
        }
    });
}

function createTabs() {
    // Tab with entities
    var tabs = $("#tabstrip").empty().kendoTabStrip({
        dataSource: window.top.JSON.parse(window.parent.Xrm.Page.getAttribute("ddsm_entities").getValue()),
        dataTextField: "Label",
        dataValueField: "LogicalName",
        select: function(e) {
            console.log("----In onSelect");
            kendo.ui.progress($("#grid"), true);
            var selectedTabName = e.item.textContent;
            var selectedLogicalName = getLogicalNameFromDictionary(selectedTabName);
            window.top.$selected_tab = selectedLogicalName;
            Controller.getSourceEntityMetadata(selectedLogicalName, "$modelDataTarget",
                function() {
                    console.log("Actualize grid from on TAB change - selectedLogicalName");
                    actualizeGridWrapper(selectedLogicalName);
                });
        }
    });
}
/*start of model */
function startBind() {
    kendo.data.binders.widget.editorDisabled = kendo.data.Binder.extend({
        init: function(element, bindings, options) {
            kendo.data.Binder.fn.init.call(this, element, bindings, options);
        },
        refresh: function() {
            // var enable = !this.bindings.editorDisabled.get();
            var enable = !this.bindings.editorDisabled.get();
            //grid
            $('[data-role=grid]', this.element.toolbar.element)
                .getKendoSelectBox()
                .enable(enable);

            //comboboxes
            $('[data-role=combobox]', this.element.toolbar.element).each(function() {
                $(this).getKendoComboBox().enable(enable);
            });

            $(this.element.body).attr('contenteditable', enable);
            $(this.element.body).css('visibility', enable ? 'visible' : 'hidden');
        }
    });
}
startBind();

function activateFirstTab() {
    console.log("-- in activateFirstTab");
    tabStrip = $("#tabstrip").kendoTabStrip().data("kendoTabStrip");
    let tab = $('.k-state-default.k-first');
    if (tab.length > 0) {
        $(tab).trigger("select");
        tabStrip.activateTab(tab);
    }
}

function activateLastTab() {
    console.log("-- in activateLastTab");
    tabStrip = $("#tabstrip").kendoTabStrip().data("kendoTabStrip");
    let tab = $('.k-state-default.k-last');
    if (tab.length > 0) {
        $(tab).trigger("select");
        tabStrip.activateTab(tab);
    }
}
/*----init Kendo controls STOP --------------*/