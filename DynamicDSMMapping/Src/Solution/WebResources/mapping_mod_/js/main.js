function onSave() {
    //debugger;
    console.log("----On Save");
    if (window.top.datasetDict) {
        Xrm.Page.getAttribute("ddsm_jsondata").setValue(window.top.JSON.stringify(window.top.datasetDict));
    }
}


var ignoreFieldDisplayNamePart = ["(Base)", "Z_"];
var ignoreFieldTypeArray = ["Virtual", "Uniqueidentifier", "EntityName", "PartyList"];
var ignoreFieldNameArray = ["ddsm_applicationfieldsmappingconfigid",
    "createdby",
    "createdonbehalfby",
    "createdon",
    "transactioncurrencyid",
    "exchangerate",
    "importsequencenumber",
    "modifiedby",
    "modifiedonbehalfby",
    "modifiedon",
    "overriddencreatedon",
    "ddsm_sourceentity",
    "ddsm_status",
    "statecode",
    "statuscode",
    "timezoneruleversionnumber",
    "traversedpath",
    "utcconversiontimezonecode",
    "owningteam",
    "owninguser",
    "owningbusinessunit",
    "ownerid",
    "ddsm_measuretemplatedata"
];

function getLogicalNameFromDictionary(labelName) {
    var dictionary = window.parent.JSON.parse(Xrm.Page.getAttribute("ddsm_entities").getValue());
    var logicalName = "";
    for (var i = 0, len = dictionary.length; i < len; i++) {
        var item = dictionary[i];
        if (item.Label === labelName) {
            return item.LogicalName;
        }
    }
};

function getDisplayNameFromDictionary(logicalName) {
    var dictionary = window.parent.JSON.parse(Xrm.Page.getAttribute("ddsm_entities").getValue());
    return dictionary.filter(function(entity) {
        return entity.LogicalName === logicalName;
    })[0].Label;
};

function getUniqueValues(list) {
    return list.map(function(v) {
            return v.Entity;
        })
        .filter(function(item, i, ar) {
            return ar.indexOf(item) === i;
        });
};

function getEntityFromSourceEntity(dataItems, logicalName) {
    var sss = dataItems.filter(function(entity) {
        return entity.LogicalName === logicalName;
    });

    var result = sss.map(function(entity) {
        return {
            Id: entity.Id,
            Label: entity.Label,
            LogicalName: logicalName
        }
    });

    return result[0];
};

function filterByDifference(array1, array2, compareField) {
    var onlyInA = differenceInFirstArray(array1, array2, compareField);
    var onlyInb = differenceInFirstArray(array2, array1, compareField);
    return onlyInA.concat(onlyInb);
};

function differenceInFirstArray(array1, array2, compareField) {
    return array1.filter(function(current) {
        return array2.filter(function(current_b) {
            return current_b[compareField] === current[compareField];
        }).length == 0;
    });
};

function getSourceEntityMetadata(entityname, result, callback) {
    console.log("get source entitie Fields for: " + entityname + " to " + result);
    // debugger;
    // window.top.prevEntityNameMetadata = entityname;

    var fields = new kendo.data.DataSource({
        schema: {
            data: function(response) {
                var value = response.value[0].Attributes;
                var model = value.map(function(el) {

                    var displayName = "";
                    var id = el.MetadataId;
                    var type = el.AttributeType;
                    var logicalName = el.SchemaName; //.toLowerCase();
                    var attributeOf = el.AttributeOf;

                    if (el.DisplayName.LocalizedLabels[0] && el.DisplayName.LocalizedLabels[0].Label) {
                        displayName = el.DisplayName.LocalizedLabels[0].Label;
                    } else if (el.DisplayName.UserLocalizedLabel && el.DisplayName.UserLocalizedLabel.Label) {
                        displayName = el.DisplayName.UserLocalizedLabel.Label;
                    } else {
                        displayName = logicalName;
                    }
                    if (result === "$sourceMetaData") {
                        return {
                            SourceField: displayName,
                            SourceFieldLogicalName: logicalName,
                            SourceFieldType: type,
                            AttributeOf: attributeOf
                        };

                    } else {
                        return {
                            TargetField: displayName,
                            TargetFieldLogicalName: logicalName,
                            TargetFieldType: type,
                            AttributeOf: attributeOf
                        };
                    }

                });
                return model;
            }
        },
        sort: {
            field: "TargetField",
            dir: "asc"
        },
        pageSize: 100,
        transport: KendoHelper.CRM.transport(clientUrl + baseUrl + "EntityDefinitions?$select=LogicalName&$expand=Attributes($select=DisplayName,LogicalName,AttributeType,SchemaName,AttributeOf)&$filter=LogicalName eq '" + entityname + "'"),
    });
    fields.fetch(function() {
        /******* START Clear Virtual and Uniqueidentifier fileds ***********/
        var filter = {
            logic: "and",
            filters: []
        };

        filter.filters.push({
            field: "AttributeOf",
            operator: "isnull",
            value: null
        });

        if (result === "$sourceMetaData") {
            ignoreFieldNameArray.forEach(function(entry) {
                filter.filters.push({
                    field: "SourceFieldLogicalName",
                    operator: "neq",
                    value: entry
                });
            }, null);


            ignoreFieldTypeArray.forEach(function(entry2) {
                filter.filters.push({
                    field: "SourceFieldType",
                    operator: "neq",
                    value: entry2
                });
            }, null);


            ignoreFieldDisplayNamePart.forEach(function(entry3) {
                filter.filters.push({
                    field: "SourceField",
                    operator: "doesnotcontain",
                    value: entry3
                });
            }, null);

        } else {
            ignoreFieldNameArray.forEach(function(entry) {
                filter.filters.push({
                    field: "TargetFieldLogicalName",
                    operator: "neq",
                    value: entry
                });
            }, null);


            ignoreFieldTypeArray.forEach(function(entry2) {
                filter.filters.push({
                    field: "TargetFieldType",
                    operator: "neq",
                    value: entry2
                });
            }, null);


            ignoreFieldDisplayNamePart.forEach(function(entry3) {
                filter.filters.push({
                    field: "TargetField",
                    operator: "doesnotcontain",
                    value: entry3
                });
            }, null);
        }
        //debugger;
        fields.filter(filter);
        /******* END Clear Virtual and Uniqueidentifier fileds ***********/
        ////////debugger;
        if (!window.top[result]) {
            window.top[result] = {};
        }
        window.top[result][entityname] = fields.data(); //fields.view();
        if (callback) {
            callback(entityname);
        }
    });
};