$(function () {
    var formFieldApplicationName = 'ddsm_applicationform';
	var defaultValue = '--';
    var formFieldApplicationId = 'ddsm_applicationformid';
    $('.application-div').kendoDropDownList({
        filter: "startswith",
        dataTextField: "displayName",
        dataValueField: "applicationFormId",
        optionLabel: defaultValue,
        dataSource: {
            schema: {
                data: function (response) {
                    var value = response.value;
                    var model = {};
                    var model = value.map(function (el) {
                        var displayName = el.name;
                        var logicalName = el.formid;
                        return {
                            displayName,
                            applicationFormId: logicalName
                        };
                    });
                    return model;
                }
            },
            sort: {
                field: "displayName",
                dir: "asc"
            },
            transport: {
                read: {
                    url: parent.Xrm.Page.context.getClientUrl() + '/api/data/v8.1/systemforms' + '?$select=name&$filter=objecttypecode eq ' + "'" + 'ddsm_application' + "'",
                    dataType: "json",
                    beforeSend: function (req) {
                        req.setRequestHeader('Accept', 'application/json');
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader('OData-MaxVersion', "4.0");
                        req.setRequestHeader("OData-Version", "4.0");
                    }
                }
            },
        },
        dataBound: function () {
            var formField = parent.Xrm.Page.getAttribute(formFieldApplicationId);
            var applicationFormId = formField.getValue();
            if (applicationFormId) {
                this.select(function(item){
                    return item.applicationFormId === applicationFormId;
                });
                this.readonly(true);
            }
        },
        select: function (e) {
        	if(e.sender.dataItem(e.item)[e.sender.options.dataTextField] !== defaultValue){
        		var dataItem = this.dataItem(e.item);

                formSetValue(formFieldApplicationId, dataItem.applicationFormId );
                formSetValue(formFieldApplicationName, dataItem.displayName);
        	}            
        }
    });

    function formSetValue(fieldName, value){
        parent.Xrm.Page.getAttribute(fieldName).setValue(value);
    }

});

