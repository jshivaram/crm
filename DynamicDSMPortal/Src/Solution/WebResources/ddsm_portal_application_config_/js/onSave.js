function onSave() {
    var applicationJsonObject = Xrm.Page.getAttribute('ddsm_applicationform').getValue();
    if(applicationJsonObject){
        var control = Xrm.Page.getControl('ddsm_name');
        control.setDisabled(false);
        var convertedObject = JSON.parse(applicationJsonObject);
        Xrm.Page.getAttribute('ddsm_name').setValue(convertedObject.Label);
        control.setDisabled(true);
    }
}