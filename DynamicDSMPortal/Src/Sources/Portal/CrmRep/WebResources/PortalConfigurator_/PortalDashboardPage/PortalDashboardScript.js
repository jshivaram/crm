$(document).ready(function() {
    var baseUrl = "/api/data/v8.1/";
    var Xrm = window.parent.Xrm;
    var clientUrl = Xrm.Page.context.getClientUrl();

    var isFiltered = false;

    reateDashboardList();

    function reateDashboardList () {
        $('#portal-dashboard-list').kendoDropDownList({
            filter: "contains",
          	ignoreCase: true,
          	filtering: function () {
                isFiltered = true;
            },
            change: function (e) {
                if (isFiltered) {
                    e.preventDefault();
                    return;
                }
            },
            dataSource: {
                schema: {
                    data: "value"
                },
                transport: {
                    read: {
                        url: encodeURI(clientUrl + baseUrl + "systemforms?$filter=type eq 0 &$orderby=name asc"),
                        dataType: "json",
                        beforeSend: function (req) {
                            req.setRequestHeader('Accept', 'application/json');
                            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                            req.setRequestHeader('OData-MaxVersion', "4.0");
                            req.setRequestHeader("OData-Version", "4.0");
                        }
                    }
                }
            },
            dataTextField: "name",
            dataValueField: "formid",
            select: function (e) {
                var dataItem = this.dataItem(e.item);
                var id = dataItem.formid.toString();
                var name = dataItem.name.toString();
                
                Xrm.Page.getAttribute("ddsm_ddsm_dashboardid").setValue(id);
                Xrm.Page.getAttribute("ddsm_name").setValue(name);
            },
            dataBound: function (e) {
                if (isFiltered) {
                    return;
                }
                var dropdownlist = $("#portal-dashboard-list").data("kendoDropDownList");

                var formIdAttribute = Xrm.Page.getAttribute("ddsm_ddsm_dashboardid");
                var selectedFormId = formIdAttribute.getValue();

                dropdownlist.value(selectedFormId);
                isFiltered = false;
            },
            optionLabel: '--'
        });
    };
});