$(document).ready(function () {

    var baseUrl = "/api/data/v8.1/";
    var Xrm = window.parent.Xrm;
    var clientUrl = Xrm.Page.context.getClientUrl();

    var defaultFormIdAttribute = Xrm.Page.getAttribute("ddsm_defaultformid");
    var defaultViewIdAttribute = Xrm.Page.getAttribute("ddsm_defaultviewid");

    var entityLogicalNameAttribute = Xrm.Page.getAttribute("ddsm_entitylogicalname");
    var nameAttribute = Xrm.Page.getAttribute("ddsm_name");

    $('#portal-forms-list-block').css('display', 'none');
    $('#portal-views-list-block').css('display', 'none');
    var isFilteredPortalForms = false;

    function createPortalFormsList(entityName) {
        $('#portal-forms-list').kendoDropDownList({
            filter: "contains",
            ignoreCase: true,
            filtering: function () {
                isFilteredPortalForms = true;
            },
            change: function (e) {
                if (isFilteredPortalForms) {
                    e.preventDefault();
                    return;
                }
            },
            dataSource: {
                schema: {
                    data: "value"
                },
                transport: {
                    read: {
                        url: encodeURI(clientUrl + baseUrl + "systemforms?$filter=objecttypecode eq '" + entityName + "' and type eq 2"),
                        dataType: "json",
                        beforeSend: function (req) {
                            req.setRequestHeader('Accept', 'application/json');
                            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                            req.setRequestHeader('OData-MaxVersion', "4.0");
                            req.setRequestHeader("OData-Version", "4.0");
                        }
                    }
                }
            },
            dataTextField: "name",
            dataValueField: "formid",
            select: function (e) {
                var dataItem = this.dataItem(e.item);
                var id = dataItem.formid.toString();
                defaultFormIdAttribute.setValue(id);
            },
            dataBound: function (e) {
                if (isFilteredPortalForms) {
                    return;
                }

                var dropdownlist = $("#portal-forms-list").data("kendoDropDownList");
                dropdownlist.value(defaultFormIdAttribute.getValue());

                isFilteredPortalForms = false;
            },
            optionLabel: '--'
        });
    }

    var isFilteredPortalViews = false;

    function createPortalViewsList(entityName) {
        $('#portal-views-list').kendoDropDownList({
            filter: "contains",
            ignoreCase: true,
            filtering: function () {
                isFilteredPortalViews = true;
            },
            change: function (e) {
                if (isFilteredPortalViews) {
                    e.preventDefault();
                    return;
                }
            },
            dataSource: {
                schema: {
                    data: "value"
                },
                transport: {
                    read: {
                        url: encodeURI(clientUrl + baseUrl + "savedqueries?$select=name,savedqueryid&$filter=statecode eq 0 and statuscode eq 1and returnedtypecode eq '" + entityName + "'"),
                        dataType: "json",
                        beforeSend: function (req) {
                            req.setRequestHeader('Accept', 'application/json');
                            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                            req.setRequestHeader('OData-MaxVersion', "4.0");
                            req.setRequestHeader("OData-Version", "4.0");
                        }
                    }
                }
            },
            dataTextField: "name",
            dataValueField: "savedqueryid",
            select: function (e) {
                var dataItem = this.dataItem(e.item);
                var id = dataItem.savedqueryid.toString();
                defaultViewIdAttribute.setValue(id);
            },
            dataBound: function (e) {
                if (isFilteredPortalViews) {
                    return;
                }

                var dropdownlist = $("#portal-views-list").data("kendoDropDownList");
                dropdownlist.value(defaultViewIdAttribute.getValue());

                isFilteredPortalViews = false;
            },
            optionLabel: '--'
        });
    }

    var isFilteredEntities = false;

    function createEntitiesList(privileges) {
        hideLoader($('#loader'), $('#controls-group'));

        $('#portal-entities-list').kendoDropDownList({
            filter: "contains",
            ignoreCase: true,
            filtering: function () {
                isFilteredEntities = true;
            },
            change: function (e) {
                if (isFilteredEntities) {
                    e.preventDefault();
                    return;
                }
            },
            dataSource: {
                schema: {
                    data: function (response) {
                        var value = response.value;

                        value = value.filter(function (el) {
                            var logicalName = el.SchemaName.toLowerCase();
                            var prvName = 'prvRead' + logicalName;

                            var result = privileges.filter(pr => pr.name.toLowerCase() === prvName.toLowerCase());
                            var containsPrv = result.length > 0;

                            return containsPrv;
                        });

                        var model = value.map(function (el) {

                            var displayName = "";
                            var logicalName = el.SchemaName.toLowerCase();

                            if (el.DisplayName.LocalizedLabels[0] && el.DisplayName.LocalizedLabels[0].Label) {
                                displayName = el.DisplayName.LocalizedLabels[0].Label;
                            } else if (el.DisplayName.UserLocalizedLabel && el.DisplayName.UserLocalizedLabel.Label) {
                                displayName = el.DisplayName.UserLocalizedLabel.Label;
                            } else {
                                displayName = logicalName;
                            }

                            return {
                                label: displayName,
                                logicalName: logicalName
                            };
                        });

                        model.sort(function(a,b) {return (a.label > b.label) ? 1 : ((b.label > a.label) ? -1 : 0);} );

                        return model;
                    }
                },
                transport: {
                    read: {
                        url: encodeURI(clientUrl + baseUrl + 'EntityDefinitions?$select=DisplayName,SchemaName'),
                        dataType: "json",
                        beforeSend: function (req) {
                            req.setRequestHeader('Accept', 'application/json');
                            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                            req.setRequestHeader('OData-MaxVersion', "4.0");
                            req.setRequestHeader("OData-Version", "4.0");
                        }
                    }
                }
            },
            dataTextField: "label",
            dataValueField: "logicalName",
            select: function (e) {
                var dataItem = this.dataItem(e.item);
                var entityLogicalName = dataItem.logicalName.toString();
                var name = dataItem.label.toString();

                nameAttribute.setValue(name);
                entityLogicalNameAttribute.setValue(entityLogicalName);

                var formDropDownList = $("#portal-forms-list").data("kendoDropDownList");
                var viewDropDownList = $("#portal-views-list").data("kendoDropDownList");

                if (!entityLogicalName) {
                    defaultFormIdAttribute.setValue("");
                    defaultViewIdAttribute.setValue("");

                    $('#portal-forms-list-block').css('display', 'none');
                    $('#portal-views-list-block').css('display', 'none');

                    return;
                }

                if (!formDropDownList && !viewDropDownList) {
                    createPortalFormsList(entityLogicalName);
                    createPortalViewsList(entityLogicalName);

                    $('#portal-forms-list-block').css('display', 'block');
                    $('#portal-views-list-block').css('display', 'block');

                    return;
                }

                $('#portal-forms-list-block').css('display', 'block');
                $('#portal-views-list-block').css('display', 'block');

                var formUrl = encodeURI(clientUrl + baseUrl + "systemforms?$filter=objecttypecode eq '" + entityLogicalName + "' and type eq 2");
                var viewUrl = encodeURI(clientUrl + baseUrl + "savedqueries?$select=name,savedqueryid&$filter=statecode eq 0 and statuscode eq 1and returnedtypecode eq '" + entityLogicalName + "'");

                formDropDownList.dataSource.options.transport.read.url = formUrl;
                formDropDownList.dataSource.read();

                viewDropDownList.dataSource.options.transport.read.url = viewUrl;
                viewDropDownList.dataSource.read();
            },
            dataBound: function (e) {
                if (isFilteredEntities) {
                    return;
                }

                var dropdownlist = $("#portal-entities-list").data("kendoDropDownList");
                var entityLogicalName = entityLogicalNameAttribute.getValue();

                dropdownlist.value(entityLogicalName);

                if (entityLogicalName) {
                    createPortalFormsList(entityLogicalName);
                    createPortalViewsList(entityLogicalName);

                    $('#portal-forms-list-block').css('display', 'block');
                    $('#portal-views-list-block').css('display', 'block');
                }

                isFilteredEntities = false;
            },
            optionLabel: '--'
        });
    }

    function getPortalTeamPromise() {

        var portalTeamId = getParameterByName('_CreateFromId', parent.window.location.href);

        if (portalTeamId) {
            portalTeamId = portalTeamId.replace(/[{}]/g, '');
        } else {

            var portalEntityId = parent.Xrm.Page.data.entity.getId();
            portalEntityId = portalEntityId.replace(/[{}]/g, '');

            var fetchxml = `
                <fetch count="50" >
                  <entity name="ddsm_portalteam" >
                    <attribute name="ddsm_teamid" />
                    <link-entity name="ddsm_ddsm_portalteam_ddsm_portalentity" from="ddsm_portalteamid" to="ddsm_portalteamid" link-type="inner" intersect="true" >
                      <filter type="and" >
                        <condition attribute="ddsm_portalentityid" operator="eq" value="${portalEntityId}" />
                      </filter>
                    </link-entity>
                  </entity>
                </fetch>
            `;

            var encfetchxml = encodeURIComponent(fetchxml);

            return $.ajax({
                url: clientUrl + baseUrl + 'ddsm_portalteams?fetchXml=' + encfetchxml,
                headers: { "Accept": "application/json", "Content-Type": "application/json; charset=utf-8", "OData-MaxVersion": "4.0", "OData-Version": "4.0" }
            });

        }

        return $.ajax({
            url: clientUrl + baseUrl + 'ddsm_portalteams?$filter=ddsm_portalteamid eq ' + portalTeamId,
            headers: { "Accept": "application/json", "Content-Type": "application/json; charset=utf-8", "OData-MaxVersion": "4.0", "OData-Version": "4.0" }
        });
    }


    function getPrivilegesPromiseByTeamPromise(portalTeamPromise) {
        return portalTeamPromise.then(function (portalTeam) {
            return getPrivilegesPromiseByTeam(portalTeam);
        });
    }

    function getPrivilegesPromiseByTeam(portalTeam) {
        var teamId = portalTeam.value[0].ddsm_teamid;

        var fetchxml = `
            <fetch version="1.0" >
                <entity name="privilege" >
                    <attribute name="privilegeid" />
                    <attribute name="name" />
                    <filter>
                    <condition attribute="name" operator="begins-with" value="prvread" />
                    </filter>
                    <link-entity name="roleprivileges" from="privilegeid" to="privilegeid" link-type="inner" >
                    <link-entity name="role" from="roleid" to="roleid" link-type="inner" >
                        <link-entity name="teamroles" from="roleid" to="roleid" link-type="inner" >
                        <link-entity name="team" from="teamid" to="teamid" link-type="inner" >
                            <filter type="and" >
                            <condition attribute="teamid" operator="eq" value="${teamId}" />
                            </filter>
                        </link-entity>
                        </link-entity>
                    </link-entity>
                    </link-entity>
                </entity>
            </fetch>
        `;

        var encfetchxml = encodeURIComponent(fetchxml);

        return $.ajax({
            url: clientUrl + baseUrl + 'privileges?fetchXml=' + encfetchxml,
            headers: { "Accept": "application/json", "Content-Type": "application/json; charset=utf-8", "OData-MaxVersion": "4.0", "OData-Version": "4.0" }
        });
    }

    showLoader($('#loader'), $('#controls-group'));

    var portalTeamPromise = getPortalTeamPromise();

    getPrivilegesPromiseByTeamPromise(portalTeamPromise).then(function (privileges) {
        createEntitiesList(privileges.value);
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    function showLoader($blockLoader, $hideContent) {
        $hideContent.hide();
        kendo.ui.progress($blockLoader, true);
    }

    function hideLoader($blockLoader, $showContent) {
        $showContent.show();
        kendo.ui.progress($blockLoader, false);
    }


});