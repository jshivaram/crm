$(document).ready(function() {
    var baseUrl = "/api/data/v8.1/";
    var Xrm = window.parent.Xrm;
    var clientUrl = Xrm.Page.context.getClientUrl();

    getEntityLogicalName(function(data) {
        createFormsList(data.entityLogicalName);
    });

    function getEntityLogicalName(callBack) {
        var lookupEntityValue = Xrm.Page.getAttribute("ddsm_portalentityid").getValue();
        var selectedPortalEntityId = lookupEntityValue ?
            lookupEntityValue[0].id.replace('{', '').replace('}', '')
            : undefined;

        if (selectedPortalEntityId) {
            $.ajax({
                url: clientUrl + baseUrl + 'ddsm_portalentities?$filter=ddsm_portalentityid eq ' + selectedPortalEntityId,
                headers: { "Accept" : "application/json", "Content-Type" : "application/json; charset=utf-8", "OData-MaxVersion" : "4.0", "OData-Version" : "4.0" },
                success: function(result) {
                    var entityLogicalName = result.value[0].ddsm_entitylogicalname;

                    callBack({
                         entityLogicalName: entityLogicalName
                    });
                },
                error: function(err) { console.log(err); }
            });
        }
    }

    Xrm.Page.getAttribute("ddsm_portalentityid").addOnChange(function() {

        getEntityLogicalName(function (data) {
            Xrm.Page.getAttribute("ddsm_formid").setValue("");
            
            var formUrl = clientUrl + baseUrl + "systemforms?$filter=objecttypecode eq '" + data.entityLogicalName + "'";

            var formDropDownList = $("#portal-forms-list").data("kendoDropDownList");

            if (!formDropDownList) {
                createFormsList(data.entityLogicalName);
                return;
            }

            formDropDownList.dataSource.options.transport.read.url = formUrl;
            formDropDownList.dataSource.read();
        });
    });

    var isFiltered = false;

    var createFormsList = function (entityLogicalName) {
        $('#portal-forms-list').kendoDropDownList({
            filter: "contains",
          	ignoreCase: true,
          	filtering: function () {
                isFiltered = true;
            },
            change: function (e) {
                if (isFiltered) {
                    e.preventDefault();
                    return;
                }
            },
            dataSource: {
                schema: {
                    data: "value"
                },
                transport: {
                    read: {
                        url: encodeURI(clientUrl + baseUrl + "systemforms?$filter=objecttypecode eq '" + entityLogicalName + "' and type eq 2"),
                        dataType: "json",
                        beforeSend: function (req) {
                            req.setRequestHeader('Accept', 'application/json');
                            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                            req.setRequestHeader('OData-MaxVersion', "4.0");
                            req.setRequestHeader("OData-Version", "4.0");
                        }
                    }
                }
            },
            dataTextField: "name",
            dataValueField: "formid",
            select: function (e) {
                var dataItem = this.dataItem(e.item);
                var id = dataItem.formid.toString();
                var name = dataItem.name.toString();
                
                Xrm.Page.getAttribute("ddsm_formid").setValue(id);
                Xrm.Page.getAttribute("ddsm_name").setValue(name);
            },
            dataBound: function (e) {
                if (isFiltered) {
                    return;
                }
                var dropdownlist = $("#portal-forms-list").data("kendoDropDownList");

                var formIdAttribute = Xrm.Page.getAttribute("ddsm_formid");
                var selectedFormId = formIdAttribute.getValue();

                dropdownlist.value(selectedFormId);
                isFiltered = false;
            },
            optionLabel: '--'
        });
        //Customized width -----------------------------------
        //$('.k-widget.k-dropdown.k-header').css('width', '100%');
        //----------------------------------------------------
    };
});