$(document).ready(function() {
    var baseUrl = "/api/data/v8.1/";
    var Xrm = window.parent.Xrm;
    var clientUrl = Xrm.Page.context.getClientUrl();

    var teamIdAttribute = Xrm.Page.getAttribute("ddsm_teamid");
    var nameAttribute = Xrm.Page.getAttribute("ddsm_name");

    var selectedTeamId = teamIdAttribute.getValue();

    var isFiltered = false;
   $('#portal-teams-list').kendoDropDownList({
    
    	filter: "contains",
        ignoreCase: true,
        
        filtering: function () {
            isFiltered = true;
        },
        change: function (e) {
            if (isFiltered) {
                e.preventDefault();
                return;
            }
        },
        dataSource: {
            schema: {
                data: "value"
            },
            transport: {
                read: {
                    url: encodeURI(clientUrl + baseUrl + 'teams'),
                    dataType: "json",
                    beforeSend: function (req) {
                        req.setRequestHeader('Accept', 'application/json');
                        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                        req.setRequestHeader('OData-MaxVersion', "4.0");
                        req.setRequestHeader("OData-Version", "4.0");
                    }
                }
            }
        },
        dataTextField: "name",
        dataValueField: "teamid",
        select: function (e) {
            var dataItem = this.dataItem(e.item);
            var id = dataItem.teamid.toString();
            var name = dataItem.name.toString();

            nameAttribute.setValue(name);
            teamIdAttribute.setValue(id);
        },
        dataBound: function (e) {
            if (isFiltered) {
                return;
            }
            var dropdownlist = $("#portal-teams-list").data("kendoDropDownList");
            dropdownlist.value(selectedTeamId);
            isFiltered = false;
        },
        optionLabel: '--'
    });
    
   $('#portal-dashboard-list').kendoDropDownList({
            filter: "contains",
          	ignoreCase: true,
          	filtering: function () {
                isFiltered = true;
            },
            change: function (e) {
                if (isFiltered) {
                    e.preventDefault();
                    return;
                }
            },
            dataSource: {
                schema: {
                    data: "value"
                },
                transport: {
                    read: {
                        url: encodeURI(clientUrl + baseUrl + "systemforms?$filter=type eq 0 &$orderby=name asc"),
                        dataType: "json",
                        beforeSend: function (req) {
                            req.setRequestHeader('Accept', 'application/json');
                            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                            req.setRequestHeader('OData-MaxVersion', "4.0");
                            req.setRequestHeader("OData-Version", "4.0");
                        }
                    }
                }
            },
            dataTextField: "name",
            dataValueField: "formid",
            select: function (e) {
                var dataItem = this.dataItem(e.item);
                var id = dataItem.formid.toString();
                var name = dataItem.name.toString();
                
                Xrm.Page.getAttribute("ddsm_defaultdashboardid").setValue(id);
            },
            dataBound: function (e) {
                if (isFiltered) {
                    return;
                }
                var dropdownlist = $("#portal-dashboard-list").data("kendoDropDownList");

                var formIdAttribute = Xrm.Page.getAttribute("ddsm_defaultdashboardid");
                var selectedFormId = formIdAttribute.getValue();

                dropdownlist.value(selectedFormId);
                isFiltered = false;
            },
            optionLabel: '--'
        });
        
   //Customized width------     
   //$('label+.k-widget').css('width', '100%');
   //----------------------
});