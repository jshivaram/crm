$(document).ready(function () {
    var baseUrl = "/api/data/v8.1/";
    var Xrm = window.parent.Xrm;
    var clientUrl = Xrm.Page.context.getClientUrl();

    getEntityLogicalName(function (data) {
        createViewsList(data.entityLogicalName);
    });

    function getEntityLogicalName(callBack) {
        var lookupEntityValue = Xrm.Page.getAttribute("ddsm_portalentityid").getValue();
        var selectedPortalEntityId = lookupEntityValue ?
            lookupEntityValue[0].id.replace('{', '').replace('}', '')
            : undefined;

        if (selectedPortalEntityId) {
            $.ajax({
                url: clientUrl + baseUrl + 'ddsm_portalentities?$filter=ddsm_portalentityid eq ' + selectedPortalEntityId,
                headers: { "Accept": "application/json", "Content-Type": "application/json; charset=utf-8", "OData-MaxVersion": "4.0", "OData-Version": "4.0" },
                success: function (result) {
                    var entityLogicalName = result.value[0].ddsm_entitylogicalname;

                    callBack({
                        entityLogicalName: entityLogicalName
                    });
                },
                error: function (err) { console.log(err); }
            });
        }
    }

    Xrm.Page.getAttribute("ddsm_portalentityid").addOnChange(function () {

        getEntityLogicalName(function (data) {
            Xrm.Page.getAttribute("ddsm_viewid").setValue("");

            var viewUrl = clientUrl + baseUrl + "savedqueries?$filter=returnedtypecode eq '"
                + data.entityLogicalName + "' and statecode eq 0 and statuscode eq 1";

            var viewDropDownList = $("#portal-views-list").data("kendoDropDownList");

            if (!viewDropDownList) {
                createViewsList(data.entityLogicalName);
                return;
            }

            viewDropDownList.dataSource.options.transport.read.url = viewUrl;
            viewDropDownList.dataSource.read();
        });
    });
	
	var isFiltered = false;
	
    var createViewsList = function (entityLogicalName) {
        $('#portal-views-list').kendoDropDownList({
            filter: "contains",
          	ignoreCase: true,
          	filtering: function () {
                isFiltered = true;
            },
            change: function (e) {
                if (isFiltered) {
                    e.preventDefault();
                    return;
                }
            },
            dataSource: {
                schema: {
                    data: "value"
                },
                transport: {
                    read: {
                        url: encodeURI(clientUrl + baseUrl + "savedqueries?$filter=returnedtypecode eq '"
                            + entityLogicalName + "' and statecode eq 0 and statuscode eq 1"),
                        dataType: "json",
                        beforeSend: function (req) {
                            req.setRequestHeader('Accept', 'application/json');
                            req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                            req.setRequestHeader('OData-MaxVersion', "4.0");
                            req.setRequestHeader("OData-Version", "4.0");
                        }
                    }
                }
            },
            dataTextField: "name",
            dataValueField: "savedqueryid",
            select: function (e) {
                var dataItem = this.dataItem(e.item);
                var id = dataItem.savedqueryid.toString();
                var name = dataItem.name.toString();

                Xrm.Page.getAttribute("ddsm_viewid").setValue(id);
                Xrm.Page.getAttribute("ddsm_name").setValue(name);
            },
            dataBound: function (e) {
             	if (isFiltered) {
                    return;
                }
                var dropdownlist = $("#portal-views-list").data("kendoDropDownList");

                var viewIdAttribute = Xrm.Page.getAttribute("ddsm_viewid");
                var selectedViewId = viewIdAttribute.getValue();

                dropdownlist.value(selectedViewId);
              	isFiltered = false;
            },
            optionLabel: '--'
        });
    };
});