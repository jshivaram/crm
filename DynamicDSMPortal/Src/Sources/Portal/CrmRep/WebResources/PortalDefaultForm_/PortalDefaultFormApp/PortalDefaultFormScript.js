$(document).ready(function() {
	var Xrm = window.parent.Xrm;


	var teamIdAttribute = Xrm.Page.getAttribute("ddsm_teamid");
	var selectedEntityIdAttribute = Xrm.Page.getAttribute("ddsm_entityid");
	var selectedFormIdAttribute = Xrm.Page.getAttribute("ddsm_formid");

	var selectedTeamId = teamIdAttribute.getValue();
	var selectedEntityId = selectedEntityIdAttribute.getValue();
	var selectedFormId = selectedFormIdAttribute.getValue();	

	var datasource = new kendo.data.DataSource({
    transport: {
        read: function (options) {
            // asynchonous operation for getting data (e.g. $.ajax)
            // then pass data in success or error handler

            var baseUrl = "/api/data/v8.0/";
            var Xrm = window.parent.Xrm;
            var clientUrl = Xrm.Page.context.getClientUrl();

            $.ajax({
                url: encodeURI(clientUrl + baseUrl + 'teams'),
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json; charset=utf-8",
                    "OData-MaxVersion": "4.0",
                    "OData-Version": "4.0"
                },
                success: function (data) {
                    result = data.value;
                    console.log(result);
                    options.success(result);
                }
            });
           
        }
    }
});

$('#team').kendoDropDownList({
    dataSource: datasource,
    dataTextField: "name",
    dataValueField: "teamid",
    select: function (e) {
        var dataItem = this.dataItem(e.item);
        console.log(dataItem);
        var id = dataItem.teamid.toString();

        teamIdAttribute.setValue(id);
        $("ul#portal-default-form-list #entity-li").css("display", "inline-block");
    },
    dataBound: function (e) {
        var dropdownlist = $("#team").data("kendoDropDownList");
        dropdownlist.value(selectedTeamId);
    }
});
    
$("#form").kendoDropDownList({
    filter: "startswith",
    dataSource: new kendo.data.DataSource({
    transport: {
        read: function (options) {
            var Xrm = window.parent.Xrm;
			var organizationUrl = Xrm.Page.context.getClientUrl(); 
			var query = "ddsm_DDSMGetentitieslogicalnames"; 
			
			$.ajax({
			    url:  organizationUrl + "/api/data/v8.0/" + query,
			    type: 'post',
			    headers: {
			        "Accept": "application/json",   
			        "Content-Type": "application/json; charset=utf-8",
			        "OData-MaxVersion": "4.0",
			        "OData-Version": "4.0"
			    },
			    dataType: 'json',
			    success: function (data) {
			        options.success(window.parent.JSON.parse(data.Result));
			    }
			});           
        }
    }
})
});

function showElement() {

}
});