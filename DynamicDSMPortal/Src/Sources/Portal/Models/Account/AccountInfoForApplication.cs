﻿
using Portal.Models.Site;

namespace Portal.Models.Account
{
    public class AccountInfoForApplication
    {
        public int? AccountType { get; set; }
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MainPhone { get; set; }
        public string Email { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }

        public SiteInfoForApplication Site { get; set; }
    }
}
