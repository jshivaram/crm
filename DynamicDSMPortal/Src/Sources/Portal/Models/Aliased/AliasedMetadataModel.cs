﻿using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Aliased
{
    public class AliasedMetadataModel
    {
        public AliasedValueModel AliasedValue { get; set; }
        public IEnumerable<AttributeMetadata> AttributesMetadata { get; set; } 
    }
}
