﻿using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;

namespace Portal.Models.Aliased
{
    public class AliasedValueModel
    {
        public string EntityLogicalName { get; set; }
        public string FromAttribute { get; set; }
        public string ToAttribute { get; set; }
        public JoinOperator JoinOperator { get; set; }
        public IEnumerable<string> AttributesList { get; set; }
        public string Alias { get; set; }

        public AliasedValueModel()
        {
            AttributesList = new List<string>();
        } 
    }
}
