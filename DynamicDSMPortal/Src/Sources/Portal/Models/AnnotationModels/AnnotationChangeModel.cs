﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.AnnotationModels
{
    public class AnnotationChangeModel
    {
        public IEnumerable<AnnotationModelForChangeFileName> ChangeModels { get; set; }
    }
}
