﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.AnnotationModels
{
    public class AnnotationGetModel
    {
        public Guid RelatedEntityId { get; set; }
        public string RelatedEntityLogicalName { get; set; }
        public string BrowserIanaTimeZone { get; set; }
    }
}
