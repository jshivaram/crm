﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using Portal.Models.UserSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.AnnotationModels
{
    public class AnnotationModel
    {
        public AnnotationModel()
        {

        }

        public AnnotationModel(Entity annotationEntity)
        {
            if (annotationEntity == null || annotationEntity.LogicalName != "annotation")
            {
                return;
            }

            Id = annotationEntity.Id;
            RelatedEntity = annotationEntity.GetAttributeValue<EntityReference>("objectid");
            RelatedEntityLogicalName = annotationEntity.GetAttributeValue<string>("objecttypecode");
            int? fileSize = annotationEntity.GetAttributeValue<int?>("filesize");
            FileSize = fileSize.HasValue ? fileSize.Value : 0;
            IsDocument = annotationEntity.GetAttributeValue<bool?>("isdocument");
            Subject = annotationEntity.GetAttributeValue<string>("subject");
            DocumentBody = annotationEntity.GetAttributeValue<string>("documentbody");
            FileName = annotationEntity.GetAttributeValue<string>("filename");
        }

        public AnnotationModel(Entity annotationEntity, DateTimeFormatModel dateTimeFormat, DateTimeFormat fieldDateTimeFormat)
        {
            if (annotationEntity == null || annotationEntity.LogicalName != "annotation")
            {
                return;
            }

            string format = dateTimeFormat.DateFormat;
            if (fieldDateTimeFormat == DateTimeFormat.DateAndTime)
            {
                format = dateTimeFormat.DateFormat + " " + dateTimeFormat.TimeFormat;
            }

            Id = annotationEntity.Id;
            RelatedEntity = annotationEntity.GetAttributeValue<EntityReference>("objectid");
            RelatedEntityLogicalName = annotationEntity.GetAttributeValue<string>("objecttypecode");
            int? fileSize = annotationEntity.GetAttributeValue<int?>("filesize");
            FileSize = fileSize.HasValue ? fileSize.Value : 0;
            IsDocument = annotationEntity.GetAttributeValue<bool?>("isdocument");
            Subject = annotationEntity.GetAttributeValue<string>("subject");
            DocumentBody = annotationEntity.GetAttributeValue<string>("documentbody");
            FileName = annotationEntity.GetAttributeValue<string>("filename");
            CreatedOn = annotationEntity.GetAttributeValue<DateTime>("createdon").ToString(format);
        }

        public Guid Id { get; set; }
        public EntityReference RelatedEntity { get; set; }
        public string RelatedEntityLogicalName { get; set; }
        public bool? IsDocument { get; set; }
        public string Subject { get; set; }
        public int FileSize { get; set; }
        public string DocumentBody { get; set; }
        public string FileName { get; set; }
        public string CreatedOn { get; set; }

        public Entity ConvertToCrmEntity()
        {
            var entity = new Entity("annotation", Id);

            if(RelatedEntity != null 
                && RelatedEntity.Id != Guid.Empty 
                && !string.IsNullOrEmpty(RelatedEntity.LogicalName))
            {
                entity["objectid"] = RelatedEntity;
            }

            if(!string.IsNullOrEmpty(RelatedEntityLogicalName))
            {
                entity["objecttypecode"] = RelatedEntityLogicalName;
            }
            entity["filesize"] = FileSize;
            entity["isdocument"] = IsDocument;
            entity["subject"] = Subject;
            entity["documentbody"] = DocumentBody;
            entity["filename"] = FileName;

            return entity;
        }
    }
}
