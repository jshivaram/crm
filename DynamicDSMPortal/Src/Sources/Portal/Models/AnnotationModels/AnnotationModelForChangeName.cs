﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.AnnotationModels
{
    public class AnnotationModelForChangeFileName
    {
        public Guid AnnotationId { get; set; }
        public string FileName { get; set; }
    }
}
