﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.AnnotationModels
{
    public class AnnotationRemoveModel
    {
        public IEnumerable<Guid> IdList { get; set; }
    }
}
