﻿namespace Portal.Models.Application
{
    public class ApplicationConfigurationFieldModel
    {
        public string Label { get; set; }
        public string LogicalName { get; set; }
    }
}
