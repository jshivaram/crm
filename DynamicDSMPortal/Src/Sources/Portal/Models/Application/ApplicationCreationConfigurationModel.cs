﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Application
{
    public class ApplicationCreationConfigurationModel
    {
        public bool IsStepForm { get; set; }
        public bool FormForUnauthorized { get; set; }

        public IReadOnlyList<Entity> Buttons{ get; set; }
    }
}
