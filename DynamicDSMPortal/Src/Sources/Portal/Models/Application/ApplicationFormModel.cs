﻿namespace Portal.Models.Application
{
    public class ApplicationFormModel
    {
        public string Label { get; set; }
        public string LogicalName { get; set; }
    }
}
