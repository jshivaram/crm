﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Application
{
    public class ApplicationLookupWithConfigModel : LookupModel
    {
        public string ConfigId { get; set; }
    }
}
