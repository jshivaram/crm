﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Application
{
    public class ApplicationRequiredFieldMapModel
    {
        public string FieldName { get; set; }
        public bool IsRequired { get; set; }

        public ApplicationRequiredFieldMapModel(string fieldName)
        {
            this.FieldName = fieldName;
            this.IsRequired = true;
        }

        public ApplicationRequiredFieldMapModel(string fieldName, bool isRequried)
            : this(fieldName)
        {
            this.IsRequired = isRequried;
        }
    }
}
