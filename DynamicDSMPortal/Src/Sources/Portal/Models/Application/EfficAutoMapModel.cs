﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Application
{
    public class EfficAutoMapModel
    {
        public AttributeCollection Site{ get; set; }
        public AttributeCollection Account { get; set; }
        public AttributeCollection Contact { get; set; }
    }
}
