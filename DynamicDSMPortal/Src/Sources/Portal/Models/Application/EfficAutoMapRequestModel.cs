﻿using System.Collections.Generic;

namespace Portal.Models.Application
{
    public class EfficAutoMapRequestModel
    {
        public List<string> Account { get; set; }
        public List<string> Contact { get; set; }
        public List<string> Site { get; set; }
    }
}
