﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Application
{
    public class MeasureTemplateModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal KwhSavingUnit { get; set; }
        public decimal IncentiveUnit { get; set; }
    }
}