﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Approve
{
    public class IsApproveModel
    {
        public string EntityLogicalName { get; set; }
        public Guid EntityId { get; set; }
    }
}
