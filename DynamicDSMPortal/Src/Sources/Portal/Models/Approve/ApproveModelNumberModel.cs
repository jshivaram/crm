﻿using System;

namespace Portal.Models.Approve
{
    public class ApproveModel
    {
        public string EntityLogicalName { get; set; }
        public Guid EntityId { get; set; }
        public Guid MeasureTemplateSelectorForApproval { get; set; }
    }
}
