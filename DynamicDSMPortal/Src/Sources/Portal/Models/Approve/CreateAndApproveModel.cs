﻿using System;
using Portal.Models.EntityModels;

namespace Portal.Models.Approve
{
    public class CreateAndApproveModel : EntityModel
    {
        public Guid MeasureTemplateSelectorForApproval { get; set; }
    }
}