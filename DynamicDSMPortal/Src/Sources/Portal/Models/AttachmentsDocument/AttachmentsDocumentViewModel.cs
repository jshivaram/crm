﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.AttachmentsDocument
{
    public class AttachmentsDocumentViewModel
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public string CreatedOn { get; set; }
    }
}
