﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.AttachmentsDocument
{
    public class DocumentUploadResponseModel
    {
        public Guid EntityId { get; set; }
        public string EntityLogicalName { get; set; }
        public bool DocumentAttachedToExistedEntity { get; set; }
    }
}
