﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Auth
{
    public class AuthorizedUserDataModel
    {
        public Guid UserId { get; set; }
        public string Login { get; set; }
        public string FullName { get; set; }
    }
}
