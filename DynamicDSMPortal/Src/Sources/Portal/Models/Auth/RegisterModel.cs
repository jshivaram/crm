﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Auth
{
    public class RegisterModel
    {
        [Required]
        public string Login { get; set; }
        [Required]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Passwords are not equals.")]
        public string ConfirmPasword { get; set; }

        [Required]
        [RegularExpression(@"^[a-zA-Z0-9_]{1}[a-zA-Z0-9._]{1,50}[@]{1}[a-zA-Z0-9_.]{1,50}[.]{1}[a-zA-Z0-9]{1,20}$", ErrorMessage = "Email is not valid.")]
        public string Email { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
    }
}
