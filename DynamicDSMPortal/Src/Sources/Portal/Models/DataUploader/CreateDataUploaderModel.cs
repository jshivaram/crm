﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Portal.Models.DataUploader
{
    public class CreateDataUploaderModel
    {
        public int TypeOfUploadedData { get; set; }
        public HttpPostedFileBase File { get; set; }
    }
}
