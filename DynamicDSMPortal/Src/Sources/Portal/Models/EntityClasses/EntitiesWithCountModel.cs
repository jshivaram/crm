﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.EntityClasses
{
    public class EntitiesWithCountModel
    {
        private int _count;
        public IEnumerable<Entity> Entities { get; set; }

        public int Count
        {
            get
            {
                return _count;
            }
            set
            {
                if (value > 0)
                {
                    _count = value;
                }
                _count = Math.Abs(value);
            }
        }
    }
}
