﻿using Microsoft.Xrm.Sdk;
using Portal.Models.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.EntityModels
{
    public class ChartDataModel<T>
    {
        public IEnumerable<T> Data { get; set; }
        public IDictionary<string, string> VisualizationData { get;  set;}
        public IEnumerable<AttributeMetadataModel> AttrMetadata { get; set; }
        public string ViewXml { get; set; }
        public IDictionary<string, string> View { get; set; }
    }
}
