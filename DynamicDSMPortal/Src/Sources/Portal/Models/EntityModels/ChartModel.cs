﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.EntityModels
{
    public class ChartModel
    {
        public string EntityLogicalName { get; set; }
        public int Count { get; set; } 
        public Guid ViewId { get; set; }
        public Guid VisualizationId { get; set; }
    }
}
