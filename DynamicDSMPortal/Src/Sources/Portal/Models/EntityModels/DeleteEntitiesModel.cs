﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.EntityModels
{
    public class DeleteEntitiesModel
    {
        public IEnumerable<Guid> IdArr { get; set; }
        public string EntityLogicalName { get; set; }
    }
}
