﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.EntityModels
{
    public class DictionaryWithTotalCountModel<T>
    {
        public IEnumerable<IDictionary<string, T>> Items { get; set; }
        public int TotalCount { get; set; }
    }
}
