﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.EntityModels
{
    public class EntityListModel
    {
        public IEnumerable<string> JsonEntityDataList { get; set; }
        public string LogicalName { get; set; }
    }
}
