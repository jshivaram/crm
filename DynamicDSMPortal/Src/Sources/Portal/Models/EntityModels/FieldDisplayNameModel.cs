﻿namespace Portal.Models.EntityModels
{
    public class FieldDisplayNameModel
    {
        public string DisplayName { get; set; }
        public string Type { get; set; }
        public int Width { get; set; }
    }
}
