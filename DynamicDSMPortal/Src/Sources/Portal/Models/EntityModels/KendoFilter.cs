﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.EntityModels
{
    public class KendoFilter
    {
        public string Value { get; set; }
        public string Field { get; set; }
        public string Operator { get; set; }
        public bool IgnoreCase { get; set; }

        public string Logic { get; set; }
        public IEnumerable<KendoFilter> Filters { get; set; }
    }
}