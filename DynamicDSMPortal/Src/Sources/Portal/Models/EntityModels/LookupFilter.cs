﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.EntityModels
{
    public class LookupFilter
    {
        public IEnumerable<LookupFilters> Filters { get; set; }
        public string Logic { get; set; }
    }
}
