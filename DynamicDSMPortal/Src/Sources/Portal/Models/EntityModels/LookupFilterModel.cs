﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.EntityModels
{
    public class LookupFilterModel
    {
        public Guid ViewId { get; set; }
        public LookupFilter Filter { get; set; }
    }
}
