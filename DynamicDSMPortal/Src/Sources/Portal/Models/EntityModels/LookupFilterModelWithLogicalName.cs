﻿namespace Portal.Models.EntityModels
{
    public class LookupFilterModelWithLogicalName
    {
        public string EntityLogicalName { get; set; }
        public KendoFilters Filter { get; set; }
    }
}