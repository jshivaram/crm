﻿using System;

namespace Portal.Models.EntityModels
{
    public class LookupFilterModelWithLogicalNameAndProgramOffering : LookupFilterModelWithLogicalName
    {
        public Guid ProgramOfferingId { get; set; }
    }
}