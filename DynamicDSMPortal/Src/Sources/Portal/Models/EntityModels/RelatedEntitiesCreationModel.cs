﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.EntityModels
{
    public class RelatedEntitiesCreationModel
    {
        public IEnumerable<string> RelatedEntitiesJsonList { get; set; }
        public string RelatedEntitiesLogicalName { get; set; }
        public Guid EntityId { get; set; }
        public string EntityLogicalName { get; set; }
        public string RelationshipShcemeName { get; set; }
    }
}
