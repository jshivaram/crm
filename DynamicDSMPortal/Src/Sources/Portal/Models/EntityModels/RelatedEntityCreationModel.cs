﻿using System;

namespace Portal.Models.EntityModels
{
    public class RelatedEntityCreationModel
    {
        public string LogicalName { get; set; } //related entity logical name, entity with this logical name will created
        public string JsonEntityData { get; set; }

        public Guid EntityId { get; set; }
        public string EntityLogicalName { get; set; }
        public string RelationshipShcemeName { get; set; }
    }
}