﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.EntityModels
{
    public class RelatedEntityLookupModel
    {
        public Guid Id { get; set; }
        public string EntityLogicalName { get; set; }
        public string DataFieldName { get; set; }
    }
}
