﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.EntityModels
{
    public class RelatedEntityModelWithoutViewId
    {
        public Guid EntityId { get; set; }
        public string EntityLogicalName { get; set; }
        public string RelatedEntityName { get; set; }
        public string RelationshipSchemaName { get; set; }
    }
}
