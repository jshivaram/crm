﻿using Portal.Models.GridModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.EntityModels
{
    public class SpecialGridGetModel
    {
        public Guid ViewId { get; set; }
        public string EntityLogicalName { get; set; }
        public string CategoryDisplayName { get; set; }
        public string Category { get; set; }
        public Guid CategoryId { get; set; }
        public int Take { get; set; }
        public int Skip { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public IEnumerable<GridSortParamModel> Sort { get; set; }
        public Guid VisualizationId { get; set; }
        public string NotFormattedValue { get; set; }
        public string DateGrouping { get; set; }
        public string BrowserIanaTimeZone { get; set; }
    }
}
