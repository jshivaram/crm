﻿namespace Portal.Models.Enums
{
    public enum PortalUserRoleEnum
    {
        Customer = 0,
        Distributor = 1
    }
}