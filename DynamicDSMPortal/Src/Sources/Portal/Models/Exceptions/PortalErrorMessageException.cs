﻿using System;

namespace Portal.Models.Exceptions
{
    public class PortalErrorMessageException : Exception
    {
        public PortalErrorMessageException(string message) : base(message)
        {
            
        }
    }
}
