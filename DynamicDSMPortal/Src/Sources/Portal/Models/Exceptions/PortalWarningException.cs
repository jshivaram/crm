﻿using System;

namespace Portal.Models.Exceptions
{
    public class PortalWarningException : Exception
    {
        public PortalWarningException(string message) 
            : base(message)
        {

        }
    }
}
