﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Form
{
    public class FormGetByIdModel
    {
        public string EntityLogicalName { get; set; }
        public Guid FormId { get; set; }
    }
}
