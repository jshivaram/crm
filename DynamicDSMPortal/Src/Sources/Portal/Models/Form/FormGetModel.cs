﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Form
{
    public class FormGetModel
    {
        public string EntityLogicalName { get; set; }
        public string FormName { get; set; }
    }
}
