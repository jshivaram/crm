﻿using Microsoft.Xrm.Sdk;
using System.Collections.Generic;

namespace Portal.Models.Form
{
    public class PortalFormConfigModel
    {
        private bool _showApproveButton = true;
        private bool _showSaveAndAppoveButton = true;

        public bool? ShowApproveButton
        {
            get => _showApproveButton;
            set => _showApproveButton = value ?? true;
        }

        public bool? ShowSaveAndAppoveButton
        {
            get => _showSaveAndAppoveButton;
            set => _showSaveAndAppoveButton = value ?? true;
        }

        public string ApproveButtonName { get; set; } = "Approve";
        public string SaveAndAppoveButtonName { get; set; } = "Save and Appove";

        public IReadOnlyList<Entity> Buttons;
    }
}