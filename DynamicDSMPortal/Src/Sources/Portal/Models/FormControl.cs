﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models
{
    public class FormControl
    {
        public string ControlId { get; set; }
        public string Description { get; set; }
        public string LanguageCode { get; set; }
        public Guid ClassId { get; set; }
        public string DataFieldName { get; set; }
        public bool ShowLabel { get; set; }
        public bool Disabled { get; set; }

        public Guid CellId { get; set; }
    }
}
