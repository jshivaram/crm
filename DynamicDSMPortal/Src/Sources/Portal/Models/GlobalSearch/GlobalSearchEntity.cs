﻿using System;

namespace Portal.Models.GlobalSearch
{
    public class GlobalSearchEntity
    {
        public string LogicalName { get; set; }
        public string DisplayName { get; set; }
        public int? SearchOrder { get; set; }
        public bool? InSearch { get; set; }
        public Guid? PortalViewId { get; set; }
        public Guid? ViewId { get; set; }
    }
}