﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.GridModels
{
    public class GridPagingModel
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public IEnumerable<GridSortParamModel> Sort { get; set; }
    }
}