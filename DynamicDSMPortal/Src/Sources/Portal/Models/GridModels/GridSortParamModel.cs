﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.GridModels
{
    public class GridSortParamModel
    {
        public string Field { get; set; }
        public string Dir { get; set; }
    }
}