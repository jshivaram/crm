﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.GridModels
{
    public class GridViewModel<T>
    {
        public int Total { get; set; }
        public IEnumerable<T> Data { get; set; }
    }
}