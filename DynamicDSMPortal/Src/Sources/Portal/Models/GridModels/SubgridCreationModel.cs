﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.GridModels
{
    public class SubgridCreationModel
    {
        public string EntityLogicalName { get; set; }
        public string RelationshipName { get; set; }
        public IEnumerable<string> GridDataJsonList { get; set; }
    }
}
