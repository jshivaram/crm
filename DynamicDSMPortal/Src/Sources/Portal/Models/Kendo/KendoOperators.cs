﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Kendo
{
    public enum KendoOperators
    {
        Contains,
        DoesNotContain,
        Eq,
        Neq,
        IsNull,
        IsNotNull,
        Lt,
        Lte,
        Gt,
        Gte,
        StartsWith,
        EndsWith,
        IsEmpty,
        IsNotEmpty
    }
}
