﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Lookup
{
    public class LookupGetModel
    {
        public string EntityLogicalName { get; set; }
        public string FieldName { get; set; }
    }
}
