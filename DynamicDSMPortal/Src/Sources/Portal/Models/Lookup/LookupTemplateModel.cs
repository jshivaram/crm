﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Lookup
{
    public class LookupTemplateModel
    {
        public string Id { get; set; }
        public string Logicalname { get; set; }
        public string Name { get; set; }
    }
}
