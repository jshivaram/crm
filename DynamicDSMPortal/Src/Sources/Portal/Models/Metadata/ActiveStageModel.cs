﻿namespace Portal.Models.Metadata
{
    public class ActiveStageModel
    {
        public int Order { get; set; }
        public string StageName { get; set; }
        public string StageId { get; set; }
    }
}