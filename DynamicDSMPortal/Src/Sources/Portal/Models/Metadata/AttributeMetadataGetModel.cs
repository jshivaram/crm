﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Metadata
{
    public class AttributeMetadataGetModel
    {
        public string EntityLogicalName { get; set; }
        public string FieldName { get; set; }
    }
}
