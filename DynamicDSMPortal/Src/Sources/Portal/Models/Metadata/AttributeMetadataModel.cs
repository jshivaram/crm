﻿using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Metadata
{
    public class AttributeMetadataModel
    {
        public string LogicalName { get; set; }
        public string DisplayName { get; set; }
        public bool Editable { get; set; }
        public AttributeRequiredLevel? Required { get; set; }
        public string Type { get; set; }
    }
}
