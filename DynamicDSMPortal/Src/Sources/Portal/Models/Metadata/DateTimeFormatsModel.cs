﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Metadata
{
    public class DateTimeFormatsModel
    {
        public string EntityLogicalName { get; set; }
        public IEnumerable<string> AttributesLogicalNames{ get; set; }
    }
}
