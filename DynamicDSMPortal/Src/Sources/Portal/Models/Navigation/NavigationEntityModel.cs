﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.Navigation
{
    public class NavigationEntityModel
    {
        public string LogicalName { get; set; }
        public string DisplayName { get; set; }
        public string ClassName { get; set; }
    }
}
