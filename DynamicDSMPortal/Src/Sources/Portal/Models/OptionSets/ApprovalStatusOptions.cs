﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.OptionSets
{
    public enum ApprovalStatusOptions
    {
        PendingInternalApproval = 962080000,
        PendingDAApproval = 962080001,
        Approved = 962080002,
        Declined = 962080003,
        Discontinued = 962080004
    }
}
