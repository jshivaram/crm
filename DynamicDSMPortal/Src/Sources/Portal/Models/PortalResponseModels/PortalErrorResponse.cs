﻿namespace Portal.Models.PortalResponseModels
{
    public class PortalErrorResponse
    {
        public bool ShowOnPortalUi { get; set; }
        public string ErrorMessage { get; set; }
    }
}
