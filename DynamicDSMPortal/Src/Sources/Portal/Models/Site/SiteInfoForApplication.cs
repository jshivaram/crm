﻿namespace Portal.Models.Site
{
    public class SiteInfoForApplication
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
        public string AccountNumber { get; set; }
        public int? SqFt { get; set; }
    }
}