﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.UserSettings
{
    public class DateTimeFormatModel
    {
        public string DateFormat { get; set; }
        public string TimeFormat { get; set; }
    }
}
