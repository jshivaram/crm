﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models.View
{
    public class FieldsWithDisplayNameModel
    {
        public string EntityLogicalName { get; set; }
        public Guid ViewId { get; set; }
    }
}
