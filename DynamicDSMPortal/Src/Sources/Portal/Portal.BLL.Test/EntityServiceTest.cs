﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Moq;
using Portal.BLL.Interfaces;
using Portal.BLL.Services;
using Portal.DAL.Interfaces;
using Portal.DAL.Repositories;
using Portal.Models.GridModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Portal.BLL.Test
{
    [TestClass]
    public class EntityServiceTest
    {
        private IEntityService _entityService;

        private List<Entity> _entities = new List<Entity>
        {
            new Entity("account", new Guid("3c0c2e25-8749-4460-bf03-dce2c4001c99")),
            new Entity("account", new Guid("8e9300aa-8203-417e-8f3a-0808d6eec1f2")),
            new Entity("account", new Guid("1494ed2e-8055-4965-ac36-24c2a67b0c14")),
            new Entity("account", new Guid("dcdc5aa8-9fd9-462b-be30-a17ee6735b68")),
            new Entity("account", new Guid("f7bface7-f6d0-4b73-a34e-8bb8cc37f770"))
        };

        [TestInitialize]
        public void Init()
        {
            var mockEntityRepository = new Mock<IEntityRepository>();

            mockEntityRepository.Setup(
                e => e.Get(
                    It.IsAny<Guid>(),
                    It.IsAny<string>(),
                    It.IsAny<ColumnSet>()
                ))
                .Returns((Guid id, string logicalName, ColumnSet columnSet) =>
                {
                    return _entities.SingleOrDefault(e => e.Id == id && e.LogicalName == logicalName);
                });

            var mockUnitOfWork = new Mock<IUnitOfWork>();

            mockUnitOfWork.SetupGet<IEntityRepository>(e => e.EntityRepository)
                .Returns(mockEntityRepository.Object);

            _entityService = new EntityService(mockUnitOfWork.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid Entity id")]
        public void Get_Entity_WithEmpty_Id()
        {
            Entity entity = _entityService.Get(new Guid(), "account");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Empty entity logical name")]
        public void Get_Entity_WithEmpty_LogicalName()
        {
            Entity entity = _entityService.Get(_entities.First().Id, "");
        }

        [TestMethod]
        public void Get_Entity_Existing()
        {
            Entity entity = _entityService.Get(_entities.First().Id, _entities.First().LogicalName);
            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void Get_Entity_NotExisting()
        {
            Entity entity = _entityService.Get(new Guid("3b19eaee-be73-4110-8b79-992a74a1af62"), "account");
            Assert.IsNull(entity);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Entity LogicalName is empty")]
        public void GetEntitiesDictionaryListWithCount_WithEmpty_EntityLogicalName()
        {
            var entities = _entityService.GetEntitiesDictionaryListWithCount(
                "",
                new GridPagingModel(),
                _entities.First().Id);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "viewId is empty")]
        public void GetEntitiesDictionaryListWithCount_WithEmpty_ViewId()
        {
            var entities = _entityService.GetEntitiesDictionaryListWithCount(
                "account",
                new GridPagingModel(),
                new Guid());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Pagination model is empty")]
        public void GetEntitiesDictionaryListWithCount_WithEmpty_GridPaginationModel()
        {
            var entities = _entityService.GetEntitiesDictionaryListWithCount(
                "account",
                null,
                _entities.First().Id);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "jsonEntityData")]
        public void Create_Entity_WithEmpty_JsonEntityData()
        {
            _entityService.Create("", "account");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "logicalName")]
        public void Create_Entity_WithEmpty_LogicalName()
        {
            _entityService.Create("{ \"ddsm_name\": \"asd\" }", "");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Cannot find entity with this logical name")]
        public void Create_Entity_WithInvalid_LogicalName()
        {
            _entityService.Create("{ \"ddsm_name\": \"asd\" }", "account2");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_Entity_WithInvalid_JsonEntityData()
        {
            _entityService.Create("{ ddsm_name: 123\" }", "account");
        }

        [TestMethod]
        public void Create_Entity_WithValid_Arguments()
        {
            try
            {
                _entityService.Create("{\"ddsm_name\": \"qwe\"}", "account");
                Assert.IsTrue(true);
            }
            catch(Exception)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "jsonEntityData")]
        public void Update_Entity_WithEmpty_JsonEntityData()
        {
            _entityService.Update("", "account");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "logicalName")]
        public void Update_Entity_WithEmpty_LogicalName()
        {
            _entityService.Update("{ \"ddsm_name\": \"asd\" }", "");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Cannot find entity with this logical name")]
        public void Update_Entity_WithInvalid_LogicalName()
        {
            _entityService.Update("{ \"ddsm_name\": \"asd\" }", "account2");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Update_Entity_WithInvalid_JsonEntityData()
        {
            _entityService.Update("{ ddsm_name: 123\" }", "account");
        }

        [TestMethod]
        public void Update_Entity_WithValid_Arguments()
        {
            try
            {
                _entityService.Update("{\"ddsm_name\": \"qwe\"}", "account");
                Assert.IsTrue(true);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

    }
}
