﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using Moq;
using Portal.BLL.Interfaces;
using Portal.BLL.Services;
using Portal.DAL.Interfaces;
using Portal.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Test
{
    [TestClass]
    public class FormServiceTest
    {
        private IUnitOfWork _uow;
        private IFormService _formService;

        private List<Entity> _contacts = new List<Entity>
        {
            new Entity("contact", new Guid("3c0c2e25-8749-4460-bf03-dce2c4001c99")),
            new Entity("contact", new Guid("8e9300aa-8203-417e-8f3a-0808d6eec1f2")),
            new Entity("contact", new Guid("1494ed2e-8055-4965-ac36-24c2a67b0c14")),
            new Entity("contact", new Guid("dcdc5aa8-9fd9-462b-be30-a17ee6735b68")),
            new Entity("contact", new Guid("f7bface7-f6d0-4b73-a34e-8bb8cc37f770"))
        };

        private List<Entity> _entities = new List<Entity>
        {
            new Entity("systemform", new Guid("3c0c2e25-8749-4460-bf03-dce2c4001c99")),
            new Entity("systemform", new Guid("8e9300aa-8203-417e-8f3a-0808d6eec1f2")),
            new Entity("systemform", new Guid("1494ed2e-8055-4965-ac36-24c2a67b0c14")),
            new Entity("systemform", new Guid("dcdc5aa8-9fd9-462b-be30-a17ee6735b68")),
            new Entity("systemform", new Guid("f7bface7-f6d0-4b73-a34e-8bb8cc37f770"))
        };


        [TestInitialize]
        public void Init()
        {
            string conectionString = ConfigurationManager.ConnectionStrings["CrmConnection"].ConnectionString;
            _formService = new FormService(_uow);
        }

        [TestMethod]
        public void FormServiceTest_GetAccountForms()
        {
            try
            {
                string entityLogicalName = "account";
                var result = _formService.GetForms(entityLogicalName);
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Empty entity logical name")]
        public void FormServiceTest_GetAccountForms_WithEmpty_LogicalName()
        {
            string entityLogicalName = "";
            var result = _formService.GetForms(entityLogicalName);
        }

        [TestMethod]
        public void FormServiceTest_GetAccountFormsByContact()
        {
            try
            {
                string entityLogicalName = "account";
                Guid contactId = new Guid("3c0c2e25-8749-4460-bf03-dce2c4001c99");
                if (_contacts.Contains(new Entity("contact", contactId)))
                {
                    var result = _formService.GetForms(entityLogicalName, contactId);
                    Assert.IsTrue(true);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Empty entity logical name")]
        public void FormServiceTest_GetAccountFormsByContact_WithEmpty_LogicalName()
        {
            string entityLogicalName = "";
            var result = _formService.GetForms(entityLogicalName, new Guid("3c0c2e25-8749-4460-bf03-dce2c4001c99"));
        }

        [TestMethod]
        public void FormServiceTest_GetAccountFormsByContact_WithInvalid_ContactGuid()
        {
            try
            {
                string entityLogicalName = "account";
                var result = _formService.GetForms(entityLogicalName, new Guid("3c0c2e25-8749-4460-bf03-dce2c4001c99"));
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void FormServiceTest_GetDefaultFormId()
        {
            try
            {
                string entityLogicalName = "account";
                Guid contactId = new Guid("3c0c2e25-8749-4460-bf03-dce2c4001c99");
                if (_contacts.Contains(new Entity("contact", contactId)))
                {
                    var result = _formService.GetDefaultFormId(entityLogicalName, contactId);
                    Assert.IsTrue(true);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Empty entity logical name")]
        public void FormServiceTest_GetDefaultFormId_WithEmpty_LogicalName()
        {
            string entityLogicalName = "";
            var result = _formService.GetDefaultFormId(entityLogicalName, new Guid("3c0c2e25-8749-4460-bf03-dce2c4001c99"));
        }

        [TestMethod]
        public void FormServiceTest_GetDefaultFormId_WithInvalid_ContactGuid()
        {
            try
            {
                string entityLogicalName = "account";
                var result = _formService.GetDefaultFormId(entityLogicalName, new Guid("3c0c2e25-8749-4460-bf03-dce2c4001c99"));
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsTrue(true);
            }
        }


        [TestMethod]
        public async Task FormServiceTest_GetFormControls()
        {
            try
            {
                string entityLogicalName = "account";
                var result = await _formService.GetFormControls(entityLogicalName);
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Empty entity logical name")]
        public async Task FormServiceTest_GetFormControls_WithEmpty_LogicalName()
        {
            string entityLogicalName = "";
            var result = await _formService.GetFormControls(entityLogicalName);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Empty entity logical name")]
        public async Task FormServiceTest_GetJsonFormStructureAsync_WithEmpty_LogicalName()
        {
            string entityLogicalName = "";
            var result = await _formService.GetJsonFormStructureAsync(entityLogicalName);
        }
    }
}
