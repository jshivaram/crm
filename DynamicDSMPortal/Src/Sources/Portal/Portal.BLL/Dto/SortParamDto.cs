﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Dto
{
    public class SortParamDto
    {
        public string Field { get; set; }
        public string Dir { get; set; }
    }
}
