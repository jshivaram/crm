﻿using Portal.Models.Aliased;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Extensions
{
    public static class AliasedValuesExtensions
    {
        public static bool IsAttributeInAliasedValues(this IEnumerable<AliasedValueModel> alisedValues, string attribute)
        {
            if(string.IsNullOrEmpty(attribute) || alisedValues == null || !attribute.Contains("."))
            {
                return false;
            }

            string[] attributeArr = attribute.Split('.');

            string alias = attributeArr[0];
            string propName = attributeArr[1];

            bool result = alisedValues
                    .Where(av => av.Alias == alias)
                    .Where(av => av.AttributesList.Contains(propName))
                    .Count() > 0;

            return result;
        }

        public static string GetAliasedEntityLogicalName(this IEnumerable<AliasedValueModel> alisedValues, string attribute)
        {
            if (string.IsNullOrEmpty(attribute) || alisedValues == null || !attribute.Contains("."))
            {
                return "";
            }

            string[] attributeArr = attribute.Split('.');

            string alias = attributeArr[0];
            string propName = attributeArr[1];

            string entityLogicalName = alisedValues
                    .Where(av => av.Alias == alias)
                    .Where(av => av.AttributesList.Contains(propName))
                    .FirstOrDefault()?.EntityLogicalName;

            return entityLogicalName;
        }
    }
}
