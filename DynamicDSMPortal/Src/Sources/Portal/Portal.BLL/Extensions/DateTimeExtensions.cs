﻿using System;
using Portal.DAL.Interfaces;

namespace Portal.BLL.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime ConvertToCrmDate(this DateTime date, IUserSettingsRepository userSettingsRepository)
        {
            //TimeZoneInfo timeZoneInfo = userSettingsRepository.GetTimeZoneByCRMConnectionWithUtc();
            //DateTime convertedDateTime = TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(date, DateTimeKind.Utc), timeZoneInfo);
            var utcDateTime = DateTime.SpecifyKind(date, DateTimeKind.Utc);
            return utcDateTime;
        }
    }
}