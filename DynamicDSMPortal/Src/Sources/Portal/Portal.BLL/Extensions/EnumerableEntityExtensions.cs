﻿using Microsoft.Xrm.Sdk;
using Portal.Models.Navigation;
using Portal.Models.OptionSets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Extensions
{
    public static class EnumerableEntityExtensions
    {
        public static IEnumerable<Entity> GetOnlyApprovedModelNumberApprovals(this IEnumerable<Entity> modelNumberApprovalList)
        {
            modelNumberApprovalList = modelNumberApprovalList.Where(e => e.LogicalName == "ddsm_modelnumberapproval").ToList();
            modelNumberApprovalList = modelNumberApprovalList
                    .Where(mn => mn["ddsm_approvalstatus"] != null)
                    .Where(mn => mn.GetAttributeValue<OptionSetValue>("ddsm_approvalstatus").Value == (int)ApprovalStatusOptions.Approved
                            || mn.GetAttributeValue<OptionSetValue>("ddsm_approvalstatus").Value == (int)ApprovalStatusOptions.PendingDAApproval
                            || mn.GetAttributeValue<OptionSetValue>("ddsm_approvalstatus").Value == (int)ApprovalStatusOptions.PendingInternalApproval)
                            .ToList();

            return modelNumberApprovalList;
        }

        public static IEnumerable<NavigationEntityModel> MapPortalEntitiesToMenuItems(this IEnumerable<Entity> portalEntities)
        {
            portalEntities = portalEntities.Where(pe => pe.LogicalName == "ddsm_portalentity").ToList();


            portalEntities = portalEntities.GroupBy(e => e.GetAttributeValue<string>("ddsm_entitylogicalname"))
                              .Select(group => group.First())
                              .ToList();

            var entitiesForNav = portalEntities.Select(e =>
            {

                string displayName = e.GetAttributeValue<string>("ddsm_name");
                string navigationName = e.GetAttributeValue<string>("ddsm_navigation_name");
                string displayNavigationName = string.IsNullOrEmpty(navigationName)
                                                        ? displayName : navigationName;

                return new NavigationEntityModel
                {
                    LogicalName = e.GetAttributeValue<string>("ddsm_entitylogicalname"),
                    DisplayName = displayNavigationName,
                    ClassName = e.GetAttributeValue<string>("ddsm_class_name")
                };
            }).ToList();

            return entitiesForNav;
        }
    }
}
