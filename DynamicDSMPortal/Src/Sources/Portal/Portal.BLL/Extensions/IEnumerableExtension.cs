﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Extensions
{
    public static class IEnumerableExtension
    {
        public static IEnumerable<Entity> GetEntitiesByPropNames(
            this IEnumerable<Entity> entities, IEnumerable<string> propsNames)
        {
            List<Entity> result = new List<Entity>();

            foreach (Entity entity in entities)
            {
                Entity bufEntity = new Entity();

                foreach(string propName in propsNames)
                {
                    string propNameBuf = propName.ToLower();

                    if(!entity.Contains(propNameBuf))
                    {
                        bufEntity.Attributes.Add(propNameBuf, "");
                        continue;
                    }

                    var propValue = entity[propNameBuf];
                    Type propValueType = propValue.GetType();

                    if(propValueType == typeof(string)
                        || propValueType.IsPrimitive 
                        || propValueType.IsValueType)
                    {
                        bufEntity.Attributes.Add(propName, propValue);
                    }
                }

                result.Add(bufEntity);
            }           

            return result;
        }

        public static IEnumerable<IDictionary<string, string>> GetEntitiesDictionariesByPropNames(
           this IEnumerable<Entity> entities, IEnumerable<string> propsNames)
        {
            List<IDictionary<string, string>> result = new List<IDictionary<string, string>>();

            foreach (Entity entity in entities)
            {
                IDictionary<string, string> bufDictionary = new Dictionary<string, string>();

                foreach (string propName in propsNames)
                {
                    string propNameBuf = propName.ToLower();

                    if (!entity.Contains(propNameBuf))
                    {
                        bufDictionary.Add(propNameBuf, "");
                        continue;
                    }

                    var propValue = entity[propNameBuf];
                    Type propValueType = propValue.GetType();

                    if (propValueType == typeof(string)
                        || propValueType.IsPrimitive
                        || propValueType.IsValueType)
                    {
                        bufDictionary.Add(propName, propValue.ToString());
                    }
                }

                result.Add(bufDictionary);
            }

            return result;
        }

        public static IEnumerable<IDictionary<string, Object>> GetObjectsByPropNames(this IEnumerable<object> objs, IEnumerable<string> propsName)
        {
            ICollection<IDictionary<string, Object>> result = new List<IDictionary<string, Object>>();

            IDictionary<string, Object> expandoObject = null;
            foreach (object obj in objs)
            {
                expandoObject = GetObjectByPropNames(obj, propsName);
                result.Add(expandoObject);
            }

            return result;
        }

        private static IDictionary<string, Object> GetObjectByPropNames(object obj, IEnumerable<string> propsName)
        {
            IDictionary<string, Object> result = new ExpandoObject() as IDictionary<string, Object>;

            foreach (string propName in propsName)
            {
                object propValue = GetPropValue(obj, propName);
                if(propValue == null)
                {
                    result.Add(propName, "");
                    continue;
                }
                Type propType = propValue.GetType();

                if (propValue.GetType() == typeof(string) || propType.IsValueType || propType.IsPrimitive)
                {
                    if (result.ContainsKey(propName))
                    {
                        continue;
                    }

                    result.Add(propName, propValue);
                }
            }

            string idName = GetPropValue(obj, "LogicalName") + "Id";            

            if (!result.ContainsKey(idName.ToLower()))
            {
                object id = GetPropValue(obj, "Id");
                result.Add(idName, id);
            }            

            return result;
        }

        private static object GetPropValue(object src, string propName)
        {
            if(src == null)
            {
                return null;
            }

            return src.GetType().GetProperty(propName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance).GetValue(src, null);
        }
    }
}
