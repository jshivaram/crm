﻿using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using Portal.BLL.Services;
using Portal.Models.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Extensions
{
    public static class KeyValuePairExtensions
    {
        public static KeyValuePair<string, string> ConvertToCrmType(
            this KeyValuePair<string, object> attribute, Entity entity,
            SavedQueryService savedQueryService)
        {
            string value = GetCrmValueFromObject(attribute.Key, attribute.Value, entity, savedQueryService);

            string key = attribute.Key.ToLower();

            if(key.Contains("."))
            {
                key = key.Split('.')[1];
            }

            return new KeyValuePair<string, string>(key.ToLower(), value);
        }

        private static string GetCrmValueFromObject(string key, object value, Entity entity, SavedQueryService savedQueryService)
        {
            if (value is AliasedValue)
            {
                object aliasedValueValue = ((AliasedValue)value)?.Value;
                string result = GetCrmValueFromObject(null, aliasedValueValue, null, savedQueryService);
                return result;
            }

            if (value is EntityReference)
            {
                EntityReference entityReferenceBuf = value as EntityReference;

                try
                {
                    Guid dedaultViewIdForEntityRefence = savedQueryService.GetLookupViewId(entityReferenceBuf.LogicalName);

                    var lookupTemplateModel = new LookupTemplateModel
                    {
                        Id = entityReferenceBuf.Id.ToString(),
                        Logicalname = entityReferenceBuf.LogicalName,
                        Name = entityReferenceBuf.Name
                    };

                    string bufValue = JsonConvert.SerializeObject(lookupTemplateModel);

                    return bufValue;
                }
                catch (Exception)
                {
                    return entityReferenceBuf.Name;
                }
            }
            else if (value is OptionSetValue)
            {
                var optionSetValue = new
                {
                    Value = ((OptionSetValue)value).Value,
                    FormattedValue = entity == null || key == null ? "" : entity.FormattedValues[key]
                };

                string jsonOptionSetValue = JsonConvert.SerializeObject(optionSetValue);

                return jsonOptionSetValue;
            }
            else if (value is Money)
            {
                return ((Money)value).Value.ToString();
            }
            else if (value is DateTime?)
            {
                return ((DateTime)value).ToString("MM'/'dd'/'yyyy HH:mm:ss");
            }
            else
            {
                return value.ToString();
            }
        }
    }
}
