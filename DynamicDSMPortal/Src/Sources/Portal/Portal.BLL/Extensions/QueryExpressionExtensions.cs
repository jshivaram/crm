﻿using Microsoft.Xrm.Sdk.Query;
using Portal.Models.Aliased;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Extensions
{
    public static class QueryExpressionExtensions
    {
        public static void AddAliasedValues(this QueryExpression query, IEnumerable<AliasedValueModel> aliasedValues)
        {
            if(query == null || aliasedValues == null || aliasedValues.Count() == 0)
            {
                return;
            }

            foreach(AliasedValueModel aliasedValue in aliasedValues)
            {
                if(aliasedValue.AttributesList == null || aliasedValue.AttributesList.Count() == 0)
                {
                    continue;
                }

                var linkEntity = new LinkEntity();

                linkEntity.LinkFromEntityName = query.EntityName;
                linkEntity.LinkToEntityName = aliasedValue.EntityLogicalName;
                linkEntity.LinkFromAttributeName = aliasedValue.FromAttribute;
                linkEntity.LinkToAttributeName = aliasedValue.ToAttribute;
                linkEntity.JoinOperator = JoinOperator.Inner;

                linkEntity.Columns.AddColumns(aliasedValue.AttributesList.ToArray());

                query.LinkEntities.Add(linkEntity);
            }
        }
    }
}
