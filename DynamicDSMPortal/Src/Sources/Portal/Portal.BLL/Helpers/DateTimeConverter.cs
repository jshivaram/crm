﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Portal.BLL.Helpers
{
    public class DateTimeConverter
    {
        public static IEnumerable<Entity> ConvertUtcByTimezone(IEnumerable<Entity> entities, string windowsTimeZoneName)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZoneName);

            IEnumerable<Entity> result = entities.Select(entity => ModifiedEntityDateProps(entity, timeZoneInfo))
                                                 .ToList();

            return result;
        }

        public static Entity ConvertUtcByTimezone(Entity entity, string windowsTimeZoneName)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZoneName);

            Entity result = ModifiedEntityDateProps(entity, timeZoneInfo);

            return result;
        }

        private static Entity ModifiedEntityDateProps(Entity entity, TimeZoneInfo timeZoneInfo)
        {
            var attributeCollection = new AttributeCollection();

            IEnumerable<KeyValuePair<string, object>> timeZoneAttrs = 
                entity.Attributes.Select(attr => ModifiedAttribute(attr, timeZoneInfo)).ToList();
            attributeCollection.AddRange(timeZoneAttrs);

            entity.Attributes = attributeCollection;

            return entity;
        }

        private static KeyValuePair<string, object> ModifiedAttribute(KeyValuePair<string, object> attribute,
            TimeZoneInfo timeZoneInfo)
        { 
            if(!(attribute.Value is DateTime))
            {
                return attribute;
            }

            var date = (DateTime)attribute.Value;
            DateTime localDate = TimeZoneInfo.ConvertTimeFromUtc(date, timeZoneInfo);

            return new KeyValuePair<string, object>(attribute.Key, localDate);
        }
    }
}
