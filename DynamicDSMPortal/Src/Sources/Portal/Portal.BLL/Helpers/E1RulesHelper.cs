﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Portal.BLL.Helpers
{
    public static class E1RulesHelper
    {
        private static IUnitOfWork _uow;
        public static Guid UserId { get; set; }

        public static void AddE1RulesToQuery(QueryExpression query, IUnitOfWork uow, Guid userId)
        {
            _uow = uow;
            UserId = userId;

            IEnumerable<Guid> teamIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);
            IEnumerable<Entity> portalTeamList = _uow.PortalConfigurationRepository.GetPortalTeams(teamIdList);
            List<Guid> portalTeamIdList = portalTeamList.Select(pt => pt.Id).ToList();

            IEnumerable<Entity> rules = uow.PortalQueryExpressionRepository.GetPortalQueryExpressions(query.EntityName, portalTeamIdList);

            if (!rules.Any())
            {
                return;
            }

            //values that we get at runtime
            IEnumerable<Entity> portalTeamsList = GetPortalTeamsList();
            IEnumerable<Guid> portalTeamsIdList = portalTeamsList.Select(pt => pt.Id).ToList();
            IEnumerable<Guid> accountIdList = GetAccountsIdListForE1(portalTeamsIdList);
            IEnumerable<Guid> programOfferingsIdListFromTeams = _uow.ProgramOfferingRepository
                .GetProgramOfferingIdListByPortalTeamIdList(portalTeamsIdList);

            var accountIdListForRules = string.Join("", accountIdList.Select(a => "<value>" + a.ToString() + "</value>").ToList());

            if (accountIdListForRules == "")
            {
                accountIdListForRules = "<value>" + new Guid() + "</value>";
            }

            var portalTeamsIdListForRules = string.Join("", portalTeamsIdList.Select(a => "<value>" + a.ToString() + "</value>"));

            var programOfferingsIdListFromTeamsForRules = string.Join("", programOfferingsIdListFromTeams.Select(a => "<value>" + a.ToString() + "</value>"));
            if (programOfferingsIdListFromTeamsForRules == "")
            {
                programOfferingsIdListFromTeamsForRules = "<value>" + new Guid() + "</value>";
            }

            Entity defaultPortalTeam = _uow.PortalConfigurationRepository.GetDefaultPortalTeamByContactId(UserId);
            var measureTemplateName = defaultPortalTeam?.GetAttributeValue<string>("ddsm_mesuretemplatename");
            if (string.IsNullOrEmpty(measureTemplateName))
            {
                measureTemplateName = "BER-Instant Rebates";
            }

            var filterDate = defaultPortalTeam?.GetAttributeValue<DateTime?>("ddsm_measuretemplatedate") ?? DateTime.Now;

            //Replace the current template values with those values that we get at runtime
            List<string> rulesXml = rules.Select(r => r["ddsm_fetchxml"].ToString()
                    .Replace("{{<value>accountIdList</value>}}", accountIdListForRules)
                    .Replace("{{<value>portalTeamsIdList</value>}}", portalTeamsIdListForRules)
                    .Replace("{{<value>programOfferingsIdListFromTeams</value>}}", programOfferingsIdListFromTeamsForRules)
                    .Replace("{{measureTemplateName}}", measureTemplateName)
                    .Replace("{{filterDate}}", filterDate.ToString())
                    .Replace("{{userId}}", UserId.ToString())
                    .Replace("{{todaysDate}}", DateTime.UtcNow.ToString("o")))
                .ToList();

            var parentFilter = new FilterExpression(LogicalOperator.Or);

            foreach (var ruleXml in rulesXml)
            {
                var conversionRequest = new FetchXmlToQueryExpressionRequest
                {
                    FetchXml = ruleXml
                };

                FetchXmlToQueryExpressionResponse conversionResponse = _uow.EntityHelperRepository.GetConversionResponse(conversionRequest);
                QueryExpression queryExpression = conversionResponse.Query;

                FilterExpression tempFilter = GetClearedFilter(queryExpression.Criteria);

                var linkEntities = queryExpression.LinkEntities;

                foreach (LinkEntity link in linkEntities)
                {
                    if(!query.LinkEntities.Any(l => l.LinkFromEntityName == link.LinkFromEntityName && l.LinkToEntityName == link.LinkToEntityName))
                    {
                        query.LinkEntities.Add(link);
                    }                    
                }

                if (tempFilter != null)
                {
                    parentFilter.AddFilter(tempFilter);
                }                
            }

            query.Criteria.AddFilter(parentFilter);
        }

        private static FilterExpression GetClearedFilter(FilterExpression filter)
        {
            if (!filter.Filters.Any())
            {
                IEnumerable<ConditionExpression> conditions = GetConditions(filter.Conditions);
                filter.RemoveConditions();

                if (conditions.Count() != 0)
                {
                    filter.Conditions.AddRange(conditions);
                    return filter;
                }
                return null;
            }
            else
            {
                IEnumerable<ConditionExpression> conditions = GetConditions(filter.Conditions);
                filter.RemoveConditions();

                if (conditions.Count() != 0)
                {
                    filter.Conditions.AddRange(conditions);
                }

                var filters = new List<FilterExpression>();

                foreach(FilterExpression childFilter in filter.Filters)
                {
                    FilterExpression bufFilter = GetClearedFilter(childFilter);

                    if(bufFilter != null)
                    {
                        filters.Add(bufFilter);
                    }
                }

                filter.RemoveFilters();

                if (filters.Count() != 0)
                {
                    filter.Filters.AddRange(filters);
                }

                return filter;
            }
        }

        private static IReadOnlyList<ConditionExpression> GetConditions(IEnumerable<ConditionExpression> conditions)
        {
            var newConditions = new List<ConditionExpression>();

            foreach (var cond in conditions)
            {
                if (!(cond.Operator != ConditionOperator.Null && cond.Operator != ConditionOperator.NotNull && !cond.Values.Any()))
                {
                    newConditions.Add(cond);
                }
            }

            return newConditions;
        }

        private static void RemoveConditions(this FilterExpression filter)
        {
            while(filter.Conditions.Any())
            {
                filter.Conditions.RemoveAt(0);
            }
        }

        private static void RemoveFilters(this FilterExpression filter)
        {
            while (filter.Filters.Any())
            {
                filter.Filters.RemoveAt(0);
            }
        }
        
        private static IEnumerable<Entity> GetPortalTeamsList()
        {
            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);
            IEnumerable<Entity> portalTeams = _uow.PortalConfigurationRepository.GetPortalTeams(teamsIdList);

            return portalTeams;
        }

        private static IReadOnlyList<Guid> GetAccountsIdListForE1(IEnumerable<Guid> portalTeamsIdList)
        {
            List<Guid> accountIdList = _uow.PortalConfigurationRepository
              .GetAccountIdListByPortalTeamIdList(portalTeamsIdList).ToList();
        
            if (!accountIdList.Any())
            {
                //contact
                Entity portalUser = _uow.ContactRepository.Get(UserId, new[] { "parentcustomerid" });
                var accountRef = portalUser?.GetAttributeValue<EntityReference>("parentcustomerid");

                if (accountRef != null)
                {
                    return new[] { accountRef.Id };
                }
            }

            return accountIdList;
        }
    }
}