﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Portal.BLL.Services;

namespace Portal.BLL.Helpers
{
    public class EntityFieldMapper
    {
        private readonly Dictionary<string, string> _contactRule = new Dictionary<string, string>
        {
            {"ddsm_contact_firstname","firstname" },
            {"ddsm_login","ddsm_login" },
            {"ddsm_contact_lastname","lastname"},
            {"ddsm_password","ddsm_password"},
            {"ddsm_email","emailaddress1"}
        };

        private readonly Dictionary<string, string> _accountResidentRule = new Dictionary<string, string>
        {
            {"ddsm_account_firstname","ddsm_firstname" },
            {"ddsm_account_lastname","ddsm_lastname" },
            {"ddsm_account_mainphone","telephone1"},
            {"ddsm_account_email","emailaddress1"},
            {"ddsm_address1street1","address1_line1"},
            {"ddsm_address1street2","address1_line2"},
            {"ddsm_address1city","address1_city"},
            {"ddsm_address1province","address1_stateorprovince"},
            {"ddsm_address1postalcode","address1_postalcode"},
            {"ddsm_accounttype", "ddsm_accounttype" }
        };

        private readonly Dictionary<string, string> _accountBusinessRule = new Dictionary<string, string>
        {
            {"ddsm_account_name","ddsm_companyname" },
            {"ddsm_account_mainphone","telephone1"},
            {"ddsm_account_email","emailaddress1"},
            {"ddsm_address1street1","address1_line1"},
            {"ddsm_address1street2","address1_line2"},
            {"ddsm_address1city","address1_city"},
            {"ddsm_address1province","address1_stateorprovince"},
            {"ddsm_address1postalcode","address1_postalcode"},            
            {"ddsm_accounttype", "ddsm_accounttype" }
        };

        private readonly Dictionary<string, string> _siteRule = new Dictionary<string, string>
        {
            {"ddsm_address1","ddsm_address1" },
            {"ddsm_address2","ddsm_address2" },
            {"ddsm_city","ddsm_city"},
            {"ddsm_province","ddsm_state"},
            {"ddsm_address1postalcode","ddsm_zip" }
        };

        public Entity MapContact(Entity applicationEntity)
        {
            return _MapEntity(_contactRule, applicationEntity, "contact");

        }

        public Entity MapAccountResidential(Entity applicationEntity)
        {
            return _MapEntity(_accountResidentRule, applicationEntity, "account");
        }

        public Entity MapAccountBusiness(Entity applicationEntity)
        {
            return _MapEntity(_accountBusinessRule, applicationEntity, "account");
        }

        public Entity MapSite(Entity applicationEntity)
        {
            return _MapEntity(_siteRule, applicationEntity, "ddsm_site");
        }

        private static Entity _MapEntity(Dictionary<string, string> rule, Entity applicationEntity, string entityName)
        {
            var targetEntity = new Entity(entityName);

            List<string> applicationFieldNames = rule.Keys.ToList();

            var authService = new AuthService(null);

            foreach (string applicationField in applicationFieldNames)
            {
                string targetFieldName = rule[applicationField];
                if (string.IsNullOrEmpty(targetFieldName))
                {
                    continue;
                }

                var temp = applicationEntity.GetAttributeValue<object>(applicationField);
                if (temp != null)
                {
                    if (applicationField.ToLower() == "ddsm_password")
                    {
                        targetEntity[targetFieldName] = authService.CreateHashedPassword(applicationEntity.GetAttributeValue<string>(applicationField));
                        continue;
                    }

                    targetEntity[targetFieldName] = temp;
                }
                
            }

            return targetEntity;
        }
    }
}
