﻿//using System.Collections.Generic;
//using System.Linq;
//using Microsoft.Xrm.Sdk;
//using Portal.BLL.Services;
//using Portal.Models.Application;

//namespace Portal.BLL.Helpers
//{
//    public class EntityFieldMapper
//    {
//        private readonly Dictionary<string, ApplicationRequiredFieldMapModel> _contactRule = new Dictionary<string, ApplicationRequiredFieldMapModel>
//        {
//            {"ddsm_contact_firstname", new ApplicationRequiredFieldMapModel("firstname") },
//            {"ddsm_login",new ApplicationRequiredFieldMapModel("ddsm_login" )},
//            {"ddsm_contact_lastname",new ApplicationRequiredFieldMapModel("lastname")},
//            {"ddsm_password",new ApplicationRequiredFieldMapModel("ddsm_password")},
//            {"ddsm_email",new ApplicationRequiredFieldMapModel("emailaddress1")}
//        };

//        private readonly Dictionary<string, ApplicationRequiredFieldMapModel> _accountResidentRule = new Dictionary<string, ApplicationRequiredFieldMapModel>
//        {
//            {"ddsm_account_firstname",new ApplicationRequiredFieldMapModel("ddsm_firstname" )},
//            {"ddsm_account_lastname",new ApplicationRequiredFieldMapModel("ddsm_lastname" )},
//            {"ddsm_account_mainphone",new ApplicationRequiredFieldMapModel("telephone1")},
//            {"ddsm_account_email",new ApplicationRequiredFieldMapModel("emailaddress1", false)},
//            {"ddsm_address1street1",new ApplicationRequiredFieldMapModel("address1_line1")},
//            {"ddsm_address1street2",new ApplicationRequiredFieldMapModel("address1_line2", false)},
//            {"ddsm_address1city",new ApplicationRequiredFieldMapModel("address1_city")},
//            {"ddsm_address1province",new ApplicationRequiredFieldMapModel("address1_stateorprovince", false)},
//            {"ddsm_address1postalcode",new ApplicationRequiredFieldMapModel("address1_postalcode", false)}
//        };

//        private readonly Dictionary<string, ApplicationRequiredFieldMapModel> _accountBusinessRule = new Dictionary<string, ApplicationRequiredFieldMapModel>
//        {
//            {"ddsm_account_name",new ApplicationRequiredFieldMapModel("ddsm_companyname" )},
//            {"ddsm_account_mainphone",new ApplicationRequiredFieldMapModel("telephone1")},
//            {"ddsm_account_email",new ApplicationRequiredFieldMapModel("emailaddress1", false)},
//            {"ddsm_address1street1",new ApplicationRequiredFieldMapModel("address1_line1")},
//            {"ddsm_address1street2",new ApplicationRequiredFieldMapModel("address1_line2", false)},
//            {"ddsm_address1city",new ApplicationRequiredFieldMapModel("address1_city")},
//            {"ddsm_address1province",new ApplicationRequiredFieldMapModel("address1_stateorprovince", false)},
//            {"ddsm_address1postalcode",new ApplicationRequiredFieldMapModel("address1_postalcode", false)}
//        };

//        private readonly Dictionary<string, ApplicationRequiredFieldMapModel> _siteRule = new Dictionary<string, ApplicationRequiredFieldMapModel>
//        {
//            {"ddsm_address1",new ApplicationRequiredFieldMapModel("ddsm_address1") },
//            {"ddsm_address2",new ApplicationRequiredFieldMapModel("ddsm_address2", false )},
//            {"ddsm_city",new ApplicationRequiredFieldMapModel("ddsm_city")},
//            {"ddsm_province",new ApplicationRequiredFieldMapModel("ddsm_state", false)},
//            {"ddsm_sqft",new ApplicationRequiredFieldMapModel("ddsm_sqft")}
//        };

//        public Entity MapContact(Entity applicationEntity)
//        {
//            return _MapEntity(_contactRule, applicationEntity, "contact");

//        }

//        public Entity MapAccountResidential(Entity applicationEntity)
//        {
//            return _MapEntity(_accountResidentRule, applicationEntity, "account");
//        }

//        public Entity MapAccountBusiness(Entity applicationEntity)
//        {
//            return _MapEntity(_accountBusinessRule, applicationEntity, "account");
//        }

//        public Entity MapSite(Entity applicationEntity)
//        {
//            return _MapEntity(_siteRule, applicationEntity, "ddsm_site");
//        }

//        public bool CheckSite(Entity applicationEntity)
//        {
//            return _CheckFieldsForMapEntity(_siteRule, applicationEntity, "ddsm_site");
//        }

//        public bool CheckAccountResident(Entity applicationEntity)
//        {
//            return _CheckFieldsForMapEntity(_accountResidentRule, applicationEntity, "account");
//        }

//        public bool CheckAccountBusiness(Entity applicationEntity)
//        {
//            return _CheckFieldsForMapEntity(_accountBusinessRule, applicationEntity, "account");
//        }

//        private static Entity _MapEntity(Dictionary<string, ApplicationRequiredFieldMapModel> rule, Entity applicationEntity, string entityName)
//        {
//            var targetEntity = new Entity(entityName);

//            List<string> applicationFieldNames = rule.Keys.ToList();

//            var authService = new AuthService(null);

//            foreach (string applicationField in applicationFieldNames)
//            {
//                string targetFieldName = rule[applicationField].FieldName;
//                if (string.IsNullOrEmpty(targetFieldName))
//                {
//                    continue;
//                }

//                var temp = applicationEntity.GetAttributeValue<object>(applicationField);
//                if (temp != null)
//                {
//                    if (applicationField.ToLower() == "ddsm_password")
//                    {
//                        targetEntity[targetFieldName] = authService.CreateHashedPassword(applicationEntity.GetAttributeValue<string>(applicationField));
//                        continue;
//                    }

//                    targetEntity[targetFieldName] = temp;
//                }
//            }

//            return targetEntity;
//        }

//        private static bool _CheckFieldsForMapEntity(Dictionary<string, ApplicationRequiredFieldMapModel> rule, Entity applicationEntity, string entityName)
//        {
//            List<string> applicationFieldNames = rule.Keys.ToList();

//            foreach (string applicationField in applicationFieldNames)
//            {
//                string targetFieldName = rule[applicationField].FieldName;
//                if (string.IsNullOrEmpty(targetFieldName))
//                {
//                    continue;
//                }

//                var temp = applicationEntity.GetAttributeValue<object>(applicationField);
//                if (temp != null)
//                {
//                    return true;

//                }
//            }
//            return false;
//        }
//    }
//}
