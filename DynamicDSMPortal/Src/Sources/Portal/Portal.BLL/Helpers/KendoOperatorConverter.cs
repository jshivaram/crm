﻿using Microsoft.Xrm.Sdk.Query;
using Portal.Models.Kendo;
using System;

namespace Portal.BLL.Helpers
{
    public static class KendoOperatorConverter
    {
        public static ConditionOperator Convert(string kendoOperatorStr)
        {
            var kendoOperator = (KendoOperators)Enum
                .Parse(typeof(KendoOperators), kendoOperatorStr, true);
            
            switch(kendoOperator)
            {
                case KendoOperators.Contains:
                    return ConditionOperator.Like;
                case KendoOperators.DoesNotContain:
                    return ConditionOperator.NotLike;
                case KendoOperators.Eq:
                    return ConditionOperator.Equal;
                case KendoOperators.Neq:
                    return ConditionOperator.NotEqual;
                case KendoOperators.IsNull:
                    return ConditionOperator.Null;
                case KendoOperators.IsNotNull:
                    return ConditionOperator.NotNull;
                case KendoOperators.Lt:
                    return ConditionOperator.LessThan;
                case KendoOperators.Lte:
                    return ConditionOperator.LessEqual;
                case KendoOperators.Gt:
                    return ConditionOperator.GreaterThan;
                case KendoOperators.Gte:
                    return ConditionOperator.GreaterEqual;
                case KendoOperators.StartsWith:
                    return ConditionOperator.BeginsWith;
                case KendoOperators.EndsWith:
                    return ConditionOperator.EndsWith;
                case KendoOperators.IsEmpty:
                    return ConditionOperator.Null;
                case KendoOperators.IsNotEmpty:
                    return ConditionOperator.NotNull;
            } 

            return ConditionOperator.Like;
        }
    }
}