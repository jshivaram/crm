﻿using Microsoft.Xrm.Sdk.Query;

namespace Portal.BLL.Helpers
{
    public static class QueryExpressionHelper
    {
        public static void AddAdditionalConditions(QueryExpression query)
        {
            if (query.EntityName == "team")
            {
                query.Criteria.AddCondition("queueid", ConditionOperator.NotNull);
            }
        }
    }
}
