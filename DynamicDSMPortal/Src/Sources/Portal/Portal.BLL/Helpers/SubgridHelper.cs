﻿using Microsoft.Xrm.Sdk;
using Portal.BLL.Serializers;
using Portal.DAL.Interfaces;
using Portal.Models.GridModels;
using System.Collections.Generic;
using System.Linq;

namespace Portal.BLL.Helpers
{
    public class SubgridHelper
    {
        private readonly IUnitOfWork _uow;

        public SubgridHelper(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IReadOnlyList<RelatedEntityCollection> GetEntitiesFromSubgrids(IEnumerable<SubgridCreationModel> subgridModels)
        {
            var result = new List<RelatedEntityCollection>();

            if (subgridModels == null)
            {
                return result;
            }

            foreach (SubgridCreationModel subgridModel in subgridModels)
            {
                RelatedEntityCollection buf = GetEntitiesFromSubgrid(subgridModel);

                if(buf != null)
                {
                    result.Add(buf);
                }
            }

            return result;
        }

        public RelatedEntityCollection GetEntitiesFromSubgrid(SubgridCreationModel subgridModel)
        {
            if(string.IsNullOrEmpty(subgridModel?.EntityLogicalName) || string.IsNullOrEmpty(subgridModel.RelationshipName) || !subgridModel.GridDataJsonList.Any())
            {
                return null;
            }

            var entities = new List<Entity>();

            var entitySerializer = new EntitySerializer(_uow);

            foreach(string entityJson in subgridModel.GridDataJsonList)
            {
                Entity entity = entitySerializer.Deserialize(entityJson, subgridModel.EntityLogicalName);

                if(entity == null)
                {
                    continue;
                }

                entities.Add(entity);
            }

            if(!entities.Any())
            {
                return null;
            }

            var relatedEntityCollection = new RelatedEntityCollection();
            var entityCollection = new EntityCollection(entities);
            var relationShip = new Relationship(subgridModel.RelationshipName);

            relatedEntityCollection.Add(relationShip, entityCollection);

            return relatedEntityCollection;
        }
    }
}
