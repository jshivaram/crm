﻿namespace Portal.BLL.Interfaces
{
    public interface IAccessService : IBaseService
    {
        bool CanDelete(string entityLogicalName);
        bool CanCreate(string entityLogicalName);
        bool CanUpdate(string entityLogicalName);
    }
}