﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Portal.Models;
using Portal.Models.Account;

namespace Portal.BLL.Interfaces
{
    public interface IAccountService
    {
        Task<AccountInfoForApplication> GetAccountInfoByAccountNumberAsync(string accountNumber);
        Task<IReadOnlyList<LookupModel>> GetContractorsAsync(string filterValue);
    }
}