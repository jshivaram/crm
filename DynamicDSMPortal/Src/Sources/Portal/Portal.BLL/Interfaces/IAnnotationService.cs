﻿using Portal.Models.AttachmentsDocument;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Portal.BLL.Interfaces
{
    public interface IAnnotationService: IBaseService
    {
        IEnumerable<AttachmentsDocumentViewModel> GetRelatedAttachmentsDocuments(string entityLogicalName, Guid entityId,
            CultureInfo browserCulture, string browserIanaTimeZone);
    }
}
