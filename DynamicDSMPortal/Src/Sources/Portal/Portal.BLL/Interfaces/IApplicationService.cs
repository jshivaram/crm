﻿using Portal.Models;
using Portal.Models.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Interfaces
{
    public interface IApplicationService
    {
         IEnumerable<MeasureTemplateModel> GetMeasureTemplates(IEnumerable<Guid> idList);
         IEnumerable<LookupModel> GetMeasureTemplatesForLookup(Guid projectTemplateId);
         IEnumerable<LookupModel> GetApplicationsFormListForUnregisters();
        string GetFormStructureForUnregisterApplication(Guid id);
        Guid CreateAndReturnId(string jsonEntityData, string logicalName);
    }
}
