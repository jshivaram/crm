﻿using Portal.Models;
using Portal.Models.Approve;
using Portal.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Portal.BLL.Interfaces
{
    public interface IAprroveService : IBaseService
    {
        void ApproveModelNumber(ApproveModel approveModel);
        bool IsModelNumberApproved(Guid id);
        void ApproveSku(ApproveModel approveModel);
        bool IsSkuApproved(Guid id);
        Task<IEnumerable<LookupModel>> GetProgramOfferingsAsync(KendoFilters kendoFilters);
        Task<IEnumerable<LookupModel>> GetMeasureTemplatesAsync(KendoFilters kendoFilters, Guid programOfferingId);
        Guid? CreateAndApprove(CreateAndApproveModel entityModel);
    }
}
