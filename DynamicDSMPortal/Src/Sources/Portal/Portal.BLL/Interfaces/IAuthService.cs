﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portal.Models.Auth;
using Microsoft.Xrm.Sdk;

namespace Portal.BLL.Interfaces
{
    public interface IAuthService
    {
        void Register(RegisterModel registerModel);
        bool ContactExists(string login);
        Entity GetContact(string login, string password);        
    }
}
