﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.Models.EntityClasses;
using Portal.Models.EntityModels;
using Portal.Models.GridModels;
using Portal.Models.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Interfaces
{
    public interface IChartService : IBaseService
    {
        DictionaryWithTotalCountModel<string> GetAllData(Guid viewId, 
            string entityLogicalName, 
            int countOfEntities, 
            Guid visualizationId);
        EntitiesWithCountModel GetEntitiesForChartWithCount(string viewXml);
        IDictionary<string, string> GetVisualizationData(Guid visualizationId);
        string GetViewXmlForChart(Guid? viewId, Guid visualizationId);
        IDictionary<string, string> GetSavedQueryForChart(Guid? viewId);
        IEnumerable<AttributeMetadataModel> GetAttributesMetadataForChart(string entityLogicalName,
            Guid viewId, Guid visualizationId);
    }
}
