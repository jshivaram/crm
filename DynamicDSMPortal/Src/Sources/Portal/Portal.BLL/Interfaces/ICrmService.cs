﻿namespace Portal.BLL.Interfaces
{
    public interface ICrmService
    {
        /// <summary>
        ///     Reads a connection string from a Web.config file
        /// </summary>
        /// <returns></returns>
        string GetUrl();
    }
}