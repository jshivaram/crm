﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Interfaces
{
    public interface IDataUploaderService : IBaseService
    {
        void CreateDataUploaderRecord(int typeOfUploadedData, byte[] file, string fileName);
    }
}
