﻿using Microsoft.Xrm.Sdk;

using Portal.Models;
using Portal.Models.EntityModels;
using Portal.Models.GridModels;
using Portal.Models.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Interfaces
{
    public interface IEntityService : IBaseService
    {
        IEnumerable<NavigationEntityModel> GetEntitiesForNavigation();

        DictionaryWithTotalCountModel<string> GetEntitiesDictionaryListWithCount(
            string entityLogicalName,
             GridPagingModel pagination,
             Guid viewId,
             string searchText = null);

        DictionaryWithTotalCountModel<string> GetRelatedEntitiesDictionaryListWithTotalCount(
            Guid entityId, Guid viewId, string entityLogicalName,
            string relatedEntityName, string relationshipShcemeName,
            GridPagingModel pagination = null,
            string searchText = null);

        Entity Get(Guid id, string entityLogicalName);
        Entity GetRelatedEntity(string entityLogicalName, string relationshipShcemeName);

        IEnumerable<object> GetRelatedEntities(Guid entityId, Guid viewId, 
            string entityLogicalName, string relatedEntityName, string relationshipShcemeName);

        IEnumerable<LookupModel> GetEntitiesForLookup(Guid viewId);
        IEnumerable<LookupModel> GetEntitiesForLookup(string entityLogicalName);

        void CreateAndAssociateRelatedEntities(RelatedEntitiesCreationModel relatedEntitiesCreationModel);
        void Delete(Guid id, string entityLogicalName);
        void DeleteRange(IEnumerable<Guid> idArr, string entityLogicalName);
        void Update(string jsonEntityData, string logicalName);
        void Update(EntityListModel entityListModel);

        void Create(string jsonEntityData, string logicalName);
        Guid CreateAndReturnId(string jsonEntityData, string logicalName);
        IEnumerable<LookupModel> GetEntitiesForGridLookupByField(string entityLogicalName, string fieldName);
    }
}
