﻿using Microsoft.Xrm.Sdk.Query;

namespace Portal.BLL.Interfaces
{
    public interface IEntityServiceHelper : IBaseService
    {
        QueryExpression AddConditionsToQuery(QueryExpression query, string viewXml);
    }
}