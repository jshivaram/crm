﻿using Portal.DAL;
using Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Interfaces
{
    public interface IFormService : IBaseService
    {
        Task<IEnumerable<FormControl>> GetFormControls(string entityLogicalName);
        Task<string> GetJsonFormStructureAsync(string entityLogicalName);

        string GetJsonFormStructure(string entityLogicalName);
        string GetJsonFormStructure(string entityLogicalName, string formName);
        string GetJsonFormStructure(string entityLogicalName, Guid formId, FormTypeEnum formType = FormTypeEnum.FormTypeCreateUpdate);

        IDictionary<Guid, string> GetForms(string entityLogicalName);
        IEnumerable<object> GetForms(string entityLogicalName, Guid contactId);
        Guid? GetDefaultFormId(string entityLogicalName, Guid contactId);
        IEnumerable<LookupModel> GetDashboards();

        string GetJsonFormStructure(Guid formId,
            FormTypeEnum formType = FormTypeEnum.FormTypeCreateUpdate);

        Guid? GetDefaultDashboardId();
    }
}
