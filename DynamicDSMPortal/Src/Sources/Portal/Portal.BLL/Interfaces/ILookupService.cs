﻿using Portal.Models;
using Portal.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Interfaces
{
    public interface ILookupService
    {
        IEnumerable<LookupModel> GetEntitiesForLookup(LookupFilterModel lookupFilterModel);
    }
}
