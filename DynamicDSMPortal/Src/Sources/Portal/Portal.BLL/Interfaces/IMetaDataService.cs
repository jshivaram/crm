﻿using Microsoft.Xrm.Sdk.Metadata;
using Portal.Models.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Interfaces
{
    public interface IMetaDataService : IBaseService
    {
        OptionSetMetadata GetOptionSet(string entityLogicalName, string fieldName);
        IEnumerable<object> GetMetaDataForDropDownList(string entityLogicalName, string fieldName);
        string GetProcesStageByEntityId(string entityId);

        IEnumerable<AttributeMetadataModel> GetAttributesMetadata(string entityLogicalName, Guid viewId);
        string GetLookupFieldEntityName(string entityName, string field);
    }
}
