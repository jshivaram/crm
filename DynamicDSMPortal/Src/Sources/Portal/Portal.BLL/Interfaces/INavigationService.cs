﻿using Portal.Models.Navigation;
using System.Collections.Generic;

namespace Portal.BLL.Interfaces
{
    public interface INavigationService : IBaseService
    {
        IEnumerable<NavigationEntityModel> GetEntitiesForNavigation();
    }
}
