﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Interfaces
{
    public interface IPostsService
    {
        IEnumerable<Entity> GetPostsByEntityId(Guid entityId);
    }
}
