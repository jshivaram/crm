﻿using System;
using System.Threading.Tasks;

namespace Portal.BLL.Interfaces
{
    public interface IProjectService : IBaseService
    {
        Task<string> GoToNextStepAsync(Guid? projectId);
        Task<bool> CantStepAsync();
    }
}
