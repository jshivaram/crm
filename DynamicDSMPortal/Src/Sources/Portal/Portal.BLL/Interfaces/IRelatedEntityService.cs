﻿using Portal.Models.EntityModels;
using System;

namespace Portal.BLL.Interfaces
{
    public interface IRelatedEntityService : IBaseService
    {
        Guid CreateRelatedEntityFromForm(RelatedEntityCreationModel relatedEntityCreationModel);
    }
}
