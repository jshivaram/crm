﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Interfaces
{
    public interface ISavedQueryService : IBaseService
    {
        IEnumerable<object> GetViews(string entityLogicalName);
        Entity GetView(Guid id);
        string GetViewXml(Guid id);
        IEnumerable<string> GetPropNamesFromView(string viewXml);
        Dictionary<string, string> GetFieldsDisplayNames(string entityLogicalName, Guid viewId);
        Guid GetDefaultViewId(string entityLogicalName);
        Guid GetLookupViewId(string entityLogicalName);
    }
}
