﻿using Portal.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Interfaces
{
    public interface ISecurityService : IBaseService
    {
        bool CheckAccess(Guid contactId, Privilege privilege, string entityLogicalName);
    }
}
