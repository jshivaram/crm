﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Interfaces
{
    public interface IThemeService: IBaseService
    {
        string GetDefaultThemeName();
        IEnumerable<string> GetThemesNamesList();
    }
}
