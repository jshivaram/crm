﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.BLL.Helpers;
using Portal.Models.EntityModels;
using Portal.Models.Enums;

namespace Portal.BLL.Services
{
    public partial class ApplicationService
    {
        public Guid CreateApplicationFromBoilerApplication(EntityModel entityModel)
        {
            Entity mappedApplication = ParseRequestToApplicationEntity(entityModel.JsonEntityData, entityModel.LogicalName, entityModel.SubgridCreationModels);

            bool isUserAnon = UserId == Guid.Empty;
            bool isNewApp = mappedApplication.Id == Guid.Empty;

            if (isUserAnon)
            {
                return SubmitForNonAuthorizedApp(mappedApplication);
            }

            if (isNewApp)
            {
                return SubmitForNewAuthorizedApp(mappedApplication);
            }

            return SubmitForCreatedAuthorizedApp(mappedApplication);
        }

        public Guid CreateApplicationFromBoilerApplicationDraft(EntityModel entityModel)
        {
            Entity mappedApplication =
                ParseRequestToApplicationEntity(entityModel.JsonEntityData,
                    entityModel.LogicalName, entityModel.SubgridCreationModels);

            _CreateBoilerApplicationNameWhenEmptyName(mappedApplication);
            var mapper = new EntityFieldMapper();
            Entity mappedContact = mapper.MapContact(mappedApplication);

            mappedContact["ddsm_isportaluser"] = true;
            mappedContact["ddsm_isuser"] = true;

            Guid createdUserGuid = CreateContact(mappedContact);
            _uow.ContactRepository.AddContactToStandartTeam(createdUserGuid);

            return CreateApplication(mappedApplication);
        }

        private Guid SubmitForNewAuthorizedApp(Entity applicationEntity)
        {
            //application field
            const string ddsmAccountNumber = "ddsm_accountnumber";
            const string accountTypeFieldName = "ddsm_accounttype";

            _CreateBoilerApplicationNameWhenEmptyName(applicationEntity);

            applicationEntity["ddsm_applicationissubmited"] = true;
            var mapper = new EntityFieldMapper();
            Entity mappedContact = GetContact(UserId);

            Entity account = null;

            var accountNumberField = applicationEntity.GetAttributeValue<string>(ddsmAccountNumber);
            if (accountNumberField != null)
            {
                account = GetAccountByAccountNumber(accountNumberField);
            }
            var accountId = applicationEntity.GetAttributeValue<EntityReference>("ddsm_parentaccountid");

            if (accountId != null)
            {
                account = new Entity("account", accountId.Id);
            }

            if (account == null)
            {
                var accountType = applicationEntity.GetAttributeValue<OptionSetValue>(accountTypeFieldName);

                if (accountType == null)
                {
                    throw new Exception("Account Type not selected");
                }

                var accountTypeEnum = (AccountTypeEnum)accountType.Value;
                Entity mappedAccount;

                switch (accountTypeEnum)
                {
                    case AccountTypeEnum.Business:
                        mappedAccount = mapper.MapAccountBusiness(applicationEntity);
                        break;
                    case AccountTypeEnum.Residential:
                        mappedAccount = mapper.MapAccountResidential(applicationEntity);
                        break;
                    default:
                        throw new Exception("Account type cannot be empty");
                }
                account = mappedAccount;
            }

            if (account == null)
            {
                throw new Exception("Cant create account");
            }

            Entity mappedSite = mapper.MapSite(applicationEntity);

            if (account.Id != Guid.Empty)
            {
                Guid? siteId = _uow.AccountRepository.GetParenteSiteId(account.Id);


                if (siteId.HasValue)
                {
                    AssociateAccountAndContact(account, mappedContact);
                    applicationEntity["ddsm_parentaccountid"] = account.ToEntityReference();
                    applicationEntity["ddsm_parentsiteid"] = new EntityReference("ddsm_site", siteId.Value);
                    return CreateApplication(applicationEntity);
                }
                else
                {
                    Guid createdSiteId = CreateSite(mappedSite);
                    var site = new Entity("ddsm_site", createdSiteId);
                    AssociateAccountAndSite(account, site);

                    AssociateAccountAndContact(account, mappedContact);
                    applicationEntity["ddsm_parentaccountid"] = new EntityReference("account", account.Id);
                    applicationEntity["ddsm_parentsiteid"] = site.ToEntityReference();
                    return CreateApplication(applicationEntity);
                }
            }
            {

                Guid createdSiteId = CreateSite(mappedSite);
                var site = new Entity("ddsm_site", createdSiteId);

                Guid accountGuid = CreateAccount(account);
                account = new Entity("account", accountGuid);
                AssociateAccountAndSite(account, site);

                AssociateAccountAndContact(account, mappedContact);

                applicationEntity["ddsm_parentaccountid"] = new EntityReference("account", accountGuid);
                applicationEntity["ddsm_parentsiteid"] = site.ToEntityReference();

                return CreateApplication(applicationEntity);
            }
        }

        private Guid SubmitForCreatedAuthorizedApp(Entity applicationEntity)
        {
            //application field
            const string ddsmAccountNumber = "ddsm_accountnumber";
            const string accountTypeFieldName = "ddsm_accounttype";

            _CreateBoilerApplicationNameWhenEmptyName(applicationEntity);

            Guid applicationGuid = applicationEntity.Id;
            applicationEntity["ddsm_applicationissubmited"] = true;
            var mapper = new EntityFieldMapper();
            Entity mappedContact = GetContact(UserId);

            Entity account = null;

            var accountNumberField = applicationEntity.GetAttributeValue<string>(ddsmAccountNumber);

            if (accountNumberField == null)
            {
                Entity retrivedApplication = _uow.EntityRepository.Get(applicationEntity.Id, "ddsm_application", new ColumnSet(ddsmAccountNumber));
                accountNumberField = retrivedApplication.GetAttributeValue<string>(ddsmAccountNumber);
            }

            if (accountNumberField != null)
            {
                account = GetAccountByAccountNumber(accountNumberField);
            }

            if (account == null)
            {
                var accountId = applicationEntity.GetAttributeValue<EntityReference>("ddsm_parentaccountid");

                if (accountId != null)
                {
                    account = new Entity("account", accountId.Id);
                }
            }

            if (account == null)
            {
                var accountType = applicationEntity.GetAttributeValue<OptionSetValue>(accountTypeFieldName);

                if (accountType == null)
                {
                    throw new Exception("Account Type not selected");
                }

                var accountTypeEnum = (AccountTypeEnum)accountType.Value;
                Entity mappedAccount;

                switch (accountTypeEnum)
                {
                    case AccountTypeEnum.Business:
                        mappedAccount = mapper.MapAccountBusiness(applicationEntity);
                        break;
                    case AccountTypeEnum.Residential:
                        mappedAccount = mapper.MapAccountResidential(applicationEntity);
                        break;
                    default:
                        throw new Exception("Account type cannot be empty");
                }
                account = mappedAccount;
            }

            if (account == null)
            {
                throw new Exception("Cant create account");
            }

            Entity mappedSite = mapper.MapSite(applicationEntity);

            if (account.Id != Guid.Empty)
            {
                Guid? siteId = _uow.AccountRepository.GetParenteSiteId(account.Id);


                if (siteId.HasValue)
                {
                    applicationEntity["ddsm_parentaccountid"] = account.ToEntityReference();
                    applicationEntity["ddsm_parentsiteid"] = new EntityReference("ddsm_site", siteId.Value);

                    AssociateAccountAndContact(account, mappedContact);
                    UpdateApplication(applicationEntity);

                    return applicationGuid;
                }
                else
                {
                    Guid createdSiteId = CreateSite(mappedSite);
                    var site = new Entity("ddsm_site", createdSiteId);
                    AssociateAccountAndSite(account, site);

                    AssociateAccountAndContact(account, mappedContact);

                    applicationEntity["ddsm_parentaccountid"] = new EntityReference("account", account.Id);
                    applicationEntity["ddsm_parentsiteid"] = site.ToEntityReference();

                    UpdateApplication(applicationEntity);

                    return applicationGuid;
                }
            }
            {

                Guid createdSiteId = CreateSite(mappedSite);
                var site = new Entity("ddsm_site", createdSiteId);

                Guid accountGuid = CreateAccount(account);
                account = new Entity("account", accountGuid);
                AssociateAccountAndSite(account, site);

                AssociateAccountAndContact(account, mappedContact);

                applicationEntity["ddsm_parentaccountid"] = new EntityReference("account", accountGuid);
                applicationEntity["ddsm_parentsiteid"] = site.ToEntityReference();

                UpdateApplication(applicationEntity);

                return applicationGuid;
            }
        }

        private Guid SubmitForNonAuthorizedApp(Entity applicationEntity)
        {
            {
                //application field
                const string ddsmAccountNumber = "ddsm_accountnumber";
                const string accountTypeFieldName = "ddsm_accounttype";

                applicationEntity["ddsm_applicationissubmited"] = true;

                _CreateBoilerApplicationNameWhenEmptyName(applicationEntity);

                var mapper = new EntityFieldMapper();
                Entity mappedContact = mapper.MapContact(applicationEntity);
                mappedContact["ddsm_isportaluser"] = true;
                mappedContact["ddsm_isuser"] = true;

                Entity account = null;

                var accountNumberField = applicationEntity.GetAttributeValue<string>(ddsmAccountNumber);
                if (accountNumberField != null)
                {
                    account = GetAccountByAccountNumber(accountNumberField);
                }

                if (account == null)
                {
                    var accountType = applicationEntity.GetAttributeValue<OptionSetValue>(accountTypeFieldName);

                    if (accountType == null)
                    {
                        throw new Exception("Account Type not selected");
                    }

                    var accountTypeEnum = (AccountTypeEnum)accountType.Value;
                    Entity mappedAccount;

                    switch (accountTypeEnum)
                    {
                        case AccountTypeEnum.Business:
                            mappedAccount = mapper.MapAccountBusiness(applicationEntity);
                            break;
                        case AccountTypeEnum.Residential:
                            mappedAccount = mapper.MapAccountResidential(applicationEntity);
                            break;
                        default:
                            throw new Exception("Account type cannot be empty");
                    }

                    account = mappedAccount;

                }

                if (account == null)
                {
                    throw new Exception("Cant create account");
                }

                Entity mappedSite = mapper.MapSite(applicationEntity);

                if (account.Id != Guid.Empty)
                {
                    Guid? siteId = _uow.AccountRepository.GetParenteSiteId(account.Id);


                    if (siteId.HasValue)
                    {
                        Guid createdUserGuid = CreateContact(mappedContact);
                        _uow.ContactRepository.AddContactToStandartTeam(createdUserGuid);

                        var createdUser = new Entity("contact", createdUserGuid);
                        AssociateAccountAndContact(account, createdUser);

                        applicationEntity["ddsm_parentaccountid"] = account.ToEntityReference();
                        applicationEntity["ddsm_parentsiteid"] = new EntityReference("ddsm_site", siteId.Value);

                        return CreateApplication(applicationEntity);

                    }
                    else
                    {
                        Guid createdSiteId = CreateSite(mappedSite);
                        var site = new Entity("ddsm_site", createdSiteId);
                        AssociateAccountAndSite(account, site);

                        Guid createdUserGuid = CreateContact(mappedContact);
                        _uow.ContactRepository.AddContactToStandartTeam(createdUserGuid);

                        var createdUser = new Entity("contact", createdUserGuid);
                        AssociateAccountAndContact(account, createdUser);

                        applicationEntity["ddsm_parentaccountid"] = new EntityReference("account", account.Id);
                        applicationEntity["ddsm_parentsiteid"] = site.ToEntityReference();

                        return CreateApplication(applicationEntity);
                    }
                }
                {
                    Guid createdUserGuid = CreateContact(mappedContact);
                    _uow.ContactRepository.AddContactToStandartTeam(createdUserGuid);
                    var createdUser = new Entity("contact", createdUserGuid);

                    Guid createdSiteId = CreateSite(mappedSite);
                    var site = new Entity("ddsm_site", createdSiteId);

                    Guid accountGuid = CreateAccount(account);
                    account = new Entity("account", accountGuid);
                    AssociateAccountAndSite(account, site);

                    AssociateAccountAndContact(account, createdUser);

                    applicationEntity["ddsm_parentaccountid"] = new EntityReference("account", accountGuid);
                    applicationEntity["ddsm_parentsiteid"] = site.ToEntityReference();

                    return CreateApplication(applicationEntity);
                }
            }
        }

        private void _CreateBoilerApplicationNameWhenEmptyName(Entity application)
        {
            var currentApplicationName = application.GetAttributeValue<string>("ddsm_name");

            if (!string.IsNullOrEmpty(currentApplicationName)) return;

            string generatedApplicationName = CreateBoilerApplicationName(application, UserId);
            application["ddsm_name"] = generatedApplicationName;
        }
    }
}
