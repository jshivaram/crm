﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.Models;
using Portal.Models.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using Portal.BLL.Extensions;
using Portal.Models.EntityClasses;

namespace Portal.BLL.Services
{
    public partial class ApplicationService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns>Returns Lookup array. If user has no teams returns null</returns>
        public IEnumerable<LookupModel> GetApplicationsFormListForRegistered()
        {
            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);
            IList<Guid> teamIdList = teamsIdList as IList<Guid> ?? teamsIdList.ToList();
            if (!teamIdList.Any())
            {
                return new List<LookupModel>();
            }

            IEnumerable<Entity> portalTeams = _uow.PortalConfigurationRepository.GetPortalTeams(teamIdList);

            IList<Entity> enumerable = portalTeams as IList<Entity> ?? portalTeams.ToList();
            if (!enumerable.Any())
            {
                return new List<LookupModel>();
            }

            IEnumerable<Guid> portalTeamsIdList = enumerable.Select(t => t.Id).ToList();
            IEnumerable<Entity> appConfigs = _GetApplicationConfigsByTeams(portalTeamsIdList);

            List<ApplicationLookupWithConfigModel> result = appConfigs
                .Select(f => new ApplicationLookupWithConfigModel
            {
                Value = _GetValueFromJsonObject(f.GetAttributeValue<string>("ddsm_applicationform")),
                Text = f.GetAttributeValue<string>("ddsm_name"),
                ConfigId = f.Id.ToString()
            })
                .DistinctBy(a=>a.ConfigId)
                .ToList();

            return result;
        }

        private IEnumerable<Entity> _GetApplicationConfigsByTeams(IEnumerable<Guid> teamIdArray)
        {
            List<Guid> teamStringedIdArray = teamIdArray
                .ToList();
            const string portalApplicationConfig = "ddsm_portalapplicationconfiguration";
            const string portalTeamEntity = "ddsm_portalteam";

            const string relationName = "ddsm_ddsm_portalteam_ddsm_portalapplicationc";

            var query = new QueryExpression(portalApplicationConfig)
            {
                ColumnSet = new ColumnSet(true)
            };

            var portalApplicationConfigLink = new LinkEntity(
                portalApplicationConfig, relationName,
                "ddsm_portalapplicationconfigurationid", "ddsm_portalapplicationconfigurationid",
                JoinOperator.Inner);

            var portalTeamLink = new LinkEntity(
                relationName, portalTeamEntity,
                "ddsm_portalteamid", "ddsm_portalteamid",
                JoinOperator.Inner);

            portalTeamLink.LinkCriteria.AddCondition(new ConditionExpression("ddsm_portalteamid", ConditionOperator.In, teamStringedIdArray));

            portalApplicationConfigLink.LinkEntities.Add(portalTeamLink);
            query.LinkEntities.Add(portalApplicationConfigLink);


            IEnumerable<Entity> appRecords = _uow.EntityRepository.GetAllWithTotalCount(query).Entities;
            return appRecords;
        }

        private void SetHardcoreDataForLookups(Entity applicationEntity)
        {
            var appConfId = applicationEntity.GetAttributeValue<EntityReference>(LookupToAppConfig);
            const string fieldAppId = ApplicationConfigurationEntityName + "id";

            if (appConfId == null)
            {
                return;
            }

            var query = new QueryExpression(ApplicationConfigurationEntityName)
            {
                ColumnSet = new ColumnSet(fieldAppId, "ddsm_name"),
                Criteria = new FilterExpression()
            };

            query.Criteria.AddCondition(fieldAppId, ConditionOperator.Equal, appConfId.Id);

            EntitiesWithCountModel applicationConfigModel = _uow.EntityRepository.GetAllWithTotalCount(query);
            Entity applicationConfigEntity = applicationConfigModel.Entities.FirstOrDefault();

            if (applicationConfigEntity == null)
            {
                return;
            }

            var applicationName = applicationConfigEntity.GetAttributeValue<string>("ddsm_name");

            if (applicationName != "Boiler Application")
            {
                return;
            }

            query = new QueryExpression("ddsm_projecttemplate");
            query.ColumnSet.AddColumns("ddsm_projecttemplateid", "ddsm_name");

            query.Criteria = new FilterExpression();
            query.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, "Boiler GH 2017");

            EntitiesWithCountModel projectTemplateModel = _uow.EntityRepository.GetAllWithTotalCount(query);
            EntityReference projectTemplateEntityReference = projectTemplateModel.Entities.FirstOrDefault()?.ToEntityReference();

            applicationEntity["ddsm_projecttemplateid"] = projectTemplateEntityReference;

            query = new QueryExpression("ddsm_programoffering");
            query.ColumnSet.AddColumns("ddsm_programofferingid", "ddsm_name");

            query.Criteria = new FilterExpression();
            query.Criteria.AddCondition("ddsm_name", ConditionOperator.Equal, "Green Heat 2017");

            EntitiesWithCountModel programOfferingModel = _uow.EntityRepository.GetAllWithTotalCount(query);
            EntityReference programOfferingEntityReference = programOfferingModel.Entities.FirstOrDefault()?.ToEntityReference();

            applicationEntity["ddsm_programofferingid"] = programOfferingEntityReference;
        }

        public void UpdateApplication(Entity applicationEntity)
        {
            _uow.EntityRepository.Update(applicationEntity);
        }
    }
}
