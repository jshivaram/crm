﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Portal.BLL.Services
{
    public partial class PortalConfigurationService
    {
        public Entity GetPortalConfigurationByName(string configName)
        {
            string entityName = "ddsm_portalconfig";

            var configs = _uow.EntityRepository.GetEntityRecordsByRecordName(entityName, configName);

            if(configs.Count() == 0)
            {
                throw new Exception("Config list with current config name is empty");
            }

            Entity currentEntity = configs.FirstOrDefault();           

            return currentEntity;
        }

        public IEnumerable<Entity> GetEntitiesByConfigId(Guid configId)
        {
            var portalEntities = _uow.PortalConfigurationRepository.GetPortalEntitiesByConfigId(configId);
            return portalEntities;
        }

        public IEnumerable<Entity> GetDashboardsByConfigId(Guid configId)
        {
            var portalEntities = _uow.PortalConfigurationRepository.GetPortalDashboardsByConfigId(configId);
            return portalEntities;
        }
    }
}
