﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.RestClients
{
    public static class StaticRestClient
    {
        public static Task<HttpResponseMessage> PostAsync(string url, string content)
        {
            string crmConn = ConfigurationManager.ConnectionStrings["CrmConnection"].ToString();
            
            string[] crmConnArr = crmConn.Split(';');
            string crmUrl = crmConnArr[0]?.Replace("Url=", "")?.Trim();
            string login = crmConnArr[1]?.Replace("Username=", "")?.Trim();
            string password = crmConnArr[2]?.Replace("Password=", "")?.Trim();

            var client = new HttpClient(new HttpClientHandler()
            {
                Credentials = new NetworkCredential(login, password)
            });

            client.BaseAddress = new Uri(crmUrl);
            client.Timeout = new TimeSpan(0, 2, 0);

            string baseUrl = crmUrl + "/api/data/v8.1";

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(baseUrl + url),
                Method = HttpMethod.Post
            };

            request.Headers.Add("Accept", "application/json");
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.Headers.Add("OData-MaxVersion", "4.0");
            request.Headers.Add("OData-Version", "4.0");

            request.Content = new StringContent(content, Encoding.UTF8, "application/json");

            var task = client.SendAsync(request);
            return task;
        }
    }
}
