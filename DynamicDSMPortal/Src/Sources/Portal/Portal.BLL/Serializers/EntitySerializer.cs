﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Portal.BLL.Serializers
{
    public class EntitySerializer
    {
        private JavaScriptSerializer _jsSerializer = new JavaScriptSerializer();

        public Entity Deserialize(string jsonEntityData, string entityLogicalName,
            IEnumerable<AttributeMetadata> attributesMetadata)
        {
            try
            {
                var dictinary = _jsSerializer.Deserialize<Dictionary<string, object>>(jsonEntityData);

                var entity = new Entity();
                entity.LogicalName = entityLogicalName;

                if(dictinary.ContainsKey("id"))
                {
                    entity.Id = new Guid(dictinary["id"].ToString());
                }
                else if(dictinary.ContainsKey(entityLogicalName + "id"))
                {
                    entity.Id = new Guid(dictinary[entityLogicalName + "id"].ToString());
                }                

                foreach (var prop in dictinary)
                {
                    string lowerKey = prop.Key.ToLower();

                    if (lowerKey == "logicalname" || lowerKey == "id")
                    {
                        continue;
                    }

                    var attributeMetadata = attributesMetadata.FirstOrDefault(a => a.LogicalName == prop.Key);
                    if (attributeMetadata == null)
                    {
                        continue;
                    }

                    switch (attributeMetadata.AttributeType)
                    {
                        case AttributeTypeCode.DateTime:
                            entity[lowerKey] = DateTime.Parse(prop.Value.ToString());
                            break;
                        case AttributeTypeCode.Lookup:
                            entity[lowerKey] = new EntityReference()
                            {
                                Id = new Guid(((Dictionary<string, object>)prop.Value)["Id"].ToString())
                            };
                            break;
                        case AttributeTypeCode.State:
                            break;
                        case AttributeTypeCode.Owner:
                            entity[lowerKey] = new EntityReference()
                            {
                                Id = new Guid(((Dictionary<string, object>)prop.Value)["Id"].ToString())
                            };
                            break;
                        case AttributeTypeCode.Customer:
                            continue;
                            entity[lowerKey] = new EntityReference()
                            {
                                Id = new Guid(((Dictionary<string, object>)prop.Value)["Id"].ToString())
                            };
                            break;
                        case AttributeTypeCode.Picklist:
                            entity[lowerKey] = new OptionSetValue
                            {
                                Value = int.Parse(((Dictionary<string, object>)prop.Value)["Value"].ToString())
                            };
                            break;
                        case AttributeTypeCode.Money:
                            entity[lowerKey] = new Money
                            {
                                Value = decimal.Parse(((Dictionary<string, object>)prop.Value)["Value"].ToString())
                            };
                            break;
                        case AttributeTypeCode.Integer:
                            entity[lowerKey] = int.Parse(prop.Value.ToString());
                            break;
                        case AttributeTypeCode.Decimal:
                            entity[lowerKey] = decimal.Parse(prop.Value.ToString());
                            break;
                        case AttributeTypeCode.Double:
                            entity[lowerKey] = double.Parse(prop.Value.ToString());
                            break;
                        case AttributeTypeCode.Boolean:
                            entity[lowerKey] = bool.Parse(prop.Value.ToString());
                            break;
                        case AttributeTypeCode.Uniqueidentifier:
                            break;
                        default:
                            entity[prop.Key.ToLower()] = prop.Value;
                            break;
                    }

                }

                return entity;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
