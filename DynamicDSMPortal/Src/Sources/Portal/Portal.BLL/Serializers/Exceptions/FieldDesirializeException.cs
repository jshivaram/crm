﻿using System;
using Microsoft.Xrm.Sdk.Metadata;

namespace Portal.BLL.Serializers.Exceptions
{
    public sealed class FieldDesirializeException : Exception
    {
        private readonly string _message;

        public FieldDesirializeException(string message)
        {
            _message = message;
        }

        public FieldDesirializeException(Exception ex)
        {
            _message = ex.Message;
        }

        public override string Message => _message + $"[ FieldName: {FieldName}; EntityName: {EntityName}; FieldType: {FieldTypeName}; Value: {Value}]";

        public string FieldName { get; set; }
        public string EntityName { get; set; }
        public AttributeTypeCode? FieldType { get; set; }
        public string FieldTypeName => FieldType.ToString();
        public object Value { get; set; }
    }
}