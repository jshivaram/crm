﻿using Portal.BLL.Interfaces;
using Portal.DAL.Interfaces;

namespace Portal.BLL.Services
{
    public class AccessService : BaseService, IAccessService
    {
        private readonly IUnitOfWork _uow;
        private readonly ISecurityService _securityService;

        public AccessService(IUnitOfWork uow)
        {
            _uow = uow;
            _securityService = new SecurityService(uow) { UserId = UserId };
        }

        public bool CanDelete(string entityLogicalName)
        {
            bool isAccessAllow = _securityService.CheckAccess(
                    UserId, DAL.Enums.Privilege.Delete, entityLogicalName);

            return isAccessAllow;
        }

        public bool CanCreate(string entityLogicalName)
        {
            bool isAccessAllow = _securityService.CheckAccess(
                    UserId, DAL.Enums.Privilege.Create, entityLogicalName);

            return isAccessAllow;
        }

        public bool CanUpdate(string entityLogicalName)
        {
            bool isAccessAllow = _securityService.CheckAccess(
                    UserId, DAL.Enums.Privilege.Write, entityLogicalName);

            return isAccessAllow;
        }
    }
}
