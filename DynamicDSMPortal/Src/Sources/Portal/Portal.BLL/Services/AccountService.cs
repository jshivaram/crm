﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Portal.BLL.Interfaces;
using Portal.DAL.Interfaces;
using Portal.Models;
using Portal.Models.Account;
using Portal.Models.Site;

namespace Portal.BLL.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _uow;

        public AccountService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<IReadOnlyList<LookupModel>> GetContractorsAsync(string filterValue)
        {
            return await Task.Run(() => GetGetContractors(filterValue));
        }

        public IReadOnlyList<LookupModel> GetGetContractors(string filterValue)
        {
            IReadOnlyList<Entity> contractors = _uow.AccountRepository.GetContractors(filterValue);

            var result = contractors.Select(c => new LookupModel
            {
                Value = c.Id.ToString(),
                Text = c.GetAttributeValue<string>("name")
            }).ToList();

            return result;
        }

        public async Task<AccountInfoForApplication> GetAccountInfoByAccountNumberAsync(string accountNumber)
        {
            return await Task.Run(() => GetAccountInfoByAccountNumber(accountNumber));
        }

        private AccountInfoForApplication GetAccountInfoByAccountNumber(string accountNumber)
        {
            var accountColumns = new[]
            {
                "accountnumber", "ddsm_accounttype", "ddsm_companyname", "ddsm_firstname", "ddsm_lastname",
                "telephone1", "emailaddress1", "address1_line1", "address1_line2", "address1_city", "address1_stateorprovince", "address1_postalcode"
            };

            Entity account = _uow.AccountRepository.GetAccountByAccountNumber(accountNumber, accountColumns);

            if (account == null)
            {
                return null;
            }

            var accountForApplicationModel = new AccountInfoForApplication
            {
                AccountType = account.GetAttributeValue<OptionSetValue>("ddsm_accounttype")?.Value,
                CompanyName = account.GetAttributeValue<string>("ddsm_companyname"),
                FirstName = account.GetAttributeValue<string>("ddsm_firstname"),
                LastName = account.GetAttributeValue<string>("ddsm_lastname"),
                MainPhone = account.GetAttributeValue<string>("telephone1"),
                Email = account.GetAttributeValue<string>("emailaddress1"),
                Street1 = account.GetAttributeValue<string>("address1_line1"),
                Street2 = account.GetAttributeValue<string>("address1_line2"),
                City = account.GetAttributeValue<string>("address1_city"),
                StateProvince = account.GetAttributeValue<string>("address1_stateorprovince"),
                PostalCode = account.GetAttributeValue<string>("address1_postalcode"),
                Site = _GetSiteInfoForApplication(account.Id, account.GetAttributeValue<string>("accountnumber"))
            };

            return accountForApplicationModel;
        }

        private SiteInfoForApplication _GetSiteInfoForApplication(Guid accountId, string accountNumber)
        {
            Entity site = _GetSite(accountId);

            if (site == null)
            {
                return null;
            }

            var siteInfoForApplication = new SiteInfoForApplication
            {
                AccountNumber = accountNumber,
                Address1 = site.GetAttributeValue<string>("ddsm_address1"),
                Address2 = site.GetAttributeValue<string>("ddsm_address2"),
                City = site.GetAttributeValue<string>("ddsm_city"),
                PostalCode = site.GetAttributeValue<string>("ddsm_zip"),
                SqFt = site.GetAttributeValue<int?>("ddsm_sqft"),
                StateProvince = site.GetAttributeValue<string>("ddsm_state")
            };

            return siteInfoForApplication;
        }

        private Entity _GetSite(Guid acountId)
        {
            var columns = new[]
            {
                "ddsm_address1", "ddsm_address2", "ddsm_city", "ddsm_county", "ddsm_state", "ddsm_zip", "ddsm_sqft"
            };
            
            Entity site = _uow.AccountRepository.GetParenteSite(acountId, columns);
            return site;
        }
    }
}