﻿using Portal.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Portal.Models.AttachmentsDocument;
using System.Globalization;
using Microsoft.Xrm.Sdk;
using Portal.DAL.Interfaces;
using Portal.Models.UserSettings;
using Microsoft.Xrm.Sdk.Metadata;
using Portal.BLL.Helpers;

namespace Portal.BLL.Services
{
    public class AnnotationService : BaseService, IAnnotationService
    {
        private readonly IUnitOfWork _uow;
        private const string AnnotationLogicalName = "annotation";

        public AnnotationService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IEnumerable<AttachmentsDocumentViewModel> GetRelatedAttachmentsDocuments(string entityLogicalName, 
            Guid entityId, CultureInfo browserCulture, string browserIanaTimeZone)
        {
            if (string.IsNullOrEmpty(entityLogicalName) || entityId == Guid.Empty)
            {
                return new List<AttachmentsDocumentViewModel>();
            }

            IEnumerable<Entity> annotationEntities = _uow.AnnotationRepository
                .GetRelatedAttachmentsDocumentEntities(entityLogicalName, entityId);
            
            string windowsTimeZoneName = _uow.UserSettingsRepository.GetTimeZone(UserId, browserIanaTimeZone);
            annotationEntities = DateTimeConverter.ConvertUtcByTimezone(annotationEntities, windowsTimeZoneName);

            if (annotationEntities.Count() == 0)
            {
                return new List<AttachmentsDocumentViewModel>();
            }

            DateTimeFormatModel dateTimeFormat = _uow.UserSettingsRepository.GetDateTimeFormat(UserId, browserCulture);
            
            DateTimeFormat fieldDateTimeFormat = _uow.MetaDataRepository
                .GetDateTimeMetadata(AnnotationLogicalName, "createdon");

            string format = dateTimeFormat.DateFormat;
            if (fieldDateTimeFormat == DateTimeFormat.DateAndTime)
            {
                format = dateTimeFormat.DateFormat + " " + dateTimeFormat.TimeFormat;
            }

            IEnumerable<AttachmentsDocumentViewModel> annotations = annotationEntities
                .Select(e => new AttachmentsDocumentViewModel
                {
                    Id = e.Id,
                    CreatedOn = e.GetAttributeValue<DateTime?>("createdon")?.ToString(format),
                    FileName = e.GetAttributeValue<string>("ddsm_name")
                }).ToList();

            return annotations;
        }
    }
}
