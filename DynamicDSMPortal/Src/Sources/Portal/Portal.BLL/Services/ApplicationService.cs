﻿using Portal.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portal.Models.Application;
using Portal.DAL.Interfaces;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk;

using Portal.Models;
using Portal.DAL;
using System.Xml;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace Portal.BLL.Services
{
    public class ApplicationService : IApplicationService
    {
        private IUnitOfWork _uow;

        public ApplicationService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IEnumerable<LookupModel> GetApplicationsFormListForUnregisters()
        {
            var forms =  _uow.FormRepository.GetApplicationsFormListForUnregisters();
            var result = forms.Select(f => new LookupModel
            {
                Value = f.Id.ToString(),
                Text = f.GetAttributeValue<string>("name")
            }).ToList();

            return result;
        }

        public IEnumerable<MeasureTemplateModel> GetMeasureTemplates(IEnumerable<Guid> idList)
        {
            var query = new QueryExpression
            {
                EntityName = "ddsm_measuretemplate",
                ColumnSet = new ColumnSet(
                    "ddsm_name",
                    "ddsm_incentivecostmeasureperunit",
                    "ddsm_kwhunitarysavingsatmeterperunit")
            };

            var idStrArray = idList.Select(id => id.ToString()).ToArray();

            query.Criteria.AddCondition("ddsm_measuretemplateid", ConditionOperator.In, idStrArray);

            IEnumerable<Entity> entities = _uow.EntityRepository.GetAllWithTotalCount(query).Entities;

            var measureTemplates = entities.Select(e => {

                object incentiveUnitObj = null;
                object kwhSavingUnitObj = null;

                e.Attributes.TryGetValue("ddsm_incentivecostmeasureperunit", out incentiveUnitObj);
                e.Attributes.TryGetValue("ddsm_kwhunitarysavingsatmeterperunit", out kwhSavingUnitObj);

                decimal incentiveUnit = incentiveUnitObj != null ? ((Money)incentiveUnitObj).Value : 0;
                decimal kwhSavingUnit = kwhSavingUnitObj != null ? (decimal)kwhSavingUnitObj : 0;

                return new MeasureTemplateModel
                {
                    Id = e.Id,
                    Name = e.Attributes["ddsm_name"].ToString(),
                    IncentiveUnit = incentiveUnit,
                    KwhSavingUnit = kwhSavingUnit
                };
            }).ToList();

            return measureTemplates;
        }

        public IEnumerable<LookupModel> GetMeasureTemplatesForLookup(Guid projectTemplateId)
        {
            var query = new QueryExpression
            {
                EntityName = "ddsm_measuretemplate",
                ColumnSet = new ColumnSet("ddsm_name")
            };

            query.Criteria.AddCondition(
                "ddsm_projecttemplatetomeasuretemplatid", 
                ConditionOperator.Equal, 
                projectTemplateId);
            

            IEnumerable<Entity> entities = _uow.EntityRepository.GetAllWithTotalCount(query).Entities;

            var measureTemplates = entities.Select(e => {
                return new LookupModel
                {
                    Value = e.Id.ToString(),
                    Text = e.Attributes["ddsm_name"].ToString()
                };
            }).ToList();

            return measureTemplates;
        }

        public string GetFormStructureForUnregisterApplication(Guid id)
        {
            try
            {
                Entity form = _uow.FormRepository.GetFormById("ddsm_application", id, FormTypeEnum.FormTypeCreateUpdate);

                if (form == null)
                {
                    throw new ArgumentNullException("form");
                }

                string xml = form.GetAttributeValue<string>("formxml");

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                string jsonText = JsonConvert.SerializeXmlNode(doc);
                return jsonText;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Guid CreateAndReturnId(string jsonEntityData, string logicalName)
        {
            Type entityType = GetEntityType(logicalName);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            Entity entity = (Entity)jsSerializer.Deserialize(jsonEntityData, entityType);

            return _uow.EntityRepository.CreateAndReturnId(entity);
        }


        private Type GetEntityType(string entityLogicalName)
        {
            Type type = AppDomain.CurrentDomain.GetAssemblies()
                .Select(a => a.GetType("XrmEntities." + entityLogicalName, false, true))
                .FirstOrDefault(t => t != null);

            if (type == null)
            {
                throw new Exception("Cannot find entity with this logical name");
            }

            return type;
        }
    }
}
