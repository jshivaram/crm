﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.BLL.Interfaces;
using Portal.DAL.Interfaces;
using Portal.Models.Approve;
using System;
using System.Linq;
using Newtonsoft.Json;
using Portal.BLL.Extensions;
using Portal.Logger;
using System.Threading.Tasks;
using System.Collections.Generic;
using Portal.BLL.Serializers;
using Portal.Models;
using Portal.Models.EntityModels;

namespace Portal.BLL.Services
{
    public class ApproveService : BaseService, IAprroveService
    {
        private readonly IUnitOfWork _uow;
        private readonly ILookupService _lookupService;

        public ApproveService(IUnitOfWork uow, ILookupService lookupService)
        {
            _uow = uow;
            _lookupService = lookupService;
            _lookupService.UserId = UserId;
        }

        public Guid? CreateAndApprove(CreateAndApproveModel entityModel)
        {
            LoggerManager.LogInformation("Enter to CreateAndApprove method");

            if (entityModel == null || string.IsNullOrEmpty(entityModel.LogicalName) ||
                string.IsNullOrEmpty(entityModel.JsonEntityData) ||
                entityModel.MeasureTemplateSelectorForApproval == Guid.Empty)
            {
                LoggerManager.LogInformation("Model is not valid in CreateAndApprove exit");
                return null;
            }

            LoggerManager.LogInformation("Mode is valid in CreateAndApprove");

            var entitySerializer = new EntitySerializer(_uow);

            Entity entity = entitySerializer.Deserialize(entityModel.JsonEntityData, entityModel.LogicalName);

            LoggerManager.LogInformation("Entity serialization in CreateAndApprove is Ok.");

            entity["ddsm_portalowner"] = new EntityReference { Id = UserId };
            entity["ddsm_modifiedbyportaluser"] = new EntityReference { Id = UserId };
            entity["ddsm_modifiedbyportaluserdate"] = DateTime.Now.ToUniversalTime();

            Guid? accountId = _uow.ContactRepository.GetAccountId(UserId);

            if (accountId.HasValue)
            {
                entity["ddsm_partner"] = new EntityReference { Id = accountId.Value };
            }

            Entity measureTemplate = _uow.EntityRepository.Get(
                entityModel.MeasureTemplateSelectorForApproval,
                "ddsm_measuretemplate",
                new ColumnSet("ddsm_measurenumber"));

            if (measureTemplate == null)
            {
                LoggerManager.LogInformation("measureTemplate in ApproveSku method is null; exit");
                return null;
            }

            LoggerManager.LogInformation($"measureTemplate in ApproveSku method has id: {measureTemplate.Id}");

            if (entityModel.LogicalName == "ddsm_modelnumber")
            {
                entity["ddsm_createmnapproval"] = new OptionSetValue(962080001);
                entity["ddsm_measurenumber"] = measureTemplate.GetAttributeValue<string>("ddsm_measurenumber");

                LoggerManager.LogInformation("Model Number in CreateAndApprove method ready to update");
                LoggerManager.LogInformation(JsonConvert.SerializeObject(entity.Attributes));
            }
            else if (entityModel.LogicalName == "ddsm_sku")
            {
                var measureNumber = measureTemplate.GetAttributeValue<string>("ddsm_measurenumber");
                string measureTemplateName = measureTemplate.GetAttributeValue<string>("ddsm_name");

                LoggerManager.LogInformation($"measureName in ApproveSku method is: {measureNumber}");
                LoggerManager.LogInformation($"measureTemplateName in ApproveSku method is: {measureTemplateName}");

                entity["ddsm_createapprovalsku"] = new OptionSetValue(962080000);
                entity["ddsm_linktomeasurenumber"] = measureNumber;
            }

            LoggerManager.LogInformation($"Ready to create {entityModel.LogicalName} in CreateAndApprove");

            return _uow.EntityRepository.CreateAndReturnId(entity);
        }

        public async Task<IEnumerable<LookupModel>> GetMeasureTemplatesAsync(KendoFilters kendoFilters, Guid programOfferingId)
        {
            _lookupService.UserId = UserId;
            return await Task.Run(() => _GetMeasureTemplates(kendoFilters, programOfferingId));
        }

        public IEnumerable<LookupModel> _GetMeasureTemplates(KendoFilters kendoFilters, Guid programOfferingId)
        {
            var result = _lookupService.GetEntitiesForLookup(new LookupFilterModelWithLogicalName
            {
                EntityLogicalName = "ddsm_measuretemplate",
                Filter = kendoFilters
            }, new[] { "ddsm_selectablestartdate", "ddsm_selectableenddate" },
                new Dictionary<string, object> { { "ddsm_programoffering", programOfferingId } });

            return result;
        }

        public async Task<IEnumerable<LookupModel>> GetProgramOfferingsAsync(KendoFilters kendoFilters)
        {
            _lookupService.UserId = UserId;
            return await Task.Run(() => _GetProgramOfferings(kendoFilters));
        }

        public IEnumerable<LookupModel> _GetProgramOfferings(KendoFilters kendoFilters)
        {
            var result = _lookupService.GetEntitiesForLookup(new LookupFilterModelWithLogicalName
            {
                EntityLogicalName = "ddsm_programoffering",
                Filter = kendoFilters
            });

            return result;
        }

        public bool IsModelNumberApproved(Guid id)
        {
            if (id == Guid.Empty)
            {
                return false;
            }

            var query = new QueryExpression("ddsm_modelnumber") { ColumnSet = new ColumnSet("ddsm_createmnapproval") };

            query.Criteria.AddCondition("ddsm_modelnumberid", ConditionOperator.Equal, id);

            Entity mn = _uow.EntityRepository.GetAllWithTotalCount(query).Entities.FirstOrDefault();

            var mnApproval = mn?.GetAttributeValue<OptionSetValue>("ddsm_createmnapproval");
            bool isMnApproved = mnApproval?.Value == 962080001;

            return isMnApproved;
        }

        public bool IsSkuApproved(Guid id)
        {
            if (id == Guid.Empty)
            {
                return false;
            }

            var query = new QueryExpression("ddsm_sku") { ColumnSet = new ColumnSet("ddsm_createapproval") };

            query.Criteria.AddCondition("ddsm_skuid", ConditionOperator.Equal, id);

            Entity sku = _uow.EntityRepository.GetAllWithTotalCount(query).Entities.FirstOrDefault();

            var skuApproval = sku?.GetAttributeValue<OptionSetValue>("ddsm_createapproval");

            bool isSkuApproved = skuApproval?.Value == 962080001;

            return isSkuApproved;
        }

        public void ApproveModelNumber(ApproveModel approveModel)
        {
            LoggerManager.LogInformation("Enter to ApproveModelNumber method");

            if (approveModel.EntityLogicalName != "ddsm_modelnumber"
                || approveModel.EntityId == Guid.Empty
                || approveModel.MeasureTemplateSelectorForApproval == Guid.Empty)
            {
                return;
            }

            LoggerManager.LogInformation("All params in ApproveModelNumber method are valid");

            Entity measureTemplate = _uow.EntityRepository.Get(
                approveModel.MeasureTemplateSelectorForApproval,
                "ddsm_measuretemplate",
                new ColumnSet("ddsm_measurenumber"));

            if (measureTemplate == null)
            {
                LoggerManager.LogInformation("measureTemplate in ApproveModelNumber method is null; exit from method");
                return;
            }

            LoggerManager.LogInformation($"measureTemplate in ApproveModelNumber method has id: {measureTemplate.Id}");

            var modelNumberQuery = new QueryExpression(approveModel.EntityLogicalName)
            {
                ColumnSet = new ColumnSet("ddsm_portalowner")
            };

            modelNumberQuery.Criteria.AddCondition(
                approveModel.EntityLogicalName + "id",
                ConditionOperator.Equal,
                approveModel.EntityId);

            Entity modelNumber = _uow.EntityRepository.GetAllWithTotalCount(modelNumberQuery).Entities.FirstOrDefault();

            if (modelNumber == null)
            {
                LoggerManager.LogInformation("modelNumber in ApproveModelNumber method is null; exit from method");
                return;
            }

            LoggerManager.LogInformation($"modelNumber in ApproveModelNumber method has id: {modelNumber.Id}");

            if (UserId != Guid.Empty && modelNumber.GetAttributeValue<EntityReference>("ddsm_portalowner") == null)
            {
                LoggerManager.LogInformation("update ddsm_portalowner field in ApproveModelNumber method");

                modelNumber["ddsm_portalowner"] = new EntityReference
                {
                    Id = UserId
                };
            }

            modelNumber["ddsm_createmnapproval"] = new OptionSetValue(962080001);
            modelNumber["ddsm_measuretemplateselectorforapproval"] = new EntityReference
            {
                Id = approveModel.MeasureTemplateSelectorForApproval
            };

            modelNumber["ddsm_measurenumber"] = measureTemplate.GetAttributeValue<string>("ddsm_measurenumber");
            modelNumber["ddsm_modifiedbyportaluser"] = new EntityReference { Id = UserId };
            modelNumber["ddsm_modifiedbyportaluserdate"] = DateTime.Now.ToUniversalTime();

            LoggerManager.LogInformation("Model Number in ApproveModelNumber method ready to update");
            LoggerManager.LogInformation(JsonConvert.SerializeObject(modelNumber.Attributes));

            _uow.EntityRepository.Update(modelNumber);
            LoggerManager.LogInformation("modelNumber in ApproveModelNumber method updated");
        }

        public void ApproveSku(ApproveModel approveModel)
        {
            LoggerManager.LogInformation("Enter to ApproveSku method");

            if (approveModel.EntityLogicalName != "ddsm_sku"
              || approveModel.EntityId == Guid.Empty
              || approveModel.MeasureTemplateSelectorForApproval == Guid.Empty)
            {
                return;
            }

            LoggerManager.LogInformation("All params in ApproveSku method are valid");

            Entity measureTemplate = _uow.EntityRepository.Get(
                approveModel.MeasureTemplateSelectorForApproval,
                "ddsm_measuretemplate",
                new ColumnSet("ddsm_name", "ddsm_measurenumber"));

            if (measureTemplate == null)
            {
                LoggerManager.LogInformation("measureTemplate in ApproveSku method is null; exit");
                return;
            }

            LoggerManager.LogInformation($"measureTemplate in ApproveSku method has id: {measureTemplate.Id}");

            var measureNumber = measureTemplate.GetAttributeValue<string>("ddsm_measurenumber");
            string measureTemplateName = measureTemplate.GetAttributeValue<string>("ddsm_name");

            LoggerManager.LogInformation($"measureName in ApproveSku method is: {measureNumber}");
            LoggerManager.LogInformation($"measureTemplateName in ApproveSku method is: {measureTemplateName}");

            if (string.IsNullOrEmpty(measureNumber))
            {
                LoggerManager.LogInformation("measureNumberId in ApproveSku method is empty. Stop method.");
            }

            var sku = new Entity("ddsm_sku", approveModel.EntityId)
            {
                ["ddsm_createapprovalsku"] = new OptionSetValue(962080000),
                ["ddsm_linktomeasurenumber"] = measureNumber,
                ["ddsm_modifiedbyportaluser"] = new EntityReference { Id = UserId },
                ["ddsm_modifiedbyportaluserdate"] = DateTime.Now.ToUniversalTime()
            };

            LoggerManager.LogInformation("sku in ApproveSku method ready to update");
            LoggerManager.LogInformation(JsonConvert.SerializeObject(sku.Attributes));

            _uow.EntityRepository.Update(sku);
            LoggerManager.LogInformation("sku in ApproveSku method updated");
        }
    }
}
