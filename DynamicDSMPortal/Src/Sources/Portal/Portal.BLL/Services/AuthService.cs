﻿using Portal.BLL.Interfaces;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portal.Models.Auth;

using System.Linq.Dynamic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Portal.BLL.Services
{
    public class AuthService : IAuthService
    {
        private readonly IUnitOfWork _uow;

        public AuthService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public bool ContactExists(string login)
        {
            var query = new QueryExpression
            {
                EntityName = "contact"
            };

            query.Criteria.AddCondition("ddsm_login", ConditionOperator.Equal, login);

            Entity contact = _uow.EntityRepository.GetAllWithTotalCount(query)
                .Entities.FirstOrDefault();

            return contact != null;
        }

        public Entity GetContact(string login, string password)
        {
            string hashedPassword = CreateHashedPassword(password);

            var query = new QueryExpression
            {
                EntityName = "contact",
                ColumnSet = new ColumnSet(
                    "ddsm_login",
                    "ddsm_isportaluser",
                    "ddsm_isuser",
                    "emailaddress1",
                    "firstname",
                    "lastname",
                    "fullname"
                    )
            };

            query.Criteria.AddCondition("ddsm_login", ConditionOperator.Equal, login);
            query.Criteria.AddCondition("ddsm_password", ConditionOperator.Equal, hashedPassword);

            Entity contact = _uow.EntityRepository.GetAllWithTotalCount(query)
                .Entities.FirstOrDefault();

            return contact;
        }

        public void Register(RegisterModel registerModel)
        {
            if (registerModel == null)
            {
                throw new ArgumentNullException("Register model cannot be null!");
            }

            if (ContactExists(registerModel.Login))
            {
                throw new Exception("Portal User with this login alredy exists");
            }

            string hashedPassword = CreateHashedPassword(registerModel.Password);


            var contact = new Entity
            {
                LogicalName = "contact"
            };

            contact["firstname"] = registerModel.FirstName;
            contact["lastname"] = registerModel.LastName;
            contact["emailaddress1"] = registerModel.Email;
            contact["ddsm_login"] = registerModel.Login;
            contact["ddsm_password"] = hashedPassword;
            contact["ddsm_isportaluser"] = false;
            contact["ddsm_isuser"] = true;

            _uow.EntityRepository.Create(contact);
        }

        private string CreateHashedPassword(string password)
        {
            var bytes = new UTF8Encoding().GetBytes(password);
            byte[] hashBytes;
            using (var algorithm = new System.Security.Cryptography.SHA512Managed())
            {
                hashBytes = algorithm.ComputeHash(bytes);
            }

            string result = Convert.ToBase64String(hashBytes);
            return result;
        }
    }
}
