﻿using Portal.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Services
{
    public abstract class BaseService : IBaseService
    {
        public Guid UserId { get; set; }
    }
}
