﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using Portal.BLL.Services;
using Portal.BLL.Interfaces;
using Portal.DAL.Interfaces;
using Portal.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using System.Xml.Linq;
using System.Reflection;
using Portal.Models.Metadata;
using Microsoft.Xrm.Sdk.Metadata;
using Portal.Models.EntityClasses;

namespace Portal.BLL.Services
{
    public class ChartService : BaseService, IChartService
    {
        private IUnitOfWork _uow;
        private ISavedQueryService _savedQueryService;

        public ChartService(IUnitOfWork uow)
        {
            _uow = uow;
            _savedQueryService = new SavedQueryService(_uow);
            _savedQueryService.UserId = UserId;
        }

        public DictionaryWithTotalCountModel<string> GetAllData(Guid viewId, string entityLogicalName,
            int countOfEntities, Guid visualizationId)
        {
            if (string.IsNullOrEmpty(entityLogicalName))
            {
                throw new ArgumentNullException("Entity LogicalName is empty");
            }

            if (countOfEntities == 0)
            {
                throw new ArgumentNullException("Count of entities = 0");
            }
            if (viewId == new Guid())
            {
                throw new ArgumentException("viewId is empty");
            }
            string viewXml = GetViewXmlForChart(viewId, visualizationId);
            IEnumerable<string> propsNames = GetPropNamesFromView(viewXml);
            propsNames = propsNames.Distinct().ToList();
            EntitiesWithCountModel entities = GetEntitiesForChartWithCount(viewXml);
            var entitiesDictionaryWithTotalCount = GetEntitiesDictionaryWithTotalCount(entities, propsNames);
            return entitiesDictionaryWithTotalCount;
        }

        public EntitiesWithCountModel GetEntitiesForChartWithCount(string viewXml = null)
        {
            EntitiesWithCountModel entities = _uow.ChartRepository.GetAllWithTotalCount(viewXml);
            return entities;
        }

        public string GetViewXmlForChart(Guid? viewId, Guid visualizationId)
        {
            if (viewId == null)
            {
                throw new NullReferenceException("ViewId cannot be null.");
            }
            var query = new QueryExpression
            {
                EntityName = "savedquery",
                ColumnSet = new ColumnSet("fetchxml")
            };
            query.Criteria.AddCondition("savedqueryid", ConditionOperator.Equal, viewId);
            var view = _uow.EntityRepository.GetAllWithTotalCount(query).Entities.FirstOrDefault();
            if (view == null)
            {
                throw new NullReferenceException("View cannot be null.");
            }

            string fetchXml = view["fetchxml"].ToString();
            XDocument viewDoc = XDocument.Parse(fetchXml);
            XElement fetch = viewDoc.Element("fetch");
            XElement entity = fetch.Element("entity");

            var visualization = _uow.ChartRepository.GetVisualizationData(visualizationId);
            string visualizationXml = visualization["datadescription"].ToString();
            XDocument visualDoc = XDocument.Parse(visualizationXml);
            XElement dataDef = visualDoc.Element("datadefinition");
            XElement fetchCol = dataDef.Element("fetchcollection");
            XElement dataFetch = fetchCol.Element("fetch");
            XElement dataEntity = dataFetch.Element("entity");

            XElement dataEntityFilter = dataEntity.Element("filter");
            XElement entityFilter = entity.Element("filter");
            if (dataEntityFilter != null && entityFilter == null)
            {
                entity.Add(dataEntityFilter);
            }
            if (dataEntityFilter != null && entityFilter != null)
            {
                entity.Element("filter").Add(dataEntityFilter);
            }

            IEnumerable<XElement> fetchAttributes = entity.Elements("attribute");
            var fetchNames = fetchAttributes.Select(f => f.Attribute("name").Value).ToList();
            IEnumerable<XElement> dataDefinitionAttributes = dataEntity.Elements("attribute");
            var dataDefinitionNames = dataDefinitionAttributes
                    .Select(d => d.Attribute("name").Value).ToList();
            foreach (var name in dataDefinitionNames)
            {
                if (!fetchNames.Contains(name))
                {
                    var xELem = new XElement("attribute");
                    var xAttr = new XAttribute("name", name);
                    xELem.Add(xAttr);
                    entity.Add(xELem);
                }
            }
            fetchAttributes = entity.Elements("attribute");
            fetchNames = fetchAttributes.Select(f => f.Attribute("name").Value).ToList();
            foreach (var name in fetchNames)
            {
                if(!dataDefinitionNames.Contains(name))
                {
                    entity.Elements("attribute").Where(e => e.Attribute("name").Value == name).Remove();         
                }
            }
            IEnumerable<XElement> linkEntities = dataEntity.Elements("link-entity");
            XElement check = linkEntities.FirstOrDefault();
            if (check != null)
            {
                foreach (var link in linkEntities)
                {
                    var xLinkName = link.Attribute("name").Value;
                    var xLinkFrom = link.Attribute("from").Value;
                    var xLinkTo = link.Attribute("to").Value;
                    var xLinkType = link.Attribute("link-type").Value;
                    IEnumerable<XElement> xLinkAttributes = link.Elements("attribute");
                    var xLinkAttrNames = xLinkAttributes.Select(d => d.Attribute("name").Value).ToList();

                    var xLink = new XElement("link-entity");
                    var xName = new XAttribute("name", xLinkName);
                    var xFrom = new XAttribute("from", xLinkFrom);
                    var xTo = new XAttribute("to", xLinkTo);
                    var xType = new XAttribute("link-type", xLinkType);
                    xLink.Add(xName);
                    xLink.Add(xFrom);
                    xLink.Add(xTo);
                    xLink.Add(xType);
                    foreach (var name in xLinkAttrNames)
                    {
                        var xElem = new XElement("attribute");
                        var xAttr = new XAttribute("name", name);
                        xElem.Add(xAttr);
                        xLink.Add(xElem);
                    }

                    IEnumerable<XElement> linkFilters = link.Elements("filter");
                    foreach (var filt in linkFilters) {
                        xLink.Add(filt);
                    }
                    entity.Add(xLink);
                }
            }
            viewDoc.Element("fetch").RemoveNodes();
            viewDoc.Element("fetch").Add(entity);
            fetchXml = viewDoc.ToString();
            return fetchXml;
        }

        private IEnumerable<string> GetPropNamesFromView(string viewXml)
        {
            ISavedQueryService viewService = new SavedQueryService(_uow);
            viewService.UserId = UserId;
            var propsNames = viewService.GetPropNamesFromView(viewXml);
            return propsNames;
        }

        private QueryExpression FormGetEntitiesQueryExpressionWithCount(string entityLogicalName,
            IEnumerable<string> propsNames,
            int count = 0,
            string viewXml = null,
            string searchText = null)
        {
            QueryExpression query = new QueryExpression(entityLogicalName.ToLower())
            {
                PageInfo = new PagingInfo
                {
                    Count = count,
                    PageNumber = 1,
                    ReturnTotalRecordCount = true
                },

            };
            Type type = GetEntityType(entityLogicalName);
            query.ColumnSet = FormColumnSetByProps(propsNames);
            query.ColumnSet = new ColumnSet(true);
            return query;
        }

        private Type GetEntityType(string entityLogicalName)
        {
            Type type = AppDomain.CurrentDomain.GetAssemblies()
                .Select(a => a.GetType("XrmEntities." + entityLogicalName, false, true))
                .FirstOrDefault(t => t != null);
            if (type == null)
            {
                throw new Exception("Cannot find entity with this logical name");
            }
            return type;
        }

        private string GetOrderEntityFieldName(XElement order)
        {
            string orderEntityFieldName = order?.Attribute("attribute")?.Value;
            return orderEntityFieldName;
        }

        private XElement GetOrderEntityAttribute(string viewXml)
        {
            XElement xelem = XElement.Parse(viewXml);
            XElement firstEntity = xelem.Elements("entity").FirstOrDefault();
            IEnumerable<XElement> attributes = firstEntity.Elements("attribute");
            List<string> propsNames = new List<string>();
            XElement order = firstEntity.Element("order");
            return order;
        }

        private DictionaryWithTotalCountModel<string> GetEntitiesDictionaryWithTotalCount(
            EntitiesWithCountModel entities,
            IEnumerable<string> propsNames)
        {
            if (entities == null)
            {
                throw new ArgumentNullException("entities");
            }
            DictionaryWithTotalCountModel<string> result = new DictionaryWithTotalCountModel<string>
            {
                TotalCount = entities.Count
            };
            List<IDictionary<string, string>> dictionaryList = new List<IDictionary<string, string>>();
            SavedQueryService savedQueryService = new SavedQueryService(_uow)
            {
                UserId = UserId
            };

            foreach (Entity entity in entities.Entities)
            {
                IDictionary<string, string> bufDictionary = new Dictionary<string, string>();
                AttributeCollection attributes = entity.Attributes;
                bufDictionary = CreateDictionary(entity);
                if (propsNames.Count() > 0)
                {
                    foreach (string propName in propsNames)
                    {
                        if (!bufDictionary.Keys.Contains(propName.ToLower()))
                        {
                            bufDictionary.Add(propName.ToLower(), "");
                        }
                    }
                }
                dictionaryList.Add(bufDictionary);
            }
            result.Items = dictionaryList;
            return result;
        }

        public IDictionary<string, string> GetVisualizationData(Guid visualizationId)
        {
            Entity chartVisualization = _uow.ChartRepository.GetVisualizationData(visualizationId);
            AttributeCollection visualAttributes = chartVisualization.Attributes;
            IDictionary<string, string> bufDictionary = new Dictionary<string, string>();
            bufDictionary = CreateDictionary(chartVisualization);
            return bufDictionary;
        }

        private IDictionary<string, string> CreateDictionary(Entity visualization)
        {
            AttributeCollection visualAttributes = visualization.Attributes;
            IDictionary<string, string> bufDictionary = new Dictionary<string, string>();

            foreach (var attribute in visualAttributes)
            {
                if (attribute.Value is EntityReference)
                {
                    EntityReference entityReferenceBuf = attribute.Value as EntityReference;
                    try
                    {
                        string bufValue = "{" + $"'Id':'{entityReferenceBuf.Id.ToString()}', 'Logicalname':'{ entityReferenceBuf.LogicalName}','Name':'{entityReferenceBuf.Name}'" + "}";

                        bufDictionary.Add(
                            attribute.Key.ToLower(),
                            bufValue);
                    }
                    catch (Exception)
                    {
                        bufDictionary.Add(
                           attribute.Key.ToLower(),
                           entityReferenceBuf.Name);
                    }
                }
                else if (attribute.Value is OptionSetValue)
                {
                    bufDictionary.Add(
                        attribute.Key.ToLower(),
                        visualization.FormattedValues[attribute.Key]);
                }
                else if (attribute.Value is Money)
                {
                    bufDictionary.Add(
                       attribute.Key.ToLower(),
                       ((Money)attribute.Value).Value.ToString());
                }
                else if (attribute.Value is DateTime?)
                {
                    var tempValue = visualization.FormattedValues[attribute.Key];
                    if (tempValue == null) { 
                        bufDictionary.Add(
                           attribute.Key.ToLower(),
                           ((DateTime)attribute.Value).ToString());
                    }
                    else
                    {
                        bufDictionary.Add(
                            attribute.Key.ToLower(),
                            visualization.FormattedValues[attribute.Key]);
                    }

                }
                else if (attribute.Value is AliasedValue)
                {
                    AliasedValue alias = attribute.Value as AliasedValue;
                    bufDictionary.Add(
                        alias.AttributeLogicalName.ToLower(),
                        alias.Value.ToString());
                }
                else
                {
                    bufDictionary.Add(attribute.Key.ToLower(), attribute.Value.ToString());
                }
            }

            return bufDictionary;
        }

        private ColumnSet FormColumnSetByProps(IEnumerable<string> propsNames)
        {
            return propsNames.Count() > 0 ? new ColumnSet(propsNames.ToArray()) : new ColumnSet(true);
        }

        public IDictionary<string, string> GetSavedQueryForChart(Guid? viewId)
        {
            if (viewId == null)
            {
                throw new NullReferenceException("ViewId cannot be null.");
            }
            var query = new QueryExpression
            {
                EntityName = "savedquery",
                ColumnSet = new ColumnSet("savedqueryid","name")
            };
            query.Criteria.AddCondition("savedqueryid", ConditionOperator.Equal, viewId);
            Entity savedQuery = _uow.EntityRepository.GetAllWithTotalCount(query).Entities.FirstOrDefault();
            if (savedQuery == null)
            {
                throw new NullReferenceException("View cannot be null.");
            }
            AttributeCollection visualAttributes = savedQuery.Attributes;
            IDictionary<string, string> bufDictionary = new Dictionary<string, string>();
            bufDictionary = CreateDictionary(savedQuery);
            return bufDictionary;
        }

        public IEnumerable<AttributeMetadataModel> GetAttributesMetadataForChart(string entityLogicalName, 
            Guid viewId, Guid visualizationId)
        {
            var props = new List<string>();
            Entity view = _uow.EntityRepository.Get(viewId, "savedquery", new ColumnSet("fetchxml"));
            string fetchXml = GetViewXmlForChart(viewId, visualizationId);
            if (view.Attributes.Contains("fetchxml"))
            {
                props = _savedQueryService.GetPropNamesFromView(fetchXml).ToList();
            }
            else
            {
                return null;
            }

            IEnumerable<AttributeMetadata> attributesMetadata = _uow.MetaDataRepository
                .GetAttributesMetadata(entityLogicalName, props);
            IEnumerable<AttributeMetadataModel> result = attributesMetadata.Select(am => new AttributeMetadataModel
            {
                DisplayName = am.DisplayName?.LocalizedLabels?.FirstOrDefault()?.Label,
                Editable = am.RequiredLevel.CanBeChanged,
                LogicalName = am.LogicalName,
                Required = am.RequiredLevel?.Value,
                Type = am.AttributeTypeName?.Value
            });
            return result;
        }
    }
}