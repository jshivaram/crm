﻿using System.Configuration;
using Portal.BLL.Interfaces;

namespace Portal.BLL.Services
{
    public class CrmService : ICrmService
    {
        private readonly string _connectionString;

        public CrmService()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["CrmConnection"].ConnectionString;
        }


        public string GetUrl()
        {
            var conn = _connectionString.Split(';');
            var url = conn[0];
            return GetValue(url);
        }

        private static string GetValue(string property)
        {
            return property.Split('=')[1];
        }
    }
}