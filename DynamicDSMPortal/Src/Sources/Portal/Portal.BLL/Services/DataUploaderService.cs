﻿using Microsoft.Xrm.Sdk;
using Portal.BLL.Interfaces;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Services
{
    public class DataUploaderService : BaseService, IDataUploaderService
    {
        private readonly IUnitOfWork _uow;

        public DataUploaderService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public void CreateDataUploaderRecord(int typeOfUploadedData, byte[] file, string fileName)
        {
            Entity adminData = _uow.AdminDataRepository.GetAdminDataForDataUploader();

            if(adminData == null)
            {
                throw new ArgumentNullException("adminData");
            }

            Entity dataUploaderConfiguration = _uow.DataUploaderRepository.GetDataUploaderConfiguration(typeOfUploadedData);

            if(dataUploaderConfiguration == null)
            {
                throw new ArgumentNullException("dataUploaderConfiguration");
            }

            _uow.DataUploaderRepository.CreateDataUploaderRecord(typeOfUploadedData, dataUploaderConfiguration.ToEntityReference(), adminData, file, fileName);
        }
    }
}
