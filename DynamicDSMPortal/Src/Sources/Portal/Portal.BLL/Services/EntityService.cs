﻿using Portal.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Portal.Models.GridModels;
using Portal.DAL.Interfaces;
using System.Reflection;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using Microsoft.Xrm.Client;
using System.Xml.Linq;
using Portal.BLL.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using Portal.Models;
using Microsoft.Xrm.Sdk.Query;

using Portal.Models.EntityModels;
using Microsoft.Xrm.Sdk.Metadata;
using Portal.Models.Navigation;
using Portal.BLL.Serializers;
using Portal.Models.EntityClasses;

namespace Portal.BLL.Services
{
    public class EntityService : BaseService, IEntityService
    {
        private readonly IUnitOfWork _uow;
        private readonly ISecurityService _securityService;
        private EntitySerializer _entitySerializer;

        public EntityService(IUnitOfWork uow)
        {
            _uow = uow;
            _securityService = new SecurityService(_uow);
            _entitySerializer = new EntitySerializer();
        }

        public IEnumerable<NavigationEntityModel> GetEntitiesForNavigation()
        {
            List<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId).ToList();
            var columns = new ColumnSet(
                "ddsm_entitylogicalname",
                "ddsm_name",
                "ddsm_class_name",
                "ddsm_navigation_name",
                "ddsm_numberinmenu");

            Guid? defaultPortalTeamId = _uow.ContactRepository.GetDefaultTeamId(UserId);
            Guid? defaulTeamId = defaultPortalTeamId.HasValue ?
                _uow.PortalConfigurationRepository.GetTeamIdByPortalTeamId(defaultPortalTeamId.Value)
                : null;
            
            if(defaulTeamId != null)
            {
                var entities = new List<Entity>();

                teamsIdList.Remove(defaulTeamId.Value);

                IEnumerable<Entity> entitiesFromDefaultTeam = _uow.PortalConfigurationRepository
                    .GetPortalEntitiesListForNavigation(defaulTeamId.Value, columns);

                IEnumerable<Entity> numberedEntitiesFromDefaultTeam = entitiesFromDefaultTeam
                    .Where(e => e.Attributes.Keys.Contains("ddsm_numberinmenu"))
                    .OrderBy(e => e.GetAttributeValue<int?>("ddsm_numberinmenu"))
                    .ToList();

                IEnumerable<Entity> notNumberedEntitiesFromDefaultTeam = entitiesFromDefaultTeam
                    .Where(e => !e.Attributes.Keys.Contains("ddsm_numberinmenu"))
                    .OrderBy(e => e.GetAttributeValue<string>("ddsm_navigation_name"))
                    .ThenBy(e => e.GetAttributeValue<string>("ddsm_name"))
                    .ThenBy(e => e.GetAttributeValue<string>("ddsm_entitylogicalname"))
                    .ToList();

                IEnumerable<Entity> entitiesNotDefaultTeams = _uow.PortalConfigurationRepository
                    .GetPortalEntitiesListForNavigation(teamsIdList, columns);

                IEnumerable<Entity> numberedEntitiesFromNotDefaultTeam = entitiesNotDefaultTeams
                    .Where(e => e.Attributes.Keys.Contains("ddsm_numberinmenu"))
                    .OrderBy(e => e.GetAttributeValue<int?>("ddsm_numberinmenu"))
                    .ToList();

                IEnumerable<Entity> notNumberedEntitiesFromNotDefaultTeam = entitiesNotDefaultTeams
                    .Where(e => !e.Attributes.Keys.Contains("ddsm_numberinmenu"))
                    .OrderBy(e => e.GetAttributeValue<string>("ddsm_navigation_name"))
                    .ThenBy(e => e.GetAttributeValue<string>("ddsm_name"))
                    .ThenBy(e => e.GetAttributeValue<string>("ddsm_entitylogicalname"))
                    .ToList();

                entities.AddRange(numberedEntitiesFromDefaultTeam);
                entities.AddRange(numberedEntitiesFromNotDefaultTeam);
                entities.AddRange(notNumberedEntitiesFromDefaultTeam);
                entities.AddRange(notNumberedEntitiesFromNotDefaultTeam);

                return entities.MapPortalEntitiesToMenuItems();
            }            
            else
            {
                IEnumerable<Entity> entities = _uow.PortalConfigurationRepository
                    .GetPortalEntitiesListForNavigation(teamsIdList, columns);

                entities = entities
                        .OrderByDescending(e => e.GetAttributeValue<int?>("ddsm_numberinmenu").HasValue)
                        .ThenBy(e => e.GetAttributeValue<int?>("ddsm_numberinmenu"))
                        .OrderBy(e => e.GetAttributeValue<string>("ddsm_navigation_name"))
                        .ThenBy(e => e.GetAttributeValue<string>("ddsm_name"))
                        .ThenBy(e => e.GetAttributeValue<string>("ddsm_entitylogicalname"))
                        .ToList();

                return entities.MapPortalEntitiesToMenuItems();
            }
        }

        public IEnumerable<LookupModel> GetEntitiesForGridLookupByField(string entityLogicalName, string fieldName)
        {
            if (string.IsNullOrEmpty(entityLogicalName))
            {
                throw new ArgumentNullException("entityLogicalName");
            }

            if (string.IsNullOrEmpty(fieldName))
            {
                throw new ArgumentNullException("fieldName");
            }

            var lookupAttributeMetadata = _uow.MetaDataRepository.GetAttributeMetadata
                (entityLogicalName, fieldName).AttributeMetadata as LookupAttributeMetadata;

            string lookupEntityLogicalName = lookupAttributeMetadata.Targets.FirstOrDefault();


            var query = new QueryExpression
            {
                EntityName = "savedquery",
                ColumnSet = new ColumnSet("fetchxml")
            };

            query.Criteria.AddCondition(
                "returnedtypecode",
                ConditionOperator.Equal,
                lookupEntityLogicalName.ToLower());

            query.Criteria.AddCondition(
                "querytype",
                ConditionOperator.Equal,
                64);

            string viewXml = _uow.EntityRepository.GetAllWithTotalCount(query).Entities.First()["fetchxml"].ToString();

            List<string> propsNames = GetPropNamesFromView(viewXml).ToList();
            string mainPropName = propsNames.FirstOrDefault();

            if (string.IsNullOrEmpty(mainPropName))
            {
                foreach (string propName in propsNames)
                {
                    if (!string.IsNullOrEmpty(propName))
                    {
                        if (propName.ToLower() == entityLogicalName.ToLower() + "id")
                        {
                            continue;
                        }

                        mainPropName = propName;
                        break;
                    }
                }
            }

            if (string.IsNullOrEmpty(mainPropName))
            {
                throw new ArgumentNullException("mainPropName");
            }

            IEnumerable<Entity> entities = _uow.EntityRepository.GetAllWithTotalCount(new QueryExpression
            {
                EntityName = lookupEntityLogicalName,
                ColumnSet = new ColumnSet(mainPropName)
            }).Entities.ToList();

            var result = new List<LookupModel>();
            string idFieldName = lookupEntityLogicalName + "id";

            foreach (Entity entity in entities)
            {
                string text = entity.Attributes[mainPropName].ToString();

                string value = "{" + $"'Id':'{entity.Id.ToString()}', 'Logicalname':'{ entity.LogicalName}', 'Name':'{text}'" + "}"; /*JsonConvert.SerializeObject(bufValueDictionary)*/;

                result.Add(new LookupModel
                {
                    Text = text,
                    Value = value
                });
            }

            return result;
        }

        public Entity Get(Guid id, string entityLogicalName)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentException("Invalid Entity id");
            }

            if (string.IsNullOrEmpty(entityLogicalName)
                || string.IsNullOrWhiteSpace(entityLogicalName))
            {
                throw new ArgumentNullException("Empty entity logical name");
            }

            Entity entity = _uow.EntityRepository.Get(id, entityLogicalName);
            return entity;
        }

        public DictionaryWithTotalCountModel<string> GetEntitiesDictionaryListWithCount(string entityLogicalName,
          GridPagingModel pagination,
          Guid viewId,
          string searchText = null)
        {
            if (string.IsNullOrEmpty(entityLogicalName))
            {
                throw new ArgumentNullException("Entity LogicalName is empty");
            }

            if (pagination == null)
            {
                throw new ArgumentNullException("Pagination model is empty");
            }

            if (viewId == new Guid())
            {
                throw new ArgumentException("viewId is empty");
            }

            string viewXml = GetViewXml(viewId);

            IEnumerable<string> propsNames = GetPropNamesFromView(viewXml);
            propsNames = propsNames.Distinct().ToList();

            EntitiesWithCountModel entities = GetEntitiesWithCount(
                entityLogicalName, propsNames, pagination, viewXml, searchText);

            var entitiesDictionaryWithTotalCount = GetEntitiesDictionaryWithTotalCount(entities, propsNames);

            return entitiesDictionaryWithTotalCount;
        }

        public EntitiesWithCountModel GetEntitiesWithCount(string entityLogicalName,
           IEnumerable<string> propsNames,
           GridPagingModel pagination = null,
           string viewXml = null,
           string searchText = null)
        {
            bool isAccessAllow = _securityService.CheckAccess(
              UserId, DAL.Enums.Privilege.Read, entityLogicalName);

            if (!isAccessAllow)
            {
                throw new Exception("Access is denied");
            }

            QueryExpression query = FormGetEntitiesQueryExpressionWithCount(
                entityLogicalName,
                propsNames,
                pagination,
                viewXml,
                searchText);

            //for e1
            AddE1RulesToQuery(query);

            EntitiesWithCountModel entities = _uow.EntityRepository.GetAllWithTotalCount(query);

            return entities;
        }

        private QueryExpression FormGetEntitiesQueryExpressionWithCount(string entityLogicalName,
           IEnumerable<string> propsNames,
           GridPagingModel pagination = null,
           string viewXml = null,
           string searchText = null)
        {
            var query = new QueryExpression(entityLogicalName.ToLower())
            {
                PageInfo = new PagingInfo
                {
                    Count = pagination.Take,
                    PageNumber = pagination.Page,
                    ReturnTotalRecordCount = true
                },

            };
            query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

            IEnumerable<string> stringSearchFields = GetStringSearchFields(propsNames, entityLogicalName);

            if (!string.IsNullOrEmpty(searchText) && !string.IsNullOrWhiteSpace(searchText))
            {
                FilterExpression filter = FormSearchFilterExpression(stringSearchFields, searchText);
                query.Criteria.AddFilter(filter);
            }

            query.ColumnSet = FormColumnSetByProps(propsNames);

            if (pagination.Sort != null)
            {
                var sortParam = pagination.Sort.FirstOrDefault();
                string direction = !string.IsNullOrEmpty(sortParam?.Dir?.ToLower())
                                    ? sortParam?.Dir?.ToLower()
                                    : "asc";

                query.AddOrder(
                    sortParam?.Field?.ToLower(),
                    direction == "asc" ? OrderType.Ascending : OrderType.Descending);

            }
            else
            {
                XElement order = GetOrderEntityAttribute(viewXml);
                string orderEntityFieldName = GetOrderEntityFieldName(order);

                if (!string.IsNullOrEmpty(orderEntityFieldName)
                    && !string.IsNullOrWhiteSpace(orderEntityFieldName))
                {
                    query.AddOrder(orderEntityFieldName, OrderType.Ascending);
                }
            }

            return query;
        }

        private DictionaryWithTotalCountModel<string> GetEntitiesDictionaryWithTotalCount(
            EntitiesWithCountModel entities,
            IEnumerable<string> propsNames)
        {
            if (entities == null)
            {
                throw new ArgumentNullException("entities");
            }

            DictionaryWithTotalCountModel<string> result = new DictionaryWithTotalCountModel<string>
            {
                TotalCount = entities.Count
            };

            List<IDictionary<string, string>> dictionaryList = new List<IDictionary<string, string>>();

            SavedQueryService savedQueryService = new SavedQueryService(_uow)
            {
                UserId = UserId
            };

            foreach (Entity entity in entities.Entities)
            {

                IDictionary<string, string> bufDictionary = new Dictionary<string, string>();
                AttributeCollection attributes = entity.Attributes;

                foreach (var attribute in attributes)
                {
                    if (attribute.Value is EntityReference)
                    {
                        EntityReference entityReferenceBuf = attribute.Value as EntityReference;

                        try
                        {
                            Guid dedaultViewIdForEntityRefence = savedQueryService.GetLookupViewId(entityReferenceBuf.LogicalName);

                            string bufValue = "{" + $"'Id':'{entityReferenceBuf.Id.ToString()}', 'Logicalname':'{ entityReferenceBuf.LogicalName}','Name':'{entityReferenceBuf.Name}'" + "}";

                            bufDictionary.Add(
                                attribute.Key.ToLower(),
                                bufValue);
                        }
                        catch (Exception)
                        {
                            bufDictionary.Add(
                               attribute.Key.ToLower(),
                               entityReferenceBuf.Name);
                        }
                    }
                    else if (attribute.Value is OptionSetValue)
                    {
                        bufDictionary.Add(
                            attribute.Key.ToLower(),
                            entity.FormattedValues[attribute.Key]);
                    }
                    else if (attribute.Value is Money)
                    {
                        bufDictionary.Add(
                           attribute.Key.ToLower(),
                           ((Money)attribute.Value).Value.ToString());
                    }
                    else if (attribute.Value is DateTime?)
                    {
                        bufDictionary.Add(
                           attribute.Key.ToLower(),
                           ((DateTime)attribute.Value).ToString());
                    }
                    else
                    {
                        bufDictionary.Add(attribute.Key.ToLower(), attribute.Value.ToString());
                    }
                }

                if (propsNames.Count() > 0)
                {
                    foreach (string propName in propsNames)
                    {
                        if (!bufDictionary.Keys.Contains(propName.ToLower()))
                        {
                            bufDictionary.Add(propName.ToLower(), "");
                        }
                    }
                }

                dictionaryList.Add(bufDictionary);
            }

            result.Items = dictionaryList;

            return result;
        }


        private FilterExpression FormSearchFilterExpression(IEnumerable<string> stringSearchFields, string searchText)
        {
            FilterExpression filter = new FilterExpression();
            filter.FilterOperator = LogicalOperator.Or;

            foreach (string searchField in stringSearchFields)
            {
                filter.AddCondition(
                      stringSearchFields.First().ToLower(),
                      ConditionOperator.Like,
                      "%" + searchText + "%");
            }

            return filter;
        }

        private ColumnSet FormColumnSetByProps(IEnumerable<string> propsNames)
        {
            return propsNames.Count() > 0 ? new ColumnSet(propsNames.ToArray()) : new ColumnSet(true);
        }

        public DictionaryWithTotalCountModel<string> GetRelatedEntitiesDictionaryListWithTotalCount(
            Guid entityId, Guid viewId, string entityLogicalName,
            string relatedEntityName, string relationshipShcemeName,
            GridPagingModel pagination = null,
            string searchText = null)
        {
            string viewXml = GetViewXml(viewId);

            IEnumerable<string> propsNames = GetPropNamesFromView(viewXml);
            propsNames = propsNames.Distinct().ToList();

            EntitiesWithCountModel entities = GetRelatedEntitiesWithTotalCount(
                entityId, propsNames, viewXml, entityLogicalName,
                relatedEntityName, relationshipShcemeName, pagination, searchText);

            if(entityLogicalName == "ddsm_measuretemplate" && relatedEntityName == "ddsm_modelnumberapproval")
            {
                entities.Entities = entities.Entities.GetOnlyApprovedModelNumberApprovals();
            }

            var result = GetEntitiesDictionaryWithTotalCount(entities, propsNames);

            return result;
        }

        private EntitiesWithCountModel GetRelatedEntitiesWithTotalCount(
            Guid entityId, IEnumerable<string> propsNames, string viewXml, string entityLogicalName,
            string relatedEntityName, string relationshipShcemeName,
            GridPagingModel pagination = null,
            string searchText = null)
        {

            QueryExpression query = FormGetEntitiesQueryExpressionWithCount(
                relatedEntityName, propsNames, pagination, viewXml, searchText);

            //for e1
            AddE1RulesToQuery(query);

            EntitiesWithCountModel relatedEntitiesWithCount = _uow.EntityRepository.GetRelatedEntitiesWithCount(
                query, relationshipShcemeName, new EntityReference(entityLogicalName, entityId));

            return relatedEntitiesWithCount;
        }

        public IEnumerable<object> GetRelatedEntities(Guid entityId, Guid viewId, 
            string entityLogicalName, string relatedEntityName, string relationshipShcemeName)
        {

            Entity entity = new Entity
            {
                Id = entityId,
                LogicalName = entityLogicalName
            };

            string viewXml = GetViewXml(viewId);

            IEnumerable<string> propsNames = GetPropNamesFromView(viewXml);
            propsNames = propsNames.Distinct().ToList();

            IEnumerable<Entity> entities = _uow.EntityRepository.GetRelatedEntities(entity, relatedEntityName, relationshipShcemeName);
            IEnumerable<object> result = entities.GetObjectsByPropNames(propsNames);

            return result;
        }

        public IEnumerable<LookupModel> GetEntitiesForLookup(Guid viewId)
        {
            string viewXml = GetViewXml(viewId);

            XElement mainElement = XElement.Parse(viewXml);
            XElement entityElement = mainElement.Element("entity");
            var nameAttribute = entityElement.Attribute("name");
            string entityLogicalName = nameAttribute.Value;

            List<string> propsNames = GetPropNamesFromView(viewXml).ToList();

            var query = new QueryExpression
            {
                EntityName = entityLogicalName,
                ColumnSet = new ColumnSet(true)
            };

            IEnumerable<Entity> entities = _uow.EntityRepository.GetAllWithTotalCount(query).Entities;
           
            string mainPropName = null;

            if(propsNames.Contains("ddsm_name"))
            {
                mainPropName = "ddsm_name";
            }

            if(propsNames.Contains("name"))
            {
                mainPropName = "name";
            }

            if (string.IsNullOrEmpty(mainPropName))
            {
                foreach (string propName in propsNames)
                {
                    if (!string.IsNullOrEmpty(propName))
                    {
                        if (propName.ToLower() == entityLogicalName.ToLower() + "id")
                        {
                            continue;
                        }

                        mainPropName = propName;
                        break;
                    }
                }
            }

            if (string.IsNullOrEmpty(mainPropName))
            {
                throw new ArgumentNullException("mainPropName");
            }

            List<LookupModel> result = new List<LookupModel>();
            string idFieldName = entityLogicalName + "id";

            foreach (Entity entity in entities)
            {

                object text = null;
                entity.Attributes.TryGetValue(mainPropName, out text);
                string value = entity.Attributes[idFieldName].ToString();

                result.Add(new LookupModel
                {
                    Text = text?.ToString(),
                    Value = value
                });
            }

            return result;
        }

        public IEnumerable<LookupModel> GetEntitiesForLookup(string entityLogicalName)
        {
            var queryForView = new QueryExpression
            {
                EntityName = "savedquery",
                ColumnSet = new ColumnSet("fetchxml")
            };

            queryForView.Criteria.AddCondition(
                "returnedtypecode",
                ConditionOperator.Equal,
                entityLogicalName.ToLower());

            queryForView.Criteria.AddCondition(
                "querytype",
                ConditionOperator.Equal,
                64);

            string viewXml = _uow.EntityRepository.GetAllWithTotalCount(queryForView)
                .Entities.First()["fetchxml"]
                .ToString();

            var query = new QueryExpression
            {
                EntityName = entityLogicalName,
                ColumnSet = new ColumnSet(true)
            };

            IEnumerable<Entity> entities = _uow.EntityRepository.GetAllWithTotalCount(query).Entities;
            List<string> propsNames = GetPropNamesFromView(viewXml).ToList();

            string mainPropName = null;

            if (propsNames.Contains("ddsm_name"))
            {
                mainPropName = "ddsm_name";
            }

            if (propsNames.Contains("name"))
            {
                mainPropName = "name";
            }

            if (string.IsNullOrEmpty(mainPropName))
            {
                foreach (string propName in propsNames)
                {
                    if (!string.IsNullOrEmpty(propName))
                    {
                        if (propName.ToLower() == entityLogicalName.ToLower() + "id")
                        {
                            continue;
                        }

                        mainPropName = propName;
                        break;
                    }
                }
            }

            if (string.IsNullOrEmpty(mainPropName))
            {
                throw new ArgumentNullException("mainPropName");
            }

            List<LookupModel> result = new List<LookupModel>();
            string idFieldName = entityLogicalName + "id";

            foreach (Entity entity in entities)
            {

                string text = entity.Attributes[mainPropName].ToString();
                string value = entity.Attributes[idFieldName].ToString();

                result.Add(new LookupModel
                {
                    Text = text,
                    Value = value
                });
            }

            return result;
        }

        public Entity GetRelatedEntity(string entityLogicalName, string relationshipShcemeName)
        {
            var entity = new Entity(entityLogicalName);
            var result = entity.GetRelatedEntity<Entity>(relationshipShcemeName);

            return result;
        }

        private void Create(Entity entity)
        {
            Entity findedEntity = _uow.EntityRepository.Get(entity.Id, entity.LogicalName);

            if (findedEntity != null)
            {
                throw new Exception(string.Format("Entity {0} alredy exists.", entity.LogicalName));
            }

            _uow.EntityRepository.Create(entity);
        }

        public void Create(string jsonEntityData, string logicalName)
        {
            if (string.IsNullOrEmpty(jsonEntityData))
            {
                throw new ArgumentNullException("jsonEntityData");
            }

            if (string.IsNullOrEmpty(logicalName))
            {
                throw new ArgumentNullException("logicalName");
            }

            try
            {
                var attributesMetadata = _uow.MetaDataRepository.GetEntityMetaData(logicalName).Attributes;

                Entity entity = _entitySerializer.Deserialize(jsonEntityData, logicalName, attributesMetadata);

                _uow.EntityRepository.Create(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Guid CreateAndReturnId(string jsonEntityData, string logicalName)
        {
            if (string.IsNullOrEmpty(jsonEntityData))
            {
                throw new ArgumentNullException("jsonEntityData");
            }

            if (string.IsNullOrEmpty(logicalName))
            {
                throw new ArgumentNullException("logicalName");
            }

            try
            {
                var attributesMetadata = _uow.MetaDataRepository.GetEntityMetaData(logicalName).Attributes;

                Entity entity = _entitySerializer.Deserialize(jsonEntityData, logicalName, attributesMetadata);

                return _uow.EntityRepository.CreateAndReturnId(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void CreateAndAssociateRelatedEntities(RelatedEntitiesCreationModel relatedEntitiesCreationModel)
        {
            IEnumerable<EntityReference> createdRelatedEntities = CreateRelatedEntities(
                    relatedEntitiesCreationModel.RelatedEntitiesJsonList,
                    relatedEntitiesCreationModel.RelatedEntitiesLogicalName);

            AssociateRelatedEntities(createdRelatedEntities,
                relatedEntitiesCreationModel.EntityId,
                relatedEntitiesCreationModel.EntityLogicalName,
                relatedEntitiesCreationModel.RelationshipShcemeName);
        }

        public void AssociateRelatedEntities(
            IEnumerable<EntityReference> createdRelatedEntities,
            Guid entityId,
            string entityLogicalName,
            string relationshipShcemeName)
        {

            Relationship relationship = new Relationship(relationshipShcemeName);
            var entityReferenceCollection = new EntityReferenceCollection(createdRelatedEntities.ToList());

            _uow.EntityRepository.Associate(entityLogicalName, entityId, relationship, entityReferenceCollection);
        }

        public IEnumerable<EntityReference> CreateRelatedEntities(
            IEnumerable<string> relatedEntitiesJsonList,
            string relatedEntitiesLogicalName)
        {
            List<EntityReference> createdRelatedEntitiedIdList = new List<EntityReference>();

            foreach (string entityJson in relatedEntitiesJsonList)
            {
                Guid createdRelatedEntityId = CreateAndReturnId(
                    entityJson,
                    relatedEntitiesLogicalName);

                createdRelatedEntitiedIdList.Add(
                    new EntityReference(
                        relatedEntitiesLogicalName,
                        createdRelatedEntityId
                    ));
            }

            return createdRelatedEntitiedIdList;
        }

        public void Update(string jsonEntityData, string logicalName)
        {
            if (string.IsNullOrEmpty(jsonEntityData))
            {
                throw new ArgumentNullException("jsonEntityData");
            }

            if (string.IsNullOrEmpty(logicalName))
            {
                throw new ArgumentNullException("logicalName");
            }

            try
            {
                var attributesMetadata = _uow.MetaDataRepository.GetEntityMetaData(logicalName).Attributes;

                Entity entity = _entitySerializer.Deserialize(jsonEntityData, logicalName, attributesMetadata);

                _uow.EntityRepository.Update(entity);
            }
            catch(Exception ex)
            {
                throw;
            }            
        }

        public void Update(EntityListModel entityListModel)
        {
            foreach (string entityJson in entityListModel.JsonEntityDataList)
            {
                Update(entityJson, entityListModel.LogicalName);
            }
        }

        public void Delete(Guid id, string entityLogicalName)
        {
            if (string.IsNullOrEmpty(entityLogicalName))
            {
                throw new ArgumentNullException("entityLogicalName");
            }

            _uow.EntityRepository.Delete(entityLogicalName, id);
        }

        public void DeleteRange(IEnumerable<Guid> idArr, string entityLogicalName)
        {
            foreach (Guid id in idArr)
            {
                Delete(id, entityLogicalName);
            }
        }

        #region private methods


        private IEnumerable<string> GetStringSearchFields(IEnumerable<string> searchFields, string entityLogicalName)
        {
            var stringSearchFields = new List<string>();
            AttributeMetadata[] attributesMetaData = _uow.MetaDataRepository
                .GetEntityMetaData(entityLogicalName)?.Attributes;

            if(attributesMetaData == null || attributesMetaData.Count() == 0)
            {
                return new List<string>();
            }

            foreach (string searchField in searchFields)
            {
                var attributeMetaData = attributesMetaData.FirstOrDefault(a => a.LogicalName == searchField);

                if(attributeMetaData == null)
                {
                    continue;
                }

                if (attributeMetaData.IsSearchable == true)
                {
                    stringSearchFields.Add(searchField);
                }
            }

            return stringSearchFields;
        }

        private string GetViewXml(Guid? viewId)
        {
            if (viewId == null)
            {
                throw new NullReferenceException("ViewId cannot be null.");
            }

            string viewXml = GetFetchXmlFromView(viewId.Value);

            return viewXml;
        }

        private IEnumerable<string> GetPropNamesFromView(string viewXml)
        {
            ISavedQueryService viewService = new SavedQueryService(_uow);
            viewService.UserId = UserId;
            var propsNames = viewService.GetPropNamesFromView(viewXml);

            propsNames = propsNames.Where(p => p != null).ToList();

            return propsNames;
        }

        private string GetOrderEntityFieldName(XElement order)
        {
            string orderEntityFieldName = order?.Attribute("attribute")?.Value;
            return orderEntityFieldName;
        }

        private XElement GetOrderEntityAttribute(string viewXml)
        {
            var xelem = XElement.Parse(viewXml);
            XElement firstEntity = xelem.Elements("entity").FirstOrDefault();

            IEnumerable<XElement> attributes = firstEntity.Elements("attribute");
            var propsNames = new List<string>();

            XElement order = firstEntity.Element("order");

            return order;
        }

        private void AddE1RulesToQuery(QueryExpression query)
        {
            IEnumerable<Entity> portalTeamsList = GetPortalTeamsList();
            IEnumerable<Guid> portalTeamsIdList = portalTeamsList.Select(pt => pt.Id).ToList();
            IEnumerable<Guid> accountIdList = GetAccountsIdListForE1(portalTeamsIdList);

            if(accountIdList.Count() == 0)
            {
                accountIdList = new List<Guid> { new Guid() };
            }

            //Add for e1 ACCOUNT CONTACTS – They should only be able to see contacts that are linked to their ACCOUNT.
            if (query.EntityName == "contact")
            {
                query.Criteria.AddCondition(
                    "parentcustomerid",
                    ConditionOperator.In,
                    accountIdList.Select(id => id.ToString()).ToArray());
            }
            //SITES – They should only be able to see SITES that are linked to their ACCOUNT.
            else if (query.EntityName == "ddsm_site")
            {
                query.Criteria.AddCondition(
                    "ddsm_parentaccount",
                    ConditionOperator.In,
                    accountIdList.Select(id => id.ToString()).ToArray());
            }
            //MEASURE TEMPLATES – Can only see MT that Program Offering contains 
            //BER -Instant Rebates and ‘TODAYS DATE’ falls in between selectable start and end dates.
            else if (query.EntityName == "ddsm_measuretemplate")
            {
                query.Criteria.AddCondition(
                    "ddsm_portalteamid",
                    ConditionOperator.In,
                    portalTeamsIdList.Select(id => id.ToString()).ToArray());

                var programOfferingLink = new LinkEntity(
                    "ddsm_measuretemplate", "ddsm_programoffering",
                    "ddsm_programoffering", "ddsm_programofferingid",
                    JoinOperator.Inner);

                Entity defaultPortalTeam = _uow.PortalConfigurationRepository.GetDefaultPortalTeamByContactId(UserId);
                var measureTemplateName = defaultPortalTeam?.GetAttributeValue<string>("ddsm_mesuretemplatename");

                if(string.IsNullOrEmpty(measureTemplateName))
                {
                    measureTemplateName = "BER-Instant Rebates";
                }

                programOfferingLink.LinkCriteria.AddCondition(
                  "ddsm_name",
                  ConditionOperator.Like,
                  $"%{measureTemplateName}%");

                var filterDate = defaultPortalTeam?.GetAttributeValue<DateTime?>("ddsm_measuretemplatedate");

                if (filterDate == null)
                {
                    filterDate = DateTime.Now;
                }

                query.Criteria.AddCondition(
                    "ddsm_selectablestartdate",
                    ConditionOperator.LessThan,
                    filterDate);

                query.Criteria.AddCondition(
                    "ddsm_selectableenddate",
                    ConditionOperator.GreaterThan,
                    filterDate);

                query.LinkEntities.Add(programOfferingLink);
            }
            else if (query.EntityName == "ddsm_modelnumber")
            {
                var measureTemplateLink = new LinkEntity(
                    "ddsm_modelnumber", "ddsm_measuretemplate",
                    "ddsm_measurename", "ddsm_measuretemplateid",
                    JoinOperator.Inner);

                var programOfferingLink = new LinkEntity(
                    "ddsm_measuretemplate", "ddsm_programoffering",
                    "ddsm_programoffering", "ddsm_programofferingid",
                    JoinOperator.Inner);

                measureTemplateLink.LinkEntities.Add(programOfferingLink);

                Entity defaultPortalTeam = _uow.PortalConfigurationRepository.GetDefaultPortalTeamByContactId(UserId);
                var measureTemplateName = defaultPortalTeam?.GetAttributeValue<string>("ddsm_mesuretemplatename");

                if (string.IsNullOrEmpty(measureTemplateName))
                {
                    measureTemplateName = "BER-Instant Rebates";
                }

                programOfferingLink.LinkCriteria.AddCondition(
                  "ddsm_name",
                  ConditionOperator.Like,
                  $"%{measureTemplateName}%");

                var filterDate = defaultPortalTeam?.GetAttributeValue<DateTime?>("ddsm_measuretemplatedate");

                if (filterDate == null)
                {
                    filterDate = DateTime.Now;
                }

                measureTemplateLink.LinkCriteria.AddCondition(
                    "ddsm_selectablestartdate",
                    ConditionOperator.LessThan,
                    filterDate);

                measureTemplateLink.LinkCriteria.AddCondition(
                    "ddsm_selectableenddate",
                    ConditionOperator.GreaterThan,
                    filterDate);

                query.LinkEntities.Add(measureTemplateLink);
            }
        }

        private IEnumerable<Entity> GetPortalTeamsList()
        {
            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);
            IEnumerable<Entity> portalTeams = _uow.PortalConfigurationRepository.GetPortalTeams(teamsIdList);

            return portalTeams;
        }

        private IEnumerable<Guid> GetPortalTeamsIdList()
        {
            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);
            IEnumerable<Entity> portalTeams = _uow.PortalConfigurationRepository.GetPortalTeams(teamsIdList);
            IEnumerable<Guid> portalTeamsIdList = portalTeams.Select(pt => pt.Id).ToList();

            return portalTeamsIdList;
        }

        private IEnumerable<Guid> GetAccountsIdListForE1()
        {
            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);
            IEnumerable<Entity> portalTeams = _uow.PortalConfigurationRepository.GetPortalTeams(teamsIdList);
            IEnumerable<Guid> portalTeamsIdList = portalTeams.Select(pt => pt.Id).ToList();

            IEnumerable<Guid> accountIdList = _uow.PortalConfigurationRepository
                .GetAccountIdListByPortalTeamIdList(portalTeamsIdList);

            return GetAccountsIdListForE1(accountIdList);
        }

        private IEnumerable<Guid> GetAccountsIdListForE1(IEnumerable<Guid> portalTeamsIdList)
        {
            IEnumerable<Guid> accountIdList = _uow.PortalConfigurationRepository
              .GetAccountIdListByPortalTeamIdList(portalTeamsIdList);

            if (accountIdList.Count() == 0)
            {
                //contact
                Entity portalUser = _uow.ContactRepository.Get(UserId, new[] { "parentcustomerid" });
                var accountRef = portalUser?.GetAttributeValue<EntityReference>("parentcustomerid");

                if (accountRef != null)
                {
                    return new[] { accountRef.Id };
                }
            }

            return accountIdList;
        }

        private string GetFetchXmlFromView(Guid id)
        {
            var query = new QueryExpression
            {
                EntityName = "savedquery",
                ColumnSet = new ColumnSet("fetchxml")
            };

            query.Criteria.AddCondition(
                "savedqueryid",
                ConditionOperator.Equal,
                id);

            string viewXml = _uow.EntityRepository.GetAllWithTotalCount(query)
                .Entities.First()["fetchxml"].ToString();

            return viewXml;
        }

        #endregion
    }
}
