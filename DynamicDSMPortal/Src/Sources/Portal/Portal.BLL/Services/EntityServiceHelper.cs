﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Portal.BLL.Interfaces;
using Portal.DAL.Interfaces;

namespace Portal.BLL.Services
{
    public class EntityServiceHelper : BaseService, IEntityServiceHelper
    {
        private readonly IUnitOfWork _uow;

        public EntityServiceHelper(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public QueryExpression AddConditionsToQuery(QueryExpression query, string viewXml) { 

            var conversionRequest = new FetchXmlToQueryExpressionRequest
            {
                FetchXml = viewXml
            };
            FetchXmlToQueryExpressionResponse conversionResponse = _uow.EntityHelperRepository.GetConversionResponse(conversionRequest);
            QueryExpression queryExpression = conversionResponse.Query;
            query.Criteria = queryExpression.Criteria;
            foreach (var link in queryExpression.LinkEntities)
            {
                query.LinkEntities.Add(link);
            }
            return query;
        }
    }
}
