﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.BLL.Helpers;
using Portal.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk.Metadata;

namespace Portal.BLL.Services
{
    //partial class for create, update, delete
    public partial class EntityService
    {
        public Guid CreateAndReturnId(string jsonEntityData, string logicalName)
        {
            return CreateAndReturnId(new EntityModel
            {
                LogicalName = logicalName,
                JsonEntityData = jsonEntityData
            });
        }

        public Guid CreateAndReturnId(EntityModel entityModel)
        {
            if (entityModel == null)
            {
                throw new ArgumentNullException(nameof(entityModel));
            }

            string jsonEntityData = entityModel.JsonEntityData;
            string logicalName = entityModel.LogicalName;

            if (string.IsNullOrEmpty(jsonEntityData))
            {
                throw new ArgumentNullException("jsonEntityData");
            }

            if (string.IsNullOrEmpty(logicalName))
            {
                throw new ArgumentNullException("logicalName");
            }

            bool isAccessAllow = _securityService.CheckAccess(
                UserId, DAL.Enums.Privilege.Create, logicalName);

            if (!isAccessAllow)
            {
                throw new Exception("Access is denied");
            }

            var attributesMetadata = _uow.MetaDataRepository.GetEntityMetaData(logicalName).Attributes;

            Entity entity = _entitySerializer.Deserialize(jsonEntityData, logicalName, attributesMetadata);

            var subgridHelaer = new SubgridHelper(_uow);
            IReadOnlyList<RelatedEntityCollection> relatedEntitiesCollections = subgridHelaer.GetEntitiesFromSubgrids(entityModel.SubgridCreationModels);

            if (relatedEntitiesCollections.Count() != 0)
            {
                foreach (RelatedEntityCollection relatedEntityCollection in relatedEntitiesCollections)
                {
                    entity.RelatedEntities.AddRange(relatedEntityCollection);
                }
            }

            ModifiedFullNameOfContactEntity(entity);//hardcode for contact fullname

            // hardcore for e1 rules
            AddRelationsForE1Rules(entity);

            bool hasPartnerAttrubute = attributesMetadata.Any(am => am.AttributeTypeName == AttributeTypeDisplayName.LookupType 
                                                                              && am.LogicalName == "ddsm_partner");

            if (hasPartnerAttrubute)
            {
                Guid? accountId = _uow.ContactRepository.GetAccountId(UserId);

                if (accountId != null)
                {
                    entity["ddsm_partner"] = new EntityReference { Id = accountId.Value };
                }
            }

            bool hasPortanOwnerAttribute = attributesMetadata.Any(am => am.AttributeTypeName == AttributeTypeDisplayName.LookupType
                                                                        && am.LogicalName == "ddsm_portalowner");

            if (hasPortanOwnerAttribute)
            {
                if (UserId != Guid.Empty)
                {
                    entity["ddsm_portalowner"] = new EntityReference { Id = UserId };
                }
            }

            bool hasModifiedByPortalUserAttribute = attributesMetadata.Any(am => am.AttributeTypeName == AttributeTypeDisplayName.LookupType
                                                                        && am.LogicalName == "ddsm_modifiedbyportaluser");
            if(hasModifiedByPortalUserAttribute)
            {
                entity["ddsm_modifiedbyportaluser"] = new EntityReference { Id = UserId };
            }

            bool hasModifiedByPortalUserDate = attributesMetadata.Any(am => am.AttributeTypeName == AttributeTypeDisplayName.DateTimeType
                                                                        && am.LogicalName == "ddsm_modifiedbyportaluserdate");

            if(hasModifiedByPortalUserDate)
            {
                entity["ddsm_modifiedbyportaluserdate"] = DateTime.Now.ToUniversalTime();
            }

            Guid entityid = _uow.EntityRepository.CreateAndReturnId(entity);

            return entityid;
        }

        public Guid CreateProject(EntityModel entityModel)
        {
            string jsonEntityData = entityModel.JsonEntityData;
            string logicalName = entityModel.LogicalName;

            if (string.IsNullOrEmpty(jsonEntityData))
            {
                throw new ArgumentNullException("jsonEntityData");
            }

            if (string.IsNullOrEmpty(logicalName))
            {
                throw new ArgumentNullException("logicalName");
            }

            if (logicalName != "ddsm_project")
            {
                throw new Exception("Its not a project");
            }

            bool isAccessAllow = _securityService.CheckAccess(
                UserId, DAL.Enums.Privilege.Create, logicalName);

            if (!isAccessAllow)
            {
                throw new Exception("Access is denied");
            }

            var attributesMetadata = _uow.MetaDataRepository.GetEntityMetaData(logicalName).Attributes;

            Entity projectEntity = _entitySerializer.Deserialize(jsonEntityData, logicalName, attributesMetadata);

            if (projectEntity == null)
            {
                throw new ArgumentNullException(nameof(projectEntity));
            }

            var parentAccount = projectEntity.GetAttributeValue<EntityReference>("ddsm_accountid");
            string parentAccountId = parentAccount?.Id.ToString();

            if (parentAccountId == null)
            {
                throw new Exception("parent account is null");
            }

            var parentSite = projectEntity.GetAttributeValue<EntityReference>("ddsm_parentsiteid");
            string parentSiteId = parentSite?.Id.ToString();

            if (parentSiteId == null)
            {
                throw new Exception("parentSite is null");
            }

            var startDate = projectEntity.GetAttributeValue<DateTime?>("ddsm_startdate");
            string startDateStr = startDate?.ToShortDateString();

            if (startDate == null)
            {
                throw new Exception("startDate is null");
            }

            var programOffering = projectEntity.GetAttributeValue<EntityReference>("ddsm_programoffering");
            string programOfferingId = programOffering?.Id.ToString();

            if (programOfferingId == null)
            {
                throw new Exception("programOffering is null");
            }

            var projectTemplate = projectEntity.GetAttributeValue<EntityReference>("ddsm_projecttemplateid");
            string projectTemplateId = projectTemplate?.Id.ToString();

            if (projectTemplateId == null)
            {
                throw new Exception("projectTemplate is null");
            }

            var request = new OrganizationRequest("ddsm_DDSMManuallyCreatingProject")
            {
                ["ParentAccount"] = parentAccountId,
                ["ParentSite"] = parentSiteId,
                ["ProgramOffering"] = programOfferingId,
                ["ProjectTemplate"] = projectTemplateId,
                ["StartDate"] = startDateStr
            };

            var partnerContact = projectEntity.GetAttributeValue<EntityReference>("ddsm_tradeallycontact");
            if (partnerContact != null)
            {
                request["PartnerContact"] = partnerContact.Id.ToString();
            }

            var partnerCompany = projectEntity.GetAttributeValue<EntityReference>("ddsm_tradeally");
            if (partnerCompany != null)
            {
                request["PartnerCompany"] = partnerCompany.Id.ToString();
            }

            OrganizationResponse response = _uow.EntityRepository.Execute(request);

            if (!(bool)response.Results["Complete"])
            {
                throw new Exception("Cant create project.");
            }

            var projectId = Guid.Parse(response.Results["ProjectId"].ToString());

            return projectId;
        }

        public void CreateAndAssociateRelatedEntities(RelatedEntitiesCreationModel relatedEntitiesCreationModel)
        {
            IEnumerable<EntityReference> createdRelatedEntities = CreateRelatedEntities(
                    relatedEntitiesCreationModel.RelatedEntitiesJsonList,
                    relatedEntitiesCreationModel.RelatedEntitiesLogicalName);

            AssociateRelatedEntities(createdRelatedEntities,
                relatedEntitiesCreationModel.EntityId,
                relatedEntitiesCreationModel.EntityLogicalName,
                relatedEntitiesCreationModel.RelationshipShcemeName);
        }

        public void AssociateRelatedEntities(
            IEnumerable<EntityReference> createdRelatedEntities,
            Guid entityId,
            string entityLogicalName,
            string relationshipShcemeName)
        {

            var relationship = new Relationship(relationshipShcemeName);
            var entityReferenceCollection = new EntityReferenceCollection(createdRelatedEntities.ToList());

            _uow.EntityRepository.Associate(entityLogicalName, entityId, relationship, entityReferenceCollection);
        }

        public IEnumerable<EntityReference> CreateRelatedEntities(
            IEnumerable<string> relatedEntitiesJsonList,
            string relatedEntitiesLogicalName)
        {
            bool isAccessAllow = _securityService.CheckAccess(
              UserId, DAL.Enums.Privilege.Create, relatedEntitiesLogicalName);

            if (!isAccessAllow)
            {
                throw new Exception("Access is denied");
            }

            List<EntityReference> createdRelatedEntitiedIdList = new List<EntityReference>();

            foreach (string entityJson in relatedEntitiesJsonList)
            {
                Guid createdRelatedEntityId = CreateAndReturnId(
                    entityJson,
                    relatedEntitiesLogicalName);

                createdRelatedEntitiedIdList.Add(
                    new EntityReference(
                        relatedEntitiesLogicalName,
                        createdRelatedEntityId
                    ));
            }

            return createdRelatedEntitiedIdList;
        }

        public void Update(string jsonEntityData, string logicalName)
        {
            if (string.IsNullOrEmpty(jsonEntityData))
            {
                throw new ArgumentNullException("jsonEntityData");
            }

            if (string.IsNullOrEmpty(logicalName))
            {
                throw new ArgumentNullException("logicalName");
            }

            bool isAccessAllow = _securityService.CheckAccess(
                UserId, DAL.Enums.Privilege.Write, logicalName);

            if (!isAccessAllow)
            {
                throw new Exception("Access is denied");
            }

            var attributesMetadata = _uow.MetaDataRepository.GetEntityMetaData(logicalName).Attributes;

            Entity entity = _entitySerializer.Deserialize(jsonEntityData, logicalName, attributesMetadata);

            //for applications only
            if (entity.LogicalName == "ddsm_application")
            {
                Entity existingApplication = _uow.EntityRepository.Get(
                    entity.Id, "ddsm_application", new ColumnSet("ddsm_status"));


                if (existingApplication.GetAttributeValue<OptionSetValue>("ddsm_status")?.Value == (int)ApplicationStatus.NotStarted)
                {
                    entity["ddsm_status"] = new OptionSetValue((int)ApplicationStatus.InProgress);
                }
            }

            ModifiedFullNameOfContactEntity(entity);

            _uow.EntityRepository.Update(entity);
        }

        public void Update(EntityListModel entityListModel)
        {
            foreach (string entityJson in entityListModel.JsonEntityDataList)
            {
                Update(entityJson, entityListModel.LogicalName);
            }
        }

        public void Delete(Guid id, string entityLogicalName)
        {
            if (string.IsNullOrEmpty(entityLogicalName))
            {
                throw new ArgumentNullException("entityLogicalName");
            }

            bool isAccessAllow = _securityService.CheckAccess(
                    UserId, DAL.Enums.Privilege.Delete, entityLogicalName);

            if (!isAccessAllow)
            {
                throw new Exception("Access is denied");
            }

            _uow.EntityRepository.Delete(entityLogicalName, id);
        }

        public void DeleteRange(IEnumerable<Guid> idArr, string entityLogicalName)
        {
            foreach (Guid id in idArr)
            {
                Delete(id, entityLogicalName);
            }
        }
    }
}