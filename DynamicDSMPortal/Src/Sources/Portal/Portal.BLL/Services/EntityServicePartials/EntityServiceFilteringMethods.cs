﻿using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Portal.BLL.Helpers;
using Portal.Models.Aliased;
using Portal.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Services
{
    public partial class EntityService
    {
        private FilterExpression FormSearchFilterExpression(IEnumerable<string> stringSearchFields, string searchText)
        {
            var filter = new FilterExpression(LogicalOperator.Or);

            foreach (string searchField in stringSearchFields)
            {
                filter.AddCondition(
                      searchField.ToLower(),
                      ConditionOperator.Like,
                      "%" + searchText + "%");
            }

            return filter;
        }

        private FilterExpression FormFilterExpressionByFilter(
            KendoFilters kendoFilters, string entityLogicalName)
        {
            var resultFilter = new FilterExpression(
                 kendoFilters.Logic == "and" ? LogicalOperator.And : LogicalOperator.Or);

            IEnumerable<KendoFilter> topLevelFilters = kendoFilters?.Filters?.Where(kf => kf != null).ToList();

            if (topLevelFilters?.Count() == 0)
            {
                return null;
            }

            IEnumerable<string> fields = GetFieldsFromTopLevelKendoFilters(topLevelFilters);

            if (fields.Count() == 0)
            {
                return null;
            }

            IEnumerable<AttributeMetadata> fieldsMetadata = _uow.MetaDataRepository
                .GetAttributesMetadata(entityLogicalName, fields);

            foreach (KendoFilter kendoFilter in topLevelFilters)
            {
                //condition from top filter
                if (!string.IsNullOrEmpty(kendoFilter.Field))
                {
                    ConditionExpression bufCondtion = GetConditionFromKendoFilter(kendoFilter, fieldsMetadata);
                    if (bufCondtion != null)
                    {
                        resultFilter.AddCondition(bufCondtion);
                    }
                }

                //conditions from child filters
                if (kendoFilter.Filters?.Count() > 0)
                {
                    var childFilter = new FilterExpression(
                               kendoFilter.Logic == "and" ? LogicalOperator.And : LogicalOperator.Or);

                    foreach (KendoFilter childKendoFilter in kendoFilter.Filters)
                    {
                        ConditionExpression bufCondition = GetConditionFromKendoFilter(childKendoFilter, fieldsMetadata);

                        if (bufCondition != null)
                        {
                            childFilter.AddCondition(bufCondition);
                        }
                    }

                    if (childFilter.Conditions.Count() > 0)
                    {
                        resultFilter.AddFilter(childFilter);
                    }
                }
            }

            return resultFilter;
        }

        private IEnumerable<string> GetFieldsFromTopLevelKendoFilters(IEnumerable<KendoFilter> topLevelFilters)
        {
            var fieldsFromFilterList = new List<string>();

            foreach (KendoFilter kendoFilter in topLevelFilters)
            {
                if (!string.IsNullOrEmpty(kendoFilter.Field))
                {
                    fieldsFromFilterList.Add(kendoFilter.Field);
                }

                if (kendoFilter.Filters == null)
                {
                    continue;
                }

                foreach (KendoFilter childKendoFilter in kendoFilter.Filters)
                {
                    if (!string.IsNullOrEmpty(childKendoFilter.Field))
                    {
                        fieldsFromFilterList.Add(childKendoFilter.Field);
                    }
                }
            }

            fieldsFromFilterList = fieldsFromFilterList.Distinct().ToList();

            return fieldsFromFilterList;
        }

        private ConditionExpression GetConditionFromKendoFilter(KendoFilter kendoFilter,
            IEnumerable<AttributeMetadata> fieldsMetadata)
        {

            AttributeMetadata fieldMetadata = fieldsMetadata
                .FirstOrDefault(fm => fm.LogicalName == kendoFilter.Field);

            if (fieldMetadata == null)
            {
                return null;
            }

            var operation = KendoOperatorConverter.Convert(kendoFilter.Operator);
            object val = kendoFilter.Value;

            if (operation == ConditionOperator.Like || operation == ConditionOperator.NotLike)
            {
                val = $"%{val}%";
            }

            if (fieldMetadata.AttributeType == AttributeTypeCode.Lookup 
                || fieldMetadata.AttributeType == AttributeTypeCode.Customer)
            {
                var lookupMetadata = fieldMetadata as LookupAttributeMetadata;
                if (lookupMetadata == null)
                {
                    return null;
                }

                string targetEntityName = lookupMetadata?.Targets?.FirstOrDefault();

                if (string.IsNullOrEmpty(targetEntityName))
                {
                    return null;
                }

                string mainFieldName = _uow.MetaDataRepository.GetMainFieldName(targetEntityName);

                if (string.IsNullOrEmpty(mainFieldName))
                {
                    return null;
                }

                ConditionExpression lookupCondition = null;

                string attributeLookupName = kendoFilter.Field + "." + mainFieldName + "." + targetEntityName;

                if (operation == ConditionOperator.Null || operation == ConditionOperator.NotNull)
                {
                    lookupCondition = new ConditionExpression(
                        kendoFilter.Field,
                        operation);
                }
                else
                {
                    lookupCondition = new ConditionExpression(
                        targetEntityName,
                        attributeLookupName,
                        operation,
                        val);
                }

                return lookupCondition;
            }

            ConditionExpression condition = null;

            if (operation == ConditionOperator.Null || operation == ConditionOperator.NotNull)
            {
                condition = new ConditionExpression(
                    kendoFilter.Field,
                    operation);
            }
            else
            {
                if (fieldMetadata.AttributeType == AttributeTypeCode.DateTime)
                {
                    string dateString = val.ToString().Replace(" (FLE Standard Time)", "");
                    string datePattern = "ddd MMM d yyyy HH:mm:ss 'GMT'K";
                    val = DateTime.ParseExact(
                        dateString, datePattern, CultureInfo.InvariantCulture, DateTimeStyles.None);
                }

                condition = new ConditionExpression(
                    kendoFilter.Field,
                    operation,
                    val);
            }

            return condition;
        }

        private FilterExpression FormFilterExpressionByFilterForAliased(
            KendoFilters kendoFilters, string entityLogicalName, IEnumerable<AliasedValueModel> aliasedValues,
            QueryExpression query)
        {
            var resultFilter = new FilterExpression(
              kendoFilters.Logic == "and" ? LogicalOperator.And : LogicalOperator.Or);

            IEnumerable<KendoFilter> topLevelFilters = kendoFilters?.Filters?.Where(kf => kf != null).ToList();

            if (topLevelFilters?.Count() == 0)
            {
                return null;
            }

            IEnumerable<string> fields = GetFieldsFromTopLevelKendoFilters(topLevelFilters);

            if (fields.Count() == 0)
            {
                return null;
            }

            var aliasedValuesForFiltering = new List<AliasedValueModel>();

            foreach (AliasedValueModel aliasedValue in aliasedValues)
            {
                var bufAliasedValue = new AliasedValueModel
                {
                    ToAttribute = aliasedValue.ToAttribute,
                    Alias = aliasedValue.Alias,
                    EntityLogicalName = aliasedValue.EntityLogicalName,
                    FromAttribute = aliasedValue.FromAttribute,
                    JoinOperator = aliasedValue.JoinOperator
                };

                bufAliasedValue.AttributesList = aliasedValue.AttributesList.Where(at => fields.Contains(at)).ToList();
                aliasedValuesForFiltering.Add(bufAliasedValue);
            }

            if (aliasedValuesForFiltering.Count() == 0)
            {
                return null;
            }

            var aliasedValuesWithMetadata = new List<AliasedMetadataModel>();

            foreach (AliasedValueModel aliasedValue in aliasedValuesForFiltering)
            {
                IEnumerable<AttributeMetadata> fieldsMetadataBuf = _uow.MetaDataRepository
                            .GetAttributesMetadata(aliasedValue.EntityLogicalName, aliasedValue.AttributesList);

                if (fieldsMetadataBuf == null)
                {
                    continue;
                }

                aliasedValuesWithMetadata.Add(new AliasedMetadataModel
                {
                    AliasedValue = aliasedValue,
                    AttributesMetadata = fieldsMetadataBuf
                });
            }

            if (aliasedValuesWithMetadata.Count() == 0)
            {
                return null;
            }

            //todo
            foreach (KendoFilter kendoFilter in topLevelFilters)
            {
                if (!string.IsNullOrEmpty(kendoFilter.Field))
                {
                    ConditionExpression bufCondition = GetConditionFromKendoFilterForAliased(
                        kendoFilter, aliasedValuesWithMetadata, query);

                    if(bufCondition != null)
                    {
                        resultFilter.AddCondition(bufCondition);
                    }                    
                }

                if (kendoFilter.Filters?.Count() > 0)
                {
                    var childFilter = new FilterExpression(
                              kendoFilters.Logic == "and" ? LogicalOperator.And : LogicalOperator.Or);

                    foreach (KendoFilter childKendoFilter in kendoFilter.Filters)
                    {
                        ConditionExpression bufCondition = GetConditionFromKendoFilterForAliased(
                            childKendoFilter, aliasedValuesWithMetadata, query);

                        if (bufCondition != null)
                        {
                            childFilter.AddCondition(bufCondition);
                        }
                    }

                    if (childFilter.Conditions.Count() > 0)
                    {
                        resultFilter.AddFilter(childFilter);
                    }
                }
            }

            return resultFilter;
        }

        private ConditionExpression GetConditionFromKendoFilterForAliased(KendoFilter kendoFilter,
            IEnumerable<AliasedMetadataModel> aliasedValuesWithMetadata, QueryExpression query)
        {
            if (kendoFilter == null)
            {
                return null;
            }

            IEnumerable<AliasedMetadataModel> aliasedValuesWithMetaFilteredByField = aliasedValuesWithMetadata
                .Where(av => av?.AliasedValue?.AttributesList?.Contains(kendoFilter.Field) == true)
                .ToList();

            AliasedMetadataModel aliasedValueWithMeta = aliasedValuesWithMetaFilteredByField.FirstOrDefault();

            if (aliasedValueWithMeta?.AliasedValue == null)
            {
                return null;
            }

            var fieldMetadata = aliasedValueWithMeta.AttributesMetadata.FirstOrDefault(am => am.LogicalName == kendoFilter.Field);

            if (fieldMetadata == null)
            {
                return null;
            }

            var operation = KendoOperatorConverter.Convert(kendoFilter.Operator);
            object val = kendoFilter.Value;

            if (operation == ConditionOperator.Like || operation == ConditionOperator.NotLike)
            {
                val = $"%{val}%";
            }

            if (fieldMetadata.AttributeType == AttributeTypeCode.Lookup)
            {
                var lookupMetadata = fieldMetadata as LookupAttributeMetadata;
                if (lookupMetadata == null)
                {
                    return null;
                }

                string targetEntityName = lookupMetadata?.Targets?.FirstOrDefault();

                if (string.IsNullOrEmpty(targetEntityName))
                {
                    return null;
                }

                string mainFieldName = _uow.MetaDataRepository.GetMainFieldName(targetEntityName);

                if (string.IsNullOrEmpty(mainFieldName))
                {
                    return null;
                }

                ConditionExpression lookupCondition = null;

                if (operation == ConditionOperator.Null || operation == ConditionOperator.NotNull)
                {
                    lookupCondition = new ConditionExpression(
                        aliasedValueWithMeta.AliasedValue.EntityLogicalName,
                        kendoFilter.Field,
                        operation);
                }
                else
                {
                    lookupCondition = new ConditionExpression(
                        targetEntityName,
                        mainFieldName,
                        operation,
                        val);

                    LinkEntity parentLinkEntity = query.LinkEntities
                        .FirstOrDefault(f => f.LinkToEntityName == aliasedValueWithMeta.AliasedValue.EntityLogicalName);

                    if(parentLinkEntity == null)
                    {
                        return null;
                    }

                    var aliasedValue = aliasedValueWithMeta.AliasedValue;

                    parentLinkEntity.AddLink(
                        targetEntityName,
                        kendoFilter.Field, targetEntityName + "id", 
                        JoinOperator.LeftOuter);
                }

                return lookupCondition;
            }

            if (fieldMetadata.AttributeType == AttributeTypeCode.DateTime)
            {
                string dateString = val.ToString().Replace(" (FLE Standard Time)", "");
                string datePattern = "ddd MMM d yyyy HH:mm:ss 'GMT'K";
                val = DateTime.ParseExact(
                    dateString, datePattern, CultureInfo.InvariantCulture, DateTimeStyles.None);
            }

            ConditionExpression condition = null;

            if (operation == ConditionOperator.Null || operation == ConditionOperator.NotNull)
            {
                condition = new ConditionExpression(
                    aliasedValueWithMeta.AliasedValue.EntityLogicalName,
                    kendoFilter.Field,
                    operation);
            }
            else
            {
                condition = new ConditionExpression(
                    aliasedValueWithMeta.AliasedValue.EntityLogicalName,
                    kendoFilter.Field,
                    operation,
                    val);
            }

            return condition;
        }

        private FilterExpression FormSearchFilterExpressionForAliased(
            IEnumerable<AliasedValueModel> aliasedValuesForSearch,
            string searchText)
        {
            var filter = new FilterExpression(LogicalOperator.Or);

            foreach (AliasedValueModel aliasedValue in aliasedValuesForSearch)
            {
                if (aliasedValue == null
                    || aliasedValue.AttributesList.Count() == 0
                    || string.IsNullOrEmpty(aliasedValue.EntityLogicalName))
                {
                    continue;
                }

                IEnumerable<string> attributesForSearch = GetFieldsForSearch(
                        aliasedValue.AttributesList, aliasedValue.EntityLogicalName);

                foreach (string attribute in attributesForSearch)
                {
                    if (string.IsNullOrEmpty(attribute))
                    {
                        continue;
                    }

                    filter.AddCondition(
                         aliasedValue.EntityLogicalName,
                         attribute,
                         ConditionOperator.Like,
                         "%" + searchText + "%");
                }
            }

            return filter;
        }

        private IEnumerable<LinkEntity> GetLookupFieldsForSearch(
            IEnumerable<string> propsNames, string entityLogicalName)
        {
            if (propsNames == null || propsNames.Count() < 1 || string.IsNullOrEmpty(entityLogicalName))
            {
                return new List<LinkEntity>();
            }

            AttributeMetadata[] attributesMetadata = _uow.MetaDataRepository
                .GetEntityMetaData(entityLogicalName)?.Attributes;

            if (attributesMetadata == null || attributesMetadata.Count() == 0)
            {
                return new List<LinkEntity>();
            }

            AttributeMetadata[] attributesMeta = attributesMetadata
                .Where(a => propsNames.Contains(a.LogicalName.ToLower()))
                .Where(a => a.AttributeType == AttributeTypeCode.Lookup
                         || a.AttributeType == AttributeTypeCode.Customer)
                .ToArray();

            LookupAttributeMetadata[] lookupsAttributesMetaData = attributesMeta
                .Select(a => a as LookupAttributeMetadata)
                .Where(a => a != null && a.Targets != null & a.Targets.Count() != 0)
                .ToArray();

            if (lookupsAttributesMetaData.Count() == 0)
            {
                return new List<LinkEntity>();
            }

            var result = lookupsAttributesMetaData.Select(a => new LinkEntity
            {
                JoinOperator = JoinOperator.LeftOuter,
                LinkFromEntityName = entityLogicalName,
                LinkToEntityName = a.Targets.First(),
                LinkFromAttributeName = a.LogicalName,
                LinkToAttributeName = a.Targets.First() + "id",
                Columns = new ColumnSet(
                    new string[] {
                        _uow.MetaDataRepository.GetMainFieldName(a.Targets.First())
                    })
            })
            .Where(a => !a.Columns.Columns.Contains(null))
            .ToList();

            return result;
        }

    }
}