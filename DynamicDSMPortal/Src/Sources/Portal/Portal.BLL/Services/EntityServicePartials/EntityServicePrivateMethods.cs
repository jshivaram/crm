﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Portal.BLL.Helpers;
using Portal.BLL.Interfaces;
using Portal.Models.Aliased;
using Portal.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Portal.BLL.Services
{
    public partial class EntityService
    {
        private IEnumerable<string> GetFieldsForSearch(IEnumerable<string> searchFields, string entityLogicalName)
        {
            var fieldsForSearch = new List<string>();
            AttributeMetadata[] attributesMetaData = _uow.MetaDataRepository
                .GetEntityMetaData(entityLogicalName)?.Attributes;

            if (attributesMetaData == null || attributesMetaData.Count() == 0)
            {
                return new List<string>();
            }

            foreach (string searchField in searchFields)
            {
                var attributeMetaData = attributesMetaData.FirstOrDefault(a => a.LogicalName == searchField);

                if (attributeMetaData == null)
                {
                    continue;
                }

                if (attributeMetaData.IsSearchable == true
                    || attributeMetaData.AttributeType == AttributeTypeCode.String)
                {
                    fieldsForSearch.Add(searchField);
                }
            }

            return fieldsForSearch;
        }

        private IEnumerable<string> GetFieldsForFiltering(IEnumerable<string> searchFields, string entityLogicalName)
        {
            var fieldsForFiltering = new List<string>();

            AttributeMetadata[] attributesMetaData = _uow.MetaDataRepository
                .GetEntityMetaData(entityLogicalName)?.Attributes;

            if (attributesMetaData == null || attributesMetaData.Count() == 0)
            {
                return new List<string>();
            }

            foreach (string searchField in searchFields)
            {
                var attributeMetaData = attributesMetaData.FirstOrDefault(a => a.LogicalName == searchField);

                if (attributeMetaData == null)
                {
                    continue;
                }

                if (attributeMetaData.IsFilterable == true)
                {
                    fieldsForFiltering.Add(searchField);
                }
            }

            return fieldsForFiltering;
        }

        private IEnumerable<AliasedValueModel> GetAliasedSearchFields(
            IEnumerable<AliasedValueModel> aliasedValues)
        {
            aliasedValues = aliasedValues.Where(av => av != null);

            var aliasedValuesForSearch = new List<AliasedValueModel>();

            foreach (AliasedValueModel aliasedValue in aliasedValues)
            {
                AttributeMetadata[] attributesMetaData = _uow.MetaDataRepository
                    .GetEntityMetaData(aliasedValue.EntityLogicalName)?.Attributes;

                if (attributesMetaData == null)
                {
                    continue;
                }

                IEnumerable<string> attributesForSearchList = aliasedValue.AttributesList
                    ?.Where(attr => attributesMetaData
                    .FirstOrDefault(a => a.LogicalName == attr) != null)
                    .ToList();

                if (attributesForSearchList != null && attributesForSearchList.Count() > 0)
                {
                    var alisedValueBuf = new AliasedValueModel
                    {
                        EntityLogicalName = aliasedValue.EntityLogicalName,
                        Alias = aliasedValue.Alias,
                        FromAttribute = aliasedValue.FromAttribute,
                        ToAttribute = aliasedValue.ToAttribute,
                        JoinOperator = aliasedValue.JoinOperator,
                        AttributesList = attributesForSearchList
                    };

                    aliasedValuesForSearch.Add(alisedValueBuf);
                }
            }

            return aliasedValuesForSearch;
        }

        private string GetViewXml(Guid? viewId)
        {
            if (viewId == null)
            {
                throw new NullReferenceException("ViewId cannot be null.");
            }

            string viewXml = GetFetchXmlFromView(viewId.Value);

            return viewXml;
        }

        private string GetLayoutXml(Guid? viewId)
        {
            if (viewId == null)
            {
                throw new NullReferenceException("ViewId cannot be null.");
            }

            string layoutXml = GetLayoutXmlFromView(viewId.Value);

            return layoutXml;
        }

        private IEnumerable<string> GetPropNamesFromView(string viewXml)
        {
            ISavedQueryService viewService = new SavedQueryService(_uow);
            viewService.UserId = UserId;
            var propsNames = viewService.GetPropNamesFromView(viewXml);

            propsNames = propsNames.Where(p => p != null).ToList();

            return propsNames;
        }

        private IEnumerable<string> GetPropNamesFromLayout(string layoutXml)
        {
            XElement xelem = XElement.Parse(layoutXml);
            XElement firstEntity = xelem.Elements("row").FirstOrDefault();

            IEnumerable<XElement> attributes = firstEntity.Elements("cell");
            List<string> propsNames = new List<string>();

            propsNames.AddRange(attributes.Select(a => a.Attribute("name")?.Value)
                .Distinct()
                .ToList());

            propsNames = propsNames.Distinct().ToList();

            return propsNames;
        }

        private string GetOrderEntityFieldName(XElement order)
        {
            string orderEntityFieldName = order?.Attribute("attribute")?.Value;
            return orderEntityFieldName;
        }

        private XElement GetOrderEntityAttribute(string viewXml)
        {
            var xelem = XElement.Parse(viewXml);
            XElement firstEntity = xelem.Elements("entity").FirstOrDefault();

            IEnumerable<XElement> attributes = firstEntity.Elements("attribute");
            var propsNames = new List<string>();

            XElement order = firstEntity.Element("order");

            return order;
        }

        private IEnumerable<Entity> GetPortalTeamsList()
        {
            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);
            IEnumerable<Entity> portalTeams = _uow.PortalConfigurationRepository.GetPortalTeams(teamsIdList);

            return portalTeams;
        }

        private IEnumerable<Guid> GetPortalTeamsIdList()
        {
            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);
            IEnumerable<Entity> portalTeams = _uow.PortalConfigurationRepository.GetPortalTeams(teamsIdList);
            IEnumerable<Guid> portalTeamsIdList = portalTeams.Select(pt => pt.Id).ToList();

            return portalTeamsIdList;
        }

        private IEnumerable<Guid> GetAccountsIdListForE1()
        {
            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);
            IEnumerable<Entity> portalTeams = _uow.PortalConfigurationRepository.GetPortalTeams(teamsIdList);
            IEnumerable<Guid> portalTeamsIdList = portalTeams.Select(pt => pt.Id).ToList();

            IEnumerable<Guid> accountIdList = _uow.PortalConfigurationRepository
                .GetAccountIdListByPortalTeamIdList(portalTeamsIdList);

            return GetAccountsIdListForE1(accountIdList);
        }

        private IEnumerable<Guid> GetAccountsIdListForE1(IEnumerable<Guid> portalTeamsIdList)
        {
            IEnumerable<Guid> accountIdList = _uow.PortalConfigurationRepository
              .GetAccountIdListByPortalTeamIdList(portalTeamsIdList);

            if (accountIdList.Count() == 0)
            {
                //contact
                Entity portalUser = _uow.ContactRepository.Get(UserId, new[] { "parentcustomerid" });
                var accountRef = portalUser?.GetAttributeValue<EntityReference>("parentcustomerid");

                if (accountRef != null)
                {
                    return new[] { accountRef.Id };
                }
            }

            return accountIdList;
        }

        private string GetFetchXmlFromView(Guid id)
        {
            var query = new QueryExpression
            {
                EntityName = "savedquery",
                ColumnSet = new ColumnSet("fetchxml")
            };

            query.Criteria.AddCondition(
                "savedqueryid",
                ConditionOperator.Equal,
                id);

            string viewXml = _uow.EntityRepository.GetAllWithTotalCount(query)
                .Entities.First()["fetchxml"].ToString();

            return viewXml;
        }

        private string GetLayoutXmlFromView(Guid id)
        {
            var query = new QueryExpression
            {
                EntityName = "savedquery",
                ColumnSet = new ColumnSet("layoutxml")
            };

            query.Criteria.AddCondition(
                "savedqueryid",
                ConditionOperator.Equal,
                id);

            string layoutXml = _uow.EntityRepository.GetAllWithTotalCount(query)
                .Entities.First()["layoutxml"].ToString();

            return layoutXml;
        }

        private ColumnSet FormColumnSetByProps(IEnumerable<string> propsNames)
        {
            return propsNames.Count() > 0 ? new ColumnSet(propsNames.ToArray()) : new ColumnSet(true);
        }

        private void ModifiedFullNameOfContactEntity(Entity contact)
        {
            if (contact == null || contact.LogicalName != "contact")
            {
                return;
            }

            string fullName = contact.GetAttributeValue<string>("fullname");

            string firstNameFromEntity = contact.GetAttributeValue<string>("firstname");
            string lastNameFromEntity = contact.GetAttributeValue<string>("lastname");

            if (!string.IsNullOrEmpty(firstNameFromEntity) || !string.IsNullOrEmpty(lastNameFromEntity))
            {
                if (!string.IsNullOrEmpty(firstNameFromEntity) && string.IsNullOrEmpty(lastNameFromEntity))
                {
                    fullName = firstNameFromEntity;
                }
                else if (string.IsNullOrEmpty(firstNameFromEntity) && !string.IsNullOrEmpty(lastNameFromEntity))
                {
                    fullName = lastNameFromEntity;
                }
                else if (!string.IsNullOrEmpty(firstNameFromEntity) && !string.IsNullOrEmpty(lastNameFromEntity))
                {
                    fullName = firstNameFromEntity + " " + lastNameFromEntity;
                }

                contact["fullname"] = fullName;

                return;
            }

            if (string.IsNullOrEmpty(fullName))
            {
                return;
            }


            fullName = fullName.Trim();

            if (!fullName.Contains(" "))
            {
                contact["firstname"] = fullName;
                return;
            }

            //replace multiple spaces with a single space
            fullName = Regex.Replace(fullName, @"\s+", " ");

            string[] fullNameArr = fullName.Split(' ');
            if (fullNameArr?.Count() < 2)
            {
                return;
            }

            string firstName = fullNameArr[0];
            string lastName = fullNameArr[1];

            if (!string.IsNullOrEmpty(firstName))
            {
                contact["firstname"] = firstName;
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                contact["lastname"] = lastName;
            }
        }
    }
}
