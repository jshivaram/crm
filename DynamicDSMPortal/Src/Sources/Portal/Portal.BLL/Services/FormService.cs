﻿using Portal.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portal.Models;
using Portal.DAL.Interfaces;
using Microsoft.Xrm.Sdk;
using System.Xml.Linq;
using System.Xml;
using Newtonsoft.Json;
using System.IO;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL;

namespace Portal.BLL.Services
{
    public class FormService : BaseService, IFormService
    {
        private IUnitOfWork _uow;

        public FormService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<IEnumerable<FormControl>> GetFormControls(string entityLogicalName)
        {
            IEnumerable<Entity> forms = _uow.FormRepository.GetForms(entityLogicalName);

            IEnumerable<string> xmlList = forms.Select(f => f.GetAttributeValue<string>("formxml")).ToList();

            return await GetControls(xmlList.FirstOrDefault());
        }

        public IEnumerable<LookupModel> GetDashboards()
        {
            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);

            IEnumerable<Entity> dashboards = _uow.PortalConfigurationRepository
                .GetPortalDashboardsByTeamIdList(teamsIdList, new ColumnSet("ddsm_ddsm_dashboardid", "ddsm_name"));

            var result = dashboards.Select(f => new LookupModel
            {
                Text = f.GetAttributeValue<string>("ddsm_name"),
                Value = f.GetAttributeValue<string>("ddsm_ddsm_dashboardid")
            });

            return result;
        }

        public Guid? GetDefaultDashboardId()
        {
            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);

            if(teamsIdList.Count() == 0)
            {
                return null;
            }

            IEnumerable<Guid> idList = _uow.PortalConfigurationRepository
                .GetDefaultDashboardsIdByTeamIdList(teamsIdList);

            if(idList.Count() == 0)
            {
                return null;
            }

            return idList.First();
        }

        private async Task<IEnumerable<FormControl>> GetControls(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return null;
            }

            return await System.Threading.Tasks.Task.Run(() =>
            {

                IEnumerable<XElement> cellList = GetCellList(xml);

                List<FormControl> formControls = new List<FormControl>();

                foreach (XElement cell in cellList)
                {
                    XElement labels = cell.Element("labels");
                    XElement label = labels.Element("label");

                    XElement control = cell.Element("control");

                    bool showLabel = true;
                    bool.TryParse(cell.Attribute("showlabel")?.Value, out showLabel);

                    Guid cellId = new Guid();
                    Guid.TryParse(cell.Attribute("id")?.Value, out cellId);

                    Guid classid = new Guid();
                    Guid.TryParse(control?.Attribute("classid")?.Value, out classid);

                    bool disabled = false;
                    bool.TryParse(control?.Attribute("disabled")?.Value, out disabled);

                    FormControl formControl = new FormControl()
                    {
                        ShowLabel = showLabel,
                        CellId = cellId,

                        Description = label.Attribute("description")?.Value,
                        LanguageCode = label.Attribute("languagecode")?.Value,

                        ControlId = control?.Attribute("id")?.Value,
                        ClassId = classid,
                        DataFieldName = control?.Attribute("datafieldname")?.Value,
                        Disabled = disabled
                    };

                    formControls.Add(formControl);
                }

                return formControls;
            });
        }

        private IEnumerable<XElement> GetCellList(string xml)
        {
            XDocument xdoc = XDocument.Parse(xml);

            XElement form = xdoc.Element("form");
            XElement tabs = form.Element("tabs");
            IEnumerable<XElement> tabList = tabs.Elements("tab");

            IEnumerable<XElement> columns = tabList.SelectMany(t => t.Elements("columns")).ToList();
            IEnumerable<XElement> columnList = columns.SelectMany(c => c.Elements("column")).ToList();

            IEnumerable<XElement> sections = columnList.SelectMany(c => c.Elements("sections")).ToList();
            IEnumerable<XElement> sectionList = sections.SelectMany(s => s.Elements("section")).ToList();

            IEnumerable<XElement> rows = sectionList.SelectMany(s => s.Elements("rows")).ToList();
            IEnumerable<XElement> rowList = rows.SelectMany(r => r.Elements("row")).ToList();

            IEnumerable<XElement> cellList = rowList.SelectMany(r => r.Elements("cell")).ToList();

            return cellList;
        }

        public async Task<string> GetJsonFormStructureAsync(string entityLogicalName)
        {
            return await Task<string>.Run(() =>
            {
                try
                {
                    return GetJsonFormStructure(entityLogicalName);
                }
                catch (Exception ex)
                {
                    throw;
                }

            });
        }

        public string GetJsonFormStructure(string entityLogicalName)
        {
            try
            {
                IEnumerable<Entity> forms = _uow.FormRepository.GetForms(entityLogicalName);

                IEnumerable<string> xmlList = forms.Select(f => f.GetAttributeValue<string>("formxml")).ToList();

                string xml = xmlList.FirstOrDefault();

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                string jsonText = JsonConvert.SerializeXmlNode(doc);
                return jsonText;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string GetJsonFormStructure(string entityLogicalName, string formName)
        {
            try
            {
                IEnumerable<Entity> forms = _uow.FormRepository.GetForms(entityLogicalName);

                Entity form = forms.SingleOrDefault(f => f.GetAttributeValue<string>("name")
                    .ToLower() == formName.ToLower());

                if(form == null)
                {
                    throw new ArgumentNullException("form");
                }

                IEnumerable<string> xmlList = forms.Select(f => f.GetAttributeValue<string>("formxml"))
                    .ToList();

                string xml = form.GetAttributeValue<string>("formxml");

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                string jsonText = JsonConvert.SerializeXmlNode(doc);
                return jsonText;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string GetJsonFormStructure(string entityLogicalName, Guid formId, 
            FormTypeEnum formType = FormTypeEnum.FormTypeCreateUpdate)
        {
            try
            {
                Entity form = _uow.FormRepository.GetFormById(entityLogicalName, formId, formType);

                if (form == null)
                {
                    throw new ArgumentNullException("form");
                }

                string xml = form.GetAttributeValue<string>("formxml");

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                string jsonText = JsonConvert.SerializeXmlNode(doc);
                return jsonText;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public string GetJsonFormStructure(Guid formId,
            FormTypeEnum formType = FormTypeEnum.FormTypeCreateUpdate)
        {
            try
            {
                Entity form = _uow.FormRepository.GetFormById(formId, formType);

                if (form == null)
                {
                    throw new ArgumentNullException("form");
                }

                string xml = form.GetAttributeValue<string>("formxml");

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                string jsonText = JsonConvert.SerializeXmlNode(doc);
                return jsonText;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IDictionary<Guid, string> GetForms(string entityLogicalName)
        {
            try
            {
                IEnumerable<Entity> forms = _uow.FormRepository.GetForms(entityLogicalName);

                IDictionary<Guid, string> result = forms.Select(
                    f => new
                    {
                        Id = f.Id,
                        Name = f.GetAttributeValue<string>("name")
                    }).ToDictionary(f => f.Id, f => f.Name);

                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<object> GetForms(string entityLogicalName, Guid contactId)
        {
            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(contactId);

            IEnumerable<Entity> portalForms = _uow.PortalConfigurationRepository
                .GetPortalFormsByTeamIdList(teamsIdList, entityLogicalName, new ColumnSet("ddsm_formid"));

            IEnumerable<Guid> formsIdList = portalForms.Select(pf => new Guid(pf.Attributes["ddsm_formid"]
                .ToString())).Distinct().ToList();

            if (formsIdList.Count() == 0)
            {
                throw new Exception("You haven't available forms");
            }

            IEnumerable<Entity> forms = _uow.PortalConfigurationRepository.GetFormsByFormsIdList(
                formsIdList, new ColumnSet("name"));
           
             
            IEnumerable<object> result = forms.Select(f => new
            {
                Key = f.Id,
                Value = f.GetAttributeValue<string>("name")
            });

            return result;
        }

        public Guid? GetDefaultFormId(string entityLogicalName, Guid contactId)
        {

            Guid? defaultTeamId = _uow.PortalConfigurationRepository.GetDefaultPortalTeamIdByContactId(contactId);

            IEnumerable<Guid> teamsIdList = null;

            if (defaultTeamId != null && defaultTeamId != new Guid())
            {
                Guid? teamId = _uow.PortalConfigurationRepository.GetTeamIdByPortalTeamId(defaultTeamId.Value);
                if (teamId != null && teamId != new Guid())
                {
                    teamsIdList = new List<Guid> { teamId.Value };
                }
            }
            else
            {
                teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(contactId);
            }

            var defaultFormsIdList = new List<Guid>();

            foreach (Guid teamId in teamsIdList)
            {
                Guid? bufDefaultFormId = _uow.PortalConfigurationRepository.GetDefaultFormIdByTeamId(
                    teamId, entityLogicalName);

                if (bufDefaultFormId.HasValue)
                {
                    defaultFormsIdList.Add(bufDefaultFormId.Value);
                }
            }

            if (defaultFormsIdList.Count == 0)
            {
                return null;
            }

            Guid defaultFormId = defaultFormsIdList.First();

            return defaultFormId;
        }
    }
}
