﻿using Portal.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portal.Models;
using Portal.Models.EntityModels;
using Portal.DAL.Interfaces;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Xml.Linq;

namespace Portal.BLL.Services
{
    public class LookupService : ILookupService
    {
        private readonly IUnitOfWork _uow;

        public LookupService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IEnumerable<LookupModel> GetEntitiesForLookup(LookupFilterModel lookupFilterModel)
        {
            if (lookupFilterModel.ViewId == new Guid())
            {
                return new List<LookupModel>();
            }

            Entity view = _uow.EntityRepository.Get(
                lookupFilterModel.ViewId,
                "savedquery",
                new ColumnSet("fetchxml"));

            if (view == null)
            {
                return new List<LookupModel>();
            }

            string viewXml = view["fetchxml"].ToString();
            if (string.IsNullOrEmpty(viewXml))
            {
                return new List<LookupModel>();
            }

            string entityLogicalName = GetEntityLogicalNameFromViewXml(viewXml);

            if (string.IsNullOrEmpty(entityLogicalName))
            {
                return new List<LookupModel>();
            }

            string mainLookupFieldName = GetMainLookupFieldName(viewXml, entityLogicalName);

            if (string.IsNullOrEmpty(mainLookupFieldName))
            {
                return new List<LookupModel>();
            }

            var query = new QueryExpression
            {
                EntityName = entityLogicalName,
                ColumnSet = new ColumnSet(mainLookupFieldName),
                TopCount = 10,
            };

            //query.AddOrder(mainLookupFieldName, OrderType.Ascending);
            //ordering remove order attribute from entity
            AddFiltersToQuery(query, lookupFilterModel.Filter, mainLookupFieldName);

            try
            {
                IEnumerable<Entity> entities = _uow.EntityRepository.GetAllWithTotalCount(query).Entities;
                IEnumerable<LookupModel> result = entities.Select(e => new LookupModel
                {
                    Value = e.Id.ToString(),
                    Text = e[mainLookupFieldName].ToString()
                }).ToList();

                result = result.OrderBy(r => r.Text).ToList();

                return result;
            }
            catch(Exception ex)
            {
                throw;
            }           
        }

        private void AddFiltersToQuery(QueryExpression query, 
            LookupFilter filter, string mainProp)
        {
            if (query == null || filter == null)
            {
                return;
            }

            var queryFilter = new FilterExpression();

            if (filter.Logic == "and")
            {
                queryFilter.FilterOperator = LogicalOperator.And;
            }
            else if (filter.Logic == "or")
            {
                queryFilter.FilterOperator = LogicalOperator.Or;
            }

            LookupFilters firstFilter = filter.Filters?.FirstOrDefault();
            if(firstFilter == null || firstFilter.Value == null)
            {
                return;
            }

            ConditionOperator conditionOperator = FormConditionOperator(firstFilter.Operator);

            if(conditionOperator == ConditionOperator.Like 
                || conditionOperator == ConditionOperator.NotLike)
            {
                firstFilter.Value = $"%{firstFilter.Value}%";
            }

            queryFilter.Conditions.Add(new ConditionExpression(
                mainProp,
                conditionOperator,
                firstFilter.Value
                ));

            query.Criteria.AddFilter(queryFilter);
        }

        private ConditionOperator FormConditionOperator(string filterOperator)
        {
            switch(filterOperator)
            {
                case "contains":
                    return ConditionOperator.Like;
                case "doesnotcontain":
                    return ConditionOperator.NotLike;
                case "eq":
                    return ConditionOperator.Equal;
                case "neq":
                    return ConditionOperator.NotEqual;
                case "startswith":
                    return ConditionOperator.Like;
                case "endswith":
                    return ConditionOperator.EndsWith;
                default:
                    return ConditionOperator.Equal;
            }
        }

        private string GetMainLookupFieldName(string viewXml, string entityLogicalName)
        {
            if (string.IsNullOrEmpty(viewXml))
            {
                return null;
            }

            var mainElement = XElement.Parse(viewXml);
            XElement firstEntity = mainElement.Elements("entity").FirstOrDefault();

            IEnumerable<XElement> attributes = firstEntity.Elements("attribute");
            var propsNames = new List<string>();

            propsNames.AddRange(attributes.Select(a => a.Attribute("name")?.Value?.ToLower()));

            if (propsNames.Contains("name"))
            {
                return "name";
            }

            if (propsNames.Contains("ddsm_name"))
            {
                return "ddsm_name";
            }

            propsNames = propsNames.Where(p => p != (entityLogicalName.ToLower() + "id"))
                                   .Distinct().ToList();

            if (propsNames.Count() == 0)
            {
                return entityLogicalName.ToLower() + "id";
            }

            return propsNames.First().ToLower();
        }

        private string GetEntityLogicalNameFromViewXml(string viewXml)
        {
            if (string.IsNullOrEmpty(viewXml))
            {
                return null;
            }

            var mainElement = XElement.Parse(viewXml);
            XElement entityElement = mainElement.Element("entity");
            XAttribute nameAttribute = entityElement.Attribute("name");
            string entityLogicalName = nameAttribute.Value;

            return entityLogicalName;
        }
    }
}
