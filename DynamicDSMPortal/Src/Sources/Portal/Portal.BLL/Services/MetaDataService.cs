﻿using Portal.BLL.Interfaces;
using System.Linq;
using Microsoft.Xrm.Sdk.Metadata;
using Portal.DAL.Interfaces;
using System.Collections.Generic;
using Portal.Models.Metadata;
using System;

using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Xml.Linq;

namespace Portal.BLL.Services
{
    public class MetaDataService : BaseService, IMetaDataService
    {
        private IUnitOfWork _uow;
        private ISavedQueryService _savedQueryService;

        public MetaDataService(IUnitOfWork uow)
        {
            _uow = uow;
            _savedQueryService = new SavedQueryService(_uow);
            _savedQueryService.UserId = UserId;
        }

        public IEnumerable<AttributeMetadataModel> GetAttributesMetadata(string entityLogicalName, Guid viewId)
        {
            if(string.IsNullOrEmpty(entityLogicalName) 
                || string.IsNullOrWhiteSpace(entityLogicalName))
            {
                throw new ArgumentNullException("entityLogical");
            }

            var props = new List<string>();
            Entity view = _uow.EntityRepository.Get(viewId, "savedquery", new ColumnSet("layoutxml", "fetchxml"));

            if(view.Attributes.ContainsKey("layoutxml"))
            {
                var grid = XElement.Parse(view.GetAttributeValue<string>("layoutxml"));
                var row = grid.Element("row");
                var cells = row.Elements("cell");
                props = cells.Select(c => c.Attribute("name").Value).ToList();

                if(!props.Contains(entityLogicalName + "id"))
                {
                    props.Add(entityLogicalName + "id");        
                }
            }
            else if(view.Attributes.Contains("fetchxml"))
            {
                props = _savedQueryService.GetPropNamesFromView(view.GetAttributeValue<string>("fetchxml")).ToList();
            }
            else
            {
                return null;
            }

            IEnumerable<AttributeMetadata> attributesMetadata = _uow.MetaDataRepository
                .GetAttributesMetadata(entityLogicalName, props);

            IEnumerable<AttributeMetadataModel> result = attributesMetadata.Select(am => new AttributeMetadataModel
            {
                DisplayName = am.DisplayName?.LocalizedLabels?.FirstOrDefault()?.Label,
                Editable = am.RequiredLevel.CanBeChanged,
                LogicalName = am.LogicalName,
                Required = am.RequiredLevel?.Value,
                Type = am.AttributeTypeName?.Value
            });

            return result;
        } 

        public OptionSetMetadata GetOptionSet(string entityLogicalName, string fieldName)
        {
            EntityMetadata entityMetadata = _uow.MetaDataRepository.GetEntityMetaData(entityLogicalName);

            AttributeMetadata attribute = entityMetadata
                        .Attributes
                        .FirstOrDefault(x => x.LogicalName.Equals(fieldName.ToLower()));

            OptionSetMetadata optionSet = ((EnumAttributeMetadata)attribute).OptionSet;

            return optionSet;
        }

        public IEnumerable<object> GetMetaDataForDropDownList(string entityLogicalName, string fieldName)
        {
            OptionSetMetadata optionSetMetadata = GetOptionSet(entityLogicalName, fieldName);

            var result = optionSetMetadata
                .Options.Select(o => new
                {
                    Value = o.Value,
                    Text = o.Label.LocalizedLabels.FirstOrDefault().Label
                })
                .ToList();

            return result;
        }

        public string GetLookupFieldEntityName(string entityName, string field)
        {
            var attributeMetaData = (LookupAttributeMetadata)_uow.MetaDataRepository
                .GetAttributeMetadata(entityName, field)?.AttributeMetadata;
            
            return attributeMetaData.Targets.FirstOrDefault();
        }

        public string GetProcesStageByEntityId(string entityId)
        {
            return _uow.MetaDataRepository.GetProcesStageByEntityId(entityId);
        }
    }
}
