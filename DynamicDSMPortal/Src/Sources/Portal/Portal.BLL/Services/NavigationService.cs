﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.BLL.Extensions;
using Portal.BLL.Interfaces;
using Portal.DAL.Interfaces;
using Portal.Models.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Portal.BLL.Services
{
    public class NavigationService : BaseService, INavigationService
    {
        private IUnitOfWork _uow;

        public NavigationService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IEnumerable<NavigationEntityModel> GetEntitiesForNavigation()
        {
            List<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId).ToList();
            var columns = new ColumnSet(
                "ddsm_entitylogicalname",
                "ddsm_name",
                "ddsm_class_name",
                "ddsm_navigation_name",
                "ddsm_numberinmenu");

            Guid? defaultPortalTeamId = _uow.ContactRepository.GetDefaultPortalTeamId(UserId);
            Guid? defaulTeamId = defaultPortalTeamId.HasValue ?
                _uow.PortalConfigurationRepository.GetTeamIdByPortalTeamId(defaultPortalTeamId.Value)
                : null;

            if (defaulTeamId != null)
            {
                var entities = new List<Entity>();

                teamsIdList.Remove(defaulTeamId.Value);

                IEnumerable<Entity> entitiesFromDefaultTeam = _uow.PortalConfigurationRepository
                    .GetPortalEntitiesListForNavigation(defaulTeamId.Value, columns);

                IEnumerable<Entity> numberedEntitiesFromDefaultTeam = entitiesFromDefaultTeam
                    .Where(e => e.Attributes.Keys.Contains("ddsm_numberinmenu"))
                    .OrderBy(e => e.GetAttributeValue<int?>("ddsm_numberinmenu"))
                    .ToList();

                IEnumerable<Entity> notNumberedEntitiesFromDefaultTeam = entitiesFromDefaultTeam
                    .Where(e => !e.Attributes.Keys.Contains("ddsm_numberinmenu"))
                    .OrderBy(e => e.GetAttributeValue<string>("ddsm_navigation_name"))
                    .ThenBy(e => e.GetAttributeValue<string>("ddsm_name"))
                    .ThenBy(e => e.GetAttributeValue<string>("ddsm_entitylogicalname"))
                    .ToList();

                IEnumerable<Entity> entitiesNotDefaultTeams = _uow.PortalConfigurationRepository
                    .GetPortalEntitiesListForNavigation(teamsIdList, columns);

                IEnumerable<Entity> numberedEntitiesFromNotDefaultTeam = entitiesNotDefaultTeams
                    .Where(e => e.Attributes.Keys.Contains("ddsm_numberinmenu"))
                    .OrderBy(e => e.GetAttributeValue<int?>("ddsm_numberinmenu"))
                    .ToList();

                IEnumerable<Entity> notNumberedEntitiesFromNotDefaultTeam = entitiesNotDefaultTeams
                    .Where(e => !e.Attributes.Keys.Contains("ddsm_numberinmenu"))
                    .OrderBy(e => e.GetAttributeValue<string>("ddsm_navigation_name"))
                    .ThenBy(e => e.GetAttributeValue<string>("ddsm_name"))
                    .ThenBy(e => e.GetAttributeValue<string>("ddsm_entitylogicalname"))
                    .ToList();

                entities.AddRange(numberedEntitiesFromDefaultTeam);
                entities.AddRange(numberedEntitiesFromNotDefaultTeam);
                entities.AddRange(notNumberedEntitiesFromDefaultTeam);
                entities.AddRange(notNumberedEntitiesFromNotDefaultTeam);

                return entities.MapPortalEntitiesToMenuItems();
            }
            else
            {
                IEnumerable<Entity> entities = _uow.PortalConfigurationRepository
                    .GetPortalEntitiesListForNavigation(teamsIdList, columns);

                entities = entities
                        .OrderByDescending(e => e.GetAttributeValue<int?>("ddsm_numberinmenu").HasValue)
                        .ThenBy(e => e.GetAttributeValue<int?>("ddsm_numberinmenu"))
                        .ThenBy(e => e.GetAttributeValue<string>("ddsm_navigation_name"))
                        .ThenBy(e => e.GetAttributeValue<string>("ddsm_name"))
                        .ThenBy(e => e.GetAttributeValue<string>("ddsm_entitylogicalname"))
                        .ToList();

                return entities.MapPortalEntitiesToMenuItems();
            }
        }
    }
}