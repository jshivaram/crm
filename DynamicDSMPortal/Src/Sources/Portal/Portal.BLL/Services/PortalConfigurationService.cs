﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.BLL.Interfaces;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Services
{
    public class PortalConfigurationService : BaseService, IPortalConfigurationService
    {
        private IUnitOfWork _uow;

        public PortalConfigurationService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public string GetDefaultAfterLoginUrl()
        {
            Entity defaultPortalTeam = _uow.PortalConfigurationRepository.GetDefaultPortalTeamByContactId(UserId);

            if(defaultPortalTeam == null)
            {
                return null;
            }

            var defaultPageRef = defaultPortalTeam
                .GetAttributeValue<EntityReference>("ddsm_defalut_portal_page_url");

            if(defaultPageRef == null)
            {
                return null;
            }

            Entity defaultPage = _uow.EntityRepository.Get(
                defaultPageRef.Id,
                defaultPageRef.LogicalName, 
                new ColumnSet("ddsm_url"));

            return defaultPage?.GetAttributeValue<string>("ddsm_url");
        }
    }
}
