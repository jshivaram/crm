﻿using Microsoft.Xrm.Sdk;
using Portal.BLL.Interfaces;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Services
{
    public class PostsService : IPostsService
    {
        private IUnitOfWork _uow;

        public PostsService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IEnumerable<Entity> GetPostsByEntityId(Guid entityId)
        {
            return _uow.PostRepository.GetPostsByEntityId(entityId);
        }
    }
}
