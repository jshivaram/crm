﻿using Portal.BLL.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Portal.DAL.Interfaces;
using Microsoft.Xrm.Client.Services;

namespace Portal.BLL.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        private readonly IUnitOfWork _uow;

        public ProjectService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<bool> CantStepAsync()
        {
            return await Task.Run(() => CantStep());
        }

        private bool CantStep()
        {
            var portalTeam = _uow.PortalConfigurationRepository.GetDefaultPortalTeamByContactId(UserId);
            var cantStep = portalTeam?.GetAttributeValue<bool?>("ddsm_cantstep");
            return cantStep ?? false;
        }

        public Task<string> GoToNextStepAsync(Guid? projectId)
        {
            return Task.Run(() => GoToNextStep(projectId));
        }

        public string GoToNextStep(Guid? projectId)
        {
            if (projectId == null || projectId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(projectId));
            }

            var projectRef = new EntityReference("ddsm_project", projectId.Value);

            var actionRequest = new OrganizationRequest("ddsm_DDSMNextStageProjectBusinessProcessFlow")
            {
                ["Target"] = projectRef,
            };

            using (var orgService = new OrganizationService("CrmConnection"))
            {
                OrganizationResponse response = orgService.Execute(actionRequest);
                var firstResult = response.Results.FirstOrDefault();

                string result = firstResult.Value?.ToString();

                return result ?? "";
            }
        }
    }
}
