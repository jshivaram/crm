﻿using Portal.BLL.Interfaces;
using System;
using Portal.Models.EntityModels;
using Portal.DAL.Interfaces;
using Microsoft.Xrm.Sdk;

namespace Portal.BLL.Services
{
    public class RelatedEntityService : BaseService, IRelatedEntityService
    {
        private readonly IUnitOfWork _uow;

        public RelatedEntityService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public Guid CreateRelatedEntityFromForm(RelatedEntityCreationModel relatedEntityCreationModel)
        {
            var entityService = new EntityService(_uow) { UserId = UserId };

            if(relatedEntityCreationModel == null)
            {
                throw new ArgumentNullException(nameof(relatedEntityCreationModel));
            }

            Guid createdEntityId = entityService.CreateAndReturnId(
                relatedEntityCreationModel.JsonEntityData, relatedEntityCreationModel.LogicalName);

            var createdEntityRef = new EntityReference
            {
                LogicalName = relatedEntityCreationModel.LogicalName,
                Id = createdEntityId
            };

            var createdEntityRefCollection = new EntityReferenceCollection { createdEntityRef };

            _uow.EntityRepository.Associate(
                relatedEntityCreationModel.EntityLogicalName,
                relatedEntityCreationModel.EntityId, 
                new Relationship(relatedEntityCreationModel.RelationshipShcemeName),
                createdEntityRefCollection);

            return createdEntityId;
        }
    }
}
