﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.BLL.Interfaces;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Portal.BLL.Services
{
    public class SavedQueryService : BaseService, ISavedQueryService
    {
        private readonly IUnitOfWork _uow;

        public SavedQueryService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public Entity GetView(Guid viewId)
        {
            var result = _uow.EntityRepository.Get(viewId, "savedquery", new ColumnSet(true));
            return result;
        }

        public IEnumerable<object> GetViews(string entityName)
        {
            entityName = entityName.ToLower();

            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);

            if (teamsIdList.Count() == 0)
            {
                throw new Exception("You are not in a team!");
            }

            IEnumerable<Entity> portalViews = _uow.PortalConfigurationRepository
                .GetPortalViewsByTeamIdList(teamsIdList, entityName, new ColumnSet("ddsm_viewid"));

            if (portalViews.Count() == 0)
            {
                throw new Exception("You haven't available views");
            }

            IEnumerable<Guid> viewsIdList = portalViews.Select(
                p => new Guid(p.Attributes["ddsm_viewid"].ToString()));

            IEnumerable<Entity> views = _uow.PortalConfigurationRepository
                .GetViewsByViewsIdList(viewsIdList, new ColumnSet("name"));

            var result = views.Select(v => new
            {
                Value = v.Id,
                Text = v.Attributes["name"]
            }).ToList();

            return result;
        }

        public Guid GetDefaultViewId(string entityLogicalName)
        {
            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);

            Guid? viewId = _uow.PortalConfigurationRepository.GetDefaultViewIdByTeamId(
                teamsIdList.First(), entityLogicalName);

            if (viewId == null)
            {
                throw new Exception("Cant find default view for entity: " + entityLogicalName);
            }

            return viewId.Value;
        }

        public string GetViewXml(Guid viewId)
        {
            var view = _uow.EntityRepository.Get(viewId, "savedquery", new ColumnSet("fetchxml"));

            if (view == null)
            {
                throw new NullReferenceException("View cannot be null.");
            }

            string xml = view["fetchxml"].ToString();

            return xml;
        }

        public Dictionary<string, string> GetFieldsDisplayNames(string entityLogicalName, Guid viewId)
        {
            string viewXml = GetViewXml(viewId);
            IEnumerable<string> propsNames = GetPropNamesFromView(viewXml);

            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach(string prop in propsNames)
            {
                string displayName = _uow.EntityRepository.GetFieldDisplayName(entityLogicalName, prop);
                result.Add(prop, displayName);
            }

            string idDisplay = entityLogicalName + "Id";
            idDisplay = idDisplay.ToLower();

            if (result.Where(r => r.Key.ToLower() ==  idDisplay).Count() == 0)
            {
                result.Add(entityLogicalName.ToLower() + "Id", idDisplay);
            }

            return result;
        }

        public IEnumerable<string> GetPropNamesFromView(string viewXml)
        {
            XElement xelem = XElement.Parse(viewXml);
            XElement firstEntity = xelem.Elements("entity").FirstOrDefault();

            IEnumerable<XElement> attributes = firstEntity.Elements("attribute");
            List<string> propsNames = new List<string>();

            XElement order = firstEntity.Element("order");

            string orderEntityFieldName = order?.Attribute("attribute")?.Value;
            propsNames.Add(orderEntityFieldName);

            propsNames.AddRange(attributes.Select(a => a.Attribute("name")?.Value)
                .Distinct()
                .ToList());

            propsNames = propsNames.Distinct().ToList();

            return propsNames;
        }

        public Guid GetLookupViewId(string entityLogicalName)
        {
            var query = new QueryExpression
            {
                EntityName = "savedquery"
            };

            query.Criteria.AddCondition(
                "returnedtypecode", 
                ConditionOperator.Equal,
                entityLogicalName.ToLower());

            query.Criteria.AddCondition(
                "isdefault",
                ConditionOperator.Equal,
                true
                );

            query.Criteria.AddCondition(
                "querytype",
                ConditionOperator.Equal,
                64
                );

            Guid lookupViewId = _uow.EntityRepository.GetAllWithTotalCount(query).Entities.First().Id;

            return lookupViewId;
        }
    }
}
