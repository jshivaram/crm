﻿using Portal.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portal.DAL.Enums;
using Portal.DAL.Interfaces;

namespace Portal.BLL.Services
{
    public class SecurityService : BaseService, ISecurityService
    {
        private readonly IUnitOfWork _uow;

        public SecurityService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public bool CheckAccess(Guid contactId, Privilege privilege, string entityLogicalName)
        {
            IEnumerable<Guid> teamIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(contactId);
            
            foreach(Guid teamId in teamIdList)
            {
                bool isAccessAllow = _uow.SecurityRepository.CheckAccessByTeamId(teamId, privilege, entityLogicalName);

                if (isAccessAllow == true)
                {
                    return true;
                }
            }

            return false;          
        }
    }
}
