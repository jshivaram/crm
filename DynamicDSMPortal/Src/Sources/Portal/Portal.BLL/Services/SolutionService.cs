﻿using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Services
{
    public class SolutionService : ISolutionService
    {
        private IUnitOfWork _uow;

        public SolutionService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public string GetPortalSolutionVersion()
        {
            string version = _uow.SolutionRepository.GetSolutionVersion("DynamicDSMApplications");
            return version;
        }
    }
}
