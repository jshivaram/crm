﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.BLL.Interfaces;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL.Services
{
    public class ThemeService : BaseService, IThemeService
    {
        private IUnitOfWork _uow;

        public ThemeService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public string GetDefaultThemeName()
        {
            IEnumerable<Guid> teamsIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(UserId);
            IEnumerable<Entity> portalTeams = _uow.PortalConfigurationRepository.GetPortalTeams(teamsIdList);

            IEnumerable<string> defaultThemes =
                portalTeams.Select(pt => pt.GetAttributeValue<EntityReference>("ddsm_defaultportalthemeid")?.Name)
                .ToList();

            string defaultTheme = defaultThemes.FirstOrDefault();
            return defaultTheme;
        }

        public IEnumerable<string> GetThemesNamesList()
        {
            var query = new QueryExpression
            {
                EntityName = "ddsm_portaltheme",
                ColumnSet = new ColumnSet("ddsm_name")
            };

            IEnumerable<Entity> themes = _uow.EntityRepository.GetAllWithTotalCount(query).Entities;
            IEnumerable<string> themesNames = themes.Select(t => t.GetAttributeValue<string>("ddsm_name"))
                                                    .ToList();
            return themesNames;
        }
    }
}
