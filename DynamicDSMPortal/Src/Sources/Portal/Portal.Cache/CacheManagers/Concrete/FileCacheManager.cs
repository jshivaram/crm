﻿using Newtonsoft.Json;
using Portal.Cache.CacheManagers.Interfaces;
using System;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace Portal.Cache.CacheManagers.Concrete
{
    public sealed class FileCacheManager : ICacheManager
    {
        private readonly FileCache _fileCache;

        public FileCacheManager()
        {
            string path = AppDomain.CurrentDomain.GetData("DataDirectory") + "\\Cache";
            _fileCache = new FileCache(path);
        }

        public FileCacheManager(string path)
        {
            _fileCache = new FileCache(path);
        }

        public void ClearAllCache()
        {
            _fileCache.CleanCache();
        }

        public bool Add(string key, object value, int seconds = 1800)
        {
            if(string.IsNullOrEmpty(key) || value == null)
            {
                return false;
            }

            var expirtation = DateTimeOffset.Now.AddSeconds(seconds);
            //if string value
            string serializedValue = value is string ? value.ToString() : JsonConvert.SerializeObject(value);
            return _fileCache.Add(key, serializedValue, expirtation);
        }

        public bool ContainsKey(string key)
        {
            return !string.IsNullOrEmpty(key) && _fileCache.Contains(key);
        }

        public void Delete(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException(nameof(key));
            }

            _fileCache.Remove(key);
        }

        public async Task<T> GetValueAsync<T>(string key)
        {
            return await Task.Run(() => GetValue<T>(key));
        }

        public T GetValue<T>(string key)
        {
            if (!ContainsKey(key))
            {
                return default(T);
            }

            object serializedValueObj = _fileCache.Get(key);
            string serializedValue = serializedValueObj?.ToString();

            if(string.IsNullOrEmpty(serializedValue))
            {
                return default(T);
            }

            if(typeof(T) == typeof(string))
            {
                return (T)serializedValueObj;
            }

            try
            {
                T value = JsonConvert.DeserializeObject<T>(serializedValue);
                return value;
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }
}