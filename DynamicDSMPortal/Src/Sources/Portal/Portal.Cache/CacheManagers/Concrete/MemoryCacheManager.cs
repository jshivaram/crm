﻿using Portal.Cache.CacheManagers.Interfaces;
using System;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace Portal.Cache.CacheManagers.Concrete
{
    public sealed class MemoryCacheManager : ICacheManager
    {
        private readonly MemoryCache _memoryCache = MemoryCache.Default;

        public bool ContainsKey(string key)
        {
            bool result = _memoryCache.Contains(key);
            return result;
        }

        public async Task<T> GetValueAsync<T>(string key)
        {
            return await Task.Run(() => GetValue<T>(key));
        }

        public bool Add(string key, object value, int seconds = 1800)
        {
            var expirtation = DateTimeOffset.Now.AddSeconds(seconds);
            return _memoryCache.Add(key, value, expirtation);
        }

        public void Delete(string key)
        {
            if (_memoryCache.Contains(key))
            {
                _memoryCache.Remove(key);
            }
        }

        public void ClearAllCache()
        {
            throw new NotImplementedException();
        }

        public T GetValue<T>(string key)
        {
            object valueObj = _memoryCache.Get(key);

            if (valueObj is T)
            {
                T result = (T)valueObj;
                return result;
            }

            return default(T);
        }
    }
}