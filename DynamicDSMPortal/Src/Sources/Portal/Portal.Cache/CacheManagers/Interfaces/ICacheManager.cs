﻿using System.Threading.Tasks;

namespace Portal.Cache.CacheManagers.Interfaces
{
    public interface ICacheManager
    {
        bool ContainsKey(string key);
        T GetValue<T>(string key);
        Task<T> GetValueAsync<T>(string key);
        bool Add(string key, object value, int seconds = 1800);
        void Delete(string key);
        void ClearAllCache();
    }
}