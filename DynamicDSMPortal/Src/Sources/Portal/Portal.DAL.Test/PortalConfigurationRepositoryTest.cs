﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;
using Portal.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Test
{
    [TestClass]
    public class PortalConfigurationRepositoryTest
    {
        IUnitOfWork _uow;

        [TestInitialize]
        public void Init()
        {
            string conectionString = ConfigurationManager.ConnectionStrings["CrmConnection"].ConnectionString;
            _uow = new CrmUnitOfWork(conectionString);
        }

        [TestMethod]
        public void PortalConfigurationRepositoryTest_GetPortalFormsByTeamId()
        {
            try
            {
                Guid id = new Guid("b36fa7c4-ea7f-e611-80d4-3a3036663635");
                IEnumerable<Entity> formsList = _uow.PortalConfigurationRepository.GetPortalFormsByTeamId(id, "ddsm_measure", new ColumnSet(true));
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void PortalConfigurationRepositoryTest_GetPortalViewsByTeamId()
        {
            try
            {
                Guid id = new Guid("b36fa7c4-ea7f-e611-80d4-3a3036663635");
                IEnumerable<Entity> viewsList = _uow.PortalConfigurationRepository.GetPortalViewsByTeamId(id, "ddsm_measure", new ColumnSet(true));
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void PortalConfigurationRepositoryTest_GetDefaultViewIdByTeamId()
        {
            try
            {
                Guid id = new Guid("b36fa7c4-ea7f-e611-80d4-3a3036663635");
                Guid? defaultViewId = _uow.PortalConfigurationRepository.GetDefaultViewIdByTeamId(id, "ddsm_measure");
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void PortalConfigurationRepositoryTest_GetViewsByViewsIdList()
        {
            try
            {
                Guid id = new Guid("b36fa7c4-ea7f-e611-80d4-3a3036663635");

                IEnumerable<Entity> portalViewsList = _uow.PortalConfigurationRepository.GetPortalViewsByTeamId(id, "ddsm_measure", new ColumnSet(true));
                IEnumerable<Guid> viewsIdList = portalViewsList.Select(v => new Guid(v.Attributes["ddsm_viewid"].ToString())).ToList();

                IEnumerable<Entity> views = _uow.PortalConfigurationRepository.GetViewsByViewsIdList(viewsIdList, new ColumnSet(true));

                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
        }
    }
}
