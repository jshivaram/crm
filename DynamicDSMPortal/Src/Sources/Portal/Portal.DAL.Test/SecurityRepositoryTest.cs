﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Portal.DAL.Interfaces;
using System.Configuration;
using Portal.DAL.Repositories;
using System.Collections;
using System.Collections.Generic;

namespace Portal.DAL.Test
{
    [TestClass]
    public class SecurityRepositoryTest
    {
        IUnitOfWork _uow;

        [TestInitialize]
        public void Init()
        {
            string conectionString = ConfigurationManager.ConnectionStrings["CrmConnection"].ConnectionString;
            _uow = new CrmUnitOfWork(conectionString);
        }

        [TestMethod]
        public void SecurityRepositoryTest_GetRelatedEntities()
        {
            try
            {
                Guid contactId = new Guid("547875F2-6B7E-E611-80D3-3A3036663635");
                IEnumerable<Guid> teamIdList = _uow.SecurityRepository.GetTeamsIdListByContactId(contactId);
                Assert.IsTrue(true);
            }
            catch(Exception ex)
            {
                Assert.Fail();
            }
        }
    }
}
