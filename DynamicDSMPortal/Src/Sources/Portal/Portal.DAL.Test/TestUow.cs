﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using Portal.DAL.Interfaces;
using Portal.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Microsoft.CSharp;
using System.Dynamic;

namespace Portal.DAL.Test
{
    [TestClass]
    public class TestUow
    {
        IUnitOfWork _uow;

        [TestInitialize]
        public void Init()
        {
            string conectionString = ConfigurationManager.ConnectionStrings["CrmConnection"].ConnectionString;
            _uow = new CrmUnitOfWork(conectionString);
        }

        [TestMethod]
        public void TestUow_GetApplicationMetadata()
        {
            try
            {
                Guid id = new Guid("c293bd34-5294-e611-80d4-3a3036663635");
                var metadata = _uow.MetaDataRepository.GetEntityMetaData("ddsm_application")
                    .Attributes
                    .Where(a => a.AttributeType == Microsoft.Xrm.Sdk.Metadata.AttributeTypeCode.Lookup)
                    //.Where(a => a.LogicalName == "ddsm_tarep")
                    .ToList();
                
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestUow_GetFieldDisplayName()
        {
            string entityName = "account";
            string fieldName = "name";

            try
            {
                string displayName = _uow.EntityRepository.GetFieldDisplayName(entityName, fieldName);
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
        }
    }
}
