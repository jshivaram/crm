﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Portal.DAL.Interfaces;
using Microsoft.Xrm.Client;
using System.Configuration;
using Portal.DAL.Repositories;
using Microsoft.Xrm.Client.Services;
using System.Linq;
using System.Collections.Generic;

namespace Portal.DAL.Test
{
    [TestClass]
    public class UnitOfWorkAccountTest
    {        
        IUnitOfWork _uow;

        [TestInitialize]
        public void Init()
        {
            string conectionString = ConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString;
            _uow = new CrmUnitOfWork(conectionString);
        }

        [TestCleanup]
        public void Distruct()
        {
            _uow.Dispose();
        }        
    }
    
}
