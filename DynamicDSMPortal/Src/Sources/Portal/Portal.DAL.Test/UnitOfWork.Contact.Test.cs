﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Portal.DAL.Interfaces;
using Portal.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Test
{
    [TestClass]
    public class UnitOfWorkContactTest
    {
        IUnitOfWork _uow;

        [TestInitialize]
        public void Init()
        {
            string conectionString = ConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString;
            _uow = new CrmUnitOfWork(conectionString);
        }

        [TestCleanup]
        public void Distruct()
        {
            _uow.Dispose();
        }

    }
}
