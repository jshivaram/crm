﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk.Metadata;
using Portal.DAL.Interfaces;
using Portal.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Test
{
    [TestClass]
    public class UnitOfWorkMetadataTest
    {
        IUnitOfWork _uow;

        [TestInitialize]
        public void Init()
        {
            string conectionString = ConfigurationManager.ConnectionStrings["CrmConnection"].ConnectionString;
            _uow = new CrmUnitOfWork(conectionString);
        }

        [TestMethod]
        public void UnitOfWorkMetadataTest_GetMetaData()
        {
            try
            {
                var result =_uow.MetaDataRepository.GetAttributesMetadata(
                    "ddsm_measuretemplaterelation", 
                    new List<string>
                    {
                        "ddsm_name",
                        "createdon",
                        "statecode",
                        "ddsm_units",
                        "ddsm_measuretemplateid",
                        "ddsm_measuretemplaterelationid"
                    }
                );

                Assert.IsTrue(true);
            }
            catch(Exception ex)
            {
                Assert.Fail();
            }
        }

    }
}
