﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portal.DAL.Interfaces;
using Portal.DAL.Repositories;
using Portal.DAL.Enums;
using System.Configuration;
using Microsoft.Xrm.Sdk;

namespace Portal.DAL.Test
{
	[TestClass]
    public class UnitOfWorkSecurityRepositoryTest
    {
        IUnitOfWork _uow;

        [TestInitialize]
        public void Init()
        {
            string conectionString = ConfigurationManager.ConnectionStrings["CrmConnection"].ConnectionString;
            _uow = new CrmUnitOfWork(conectionString);
        }

        [TestMethod]
        public void UnitOfWorkSecurityTest_CheckAccessByTeamId()
        {
            try
            {
                Guid teamId = new Guid("301c7496-4cfa-e511-80c0-323166616637");
                Portal.DAL.Enums.Privilege priv = Portal.DAL.Enums.Privilege.Create;
                string entityLogicalName = "Account";

                bool isAccessAllow = _uow.SecurityRepository.CheckAccessByTeamId(teamId, priv, entityLogicalName);

                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
        }

    }
}