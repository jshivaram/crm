﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Portal.DAL.Interfaces;
using Portal.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Test
{
    [TestClass]
    public class UnitOfWork_Forms_Test
    {
        IUnitOfWork _uow;

        [TestInitialize]
        public void Init()
        {
            string conectionString = ConfigurationManager.ConnectionStrings["CrmConnection"].ConnectionString;
            _uow = new CrmUnitOfWork(conectionString);
        }

        [TestMethod]
        public void UnitOfWork_Forms_Test_GetAllAccountForms()
        {
            try
            {
                string entityLogicalName = "account";
                var forms = _uow.FormRepository.GetForms(entityLogicalName).ToList();
                Assert.IsTrue(true);
            }
            catch(Exception ex)
            {
                Assert.Fail();
            }            
        }
    }
}
