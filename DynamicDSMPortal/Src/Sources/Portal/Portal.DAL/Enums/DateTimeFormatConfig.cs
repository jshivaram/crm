﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Enums
{
    public enum DateTimeFormatConfig
    {
        BrowserDateTimeFormat = 962080000,
        CRMDateTimeFormat = 962080001,
        CustomDateTimeFormat = 962080002
    }
}
