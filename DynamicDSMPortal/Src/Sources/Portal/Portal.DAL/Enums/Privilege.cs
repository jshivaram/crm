﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Enums
{
    public enum Privilege
    {
        Read = 1,
        Write = 2,
        Append = 4,
        AppendTo = 16,
        Create = 32,
        Delete = 65536,
        Share = 262144,
        Assign = 524288
    }
}
