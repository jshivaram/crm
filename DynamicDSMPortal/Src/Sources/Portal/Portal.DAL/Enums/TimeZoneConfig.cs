﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Enums
{
    public enum TimeZoneConfig
    {
        BrowserTimeZone = 962080000,
        CRMTimeZone = 962080001,
        CustomTimeZone = 962080002
    }
}
