﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL
{
    public enum FormTypeEnum
    {
        FormTypeCreate = 1,
        FormTypeCreateUpdate = 2,
        FormTypeReadonly = 3,
        FormTypeDisabled = 4,
        FormTypeQuickcreate = 5,
        FormTypeBulkEdit = 6
    }
}
