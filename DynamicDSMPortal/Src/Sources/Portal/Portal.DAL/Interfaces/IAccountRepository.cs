﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace Portal.DAL.Interfaces
{
    public interface IAccountRepository
    {
        Guid? GetParenteSiteId(Guid accountId);
        Entity GetParenteSite(Guid accountId, string[] columns = null);
        Entity GetAccountByAccountNumber(string accountNumber, string[] columns = null);
        IReadOnlyList<Entity> GetContractors(string filterValue);
    }
}
