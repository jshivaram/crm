﻿using Microsoft.Xrm.Sdk;
using Portal.Models.AnnotationModels;
using Portal.Models.AttachmentsDocument;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Interfaces
{
    public interface IAnnotationRepository
    {
        IEnumerable<Entity> GetRelatedAttachmentsDocumentEntities(string entityLogicalName, Guid entityId);

        Guid? CreateAnnotation(string file, string fileName, EntityReference entityReference);
        Guid? CreateAnnotation(byte[] file, string fileName, EntityReference entityReference);

        void Remove(Guid id);
        void Remove(IEnumerable<Guid> idList);

        AnnotationDownloadModel DownladAnnotationFile(Guid annotationId);
        void ChangeFileName(Guid annotationId, string fileName);
    }
}
