﻿using System;

namespace Portal.DAL.Interfaces
{
    public interface IApplicationRepository
    {
        Guid ConvertApplication(Guid applicationId);
    }
}
