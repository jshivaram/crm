﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.Models.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Interfaces
{
    public interface IChartRepository
    {
        EntitiesWithCountModel GetAllWithTotalCount(string xml);
        Entity GetVisualizationData(Guid visualizationId);
    }
}
