﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Interfaces
{
    public interface IContactRepository
    {
        Entity Get(Guid id, string[] columns = null);
        Guid? GetDefaultTeamId(Guid contactId);
        Guid? GetDefaultPortalTeamId(Guid contactId);
    }
}
