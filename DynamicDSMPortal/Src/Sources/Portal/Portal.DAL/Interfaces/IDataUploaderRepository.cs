﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Interfaces
{
    public interface IDataUploaderRepository
    {
        Entity GetDataUploaderConfiguration(int typeOfUploadedData);
        void CreateDataUploaderRecord(int typeofUploadedData, EntityReference dataUploaderConfiguration, Entity adminData, byte[] file, string fileName);
    }
}
