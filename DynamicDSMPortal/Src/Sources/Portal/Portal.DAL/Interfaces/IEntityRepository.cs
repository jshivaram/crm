﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.Models.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Interfaces
{
    public interface IEntityRepository
    {
        EntitiesWithCountModel GetAllWithTotalCount(QueryExpression query);

        Entity Get(Guid id, string type, ColumnSet columnSet = null);

        void Create(Entity entity);
        Guid CreateAndReturnId(Entity entity);
        void Update(Entity entity);
        void Delete(string entityLogicalName, Guid id);

        int Count(string entityLogicalName);

        IEnumerable<Entity> GetRelatedEntities(Entity entity, string relatedEntityName, string key);

        EntitiesWithCountModel GetRelatedEntitiesWithCount(
            QueryExpression query,
            string relationshipShcemeName,
            EntityReference entityReference);

        void Associate(string logicalName, Guid entityId, 
            Relationship relationship, EntityReferenceCollection relatedEntities);
        void Disassociate(string logicalName, Guid entityId, Relationship relationship, EntityReferenceCollection relatedEntities);

        string GetFieldDisplayName(string entityLogicalName, string fieldName);
    }
}
