﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Interfaces
{
    public interface IFormRepository
    {
        IEnumerable<Entity> GetForms(string entityLogicalName=null, int formType=2);
        Entity GetFormById(string entityLogicalName, Guid formId, FormTypeEnum formType = FormTypeEnum.FormTypeCreateUpdate);
        IEnumerable<Entity> GetApplicationsFormListForUnregisters();
        Entity GetFormById(Guid formId, FormTypeEnum formType = FormTypeEnum.FormTypeCreateUpdate);
    }
}
