﻿using Portal.Models.GlobalSearch;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Portal.DAL.Interfaces
{
    public interface IGlobalSearchRepository
    {
        Task<IReadOnlyList<GlobalSearchEntity>> GetGlobalSearchConfigAsync(Guid contactId);
    }
}