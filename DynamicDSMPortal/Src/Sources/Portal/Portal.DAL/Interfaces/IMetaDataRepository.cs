﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Interfaces
{
    public interface IMetaDataRepository
    {
        EntityMetadata GetEntityMetaData(string entityLogicalName);
        RetrieveAttributeResponse GetAttributeMetadata(string entityLogicalName, string attributeLogicalName);
        string GetProcesStageByEntityId(string entityLogicalName);

        IEnumerable<AttributeMetadata> GetAttributesMetadata(
            string entityLogicalName, IEnumerable<string> attributesLogicalNames);
    }
}
