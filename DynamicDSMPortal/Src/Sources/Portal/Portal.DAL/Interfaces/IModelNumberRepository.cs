﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Portal.Models;

namespace Portal.DAL.Interfaces
{
    public interface IModelNumberRepository
    {
        Task<IReadOnlyList<LookupModel>> GetModelNumbersForLookupByEfficiencyAsync(int efficiency);
    }
}
