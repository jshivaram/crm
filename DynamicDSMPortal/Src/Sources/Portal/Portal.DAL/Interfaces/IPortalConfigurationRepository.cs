﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Interfaces
{
    public interface IPortalConfigurationRepository
    {
        IEnumerable<Entity> GetPortalFormsByTeamIdList(IEnumerable<Guid> teamIdList, string entityLogicalName, ColumnSet columnSet);
        IEnumerable<Entity> GetPortalViewsByTeamIdList(IEnumerable<Guid> teamIdList, string entityLogicalName, ColumnSet columnSet);

        IEnumerable<Entity> GetPortalFormsByTeamId(Guid teamId, string entityLogicalName, ColumnSet culumnSet);
        IEnumerable<Entity> GetPortalViewsByTeamId(Guid teamId, string entityLogicalName, ColumnSet columnSet);

        IEnumerable<Entity> GetFormsByFormsIdList(IEnumerable<Guid> formsIdList, ColumnSet columnSet);
        IEnumerable<Entity> GetViewsByViewsIdList(IEnumerable<Guid> viewsIdList, ColumnSet columnSet);

        Guid? GetDefaultFormIdByTeamId(Guid teamId, string entityLogicalName);
        Guid? GetDefaultViewIdByTeamId(Guid teamId, string entityLogicalName);
        IEnumerable<Entity> GetPortalEntitiesListForNavigation(IEnumerable<Guid> teamIdList, ColumnSet columnSet);
        IEnumerable<Entity> GetPortalTeams(IEnumerable<Guid> teamIdList);

        IEnumerable<Entity> GetPortalDashboardsByTeamIdList(IEnumerable<Guid> teamIdList, ColumnSet columnSet);
        IEnumerable<Guid> GetDefaultDashboardsIdByTeamIdList(IEnumerable<Guid> teamIdList);
        Guid? GetDefaultPortalTeamIdByContactId(Guid contactId);
        Guid? GetTeamIdByPortalTeamId(Guid portalTeamId);
        Entity GetDefaultPortalTeamByContactId(Guid contactId);

        IEnumerable<Guid> GetAccountIdListByPortalTeamIdList(IEnumerable<Guid> portalTeamIdList);
        IEnumerable<Entity> GetPortalEntitiesListForNavigation(Guid teamId,
            ColumnSet columnSet);
    }
}
