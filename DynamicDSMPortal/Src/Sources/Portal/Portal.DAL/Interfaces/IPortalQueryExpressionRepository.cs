﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace Portal.DAL.Interfaces
{
    public interface IPortalQueryExpressionRepository
    {
        IReadOnlyList<Entity> GetPortalQueryExpressions(string entityLogicalName, List<Guid> portalTeamsIdList = null);
    }
}