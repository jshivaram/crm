﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portal.DAL.Enums;
using Microsoft.Xrm.Sdk;

namespace Portal.DAL.Interfaces
{
    public interface ISecurityRepository
    {
        bool CheckAccessByTeamId(Guid teamId, Privilege privilege, string entityLogicalName);
        IEnumerable<Guid> GetTeamsIdListByContactId(Guid contactId);
    }
}
