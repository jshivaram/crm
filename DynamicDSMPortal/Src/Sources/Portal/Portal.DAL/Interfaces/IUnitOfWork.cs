﻿using System;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;

namespace Portal.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IContactRepository ContactRepository { get; }

        //views
        ISavedQueryRepository SavedQueryRepository { get; }
        //data uploader views
        IUserQueryRepository UserQueryRepository { get; }

        IFormRepository FormRepository { get; }
        IMetaDataRepository MetaDataRepository { get; }

        IEntityRepository EntityRepository { get; }

        ISecurityRepository SecurityRepository { get; }

        IAdminDataRepository AdminDataRepository { get; }
        IDataUploaderRepository DataUploaderRepository { get; }


        IPortalConfigurationRepository PortalConfigurationRepository { get; }

        IChartRepository ChartRepository { get; }

        ISolutionRepository SolutionRepository { get; }

        IPostsRepository PostRepository { get; }
    }
}