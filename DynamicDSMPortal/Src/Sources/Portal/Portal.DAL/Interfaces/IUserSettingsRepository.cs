﻿using Portal.Models.UserSettings;
using System;
using System.Globalization;

namespace Portal.DAL.Interfaces
{
    public interface IUserSettingsRepository
    {
        DateTimeFormatModel GetDateTimeFormat(Guid userId, CultureInfo browserCulture);
        string GetTimeZone(Guid userId, string browserIanaTimeZone);
        TimeZoneInfo GetTimeZoneByCRMConnectionWithUtc();
    }
}
