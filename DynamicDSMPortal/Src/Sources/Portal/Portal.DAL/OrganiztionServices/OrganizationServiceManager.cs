﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Linq;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Portal.Logger;

namespace Portal.DAL.OrganiztionServices
{
    public static class OrganizationServiceManager
    {
        private const string ConnectionStringName = "CrmConnection";
        private static string _currentConnectionStringName = ConnectionStringName;

        private static OrganizationService _organizationService;
        private static DateTime _createdOn;

        private static readonly ConcurrentDictionary<string, long> ConnectionStringsStatistic = new ConcurrentDictionary<string, long>();

        static OrganizationServiceManager()
        {
            foreach (ConnectionStringSettings conn in ConfigurationManager.ConnectionStrings)
            {
                if (conn?.Name?.Contains(ConnectionStringName) == true)
                {
                    ConnectionStringsStatistic.TryAdd(conn.Name, 0);
                }
            }
        }

        public static OrganizationService OrganizationService
        {
            get
            {
                if (_organizationService == null)
                {
                    _RecreateOrgnizarionService();
                }

                var crmConnection = new CrmConnection(_currentConnectionStringName);

                var lifeTime = DateTime.Now - _createdOn;

                if (lifeTime >= crmConnection.Timeout)
                {
                    _RecreateOrgnizarionService();
                }

                return _organizationService;
            }
        }

        private static void _RecreateOrgnizarionService()
        {
            string connectionStringName = ConnectionStringsStatistic.OrderByDescending(cs => cs.Value).Select(cs => cs.Key).FirstOrDefault();

            if (connectionStringName == null)
            {
                var exception = new Exception("Can`t find CRM coonection strings.");
                LoggerManager.LogException(exception);
                throw exception;
            }

            ConnectionStringsStatistic[connectionStringName]++;
            _currentConnectionStringName = connectionStringName;

            _createdOn = DateTime.Now;
            _organizationService = new OrganizationService(connectionStringName);
        }

        public static void Dispose()
        {
            _organizationService.Dispose();
        }
    }
}