﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;

namespace Portal.DAL.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly IOrganizationService _orgService;

        public AccountRepository(IOrganizationService orgService)
        {
            _orgService = orgService;
        }

        public IReadOnlyList<Entity> GetContractors(string filterValue)
        {
            var query = new QueryExpression("account")
            {
                ColumnSet = new ColumnSet("name")
            };

            var filter = new FilterExpression(LogicalOperator.Or);
            filter.AddCondition("ddsm_recordtypemultiselect", ConditionOperator.Like, "%Delivery Agent%");
            filter.AddCondition("ddsm_recordtypemultiselect", ConditionOperator.Like, "%Partner%");

            query.Criteria.AddFilter(filter);

            if (!string.IsNullOrEmpty(filterValue))
            {
                query.Criteria.AddCondition("name", ConditionOperator.BeginsWith, filterValue);
            }

            DataCollection<Entity> result = _orgService.RetrieveMultiple(query).Entities;
            return result;
        }

        public Guid? GetParenteSiteId(Guid accountId)
        {
            var query = new QueryExpression("ddsm_site");
            query.Criteria.AddCondition("ddsm_parentaccount", ConditionOperator.In, new[] { accountId.ToString() });

            var entities = _orgService.RetrieveMultiple(query).Entities;
            var parentSiteId = entities.FirstOrDefault()?.Id;

            return parentSiteId;
        }

        public Entity GetParenteSite(Guid accountId, string[] columns = null)
        {
            var query = new QueryExpression("ddsm_site");

            if (columns?.Length > 0)
            {
                query.ColumnSet = new ColumnSet(columns);
            }

            query.Criteria.AddCondition("ddsm_parentaccount", ConditionOperator.In, new[] { accountId.ToString() });

            var entities = _orgService.RetrieveMultiple(query).Entities;
            var parentSite = entities.FirstOrDefault();

            return parentSite;
        }

        public Entity GetAccountByAccountNumber(string accountNumber, string[] columns = null)
        {
            if (string.IsNullOrEmpty(accountNumber))
            {
                return null;
            }

            var query = new QueryExpression("account");

            if (columns?.Length > 0)
            {
                query.ColumnSet = new ColumnSet(columns);
            }

            query.Criteria.AddCondition("accountnumber", ConditionOperator.Equal, accountNumber);

            var account = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();
            return account;
        }
    }
}