﻿using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Repositories
{
    public class AdminDataRepository : IAdminDataRepository
    {
        private readonly OrganizationService _orgService;

        public AdminDataRepository(OrganizationService orgService)
        {
            _orgService = orgService;
        }

        public Entity GetAdminDataForDataUploader()
        {
            string entityLogicalName = "ddsm_admindata";

            FilterExpression filter = new FilterExpression();
            filter.Conditions.Add(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
            filter.Conditions.Add(new ConditionExpression("ddsm_name", ConditionOperator.Equal, "Admin Data"));

            QueryExpression query = new QueryExpression
            {
                EntityName = entityLogicalName,
                ColumnSet = new ColumnSet("ddsm_deduplicationrules",
                                          "ddsm_duplicatedetection",
                                          "ddsm_datauploaderdevmode",
                                          "ddsm_globalrequestscount",
                                          "ddsm_globalpagerecordscount",
                                          "ddsm_startdatarowdu"),
                Criteria = filter
            };

            EntityCollection entities = _orgService.RetrieveMultiple(query);
            Entity entity = entities.Entities.FirstOrDefault();
            return entity;
        }
    }
}
