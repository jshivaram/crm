﻿using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;
using Portal.Models.AnnotationModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Portal.DAL.Repositories
{
    public class AnnotationRepository : IAnnotationRepository
    {
        private readonly OrganizationService _orgService;
        private const string AnnotationLogicalName = "annotation";
        private const string AttachmentsDocumentLogicalName = "ddsm_attachmentdocument";

        public AnnotationRepository(OrganizationService orgService)
        {
            _orgService = orgService;
        }

        public void ChangeFileName(Guid attachmentDocumentId, string fileName)
        {
            if (attachmentDocumentId == Guid.Empty || string.IsNullOrEmpty(fileName))
            {
                return;
            }

            Entity attachmentsDocument = _orgService
                .Retrieve(AttachmentsDocumentLogicalName, attachmentDocumentId, new ColumnSet("ddsm_name"));

            var annotationQuery = new QueryExpression(AnnotationLogicalName) { ColumnSet = new ColumnSet("filename", "subject") };

            annotationQuery.Criteria.AddCondition("objectid", ConditionOperator.Equal, attachmentDocumentId);
            annotationQuery.Criteria.AddCondition("objecttypecode", ConditionOperator.Equal, AttachmentsDocumentLogicalName);

            Entity annotationEntity = _orgService.RetrieveMultiple(annotationQuery).Entities.FirstOrDefault();

            if (attachmentsDocument == null || annotationEntity == null)
            {
                return;
            }

            attachmentsDocument["ddsm_name"] = fileName;
            _orgService.Update(attachmentsDocument);

            annotationEntity["subject"] = fileName;
            annotationEntity["filename"] = fileName;

            _orgService.Update(annotationEntity);
        }

        public AnnotationDownloadModel DownladAnnotationFile(Guid attachmentsDocumentId)
        {
            if (attachmentsDocumentId == Guid.Empty)
            {
                return null;
            }

            var query = new QueryExpression(AnnotationLogicalName);
            query.ColumnSet = new ColumnSet("documentbody", "filename", "subject");

            query.Criteria.AddCondition(
                "objectid",
                ConditionOperator.Equal,
                attachmentsDocumentId);

            query.Criteria.AddCondition(
                "objecttypecode",
                ConditionOperator.Equal,
                AttachmentsDocumentLogicalName);

            Entity annotationEntity = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            if (annotationEntity == null)
            {
                return null;
            }

            var annotation = new AnnotationModel(annotationEntity);

            var downloadModel = new AnnotationDownloadModel
            {
                FileName = !string.IsNullOrEmpty(annotation.FileName) ? annotation.FileName : annotation.Subject,
                FileContent = annotation.DocumentBody
            };

            return downloadModel;
        }

        public Guid? CreateAnnotation(string file, string fileName, EntityReference entityReference)
        {
            if (string.IsNullOrEmpty(file)
                || entityReference == null
                || entityReference.Id == Guid.Empty
                || string.IsNullOrEmpty(entityReference.LogicalName))
            {
                return null;
            }

            var attachDocumentEntity = new Entity(AttachmentsDocumentLogicalName) { ["ddsm_name"] = fileName };

            Guid attachDocumentEntityId = _orgService.Create(attachDocumentEntity);

            var relatedEntities = new EntityReferenceCollection { entityReference };

            string schemaName = "ddsm_attachdoc_" + entityReference.LogicalName;

            var relationship = new Relationship(schemaName);
            _orgService.Associate(AttachmentsDocumentLogicalName, attachDocumentEntityId, relationship, relatedEntities);

            var attachDocumentRef = new EntityReference(attachDocumentEntity.LogicalName, attachDocumentEntityId);

            var annotation = new AnnotationModel
            {
                DocumentBody = file,
                IsDocument = true,
                FileName = fileName,
                RelatedEntity = attachDocumentRef,
                RelatedEntityLogicalName = attachDocumentRef.LogicalName
            };

            Entity annotationEntity = annotation.ConvertToCrmEntity();

            Guid annotationId = _orgService.Create(annotationEntity);


            //Hardcode for Arizona demo
            //var queryRequiredDocs = new QueryExpression("ddsm_requireddocument");
            //queryRequiredDocs.ColumnSet = new ColumnSet(true);
            //queryRequiredDocs.Criteria.AddCondition("ddsm_subdivisionid", ConditionOperator.Equal, entityReference.Id);

            //var reqDocs = _orgService.RetrieveMultiple(queryRequiredDocs).Entities;

            //var collection = new EntityReferenceCollection();


            //foreach (var doc in reqDocs)
            //{
            //    if (doc == null)
            //    {
            //        continue;
            //    }
            //    collection.Add(doc.ToEntityReference());
            //    doc["ddsm_uploaded"] = true;
            //    _orgService.Update(doc);
            //}

            //if (collection.Any())
            //{
            //    _orgService.Associate(AttachmentsDocumentLogicalName, attachDocumentEntityId, new Relationship("ddsm_attachdoc_ddsm_requireddocument"), collection);
            //}

            return annotationId;
        }

        public Guid? CreateAnnotation(byte[] file, string fileName, EntityReference entityReference)
        {
            if (file == null)
            {
                return null;
            }

            string convertedFile = Convert.ToBase64String(file);
            return CreateAnnotation(convertedFile, fileName, entityReference);
        }

        public void Remove(Guid id)
        {
            _orgService.Delete(AttachmentsDocumentLogicalName, id);
        }

        public void Remove(IEnumerable<Guid> idList)
        {
            if (idList == null)
            {
                return;
            }

            foreach (Guid id in idList)
            {
                Remove(id);
            }
        }

        public IEnumerable<Entity> GetRelatedAttachmentsDocumentEntities(string entityLogicalName, Guid entityId)
        {
            if (string.IsNullOrEmpty(entityLogicalName) || entityId == Guid.Empty)
            {
                return new List<Entity>();
            }

            var query = new QueryExpression(AttachmentsDocumentLogicalName) { ColumnSet = new ColumnSet(true) };

            string schemaName = "ddsm_attachdoc_" + entityLogicalName;

            string fieldIdAttachDocs = AttachmentsDocumentLogicalName + "id";
            var attachDocsLink = new LinkEntity(
                AttachmentsDocumentLogicalName, schemaName,
                fieldIdAttachDocs, fieldIdAttachDocs,
                JoinOperator.Inner);

            string fieldIdEntity = entityLogicalName + "id";
            var entityLink = new LinkEntity(
                schemaName, entityLogicalName,
                fieldIdEntity, fieldIdEntity,
                JoinOperator.Inner);

            entityLink.LinkCriteria.AddCondition(fieldIdEntity, ConditionOperator.Equal, entityId);

            attachDocsLink.LinkEntities.Add(entityLink);
            query.LinkEntities.Add(attachDocsLink);

            query.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);

            IEnumerable<Entity> entities = _orgService.RetrieveMultiple(query).Entities;
            return entities;
        }
    }
}
