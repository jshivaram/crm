﻿using System;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using Portal.DAL.Interfaces;

namespace Portal.DAL.Repositories
{
    public class ApplicationRepository : IApplicationRepository
    {
        private readonly IOrganizationService _orgService;
        private const string EntityName = "ddsm_application";

        public ApplicationRepository(IOrganizationService orgService)
        {
            _orgService = orgService;
        }

        public Guid ConvertApplication(Guid applicationId)
        {
            var convertRequest = new OrganizationRequest("ddsm_DDSMApplicationStart")
            {
                Parameters = new ParameterCollection
                {
                    {"Target", new EntityReference(EntityName, applicationId)}
                }
            };

            OrganizationResponse respone = _orgService.Execute(convertRequest);
            ParameterCollection results = respone.Results;

            object complete = results["Complete"];

            if (complete == null)
            {
                throw new Exception("You cannot convert an Application");
            }

            if (complete is bool && (bool) complete)
            {
                var createdRecordJson = results["Result"].ToString();
                dynamic dynamic = JsonConvert.DeserializeObject(createdRecordJson);
                return dynamic.CreatedRecord.Id;
            }

            string msg = results["Result"].ToString();
            throw new Exception(msg);
        }        
    }
}