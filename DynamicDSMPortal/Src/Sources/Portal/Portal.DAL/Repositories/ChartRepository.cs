﻿using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;
using Portal.Models.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Portal.DAL.Repositories
{
    public class ChartRepository : IChartRepository
    {
        private readonly OrganizationService _orgService;

        public ChartRepository(OrganizationService orgService)
        {
            _orgService = orgService;
        }

        public EntitiesWithCountModel GetAllWithTotalCount(string xml)
        {
            EntityCollection entityCollection = _orgService.RetrieveMultiple(new FetchExpression(xml));

            EntitiesWithCountModel result = new EntitiesWithCountModel
            {
                Entities = entityCollection.Entities,
                Count = entityCollection.TotalRecordCount
            };

            return result;
        }

        public Entity GetVisualizationData(Guid visualizationId)
        {
            Entity savedQueryVisualization = _orgService.Retrieve(
                "savedqueryvisualization", visualizationId, 
                new ColumnSet("name", "presentationdescription","datadescription"));
            return savedQueryVisualization;
        }
    }
}
