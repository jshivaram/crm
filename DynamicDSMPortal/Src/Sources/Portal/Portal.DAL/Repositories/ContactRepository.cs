﻿using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Repositories
{
    public class ContactRepository : IContactRepository
    {
        private OrganizationService _orgService;

        public ContactRepository(OrganizationService orgService)
        {
            _orgService = orgService;
        }

        public Entity Get(Guid id, string[] columns = null)
        {
            var columnSet = new ColumnSet(true);

            if(columns != null && columns.Count() > 0)
            {
                columns = columns.Where(c => !string.IsNullOrEmpty(c)).ToArray();//remove empty columns
                columnSet = new ColumnSet(columns);
            }

            Entity contact = _orgService.Retrieve("contact", id, columnSet);
            return contact;
        }

        public Guid? GetDefaultTeamId(Guid contactId)
        {
            Entity contact = Get(contactId, new string[] { "ddsm_defaultportalteamid" });
            if(contact == null)
            {
                return null;
            }

            EntityReference portaTeamRef = contact.GetAttributeValue<EntityReference>("ddsm_defaultportalteamid");
            return portaTeamRef?.Id;
        }

        public Guid? GetDefaultPortalTeamId(Guid contactId)
        {
            Guid? teamId = GetDefaultTeamId(contactId);
            
            if(teamId == null)
            {
                return null;
            }

            var query = new QueryExpression("ddsm_portalteam");
            query.Criteria.AddCondition("ddsm_teamid", ConditionOperator.Equal, teamId.ToString());

            Guid? defaultPortalteamId = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault()?.Id;
            return defaultPortalteamId;
        }
    }
}
