﻿using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;
using Portal.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Repositories
{
    public class CrmUnitOfWork : IUnitOfWork
    {
        private CrmConnection _connection;
        private OrganizationService _orgService;

        private IContactRepository _contactRepository;

        private ISavedQueryRepository _savedQueryRepository;
        private IUserQueryRepository _userQueryRepository;

        private IMetaDataRepository _metaDataRepository;
        private IFormRepository _formRepository;

        private IEntityRepository _entityRepository;

        private ISecurityRepository _securityRepository;

        private IAdminDataRepository _adminDataRepository;

        private IDataUploaderRepository _dataUploaderRepository;

        private IPortalConfigurationRepository _portalConfigurationRepository;

        private IChartRepository _chartRepository;

        private ISolutionRepository _solutionRepository;

        private IPostsRepository _postRepository;

        public CrmUnitOfWork(OrganizationService orgService)
        {
            _orgService = orgService;
        }
        
        public CrmUnitOfWork(string connectionString)
        {
            _connection = CrmConnection.Parse(connectionString);
            _orgService = new OrganizationService(_connection);
        }

        public IPortalConfigurationRepository PortalConfigurationRepository
        {
            get
            {
                if(_portalConfigurationRepository == null)
                {
                    _portalConfigurationRepository = new PortalConfigurationRepository(_orgService);
                }

                return _portalConfigurationRepository;
            }
        }

        public IFormRepository FormRepository
        {
            get
            {
                if (_formRepository == null)
                {
                    _formRepository = new FormRepository(_orgService);
                }

                return _formRepository;
            }
        }       

        public IMetaDataRepository MetaDataRepository
        {
            get
            {
                if (_metaDataRepository == null)
                {
                    _metaDataRepository = new MetaDataRepository(_orgService);
                }

                return _metaDataRepository;
            }
        }

        public IEntityRepository EntityRepository
        {
            get
            {
                if(_entityRepository == null)
                {
                    _entityRepository = new EntityRepository(_orgService);
                }

                return _entityRepository;
            }
        }

        public ISecurityRepository SecurityRepository
        {
            get
            {
                if (_securityRepository == null)
                {
                    _securityRepository = new SecurityRepository(_orgService);
                }

                return _securityRepository;
            }
        }

        public IAdminDataRepository AdminDataRepository
        {
            get
            {
                if(_adminDataRepository == null)
                {
                    _adminDataRepository = new AdminDataRepository(_orgService);
                }

                return _adminDataRepository;
            }
        }

        public IDataUploaderRepository DataUploaderRepository
        {
            get
            {
                if(_dataUploaderRepository == null)
                {
                    _dataUploaderRepository = new DataUploaderRepository(_orgService);
                }

                return _dataUploaderRepository;
            }
        }

        public IChartRepository ChartRepository
        {
            get
            {
                if (_chartRepository == null)
                {
                    _chartRepository = new ChartRepository(_orgService);
                }

                return _chartRepository;
            }
        }



        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {

                    if(_orgService != null)
                    {
                        _orgService.Dispose();
                    }                    
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public ISolutionRepository SolutionRepository
        {
            get
            {
                if(_solutionRepository == null)
                {
                    _solutionRepository = new SolutionRepository(_orgService);
                }

                return _solutionRepository;
            }
        }

        public IPostsRepository PostRepository
        {
            get
            {
                if(_postRepository == null)
                {
                    _postRepository = new PostsRepository(_orgService);
                }

                return _postRepository;
            }
        }

        public ISavedQueryRepository SavedQueryRepository
        {
            get
            {
                if(_savedQueryRepository == null)
                {
                    _savedQueryRepository = new SavedQueryRepository(_orgService);
                }

                return _savedQueryRepository;
            }
        }

        public IUserQueryRepository UserQueryRepository
        {
            get
            {
                if(_userQueryRepository == null)
                {
                    _userQueryRepository = new UserQueryRepository(_orgService);
                }

                return _userQueryRepository;
            }
        }

        public IContactRepository ContactRepository
        {
            get
            {
                if(_contactRepository == null)
                {
                    _contactRepository = new ContactRepository(_orgService);
                }

                return _contactRepository;
            }
        }
    }
}
