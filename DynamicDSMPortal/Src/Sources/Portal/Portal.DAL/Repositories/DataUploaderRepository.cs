﻿using Excel;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Repositories
{
    public class DataUploaderRepository : IDataUploaderRepository
    {
        private readonly OrganizationService _orgService;

        public DataUploaderRepository(OrganizationService orgService)
        {
            _orgService = orgService;
        }

        public Entity GetDataUploaderConfiguration(int typeOfUploadedData)
        {
            string entityLogicalName = "ddsm_datauploaderconfiguration";

            FilterExpression filter = new FilterExpression();
            filter.Conditions.Add(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
            filter.Conditions.Add(new ConditionExpression("ddsm_typeofuploadeddata", ConditionOperator.Equal, typeOfUploadedData));

            QueryExpression query = new QueryExpression
            {
                EntityName = entityLogicalName,
                ColumnSet = new ColumnSet("ddsm_datauploaderconfigurationid", "ddsm_name"),
                Criteria = filter
            };

            EntityCollection entities = _orgService.RetrieveMultiple(query);
            Entity entity = entities.Entities.FirstOrDefault();
            return entity;
        }

        public void CreateDataUploaderRecord(int typeofUploadedData, EntityReference dataUploaderConfiguration, Entity adminData, byte[] file, string fileName)
        {
            Entity dataUploader = new Entity("ddsm_datauploader");
            dataUploader.Attributes["ddsm_name"] = fileName;
            dataUploader.Attributes["ddsm_typeofuploadeddata"] = new OptionSetValue(typeofUploadedData);
            dataUploader.Attributes["ddsm_statusfiledatauploading"] = new OptionSetValue(962080000);
            dataUploader.Attributes["ddsm_datauploaderconfiguration"] = dataUploaderConfiguration;

            //когда 0 - не форм как в документе - Юрына Калькуляция 1 - Юра не обращает внимания и все-равно считает 2 - форс
            dataUploader.Attributes["ddsm_esprecalculationcode"] = 962080001;//Расхардкодить в данном случае force_esp

            dataUploader.Attributes["ddsm_deduplicationrules"] = adminData.Attributes["ddsm_deduplicationrules"];
            dataUploader.Attributes["ddsm_duplicatedetection"] = adminData.Attributes["ddsm_duplicatedetection"];
            dataUploader.Attributes["ddsm_fileparsed"] = false;
            dataUploader.Attributes["ddsm_fileuploaded"] = false;

            dataUploader.Attributes["ddsm_startdatarowdu"] = adminData.Attributes["ddsm_startdatarowdu"];
            dataUploader.Attributes["ddsm_globalrequestscount"] = adminData.Attributes["ddsm_globalrequestscount"];
            dataUploader.Attributes["ddsm_globalpagerecordscount"] = adminData.Attributes["ddsm_globalpagerecordscount"];
            dataUploader.Attributes["ddsm_datauploaderdevmode"] = adminData.Attributes["ddsm_datauploaderdevmode"];

            Guid dataUploaderId = new Guid();
            Guid noteId = new Guid();

            try
            {
                dataUploaderId = _orgService.Create(dataUploader);
                noteId = CreateNote(new EntityReference("ddsm_datauploader", dataUploaderId), file, fileName);

                Entity note = _orgService.Retrieve("annotation", noteId, new ColumnSet(true));

                Entity dataUploaderForUpdate = new Entity("ddsm_datauploader", dataUploaderId);
                dataUploaderForUpdate.Attributes["ddsm_fileuploaded"] = true;
                dataUploaderForUpdate.Attributes["ddsm_statusfiledatauploading"] = new OptionSetValue(962080001);
                _orgService.Update(dataUploaderForUpdate);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //private async Task ParseFile(string fileName, byte[] fileContent, Guid dataUploaderId)
        //{
        //    Stream msFile = new MemoryStream(fileContent);

        //    if(Path.GetExtension(fileName) == ".xlsx")
        //    {
        //        throw new Exception("Parse only xls files.");
        //    }

        //    IExcelDataReader excelReader2007 = ExcelReaderFactory.CreateBinaryReader(msFile);

        //    var resultNotCleared = excelReader2007.AsDataSet();
        //    excelReader2007.Close();

        //    DataSet result = new DataSet();

        //    foreach (DataTable _dt in resultNotCleared.Tables)
        //    {
        //        //var filteredValues = _dt.AsEnumerable().Where(row => row.ItemArray.Any(col => !String.IsNullOrWhiteSpace(col.ToString()))).ToArray();
        //        var filteredValues = _dt.AsEnumerable().Where(row => row.Field<dynamic>("A") != null && string.Compare((Convert.ToString(row.Field<dynamic>("A"))).Trim(), string.Empty) != 0).ToArray();
        //        var DT = filteredValues.CopyToDataTable();
        //        DT.TableName = _dt.TableName;
        //        result.Tables.Add(DT);
        //    }

        //    //Clear resultNotCleared (XLSX) DataSet
        //    resultNotCleared.Tables.Clear();
        //    resultNotCleared.Dispose();
        //    resultNotCleared = null;

        //    ExecuteMultipleRequest emRequest = new ExecuteMultipleRequest
        //    {
        //        Requests = new OrganizationRequestCollection(),
        //        Settings = new ExecuteMultipleSettings
        //        {
        //            ContinueOnError = false,
        //            ReturnResponses = true
        //        }
        //    };

        //    int NumberofRecords = 0;
        //    int itemsResponceCount = 0;
        //    int itemsRequestCount = 0;
        //    bool devMode = false;
        //    int requestSets = 1;
        //    int itemsErrorCount = 0;

        //    foreach (DataTable _dt in result.Tables)
        //    {
        //        if (_dt.Rows.Count == 0) continue;
        //        int SheetDataRow = 0;
        //        foreach (DataRow _dr in _dt.Rows)
        //        {
        //            SheetDataRow++;
        //            if (SheetDataRow >= 2)
        //            {
        //                Entity newEntity = new Entity("ddsm_exceldata");
        //                newEntity["ddsm_name"] = _dt.TableName;
        //                newEntity["ddsm_statusxlsrecord"] = new OptionSetValue((int)StatusXLSRecord.NotProcessed);
        //                newEntity["ddsm_datauploaderfile"] = new EntityReference("ddsm_datauploader", dataUploaderId);
        //                foreach (var attr in objects[_dt.TableName].Attrs)
        //                {
        //                    if (_dr[attr.Key].ToString() != "")
        //                    {
        //                        if (attr.Value.AttrType.ToString() == "DateTime")
        //                            newEntity["ddsm_" + (attr.Key).ToLower()] = (_DataUploader.convertAttributToDateTime(_dr[attr.Key])).ToString();
        //                        else
        //                            newEntity["ddsm_" + (attr.Key).ToLower()] = (_dr[attr.Key]).ToString();
        //                    }
        //                }
        //                CreateRequest createRequest = new CreateRequest();
        //                createRequest.Target = newEntity;
        //                emRequest.Requests.Add(createRequest);
        //                itemsRequestCount++;

        //                //Create Multiple
        //                if (itemsRequestCount == (requestSets * _DataUploader.globalRequestsCount))
        //                {
        //                    ExecuteMultipleRequestTime = _DataUploader.convertAttributToDateTime("today");

        //                    ExecuteMultipleResponse emResponse = (ExecuteMultipleResponse)_orgService.Execute(emRequest);

        //                    foreach (ExecuteMultipleResponseItem responseItem in emResponse.Responses)
        //                    {
        //                        // A valid response.
        //                        if (responseItem.Response != null)
        //                        {
        //                            itemsResponceCount++;
        //                        }

        //                        // An error has occurred.
        //                        else if (responseItem.Fault != null)
        //                        {
        //                            itemsErrorCount++;
        //                        }
        //                    }

        //                    ExecuteMultipleResponseTime = _DataUploader.convertAttributToDateTime("today");
        //                    if (devMode)
        //                        _DataUploader.createDevStatistics(_orgService, recordUploaded, ExecuteMultipleRequestTime, ExecuteMultipleResponseTime, (int)DataUploaderTypeRecord.ExcelData, (int)ParsedUploaded.Parsed, (int)DataUploaderMethod.CreateRecord, emRequest.Requests.Count, true);
        //                    NumberofRecords = NumberofRecords + emRequest.Requests.Count;

        //                    requestSets++;
        //                    emRequest = new ExecuteMultipleRequest
        //                    {
        //                        Requests = new OrganizationRequestCollection(),
        //                        Settings = new ExecuteMultipleSettings
        //                        {
        //                            ContinueOnError = false,
        //                            ReturnResponses = true
        //                        }
        //                    };
        //                }

        //            }
        //        }
        //    }

        //}

        private Guid CreateNote(EntityReference dataUploader, byte[] bytes, string fileName)
        {
            string file = Convert.ToBase64String(bytes);

            Entity note = new Entity("annotation");

            note.Attributes["subject"] = fileName;
            note.Attributes["filename"] = fileName;
            note.Attributes["documentbody"] = file;
            note.Attributes["objectid"] = dataUploader;

            Guid noteId = _orgService.Create(note);

            return noteId;
        }
    }
}
