﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Portal.DAL.Interfaces;

namespace Portal.DAL.Repositories
{
    public class EntityHelperRepository : IEntityHelperRepository
    {
        private readonly IOrganizationService _orgService;

        public EntityHelperRepository(IOrganizationService orgService)
        {
            _orgService = orgService;
        }

        public FetchXmlToQueryExpressionResponse GetConversionResponse(FetchXmlToQueryExpressionRequest conversionRequest)
        {
            var conversionResponse = (FetchXmlToQueryExpressionResponse)_orgService.Execute(conversionRequest);
            return conversionResponse;
        }
    }
}
