﻿using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;

using System.Reflection;
using System.Linq.Dynamic;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Portal.Models.EntityClasses;

namespace Portal.DAL.Repositories
{
    public class EntityRepository : IEntityRepository
    {
        private readonly IOrganizationService _orgService;

        public EntityRepository(IOrganizationService orgService)
        {
            _orgService = orgService;
        }

        public Entity Get(Guid id, string type, ColumnSet columnSet = null)
        {
            if(columnSet == null)
            {
                columnSet = new ColumnSet(true);
            }

            Entity entity = _orgService.Retrieve(type, id, columnSet);
            return entity;
        }

        public void Create(Entity entity)
        {
            _orgService.Create(entity);
        }

        public Guid CreateAndReturnId(Entity entity)
        {
            return _orgService.Create(entity);
        }

        public void Update(Entity entity)
        {
            _orgService.Update(entity);
        }

        public void Delete(string entityLogicalName, Guid id)
        {
            _orgService.Delete(entityLogicalName, id);
        }

        public int Count(string entityLogicalName)
        {
            var query = new QueryExpression(entityLogicalName)
            {
                PageInfo = new PagingInfo
                {
                    Count = 1,
                    PageNumber = 1,
                    ReturnTotalRecordCount = true
                }
            };

            var result = _orgService.RetrieveMultiple(query);

            if (result.TotalRecordCountLimitExceeded)
            {
                throw new InvalidOperationException("Cannot get record count.");
            }

            return result.TotalRecordCount;
        }

        public IEnumerable<Entity> GetRelatedEntities(Entity entity, string relatedEntityName, string key)
        {
            Relationship relationship = new Relationship();

            relationship.SchemaName = key;

            QueryExpression query = new QueryExpression();
            query.EntityName = relatedEntityName;
            query.ColumnSet = new ColumnSet(true);
            query.Criteria = new FilterExpression();
            query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

            OrganizationServiceContext orgServiceContext = new OrganizationServiceContext(_orgService);
            orgServiceContext.MergeOption = MergeOption.NoTracking;
            return entity.GetRelatedEntities(orgServiceContext, relationship);
        }

        public EntitiesWithCountModel GetRelatedEntitiesWithCount(
            QueryExpression query,
            string relationshipShcemeName,
            EntityReference entityReference)
        {
            Relationship relationship = new Relationship();
            relationship.SchemaName = relationshipShcemeName;

            RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);

            RetrieveRequest request = new RetrieveRequest();
            request.Target = entityReference;
            request.RelatedEntitiesQuery = relatedEntity;
            request.ColumnSet = new ColumnSet(entityReference.LogicalName + "id");

            RetrieveResponse response = (RetrieveResponse)_orgService.Execute(request);
            
            Relationship rel = new Relationship(relationshipShcemeName);

            var relatedEntities = response.Entity.RelatedEntities[rel];

            return new EntitiesWithCountModel
            {
                Entities = relatedEntities.Entities,
                Count = relatedEntities.TotalRecordCount
            };
        }

        public void Associate(string logicalName, Guid entityId, 
            Relationship relationship, EntityReferenceCollection relatedEntities)
        {
            _orgService.Associate(logicalName, entityId, relationship, relatedEntities);
        }

        public void Disassociate(string logicalName, Guid entityId, Relationship relationship, 
            EntityReferenceCollection relatedEntities)
        {
            _orgService.Associate(logicalName, entityId, relationship, relatedEntities);
        }

        public string GetFieldDisplayName(string entityLogicalName, string fieldName)
        {
            RetrieveAttributeRequest retrieveAttributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityLogicalName,
                LogicalName = fieldName,
                RetrieveAsIfPublished = true
            };

            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)
                    _orgService.Execute(retrieveAttributeRequest);
            AttributeMetadata retrievedAttributeMetadata = retrieveAttributeResponse.AttributeMetadata;
            return retrievedAttributeMetadata.DisplayName.UserLocalizedLabel.Label;
        }

        public EntitiesWithCountModel GetAllWithTotalCount(QueryExpression query)
        {
            EntityCollection entityCollection = _orgService.RetrieveMultiple(query);

            EntitiesWithCountModel result = new EntitiesWithCountModel
            {
                Entities = entityCollection.Entities,
                Count = entityCollection.TotalRecordCount
            };

            return result;
        }
    }
}
