﻿using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Client.Services;

using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace Portal.DAL.Repositories
{
    public class FormRepository : IFormRepository
    {
        private readonly OrganizationService _orgService;

        public FormRepository(OrganizationService orgService)
        {
            _orgService = orgService;;
        }

        public IEnumerable<Entity> GetApplicationsFormListForUnregisters()
        {
            var query = new QueryExpression
            {
                EntityName = "systemform",
                ColumnSet = new ColumnSet("description", "name")
            };

            query.Criteria.AddCondition("description", ConditionOperator.Equal, "its_application_for_unregisters");

            var forms = _orgService.RetrieveMultiple(query).Entities;
            return forms;
        }

        public IEnumerable<Entity> GetForms(string entityLogicalName=null, int formType = 2)
        {
            var query = new QueryExpression
            {
                EntityName = "systemform",
                ColumnSet = new ColumnSet(true),
                Criteria =
                    new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("type", ConditionOperator.Equal, formType)
                        }
                    }
            };

            if(entityLogicalName != null)
            {
                query.Criteria.AddCondition(new ConditionExpression(
                                "objecttypecode",
                                ConditionOperator.Equal,
                                entityLogicalName));
            }            

            var forms = _orgService.RetrieveMultiple(query).Entities;

            return forms;
        }

        public Entity GetFormById(string entityLogicalName, Guid formId, FormTypeEnum formType = FormTypeEnum.FormTypeCreateUpdate)
        {
            var query = new QueryExpression
            {
                EntityName = "systemform",
                ColumnSet = new ColumnSet(true),
                Criteria =
                    new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("objecttypecode", ConditionOperator.Equal, entityLogicalName),
                            new ConditionExpression("type", ConditionOperator.Equal, (int)formType),
                            new ConditionExpression("formid", ConditionOperator.Equal, formId)
                        }
                    }
            };

            var form = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            return form;
        }

        public Entity GetFormById(Guid formId, FormTypeEnum formType = FormTypeEnum.FormTypeCreateUpdate)
        {
            var query = new QueryExpression
            {
                EntityName = "systemform",
                ColumnSet = new ColumnSet(true),
                Criteria =
                    new FilterExpression(LogicalOperator.And)
                    {
                        Conditions =
                        {
                            new ConditionExpression("type", ConditionOperator.Equal, (int)formType),
                            new ConditionExpression("formid", ConditionOperator.Equal, formId)
                        }
                    }
            };

            var form = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            return form;
        }

    }
}
