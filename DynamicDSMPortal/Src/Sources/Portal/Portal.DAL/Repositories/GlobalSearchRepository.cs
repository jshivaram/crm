﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;
using Portal.DAL.OrganiztionServices;
using Portal.Logger;
using Portal.Models.GlobalSearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal.DAL.Repositories
{
    public class GlobalSearchRepository : IGlobalSearchRepository
    {
        private readonly IOrganizationService _orgService = OrganizationServiceManager.OrganizationService;
        private readonly IUnitOfWork _uow;

        public GlobalSearchRepository(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<IReadOnlyList<GlobalSearchEntity>> GetGlobalSearchConfigAsync(Guid contactId)
        {
            return await Task.Run(() => GetGlobalSearchConfig(contactId));
        }

        private IReadOnlyList<GlobalSearchEntity> GetGlobalSearchConfig(Guid contactId)
        {
            try
            {
                Guid? defaultPortalTeamId = _uow.ContactRepository.GetDefaultPortalTeamId(contactId);

                if (defaultPortalTeamId == null)
                {
                    throw new Exception("defaultPortalTeamId is not defined");
                }

                var columnSet = new ColumnSet("ddsm_searchorder", "ddsm_searchview", "ddsm_insearch", "ddsm_entitylogicalname");
                IEnumerable<Entity> portalEntities = _uow.PortalConfigurationRepository.GetPortalEntitiesList(defaultPortalTeamId.Value, columnSet);

                var result = portalEntities.Select(e => new GlobalSearchEntity
                {
                    LogicalName = e.GetAttributeValue<string>("ddsm_entitylogicalname"),
                    InSearch = e.GetAttributeValue<bool?>("ddsm_insearch"),
                    SearchOrder = e.GetAttributeValue<int?>("ddsm_searchorder"),
                    PortalViewId = e.GetAttributeValue<EntityReference>("ddsm_searchview")?.Id
                })
                                                .Where(ge => ge.InSearch == true)
                                                .OrderBy(ge => ge.SearchOrder)
                                                .ThenBy(ge => ge.LogicalName)
                                                .ToList();

                _AddDisplayNamesToResult(result);
                _AddViewsIdsToResult(result);

                return result;
            }
            catch (Exception ex)
            {
                string message = "Cant get global search config.";
                LoggerManager.LogError(ex, message);
                throw new Exception(message);
            }
        }

        private void _AddDisplayNamesToResult(IReadOnlyList<GlobalSearchEntity> result)
        {
            try
            {
                var metadataRequests = new OrganizationRequestCollection();

                foreach (GlobalSearchEntity portalEntity in result)
                {
                    metadataRequests.Add(new RetrieveEntityRequest
                    {
                        LogicalName = portalEntity.LogicalName,
                        EntityFilters = EntityFilters.Entity
                    });
                }

                var entitiesLogicalNamesRequest = new ExecuteMultipleRequest
                {
                    Requests = metadataRequests,

                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = false,
                        ReturnResponses = true
                    }
                };

                var response = (ExecuteMultipleResponse)_orgService.Execute(entitiesLogicalNamesRequest);

                int index = 0;
                foreach (ExecuteMultipleResponseItem reponseItem in response.Responses)
                {
                    OrganizationResponse metadataResponse = reponseItem?.Response;
                    ParameterCollection metadataResponseResults = metadataResponse?.Results;

                    if (metadataResponseResults != null)
                    {
                        var entityMetadata = metadataResponse.Results["EntityMetadata"] as EntityMetadata;
                        GlobalSearchEntity entityForUpdate = result.FirstOrDefault(re => re.LogicalName == entityMetadata.LogicalName);
                        entityForUpdate.DisplayName = entityMetadata.DisplayName.LocalizedLabels.FirstOrDefault().Label;
                        index++;
                    }
                }
            }
            catch (Exception)
            {
                LoggerManager.LogError(new Exception("Cant get entities display names for Global Search"));
                throw;
            }
        }

        private void _AddViewsIdsToResult(IReadOnlyList<GlobalSearchEntity> result)
        {
            try
            {
                string[] portalViewsIdList = result.Where(pv => pv.PortalViewId.HasValue)
                                                              .Select(pv => pv.PortalViewId.Value.ToString())
                                                              .ToArray();

                var query = new QueryExpression("ddsm_portalview")
                {
                    ColumnSet = new ColumnSet("ddsm_viewid")
                };

                if(!portalViewsIdList.Any())
                {
                    return;
                }

                query.Criteria.AddCondition("ddsm_portalviewid", ConditionOperator.In, portalViewsIdList);

                var retrivedPortalViews = _orgService.RetrieveMultiple(query).Entities;

                foreach(GlobalSearchEntity entity in result)
                {
                    string viewIdBuf = retrivedPortalViews.FirstOrDefault(r => r.Id == entity.PortalViewId)?.GetAttributeValue<string>("ddsm_viewid");

                    if(!string.IsNullOrEmpty(viewIdBuf))
                    {
                        entity.ViewId = Guid.Parse(viewIdBuf);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerManager.LogError(ex);
                throw;
            }
        }
    }
}