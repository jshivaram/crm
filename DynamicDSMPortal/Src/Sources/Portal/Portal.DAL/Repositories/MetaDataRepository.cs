﻿using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml;
using Newtonsoft.Json;


namespace Portal.DAL.Repositories
{
    public class MetaDataRepository : IMetaDataRepository
    {
        private readonly OrganizationService _orgService;

        public MetaDataRepository(OrganizationService orgService)
        {
            _orgService = orgService;
        }

        public EntityMetadata GetEntityMetaData(string entityLogicalName)
        {
            RetrieveEntityRequest metaDataRequest = new RetrieveEntityRequest();
            metaDataRequest.EntityFilters = EntityFilters.Attributes;
            metaDataRequest.LogicalName = entityLogicalName.ToLower();

            RetrieveEntityResponse metaDataResponse = _orgService.Execute<RetrieveEntityResponse>(metaDataRequest);

            EntityMetadata metaData = metaDataResponse.EntityMetadata;
            return metaData;
        }

        public RetrieveAttributeResponse GetAttributeMetadata(
            string entityLogicalName, string attributeLogicalName)
        {
            RetrieveAttributeRequest attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityLogicalName,
                LogicalName = attributeLogicalName,
                RetrieveAsIfPublished = true
            };

            RetrieveAttributeResponse attributeResponse =
                (RetrieveAttributeResponse)_orgService.Execute(attributeRequest);


            return attributeResponse;
        }

        public IEnumerable<AttributeMetadata> GetAttributesMetadata(
            string entityLogicalName, IEnumerable<string> attributesLogicalNames)
        {
            OrganizationRequestCollection requestCollection = new OrganizationRequestCollection();

            foreach(string attribute in attributesLogicalNames)
            {
                RetrieveAttributeRequest attributeRequest = new RetrieveAttributeRequest
                {
                    EntityLogicalName = entityLogicalName,
                    LogicalName = attribute,
                    RetrieveAsIfPublished = true
                };

                requestCollection.Add(attributeRequest);
            }            

            ExecuteMultipleRequest multipleRequest = new ExecuteMultipleRequest
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true,
                    
                },

                Requests = requestCollection
            };

            var attributesListReponses = _orgService.Execute(multipleRequest)
                ?.Results?.ToList().LastOrDefault().Value as IEnumerable<ExecuteMultipleResponseItem>;


            var attributes = attributesListReponses?.Where(res => res != null && res.Response != null)
                .SelectMany(res => res.Response.Results)
                .Where(res => res.Value != null)
                .Select(res => res.Value as AttributeMetadata).ToList();

            return attributes;
        }

        public string GetProcesStageByEntityId(string entityId)
        {
            return GetProcessStage(entityId);
        }

        private string GetProcessStage(string entityId)
        {
            var stages = RetreiveProcessStageFromCrm(entityId);
            if (stages.Entities.Count == 0)
            {
                return string.Empty;
            }

            var stageIds = new List<string>();



            var activeStage = string.Empty;
            var processid = Guid.Empty;
            var traversedPath = string.Empty;
            foreach (var item in stages.Entities)
            {
                activeStage = item.GetAttributeValue<AliasedValue>("active_stage_id").Value.ToString();
                var stageId = item.GetAttributeValue<Guid>("processstageid").ToString();
                stageIds.Add(stageId);
                processid = item.GetAttributeValue<EntityReference>("processid").Id;
                if (string.IsNullOrEmpty(traversedPath))
                {
                    traversedPath = item.GetAttributeValue<string>("traversedpath");
                }

            }
            return GetOrder(processid, stageIds, traversedPath, activeStage);

        }

        /// <summary>
        /// Retrieving the process stage record of Business process flow for an entity.
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        private EntityCollection RetreiveProcessStageFromCrm(string entityId)
        {
            var fetch = $@"<fetch mapping=""logical"" >
                <entity name=""processstage"" >                
                <attribute name=""processid"" />
                <attribute name=""processstageid"" />
                <link-entity name=""businessprocessflowinstance"" from=""processid"" to=""processid"" >
                  <attribute name=""processstageid"" alias=""active_stage_id"" />
                  <attribute name=""traversedpath"" alias=""traversedpath"" />  
                  <filter>
                    <condition attribute=""entity1id"" operator=""eq"" value=""{entityId}"" />
                  </filter>
                </link-entity>
              </entity>
            </fetch>";

            return _orgService.RetrieveMultiple(new FetchExpression(fetch));

        }

        private string GetOrder(Guid processid, List<string> stageIds, string traversedPath, string activeStage)
        {
            var cnt = 0;
            var dto = new object[stageIds.Count + 1];
            var bpfTemplate = GetBpfTemplate(stageIds, processid);
            var doc = new XmlDocument();
            doc.LoadXml(bpfTemplate);
            var elemList = doc.GetElementsByTagName("mcwo:StepLabel");
            for (var i = 0; i < elemList.Count; i++)
            {
                var xmlAttributeCollection = elemList[i].Attributes;
                var description = string.Empty;
                if (xmlAttributeCollection != null)
                {
                    description = xmlAttributeCollection["Description"].Value;
                }

                var stageId = string.Empty;
                if (xmlAttributeCollection != null)
                {
                    stageId = xmlAttributeCollection["LabelId"].Value;
                }
                if (!stageIds.Contains(stageId)) continue;
                dto[cnt] = new
                {
                    order = cnt,
                    stageName = description,
                    stageId = stageId,
                    status = GetStageStatus(stageId, traversedPath, activeStage) };
                cnt++;
            }

            XmlNode xmlNodeControl = GetXmlNodeControl(doc);
            var xmlNodeControlDict = new Dictionary<string, string>();

            foreach(object attribute in xmlNodeControl.Attributes)
            {
                var xmlAttribute = (XmlAttribute)attribute;
                xmlNodeControlDict.Add(xmlAttribute.Name, xmlAttribute.Value);
            }

            dto[stageIds.Count] = xmlNodeControlDict;

            return new JavaScriptSerializer().Serialize(dto);
        }

        private XmlNode GetXmlNodeControl(XmlDocument doc)
        {
            XmlNodeList controls = doc.GetElementsByTagName("mcwb:Control");

            var xmlNodesControls = new List<XmlNode>();
            foreach (XmlNode xmlNode in controls)
            {
                xmlNodesControls.Add(xmlNode);
            }

            XmlNode xmlNodeControl = xmlNodesControls.FirstOrDefault();
            return xmlNodeControl;
        }

        private static string GetStageStatus(string stageId, string traversedPath, string activeStage)
        {
            if (activeStage == stageId)
            {
                return "process";
            }
            var finishedStages = traversedPath.Split(',');
            return finishedStages.Contains(stageId) ? "finish" : "wait";
        }

        private string GetBpfTemplate(List<string> stageIds, Guid processid)
        {
            var query = new QueryExpression("workflow");
            var col = new ColumnSet("xaml");
            query.ColumnSet = col;
            var workflowId = new ConditionExpression("workflowid", ConditionOperator.Equal, processid);
            var bpType = new ConditionExpression("businessprocesstype", ConditionOperator.Equal, 0);

            var filter = new FilterExpression(LogicalOperator.And);
            filter.Conditions.Add(workflowId);
            filter.Conditions.Add(bpType);

            query.Criteria.AddFilter(filter);

            var rs = _orgService.RetrieveMultiple(query);
            return rs.Entities.Count == 0 ? string.Empty : rs.Entities[0].GetAttributeValue<string>("xaml");
        }
    }
}
