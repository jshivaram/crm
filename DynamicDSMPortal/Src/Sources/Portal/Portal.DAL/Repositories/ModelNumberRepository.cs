﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;
using Portal.Models;

namespace Portal.DAL.Repositories
{
    public class ModelNumberRepository : IModelNumberRepository
    {
        private readonly IOrganizationService _orgsService;

        public ModelNumberRepository(IOrganizationService orgsService)
        {
            _orgsService = orgsService;
        }

        public async Task<IReadOnlyList<LookupModel>> GetModelNumbersForLookupByEfficiencyAsync(int efficiency)
        {
            return await Task.Run(() => _GetModelNumbersForLookupByEfficiency(efficiency));
        }

        private IReadOnlyList<LookupModel> _GetModelNumbersForLookupByEfficiency(int efficiency)
        {
            var query = new QueryExpression("ddsm_modelnumberapproval")
            {
                ColumnSet = new ColumnSet("ddsm_name")
            };

            query.Criteria.AddCondition("ddsm_efficiency", ConditionOperator.GreaterEqual, (decimal)efficiency);

            var modelNumbers = _orgsService.RetrieveMultiple(query).Entities;

            var result = modelNumbers.Select(mn => new LookupModel
            {
                Value = mn.Id.ToString(),
                Text = mn.GetAttributeValue<string>("ddsm_name")
            }).ToList();
            
            return result;
        }
    }
}
