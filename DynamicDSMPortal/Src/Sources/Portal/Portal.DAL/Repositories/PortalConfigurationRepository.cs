﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Repositories
{
    public class PortalConfigurationRepository : IPortalConfigurationRepository
    {
        private readonly IOrganizationService _orgService;

        public PortalConfigurationRepository(IOrganizationService orgService)
        {
            _orgService = orgService;
        }

        public IEnumerable<Guid> GetAccountIdListByPortalTeamIdList(IEnumerable<Guid> portalTeamIdList)
        {
            var query = new QueryExpression("account");

            var portalTeamLink = new LinkEntity(
                "account", "ddsm_portalteam",
                "accountid", "ddsm_accountid", 
                JoinOperator.Inner);

            portalTeamLink.LinkCriteria.AddCondition(
                "ddsm_portalteamid", 
                ConditionOperator.In,
                portalTeamIdList.Select(t => t.ToString()).ToArray());

            portalTeamLink.LinkCriteria.AddCondition(
                "ddsm_get_account_from_team",
                ConditionOperator.Equal,
                true);

            query.LinkEntities.Add(portalTeamLink);

            IEnumerable<Entity> accounts = _orgService.RetrieveMultiple(query).Entities;
            IEnumerable<Guid> accountsIdList = accounts.Select(a => a.Id).ToList();

            return accountsIdList;
        }

        public Guid? GetDefaultPortalTeamIdByContactId(Guid contactId)
        {
            var query = new QueryExpression
            {
                EntityName = "contact",
                ColumnSet = new ColumnSet("ddsm_defaultportalteamid")
            };

            query.Criteria.AddCondition("contactid", ConditionOperator.Equal, contactId);

            Entity contact = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            var defaultPortalTeam = contact.GetAttributeValue<EntityReference>("ddsm_defaultportalteamid");
            return defaultPortalTeam?.Id;
        }

        public Entity GetDefaultPortalTeamByContactId(Guid contactId)
        {
            var query = new QueryExpression
            {
                EntityName = "contact",
                ColumnSet = new ColumnSet("ddsm_defaultportalteamid")
            };

            query.Criteria.AddCondition("contactid", ConditionOperator.Equal, contactId);

            Entity contact = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            var defaultPortalTeamRef = contact.GetAttributeValue<EntityReference>("ddsm_defaultportalteamid");

            if(defaultPortalTeamRef == null)
            {
                return null;
            }

            Entity defaultPortalTeam = _orgService.Retrieve(
                defaultPortalTeamRef.LogicalName, defaultPortalTeamRef.Id, new ColumnSet(true));

            return defaultPortalTeam;
        }

        public Guid? GetTeamIdByPortalTeamId(Guid portalTeamId)
        {
            var query = new QueryExpression
            {
                EntityName = "ddsm_portalteam",
                ColumnSet = new ColumnSet("ddsm_teamid")
            };

            query.Criteria.AddCondition("ddsm_portalteamid", ConditionOperator.Equal, portalTeamId);

            Entity portalTeam = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();
            string teamIdStr = portalTeam?.GetAttributeValue<string>("ddsm_teamid");

            if(teamIdStr == null)
            {
                return null;
            }

            return new Guid(teamIdStr);
        }

        public IEnumerable<Entity> GetPortalTeams(IEnumerable<Guid> teamIdList)
        {
            var query = new QueryExpression("ddsm_portalteam");
            query.ColumnSet = new ColumnSet(true);

            query.Criteria.AddCondition(
               "ddsm_teamid",
               ConditionOperator.In,
               teamIdList.Select(t => t.ToString()).ToArray());

            IEnumerable<Entity> teams = _orgService.RetrieveMultiple(query).Entities;
            return teams;
        }

        public IEnumerable<Entity> GetPortalEntitiesListForNavigation(IEnumerable<Guid> teamIdList,
            ColumnSet columnSet)
        {
            var query = new QueryExpression("ddsm_portalentity");
            query.ColumnSet = columnSet;

            query.Criteria.AddCondition(
                 "ddsm_show_in_portal_navigation",
                 ConditionOperator.Equal,
                 true
                 );

            var portalTeamPortalEntityLink = new LinkEntity(
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalteam",
                "ddsm_portalteamid",
                "ddsm_portalteamid",
                JoinOperator.Inner);

            portalTeamPortalEntityLink.LinkCriteria.AddCondition(
                "ddsm_teamid",
                ConditionOperator.In,
                teamIdList.Select(t => t.ToString()).ToArray());

            var portalEntityLink = new LinkEntity(
                "ddsm_portalentity",
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalentityid",
                "ddsm_portalentityid",
                JoinOperator.Inner);

            portalEntityLink.LinkEntities.Add(portalTeamPortalEntityLink);


            query.LinkEntities.Add(portalEntityLink);
            var portalEntities = _orgService.RetrieveMultiple(query).Entities;

            return portalEntities;
        }

        public IEnumerable<Entity> GetPortalEntitiesListForNavigation(Guid teamId,
            ColumnSet columnSet)
        {
            var query = new QueryExpression("ddsm_portalentity");
            query.ColumnSet = columnSet;

            query.Criteria.AddCondition(
                 "ddsm_show_in_portal_navigation",
                 ConditionOperator.Equal,
                 true
                 );

            var portalTeamPortalEntityLink = new LinkEntity(
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalteam",
                "ddsm_portalteamid",
                "ddsm_portalteamid",
                JoinOperator.Inner);

            portalTeamPortalEntityLink.LinkCriteria.AddCondition(
                "ddsm_teamid",
                ConditionOperator.Equal,
                teamId.ToString());

            var portalEntityLink = new LinkEntity(
                "ddsm_portalentity",
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalentityid",
                "ddsm_portalentityid",
                JoinOperator.Inner);

            portalEntityLink.LinkEntities.Add(portalTeamPortalEntityLink);


            query.LinkEntities.Add(portalEntityLink);
            var portalEntities = _orgService.RetrieveMultiple(query).Entities;

            return portalEntities;
        }

        public IEnumerable<Entity> GetPortalDashboardsByTeamIdList(IEnumerable<Guid> teamIdList, 
            ColumnSet columnSet)
        {
            var query = new QueryExpression("ddsm_portaldashboard");
            query.ColumnSet = columnSet;

            var portalDashboardPortalEntityLink = new LinkEntity(
                "ddsm_ddsm_portalteam_ddsm_portaldashboard",
                "ddsm_portalteam",
                "ddsm_portalteamid",
                "ddsm_portalteamid",
                JoinOperator.Inner);

            portalDashboardPortalEntityLink.LinkCriteria.AddCondition(
                "ddsm_teamid",
                ConditionOperator.In,
                teamIdList.Select(t => t.ToString()).ToArray());

            query.LinkEntities.Add(portalDashboardPortalEntityLink);
            IEnumerable<Entity> portalDashboards = _orgService.RetrieveMultiple(query).Entities;

            return portalDashboards;
        }

        public IEnumerable<Guid> GetDefaultDashboardsIdByTeamIdList(IEnumerable<Guid> teamIdList)
        {
            var query = new QueryExpression("ddsm_portalteam");
            query.ColumnSet = new ColumnSet("ddsm_defaultdashboardid");

            query.Criteria.AddCondition(
                "ddsm_teamid",
                ConditionOperator.In,
                teamIdList.Select(t => t.ToString()).ToArray());

            IEnumerable<Entity> portalTeams = _orgService.RetrieveMultiple(query).Entities;
            portalTeams = portalTeams.Where(pt => pt.GetAttributeValue<string>("ddsm_defaultdashboardid") != null);

            IEnumerable<Guid> dashboardIdList = portalTeams
                .Select(pt => Guid.Parse(pt.GetAttributeValue<string>("ddsm_defaultdashboardid")))
                .ToList();

            return dashboardIdList;
        }

        public IEnumerable<Entity> GetPortalFormsByTeamIdList(IEnumerable<Guid> teamIdList,
            string entityLogicalName, ColumnSet columnSet)
        {
            var query = new QueryExpression("ddsm_portalform");
            query.ColumnSet = columnSet;

            var portalTeamPortalEntityLink = new LinkEntity(
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalteam",
                "ddsm_portalteamid",
                "ddsm_portalteamid",
                JoinOperator.Inner);

            portalTeamPortalEntityLink.LinkCriteria.AddCondition(
                "ddsm_teamid",
                ConditionOperator.In,
                teamIdList.Select(t => t.ToString()).ToArray());

            var portalEntityLink = new LinkEntity(
                "ddsm_portalentity",
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalentityid",
                "ddsm_portalentityid",
                JoinOperator.Inner);

            portalEntityLink.LinkEntities.Add(portalTeamPortalEntityLink);

            var portalFormLink = new LinkEntity(
                "ddsm_portalform",
                "ddsm_portalentity",
                "ddsm_portalentityid",
                "ddsm_portalentityid",
                JoinOperator.Inner);

            portalFormLink.LinkCriteria.AddCondition(
                "ddsm_entitylogicalname",
                ConditionOperator.Equal,
                entityLogicalName.ToLower());

            portalFormLink.LinkEntities.Add(portalEntityLink);

            query.LinkEntities.Add(portalFormLink);
            IEnumerable<Entity> portalForms = _orgService.RetrieveMultiple(query).Entities;

            return portalForms;
        }

        public IEnumerable<Entity> GetPortalFormsByTeamId(Guid teamId, string entityLogicalName, ColumnSet columnSet)
        {
            QueryExpression query = new QueryExpression("ddsm_portalform");
            query.ColumnSet = columnSet;

            LinkEntity portalTeamPortalEntityLink = new LinkEntity(
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalteam",
                "ddsm_portalteamid",
                "ddsm_portalteamid",
                JoinOperator.Inner);

            portalTeamPortalEntityLink.LinkCriteria.AddCondition(
                "ddsm_teamid",
                ConditionOperator.Equal,
                teamId.ToString().ToLower());

            LinkEntity portalEntityLink = new LinkEntity(
                "ddsm_portalentity",
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalentityid",
                "ddsm_portalentityid",
                JoinOperator.Inner);

            portalEntityLink.LinkEntities.Add(portalTeamPortalEntityLink);

            LinkEntity portalFormLink = new LinkEntity(
                "ddsm_portalform",
                "ddsm_portalentity",
                "ddsm_portalentityid",
                "ddsm_portalentityid",
                JoinOperator.Inner);

            portalFormLink.LinkCriteria.AddCondition(
                "ddsm_entitylogicalname",
                ConditionOperator.Equal,
                entityLogicalName.ToLower());

            portalFormLink.LinkEntities.Add(portalEntityLink);

            query.LinkEntities.Add(portalFormLink);
            IEnumerable<Entity> portalForms = _orgService.RetrieveMultiple(query).Entities;

            return portalForms;
        }

        public IEnumerable<Entity> GetPortalViewsByTeamId(Guid teamId, string entityLogicalName, ColumnSet columnSet)
        {
            QueryExpression query = new QueryExpression("ddsm_portalview");
            query.ColumnSet = columnSet;

            LinkEntity portalTeamPortalEntityLink = new LinkEntity(
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalteam",
                "ddsm_portalteamid",
                "ddsm_portalteamid",
                JoinOperator.Inner);

            portalTeamPortalEntityLink.LinkCriteria.AddCondition(
                "ddsm_teamid",
                ConditionOperator.Equal,
                teamId.ToString().ToLower());

            LinkEntity portalEntityLink = new LinkEntity(
                "ddsm_portalentity",
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalentityid",
                "ddsm_portalentityid",
                JoinOperator.Inner);

            portalEntityLink.LinkEntities.Add(portalTeamPortalEntityLink);

            LinkEntity portalViewLink = new LinkEntity(
                "ddsm_portalview",
                "ddsm_portalentity",
                "ddsm_portalentityid",
                "ddsm_portalentityid",
                JoinOperator.Inner);

            portalViewLink.LinkCriteria.AddCondition(
                "ddsm_entitylogicalname",
                ConditionOperator.Equal,
                entityLogicalName.ToLower());

            portalViewLink.LinkEntities.Add(portalEntityLink);

            query.LinkEntities.Add(portalViewLink);
            IEnumerable<Entity> portalViews = _orgService.RetrieveMultiple(query).Entities;

            return portalViews;
        }

        public IEnumerable<Entity> GetPortalViewsByTeamIdList(IEnumerable<Guid> teamIdList,
            string entityLogicalName, ColumnSet columnSet)
        {
            QueryExpression query = new QueryExpression("ddsm_portalview");
            query.ColumnSet = columnSet;

            LinkEntity portalTeamPortalEntityLink = new LinkEntity(
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalteam",
                "ddsm_portalteamid",
                "ddsm_portalteamid",
                JoinOperator.Inner);

            portalTeamPortalEntityLink.LinkCriteria.AddCondition(
                "ddsm_teamid",
                ConditionOperator.In,
                teamIdList.Select(f => f.ToString()).ToArray());

            LinkEntity portalEntityLink = new LinkEntity(
                "ddsm_portalentity",
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalentityid",
                "ddsm_portalentityid",
                JoinOperator.Inner);

            portalEntityLink.LinkEntities.Add(portalTeamPortalEntityLink);

            LinkEntity portalViewLink = new LinkEntity(
                "ddsm_portalview",
                "ddsm_portalentity",
                "ddsm_portalentityid",
                "ddsm_portalentityid",
                JoinOperator.Inner);

            portalViewLink.LinkCriteria.AddCondition(
                "ddsm_entitylogicalname",
                ConditionOperator.Equal,
                entityLogicalName.ToLower());

            portalViewLink.LinkEntities.Add(portalEntityLink);

            query.LinkEntities.Add(portalViewLink);
            IEnumerable<Entity> portalViews = _orgService.RetrieveMultiple(query).Entities;

            return portalViews;
        }

        public Guid? GetDefaultFormIdByTeamId(Guid teamId, string entityLogicalName)
        {
            var query = new QueryExpression("ddsm_portalentity");
            query.ColumnSet = new ColumnSet(true);

            query.Criteria.AddCondition(
                "ddsm_entitylogicalname",
                ConditionOperator.Equal,
                entityLogicalName.ToLower());

            var portalTeamPortalEntityLink = new LinkEntity(
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalteam",
                "ddsm_portalteamid",
                "ddsm_portalteamid",
                JoinOperator.Inner);

            portalTeamPortalEntityLink.LinkCriteria.AddCondition(
                "ddsm_teamid",
                ConditionOperator.Equal,
                teamId.ToString().ToLower());

            var portalEntityLink = new LinkEntity(
                "ddsm_portalentity",
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalentityid",
                "ddsm_portalentityid",
                JoinOperator.Inner);

            portalEntityLink.LinkEntities.Add(portalTeamPortalEntityLink);
            query.LinkEntities.Add(portalEntityLink);

            Entity portalEntity = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            if (portalEntity == null)
            {
                return null;
            }

            string defaultFormIdStr = portalEntity.Attributes["ddsm_defaultformid"].ToString();

            if (string.IsNullOrEmpty(defaultFormIdStr))
            {
                return null;
            }

            Guid defaultFormId = new Guid(defaultFormIdStr);

            return defaultFormId;
        }


        public Guid? GetDefaultViewIdByTeamId(Guid teamId, string entityLogicalName)
        {
            QueryExpression query = new QueryExpression("ddsm_portalentity");
            query.ColumnSet = new ColumnSet(true);

            query.Criteria.AddCondition(
                "ddsm_entitylogicalname",
                ConditionOperator.Equal,
                entityLogicalName.ToLower());

            LinkEntity portalTeamPortalEntityLink = new LinkEntity(
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalteam",
                "ddsm_portalteamid",
                "ddsm_portalteamid",
                JoinOperator.Inner);

            portalTeamPortalEntityLink.LinkCriteria.AddCondition(
                "ddsm_teamid",
                ConditionOperator.Equal,
                teamId.ToString().ToLower());

            LinkEntity portalEntityLink = new LinkEntity(
                "ddsm_portalentity",
                "ddsm_ddsm_portalteam_ddsm_portalentity",
                "ddsm_portalentityid",
                "ddsm_portalentityid",
                JoinOperator.Inner);

            portalEntityLink.LinkEntities.Add(portalTeamPortalEntityLink);
            query.LinkEntities.Add(portalEntityLink);

            Entity portalEntity = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            if (portalEntity == null)
            {
                return null;
            }

            string defaultViewIdStr = portalEntity.Attributes["ddsm_defaultviewid"].ToString();

            if (string.IsNullOrEmpty(defaultViewIdStr))
            {
                return null;
            }

            Guid defaultViewId = new Guid(defaultViewIdStr);

            return defaultViewId;
        }

        public IEnumerable<Entity> GetFormsByFormsIdList(IEnumerable<Guid> formsIdList, ColumnSet columnSet)
        {
            QueryExpression query = new QueryExpression
            {
                EntityName = "systemform",
                ColumnSet = columnSet
            };

            query.Criteria.AddCondition("formid", ConditionOperator.In, formsIdList.Select(f => f.ToString()).ToArray());

            IEnumerable<Entity> forms = _orgService.RetrieveMultiple(query).Entities;

            return forms;
        }

        public IEnumerable<Entity> GetViewsByViewsIdList(IEnumerable<Guid> viewsIdList, ColumnSet columnSet)
        {
            QueryExpression query = new QueryExpression
            {
                EntityName = "savedquery",
                ColumnSet = columnSet
            };

            query.Criteria.AddCondition(
                "savedqueryid",
                ConditionOperator.In,
                viewsIdList.Select(f => f.ToString()).ToArray());

            IEnumerable<Entity> views = _orgService.RetrieveMultiple(query).Entities;

            return views;
        }
    }
}
