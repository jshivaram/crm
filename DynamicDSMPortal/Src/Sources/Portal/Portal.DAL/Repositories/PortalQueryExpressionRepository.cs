﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;

namespace Portal.DAL.Repositories
{
    public class PortalQueryExpressionRepository : IPortalQueryExpressionRepository
    {
        private readonly IOrganizationService _organizationService;

        public PortalQueryExpressionRepository(IOrganizationService organizationService)
        {
            if (organizationService == null)
            {
                throw new ArgumentNullException(nameof(organizationService));
            }

            _organizationService = organizationService;
        }

        public IReadOnlyList<Entity> GetPortalQueryExpressions(string entityLogicalName, List<Guid> portalTeamsIdList = null)
        {
            IReadOnlyList<Entity> result = new List<Entity>();

            if (portalTeamsIdList?.Any() == true)
            {
                result = GetPortalQueryExpressionsByPortalTeams(entityLogicalName, portalTeamsIdList);
            }

            if (!result.Any())
            {
                result = GetPortalQueryExpressionsByEntityLogicalName(entityLogicalName);
            }

            return result;
        }

        private IReadOnlyList<Entity> GetPortalQueryExpressionsByEntityLogicalName(string entityLogicalName)
        {
            var rulesQuery = new QueryExpression
            {
                EntityName = "ddsm_queryexpression",
                ColumnSet = new ColumnSet("ddsm_fetchxml")
            };

            var filterAnd = new FilterExpression(LogicalOperator.And);
            filterAnd.AddCondition("statecode", ConditionOperator.Equal, "Active");
            filterAnd.AddCondition("ddsm_entitylogicalname", ConditionOperator.Like, "%\"LogicalName\":\"" + entityLogicalName + "\"%");

            var filterOr = new FilterExpression(LogicalOperator.Or);

            //to do ddsm_portalformid
            filterOr.AddCondition(new ConditionExpression
            {
                AttributeName = "ddsm_portalteamid",
                Operator = ConditionOperator.Null,
            });

            filterAnd.AddFilter(filterOr);
            rulesQuery.Criteria.AddFilter(filterAnd);

            IReadOnlyList<Entity> rules = _organizationService.RetrieveMultiple(rulesQuery).Entities;
            return rules;
        }

        private IReadOnlyList<Entity> GetPortalQueryExpressionsByPortalTeams(string entityLogicalName, List<Guid> portalTeamsIdList)
        {
            var rulesQuery = new QueryExpression
            {
                EntityName = "ddsm_queryexpression",
                ColumnSet = new ColumnSet("ddsm_fetchxml")
            };

            var filterAnd = new FilterExpression(LogicalOperator.And);
            filterAnd.AddCondition("statecode", ConditionOperator.Equal, "Active");
            filterAnd.AddCondition("ddsm_entitylogicalname", ConditionOperator.Like, "%\"LogicalName\":\"" + entityLogicalName + "\"%");

            var filterOr = new FilterExpression(LogicalOperator.Or);

            if (portalTeamsIdList?.Any() == true)
            {
                filterOr.AddCondition(new ConditionExpression("ddsm_portalteamid", ConditionOperator.In, portalTeamsIdList));
            }

            filterAnd.AddFilter(filterOr);
            rulesQuery.Criteria.AddFilter(filterAnd);

            IReadOnlyList<Entity> rules = _organizationService.RetrieveMultiple(rulesQuery).Entities;
            return rules;
        }
    }
}
