﻿using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Query;

namespace Portal.DAL.Repositories
{
    public class PostsRepository : IPostsRepository
    {
        private OrganizationService _orgService;

        public PostsRepository(OrganizationService orgService)
        {
            _orgService = orgService;
        }

        public IEnumerable<Entity> GetPostsByEntityId(Guid entityId)
        {
            var query = new QueryExpression
            {
                EntityName = "post",
                ColumnSet = new ColumnSet(true)
            };

            query.Criteria.AddCondition(
                "regardingobjectid",
                ConditionOperator.Equal,
                entityId);

            var entities = _orgService.RetrieveMultiple(query).Entities;
            return entities;
        }
    }
}
