﻿using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Query;

namespace Portal.DAL.Repositories
{
    public class ProgramOfferingRepository : IProgramOfferingRepository
    {
        private readonly OrganizationService _orgService;

        public ProgramOfferingRepository(OrganizationService orgService)
        {
            _orgService = orgService;
        }

        public IEnumerable<Guid> GetProgramOfferingIdListByPortalTeamIdList(IEnumerable<Guid> portalTeamIdList)
        {
            if(portalTeamIdList == null)
            {
                return new List<Guid>();
            }

            portalTeamIdList = portalTeamIdList.Where(id => id != Guid.Empty).ToList();

            if(!portalTeamIdList.Any())
            {
                return new List<Guid>();
            }

            var query = new QueryExpression("ddsm_programoffering");

            var programOfferingLink = new LinkEntity
            {
                LinkFromEntityName = "ddsm_programoffering",
                LinkToEntityName = "ddsm_ddsm_portalteam_ddsm_programoffering",
                LinkFromAttributeName = "ddsm_programofferingid",
                LinkToAttributeName = "ddsm_programofferingid",
                JoinOperator = JoinOperator.Inner
            };

            var portalTeamLink = new LinkEntity
            {
                LinkFromEntityName = "ddsm_ddsm_portalteam_ddsm_programoffering",
                LinkToEntityName = "ddsm_portalteam",
                LinkFromAttributeName = "ddsm_portalteamid",
                LinkToAttributeName = "ddsm_portalteamid",
                JoinOperator = JoinOperator.Inner
            };

            portalTeamLink.LinkCriteria.AddCondition(
                "ddsm_portalteamid",
                ConditionOperator.In,
                portalTeamIdList.Select(id => id.ToString()).ToArray());

            programOfferingLink.LinkEntities.Add(portalTeamLink);

            query.LinkEntities.Add(programOfferingLink);

            IEnumerable<Entity> programOfferings = _orgService.RetrieveMultiple(query).Entities;
            IEnumerable<Guid> programOfferingIdList = programOfferings.Select(e => e.Id).ToList();
            return programOfferingIdList;
        }
    }
}
