﻿using Microsoft.Xrm.Client.Services;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Repositories
{
    public class SavedQueryRepository : ISavedQueryRepository
    {
        private OrganizationService _orgService;

        public SavedQueryRepository(OrganizationService orgService)
        {
            _orgService = orgService;
        }
    }
}
