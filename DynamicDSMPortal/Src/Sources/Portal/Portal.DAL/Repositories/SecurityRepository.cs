﻿using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portal.DAL.Enums;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk.Deployment;

namespace Portal.DAL.Repositories
{
    public class SecurityRepository : ISecurityRepository
    {
        private readonly OrganizationService _orgService;

        public SecurityRepository(OrganizationService orgService)
        {
            _orgService = orgService;
        }

        public bool CheckAccessByTeamId(Guid teamId, Privilege privilege, string entityLogicalName)
        {
            string privilegeName = ("prv" + privilege.ToString() + entityLogicalName).ToLower();

            QueryExpression privilegeQuery = new QueryExpression("privilege");
            privilegeQuery.ColumnSet = new ColumnSet(true);

            LinkEntity privilegeLink = new LinkEntity("privilege", "roleprivileges", "privilegeid", "privilegeid", JoinOperator.Inner);
            LinkEntity rolePrivilegesLink = new LinkEntity("roleprivileges", "role", "roleid", "roleid", JoinOperator.Inner);
            LinkEntity roleLink = new LinkEntity("role", "teamroles", "roleid", "roleid", JoinOperator.Inner);
            LinkEntity teamRolesLink = new LinkEntity("teamroles", "team", "teamid", "teamid", JoinOperator.Inner);

            ConditionExpression teamCondition = new ConditionExpression("teamid", ConditionOperator.Equal, teamId);
            ConditionExpression prvCondition = new ConditionExpression("name", ConditionOperator.Equal, privilegeName);

            teamRolesLink.LinkCriteria.AddCondition(teamCondition);

            FilterExpression privilegeFilter = new FilterExpression(LogicalOperator.And);
            privilegeFilter.Conditions.Add(prvCondition);

            privilegeQuery.Criteria = privilegeFilter;

            roleLink.LinkEntities.Add(teamRolesLink);
            rolePrivilegesLink.LinkEntities.Add(roleLink);
            privilegeLink.LinkEntities.Add(rolePrivilegesLink);
            privilegeQuery.LinkEntities.Add(privilegeLink);

            EntityCollection retrievedPrivileges = _orgService.RetrieveMultiple(privilegeQuery);

            bool isAccessAllow = retrievedPrivileges.Entities.Count > 0;

            return isAccessAllow;
        }

        public IEnumerable<Guid> GetTeamsIdListByContactId(Guid contactId)
        {
            QueryExpression query = new QueryExpression("team");
            query.ColumnSet = new ColumnSet("teamid");

            LinkEntity ddsmContactTeam = new LinkEntity("ddsm_contact_team", "contact", "contactid", "contactid", JoinOperator.Inner);
            ddsmContactTeam.LinkCriteria.AddCondition(new ConditionExpression("contactid", ConditionOperator.Equal, contactId));

            LinkEntity teamLink = new LinkEntity("team", "ddsm_contact_team", "teamid", "teamid", JoinOperator.Inner);
            teamLink.LinkEntities.Add(ddsmContactTeam);
            query.LinkEntities.Add(teamLink);

            IEnumerable<Entity> teams = _orgService.RetrieveMultiple(query).Entities;
            IEnumerable<Guid> teamsIdList = teams.Select(t => t.Id).ToList();

            return teamsIdList;
        }
    }
}
