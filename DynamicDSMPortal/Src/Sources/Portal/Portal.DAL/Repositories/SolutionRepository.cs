﻿using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.DAL.Repositories
{
    public class SolutionRepository : ISolutionRepository
    {
        private readonly OrganizationService _orgService;

        public SolutionRepository(OrganizationService orgService)
        {
            _orgService = orgService;
        }

        public string GetSolutionVersion(string solutionName)
        {
            var query = new QueryExpression
            {
                EntityName = "solution",
                ColumnSet = new ColumnSet("version")
            };

            query.Criteria.AddCondition(
                "uniquename", 
                ConditionOperator.Equal,
                solutionName);

            Entity solution = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();
            return solution?.GetAttributeValue<string>("version");
        }
    }
}
