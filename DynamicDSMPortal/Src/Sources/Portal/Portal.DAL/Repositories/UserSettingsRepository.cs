﻿using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using Portal.DAL.Enums;
using Portal.DAL.Interfaces;
using Portal.Models.UserSettings;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using TimeZoneConverter;

namespace Portal.DAL.Repositories
{
    public class UserSettingsRepository : IUserSettingsRepository
    {
        private readonly OrganizationService _orgService;
        private readonly string _usersettingsLogicalName = "usersettings";

        public UserSettingsRepository(OrganizationService orgService)
        {
            _orgService = orgService;
        }

        public TimeZoneInfo GetTimeZoneByCRMConnectionWithUtc()
        {
            var whoIamRequest = new WhoAmIRequest();
            var whoAmIResponse = (WhoAmIResponse)_orgService.Execute(whoIamRequest);
            Guid userId = whoAmIResponse.UserId;

            string timeZoneCode = "92"; //UTC
            var query = new QueryExpression(_usersettingsLogicalName);
            query.ColumnSet = new ColumnSet("systemuserid", "timezonecode");
            query.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, userId);

            Entity userSettings = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();
            if (userSettings != null && userSettings.Attributes.ContainsKey("timezonecode"))
            {
                timeZoneCode = userSettings.Attributes["timezonecode"].ToString();
            }
            else
            {
                string timeZoneBuf = TimeZone.CurrentTimeZone.StandardName; //return local
                var timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneBuf);
                return timeZone;
            }

            query = new QueryExpression("timezonedefinition");
            query.ColumnSet = new ColumnSet("timezonecode", "standardname");

            query.Criteria.AddCondition("timezonecode", ConditionOperator.Equal, timeZoneCode);
            Entity timeZoneDefinitions = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            string timeZoneStr = timeZoneDefinitions?.Attributes["standardname"].ToString();

            if (timeZoneStr != null)
            {
                var timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneStr);
                return timeZone;
            }

            return null;
        }

        public DateTimeFormatModel GetDateTimeFormat(Guid userId, CultureInfo browserCulture)
        {
            int dateTimeFormatConfig = GetDateTimeConfig(userId, "ddsm_dateandtimeformatconfig", 
                (int)DateTimeFormatConfig.BrowserDateTimeFormat);

            if (dateTimeFormatConfig == (int)DateTimeFormatConfig.CRMDateTimeFormat)
            {
                return GetDateTimeFormatByCRMConnection();
            }
            else if (dateTimeFormatConfig == (int)DateTimeFormatConfig.CustomDateTimeFormat)
            {
                return GetDateTimeFormatByCustomSettings(userId, browserCulture);
            }

            return GetDateTimeFormatByBrowserCulture(browserCulture);
        }

        private DateTimeFormatModel GetDateTimeFormatByBrowserCulture(CultureInfo browserCulture)
        {
            var model = new DateTimeFormatModel
            {
                DateFormat = browserCulture.DateTimeFormat.ShortDatePattern,
                TimeFormat = browserCulture.DateTimeFormat.ShortTimePattern
            };

            return model;
        }

        private DateTimeFormatModel GetDateTimeFormatByCRMConnection()
        {
            var query = new QueryExpression(_usersettingsLogicalName);
            query.ColumnSet = new ColumnSet("systemuserid", "dateformatstring", "timeformatstring");
            query.Criteria.AddCondition("systemuserid", ConditionOperator.EqualUserId);

            Entity entity = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            var model = new DateTimeFormatModel
            {
                DateFormat = entity.Attributes["dateformatstring"].ToString(),
                TimeFormat = entity.Attributes["timeformatstring"].ToString(),
            };

            return model;
        }

        private DateTimeFormatModel GetDateTimeFormatByCustomSettings(Guid userId, CultureInfo browserCulture)
        {
            var contactQuery = new QueryExpression("contact");
            contactQuery.ColumnSet = new ColumnSet("ddsm_customdateandtimeformat");
            contactQuery.Criteria.AddCondition("contactid", ConditionOperator.Equal, userId);

            Entity contactEntity = _orgService.RetrieveMultiple(contactQuery).Entities.FirstOrDefault();

            Dictionary<string, string> langLocaleDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(
                contactEntity.Attributes["ddsm_customdateandtimeformat"].ToString()
            );

            Guid languageLocaleId = Guid.Empty;
            try
            {
                languageLocaleId = Guid.Parse(
                    langLocaleDict["LogicalName"].ToString()
                );
            }
            catch(FormatException)
            {
                return GetDateTimeFormatByBrowserCulture(browserCulture);
            }

            var languageLocaleQuery = new QueryExpression("languagelocale");
            languageLocaleQuery.ColumnSet = new ColumnSet("code");
            languageLocaleQuery.Criteria.AddCondition("languagelocaleid", ConditionOperator.Equal, languageLocaleId);

            Entity languageLocaleEntity = _orgService.RetrieveMultiple(languageLocaleQuery).Entities.FirstOrDefault();

            string cultureCode = languageLocaleEntity.Attributes["code"].ToString();
            var cultureInfo = CultureInfo.GetCultureInfo(cultureCode);

            var model = new DateTimeFormatModel
            {
                DateFormat = cultureInfo.DateTimeFormat.ShortDatePattern,
                TimeFormat = cultureInfo.DateTimeFormat.ShortTimePattern
            };

            return model;
        }

        private int GetDateTimeConfig(Guid userId, string fieldLogicalName, int defaultValue)
        {
            var query = new QueryExpression("contact");
            query.ColumnSet = new ColumnSet(fieldLogicalName);
            query.Criteria.AddCondition("contactid", ConditionOperator.Equal, userId);

            Entity user = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            var dateTimeConfig = user.GetAttributeValue<OptionSetValue>(fieldLogicalName);

            return dateTimeConfig == null ? defaultValue : dateTimeConfig.Value;
        }

        public string GetTimeZone(Guid userId, string browserIanaTimeZone)
        {
            int timeZoneConfig = GetDateTimeConfig(userId, "ddsm_timezoneconfig", 
                (int)TimeZoneConfig.BrowserTimeZone);

            if (timeZoneConfig == (int)TimeZoneConfig.CRMTimeZone)
            {
                return GetTimeZoneByCRMConnection();
            }
            else if (timeZoneConfig == (int)TimeZoneConfig.CustomTimeZone)
            {
                return GetTimeZoneByCustomSettings(userId, browserIanaTimeZone);
            }

            string windowsTimeZoneName = TZConvert.IanaToWindows(browserIanaTimeZone);

            return windowsTimeZoneName;
        }

        private string GetTimeZoneByCRMConnection()
        {
            var query = new QueryExpression(_usersettingsLogicalName);
            query.ColumnSet = new ColumnSet("systemuserid", "timezonecode");
            query.Criteria.AddCondition("systemuserid", ConditionOperator.EqualUserId);

            Entity userSettings = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();
            string timeZoneCode = userSettings.Attributes["timezonecode"].ToString();

            query = new QueryExpression("timezonedefinition");
            query.ColumnSet = new ColumnSet("timezonecode", "standardname");

            query.Criteria.AddCondition("timezonecode", ConditionOperator.Equal, timeZoneCode);
            Entity timeZoneDefinitions = _orgService.RetrieveMultiple(query).Entities.FirstOrDefault();

            return timeZoneDefinitions.Attributes["standardname"].ToString();
        }

        private string GetTimeZoneByCustomSettings(Guid userId, string browserIanaTimeZone)
        {
            var contactQuery = new QueryExpression("contact");
            contactQuery.ColumnSet = new ColumnSet("ddsm_customtimezone");
            contactQuery.Criteria.AddCondition("contactid", ConditionOperator.Equal, userId);

            Entity contactEntity = _orgService.RetrieveMultiple(contactQuery).Entities.FirstOrDefault();

            Dictionary<string, string> timezoneDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(
                contactEntity.Attributes["ddsm_customtimezone"].ToString()
            );

            Guid timezoneId = Guid.Empty;
            try
            {
                timezoneId = Guid.Parse(timezoneDict["LogicalName"]);
            }
            catch (FormatException)
            {
                string windowsTimeZoneName = TZConvert.IanaToWindows(browserIanaTimeZone);
                return windowsTimeZoneName;
            }

            var timezoneQuery = new QueryExpression("timezonedefinition");
            timezoneQuery.ColumnSet = new ColumnSet("standardname");
            timezoneQuery.Criteria.AddCondition("timezonedefinitionid", ConditionOperator.Equal, timezoneId);

            Entity timezoneEntity = _orgService.RetrieveMultiple(timezoneQuery).Entities.FirstOrDefault();
            string standardName = timezoneEntity.Attributes["standardname"].ToString();

            return standardName;
        }
    }
}
