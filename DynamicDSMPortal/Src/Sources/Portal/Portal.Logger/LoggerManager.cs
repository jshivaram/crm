﻿using System;
using Portal.Models.Exceptions;
using Serilog;

namespace Portal.Logger
{
    public static class LoggerManager
    {
        private static readonly ILogger _errorLogger;
        private static readonly ILogger _warningLogger;
        private static readonly ILogger _infoLogger;

        static LoggerManager()
        {
            _errorLogger = _CreateLoggerInstance("\\Log\\Errors\\Errors.txt");
            _warningLogger = _CreateLoggerInstance("\\Log\\Warnings\\Warnings.txt");
            _infoLogger = _CreateLoggerInstance("\\Log\\Infomations\\Infomations.txt");
        }

        public static void LogInformation(string message)
        {
            _infoLogger.Information(message);
        }

        public static void LogException(Exception e, string message = "")
        {
            if (e is PortalWarningException)
            {
                LogWarning((PortalWarningException)e, message);
            }
            else
            {
                LogError(e, message);
            }
        }

        public static void LogError(Exception e, string message = "")
        {
            _errorLogger.Error(e, message);
        }

        public static void LogWarning(PortalWarningException e, string message = "")
        {
            _warningLogger.Warning(e, message);
        }

        #region PrivateMethods
        private static ILogger _CreateLoggerInstance(string relativeWayToFile)
        {
            string wayToFile = _GetAbsoluteFileWay(relativeWayToFile);

            return new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.Async(a => a.RollingFile(wayToFile))
                .CreateLogger();
        }

        private static string _GetAbsoluteFileWay(string relativeWayToFile)
        {
            string dataDirectory = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
            string wayToFile = dataDirectory + relativeWayToFile;

            return wayToFile;
        }
        #endregion
    }
}
