[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Portal.SPA.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Portal.SPA.App_Start.NinjectWebCommon), "Stop")]

namespace Portal.SPA.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using BLL.Interfaces;
    using BLL.Services;
    using DAL.Interfaces;
    using DAL.Repositories;
    using System.Configuration;
    using Microsoft.Owin.Security;
    using Extensions;
    using System.Diagnostics;
    using System.IO;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            string conn = ConfigurationManager.ConnectionStrings["CrmConnection"].ConnectionString;         

            kernel.Bind<IUnitOfWork>()
                .To<CrmUnitOfWork>()
                .InRequestScope()
                .WithConstructorArgument(conn);

            kernel.Bind<IEntityService>()
                .To<EntityService>()
                .InRequestScope()
                .WithConstructorArgument(kernel.Get<IUnitOfWork>());

            kernel.Bind<ISavedQueryService>()
                .To<SavedQueryService>()
                .InRequestScope()
                .WithConstructorArgument(kernel.Get<IUnitOfWork>());

            kernel.Bind<IFormService>()
                .To<FormService>()
                .InRequestScope()
                .WithConstructorArgument(kernel.Get<IUnitOfWork>());

            kernel.Bind<IMetaDataService>()
                .To<MetaDataService>()
                .InRequestScope()
                .WithConstructorArgument(kernel.Get<IUnitOfWork>());

            kernel.Bind<IDataUploaderService>()
                .To<DataUploaderService>()
                .InRequestScope()
                .WithConstructorArgument(kernel.Get<IUnitOfWork>());

            kernel.Bind<IApplicationService>()
                .To<ApplicationService>()
                .InRequestScope()
                .WithConstructorArgument(kernel.Get<IUnitOfWork>());

            kernel.Bind<IChartService>()
                .To<ChartService>()
                .InRequestScope()
                .WithConstructorArgument(kernel.Get<IUnitOfWork>());

            kernel.Bind<IThemeService>()
                .To<ThemeService>()
                .InRequestScope()
                .WithConstructorArgument(kernel.Get<IUnitOfWork>());

            kernel.Bind<ISolutionService>()
                .To<SolutionService>()
                .WithConstructorArgument(kernel.Get<IUnitOfWork>());

            kernel.Bind<IPostsService>()
                .To<PostsService>()
                .WithConstructorArgument(kernel.Get<IUnitOfWork>());

            kernel.Bind<IAuthService>()
                .To<AuthService>()
                .WithConstructorArgument(kernel.Get<IUnitOfWork>());

            kernel.Bind<ILookupService>()
                .To<LookupService>()
                .WithConstructorArgument(kernel.Get<IUnitOfWork>());

            kernel.Bind<IPortalConfigurationService>()
                .To<PortalConfigurationService>()
                .WithConstructorArgument(kernel.Get<IUnitOfWork>());
        }        
    }
}
