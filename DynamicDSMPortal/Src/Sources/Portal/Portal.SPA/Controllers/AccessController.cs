﻿using Portal.BLL.Interfaces;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    public class AccessController : BaseApiController
    {
        private readonly IAccessService _accessService;

        public AccessController(IAccessService accessService)
        {
            _accessService = accessService;
            _accessService.UserId = CurrenUserId;
        }
        
        [HttpGet]
        public bool CanDelete(string entityLogicalName)
        {
            bool canDelete = _accessService.CanDelete(entityLogicalName);
            return canDelete;
        }

        [HttpGet]
        public bool CanCreate(string entityLogicalName)
        {
            bool canCreate = _accessService.CanCreate(entityLogicalName);
            return canCreate;
        }

        [HttpGet]
        public bool CanUpdate(string entityLogicalName)
        {
            bool canUpdate = _accessService.CanUpdate(entityLogicalName);
            return canUpdate;
        }
    }
}