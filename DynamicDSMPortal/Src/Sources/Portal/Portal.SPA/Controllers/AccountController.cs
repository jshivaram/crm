﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Portal.BLL.Interfaces;
using Portal.Models;
using Portal.Models.Account;
using Portal.Models.EntityModels;
using Portal.SPA.Filter;

namespace Portal.SPA.Controllers
{
    [CustomExceptionFilter]
    public class AccountController : ApiController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet]
        public async Task<AccountInfoForApplication> GetAccountInfoByAccountNumber([FromUri] ArgModel accountNumber)
        {
            return await _accountService.GetAccountInfoByAccountNumberAsync(accountNumber.Value);
        }

        [Authorize]
        public async Task<IReadOnlyList<LookupModel>> GetContractorsForLookup([FromUri]KendoFilters filter)
        {
            return await _accountService.GetContractorsAsync(filter?.Filters?.FirstOrDefault()?.Value);
        }
    }
}