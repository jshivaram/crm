﻿using Microsoft.Xrm.Sdk;
using Portal.BLL.Interfaces;
using Portal.DAL.Interfaces;
using Portal.Models.AnnotationModels;
using Portal.Models.AttachmentsDocument;
using Portal.Models.GridModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using Microsoft.Xrm.Sdk.Query;
using Portal.SPA.Filter;

namespace Portal.SPA.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    public class AnnotationController : BaseApiController
    {
        private readonly IUnitOfWork _uow;
        private IAnnotationService _annotationService;
        private IEntityService _entityService;

        public AnnotationController(IUnitOfWork uow, IAnnotationService annotationService, IEntityService entityService)
        {
            _uow = uow;

            _annotationService = annotationService;
            _annotationService.UserId = CurrenUserId;

            _entityService = entityService;
            _entityService.UserId = CurrenUserId;
        }

        public GridViewModel<AttachmentsDocumentViewModel> GetRelatedAnnotations(
            [FromUri] AnnotationGetModel annotationGetModel)
        {
            string userLanguage = HttpContext.Current.Request.UserLanguages.FirstOrDefault();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(userLanguage);

            IEnumerable<AttachmentsDocumentViewModel> annotations = _annotationService
                .GetRelatedAttachmentsDocuments(
                    annotationGetModel.RelatedEntityLogicalName,
                    annotationGetModel.RelatedEntityId,
                    culture,
                    annotationGetModel.BrowserIanaTimeZone);

            var gridData = new GridViewModel<AttachmentsDocumentViewModel>
            {
                Data = annotations,
                Total = annotations.Count()
            };

            return gridData;
        }

        [HttpGet]
        public HttpResponseMessage DownloadFile(Guid annotationId)
        {
            // sendo file to client
            var file = _uow.AnnotationRepository.DownladAnnotationFile(annotationId);

            if (file == null || file.FileContent == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.Gone, "Cant download file.");
            }

            byte[] bytes = Convert.FromBase64String(file.FileContent);

            HttpResponseMessage result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(bytes);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = file.FileName;

            return result;
        }

        [HttpPost]
        public HttpResponseMessage Create()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            try
            {
                HttpRequest request = HttpContext.Current.Request;
                string id = request.Form["id"];
                string logicalName = request.Form["logicalname"];

                //hardcode for project start

                if (logicalName == "ddsm_project")
                {
                    var mileStoneQuery = new QueryExpression("ddsm_milestone")
                    {
                        ColumnSet = new ColumnSet("ddsm_index")
                    };

                    mileStoneQuery.Criteria.AddCondition("ddsm_status", ConditionOperator.Equal, 962080001);
                    var projectId = Guid.Parse(id);
                    mileStoneQuery.Criteria.AddCondition("ddsm_projecttomilestoneid", ConditionOperator.Equal, projectId);

                    IEnumerable<Entity> mileStones = _uow.EntityRepository.GetAllWithTotalCount(mileStoneQuery).Entities;
                    Entity milestone = mileStones.FirstOrDefault();

                    if (milestone == null)
                    {
                        throw new Exception("milestone is null");
                    }

                    var ddsmIndex = milestone.GetAttributeValue<int?>("ddsm_index");

                    if (ddsmIndex == null)
                    {
                        throw new Exception("ddsmIndex is null");
                    }

                    var requiredDocumentQuery = new QueryExpression("ddsm_requireddocument")
                    {
                        ColumnSet = new ColumnSet("ddsm_uploaded")
                    };

                    requiredDocumentQuery.Criteria.AddCondition("ddsm_requiredbystatus", ConditionOperator.Equal, ddsmIndex.Value);
                    requiredDocumentQuery.Criteria.AddCondition("ddsm_project", ConditionOperator.Equal, projectId);

                    var requiredDocuments = _uow.EntityRepository.GetAllWithTotalCount(requiredDocumentQuery).Entities;

                    foreach (var requiredDocument in requiredDocuments)
                    {
                        if (requiredDocument == null)
                        {
                            throw new Exception("requiredDocument is null");
                        }

                        requiredDocument["ddsm_uploaded"] = true;
                    }

                    _uow.EntityRepository.UpdateMultiple(requiredDocuments);
                }

                //hardcode for project start end

                string entityDataJson = request.Form["entityDataJson"];

                IEnumerable<HttpPostedFile> postedFiles = request.Files.GetMultiple("uploadedfile");

                var entityReference = new EntityReference(logicalName);

                if (!string.IsNullOrEmpty(id))
                {
                    entityReference.Id = Guid.Parse(id);
                }
                else
                {
                    entityReference.Id = _entityService.CreateAndReturnId(entityDataJson, logicalName);
                }

                foreach (HttpPostedFile postedFile in postedFiles)
                {
                    var binaryReader = new BinaryReader(postedFile.InputStream);
                    byte[] uploadedFile = binaryReader.ReadBytes(postedFile.ContentLength);

                    _uow.AnnotationRepository.CreateAnnotation(uploadedFile, postedFile.FileName, entityReference);
                }

                var documentResponseModel = new DocumentUploadResponseModel
                {
                    EntityId = entityReference.Id,
                    EntityLogicalName = logicalName,
                    DocumentAttachedToExistedEntity = !string.IsNullOrEmpty(id)
                };

                return Request.CreateResponse(HttpStatusCode.OK, documentResponseModel);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPut]
        public void ChangeFileName(Guid annotationId, string fileName)
        {
            _uow.AnnotationRepository.ChangeFileName(annotationId, fileName);
        }

        [HttpPut]
        public void ChangeFilesNames([FromBody]AnnotationChangeModel model)
        {
            foreach (var changeModel in model.ChangeModels)
            {
                _uow.AnnotationRepository.ChangeFileName(changeModel.AnnotationId, changeModel.FileName);
            }
        }

        [HttpDelete]
        public void Remove(Guid id)
        {
            _uow.AnnotationRepository.Remove(id);
        }

        [HttpDelete]
        public void Remove([FromBody]AnnotationRemoveModel model)
        {
            _uow.AnnotationRepository.Remove(model.IdList);
        }
    }
}
