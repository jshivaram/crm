﻿using Microsoft.Owin.Security;
using Microsoft.Xrm.Sdk;
using Portal.BLL.Interfaces;
using Portal.BLL.Services;
using Portal.DAL.Repositories;
using Portal.Models;
using Portal.Models.Application;
using Portal.Models.EntityModels;
using Portal.SPA.Extensions;
using Portal.SPA.Filter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    [CustomExceptionFilter]
    public class ApplicationController : ApiController
    {
        private readonly IApplicationService _applicationService;

        public ApplicationController(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        [HttpGet]
        public IEnumerable<MeasureTemplateModel> GetMeasureTemplates([FromUri] IEnumerable<Guid> idList)
        {
            var result = _applicationService.GetMeasureTemplates(idList);
            return result;
        }

        [HttpGet]
        public IEnumerable<LookupModel> GetMeasureTemplatesForLookup(Guid projectTemplateId)
        {
            var result = _applicationService.GetMeasureTemplatesForLookup(projectTemplateId);
            return result;
        }

        [HttpGet]
        public IEnumerable<LookupModel> GetApplicationsFormListForUnregisters()
        {
            var result = _applicationService.GetApplicationsFormListForUnregisters();
            return result;
        }

        [HttpGet]
        public string GetFormStructureForUnregisterApplication(Guid id)
        {
            string result = _applicationService.GetFormStructureForUnregisterApplication(id);
            return result;
        }

        [HttpPost]
        public Guid Create([FromBody]EntityModel entityModel)
        {
            return _applicationService.CreateAndReturnId(entityModel.JsonEntityData, entityModel.LogicalName);
        }
    }
}
