﻿using Portal.BLL.Interfaces;
using Portal.Models;
using Portal.Models.Approve;
using Portal.Models.EntityModels;
using Portal.SPA.Filter;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    public class ApproveController : BaseApiController
    {
        private readonly IAprroveService _approveService;

        public ApproveController(IAprroveService approveService)
        {
            _approveService = approveService;
            _approveService.UserId = CurrenUserId;
        }

        [HttpPost]
        public Guid? CreateAndApprove([FromBody] CreateAndApproveModel entityModel)
        {
            return _approveService.CreateAndApprove(entityModel);
        }

        public async Task<IEnumerable<LookupModel>> GetMeasureTemplates([FromUri]
            LookupFilterModelWithLogicalNameAndProgramOffering lookupFilterModelWithLogicalNameAndProgramOffering)
        {
            return await _approveService.GetMeasureTemplatesAsync(
                lookupFilterModelWithLogicalNameAndProgramOffering.Filter, 
                lookupFilterModelWithLogicalNameAndProgramOffering.ProgramOfferingId);
        }

        public async Task<IEnumerable<LookupModel>> GetProgramOfferings([FromUri] LookupFilterModelWithLogicalName lookupFilterModelWithLogicalName)
        {
            return await _approveService.GetProgramOfferingsAsync(lookupFilterModelWithLogicalName.Filter);
        }

        [HttpGet]
        public bool IsModelNumberApproved(Guid id)
        {
            return _approveService.IsModelNumberApproved(id);
        }

        [HttpGet]
        public bool IsSkuApproved(Guid id)
        {
            return _approveService.IsSkuApproved(id);
        }

        [HttpPost]
        public void ApproveModelNumber([FromBody] ApproveModel approveMnModel)
        {
            _approveService.ApproveModelNumber(approveMnModel);
        }

        [HttpPost]
        public void ApproveSku([FromBody] ApproveModel approveModel)
        {
            _approveService.ApproveSku(approveModel);
        }
    }
}
