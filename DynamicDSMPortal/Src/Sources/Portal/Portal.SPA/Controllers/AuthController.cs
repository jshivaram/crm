﻿using Portal.BLL.Interfaces;
using Portal.BLL.Services;
using Portal.DAL.Repositories;
using Portal.Models.Auth;

using System;
using System.Collections.Generic;
using System.Configuration;

using System.Net;
using System.Net.Http;

using System.Web.Http;

using System.Security.Claims;
using Microsoft.Owin.Security;
using System.Web;
using System.Web.Http.ModelBinding;
using Portal.SPA.Extensions;
using Microsoft.Owin.Security.Cookies;
using System.Web.Security;
using Microsoft.Xrm.Sdk;
using System.Linq;

namespace Portal.SPA.Controllers
{
    public class AuthController : ApiController
    {
        private IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }

        [HttpGet]
        public bool IsUserAuthorized()
        {
            if(AuthenticationManager.User == null || !AuthenticationManager.User.Identity.IsAuthenticated)
            {
                return false;
            }

            return true;
        }

        [HttpGet]
        [Authorize]
        public string GetAuthorizedUserName()
        {
            return AuthenticationManager.User.Identity.Name;
        }

        [HttpGet]
        [Authorize]
        public AuthorizedUserDataModel GetAuthorizedUserData()
        {
            return new AuthorizedUserDataModel
            {
                Login = AuthenticationManager.User.Identity.Name,
                UserId = new Guid(AuthenticationManager.User.Identity.GetUserId<string>()),
                FullName = AuthenticationManager.User.Claims.FirstOrDefault(c => c.Type.ToLower().Contains("surname"))?.Value
            };
        }

        [HttpPost]
        public HttpResponseMessage Login([FromBody] LoginModel loginModel)
        {
            if(!ModelState.IsValid)
            {
                return CreateResponseWithModelStateErrors(ModelState);
            }

            try
            {
                bool contactExists = _authService.ContactExists(loginModel.Login);

                if (!contactExists)
                {
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        ReasonPhrase = "Cant find user with this login."
                    };
                }

                Entity contact = _authService.GetContact(loginModel.Login, loginModel.Password);

                if (contact == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        ReasonPhrase = "Incorect login or password."
                    };
                }

                object isPortalUserObj = null;
                contact.Attributes.TryGetValue("ddsm_isportaluser", out isPortalUserObj);

                bool isPortalUser = false;
                bool.TryParse(isPortalUserObj?.ToString(), out isPortalUser);

                if (!isPortalUser)
                {
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        ReasonPhrase = "You cant login, because you are not portal user."
                    };
                }

                var claim = new ClaimsIdentity("ApplicationCookie", 
                    ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);

                claim.AddClaim(new Claim(
                    System.Security.Claims.ClaimTypes.NameIdentifier, 
                    contact.Id.ToString(), 
                    ClaimValueTypes.String));

                claim.AddClaim(new Claim(
                    System.Security.Claims.ClaimTypes.Surname,
                    contact["fullname"].ToString(),
                    ClaimValueTypes.String));

                claim.AddClaim(new Claim(ClaimsIdentity.DefaultNameClaimType, loginModel.Login, 
                    ClaimValueTypes.String));

                claim.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                    "OWIN Provider", ClaimValueTypes.String));


                AuthenticationManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true
                }, claim);

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    ReasonPhrase = "Login success."
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    ReasonPhrase = "Error. Cant login."
                };
            }

        }

        [Authorize]
        [HttpPost]        
        public void Logout()
        {
            AuthenticationManager.SignOut();
        }

        public HttpResponseMessage Register(RegisterModel registerModel)
        {
            if (!ModelState.IsValid)
            {
                return CreateResponseWithModelStateErrors(ModelState);
            }

            bool contactExists = _authService.ContactExists(registerModel.Login);

            if (contactExists)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    ReasonPhrase = "User with this login is alredy exists."
                };
            }

            try
            {
                _authService.Register(registerModel);
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    ReasonPhrase = "Register Success."
                };
            }
            catch(Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    ReasonPhrase = "Error. Cant Register."
                };
            }
        }

        private HttpResponseMessage CreateResponseWithModelStateErrors(ModelStateDictionary modelState)
        {
            string json = "[";

            int count = 0;

            foreach (var state in modelState)
            {
                foreach (var value in state.Value.Errors)
                {
                    string error = "{\"" + state.Key.Replace("registerModel.", "")
                                                  .Replace("loginModel.", "") + "\" : \"" + value.ErrorMessage + "\"}";

                    json += error;

                    count++;

                    if (count != modelState.Count)
                    {
                        json += ",";
                    }                    
                }
            }

            json += "]";

            return new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                ReasonPhrase = json
            };
        }
    }
}
