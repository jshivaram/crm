﻿using Microsoft.Owin.Security;
using Portal.SPA.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        public Guid CurrenUserId
        {
            get
            {
                try
                {
                    IAuthenticationManager authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                    Guid userId = new Guid(authenticationManager.User.Identity.GetUserId<string>());
                    return userId;
                }
                catch(Exception ex)
                {
                    return new Guid();
                }
               
            }
        }
    }
}
