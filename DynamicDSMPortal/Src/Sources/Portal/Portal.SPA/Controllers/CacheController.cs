﻿using System;
using System.IO;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    public class CacheController : ApiController
    {
        [HttpPost]
        public void ClearAllCache()
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Cache");

            if (path == null)
            {
                throw new Exception("Can`t find Cache foler.");
            }

            Directory.Delete(path, true);
        }
    }
}