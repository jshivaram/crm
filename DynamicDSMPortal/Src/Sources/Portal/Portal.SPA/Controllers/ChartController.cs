﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.BLL.Interfaces;
using Portal.Models.EntityModels;
using Portal.Models.GridModels;
using Portal.SPA.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    public class ChartController : BaseApiController
    {
        private readonly IChartService _chartService;
        private readonly IMetaDataService _metaDataService;
        public ChartController(IChartService chartService, IMetaDataService metaDataService)
        {
            _chartService = chartService;
            _chartService.UserId = CurrenUserId;
            _metaDataService = metaDataService;
            _metaDataService.UserId = CurrenUserId;
        }

        [HttpGet]
        public async Task<ChartDataModel<IDictionary<string, string>>> GetAll(
        [FromUri]ChartModel chartModel)
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                var records = _chartService.GetAllData(chartModel.ViewId,
                chartModel.EntityLogicalName, chartModel.Count, chartModel.VisualizationId);
                var visualizationData = _chartService.GetVisualizationData(chartModel.VisualizationId);
                var metaData = _chartService.GetAttributesMetadataForChart(chartModel.EntityLogicalName, 
                    chartModel.ViewId, chartModel.VisualizationId);
                var viewXml = _chartService.GetViewXmlForChart(chartModel.ViewId, chartModel.VisualizationId);
                var view = _chartService.GetSavedQueryForChart(chartModel.ViewId);
                var result = new ChartDataModel<IDictionary<string, string>>
                {
                    Data = records.Items,
                    VisualizationData = visualizationData,
                    AttrMetadata = metaData,
                    ViewXml = viewXml,
                    View = view
                };
                return result;
            });
        }
    }
}
