﻿using Portal.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    [Authorize]
    public class ConfigurationController : BaseApiController
    {
        private IPortalConfigurationService _portalConfigurationService;

        public ConfigurationController(IPortalConfigurationService portalConfigurationService)
        {
            _portalConfigurationService = portalConfigurationService;
            _portalConfigurationService.UserId = CurrenUserId;
        }

        [HttpGet]
        public string GetDefaultPageUrlAfterLogin()
        {
            string url = _portalConfigurationService.GetDefaultAfterLoginUrl();
            return url;
        } 
    }
}
