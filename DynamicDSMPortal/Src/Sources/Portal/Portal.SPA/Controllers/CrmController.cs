﻿using System.Web.Http;
using Portal.BLL.Interfaces;
using Portal.BLL.Services;
using Portal.Models.CrmModel;
using Portal.SPA.Filter;

namespace Portal.SPA.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    public class CrmController : ApiController
    {
        private readonly ICrmService _crmService;

        public CrmController()
        {
            _crmService = new CrmService();
        }

        [HttpGet]
        public CrmModel GetUrl()
        {
            return new CrmModel
            {
                Url = _crmService.GetUrl()
            };
        }
    }
}