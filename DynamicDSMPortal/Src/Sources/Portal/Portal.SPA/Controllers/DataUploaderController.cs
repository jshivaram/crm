﻿using Newtonsoft.Json.Linq;
using Portal.BLL.Interfaces;
using Portal.BLL.Services;
using Portal.DAL.Interfaces;
using Portal.DAL.Repositories;
using Portal.Models.DataUploader;
using Portal.SPA.Filter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    public class DataUploaderController : BaseApiController
    {
        private IDataUploaderService _dataUploaderService;

        public DataUploaderController(IDataUploaderService dataUploaderService)
        {
            _dataUploaderService = dataUploaderService;
            _dataUploaderService.UserId = CurrenUserId;
        }

        [HttpPost]
        public async Task<HttpResponseMessage> CreateDataUploaderRecord()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                await Request.Content.ReadAsMultipartAsync(provider);

                MultipartFileData file = provider.FileData.FirstOrDefault();
                int typeUploadedData = int.Parse(provider.FormData["typeUploadedData"]);

                byte[] bytes = File.ReadAllBytes(file.LocalFileName);
                _dataUploaderService.CreateDataUploaderRecord(typeUploadedData, bytes, file.Headers.ContentDisposition.FileName);

                File.Delete(file.LocalFileName);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
    }
}