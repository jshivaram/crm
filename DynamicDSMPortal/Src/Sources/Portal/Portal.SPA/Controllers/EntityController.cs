﻿using Microsoft.Xrm.Sdk;
using Portal.BLL.Extensions;
using Portal.BLL.Interfaces;
using Portal.BLL.Services;
using Portal.DAL.Interfaces;
using Portal.DAL.Repositories;
using Portal.Models.EntityModels;
using Portal.SPA.Filter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Dynamic;
using Portal.Models.GridModels;
using Microsoft.Owin.Security;
using System.Web;
using Portal.SPA.Extensions;
using Portal.Models;
using System.Threading.Tasks;
using SharpTask = System.Threading.Tasks.Task;
using Portal.Models.Navigation;

namespace Portal.SPA.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    public class EntityController : BaseApiController
    {
        private readonly IEntityService _entityService;
        private readonly ISavedQueryService _savedQueryService;
        private readonly ILookupService _lookupService;

        public EntityController(IEntityService entityService, ISavedQueryService savedQueryService,
            ILookupService lookupService)
        {        
            _entityService = entityService;
            _entityService.UserId = CurrenUserId;

            _savedQueryService = savedQueryService;
            _savedQueryService.UserId = CurrenUserId;

            _lookupService = lookupService;
        }

        [HttpGet]
        public async Task<GridViewModel<IDictionary<string, string>>> GetAll(
            string entityLogicalName, [FromUri]GridPagingModel pagination,
            Guid viewId, string searchText = null)
        {
            return await SharpTask.Factory.StartNew(() =>
            {
                var records = _entityService.GetEntitiesDictionaryListWithCount(
                    entityLogicalName, pagination, viewId, searchText);

                var result = new GridViewModel<IDictionary<string, string>>
                {
                    Data = records.Items,
                    Total = records.TotalCount
                };

                return result;
            });
        }

        [HttpGet]
        public async Task<Entity> Get(Guid id, string entityLogicalName)
        {
            return await SharpTask.Factory.StartNew(() =>
            {
                Entity entity = _entityService.Get(id, entityLogicalName);
                return entity;
            });
        }

        [HttpGet] 
        public EntityReference GetRelatedEntityForLookup([FromUri]RelatedEntityLookupModel lookupModel)
        {

            if (lookupModel == null)
            {
                throw new ArgumentNullException("lookupModel");
            }

            Entity entity = _entityService.Get(lookupModel.Id, lookupModel.EntityLogicalName);

            if(entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            if(!entity.Contains(lookupModel.DataFieldName))
            {
                return null;
            }

            EntityReference lookupEntity = entity.Attributes[lookupModel.DataFieldName] as EntityReference;
            return lookupEntity;
        }

        [HttpGet]
        public async Task<GridViewModel<IDictionary<string, string>>> GetRelatedEntities(
            [FromUri]RelatedEntityModel relatedEntityModel, 
            [FromUri]GridPagingModel pagination)
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                if(relatedEntityModel.EntityId == new Guid())
                {
                    return new GridViewModel<IDictionary<string, string>>
                    {
                        Data = null,
                        Total = 0
                    };
                }

                var relatedEntities = _entityService.GetRelatedEntitiesDictionaryListWithTotalCount(
                    relatedEntityModel.EntityId,
                    relatedEntityModel.ViewId,
                    relatedEntityModel.EntityLogicalName,
                    relatedEntityModel.RelatedEntityName,
                    relatedEntityModel.RelationshipSchemaName,
                    pagination);

                var result = new GridViewModel<IDictionary<string, string>>
                {
                    Data = relatedEntities.Items,
                    Total = relatedEntities.TotalCount
                };

                return result;
            });            
        }

        [HttpGet]
        public GridViewModel<object> GetRelatedEntitiesForApplication(
            [FromUri]RelatedEntityModelWithoutViewId relatedEntityModel,
            [FromUri]GridPagingModel pagination)
        {
            Guid defaultViewId = _savedQueryService.GetDefaultViewId(relatedEntityModel.EntityLogicalName);

            IEnumerable<object> entities = _entityService.GetRelatedEntities(
               relatedEntityModel.EntityId,
               defaultViewId,
               relatedEntityModel.EntityLogicalName,
               relatedEntityModel.RelatedEntityName,
               relatedEntityModel.RelationshipSchemaName
           );

            GridViewModel<object> result = new GridViewModel<object>
            {
                Data = entities.Skip(pagination.Skip).Take(pagination.Take).ToList(),
                Total = entities.Count()
            };

            return result;
        }

        [HttpGet]
        public Entity GetRelatedEntity(string entityLogicalName, string relationshipShcemeName)
        {
            Entity result = _entityService.GetRelatedEntity(entityLogicalName, relationshipShcemeName);
            return result;
        }

        [HttpGet]
        public IEnumerable<LookupModel> GetEntitiesForLookup(Guid viewId)
        {
            IEnumerable<LookupModel> entities = _entityService.GetEntitiesForLookup(viewId);
            return entities;
        }

        [HttpGet]
        public IEnumerable<LookupModel> GetEntitiesForLookup(string entityLogicalName)
        {
            IEnumerable<LookupModel> entities = _entityService.GetEntitiesForLookup(entityLogicalName);
            return entities;
        }

        [HttpGet]
        public IEnumerable<LookupModel> GetFilteredEntitiesForLookup([FromUri]LookupFilterModel lookupFilterModel)
        {
            var result = _lookupService.GetEntitiesForLookup(lookupFilterModel);
            return result;
        }

        [HttpGet]
        public IEnumerable<LookupModel> GetEntitiesForGridLookup([FromUri]GridLookupGetModel gridLookupGetModel)
        {
            IEnumerable<LookupModel> entities = _entityService.GetEntitiesForGridLookupByField(
                gridLookupGetModel.EntityLogicalName,
                gridLookupGetModel.FieldName);

            return entities;
        }

        [HttpPost]
        public Guid Create([FromBody]EntityModel entityModel)
        {
            return _entityService.CreateAndReturnId(entityModel.JsonEntityData, entityModel.LogicalName);
        }

        [HttpPost]
        public void CreateRelatedEntities([FromBody] RelatedEntitiesCreationModel relatedEntitiesCreationModel)
        {
            _entityService.CreateAndAssociateRelatedEntities(relatedEntitiesCreationModel);
        }

        [HttpPut]
        public void Update([FromBody]EntityModel entityModel)
        {
            _entityService.Update(entityModel.JsonEntityData, entityModel.LogicalName);
        }

        [HttpPut]
        public void UpdateMultiple([FromBody]EntityListModel entityListModel)
        {
            _entityService.Update(entityListModel);
        }

        [HttpDelete]
        public async SharpTask Delete(Guid id, string entityLogicalName)
        {
            await SharpTask.Factory.StartNew(() =>
            {
                _entityService.Delete(id, entityLogicalName);
            });
        }

        [HttpDelete]
        public async SharpTask DeleteRange(
            [FromUri]IEnumerable<Guid> idArr, string entityLogicalName)
        {
            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                _entityService.DeleteRange(idArr, entityLogicalName);
            });
        }

        [HttpDelete]
        public async SharpTask DeleteRelatedMultiple(
            [FromBody]DeleteEntitiesModel deleteEntitiesModel)
        {
            await SharpTask.Factory.StartNew(() =>
            {
                _entityService.DeleteRange(deleteEntitiesModel.IdArr,
                    deleteEntitiesModel.EntityLogicalName);
            });
        }

        [HttpGet]
        public IEnumerable<NavigationEntityModel> GetEntitiesForNavigation()
        {
            var entities = _entityService.GetEntitiesForNavigation();
            return entities;
        }
    }
}
