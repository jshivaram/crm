﻿using Portal.BLL.Interfaces;
using Portal.BLL.Services;
using Portal.DAL;
using Portal.DAL.Repositories;
using Portal.Models;
using Portal.Models.Form;
using Portal.SPA.Extensions;
using Portal.SPA.Filter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using SharpTask = System.Threading.Tasks.Task;

namespace Portal.SPA.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    public class FormController : BaseApiController
    {
        IFormService _formService;

        public FormController(IFormService formService)
        {
            _formService = formService;
            _formService.UserId = CurrenUserId;
        }

        public async Task<IEnumerable<LookupModel>> GetDashboards()
        {
            return await SharpTask.Factory.StartNew(() =>
            {
                var dashBoards = _formService.GetDashboards();
                dashBoards = dashBoards.OrderBy(d => d.Text).ToList();
                return dashBoards;
            });
        }

        public async Task<Guid?> GetDefaultDashboardId()
        {
            return await SharpTask.Factory.StartNew(() =>
            {
                Guid? defaultDashBoardId = _formService.GetDefaultDashboardId();
                return defaultDashBoardId;
            });
        }

        public async Task<string> Get(string entityLogicalName)
        {
            string jsonText = await _formService.GetJsonFormStructureAsync(entityLogicalName);
            return jsonText;
        }

        public async Task<IEnumerable<object>> GetForms(string entityLogicalName)
        {
            string contactIdStr = HttpContext.Current.GetOwinContext()
                .Authentication.User.Identity.GetUserId<string>();

            return await SharpTask.Factory.StartNew(() =>
            {

                if (string.IsNullOrEmpty(contactIdStr))
                {
                    throw new Exception("Portal User not found.");
                }

                Guid contactId = new Guid(contactIdStr);

                IEnumerable<object> forms = _formService.GetForms(entityLogicalName, contactId);

                return forms;
            });
        }

        public async Task<Guid?> GetDefaultFormId(string entityLogicalName)
        {
            string contactIdStr = HttpContext.Current.GetOwinContext()
              .Authentication.User.Identity.GetUserId<string>();

            return await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                if (string.IsNullOrEmpty(contactIdStr))
                {
                    throw new Exception("Portal User not found.");
                }

                Guid contactId = new Guid(contactIdStr);

                var result = _formService.GetDefaultFormId(entityLogicalName, contactId);
                return result;
            });
        }

        public async Task<string> GetForm([FromUri]FormGetModel formModel)
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {

                var result = _formService.GetJsonFormStructure(
                formModel.EntityLogicalName,
                formModel.FormName);

                return result;
            });
        }

        public async Task<string> GetFormByIdOnly(Guid formId)
        {
            return await SharpTask.Factory.StartNew(() =>
            {
                var result = _formService.GetJsonFormStructure(formId, 0);
                return result;
            });
        }

        public async Task<string> GetFormById([FromUri]FormGetByIdModel formModel)
        {
            return await SharpTask.Factory.StartNew(() =>
            {

                var result = _formService.GetJsonFormStructure(
                formModel.EntityLogicalName,
                formModel.FormId);

                return result;
            });
        }

        public async Task<string> GetBulkEditFormById([FromUri]FormGetByIdModel formModel)
        {
            return await SharpTask.Factory.StartNew(() =>
            {

                var result = _formService.GetJsonFormStructure(
                formModel.EntityLogicalName,
                formModel.FormId,
                FormTypeEnum.FormTypeBulkEdit);

                return result;
            });
        }
    }
}
