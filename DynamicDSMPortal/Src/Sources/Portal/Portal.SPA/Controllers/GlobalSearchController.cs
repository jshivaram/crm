﻿using Portal.DAL.Interfaces;
using Portal.Models.GlobalSearch;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Portal.SPA.Controllers
{
    public class GlobalSearchController : BaseApiController
    {
        private readonly IUnitOfWork _uow;

        public GlobalSearchController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<IReadOnlyList<GlobalSearchEntity>> GetGlobalSearchConfig()
        {
            return await _uow.GlobalSearchRepository.GetGlobalSearchConfigAsync(CurrenUserId);
        }
    }
}