﻿using Microsoft.Xrm.Sdk.Metadata;
using Portal.BLL.Interfaces;
using Portal.BLL.Services;
using Portal.DAL.Repositories;
using Portal.Models.Metadata;
using Portal.SPA.Filter;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    public class MetaDataController : BaseApiController
    {
        private IMetaDataService _metaDataService;

        public MetaDataController(IMetaDataService metaDataService)
        {
            _metaDataService = metaDataService;
            _metaDataService.UserId = CurrenUserId;
        }

        [HttpGet]
        public async Task<OptionSetMetadata> GetOptionSetMetadata(
            string entityLogicalName, string fieldName)
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                return _metaDataService.GetOptionSet(entityLogicalName, fieldName);
            });
        }

        [HttpGet]
        public async Task<IEnumerable<object>> GetMetaDataForDropDownList(
            [FromUri] AttributeMetadataGetModel attributeMetadata)
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                return _metaDataService.GetMetaDataForDropDownList(
                    attributeMetadata.EntityLogicalName, 
                    attributeMetadata.FieldName);
            });
        }

        [HttpGet]
        public string GetAttributeEntityName([FromUri] AttributeMetadataGetModel attributeMetadata)
        {
            return _metaDataService.GetLookupFieldEntityName(
                   attributeMetadata.EntityLogicalName,
                   attributeMetadata.FieldName);
        }


        [HttpGet]
        public async Task<string> GetProcesStageByEntityId(string entityId)
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                return _metaDataService.GetProcesStageByEntityId(entityId);
            });
        }

        [HttpGet]
        public async Task<IEnumerable<AttributeMetadataModel>> GetAttributesMetadata(
            string entityLogicalName, Guid viewId)
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                return _metaDataService.GetAttributesMetadata(entityLogicalName, viewId);
            });
        }
    }
}
