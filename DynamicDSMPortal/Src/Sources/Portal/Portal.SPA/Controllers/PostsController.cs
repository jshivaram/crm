﻿using Microsoft.Xrm.Sdk;
using Portal.BLL.Interfaces;
using Portal.SPA.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    [CustomExceptionFilter]
    [Authorize]
    public class PostsController : BaseApiController
    {
        private IPostsService _postService;

        public PostsController(IPostsService postService)
        {
            _postService = postService;
        }

        public IEnumerable<Entity> GetPostsByEntityId(Guid entityId)
        {
            var posts = _postService.GetPostsByEntityId(entityId);
            return posts;
        }
    }
}
