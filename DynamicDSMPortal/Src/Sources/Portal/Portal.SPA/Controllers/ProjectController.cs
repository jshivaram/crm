﻿using Portal.BLL.Interfaces;
using Portal.Models.EntityModels;
using Portal.SPA.Filter;
using System.Threading.Tasks;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    public class ProjectController : BaseApiController
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
            _projectService.UserId = CurrenUserId;
        }

        [HttpGet]
        public async Task<bool> CantStep()
        {
            return await _projectService.CantStepAsync();
        }

        [HttpPost]
        public async Task<string> GotToTheNextStep([FromBody] IdModel projectIdModel)
        {
            return await _projectService.GoToNextStepAsync(projectIdModel?.Id);
        }
    }
}