﻿using Portal.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    public class SolutionController : ApiController
    {
        private ISolutionService _solutionService;

        public SolutionController(ISolutionService solutionService)
        {
            _solutionService = solutionService;
        }

        public string GetPortalSolutionVersion()
        {
            string version = _solutionService.GetPortalSolutionVersion();
            return version;
        }
    }
}
