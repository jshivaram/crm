﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Portal.BLL.Interfaces;
using Portal.Models;

namespace Portal.SPA.Controllers
{
    public class ThemesController : BaseApiController
    {
        private IThemeService _themeService;

        public ThemesController(IThemeService themeService)
        {
            _themeService = themeService;
            _themeService.UserId = CurrenUserId;
        }

        public IEnumerable<LookupModel> GetThemesFilesNames()
        {
            List<string> filesNames = _themeService.GetThemesNamesList().ToList();

            if(CurrenUserId != new Guid())
            {
                string defaultThemeName = _themeService.GetDefaultThemeName();

                if (!string.IsNullOrEmpty(defaultThemeName))
                {
                    filesNames.RemoveAll(f => f == defaultThemeName);
                    filesNames.Insert(0, defaultThemeName);
                }
            }         

            var result = filesNames.Select(f => new LookupModel
            {
                Value = f,
                Text = f
            }).ToList();

            return result;
        }

        public string GetDefaultThemeName()
        {
            string defaultThemeName = _themeService.GetDefaultThemeName();
            return defaultThemeName;
        }

        private IEnumerable<LookupModel> GetFilesNamesFromThemesFolder()
        {

            string path = HttpContext.Current.Request.MapPath("~/Content/Themes");
            var themesDir = new DirectoryInfo(path);

            if (!themesDir.Exists)
            {
                throw new Exception("Themes folder not found!");
            }

            IEnumerable<FileInfo> styleFiles = themesDir.GetFiles("*.css")
                      .Where(f => !f.Name.ToLower().Contains("ddsm_")
                                  && !f.Name.ToLower().Contains("dataviz")
                                  && !f.Name.ToLower().Contains("common")
                                  && !f.Name.ToLower().Contains("default")
                                  && !f.Name.ToLower().Contains("mobile"))
                          .ToList();

            var filesNames = styleFiles
                .Select(f => FormThemeFileName(f.Name))
                .Select(name => new LookupModel
                {
                    Value = name,
                    Text = name
                })
                .ToList();

            return filesNames;
        }

        private string FormThemeFileName(string fileName)
        {
            return fileName
                .Replace("ddsm_", "")
                .Replace("kendo.", "")
                .Replace(".min.css", "")
                .Replace(".mobile.", "");
        }
    }
}
