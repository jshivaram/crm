﻿using Portal.DAL.Interfaces;
using Portal.Models.UserSettings;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    [Authorize]
    public class UserSettingsController : BaseApiController
    {
        public IUnitOfWork _uow;

        public UserSettingsController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [HttpGet]
        public DateTimeFormatModel GetDateFormat()
        {
            // Use it by condition (from contact config)
            string userLanguage = HttpContext.Current.Request.UserLanguages.FirstOrDefault();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(userLanguage);

            return _uow.UserSettingsRepository.GetDateTimeFormat(CurrenUserId, culture);
        }
    }
}
