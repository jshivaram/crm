﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    public class VersionController : ApiController
    {
        public string GetPortalVersion()
        {
            string dataDirectory = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
            string fileName = "portal_version.txt";
            string version = File.ReadAllText(dataDirectory + "/" + fileName);
            return version;
        }
    }
}
