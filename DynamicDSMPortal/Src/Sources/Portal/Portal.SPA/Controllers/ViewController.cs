﻿using Microsoft.Owin.Security;
using Portal.BLL.Interfaces;
using Portal.BLL.Services;
using Portal.DAL.Repositories;
using Portal.Models.Lookup;
using Portal.Models.View;
using Portal.SPA.Extensions;
using Portal.SPA.Filter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    public class ViewController : BaseApiController
    {
        private readonly ISavedQueryService _savedQueryService;
        private readonly IMetaDataService _metaDataService;

        public ViewController(ISavedQueryService savedQueryService, IMetaDataService metaDataService)
        {
            _savedQueryService = savedQueryService;
            _savedQueryService.UserId = CurrenUserId;
            _metaDataService = metaDataService;
        }

        [HttpGet]
        public async Task<IEnumerable<object>> GetViewList(string entityLogicalName)
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                var result = _savedQueryService.GetViews(entityLogicalName);
                return result;
            });
        }

        [HttpGet]
        public async Task<Guid> GetDefaultViewId(string entityLogicalName)
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                Guid defaultViewId = _savedQueryService.GetDefaultViewId(entityLogicalName);
                return defaultViewId;
            });
        }

        [HttpGet]
        public async Task<string> GetViewXml(Guid id)
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                var result = _savedQueryService.GetViewXml(id);
                return result;
            });
        }

        [HttpGet]
        public async Task<Dictionary<string, string>> GetFieldsWithDisplayNames([FromUri]FieldsWithDisplayNameModel model)
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                var result = _savedQueryService.GetFieldsDisplayNames(model.EntityLogicalName, model.ViewId);
                return result;
            });
        }

        [HttpGet]
        public async Task<Guid> GetLookupViewId(string entityLogicalName)
        {
            return await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                Guid lookupViewId = _savedQueryService.GetLookupViewId(entityLogicalName);
                return lookupViewId;
            });
        }

        [HttpGet]
        public async Task<Guid?> GetLookupViewIdByLookupGetModel([FromUri]LookupGetModel lookupModel)
        {
            return await Task<Guid?>.Factory.StartNew(() =>
            {
                string entityLogicalName = _metaDataService.GetLookupFieldEntityName(
                    lookupModel.EntityLogicalName, lookupModel.FieldName);

                Guid lookupViewId = _savedQueryService.GetLookupViewId(entityLogicalName);
                return lookupViewId;
            });
        }
    }
}
