﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Portal.DAL.Interfaces;
using Portal.SPA.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Portal.SPA.Controllers
{
    [CustomExceptionFilter]
    public class WebResourseController : BaseApiController
    {
        private IUnitOfWork _uow;

        public WebResourseController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public string GetById(Guid id)
        {
            Entity webResourse = _uow.EntityRepository.Get(id, "webresource", new ColumnSet("content"));
            string encodedContent = webResourse.GetAttributeValue<string>("content");

            byte[] data = Convert.FromBase64String(encodedContent);
            string content = Encoding.UTF8.GetString(data);

            return content;
        }
    }
}
