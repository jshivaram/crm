﻿node-v4.4.7-x64.msi

├── autoprefixer@6.4.0
├── babel-core@6.14.0
├── babel-eslint@6.1.2
├── babel-loader@6.2.5
├── babel-polyfill@6.13.0
├── babel-preset-es2015@6.14.0
├── babel-preset-react@6.11.1
├── babel-preset-react-hmre@1.1.1
├── babel-preset-stage-0@6.5.0
├── babel-root-import@4.1.0
├── bootstrap-loader@1.1.4
├── bootstrap-sass@3.3.7
├── css-loader@0.24.0
├── es6-promise@3.2.1
├── eslint@3.4.0
├── eslint-loader@1.5.0
├── eslint-plugin-react@6.2.0
├── extract-text-webpack-plugin@1.0.1
├── file-loader@0.9.0
├── imports-loader@0.6.5
├── jquery@3.1.0
├── node-sass@3.8.0
├── postcss-import@8.1.2
├── postcss-loader@0.11.0
├── resolve-url-loader@1.6.0
├── sass-loader@4.0.0
├── style-loader@0.13.1
├── url-loader@0.5.7
└── webpack@1.13.2

global dependencies(need create links):

autoprefixer babel-core babel-eslint babel-loader babel-polyfill babel-preset-es2015 babel-preset-react babel-preset-react-hmre babel-preset-stage-0 bootstrap-loader bootstrap-sass css-loader eslint eslint-loader eslint-plugin-react extract-text-webpack-plugin file-loader imports-loader postcss-import postcss-loader resolve-url-loader sass-loader style-loader url-loader webpack node-sass jquery es6-promise babel-root-import

How to install global(npm i [packages] -g):

Example:
npm i autoprefixer babel-core babel-eslint babel-loader babel-polyfill babel-preset-es2015 babel-preset-react babel-preset-react-hmre babel-preset-stage-0 bootstrap-loader bootstrap-sass css-loader eslint eslint-loader eslint-plugin-react extract-text-webpack-plugin file-loader imports-loader postcss-import postcss-loader resolve-url-loader sass-loader style-loader url-loader webpack node-sass jquery es6-promise babel-root-import -g

How to links to global packages(npm link [packages]):

Example:
npm link autoprefixer babel-core babel-eslint babel-loader babel-polyfill babel-preset-es2015 babel-preset-react babel-preset-react-hmre babel-preset-stage-0 bootstrap-loader bootstrap-sass css-loader eslint eslint-loader eslint-plugin-react extract-text-webpack-plugin file-loader imports-loader postcss-import postcss-loader resolve-url-loader sass-loader style-loader url-loader webpack node-sass jquery es6-promise babel-root-import

*******************************************************************************************

local dependencies

jquery react react-dom react-router

How to restore local packages from package.json(npm i)

Example:
npm i

********************************************************************************************
npm i

npm i autoprefixer@6.4.0 babel-core@6.14.0 babel-eslint@6.1.2 babel-loader@6.2.5 babel-polyfill@6.13.0 babel-preset-es2015@6.14.0 babel-preset-react@6.11.1 babel-preset-react-hmre@1.1.1 babel-preset-stage-0@6.5.0 babel-root-import@4.1.0 bootstrap-loader@1.1.4 bootstrap-sass@3.3.7 css-loader@0.24.0 es6-promise@3.2.1 eslint@3.4.0 eslint-loader@1.5.0 eslint-plugin-react@6.2.0 extract-text-webpack-plugin@1.0.1 file-loader@0.9.0 imports-loader@0.6.5 jquery@3.1.0 node-sass@3.8.0 postcss-import@8.1.2 postcss-loader@0.11.0 resolve-url-loader@1.6.0 sass-loader@4.0.0 style-loader@0.13.1 url-loader@0.5.7 webpack@1.13.2 -g

npm link autoprefixer@6.4.0 babel-core@6.14.0 babel-eslint@6.1.2 babel-loader@6.2.5 babel-polyfill@6.13.0 babel-preset-es2015@6.14.0 babel-preset-react@6.11.1 babel-preset-react-hmre@1.1.1 babel-preset-stage-0@6.5.0 babel-root-import@4.1.0 bootstrap-loader@1.1.4 bootstrap-sass@3.3.7 css-loader@0.24.0 es6-promise@3.2.1 eslint@3.4.0 eslint-loader@1.5.0 eslint-plugin-react@6.2.0 extract-text-webpack-plugin@1.0.1 file-loader@0.9.0 imports-loader@0.6.5 jquery@3.1.0 node-sass@3.8.0 postcss-import@8.1.2 postcss-loader@0.11.0 resolve-url-loader@1.6.0 sass-loader@4.0.0 style-loader@0.13.1 url-loader@0.5.7 webpack@1.13.2