
export default {
    none: 0,
    systemRequired: 1,
    applicationRequired: 2,
    recommended: 3
};