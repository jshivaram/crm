export function setUserName(userName) {

  return {
    type: 'SET_UserName',
    user: userName
  }

}