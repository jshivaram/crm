import dictionary from  './optionSetDictionary'

export default function(buttonArray){
    try {
        if(!buttonArray || buttonArray.length < 1){
            return null;
        }
        let resultButtonArray = buttonArray.map(convertSingleButton);

        return resultButtonArray;
        
    } catch (error) {
        console.error(error);    
    }
}

function convertSingleButton(button){
    let attributes = button.Attributes;
    let resultButton = {};

    attributes.forEach(item =>{
        if(item.Key === 'ddsm_buttontype'){
            resultButton[item.Key] = dictionary.getName(item.Value.Value);
        }else{
            resultButton[item.Key] = item.Value;
        }
    });

    return resultButton;
}
