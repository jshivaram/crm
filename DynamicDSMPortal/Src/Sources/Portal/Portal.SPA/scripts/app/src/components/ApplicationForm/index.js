import React, { Component } from 'react'
import Form from '../CRMForm/CRMFormComponents/Form'

import Loader from '~/components/Loader'
import guid from '~/helpers/guid'
const $ = window.$;

export default class ApplicationForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: undefined
        };
    }
    componentWillMount() {
        Loader.show($('#loader'), $('#wrapper'));
    }
    render() {
        $('head link[crm-application]').remove();
        let theme = $('#themes').val();
        if(!theme) {
            theme = 'e1';
        }

        if (theme === 'e1') {
            $('head').append('<link crm-application href="/Content/Applications/application-e1.css" rel="stylesheet" />');
        } else {
            $('head').append('<link crm-application href="/Content/Applications/application.css" rel="stylesheet" />');
        }

        return (
            <div key={guid()} className='unreg-application-form-wrapper row'>
                <Form key={guid()}
                    data={this.state.data} />
            </div>
        );
    }
    componentDidMount() {
        let self = this;

        $.get(`/api/application/GetFormStructureForUnregisterApplication?id=${self.props.params.applicationFormId}`)
            .done((formJson) => {

                let tabs = JSON.parse(formJson).form.tabs.tab;
                let data = {
                    tabs: tabs,
                    entity: {
                        Name: 'ddsm_application'
                    },
                    objectAttributes: {},
                    enityAttributes: {},
                    infoForBtnSave: {
                        method: 'POST',
                        action: '/api/application',
                        isApplicationForUnreg: true
                    }
                };

                self.setState({ data: data });
                changeBodyClasses(self.props.params.ApplicationFormName);
                Loader.hide($('#loader'), $('#wrapper'));
            })
            .fail((err) => {
                console.log(err);
                Loader.hide($('#loader'), $('#wrapper'));
            });
    }
}

function changeBodyClasses(formName) {
    let applicationFormClassName = formName.toLowerCase().split(' ').join('-');
    $(document.body).attr('class', 'ddsm-application-wrapper-body ' + applicationFormClassName);
}