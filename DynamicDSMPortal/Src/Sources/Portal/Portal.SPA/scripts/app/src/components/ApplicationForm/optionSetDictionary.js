function CreateEnum(string,value) {
    this.number = value;
    this.name = string;
}

export default {
    _enum: [
        new CreateEnum('Link', 962080000),
        new CreateEnum('Action', 962080001)
    ],
    getName(numberValue) {
        let enumSelect = this._enum.find(item => item.number === numberValue);
        return enumSelect? enumSelect.name : undefined;
    }
}