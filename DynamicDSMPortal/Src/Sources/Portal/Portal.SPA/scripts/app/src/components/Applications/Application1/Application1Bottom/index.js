import React, { Component } from 'react'

export default class Application1Bottom extends Component {
    render() {
        return (
            <div className='row'>
                <div className='row'>
                    <h2>Eligible appliances</h2>
                    <ul>
                        <li><a href='https://www.fortisbc.com/Rebates/RebatesOffers/ENERGYSTARRetailApplianceProgram/Documents/ClothesWasherQualifyingList.pdf'>clothes washers</a></li>
                        <li><a href='https://www.fortisbc.com/Rebates/RebatesOffers/ENERGYSTARRetailApplianceProgram/Documents/ClothesDryerQualifyingList.pdf'>clothes dryers</a></li>
                        <li><a href='https://www.fortisbc.com/Rebates/RebatesOffers/ENERGYSTARRetailApplianceProgram/Documents/RefrigeratorQualifyingList.pdf'>refrigerators</a></li>
                    </ul>
                    <p><b>Please note</b>: for an appliance to be eligible, the full model number must appear in the directory.
                    </p>
                </div>
                <div className='row'>
                    <h2>Questions?</h2>
                    <p>
                        If you’re a FortisBC electricity customer or a municipal electricity customer of Grand Forks, Summerland, Penticton
                        or Nelson Hydro, call 1-866-436-7847 or email
                        <a href='mailto:electricrebates@fortisbc.com'>electricrebates@fortisbc.com</a>.
                    </p>
                </div>
                <div className='row'>
                </div>
            </div>
        );
    }
}