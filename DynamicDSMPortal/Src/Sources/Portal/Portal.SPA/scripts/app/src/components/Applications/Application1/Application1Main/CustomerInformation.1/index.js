import React, { Component } from 'react'

export default class CustomerInformation extends Component {
    render() {
        return (
            <div className='col-md-5'>
                <h3>Customer Information</h3>
                <div className='row'>
                    <div className='col-md-4'>Contact</div>
                    <div className='col-md-8'><input type='text' className='k-textbox' readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Website:</div>
                    <div className='col-md-8'><input type='text' className='k-textbox' readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Street 1:</div>
                    <div className='col-md-8'><input type='text' className='k-textbox' readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Street 2:</div>
                    <div className='col-md-8'><input type='text' className='k-textbox' readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>City:</div>
                    <div className='col-md-8'><input type='text' className='k-textbox' readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>State:</div>
                    <div className='col-md-8'><input type='text' className='k-textbox' readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>ZIP/Postal Code:</div>
                    <div className='col-md-8'><input type='text' className='k-textbox' readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Email:</div>
                    <div className='col-md-8'><input type='text' className='k-textbox' readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Main Phone:</div>
                    <div className='col-md-8'><input type='text' className='k-textbox' readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Other Phone:</div>
                    <div className='col-md-8'><input type='text' className='k-textbox' readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Fax:</div>
                    <div className='col-md-8'><input type='text' className='k-textbox' readOnly /></div>
                </div>
            </div>
        );
    }
}