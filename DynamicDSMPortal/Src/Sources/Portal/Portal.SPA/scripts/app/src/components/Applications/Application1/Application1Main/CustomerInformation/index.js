import React, { Component } from 'react'
const $ = window.$;

export default class CustomerInformation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: undefined
        };
    }
    render() {
        if(!this.state.data) {
            return (<span></span>);
        }

        let props = {};

        this.state.data.Attributes.map(a => { 
            let buf =  {};
            buf[a.Key] = a.Value;
            props[a.Key] = a.Value;
            return buf;
        });

        let fullName = props['fullname'] ? props['fullname'] : '';
        let emailAddress1 = props['emailaddress1'] ? props['emailaddress1'] : '';
        let webSite = props['websiteurl'] ?  props['websiteurl'] : '';
        let street1 = props['address1_line1'] ? props['address1_line1'] : '';
        let street2 = props['address1_line2'] ? props['address1_line2'] : '';
        let city = props['address1_city'] ? props['address1_city'] : '';
        let state = props['address1_stateorprovince'] ? props['address1_stateorprovince'] : '';
        let postalCode = props['address1_postalcode'] ? props['address1_postalcode'] : '';
        let mainPhone = props['telephone1'] ?  props['telephone1'] : '';
        let otherPhone = props['telephone2'] ?  props['telephone2'] : '';
        let fax = props['fax'] ?  props['fax'] : '';

        return (
            <div className='col-md-5'>
                <h3>Customer Information</h3>
                <div className='row'>
                    <div className='col-md-4'>Contact</div>
                    <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={fullName} readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Website: </div>
                    <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={webSite} readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Street 1: </div>
                    <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={street1} readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Street 2: </div>
                    <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={street2} readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>City: </div>
                    <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={city} readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>State: </div>
                    <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={state} readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>ZIP/Postal Code: </div>
                    <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={postalCode} readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Email: </div>
                    <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={emailAddress1} readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Main Phone: </div>
                    <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={mainPhone} readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Other Phone: </div>
                    <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={otherPhone} readOnly /></div>
                </div>
                <div className='row'>
                    <div className='col-md-4'>Fax: </div>
                    <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={fax} readOnly /></div>
                </div>
            </div>
        );
    }

    componentDidMount() {
        let self = this;
        $.get('/api/Application/GetUserInfo', function (data) {
            self.setState({data: data});
        });
    }

}