import React, { Component } from 'react'
import guid from '~/helpers/guid'
const $ = window.$;

export default class AccountLookup extends Component {
    render() {
        this.accountLookupId = guid();

        return (
            <select type='text' id={this.accountLookupId}></select>
        );
    }

    componentDidMount() {
        let url = '/api/entity/GetEntitiesForLookup?entityLogicalName=account';
        let self = this;

        $('#' + self.accountLookupId).kendoDropDownList({
            dataTextField: 'Text',
            dataValueField: 'Value',
            dataSource: {
                transport: {
                    read: {
                        dataType: 'json',
                        url: url,
                        type: 'GET'
                    }
                }
            },
            dataBound: function () {
                var lookup = this;
                if (self.props.selectedAccountId) {
                    lookup.value(self.props.selectedAccountId);
                }
            },
            optionLabel: 'Select CA Company'
        });

        let parent = this.props.parent;

        document.getElementById(this.accountLookupId).onchange = function (e) {
            let target = e.target;

            if (!target.value) {
                return;
            }

            let value = target.options[target.selectedIndex].value;

            $.get('/api/entity/Get?entityLogicalName=account&id=' + value, function (data) {
                parent.setState({ 'data': data, 'selectedAccountId': value });
            });
        }
    }
}