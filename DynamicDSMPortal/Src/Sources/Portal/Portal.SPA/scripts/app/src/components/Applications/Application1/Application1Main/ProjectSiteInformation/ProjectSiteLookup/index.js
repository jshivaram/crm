import React, { Component } from 'react'
import guid from '~/helpers/guid'
const $ = window.$;

export default class ProjectSiteLookup extends Component {
    render() {
        this.lookupId = guid();

        return (
            <select type='text' id={this.lookupId}></select>
        );
    }

    componentDidMount() {
        let url = '/api/entity/GetEntitiesForLookup?entityLogicalName=ddsm_site';
        let self = this;

        $('#' + self.lookupId).kendoDropDownList({
            dataTextField: 'Text',
            dataValueField: 'Value',
            dataSource: {
                transport: {
                    read: {
                        dataType: 'json',
                        url: url,
                        type: 'GET'
                    }
                }
            },
            dataBound: function () {
                var lookup = this;
                if (self.props.selectedAccountId) {
                    lookup.value(self.props.selectedAccountId);
                }
            },
            optionLabel: '--'
        });

        let parent = this.props.parent;

        document.getElementById(this.lookupId).onchange = function (e) {
            let target = e.target;

            if (!target.value) {
                return;
            }

            let value = target.options[target.selectedIndex].value;

            $.get('/api/entity/Get?entityLogicalName=ddsm_site&id=' + value, function (data) {
                parent.setState({ 'data': data, 'selectedAccountId': value });
            });
        }
    }
}