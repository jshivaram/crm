import React, { Component } from 'react'
import guid from '~/helpers/guid'
import ProjectSiteLookup from './ProjectSiteLookup'

export default class ProjectSiteInformation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: undefined,
            selectedProjectId: undefined
        };
    }
    render() {

        let props = {};

        if (this.state.data) {
            this.state.data.Attributes.forEach(a => {
                props[a.Key] = a.Value;
            });

            console.log('props');
            console.log(props);
        }

        let primaryContact = props[''] ? props[''] : '';
        let account = '';
        let buildingType = '';
        let sqft = '';

        let address1 = '';
        let address2 = '';
        let city = '';
        let state = '';
        let zip = '';

        return (
            <div className='row' key={guid() }>
                <h1>Project site information: </h1>
                <div className='col-md-5'>
                    <div className='col-md-4'>Project Site</div>
                    <div className='col-md-8'>
                        <ProjectSiteLookup selectedProjectId={this.state.selectedProjectId} parent={this} key={guid() } />
                    </div>
                    <div className='col-md-4'>Primary Contact</div>
                    <div className='col-md-8'>
                        <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={primaryContact} readOnly /></div>
                    </div>
                    <div className='col-md-4'>Account</div>
                    <div className='col-md-8'>
                        <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={account} readOnly /></div>
                    </div>
                    <div className='col-md-4'>Building Type</div>
                    <div className='col-md-8'>
                        <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={buildingType} readOnly /></div>
                    </div>
                    <div className='col-md-4'>SqFt</div>
                    <div className='col-md-8'>
                        <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={sqft} readOnly /></div>
                    </div>
                </div>
                <div className='col-md-5 col-md-offset-1'>
                    <div className='col-md-4'>Address 1</div>
                    <div className='col-md-8'>
                        <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={address1} readOnly /></div>
                    </div>
                    <div className='col-md-4'>Address 2</div>
                    <div className='col-md-8'>
                        <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={address2} readOnly /></div>
                    </div>
                    <div className='col-md-4'>City</div>
                    <div className='col-md-8'>
                        <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={city} readOnly /></div>
                    </div>
                    <div className='col-md-4'>State</div>
                    <div className='col-md-8'>
                        <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={state} readOnly /></div>
                    </div>
                    <div className='col-md-4'>ZAP Code</div>
                    <div className='col-md-8'>
                        <div className='col-md-8'><input type='text' className='k-textbox' defaultValue={zip} readOnly /></div>
                    </div>
                </div>
            </div>
        );
    }
}