import React, { Component } from 'react'
import CustomerInformation from './CustomerInformation'
import InstallerInformation from './InstallerInformation'
import ProjectSiteInformation from './ProjectSiteInformation'

export default class Application1Main extends Component {
    render() {
        return (
            <div>
                <div className='row well'>
                    <CustomerInformation />
                    <InstallerInformation />
                </div>
                <div className='row well'>
                    <ProjectSiteInformation />
                </div>
            </div>
        );
    }
}