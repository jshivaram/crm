import React, { Component } from 'react'

export default class Application1Top extends Component {
    render() {
        return (
            <div className='row'>
                <h1 className='row'>Business Energy Rebates</h1>
                <div className='row'>
                    <h2>Application</h2>
                    <p>Shopping for a new clothes washer, dryer or refrigerator? Melissa, our residential program manager, works with retailers
                        to bring you the ENERGY STAR® Appliance Rebate. With rebates of $50 to $100 on qualifying ENERGY STAR® appliances,
                        it’s easier to upgrade to high efficiency and save on your energy costs year after year.</p>
                </div>
                <div className='row'>
                    <h2>Eligibility requirements</h2>
                    <ul>
                        <li><a>If you’re a FortisBC residential electricity customer OR</a></li>
                        <li><a>If you're a municipal electricity customer of Grand Forks, Summerland, Penticton or Nelson Hydro</a></li>
                    </ul>
                    <p>
                        <b>Please note</b>: If you’re a BC Hydro or City of New Westminster residential electricity customer, visit
                        <a href='http://bchydro.com/appliances'>bchydro.com/appliances</a>.
                    </p>
                </div>
                <div className='row'>
                    <h2>How to apply</h2>
                    <ul>
                        <li><a>If you’re a FortisBC residential electricity customer OR</a></li>
                        <li><a>If you're a municipal electricity customer of Grand Forks, Summerland, Penticton or Nelson Hydro</a></li>
                    </ul>
                    <p>
                        <b>Please note</b>: If you’re a BC Hydro or City of New Westminster residential electricity customer, visit
                        <a href='http://bchydro.com/appliances'>bchydro.com/appliances</a>.
                    </p>
                </div>
            </div>
        );
    }
}