import React, { Component } from 'react'
import Application1Top from './Application1Top'
import Application1Bottom from './Application1Bottom'
import Application1Main from './Application1Main'
import guid from '~/helpers/guid'

export default class Application1 extends Component {
    render() {
        return (
            <form className='row' key={guid()}>
                <Application1Top key={guid()}/>

                <Application1Main key={guid()} />

                <Application1Bottom key={guid()} />
            </form>
        );
    }
}