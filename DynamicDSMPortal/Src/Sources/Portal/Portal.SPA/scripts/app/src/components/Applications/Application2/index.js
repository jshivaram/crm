import React, { Component } from 'react'
import ProjectTemplateLookup from '../ProjectTemplateLookup';
import guid from '~/helpers/guid'

export default class Application2 extends Component {
    render() {
        return (
            <div>
                <h1>Application #2</h1>
                <ProjectTemplateLookup key={guid()}/>
            </div>
        );
    }
}