import React, { Component } from 'react'
import CRMFormControls from '~/components/CRMForm/CRMFormControls'
import guid from '~/helpers/guid'
//const $ = window.$;

export default class MeasureTemplate extends Component {
    render() {
        let measureTemplateId = guid();

        let entity = {
            Id: this.props.projectTemplateId,
            Name: 'ddsm_projecttemplate'
        };
        
        let control = {
            parameters: {
                TargetEntityType: 'ddsm_measuretemplate',
                RelationshipName: 'ddsm_ddsm_projecttemplate_ddsm_measuretemplate',
                ViewId: 'B967525E-BD80-E611-80D4-3A3036663635'
            }
        };

        return (
            <div key={measureTemplateId}>
                <CRMFormControls.GridControl
                    withoutLabel={true}
                    key={guid()}
                    entity={entity}
                    control={control} />
            </div>
        );
    }
    componentDidMount() {

    }
}