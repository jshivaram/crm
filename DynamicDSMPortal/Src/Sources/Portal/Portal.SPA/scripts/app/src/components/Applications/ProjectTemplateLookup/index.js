import React, { Component } from 'react'
import guid from '~/helpers/guid'
const $ = window.$;
import MeasureTemplate from '../MeasureTemplate';

export default class ProjectTemplateLookup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projectTemplateId: undefined
        };
    }
    render() {
        let projectTemplateLookupId = guid();
        this.lookupId = projectTemplateLookupId;

        let createMeasureTemplateGrid = () => {
            if (this.state.projectTemplateId) {
                return (
                    <MeasureTemplate projectTemplateId={this.state.projectTemplateId} />
                );
            } else {
                return (
                    <span></span>
                )
            }
        };

        return (
            <div>
                <select id={projectTemplateLookupId}>
                </select>
                {createMeasureTemplateGrid() }
            </div>
        );
    }
    componentDidMount() {

        let self = this;
        let url = '/api/entity/GetEntitiesForLookup?viewId=E1EDC6DC-A85A-49C3-97B9-A27F866A4806';

        $('#' + this.lookupId).kendoDropDownList({
            filter: 'contains',
            ignoreCase: true,
            dataTextField: 'Text',
            dataValueField: 'Value',
            dataSource: {
                transport: {
                    read: {
                        dataType: 'json',
                        url: url,
                        type: 'GET'
                    }
                }
            },
            change: function (e) {
                let projectTemplateId = e.sender._old;
                if (projectTemplateId) {
                    self.setState({ projectTemplateId: projectTemplateId });
                }
            },
            dataBound: function (e) {
                let projectTemplateId = e.sender._old;
                if (projectTemplateId) {
                    self.setState({ projectTemplateId: projectTemplateId });
                }
            }
        });
    }
}