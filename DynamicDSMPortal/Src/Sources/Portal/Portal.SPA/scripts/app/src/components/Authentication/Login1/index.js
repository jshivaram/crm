import React, { Component } from 'react'
import '../style.scss'
import { browserHistory } from 'react-router'
import Loader from '~/components/Loader'
import { setUserName } from '~/actions/authActions.js'

const $ = window.$;

export default class Login1 extends Component {
    render() {
        return (
            <form id='portal-login-form' className='login-form-style-1' onKeyDown={this.keyDownHandler(this)}>
                <div className='login-form-container'>
                    <div className='header-box'>
                         <div className='header-form-1'>
                              <h2>Log in</h2>
                              <p>Sign in to your account:</p>
                         </div>
                         <div className='header-form-logo'>
                             <span className='k-font-icon k-i-lock'></span>
                         </div>
                    </div>
                    <div className='row custom-row'>
                        <div id='auth-err-msg' className='alert alert-danger'></div>
                    </div>
                    <div className='row custom-row'>
                         <div className='input-field-2'>
                             <span  className='user-name-icon'></span><input id='crm-portal-login' className='k-textbox' type='text' name='Login' placeholder='User name' required />
                                 <label htmlFor='crm-portal-login' className=''>Username</label>
                         </div>
                         <div className='input-field-2'>
                             <span  className='user-password-icon'></span><input className='k-textbox crm-portal-form-password' id='crm-portal-form-password' type='password' name='Password' placeholder='Password' required />
                                 <label htmlFor='crm-portal-form-password' className=''>Password</label>  
                         </div>
                    </div>
                     <div className='row custom-row'>
                         <div className='col-md-8'>
                                <div className='switch'>
                                        <input id='cmn-toggle-1' className='cmn-toggle cmn-toggle-round' type='checkbox'/>
                                        <label htmlFor='cmn-toggle-1'></label> <span>Remember me</span>
                                 </div> 
                         </div>
                         <div className='col-md-4'>
                              <a className='forgot_details' href='#'>forgot details?</a>
                         </div>
                    </div>
                    <div className='row custom-row'>
                                  <input onClick={this.btnLoginHandler(this)} className='k-button' type='button' value='Login' />
                    </div>
                    <div className='row custom-row'>
                        <p>Don't have an account? <a href='#'>Sign up</a> now.</p>
                    </div>
                </div>
            </form>
        );
    }

    keyDownHandler(self) {
        return function(e) {
            if (e.keyCode !== 13) {
                return;
            }

            self.Login(self);
        }
    }

    btnLoginHandler(self) {
        return function() {
            self.Login(self);
        }
    }

    Login(self) {
        let formData = $('#portal-login-form').serialize();

        Loader.show($('#loader'), $('#wrapper'));

        $.ajax({
            url: '/api/auth/login',
            method: 'POST',
            data: formData,
            success: function () {
                Loader.hide($('#loader'), $('#wrapper'));

                let login = $('#crm-portal-login').val();
                self.props.dispatch(setUserName(login));

                browserHistory.push('/');
            },
            error: function(err) {
                Loader.hide($('#loader'), $('#wrapper'));

                let response;
                try {
                    response = JSON.parse(err.statusText);

                    let errMsg = '';
                    response.forEach(function(el){
                        errMsg += el[Object.keys(el)[0]];
                        errMsg += '</br>'
                    });

                    $('#auth-err-msg').css('display', 'block');
                    $('#auth-err-msg').html(errMsg);
                }
                catch(ex) {
                    console.log(ex);

                    $('#auth-err-msg').css('display', 'block');
                    $('#auth-err-msg').text(err.statusText);
                }
            }
        });
    }
}