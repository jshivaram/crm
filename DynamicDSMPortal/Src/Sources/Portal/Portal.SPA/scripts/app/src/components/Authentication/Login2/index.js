import React, { Component } from 'react'
import '../style.scss'
import { browserHistory } from 'react-router'
import Loader from '~/components/Loader'
import { setUserName } from '~/actions/authActions.js'

const $ = window.$;

export default class Login2 extends Component {
    render() {
        return (
            <form id='portal-login-form' className='login-form-style-2' onKeyDown={this.keyDownHandler(this)}>
                <div className=' login-bg-form'>
                    <h3 className='custom-h2'>Access your Account Online profile and account information.</h3>

                    <div className='row custom-row form-row'>
                        <div id='auth-err-msg' className='alert alert-danger'></div>
                    </div>

                    <div className='row custom-row form-row'>
                        <div className='col-md-4'>
                            <label>*Login</label>
                        </div>
                        <div className='col-md-8'>
                            <input id='crm-portal-login' className='k-textbox' type='text' name='Login' />
                        </div>
                    </div>
                    <div className='row custom-row form-row'>
                        <div className='col-md-4'>
                            <label>*Password</label>
                        </div>
                        <div className='col-md-8'>
                                   <input className='k-textbox' type='password' name='Password' />
                        </div>
                    </div>

                    <div className='row custom-row form-row'>
                        <div className='col-md-6'>
                            <div className='form-remember-me'>
                                <input type='checkbox' className='filled-in' id='filled-in-box-example' value='1'/>
                                <label htmlFor='filled-in-box-example'>Remember me</label>                      
                            </div>
                        </div>
                        <div className='col-md-6'>
                            <div className='form-forgot-password'>
                               <a href='#' id='form-forget-password'>Forgot Password?</a>
                            </div>
                        </div>
                     </div>
                     <div className='row custom-row form-row'>
                          <input onClick={this.btnLoginHandler(this)} className='k-button' type='button' value='Login' />
                     </div>
                     <div className='row custom-row form-row login-footer'>
                         <div className='form-footer-copirate'><p><img src='/Content/Themes/Default/glyph_footer.png' align='left'/> Your work or school account can be used anywhere you see this symbol. &copy; 2015  efficiency</p></div>
                     </div>
                </div>
            </form>
        );
    }

    keyDownHandler(self) {
        return function(e) {
            if (e.keyCode !== 13) {
                return;
            }

            self.Login(self);
        }
    }

    btnLoginHandler(self) {
        return function() {
            self.Login(self);
        }
    }

    Login(self) {
        let formData = $('#portal-login-form').serialize();

        Loader.show($('#loader'), $('#wrapper'));

        $.ajax({
            url: '/api/auth/login',
            method: 'POST',
            data: formData,
            success: function () {
                Loader.hide($('#loader'), $('#wrapper'));

                let login = $('#crm-portal-login').val();
                self.props.dispatch(setUserName(login));

                browserHistory.push('/');
            },
            error: function(err) {
                Loader.hide($('#loader'), $('#wrapper'));

                let response;
                try {
                    response = JSON.parse(err.statusText);

                    let errMsg = '';
                    response.forEach(function(el){
                        errMsg += el[Object.keys(el)[0]];
                        errMsg += '</br>'
                    });

                    $('#auth-err-msg').css('display', 'block');
                    $('#auth-err-msg').html(errMsg);
                }
                catch(ex) {
                    console.log(ex);

                    $('#auth-err-msg').css('display', 'block');
                    $('#auth-err-msg').text(err.statusText);
                }
            }
        });
    }
}