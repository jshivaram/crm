import React, { Component } from 'react'
import '../style.scss'
import { browserHistory } from 'react-router'
import Loader from '~/components/Loader'
import { setUserName } from '~/actions/authActions.js'

const $ = window.$;

export default class Login3 extends Component {
    render() {
        return (
            <form id='portal-login-form' className='login-form-style-3' onKeyDown={this.keyDownHandler(this)}>
            <div className='login-form-window'>
                <div className='login-form-window-top'>
                    <img src='/Content/Themes/Default/login.png'/>
                </div>
                <h3 className='custom-h2'>Access your Account Online profile and account information.</h3>
                <div className='login-form-window-body'>
                     <div className='row custom-row'>
                        <div id='auth-err-msg' className='alert alert-danger'></div>
                    </div>
                    <div className='row custom-row'>
                        <div className='input-field col-md-12'>
                             <input id='crm-portal-login' className='k-textbox' type='text' name='Login' required/>
                             <label htmlFor='crm-portal-login' className=''>Username</label>
                             <input id='crm-portal-form-password' className='k-textbox' type='password' name='Password' required />
                             <label htmlFor='crm-portal-form-password' className=''>Password</label>  
                        </div>
                    </div>
                     <div className='row custom-row'>
                        <input onClick={this.btnLoginHandler(this)} className='k-button' type='button' value='Login' />
                    </div>
                    <div className='row custom-row'>
                        <p>Don't have an account? <a href='#'>Sign up</a> now.</p>
                    </div>
                </div>
            </div>
            </form>
        );
    }

    keyDownHandler(self) {
        return function(e) {
            if (e.keyCode !== 13) {
                return;
            }

            self.Login(self);
        }
    }

    btnLoginHandler(self) {
        return function() {
            self.Login(self);
        }
    }

    Login(self) {
        let formData = $('#portal-login-form').serialize();

        Loader.show($('#loader'), $('#wrapper'));

        $.ajax({
            url: '/api/auth/login',
            method: 'POST',
            data: formData,
            success: function () {
                Loader.hide($('#loader'), $('#wrapper'));

                let login = $('#crm-portal-login').val();
                self.props.dispatch(setUserName(login));

                browserHistory.push('/');
            },
            error: function(err) {
                Loader.hide($('#loader'), $('#wrapper'));

                let response;
                try {
                    response = JSON.parse(err.statusText);

                    let errMsg = '';
                    response.forEach(function(el){
                        errMsg += el[Object.keys(el)[0]];
                        errMsg += '</br>'
                    });

                    $('#auth-err-msg').css('display', 'block');
                    $('#auth-err-msg').html(errMsg);
                }
                catch(ex) {
                    console.log(ex);

                    $('#auth-err-msg').css('display', 'block');
                    $('#auth-err-msg').text(err.statusText);
                }
            }
        });
    }
}