import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import Loader from '~/components/Loader'

const $ = window.$;

export default class Registration1 extends Component {
    render() {
        return (
            <form id='portal-registration-form' className='row registration-form-style-1' onKeyDown={this.keyDownHandler(this)}>
                <div className='login-form-container'>
                    
                     <div className='header-box'>
                         <div className='header-form-1'>
                              <h2>Sign in</h2>
                              <p>Register for Account Online:</p>
                         </div>
                         <div className='header-form-logo'>
                             <span className='k-font-icon k-i-pencil'></span>
                         </div>
                    </div>
                    <div className='row custom-row'>
                        <div id='auth-err-msg' className='alert alert-danger'></div>
                    </div>
                    <div className='row custom-row'>
                        <div className='input-field-2'>
                             <span  className='user-name-icon-1'></span><input className='k-textbox form-imput-1' type='text' name='FirstName' id='crm-portal-first-name' placeholder='First name' required />
                             <label htmlFor='crm-portal-first-name' className=''>First Name</label>
                        </div>
                        <div className='input-field-2'>
                             <span  className='user-name-icon-2'></span><input className='k-textbox  form-imput-2' type='text' name='LastName' id='crm-portal-last-name' placeholder='Last name' required />
                             <label htmlFor='crm-portal-last-name' className=''>Last name</label>
                        </div>
                        <div className='input-field-2'>
                             <span  className='user-email-icon'></span><input className='k-textbox  form-imput-3' type='text' name='Email' id='crm-portal-email' placeholder='Your email' required />
                             <label htmlFor='crm-portal-email' className=''>Your email</label>
                        </div>
                        <div className='input-field-2'>
                             <span  className='user-name-icon-3'></span><input className='k-textbox  form-imput-4' type='text' name='Login' id='crm-portal-new-login' placeholder='Choose username' required />     
                             <label htmlFor='crm-portal-new-login' className=''>Choose username</label>
                        </div>
                        <div className='input-field-2'>
                             <span  className='user-password-icon-1'></span><input className='k-textbox  form-imput-5' type='password' name='Password' id='crm-portal-new-password' placeholder='Create password' required />
                             <label htmlFor='crm-portal-new-password' className=''>Create password</label>
                        </div>
                        <div className='input-field-2'>
                             <span  className='user-password-icon-2'></span><input className='k-textbox form-imput-6' type='password' name='ConfirmPasword' id='crm-portal-new-confirm-password' placeholder='Repeat password' required /> 
                             <label htmlFor='crm-portal-new-confirm-password' className=''>Repeat password</label>
                        </div>
                    </div>
                    <div className='row custom-row'>
                        <input onClick={this.clickHandler(this)} className='k-button' type='button' value='Registration' />
                    </div>
                </div>
            </form>
        );
    }
    keyDownHandler(self) {
        return function(e) {
            if (e.keyCode !== 13) {
                return;
            }

            self.Register();
        }
    }
    clickHandler(self) {
        return function() {
            self.Register();
        }
    }
    Register() {
        let formData = $('#portal-registration-form').serialize();

        Loader.show($('#loader'), $('#wrapper'));

        $.ajax({
            url: '/api/auth/register',
            method: 'POST',
            data: formData,
            success: function () {
                Loader.hide($('#loader'), $('#wrapper'));

                let registration = $('#crm-portal-registration-list').data('kendoDropDownList');
                registration.value('');

                browserHistory.push('/');

                let notification = $('#notification').data('kendoNotification');
                notification.show({
                    message: 'Success',
                }, 'upload-success');
            },
            error: function(err) {
                Loader.hide($('#loader'), $('#wrapper'));

                console.log(err);
                let response;
                try {
                    response = JSON.parse(err.statusText);

                    let errMsg = '';
                    response.forEach(function(el){
                        errMsg += el[Object.keys(el)[0]];
                        errMsg += '</br>'
                    });

                    $('#auth-err-msg').css('display', 'block');
                    $('#auth-err-msg').html(errMsg);
                }
                catch(ex) {
                    console.log(ex);

                    $('#auth-err-msg').css('display', 'block');
                    $('#auth-err-msg').text(err.statusText);
                }
            }
        });
    }
}