import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import Loader from '~/components/Loader'

const $ = window.$;

export default class Registration2 extends Component {
    render() {
        return (
            <form id='portal-registration-form' className='row login-form-style-2' onKeyDown={this.keyDownHandler(this)}>
                <div className='login-bg-form'>
                    <h2 className='custom-h2'>Register for Account Online</h2>

                    <div className='row custom-row form-row'>
                        <div id='auth-err-msg' className='alert alert-danger'></div>
                    </div>

                    <div className='row custom-row form-row'>
                        <div className='col-md-4'>
                            <label>*First Name</label>
                        </div>
                        <div className='col-md-8'>
                            <input className='k-textbox' type='text' name='FirstName' />
                        </div>
                    </div>
                    <div className='row custom-row form-row'>
                        <div className='col-md-4'>
                            <label>*Last Name</label>
                        </div>
                        <div className='col-md-8'>
                            <input className='k-textbox' type='text' name='LastName' />
                        </div>
                    </div>
                    <div className='row custom-row form-row'>
                        <div className='col-md-4'>
                            <label>*Email</label>
                        </div>
                        <div className='col-md-8'>
                            <input className='k-textbox' type='email' name='Email' />
                        </div>
                    </div>
                    <div className='row custom-row form-row'>
                        <div className='col-md-4'>
                            <label>*Login</label>
                        </div>
                        <div className='col-md-8'>
                            <input id='crm-portal-login' className='k-textbox' type='text' name='Login' />
                        </div>
                    </div>
                    <div className='row custom-row form-row'>
                        <div className='col-md-4'>
                            <label>*Password</label>
                        </div>
                        <div className='col-md-8'>
                            <input className='k-textbox' type='password' name='Password' />
                        </div>
                    </div>
                    <div className='row custom-row form-row'>
                        <div className='col-md-4'>
                            <label>*Confirm password</label>
                        </div>
                        <div className='col-md-8'>
                            <input className='k-textbox' type='password' name='ConfirmPasword' />
                        </div>
                    </div>

                    <div className='row custom-row button-form-row'>
                        <input onClick={this.clickHandler(this)} className='k-button' type='button' value='Registration' />
                    </div>
                    <div className='row custom-row form-row login-footer'>
                         <div className='form-footer-copirate'><p><img src='/Content/Themes/Default/glyph_footer.png' align='left'/> Your work or school account can be used anywhere you see this symbol. &copy; 2015  efficiency</p></div>
                     </div>
                </div>
            </form>
        );
    }
    keyDownHandler(self) {
        return function(e) {
            if (e.keyCode !== 13) {
                return;
            }

            self.Register();
        }
    }
    clickHandler(self) {
        return function() {
            self.Register();
        }
    }
    Register() {
        let formData = $('#portal-registration-form').serialize();

        Loader.show($('#loader'), $('#wrapper'));

        $.ajax({
            url: '/api/auth/register',
            method: 'POST',
            data: formData,
            success: function () {
                Loader.hide($('#loader'), $('#wrapper'));

                let registration = $('#crm-portal-registration-list').data('kendoDropDownList');
                registration.value('');

                browserHistory.push('/');

                let notification = $('#notification').data('kendoNotification');
                notification.show({
                    message: 'Success',
                }, 'upload-success');
            },
            error: function(err) {
                Loader.hide($('#loader'), $('#wrapper'));

                console.log(err);
                let response;
                try {
                    response = JSON.parse(err.statusText);

                    let errMsg = '';
                    response.forEach(function(el){
                        errMsg += el[Object.keys(el)[0]];
                        errMsg += '</br>'
                    });

                    $('#auth-err-msg').css('display', 'block');
                    $('#auth-err-msg').html(errMsg);
                }
                catch(ex) {
                    console.log(ex);

                    $('#auth-err-msg').css('display', 'block');
                    $('#auth-err-msg').text(err.statusText);
                }
            }
        });
    }
}