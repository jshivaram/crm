import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import Loader from '~/components/Loader'

const $ = window.$;

export default class Registration3 extends Component {
    render() {
        return (
            <form id='portal-registration-form' className='row registration-form-style-3' onKeyDown={this.keyDownHandler(this)}>
               <div className='login-form-window'>
                <div className='login-form-window-top'>
                    <img src='/Content/Themes/Default/registration.png'/>
                </div>
                <h3 className='custom-h2'>Register for Account Online</h3>
                <div className='login-form-window-body'>
                     <div className='row custom-row'>
                        <div id='auth-err-msg' className='alert alert-danger'></div>
                    </div>
                    <div className='row custom-row'>
                        <div className='input-field col-md-12'>
                        <input className='k-textbox' type='text' id='crm-portal-first-name' name='FirstName'  required />
                        <label htmlFor='crm-portal-first-name' className=''>First name</label>
                        <input className='k-textbox' type='text' id='crm-portal-last-name' name='LastName'  required />
                        <label htmlFor='crm-portal-last-name' className=''>Last name</label>
                        <input className='k-textbox' type='email' id='crm-portal-email' name='Email'  required />
                        <label htmlFor='crm-portal-email' className=''>Your email</label>
                        <input className='k-textbox' type='text' id='crm-portal-new-login' name='Login'  required />
                        <label htmlFor='crm-portal-new-login' className=''>Choose username</label>
                        <input className='k-textbox' type='password' id='crm-portal-new-password' name='Password'  required />
                        <label htmlFor='crm-portal-new-password' className=''>Create password</label>
                        <input className='k-textbox' type='password'  id='crm-portal-new-confirm-password' name='ConfirmPasword'  required /> 
                        <label htmlFor='crm-portal-new-confirm-password' className=''>Repeat password</label>
                        </div>
                    </div>
                     <div className='row custom-row'>
                         <input onClick={this.clickHandler(this)} className='k-button' type='button' value='Registration' />
                    </div>
                </div>
            </div>
            </form>
        );
    }
    keyDownHandler(self) {
        return function(e) {
            if (e.keyCode !== 13) {
                return;
            }

            self.Register();
        }
    }
    clickHandler(self) {
        return function() {
            self.Register();
        }
    }
    Register() {
        let formData = $('#portal-registration-form').serialize();

        Loader.show($('#loader'), $('#wrapper'));

        $.ajax({
            url: '/api/auth/register',
            method: 'POST',
            data: formData,
            success: function () {
                Loader.hide($('#loader'), $('#wrapper'));

                let registration = $('#crm-portal-registration-list').data('kendoDropDownList');
                registration.value('');

                browserHistory.push('/');

                let notification = $('#notification').data('kendoNotification');
                notification.show({
                    message: 'Success',
                }, 'upload-success');
            },
            error: function(err) {
                Loader.hide($('#loader'), $('#wrapper'));

                console.log(err);
                let response;
                try {
                    response = JSON.parse(err.statusText);

                    let errMsg = '';
                    response.forEach(function(el){
                        errMsg += el[Object.keys(el)[0]];
                        errMsg += '</br>'
                    });

                    $('#auth-err-msg').css('display', 'block');
                    $('#auth-err-msg').html(errMsg);
                }
                catch(ex) {
                    console.log(ex);

                    $('#auth-err-msg').css('display', 'block');
                    $('#auth-err-msg').text(err.statusText);
                }
            }
        });
    }
}