const $ = window.$;

export default function (entityName, fieldName, componentSelector) {
    if (entityName !== 'ddsm_application' && entityName !== 'ddsm_project') {
        return;
    }

    $.get(`/api/application/GetApplicationFieldDescription?fieldName=${fieldName}&entityName=${entityName}`,
        function (item) {
            
            if (!item) {
                return;
            }
            $(componentSelector)
                .parent()
                .parent()
                .find('.show-tooltip')
                .removeClass('hidden-tooltip')
                .kendoTooltip({
                    showOn: 'click',
                    //width: 500,
                    content: function () {

                        return item;
                    },
                    animation: {
                        open: {
                            effects: 'fade:in',
                            duration: 300
                        },
                        close: {
                            effects: 'fade:out',
                            duration: 300
                        }
                    }
                });
        });
}