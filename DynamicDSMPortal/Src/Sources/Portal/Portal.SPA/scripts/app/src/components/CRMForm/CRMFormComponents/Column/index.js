import React, { Component } from 'react'
import CRMFormComponents from '../../CRMFormComponents'
import guid from '~/helpers/guid'

export default class Column extends Component {
    componentDidMount() {
        if(this.refs.columnRef) {
            this.refs.columnRef.setAttribute('style', 'width: ' + this.props.column['@width'] + ';');
        }        
    }
    render() {
        if(!this.props || !this.props.column 
            || !this.props.column.sections
            || !this.props.column.sections.section
            || !this.props.entity || !this.props.data) {
            return (<div className='empty-section'></div>);
        }

        if (!Array.isArray(this.props.column.sections.section)) {
            return (
                <div ref='columnRef' className='portal-column col-md-2'>
                    <CRMFormComponents.Section
                        entity={this.props.entity}
                        section={this.props.column.sections.section} 
                        data={this.props.data}
                        objectAttributes={this.props.objectAttributes}
                        dashboard={this.props.dashboard}
                        >
                    </CRMFormComponents.Section>
                </div>
            );
        }

        var sectionNodes = this.props.column.sections.section.map(section => {
            return (
                <CRMFormComponents.Section 
                    entity={this.props.entity} 
                    key={guid()} 
                    section={section} 
                    data={this.props.data}
                    objectAttributes={this.props.objectAttributes}
                    dashboard={this.props.dashboard}
                    >
                </CRMFormComponents.Section>
            );
        });

        return (
            <div ref='columnRef' className='portal-column col-md-2'>{sectionNodes}
            </div>
        );
    }
}