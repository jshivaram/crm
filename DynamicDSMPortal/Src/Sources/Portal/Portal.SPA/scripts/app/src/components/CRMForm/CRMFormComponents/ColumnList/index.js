import React, { Component } from 'react'
import CRMFormComponents from '../../CRMFormComponents'
import guid from '~/helpers/guid'

export default class ColumnList extends Component {
    render() {
        
        if(!this.props || !this.props.columns 
            || !this.props.entity || !this.props.data) {
            return (<div>Columns is empty</div>);
        }

        if (!Array.isArray(this.props.columns)) {
            return(<CRMFormComponents.Column
                entity={this.props.entity}
                column={this.props.columns} key={guid() }
                data={this.props.data}
                objectAttributes={this.props.objectAttributes}
                dashboard={this.props.dashboard}
                >
            </CRMFormComponents.Column>)
        }

        var columnNodes = this.props.columns.map(column => {
            return (
                <CRMFormComponents.Column
                    entity={this.props.entity}
                    column={column} key={guid() }
                    data={this.props.data}
                    objectAttributes={this.props.objectAttributes}
                    dashboard={this.props.dashboard}
                    >
                </CRMFormComponents.Column>
            );
        });

        return (
            <div className='portal-column-list row'>{columnNodes}
            </div>
        );
    }
}