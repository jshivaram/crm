import React, { Component } from 'react'
import CRMFormControls from '../../CRMFormControls'

const $ = window.$;

export default class Control extends Component {
    render() {

        if (!this.props.cell) {
            return (<span></span>);
        }

        if (this.props.cell['@visible'] === 'false') {

            let classId = this.props.cell.control['@classid'].toLowerCase();
            let control = CRMFormControls.controlDictionary[classId];

            if (control) {
                let component = React.createElement(control, {
                    control: this.props.cell.control,
                    label: this.props.cell.labels.label,
                    data: this.props.data,
                    objectAttributes: this.props.objectAttributes,
                    entity: this.props.entity,
                    showlabel: this.props.cell['@showlabel'],
                    isHidden: true
                });

                return (<div className='hiddenControlWrapper'>
                    {component}
                </div>);
            }
            return (
                <div className='hiddenControlWrapper'>

                </div>
            );
        }

        if (!this.props.cell.control) {
            return (<span></span>);
        }

        let classId = this.props.cell.control['@classid'].toLowerCase();
        let control = CRMFormControls.controlDictionary[classId];
        let targetEntityType = this.props.entity;
        
        //Special for grids and charts on dashboard
        let dashboardValue = this.props.dashboard;
        if (dashboardValue === true) {
            
            if(classId === '{9fdf5f91-88b1-47f4-ad53-c11efc01a01d}') {
                control = undefined;
            }

            targetEntityType = {
                Name: this.props.cell.control.parameters.TargetEntityType
            };
        }
        if (dashboardValue && Array.isArray(control)) {
            if (this.props.cell.control.parameters.ChartGridMode === 'Grid') {
                control = control[0];
            } else if (this.props.cell.control.parameters.ChartGridMode === 'All') {
                control = control[0];
            } else {
                control = control[1];
            }
        } else if (!dashboardValue && Array.isArray(control)) {
            if (this.props.cell.control.parameters.ChartGridMode === 'Grid') {
                control = control[0];
            } else if (this.props.cell.control.parameters.ChartGridMode === 'All') {
                control = control[0];
            } else {
                control = control[1];
            }
        }


        if (!control) {
            return (
                <div className='unknowControl'></div>
            );
        }

        return (React.createElement(control, {
            control: this.props.cell.control,
            label: this.props.cell.labels.label,
            data: this.props.data,
            objectAttributes: this.props.objectAttributes,
            entity: targetEntityType,
            showlabel: this.props.cell['@showlabel'],
            dashboard: dashboardValue
        }));
    }

    componentDidMount() {
        let hiddenControlWrapper = $('.hiddenControlWrapper');
        if(hiddenControlWrapper.length !== 0) {
            hiddenControlWrapper.hide();
        }
    }
}