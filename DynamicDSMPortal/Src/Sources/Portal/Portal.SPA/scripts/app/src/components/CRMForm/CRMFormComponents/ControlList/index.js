import React, { Component } from 'react'
import CRMFormComponents from '../../CRMFormComponents'
import guid from '~/helpers/guid'

export default class ControlList extends Component {
    render() {
        if (!Array.isArray(this.props.cells)) {
            let cell = this.props.cells;

            console.log(cell);

            return (
                <CRMFormComponents.Control 
                    entity={this.props.entity} 
                    cell={cell} key={cell['@id'] || guid()} 
                    data={this.props.data}
                    objectAttributes={this.props.objectAttributes}
                    onLookupChange={this.props.onLookupChange}>
                </CRMFormComponents.Control>
            );
        }

        var controlNodes = this.props.cells.map(cell => {

            if (!Array.isArray(cell)) {
                return (
                    <CRMFormComponents.Control 
                        entity={this.props.entity} 
                        cell={cell} key={cell['@id'] || guid()} 
                        data={this.props.data}
                        objectAttributes={this.props.objectAttributes}
                        //dashboard={this.props.dashboard}
                        >
                    </CRMFormComponents.Control>
                );
            }

            return cell.map(c => {
                return (
                    <div class='control-in-row'>
                        <CRMFormComponents.Control 
                            entity={this.props.entity} 
                            cell={c} key={c['@id'] || guid()} 
                            data={this.props.data}
                            objectAttributes={this.props.objectAttributes}
                            //dashboard={this.props.dashboard}
                            >
                        </CRMFormComponents.Control>
                    </div>
                );
            });
        });

        return (
            <div className='portal-control-list row'>
                {controlNodes}
            </div>
        );
    }
}