import React, { Component } from 'react'
import CRMFormComponents from '../../../CRMFormComponents'
import './style.scss'

export default class DashboardForm extends Component {
    render() {
        let data = this.props.layout;
        if (!data) {
            return (<div></div>);
        }
        if (!Array.isArray(data.tabs)) {
            let tab = data.tabs;
            return (
                <form id='form-viewer'>
                    <CRMFormComponents.Tab
                        entity={data.entity}
                        tab={tab} key={tab['@id']}
                        data={data.enityAttributes}
                        objectAttributes={data.objectAttributes}
                        dashboard={this.props.dashboard}
                        >
                    </CRMFormComponents.Tab>
                </form>
            )
        } else {
            var tabNodes = data.tabs.map(tab => {
                return (
                    <CRMFormComponents.Tab
                        entity={data.entity}
                        tab={tab} key={tab['@id']}
                        data={data.enityAttributes}
                        objectAttributes={data.objectAttributes}
                        dashboard={this.props.dashboard}
                        >
                    </CRMFormComponents.Tab>
                );
            });
            return (
                <form id='form-viewer'>
                    {tabNodes}
                </form>
            );
        }
    }
}