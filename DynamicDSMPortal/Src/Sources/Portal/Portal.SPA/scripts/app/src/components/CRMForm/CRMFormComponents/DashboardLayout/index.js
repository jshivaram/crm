import React, { Component } from 'react'
import DashboardForm from './DashboardForm'
import './style.scss'
export default class DashboardLayout extends Component {
    render() {
        let layout = this.props.layout;
        return (<DashboardForm dashboard={this.props.dashboard} layout={layout}/>);
    }
}