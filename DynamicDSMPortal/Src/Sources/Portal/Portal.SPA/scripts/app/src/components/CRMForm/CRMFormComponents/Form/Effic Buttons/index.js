import React, { Component } from 'react'
import customStorage from '../../../../../customStore/index'
import constans from '../../../../MainNav/DropDownApplications/constants'
import guid from '~/helpers/guid'
import './style.css'
import GlobalLoader from '~/components/GlobalLoader'

import checkIsBoilerApplication from '~/tools/checkIsBoilerApplication'
const openGlobalPopup = window.openGlobalPopup;
const $ = window.$;

export default class AdditionalButtons extends Component {
    render() {
        if (this.isApplication()) {
            let buttons = this.props.buttons || [];

            let config = customStorage.get(constans.portalAppConfigStorageKey);
            if (!config) {
                return (<span></span>);
            }
            //HARDCODE///////////////////
            let sortArray;
            if (config.applicationName === 'Boiler Application') {
                buttons.push({
                    ddsm_buttontype: 'Save',
                    ddsm_buttonlogicalname: 'submit'
                });
                sortArray = this.customSortButtons(buttons);
            } else {
                sortArray = buttons;
            }
            let result = this.mapButtonsInHtml(sortArray);

            return result;

        } else {
            return <span></span>;
        }
    }

    isApplication() {
        return window.location.href.indexOf('/application') !== -1
            || window.location.href.indexOf('ddsm_application') !== -1;

    }

    mapButtonsInHtml(buttonArray) {
        let convertedArray = buttonArray.map((item, index) => {
            if (!item) {
                return;
            }

            if (item.ddsm_buttontype === 'Link') {
                return this.createLinkButton(item, index);
            } else if (item.ddsm_buttontype === 'Action') {
                return this.createActionButton(item, index);
            } else if (item.ddsm_buttontype === 'Save') {
                return this.props.buttonSave;
            }
        });
        return <div>{convertedArray}</div>;
    }

    createLinkButton(button, index) {
        return (
            <div id={button.ddsm_buttonlogicalname} className={`row application-form-button ${button.ddsm_cssclassname}`} key={index}>
                <a className='k-button'
                    href={button.ddsm_buttondata}
                    target='_blank'
                >
                    <i className='k-font-icon k-i-paste'></i>
                    <span>{button.ddsm_buttonlabel}</span>
                </a>
            </div>
        );
    }

    createActionButton(button, index) {
        let click;
        const entityAttributes = this.props.entityAttributes;
        const isSubmited = entityAttributes ? entityAttributes['ddsm_applicationissubmited'] : false;

        let defaultButtonBehavour = function (e) {
            e.preventDefault();
        }

        if (!button.ddsm_buttondata) {
            return (<div id={button.ddsm_buttonlogicalname} className={`row application-form-button ${button.ddsm_cssclassname}`} key={index}>
                <button className='k-button' onClick={defaultButtonBehavour}>
                    <i className='k-font-icon k-i-paste'></i>
                    <span>{button.ddsm_buttonlabel}</span>
                </button>
            </div>);
        }

        if (button.ddsm_buttonlogicalname === 'convertToProject' && isSubmited) {
            let isConverted = this.props.entityAttributes ? this.props.entityAttributes.ddsm_isconverted : false;

            if (window.location.href.indexOf('form') === -1 || isConverted) {
                return <span></span>;
            }

            let convertToProjectCount = 0;

            click = function (e) {
                e.preventDefault();
                const login = customStorage.get('login');
                let isBoilerApplication = checkIsBoilerApplication();
                if (isBoilerApplication && login && isSubmited) {
                    const isTermsAndConditionsChecked = $('[name=ddsm_termsandconditions].k-checkbox').prop('checked');

                    if (!isTermsAndConditionsChecked) {
                        return openGlobalPopup('Please agree with terms and coditions', false);
                    }
                }
                convertToProjectCount++;
                if (convertToProjectCount > 1) {
                    return;
                }

                const loader = new GlobalLoader();
                loader.show();

                updateApplication(function () {
                    const applicationId = $('input[name="id"]').val();
                    $.post('/api/application/convertToProject?applicationId=' + applicationId)
                        .done((projectId) => {
                            loader.hide();

                            window.openGlobalPopupWithProgress(function () {
                                 const projectUrl = `/form/${projectId}/ddsm_project`;
                                 window.location.replace(projectUrl);
                                 convertToProjectCount = 0;
                            });                           
                        })
                        .fail((err) => {
                            window.openGlobalPopup(err.statusText);
                            loader.hide();
                            convertToProjectCount = 0;
                        });
                }, function () {
                    loader.hide();
                    convertToProjectCount = 0;
                });
            };

            return (<div id={button.ddsm_buttonlogicalname} className={`row application-form-button ${button.ddsm_cssclassname}`} key={index}>
                <button className='k-button' onClick={click}>
                    <i className='k-font-icon k-i-paste'></i>
                    <span>{button.ddsm_buttonlabel}</span>
                </button>
            </div>);
        }

        if (button.ddsm_buttondata.toLowerCase() === 'save' && !isSubmited) {
            this.popupId = guid();

            click = function (e) {
                e.preventDefault();

                window.isDraftSave = true;
                $('#crm-btn-save button').eq(0).trigger('click');

            };

            return (<div id={button.ddsm_buttonlogicalname} className={`row application-form-button ${button.ddsm_cssclassname} hidden-effic`} key={index}>
                <button className='k-button' onClick={click}>
                    <i className='k-font-icon k-i-paste'></i>
                    <span>{button.ddsm_buttonlabel}</span>
                </button>
            </div>);
        }

        return (<span></span>);
    }

    customSortButtons(buttons) {
        let sortArrayByNameList = function (targetArray, stringArray) {
            let resultArray = [];

            stringArray.forEach(function (name) {
                let element = targetArray.find(item => item.ddsm_buttonlogicalname === name);
                if (!element) {
                    console.error('Error when sort application additional buttons');
                } else {
                    resultArray.push(element);
                }
            });

            return resultArray;
        }

        const priorityLogicalNameList = ['visit', 'save', 'submit'];
        let arrayWithPriority = buttons
            .filter(item => priorityLogicalNameList.includes(item.ddsm_buttonlogicalname));

        arrayWithPriority = sortArrayByNameList(arrayWithPriority, priorityLogicalNameList);

        let arrayWithoutPriority = buttons
            .filter(item => !(priorityLogicalNameList.includes(item.ddsm_buttonlogicalname)))
            .sort(function (a, b) {
                if (a.ddsm_buttonlogicalname < b.ddsm_buttonlogicalname) {
                    return -1;
                }
                if (a.ddsm_buttonlogicalname > b.ddsm_buttonlogicalname) {
                    return 1;
                }
                return 0;
            });

        let resultArray = arrayWithPriority.concat(arrayWithoutPriority);
        return resultArray;
    }
}

function updateApplication(success, error) {
    saveSubgridsData();

    var formData = $('#form-viewer').serializeObject();
    formData.LogicalName = 'ddsm_application'

    var keys = Object.keys(formData);

    keys.forEach(key => {
        let value = formData[key]

        if (value === '') {
            delete formData[key];
            return;
        }

        if (Array.isArray(value)) {
            value = value[0];
            formData[key] = value;
        }

        if (isDateInput(key)) {
            try {
                const datePicker = $(`input[name="${key}"]`).data('kendoDatePicker');
                const date = datePicker.value();
                const isoStringDate = date.toISOString();
                formData[key] = isoStringDate;
            } catch (err) {
                console.log('err when parse date', err);
                delete formData[key];
            }
        }

        let i = key.indexOf('.');
        if (i !== -1) {
            let keyForBase = key.substring(0, i);
            let newKey = key.substring(i + 1);
            let val = formData[key];

            delete formData[key];

            if (!formData.hasOwnProperty(keyForBase)) {
                formData[keyForBase] = {};
            }

            if (Array.isArray(val)) {
                val = val[0];
            }
            formData[keyForBase][newKey] = val;
        }
    });

    var jsonEntityData = JSON.stringify(formData);

    var logicalName = 'ddsm_application';

    var data = {
        JsonEntityData: jsonEntityData,
        LogicalName: logicalName
    };

    data = JSON.stringify(data);

    $.ajax({
        contentType: 'application/json',
        dataType: 'json',
        url: '/api/entity/update',
        method: 'PUT',
        data: data,
        success: function (res) {
            if (!success || typeof (success) !== 'function') {
                return;
            }

            success(res);
        },
        error: function (err) {
            if (!error || typeof (error) !== 'function') {
                return;
            }

            error(err);
        }
    });
}

function isDateInput(fieldName) {
    const $input = $(`input[name="${fieldName}"]`);
    if ($input.length === 0) {
        return false;
    }

    return $input.hasClass('date-time-control');
}

function saveSubgridsData() {
    let $grids = $('div.k-grid');
    if ($grids.length === 0) {
        return;
    }

    $.each($grids, (i, grid) => {
        let gridData = $(grid).data('kendoGrid');
        if (!gridData) {
            return;
        }

        let gridDataSource = gridData.dataSource;
        if (!gridDataSource || !gridDataSource.hasChanges()) {
            return;
        }

        gridDataSource.sync();
    });
}