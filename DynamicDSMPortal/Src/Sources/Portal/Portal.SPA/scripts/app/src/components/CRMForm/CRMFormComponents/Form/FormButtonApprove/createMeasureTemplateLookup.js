
const $ = window.$;

export default function (programOfferingId, onRenderComplete) {
    const url = '/api/approve/GetMeasureTemplates?ProgramOfferingId=' + programOfferingId;

    const id = 'approve-popup-mt-lookup';

    const $mtDropDown = $('#' + id);
    $mtDropDown.empty();

    const mtLookup = $mtDropDown.data('kendoDropDownList');
    mtLookup && mtLookup.destroy();
    
    $mtDropDown.kendoDropDownList({
        filter: 'contains',
        height: 600,
        ignoreCase: true,
        dataTextField: 'Text',
        dataValueField: 'Value',
        dataSource: {
            serverFiltering: true,
            transport: {
                read: {
                    dataType: 'json',
                    url: url,
                    type: 'GET'
                }
            }
        },
        optionLabel: '--',
        select: function (ev) {
            const selectedMeasureTemplate = ev.dataItem.Value;

            if (selectedMeasureTemplate) {
                $('#pop-up-approve-button').show();
            } else {
                $('#pop-up-approve-button').hide();
            }
        },
        dataBound: function() {
            onRenderComplete && onRenderComplete();
        }
    });
}