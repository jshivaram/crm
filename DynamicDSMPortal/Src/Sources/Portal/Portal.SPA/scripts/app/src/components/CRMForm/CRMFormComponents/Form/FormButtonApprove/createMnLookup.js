
const $ = window.$;

export default function (id) {
    const url =  '/api/entity/GetFilteredEntitiesForLookupByEntityName?entityLogicalName=ddsm_modelnumber';

    

    $('#' + id).kendoDropDownList({
        filter: 'contains',
        height: 600,
        ignoreCase: true,
        dataTextField: 'Text',
        dataValueField: 'Value',
        dataSource: {
            serverFiltering: true,
            transport: {
                read: {
                    dataType: 'json',
                    url: url,
                    type: 'GET'
                }
            }
        },
        optionLabel: '--',
    });
}