
import createMeasureTemplateLookup from './createMeasureTemplateLookup';
const $ = window.$;

export default function () {
    const $measureTemplateDropDown = $('#approve-popup-mt-lookup');
    //const $measureTemplateLabel = $('#MT-Lookup-Approve-Label');

    hideMeasureTemplateLookup();

    const programOfferingIdUrl = '/api/approve/GetProgramOfferings';

    const poDropdown = $('#approve-popup-po-lookup');
    poDropdown.empty();
    const $poLookup = poDropdown.data('kendoDropDownList');

    $poLookup && $poLookup.destroy();

    poDropdown.kendoDropDownList({
        filter: 'contains',
        height: 600,
        ignoreCase: true,
        dataTextField: 'Text',
        dataValueField: 'Value',
        dataSource: {
            serverFiltering: true,
            transport: {
                read: {
                    dataType: 'json',
                    url: programOfferingIdUrl,
                    type: 'GET'
                }
            }
        },
        optionLabel: '--',
        select: function (ev) {
            const selectedProgramOfferingId = ev.dataItem.Value;

            if (selectedProgramOfferingId) {
                createMeasureTemplateLookup(selectedProgramOfferingId, function () {
                    showMeasuerTemplateLookup();
                });
            } else {
                hideMeasureTemplateLookup();
                $('#pop-up-approve-button').hide();
            }
        }
    });

    function hideMeasureTemplateLookup() {
        $measureTemplateDropDown.parents('div#approve-popup-mt-lookup-wrapper').hide();
    }

    function showMeasuerTemplateLookup() {
        $measureTemplateDropDown.parents('div#approve-popup-mt-lookup-wrapper').show();
    }
}