import React, { Component } from 'react'
import guid from '~/helpers/guid.js'
import createProgramOfferingLookup from './createProgramOfferingLookup'
import onPopupApproveClickWrapper from './onPopupApproveClickWrapper'
import MessagePopUp from '../../../../MessagePopUp'
import btnSaveHandler from '~/components/CRMForm/CRMFormComponents/Form/FormButtonSave/btnSaveHandler.js'
import GlobalLoader from '~/components/GlobalLoader'

const $ = window.$;
const openGlobalPopup = window.openGlobalPopup;

export default class FormButtonApprove extends Component {
    render() {
        const infoForButtonApprove = this.props.infoForButtonApprove;
        const entityLogicalName = infoForButtonApprove.entityLogicalName;
        this.entityLogicalName = entityLogicalName;

        let text = this.props.text;

        if (!text) {
            text = 'Approve';
        }

        this.needToCreateDropDown = false;

        if (!entityLogicalName
            || (entityLogicalName !== 'ddsm_modelnumber'
                && entityLogicalName !== 'ddsm_sku')) {
            return <span></span>;
        }

        this.needToCreateDropDown = true;
        this.measureTemplateLookupId = guid();
        this.modelNumberLookupId = guid();
        infoForButtonApprove.measureTemplateLookupId = this.measureTemplateLookupId;
        infoForButtonApprove.modelNumberLookupId = this.modelNumberLookupId;
        this.popUpId = guid();

        this.programOfferingLookupId = guid();

        return (
            <div id='crm-btn-close' className='row'>
                <button className='k-button' onClick={this.onClicCallPopupkWrapper(infoForButtonApprove)}>
                    {text}
                </button>
                <div className='pop-up-wrapper' id={this.popUpId} style={{ display: 'none' }}>

                    <div>

                        <label htmlFor='approve-popup-po-lookup'>
                            Program Offering
                        </label>
                        <select
                            id='approve-popup-po-lookup'
                            name='popup-programm-offering'
                            className={'crm-lookup-select'}>
                        </select>
                    </div>

                    <div id='approve-popup-mt-lookup-wrapper'>
                        <label htmlFor='approve-popup-mt-lookup' id='MT-Lookup-Approve-Label'>
                            Measure Template
                    </label>
                        <select
                            id='approve-popup-mt-lookup'
                            name='ddsm_measuretemplateselectorforapproval.id'
                            className={'crm-lookup-select'}>
                        </select>
                    </div>

                    <div className='pop-up-buttons'>
                        <div className='col-md-1 col-md-offset-8'>
                            <button id='pop-up-approve-button' className='k-button' onClick={onPopupApproveClickWrapper.call(this, infoForButtonApprove)}>
                                Proceed
                            </button>
                        </div>
                    </div>
                    <MessagePopUp listener={this}>
                    </MessagePopUp>
                </div>

            </div>
        );
    }
    componentDidMount() {
        $('#pop-up-approve-button').hide();

        if (!this.needToCreateDropDown) {
            return;
        }

        createProgramOfferingLookup('approve-popup-po-lookup', 'approve-popup-mt-lookup');
    }
    //this events open popup window for approve MN
    onClicCallPopupkWrapper() {
        const self = this;

        return function (e) {
            e.preventDefault();
            approve(self);
            //self.openPopup();
        }
    }

    openPopup() {
        const popUp = $('#' + this.popUpId);
        const self = this;

        if (!popUp.data('kendoWindow')) {
            popUp.kendoWindow({
                width: '600px',
                modal: true,
                title: 'Please select the matching Program Offering and Measure Template',
                visible: false,
                actions: [
                    'Close'
                ],
                open: function (ev) {
                    const $parent = ev.sender.element.parent()
                    $parent.addClass('approve-pop-up');

                    const $poDropDown = $('#approve-popup-po-lookup');
                    if($poDropDown.length) {
                        const poLookup = $poDropDown.data('kendoDropDownList');
                        if(poLookup) {
                            poLookup.value(null);
                        }
                    }

                    $('#approve-popup-mt-lookup-wrapper').hide();
                    const $mtDropDown = $('#approve-popup-mt-lookup');
                    if($mtDropDown.length) {
                        const mtLookup = $mtDropDown.data('kendoDropDownList');
                        if(mtLookup) {
                            mtLookup.value(null);
                        }
                    }

                    $('#pop-up-approve-button').hide();
                }
            });
        }

        popUp.data('kendoWindow').center().open();

        $(document).on('click', '.k-overlay', function () {
            self.closePopUp();
        });
    }

    closePopUp() {
        const popUp = $('#' + this.popUpId);
        popUp.data('kendoWindow').close();
    }
}

function approve(formButtonApprove) {
    const entityLogicalName = formButtonApprove.entityLogicalName;
    const entityId = formButtonApprove.props.infoForButtonApprove.entityId;

    if (entityLogicalName === 'ddsm_modelnumber') {
        if (!entityId) {
            createAndApproveModelNumber();
        } else {
            approveModelNumber(entityId);
        }
    } else if (entityLogicalName === 'ddsm_sku') {
        if (window.location.href.indexOf('/form/createSkuFromModelNumberApproval/' !== -1)) {
            CreateAndApproveSkuFromQuickForm();
        }
    }
}

function createAndApproveModelNumber() {
    let mtId = $('input[name="ddsm_measuretemplateselectorforapproval.Id"]').val();

    if (!mtId) {
        mtId = $('select[name="ddsm_measuretemplateselectorforapproval.Id"]').val();
    }

    if (!mtId) {
        openGlobalPopup('Please, select Measure');
        return;
    }

    const entityLogicalName = 'ddsm_modelnumber';

    const infoForBtnSave = {
        entityName: entityLogicalName
    };

    let formDataJson = btnSaveHandler(infoForBtnSave, null, null, null, true)();

    const formData = JSON.parse(formDataJson);
    formData['ddsm_createmnapproval'] = { Value: '962080001' };
    formDataJson = JSON.stringify(formData);

    const dataToSent = JSON.stringify({
        JsonEntityData: formDataJson,
        LogicalName: entityLogicalName
    });

    const loader = new GlobalLoader();
    loader.show();

    $.ajax({
        contentType: 'application/json',
        dataType: 'json',
        url: '/api/entity/create',
        method: 'POST',
        data: dataToSent,
        success: function (createdEntityId) {
            loader.hide();
            openGlobalPopup('Please upload support documentation for submitted Model Number.');

            setTimeout(function () {
                window.location.replace(`/form/${createdEntityId}/${entityLogicalName}`)
            }, 3000);
        },
        error: function () {
            loader.hide();
            openGlobalPopup('Error. Cannot approve.');
        }
    });
}

function approveModelNumber(entityId) {
    const entityLogicalName = 'ddsm_modelnumber';

    const infoForBtnSave = {
        entityName: entityLogicalName,
        action: '/api/entity',
        method: 'PUT'
    };

    let mtId = $('input[name="ddsm_measuretemplateselectorforapproval.Id"]').val();

    if (!mtId) {
        mtId = $('select[name="ddsm_measuretemplateselectorforapproval.Id"]').val();
    }

    if (!mtId) {
        openGlobalPopup('Please, select Measure');
        return;
    }

    const loader2 = new GlobalLoader();
    loader2.show();

    btnSaveHandler(infoForBtnSave, null, function () {
        const approveUrl = '/api/approve/approveModelNumber';

        const dataToSent = {
            MeasureTemplateSelectorForApproval: mtId,
            entityId: entityId,
            entityLogicalName: entityLogicalName
        };

        const loader2 = new GlobalLoader();
        loader2.show();

        $.post(approveUrl, dataToSent)
            .done(() => {
                loader2.hide();
                openGlobalPopup('Request has been submitted.');

                setTimeout(function () {
                    window.location.replace(`/form/${dataToSent.entityId}/${entityLogicalName}`);
                }, 3000);
            })
            .fail(err => {
                loader2.hide();
                openGlobalPopup('Error. Cannot approve.');
                console.error(err);
            });
    }, function () {
        loader2.hide();
        openGlobalPopup('Error. Cannot save entity.');
    })();
}

function CreateAndApproveSkuFromQuickForm() {
    const url = window.location.href;
    const urlArr = url.split('/');
    const segmentsCount = urlArr.length;

    const modelNumberApprovalId = urlArr[segmentsCount - 1]
    const measureTeamplateId = urlArr[segmentsCount - 2];
    const modelNumberId = urlArr[segmentsCount - 3];

    const entityLogicalName = 'ddsm_sku';

    const infoForBtnSave = {
        entityName: entityLogicalName
    };

    let formDataJson = btnSaveHandler(infoForBtnSave, null, null, null, true)();

    const formData = JSON.parse(formDataJson);
    formData['ddsm_modelnumber'] = { Id: modelNumberId };
    formData['ddsm_measuretemplateselectorforapproval'] = { Id: measureTeamplateId };
    formData['ddsm_createapprovalsku'] = { Value: '962080000' };
    formDataJson = JSON.stringify(formData);

    const dataToSent = JSON.stringify({
        JsonEntityData: formDataJson,
        LogicalName: entityLogicalName
    });

    const loader = new GlobalLoader();
    loader.show();

    $.ajax({
        contentType: 'application/json',
        dataType: 'json',
        url: '/api/entity/create',
        method: 'POST',
        data: dataToSent,
        success: function () {
            loader.hide();
            openGlobalPopup('Your approval request is submitted.');

            setTimeout(function () {
                window.location.replace(`/form/${modelNumberApprovalId}/ddsm_modelnumberapproval`);
            }, 3000);
        },
        error: function () {
            loader.hide();
            openGlobalPopup('Error. Cannot approve.');
        }
    });
}