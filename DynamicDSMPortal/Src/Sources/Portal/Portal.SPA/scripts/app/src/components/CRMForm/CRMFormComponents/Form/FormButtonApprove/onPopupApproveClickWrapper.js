const $ = window.$;
import Loader from '../../../../Loader'
import btnSaveHandler from '~/components/CRMForm/CRMFormComponents/Form/FormButtonSave/btnSaveHandler.js'

const openGlobalPopup = window.openGlobalPopup;

export default function (infoForButtonApprove) {
    const self = this;

    return function (ev) {
        const $button = $(ev.target);

        Loader.show($('#loader'), $('#wrapper'));

        const entityLogicalName = infoForButtonApprove.entityLogicalName;

        const method = !infoForButtonApprove.entityId ? 'POST' : 'PUT';

        const infoForBtnSave = {
            entityName: entityLogicalName,
            action: '/api/entity',
            method: method
        };

        $button.hide();

        if (!infoForButtonApprove.entityId) {
            const jsonEntityData = btnSaveHandler(infoForBtnSave, null, null, function () {
                Loader.hide($('#loader'), $('#wrapper'));
                openGlobalPopup('Error. Cannot save entity.');
                $button.show();
            }, true)();

            if (!jsonEntityData) {
                return;
            }

            const mtLookup = $('#approve-popup-mt-lookup');
            const mtVal = mtLookup.val();

            if (!mtVal) {
                return;
            }

            var data = {
                JsonEntityData: jsonEntityData,
                LogicalName: entityLogicalName,
                MeasureTemplateSelectorForApproval: mtVal
            };

            $.post('/api/Approve/CreateAndApprove', data)
                .done((createdEntityId) => {
                    self.openMessagePopup('Your approval request is submitted.');
                    Loader.hide($('#loader'), $('#wrapper'));

                    setTimeout(function () {
                        window.location.replace(`/form/${createdEntityId}/${entityLogicalName}`)
                    }, 3000);
                })
                .fail(err => {
                    Loader.hide($('#loader'), $('#wrapper'));
                    const popupErrorMessage = entityLogicalName === 'ddsm_modelnumber' ? 'Cannot create MN' : 'Cannot create SKU';
                    openGlobalPopup(popupErrorMessage);

                    console.error(err);
                })
                .always(self.closePopUp());

            return;
        }

        btnSaveHandler(infoForBtnSave, null, function (createdEntityId) {
            const mtLookup = $('#approve-popup-mt-lookup');

            if (mtLookup.length === 0 || !entityLogicalName) {
                return;
            }

            const mtVal = mtLookup.val();

            if (!mtVal) {
                return;
            }

            const dataToSent = {
                MeasureTemplateSelectorForApproval: mtVal,
                entityId: infoForButtonApprove.entityId ? infoForButtonApprove.entityId : createdEntityId,
                entityLogicalName: entityLogicalName
            };

            let approveUrl = '/api/approve'

            if (entityLogicalName === 'ddsm_modelnumber') {
                approveUrl += '/approveModelNumber';
            } else {
                approveUrl += '/ApproveSku';
            }

            $.post(approveUrl, dataToSent)
                .done(() => {
                    self.openMessagePopup('Your approval request is submitted.');
                    Loader.hide($('#loader'), $('#wrapper'));

                    setTimeout(function () {
                        window.location.replace(`/form/${dataToSent.entityId}/${entityLogicalName}`)
                    }, 3000);
                })
                .fail(err => {
                    Loader.hide($('#loader'), $('#wrapper'));
                    const popupErrorMessage = entityLogicalName === 'ddsm_modelnumber' ? 'MN was created without MNA' : 'SKU was created without SKUA.';
                    openGlobalPopup(popupErrorMessage);

                    setTimeout(function () {
                        window.location.replace(`/form/${dataToSent.entityId}/${entityLogicalName}`)
                    }, 3000);
                    // $button.show();
                    console.error(err);
                })
                .always(self.closePopUp());
        }, function () {
            Loader.hide($('#loader'), $('#wrapper'));
            openGlobalPopup('Error. Cannot save entity.');
            $button.show();
        })();
    }
}