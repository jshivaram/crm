import React, { Component } from 'react'

export default class FormButtonClose extends Component {
    render() {
        var infoForBtnClose = this.props.infoForBtnClose;
        return (
            <div id='crm-btn-close' className='row'>
                <button className='k-button' onClick={this.props.btnCloseHandler(infoForBtnClose)}>
                    <i className='k-font-icon  k-i-x'></i>
                </button>
            </div>
        );
    }
}