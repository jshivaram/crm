Где закодено не верно
===================
```
Файл index.js
Класс FormButtonSave
```
Как закодено не верно
===================
```
Хардкод
```

Было необходимо добавить возможность с формы application переходить по ссылке на PDF файл и сайт
***
Для этого нужно сделать проверку что мы находимся на том же application что нам нужно. Название аппликейшена можно получить из storage,
```
    const applicationConfig = customStorage.get(constans.portalAppConfigStorageKey);
    const applicationName = applicationConfig ? applicationConfig.applicationName : null;
```

***
если заявка имеет текст нужной нам заявки, 
```
if (applicationName && applicationName.indexOf('Efficient Boiler Program application') !== -1)
```

то поле кнопки Save добавляется кнопкой PDF и ссылкой на сайт

```
return (
                <div>
                    <div id='crm-btn-save' className='row crm-form-save-button'>
                        <button className='k-button' onClick={this.props.btnSaveHandler(infoForBtnSave, form)}
                            type='button' >
                            <i className='k-font-icon k-i-checkmark'></i>
                            <span> Save</span>
                        </button>
                    </div>
                    <div className='row crm-form-save-button'>
                        <a className='k-button' 
                            href='https://www.fortisbc.com/Rebates/RebatesOffers/EfficientBoilerProgram/Documents/EBPApp.pdf'
                            target= '_blank'
                             >
                            <i className='k-font-icon k-i-paste'></i>
                            <span> PDF</span>
                        </a>
                    </div>
                    <div className='row crm-form-save-button'>
                        <a className='k-button'
                            href='https://www.fortisbc.com/Rebates/RebatesOffers/EfficientBoilerProgram/Pages/default.aspx'
                            target= '_blank'
                             >
                            <i className='k-font-icon k-i-redo'></i>
                            <span> Visit Site</span>
                        </a>
                    </div>
                </div>
            );
```
