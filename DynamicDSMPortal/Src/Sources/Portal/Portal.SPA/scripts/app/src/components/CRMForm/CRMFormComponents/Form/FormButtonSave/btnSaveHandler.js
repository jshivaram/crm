import { browserHistory } from 'react-router'
import Loader from '~/components/Loader'
import _ from 'lodash'
import customStorage from '../../../../../customStore'
import loginAndRedirectToApplication from './loginAndRedirectToApplication'
import getApplicationFromStore from '~/tools/getCurrentApplicationFromStore'
import moment from 'moment'

const $ = window.$;
const openGlobalPopup = window.openGlobalPopup;

export function addFormValidator() {
    const validator = $('#form-viewer').kendoValidator({
        rules: {
            customRule1: function ($input) {

                if ($input.hasClass('portal-picklist') && $input.val() === '-1') {
                    return false;
                }

                if ($input.attr('required')) {
                    return $.trim($input.val()) !== '';
                }

                return true;
            }
        }
    }).data('kendoValidator');

    return validator;
}

export default function (infoForBtnSave, form, onSuccess, onError, getFormDataWithoutSave) {
    return function (e) {
        if (e) {
            e.preventDefault();
        }
        let application = getApplicationFromStore();
        application = application ? application : {};
        let isBoilerApplication = application.applicationName === 'Boiler Application' &&
            (window.location.href.indexOf('/ddsm_application') !== -1 ||
                window.location.href.indexOf('/application') !== -1);
        const login = customStorage.get('login');

        const validator = addFormValidator();
        const isFormValid = validator.validate();

        if (!isFormValid) {
            const errors = validator.errors();
            errors.forEach(err => showNotificationAlert(err));
            Loader.hide($('#loader'), $('#wrapper'));

            if(onError) {
                onError(); 
            }

            return;
        }

        removeNamesFromQuickFormControls();

        var formData = $('#form-viewer').serializeObject();
        formData.LogicalName = infoForBtnSave.entityName;

        var keys = Object.keys(formData);

        keys.forEach(key => {
            let value = formData[key]

            // if (value === '') {
            //     delete formData[key];
            //     return;
            // }

            if (Array.isArray(value)) {
                value = value.filter(v => v)[0];
                value = value ? value : '';
                formData[key] = value;
            }

            if (isDateInput(key)) {
                try {
                    const datePicker = $(`input[name="${key}"]`).data('kendoDatePicker');
                    const date = datePicker.value();
                    const nowDate = new Date();

                    const combineDateTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 
                                                    nowDate.getHours(), nowDate.getMinutes(), nowDate.getSeconds(), nowDate.getMilliseconds());

                    const utcDateStr = moment.utc(combineDateTime.toISOString()).format();
                    formData[key] = utcDateStr;
                } catch (err) {
                    console.error('err when parse date', err);
                    delete formData[key];
                }
            }

            let i = key.indexOf('.');
            if (i !== -1) {
                let keyForBase = key.substring(0, i);
                let newKey = key.substring(i + 1);
                let val = formData[key];

                delete formData[key];

                if (!formData.hasOwnProperty(keyForBase)) {
                    formData[keyForBase] = {};
                }

                if (Array.isArray(val)) {
                    val = val.filter(v => v)[0];
                    val = val ? val : '';
                }

                formData[keyForBase][newKey] = val;
            }
        });

        var jsonEntityData = JSON.stringify(formData);

        var logicalName = infoForBtnSave.entityName;

        if (getFormDataWithoutSave) {
            Loader.hide($('#loader'), $('#wrapper'));
            return jsonEntityData;
        }

        var data = {
            JsonEntityData: jsonEntityData,
            LogicalName: logicalName
        };

        const subgridsData = getSubgridsData(infoForBtnSave);

        if (subgridsData) {
            data.subgridCreationModels = subgridsData;
        }

        saveSubgridsData(infoForBtnSave);

        Loader.show($('#loader'), $('#wrapper'));

        if (form && form.props.isRelatedEntityCreationForm) {
            createRelatedEntityFromForm(form, data);
            return;
        }

        data = JSON.stringify(data);

        let isPost = infoForBtnSave.method.toLowerCase() === 'post';
        let urlForCreateOrUpdate = infoForBtnSave.action + (isPost ? '/create' : '/update');
        isBoilerApplication = false;

        try {
            isBoilerApplication = application.applicationName === 'Boiler Application' &&
                (window.location.href.indexOf('/ddsm_application') !== -1 ||
                    window.location.href.indexOf('/application') !== -1);
            if (isBoilerApplication) {
                if (!login) {
                    if (window.isDraftSave) {
                        urlForCreateOrUpdate = '/api/application/CreateApplicationFromBoilerApplicationDraft';
                        window.isDraftSave = undefined;
                    } else {
                        infoForBtnSave.method = 'post';
                        urlForCreateOrUpdate = '/api/application/CreateApplicationFromBoilerApplication';
                    }
                }
                if (login) {

                    if (!window.isDraftSave) {
                        infoForBtnSave.method = 'post';
                        urlForCreateOrUpdate = '/api/application/CreateApplicationFromBoilerApplication';
                        isPost = true;
                    }
                }
            }
        } catch (error) {
            console.log(error);
        }

        if (!onSuccess) {
            onSuccess = (entityId) => {

                if (!isPost) {
                    let isBoilerApplication = application.applicationName === 'Boiler Application' &&
                        (window.location.href.indexOf('/ddsm_application') !== -1 ||
                            window.location.href.indexOf('/application') !== -1);

                    if (isBoilerApplication) {
                        window.location.reload();
                    }
                    return;
                }

                if (!entityId) {
                    return openGlobalPopup('Can`t create Application', false);
                }

                if (logicalName === 'ddsm_application') {
                    //openGlobalPopup('Contact, Account and Site has been created. Please add measure and required documents', false);
                    openGlobalPopup('Your User Login has been generated. You may continue to fill out the application.', false);
                    if (infoForBtnSave.isApplicationForUnreg) {

                        if (isBoilerApplication) {
                            if (!customStorage.get('login')) {
                                const loginFromForm = $('input[name="ddsm_login"]').val();
                                const passwordFromForm = $('input[name="ddsm_password"]').val();
                                loginAndRedirectToApplication(loginFromForm, passwordFromForm, entityId);
                                return;
                            } else {
                                window.location.replace(`/form/${entityId}/ddsm_application`);
                            }
                        }

                        browserHistory.push('/');
                        return;
                    } else {
                        window.location.replace(`/form/${entityId}/ddsm_application`);
                        return;
                    }
                }
                if (entityId) {
                    if (infoForBtnSave.isApplicationForUnreg) {
                        browserHistory.push('/');
                        return;
                    }
                    browserHistory.push(`/form/${entityId}/${logicalName}`);
                } else if (!isPost) {
                    browserHistory.push(`/form/${formData.id}/${logicalName}`);
                }
            };
        }

        if (!onError) {
            onError = err => {
                console.log(err);
                if (logicalName === 'ddsm_application') {
                    openGlobalPopup('Application is not created cause of error', false);
                }
                if (err.statusText === 'Not enough rights for edit') {
                    showErrorAlert(err.statusText);
                } else {
                    showErrorAlert(err.statusText);
                }
            };
        }
        window.isDraftSave = undefined;
        $.ajax({
            contentType: 'application/json',
            dataType: 'json',
            url: urlForCreateOrUpdate,
            method: infoForBtnSave.method,
            data: data,
            success: function (res) {
                onSuccess(res);
                Loader.hide($('#loader'), $('#wrapper'));
            },
            error: function (err) {
                onError(err);
                Loader.hide($('#loader'), $('#wrapper'));
            }
        });
    }
}

function createRelatedEntityFromForm(form, data) {
    const props = form.props;

    data.relationshipShcemeName = props.relationshipName;
    data.entityLogicalName = props.relatedEntityName;
    data.entityId = props.relatedEntityId;

    const url = '/api/entity/CreateRelatedEntity';

    const dataToSent = JSON.stringify(data);

    $.ajax({
        contentType: 'application/json',
        dataType: 'json',
        url: url,
        method: 'POST',
        data: dataToSent,
        success: () => {
            Loader.hide($('#loader'), $('#wrapper'));
            browserHistory.push(`/form/${data.entityId}/${data.entityLogicalName}`);
        },
        error: err => {
            Loader.hide($('#loader'), $('#wrapper'));
            console.log(err);

            browserHistory.push(`/form/${data.entityId}/${data.entityLogicalName}`);
        }
    });
}

function isDateInput(fieldName) {
    const $input = $(`input[name="${fieldName}"]`);
    if ($input.length === 0) {
        return false;
    }

    return $input.hasClass('date-time-control');
}

function removeNamesFromQuickFormControls() {
    let $quickFormInputs = $('.quick-form input');

    $.each($quickFormInputs, function (i, input) {
        let $input = $(input);
        $input.removeAttr('name');
    });
}

function showErrorAlert(errText) {
    const notification = $('#notification').data('kendoNotification');

    if (!notification) {
        return;
    }

    notification.show({
        title: 'Error',
        message: errText,
    }, 'error');
}

export function showNotificationAlert(errText) {
    const notification = $('#notification').data('kendoNotification');

    if (!notification) {
        return;
    }

    notification.showText(errText, 'warning');
}

function getSubgridsData(infoForBtnSave) {

    if (infoForBtnSave.method === 'POST') {
        const $grids = $('div.k-grid');
        if ($grids.length === 0) {
            return;
        }

        const gridsData = [];

        $.each($grids, (i, grid) => {
            const kendoGrid = $(grid).data('kendoGrid');
            if (!kendoGrid) {
                return;
            }

            const columns = getColumnsLogicalNamesFromGrid(kendoGrid);

            if (!columns || columns.length < 1) {
                return;
            }

            const gridData = getDataFromGrid(kendoGrid, columns);

            if (!gridData || gridData.length < 1) {
                return;
            }

            const additionalData = getAdditionalDataFromGrid(kendoGrid);
            if (!additionalData) {
                return;
            }

            const gridsDataItem = {
                entityLogicalName: additionalData.entityLogicalName,
                relationshipName: additionalData.relationshipName,
                GridDataJsonList: gridData
            };

            gridsData.push(gridsDataItem);
        });

        return gridsData;
    }
}

function saveSubgridsData(infoForBtnSave) {

    if (infoForBtnSave.method === 'POST') {
        return;
    }

    let $grids = $('div.k-grid');
    if ($grids.length === 0) {
        return;
    }

    $.each($grids, (i, grid) => {
        let gridData = $(grid).data('kendoGrid');
        if (!gridData) {
            return;
        }

        let gridDataSource = gridData.dataSource;
        if (!gridDataSource || !gridDataSource.hasChanges()) {
            return;
        }

        gridDataSource.sync();
    });
}

function getColumnsLogicalNamesFromGrid(kendoGrid) {
    const result = kendoGrid.columns.map(c => c.field).filter(f => f);
    return result;
}

function getDataFromGrid(kendoGrid, columns) {
    const gridDataSource = kendoGrid.dataSource;

    if (!gridDataSource) {
        return;
    }

    const gridData = gridDataSource.data();

    const result = gridData.map(row => {
        const bufRow = _.clone(row);
        const bufRowKeys = Object.keys(bufRow);

        bufRowKeys.forEach((key) => {
            if (!_.includes(columns, key) || !bufRow[key]) {
                delete bufRow[key];
            }
        });

        const jsonBufRow = JSON.stringify(bufRow);

        return jsonBufRow;
    });

    return result;
}

//get relationshipName and logical name
function getAdditionalDataFromGrid(kendoGrid) {
    const columns = kendoGrid.columns;
    if (!columns || columns.length < 1) {
        return;
    }

    const entityLogicalName = columns.filter(c => c.field === 'entityLogicalName')[0].title;
    const relationshipName = columns.filter(c => c.field === 'relationshipName')[0].title;

    return {
        entityLogicalName,
        relationshipName
    };
}

// function dateToMDY(date) {
//     var d = date.getDate();
//     var m = date.getMonth() + 1;
//     var y = date.getFullYear();
//     return (m <= 9 ? '0' + m : m) + '/' + (d <= 9 ? '0' + d : d) + '/' + y;
// }