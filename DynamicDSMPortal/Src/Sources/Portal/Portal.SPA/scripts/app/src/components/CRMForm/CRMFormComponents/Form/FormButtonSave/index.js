import React, { Component } from 'react'

export default class FormButtonSave extends Component {
    render() {
        var infoForBtnSave = this.props.infoForBtnSave;
        let entityName = infoForBtnSave.entityName;

        var nonSaveEntities = [
            'ddsm_site',
            'ddsm_project',
            'ddsm_measure'
        ];

        if(nonSaveEntities.includes(entityName)) {
            return (<div></div>)
        }

        return (
            <div id='crm-btn-save' className='row'>
                <button className='k-button' onClick={this.props.btnSaveHandler(infoForBtnSave) }
                    type='button' >
                    <i className='k-font-icon k-i-checkmark'></i>
                    <span> Save</span>
                </button>
            </div>
        );
    }
}