import GlobalLoader from '~/components/GlobalLoader'
import customStore from '~/customStore'
const $ = window.$;

export default function (login, password, applicationId) {
    const loader = new GlobalLoader();
    loader.show();

    const formData = {
        Login: login,
        Password: password
    };

    $.ajax({
        url: '/api/auth/login',
        method: 'POST',
        data: formData,
        success: function () {
            customStore.set('login', login);

            setTimeout(function () {
                loader.hide();
                window.location.replace(`/form/${applicationId}/ddsm_application`);
            }, 3000);            
        },
        error: function () {
            console.error('cant login after application creation');
            loader.hide();
        }
    });
}