import React, { Component } from 'react'
import btnSaveHandler from '~/components/CRMForm/CRMFormComponents/Form/FormButtonSave/btnSaveHandler.js'
import GlobalLoader from '~/components/GlobalLoader'
import { browserHistory } from 'react-router'

const $ = window.$;

export default class FormButtonSaveAndApprove extends Component {
    render() {
        const infoForButtonApprove = this.props.infoForButtonApprove;
        const entityLogicalName = infoForButtonApprove.entityLogicalName;
        this.entityLogicalName = entityLogicalName;
        let text = this.props.text; 

        if(!text) {
            text = 'Request to Approve';
        }

        this.needToCreateDropDown = false;

        const onClick = (ev) => {
            ev.preventDefault();

            const infoForBtnSave = {
                entityName: entityLogicalName,
                action: '/api/entity',
                method: 'POST'
            };

            const loader1 = new GlobalLoader();
            loader1.show();

            btnSaveHandler(infoForBtnSave, null, function (id) {
                const mtVal = $('select[name="ddsm_measuretemplateselectorforapproval.Id"]').val();
                if(!mtVal || !id) {
                    return;
                }

                const dataToSent = {
                    MeasureTemplateSelectorForApproval: mtVal,
                    entityId: id,
                    entityLogicalName: entityLogicalName
                };

                let approveUrl = '/api/approve/approveModelNumber';

                $.post(approveUrl, dataToSent)
                    .fail(err => console.error(err))
                    .always(() => {
                         loader1.hide();
                         browserHistory.push(`/form/${id}/${entityLogicalName}`);
                    });

            }, function (err) {
                console.error(err);
            }, false)();
        }

        return (
            <div id='crm-btn-approve-and-save-wrapper'>
                <button className='k-button' onClick={onClick}>
                    {text}
                </button>
            </div>
        );
    }
}