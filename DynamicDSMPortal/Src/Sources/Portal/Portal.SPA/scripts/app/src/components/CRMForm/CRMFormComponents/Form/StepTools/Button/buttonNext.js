import React, { Component } from 'react'

const $ = window.$;

export default class ButtonNext extends Component {
    render() {
        const currentTab = this.props.currentTab;
        const tabsCount = this.props.tabCount;
        let className = 'k-button next-step-button';
        if (currentTab +1 >= tabsCount) {
            className += ' limited-step-button'
            $('#form-viewer>#crm-btn-save').show(500);
        }else{
            $('#form-viewer>#crm-btn-save').hide();
        }
        return (<button className = {className} onClick={this.props.onClick}>Next</button>)
    }
}