import React, { Component } from 'react'

export default class ButtonPrev extends Component{
    render(){
        const currentTab = this.props.currentTab;
        let className = 'k-button previous-step-button';
        if(currentTab === 0){
            className += ' limited-step-button';        
        }
        return <button className = {className} onClick ={this.props.onClick}>Previous</button>
    }
}
