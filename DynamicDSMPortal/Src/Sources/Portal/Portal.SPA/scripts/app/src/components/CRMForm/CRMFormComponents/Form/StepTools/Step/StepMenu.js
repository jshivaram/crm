const commonClassName = 'step-element';
const currentElementClassName = `current-element-step ${commonClassName}`;
const doneStepElementClassName = `done-step ${commonClassName}`;
const unDoneStepElementClassName = `undone-step ${commonClassName}`;
const $ = window.$;
const Math = window.Math;

import _ from 'lodash'
import Step from './Step'
import React, { Component } from 'react'

export default class StepMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stepCount: +this.props.stepCount,
            currentStep: +this.props.currentStep
        };
    }

    render() {
        const stepsCount = this.props.stepCount;
        const currentStep = this.props.currentStep;
        
        const steps = (_.range(0, stepsCount)).map((el, i) => {
            if (i === currentStep) {
                return <Step stepClass={currentElementClassName} key={i} />
            } else if (i < currentStep) {
                return <Step stepClass={doneStepElementClassName} key={i} />
            } else if (i > currentStep) {
                return <Step stepClass={unDoneStepElementClassName} key={i} />
            }
        });

        return (
            <div className='stepper'>
                {
                    steps
                }
            </div>
        );
    }

    componentDidMount(){
        let widthPercent = 100 / this.state.stepCount;
        widthPercent = Math.floor(widthPercent);
        
        $(`.${commonClassName}`).css('width', `${widthPercent}%`);
    }

}


