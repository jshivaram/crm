//const activeTabClass = 'tab-is-active';

import React, { Component } from 'react'
import CRMFormComponents from '../../../../CRMFormComponents'

export default class Tabber extends Component {
    render() {
        //const activeTabNumber = this.props.activeTabNumber;
        const data = this.props.data;

        const createConvertedTabs = () => {
            return data.tabs.map((tab) => {
                
                    return <CRMFormComponents.Tab
                        tabClass={'tab'}
                        entity={data.entity}
                        tab={tab} key={tab['@id']}
                        data={data.enityAttributes}
                        objectAttributes={data.objectAttributes}
                        canEditFields={data.canEditFields}
                        formattedValues={data.formattedValues}
                        dateFormat={data.dateFormat}
                    >
                    </CRMFormComponents.Tab>
                
            });
        };


        return <div className='tabber'>
            {createConvertedTabs()}
        </div>
    }

    shouldComponentUpdate(){
        return false;
    }

}



