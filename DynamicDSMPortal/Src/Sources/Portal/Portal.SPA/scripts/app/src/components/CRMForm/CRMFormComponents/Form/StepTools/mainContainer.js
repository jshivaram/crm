import StepMenu from './Step/StepMenu'
import Tabber from './Tab/TabMenu'
import ButtonPrev from './Button/buttonPrev'
import ButtonNext from './Button/buttonNext'
import React, { Component } from 'react'

const $ = window.$;

export default class MainContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tabCount: +this.props.tabCount,
            currentTab: +this.props.currentTab
        };
    }

    render() {
        return (
            <div className={'step-container'}>
                <StepMenu stepCount={this.state.tabCount} currentStep={this.state.currentTab} />
                <Tabber tabCount={this.state.tabCount} activeTabNumber={this.state.currentTab} data={this.props.data} />
                <div className={'button-container'}>
                    <ButtonPrev onClick={this.prevTabWrapper()} currentTab={this.state.currentTab} />
                    <ButtonNext onClick={this.nextTabWrapper()} currentTab={this.state.currentTab} tabCount={this.state.tabCount} />
                </div>

            </div>
        );

    }

    nextTabWrapper() {
        let self = this;

        return function (e) {
            e.preventDefault();

            let tabCount = self.state.tabCount;
            let currentTab = self.state.currentTab;

            if (tabCount > currentTab + 1) {
                self.setState({ currentTab: currentTab + 1 });
            }
            
        }
    }
    prevTabWrapper() {
        let self = this;

        return function (e) {
            e.preventDefault();

            let currentTab = self.state.currentTab;
            if (0 < currentTab) {
                self.setState({ currentTab: currentTab - 1 });
            }
            
        }
    }
    showSaveButtons(currentTabNumber, tabsCount) {
        const selector = '.crm-form-header .crm-form-save-button';

        if (currentTabNumber + 1 === tabsCount) {
            $(selector).show('fast');
        } else {
            $(selector).hide();
        }
    }

    componentDidMount() {

        $('.portal-tab.row.tab').removeClass('tab-is-active');
        $('.portal-tab.row.tab').eq(this.state.currentTab).addClass('tab-is-active');
        $('.crm-form-save-button').hide();
    }
    componentDidUpdate() {
        $('.portal-tab.row.tab').removeClass('tab-is-active');
        $('.portal-tab.row.tab').eq(this.state.currentTab).addClass('tab-is-active');
        this.showSaveButtons(this.state.currentTab, this.state.tabCount);
    }
}