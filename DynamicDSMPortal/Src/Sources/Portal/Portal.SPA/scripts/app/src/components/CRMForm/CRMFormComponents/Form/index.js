import React, { Component } from 'react'
import FormButtonClose from './FormButtonClose'
import FormButtonSave from './FormButtonSave'
import CRMFormComponents from '../../CRMFormComponents'
import { browserHistory } from 'react-router'
import Loader from '~/components/Loader'
import ProcessBar from '../../CRMFormControls/ProcessBar'
import './style.scss'

const $ = window.$;

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();

    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });

    return o;
};

export default class Form extends Component {
    render() {

        let data = this.props.data;

        if (!data) {
            return (<div></div>);
        }

        if (!data.infoForBtnSave) {
            data.infoForBtnSave = {};
        }

        data.infoForBtnSave.entityName = data.entity.Name;

        if (!Array.isArray(data.tabs)) {
            let tab = data.tabs;
            return (
                <form id='form-viewer'>
                    <input type='hidden' value={data.entity ? data.entity.Id : ''} name='id' />

                    <ProcessBar
                        entityId={data.entity.Id}
                        enityAttributes={data.enityAttributes}
                        formattedValues={data.formattedValues}
                        />

                    <div className='row'>
                        <div className='col-md-6'>
                            <FormButtonSave infoForBtnSave={data.infoForBtnSave}
                                btnSaveHandler={this.btnSaveHandler} />
                        </div>
                        <div className='col-md-6'>
                            <FormButtonClose infoForBtnClose={data.entity.Name}
                                btnCloseHandler={this.btnCloseHandler} />
                        </div>
                    </div>

                    <CRMFormComponents.Tab
                        entity={data.entity}
                        tab={tab} key={tab['@id']}
                        data={data.enityAttributes}
                        objectAttributes={data.objectAttributes}>
                    </CRMFormComponents.Tab>

                    <FormButtonSave infoForBtnSave={data.infoForBtnSave} btnSaveHandler={this.btnSaveHandler} />
                </form>
            );
        }

        var tabNodes = data.tabs.map(tab => {
            return (
                <CRMFormComponents.Tab
                    entity={data.entity}
                    tab={tab} key={tab['@id']}
                    data={data.enityAttributes}
                    objectAttributes={data.objectAttributes}>
                </CRMFormComponents.Tab>
            );
        });

        return (
            <form id='form-viewer'>
                <input type='hidden' value={data.entity ? data.entity.Id : ''} name='id' />

                <ProcessBar
                    entityId={data.entity.Id}
                    enityAttributes={data.enityAttributes}
                    formattedValues={data.formattedValues}
                    />

                <div className='row'>

                    <div className='col-md-6'>
                        <FormButtonSave infoForBtnSave={data.infoForBtnSave}
                            btnSaveHandler={this.btnSaveHandler} />
                    </div>
                    <div className='col-md-6'>
                        <FormButtonClose infoForBtnClose={data.entity.Name}
                            btnCloseHandler={this.btnCloseHandler} />
                    </div>
                </div>

                {tabNodes}
                <FormButtonSave infoForBtnSave={data.infoForBtnSave} btnSaveHandler={this.btnSaveHandler} />
            </form>
        );
    }
    btnCloseHandler(entityName) {
        return function (e) {
            e.preventDefault();

            browserHistory.push(`/entities/${entityName}`);
        }
    }
    btnSaveHandler(infoForBtnSave) {
        return function (e) {
            e.preventDefault();

            removeNamesFromQuickFormControls();

            saveSubgridsData();

            var formData = $('#form-viewer').serializeObject();
            formData.LogicalName = infoForBtnSave.entityName;

            var keys = Object.keys(formData);

            keys.forEach(key => {

                if (formData[key] === '') {
                    delete formData[key];
                    return;
                }

                if (isDateInput(key)) {
                    formData[key] = (new Date(formData[key])).toISOString();
                }

                let i = key.indexOf('.');
                if (i !== -1) {
                    let keyForBase = key.substring(0, i);
                    let newKey = key.substring(i + 1);
                    let val = formData[key];

                    delete formData[key];

                    if (!formData.hasOwnProperty(keyForBase)) {
                        formData[keyForBase] = {};
                    }

                    formData[keyForBase][newKey] = val;
                }
            });

            var jsonEntityData = JSON.stringify(formData);

            var logicalName = infoForBtnSave.entityName;

            var data = {
                JsonEntityData: jsonEntityData,
                LogicalName: logicalName
            };

            data = JSON.stringify(data);

            Loader.show($('#loader'), $('#wrapper'));

            let isPost = infoForBtnSave.method.toLowerCase() === 'post';
            let urlForCreateOrUpdate = infoForBtnSave.action + (isPost ? '/create' : '/update');

            $.ajax({
                contentType: 'application/json',
                dataType: 'json',
                url: urlForCreateOrUpdate,
                method: infoForBtnSave.method,
                data: data,
                success: (entityId) => {
                    Loader.hide($('#loader'), $('#wrapper'));
                    if (entityId) {
                        if (infoForBtnSave.isApplicationForUnreg) {
                            browserHistory.push('/');
                            return;
                        }
                        browserHistory.push(`/form/${entityId}/${logicalName}`);
                    } else if (!isPost) {
                        browserHistory.push(`/form/${formData.id}/${logicalName}`);
                    }
                },
                error: err => {
                    console.log(err);

                    if (err.statusText === 'Not enough rights for edit') {
                        showErrorAlert(err.statusText);
                    }
                }
            });
        }
    }
}

function saveSubgridsData() {

    if (window.location.href.indexOf('/form/new/') !== -1) {
        return;
    }

    let $grids = $('div.k-grid');
    if ($grids.length === 0) {
        return;
    }

    $.each($grids, (i, grid) => {
        let gridData = $(grid).data('kendoGrid');
        if (!gridData) {
            return;
        }

        let gridDataSource = gridData.dataSource;
        if (!gridDataSource || !gridDataSource.hasChanges()) {
            return;
        }

        gridDataSource.sync();
    });
}

function removeNamesFromQuickFormControls() {
    let $quickFormInputs = $('.quick-form input');

    $.each($quickFormInputs, function (i, input) {
        let $input = $(input);
        $input.removeAttr('name');
    });
}

function isDateInput(fieldName) {
    const $input = $(`input[name="${fieldName}"]`);
    if ($input.length === 0) {
        return false;
    }

    return $input.hasClass('date-time-control');
}

function showErrorAlert(errText) {
    const notification = $('#notification').data('kendoNotification');

    if (!notification) {
        return;
    }

    notification.show({
        title: 'Error',
        message: errText,
    }, 'error');
}