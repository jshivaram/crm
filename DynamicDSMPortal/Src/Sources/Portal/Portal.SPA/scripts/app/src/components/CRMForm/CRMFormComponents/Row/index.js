import React, { Component } from 'react'
import CRMFormComponents from '../../CRMFormComponents'
import guid from '~/helpers/guid'

export default class Row extends Component {
    render() {
        let cells = [];

        if (!Array.isArray(this.props.cell)) {
            cells.push(this.props.cell);
        } else {
            cells = this.props.cell;
        }

        cells = cells.filter(c => c);

        const cellSize = calculateCellMdSize(cells.length);

        const controls = cells.map((c, i) => {
            return (
                <div key={guid()} className={'crm-portal-cell ' + formCellClassName(i, cellSize)}>
                    <CRMFormComponents.Control
                        entity={this.props.entity}
                        cell={c}
                        key={guid() + i}
                        data={this.props.data}
                        objectAttributes={this.props.objectAttributes}
                        dashboard={this.props.dashboard}
                        >
                    </CRMFormComponents.Control>
                </div>
            );
        });

        return (
            <div key={guid()} className='crm-form-row row'>
                {controls}
            </div>
        );
    }
}

function formCellClassName(index, cellSize) {
    if (index === 0) {
        return 'col-md-' + cellSize.size;
    }

    return 'col-md-' + cellSize.size + ' col-md-offset-' + cellSize.offset;
}

function calculateCellMdSize(cellsCount) {

    if (cellsCount === 2) {
        return {
            size: 5,
            offset: 1
        };
    }

    if (cellsCount === 3) {
        return {
            size: 3,
            offset: 1
        };
    }

    if (cellsCount === 4) {
        return {
            size: 2,
            offset: 1
        };
    }

    return {
        size: 12,
        offset: 0
    };
}