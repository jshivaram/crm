import React, { Component } from 'react'
import CRMFormComponents from '../../CRMFormComponents'
import guid from '~/helpers/guid'
import CRMFormControls from '../../CRMFormControls'

export default class Section extends Component {
    render() {

        if (!this.props.section.rows.row) {
            return (
                <div key={guid()}></div>
            );
        }

        if (!Array.isArray(this.props.section.rows.row)) {
            let cell = this.props.section.rows.row.cell;

            if (Array.isArray(cell)) {
                return (
                    <CRMFormComponents.Row
                        entity={this.props.entity}
                        cell={cell}
                        key={guid()}
                        data={this.props.data}
                        objectAttributes={this.props.objectAttributes}
                        dashboard={this.props.dashboard}
                        >
                    </CRMFormComponents.Row>
                );
            }

            return (
                <div key={guid()} ref='sectionRef' className='portal-section well row'>
                    {
                        this.props.section['@showlabel'] === 'true' ?
                            <CRMFormControls.Label
                                key={guid()}
                                label={this.props.section.labels.label}
                                className='portal-section-label'
                                />
                            : false
                    }
                    <CRMFormComponents.Control
                        key={guid()}
                        entity={this.props.entity}
                        cell={cell} key={cell['@id']}
                        data={this.props.data}
                        objectAttributes={this.props.objectAttributes}
                        dashboard={this.props.dashboard}
                        >
                    </CRMFormComponents.Control>
                </div>
            );
        }

        let rows = this.props.section.rows.row.filter(e => e);
        let rowComponentList = rows.map(row => {
            return (
                <CRMFormComponents.Row
                    entity={this.props.entity}
                    cell={row.cell}
                    key={guid()}
                    data={this.props.data}
                    objectAttributes={this.props.objectAttributes}
                    dashboard={this.props.dashboard}
                    >
                </CRMFormComponents.Row>
            );
        });

        return (
            <div key={guid()} ref='sectionRef' className='portal-section well row'>
                {
                    this.props.section['@showlabel'] === 'true' ?
                        <CRMFormControls.Label
                            key={guid()}
                            label={this.props.section.labels.label}
                            className='portal-section-label'
                            />
                        : false
                }
                <div key={guid()} className='rows-wrapper portal-control-list row'>
                    {rowComponentList}
                </div>
            </div>
        );

        // var cells = rows.map(el => {
        //     return el.cell;
        // });


        // cells = cells.filter(c => c);

        // return (
        //     <div ref='sectionRef' className='portal-section well row'>
        //         {
        //             this.props.section['@showlabel'] === 'true' ?
        //                 <CRMFormControls.Label
        //                     label={this.props.section.labels.label}
        //                     className='portal-section-label'
        //                     />
        //                 : false
        //         }
        //         <CRMFormComponents.ControlList
        //             entity={this.props.entity}
        //             cells={cells}
        //             data={this.props.data}
        //             objectAttributes={this.props.objectAttributes} />
        //     </div>
        // );
    }
}