import React, { Component } from 'react'
import CRMFormComponents from '../../CRMFormComponents'
import CRMFormControls from '../../CRMFormControls'

export default class Tab extends Component {
    componentDidMount() {
        this.refs.tabRef.setAttribute('tabId', this.props.tab['@id']);
        this.refs.tabRef.setAttribute('tabName', this.props.tab['@name']);
    }
    render() {
        var style = {
            display: this.props.tab['@visible'] === 'false' ? 'none' : 'block'
        };
        return (
            <div ref='tabRef' className='portal-tab row'
                style={style}>
                {
                    this.props.tab['@showlabel'] === 'true' ?
                    <CRMFormControls.Label 
                        label={this.props.tab.labels.label}
                        className='portal-tab-label' 
                    />
                    : false
                }
                <CRMFormComponents.ColumnList 
                    entity={this.props.entity} 
                    columns={this.props.tab.columns.column} 
                    data={this.props.data}
                    objectAttributes={this.props.objectAttributes}
                    dashboard={this.props.dashboard}
                    >
                </CRMFormComponents.ColumnList>
            </div>
        );
    }
}