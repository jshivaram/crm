import Column from './Column';
import ColumnList from './ColumnList';
import Control from './Control';
import ControlList from './ControlList';
import Section from './Section';
import Form from './Form';
import Tab from './Tab';
import Row from './Row';

export default {
    Column: Column,
    ColumnList: ColumnList,
    Control: Control,
    ControlList: ControlList,
    Section: Section,
    Form: Form,
    Tab: Tab,
    Row: Row
};