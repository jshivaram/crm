import React, { Component } from 'react';
import guid from '~/helpers/guid';
const $ = window.$;

export default class ApplicationMeasureTemplatesGridControl extends Component {
    render() {
        this.gridId = guid();
        $('textarea[name=ddsm_measuretemplatedata]').removeAttr('disabled');

        return (
            <div className='ApplicationMeasureTemplatesGridControl' id={this.gridId}>
            </div>
        );
    }

    componentDidMount() {
        renderGrid(this.gridId);
    }
}

function renderGrid(id, data = []) {

    $('#' + id).kendoGrid({
        dataSource: {
            data: data,
            schema: {
                model: {
                    fields: {
                        Id: { type: 'string' },
                        KwhSavingUnit: { type: 'number' },
                        IncentiveUnit: { type: 'number' },
                        Qty: { type: 'number' }
                    }
                }
            },
            pageSize: 5,
            batch: true,
            transport: {// custom transport to serialize the value in hidden input elements
                read: function (options) { // this is only for the sample you should be able to use original read
                    let dataFromHiddenInput = getDataFromHiddenInput();

                    if (!dataFromHiddenInput || dataFromHiddenInput.length === 0) {
                        options.success([]);
                        return;
                    }

                    getMeasureTemplatesData(dataFromHiddenInput, function (data) {
                        options.success(data);
                    });
                },
                create: function (options) {
                    console.log(options);
                    writeDataToHiddenInput(options.data.models);
                    //$("input#created").val(kendo.stringify(options.data.models));
                },
                update: function (options) {
                    console.log(options);
                    //$("input#updated").val(kendo.stringify(options.data.models));
                },
                destroy: function (options) {
                    console.log(options);
                    //$("input#destroyed").val(kendo.stringify(options.data.models));
                }
            },
        },
        toolbar: ['create', 'save', 'cancel'],
        sortable: true,
        pageable: {
            input: true,
            numeric: false,
            refresh: true
        },
        scrollable: false,
        columns: [
            {
                field: 'Measure',
                title: 'Measure Name',
                editor: getMeasureLookupEditor(id),
                template: data => data.Measure ? data.Measure.Name : ''
            },
            { field: 'KwhSavingUnit', title: 'kWh Savings/Unit' },
            { field: 'IncentiveUnit', title: 'Incentive/Unit' },
            { field: 'Qty', title: 'Quantity' },
            { field: 'Id', hidden: true },
            { command: 'destroy', title: '&nbsp;', width: 150 }
        ],
        editable: true
    });
}

function isJsonString(str) {
    if (/^[\],:{}\s]*$/.test(str.replace(/\\["\\\/bfnrtu]/g, '@').
        replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
        replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

        return true;
    } else {
        return false;
    }
}

function getMeasureLookupEditor(gridId) {
    return function measureLookupEditor(container, options) {

        let url = '/api/entity/GetEntitiesForLookup?entityLogicalName=ddsm_measuretemplate';
        debugger;

        let projectTemplateId = $('input[name="ddsm_projecttemplateid.Id"]').val();
        if (projectTemplateId) {
            url = '/api/application/GetMeasureTemplatesForLookup?projectTemplateId=' + projectTemplateId;
        }

        console.log('url');
        console.log(url);

        $('<select name="' + options.field + '"></select>')
            .appendTo(container)
            .kendoDropDownList({
                autoBind: false,
                dataTextField: 'Text',
                dataValueField: 'Value',
                dataSource: {
                    transport: {
                        read: {
                            dataType: 'json',
                            url: url,
                            type: 'GET'
                        }
                    }
                },
                select: function (e) {
                    let dataItem = this.dataItem(e.item);
                    let resultObject = {
                        Id: dataItem.Value,
                        Name: dataItem.Text
                    };
                    options.model[options.field] = resultObject;

                    let gridData = $('#' + gridId).data('kendoGrid');
                    gridData.refresh();
                }
            });
    }
}

function getDataFromHiddenInput() {
    let jsonData = $('textarea[name=ddsm_measuretemplatedata]').val();
    if (!jsonData || !isJsonString(jsonData)) {
        return [];
    }

    let data = JSON.parse(jsonData);
    return data;
}

function getMeasureTemplatesData(dataFromHiddenInput, success, error) {
    let idList = dataFromHiddenInput.map(a => a.Id);
    let idListStr = '';

    idList.forEach((id, i) => idListStr += `idList[${i}]=${id}&`);
    idListStr = idListStr.substring(0, idListStr.length - 1);

    return $.get('/api/Application/GetMeasureTemplates?' + idListStr)
        .done(result => {
            if (!result || !result.length) {
                return;
            }

            let dataForGrid = result.map(d => {
                let measureTemplateFromJson = dataFromHiddenInput.filter(m => m.Id.toLowerCase() === d.Id.toLowerCase())[0];
                let qty = measureTemplateFromJson.Qty;

                return {
                    Id: d.Id,
                    Measure: {
                        Name: d.Name,
                        Id: d.Id
                    },
                    KwhSavingUnit: d.KwhSavingUnit,
                    IncentiveUnit: d.IncentiveUnit,
                    Qty: qty
                };
            });
            success(dataForGrid);
        })
        .fail(() => error('Error in measure template grid in application(when get info from portal)'));
}

function writeDataToHiddenInput(modelsFromOption) {
    if (!modelsFromOption) {
        return;
    }

    let data = modelsFromOption.map(el => {
        return {
            Id: el.Measure.Id,
            Name: el.Measure.Name,
            Qty: el.Qty,
            Deleted: false
        };
    });

    let jsonData = JSON.stringify(data);
    $('textarea[name=ddsm_measuretemplatedata]').val(jsonData);
}