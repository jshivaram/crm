export function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

export function weekHelper(date) {
    let tempDate = new Date(date.getTime());
    tempDate.setHours(0, 0, 0, 0);
    // Thursday in current week decides the year.
    tempDate.setDate(tempDate.getDate() + 3 - (tempDate.getDay() + 6) % 7);
    // January 4 is always in week 1.
    var week1 = new Date(tempDate.getFullYear(), 0, 4);
    return 1 + Math.round(((tempDate.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
}

function quarterHelper(month) {
    let quarter;
    if (month == 0 || month == 1 || month == 2) {
        quarter = 0;
    } else if (month == 3 || month == 4 || month == 5) {
        quarter = 1;
    } else if (month == 6 || month == 7 || month == 8) {
        quarter = 2;
    } else {
        quarter = 3;
    }
    return quarter;
}

export function getValuesByDate(records, dateGrouping, textName, textValue) {
    let tempFieldArray = [];
    switch (dateGrouping) {
        case 'day':
            tempFieldArray = records.filter(function (record) {
                let date = new Date(record[textName]);
                let day = date.getDate();
                if (!isNaN(day)) {
                    return day === textValue;
                } else {
                    return textValue === '';
                }
            });
            break;
        case 'week':
            tempFieldArray = records.filter(function (record) {
                let date = new Date(record[textName]);
                let week = weekHelper(date);
                if (!isNaN(date.getDate())) {
                    return week === textValue;
                } else {
                    return textValue === '';
                }
            });
            break;
        case 'month':
            tempFieldArray = records.filter(function (record) {
                let date = new Date(record[textName]);
                let month = date.getMonth();
                if (!isNaN(month)) {
                    return month === textValue;
                } else {
                    return textValue === '';
                }
            });
            break;
        case 'year':
            tempFieldArray = records.filter(function (record) {
                let date = new Date(record[textName]);
                let year = date.getFullYear();
                if (!isNaN(year)) {
                    return year === textValue;
                } else {
                    return textValue === '';
                }
            });
            break;
        case 'quarter':
            tempFieldArray = records.filter(function (record) {
                let date = new Date(record[textName]);
                let month = date.getMonth();
                let quarter = quarterHelper(month);
                if (!isNaN(month)) {
                    return quarter === textValue;
                } else {
                    return textValue === '';
                }
            });
            break;
        case 'fiscal-period':
            tempFieldArray = records.filter(function (record) {
                let date = new Date(record[textName]);
                let month = date.getMonth();
                let quarter = quarterHelper(month);
                if (!isNaN(month)) {
                    return quarter === textValue;
                } else {
                    return textValue === '';
                }
            });
            break;
        case 'fiscal-year':
            tempFieldArray = records.filter(function (record) {
                let date = new Date(record[textName]);
                let year = date.getFullYear();
                if (!isNaN(year)) {
                    return year === textValue;
                } else {
                    return textValue === '';
                }
            });
            break;
        default:
            console.log('Unexpected error in function for getting values');
            break;
    }
    return tempFieldArray;
}

export function getDatesCategories(dateGrouping, textValueArray) {
    let dates = [];

    for (let i = 0; i < textValueArray.length; i++) {
        let date = new Date(textValueArray[i]);
        let month;
        if (!isNaN(date.getDate())) {
            switch (dateGrouping) {
                case 'day':
                    dates[i] = date.getDate();
                    break;
                case 'week':
                    dates[i] = weekHelper(date);
                    break;
                case 'month':
                    dates[i] = date.getMonth();
                    break;
                case 'year':
                    dates[i] = date.getFullYear();
                    break;
                case 'quarter':
                    month = date.getMonth();
                    dates[i] = quarterHelper(month);

                    break;
                case 'fiscal-period':
                    month = date.getMonth();
                    dates[i] = quarterHelper(month);
                    break;
                case 'fiscal-year':
                    dates[i] = date.getFullYear();
                    break;
                default:
                    console.log('Undefined dategrouping attribute');
                    break;
            }
        } else {
            dates[i] = '';
        }
    }
    textValueArray = dates.filter(onlyUnique);
    return textValueArray;
}

export function GetDisplayNamesForDates(tempArray, dateGrouping) {
    let tempArrayWithCategories = tempArray;
    for (let i = 0; i < tempArrayWithCategories.length; i++) {
        let tempDateValue = new Date(tempArrayWithCategories[i].category);
        let tempDateValue2;
        let tempDateValue3;
        let monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        if (!isNaN(tempDateValue.getDate())) {
            switch (dateGrouping) {
                case 'week':
                    tempDateValue2 = weekHelper(tempDateValue);
                    tempDateValue = tempDateValue.getFullYear();
                    tempArrayWithCategories[i].category = `Week ${tempDateValue2} of ${tempDateValue}`;
                    break;
                case 'month':
                    tempDateValue2 = monthNames[tempDateValue.getMonth()];
                    tempDateValue = tempDateValue.getFullYear();
                    tempArrayWithCategories[i].category = `${tempDateValue2} ${tempDateValue}`;
                    break;
                case 'day':
                    tempDateValue3 = tempDateValue.getDate();
                    tempDateValue2 = tempDateValue.getMonth();
                    tempDateValue2++;
                    tempDateValue = tempDateValue.getFullYear();
                    tempArrayWithCategories[i].category = `${tempDateValue2}/${tempDateValue3}/${tempDateValue}`;
                    break;
                case 'year':
                    tempDateValue = tempDateValue.getFullYear();
                    tempArrayWithCategories[i].category = `${tempDateValue}`;
                    break;
                case 'fiscal-year':
                    tempDateValue = tempDateValue.getFullYear();
                    tempArrayWithCategories[i].category = `${tempDateValue} FY`;
                    break;
            }
        } else {
            tempArrayWithCategories[i].category = '(blank)';
        }
    }
    return tempArrayWithCategories;
}