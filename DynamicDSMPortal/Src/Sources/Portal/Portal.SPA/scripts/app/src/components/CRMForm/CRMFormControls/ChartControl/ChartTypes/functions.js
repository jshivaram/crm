import { onlyUnique, getDatesCategories } from './dateHelpers.js'
import getTempFieldArray from './valuesHelper.js'

export default function getValuesForChart(records, aggregate, fieldName, categoryName, seriesName, dateFlag, dateGrouping) {
    let textValueArray = [];
    let type;
    let objectNameArray = [];

    if (aggregate !== 'count' && aggregate !== 'countcolumn' && aggregate !== 'avg') {
        records.forEach(element => {
            element[fieldName] = +element[fieldName];
        });
    } else if (aggregate === 'avg') {
        records = records.filter(function (record) {
            let notNumber = +record[fieldName];
            return record[fieldName] !== '' && !isNaN(notNumber) && record[fieldName] !== ' ';
        });
        records.forEach(element => {
            element[fieldName] = +element[fieldName];
        });
    }

    for (let i = 0; i < records.length; i++) {
        textValueArray[i] = records[i][categoryName];
        try {
            textValueArray[i] = textValueArray[i].replace(/'/g, '"');
        } catch (e) {
            textValueArray[i] = '';
        }
        try {
            if (typeof (JSON.parse(textValueArray[i])) !== 'number') {
                textValueArray[i] = JSON.parse(textValueArray[i]);
            } else {
                textValueArray[i] = textValueArray[i];
            }
        } catch (e) {
            textValueArray[i] = textValueArray[i];
        }
        type = typeof (textValueArray[i]);
        if (type === 'object') {
            try {
                objectNameArray[i] = textValueArray[i]['Name'];
                textValueArray[i] = textValueArray[i]['Id'];
            } catch (e) {
                textValueArray[i] = '';
            }
        } else {
            objectNameArray[i] = '(blank)'
        }
    }
    if (!dateFlag) {
        textValueArray = textValueArray.filter(onlyUnique);
        objectNameArray = objectNameArray.filter(onlyUnique);
    } else {
        textValueArray = getDatesCategories(dateGrouping, textValueArray);
    }

    let chartData = [];
    let values = [];
    let tempFieldArray;
    switch (aggregate) {
        case 'avg':
            for (let i = 0; i < textValueArray.length; i++) {
                let tempObject = getTempFieldArray(dateFlag, type, records, categoryName, textValueArray[i], dateGrouping, 1);
                tempFieldArray = tempObject.tempFieldArray;
                textValueArray[i] = tempObject.textValue;
                let sum = 0;
                for (let j = 0; j < tempFieldArray.length; j++) {
                    sum = sum + tempFieldArray[j][fieldName];
                }
                if (tempFieldArray.length !== 0) {
                    values[i] = sum / tempFieldArray.length;
                }
                values[i] = values[i] - (values[i] % 1);
            }
            break;
        case 'max':
            for (let i = 0; i < textValueArray.length; i++) {
                let tempObject = getTempFieldArray(dateFlag, type, records, categoryName, textValueArray[i], dateGrouping, 1);
                tempFieldArray = tempObject.tempFieldArray;
                textValueArray[i] = tempObject.textValue;
                values[i] = tempFieldArray[0][fieldName];
                for (let j = 1; j < tempFieldArray.length; j++) {
                    if (values[i] < tempFieldArray[j][fieldName]) {
                        values[i] = tempFieldArray[j][fieldName];
                    }
                }
                values[i] = values[i] - (values[i] % 1);
            }
            break;
        case 'min':
            for (let i = 0; i < textValueArray.length; i++) {
                let tempObject = getTempFieldArray(dateFlag, type, records, categoryName, textValueArray[i], dateGrouping, 1);
                tempFieldArray = tempObject.tempFieldArray;
                textValueArray[i] = tempObject.textValue;
                values[i] = tempFieldArray[0][fieldName];
                for (let j = 1; j < tempFieldArray.length; j++) {
                    if (values[i] > tempFieldArray[j][fieldName]) {
                        values[i] = tempFieldArray[j][fieldName];
                    }
                }
                values[i] = values[i] - (values[i] % 1);
            }
            break;
        case 'count':
            values = [];
            for (let i = 0; i < textValueArray.length; i++) {
                let tempObject = getTempFieldArray(dateFlag, type, records, categoryName, textValueArray[i], dateGrouping, 1);
                tempFieldArray = tempObject.tempFieldArray;
                textValueArray[i] = tempObject.textValue;
                values[i] = tempFieldArray.length;
            }
            break;
        case 'countcolumn':
            for (let i = 0; i < textValueArray.length; i++) {
                let tempObject = getTempFieldArray(dateFlag, type, records, categoryName, textValueArray[i], dateGrouping, 1);
                tempFieldArray = tempObject.tempFieldArray;
                textValueArray[i] = tempObject.textValue;
                values[i] = 0;
                for (let j = 0; j < tempFieldArray.length; j++) {
                    if (tempFieldArray[j][fieldName] !== '') {
                        values[i]++;
                    }
                }
            }
            break;
        case 'sum':
            for (let i = 0; i < textValueArray.length; i++) {
                let tempObject = getTempFieldArray(dateFlag, type, records, categoryName, textValueArray[i], dateGrouping, 1);
                tempFieldArray = tempObject.tempFieldArray;
                textValueArray[i] = tempObject.textValue;
                values[i] = 0;
                for (let j = 0; j < tempFieldArray.length; j++) {
                    values[i] = values[i] + tempFieldArray[j][fieldName];
                }
                values[i] = values[i] - (values[i] % 1);
            }
            break;
        default:
            console.log('Undefined type of aggregate function');
            break;
    }
    chartData[0] = {
        name: seriesName,
        data: values
    }
    if (type === 'object') {
        textValueArray = objectNameArray;
    }

    textValueArray.forEach(function (element) {
        if (element === '') {
            element = '(blank)';
        }
    });
    chartData[1] = textValueArray;
    return chartData;
}