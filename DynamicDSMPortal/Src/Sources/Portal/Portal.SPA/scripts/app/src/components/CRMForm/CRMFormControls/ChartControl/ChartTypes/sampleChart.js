import getValuesForChart from './functions.js'
import getTempFieldArray from './valuesHelper.js'
import { onlyUnique, getDatesCategories, GetDisplayNamesForDates } from './dateHelpers.js'

export default function getOptionsForSampleChart(records, settings) {

    function sortByDate(a, b) {
        let aa = new Date(a.category),
            bb = new Date(b.category);
        if (aa !== bb) {
            if (aa > bb) { return 1; }
            if (aa < bb) { return -1; }
        }
        return aa - bb;
    }

    let seriesMany = [];
    let categoriesArray;
    let categoryTitle;
    let aggregate;
    let seriesName;
    let fieldName;
    let categoryName;
    //Get series for any number of fields and 1 category
    if (settings.attrNameValues.length === 1) {
        categoryName = settings.attrNameValues[0];
        categoryTitle = settings.categoryTitles[0];
        let data = [];
        let dateFlag = settings.dateFlag[0];
        let dateGrouping = settings.dateGrouping[0];
        for (let i = 0; i < settings.attrFieldValues.length; i++) {
            fieldName = settings.attrFieldValues[i];
            aggregate = settings.aggregatesArray[i];
            seriesName = settings.seriesNames[i];
            data = getValuesForChart(records, aggregate, fieldName, categoryName, seriesName, dateFlag, dateGrouping);
            seriesMany[i] = data[0];
        }
        categoriesArray = data[1];
        //Get series for 1 field and 2 categories
    } else if (settings.attrNameValues.length === 2) {
        let categoryNames = settings.attrNameValues;
        categoryTitle = settings.categoryTitles[0];
        let dateFlags = settings.dateFlag;
        let dateGroupings = settings.dateGrouping;
        let textValueArray = [];
        let type;
        let objectNameArray = [];

        for (let i = 0; i < records.length; i++) {
            textValueArray[i] = records[i][categoryNames[0]];
            textValueArray[i] = textValueArray[i].replace(/'/g, '"');
            try {
                if (typeof (JSON.parse(textValueArray[i])) !== 'number') {
                    textValueArray[i] = JSON.parse(textValueArray[i]);
                } else {
                    textValueArray[i] = textValueArray[i];
                }
            } catch (e) {
                textValueArray[i] = textValueArray[i];
            }
            type = typeof (textValueArray[i]);
            if (type === 'object') {
                try {
                    objectNameArray[i] = textValueArray[i]['Name'];
                    textValueArray[i] = textValueArray[i]['Id'];
                } catch (e) {
                    textValueArray[i] = '';
                }
            } else {
                objectNameArray[i] = '(blank)';
            }
        }

        if (!dateFlags[0]) {
            textValueArray = textValueArray.filter(onlyUnique);
            objectNameArray = objectNameArray.filter(onlyUnique);
        } else {
            textValueArray = getDatesCategories(dateGroupings[0], textValueArray);
        }

        let tempObject;
        let tempRecordsArrays = [];
        for (let i = 0; i < textValueArray.length; i++) {
            tempObject = getTempFieldArray(dateFlags[0], type, records, categoryNames[0], textValueArray[i], dateGroupings[0], 2);
            tempRecordsArrays[i] = tempObject.tempFieldArray;
            textValueArray[i] = tempObject.textValue;
        }

        let data = [];
        fieldName = settings.attrFieldValues[0];
        aggregate = settings.aggregatesArray[0];
        seriesName = settings.seriesNames[0];
        for (let i = 0; i < tempRecordsArrays.length; i++) {
            data[i] = getValuesForChart(tempRecordsArrays[i], aggregate, fieldName, categoryNames[1], seriesName, dateFlags[1], dateGroupings[1]);
        }

        let objectsForSeries = [];
        for (let i = 0; i < data.length; i++) {
            objectsForSeries[i] = {
                data: data[i][0].data,
                name: data[i][1]
            };
        }

        let valuesAndNamesArray = [];
        let counter = 0;
        for (let i = 0; i < objectsForSeries.length; i++) {
            for (let j = 0; j < objectsForSeries[i].data.length; j++) {
                if (type !== 'object') {
                    valuesAndNamesArray[counter] = {
                        value: objectsForSeries[i].data[j],
                        name: objectsForSeries[i].name[j],
                        category: textValueArray[i]
                    }
                } else {
                    valuesAndNamesArray[counter] = {
                        value: objectsForSeries[i].data[j],
                        name: objectsForSeries[i].name[j],
                        category: objectNameArray[i]
                    }
                }
                counter++;
            }
        }

        let tempCategories = [];
        for (let i = 0; i < valuesAndNamesArray.length; i++) {
            tempCategories[i] = valuesAndNamesArray[i].name;
        }
        tempCategories = tempCategories.filter(onlyUnique);
        tempCategories.sort();

        for (let i = 0; i < textValueArray.length; i++) {
            let tempArrayByCategory = [];
            if (type !== 'object') {
                tempArrayByCategory = valuesAndNamesArray.filter(function (elem) {
                    return elem.category === textValueArray[i];
                });
            } else {
                tempArrayByCategory = valuesAndNamesArray.filter(function (elem) {
                    return elem.category === objectNameArray[i];
                });
            }
            for (let j = 0; j < tempCategories.length; j++) {
                let tempArrayByCategoryAndName = tempArrayByCategory.filter(function (elem) {
                    return elem.name === tempCategories[j];
                });
                if (tempArrayByCategoryAndName.length === 0) {
                    if (type !== 'object') {
                        valuesAndNamesArray[counter] = {
                            value: 0,
                            name: tempCategories[j],
                            category: textValueArray[i]
                        }
                    } else {
                        valuesAndNamesArray[counter] = {
                            value: 0,
                            name: tempCategories[j],
                            category: objectNameArray[i]
                        }
                    }
                    counter++;
                }
            }
        }

        for (let i = 0; i < tempCategories.length; i++) {
            let tempArray = valuesAndNamesArray.filter(function (elem) {
                return elem.name === tempCategories[i];
            });
            if (!dateGroupings[0]) {
                tempArray.sort((a, b) => a.category < b.category ? -1 : 1);
            } else {
                debugger;
                tempArray.sort(sortByDate);
                tempArray = GetDisplayNamesForDates(tempArray, dateGroupings[0]);
                for (let j = 0; j < tempArray.length; j++) {
                    textValueArray[j] = tempArray[j].category;
                }
            }
            let tempDataArray = [];
            for (let j = 0; j < tempArray.length; j++) {
                tempDataArray[j] = tempArray[j].value;
            }
            seriesMany[i] = {
                data: tempDataArray,
                name: tempCategories[i]
            }
        }

        if (type !== 'object') {
            categoriesArray = textValueArray;
        } else {
            categoriesArray = objectNameArray;
        }
        categoriesArray.sort();
    } else {
        console.log('cannot visualize this chart');
    }

    for (let i = 0; i < categoriesArray.length; i++) {
        if (categoriesArray[i] === '') {
            categoriesArray[i] = '(blank)';
        }
    }
    //settings for axis titles
    if (categoryTitle !== null) {
        if (settings.dateGrouping[0]) {
            let dateTitle = settings.dateGrouping[0].charAt(0).toUpperCase() + settings.dateGrouping[0].substring(1);
            categoryTitle = `${dateTitle} ( ${categoryTitle} )`
        }
    } else {
        categoryTitle = '';
    }
    let valueAxisTitle;
    if (aggregate === 'count') {
        valueAxisTitle = `Count:All (${seriesName})`;
    } else if (aggregate === 'countcolumn') {
        valueAxisTitle = `Count:Non-empty (${seriesName})`;
    } else {
        valueAxisTitle = `${aggregate.charAt(0).toUpperCase() + aggregate.substring(1)} (${seriesName})`
    }

    //settings for rotation of categories
    let rotationCategoryAxis = 0;
    if (settings.chartType === 'bar') {
        rotationCategoryAxis = -90;
    }
    if (categoriesArray.length > 3) {
        rotationCategoryAxis = -90;
        if (settings.chartType === 'bar') {
            rotationCategoryAxis = 0;
        }
    }
    let rotationValueAxis = 0;
    if (settings.chartType === 'bar') {
        rotationValueAxis = -90;
    }

    if (seriesMany.length > 15) {
        settings.legendSettings = {
            visible: false
        }
    }
    for (let i = 0; i < seriesMany.length; i++) {
        if (seriesMany[i].name === '')
            seriesMany[i].name = '(blank)';
    }


    if (settings.chartType !== 'funnel' && settings.chartType !== 'pie') {
        return {
            ChartArea: settings.chartArea,
            dataSource: {
                sort: {
                    field: `${settings.$orderAttribute}`,
                    dir: `${settings.order}`
                }
            },
            title: settings.titleSettings,
            legend: settings.legendSettings,
            seriesDefaults: {
                type: settings.chartType,
                labels: settings.labelsSettings,
                stack: settings.StackObject
            },
            series: seriesMany,
            seriesColors: settings.seriesColors,
            valueAxis: {
                labels: {
                    color: `rgb(${settings.$AxisYLabelForeColor})`,
                    font: '11px sans-serif',
                    rotation: rotationValueAxis
                },
                line: {
                    color: `rgb(${settings.$AxisYLineColor})`
                },
                axisCrossingValue: 0,
                majorGridLines: {
                    width: 1,
                    visible: true,
                    color: `rgb(${settings.$AxisYMajorGridLineColor})`
                },
                majorTicks: {
                    size: 4,
                    color: `rgb(${settings.$AxisYTickLineColor})`,
                    width: 4
                },
                title: {
                    text: valueAxisTitle,
                    color: `rgb(${settings.$AxisYTitleForeColor})`,
                    font: '14px sans-serif'
                }
            },
            categoryAxis: {
                categories: categoriesArray,
                line: {
                    visible: true
                },
                labels: {
                    color: `rgb(${settings.$AxisXLabelForeColor})`,
                    rotation: rotationCategoryAxis,
                    font: '11px sans-serif'
                },
                title: {
                    text: `${categoryTitle}`,
                    color: `rgb(${settings.$AxisXTitleForeColor})`,
                    font: '14px sans-serif'
                }
            },
            tooltip: {
                visible: true,
                format: 'N0',
                template: '#= series.name #: #= value #'
            }
        }
    } else {
        let seriesOne = [];
        for (let i = 0; i < seriesMany[0].data.length; i++) {
            seriesOne[i] = {
                value: seriesMany[0].data[i],
                category: `${categoriesArray[i]}`
            }
        }
        if (settings.chartType === 'funnel') {
            settings.labelsSettings = undefined;
        }
        return {
            ChartArea: settings.chartArea,
            dataSource: {
                sort: {
                    field: `${settings.$orderAttribute}`,
                    dir: `${settings.order}`
                }
            },
            title: settings.titleSettings,
            legend: settings.legendSettings,
            seriesDefaults: {
                labels: settings.labelsSettings
            },
            series: [{
                type: settings.chartType,
                data: seriesOne
            }],
            seriesColors: settings.seriesColors,
            tooltip: {
                visible: true,
                format: 'N0',
                template: '#= category #: #= value #'
            }
        }
    }
}