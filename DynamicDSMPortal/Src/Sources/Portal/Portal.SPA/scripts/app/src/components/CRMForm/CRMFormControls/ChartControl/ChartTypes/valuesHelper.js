import { getValuesByDate, weekHelper } from './dateHelpers.js'

export default function getTempFieldArray(dateFlag, type, records, categoryName, textValue, dateGrouping, length) {
    let tempObject = {};
    let tempFieldArray;
    if (!dateFlag) {
        if (type !== 'object') {
            tempFieldArray = records.filter(function (record) {
                return record[categoryName] === textValue;
            });
        } else {
            tempFieldArray = records.filter(function (record) {
                record = record[categoryName];
                record = record.replace(/'/g, '"');
                try {
                    record = JSON.parse(record);
                    record = record['Id'];
                } catch (e) {
                    record = '';
                }
                return record === textValue;
            });
        }
    } else {
        tempFieldArray = getValuesByDate(records, dateGrouping, categoryName, textValue);
        if (dateGrouping !== 'year' && dateGrouping !== 'fiscal-year' && dateGrouping !== 'day') {
            textValue++;
        }
        let tempDateValue = new Date(tempFieldArray[0][categoryName]);
        if (!isNaN(tempDateValue.getDate())) {
            let tempDateValue2;
            let tempDateValue3;
            if (length === 2) {
                switch (dateGrouping) {
                    case 'quarter':
                        tempDateValue = tempDateValue.getFullYear();
                        textValue = `${tempDateValue}/${textValue} ${dateGrouping}`;
                        break;
                    case 'week':
                        tempDateValue3 = tempDateValue.getDate();
                        tempDateValue2 = tempDateValue.getMonth();
                        tempDateValue2++;
                        tempDateValue = tempDateValue.getFullYear();
                        textValue = `${tempDateValue2}/${tempDateValue3}/${tempDateValue}`;
                        break;
                    case 'month':
                        tempDateValue = tempDateValue.getFullYear();
                        textValue = `${textValue}/1/${tempDateValue}`;
                        break;
                    case 'day':
                        tempDateValue2 = tempDateValue.getMonth();
                        tempDateValue2++;
                        tempDateValue = tempDateValue.getFullYear();
                        textValue = `${tempDateValue2}/${textValue}/${tempDateValue}`;
                        break;
                    case 'year':
                        tempDateValue = tempDateValue.getFullYear();
                        textValue = `1/1/${tempDateValue}`;
                        break;
                    case 'fiscal-period':
                        tempDateValue = tempDateValue.getFullYear();
                        textValue = `${tempDateValue}/${textValue} fiscal-period`;
                        break;
                    case 'fiscal-year':
                        tempDateValue = tempDateValue.getFullYear();
                        textValue = `1/1/${tempDateValue}`;
                        break;
                }
            } else if (length === 1) {
                let monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                switch (dateGrouping) {
                    case 'week':
                        tempDateValue2 = weekHelper(tempDateValue);
                        tempDateValue = tempDateValue.getFullYear();
                        textValue = `Week ${tempDateValue2} of ${tempDateValue}`;
                        break;
                    case 'month':
                        tempDateValue2 = monthNames[tempDateValue.getMonth()];
                        tempDateValue = tempDateValue.getFullYear();
                        textValue = `${tempDateValue2} ${tempDateValue}`;
                        break;
                    case 'day':
                        tempDateValue3 = tempDateValue.getDate();
                        tempDateValue2 = tempDateValue.getMonth();
                        tempDateValue2++;
                        tempDateValue = tempDateValue.getFullYear();
                        textValue = `${tempDateValue2}/${tempDateValue3}/${tempDateValue}`;
                        break;
                    case 'year':
                        tempDateValue = tempDateValue.getFullYear();
                        textValue = `${tempDateValue}`;
                        break;
                    case 'fiscal-year':
                        tempDateValue = tempDateValue.getFullYear();
                        textValue = `${tempDateValue} FY`;
                        break;
                    case 'fiscal-period':
                        tempDateValue = tempDateValue.getFullYear();
                        textValue = `Quarter ${textValue} FY${tempDateValue}`;
                        break;
                }
            }
        } else {
            textValue = '(blank)';
        }
    }
    tempObject.tempFieldArray = tempFieldArray;
    tempObject.textValue = textValue;
    return tempObject;
}