import React, { Component } from 'react'
import guid from '~/helpers/guid'
import getOptionsForSampleChart from './ChartTypes/sampleChart.js'
const $ = window.$;

export default class ChartControl extends Component {
    render() {
        this.id = guid();
        return (
            <div id={this.id} />
        );
    }

    componentDidMount() {
        let self = this;
        let control = this.props.control;
        let relatedEntityName = control.parameters.TargetEntityType;
        let count = 1000;
        let viewId = control.parameters.ViewId;
        let visualizationId = control.parameters.VisualizationId;
        //Special for chart 6 on Customer Service Perfomance Dashboard
        if (Array.isArray(viewId) && relatedEntityName === 'incident') {
            viewId = viewId[1];
        }
        if (Array.isArray(visualizationId) && relatedEntityName === 'incident') {
            visualizationId = visualizationId[1];
        }

        if (viewId && visualizationId && !Array.isArray(viewId) && !Array.isArray(visualizationId)) {
            viewId = viewId.replace('{', '').replace('}', '').toLowerCase();
            visualizationId = visualizationId.replace('{', '').replace('}', '').toLowerCase();
            let visualizationUrl = `/api/chart/GetAll?VisualizationId=${visualizationId}&Count=${count}&EntityLogicalName=${relatedEntityName}&ViewId=${viewId}`;
            $.get(visualizationUrl)
                .done((chartData) => {
                    buildChart(chartData, self);
                })
                .fail((err) => {
                    console.log(err);
                });
        }

        function buildChart(chartData, self) {
            if (chartData.Data.length != 0) {
                let records = chartData.Data;
                //get array of attributes for visualization
                let datadescription = chartData.VisualizationData.datadescription;
                let $attrAllData = {};
                $attrAllData = $(datadescription).find('entity').each(function (idx, v) {
                    $(v).find('attribute').each(function (i, vi) {
                        $attrAllData[idx] = [];
                        $attrAllData[idx].push($(vi).text());
                    });
                });
                let attrChildNodes = $attrAllData[0].childNodes;
                let attrXmlNodes = [];
                for (var i = 0; i < attrChildNodes.length; i++) {
                    attrXmlNodes[i] = attrChildNodes[i].outerHTML;
                }
                //get array of attributes that must be treated by aggregation functions
                let attrXmlFieldNodes = [];
                attrXmlFieldNodes = attrXmlNodes.filter(function (attr) {
                    let attrXml = $.parseXML(attr);
                    let $attr = $(attrXml);
                    let attrAggregate = $attr.find('attribute').attr('aggregate');
                    return attrAggregate;
                });
                //Get attributes names
                let attrFieldValues = [];
                for (i = 0; i < attrXmlFieldNodes.length; i++) {
                    let attrXml = $.parseXML(attrXmlFieldNodes[i]);
                    let $attr = $(attrXml);
                    let attrName = $attr.find('attribute').attr('name');
                    attrFieldValues[i] = attrName;
                }
                attrFieldValues = attrFieldValues.filter(function (attr) {
                    return attr;
                });
                //get aggregates values
                let aggregatesArray = [];
                for (i = 0; i < attrXmlFieldNodes.length; i++) {
                    let attrXml = $.parseXML(attrXmlFieldNodes[i]);
                    let $attr = $(attrXml);
                    let attrName = $attr.find('attribute').attr('aggregate');
                    aggregatesArray[i] = attrName;
                }
                //get array of attributes that must be displayed
                let attrXmlNameNodes = [];
                attrXmlNameNodes = attrXmlNodes.filter(function (attr) {
                    let attrXml = $.parseXML(attr);
                    let $attr = $(attrXml);
                    let attrGroupBy = $attr.find('attribute').attr('groupby');
                    return attrGroupBy;
                });
                //Get attributes names
                let attrNameValues = [];
                for (i = 0; i < attrXmlNameNodes.length; i++) {
                    let attrXml = $.parseXML(attrXmlNameNodes[i]);
                    let $attr = $(attrXml);
                    let attrName = $attr.find('attribute').attr('name');
                    attrNameValues[i] = attrName;
                }
                attrNameValues = attrNameValues.filter(function (attr) {
                    return attr;
                });
                //get DisplayNames of metadata for series and categories and attributes for data
                let metadata = chartData.AttrMetadata;
                let seriesNameArray = [];
                let categoryTitleArray = [];
                let dateFlag = [];
                let dateGrouping = [];
                if (metadata.length != 0) {
                    for (i = 0; i < attrFieldValues.length; i++) {
                        let seriesNameArrayMeta = [];
                        seriesNameArrayMeta = metadata.filter(function (item) {
                            return item.LogicalName === attrFieldValues[i];
                        });
                        if (seriesNameArrayMeta.length > 0) {
                            seriesNameArray[i] = seriesNameArrayMeta[0].DisplayName;
                        }
                        else {
                            seriesNameArray[i] = attrFieldValues[i];
                            seriesNameArray[i] = seriesNameArray[i].replace('ddsm_', '').replace('_', ' ').replace('id', '');
                            seriesNameArray[i] = seriesNameArray[i].charAt(0).toUpperCase() + seriesNameArray[i].substring(1);
                        }
                    }
                    for (i = 0; i < attrNameValues.length; i++) {
                        let categoryTitleArrayMeta = [];
                        categoryTitleArrayMeta = metadata.filter(function (item) {
                            return item.LogicalName === attrNameValues[i];
                        });
                        if (categoryTitleArrayMeta.length > 0) {
                            categoryTitleArray[i] = categoryTitleArrayMeta[0].DisplayName;
                            if (categoryTitleArrayMeta[0].Type === 'DateTimeType') {
                                dateFlag[i] = true;
                                let attrXml = $.parseXML(attrXmlNameNodes[i]);
                                let $attr = $(attrXml);
                                let dateGroupingAttr = $attr.find('attribute').attr('dategrouping');
                                dateGrouping[i] = dateGroupingAttr;
                            } else {
                                dateFlag[i] = undefined;
                                dateGrouping[i] = undefined;
                            }
                        }
                        else {
                            categoryTitleArray[i] = attrNameValues[i];
                            categoryTitleArray[i] = categoryTitleArray[i].replace('ddsm_', '').replace('_', ' ').replace('id', '');
                            categoryTitleArray[i] = categoryTitleArray[i].charAt(0).toUpperCase() + categoryTitleArray[i].substring(1);
                        }
                    }
                }

                let settings = {};
                settings.dateFlag = dateFlag;
                settings.dateGrouping = dateGrouping;
                settings.attrFieldValues = attrFieldValues;
                settings.aggregatesArray = aggregatesArray;
                settings.attrNameValues = attrNameValues;
                settings.seriesNames = seriesNameArray;
                settings.categoryTitles = categoryTitleArray;
                let presentationdescription = chartData.VisualizationData.presentationdescription;
                let xmlPresentation = $.parseXML(presentationdescription);
                let $presentation = $(xmlPresentation);
                let title = chartData.VisualizationData.name;
                let $titleForeColor = $presentation.find('Titles Title').attr('ForeColor');
                let $legendPosition = $presentation.find('Legends Legend').attr('Docking');
                let $legendAlignment = $presentation.find('Legends Legend').attr('Alignment');
                let $legendForeColor = $presentation.find('Legends Legend').attr('ForeColor');
                settings.$AxisYLabelForeColor = $presentation.find('ChartAreas ChartArea AxisY LabelStyle').attr('ForeColor');
                settings.$AxisYLineColor = $presentation.find('ChartAreas ChartArea AxisY').attr('LineColor');
                settings.$AxisYMajorGridLineColor = $presentation.find('ChartAreas ChartArea AxisY MajorGrid').attr('LineColor');
                settings.$AxisYTickLineColor = $presentation.find('ChartAreas ChartArea AxisY MajorTickMark').attr('LineColor');
                settings.$AxisYTitleForeColor = $presentation.find('ChartAreas ChartArea AxisY').attr('TitleForeColor');
                settings.$AxisXLabelForeColor = $presentation.find('ChartAreas ChartArea AxisX LabelStyle').attr('ForeColor');
                settings.$AxisXTitleForeColor = $presentation.find('ChartAreas ChartArea AxisX').attr('TitleForeColor');
                if ($legendPosition) {
                    $legendPosition = $legendPosition.toLowerCase();
                }
                if ($legendAlignment) {
                    $legendAlignment = $legendAlignment.toLowerCase();
                }

                let fetch = chartData.ViewXml;
                fetch = $.parseXML(fetch);
                let $fetch = $(fetch);
                settings.$orderAttribute = $fetch.find('fetch entity order').attr('attribute');
                let descending = $fetch.find('fetch entity order').attr('descending');
                settings.order = 'desc';
                if (descending === 'false') {
                    settings.order = 'asc';
                }
                let $chartType = $presentation.find('Series Series').attr('ChartType');
                if ($chartType) {
                    $chartType = $chartType.toLowerCase();
                } else {
                    $chartType = 'column';
                }
                settings.chartArea = {
                    width: 800,
                    height: 600
                };
                settings.seriesColors = ['#1660e9', '#f9a839', '#e2e817', '#00ff00',
                    '#d32934', '#ff00ff', '#00ccff', '#66ffcc', '#ff6666', '#3399ff', '#9900cc'];
                settings.titleSettings = {
                    text: `${title} \n ${chartData.View.name}`,
                    position: 'top',
                    align: 'left',
                    color: `rgb(${$titleForeColor})`,
                    font: '16px sans-serif'
                };
                settings.legendSettings = {
                    position: 'bottom', //`${$legendPosition}`
                    align: `${$legendAlignment}`,
                    labels: {
                        font: '11px sans-serif',
                        color: `rgb(${$legendForeColor})`
                    }
                };
                settings.labelsSettings = {
                    position: 'outsideEnd',
                    visible: function (point) {
                        if (point.value <= 0) {
                            return false
                        }
                        else { return true }
                    },
                    background: 'transparent',
                    font: '11px sans-serif'
                };

                let options = {};
                switch ($chartType) {
                    case 'column':
                        settings.chartType = $chartType;
                        break;
                    case 'bar':
                        settings.chartType = $chartType;
                        break;
                    case 'area':
                        settings.chartType = $chartType;
                        break;
                    case 'line':
                        settings.chartType = $chartType;
                        break;
                    case 'stackedcolumn':
                        settings.chartType = 'column';
                        settings.StackObject = {};
                        settings.labelsSettings = undefined;
                        break;
                    case 'stackedcolumn100':
                        settings.chartType = 'column';
                        settings.StackObject = {
                            type: '100%'
                        };
                        settings.labelsSettings = undefined;
                        break;
                    case 'stackedbar':
                        settings.chartType = 'bar';
                        settings.StackObject = {};
                        settings.labelsSettings = undefined;
                        break;
                    case 'stackedbar100':
                        settings.chartType = 'bar';
                        settings.StackObject = {
                            type: '100%'
                        };
                        settings.labelsSettings = undefined;
                        break;
                    case 'stackedarea':
                        settings.chartType = 'area';
                        settings.StackObject = {};
                        settings.labelsSettings = undefined;
                        break;
                    case 'stackedarea100':
                        settings.chartType = 'area';
                        settings.StackObject = {
                            type: '100%'
                        };
                        settings.labelsSettings = undefined;
                        break;
                    case 'pie':
                        settings.chartType = $chartType;
                        break;
                    case 'funnel':
                        settings.chartType = $chartType;
                        break;
                    default:
                        console.log('Unknown type of chart, cannot picture this');
                        break;
                }
                options = getOptionsForSampleChart(records, settings);
                $('#' + self.id).kendoChart(options);
            }
        }
    }
}