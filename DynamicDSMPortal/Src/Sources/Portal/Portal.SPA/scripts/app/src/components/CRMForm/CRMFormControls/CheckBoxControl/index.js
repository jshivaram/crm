import React, { Component } from 'react'
import guid from '~/helpers/guid'
import Label from '../Label'
const $ = window.$;

export default class CheckBoxControl extends Component {
    render() {
        let fieldName = this.props.control['@datafieldname'];
        fieldName = fieldName ? fieldName : '';

        const id = guid();
        this.checkBoxId = id;
        this.hiddenId = guid();

        if (this.props.showlabel !== 'false' && this.props.label) {
            return (
                <div className='portal-control row'>
                    <div className='col-md-8'>
                        <Label label={this.props.label}
                            className='portal-textbox-label' />
                    </div>
                    <div className='col-md-4' key={guid()} className='checkbox-control-wrapper'>
                        <input type='checkbox' name={fieldName} id={id} className='k-checkbox' />                        
                        <label className='k-checkbox-label' htmlFor={id}></label>
                        <input type='hidden' id={this.hiddenId} name={fieldName}  defaultValue='false' />
                    </div>
                </div>
            );
        }

        return (
            <div key={guid()} className='checkbox-control-wrapper'>
                <input type='checkbox' name={fieldName} id={id} className='k-checkbox' />
                <label className='k-checkbox-label' htmlFor={id}></label>
                <input type='hidden' id={this.hiddenId} name={fieldName}  defaultValue='false' />
            </div>
        );
    }
    componentDidMount() {
        const self = this;

        $('#' + this.checkBoxId).on('click', e => {
            let $checkbox = $(e.target);
            if (!$checkbox.length) {
                return;
            }

            let val = $checkbox.val();
            debugger;
            if (val !== 'true') {
                $checkbox.val(true);
                $checkbox.prop('value', true);
                $('#' + self.hiddenId).prop('disabled', true);
            } else {
                $checkbox.val(false);
                $checkbox.prop('value', false);
                $('#' + self.hiddenId).prop('disabled', false);
            }
        })
    }
}