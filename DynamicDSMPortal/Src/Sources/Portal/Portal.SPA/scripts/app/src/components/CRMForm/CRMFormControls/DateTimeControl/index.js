import React, { Component } from 'react'
import Label from '../Label'
import guid from '~/helpers/guid'
const $ = window.$;

export default class DateTimeControl extends Component {
	render() {
		const fieldName = this.props.control['@datafieldname'];
		this.controlId = guid();

		if (this.props.showlabel !== 'false' && this.props.label) {

			if (this.props.control['@disabled'] !== 'false') {
				return (
					<div className='portal-control row'>
						<div className='col-md-4'>
							<Label label={this.props.label}
								className='portal-textbox-label' />
						</div>
						<div className='col-md-8'>
							<input
								name={fieldName}
								defaultValue={this.props.data[fieldName]}
								className='date-time-control' id={this.controlId} readOnly='readonly' />
						</div>
					</div>
				);
			}

			return (
				<div className='portal-control row'>
					<div className='col-md-4'>
						<Label label={this.props.label}
							className='portal-textbox-label' />
					</div>
					<div className='col-md-8'>
						<input
							name={fieldName}
							defaultValue={this.props.data[fieldName]}
							className='date-time-control' id={this.controlId} />
					</div>
				</div>
			);
		}

		if (this.props.control['@disabled'] !== 'false') {
			return (
				<div className='portal-control row date-time-control-wrapper'>
					<input
						defaultValue={this.props.data[fieldName]}
						className='date-time-control'
						id={this.controlId}
						readOnly='readonly' />
				</div>
			);
		}

		return (
			<div className='portal-control row date-time-control-wrapper'>
				<input
					name={fieldName}
					defaultValue={this.props.data[fieldName]}
					className='date-time-control' id={this.controlId} />
			</div>
		);
	}
	componentDidMount() {
		const fieldName = this.props.control['@datafieldname'];
		const val = this.props.data[fieldName]; 

		let date = val && !isNaN(Date.parse(val)) ? new Date(val) : new Date();

		$('#' + this.controlId).kendoDateTimePicker({
			value: date
		});
	}
}
