import React, { Component } from 'react'
import Label from '../Label'

export default class DecimalControl extends Component {
    render() {

        const fieldName = this.props.control['@datafieldname'];
        const id = this.props.control['@id'];
        const isReadonly = this.props.control['@disabled'] === 'true';

        const createInput = () => {
            return (
                <input id={id}
                    type='number'
                    className='k-textbox'
                    name={fieldName}
                    step='any'
                    defaultValue={this.props.data[fieldName]}
                    readOnly={isReadonly} />
            );
        };

        if (this.props.showlabel !== 'false' && this.props.label) {
            const divId = id + '_div';

            return (
                <div className='portal-control row' id={divId}>
                    <div className='col-md-4'>
                        <Label label={this.props.label}
                            className='portal-textbox-label' />
                    </div>
                    <div className='col-md-8'>
                        {createInput()}
                    </div>
                </div>
            );
        }

        return (
            <div className='portal-control row'>
                {createInput()}
            </div>
        );
    }
}