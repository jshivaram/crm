import React, { Component } from 'react'
import Label from '../Label'

export default class EmailAddressControl extends Component {
    render() {

        const fieldName = this.props.control['@datafieldname'];
        const isReadonly = this.props.control['@disabled'] === 'true';

        const createInput = () => {
            return (
                <input
                    className='k-textbox'
                    type='email'
                    name={this.props.control['@datafieldname']}
                    defaultValue={this.props.data[fieldName]}
                    readOnly={isReadonly} />
            );
        };

        if (this.props.showlabel !== 'false' && this.props.label) {
            return (
                <div className='portal-control row'>
                    <div className='col-md-4'>
                        <Label label={this.props.label} className='portal-email-label' />
                    </div>
                    <div className='col-md-8'>
                        {createInput()}
                    </div>
                </div>
            );
        }

        return (
            <div className='portal-control row'>
                {createInput()}
            </div>
        );
    }
}