import React, { Component } from 'react'
import ChartControl from '../ChartControl'
import guid from '~/helpers/guid'
import SpecialGridForChart from './specialGridForChart.js'

export default class GridChartSwitchControl extends Component {

    constructor(props) {
        super(props);
        if (this.props.control.parameters.ChartGridMode === 'All') {
            this.state = {
                mode: 'Grid',
                fieldDisplayName: 'None1235152',
                category: 'None623431634',
                categoryDisplayName: 'None123515',
                categoryId: 'None1235215',
                dateGrouping: 'None5361'
            };
        } else {
            this.state = {
                mode: 'Chart',
                fieldDisplayName: 'None3453154',
                category: 'None1451345',
                categoryDisplayName: 'None134535',
                categoryId: 'None55345',
                dateGrouping: 'None5361'
            };
        }
    }

    onClickWraper(self) {
        return function onClickHandler() {
            let typeOfComponent = self.state.mode;

            if (typeOfComponent === 'Grid') {
                self.setState({
                    mode: 'Chart',
                    fieldDisplayName: 'None34135',
                    category: 'None246246',
                    categoryDisplayName: 'None1345631',
                    categoryId: 'None136371376143',
                    dateGrouping: 'None5361'
                });
            } else if (typeOfComponent === 'Chart') {
                self.setState({
                    mode: 'Grid',
                    fieldDisplayName: 'None136316136',
                    category: 'None6246246245',
                    categoryDisplayName: 'None13613646',
                    categoryId: 'None1346314613',
                    dateGrouping: 'None5361'
                });
            }
        }
    }

    whenClickOnSeries(self) {
        return function ClickOnChartHandler(data) {
            let fieldDisplayName = data.sender.options.valueAxis.title.text;
            let categoryDisplayName = data.sender.options.categoryAxis.title.text;
            fieldDisplayName = fieldDisplayName
                .substring(fieldDisplayName.indexOf('(') + 1, fieldDisplayName.length)
                .replace(')', '');
            categoryDisplayName = categoryDisplayName
                .substring(categoryDisplayName.indexOf('(') + 1, categoryDisplayName.length)
                .replace(')', '');

            const categories = data.sender.options.categoryAxis.categories;
            const indexOfCategory = categories.indexOf(data.category);
            self.setState({
                mode: 'Grid',
                fieldDisplayName: fieldDisplayName,
                category: data.category,
                categoryDisplayName: categoryDisplayName,
                categoryId: data.sender.options.valueAxis.categoryIds[indexOfCategory],
                categoryNotFormattedValue: data.sender.options.valueAxis.nonFormattedValues[indexOfCategory],
                dateGrouping: data.sender.options.valueAxis.dateGrouping
            });
        }
    }

    render() {
        this.id = guid();
        this.buttonId = guid();

        let data = {};
        data.value = 1;
        let chartOrGridComponent = () => {
            let typeOfComponent = this.state.mode;
            if (typeOfComponent === 'Grid') {
                return (
                    <div>
                        <SpecialGridForChart
                            control={this.props.control}
                            label={this.props.label}
                            data={this.props.data}
                            objectAttributes={this.props.objectAttributes}
                            entity={this.props.entity}
                            showlabel={this.props.showlabel}
                            dashboard={this.props.dashboard}
                            buttonId={this.buttonId}
                            stateOf={this.state}
                            />
                    </div>);
            } else if (typeOfComponent === 'Chart') {
                return (
                    <div>
                        <ChartControl
                            control={this.props.control}
                            label={this.props.label}
                            data={this.props.data}
                            objectAttributes={this.props.objectAttributes}
                            entity={this.props.entity}
                            showlabel={this.props.showlabel}
                            dashboard={this.props.dashboard}
                            buttonId={this.buttonId}
                            funcOnClickSeries={this.whenClickOnSeries(this)}
                            />
                    </div>
                );
            }
        }
        let createButton = () => {
            if (!this.props.control.parameters.VisualizationId) {
                return <span></span>;
            }
            return (
                <input
                    onClick={this.onClickWraper(this)}
                    type='button'
                    className='k-button chart-grid-switcher'
                    value={this.state.mode === 'Grid' ? 'Chart' : 'Grid'}
                    id={this.buttonId}
                    />
            )
        }
        return (
            <div className='row' id={this.id}>
                {chartOrGridComponent()}
                {createButton()}
            </div>
        );
    }
}