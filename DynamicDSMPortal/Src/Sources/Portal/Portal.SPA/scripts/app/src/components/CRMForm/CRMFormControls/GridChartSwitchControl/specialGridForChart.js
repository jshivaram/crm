import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import Label from '../Label'
import guid from '~/helpers/guid'
import toolsForGrid from '../GridControl/toolsForGrid.js'
import createNonObjectConfig from '../GridControl/GridBuilding/nonObjectConfigute'
import getConfigFromCrm from '../GridControl/GridBuilding/crmConfigGetter.js'
import concatObjects from '../GridControl/GridBuilding/toolMethods/objectConcatination.js'
import createLookupConfig from '../GridControl/GridBuilding/lookupTypeConfigure'
import createPicklistConfig from '../GridControl/GridBuilding/picklistType'
import jstz from 'jstz'

import getDateTimeSettings from '~/components/Grid/getDateTimeSettings'
import getDateTimeControl from '../GridControl/GridBuilding/getDateTimeControl'
import createDateTimeConfig from '../GridControl/GridBuilding/dateTimeConfigure'

const $ = window.$;

export default class SpecialGridForChart extends Component {
    render() {
        this.gridId = guid();
        let createLabel = () => {
            if (this.props.showlabel === 'false') {
                return (<span></span>);
            } else {
                return (<Label label={this.props.label} className='portal-grid-label' />);
            }
        };

        return (
            <div className='portal-control row portal-control-grid portal-control-subgrid'>
                {createLabel()}
                <div className='row' id={this.gridId}></div>
            </div>
        );
    }

    componentDidMount() {
        $('#' + this.props.buttonId).hide();
        const self = this;
        const entity = this.props.entity;
        const control = this.props.control;
        const relatedEntityName = control.parameters.TargetEntityType;

        const timezone = jstz.determine().name();

        if (entity && control) {
            const viewId = control.parameters.ViewId
                .replace('{', '').replace('}', '');
            const visualizationId = control.parameters.VisualizationId
                .replace('{', '').replace('}', '');
            const parametersForQuery = this.props.stateOf;
            let categoryId = parametersForQuery.categoryId;
            if (categoryId) {
                categoryId = categoryId.replace('{', '').replace('}', '');
            }

            const readUrl = `/api/chart/GetDataForSwitchedChart?ViewId=${viewId}&EntityLogicalName=${entity.Name}&CategoryDisplayName=${parametersForQuery.categoryDisplayName}&Category=${parametersForQuery.category}&CategoryId=${categoryId}&VisualizationId=${visualizationId}&NotFormattedValue=${parametersForQuery.categoryNotFormattedValue}&DateGrouping=${parametersForQuery.dateGrouping}&browserIanaTimeZone=${timezone}`;
            BuildGrid(relatedEntityName, entity, control, readUrl, self);
        }

        async function BuildGrid(relatedEntityName, entity, control, url, self) {

            let configFromCrm = await getConfigFromCrm();
            let options = GetOptions(url, self);
            if (options == null) {
                return;
            }
            if (configFromCrm) {
                options = concatObjects(options, configFromCrm);
            }
            const viewId = control.parameters.ViewId
                .replace('{', '').replace('}', '');
            const urlForAttributesMetadata = `/api/metadata/GetAttributesMetadata?entityLogicalName=${relatedEntityName}&viewId=${viewId}`;
            $.ajax({
                url: urlForAttributesMetadata,
                method: 'GET',
                success: async function (attributes) {
                    options.columns = [];
                    let fields = {}; 

                    let dateTimeAttrs = {};
                    attributes.forEach(attr => {
                        dateTimeAttrs[attr.LogicalName] = {
                            Type: attr.Type
                        };
                    });

                    let dateTimeSettings = await getDateTimeSettings(control.parameters.TargetEntityType, dateTimeAttrs);

                    options.columns.push({
                        field: 'entityLogicalName',
                        title: control.parameters.TargetEntityType,
                        hidden: true
                    });

                    options.columns.push({
                        field: 'relationshipName',
                        title: control.parameters.RelationshipName,
                        hidden: true
                    });

                    attributes.forEach(attr => {
                        let requiredLevel = toolsForGrid.requiredLevelsDictionary[attr.Required];
                        let type = toolsForGrid.typesDictionary[attr.Type];

                        fields[attr.LogicalName] = {
                            validation: { required: requiredLevel },
                            editable: attr.Editable,
                            type: type
                        };

                        if (attr.LogicalName === relatedEntityName + 'id') {
                            options.columns.push({
                                field: attr.LogicalName,
                                title: attr.DisplayName,
                                hidden: true
                            });

                            return;
                        }

                        if (type === 'object') {
                            let control = toolsForGrid.controlDictionary[attr.Type](relatedEntityName);

                            if (attr.Type === 'LookupType') {
                                createLookupConfig(attr, control, options);
                            }
                            else if (attr.Type === 'PicklistType') {
                                createPicklistConfig(attr, control, options);
                            }
                        }
                        else if (type == 'date') {
                            let control = getDateTimeControl(relatedEntityName, attr, dateTimeSettings);
                            createDateTimeConfig(attr, control, options, dateTimeSettings);
                        }
                        else {
                            createNonObjectConfig(attr, control, options);
                        }
                    });

                    options.dataSource.schema.model.fields = fields;

                    // Column for delete is added in end array
                    if (!self.props.dashboard) {
                        options.columns.push({ command: 'destroy', title: '&nbsp;', width: 150 });
                    }

                    $('#' + self.gridId).html('');
                    $('#' + self.gridId).kendoGrid(options);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        }

        function GetOptions(readUrl, self) {
            return {
                dataSource: {
                    transport: {
                        read: {
                            url: readUrl,
                            dataType: 'json',
                            type: 'GET',
                            complete: function () {
                                $('#' + self.props.buttonId).show();
                            }
                        }
                    },
                    batch: true,
                    pageSize: 5,
                    serverSorting: true,
                    serverPaging: true,
                    schema: {
                        model: { id: relatedEntityName + 'id' },
                        data: 'Data',
                        total: 'Total'
                    },
                    error: function (e) {
                        console.log(e);
                    }
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                toolbar: false,
                sortable: {
                    mode: 'single',
                    allowUnsort: false
                },
                selectable: 'multiple',
                resizable: true,
                editable: false,
                dataBound: function (self) {
                    const gridLinks = $(`#${self.gridId}`).find('a.grid-link');
                    gridLinks.on('click', function (e) {
                        e.preventDefault();
                        const href = $(e.target).attr('href');
                        browserHistory.push(href);
                    });
                }
            }
        }
    }
}