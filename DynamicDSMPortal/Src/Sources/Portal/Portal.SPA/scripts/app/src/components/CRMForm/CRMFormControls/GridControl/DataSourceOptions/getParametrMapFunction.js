
const $ = window.$;

export default function (entity, relatedEntityName, control) {
    return function (options, operation) {
        if (operation === 'read' && !entity.Id) {
            // $('.k-grid-add').on('click', () => {
            //     $('#crm-btn-save button.k-button').first().trigger('click');
            // });
        }

        let result = '';

        let getEntitiesListForUrl = function (models, listName) {
            let res = '';

            $.each(models, function (index, value) {

                let keys = Object.keys(value);

                keys.forEach(key => {
                    if (!value[key]) {
                        delete value[key];
                    }
                    if (typeof (value[key]) === 'object' && value[key].Value) {
                        value[key] = value[key].Value;
                    }
                });

                let json = JSON.stringify(value);
                json = json.replace('\"{', '{').replace('}\"', '}');

                res += listName + '[' + index + ']=' + json + '&';
            });

            return res;
        }

        if (operation === 'create' && options.models) {

            result += getEntitiesListForUrl(options.models, 'RelatedEntitiesJsonList');

            result += 'RelatedEntitiesLogicalName=' + relatedEntityName + '&';
            result += 'EntityId=' + entity.Id + '&';
            result += 'EntityLogicalName=' + entity.Name + '&';
            result += 'RelationshipShcemeName=' + control.parameters.RelationshipName;

            return result;
        } else if (operation === 'update' && options.models) {

            result += getEntitiesListForUrl(options.models, 'JsonEntityDataList');
            result += 'LogicalName=' + relatedEntityName + '&';

            return result;
        } else if (operation === 'destroy' && options.models) {
            $.each(options.models, function (index, value) {

                let fieldNameId = relatedEntityName + 'id';
                result += 'IdArr[' + index + ']=' + value[fieldNameId] + '&';
            });

            result += 'EntityLogicalName=' + relatedEntityName;

            return result;
        }

        result += $.param(options);

        return result;
    }
}