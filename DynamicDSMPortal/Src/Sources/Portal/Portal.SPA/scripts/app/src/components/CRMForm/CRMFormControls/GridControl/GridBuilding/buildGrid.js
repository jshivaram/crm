import getConfigFromCrm from './crmConfigGetter.js'
import getGridOptionsWrapper from '../GridOptions/getGridOptionsWrapper.js'
import concatObjects from './toolMethods/objectConcatination.js'
import toolsForGrid from '../toolsForGrid.js'
import getDateTimeSettings from '~/components/Grid/getDateTimeSettings'
import getDateTimeControl from './getDateTimeControl'

// micro methods
import createLookupConfig from './lookupTypeConfigure'
import createPicklistConfig from './picklistType'
import createNonObjectConfig from './nonObjectConfigute'
import createDateTimeConfig from './dateTimeConfigure'
import createMoneyConfig from './moneyConfigure'
//--------------
const $ = window.$;


async function createGrid(relatedEntityName, crudServiceBaseUrl, entity, control, url, self) {
    let configFromCrm = await getConfigFromCrm();
    const formName = self.props.formName;
    let getOptions = getGridOptionsWrapper(relatedEntityName, crudServiceBaseUrl, entity, control, formName);

    var options = getOptions(url, self);
    if (options == null) {
        return;
    }
    if (configFromCrm) {
        options = concatObjects(options, configFromCrm);
    }

    let urlForAttributesMetadata = '/api/metadata/GetAttributesMetadata?' + 'entityLogicalName=' + control.parameters.TargetEntityType + '&viewId=' + control.parameters.ViewId;

    $.ajax({
        url: urlForAttributesMetadata,
        method: 'GET',
        success: async function (attributes) {
            options.columns = [];
            let fields = {};

            let dateTimeAttrs = {};
            attributes.forEach(attr => {
                dateTimeAttrs[attr.LogicalName] = {
                    Type: attr.Type
                };
            });

            let dateTimeSettings = await getDateTimeSettings(control.parameters.TargetEntityType, dateTimeAttrs);

            options.columns.push({
                field: 'entityLogicalName',
                title: control.parameters.TargetEntityType,
                hidden: true
            });

            options.columns.push({
                field: 'relationshipName',
                title: control.parameters.RelationshipName,
                hidden: true
            });

            attributes.forEach(attr => {
                let requiredLevel = toolsForGrid.requiredLevelsDictionary[attr.Required];
                let type = toolsForGrid.typesDictionary[attr.Type];

                fields[attr.LogicalName] = {
                    validation: { required: requiredLevel },
                    editable: attr.Editable,
                    type: type
                };

                if (attr.LogicalName === relatedEntityName + 'id') {
                    options.columns.push({
                        field: attr.LogicalName,
                        title: attr.DisplayName,
                        hidden: true
                    });

                    return;
                }

                if (type === 'object' || type === 'picklist') {
                    let control = toolsForGrid.controlDictionary[attr.Type](relatedEntityName);

                    if (type === 'object') {
                        createLookupConfig(attr, control, options);
                    }
                    else if (type === 'picklist') {
                        createPicklistConfig(attr, control, options);
                    }
                }
                else if (type == 'date') {
                    let control = getDateTimeControl(relatedEntityName, attr, dateTimeSettings);
                    createDateTimeConfig(attr, control, options, dateTimeSettings);
                }
                else if(attr.Type === 'MoneyType') {
                    createMoneyConfig(attr, control, options);
                }
                else {
                    createNonObjectConfig(attr, control, options);
                }
            });

            options.dataSource.schema.model.fields = fields;

            // Column for delete is added in end array
            if (!self.props.dashboard) {
                options.columns.push({ command: 'destroy', title: '&nbsp;', width: 150 });
            }

            $('#' + self.gridId).html('');
            $('#' + self.gridId).kendoGrid(options);
        },
        error: function (err) {
            console.log(err);
        }
    });
}

export default createGrid;