import constantConfig from './constants'
import createProjection from './toolMethods/createConfigProection'
const $ = window.$;

export default async function () {
    try {
        const response = await $.get(`/api/Configuration/GetPortalConfigurationByName?configName=${constantConfig.configName}`);
        const config = {};

        response.Attributes.forEach(item => {
            config[item.Key] = item.Value;
        });

        
        const configProjection = createProjection(config);

        return configProjection;
    } catch (err) {
        console.log('Crm config was downloaded with error', err);
        return null;
    }
}