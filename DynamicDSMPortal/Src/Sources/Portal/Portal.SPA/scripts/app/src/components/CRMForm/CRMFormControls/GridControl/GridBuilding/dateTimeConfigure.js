
import constDateTimeFormat from '~/constants/dateTimeFormat'

const kendo = window.kendo;

export default function(attr, control, options, dateTimeSettings) {
    const columnWidth = attr.Width;
    options.columns.push({
        field: attr.LogicalName,
        title: attr.DisplayName,
        editor: control,
        width: columnWidth ? columnWidth : '200px',
        template: function (data) {

            let date = data[attr.LogicalName];
            if (!date) {
                return '';
            }

            if (dateTimeSettings.fieldsDateTimeFormats[attr.LogicalName] === constDateTimeFormat.DateAndTime) {
                let dateTimeFormat = dateTimeSettings.userDateTimeFormat.DateFormat
                    + ' ' + dateTimeSettings.userDateTimeFormat.TimeFormat;
                
                return kendo.toString(date, dateTimeFormat);
            }

            let dateFormat = dateTimeSettings.userDateTimeFormat.DateFormat;
            return kendo.toString(date, dateFormat);
        }
    });
}