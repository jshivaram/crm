
import constDateTimeFormat from '~/constants/dateTimeFormat'

const $ = window.$;

export default function(entityLogicalName, attr, dateTimeSettings) {
    return function (container, options) {

        let date = options.model[options.field];
        
        if (!date) {
            date = new Date();
        }

        if (dateTimeSettings.fieldsDateTimeFormats[attr.LogicalName] === constDateTimeFormat.DateAndTime) {
            let dateTimeFormat = dateTimeSettings.userDateTimeFormat.DateFormat
                + ' ' + dateTimeSettings.userDateTimeFormat.TimeFormat

            $('<input name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDateTimePicker({
                    value: date,
                    format: dateTimeFormat
                });

            return;
        }

        let dateFormat = dateTimeSettings.userDateTimeFormat.DateFormat;

        $('<input name="' + options.field + '"/>')
            .appendTo(container)
            .kendoDatePicker({
                value: date,
                format: dateFormat
            });
    }
}