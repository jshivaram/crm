import formTmplateForGridLookup from './toolMethods/formTemplateForGridLookup'

export default function createLookupConfig(attr, control, options) {
    const columnWidth = attr.Width;

    options.columns.push({
        field: attr.LogicalName,
        title: attr.DisplayName,
        editor: control,
        width: columnWidth ? columnWidth : '200px',
        template: function (data) {
            let fieldData = data[attr.LogicalName];
            if (fieldData) {
                if (typeof (fieldData) === 'object') {
                    let fieldDataObj = data[attr.LogicalName];
                    let res = formTmplateForGridLookup(fieldDataObj.Logicalname, fieldDataObj.Id, fieldDataObj.Name);
                    return res;
                } else if (typeof (fieldData) === 'string') {

                    let element = fieldData.replace(/'/g, '"');
                    try {
                        let fieldDataObj = JSON.parse(element);
                        let res = formTmplateForGridLookup(fieldDataObj.Logicalname, fieldDataObj.Id, fieldDataObj.Name);

                        return res;
                    } catch (error) {
                        let res = formTmplateForGridLookup(data.LogicalName, element, data.Name);
                        return res;
                    }
                } else {
                    return '';
                }
            }

            return '--';
        }
    });
}