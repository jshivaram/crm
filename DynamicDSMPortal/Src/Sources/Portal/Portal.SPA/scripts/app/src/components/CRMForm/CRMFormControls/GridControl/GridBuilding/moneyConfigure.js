
export default function (attr, control, options) {
    const columnWidth = attr.Width;
    options.columns.push({
        field: attr.LogicalName,
        title: attr.DisplayName,
        editor: control,
        width: columnWidth ? columnWidth : '200px',
        template: function (data) {
            const fieldData = data[attr.LogicalName];
            const formatedMoney = formatMoney(fieldData);

            if (!formatMoney) {
                return '';
            }

            return '$' + formatedMoney;
        }
    });
}

function formatMoney(value) {
    if(!value) {
        return '';
    }

    if (typeof (value) === 'string') {
        value = value.replace('$', '');
    }
    value = +value;

    if (isNaN(value)) {
        return '';
    }

    let result = value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

    if (result.toString().indexOf('.') === -1) {
        result += + '.00';
    }

    return result;
}