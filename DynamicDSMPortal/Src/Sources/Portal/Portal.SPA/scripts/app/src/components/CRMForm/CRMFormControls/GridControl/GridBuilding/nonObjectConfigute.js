import isEntityInMenu from './toolMethods/isEntityMenu'
import formUrlForRelatedEntity from './toolMethods/formUrlForRelatedEntity'

export default function createNonObjectConfigure(attr, control, options) {
    const entityLogicalName = control.parameters.TargetEntityType;
    const columnWidth = attr.Width;
    
    if (attr.LogicalName === 'ddsm_name'
        || attr.LogicalName === 'name'
        || (entityLogicalName === 'contact' && attr.LogicalName === 'fullname')) {
        options.columns.push({
            field: attr.LogicalName,
            title: attr.DisplayName,
            width: columnWidth ? columnWidth : undefined,
            template: data => {
                if (isEntityInMenu(entityLogicalName)) {
                    const entityId = data[control.parameters.TargetEntityType + 'id'];
                    if (!entityId) {
                        return data[attr.LogicalName];
                    }
                    return formUrlForRelatedEntity(entityLogicalName, entityId, data[attr.LogicalName]);
                } else {
                    return data[attr.LogicalName];
                }
            }
        });
    } else {
        options.columns.push({
            field: attr.LogicalName,
            title: attr.DisplayName,
            width: columnWidth ? columnWidth : undefined,
        });
    }
}