
export default function picklistType(attr, control, options) {
    const columnWidth = attr.Width;

    options.columns.push({
        field: attr.LogicalName,
        title: attr.DisplayName,
        editor: control,
        width: columnWidth ? columnWidth : '200px',
        template: function (data) {
            let jsonFieldData = data[attr.LogicalName];
            
            if (jsonFieldData) {
                jsonFieldData = jsonFieldData.replace(/'/g, '"');
                let objFieldData;
                try {
                    objFieldData = JSON.parse(jsonFieldData);
                } catch (e) {
                    return jsonFieldData;
                }
                return objFieldData.FormattedValue;
            }

            return '--';
        },
        filterable: {
            ui: getPicklistFilterControl(attr.EntityLogicalName, attr.LogicalName)
        }
    });
}

function getPicklistFilterControl(entityLogicalName, atttibuteLogicalName) {
    const url = '/api/metadata/GetMetaDataForDropDownList?'
        + 'entityLogicalName=' + entityLogicalName
        + '&fieldName=' + atttibuteLogicalName;

    return function (element) {
        element.kendoDropDownList({
            dataTextField: 'Text',
            dataValueField: 'Value',
            dataSource: {
                transport: {
                    read: {
                        dataType: 'json',
                        url: url,
                        type: 'GET'
                    }
                }
            },
            optionLabel: '--',
        });
    }
}