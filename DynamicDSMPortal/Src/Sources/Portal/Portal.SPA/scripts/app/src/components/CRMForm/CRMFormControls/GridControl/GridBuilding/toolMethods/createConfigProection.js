let createProjection = function(crmConfig){
    let projectionObject = {
        dataSource:{
            pageSize: crmConfig['ddsm_gridrecordscount']        
        }
    }
    return projectionObject;
}

export default createProjection;