 import isEntityInMenu from './isEntityMenu'

export default function formTmplateForGridLookup(entityLogicalName, entityId, name) {
    name = name ? name : ''

    if (!entityLogicalName || !entityId) {
        return name;
    }

    if (!isEntityInMenu(entityLogicalName)) {
        return name;
    }

    return `<a class='grid-link' href="/form/${entityId}/${entityLogicalName}">${name ? name : '(No Value)'}</a>`;
}