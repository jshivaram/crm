export default function formUrlForRelatedEntity(entityLogicalName, entityId, displayValue) {
    displayValue = displayValue ? displayValue : '(No Value)';

    return `<a class='grid-link' href="/form/${entityId}/${entityLogicalName}">${displayValue}</a>`;
}