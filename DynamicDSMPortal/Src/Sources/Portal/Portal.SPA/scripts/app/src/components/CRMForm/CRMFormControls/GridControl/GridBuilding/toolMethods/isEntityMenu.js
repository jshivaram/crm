 const $ = window.$;
 
 export default function isEntityInMenu(menuItem) {
    if (!menuItem) {
        return false;
    }

    const menuList = $('#main-menu-ul li a').map((i, el) => $(el).attr('href').split('/')[2])
    const result = menuList.filter((i, mi) => mi.toLowerCase() === menuItem.toLowerCase())[0];
    return Boolean(result);
}
