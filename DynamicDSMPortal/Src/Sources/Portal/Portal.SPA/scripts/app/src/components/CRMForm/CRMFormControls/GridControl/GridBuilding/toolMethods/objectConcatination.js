let concatObjects = function(source, target){
    let properties = Object.keys(target);
    let localSource = source;
    properties.forEach(item=>{
        let targetProp = target[item];
        let sourceProp = source[item];

        if (typeof targetProp === 'object' && typeof sourceProp === 'object'){
            localSource[item] = concatObjects(sourceProp,targetProp );
        }else{
            localSource[item] = targetProp;
        }
    });

    return localSource;
}

export default concatObjects;
