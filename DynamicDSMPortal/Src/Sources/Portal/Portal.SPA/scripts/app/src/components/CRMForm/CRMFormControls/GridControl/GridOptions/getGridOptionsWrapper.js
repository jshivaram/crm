import onDataBoundWrapper from '../onDataBoundWrapper.js'
import getDataSource from '../getDataSource.js'
import checkIsBoilerApplication from '~/tools/checkIsBoilerApplication'

const $ = window.$;

export default function (relatedEntityName, crudServiceBaseUrl, entity, control, formName) {
    return function (url, self) {
        return {
            dataBound: onDataBoundWrapper(self, relatedEntityName, entity.Name),
            dataBinding: function () {
                if (entity.Name !== 'ddsm_modelnumberapproval' && relatedEntityName !== 'ddsm_skuapproval') {
                    $(`#${self.gridId} a.k-grid-createOnForm`).hide();
                }
                
                $(`#${self.gridId} a.k-grid-add`).hide();
                $(`#${self.gridId} a.k-grid-save-changes`).hide();
                $(`#${self.gridId} a.k-grid-cancel-changes`).hide();
            },
            dataSource: getDataSource(self, url, crudServiceBaseUrl, entity, relatedEntityName, control),
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            toolbar: !self.props.dashboard ? getToolBarButtons(entity.Id, entity.Name, control.parameters.TargetEntityType, formName) : false,
            sortable: {
                mode: 'single',
                allowUnsort: false
            },
            selectable: 'multiple',
            resizable: true,
            editable: false,// !self.props.dashboard ? true : undefined,
            edit: function (e) {
                if (!e.model.isNew()) {
                    e.preventDefault();
                }

                const $gridCointainer = $(this.content.parents('div.k-grid'));

                e.container.find('.k-grid-cancel-changes').bind('click', function () {
                    $gridCointainer.find('.k-grid-header .k-link').on('click', function (ev) {
                        ev.preventDefault();
                    });
                });

                $gridCointainer.find('.k-grid-header .k-link').on('click', function (ev) {
                    ev.stopPropagation();
                    ev.preventDefault();
                });
            }
        }
    }
}

function getToolBarButtons(entityId, entityLogicalName, targetEntityType, formName = '') {
    let quickAddBtnLabel = 'Quick add';

    const isBoiler = checkIsBoilerApplication();
    try {
        if (isBoiler) {
            quickAddBtnLabel = 'Add Equipment Rebate';
        }
    } catch (error) {
        console.log(error);
    }

    //Hardcode for Fortis

    if (entityLogicalName === 'ddsm_project' && formName === 'DDSM Project Light_Customer Portal') {
        return;
    }

    //e1 hardcode
    if (entityLogicalName === 'ddsm_measuretemplate' && (targetEntityType === 'ddsm_modelnumberapproval' || targetEntityType === 'ddsm_skuapproval')) {
        return [];
    }

    if (entityLogicalName === 'ddsm_modelnumber' && targetEntityType === 'ddsm_modelnumberapproval') {
        return [];
    }

    if (entityLogicalName === 'ddsm_sku' && targetEntityType === 'ddsm_skuapproval') {
        return [];
    }

    //end e1 hardcode

    if (!entityId) {

        if (entityLogicalName === 'ddsm_project' && targetEntityType == 'ddsm_measure') {
            //Remove any buttons from measure grid

            return [
                { name: 'create', text: quickAddBtnLabel },
                'cancel'
            ];
        } else {
            return [
                { name: 'createOnForm', text: 'Add' },
                { name: 'create', text: quickAddBtnLabel },
                'cancel'
            ];
        }


    }

    //mark issues/12527
    return [
        { name: 'createOnForm', text: 'Add' }
    ]
    //end

    // return [
    //     { name: 'createOnForm', text: 'Add' },
    //     { name: 'create', text: quickAddBtnLabel },
    //     isBoiler ? undefined : 'save',
    //     isBoiler ? undefined : 'cancel'
    // ];
}