
import getParametrMapFunction from './DataSourceOptions/getParametrMapFunction.js'

const $ = window.$;

export default function (self, url, crudServiceBaseUrl, entity, relatedEntityName, control) {
    return {
        transport: {
            read: {
                url: url,
                dataType: 'json',
                type: 'GET'
            },
            update: {
                url: crudServiceBaseUrl + '/UpdateMultiple',
                dataType: 'json',
                type: 'PUT',
                complete: function () {
                    $('#' + self.gridId).data('kendoGrid').dataSource.read();
                }
            },
            destroy: {
                url: crudServiceBaseUrl + '/DeleteRelatedMultiple',
                dataType: 'json',
                type: 'DELETE',
                complete: function () {
                    $('#' + self.gridId).data('kendoGrid').dataSource.read();
                },
                error: function (err) {
                    console.log(err);
                }
            },
            create: {
                url: crudServiceBaseUrl + '/CreateRelatedEntities',
                dataType: 'json',
                type: 'POST',
                complete: function () {
                    $('#' + self.gridId).data('kendoGrid').dataSource.read();
                }
            },
            parameterMap: getParametrMapFunction(entity, relatedEntityName, control)
        },
        batch: true,
        pageSize: 5,
        serverSorting: true,
        serverPaging: true,
        serverFiltering: true,
        schema: {
            model: { id: relatedEntityName + 'id' },
            data: 'Data',
            total: 'Total'
        }
    }
}