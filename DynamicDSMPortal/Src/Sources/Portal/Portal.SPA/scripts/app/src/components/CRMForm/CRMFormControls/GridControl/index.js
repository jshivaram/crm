import React, { Component } from 'react'
import Label from '../Label'
import guid from '~/helpers/guid'
import toolsForGrid from './toolsForGrid.js'

const $ = window.$;

export default class GridControl extends Component {
    render() {
        this.gridId = guid();

        let createLabel = () => {
            if (this.props.showlabel === 'false') {
                return (<span></span>);
            } else {
                return (<Label label={this.props.label} className='portal-grid-label' />);
            }
        };

        return (
            <div className='portal-control row portal-control-grid portal-control-subgrid'>
                {createLabel()}
                <div className='row' id={this.gridId}></div>
            </div>
        );
    }
    componentDidMount() {
        let self = this;

        var entity = this.props.entity;
        var control = this.props.control;

        var relatedEntityName = control.parameters.TargetEntityType;

        var crudServiceBaseUrl = '/api/entity';

        var getOptions = function (url, self) {
            return {
                dataSource: {
                    transport: {
                        read: {
                            url: url,
                            dataType: 'json',
                            type: 'GET'
                        },
                        update: {
                            url: crudServiceBaseUrl + '/UpdateMultiple',
                            dataType: 'json',
                            type: 'PUT',
                            complete: function () {
                                $('#' + self.gridId).data('kendoGrid').dataSource.read();
                            }
                        },
                        destroy: {
                            url: crudServiceBaseUrl + '/DeleteRelatedMultiple',
                            dataType: 'json',
                            type: 'DELETE',
                            complete: function () {
                                $('#' + self.gridId).data('kendoGrid').dataSource.read();
                            }
                        },
                        create: {
                            url: crudServiceBaseUrl + '/CreateRelatedEntities',
                            dataType: 'json',
                            type: 'POST',
                            complete: function () {
                                $('#' + self.gridId).data('kendoGrid').dataSource.read();
                            }
                        },
                        parameterMap: function (options, operation) {

                            if (operation === 'read' && !entity.Id) {
                                $('.k-grid-add').on('click', () => {
                                    $('#crm-btn-save button.k-button').first().trigger('click');
                                });
                            }

                            let result = '';

                            let getEntitiesListForUrl = function (models, listName) {
                                let res = '';

                                $.each(models, function (index, value) {

                                    let keys = Object.keys(value);

                                    keys.forEach(key => {
                                        if (!value[key]) {
                                            delete value[key];
                                        }
                                        if (typeof (value[key]) === 'object' && value[key].Value) {
                                            value[key] = value[key].Value;
                                        }
                                    });

                                    let json = JSON.stringify(value);
                                    json = json.replace('\"{', '{').replace('}\"', '}');

                                    res += listName + '[' + index + ']=' + json + '&';
                                });

                                return res;
                            }

                            if (operation === 'create' && options.models) {

                                result += getEntitiesListForUrl(options.models, 'RelatedEntitiesJsonList');

                                result += 'RelatedEntitiesLogicalName=' + relatedEntityName + '&';
                                result += 'EntityId=' + entity.Id + '&';
                                result += 'EntityLogicalName=' + entity.Name + '&';
                                result += 'RelationshipShcemeName=' + control.parameters.RelationshipName;

                                return result;
                            } else if (operation === 'update' && options.models) {

                                result += getEntitiesListForUrl(options.models, 'JsonEntityDataList');
                                result += 'LogicalName=' + relatedEntityName + '&';

                                return result;
                            } else if (operation === 'destroy' && options.models) {
                                $.each(options.models, function (index, value) {

                                    let fieldNameId = relatedEntityName + 'id';
                                    result += 'IdArr[' + index + ']=' + value[fieldNameId] + '&';
                                });

                                result += 'EntityLogicalName=' + relatedEntityName;

                                return result;
                            }

                            result += $.param(options);

                            return result;
                        }
                    },
                    batch: true,
                    pageSize: 5,
                    serverSorting: true,
                    serverPaging: true,
                    schema: {
                        model: { id: relatedEntityName + 'id' },
                        data: 'Data',
                        total: 'Total'
                    }
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                toolbar: !self.props.dashboard ? ['create', 'save', 'cancel'] : false,
                sortable: {
                    mode: 'single',
                    allowUnsort: false
                },
                selectable: 'multiple',
                resizable: true,
                editable: !self.props.dashboard ? true : undefined
            }
        }

        if (entity && control) {
            control.parameters.ViewId = control.parameters.ViewId
                .replace('{', '').replace('}', '');

            let dashboardValue = this.props.dashboard;
            var url = '/api/entity/?' + '&EntityLogicalName=' + entity.Name + '&ViewId=' + control.parameters.ViewId;
            if (!dashboardValue) {

                /*if (!entity.Id) {
                    return null;
                }*/

                url = '/api/entity/GetRelatedEntities?' + 'EntityId=' + entity.Id + '&EntityLogicalName=' + entity.Name + '&RelatedEntityName=' + control.parameters.TargetEntityType + '&RelationshipSchemaName=' + control.parameters.RelationshipName + '&ViewId=' + control.parameters.ViewId;
            }

            var options = getOptions(url, self, control);

            if (options == null) {
                return;
            }

            let urlForAttributesMetadata = '/api/metadata/GetAttributesMetadata?' + 'entityLogicalName=' + control.parameters.TargetEntityType + '&viewId=' + control.parameters.ViewId;

            $.ajax({
                url: urlForAttributesMetadata,
                method: 'GET',
                success: function (attributes) {
                    options.columns = [];
                    let fields = {};

                    attributes.forEach(attr => {
                        let requiredLevel = toolsForGrid.requiredLevelsDictionary[attr.Required];
                        let type = toolsForGrid.typesDictionary[attr.Type];

                        fields[attr.LogicalName] = {
                            validation: { required: requiredLevel },
                            editable: attr.Editable,
                            type: type
                        };

                        if (attr.LogicalName === relatedEntityName + 'id') {
                            options.columns.push({
                                field: attr.LogicalName,
                                title: attr.DisplayName,
                                hidden: true
                            });

                            return;
                        }

                        if (type === 'object') {
                            let control = toolsForGrid.controlDictionary[attr.Type](relatedEntityName);

                            options.columns.push({
                                field: attr.LogicalName,
                                title: attr.DisplayName,
                                editor: control,
                                width: '200px',
                                template: function (data) {
                                    let fieldData = data[attr.LogicalName];
                                    if (fieldData) {
                                        if (typeof (fieldData) === 'object') {
                                            let fieldDataObj = data[attr.LogicalName];
                                            return fieldDataObj.Name;
                                        } else if (typeof (fieldData) === 'string') {
                                            let json = fieldData.replace(/'/g, '"');
                                            let fieldDataObj = JSON.parse(json);
                                            return fieldDataObj.Name;
                                        } else {
                                            return '';
                                        }
                                    }

                                    return '--';
                                }
                            });
                        } else {
                            if (attr.LogicalName === 'ddsm_name' || attr.LogicalName === 'name') {
                                options.columns.push({
                                    field: attr.LogicalName,
                                    title: attr.DisplayName,
                                    template: data => {
                                        const entityLogicalName = control.parameters.TargetEntityType;

                                        if (isEntityInMenu(entityLogicalName)) {
                                            const entityId = data[control.parameters.TargetEntityType + 'id'];
                                            if(!entityId) {
                                                return data[attr.LogicalName];
                                            }
                                            return formUrlForRelatedEntity(entityLogicalName, entityId, data[attr.LogicalName]);
                                        } else {
                                            return data[attr.LogicalName];
                                        }
                                    }
                                });
                            } else {
                                options.columns.push({
                                    field: attr.LogicalName,
                                    title: attr.DisplayName
                                });
                            }
                        }
                    });

                    options.dataSource.schema.model.fields = fields;

                    // Column for delete is added in end array
                    if (!self.props.dashboard) {
                        options.columns.push({ command: 'destroy', title: '&nbsp;', width: 150 });
                    }

                    $('#' + self.gridId).html('');
                    $('#' + self.gridId).kendoGrid(options);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        }
    }
}


function formUrlForRelatedEntity(entityLogicalName, entityId, displayValue) {
    return `<a href="/form/${entityId}/${entityLogicalName}">${displayValue}</a>`;
}

function isEntityInMenu(menuItem) {
    if (!menuItem) {
        return false;
    }

    const menuList = $('#main-menu-ul li a').map((i, el) => $(el).attr('href').split('/')[2])
    const result = menuList.filter((i, mi) => mi.toLowerCase() === menuItem.toLowerCase())[0];
    return Boolean(result);
}