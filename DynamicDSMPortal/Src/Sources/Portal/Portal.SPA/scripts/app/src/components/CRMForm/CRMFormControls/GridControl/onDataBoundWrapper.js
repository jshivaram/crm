
import { browserHistory } from 'react-router'

const $ = window.$;
const openGlobalPopup = window.openGlobalPopup;

export default function (self, relatedEntityName, entityLogicalName) {
    return function () {
        $(`#${self.gridId} a.k-grid-delete`).hide();

        //e1 hardcode
        if (entityLogicalName === 'ddsm_measuretemplate' && (relatedEntityName === 'ddsm_modelnumberapproval' || relatedEntityName === 'ddsm_skuapproval')) {
            $(`#${self.gridId} .k-grid-toolbar`).hide();
        }

        if (entityLogicalName === 'ddsm_modelnumber' && relatedEntityName === 'ddsm_modelnumberapproval') {
            $(`#${self.gridId} .k-grid-toolbar`).hide();
            return;
        }

        if (entityLogicalName === 'ddsm_sku' && relatedEntityName === 'ddsm_skuapproval') {
            $(`#${self.gridId} .k-grid-toolbar`).hide();
            return;
        }

         if (entityLogicalName === 'ddsm_modelnumber' && relatedEntityName === 'ddsm_sku') {
            $(`#${self.gridId} .k-grid-toolbar`).hide();
            return;
        }

        $.get(`/api/access/CanDelete?entityLogicalName=${relatedEntityName}`)
            .done(res => {
                if (res === true) {
                    $(`#${self.gridId} a.k-grid-delete`).show();
                }
            })
            .fail(err => console.log(err));

        $.get(`/api/access/CanCreate?entityLogicalName=${relatedEntityName}`)
            .done(res => {
                if (res === true) {
                    if (relatedEntityName !== 'ddsm_applicationmeasures') {
                        $(`#${self.gridId} a.k-grid-createOnForm`).show();
                    }
                    $(`#${self.gridId} a.k-grid-add`).show();
                    $(`#${self.gridId} a.k-grid-save-changes`).show();
                    $(`#${self.gridId} a.k-grid-cancel-changes`).show();
                }
            })
            .fail(err => console.log(err));

        $.get(`/api/access/CanUpdate?entityLogicalName=${relatedEntityName}`)
            .done(res => {
                if (res === true) {
                    $(`#${self.gridId} a.k-grid-save-changes`).show();
                    $(`#${self.gridId} a.k-grid-cancel-changes`).show();
                }

            })
            .fail(err => console.log(err));

        const gridLinks = $(`#${self.gridId}`).find('a.grid-link');

        gridLinks.on('click', function (e) {
            e.preventDefault();
            const href = $(e.target).attr('href');
            browserHistory.push(href);
        });

        addEventToAddOnFormButton(self);
    }
}

function addEventToAddOnFormButton(self) {
    const $createButton = $(`#${self.gridId} .k-grid-createOnForm`);

    if ($createButton.length === 0) {
        return;
    }

    const controlParams = self.props.control.parameters;

    if (!controlParams) {
        return;
    }

    const entityLogicalName = controlParams.TargetEntityType;
    const relationshipName = controlParams.RelationshipName;

    const relatedEntity = self.props.entity;
    const relatedEntityName = relatedEntity.Name;
    const relatedEntityId = relatedEntity.Id;

    if (!entityLogicalName || !relationshipName || !relatedEntityName || !relatedEntityId) {
        return;
    }

    $createButton.on('click', function (ev) {
        ev.preventDefault();

        if(relatedEntityName === 'ddsm_modelnumberapproval' && entityLogicalName === 'ddsm_skuapproval') {
            const modelNumberId = $('.hidden_lookup_ddsm_modelnumberid').val();
            const measuteTemplateId = $('.hidden_lookup_ddsm_measuretemplateid').val();
            const modelNumberApprovalId = $('input[name="id"]').val();

            if(!modelNumberId) {
                openGlobalPopup('Cant get MN id');
                return;
            }

            if(!measuteTemplateId) {
                openGlobalPopup('Cant get MT id');
                return;
            }

            if(!modelNumberApprovalId) {
                openGlobalPopup('Cant get MNA id');
                return;
            }

            browserHistory.push(`/form/createSkuFromModelNumberApproval/${modelNumberId}/${measuteTemplateId}/${modelNumberApprovalId}`);
            return;
        }

        browserHistory.push(`/form/newRelated/${entityLogicalName}/${relationshipName}/${relatedEntityName}/${relatedEntityId}`);
    });
}