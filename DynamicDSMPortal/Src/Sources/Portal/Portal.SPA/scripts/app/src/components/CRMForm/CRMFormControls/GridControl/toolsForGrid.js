
const $ = window.$;

let controlDictionary = {
    'LookupType': function (entityLogicalName) {
        return function (container, options) {
            let url = '/api/entity/GetEntitiesForGridLookup?EntityLogicalName=' + entityLogicalName + '&FieldName=' + options.field + '&asd=asd';

            let projectTemplateId = $('input[name="ddsm_projecttemplateid.Id"]').val();
            let isApplicationForm = window.location.href.indexOf('/ddsm_application') !== -1;
            let isMeasureTemplateField = (entityLogicalName === 'ddsm_applicationmeasures');
            
            if (projectTemplateId && isApplicationForm && isMeasureTemplateField) {
                url = '/api/application/GetMeasureTemplatesForLookup?projectTemplateId=' + projectTemplateId;
            }

            $('<select name="' + options.field + '"></select>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataTextField: 'Text',
                    dataValueField: 'Value',
                    dataSource: {
                        transport: {
                            read: {
                                dataType: 'json',
                                url: url,
                                type: 'GET'
                            }
                        }
                    },
                    select: function (e) {
                        let dataItem = this.dataItem(e.item);

                        let resultObject = {
                            Id: dataItem.Value,
                            Name: dataItem.Text
                        };

                        options.model[options.field] = resultObject;
                        options.model.dirty = true;
                        container.parents('.k-grid').data('kendoGrid').refresh();
                    }
                });
        }
    }
};

let typesDictionary = {
    'StringType': 'string',
    null: 'string',
    'DecimalType': 'number',
    'asd3': 'boolean',
    'DateTimeType': 'date',
    'LookupType': 'object'
}

let requiredLevelsDictionary = {
    2: true,
    0: false,
    3: false,
    1: true
}

export default {
    typesDictionary: typesDictionary,
    controlDictionary: controlDictionary,
    requiredLevelsDictionary: requiredLevelsDictionary
};