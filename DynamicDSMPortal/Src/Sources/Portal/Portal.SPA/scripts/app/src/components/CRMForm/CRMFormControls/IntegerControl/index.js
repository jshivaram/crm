import React, { Component } from 'react'
import Label from '../Label'

export default class IntegerControl extends Component {
    render() {

        const fieldName = this.props.control['@datafieldname'];
        const id = this.props.control['@id'];
        const isReadonly = this.props.control['@disabled'] === 'true';

        const createInput = () => {
            return (
                <input type='number' id={id} className='k-textbox' type='text'
                    name={fieldName}
                    defaultValue={this.props.data[fieldName]}
                    readOnly={isReadonly} />
            );
        };

        if (this.props.showlabel === 'true') {
            const divId = id + '_div';

            return (
                <div className='portal-control row' id={divId}>
                    <div className='col-md-4'>
                        <Label label={this.props.label}
                            className='portal-textbox-label' />
                    </div>
                    <div className='col-md-8'>
                        {createInput()}
                    </div>
                </div>
            );
        }

        return (
            <div className='portal-control row'>
                {createInput()}
            </div>
        );
    }
}