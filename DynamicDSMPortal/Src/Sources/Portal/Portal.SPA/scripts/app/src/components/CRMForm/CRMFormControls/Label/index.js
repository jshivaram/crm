import React, { Component } from 'react'

export default class Label extends Component {
    render() {
        return (
            <div className={'row ' + (this.props.className ? ' ' + this.props.className : '') }>
                <span className={'ra-well-title'}>
                    {this.props.label['@description'].toLowerCase() !== 'section' 
                        ? this.props.label['@description'] : '' }
                </span>
            </div>
        );
    }
}