import { browserHistory } from 'react-router'
import GlobalLoader from '~/components/GlobalLoader'
const $ = window.$;

export default function createLookup(lookupEntityLogicalName, self) {
    const globalLoader2 = new GlobalLoader();
    globalLoader2.show();
    let url = '';
    const entity = self.props.entity;
    let entityLogicalName = entity.entityLogicalName;
    if (!entityLogicalName) {
        entityLogicalName = entity.EntityName;
    }

    if (!entityLogicalName) {
        entityLogicalName = entity.Name;
    }

    if (lookupEntityLogicalName) {
        url = '/api/entity/GetFilteredEntitiesForLookupByEntityName?EntityLogicalName=' + lookupEntityLogicalName
    } else if (entityLogicalName === 'ddsm_application' && self.dataFieldName === 'ddsm_contractorid') {
        url = '/api/account/GetContractorsForLookup';
    } else {
        url = `/api/entity/GetEntitiesForLookupByFieldName?EntityLogicalName=${entityLogicalName}&FieldName=${self.dataFieldName}`;
    }

    const dataFieldName = self.props.control['@datafieldname'];
    const objectAttributes = self.props.objectAttributes;

    if (self.required) {
        const $lookup = $('#' + self.lookupId);
        $lookup.attr('required', true);
        const label = self.props.label;

        if (!label) {
            globalLoader2.hide();
            return;
        }

        const labelText = label['@description'] + ' is required';
        $lookup.attr('validationMessage', labelText);
    }

    let isFirstLoading = true;

    $('#' + self.lookupId).kendoDropDownList({
        filter: 'contains',
        ignoreCase: true,
        dataTextField: 'Text',
        dataValueField: 'Value',
        dataSource: {
            serverFiltering: true,
            transport: {
                read: {
                    dataType: 'json',
                    url: url,
                    type: 'GET'
                }
            },
            schema: {
                errors: function (response) {
                    return response.error; // twitter's response is { "error": "Invalid query" }
                }
            },
            error: function (e) {
                console.log(e.errors); // displays "Invalid  query"
                globalLoader2.hide();
            }
        },
        optionLabel: '--',
        dataBound: function () {

            if (isFirstLoading) {
                isFirstLoading = false;
            } else {
                globalLoader2.hide();
                return;
            }
            const lookup = this;
            const $lookupField = $('#' + self.lookupId);
            if (!objectAttributes || !objectAttributes[dataFieldName]) {
                const lookupInputIdName = $lookupField.attr('name');

                //hardcode for quickcreate form for sku start
                const href = window.location.href;
                if (href.indexOf('form/newRelated/ddsm_sku/ddsm_ddsm_modelnumber_ddsm_sku_ModelNumber/ddsm_modelnumber/') !== -1
                    && lookupInputIdName === 'ddsm_modelnumber.Id') {
                    const splitedHrefArr = href.split('/');
                    const modelNumberId = splitedHrefArr[splitedHrefArr.length - 1];

                    if (modelNumberId) {
                        const modelNumberUrl = `/api/entity/get?id=${modelNumberId}&entityLogicalName=ddsm_modelnumber`;
                        $.get(modelNumberUrl).done(retrivedEntityData => {
                            if (!retrivedEntityData) {
                                alert('Model number removed.');
                                return;
                            }

                            const modelNumberName = retrivedEntityData.Attributes.filter(a => a.Key === 'ddsm_name')[0].Value;
                            
                            if (!lookup.dataSource.data().filter(d => d.Value === modelNumberId)[0]) {
                                lookup.dataSource.add({ Text: modelNumberName, Value: modelNumberId });
                            }

                            lookup.value(modelNumberId);
                        });
                        return;
                    }
                }
                //hardcode for quickcreate form for sku end

                //Hardcode for fortis boiler application
                if (!$lookupField.val()) {
                    if (self.props.entity.Name === 'ddsm_application') {
                        if (lookupInputIdName === 'ddsm_parentaccountid.Id' ||
                            lookupInputIdName === 'ddsm_parentsiteid.Id') {
                            setTimeout(function () {
                                $.get('/api/auth/GetAuthorizedUserData')
                                    .done(data => {
                                        if (!lookup.dataSource.data().filter(d => d.Value === data.UserId)[0]) {
                                            lookup.dataSource.add({ Text: data.FullName, Value: data.UserId });
                                        }
                                        if (lookupInputIdName === 'ddsm_parentaccountid.Id') {

                                            if (!lookup.dataSource.data().filter(d => d.Value === data.AccountId)[0]) {
                                                lookup.dataSource.add({ Text: data.AccountName, Value: data.AccountId });
                                            }
                                            lookup.value(data.AccountId);

                                            $lookupField.trigger('change');

                                            $lookupField.val(data.AccountId);
                                        } else if (lookupInputIdName === 'ddsm_parentsiteid.Id') {
                                            if (!lookup.dataSource.data().filter(d => d.Value === data.SiteId)[0]) {
                                                lookup.dataSource.add({ Text: data.SiteName, Value: data.SiteId });
                                            }
                                            lookup.value(data.SiteId);

                                            $lookupField.trigger('change');

                                            $lookupField.val(data.SiteId);
                                        }
                                    })
                                    .fail(err => {
                                        console.log(err);
                                        globalLoader2.hide();
                                        throw 'Cant get user data';
                                    })
                                    .always(() => globalLoader2.hide());
                                $lookupField.trigger('change');
                            }, 1500);
                        }
                    }
                    else {
                        $lookupField.val(null);
                    }
                } else if (lookupInputIdName === 'ddsm_contactid.Id'
                    && self.props.entity.Name === 'ddsm_application') {

                    $.get('/api/auth/GetAuthorizedUserData')
                        .done(data => {
                            if (!lookup.dataSource.data().filter(d => d.Value === data.UserId)[0]) {
                                lookup.dataSource.add({ Text: data.FullName, Value: data.UserId });
                            }

                            lookup.value(data.UserId);

                            $lookupField.trigger('change');

                            $lookupField.val(data.UserId);
                        })
                        .fail(err => {
                            console.log(err);
                            globalLoader2.hide();
                            throw 'Cant get user data';
                        })
                        .always(() => globalLoader2.hide());
                }

            } else {
                const dataField = objectAttributes[dataFieldName];
                if (!lookup.dataSource.data().filter(d => d.Value === dataField.Id)[0]) {
                    lookup.dataSource.add({ Text: dataField.Name, Value: dataField.Id });
                }

                lookup.value(dataField.Id);
            }

            addOnClickEventToLookup(self.lookupWrapperId, self.lookupId, self.lookupEntityLogicalName);

            //Hardcode for Fortis autofilling Project QuickCreate Form
            if (self.props.isQuickForm) {
                $.get(`/api/entity/Get?id=${self.props.params.relatedEntityId}&entityLogicalName=${self.props.params.relatedEntityName}`)
                    .done(relatedEntity => {
                        if (entityLogicalName === 'ddsm_project') {
                            let kendoLookup = $lookupField.data('kendoDropDownList');
                            let name = kendoLookup.element[0].name;
                            if (name === 'ddsm_accountid.Id') {
                                let attrs = relatedEntity.Attributes;
                                let account = attrs.filter(attr => {
                                    return attr.Key === 'ddsm_parentaccount';
                                });
                                let accId = account[0].Value.Id;
                                kendoLookup.value(accId);
                            }
                            if (name === 'ddsm_parentsiteid.Id') {
                                kendoLookup.value(relatedEntity.Id)
                            }
                        }
                    })
                    .fail(err => {
                        console.log(err);
                    })
                    .always(() => globalLoader2.hide());
            }
            globalLoader2.hide();
        }
    });

    setTimeout(function () {
        globalLoader2.hide();
    }, 3000);
}


function addOnClickEventToLookup(lookupWrapperId, lookupId, lookupEntityLogicalName) {

    if (!lookupWrapperId || !lookupId || !lookupEntityLogicalName) {
        return;
    }

    const $lookupTextInput = $('#' + lookupWrapperId + '>span>span>span.k-input');

    if ($lookupTextInput.length === 0) {
        return;
    }

    const lookup = $('#' + lookupId).data('kendoDropDownList');
    if (!lookup) {
        return;
    }

    const val = lookup.value();

    if (!val) {
        return;
    }

    $lookupTextInput.unbind('click');
    $lookupTextInput.on('click', () => {
        if (isEntityInMenu(lookupEntityLogicalName)) {
            browserHistory.push(`/form/${val}/${lookupEntityLogicalName}`);
        }
    });
}

function isEntityInMenu(menuItem) {
    if (!menuItem) {
        return false;
    }

    const menuList = $('#main-menu-ul li a').map((i, el) => $(el).attr('href').split('/')[2])
    const result = menuList.filter((i, mi) => mi.toLowerCase() === menuItem.toLowerCase())[0];
    return Boolean(result);
}