import React, { Component } from 'react'
import Label from '../Label'
import guid from '~/helpers/guid'
import { browserHistory } from 'react-router'

const $ = window.$;

export default class LookupControl extends Component {
    render() {

        this.lookupId = guid();
        this.lookupWrapperId = guid();

        const control = this.props.control;
        const dataFieldName = control['@datafieldname'];
        this.dataFieldName = dataFieldName;
        const dataField = this.props.objectAttributes[dataFieldName];
        const defaultValue = dataField ? dataField.Name : '';
        const lookupEntityLogicalName = dataField ? dataField.LogicalName : '';
        this.lookupEntityLogicalName = lookupEntityLogicalName;

        const createLookupDiv = () => {
            if (control['@disabled'] !== 'false') {
                return (
                    <div id={this.lookupWrapperId} className='lookup-wrapper'>
                        <input
                            className='k-textbox crm-lookup-readonly'
                            defaultValue={defaultValue}
                            readOnly='readonly' />
                    </div>
                );
            }

            return (
                <div id={this.lookupWrapperId} className='lookup-wrapper'>
                    <select
                        id={this.lookupId}
                        className={'crm-lookup-select ' + lookupEntityLogicalName}>
                    </select>
                    <input type='hidden'
                        id={this.lookupId + '_Id'}
                        name={dataFieldName ? dataFieldName + '.Id' : ''}
                        defaultValue={dataField ? dataField.Id : ''}
                        />
                    <input type='hidden'
                        id={this.lookupId + '_LogicalName'}
                        name={dataFieldName ? dataFieldName + '.LogicalName' : ''}
                        defaultValue={dataField ? dataField.LogicalName : ''}
                        />
                </div>
            );
        };

        if (this.props.showlabel !== 'false' && this.props.label) {
            return (
                <div className='portal-control row'>
                    <div className='col-md-4'>
                        <Label label={this.props.label} className='portal-lookup-label' />
                    </div>
                    <div className='col-md-8'>
                        {createLookupDiv()}
                    </div>
                </div>
            );
        }

        return (
            <div className='portal-control row'>
                <div id={this.lookupId}>
                    {createLookupDiv()}
                </div>
            </div>
        );
    }
    componentDidMount() {

        if (this.props.control['@disabled'] !== 'false') {
            return;
        }

        let self = this;

        if (!this.lookupEntityLogicalName) {
            let url = '/api/metaData/getAttributeEntityName?entityLogicalName=' +
                this.props.entity.Name + '&fieldName=' + this.dataFieldName;
            $.get(url).done(entityName => {
                if (entityName) {
                    $('#' + this.lookupId).addClass(entityName);
                }
            }).fail(err => console.log(err));
        }

        let createLookup = (viewId) => {

            if (!viewId) {
                return;
            }

            let url = '/api/entity/GetFilteredEntitiesForLookup?viewId=' + viewId;

            let dataFieldName = this.props.control['@datafieldname'];

            let isFiltered = false;

            let objectAttributes = this.props.objectAttributes;
            let lookup = null;

            $('#' + this.lookupId).kendoDropDownList({
                filter: 'contains',
                ignoreCase: true,
                dataTextField: 'Text',
                dataValueField: 'Value',
                dataSource: {
                    serverFiltering: true,
                    transport: {
                        read: {
                            dataType: 'json',
                            url: url,
                            type: 'GET'
                        }
                    }
                },
                filtering: function (ev) {
                    if (ev.filter.value && ev.filter.value.length < 3) {
                        ev.preventDefault();
                    }

                    isFiltered = true;
                },
                change: function (e) {
                    if (isFiltered) {
                        e.preventDefault();
                        return;
                    }

                    const data = $('#' + self.lookupId).data('kendoDropDownList');
                    const value = data.value();

                    if (!value) {
                        data.value(null);
                        return;
                    }

                    const lookupIdInput = document.getElementById(self.lookupId + '_Id');
                    lookupIdInput.value = value;
                    addOnLickEventToLookup(self.lookupWrapperId, self.lookupId, self.lookupEntityLogicalName);
                },
                optionLabel: '--',
                dataBound: function () {
                    if (isFiltered) {
                        return;
                    }

                    lookup = this;

                    if (!objectAttributes || !objectAttributes[dataFieldName]) {
                        const lookupInputIdName = $('#' + self.lookupId).parent().next().attr('name');

                        if ($('#' + self.lookupId).val()) {
                            $('#' + self.lookupId).val(null);
                        } else if (lookupInputIdName === 'ddsm_contactid.Id'
                            && self.props.entity.Name === 'ddsm_application') {

                            $.get('/api/auth/GetAuthorizedUserData')
                                .done(data => {
                                    if (!lookup.dataSource.data().filter(d => d.Value === data.UserId)[0]) {
                                        lookup.dataSource.add({ Text: data.FullName, Value: data.UserId });
                                    }

                                    lookup.value(data.UserId);

                                    $('#' + self.lookupId).trigger('change');

                                    const lookupIdInput = document.getElementById(self.lookupId + '_Id');
                                    lookupIdInput.value = data.UserId;
                                })
                                .fail(err => {
                                    console.log(err);
                                    throw 'Cant get user data';
                                });
                        }

                    } else {
                        const dataField = objectAttributes[dataFieldName];

                        if (!lookup.dataSource.data().filter(d => d.Value === dataField.Id)[0]) {
                            lookup.dataSource.add({ Text: dataField.Name, Value: dataField.Id});
                        }

                        lookup.value(dataField.Id);
                    }

                    isFiltered = false;
                    addOnLickEventToLookup(self.lookupWrapperId, self.lookupId, self.lookupEntityLogicalName);
                }
            });
        }

        if (!this.props.control.parameters) {

            const dataFieldName = this.props.control['@datafieldname'];
            const dataField = this.props.objectAttributes[dataFieldName];

            if (!dataField) {
                let viewIdUrl = `/api/view/GetLookupViewIdByLookupGetModel?entityLogicalName=${this.props.entity.Name}&fieldName=${dataFieldName}`;

                $.get(viewIdUrl)
                    .done(viewId => createLookup(viewId))
                    .fail(() => {
                        throw 'Error when GetLookupViewId';
                    });
                return;
            }

            $.get('/api/view/GetLookupViewId?entityLogicalName=' + dataField.LogicalName)
                .done(viewId => createLookup(viewId));

            let dataFieldId = dataField ? dataField.Id : '';
            $('#' + this.lookupId).attr('entityId', dataFieldId);

            return;
        }

        if (this.props.control.parameters) {
            let viewId = this.props.control.parameters.DefaultViewId;

            if (viewId) {
                viewId = viewId.replace('{', '').replace('}', '');
                createLookup(viewId);
                return;
            }

            let url = `/api/view/GetLookupViewIdByLookupGetModel?EntityLogicalName=${this.props.entity.Name}&FieldName=${this.props.control['@datafieldname']}`;
            $.get(url)
                .done(viewId => {

                    if (!viewId) {
                        return;
                    }
                    createLookup(viewId);
                }).fail(err => console.log(err));

        }

    }
}

function addOnLickEventToLookup(lookupWrapperId, lookupId, lookupEntityLogicalName) {

    if (!lookupWrapperId || !lookupId || !lookupEntityLogicalName) {
        return;
    }

    const $lookupTextInput = $('#' + lookupWrapperId + '>span>span>span.k-input');

    if ($lookupTextInput.length === 0) {
        return;
    }

    const lookup = $('#' + lookupId).data('kendoDropDownList');
    if (!lookup) {
        return;
    }

    const val = lookup.value();

    if (!val) {
        return;
    }

    $lookupTextInput.unbind('click');
    $lookupTextInput.on('click', () => {
        if (isEntityInMenu(lookupEntityLogicalName)) {
            browserHistory.push(`/form/${val}/${lookupEntityLogicalName}`);
        }
    });
}

function isEntityInMenu(menuItem) {
    if (!menuItem) {
        return false;
    }

    const menuList = $('#main-menu-ul li a').map((i, el) => $(el).attr('href').split('/')[2])
    const result = menuList.filter((i, mi) => mi.toLowerCase() === menuItem.toLowerCase())[0];
    return Boolean(result);
}