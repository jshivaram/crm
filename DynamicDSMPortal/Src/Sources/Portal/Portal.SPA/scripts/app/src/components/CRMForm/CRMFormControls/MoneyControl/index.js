import React, { Component } from 'react'
import Label from '../Label'

export default class MoneyControl extends Component {
    render() {

        const fieldName = this.props.control['@datafieldname'];
        const id = this.props.control['@id'];
        const isReadonly = this.props.control['@disabled'] === 'true';

        const currency = getCurrencyFromProps(this.props);
        const currencyCssClass = getCssClassFromCurrency(currency);

        const createInput = () => {
            return (
                <input
                    id={id}
                    type='number'
                    className={'k-textbox money-input ' + currencyCssClass}
                    name={fieldName + '.Value'}
                    step='any'
                    defaultValue={this.props.data[fieldName]}
                    readOnly={isReadonly} />
            );
        };

        if (this.props.showlabel !== 'false' && this.props.label) {
            const divId = id + '_div';

            return (
                <div className='portal-control row' id={divId}>
                    <div className='col-md-4'>
                        <Label label={this.props.label}
                            className='portal-textbox-label' />
                    </div>
                    <div className='col-md-8'>
                        {createInput()}
                    </div>
                </div>
            );
        }

        return (
            <div className='portal-control row'>
                {createInput()}
            </div>
        );
    }
}

function getCssClassFromCurrency(currency) {
    if (!currency) {
        return '';
    }

    let result = currency;

    while (result.indexOf(' ') !== -1) {
        result = result.replace(' ', '-');
    }

    result += '-currency';

    return result;
}

function getCurrencyFromProps(props) {
    if(!props || !props.objectAttributes || !props.objectAttributes.transactioncurrencyid) {
        return;
    }

    let currency = props.objectAttributes.transactioncurrencyid.Name;
    currency = currency ? currency : '';

    return currency;
}