import React, { Component } from 'react'
import notesConsts from '../notesConstants'
import jstz from 'jstz'

import checkIsBoilerApplication from '~/tools/checkIsBoilerApplication'

const $ = window.$;
const timezone = jstz.determine().name();

export default class NotesGridControl extends Component {
    render() {
        return (
            <div className='row'>
                <div id={notesConsts.gridControlId}></div>
            </div>
        );
    }
    componentDidMount() {
        let self = this;
        const isBoilerOrBoilerProject = checkIsBoilerApplication() ||
            (this.props.entity.Name === 'ddsm_project' &&
                this.props.formName &&
                this.props.formName === 'DDSM Project Light_Customer Portal');

        let notesUrl = '/api/annotation/GetRelatedAnnotations?'
            + 'RelatedEntityId=' + this.props.entity.Id
            + '&RelatedEntityLogicalName=' + this.props.entity.Name
            + '&BrowserIanaTimeZone=' + timezone;

        $('#' + notesConsts.gridControlId).kendoGrid({
            dataSource: {
                schema: {
                    model: {
                        id: 'Id',
                        fields: {
                            Id: { editable: false },
                            CreatedOn: { editable: false },
                            FileName: { validation: { required: true } }
                        }
                    },
                    data: 'Data',
                    total: 'Total'
                },
                pageSize: 7,
                transport: {
                    read: {
                        url: notesUrl,
                        dataType: 'json',
                        type: 'GET'
                    },
                    update: {
                        url: '/api/annotation/ChangeFilesNames',
                        dataType: 'json',
                        type: 'PUT',
                        complete: function () {
                            $('#' + notesConsts.gridControlId).data('kendoGrid').dataSource.read();
                        }
                    },
                    destroy: {
                        url: '/api/annotation/Remove',
                        dataType: 'json',
                        type: 'DELETE',
                        complete: function () {
                            $('#' + notesConsts.gridControlId).data('kendoGrid').dataSource.read();
                        }
                    },
                    parameterMap: function (options, operation) {

                        if (operation === 'update' && options.models) {

                            let arr = options.models.map(item => {
                                return {
                                    AnnotationId: item.Id,
                                    FileName: item.FileName
                                };
                            });

                            let result = {
                                ChangeModels: arr
                            };

                            return result;
                        } else if (operation === 'destroy' && options.models) {

                            let arr = options.models.map(item => {
                                return item.Id;
                            });

                            let result = {
                                IdList: arr
                            };

                            return result;
                        }
                    }
                },
                batch: true,
                error: function (e) {
                    var notification = $('#notification').data('kendoNotification');
                    notification.show({
                        title: 'Error',
                        message: e.errorThrown,
                    }, 'error');
                }
            },
            editable: false,
            toolbar: [
                {
                    name: 'upload',
                    text: isBoilerOrBoilerProject ? 'Upload Supporting Documents' : 'Upload',
                    imageClass: 'k-icon k-add'
                }
            ],
            sortable: {
                mode: 'single',
                allowUnsort: false
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            selectable: 'multiple',
            resizable: true,
            mobile: true,
            columns: [
                {
                    command: {
                        text: 'Download',
                        click: self.downloadHandler
                    },
                    title: ' ',
                    width: '180px'
                },
                {
                    hidden: true,
                    field: 'Id',
                    title: 'Id'
                },
                {
                    field: 'CreatedOn',
                    title: 'Uploading Date',
                    width: 250
                },
                {
                    field: 'FileName',
                    title: 'File Name'
                }/*,
                {
                    command: 'destroy',
                    title: '&nbsp;'//,
                    // width: 950
                }*/
            ],
            dataBound: function () {
                $('.k-grid-upload').unbind('click');
                $('.k-grid-upload').click(function (e) {
                    e.preventDefault();
                    $('#' + notesConsts.uploadControlId).trigger('click');
                });
            }
        });
    }
    downloadHandler(e) {
        e.preventDefault();

        let dataItem = this.dataItem($(e.currentTarget).closest('tr'));

        let url = window.location.protocol + '//'
            + window.location.host + '/api/annotation/DownloadFile?annotationId=' + dataItem.Id;

        window.open(url);
    }
}