import React, { Component } from 'react'
import notesConsts from '../notesConstants'
import btnSaveHandler from '~/components/CRMForm/CRMFormComponents/Form/FormButtonSave/btnSaveHandler.js'
import { browserHistory } from 'react-router'

const $ = window.$;

export default class NotesUploadControl extends Component {
    render() {
        return (
            <div className={'row ' + notesConsts.uploadControlId} >
                <input name='uploadedfile' id={notesConsts.uploadControlId} type='file' />
            </div>
        );
    }
    componentDidMount() {

        let uploadUrl = '/api/annotation/create';

        let self = this;

        $('#' + notesConsts.uploadControlId).kendoUpload({
            async: {
                saveUrl: uploadUrl,
                autoUpload: true,
                allowmultiple: true,
                batch: true
            },
            upload: function (e) {
                if (self.props.entity.Id) {
                    e.data = {
                        id: self.props.entity.Id,
                        logicalname: self.props.entity.Name
                    };
                    return;
                }

                const infoForBtnSave = {
                    entityName: self.props.entity.Name,
                    action: '/api/entity',
                    method: 'POST'
                };

                const formDataJson = btnSaveHandler(infoForBtnSave, null, function () { }, function () { }, true)();

                if (!formDataJson) {
                    e.preventDefault();
                    return;
                }

                e.data = {
                    id: self.props.entity.Id,
                    logicalname: self.props.entity.Name,
                    entityDataJson: formDataJson
                };
            },
            complete: function () {
                $('#' + notesConsts.gridControlId).data('kendoGrid').dataSource.read();

                window.setTimeout(function () {
                    let $upload = $('#' + notesConsts.uploadControlId).data('kendoUpload');
                    $upload.clearAllFiles();
                }, 5000);
            },
            success: function(e) {
                const response = e.response;
                if(!response.DocumentAttachedToExistedEntity) {
                    browserHistory.push(`/form/${response.EntityId}/${response.EntityLogicalName}`);
                }
            },
            dropZone: '.dropZoneElement'
        });

        $('.' + notesConsts.uploadControlId + ' .k-dropzone').hide();
    }
}