import React, { Component } from 'react'
import NotesUploadControl from './NotesUploadControl'
import NotesGridControl from './NotesGridControl'

export default class NotesControl extends Component {
    render() {
        
        if (!this.props.entity.Id) {
            return (<span></span>);
        }
        return (
            <div className='portal-notes-control row'>
                <div className='dropZoneElement'>
                    <NotesUploadControl 
                        entity={this.props.entity}
                        formName = {this.props.formName} />
                    <NotesGridControl 
                        entity={this.props.entity}
                        formName = {this.props.formName} />
                </div>
            </div>
        );
    }
}