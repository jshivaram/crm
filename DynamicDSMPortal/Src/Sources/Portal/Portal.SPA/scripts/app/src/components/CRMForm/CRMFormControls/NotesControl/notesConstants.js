
const notesConstants = {
    gridControlId: 'portal-notes-grid-control',
    uploadControlId: 'portal-notes-upload-control'
}

export default notesConstants;