import React, { Component } from 'react'
import Label from '../Label'
import guid from '~/helpers/guid'
const $ = window.$;

export default class PicklistControl extends Component {
    render() {
        const fieldName = this.props.control['@datafieldname'];
        this.id = guid();

        if (this.props.showlabel !== 'false' && this.props.label) {
            return (
                <div className='portal-control row portal-control-picklist'>
                    <div className='col-md-4'>
                        <Label label={this.props.label} className='portal-picklist-label' />
                    </div>
                    <div className='col-md-8'>
                        <input
                            id={this.id} name={fieldName + '.Value'}
                            defaultValue={this.props.data[fieldName]}
                            />
                    </div>
                </div>
            );
        }

        return (
            <div className='portal-control row portal-control-picklist'>
                <input
                    id={this.id} name={fieldName + '.Value'}
                    defaultValue={this.props.data[fieldName]}
                    />
            </div>
        );
    }
    componentDidMount() {
        const entity = this.props.entity;
        const control = this.props.control;

        if (!entity || !entity.Name || !control || !control['@id']) {
            return;
        }

        const url = '/api/metadata/GetMetaDataForDropDownList?'
            + 'entityLogicalName=' + entity.Name
            + '&fieldName=' + control['@id'];

        const self = this;

        $('#' + this.id).kendoDropDownList({
            dataTextField: 'Text',
            dataValueField: 'Value',
            dataSource: {
                transport: {
                    read: {
                        dataType: 'json',
                        url: url,
                        type: 'GET'
                    }
                }
            },
            optionLabel: '--',
            dataBound: function () {
                const dropdownlist = $('#' + self.id).data('kendoDropDownList');
                dropdownlist.readonly(self.props.control['@disabled'] === 'true');
            }
        });

        document.getElementById(self.id).onchange = function (e) {
            if (!e.target.value) {
                $('#' + self.id).val(-1);
            }
        }
    }
}