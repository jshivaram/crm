import React, { Component } from 'react'
import guid from '~/helpers/guid'

const $ = window.$;

export default class PostControl extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bothPosts: [],
            autoPosts: [],
            userPosts: []
        };
    }
    render() {
        this.wrapperId = guid();
        const bothPosts = this.state.bothPosts;
        const autoPosts = this.state.autoPosts;
        const userPosts = this.state.userPosts;

        return (
            <div className='posts-wrapper' id={this.wrapperId}>
                <div className='posts-nav row'>
                    <span className='posts-nav-item posts-nav-item-both col-md-1 col-md-offset-1'>Both</span>
                    <span className='posts-nav-item posts-nav-item-auto col-md-1 col-md-offset-1'>Auto posts</span>
                    <span className='posts-nav-item posts-nav-item-user col-md-1 col-md-offset-1'>User posts</span>
                </div>
                <div className='posts-both-container posts-container active-posts'>
                    {getPosts(bothPosts)}
                </div>
                <div className='posts-auto-container posts-container'>
                    {getPosts(autoPosts)}
                </div>
                <div className='posts-user-container posts-container'>
                    {getPosts(userPosts)}
                </div>
            </div>
        );
    }
    componentDidMount() {
        const entityId = this.props.entity.Id;
        const self = this;

        if (!entityId) {
            return;
        }

        const $postsBothContainer = $('#' + this.wrapperId + ' .posts-both-container');
        const $bothBtn = $('#' + this.wrapperId + ' .posts-nav-item-both');

        const $postsAutoContainer = $('#' + this.wrapperId + ' .posts-auto-container');
        $postsAutoContainer.hide();
        const $autoBtn = $('#' + this.wrapperId + ' .posts-nav-item-auto');

        const $postsUserContainer = $('#' + this.wrapperId + ' .posts-user-container');
        $postsUserContainer.hide();
        const $userBtn = $('#' + this.wrapperId + ' .posts-nav-item-user');

        $bothBtn.on('click', () => {
            if (!$bothBtn.hasClass('active-posts')) {
                $bothBtn.addClass('active-posts');
            }

            $postsBothContainer.show();

            $autoBtn.removeClass('active-posts');
            $postsAutoContainer.hide();

            $userBtn.removeClass('active-posts');
            $postsUserContainer.hide();
        });

        $autoBtn.on('click', () => {
            if (!$autoBtn.hasClass('active-posts')) {
                $autoBtn.addClass('active-posts');
            }

            $postsAutoContainer.show();

            $bothBtn.removeClass('active-posts');
            $postsBothContainer.hide();

            $userBtn.removeClass('active-posts');
            $postsUserContainer.hide();
        });

        $userBtn.on('click', () => {
            if (!$userBtn.hasClass('active-posts')) {
                $userBtn.addClass('active-posts');
            }

            $postsUserContainer.show();

            $bothBtn.removeClass('active-posts');
            $postsBothContainer.hide();

            $autoBtn.removeClass('active-posts');
            $postsAutoContainer.hide();
        });

        $.get('/api/posts/getPostsByEntityId?entityId=' + entityId)
            .done(posts => {
                if (!posts) {
                    return;
                }

                const bothPosts = formBothPosts(posts);
                const userPosts = formUserPosts(bothPosts);
                const autoPosts = formAutoPosts(bothPosts);

                self.setState({
                    bothPosts,
                    userPosts,
                    autoPosts,
                });
                console.log(posts);
            })
            .fail(err => console.log('err when get posts', err));
    }
}

function getPosts(posts) {
    const result = posts.map(p => {
        return (
            <div key={guid()} className='post-wrapper'>
                <h4 className='post-title'>{p.title}</h4>
                <p className='post-text'>{p.text}</p>
                <p className='post-modifiedon'>{p.modifiedon}</p>
            </div>
        );
    });

return result;
}

function formBothPosts(posts) {
    
    const result = posts.map(p => {
        let text = '';
        let title = '';
        let sourceType = null;
        let modifiedon = '';

        p.Attributes.forEach(a => {
            const source = p.Attributes.filter(a => a.Key === 'source')[0].Value.Value;
            sourceType = source;

            if (source === 2) {
                if (a.Key === 'text') {
                    text = a.Value;
                }
                if (a.Key === 'createdby') {
                    title = a.Value.Name;
                }
            } else if (source === 1) {
                if (a.Key === 'text') {
                    text = formTextFromXml(a.Value);
                }
                if (a.Key === 'regardingobjectid') {
                    title = a.Value.Name;
                }
            }

            if (a.Key === 'modifiedon') {
                const date = new Date(a.Value);
                const today = new Date();

                if (date.getDay() === today.getDay()
                    && date.getMonth() === today.getMonth()
                    && date.getFullYear() === today.getFullYear()) {
                    modifiedon = 'Today';
                } else {
                    modifiedon = p.FormattedValues.filter(fv => fv.Key === 'modifiedon')[0].Value;
                }
            }
        });

        return {
            title,
            text,
            modifiedon,
            source: sourceType
        }
    });

    return result;
}

function formUserPosts(bothPosts) {
    const userPosts = bothPosts.filter(p => p.source === 2);
    return userPosts;
}

function formAutoPosts(bothPosts) {
    const autoPosts = bothPosts.filter(p => p.source === 1);
    return autoPosts;
}

function formTextFromXml(xml) {
    const $xmlDoc = $($.parseXML(xml));
    const piId = $xmlDoc.find('pi').attr('id');
    const author = $($xmlDoc.find('pi').find('p')[0]).text();

    const items = piId.split('.');
    const entityName = items[0];
    const action = items[1];

    const modifiedAction = action === 'Create' ? 'Created By' : action;

    const text = entityName + ': ' + modifiedAction + ' ' + author;
    return text;
}