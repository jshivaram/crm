import React, { Component } from 'react'
import './style.scss'
import controlTypesFunctions from './controlTypesFunctions.js'

const Steps = require('rc-steps');

const $ = window.$;

export default class ProcessBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: undefined
        };
    }
    render() {
        if (!this.state.data) {
            return (
                <div></div>
            );
        }

        const newsTemplate = this.state.data.map((s, i) => {
            return (
                <Steps.Step
                    key={i}
                    status={s.status}
                    title={s.title}
                    description={s.description}
                    />
            );
        });

        const controlProps = this.state.controlProps;

        const getFormatedValue = () => {

            const formatedValue = this.props.formattedValues[controlProps.DataFieldName];

            if(formatedValue) {
                return formatedValue;
            }

            const nonFormatedValue = this.props.enityAttributes[controlProps.DataFieldName];

            if (!nonFormatedValue) {
                return '';
            }

            const classId = controlProps.ClassId;
            if(!classId) {
                return nonFormatedValue;
            }

            const valueFunc = controlTypesFunctions[classId];
            if(!valueFunc) {
                return nonFormatedValue;
            }

            const result = valueFunc(nonFormatedValue);
            return result;
        };

        const creteStepsControl = () => {

            if (!controlProps) {
                return (<span></span>);
            }

            return (
                <div className='steps-control-wrapper'>
                    <span className='steps-control-label'>
                        {controlProps.ControlDisplayName}
                    </span>
                    <span className='steps-control-value'>
                        {getFormatedValue()}
                    </span>
                </div>
            );
        };

        return (
            <div className='steps-wrapper'>
                <Steps current={1}>
                    {newsTemplate}
                </Steps>
                {creteStepsControl()}
            </div>
        );
    }

    componentDidMount() {
        var entityId = this.props.entityId;
        if (!entityId) {
            return;
        }

        var url = '/api/metadata/GetProcesStageByEntityId?'
            + 'entityId=' + entityId;

        let self = this;
        $.ajax({
            url: url,
            method: 'GET',
            success: function (data) {
                if (data.length == 0)
                    return;
                data = JSON.parse(data);
                var steps = [];
                for (var i = 0; i < data.length - 1; i++) {
                    steps.push(
                        {
                            status: data[i].status,
                            title: data[i].stageName
                        }
                    );

                }
                const controlProps = data[data.length - 1];
                steps.push({});
                self.setState({ data: steps, controlProps: controlProps });
            },
            error: function (err) {
                console.log(err);
            }
        });

    }

}