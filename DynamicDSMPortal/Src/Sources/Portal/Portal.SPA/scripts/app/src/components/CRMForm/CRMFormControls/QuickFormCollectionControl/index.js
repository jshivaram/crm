import React, { Component } from 'react'
import QuickFormControl from '../QuickFormControl'
import guid from '~/helpers/guid'

const $ = window.$;

export default class QuickFormCollectionControl extends Component {
    render() {
        let entityId;

        const dataFieldName = this.props.control['@datafieldname'];
        const dataField = this.props.objectAttributes[dataFieldName];

        if (dataField) {
            entityId = dataField.Id;
        }

        let xml = this.props.control.parameters.QuickForms;
        let xmlDoc = $.parseXML(xml);
        let $xmlDoc = $(xmlDoc);

        let quickFormIds = $xmlDoc.find('QuickFormIds QuickFormId');

        let $quickFormId;
        let quickForms = $.map(quickFormIds, function(quickFormId) {
            $quickFormId = $(quickFormId);

            let entityName = $quickFormId.attr('entityname');
            let formId = $quickFormId.text();

            return (
                <QuickFormControl
                    key={guid()}
                    entityId={entityId}
                    entityName={entityName}
                    formId={formId} >
                </QuickFormControl>
            );
        });

        return (
            <div className='portal-control row'>
                { quickForms }
            </div>
        );
    }
}