import React, { Component } from 'react'
import CRMFormComponents from '~/components/CRMForm/CRMFormComponents'

export default class QuickForm extends Component {
    render() {
        let data = this.props.data;

        if (!data) {
            return (<div></div>);
        }

        if (!Array.isArray(data.tabs)) {
            let tab = data.tabs;
            return (
                <CRMFormComponents.Tab
                    entity={data.entity}
                    tab={tab} key={tab['@id']}
                    data={data.enityAttributes}
                    objectAttributes={data.objectAttributes}>
                </CRMFormComponents.Tab>
            );
        }

        var tabNodes = data.tabs.map(tab => {
            return (
                <CRMFormComponents.Tab
                    entity={data.entity}
                    tab={tab} key={tab['@id']}
                    data={data.enityAttributes}
                    objectAttributes={data.objectAttributes}>
                </CRMFormComponents.Tab>
            );
        });

        return (
            <div className='portal-control row'>
                { tabNodes }
            </div>
        );
    }
}