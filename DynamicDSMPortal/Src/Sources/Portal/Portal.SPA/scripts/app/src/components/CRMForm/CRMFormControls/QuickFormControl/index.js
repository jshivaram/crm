import React, { Component } from 'react'
import FormViewer from '~/middlewares/FormViewer'
import QuickForm from './QuickForm'
import guid from '~/helpers/guid'

const $ = window.$;

export default class QuickFormControl extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: undefined
        };

        this.formId = guid();
    }
    render() {
        return (
            <div id={this.formId} className='quick-form'>
                <QuickForm
                    key={guid()}
                    data={this.state.data}
                    />
            </div>
        );
    }
    componentDidMount() {

        let formName = undefined;
        let isQuickForm = true;

        let rerenderQuickForm = entityId => {
            FormViewer.getFormStructureWithData(
                entityId,
                this.props.entityName,
                this.props.formId,
                formName,
                isQuickForm
            ).then(data => {
                this.setState({ data: data });
            }).catch(error => {
                throw error;
            });
        }

        rerenderQuickForm(this.props.entityId);
        const entityName = this.props.entityName;

        const quickFormLookupSelector = 'select.crm-lookup-select.' + entityName
        let $quickFormLookup = $(quickFormLookupSelector);

        if (!$quickFormLookup.length) {
            const lookupTimer = setInterval(function () {
                if(entityName === 'contact') {
                    debugger;
                }

                $quickFormLookup = $(quickFormLookupSelector);
                if (!$quickFormLookup.length) {
                    return;
                }

                $('quickFormLookup').unbind('change');

                $quickFormLookup.on('change', function () {
                    debugger;
                    rerenderQuickForm(this.value);
                });

                clearInterval(lookupTimer);
            }, 100);
            return;
        }

        $quickFormLookup.on('change', function () {
            rerenderQuickForm(this.value);
        });
    }
}