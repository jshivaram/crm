import React, { Component } from 'react'
import guid from '~/helpers/guid'
import Label from '../Label'

export default class RadioControl extends Component {
    render() {
        let fieldName = this.props.control['@datafieldname'];
        fieldName = fieldName ? fieldName : '';

        const yesId = guid();
        const noId = guid();

        const createRadioButtonWrapper = (className) => {
            return (
                <div key={guid()} className={className}>
                    <input type='radio' name={fieldName} id={yesId} className='k-radio' defaultValue='true' />
                    <label className='k-radio-label' htmlFor={yesId}>Yes</label>
                    <input type='radio' name={fieldName} id={noId} className='k-radio' defaultValue='false' />
                    <label className='k-radio-label' htmlFor={noId}>No</label>
                </div>
            );
        };

        if (this.props.showlabel !== 'false' && this.props.label) {
            return (
                <div className='portal-control row'>
                    <div className='col-md-6'>
                        <Label label={this.props.label}
                            className='portal-textbox-label' />
                    </div>
                    { createRadioButtonWrapper('col-md-6 radio-control-wrapper')}
                </div>
            );
        }

        return createRadioButtonWrapper('radio-control-wrapper');
    }
}