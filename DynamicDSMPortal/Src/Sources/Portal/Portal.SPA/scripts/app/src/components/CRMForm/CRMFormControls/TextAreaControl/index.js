import React, { Component } from 'react'
import Label from '../Label'

export default class TextAreaControl extends Component {
    render() {
        const fieldName = this.props.control['@datafieldname'];
        const isReadonly = this.props.control['@disabled'] === 'true';

        const createTextArea = () => {
            return (
                <textarea
                    className='k-textbox portal-control-textarea'
                    name={this.props.control['@datafieldname']}
                    defaultValue={this.props.data[fieldName]}
                    readOnly={isReadonly}>
                </textarea>
            );
        };

        if (this.props.showlabel !== 'false' && this.props.label) {
            return (
                <div className='portal-control row'>
                    <div className='col-md-4'>
                        <Label label={this.props.label}
                            className='portal-textbox-label' />
                    </div>
                    <div className='col-md-8'>
                        {createTextArea()}
                    </div>
                </div>
            );
        }

        return (
            <div className='portal-control row'>
                {createTextArea()}
            </div>
        );
    }
}