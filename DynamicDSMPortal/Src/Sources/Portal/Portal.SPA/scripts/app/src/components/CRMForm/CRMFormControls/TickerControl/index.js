import React, { Component } from 'react'
import Label from '../Label'

export default class TickerControl extends Component {
    render() {

        const fieldName = this.props.control['@datafieldname'];
        const isReadonly = this.props.control['@disabled'] === 'true';

        const createInput = () => {
            return (
                <input
                    className='k-textbox tickerControl'
                    type='text'
                    name={this.props.control['@datafieldname']}
                    defaultValue={this.props.data[fieldName]}
                    readOnly={isReadonly} />
            );
        };

        if (this.props.showlabel !== 'false' && this.props.label) {
            return (
                <div className='portal-control row'>
                    <div className='col-md-4'>
                        <Label label={this.props.label} className='portal-ticker-label' />
                    </div>
                    <div className='col-md-8'>
                        {createInput()}
                    </div>
                </div>
            );
        }

        return (
            <div className='portal-control row'>
                {createInput()}
            </div>
        );
    }
}