import React, { Component } from 'react'
import CRMFormControls from '../../CRMFormControls'

export default class UrlControl extends Component {
    render() {
        const fieldName = this.props.control['@datafieldname'];
        const isReadonly = this.props.control['@disabled'] === 'true';

        const createInpit = () => {
            return (
                <input className='k-textbox'
                    type='url'
                    name={this.props.control['@datafieldname']}
                    defaultValue={this.props.data[fieldName]}
                    readOnly={isReadonly} />
            );
        };


        if (this.props.showlabel !== 'false' && this.props.label) {
            return (
                <div className='portal-control row'>
                    <div className='col-md-4'>
                        <CRMFormControls.Label label={this.props.label} className='portal-url-label' />
                    </div>
                    <div className='col-md-8'>
                        {createInpit()}
                    </div>
                </div>
            );
        }

        return (
            <div className='portal-control row'>
                {createInpit()}
            </div>
        );
    }
}