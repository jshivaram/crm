import React, { Component } from 'react';
import guid from '~/helpers/guid';
import PostControl from '../PostControl';

const $ = window.$;

export default class WebResourceHtmlControl extends Component {
    render() {
        this.controlId = guid();
        this.webResControlId = this.props.control['@id'];
        this.webResourceId = this.props.control.parameters.WebResourceId;

        if (!this.webResourceId && this.webResControlId) {
            return (
                <div className='portal-control row portal-web-resource-html-control'
                    id={this.controlId}>
                    <PostControl entity={this.props.entity} />
                </div>
            );
        }

        return (
            <div className='portal-control row portal-web-resource-html-control'
                id={this.controlId}>

            </div>
        );
    }
    componentDidMount() {
        componentDidMountAsync(this);
    }
}

async function componentDidMountAsync(componentContext) {
    let webResourceId = componentContext.webResourceId;

    if (!webResourceId) {
        return;
    }
    webResourceId = webResourceId.replace('{', '').replace('}', '');

    let html = await $.get('/api/webresourse/getById?id=' + webResourceId);
    $('#' + componentContext.controlId).html(html);
}