import React, { Component } from 'react'

const $ = window.$;

export default class WebResourceImageControl extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imageSrc: undefined
        };
    }

    render() {        
        if (this.state.imageSrc != undefined) {
            return (
                <div>
                    <img src={this.state.imageSrc} />
                </div>
            );
        }
        return (
            <div></div>
        );

    }

    componentDidMount() {
        var imageName = this.props.control.parameters.Url;
        let self = this;
        $.ajax({
            url: '/api/crm/GetUrl',
            method: 'GET',
            success: function (data) {
                var imageSrc = data.Url + 'WebResources/' + imageName;
                self.setState({ imageSrc: imageSrc });

            },
            error: function (err) {
                console.log(err);
            }
        });
    }

}



