import React, { Component } from 'react'
import NotesControl from '../NotesControl'
import GridControl from '../GridControl'

export default class WrapperGridControl extends Component {
    render() {
        if (this.props.control.parameters.TargetEntityType === 'ddsm_attachmentdocument') {
            return (<NotesControl
                control={this.props.control}
                label={this.props.label}
                data={this.props.data}
                objectAttributes={this.props.objectAttributes}
                entity={this.props.entity}
                showlabel={this.props.showlabel}
                dashboard={this.props.dashboard}
                canEditFields={this.props.canEditFields}
                formattedValues={this.props.formattedValues}
                dateFormat={this.props.dateFormat}
                formName = {this.props.formName}
            />);
        }
        else {
            return (<GridControl
                control={this.props.control}
                label={this.props.label}
                data={this.props.data}
                objectAttributes={this.props.objectAttributes}
                entity={this.props.entity}
                showlabel={this.props.showlabel}
                dashboard={this.props.dashboard}
                canEditFields={this.props.canEditFields}
                formattedValues={this.props.formattedValues}
                dateFormat={this.props.dateFormat}
                formName = {this.props.formName}
            />);
        }
    }
}