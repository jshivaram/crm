import EmailAddressControl from './EmailAddressControl';
import Label from './Label';
import LookupControl from './LookupControl';
import TextAreaControl from './TextAreaControl';
import TextBoxControl from './TextBoxControl';
import IntegerControl from './IntegerControl';
import DecimalControl from './DecimalControl';
import FloatControl from './FloatControl';
import TickerControl from './TickerControl';
import UrlControl from './UrlControl';
import PicklistControl from './PicklistControl';
import WebResourceImageControl from './WebResourceImageControl';
import MoneyControl from './MoneyControl';
import QuickFormCollectionControl from './QuickFormCollectionControl';
import QuickFormControl from './QuickFormControl';
import GridControl from './GridControl';
import ApplicationMeasureTemplatesGridControl from './ApplicationMeasureTemplatesGridControl';
import WebResourceHtmlControl from './WebResourceHtmlControl';
import DateTimeControl from './DateTimeControl';
import RadioControl from './RadioControl';
import CheckBoxControl from './CheckBoxControl';
import ChartControl from './ChartControl';


let controlDictionary = {
    //'{f93a31b2-99ac-4084-8ec2-d4027c31369a}': AccessPrivilegeControl,
    //'{3f4e2a56-f102-4b4d-ab9c-f1574ca5bfda}': AccessTeamEntityPicker,
    //'{c72511ab-84e5-4fb7-a543-25b4fc01e83e}': ActivitiesContainerControl,
    //'{6636847d-b74d-4994-b55a-a6faf97ecea2}': ActivitiesWallControl,
    //'{f02ef977-2564-4b9a-b2f0-df083d8a019b}': ArticleContentControl,
    //'{00ad73da-bd4d-49c6-88a8-2f4f4cad4a20}': ButtonControl,
    '{b0c6723a-8503-4fd7-bb28-c8a06ac933c2}': CheckBoxControl,
    //'{db1284ef-9ffc-4e99-b382-0cc082fe2364}': CompositionLinkControl,
    //'{3246f906-1f71-45f7-b11f-d7be0f9d04c9}': ConnectionControl,
    //'{821acf1a-7e46-4a0c-965d-fe14a57d78c7}': ConnectionRoleObjectTypeListControl,
    //'{4168a05c-d857-46af-8457-5bb47eb04ea1}': CoverPagePicklistControl,
    //'{f9a8a302-114e-466a-b582-6771b2ae0d92}': CustomControl,
    '{5b773807-9fb2-42db-97c3-7a91eff8adff}': DateTimeControl,
    '{c3efe0c3-0ec6-42be-8349-cbd9079dfd8e}': DecimalControl,
    //'{aa987274-ce4e-4271-a803-66164311a958}': DurationControl,
    //'{6896f004-b17a-4202-861e-8b7ea2080e0b}': DynamicPropertyListControl,
    '{ada2203e-b4cd-49be-9ddf-234642b43b52}': EmailAddressControl,
    //'{6f3fb987-393b-4d2d-859f-9d0f0349b6ad}': EmailBodyControl,
    //'{f4c16eca-ca81-4e39-9448-834b8378721e}': ErrorStatusControl,
    '{0d2c745a-e5a8-4c8f-ba63-c6d3bb604660}': FloatControl,
    //'{fd2a7985-3187-444e-908d-6624b21f69c0}': FrameControl,
    '{e7a81278-8635-4d9e-8d4d-59480b391c5b}': [GridControl, ChartControl],
    //'{5546e6cd-394c-4bee-94a8-4425e17ef6c6}': HiddenInputControl,
    '{c6d124ca-7eda-4a60-aea9-7fb8d318b68f}': IntegerControl,
    //'{a62b6fa9-169e-406c-b1aa-eab828cb6026}': KBViewerControl,
    //'{5635c4df-1453-413e-b213-e81b65411150}': LabelControl,
    //'{671a9387-ca5a-4d1e-8ab7-06e39ddcf6b5}': LanguagePicker,
    //'{dfdf1cde-837b-4ac9-98cf-ac74361fd89d}': LinkControl,
    '{270bd3db-d9af-4782-9025-509e298dec0a}': LookupControl,
    //'{b634828e-c390-444a-afe6-e07315d9d970}': MailMergeLanguagePicker,
    //'{91dc0675-c8b9-4421-b1e0-261cebf02bac}': MapLinkControl,
    //'{62b0df79-0464-470f-8af7-4483cfea0c7d}': MapsControl,
    '{533b9e00-756b-4312-95a0-dc888637ac78}': MoneyControl,
    //'{06375649-c143-495e-a496-c962e5b4488e}': NotesControl,
    //'{cbfb742c-14e7-4a17-96bb-1a13f7f64aa2}': PartyListControl,
    //'{8c10015a-b339-4982-9474-a95fe05631a5}': PhoneNumberControl,
    '{3ef39988-22bb-4f0b-bbbe-64b5a3748aee}': PicklistControl,
    //'{2305e33a-bad3-4022-9e15-1856cf218333}': PicklistLookupControl,
    //'{5d68b988-0661-4db2-bc3e-17598ad3be6c}': PicklistStatusControl,
    //'{06e9f7af-1f54-4681-8eec-1e21a1ceb465}': ProcessControl,
    '{5c5600e0-1d6e-4205-a272-be80da87fd42}': QuickFormCollectionControl,
    '{69af7dca-2e3b-4ee7-9201-0da731dd2413}': QuickFormControl,
    '{67fac785-cd58-4f9f-abb3-4b7ddc6ed5ed}': RadioControl,
    //'{f3015350-44a2-4aa0-97b5-00166532b5e9}': RegardingControl,
    //'{163b90a6-eb64-49d2-9df8-3c84a4f0a0f8}': RelatedInformationControl,
    //'{5f986642-5961-4d9f-ab5e-643d71e231e9}': RelationshipRolePicklist,
    //'{a28f441b-916c-4865-87fd-0c5d53bd59c9}': ReportControl,
    //'{e616a57f-20e0-4534-8662-a101b5ddf4e0}': SearchWidget,
    //'{86b9e25e-695e-4fef-ac69-f05cfa96739c}': SocialInsightControl,
    '{e0dece4b-6fc8-4a8f-a065-082708572369}': TextAreaControl,
    '{4273edbd-ac1d-40d3-9fb2-095c621b552d}': TextBoxControl,
    '{1e1fc551-f7a8-43af-ac34-a8dc35c7b6d4}': TickerControl,
    //'{9c5ca0a1-ab4d-4781-be7e-8dfbe867b87e}': TimerControl,
    //'{7c624a0b-f59e-493d-9583-638d34759266}': TimeZonePicklistControl,
    '{71716b6c-711e-476c-8ab8-5d11542bfb47}': UrlControl,
    '{9fdf5f91-88b1-47f4-ad53-c11efc01a01d}': WebResourceHtmlControl,
    '{587cdf98-c1d5-4bde-8473-14a0bc7644a7}': WebResourceImageControl
};

export default {
    EmailAddressControl: EmailAddressControl,
    GridControl: GridControl,
    Label: Label,
    LookupControl: LookupControl,
    TextAreaControl: TextAreaControl,
    TextBoxControl: TextBoxControl,
    TickerControl: TickerControl,
    UrlControl: UrlControl,
    controlDictionary: controlDictionary,
    ApplicationMeasureTemplatesGridControl: ApplicationMeasureTemplatesGridControl,
    WebResourceHtmlControl: WebResourceHtmlControl,
    DateTimeControl: DateTimeControl,
    RadioControl: RadioControl,
    CheckBoxControl: CheckBoxControl,
    ChartControl: ChartControl
};