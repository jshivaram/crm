import React, { Component } from 'react'
import Form from './CRMFormComponents/Form'
import Loader from '../Loader'
import FormViewer from '../../middlewares/FormViewer'
import guid from '~/helpers/guid'
import { browserHistory } from 'react-router'
const $ = window.$;

export default class CRMForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: undefined
        };
    }
    componentWillMount() {
        Loader.show($('#loader'), $('#wrapper'));
    }
    render() {
        $('head link[crm-application]').remove();
        
        if (isApplicationForUnregester() || isApplicationForRegisters()) {

            
            let theme = $('#themes').val();
            if (!theme) {
                theme = 'e1';
            }

            if (theme === 'e1') {
                $('head').append('<link crm-application href="/Content/Applications/application-e1.css" rel="stylesheet" />');
            } else {
                $('head').append('<link crm-application href="/Content/Applications/application.css" rel="stylesheet" />');
            }
        }

        return (
            <div className='row'>
                <div className='row'>
                    <div className='col-md-3'>
                        <select id='crm-portal-form-picker'></select>
                    </div>
                </div>
                <Form
                    key={guid()}
                    data={this.state.data}
                    />
            </div>
        );
    }
    componentDidMount() {
        let urlForGetFormsList = '/api/form/GetForms?'
            + 'entityLogicalName=' + this.props.params.entityName;

        var entityLogicalName = this.props.params.entityName;

        $('#crm-portal-form-picker').kendoDropDownList({
            dataTextField: 'Value',
            dataValueField: 'Key',
            dataSource: {
                transport: {
                    read: {
                        dataType: 'json',
                        url: urlForGetFormsList,
                        type: 'GET'
                    }
                },
                error: function (err) {
                    browserHistory.push('/');

                    var notification = $('#notification').data('kendoNotification');
                    notification.show({
                        title: 'Error',
                        message: err.errorThrown,
                    }, 'error');
                }
            },
            change: this.onChangeHandler(this),
            dataBound: this.dataBoundHandler(this, entityLogicalName)
        });
    }
    onChangeHandler(self) {
        return function () {
            Loader.show($('#loader'), $('#wrapper'));
            self.LoadData(self);

            changeBodyClasses(self.text());
        }
    }
    dataBoundHandler(self, entityLogicalName) {
        return function () {
            var url = '/api/form/GetDefaultFormId?entityLogicalName=' + entityLogicalName;

            $.get(url).done(function (defaultFormId) {
                if (defaultFormId) {
                    var dropdownList = $('#crm-portal-form-picker').data('kendoDropDownList');
                    dropdownList.value(defaultFormId);
                }
            }).fail(function (err) {
                console.log(err);
            }).always(function () {
                self.LoadData(self);
            });
        }
    }
    LoadData(self) {
        let formPicker = $('#crm-portal-form-picker').data('kendoDropDownList');

        FormViewer.getFormStructureWithData(
            self.props.params.entityId,
            self.props.params.entityName,
            formPicker.value()
        ).then(data => {
            self.setState({ data: data });

            changeBodyClasses(formPicker.text());

            Loader.hide($('#loader'), $('#wrapper'));
        }).catch(error => {
            Loader.hide($('#loader'), $('#wrapper'));

            alert(error.responseText);

            throw error;
        });
    }
}

function changeBodyClasses(formName) {
    let href = window.location.href.toLowerCase();
    if (href.indexOf('/ddsm_application') === -1 || href.indexOf('/form') === -1) {
        return;
    }

    let applicationFormClassName = formName.toLowerCase().split(' ').join('-');

    $(document.body).attr('class', 'ddsm-application-wrapper-body ' + applicationFormClassName);
}

function isApplicationForUnregester() {
    return document.location.href.indexOf('/application/') !== -1;
}

function isApplicationForRegisters() {
    return document.location.href.indexOf('/form/') !== -1 && document.location.href.indexOf('/ddsm_application') !== -1;
}