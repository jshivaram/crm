import React, { Component } from 'react'
import DashboardLayout from '../CRMForm/CRMFormComponents/DashboardLayout'
import Loader from '../Loader'
import DashboardViewer from '../../middlewares/DashboardViewer'
import guid from '~/helpers/guid'

const $ = window.$;

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      layout: undefined
    };
  }
  componentWillMount() {
    Loader.show($('#loader'), $('#wrapper'));
  }
  render() {
    return (
      <div className='row'>
        <div className='row'>
          <div className='col-md-3'>
            <input id='crm-portal-form-picker' />
          </div>
        </div>
        <DashboardLayout
          key={guid()}
          layout={this.state.layout}
          dashboard={true}
          />
      </div>
    );

  }
  componentDidMount() {
    let urlForGetFormsList = '/api/form/GetDashboards';

    $('#crm-portal-form-picker').kendoDropDownList({
      dataTextField: 'Text',
      dataValueField: 'Value',
      dataSource: {
        transport: {
          read: {
            dataType: 'json',
            url: urlForGetFormsList,
            type: 'GET'
          }
        }
      },
      change: this.onChangeHandler(this),
      dataBound: this.dataBoundHandler(this)
    });
  }
  onChangeHandler(self) {
    return function () {
      Loader.show($('#loader'), $('#wrapper'));
      self.LoadData(self);
    }
  }
  dataBoundHandler(self) {
    return function () {
      const formPicker = $('#crm-portal-form-picker').data('kendoDropDownList');

      $.get('/api/form/GetDefaultDashboardId')
        .done(id => {
          if (id) {
            formPicker.value(id);
            self.LoadData(self, id);
          }
        })
        .fail(self.LoadData(self));
    }
  }
  LoadData(self, dashboardId) {
    const formPicker = $('#crm-portal-form-picker').data('kendoDropDownList');

    DashboardViewer.getFormStructureWithData(
      'systemforms',
      dashboardId ? dashboardId : formPicker.value()
    ).then(data => {
      self.setState({ layout: data });

      Loader.hide($('#loader'), $('#wrapper'));
    }).catch(error => {
      Loader.hide($('#loader'), $('#wrapper'));

      alert(error.responseText);

      throw error;
    });
  }
}