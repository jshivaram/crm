import React, { Component } from 'react'
import Loader from '~/components/Loader'
import { browserHistory } from 'react-router'

const $ = window.$;

export default class DataUploaderForm extends Component {
    render() {
        return (
            <form id='crm-portal-datauploader-form' className='col-md-6 col-md-offset-3'>
                <div className='well'>
                    <div className='row'>
                        <div className='col-md-4'>
                            Type of Uploaded Data
                        </div>
                        <div className='col-md-8'>
                            <input id='crm-portal-type-uploaded-data' name='typeUploadedData' />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-md-4'>
                            File
                        </div>
                        <div className='col-md-8'>
                            <input id='crm-portal-file-uploader' name='file' type='file' />
                        </div>
                    </div>
                    <div className='row'>
                        <input onClick={this.onClickHandler} type='button' className='k-button' value='Submit' />
                    </div>
                </div>
            </form>
        );
    }
    componentDidMount() {
        let urlForTypeUploadedData = '/api/metadata/GetMetaDataForDropDownList?'
            + 'entityLogicalName=ddsm_datauploader'
            + '&fieldName=ddsm_typeofuploadeddata';

        $('#crm-portal-type-uploaded-data').kendoDropDownList({
            dataTextField: 'Text',
            dataValueField: 'Value',
            dataSource: {
                transport: {
                    read: {
                        dataType: 'json',
                        url: urlForTypeUploadedData,
                        type: 'GET'
                    }
                }
            }
        });

        $('#crm-portal-file-uploader').kendoUpload({
            multiple: false
        });
    }
    onClickHandler() {
        let formData = new FormData();

        let typeUploadedData = $('#crm-portal-type-uploaded-data').val();

        let $file = $('#crm-portal-datauploader-form')[0]['file'][0];

        if(!$file) {
            var notification = $('#notification').data('kendoNotification');
            notification.show({
                title: 'Error',
                message: 'File is not selected',
            }, 'error');

            return;
        }

        let file = $file.files[0];

        formData.append('typeUploadedData', typeUploadedData);
        formData.append('file', file, file.name);

        Loader.show($('#loader'), $('#wrapper'));

        let urlForUploadData = '/api/datauploader/CreateDataUploaderRecord';

        $.ajax({
            type: 'POST',
            url: urlForUploadData,
            contentType: false,
            processData: false,
            data: formData,
            success: function () {
                Loader.hide($('#loader'), $('#wrapper'));

                browserHistory.push('/entities/ddsm_datauploader');

                let notification = $('#notification').data('kendoNotification');
                notification.show({
                    message: 'Success',
                }, 'upload-success');
            },
            error: function () {
                
                Loader.hide($('#loader'), $('#wrapper'));

                var notification = $('#notification').data('kendoNotification');
                notification.show({
                    title: 'Error',
                    message: 'Unable to send data',
                }, 'error');
            }
        });
    }
}