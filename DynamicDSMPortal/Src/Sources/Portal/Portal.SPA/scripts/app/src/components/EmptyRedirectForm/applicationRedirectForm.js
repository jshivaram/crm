import React, { Component } from 'react'
import constants from '../MainNav/DropDownApplications/constants'
import customStorage from '../../customStore'

import './style.css'

const $ = window.$;

export default class ApplicationRedirectForm extends Component {
    render() {       
        return <span></span>;
    }


    componentDidMount() {
        this.redirectToApplicationForm();
    }

    redirectToApplicationForm() {
        const portalConfigName = this.props.params.applicationConfigDisplayName;
        //TO.DO Create popup
        $.get(`/api/application/GetApplicationConfigurationNameByName?applicationConfigurationName=${portalConfigName}`,
            function (response) {
                let storageObject = {
                    applicationId: response.Value,
                    configId: response.ConfigId,
                    applicationName: response.Text
                };

                customStorage.set(constants.portalAppConfigStorageKey, storageObject);
                window.location.replace('/application');
            });
    }
}