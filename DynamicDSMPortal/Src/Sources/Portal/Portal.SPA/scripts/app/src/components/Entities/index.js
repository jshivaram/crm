import React, { Component } from 'react'
import Grid from '~/components/Grid'
import guid from '~/helpers/guid.js'

const $ = window.$;

export default class Entities extends Component {
  render() {
    // Reset DropDownList for applications
    let app = $('#crm-portal-application').data('kendoDropDownList');
    if (app) {
      app.value('');
    }

    return (
      <Grid key={guid()} entityName={this.props.params.entityName} />
    );
  }
}