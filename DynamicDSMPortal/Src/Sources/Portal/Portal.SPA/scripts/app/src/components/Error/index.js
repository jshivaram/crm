import React, { Component } from 'react'

export default class Login extends Component {
    render() {
        return (
            <div className='alert alert-danger'>{this.props.message}</div>
        );
    }
}