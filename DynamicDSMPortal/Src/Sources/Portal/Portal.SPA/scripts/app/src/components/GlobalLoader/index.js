import guid from '~/helpers/guid'
import _ from 'lodash'

const kendo = window.kendo;
const $ = window.$;
const $root = $('#root');

const loadersIdlList = [];

const GlobalLoader = function (hideRoot) {
    this._hideRoot = true;

    if(!hideRoot) {
        this._hideRoot = false;
    }

    return this;
};

GlobalLoader.prototype.show = function () {
    this._loaderId = guid();
    loadersIdlList.push(this._loaderId);

    if(this._hideRoot) {
        $root.hide();
    }

    $root.after(`<div id=${this._loaderId} style={z-index: 1}></div>`);
    kendo.ui.progress($('#' + this._loaderId), true);
};

GlobalLoader.prototype.hide = function (miliseconds) {
    const hideLoader = () => {
        kendo.ui.progress($('#' + this._loaderId), false);
        $(`div#${this._loaderId}`).remove();
        _.pull(loadersIdlList, this._loaderId);

        if (loadersIdlList.length === 0 && this._hideRoot) {
            $root.show();
        }
    };

    if (!miliseconds) {
        hideLoader();
        return;
    }

    setTimeout(function () {
        hideLoader();
    }, miliseconds);
};


export default GlobalLoader;