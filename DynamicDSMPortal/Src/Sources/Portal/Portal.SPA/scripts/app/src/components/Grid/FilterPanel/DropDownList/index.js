import React, { Component } from 'react'
import Loader from '~/components/Loader'
import ToolsForGrid from '~/components/Grid/ToolsForGrid.js'
import { browserHistory } from 'react-router'

const $ = window.$;

export default class DropdownlistForView extends Component {
    render() {
        return (
            <input id='portal-dropdownlistForView' />
        );
    }
    componentDidMount() {
        var url = '/api/view/GetViewList?entityLogicalName='
            + this.props.entityName;

        let entityName = this.props.entityName;

        Loader.show($('#loader'), $('#wrapper'));

        $('#portal-dropdownlistForView').kendoDropDownList({
            dataTextField: 'Text',
            dataValueField: 'Value',
            dataSource: {
                transport: {
                    read: {
                        dataType: 'json',
                        url: url,
                        type: 'GET'
                    }
                },
                error: function (err) {
                    browserHistory.push('/');

                    var notification = $('#notification').data('kendoNotification');
                    notification.show({
                        title: 'Error',
                        message: err.errorThrown,
                    }, 'error');
                }
            },
            change: function (e) {
                let viewId = e.sender._old;

                let urlForGrid = '/api/entity?entityLogicalName='
                    + entityName
                    + '&viewId=' + viewId;

                let hideLoader = function () {
                    Loader.hide($('#loader'), $('#wrapper'));
                }

                ToolsForGrid.recreate(urlForGrid, entityName, viewId, hideLoader);
            },
            dataBound: function (e) {
                let viewId = e.sender._old;

                var viewIdPromise = $.get('/api/view/GetDefaultViewId?entityLogicalName=' + entityName);

                viewIdPromise.done(function (defaultViewId) {
                    if (defaultViewId) {
                        viewId = defaultViewId;
                        var dropdown = $('#portal-dropdownlistForView').data('kendoDropDownList');
                        dropdown.value(viewId);
                    }
                }).fail(function (err) {
                    console.log(err);
                }).always(function () {

                    let hideLoader = function () {
                        Loader.hide($('#loader'), $('#wrapper'));
                    }

                    let urlForGrid = '/api/entity?entityLogicalName='
                        + entityName
                        + '&viewId=' + viewId;
                    ToolsForGrid.recreate(urlForGrid, entityName, viewId, hideLoader);
                });

            }
        });
    }
}