import React, { Component } from 'react'
import ToolsForGrid from '~/components/Grid/ToolsForGrid.js'

const $ = window.$;

export default class Search extends Component {
    render() {
        let entityName = this.props.entityName;
        return (
            <div className='input-group searchForGrid'>
                <input type='text' className='k-textbox' 
                    placeholder='Search' name='srch-term'
                    id='search-text-for-grid'
                    onKeyDown={this.keyDownHandler(entityName)} />
                <div className='input-group-btn'>
                    <button className='k-button' 
                        type='submit' onClick={this.btnSearchHandler(entityName)}>
                        <i className='k-font-icon k-i-search'></i>
                    </button>
                </div>
            </div>
        );
    }
    btnSearchHandler(entityName) {
        return function () {
            var searchText = $('#search-text-for-grid').val();

            var grid = $('#grid').data('kendoGrid');
            var url = grid.dataSource.transport.options.read.url;

            var i = url.indexOf('&searchText');
            if (i !== -1) {
                url = url.slice(0, i);
            }

            url += '&searchText=' + searchText;

            var viewId = $('#portal-dropdownlistForView').data('kendoDropDownList').value();

            ToolsForGrid.recreate(url, entityName, viewId, function(){});
        }
    }
    keyDownHandler(entityName) {
        return function (e) {
            if (e.keyCode !== 13) {
                return;
            }

            var searchText = e.target.value;

            var grid = $('#grid').data('kendoGrid');
            var url = grid.dataSource.transport.options.read.url;

            var i = url.indexOf('&searchText');
            if (i !== -1) {
                url = url.slice(0, i);
            }

            url += '&searchText=' + searchText;

            var viewId = $('#portal-dropdownlistForView').data('kendoDropDownList').value();

            ToolsForGrid.recreate(url, entityName, viewId, function(){});
        }
    }
}