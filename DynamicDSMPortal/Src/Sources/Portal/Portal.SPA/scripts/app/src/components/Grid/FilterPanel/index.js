import React, { Component } from 'react'
import DropDownList from './DropDownList'
import Search from './Search'

export default class FilterPanel extends Component {
    render() {
        return (
            <div className='row filterPanelForGrid'>
                <div className='col-md-3'>
                    <DropDownList entityName={this.props.entityName} />
                </div>
                <div className='col-md-3 col-md-offset-6'>
                    <Search entityName={this.props.entityName} />
                </div>
            </div>
        );
    }
}