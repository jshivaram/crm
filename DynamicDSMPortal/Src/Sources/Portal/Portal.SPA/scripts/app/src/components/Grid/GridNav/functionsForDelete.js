import Loader from '~/components/Loader'
const $ = window.$;

export default {
    getSelectedGridItems: function () {
        const entityGrid = $('#grid').data('kendoGrid');
        const rows = entityGrid.select();
        return $.map(rows, function (row) {
            const selectedItem = entityGrid.dataItem(row);
            return selectedItem;
        });
    },
    getIdArrFromGridItems: function (gridItems, entityName) {
        return $.map(gridItems, function (item) {
            return item[entityName + 'id'];
        });
    },
    deleteSelectedAccounts: function (idArr, entityName) {
        const self = this;

        const strParam = this.createStringFromArray('idArr', idArr);
        $.ajax({
            url: '/api/entity?entityLogicalName=' + entityName + '&' + strParam,
            type: 'DELETE',
            success: function () {
                self.afterDelete();
            }
        });
    },
    deleteSelectedAccount: function (id, entityName) {
        const self = this;

        $.ajax({
            url: '/api/entity?entityLogicalName=' + entityName + '&id=' + id,
            type: 'DELETE',
            success: function () {
                self.afterDelete();
            },
            error: function (err) {
                console.log(err);

                if (err.statusText === 'The object you tried to delete is associated with another object and cannot be deleted.') {
                    showErrorAlert('The object you tried to delete is associated with another object and cannot be deleted.');
                }

                self.afterDelete();
            }
        });
    },
    createStringFromArray: function (arrayName, arr) {
        let str = '';
        let count = 0;
        arr.forEach(function (val) {
            str += arrayName + '[' + count + ']' + '=' + val + '&'
            count++;
        });
        str = str.slice(0, -1);
        return str;
    },
    afterDelete: function () {
        $('#grid').data('kendoGrid').dataSource.read();
        Loader.hide($('#loader'), $('#wrapper'));
    }
}

function showErrorAlert(errText) {
    const notification = $('#notification').data('kendoNotification');

    if(!notification) {
        return;
    }

    notification.show({
        title: 'Error',
        message: errText,
    }, 'error');
}