import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import Loader from '~/components/Loader'
import functionsForDelete from './functionsForDelete.js'
import guid from '~/helpers/guid.js'

import './style.scss'

const $ = window.$;


export default class GridNav extends Component {
    render() {
        this.deleteBtnId = guid();
        this.cancelBtnId = guid();

        const newButtonCreate = (entityName) => {
            const nonCreteEntities = [
                'ddsm_site',
                'ddsm_project',
                'ddsm_measure',
                'ddsm_documentconvention'
            ];

            if (nonCreteEntities.includes(entityName)) {
                return (<span></span>);
            }
            return (
                <button id='crm-btn-add' className='k-button'
                    onClick={this.btnAddHandler(this.props.entityName)}>
                    <i className='k-font-icon k-i-plus'></i>
                    New
                </button>
            );
        }

        return (
            <div className='row' id='grid-panel'>
                <ul>
                    <li>
                        {newButtonCreate(this.props.entityName)}
                    </li>
                    <li>
                        <button id='grid-remove-button' className='k-button'
                            onClick={this.btnDeleteHandler()}>
                            <i className='k-font-icon k-i-trash'></i>
                            Delete
                    </button>
                    </li>
                </ul>
                <div id='confirm-delete-window' style={{ display: 'none' }}>
                    <p>Deleting the record will delete all records under the record as well (for example, Social profiles, opportunities, activities, and contacts). Sub-{this.props.entityName}s will be removed from the account, but not deleted.SharePoint documents will not be deleted, but may not be accessible through Microsoft Dynamics CRM.
To keep the records under the record available for further use or for reporting, click Cancel and then select Deactivate from the command bar.
To continue deleting the entity, click Delete.</p>
                    <div className='confirm-delete-buttons'>
                        <div className='col-md-1 col-md-offset-8'>
                            <button id={this.deleteBtnId} className='k-button'>Delete</button>
                        </div>
                        <div className='col-md-1 col-md-offset-1'>
                            <button id={this.cancelBtnId} className='k-button'>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    componentDidMount() {
        $('#' + this.cancelBtnId).click(() => {
            $('#confirm-delete-window').data('kendoWindow').close();
        });

        const entityName = this.props.entityName;

        $('#' + this.deleteBtnId).click(() => {
            $('#confirm-delete-window').data('kendoWindow').close();

            var selectedItems = functionsForDelete.getSelectedGridItems();
            var selectedIdArr = functionsForDelete.getIdArrFromGridItems(selectedItems, entityName);

            if (selectedIdArr.length === 0) {
                return;
            }

            Loader.show($('#loader'), $('#wrapper'));

            if (selectedIdArr.length === 1) {
                functionsForDelete.deleteSelectedAccount(selectedIdArr[0], entityName);
                return;
            }

            functionsForDelete.deleteSelectedAccounts(selectedIdArr, entityName);
        });
    }

    btnAddHandler(entityName) {
        return function () {
            browserHistory.push(`/form/new/${entityName}`);
        }
    }

    btnDeleteHandler() {

        return function () {
            const popUp = $('#confirm-delete-window');

            if (!popUp.data('kendoWindow')) {
                popUp.kendoWindow({
                    width: '600px',
                    title: 'Delete Confirmation',
                    visible: false,
                    actions: [
                        'Close'
                    ]
                });

            }

            popUp.data('kendoWindow').center().open();
        }
    }
}