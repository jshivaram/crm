const $ = window.$;

export default {
    recreate: function (url, entityName, viewId, hideLoader) {
        let options = this.getOptions(url);

        var self = this;

        let urlForDisplayNames = '/api/view/GetFieldsWithDisplayNames?'
            + 'entityLogicalName=' + entityName
            + '&viewId=' + viewId;

        console.log(urlForDisplayNames);

        $.ajax({
            url: urlForDisplayNames,
            method: 'GET',
            success: function (data) {
                console.log(data);

                let keys = Object.keys(data);

                options.columns = [];

                keys.forEach((key) => {
                    options.columns.push({
                        field: key,
                        title: data[key]
                    });
                }, this);

                options.dataBound = function () {
                    let hiddenColumnIndex = self.getColumnIndexByEntityName(entityName);

                    var grid = $('#grid').data('kendoGrid');
                    grid.hideColumn(hiddenColumnIndex);

                    $.each($('#grid td:first-child'), function (key, value) {
                        let text = $(value).text();
                        let a = '<a href=\'\'>' + text + '</a>';
                        $(value).html(a);
                    });

                    $.each($('#grid td'), function (key, value) {
                        let text = $(value).text();
                        if (!text || text.indexOf('\'') === -1) {
                            return;
                        }

                        let json = text.split('\'').join('"');

                        try {
                            let entityRefObj = JSON.parse(json);
                            let val = entityRefObj.Name ? entityRefObj.Name : '';
                            $(value).html(val);
                        }
                        catch (err) {
                            console.log(err);
                            console.log('error when parse json field in view');
                        }
                    });
                }

                $('#grid').html('');
                $('#grid').kendoGrid(options);

                hideLoader();
            },
            error: function (err) {
                console.log(err);

                hideLoader();
            }
        });


    },
    getOptions: function (url) {
        return {
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            filterable: true,
            dataSource: {
                schema: {
                    data: 'Data',
                    total: 'Total'
                },
                pageSize: 20,
                transport: {
                    read: {
                        url: url,
                        dataType: 'json',
                        type: 'GET'
                    }
                },
                serverSorting: true,
                //serverFiltering: true,
                serverPaging: true,
                error: function (e) {
                    var notification = $('#notification').data('kendoNotification');
                    notification.show({
                        title: 'Error',
                        message: e.errorThrown,
                    }, 'error');
                }
            },
            sortable: {
                mode: 'single',
                allowUnsort: false
            },
            selectable: 'multiple',
            resizable: true,
            mobile: true
        }
    },
    getColumnIndexByEntityName: function (entityName) {
        var i = -1;
        var grid = $('#grid').data('kendoGrid');
        grid.columns.forEach(function (element, index) {
            if (element.field.toLowerCase() === entityName.toLowerCase() + 'id') {
                i = index;
            }
        });

        if (i === -1) {
            throw 'Entity id is not found';
        }

        return i;
    }
};