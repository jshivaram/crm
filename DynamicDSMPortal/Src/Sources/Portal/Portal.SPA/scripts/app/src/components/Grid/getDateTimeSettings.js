const $ = window.$;

export default async function(entityName, attributes) {
    let dateTimeAttrs = Object.keys(attributes)
        .filter(attrLogicalName => attributes[attrLogicalName].Type === 'DateTimeType');

    let fieldsDateTimeFormats;
    let userDateTimeFormat;
    if (dateTimeAttrs.length !== 0) {
        fieldsDateTimeFormats = await $.ajax({
            url: '/api/metadata/GetDateTimeFormats',
            type: 'GET',
            dataType: 'json',
            data: {
                EntityLogicalName: entityName,
                AttributesLogicalNames: dateTimeAttrs
            }
        });

        userDateTimeFormat = await $.get('/api/usersettings/GetDateFormat');
    }

    return {
        fieldsDateTimeFormats: fieldsDateTimeFormats,
        userDateTimeFormat: userDateTimeFormat
    };
}