
const kendo = window.kendo;
import constDateTimeFormat from '~/constants/dateTimeFormat'

export default function (dateTimeSettings, attrLogicalName) {
    return function (data) {

        if (!data[attrLogicalName]) {
            return '';
        }

        let date = new Date(data[attrLogicalName]);
        if (dateTimeSettings.fieldsDateTimeFormats[attrLogicalName] === constDateTimeFormat.DateAndTime) {

            let dateTimeFormat = dateTimeSettings.userDateTimeFormat.DateFormat
                + ' ' + dateTimeSettings.userDateTimeFormat.TimeFormat;

            return kendo.toString(date, dateTimeFormat);
        }

        let dateFormat = dateTimeSettings.userDateTimeFormat.DateFormat;
        return kendo.toString(date, dateFormat);
    }
}