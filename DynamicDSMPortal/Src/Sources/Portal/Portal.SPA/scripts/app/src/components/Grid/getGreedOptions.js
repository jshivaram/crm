
import { browserHistory } from 'react-router'
import GlobalLoader from '~/components/GlobalLoader'

const $ = window.$;

const stringOperators = {
    contains: 'Contains',
    doesnotcontain: 'Does Not Contain',
    eq: 'Equal',
    neq: 'Not Equal',
    startswith: 'Starts with',
    endswith: 'End With',
    isempty: 'Is empty',
    isnotempty: 'Is not empty'
};

const dateOperators = {
    lt: '<',
    gt: '>',
    isempty: 'Is empty',
    isnotempty: 'Is not empty'
};

const picklistOperators = {
    eq: 'Equal',
    neq: 'Not Equal',
    isempty: 'Is empty',
    isnotempty: 'Is not empty'
};

const numberOperators = {
    lt: '<',
    gt: '>',
    lte: '<=',
    gte: '>=',
    eq: 'Equal',
    neq: 'Not Equal',
};

const booleadOperator = {
    eq: 'Equal',
    neq: 'Not Equal'
};

export default function (url) {
    const globalLoader = new GlobalLoader();
    globalLoader.show();

    return {
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        filterable: {
            operators: {
                date: dateOperators,
                string: stringOperators,
                object: stringOperators,
                picklist: picklistOperators,
                number: numberOperators,
                boolean: booleadOperator
            },
        },
        dataSource: {
            schema: {
                data: 'Data',
                total: 'Total'
            },
            pageSize: 20,
            transport: {
                read: {
                    url: url,
                    dataType: 'json',
                    type: 'GET'
                }
            },
            serverSorting: true,
            serverFiltering: true,
            serverPaging: true,
            error: function (e) {
                globalLoader.hide();
                var notification = $('#notification').data('kendoNotification');
                notification.show({
                    title: 'Error',
                    message: e.errorThrown,
                }, 'error');
            }
        },
        dataBound: function (arg) {
            const gridLinks = arg.sender.element.find('a.grid-link');

            gridLinks.on('click', function (e) {
                e.preventDefault();
                const href = $(e.target).attr('href');
                browserHistory.push(href);
            });

            globalLoader.hide();
        },
        sortable: {
            mode: 'single',
            allowUnsort: false
        },
        selectable: 'multiple',
        resizable: true,
        mobile: true
    }
}