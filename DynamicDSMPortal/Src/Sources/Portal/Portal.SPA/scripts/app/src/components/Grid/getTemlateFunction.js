const $ = window.$;

export default function (entityName, attrLogicalName) {
    return function (data) {
        const fieldData = data[attrLogicalName];
        const id = data[entityName + 'id'];

        if (!fieldData) {
            return '';
        }

        if (attrLogicalName === 'ddsm_name' || attrLogicalName === 'name' || attrLogicalName === 'fullname') {
            return `<a class='grid-link' href="/form/${id}/${entityName}">${fieldData}</a>`;
        }

        if(typeof(fieldData) !== 'string') {
            return fieldData;
        }

        if(fieldData.indexOf('{') === -1 && fieldData.indexOf('[') === -1) {
            return fieldData;
        }

        const json = fieldData;

        try {
            const entityRefObj = JSON.parse(json);
            //if it is lookup
            if (entityRefObj.Logicalname && entityRefObj.Id && entityRefObj.Name !== undefined) {
                return formTmplateForGridLookup(entityRefObj.Logicalname, entityRefObj.Id, entityRefObj.Name);
            }
            //if it picklist
            
            if ((entityRefObj.Value || entityRefObj.Value === 0) && entityRefObj.FormattedValue) {
                return entityRefObj.FormattedValue;
            }
            //if it picklist with empty formattedValue
            if(entityRefObj.Value) {
                if(entityRefObj.Value === -1) {
                    return '';
                }

                return entityRefObj.Value;
            }

            return fieldData;
        }
        catch (err) {
            console.log(err, 'error when parse json field in view');
            return fieldData;
        }
    }
}

function formTmplateForGridLookup(entityLogicalName, entityId, name) {
    if (name === '') {
        name = '(No name)';
    }
    if (!entityLogicalName || !entityId) {
        return name;
    }

    if (!isEntityInMenu(entityLogicalName)) {
        return name;
    }

    return `<a class='grid-link' href="/form/${entityId}/${entityLogicalName}">${name}</a>`;
}

function isEntityInMenu(menuItem) {
    if (!menuItem) {
        return false;
    }

    const menuList = $('#main-menu-ul li a').map((i, el) => $(el).attr('href').split('/')[2])
    const result = menuList.filter((i, mi) => mi.toLowerCase() === menuItem.toLowerCase())[0];
    return Boolean(result);
}