import React, { Component } from 'react'
import GridNav from './GridNav'
import { browserHistory } from 'react-router'
import FilterPanel from './FilterPanel'
import ToolsForGrid from '~/components/Grid/ToolsForGrid.js'

const $ = window.$;

import './style.scss'

export default class Grid extends Component {
    render() {
        return (
            <div>
                <GridNav entityName={this.props.entityName} />

                <FilterPanel entityName={this.props.entityName} />
                
                <div id='grid'></div>
            </div>
        );
    }
    componentDidMount() {

        var entityName = this.props.entityName;

        $('#grid').on('click', ' tbody > tr > td > a', function (ev) {
            ev.preventDefault();

            let $tr = $(ev.target.parentElement.parentElement);

            let index = ToolsForGrid.getColumnIndexByEntityName(entityName);
            let entityId = $($tr.find('td').get(index)).text();

            browserHistory.push(`/form/${ entityId }/${ entityName }`);
        });
        
    }
}