import React, { Component } from 'react';
import { browserHistory } from 'react-router'
import Loader from '~/components/Loader'
import { setUserName } from '~/actions/authActions.js'

const $ = window.$;

export default class Login extends Component {
    render() {
        return (
            <form id='form-login-tab' onKeyDown={this.keyDownLoginHandler(this)}>
                <div className='form-icon'>
                    <span className='k-font-icon k-i-lock'></span>
                </div>
                <h3 className='custom-h2'>Access your Account Online profile and account information.</h3>
                <div className='login-form-window-body'>
                    <div className='row custom-row'>
                        <div id='auth-err-msg-login' className='alert alert-danger'></div>
                    </div>
                    <div className='row custom-row'>
                        <div className='input-field col-md-12'>
                            <input id='crm-portal-login' className='k-textbox' type='text' name='Login' required />
                            <label htmlFor='crm-portal-login' className=''>Username</label>
                            <input id='crm-portal-form-password' className='k-textbox' type='password' name='Password' required />
                            <label htmlFor='crm-portal-form-password' className=''>Password</label>
                        </div>
                    </div>
                    <div className='row custom-row'>
                        <input onClick={this.btnLoginHandler(this)} className='k-button' type='button' value='Login' />
                    </div>
                </div>
            </form>
        );
    }
    keyDownLoginHandler(self) {
        return function (e) {
            if (e.keyCode !== 13) {
                return;
            }

            self.Login(self);
        }
    }

    btnLoginHandler(self) {
        return function () {
            self.Login(self);
        }
    }

    Login(self) {
        let formData = $('#form-login-tab').serialize();

        Loader.show($('#loader'), $('#wrapper'));

        $.ajax({
            url: '/api/auth/login',
            method: 'POST',
            data: formData,
            success: function () {
                Loader.hide($('#loader'), $('#wrapper'));

                let login = $('#crm-portal-login').val();
                self.props.dispatch(setUserName(login));

                $.get('/api/Configuration/GetDefaultPageUrlAfterLogin')
                    .done(url => {
                        if (url) {
                            browserHistory.push(url);
                        } else {
                            browserHistory.push('/');
                        }
                    })
                    .fail(err =>{
                        console.log('err when try to get', err);
                    });

                browserHistory.push('/');
            },
            error: function (err) {
                Loader.hide($('#loader'), $('#wrapper'));

                let response;
                try {
                    response = JSON.parse(err.statusText);

                    let errMsg = '';
                    response.forEach(function (el) {
                        errMsg += el[Object.keys(el)[0]];
                        errMsg += '</br>'
                    });

                    $('#auth-err-msg-login').css('display', 'block');
                    $('#auth-err-msg-login').html(errMsg);
                }
                catch (ex) {
                    console.log(ex);

                    $('#auth-err-msg-login').css('display', 'block');
                    $('#auth-err-msg-login').text(err.statusText);
                }
            }
        });
    }
}