import React, { Component } from 'react';
import Login from './Login'
import Register from './Register'

const $ = window.$;

export default class Auth extends Component {
  render() {
    return(
        <div className='row form-login-register-home'>

            <div className='login-bg-form'>

                <div id='tabstrip' className='well'>
                    <ul>
                        <li className='k-state-active'>
                            LOG IN
                        </li>
                        <li>
                            SIGN UP
                        </li>
                    </ul>

                    <div>
                        <Login dispatch={this.props.dispatch} />
                    </div>

                    <div>
                        <Register />
                    </div>

                </div>
            </div>
        </div>
    );
  }
  componentDidMount() {
    $('#tabstrip').kendoTabStrip({
        animation:  {
            open: {
                effects: 'fadeIn'
            }
        }
    });
  }
}