import React, { Component } from 'react';
import './style.scss';
import Dashboard from '~/components/Dashboard';
import Auth from './Auth'
import guid from '~/helpers/guid'

const $ = window.$;

export default class Home extends Component {
  render() {
    // Reset DropDownList for applications
    let app = $('#crm-portal-application').data('kendoDropDownList');
    if (app) {
      app.value('');
    }
    if (!this.props.user) {
        return (
            <Auth key={guid()} dispatch={this.props.dispatch} />
        );
      } 
      return (<Dashboard/>);     
  }
}