const kendo = window.kendo;

export default {
    show: show,
    hide: hide
};

function show($blockLoader, $hideContent) {
    $hideContent.hide();
    kendo.ui.progress($blockLoader, true);
}

function hide($blockLoader, $showContent) {
    $showContent.show();
    kendo.ui.progress($blockLoader, false);
}