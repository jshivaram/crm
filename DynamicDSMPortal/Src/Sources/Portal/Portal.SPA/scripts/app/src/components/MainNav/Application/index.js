import React, { Component } from 'react'
import { browserHistory } from 'react-router'

const $ = window.$;

const dataApp = [
    { text: 'Applications', value: '' },
    { text: 'Application #1', value: 'application1' },
    { text: 'Application #2', value: 'application2' },
    { text: 'Application #3', value: 'application3' }
];

export default class Application extends Component {
    render() {
        return(
            <input id='crm-portal-application' style={{width: '150px'}} />
        );
    }
    componentDidMount() {
        $('#crm-portal-application').kendoDropDownList({
            dataTextField: 'text',
            dataValueField: 'value',
            dataSource: dataApp,
            change: this.appChangeHandler
        });
    }
    appChangeHandler() {
        let app = $('#crm-portal-application').data('kendoDropDownList');
        let value = '/' + app.value();
        browserHistory.push(value);
    }
}