import React, { Component } from 'react'
import Loader from '~/components/Loader'
import { setUserName } from '~/actions/authActions.js'
import { browserHistory } from 'react-router'
import NavLink from '~/components/NavLink'

import './style.scss'

const $ = window.$;

const loginData = [
    { text: '--', value: '' },
    { text: 'Login #1', value: 'login1' },
    { text: 'Login #2', value: 'login2' },
    { text: 'Login #3', value: 'login3' }
];

const registrationData = [
    { text: '--', value: '' },
    { text: 'Registration #1', value: 'registration1' },
    { text: 'Registration #2', value: 'registration2' },
    { text: 'Registration #3', value: 'registration3' }
];

export default class Authentication extends Component {
    render() {

        let loginList = $('#crm-portal-login-list').data('kendoDropDownList');
        let registrationList = $('#crm-portal-registration-list').data('kendoDropDownList');

        if (!this.props.user) {
            if (!loginList && !registrationList) {
                $('#crm-portal-login-list').kendoDropDownList({
                    dataTextField: 'text',
                    dataValueField: 'value',
                    dataSource: loginData,
                    change: this.onChangeLogin
                });

                $('#crm-portal-registration-list').kendoDropDownList({
                    dataTextField: 'text',
                    dataValueField: 'value',
                    dataSource: registrationData,
                    change: this.onChangeRegistration
                });
            }

            $('#crm-portal-user-isnt-auth').css('display', 'block');
            $('#crm-portal-user-is-auth').css('display', 'none');
        }
        else {
            $('#crm-portal-user-is-auth').css('display', 'block');
            $('#crm-portal-user-isnt-auth').css('display', 'none');
        }

        return (
            <div>
                <ul id='crm-portal-user-isnt-auth' className='auth-nav-for-list' style={{ display: 'none' }}>
                    <li>
                        <input id='crm-portal-login-list' style={{ width: '150px' }} />
                    </li>
                    <li>
                        <input id='crm-portal-registration-list' style={{ width: '150px' }} />
                    </li>
                </ul>
                <ul id='crm-portal-user-is-auth' className='auth-nav' style={{ display: 'none' }}>
                    <li><NavLink to='/userprofile'><span className='glyphicon glyphicon-user'></span><span className='auth-uaser-text'>{this.props.user}</span></NavLink></li>
                    <li><a id='crm-portal-logout' href='#'><span className='glyphicon glyphicon-log-out'></span><span className='auth-uaser-logout-text'>Logout</span></a></li>
                </ul>
            </div>
        );
    }
    componentDidMount() {

        let loginList = $('#crm-portal-login-list').data('kendoDropDownList');
        let registrationList = $('#crm-portal-registration-list').data('kendoDropDownList');

        if (!this.props.user) {
            if (!loginList && !registrationList) {
                $('#crm-portal-login-list').kendoDropDownList({
                    dataTextField: 'text',
                    dataValueField: 'value',
                    dataSource: loginData,
                    change: this.onChangeLogin
                });

                $('#crm-portal-registration-list').kendoDropDownList({
                    dataTextField: 'text',
                    dataValueField: 'value',
                    dataSource: registrationData,
                    change: this.onChangeRegistration
                });
            }

            $('#crm-portal-user-isnt-auth').css('display', 'block');
            $('#crm-portal-user-is-auth').css('display', 'none');
        }

        let self = this;

        $(document.body).on('click', '#crm-portal-logout', function(e) {
            e.preventDefault();

            Loader.show($('#loader'), $('#wrapper'));

            let login = $('#crm-portal-login-list').data('kendoDropDownList');
            let registration = $('#crm-portal-registration-list').data('kendoDropDownList');

            login.value('');
            registration.value('');

            $.ajax({
                url: '/api/auth/logout',
                method: 'POST',
                success: function() {
                    Loader.hide($('#loader'), $('#wrapper'));

                    self.props.dispatch(setUserName(undefined));

                    browserHistory.push('/');
                },
                error: function(err) {
                    Loader.hide($('#loader'), $('#wrapper'));

                    console.log(err);
                }
            });
        });
    }
    onChangeLogin() {
        let login = $('#crm-portal-login-list').data('kendoDropDownList');
        let registration = $('#crm-portal-registration-list').data('kendoDropDownList');

        let value = '/' + login.value();
        registration.value('');

        browserHistory.push(value);
    }
    onChangeRegistration() {
        let login = $('#crm-portal-login-list').data('kendoDropDownList');
        let registration = $('#crm-portal-registration-list').data('kendoDropDownList');

        let value = '/' + registration.value();
        login.value('');

        browserHistory.push(value);
    }
}