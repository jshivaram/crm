import React, { Component } from 'react'
const $ = window.$;
import { browserHistory } from 'react-router'

export default class DropDownApplications extends Component {
    render() {
        onRenderAsync();

        return (
            <div id='applications-for-unreg-container'>
                <select id='applications-for-unreg-dropdown'>
                </select>
            </div>
        );
    }
    componentDidMount() {
        componentDidMountAsync();
    }
}

async function componentDidMountAsync() {
    let $selectContainer = $('#applications-for-unreg-container');

    let isUserAuthorized = await $.get('/api/auth/isUserAuthorized');

    if (isUserAuthorized) {
        $selectContainer.hide();
        return;
    }

    $selectContainer.show();
    renderKendoDropDown();
}

async function onRenderAsync() {
    let $selectContainer = $('#applications-for-unreg-container');

    if ($selectContainer.length === 0) {
        return;
    }

    let isUserAuthorized = await $.get('/api/auth/isUserAuthorized');

    if (isUserAuthorized) {
        $selectContainer.hide();
        return;
    }

    $selectContainer.show();
    renderKendoDropDown();
}

function changeBodyClasses(formName) {
    let applicationFormClassName = formName.toLowerCase().split(' ').join('-');
    $(document.body).attr('class', 'ddsm-application-wrapper-body ' + applicationFormClassName);
}

function renderKendoDropDown() {
    let url = '/api/application/getApplicationsFormListForUnregisters';
    let $select = $('#applications-for-unreg-dropdown');

    $select.kendoDropDownList({
        filter: 'contains',
        ignoreCase: true,
        dataTextField: 'Text',
        dataValueField: 'Value',
        dataSource: {
            transport: {
                read: {
                    dataType: 'json',
                    url: url,
                    type: 'GET'
                }
            }
        },
        optionLabel: '--',
        dataBound: () => {
            let data = $select.data('kendoDropDownList');
            data.value(null);
        },
        select: function (e) {
            let dataItem = this.dataItem(e.item);
            if (dataItem.Value) {
                browserHistory.push('/application/' + dataItem.Value + '/' + dataItem.Text);
                changeBodyClasses(dataItem.Text);
            }
        }
    });
}