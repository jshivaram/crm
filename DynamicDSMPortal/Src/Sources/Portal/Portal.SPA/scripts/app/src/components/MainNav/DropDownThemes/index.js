import React, { Component } from 'react'

const $ = window.$;

export default class DropDownThemes extends Component {
    render() {
        return (
            <input id='themes' />
        );
    }
    componentDidMount() {
        const url = '/api/themes/GetThemesFilesNames'

        $('#themes').kendoDropDownList({
            dataTextField: 'Text',
            dataValueField: 'Value',
            dataSource: {
                transport: {
                    read: {
                        dataType: 'json',
                        url: url,
                        type: 'GET'
                    }
                }
            },
            dataBound: function () {
                $.get('/api/themes/GetDefaultThemeName')
                    .done(t => changeTheme(t))
                    .fail(err => {
                        console.log('err when change theme', err);
                        changeTheme('e1');
                    });
            },
            change: function (e) {
                const theme = e.sender._old;
                changeTheme(theme);
            }
        });
    }
}

function changeTheme(theme) {
    $('head link[crm-theme]').remove();
    $('head link[href="/Content/Applications/application.css"]').remove()
    $('head link[crm-application]').remove();

    $('head').append('<link crm-theme href=\'/Content/Themes/kendo.'
        + theme + '.min.css\' rel=\'stylesheet\' />');

    $('head').append('<link crm-theme href="/Content/Themes/kendo.'
        + theme + '.mobile.min.css" rel="stylesheet" />');

    $('head').append('<link crm-theme href=\'/Content/Themes/ddsm_kendo.'
        + theme + '.min.css\' rel=\'stylesheet\' />');

    $('head').append('<link crm-theme href="/Content/Themes/ddsm_kendo.'
        + theme + '.mobile.min.css" rel="stylesheet" />');

    if (!isApplicationForUnregester() && !isApplicationForRegisters()) {
        return;
    }

    if (theme === 'e1') {
        $('head').append('<link crm-application href="/Content/Applications/application-e1.css" rel="stylesheet" />');
    } else {
        $('head').append('<link crm-application href="/Content/Applications/application.css" rel="stylesheet" />');
    }
}

function isApplicationForUnregester() {
    return document.location.href.indexOf('/application/') !== -1;
}

function isApplicationForRegisters() {
    return document.location.href.indexOf('/form/') !== -1 && document.location.href.indexOf('/ddsm_application') !== -1;
}