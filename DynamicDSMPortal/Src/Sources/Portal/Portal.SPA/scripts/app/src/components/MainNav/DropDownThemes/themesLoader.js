const kendo = window.kendo;
const $ = window.$;

export default {
    show: show,
    hide: hide
};

function show() {
    $('#app-container').hide();
    kendo.ui.progress($('#themes-loader'), true);
}

function hide() {
    $('#app-container').show();
    kendo.ui.progress($('#themes-loader'), false);
}