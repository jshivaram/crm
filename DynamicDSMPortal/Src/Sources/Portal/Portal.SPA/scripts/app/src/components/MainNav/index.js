import React, { Component } from 'react'
import NavLink from '~/components/NavLink'
import DropDownThemes from './DropDownThemes'
import Authentication from './Authentication'
import DropDownApplications from './DropDownApplications'
import guid from '~/helpers/guid'

const $ = window.$;
import './style.scss'

export default class MainNav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            entities: undefined
        };
    }

    render() {
        const entities = this.state.entities;

        let menu = (user) => {
            if (user && entities) {
                return (
                    <nav className='navigation-menu'>
                        <input className='trigger' type='checkbox' id='mainNavButton' />
                        <label htmlFor='mainNavButton'>&nbsp;</label>
                        <ul id='main-menu-ul'>
                            <li>
                                <NavLink className='main-logo' onlyActiveOnIndex={true} to='/'>
                                    <span className='glyphicon glyphicon-dashboard'></span>
                                    <strong>Dashboard</strong>
                                </NavLink>
                            </li>
                            {createNavLinksList(entities)}
                        </ul>
                    </nav>
                );
            }

            if (!entities) {
                const self = this;
                $.get('/api/entity/GetEntitiesForNavigation')
                    .done(entities => self.setState({ entities }))
                    .fail(err => console.log(err));
            }

            return (
                <ul id='main-menu-ul'>
                    <li>
                        <NavLink className='main-logo' onlyActiveOnIndex={true} to='/'>
                            <span className='glyphicon glyphicon-dashboard'></span>
                            <strong>Dashboard</strong>
                        </NavLink>
                    </li>
                </ul>
            );
        }

        return (
            <div id='header' className='row'>
                <div className='col-md-7'>
                    {menu(this.props.user)}
                </div>
                <div className='col-md-1'>
                    <DropDownThemes />
                </div>
                <div className='col-md-1'>
                    <DropDownApplications />
                </div>
                <div className='col-md-3'>
                    <Authentication {...this.props} />
                </div>
            </div>
        )
    }

    componentDidMount() {

    }
}

function createNavLinksList(entities) {
    return (
        <span className='main-menu-span'>
            {entities.map((e) => {
                console.log(e);
                return (
                    <li key={guid()}>
                        <NavLink to={'/entities/' + e.LogicalName}>
                            <span className={'glyphicon ' + e.ClassName}></span>
                            <strong>{e.DisplayName}</strong>
                        </NavLink>
                    </li>
                );
            })}
        </span>
    );
}