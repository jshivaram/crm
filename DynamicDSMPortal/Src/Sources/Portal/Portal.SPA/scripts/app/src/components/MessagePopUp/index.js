import React, { Component } from 'react'
import guid from '~/helpers/guid.js'

const $ = window.$;

export default class MessagePopUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: undefined
        };
    }
    render() {
        this.popUpId = guid();
        this.title = this.props.title ? this.props.title : '';

        return (
            <div className='pop-up-wrapper' id={this.popUpId} style={{ display: 'none' }}>
                <p className='pop-up-message'>{this.state.message}</p>
            </div>
        );
    }
    componentDidMount() {
        const self = this;
        const listener = this.props.listener;

        listener.openMessagePopup = function (message) {
            self.open(message);
        }

        listener.closeMessagePopup = function () {
            self.close();
        }
    }
    open(message) {

        if (message) {
            this.setState({ message: message });
        }

        const popUp = $('#' + this.popUpId);
        const self = this;

        if (!popUp.data('kendoWindow')) {
            popUp.kendoWindow({
                width: '600px',
                modal: true,
                title: this.title,
                visible: false,
                actions: [
                    'Close'
                ]
            });
        }

        popUp.data('kendoWindow').center().open();

        $(document).on('click', '.k-overlay', function () {
            self.close();
        });
    }
    close() {
        const popUp = $('#' + this.popUpId);
        popUp.data('kendoWindow').close();
    }
}