import React, { Component } from 'react'
import { Link } from 'react-router'

export default class NotFound extends Component {
  render() {
    return (
      <div className='container'>
        <div className='row'>
          <div className='col-md-12'>
            Page not found 404. Return to <Link to='/'>Home Page</Link>?
          </div>
        </div>
      </div>
    )
  }
}