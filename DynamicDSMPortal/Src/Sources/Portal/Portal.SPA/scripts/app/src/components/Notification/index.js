import React, { Component } from 'react'

import './style.scss'

const $ = window.$;

export default class Notification extends Component {
    render() {
        return (
            <div>
                <span id='notification' style={{display: 'none'}}></span>

                <script id='emailTemplate' type='text/x-kendo-template'>
                    <div className='new-mail'>
                        <img src='/Content/Kendo/KendoUI.Styles/images/envelope.png' />
                        <h3>#= title #</h3>
                        <p>#= message #</p>
                    </div>
                </script>

                <script id='errorTemplate' type='text/x-kendo-template'>
                    <div className='wrong-pass'>
                        <img src='/Content/Kendo/KendoUI.Styles/images/error-icon.png' />
                        <h3>#= title #</h3>
                        <p>#= message #</p>
                    </div>
                </script>

                <script id='successTemplate' type='text/x-kendo-template'>
                    <div className='upload-success'>
                        <img src='/Content/Kendo/KendoUI.Styles/images/success-icon.png' />
                        <h3>#= message #</h3>
                    </div>
                </script>
            </div>
        );
    }
    componentDidMount() {
        $('#notification').kendoNotification({
            position: {
                pinned: true,
                top: 30,
                right: 30
            },
            autoHideAfter: 0,
            stacking: 'down',
            templates: [
                {
                    type: 'info',
                    template: $('#emailTemplate').html()
                }, {
                    type: 'error',
                    template: $('#errorTemplate').html()
                }, {
                    type: 'upload-success',
                    template: $('#successTemplate').html()
                }
            ]
        });
    }
}