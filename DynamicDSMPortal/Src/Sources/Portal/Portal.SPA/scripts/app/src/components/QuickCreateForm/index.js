import React, { Component } from 'react'
import CRMFormComponents from '../CRMForm/CRMFormComponents'
import { browserHistory } from 'react-router'
import GlobalLoader from '../GlobalLoader'
import onQuickFormCreateClickWrapper from './onQuickFormCreateClickWrapper'
import FormButtonApprove from '~/components/CRMForm/CRMFormComponents/Form/FormButtonApprove'

const $ = window.$;

export default class QuickCreateForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: undefined
        };
    }
    render() {
        const data = this.state.data;
        if (!data) {
            return <span></span>;
        }

        const entityLogicalName = this.props.params.entityName;

        const createApproveButton = () => {
            if (entityLogicalName !== 'ddsm_sku') {
                return (<span></span>);
            }

            const infoForButtonApprove = {
                entityLogicalName: entityLogicalName,

            };

            return (<FormButtonApprove infoForButtonApprove={infoForButtonApprove} text='Save & Request to Approve' />);
        };

        const createTab = (tab) => {
            return (
                <CRMFormComponents.Tab
                    entity={data.entity}
                    tab={tab} key={tab['@id']}
                    canEditFields={true}
                    dateFormat={data.dateFormat}
                    attributesMetadata={data.attributesMetadata}
                    data={{}}
                    objectAttributes={{}}
                    isQuickForm={true}
                    params={this.props.params}
                >
                </CRMFormComponents.Tab>
            );
        };

        const createFormBody = () => {
            const tabs = JSON.parse(data.jsonQuickCreateFormStructure).form.tabs.tab;

            if (!Array.isArray(tabs)) {
                return (
                    <form id='form-viewer'>
                        {createTab(tabs)}
                    </form>
                );
            } else {
                return (
                    <form id='form-viewer'>
                        {tabs.map(tab => createTab(tab))}
                    </form>
                );
            }
        };

        const createFormFooter = () => {          
            if (entityLogicalName === 'ddsm_sku') {
                return (<span></span>);
            }

            return (
                <div className='quick-form-footer'>
                    <button className='k-button' onClick={this.onQuickFormCancelClickWrapper(this.props.params)}>Cancel</button>
                    <button className='k-button' onClick={onQuickFormCreateClickWrapper(this)}>Create</button>
                </div>
            );
        };

        const quickFormDisplayName = entityLogicalName === 'ddsm_sku' ? 'E1 SKU Portal (creation)' : this.state.data.entityDisplayName;

        return (
            <div className='row quick-create-form'>
                <div className='quick-form-hader'>
                    <div className='row'>
                        <div className='col-md-1'>
                            <span>{quickFormDisplayName}</span>
                        </div>
                        <div className='col-md-2'>
                            {createApproveButton()}
                        </div>
                    </div>
                </div>
                <div className='quick-form-body'>
                    {createFormBody()}
                </div>

                {createFormFooter()}
            </div>
        );
    }

    async componentDidMount() {
        const globalLoader = new GlobalLoader();
        globalLoader.show();

        const entityLogicalName = this.props.params.entityName;

        const entity = {
            entityLogicalName,
            Name: entityLogicalName
        };

        const data = { entity };

        try {
            const jsonQuickCreateFormStructure = await $.get(`/api/form/GetJsonQuickCreateFormStructureAsync?entityLogicalName=${entityLogicalName}`);
            const attributesMetadata = await $.get(`/api/metaData/getAttributesMetadata?entityLogicalName=${entityLogicalName}`);
            const dateFormat = await $.get('/api/usersettings/GetDateFormat');
            const entityDisplayName = await $.get(`/api/metaData/GetEntityDisplayName?entityLogicalName=${entityLogicalName}`);

            data.jsonQuickCreateFormStructure = jsonQuickCreateFormStructure;
            data.attributesMetadata = attributesMetadata;
            data.dateFormat = dateFormat;
            data.entityDisplayName = entityDisplayName;

            this.setState({ data });
        } catch (err) {
            console.error(err);
        }
        finally {
            globalLoader.hide(2000);
        }
    }

    onQuickFormCancelClickWrapper(params) {
        return function () {
            browserHistory.push(`/form/${params.relatedEntityId}/${params.relatedEntityName}`);
        }
    }
}