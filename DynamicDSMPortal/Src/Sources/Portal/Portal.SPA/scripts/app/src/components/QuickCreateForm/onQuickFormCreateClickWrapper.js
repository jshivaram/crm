import { browserHistory } from 'react-router'
import GlobalLoader from '../GlobalLoader'

const $ = window.$;

export default function onQuickFormCreateClickWrapper(quickFormComponent) {
    return function (e) {
        if (e) {
            e.preventDefault();
        }

        const validator = addFormValidator();
        const isFormValid = validator.validate();

        if (!isFormValid) {
            const errors = validator.errors();
            errors.forEach(err => showNotificationAlert(err));
            return;
        }

        const loader = new GlobalLoader();
        loader.show();
        const logicalName = quickFormComponent.props.params.entityName;

        var formData = $('.quick-create-form #form-viewer').serializeObject();
        formData.LogicalName = logicalName;

        var keys = Object.keys(formData);

        keys.forEach(key => {

            if (formData[key] === '') {
                delete formData[key];
                return;
            }

            if (isDateInput(key)) {
                try {
                    const datePicker = $(`input[name="${key}"]`).data('kendoDatePicker');
                    const date = datePicker.value();
                    const isoStringDate = date.toISOString();
                    formData[key] = isoStringDate;
                } catch (err) {
                    console.log('err when parse date', err);
                    delete formData[key];
                }
            }

            let i = key.indexOf('.');
            if (i !== -1) {
                let keyForBase = key.substring(0, i);
                let newKey = key.substring(i + 1);
                let val = formData[key];

                delete formData[key];

                if (!formData.hasOwnProperty(keyForBase)) {
                    formData[keyForBase] = {};
                }

                formData[keyForBase][newKey] = val;
            }
        });

        const jsonEntityData = JSON.stringify(formData);

        const params = quickFormComponent.props.params;

        const data = {
            JsonEntityData: jsonEntityData,
            LogicalName: logicalName,
            EntityId: params.relatedEntityId,
            EntityLogicalName: params.relatedEntityName,
            RelationshipShcemeName: params.relationshipName
        };

        if (logicalName === 'ddsm_project') {
            $.post('/api/entity/CreateProject', data)
                .done(entityId => {
                    if (entityId) {
                        browserHistory.push(`/form/${entityId}/${logicalName}`);
                    }
                })
                .fail(err => console.error(err))
                .always(() => loader.hide());
            return;
        }

        $.post('/api/entity/CreateRelatedEntity', data)
            .done(entityId => {
                if (entityId) {
                    browserHistory.push(`/form/${entityId}/${logicalName}`);
                }
            })
            .fail(err => console.error(err))
            .always(() => loader.hide());
    }
}

function addFormValidator() {
    const validator = $('.quick-create-form #form-viewer').kendoValidator({
        rules: {
            customRule1: function ($input) {

                if ($input.attr('required')) {

                    const dropDownList = $input.data('kendoDropDownList');
                    if (dropDownList) {
                        return $.trim(dropDownList.value() !== '')
                    }

                    return $.trim($input.val()) !== '';
                }

                return true;
            }
        }
    }).data('kendoValidator');

    return validator;
}

function showNotificationAlert(errText) {
    const notification = $('#notification').data('kendoNotification');

    if (!notification) {
        return;
    }

    notification.showText(errText, 'warning');
}

function isDateInput(fieldName) {
    const $input = $(`input[name="${fieldName}"]`);
    if ($input.length === 0) {
        return false;
    }

    return $input.hasClass('date-time-control');
}