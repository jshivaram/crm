import React, { Component } from 'react'
import CRMFormComponents from '../CRMForm/CRMFormComponents'
import GlobalLoader from '../GlobalLoader'
import FormButtonApprove from '~/components/CRMForm/CRMFormComponents/Form/FormButtonApprove'

const $ = window.$;
const entityLogicalName = 'ddsm_sku';

export default class QuickCreateFormForSkuFromModelNumberApproval extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: undefined
        };
    }
    render() {
        const data = this.state.data;
        if (!data) {
            return <span></span>;
        }

        const createApproveButton = () => {
            const infoForButtonApprove = {
                entityLogicalName: entityLogicalName
            };

            return (<FormButtonApprove infoForButtonApprove={infoForButtonApprove} text='Save & Request to Approve' />);
        };

        const createTab = (tab) => {
            return (
                <CRMFormComponents.Tab
                    entity={data.entity}
                    tab={tab} key={tab['@id']}
                    canEditFields={true}
                    dateFormat={data.dateFormat}
                    attributesMetadata={data.attributesMetadata}
                    data={{}}
                    objectAttributes={{}}
                    isQuickForm={true}
                    params={this.props.params}
                >
                </CRMFormComponents.Tab>
            );
        };

        const createFormBody = () => {
            const tabs = JSON.parse(data.jsonQuickCreateFormStructure).form.tabs.tab;

            if (!Array.isArray(tabs)) {
                return (
                    <form id='form-viewer'>
                        {createTab(tabs)}
                    </form>
                );
            } else {
                return (
                    <form id='form-viewer'>
                        {tabs.map(tab => createTab(tab))}
                    </form>
                );
            }
        };

        return (
            <div className='row quick-create-form'>
                <div className='quick-form-hader'>
                    <div className='row'>
                        <div className='col-md-2'>
                            <span>E1 SKU Portal (creation)</span>
                        </div>
                        <div className='col-md-2'>
                            {createApproveButton()}
                        </div>
                    </div>
                </div>
                <div className='quick-form-body'>
                    {createFormBody()}
                </div>
            </div>
        );
    }

    async componentDidMount() {
        const globalLoader = new GlobalLoader();
        globalLoader.show();

        const entity = {
            entityLogicalName,
            Name: entityLogicalName
        };

        const data = { entity };

        try {
            const jsonQuickCreateFormStructure = await $.get(`/api/form/GetJsonQuickCreateFormStructureAsync?entityLogicalName=${entityLogicalName}`);
            const attributesMetadata = await $.get(`/api/metaData/getAttributesMetadata?entityLogicalName=${entityLogicalName}`);
            const dateFormat = await $.get('/api/usersettings/GetDateFormat');
            const entityDisplayName = await $.get(`/api/metaData/GetEntityDisplayName?entityLogicalName=${entityLogicalName}`);

            data.jsonQuickCreateFormStructure = jsonQuickCreateFormStructure;
            data.attributesMetadata = attributesMetadata;
            data.dateFormat = dateFormat;
            data.entityDisplayName = entityDisplayName;

            this.setState({ data });
        } catch (err) {
            console.error(err);
        }
        finally {
            globalLoader.hide(2000);
        }
    }
}