import React, { Component } from 'react'
import guid from '~/helpers/guid'
import GridControl from '~/components/CRMForm/CRMFormControls/GridControl'

const $ = window.$;
const openGlobalPopup = window.openGlobalPopup;

export default class SearchPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: undefined
        };
    }
    render() {
        const globalSearchConfig = this.state.globalSearchConfig;

        if (!globalSearchConfig) {
            return <span></span>
        }

        const searchText = this.props.params.searchText;

        if(searchText) {
            $('.global-search-field').val(searchText);
        }

        const createGrids = () => {
            return globalSearchConfig.map(e => {
                const entityParam = {
                    Name: e.LogicalName
                };

                const controlParam = {
                    parameters: {
                        ViewId: e.ViewId,
                        TargetEntityType: e.LogicalName
                    }
                };

                const labelParam = {
                    '@description': e.DisplayName
                };

                return (
                    <div className='global-search-grid-wrapper' key={guid()}>
                        <br/>
                        <GridControl
                            dashboard={true}
                            entity={entityParam}
                            control={controlParam}
                            label={labelParam}
                            showlabel='false'
                            searchText={searchText}
                        />
                    </div>
                );
            })
        };

        return (
            <div className='global-serarch-wrapper'>
                {createGrids()}
            </div>
        );
    }
    componentDidMount() {
        $.get('/api/globalSearch/GetGlobalSearchConfig')
            .done(globalSearchConfig => {
                if (globalSearchConfig && globalSearchConfig.length) {
                    this.setState({ globalSearchConfig });
                } else {
                    openGlobalPopup('Global search not configured.');
                }
            })
            .fail(err => {
                console.error(err);
                openGlobalPopup('error when try get global search config.');
            })
    }
}