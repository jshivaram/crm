import React, { Component } from 'react'
import Form from '../CRMForm/CRMFormComponents/Form'
import Loader from '../Loader'
import FormViewer from '../../middlewares/FormViewer'
import guid from '~/helpers/guid'

const $ = window.$;

export default class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined
    };
  }
  render() {
    return (
      <Form
        key={guid() }
        data={this.state.data}
        />
    );
  }
  componentDidMount() {
    Loader.show($('#loader'), $('#wrapper'));
    let self = this;


    $.ajax({
      url: '/api/auth/GetAuthorizedUserData',
      method: 'GET',
      success: function (userData) {
        FormViewer.getFormStructureWithData(
          userData.UserId,
          'contact', undefined, 'PORTAL USER PROFILE PAGE 1'
        ).then(data => {
          self.setState({ data: data });

          Loader.hide($('#loader'), $('#wrapper'));
        }).catch(error => {
          Loader.hide($('#loader'), $('#wrapper'));

          alert(error.responseText);

          throw error;
        });
      },
      error: function (err) {
        console.log(err);
      }
    });
  }
}