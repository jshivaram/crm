const dateEnum = {
    DateOnly: 0,
    DateAndTime: 1
}

export default dateEnum;