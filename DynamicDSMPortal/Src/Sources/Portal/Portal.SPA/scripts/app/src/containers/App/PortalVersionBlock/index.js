import React, { Component } from 'react'
const $ = window.$;

export default class PortalVersionBlock extends Component {
    render() {
        return (
            <div id='portal-versions-wrapper'>
                <span id='portal-solution-version-wrapper'>
                    <span id='portal-solution-version-label'>
                        Portal Solution Version:
                </span>
                    <span id='portal-solution-version'>

                    </span>
                </span>

                <span id='portal-version-wrapper'>
                    <span id='portal-version-label'>
                        Portal Version:
                </span>
                    <span id='portal-version'>

                    </span>
                </span>
            </div>
        );
    }
    componentDidMount() {
        $.get('/api/solution/GetPortalSolutionVersion')
            .done(solutionVersion => {
                if (solutionVersion) {
                    $('#portal-solution-version').text(solutionVersion);
                }
            })
            .fail(err => console.log(err));

        $.get('/api/Version/GetPortalVersion')
            .done(portalVersion => {
                if (portalVersion) {
                    $('#portal-version').text(portalVersion);
                }
            })
            .fail(err => console.log(err));
    }
}