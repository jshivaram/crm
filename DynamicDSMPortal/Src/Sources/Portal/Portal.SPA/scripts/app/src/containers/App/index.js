import React, { Component } from 'react'
import MainNav from '~/components/MainNav'
import Notification from '~/components/Notification'

import './reset.css'
import './style.scss'

const $ = window.$;

export default class App extends Component {
  componentWillMount() {
    $('head link[crm-theme]').remove();

    let theme = 'e1';

    $('head').append('<link crm-theme href=\'/Content/Themes/kendo.'
      + theme + '.min.css\' rel=\'stylesheet\' />');

    $('head').append('<link crm-theme href="/Content/Themes/kendo.'
      + theme + '.mobile.min.css" rel="stylesheet" />');

    $('head').append('<link crm-theme href=\'/Content/Themes/ddsm_kendo.'
      + theme + '.min.css\' rel=\'stylesheet\' />');

    $('head').append('<link crm-theme href="/Content/Themes/ddsm_kendo.'
      + theme + '.mobile.min.css" rel="stylesheet" />');
  }

  render() {
    $('head link[crm-application]').remove();

    const href = window.location.href;

    let isApplication = href.indexOf('ddsm_application') !== -1;
    let isUnregApplication = href.indexOf('/application/') !== -1;
    let isForm = href.indexOf('form') !== -1;

    let $body = $(document.body);
    if (isUnregApplication || (isForm && isApplication)) {
      if (!$body.hasClass('ddsm-application-wrapper-body')) {
        $body.addClass('ddsm-application-wrapper-body');
      }
    } else {
      $body.attr('class', '');
    }

    return (
      <div className='container-fluid'>
        <MainNav {...this.props} />
        <div id='wrapper'
          className={(isUnregApplication || (isForm && isApplication)) ? 'ddsm-application-wrapper' : ''}>
          {this.props.children}
        </div>
        {createVersionBlock()}
        <div id='loader'></div>
        <Notification />
      </div>
    )
  }

  componentDidMount() {
    $.get('/api/solution/GetPortalSolutionVersion')
      .done(solutionVersion => {
        if (solutionVersion) {
          $('#portal-solution-version').text(solutionVersion);
        }
      })
      .fail(err => console.log(err));

    $.get('/api/Version/GetPortalVersion')
      .done(portalVersion => {
        if (portalVersion) {
          $('#portal-version').text(portalVersion);
        }
      })
      .fail(err => console.log(err));
  }
}

function createVersionBlock() {
  return (
    <div id='portal-versions-wrapper'>
      <span id='portal-solution-version-wrapper'>
        <span id='portal-solution-version-label'>
          Portal Solution Version:
        </span>
        <span id='portal-solution-version'>

        </span>
      </span>

      <span id='portal-version-wrapper'>
        <span id='portal-version-label'>
          Portal Version:
        </span>
        <span id='portal-version'>

        </span>
      </span>
    </div>
  );
}