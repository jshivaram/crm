import React from 'react'
import { connect } from 'react-redux'
import { setUserName } from '~/actions/authActions.js'
import Loader from '~/components/Loader'

const $ = window.$;

export default function requireAuthentication(Component) {

  class AuthenticatedComponent extends React.Component {
    componentWillMount() {
      this.checkAuth(this.props.user)
    }
    componentWillReceiveProps(nextProps) {
      this.checkAuth(nextProps.user)
    }
    checkAuth(user) {
      let self = this;

      if (!user) {
        Loader.show($('#loader'), $('#wrapper'));

        $.ajax({
            url: '/api/auth/GetAuthorizedUserName',
            method: 'GET',
            success: function (login) {
                Loader.hide($('#loader'), $('#wrapper'));

                self.props.dispatch(setUserName(login));
            },
            error: function(err) {
                Loader.hide($('#loader'), $('#wrapper'));

                self.props.dispatch(setUserName(undefined));

                console.log(err);
            }
        });
      }
      else {
        self.props.dispatch(setUserName(user));
      }
    }
    render() {
      return (
        <Component {...this.props} />
      )
    }
  }

  function mapStateToProps(state) {
    return {
      user: state.user
    }
  }

  return connect(mapStateToProps)(AuthenticatedComponent)
}