import _ from 'lodash'
const localStorage = window.localStorage;

const customStore = {
    set: function (key, value) {
        
        if (!key || !_.isString(key) || !value) {
            return;
        }
        
        let storeFromLocalStorage = getStoreFromLocalStorage();
        
        if(!storeFromLocalStorage) {
            storeFromLocalStorage = {};
        }

        storeFromLocalStorage[key] = value;
        saveStoreToLocalStorage(storeFromLocalStorage);
        onChangeEvent(key);
    },
    get: function (key) {
        if (!key || !_.isString(key)) {
            return;
        }
        
        const storeFromLocalStorage = getStoreFromLocalStorage();

        if(storeFromLocalStorage) {
            return storeFromLocalStorage[key];
        }
    },
    remove: function(key) {
        if (!key || !_.isString(key)) {
            return;
        }
        
        const storeFromLocalStorage = getStoreFromLocalStorage();

        if(!storeFromLocalStorage) {
            return;
        }

        delete storeFromLocalStorage[key];
        saveStoreToLocalStorage(storeFromLocalStorage);
        onChangeEvent(key);
    },
    addOnChangeEventListener: function(component, eventListener) {
        if(!component || !eventListener || !_.isFunction(eventListener)) {
            return;
        }

        const eventsListeners = storeEventsListeners.filter(se => se.component === component);
        //is change eventListener for component is alredy exists
        if(eventsListeners.length > 0) {
            eventsListeners.forEach(el => {
                el.eventListener = eventListener;
            });
            return;
        }

        const listener = {
            'component': component,
            'eventListener': eventListener
        };

        storeEventsListeners.push(listener);
    },
    removeOnChangeEventListener: function(component) {
        if(!component) {
            return;
        }

        const eventsListeners = storeEventsListeners.filter(se => se.component === component);
        storeEventsListeners = eventsListeners;
    }
};

let storeEventsListeners = [];

function onChangeEvent(key) {
    storeEventsListeners.forEach(se => {
        se.eventListener(key);
    });
}

function saveStoreToLocalStorage(store) {
    localStorage.setItem('customStore', JSON.stringify(store));
}

function getStoreFromLocalStorage() {
    const customStoreJson = localStorage.getItem('customStore');
    const store = JSON.parse(customStoreJson);
    return store;
}

export default customStore;