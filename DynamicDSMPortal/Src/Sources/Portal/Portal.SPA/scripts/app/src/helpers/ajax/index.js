const $ = window.$;
const Promise = require('es6-promise').Promise;

export default {
    getAsync: (url) => {
        return new Promise((resolve, reject) => {
            $.ajax({
                method: 'GET',
                url: url,
                success: (data) => resolve(data),
                error: (err) => reject(err)
            });
        });
    }
}