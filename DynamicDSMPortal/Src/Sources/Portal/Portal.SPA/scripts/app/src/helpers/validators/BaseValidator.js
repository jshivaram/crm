
export default class BaseValidator {
    getValidValue(value) {
        if(!value) {
            return '';
        }

        while (!this.validate(value)) {
            value = value.slice(0, value.length - 1);
        }

        return value;
    }
}