
import BaseValidator from './BaseValidator'

export default class DoubleValidator extends BaseValidator {
    validate(value) {
        return !isNaN(Number(value));
    }
}