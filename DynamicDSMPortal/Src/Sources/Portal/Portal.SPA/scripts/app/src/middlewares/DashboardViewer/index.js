import ajax from '../../helpers/ajax'

export default {
    getFormStructureWithData: async function (entityName, dashboardId) {

        let enityAttributes = {};
        let objectAttributes = {};

        const formResult = await ajax.getAsync(`/api/form/GetFormByIdOnly?formId=${dashboardId}`);
        const formJson = {
            entity: {},
            tabs: JSON.parse(formResult).form.tabs.tab,
            enityAttributes: enityAttributes,
            objectAttributes: objectAttributes
        }

        return formJson;
    }
};