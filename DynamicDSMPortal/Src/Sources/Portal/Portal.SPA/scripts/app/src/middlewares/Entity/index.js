import ajax from '../../helpers/ajax'

export default {
    getById: (entityId, entityName) => {
        return ajax.getAsync(`/api/Entity?entityLogicalName=${entityName}&id=${entityId}`);
    }
};