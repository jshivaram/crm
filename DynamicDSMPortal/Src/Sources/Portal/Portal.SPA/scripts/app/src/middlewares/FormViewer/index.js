import Entity from '../Entity'
import ajax from '../../helpers/ajax'

export default {
    getFormStructureWithData: async function (entityId, entityName, formId, formName, isQuickForm) {

        let enityAttributes = {};
        let objectAttributes = {};
        let formattedValues = {};

        if (entityId) {
            let entityResult = await Entity.getById(entityId, entityName);
            if(entityResult.FormattedValues) {
                entityResult.FormattedValues.forEach(fv => { formattedValues[fv.Key] = fv.Value; });
            }

            if (entityResult) {
                entityResult.Attributes.forEach(a => {
                    enityAttributes[a.Key] = typeof a.Value !== 'object' ? a.Value : a.Value.Value;
                    if (typeof a.Value === 'object') {
                        objectAttributes[a.Key] = a.Value;
                    }
                });
            }
        }

        let formResult = await ajax.getAsync(getRequestURL(entityName, formId, formName, isQuickForm));

        let formJson = {
            entity: {
                Id: entityId,
                Name: entityName
            },
            tabs: JSON.parse(formResult).form.tabs.tab,
            enityAttributes: enityAttributes,
            objectAttributes: objectAttributes,
            formattedValues: formattedValues,
            infoForBtnSave: {
                method: entityId ? 'PUT' : 'POST',
                action: '/api/entity'
            }
        }

        return formJson;
    }

};

function getRequestURL(entityName, formId, formName, isQuickForm) {
    if (isQuickForm) {
        return `/api/form/GetBulkEditFormById?entityLogicalName=${entityName}&FormId=${formId}`;
    }
    else if (formId) {
        return `/api/form/GetFormById?entityLogicalName=${entityName}&FormId=${formId}`;
    }
    else if (formName) {
        return `/api/form/GetForm?entityLogicalName=${entityName}&FormName=${formName}`;
    }
    else {
        return `/api/form/Get?entityLogicalName=${entityName}`;
    }
}