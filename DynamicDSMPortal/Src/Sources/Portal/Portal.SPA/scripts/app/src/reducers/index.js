const initialState = {
  user: undefined
};

export default function userstate(state = initialState, action) {
  switch (action.type) {
    case 'SET_UserName': return {...state, user: action.user}
    default: return state;
  }
}