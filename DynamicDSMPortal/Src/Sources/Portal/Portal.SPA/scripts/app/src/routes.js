import React from 'react'
import { Route, IndexRoute } from 'react-router'
import guid from '~/helpers/guid'
import App from './containers/App'

import Home from './components/Home'
import UserProfile from './components/UserProfile'
import Entities from './components/Entities'
import CRMForm from './components/CRMForm'
import DataUploaderForm from './components/DataUploaderForm'

import Login1 from './components/Authentication/Login1'
import Login2 from './components/Authentication/Login2'
import Login3 from './components/Authentication/Login3'

import Registration1 from './components/Authentication/Registration1'
import Registration2 from './components/Authentication/Registration2'
import Registration3 from './components/Authentication/Registration3'

import NotFound from './components/NotFound'

import requireAuthentication from '~/containers/AuthenticatedComponent'
import checkAuthentication from '~/containers/AuthForComponents'

import ApplicationForm from '~/components/ApplicationForm'

function createCRMForm(component) {
  return (
    <CRMForm key={guid()} params={component.params} />
  );
}

function createApplicationForm(component) {
  return (
    <ApplicationForm key={guid()} params={component.params} />
  );
}

export const routes = (
  <div>
    <Route path='/' component={requireAuthentication(App)}>
      <IndexRoute component={requireAuthentication(Home)} />
      <Route path='/entities/:entityName' component={checkAuthentication(Entities)} />

      <Route path='/form/new/ddsm_datauploader' component={checkAuthentication(DataUploaderForm)} />
      <Route path='/form/new/:entityName' component={checkAuthentication(CRMForm)} />

      <Route path='/form/:entityId/:entityName' component={checkAuthentication(createCRMForm)} />

      <Route path='/application/:applicationFormId/:ApplicationFormName' key={guid()} component={createApplicationForm} />

      <Route path='/login1' component={requireAuthentication(Login1)} />
      <Route path='/login2' component={requireAuthentication(Login2)} />
      <Route path='/login3' component={requireAuthentication(Login3)} />

      <Route path='/registration1' component={Registration1} />
      <Route path='/registration2' component={Registration2} />
      <Route path='/registration3' component={Registration3} />

      <Route path='/userprofile' component={UserProfile} />
    </Route>
    <Route path='*' component={NotFound} />
  </div>
)