import configureStore from '~/store/configureStore'

let store;

export default function getStore() {
    if (!store) {
        store = configureStore();
    }

    return store;
}