let DictinaryElement = function (applicationField, contactField) {
    this.applicationField = applicationField;
    this.entityField = contactField;
}

let config = {
    'contact': [
        // new DictinaryElement('ddsm_contractorcontactname', 'fullname'),
        // new DictinaryElement('ddsm_ddsm_infparticipanttitle', 'ddsm_primarycontacttitle'),
        // new DictinaryElement('ddsm_participanttowncity', 'address1_city'),
        // new DictinaryElement('ddsm_participantstreetaddress', 'address1_line1'),
        // new DictinaryElement('ddsm_account_email', 'emailaddress1'),
        // new DictinaryElement('ddsm_participantprovincestate', 'address1_stateorprovince'),
        // new DictinaryElement('ddsm_participantpostalcodezipcode', 'address1_postalcode'),
        // new DictinaryElement('ddsm_contactphonenumber', 'address1_telephone1')
    ],
    'account':[
        new DictinaryElement('ddsm_accountnumber', 'accountnumber'),
        new DictinaryElement('ddsm_participanttowncity', 'address1_city'),
        new DictinaryElement('ddsm_account_email', 'emailaddress1'),
        new DictinaryElement('ddsm_contactphonenumber', 'telephone1')
    ],
    'site':[
        new DictinaryElement('ddsm_buildingaddress', 'ddsm_address1'),
        new DictinaryElement('ddsm_buildingtowncity', 'ddsm_city'),
        new DictinaryElement('ddsm_buildingproviencestate', 'ddsm_state'),
        new DictinaryElement('ddsm_postalcodezipcode', 'ddsm_zip')
    ]
};

export default config;