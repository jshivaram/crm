const $ = window.$;

export default function(propertyName, propertyValue){
    $(`[name=${propertyName}]`).val(propertyValue);
}