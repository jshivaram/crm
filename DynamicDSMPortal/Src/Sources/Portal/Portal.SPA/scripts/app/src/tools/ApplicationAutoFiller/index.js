import config from './config'
import filler from './filler'
import request from './requsetCreator'
import createLink from './linkCreator'

export default function () {
    const propertyNames = Object.keys(config);

    const objectWithFieldNames = {};

    propertyNames.forEach(item => {
        objectWithFieldNames[item] = config[item].map(field => field.entityField);
    });


    const link = createLink(objectWithFieldNames);

    request(link,
        function (response) {
            if (!response) {
                return;
            }
            response = JSON.parse(response);

            propertyNames.forEach(item => {
                mapFields(config[item], response[item]);
            });
        });
}

function mapFields(configObject, responseObject) {
    configObject.forEach(item => {
        const field = responseObject.find(el => el.key === item.entityField);
        if (!field) {
            return;
        }
        const applicationFieldName = item.applicationField;

        filler(applicationFieldName, field.value);
    });
}