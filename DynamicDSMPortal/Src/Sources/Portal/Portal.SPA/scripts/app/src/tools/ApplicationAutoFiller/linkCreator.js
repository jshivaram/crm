export default function(dataObj){
    let paramLinkString = '/api/Application/GetContactFields?';

    let json = JSON.stringify(dataObj);

    paramLinkString += `json=${json}`;

    return paramLinkString;
}