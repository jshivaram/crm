import getCurrentApplicationFromStore from './getCurrentApplicationFromStore'

export default function () {
    let application = getCurrentApplicationFromStore();
    let isBoilerApplication = application &&
        application.applicationName === 'Boiler Application' &&
        (window.location.href.indexOf('/ddsm_application') !== -1 ||
            window.location.href.indexOf('/application') !== -1);

    return isBoilerApplication;
}