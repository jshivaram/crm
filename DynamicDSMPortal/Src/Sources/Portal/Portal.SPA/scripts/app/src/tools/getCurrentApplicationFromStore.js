import customStore from '../customStore/index'
import constants from '../components/MainNav/DropDownApplications/constants'

export default function(){
    return customStore.get(constants.portalAppConfigStorageKey);
}