﻿; (function () {
    var $globalMessageWindow = $("#global-message-window");

    $globalMessageWindow.kendoWindow({
        width: "600px",
        title: "",
        visible: false,
        resizable: false,
        modal: true,        
        actions: [
            "Close"
        ]
    }).data("kendoWindow").center();

    window.openGlobalPopup = function (message, manualClose, onClose) {
        if (!message) {
            return;
        }

        var $popUpMessage = $globalMessageWindow.find(".pop-up-message");
        $popUpMessage.text(message);

        var kendoWindow = $globalMessageWindow.data("kendoWindow");

        kendoWindow.open();

        if (onClose && typeof (onClose) === 'function') {
            $globalMessageWindow.parent().find('.k-i-close').on('click', function () {
                onClose();
            });   
        }

        if (manualClose === true) {
            return;
        }

        setTimeout(function () {
            kendoWindow.close();
        }, 5000);
    };
})();