﻿; (function () {
    var $globalMessageWindow = $("#global-message-window-with-progress");
    var $okeyButton = $globalMessageWindow.find('.popup-process-okey-button');
    $okeyButton.hide();

    $globalMessageWindow.kendoWindow({
        width: "600px",
        title: "",
        visible: false,
        resizable: false,
        modal: true,
        actions: [
            "Close"
        ]
    }).data("kendoWindow").center();

    window.openGlobalPopupWithProgress = function (onOkeyClick) {
        var kendoWindow = $globalMessageWindow.data("kendoWindow");
        kendoWindow.open();

        var itemsToLoad = [
            "Sending Email to Customer",
            "Checking Application Eligibility",
            "Calculation ESP Boiler values",
            "Calculation Boiler Funding Source",
            "Rollups Boiler values to Application"];

        $("#totalProgressBar").kendoProgressBar({
            type: "chunk",
            chunkCount: 5,
            min: 0,
            max: 5,
            orientation: "horizontal",
            complete: onTotalComplete
        });

        $("#loadingProgressBar").kendoProgressBar({
            orientation: "horizontal",
            showStatus: false,
            animation: {
                duration: 30
            },
            change: onChange,
            complete: onComplete
        });

        load();

        function onChange(e) {
            $(".loadingStatus").text(e.value + "%");
        }

        function onComplete(e) {
            var total = $("#totalProgressBar").data("kendoProgressBar");
            total.value(total.value() + 1);

            if (total.value() < total.options.max) {
                $(".chunkStatus").text(total.value() + 1);
                $(".loadingInfo h4").text(itemsToLoad[total.value()]);

                load();
            }
        }

        function onTotalComplete() {
            $('#progress-header-message').html('Your application has been successfully submitted and is under review.<br>The Pending Status of your application is "Application Pre-Approval".<br>Please upload the following document: Heating Confirmation.');
            $(".loadingInfo h4").text("");
            $(".statusContainer").hide();
            $(".reloadButton").show();
            $okeyButton.show();
        }

        function load() {
            var pb = $("#loadingProgressBar").data("kendoProgressBar");
            pb.value(0);

            var interval = setInterval(function () {
                if (pb.value() < 100) {
                    pb.value(pb.value() + 1);
                } else {
                    clearInterval(interval);
                }
            }, 30);
        }

        function _onClose() {
            $(this).hide();

            if (onOkeyClick && typeof (onOkeyClick) === 'function') {
                onOkeyClick();
            }

            $(".statusContainer").show();

            $("#totalProgressBar").data("kendoProgressBar").value(0);
            $("#loadingProgressBar").data("kendoProgressBar").value(0);
            $(".loadingInfo h4").text("Loading " + itemsToLoad[0]);
            $(".chunkStatus").text(1);
        }

        $(".reloadButton").click(function () {
            _onClose();
        });

        $globalMessageWindow.parent().find('.k-i-close').on('click', function () {
            _onClose();
        });
    };
})();