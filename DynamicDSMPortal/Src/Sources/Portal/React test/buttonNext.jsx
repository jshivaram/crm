export default class ButtonNext extends React.Component{
    render(){
        return <button className = 'button-next' onClick ={this.props.onClick}>Next</button>
    }
}