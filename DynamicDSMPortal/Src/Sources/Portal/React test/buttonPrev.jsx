export default class ButtonPrev extends React.Component{
    render(){
        return <button className = 'button-prev' onClick ={this.props.onClick}>Previous</button>
    }
}