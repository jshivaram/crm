import StepMenu from './stepper.jsx'
import Tabber from './tabForm.jsx'
import ButtonPrev from './buttonPrev.jsx'
import ButtonNext from './buttonNext.jsx'

export default class MainContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tabCount: +this.props.tabCount,
            currentTab: +this.props.currentTab
        };
    }

    render() {

        return (
            <div>
                <StepMenu stepCount={this.state.tabCount} currentStep={this.state.currentTab} />
                <Tabber tabCount={this.state.tabCount} activeTabNumber={this.state.currentTab} />
                <ButtonPrev onClick={this.prevTabWrapper()} />
                <ButtonNext onClick={this.nextTabWrapper()} />
            </div>
        );

    }

    nextTabWrapper() {

        let self = this;

        return function () {
            let tabCount = self.state.tabCount;
            let currentTab = self.state.currentTab ;
            if (tabCount > currentTab + 1) {
                self.setState({ currentTab: currentTab + 1 });
            }

        }
    }
    prevTabWrapper() {

        let self = this;

        return function () {
            let tabCount = self.state.tabCount;
            let currentTab = self.state.currentTab;

            if (0 < currentTab) {
                self.setState({ currentTab: currentTab - 1 });
            }
        }
    }
}