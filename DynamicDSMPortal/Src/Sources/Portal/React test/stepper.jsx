const currentElementClassName = 'current-element-step step-element';
const doneStepElementClassName = 'done-step step-element';
const unDoneStepElementClassName = 'undone-step step-element';
import _ from 'lodash'

export default class StepMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stepCount: +this.props.stepCount,
            currentStep: +this.props.currentStep
        };
    }

    render() {
        const stepsCount = this.props.stepCount;
        const currentStep = this.props.currentStep;

        const steps = (_.range(0, stepsCount)).map((el, i) => {

            if (i === currentStep) {
                return <Step class={currentElementClassName} key={i} />
            } else if (i < currentStep) {
                return <Step class={doneStepElementClassName} key={i} />
            } else if (i > currentStep) {
                return <Step class={unDoneStepElementClassName} key={i} />
            }
        });

        return (
            <div className='stepper'>
                {
                    steps
                }
            </div>
        );
    }
}


class Step extends React.Component {
    constructor(props) {
        super(props);
        this.state = { class: this.props.class };
    }
    render() {
        return <div className={this.props.class} key={this.props.key}>

        </div>
    }


}