const activeTabClass = 'tab-is-active';

class Tab extends React.Component {

    render() {
        return <div className={this.props.class} >{'Number =' + this.props.number + ' '}Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit quidem vitae tenetur voluptate.</div>
    }
}

export default class Tabber extends React.Component {
    render() {
        let tabCount = this.props.tabCount;
        let activeTabNumber = this.props.activeTabNumber;

        const tabs = (_.range(0, tabCount)).map((el, i) => {
            if (i === activeTabNumber) {
                return <Tab class={activeTabClass + ' tab'} key={i} number={i} />
            } else {
                return <Tab class={'tab'} key={i} number={i}/>
            }
        });

        return <div className='tabber'>
            {
                tabs
            }
        </div>
    }

}