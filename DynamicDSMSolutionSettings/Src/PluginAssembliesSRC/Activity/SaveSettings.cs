﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.ModelBinding;
using DynamicDSMSettings.Model;
using DynamicDSMSettingsPackage.DynamicDSMSettingsAssembly;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

public class SaveSettings : WorkFlowActivityBase
{

    [Output("Complete")]
    public OutArgument<bool> Complete { get; set; }
    [Output("Result")]
    public OutArgument<string> Result { get; set; }

    [Input("UserInput")]
    public InArgument<string> UserInput { get; set; }


    private string configName = "accentgold_/settings/config.js";
    private static string webrescXml = "<importexportxml><webresources><webresource>{0}</webresource></webresources></importexportxml>";



    public override void ExecuteCRMWorkFlowActivity(CodeActivityContext context, LocalWorkflowContext crmWorkflowContext)
    {
        try
        {
            var userInput = UserInput.Get(context);

            if (string.IsNullOrEmpty(userInput))
            {
                throw new Exception("Saving configuration aborted. Configuration JSON is Empty. Please fill It and try again");
            }
            var configId = GetConfigId(crmWorkflowContext);
            var publishReq = GenerateSaveSettingsRequest(configId, userInput, crmWorkflowContext.GetOrgService(true));
            var requestWithResults = (ExecuteMultipleResponse)crmWorkflowContext.GetOrgService(true).Execute(publishReq);

            Result.Set(context, "");
            if (requestWithResults.Responses.Count > 0)
            {
                var counter = 0;
                foreach (var resp in requestWithResults.Responses)
                {
                    counter++;
                    if (resp.Fault != null)
                    {
                        Result.Set(context, "Error on Request:" + counter + " Message: " + resp.Fault.Message);
                        break;
                    }
                }
            }
            Complete.Set(context, true);
        }
        catch (Exception e)
        {
            crmWorkflowContext.TracingService.Trace("Erorr!!!: " + e.Message + e.StackTrace);
            Complete.Set(context, false);
            Result.Set(context, "Erorr!!!: " + e.Message + e.StackTrace);
        }
    }
    private ExecuteMultipleRequest GenerateSaveSettingsRequest(Guid id, string userInput, IOrganizationService service)
    {
        var data = Convert.ToBase64String(Encoding.UTF8.GetBytes(userInput));

        var executemultiplerequest = new ExecuteMultipleRequest
        {
            Settings = new ExecuteMultipleSettings
            {
                ContinueOnError = true,
                ReturnResponses = true
            },
            Requests = new OrganizationRequestCollection()
        };



        var webSrc = new Entity("webresource", id)
        {
            Attributes = { { "content", data }, { "displayname", configName }, { "name", configName }, { "webresourcetype", new OptionSetValue((int)WebResourceType.Script) } }
        };
        // if config not found
        if (id == Guid.Empty)
        {
            id = service.Create(webSrc);

            if (id != Guid.Empty)
            {
                var publishxmlrequest = new PublishXmlRequest
                {
                    ParameterXml = string.Format(webrescXml, id)
                };

                service.Execute(publishxmlrequest);
            }
            //throw new Exception("Create with id:"+ id);
        }
        //if config Existed
        else
        {
            var updateRequest = new UpdateRequest { Target = webSrc };

            var publishxmlrequest = new PublishXmlRequest
            {
                ParameterXml = string.Format(webrescXml, id)
            };
            executemultiplerequest.Requests.Add(updateRequest);
            executemultiplerequest.Requests.Add(publishxmlrequest);
        }
        return executemultiplerequest;
    }
    private Guid GetConfigId(LocalWorkflowContext crmWorkflowContext)
    {
        Entity result = null;
        var query = new QueryExpression()
        {
            EntityName = "webresource",
            Criteria = new FilterExpression(LogicalOperator.And) { Conditions = { new ConditionExpression("name", ConditionOperator.Equal, configName) } }
        };
        var configs = crmWorkflowContext.GetOrgService(true).RetrieveMultiple(query);
        crmWorkflowContext.TracingService.Trace("Before get configs is. Count = " + configs.Entities.Count);
        if (configs?.Entities?.Count > 0)
        {
            result = configs.Entities[0];
        }

        return result?.Id ?? Guid.Empty;
    }
}

