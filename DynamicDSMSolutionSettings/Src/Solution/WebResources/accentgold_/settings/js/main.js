var baseUrl = "/api/data/v8.2/";
var Xrm = Xrm || window.parent.Xrm;
var clientUrl = Xrm.Page.context.getClientUrl();
var webResourceId = "";
var viewModel = {};
var config = {};
var implementationMappingArray = [];



function init() {
    if (window.configJSON) {
        var defModel = getDefConfigModel();
        config = window.parent.JSON.parse(configJSON);
        var DUImpMappingData = config.DUConfig.DU_MappingImplementation;
        if (DUImpMappingData.length > 0) {
            implementationMappingArray = DUImpMappingData
        }

        for (var key in defModel) {
            if (!defModel.hasOwnProperty(key)) continue;

            var defVal = defModel[key];
            if (!config[key]) {
                // console.log("config1 KEY: " + key);
                config[key] = defVal;
            } else {
                for (var prop in defVal) {
                    if (!defVal.hasOwnProperty(prop)) continue;

                    var existVal = config[key];

                    if (!existVal[prop]) {
                        // console.log("config 2 KEY: " + prop);
                        existVal[prop] = defVal[prop];
                    }
                }
            }
        }
        //   console.log("config : "+config);
    } else {
        config = getDefConfigModel();
    }

    createGrid();

    viewModel = kendo.observable({
        configData: config,
        isEnabled: false,
        // ds of entities for excluding
        excludingEntitiesDs: new kendo.data.DataSource({
            schema: KendoHelper.CRM.defEntityMetadataSchema(),
            sort: {
                field: "Label",
                dir: "asc"
            },
            transport: KendoHelper.CRM.transport(encodeURI(clientUrl + baseUrl + 'EntityDefinitions?$select=DisplayName,SchemaName,MetadataId')),
        }),
        smLibraries: new kendo.data.DataSource({
            schema: {
                data: function (response) {
                    var value = response.value;
                    var model = {};
                    model = value.map(function (el) {
                        return {
                            Label: el.ddsm_name,
                            LogicalName: el.ddsm_espmeasurelibraryid
                        };
                    });
                    return model;
                }
            },
            sort: {
                field: "Label",
                dir: "asc"
            },
            transport: KendoHelper.CRM.transport(clientUrl + baseUrl + "ddsm_espmeasurelibraries" + "?$select=ddsm_name, ddsm_espmeasurelibraryid"),
        }),
        espAdministrators: new kendo.data.DataSource({
            schema: {
                data: function (response) {
                    var value = response.value;
                    var model = {};
                    model = value.map(function (el) {
                        return {
                            Label: el.fullname,
                            LogicalName: el.systemuserid,
                        };
                    });
                    return model;
                }
            },
            sort: {
                field: "Label",
                dir: "asc"
            },
            transport: KendoHelper.CRM.transport(clientUrl + baseUrl + "systemusers" + "?$select=fullname"),
        }),
        applySettings: function (e) {
            Alert.show("Would you like to Apply Settings?", null, [{
                label: "Yes",
                callback: function () {
                    kendo.ui.progress($("#pageBody"), true);
                    var callback = function () {
                        //debugger;
                        $('#applyBtn').hide();
                        kendo.ui.progress($("#pageBody"), false);
                    };

                    //HARDCODE for DU Implementation Mapping
                    viewModel.configData.DUConfig.DU_MappingImplementation = implementationMappingArray;


                    var updatedConfig = "var configJSON = '" + window.parent.JSON.stringify(viewModel.configData) + "';";
                    DDSM.WebApi.CallActionGlobal("ddsm_DDSMSaveSettings", {
                        UserInput: updatedConfig
                    }, callback, function (error) {
                        console.log(error);
                    });
                }
            }, {
                label: "No",
                callback: function () { }
            }], "QUESTION", 500, 200);
        },
        syncMetadata: function (e) {
            Alert.showWebResource("/accentgold_/ESP_mod/html/ESPChooseSyncTargetDate.html", 400, 380, "Sync Metadata", [
                new Alert.Button("Start", function () {
                    if (!window.parent.selectedTargetDate) {
                        Alert.show("Error", "Please select Target Date for continue", null, "ERROR", 400, 200);
                        return;
                    }
                    DDSM.WebApi.CallActionGlobal("ddsm_ESPSyncMetadata", {
                        TargetDate: window.parent.selectedTargetDate
                    }, null, function (error) {
                        console.log(error);
                    });

                }, true), new Alert.Button("Сancel")
            ], null, null, 20);
        },

        DU_AllowSimultaneous: [{
            DisplayName: "Allowed",
            Value: 962080000
        }, {
            DisplayName: "Forbidden",
            Value: 962080001
        }, {
            DisplayName: "Queued (Disabled)",
            Value: 962080002
        }],
        DU_DeduplicationRules: [{
            DisplayName: "Update",
            Value: 962080000
        },
        {
            DisplayName: "Do not do anything",
            Value: 962080001
        }
        ],
        RolesForDiscontinueDs: new kendo.data.DataSource({
            schema: {
                data: function (response) {
                    var value = response.value;
                    var model = {};
                    model = value.map(function (el) {
                        return {
                            Label: el.name,
                            LogicalName: el.roleid,
                        };
                    });
                    return model;
                }
            },
            sort: {
                field: "Label",
                dir: "asc"
            },
            transport: KendoHelper.CRM.transport(clientUrl + baseUrl + "roles" + "?$select=name"),
        }),
        RolesForOnHoldDs: new kendo.data.DataSource({
            schema: {
                data: function (response) {
                    var value = response.value;
                    var model = {};
                    model = value.map(function (el) {
                        return {
                            Label: el.name,
                            LogicalName: el.roleid,
                        };
                    });
                    return model;
                }
            },
            sort: {
                field: "Label",
                dir: "asc"
            },
            transport: KendoHelper.CRM.transport(clientUrl + baseUrl + "roles" + "?$select=name"),
        }),
        EntitiesForProcessFlowDs: new kendo.data.DataSource({
            schema: {
                data: function (response) {
                    var value = response.value;
                    var model = {};
                    var model = value.map(function (el) {
                        return {

                            Label: !!el.DisplayName.UserLocalizedLabel ? el.DisplayName.UserLocalizedLabel.Label : el.LogicalName,
                            LogicalName: el.LogicalName,
                        };
                    });
                    return model;
                }
            },
            sort: {
                field: "Label",
                dir: "asc"
            },
            transport: KendoHelper.CRM.transport(clientUrl + baseUrl + "EntityDefinitions?$select=LogicalName,DisplayName"),
        }),
        EntitiesListDs: new kendo.data.DataSource({
            schema: {
                data: function (response) {
                    var value = response.value;
                    var model = {};
                    var model = value.map(function (el) {
                        return {

                            Label: !!el.DisplayName.UserLocalizedLabel ? el.DisplayName.UserLocalizedLabel.Label : el.LogicalName,
                            LogicalName: el.LogicalName,
                        };
                    });
                    return model;
                }
            },
            sort: {
                field: "Label",
                dir: "asc"
            },
            transport: KendoHelper.CRM.transport(clientUrl + baseUrl + "EntityDefinitions?$select=LogicalName,DisplayName"),
        }),
        meaureFormFiltersDs: [/*{
            DisplayName: "Delivery Agent",
            Value: 962080000
        },*/ {
            DisplayName: "Measure Library",
            Value: 962080001
        },/* {
            DisplayName: "Calculation Type",
            Value: 962080002
        }*/],
    });

    //add DS name for ignore activate Apply Button on page load
    // bind on change model
    viewModel.bind("change", function (e) {
        if (e.field != "excludingEntitiesDs" &&
            e.field != "smLibraries" &&
            e.field != "espAdministrators" &&
            e.field != "RolesForDiscontinueDs" &&
            e.field != "RolesForOnHoldDs" &&
            e.field != "EntitiesListDs" &&
            e.field != "EntitiesForProcessFlowDs") {

            // console.log("Change: " + e.field);
            $('#applyBtn').show();
        }
       // debugger;
        if(e.field == "configData.ESPConfig.Login" || e.field == "configData.ESPConfig.Pass" ||e.field == "configData.ESPConfig.Url"){
            viewModel.set("configData.ESPConfig.LastToken","");
            viewModel.set("configData.ESPConfig.RequestReceivedDate",null);


        }

        // will output the field name when the event is raised
    });
    // apply the bindings
    kendo.bind(document.body.children, viewModel);
    //activate first tab
    $("#tabstrip").data("kendoTabStrip").activateTab($("li:first", "#tabstrip"));
}

//get def config
function getDefConfigModel() {
    return {
        MultiSite_project_id_counter: null,
        project_id_counter: null,
        MappingConfig: getDefMappingConfigModel(),
        ESPConfig: getDefESPConfigModel(),
        CertLibConfig: getDefCertLibConfigModel(),
        DMNConfig: getDefDMNConfigModel(),
        WSConfig: getDefWSConfigModel(),
        ProcessFlowConfig: getDefProcessFlowConfigModel(),
        DUConfig: DataUploaderConfigModel(),
        OnHoldDiscontinueConfig: getDefOnHoldDiscontinueConfig(),
    };
}


function getDefMappingConfigModel() {
    return {
        excludingEntities: null,
        madMappingEntities: [{
            Label: "Measure Template",
            LogicalName: "ddsm_measuretemplate"
        },
        {
            Label: "Project",
            LogicalName: "ddsm_project"
        },
        {
            Label: "Site",
            LogicalName: "ddsm_site"
        },
        {
            Label: "Account",
            LogicalName: "account"
        },
        ]
    };
}

function getDefESPConfigModel() {
    return {
        Url: null,
        Login: null,
        Pass: null,
        DataPortion: null,
        LastToken: null,
        RequestReceivedDate: null,
        EspMapingData: null,
        EspUniqueQdd: null,
        EspQddMappingData: null,
        EspRemoteCalculation: false,
        EspRemoteCalculationApiUrl: null,
        DefaultLibrary: null,
        EspAllMadRequired: false,
        ESPAdministrators: null,
        CreateMeasureFormFilters: [962080001],
        Esp2CrmTypes: {
            ComboboxDoubleDollars0Decimal: "Money",
            ComboboxDoubleDollars2Decimal: "Money",
            ComboboxDouble0Decimal: "Picklist",
            ComboboxDouble1Decimal: "Picklist",
            ComboboxDouble2Decimal: "Picklist",
            ComboboxDouble3Decimal: "Picklist",
            ComboboxDouble4Decimal: "Picklist",
            ComboboxDouble5Decimal: "Picklist",
            ComboboxDouble6Decimal: "Picklist",
            TextboxDouble0Decimal: "Decimal",
            TextboxDouble1Decimal: "Decimal",
            TextboxDouble2Decimal: "Decimal",
            TextboxDouble3Decimal: "Decimal",
            TextboxDouble4Decimal: "Decimal",
            TextboxDouble5Decimal: "Decimal",
            TextboxDouble6Decimal: "Decimal",
            TextboxDoubleInteger: "Decimal",
            TextboxDoubleDollars0Decimal: "Money",
            TextboxDoubleDollars2Decimal: "Money",
            ComboboxStringValue: "Picklist",
            ComboboxDoubleInteger: "Picklist",
            TextboxStringValue: "String",
            TextboxStringDateMMDDYYYY: "DateTime"
        }
    };
}

function getDefDMNConfigModel() {
    return {
        RemoteCalculation: false,
        RemoteCalculationApiUrl: null,
        RemoteCalculationApiUrlSync: null,
        WebSocketURL: null,
    };

}

function getDefCertLibConfigModel() {
    return {};
}

function getDefWSConfigModel() {
    return {
        additionalDataTypeWebsocketUrl: "",
        milestoneWebsocketUrl: ""
    };
}


function getDefProcessFlowConfigModel() {
    return {
        ddsm_ddsm_project_processflow: false
    };
}

function DataUploaderConfigModel() {
    return {
        DU_ExcelIsHeader: true,
        DU_RequestsCount: 100,
        DU_PageRecordsCount: 1000,
        DU_CallExecuteMultiple: false,
        DU_DuplicateDetection: true,
        DU_AllowSimultaneous: 962080001,
        DU_DeduplicationRules: 962080000,
        DU_RemoteCalculation: false,
        DU_ParserRemoteCalculationApiUrl: "",
        DU_CreatorRemoteCalculationApiUrl: "",
        DU_OrderEntitiesList: null,
        DU_MappingImplementation: null
    };
}

function getDefOnHoldDiscontinueConfig() {
    return {
        RolesForDiscontinue: null,
        RolesForOnHold: null
    };
}

var compare = function compare(a, b) {
    if (a.Label < b.Label)
        return -1;
    if (a.Label > b.Label)
        return 1;
    return 0;
}

function createGrid() {
    var options = {
        dataSource: {
            data: implementationMappingArray,
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        EntityLogicalName: {
                            editable: true
                        },
                        MappingImplementation: {
                            editable: true
                        },
                        ID: {
                            editable: true
                        }
                    }
                }
            },
            change: function (e) {
                if (!e.action || e.action === "sync") {
                    return;
                }
                if (e.action === "add") {
                    var numArray = implementationMappingArray.map(function (elem) {
                        return elem.ID;
                    });
                    numArray = numArray.sort(function (a, b) { return a - b; });
                    var currentID = numArray[numArray.length - 1] + 1;
                    if (!currentID) {
                        currentID = 0;
                    }
                    implementationMappingArray.push(
                        {
                            ID: currentID,
                            MappingImplementation: {
                                Id: "",
                                Label: ""
                            },
                            EntityLogicalName: {
                                LogicalName: "",
                                Label: ""
                            }
                        });
                    var grid = $('#DU_MappingImplementationGrid').data("kendoGrid");
                    grid.dataSource.data()[0].ID = currentID;
                    if (implementationMappingArray.length > 0) {
                        var input = $('#DU_MappingImplementation');
                        input.val(implementationMappingArray)
                    } else {
                        var input = $('#DU_MappingImplementation');
                        input.val("")
                    }
                }
                if (e.action === "remove") {
                    var itemId = e.items[0].ID;
                    implementationMappingArray = implementationMappingArray.filter(function (element) {
                        return element.ID !== itemId;
                    });
                    if (implementationMappingArray.length > 0) {
                        var input = $('#DU_MappingImplementation');
                        input.val(implementationMappingArray)
                    } else {
                        var input = $('#DU_MappingImplementation');
                        input.val("")
                    }
                }
                $('#applyBtn').show();
            },
        },
        scrollable: true,
        pageable: {
            numeric: false,
            previousNext: false,
            messages: {
                display: "Total: {2}"
            }
        },
        height: 300,
        toolbar: ["create"],
        columns: [
            {
                field: "EntityLogicalName",
                title: "Entity Logical Name",
                width: "150px",
                editor: entityLogicalNameDropDownEditor,
                template: "#: EntityLogicalName.Label # (#:EntityLogicalName.LogicalName #)"
            },
            {
                field: "MappingImplementation",
                title: "Mapping Implementation",
                width: "175px",
                editor: MappingImplementationDropDownEditor,
                template: "#=MappingImplementation.Label # (#:MappingImplementation.Id #)"
            },
            {
                field: "ID",
                hidden: true
            },
            {
                command: ["edit", "destroy"],
                title: "&nbsp;",
                width: "150px"
            },
        ],
        editable: "inline"
    };
    $('#DU_MappingImplementationGrid').kendoGrid(options);
}



function entityLogicalNameDropDownEditor(container, options) {
    var queryOptions = {
        Select: ['LogicalName', 'DisplayName']
    }
    GlobalJs.WebAPI.Get("EntityDefinitions", null, queryOptions).then(
        function (entityDefinitions) {
            entityDefinitions.value = entityDefinitions.value.filter(function (elem) {
                return elem.DisplayName.UserLocalizedLabel && elem.DisplayName.UserLocalizedLabel.Label
            });
            var entityDefinitionsDropDownData = entityDefinitions.value.map(function (elem) {
                var element = {
                    LogicalName: elem.LogicalName,
                    Label: elem.DisplayName.UserLocalizedLabel.Label
                };
                return element;
            });
            var defaultOption = {
                Label: "--",
                LogicalName: "00000000-0000-0000-0000-000000000000"
            };
            entityDefinitionsDropDownData[entityDefinitionsDropDownData.length] = entityDefinitionsDropDownData[0];
            entityDefinitionsDropDownData[0] = defaultOption;
            entityDefinitionsDropDownData = entityDefinitionsDropDownData.sort(compare);
            var entityDefinitionsDataSource = new kendo.data.DataSource({
                data: entityDefinitionsDropDownData
            });
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: true,
                    dataTextField: "Label",
                    dataValueField: "LogicalName",
                    dataSource: entityDefinitionsDataSource,
                    change: function (e) {
                        var data = e.sender.dataItem();
                        for (var i = 0; i < implementationMappingArray.length; i++) {
                            if (implementationMappingArray[i].ID == options.model.ID) {
                                implementationMappingArray[i].EntityLogicalName.LogicalName = data.LogicalName;
                                implementationMappingArray[i].EntityLogicalName.Label = data.Label;
                            }
                        }
                        options.model.EntityLogicalName = {};
                        options.model.EntityLogicalName.LogicalName = data.LogicalName;
                        options.model.EntityLogicalName.Label = data.Label;
                        if (implementationMappingArray.length > 0) {
                            var input = $('#DU_MappingImplementation');
                            input.val(implementationMappingArray)
                        } else {
                            var input = $('#DU_MappingImplementation');
                            input.val("")
                        }
                        $('#applyBtn').show();
                    },
                });
        },
        function (error) {
            console.log(error);
        }
    );
}

function MappingImplementationDropDownEditor(container, options) {
    var queryOptions = {
        Select: ['ddsm_mappingimplementationid', 'ddsm_name']
    }
    GlobalJs.WebAPI.Get("ddsm_mappingimplementations", null, queryOptions).then(
        function (mappingImplementations) {
            var mappingImplementationsDropDownData = mappingImplementations.value.map(function (elem) {
                var element = {
                    Id: elem.ddsm_mappingimplementationid,
                    Label: elem.ddsm_name
                };
                return element;
            });
            var defaultOption = {
                Label: "--",
                Id: "00000000-0000-0000-0000-000000000000"
            };
            mappingImplementationsDropDownData[mappingImplementationsDropDownData.length] = mappingImplementationsDropDownData[0];
            mappingImplementationsDropDownData[0] = defaultOption;
            mappingImplementationsDropDownData = mappingImplementationsDropDownData.sort(compare);
            var mappingImplementationsDataSource = new kendo.data.DataSource({
                data: mappingImplementationsDropDownData
            });
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: true,
                    dataTextField: "Label",
                    dataValueField: "Id",
                    dataSource: mappingImplementationsDataSource,
                    change: function (e) {
                        var data = e.sender.dataItem();
                        for (var i = 0; i < implementationMappingArray.length; i++) {
                            if (implementationMappingArray[i].ID == options.model.ID) {
                                implementationMappingArray[i].MappingImplementation.Id = data.Id;
                                implementationMappingArray[i].MappingImplementation.Label = data.Label;
                            }
                        }
                        options.model.MappingImplementation = {};
                        options.model.MappingImplementation.Id = data.Id;
                        options.model.MappingImplementation.Label = data.Label;
                        if (implementationMappingArray.length > 0) {
                            var input = $('#DU_MappingImplementation');
                            input.val(implementationMappingArray);
                        } else {
                            var input = $('#DU_MappingImplementation');
                            input.val("");
                        }
                        $('#applyBtn').show();
                    }
                });
        },
        function (error) {
            console.log(error);
        }
    );
}