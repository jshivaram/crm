//debugger;
//console.log("-- in DDSM.Settings1");
if (typeof (GlobalJs) == "undefined") {
	GlobalJs = {__namespace: true};
}

if (typeof (GlobalJs.DDSM) == "undefined") {
    GlobalJs.DDSM = {__namespace: true};
}

GlobalJs.DDSM.Settings = GlobalJs.DDSM.Settings || {__namespace: true};

(function () {
//console.log("-- in DDSM.Settings2");
    var _this = this;
    this.IsLoaded = false;
    _this.config = {};
    function processConfig(conf, callback) {
        if (window.configJSON) {
            _this.config = JSON.parse(window.configJSON);
            _this.IsLoaded = true;
            if (callback) {
                callback(_this.config);
            }
        }
        else {
            console.log("Can't parse existing config. configJSON is null.");
            return;
            //throw "Config is NULL";
        }
    }
    
    this.GetEspSettings = function (callback, errorCallback) {
        var configName = "ESPConfig";

        if (_this.config && _this.config[configName]) {
            if (callback && _this.config[configName]) {
                callback(_this.config[configName]);
            }
        }
    };
        this.GetMappingSettings = function (callback, errorCallback) {
            var configName = "MappingConfig";
            if (callback && _this.config[configName]) {
                callback(_this.config[configName]);
            }else{
              return _this.config[configName];
            }
        };

        this.GetSettings =  function (callback, errorCallback) {
            console.log("in GetSettings");
            if (callback && _this.config) {
                console.log("callback and config existed.");
                console.dir(_this.config);

                callback(_this.config);
            }
        };

    GlobalJs.Loader("/accentgold_/settings/config.js","Config", "js", processConfig);
}).call(GlobalJs.DDSM.Settings);










