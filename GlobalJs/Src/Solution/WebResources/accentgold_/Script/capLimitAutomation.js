(function () {
    debugger;

    var currentWindow = parent.Xrm.Internal.isTurboForm() ? parent.window : window;
    GlobalJs = currentWindow.GlobalJs;

    if (currentWindow.Xrm.Page.data === null || currentWindow.Xrm.Page.data.entity === null)
        return;

    if (!currentWindow.Xrm.Page.data.entity.getId())
        return;

    if (typeof ($) === 'undefined') {
        $ = currentWindow.$;
        jQuery = currentWindow.jQuery;
    }
    
    try {
        var entityInfo = {};

        entityInfo.Name = currentWindow.Xrm.Page.data.entity.getEntityName();
        if (entityInfo.Name == "ddsm_project")
            entityInfo.ProjectId = currentWindow.Xrm.Page.data.entity.getId();
        else if (entityInfo.Name == "ddsm_financial")
            entityInfo.ProjectId = currentWindow.Xrm.Page.getAttribute('ddsm_projecttofinancialid').getValue()[0].id;
        else 
            return;

        var data = [{
            key: "TargetEntity",
            type: Process.Type.String,
            value: JSON.stringify(entityInfo)
          }]
    
        Process.callAction("ddsm_CapLimitCheck", data,
            function(params) {
                debugger;
                //currentWindow.showSpinner(false);
                var response = JSON.parse(params[0].value);
    
                if (response.IsExecuted) {
                    var message = "";
                    var info = "";
                    if (response.IsLimitReached) {
                        message = "The Customer Cap should be reviewed prior to moving this project forward to the next stage";
                    }
                    if (response.IsCustomerLimitReached) {
                        message += "Customer Cap is reached";
                        info += "<p>Customer Cap for Parent Account's Total Incentive Amount is reached</p>";
                    }
                    if (response.IsSiteLimitReached) {
                        message += "<p>Site Cap is reached</p>";
                        info += "<p>Site Cap for Parent Account's Total Incentive Amount is reached</p>";
                    }
                    if (info != "") {
                        info += "<p>Please manually review to determine the payment amount</p>";
                    }

                    if (message)
                        currentWindow.Alert.show(message, info, [{ label: "OK" }], "WARNING");   
                } else { 
                    console.log("CapLimitCheck error");
                }
    
                
            },
            function(e) {
                debugger;
            }
        );
    } catch(e) {
        console.log(e);
    }

    
})();