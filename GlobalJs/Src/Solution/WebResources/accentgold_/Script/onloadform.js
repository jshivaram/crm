// Init GlobalJs.ContentPanel
GlobalJs.ContentPanel = GlobalJs.ContentPanel || {};
(function () {
        this.onLoad = function (callback) {
            $(document).ready(function() {
                $($("#crmContentPanel iframe").contents()).ready(function() {
                    console.log("content IFrame Loaded");
                    if(!!callback && typeof callback != "undefined" && typeof callback == "function") {
                        callback();
                    }
                });
            })
        };
    GlobalJs.crmWindow().initGlobalJsContentPanel = true;
}).call(GlobalJs.ContentPanel);

//Standart function
var spinnerGlobal = window.top.spinnerGlobal
    , timeOut_CheckBackgroundProcess = 5000;


//Example:
//Show: showSpinner(true) //window.top
//showSpinner(true, null, "Text Message")
//showSpinner(true, document.getElementsById("Element Id"), "Text Message") //for current element
//Stop: showSpinner(false)
var showSpinner = GlobalJs.showSpinner;

//Example:
//CheckBackgroundProcess(null, true)
//CheckBackgroundProcess('{"EntityNames":[ { "EntityName":"ddsm_measure"}, {"EntityName":"ddsm_financial"}]}', function() {showSpinner(true, null, "Please note! <br>There are background activities <br>working with the record.");})
//CheckBackgroundProcess('{"EntityNames":[ { "EntityName":"ddsm_measure"}, {"EntityName":"ddsm_financial"}]}', null, null, null, true, 3000)

function CheckBackgroundProcess (relatedEntities, onStartCall, successCallback, errorCallback, loop, timeOut) {

    relatedEntities = (!!relatedEntities && typeof relatedEntities =='string') ? relatedEntities : null;
    onStartCall = (!!onStartCall && typeof onStartCall =='function') ? onStartCall : null;
    successCallback = (!!successCallback && typeof successCallback =='function') ? successCallback : null;
    errorCallback = (!!errorCallback && typeof errorCallback =='function') ? errorCallback : null;
    loop = (!!loop && typeof loop =='boolean') ? loop : false;
    timeOut = (!!timeOut && typeof timeOut =='number') ? loop : 5000;

    console.log(">>> Check Background Process Job");

    var params = [{
        key: "Target",
        type: Process.Type.EntityReference,
        value: {
            id: Xrm.Page.data.entity.getId(),
            entityType: Xrm.Page.data.entity.getEntityName()
        }
    }];
    if(!!relatedEntities) {
        params.push({
            key: "IsCheckRelatedEntities",
            type: Process.Type.Bool,
            value: true
        });
        params.push({
            key: "ListRelatedEntities",
            type: Process.Type.String,
            value: relatedEntities
        });
    }
    //console.dir(params);
    Process.callAction("ddsm_DDSMManualcheckofBackgroundJobs", params,
        function(params) {
            //console.dir(params);
            var rs = JSON.parse(params[0].value);            
            var isBackgrondJobs = rs.Result;
            if(isBackgrondJobs)
            {
                if(!!onStartCall && typeof onStartCall == "function") {
                    onStartCall();
                }

                if(loop) {
                    setTimeout(function (){
                        CheckBackgroundProcess(relatedEntities, onStartCall, successCallback, errorCallback, loop, timeOut);
                    }, timeOut || timeOut_CheckBackgroundProcess);
                } else {
                    if(!!successCallback && typeof successCallback == "function") {
                        successCallback(true);
                    }
                }
            } else {
                if(!!successCallback && typeof successCallback == "function") {
                    successCallback(false);
                }
            }
        },
        function(e) {
            if(!!errorCallback && typeof errorCallback == "function") {
                errorCallback(e);
            } else {
                console.log(e);
            }
        }
    );
}

function ProcessStageTail() {
    //$(".processStageContainer").unbind("click");
    if(!Xrm.Page.data.process.getActiveStage())
        return;

    $("#processStep_"+Xrm.Page.data.process.getSelectedStage().getSteps().getByIndex(Xrm.Page.data.process.getSelectedStage().getSteps().getLength()-1).getAttribute()).hide();
        var stageId = $(this).attr("id");
        var stageIdx = parseInt((stageId.split("_"))[1]);
        //console.log("Stage Id: " + stageId + "; Stage Idx: " + stageIdx);
        var activeMsIsx = -1;
        if(!!Xrm.Page.getAttribute("ddsm_pendingmilestoneindex")) {
            activeMsIsx = parseInt(Xrm.Page.getAttribute("ddsm_pendingmilestoneindex").getValue()) - 1;
        }
        //console.log("activeMsIsx: " + activeMsIsx);
        var timeoutCount = 5;
        if(stageIdx == activeMsIsx && activeMsIsx != 0) {
            $("#stageBackActionContainer").addClass("hidden");
            $("#stageBackActionContainer").addClass("disabled");
            for(var i = 1; i <= timeoutCount; i++) {
                if(i != timeoutCount) {
                    setTimeout(function(){$("#stageBackActionContainer").addClass("hidden");$("#stageBackActionContainer").addClass("disabled");}, i * 25);
                }
                if(i == timeoutCount) {
                    setTimeout(function(){
                        //$( ".processStageContainer" ).on( "click", ProcessStageTail);

                        $("#stageBackActionContainer").addClass("hidden");
                        $("#stageBackActionContainer").addClass("disabled");
                        if(window.StageClickIdx != stageIdx) {
                            //console.log("stageBackActionContainer");
                            window.StageClickIdx = stageIdx;
                            //$("#processStagesContainer").width($("#processStagesContainer").width() + 20);
                        }
                    }, i * 25);
                }
            }

        } else if(activeMsIsx == 0 && stageIdx == activeMsIsx) {

        } else {

            $("#stageSetActiveActionContainer").addClass("hidden");
            $("#stageSetActiveActionContainer").addClass("disabled");
            for(var i = 1; i <= timeoutCount; i++) {
                if(i != timeoutCount) {
                    setTimeout(function(){$("#stageSetActiveActionContainer").addClass("hidden");$("#stageSetActiveActionContainer").addClass("disabled");}, i * 25);
                }
                if(i == timeoutCount) {
                    setTimeout(function(){
                        //$( ".processStageContainer" ).on( "click", ProcessStageTail);

                        $("#stageSetActiveActionContainer").addClass("hidden");
                        $("#stageSetActiveActionContainer").addClass("disabled");
                        if(window.StageClickIdx != stageIdx) {
                            //console.log("stageBackActionContainer");
                            window.StageClickIdx = stageIdx;
                            $("#processStagesContainer").width($("#processStagesContainer").width() + 91);
                        }
                    }, i * 25);
                }
            }

            //disabled fields
            let lenStr = (Xrm.Page.data.process.getSelectedStage().getSteps().getLength()).toString();
            //console.log("Steps Length: " + lenStr);
            for(var i = 0; i < parseInt(lenStr); i++)
            {
                if($("#header_process_"+Xrm.Page.data.process.getSelectedStage().getSteps().getByIndex(i).getAttribute() + " span.ms-crm-Inline-LockIcon").length > 0) {
                    $("#header_process_"+Xrm.Page.data.process.getSelectedStage().getSteps().getByIndex(i).getAttribute() + " span.ms-crm-Inline-LockIcon").removeAttr("style");
                } else {
                    $("#header_process_"+Xrm.Page.data.process.getSelectedStage().getSteps().getByIndex(i).getAttribute()).append('<span class="ms-crm-Inline-LockIcon"><img src="/_imgs/imagestrips/transparent_spacer.gif" class="ms-crm-ImageStrip-inlineedit_locked" alt=""></span>');
                }
                $("#header_process_"+Xrm.Page.data.process.getSelectedStage().getSteps().getByIndex(i).getAttribute() + " div.ms-crm-Inline-Value").removeAttr("style");
                $("#header_process_"+Xrm.Page.data.process.getSelectedStage().getSteps().getByIndex(i).getAttribute() + " div.ms-crm-Inline-Value").addClass("ms-crm-Inline-Locked");
                $("#header_process_"+Xrm.Page.data.process.getSelectedStage().getSteps().getByIndex(i).getAttribute()).attr("data-controlmode", "locked");
                $("#header_process_"+Xrm.Page.data.process.getSelectedStage().getSteps().getByIndex(i).getAttribute() +" div.ms-crm-Inline-Edit table").attr("controlmode", "locked");



                $("#header_process_"+Xrm.Page.data.process.getSelectedStage().getSteps().getByIndex(i).getAttribute() + " div.ms-crm-Inline-Value").unbind('mouseover').unbind('mouseout')
                    .off('mouseover')
                    .die('mouseover')
                    .off('mouseout')
                    .die('mouseout')
                    .off('click')
                    .die('click');

                $("#header_process_"+Xrm.Page.data.process.getSelectedStage().getSteps().getByIndex(i).getAttribute() + " div.ms-crm-Inline-Value").removeAttr('onmouseover');
                $("#header_process_"+Xrm.Page.data.process.getSelectedStage().getSteps().getByIndex(i).getAttribute() + " div.ms-crm-Inline-Value").removeAttr('onmouseout');
            }

        }
}
function OnLoadEntityForm() {
    console.log(">>> OnLoad Entity Form");
    GlobalJs.showSpinner(false);

    /*
        if(typeof window.startOnLoadEntityForm != 'undefined' && typeof window.startOnLoadEntityForm =='boolean' && window.startOnLoadEntityForm) {
            return;
        }
    */
    if(!!Xrm && !!Xrm.Page && !!Xrm.Page.data && !!Xrm.Page.data.entity) {
        if (Xrm.Page.ui.getFormType() != 2) {
            return;
        }

        window.startOnLoadEntityForm = true;
        //Verifying Background Process Job
        //showSpinner(true, null, null);

        if (Xrm.Page.ui.getFormType() == 1) {
            return;
        }

        switch (Xrm.Page.data.entity.getEntityName()) {
            case "ddsm_projectgroup":
                CheckBackgroundProcess(
                    '{"EntityNames":[ { "EntityName":"ddsm_project"}, {"EntityName":"ddsm_projectgroupfinancials"}]}'
                    , function() {
                        if(window.top.OnLoadFormShowSpinner)
                            showSpinner(true, null, "Please note! <br>There are background activities <br>working with the record.");
                    }
                    , function(checkStatus) {
                        console.log(checkStatus);
                        showSpinner(false);
                    }
                    , function(msg){
                        console.log(msg);
                        showSpinner(false);
                    }
                    , true
                    , 5000);

                break;
            case "ddsm_project":
                // unbind and save current event handlers
                //$("#stageAdvanceActionContainer").unbind("click");
                $("#stageBackActionContainer").unbind("click");
                $("#stageSetActiveActionContainer").unbind("click");
                $(".processStageTailContainer").unbind("click");

                $("#stageSetActiveActionContainer").addClass("hidden");
                $("#stageSetActiveActionContainer").addClass("disabled");
                $("#stageBackActionContainer").addClass("hidden");
                $("#stageBackActionContainer").addClass("disabled");

                //setHandler
                //Bind Next Stage
                //$("#stageAdvanceActionContainer").click(function(e){e.preventDefault();return false;});
                //Bind Prev Stage
                $("#stageBackActionContainer").click(function(e){e.preventDefault();return false;});
                //Bind Set Active
                $("#stageSetActiveActionContainer").click(function(e){e.preventDefault();return false;});

                $(".processStageContainer").click(function(e){e.preventDefault();return false;});

                window.StageClickIdx = 0;
                //$( ".processStageContainer" ).on( "click", ProcessStageTail);

                if(!!Xrm.Page.data.process.getActiveStage())
                    $("#processStep_"+Xrm.Page.data.process.getActiveStage().getSteps().getByIndex(Xrm.Page.data.process.getActiveStage().getSteps().getLength()-1).getAttribute()).hide();

                CheckBackgroundProcess(
                    '{"EntityNames":[ { "EntityName":"ddsm_measure"}, {"EntityName":"ddsm_financial"}]}'
                    , function() {
                        if(window.top.OnLoadFormShowSpinner)
                            showSpinner(true, null, "Please note! <br>There are background activities <br>working with the record.");
                    }
                    , function(checkStatus) {
                        console.log(checkStatus);
                        showSpinner(false);
                    }
                    , function(msg){
                        console.log(msg);
                        showSpinner(false);
                    }
                    , true
                    , 5000);
                break;
            case "ddsm_measure":
                CheckBackgroundProcess(
                    '{"EntityNames":[ { "EntityName":"ddsm_rateclass"}]}'
                    , function() {
                        if(window.top.OnLoadFormShowSpinner)
                            showSpinner(true, null, "Please note! <br>There are background activities <br>working with the record.");
                    }
                    , function(checkStatus) {
                        console.log(checkStatus);
                        showSpinner(false);
                    }
                    , function(msg){
                        console.log(msg);
                        showSpinner(false);
                    }
                    , true
                    , 5000);
                break;
            default :
                CheckBackgroundProcess(
                    null
                    , function() {
                        if(window.top.OnLoadFormShowSpinner)
                            showSpinner(true, null, "Please note! <br>There are background activities <br>working with the record.");
                    }
                    , function(checkStatus) {
                        console.log(checkStatus);
                        showSpinner(false);
                    }
                    , function(msg){
                        console.log(msg);
                        showSpinner(false);
                    }
                    , true
                    , 5000);
                break;
        }
    }
}

(function(){
    OnLoadEntityForm();
})();

