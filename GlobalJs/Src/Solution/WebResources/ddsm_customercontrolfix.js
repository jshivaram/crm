    var timerId;

    function FixCustomersControl() {
       // debugger;
        if (!window.customerControlFix &&  Xrm && Xrm.Page && Xrm.Page.data && Xrm.Page.data.entity) {
            getEntityMetadata(Xrm.Page.data.entity.getEntityName(), processControls)
        }
    }
    FixCustomersControl();

    function FixControl(sss) {
        //debugger;
        if (!sss) return;


        var nameFiled = sss + '_i';
        var menuName = '#Dialog_' + nameFiled + '_IMenu';
        var lookup = document.getElementById(nameFiled);
        if (!lookup) {
            if (!timerId) {
                timerId = setInterval(function() {
                    FixControl(sss);
                }, 400);
            }
        } else {
            clearInterval(timerId);
            
            $('#' + nameFiled).on('click', function() {
                // select the target node

                console.log("menuName: " + menuName);
                var target = document.querySelector(menuName);
                if (target && !window.AlreadyCustomerTuned) {
                    window.AlreadyCustomerTuned = true;
                    // create an observer instance
                    var observer = new MutationObserver(function(mutations) {
                        mutations.forEach(function(mutation) {
                            if (mutation.attributeName == "displayed") {
                                addCustomButtonsToFooter(menuName);
                            }
                        });
                    });
                    // configuration of the observer:
                    var config = { attributes: true, childList: false, characterData: false };
                    // pass in the target node, as well as the observer options
                    observer.observe(target, config);
                } else {
                    setTimeout(function() {
                        console.log("By Timeout:");
                        addCustomButtonsToFooter(menuName);
                    }, 400);
                }
            });
            setTimeout(function() {
                console.log("By Timeout after Onclick bind:");
                addCustomButtonsToFooter(menuName);
            }, 400);

        }
    }

    function addCustomButtonsToFooter(menuName) {
        var footer = $(menuName + ' > div.ms-crm-IL-Footer > div.ms-crm-InlineLookup-FooterSection-AddNew');
        footer.html('<a href="#" id="addNewAccLookupBtn" onclick="createCustomer()" title="Create a new Account." tabindex="0" class="ms-crm-InlineLookup-FooterSection-AddAnchor"><img src="/_imgs/add_10.png" class="ms-crm-InlineLookup-FooterSection-AddImage">New</a>');
        // footer.html('<a href="#" id="addNewAccLookupBtn" onclick="createCustomer()" title="Create a new Account." tabindex="0" class="ms-crm-InlineLookup-FooterSection-AddAnchor"><img src="/_imgs/add_10.png" class="ms-crm-InlineLookup-FooterSection-AddImage">Account</a><a href="#" id="addNewContLookupBtn" onclick="createContact()" title="Create a new Contact." tabindex="0" class="ms-crm-InlineLookup-FooterSection-AddAnchor"><img src="/_imgs/add_10.png" class="ms-crm-InlineLookup-FooterSection-AddImage">Contact</a>');
    }

    function createContact() {
        createCustomer("contact");
    }

    function createCustomer(recordType) {
        if (!recordType) recordType = "account";
        var successCallback = function(e) {
            var lookupValue = new Array();
            lookupValue[0] = e.savedEntityReference;
            var control = Xrm.Page.getAttribute("customerid");
            control.setValue(lookupValue);
            control.fireOnChange();
        };

        Xrm.Utility.openQuickCreate(recordType, null, null).then(successCallback, null);
    }


    function getEntityMetadata(entityName, callback) {
        
        window.customerControlFix =true;
        var organizationUrl = Xrm.Page.context.getClientUrl();
        var query = "EntityDefinitions?$select=LogicalName&$expand=Attributes($select=LogicalName,AttributeType,AttributeOf)&$filter=LogicalName eq '" + entityName + "'";
        var req = new XMLHttpRequest();
        req.open("GET", encodeURI(clientUrl + baseUrl + query), true);
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.onreadystatechange = function() {
            if (this.readyState == 4) {
                req.onreadystatechange = null;
                if (this.status == 200) {
                    var data = window.top.JSON.parse(this.response);
                    var dat = data.value[0].Attributes;
                    if (callback) {
                        callback(dat);
                    }
                    console.log("Action called successfully");
                } else {
                    console.log("callAction error:" + this.response);
                }
            }
        };
        req.send();
    }


    function processControls(attrs) {
        var length = attrs.length;
        for (var i = 0; i < length; i++) {
            var attr = attrs[i];
            if (attr.AttributeType === "Customer" && attr.AttributeOff == null) {
                console.log(attr.LogicalName);
                FixControl(attr.LogicalName);
            }
        }
    }