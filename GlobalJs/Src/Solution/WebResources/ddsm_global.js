var baseUrl = "/api/data/v8.1/";
var clientUrl = Xrm.Page.context.getClientUrl();

function globalCheck() {
    if (!window.useGlobalJs) {
        console.log('Global JS script Loader');

        var ddsmIncludes = function () {
            LoadResource("/WebResources/ddsm_includes.js", "js", null);
        };

        LoadResource("/WebResources/libs_/require.js", "js", ddsmIncludes);
    }
}

function loadError(oError) {
    console.log("The " + oError.target.src + " is not accessible.");
}
function LoadResource(url, type, callback) {
    window.useGlobalJs = true;
    var callback = callback;
    url = clientUrl + url;
    // Adding the script tag to the head as suggested before
    var head = document.getElementsByTagName('head')[0];
    if (type == "js") {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;

        // Then bind the event to the callback function.
        // There are several events for cross browser compatibility.
        //script.onreadystatechange = callback;
        script.onload = callback;
        script.onerror = function (oError) {
            console.log("The " + oError.target.src + " is not accessible.");
            callback();
        };

        // Fire the loading
        head.appendChild(script);
        console.log | (url);
    }

    if (type == "css") {
        setCss(head, url, callback);
    }

    if (type == 'parent-css') {
        var parentHead = window.parent.document.getElementsByTagName('head')[0];
        setCss(parentHead, url, callback);
    }
}

function setCss(head, url, callback) {
    var css = document.createElement('link');
    css.type = 'text/css';
    css.rel = 'stylesheet';
    css.href = url;

    css.onload = callback;
    css.onerror = function (oError) {
        console.log("The " + oError.target.src + " is not accessible.");
        callback();
    };

    head.appendChild(css);
    console.log | (url);
}

function Format() {
    try {

        //  console.log("in Grid Format"); //debugger;   
        var contFrame0visability = $(window.top.document.getElementById('contentIFrame0')).css('visibility');
        var entityType = "";
        var tblHeaders = "";
        var rows = "";
        if (contFrame0visability == "visible") {
            entityType = $(window.top.document.getElementById('contentIFrame0').contentWindow.document.getElementById('gridBodyTable').rows[1]).attr("otypename");
            tblHeaders = window.top.document.getElementById('contentIFrame0').contentWindow.document.getElementById('gridBodyTable').getElementsByTagName('TH');
            rows = window.top.document.getElementById('contentIFrame0').contentWindow.document.getElementById('gridBodyTable').rows;
        } else {
            entityType = $(window.top.document.getElementById('contentIFrame1').contentWindow.document.getElementById('gridBodyTable').rows[1]).attr("otypename");
            tblHeaders = window.top.document.getElementById('contentIFrame1').contentWindow.document.getElementById('gridBodyTable').getElementsByTagName('TH');
            rows = window.top.document.getElementById('contentIFrame1').contentWindow.document.getElementById('gridBodyTable').rows;
        }




        getCustomControlMetadata(entityType, function (config) {
            config.forEach(function (element) {
                var def = JSON.parse(element.ddsm_definition);
                //  console.log(def.selectedField);                
                fillGrid(def.selectedFieldLabel);
            }, this);

        });

        var fillGrid = function (hdText) {
            //   console.log("Format grid for: " + entityType);
            var timerId = "";
            var hdIndex = 0;
            var i = 0;
            var chkKeyWord = "";
            // var hdText = "Log";
            //Loop through the headers to find the index of hdText
            for (i = 0; i < tblHeaders.length; i++) {
                //  console.log("TAB: " + tblHeaders[i].innerText);
                if (hdText == tblHeaders[i].innerText) {
                    //   console.log("column index is: " + i);
                    hdIndex = i;
                    break;
                }
            } //end of for
            //   console.log("after find column");


            // console.log("after rows");
            var ctr = 0;

            for (i = 1; i < rows.length; i++) {

                // console.log("in rows FOR");
                if ((rows[i].cells[hdIndex].innerText).substring(rows[i].cells[hdIndex].innerText.length - 1, rows[i].cells[hdIndex].innerText.length) == "\n")
                    chkKeyWord = (rows[i].cells[hdIndex].innerText).substring(0, rows[i].cells[hdIndex].innerText.length - 1);
                else
                    chkKeyWord = rows[i].cells[hdIndex].innerText;
                //debugger;



                var json = rows[i].cells[hdIndex].innerText;
                if (json.startsWith("[")) { //for array
                    var listOfVal = JSON.parse(json);
                    var list = listOfVal.map(function (val) {
                        if (val["Label"]) {
                            return val["Label"];
                        }
                    });
                    rows[i].cells[hdIndex].innerText = list.join(", ");
                }
                if (json.startsWith("{")) { //for one
                    var val = JSON.parse(json);
                    if (val.Label) {
                        rows[i].cells[hdIndex].innerText = val.Label;
                    }
                }

            }
        }



    } catch (e) {
        console.error("In Grid Format: " + e);
    }
}

function getCustomControlMetadata(entityName, callback) {
    callAction(entityName, callback);
}

function callAction(entityName, callback) {
    if (!entityName || entityName == "") return;
    var organizationUrl = Xrm.Page.context.getClientUrl();
    var data = {
        "EntityName": entityName,
    };
    var query = "ddsm_DDSMGetCustomControlMetadata";
    var req = new XMLHttpRequest();
    req.open("POST", clientUrl + baseUrl + query, true);
    req.setRequestHeader("Accept", "application/json");
    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    req.setRequestHeader("OData-MaxVersion", "4.0");
    req.setRequestHeader("OData-Version", "4.0");
    req.onreadystatechange = function () {
        if (this.readyState == 4) {
            req.onreadystatechange = null;
            if (this.status == 200) {
                var data = window.top.JSON.parse(this.response);
                var dat = window.top.JSON.parse(data.Result);
                if (callback) {
                    callback(dat);
                }

                console.log("Action called successfully");
            } else {
                console.log("callAction error:" + this.response);
            }
        }
    };
    req.send(window.JSON.stringify(data));
}

function loadStyle(head, url) {
    url = clientUrl + url;

    var css = document.createElement('link');
    css.type = 'text/css';
    css.rel = 'stylesheet';
    css.href = url;

    css.onerror = function (oError) {
        console.log("The " + oError.target.src + " is not accessible.");
    };

    head.appendChild(css);
    console.log | (url);
}

window.top.ADT = {};
window.top.ADT.getControlsMetadata = callAction;
window.top.ADT.loadStyle = loadStyle;