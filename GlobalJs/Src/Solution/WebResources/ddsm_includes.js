
var clientUrl = Xrm.Page.context.getClientUrl();

try {
    requirejs.config({
        baseUrl: "../WebResources",
        waitSeconds: 60,
        paths: {
            "jquery": clientUrl + "/WebResources/kendoui_/jquery2.1.1.min",
            "kendo.all.min": clientUrl + "/WebResources/kendoui_/kendo.all.min",
            "ribbon": clientUrl + "/WebResources/adatatypes_/js/ribbon",
            "helper": clientUrl + "/WebResources/adatatypes_/js/helper",
            "notification": clientUrl + "/WebResources/notification_/js/CreateNotification"
        }
    });
    
    requirejs(["ribbon", "notification"]);
}
catch (err) {
    console.log(err.message);
}

function loadGlobalStyles() {
    var head = document.getElementsByTagName('head')[0];

    window.top.ADT.loadStyle(head, "/WebResources/kendoui_/kendo.common.min.css");
    window.top.ADT.loadStyle(head, "/WebResources/kendoui_/kendo.default.min.css");
    window.top.ADT.loadStyle(head, "/WebResources/kendoui_/kendo.office365.min.css");
    window.top.ADT.loadStyle(head, "/WebResources/kendoui_/custom.kendo.office365.css");
}

loadGlobalStyles();

function globalInclude() {

    console.log("-- in globalInclude");

    var Spinnerjs = function () {
        LoadResource("/WebResources/accentgold_/Script/clab.spinner.js", "js", Alertjs);
    };

    var Alertjs = function () {
        LoadResource("/WebResources/mag_/js/alert.js", "js", AgsCore);
    };

    var AgsCore = function () {
        LoadResource("/WebResources/accentgold_/Script/ags.core.js", "js", jsNotificationSetting);
    };

    var jsNotificationSetting = function () {
        LoadResource("/WebResources/bpmn_/js/NotificationSetting.js", "js", jsDmnNotifications);
    };

    var jsDmnNotifications = function () {
        LoadResource("/WebResources/bpmn_/js/DmnNotifications.js", "js", jsAttachmentDocuments);
    };

    var jsAttachmentDocuments = function () {
        LoadResource("/WebResources/accentgold_/AttachDocs/js/AttachmentDocuments.js", "js", jsCustomerContolFix);
    };

    var jsCustomerContolFix = function () {
        LoadResource("/WebResources/ddsm_customercontrolfix.js", "js", SuccessLoadedForm);
    };

    var SuccessLoadedForm = function () {
        LoadResource("/WebResources/accentgold_/Script/onloadform.js", "js", SideMenuParentStyle);
    };

    var SideMenuParentStyle = function () {
        LoadResource("/WebResources/notification_/css/sideMenu.css", "parent-css", NotificationStyle);
    };

    var NotificationStyle = function () {
        LoadResource("/WebResources/notification_/css/notific.css", "css", null);
    };

    LoadResource("/WebResources/mag_/js/process.js", "js", Spinnerjs);

}

globalInclude();