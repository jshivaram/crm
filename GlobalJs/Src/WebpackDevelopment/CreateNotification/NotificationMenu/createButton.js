var createButtonToBody = function(target){
    var $ = window.$;

    var img = $('<img />',{
        class: 'side-menu-open-image'
    });

    var closeIcon = $('<div />',{
        class:"open-side-menu-button"
    });

    closeIcon.append(img);

    $(target).append(closeIcon);

}

export default createButtonToBody;