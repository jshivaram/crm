var createMenuToBody = function(target){
    var $ = window.$;

    var closeIcon = $('<a />',{
        class:"closebtn",
        text:'Menu'
    });

    var clearIcon = $('<a />',{
        class:"clearbtn",
        text:'Clear'
    });

    let panelDiv = $('<div />',{
        class:'settings-panel'
    });

    panelDiv
        .append(closeIcon)
        .append(clearIcon);

    var contentDiv = $('<div />',{
        class:"side-menu-content"
    });

    var sideMenuDiv = $('<div />',{
        id:'side-menu'
    })
        .append(panelDiv)
        .append(contentDiv);

    $(target).append(sideMenuDiv);

}

export default createMenuToBody;