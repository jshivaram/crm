var setEvents = function () {
    var $ = window.$;
    var $parentWindow = $(window.parent.document.body);

    $parentWindow.find('.open-side-menu-button').click(function () {
        $parentWindow.find("#side-menu").css('width', "250px");
    });
    $parentWindow.find('.closebtn').click(function () {
        $parentWindow.find("#side-menu").css('width', "0px");
    });
    $parentWindow.find('.clearbtn').click(function () {
        $parentWindow.find("#side-menu .side-menu-content .message-div").hide('fast', function(){
            $parentWindow.find("#side-menu .side-menu-content").empty();
        });
    });
}

export default setEvents;