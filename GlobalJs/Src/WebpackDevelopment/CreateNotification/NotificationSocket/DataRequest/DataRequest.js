import onOpen from '../WebSocketConfiguration/onOpenEvent'
import onClose from '../WebSocketConfiguration/onCloseEvent'
import onMessage from '../WebSocketConfiguration/onMessageEvent'
import onError from '../WebSocketConfiguration/onErrorEvent'

/*test method dependency*/
//import createNotification from '../NotificationConfiguration/createNotification'
/* */
const $ = window.$;

let dataRequest = function () {
    var url = Xrm.Page.context.getClientUrl() + "/api/data/v8.1/ddsm_admindatas?$select=ddsm_dmnwebsocketurl&$filter= ddsm_name eq 'Admin Data' ";
    $.ajax({
        type: 'GET',
        url: url,
        headers: {
            'Accept': 'application/json',
            "Content-Type": "application/json; charset=utf-8",
            'OData-MaxVersion': "4.0",
            "OData-Version": "4.0"
        }
    }).done(function (response) {
        try {
            var adminData = response.value[0];
            var vsUrl = adminData.ddsm_dmnwebsocketurl;
            if (!window.ws) {
                /* --------test method --------- */
                /*
                window.top.showMessage = function(type ='INFORMATION' ){
                    createNotification({
                        Message:'asd',
                        MessageType:type
                    });
                }
                */
                /* ---------------- */
                if (!vsUrl) {
                    console.warn('Websocket module: Admin data field has empty or undefined websocket server url');
                    throw {};
                }
                var ws = new WebSocket(vsUrl);
                window.ws = ws;

                ws.onopen = onOpen;
                ws.onclose = onClose;
                ws.onmessage = onMessage;
                ws.onerror = onError;
            }
        } catch (error) {
            console.error('Websocket module: Failed to create websocket stream');
        }
    }).fail(function (errorResponse) {
        console.error('Websocket module: Failed to load Admin data "ddsm_dmnwebsocketurl" field before create websocket connection');
        //console.log(errorResponse);
    });
}

export default dataRequest;