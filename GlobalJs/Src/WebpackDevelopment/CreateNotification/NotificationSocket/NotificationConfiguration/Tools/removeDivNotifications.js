import  templateConfig from '../notificationTemplateConfig'
const $ = window.$;

export default function (notifications) {
    const length = notifications.length;
    const lastIndex = length - 1;

    $.each(notifications, function (index, item) {
        if (index !== lastIndex) {
            let isExceptNotification = checkForClassess(item, templateConfig.errorClass, templateConfig.warningClass);

            if (!isExceptNotification) {
                $(item).parent().remove();
            }
        }
    });

}

function checkForClassess  (target, ...classNames) {
    let isContains = false;

    classNames.forEach(item => {
        if (isContains) {
            return;
        }

        isContains |= $(target).find(`.${item}`).length > 0;
    });

    return isContains;
};

