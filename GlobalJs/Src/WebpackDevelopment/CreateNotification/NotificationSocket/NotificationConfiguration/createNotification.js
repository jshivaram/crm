import createNotification from './showNotification'
import './notification.css'
 
export default function (object) {
    const $ = window.$;
    let height = 110;
    let config = {
        height: height,
        baseTop: 40,
        shift: height + 2,
        width: 330
    }

    var $doc = $(window.document);

    var $notification = $doc.find('#notification');
    if ($notification.length === 0) {
        $notification = $('<div />', {
            id: 'notification'
        });

        $doc.find('body').append($notification);
    }

    createNotification(object, $notification, config);
}