import tryCreateNotification from './tryCreateNotification'


var createNotification = function (data, $notification, config) {
    const $ = window.$;
    if (!($().kendoNotification)) {
        let timeOutFunction = function () {
            createNotification(data, $notification, config);
        }
        setTimeout(timeOutFunction, 1000);

    } else {
        let type = data.MessageType.toUpperCase();
        switch (type) {
            case 'WARNING':
                tryCreateNotification($notification, { 
                    message: data.Message,
                     title: 'Warning' 
                    },
                     'warning',
                      config);
                break;

            case 'INFORMATION':
                tryCreateNotification($notification, {
                    message: data.Message,
                    title: 'In Progress'
                },
                    'information',
                     config);
                break;

            case 'SUCCESS':
                tryCreateNotification($notification, {
                    message: data.Message,
                    title: 'Completed' 
                },
                'success',
                config);
                break;

            case 'ERROR':
                tryCreateNotification($notification, {
                    message: data.Message, 
                    title: 'Error' 
                }, 
                'error', 
                config);
                break;
        }
    }
}
export default createNotification;