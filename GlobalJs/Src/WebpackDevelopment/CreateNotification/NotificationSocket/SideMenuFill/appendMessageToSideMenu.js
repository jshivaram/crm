import createDomElement from './createMessageDomElement'

let appendMessageToSideMenu = function(dataMessage){
    try {
        const $ = window.$;

        let createdDomElement = createDomElement(dataMessage);
        let $dataContainer = $(window.parent.document.body).find('#side-menu .side-menu-content');

        if($dataContainer.length === 0){
            console.error('Side-menu data-container not found');
            throw {};
        }

        $dataContainer.append(createdDomElement);                

    } catch (error) {
        console.error('Something wrong with appending element to side-menu content div');
    }
}

export default appendMessageToSideMenu;