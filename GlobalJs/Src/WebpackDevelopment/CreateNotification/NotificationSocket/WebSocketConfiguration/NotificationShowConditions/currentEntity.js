export default function (notificationData) {
    try {
        let currentEntityName;
        try {
            currentEntityName = Xrm.Page.data.entity.getEntityName().toUpperCase();
        } catch (error) {
            console.warn('Websocket module: Cannot get Xrm.Page.data.entity.getEntityName. Filter condition assigned as false.')
            return false;
        }

        let entityName = notificationData.PrimaryEntityName.toUpperCase();
        const isCurrentEntity = (entityName === currentEntityName);

        if (!isCurrentEntity) {
            return false;
            ///we no need to check farther if this condition is false.
        }

        let notificationId = notificationData.RecordId.toUpperCase();

        let currentEntityId;
        try {
            currentEntityId = Xrm.Page.data.entity.getId();
        } catch (error) {
            console.warn('Websocket module: Cannot get Xrm.Page.data.entity.getId. Filter condition assigned as false.')
            return false;
        }
        currentEntityId = currentEntityId.substring(1, currentEntityId.length - 1)
            .toUpperCase();

        const isCurrentEntityId = notificationId === currentEntityId;


        return isCurrentEntity && isCurrentEntityId;
    } catch (error) {
        console.error('Websocket module: Wrong entity record check. Condition assigned as false');
        return false;
    }
}

function tryGetAccountNameFromContentFrame(contentFrameId){
    try{
        return window.parent.document.getElementById(contentFrameId).contentWindow.Xrm.Page.data.entity.getEntityName();
    }catch(error){
        return null;
    }
}
// function tryGetAccountNameFromContentFrame(contentFrameId) {
//     try {
//         return window.parent.document.getElementById(contentFrameId).contentWindow.Xrm.Page.data.entity.getEntityName();
//     } catch (error) {
//         return null;
//     }
// }