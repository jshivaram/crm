export default function (notificationData) {
    try {
        let currentEntityName;
        try {
            currentEntityName = Xrm.Page.data.entity.getEntityName().toUpperCase();
        } catch (error) {
            console.warn('Websocket module: Cannot get Xrm.Page.data.entity.getEntityName. Filter condition assigned as false.')
            return false;
        }     
        return currentEntityName === 'DDSM_DATAUPLOADER';
    } catch (error) {
        console.error('Websocket module: Wrong "ddsm datauploader check. Filter condition assigned as false');
        return false;
    } 
    
}