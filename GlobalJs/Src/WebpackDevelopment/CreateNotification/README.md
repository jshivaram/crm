## How to use project

First, you need to install NODE.js

If you allready have node.js, open command line in current foulder. Write next command 
```
npm install
```

This command get all requried modules, and install it. It will take some time, therefore you should wait until it end.

## How to start 
To join all files in single file you should compile project by webpack. And builded project located inside "build" foulder, and file-name depends of your compile mode.


If you want to compile file you have 4 options to launch webpack

### Develop mode with watch
```
webpack --env=develop --watch --progress --colors
```
Launch webpack without minification, like your own code. Allows you to debug code inside CRM. Additionaly, it will be watching for every file you use, and if you change some of them it rebuild changed files. This style most used when develop and test functionality.

### Single develop without watch
```
webpack --env=develop --progress --colors
```
Launch webpack like previous option, except that moment that it will not be watching you files and compiles whole project for single time

### Babel mode
```
webpack --env=build --progress --colors
```
Create compiled file using babel-core, that makes new ECMA-Script standarts to old kind. It allows to use code on the elder browsers, that dont support new ECMA-Script

### Babel + minific mode
```
webpack --env=prod --progress --colors
```
Create minificated and babeled file. It's production version, therefore this option unrecommended for debug and development. Use it only when you have finished development and testing, and you are prepearing to push code for build servers.

