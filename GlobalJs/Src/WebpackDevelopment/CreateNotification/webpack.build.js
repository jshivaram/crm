var path = require('path');
const ExtractText = require('extract-text-webpack-plugin');


module.exports = {
    entry: ['./entryPoint.js'],
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'CreateNotification.js'
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractText.extract({
                    fallback: 'style-loader',
                    use: {
                        loader: 'css-loader',
                        options: {
                            minimize:true,
                            sourceMap: true
                        }
                    }
                })
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            }
        ]
    },
    plugins: [
        new ExtractText('css/styles.css')
    ]
};

// path: '../../Solution/WebResources/notification_/js',