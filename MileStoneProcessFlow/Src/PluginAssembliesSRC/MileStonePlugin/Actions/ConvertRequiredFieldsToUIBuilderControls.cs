﻿using DDSM.CommonProvider.JSON;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Metadata.Query;
using Microsoft.Xrm.Sdk.Workflow;
using MileStonePlugin.Model;
using MileStonePlugin.Model.UIBuilderConvertor;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;

public class ConvertRequiredFieldsToUIBuilderControls : BaseCodeActivity
{
    [Input("RequiredFieldsJson")]
    public InArgument<string> RequiredFieldsJson { get; set; }

    public override StatusFileDataUploading CurrentOperationStatus { get; set; }

    private readonly IDictionary<string, string> _uibuilderControlsType;
    private IDictionary<string, EntityMetadata> _lookupsMetadata;

    public ConvertRequiredFieldsToUIBuilderControls()
    {
        _uibuilderControlsType = new Dictionary<string, string>
        {
            { "String", "text" },
            { "Picklist", "combobox" },
            { "Decimal", "number" },
            { "Integer", "number" },
            { "Money", "number" },
            { "DateTime", "datetime" },
            { "Lookup", "dropdownlist" },
            { "Memo", "textarea" }
        };
    }

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        var requiredFieldsJson = RequiredFieldsJson.Get<string>(executionContext);
        var requiredFields = JsonConvert.DeserializeObject<ICollection<EntityFieldModel>>(requiredFieldsJson);
        _lookupsMetadata = GetDropDownListMetadata(requiredFields);

        var metadata = new List<BaseControlModel>();
        foreach (var requiredField in requiredFields)
        {
            BaseControlModel control;

            if (requiredField.Attribute.AttributeType == AttributeTypeCode.Lookup.ToString())
            {
                control = GetDropDownControl(requiredField);
            }
            else if (requiredField.Attribute.AttributeType == AttributeTypeCode.Picklist.ToString())
            {
                control = GetComboBoxControl(requiredField);
            }
            else
            {
                control = GetBaseControl(requiredField);
            }

            var duplicateMetadata = metadata.SingleOrDefault(m => m.Name == control.Name);
            if (duplicateMetadata != null)
            {
                continue;
            }

            var additionalData = new AdditionalDataModel
            {
                EntityLogicalName = requiredField.Entity.LogicalName,
                AttributeLogicalName = requiredField.Attribute.LogicalName,
                AttributeType = requiredField.Attribute.AttributeType,
                RecordId = requiredField.RecordId
            };

            string jsonAdditionalData = JsonConvert.SerializeObject(additionalData);
            control.AdditionalData = jsonAdditionalData;

            metadata.Add(control);
        }

        var result = new Dictionary<string, object> { { "Metadata", metadata } };

        string json = JsonConvert.SerializeObject(result);
        Result.Set(executionContext, json);
    }

    private BaseControlModel GetBaseControl(EntityFieldModel requiredField)
    {
        var control = new BaseControlModel
        {
            Label = requiredField.Attribute.DisplayName,
            Name = requiredField.Entity.LogicalName + "." + requiredField.Attribute.LogicalName + requiredField.RecordId,
            Type = _uibuilderControlsType[requiredField.Attribute.AttributeType],
            Required = false
        };

        return control;
    }

    private ComboBoxControlModel GetComboBoxControl(EntityFieldModel requiredField)
    {
        var control = new ComboBoxControlModel
        {
            Label = requiredField.Attribute.DisplayName,
            Name = requiredField.Entity.LogicalName + "." + requiredField.Attribute.LogicalName + requiredField.RecordId,
            Type = _uibuilderControlsType[requiredField.Attribute.AttributeType],
            Required = false,
            FieldsConfig = new FieldsConfigModel
            {
                TextField = "Label",
                ValueField = "Value"
            },
            LocalData = requiredField.Attribute.Options
        };

        return control;
    }

    private DropDownListControlModel GetDropDownControl(EntityFieldModel requiredField)
    {
        var entityMetadata = _lookupsMetadata[requiredField.Attribute.Target];

        var control = new DropDownListControlModel
        {
            Label = requiredField.Attribute.DisplayName,
            Name = requiredField.Entity.LogicalName + "." + requiredField.Attribute.LogicalName + requiredField.RecordId,
            Type = _uibuilderControlsType[requiredField.Attribute.AttributeType],
            Required = false,
            FieldsConfig = new FieldsConfigModel
            {
                TextField = entityMetadata.PrimaryNameAttribute,
                ValueField = entityMetadata.PrimaryIdAttribute
            },
            RemoteData = new RemoteDataModel
            {
                QueryOptions = new Dictionary<string, object>
                {
                    { "FormattedValues", true },
                    { "Select", new List<string>() { entityMetadata.PrimaryIdAttribute, entityMetadata.PrimaryNameAttribute } },
                    { "OrderBy", new List<string>() { entityMetadata.PrimaryNameAttribute } },
                },
                Uri = entityMetadata.LogicalCollectionName,
                ResponsePropertyPath = "List",
                ConvertJsonToObject = false
            }
        };

        return control;
    }

    private IDictionary<string, EntityMetadata> GetDropDownListMetadata(IEnumerable<EntityFieldModel> requiredFields)
    {
        var lookups = requiredFields.Where(r => r.Attribute.AttributeType == AttributeTypeCode.Lookup.ToString())
            .Select(r => r.Attribute.Target).Distinct().ToList();
        var requests = new OrganizationRequestCollection();

        foreach (var lookup in lookups)
        {
            var request = new RetrieveEntityRequest
            {
                LogicalName = lookup

            };
            requests.Add(request);
        }

        var executeMultipleRequest = new ExecuteMultipleRequest
        {
            Requests = requests,
            Settings = new ExecuteMultipleSettings() { ContinueOnError = true, ReturnResponses = true }
        };

        var executeMultipleResponses = ((ExecuteMultipleResponse)_objCommon.GetOrgService(systemCall: true).Execute(executeMultipleRequest)).Responses.ToList();
        var entitiesMetadata = executeMultipleResponses.Select(r => ((RetrieveEntityResponse)r.Response).EntityMetadata)
            .ToDictionary(e => e.LogicalName, e => e);

        return entitiesMetadata;
    }
}
