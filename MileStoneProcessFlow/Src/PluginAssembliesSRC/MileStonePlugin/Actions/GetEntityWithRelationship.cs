﻿using DDSM.CommonProvider.JSON;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Workflow;
using MileStonePlugin.Model;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

public class GetEntityWithRelationship : BaseCodeActivity
{
    public override StatusFileDataUploading CurrentOperationStatus { get; set; }
    [Input("TargetEntity")]
    public InArgument<string> TargetEntity { get; set; }
    [Output("ResultJson")]
    public OutArgument<string> ResultJson { get; set; }
    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        var tracer = executionContext.GetExtension<ITracingService>();
        var context = executionContext.GetExtension<IWorkflowContext>();
        var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        var service = serviceFactory.CreateOrganizationService(context.UserId);
        try
        {
            List<EntityMetadataModel> result = new List<EntityMetadataModel>();
            var targetEntity = TargetEntity.Get<string>(executionContext);
            var retrieveBankEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = targetEntity
            };
            var entityMetadataResponse = (RetrieveEntityResponse)service.Execute(retrieveBankEntityRequest);
            var retrieveBankEntityResponse =
            (RetrieveEntityResponse)_objCommon.GetOrgService(systemCall: true).Execute(retrieveBankEntityRequest);
            OneToManyRelationshipMetadata[] oneToManyRelationships =
                retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships;


            var retrieveIndicatorRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = "ddsm_indicator"
            };
            var entityIndicatorResponse = (RetrieveEntityResponse)service.Execute(retrieveIndicatorRequest);
            var retrieveIndicatorResponse =
            (RetrieveEntityResponse)_objCommon.GetOrgService(systemCall: true).Execute(retrieveIndicatorRequest);

            OneToManyRelationshipMetadata[] oneToManyRelationshipsIndicator =
                retrieveIndicatorResponse.EntityMetadata.OneToManyRelationships;

            foreach (var entity in oneToManyRelationships)
            {
                foreach (var entityOfIndicator in oneToManyRelationshipsIndicator)
                {
                    if (entity.ReferencingEntity == entityOfIndicator.ReferencingEntity)
                    {
                        RetrieveEntityRequest retrieveBankAccountEntityRequest = new RetrieveEntityRequest
                        {
                            EntityFilters = EntityFilters.Entity,
                            LogicalName = entity.ReferencingEntity
                        };
                        RetrieveEntityResponse retrieveBankAccountEntityResponse = (RetrieveEntityResponse)service.Execute(retrieveBankAccountEntityRequest);

                        EntityMetadataModel res = new EntityMetadataModel() { DisplayName = retrieveBankAccountEntityResponse.EntityMetadata.DisplayName.UserLocalizedLabel.Label, LogicalName = entity.ReferencingEntity };

                        bool check = result.Where(x => x.LogicalName == res.LogicalName).Any();
                        if (!check)
                        {
                            result.Add(res);
                        }
                    }
                }
            }
            var jsonResult = JsonConvert.SerializeObject(result.OrderBy(x => x.DisplayName));
            ResultJson.Set(executionContext, jsonResult);
        }
        catch (Exception e)
        {
            throw new InvalidPluginExecutionException(e.Message);
        }
    }
}