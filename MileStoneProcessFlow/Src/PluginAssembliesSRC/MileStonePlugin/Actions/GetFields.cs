﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Workflow;
using MileStonePlugin.Model;
using Newtonsoft.Json;

public class GetFields : CodeActivity
{
    [Input("EntityLogicalName")]
    public InArgument<string> EntityLogicalName { get; set; }

    [Output("Result")]
    public OutArgument<string> Result { get; set; }

    protected override void Execute(CodeActivityContext executionContext)
    {
        var tracer = executionContext.GetExtension<ITracingService>();
        var context = executionContext.GetExtension<IWorkflowContext>();
        var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        var service = serviceFactory.CreateOrganizationService(context.UserId);

        try
        {
            var entityLogicalName = EntityLogicalName.Get<string>(executionContext);

            var entityMetadataRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Attributes,
                LogicalName = entityLogicalName
            };

            var entityMetadataResponse = (RetrieveEntityResponse) service.Execute(entityMetadataRequest);

            var entityAttributes = entityMetadataResponse.EntityMetadata.Attributes.Select(attr =>
                {
                    var attrResult = new AttributeMetadataModel()
                    {
                        DisplayName = GetLabel(attr.DisplayName),
                        LogicalName = attr.LogicalName,
                        AttributeType = attr.AttributeType.ToString()
                    };

                    if (attr.AttributeType == AttributeTypeCode.Picklist)
                    {
                        var options = ((PicklistAttributeMetadata) attr).OptionSet.Options.Select(opt =>
                        {
                            return new Option
                            {
                                Label = GetLabel(opt.Label),
                                Value = opt.Value
                            };
                        }).ToList();

                        attrResult.Options = options;
                    }
                    else if (attr.AttributeType == AttributeTypeCode.Lookup)
                    {
                        attrResult.Target = ((LookupAttributeMetadata)attr).Targets.FirstOrDefault();
                    }

                    return attrResult;
                })
                .Where(attr => !string.IsNullOrEmpty(attr.DisplayName))
                .OrderBy(attr => attr.DisplayName).ToList();

            var jsonResult = JsonConvert.SerializeObject(entityAttributes);
            Result.Set(executionContext, jsonResult);
        }
        catch (Exception e)
        {
            throw new InvalidPluginExecutionException(e.Message);
        }
    }

    public string GetLabel(Label label)
    {
        if (label != null && label.UserLocalizedLabel != null)
            return label.UserLocalizedLabel.Label;
        if (label != null && label.LocalizedLabels != null && label.LocalizedLabels.Count != 0)
            return label.LocalizedLabels.First().Label;

        return string.Empty;
    }
}