﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using MileStonePlugin;
using MileStonePlugin.Interfaces;
using MileStonePlugin.Service;
using MileStonePlugin.Tools;
using Newtonsoft.Json;

public class GetHistoryMileStoneStepsHistory : CodeActivity
{
    [Input("MileStoneListArray")]
    public InArgument<string> MileStoneListArray { get; set; }

    [Output("Json")]
    public OutArgument<string> Json { get; set; }

    protected override void Execute(CodeActivityContext executionContext)
    {
        var tracer = executionContext.GetExtension<ITracingService>();
        var context = executionContext.GetExtension<IWorkflowContext>();
        var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        var service = serviceFactory.CreateOrganizationService(context.UserId);

        try
        {
            var milestoneListArray = MileStoneListArray.Get<string>(executionContext);

            if (string.IsNullOrEmpty(milestoneListArray))
            {
                Json.Set(executionContext, "[]");
                return;
            }

            IReadOnlyList<string> parsedMilestoneListArray = milestoneListArray.Split(',').ToList();

            #region Assert tools

            IMapperHistory mapper = new MapperHistory();
            IMileStoneServiceHistory msSerivce = new MileStoneServiceHistory(service);

            #endregion

            #region Action

            var milestoneManager = new MileStoneManagerHistory(mapper, msSerivce);
            var milestoneSteps = milestoneManager.GetMilestoneHistory(parsedMilestoneListArray);

            #endregion

            #region Set Output

            var jsonResult = JsonConvert.SerializeObject(milestoneSteps);
            Json.Set(executionContext, jsonResult);

            #endregion
        }
        catch (Exception e)
        {
            throw new InvalidPluginExecutionException(e.Message);
        }
    }
}