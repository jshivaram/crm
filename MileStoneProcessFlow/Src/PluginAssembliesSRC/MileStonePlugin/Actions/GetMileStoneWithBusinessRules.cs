﻿using System;
using System.Activities;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using MileStonePlugin;
using MileStonePlugin.Interfaces;
using MileStonePlugin.Model;
using MileStonePlugin.Service;
using Newtonsoft.Json;

public class GetMileStoneWithBusinessRules : CodeActivity
{
    [Input("EntityLogicalName")]
    public InArgument<string> EntityLogicalName { get; set; }

    [Input("RecordId")]
    public InArgument<string> RecordId { get; set; }

    [Input("UserId")]
    public InArgument<string> UserId { get; set; }

    [Output("Json")]
    public OutArgument<string> Json { get; set; }

    protected override void Execute(CodeActivityContext executionContext)
    {
        var tracer = executionContext.GetExtension<ITracingService>();
        var context = executionContext.GetExtension<IWorkflowContext>();
        var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        var service = serviceFactory.CreateOrganizationService(context.UserId);

        try
        {
            var entityLogicalName = EntityLogicalName.Get<string>(executionContext);
            var recordId = RecordId.Get<string>(executionContext);
            var userId = UserId.Get<string>(executionContext);

            #region Assert tools

            IMetadataService metadataService = new CustomMetadataService(service);
            IMileStoneMapper mapper = new MileStoneMapper();
            IMileStoneService milestoneService = new MileStoneService(service);
            #endregion

            #region Action

            var milestoneManager = new MileStoneManager(metadataService, mapper, milestoneService);
            IEnumerable<Step> milestoneSteps = milestoneManager.GetMilestones(entityLogicalName, recordId, userId, service);

            #endregion

            #region Set Output

            var jsonResult = JsonConvert.SerializeObject(milestoneSteps);
            Json.Set(executionContext, jsonResult);

            #endregion
        }
        catch (Exception e)
        {
            throw new InvalidPluginExecutionException(e.Message);
        }
    }
}