﻿using System;
using System.Activities;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Workflow;
using MileStonePlugin.Model;
using System.Collections.Generic;
using Newtonsoft.Json;
public class GetMilestoneLookupFields : CodeActivity
{
    [Input("EntityLogicalName")]
    public InArgument<string> EntityLogicalName { get; set; }
    [Output("Result")]
    public OutArgument<string> Result { get; set; }
    protected override void Execute(CodeActivityContext executionContext)
    {
        var tracer = executionContext.GetExtension<ITracingService>();
        var context = executionContext.GetExtension<IWorkflowContext>();
        var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        var service = serviceFactory.CreateOrganizationService(context.UserId);
        try
        {
            List<MilestoneLookupJson> milestoneLookup = new List<MilestoneLookupJson>();
            var entityLogicalName = EntityLogicalName.Get<string>(executionContext);
            var entityMetadataRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Attributes,
                LogicalName = entityLogicalName
            };
            var entityMetadataResponse = (RetrieveEntityResponse)service.Execute(entityMetadataRequest);
            var entityAttributes = entityMetadataResponse.EntityMetadata.Attributes.Where(x => x.AttributeType == AttributeTypeCode.Lookup).Select(attr =>
            {
                return new
                {
                    DisplayName = GetLabel(attr.DisplayName),
                    LogicalName = attr.LogicalName,
                    Targets = ((LookupAttributeMetadata)attr).Targets
                };
            })
                .Where(attr => !string.IsNullOrEmpty(attr.DisplayName))
                .OrderBy(attr => attr.DisplayName).ToList();
            foreach (var targetEntity in entityAttributes)
            {
                var targetEntityRequest = new RetrieveEntityRequest
                {
                    EntityFilters = EntityFilters.Attributes,
                    LogicalName = targetEntity.Targets[0]
                };
                var targetEntityResponse = (RetrieveEntityResponse)service.Execute(targetEntityRequest);
                EntityMetadataModel target = new EntityMetadataModel
                {
                    DisplayName = targetEntityResponse.EntityMetadata.DisplayName.UserLocalizedLabel.Label,
                    LogicalName = targetEntityResponse.EntityMetadata.LogicalName
                };
                MilestoneLookupJson milestone = new MilestoneLookupJson() { DisplayName = targetEntity.DisplayName, LogicalName = targetEntity.LogicalName, TargetEntity = target };
                milestoneLookup.Add(milestone);
            }
            var jsonResult = JsonConvert.SerializeObject(milestoneLookup);
            Result.Set(executionContext, jsonResult);
        }
        catch (Exception e)
        {
            throw new InvalidPluginExecutionException(e.Message);
        }
    }
    public string GetLabel(Label label)
    {
        if (label != null && label.UserLocalizedLabel != null)
            return label.UserLocalizedLabel.Label;
        if (label != null && label.LocalizedLabels != null && label.LocalizedLabels.Count != 0)
            return label.LocalizedLabels.First().Label;
        return string.Empty;
    }
}