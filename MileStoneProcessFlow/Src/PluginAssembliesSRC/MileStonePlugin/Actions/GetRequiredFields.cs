﻿using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using MileStonePlugin.Model;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.JSON;
using DDSM.CommonProvider.Utils;
using Microsoft.Xrm.Sdk.Metadata.Query;
using MileStonePlugin.Dto;
using MileStonePlugin.Model.RequredFields;
using Microsoft.Crm.Sdk.Messages;
using System.Xml.Linq;

public class GetRequiredFields : BaseCodeActivity
{
    [Input("MilestoneId")]
    public InArgument<string> MilestoneId { get; set; }

    public override StatusFileDataUploading CurrentOperationStatus { get; set; }

    private BaseDto _baseDto;

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        //AttributeTypeCode

        var milestoneId = Guid.Parse(MilestoneId.Get<string>(executionContext));
        _baseDto = new BaseDto(executionContext);

        IEnumerable<string> entities = GetMilestoneEntities();
        IEnumerable<OneToManyRelationshipMetadata> lookupFields = GetMilestoneParentLookups(entities);
        OneToManyRelationshipMetadata parentLookup = GetMilestoneParentLookup(milestoneId, lookupFields);

        if (parentLookup == null)
        {
            Result.Set(executionContext, "[]");
            return;
        }

        Entity milestone = _objCommon.GetOrgService(systemCall: true).Retrieve("ddsm_milestone", milestoneId, new ColumnSet("ddsm_index", parentLookup.ReferencingAttribute));
        var parentEntity = milestone.GetAttributeValue<EntityReference>(parentLookup.ReferencingAttribute);
        List<Guid> milestonesIds = GetMilestonesBefore(milestone, parentLookup.ReferencingAttribute, parentEntity);

        if (milestonesIds == null || milestonesIds.Count == 0)
        {
            Result.Set(executionContext, "[]");
            return;
        }
        //milestonesIds.Add(milestoneId);

        DataCollection<Entity> milestoneRequirements = GetAllMilestoneRequirements(milestonesIds);
        IEnumerable<MilestoneRequiredFieldModel> requiredFields = GetMilestoneRequirementsConfigs(milestoneRequirements);

        //foreach (var requiredField in requiredFields)
        //{
        //    if (requiredField.RequiredField.ChildTargetField != null && requiredField.RequiredField.ChildTargetField.LogicalName == "ddsm_characteristicdescription")
        //    {
        //        Console.WriteLine();
        //    }
        //}

        //GetDmnResults(requiredFields, parentEntity);
        IEnumerable<EntityFieldModel> milestoneRequiredFields = GetRequiredFieldsByMilestones(requiredFields, parentLookup, parentEntity);

        string json = JsonConvert.SerializeObject(milestoneRequiredFields);
        Result.Set(executionContext, json);
    }

    private IEnumerable<string> GetMilestoneEntities()
    {
        var config = SettingsProvider.GetConfig<ProcessFlowConfig>(_objCommon.GetOrgService(systemCall: true));
        var entities = config.EntitiesForProcessFlow.Select(c => c.LogicalName).ToList();

        return entities;
    }

    private OneToManyRelationshipMetadata GetMilestoneParentLookup(Guid milestoneId, IEnumerable<OneToManyRelationshipMetadata> lookupFields)
    {
        var lookups = lookupFields.Select(l => l.ReferencingAttribute);
        Entity milestone = _objCommon.GetOrgService(systemCall: true).Retrieve("ddsm_milestone", milestoneId, new ColumnSet(lookups.ToArray()));

        if (milestone == null || milestone.Attributes == null || milestone.Attributes.Count < 2)
        {
            return null;
        }

        string parentLookupLogicalName = milestone.Attributes.Where(attr => lookups.Contains(attr.Key)).FirstOrDefault().Key;
        OneToManyRelationshipMetadata parentLookup = lookupFields.SingleOrDefault(l => l.ReferencingAttribute == parentLookupLogicalName);

        return parentLookup;
    }

    private IEnumerable<OneToManyRelationshipMetadata> GetMilestoneParentLookups(IEnumerable<string> entities)
    {
        var request = new RetrieveEntityRequest
        {
            EntityFilters = EntityFilters.Relationships,
            LogicalName = "ddsm_milestone"
        };

        var response = (RetrieveEntityResponse)_objCommon.GetOrgService(systemCall: true).Execute(request);
        OneToManyRelationshipMetadata[] manyToOneRelationships = response.EntityMetadata.ManyToOneRelationships;

        List<OneToManyRelationshipMetadata> lookupFields = manyToOneRelationships
            .Where(r => entities.Contains(r.ReferencedEntity))
            .ToList();

        return lookupFields;
    }

    private List<Guid> GetMilestonesBefore(Entity milestone, string parentLookup, EntityReference parentEntity)
    {
        var order = milestone.GetAttributeValue<int>("ddsm_index");

        int completedStatus = 962080002;

        var query = new QueryExpression
        {
            EntityName = "ddsm_milestone",
            Criteria = new FilterExpression
            {
                FilterOperator = LogicalOperator.And,
                Conditions =
                {
                    new ConditionExpression("ddsm_index", ConditionOperator.LessThan, order),
                    new ConditionExpression("ddsm_status", ConditionOperator.NotEqual, completedStatus),
                    new ConditionExpression(parentLookup, ConditionOperator.Equal, parentEntity.Id)
                }
            }
        };
        DataCollection<Entity> entities = _objCommon.GetOrgService(systemCall: true).RetrieveMultiple(query).Entities;
        List<Guid> milestonesIds = entities.Select(m => m.Id).ToList();

        return milestonesIds;
    }
    
    private DataCollection<Entity> GetAllMilestoneRequirements(IEnumerable<Guid> milestonesIds)
    {
        var query = new QueryExpression
        {
            EntityName = "ddsm_milestonerequirements",
            ColumnSet = new ColumnSet("ddsm_name", "ddsm_config", "ddsm_businessrule", "ddsm_required", "ddsm_parentmilestone"),
            Criteria = new FilterExpression
            {
                Conditions =
                {
                    new ConditionExpression("ddsm_parentmilestone", ConditionOperator.In, milestonesIds.ToArray())
                }
            }
        };
        DataCollection<Entity> milestoneRequirements = _objCommon.GetOrgService(systemCall: true).RetrieveMultiple(query).Entities;

        return milestoneRequirements;
    }

    private IEnumerable<MilestoneRequiredFieldModel> GetMilestoneRequirementsConfigs(DataCollection<Entity> milestoneRequirements)
    {
        var requiredFieldsWithoutDuplicates = new List<Entity>();
        foreach (Entity requiredField in milestoneRequirements)
        {
            var json = requiredField.GetAttributeValue<string>("ddsm_config");

            if (string.IsNullOrEmpty(json))
            {
                continue;
            }

            Entity duplicateRequiredField = requiredFieldsWithoutDuplicates.Find(r => r.GetAttributeValue<string>("ddsm_config") == json);
            if (duplicateRequiredField != null)
            {
                continue;
            }

            requiredFieldsWithoutDuplicates.Add(requiredField);
        }

        var requiredFields = new List<MilestoneRequiredFieldModel>();
        foreach (Entity entity in requiredFieldsWithoutDuplicates)
        {
            var isRequired = entity.GetAttributeValue<bool>("ddsm_required");
            if (!isRequired)
            {
                continue;
            }

            var json = entity.GetAttributeValue<string>("ddsm_config");
            var milestone = entity.GetAttributeValue<EntityReference>("ddsm_parentmilestone");

            RequiredFieldModel requiredField = null;
            try
            {
                requiredField = JsonConvert.DeserializeObject<RequiredFieldModel>(json);
            }
            catch
            {
                var requiredFieldName = entity.GetAttributeValue<string>("ddsm_name");
                TracingService.Trace("Milestone: " + milestone.Name + ", Required Field: " + requiredFieldName + ". Config is wrong.");
                continue;
            }

            requiredField.BusinessRuleId = entity.GetAttributeValue<EntityReference>("ddsm_businessrule")?.Id;

            var milestoneRequiredField = new MilestoneRequiredFieldModel
            {
                MilestoneId = milestone.Id,
                RequiredField = requiredField
            };

            requiredFields.Add(milestoneRequiredField);
        }

        return requiredFields;
    }

    private IEnumerable<EntityFieldModel> GetRequiredFieldsByMilestones(IEnumerable<MilestoneRequiredFieldModel> requiredFields, OneToManyRelationshipMetadata milestoneLookupAttr, EntityReference lookupValueToRelatedEntity)
    {
        IEnumerable<CheckedResultWithResponseModel> dataForCheckWithRecords = GetDataForCheckWithRecords(requiredFields, milestoneLookupAttr, lookupValueToRelatedEntity);

        IEnumerable<DmnResultModel> dmnResults = GetDmnResults(requiredFields, lookupValueToRelatedEntity);

        var entityFields = new List<EntityFieldModel>();
        foreach (CheckedResultWithResponseModel dataForCheckWithRecord in dataForCheckWithRecords)
        {
            RecordCheckedResultModel checkedResult = CheckEntityField(dataForCheckWithRecord, lookupValueToRelatedEntity, dmnResults);
            if (!checkedResult.CheckedResult)
            {
                continue;
            }

            dataForCheckWithRecord.CheckedResultModel.Attribute.DisplayName = GetAttributeDisplayName(dataForCheckWithRecord.RequiredFieldWithMilestoneId, dataForCheckWithRecord.CheckedResultModel);

            entityFields.Add(new EntityFieldModel
            {
                Entity = dataForCheckWithRecord.CheckedResultModel.Entity,
                Attribute = dataForCheckWithRecord.CheckedResultModel.Attribute,
                RecordId = checkedResult.RecordId
            });
        }

        return entityFields;
    }

    private IEnumerable<CheckedResultWithResponseModel> GetDataForCheckWithRecords(IEnumerable<MilestoneRequiredFieldModel> requiredFields, OneToManyRelationshipMetadata milestoneLookupAttr, EntityReference lookupValueToRelatedEntity)
    {
        IEnumerable<RetrieveEntityResponse> entitiesMetadata = GetEntitiesMetadata(requiredFields);

        var requestWithResults = new ExecuteMultipleRequest()
        {
            // Assign settings that define execution behavior: continue on error, return responses. 
            Settings = new ExecuteMultipleSettings()
            {
                ContinueOnError = false,
                ReturnResponses = true
            },
            // Create an empty organization request collection.
            Requests = new OrganizationRequestCollection()
        };

        var dataForCheck = new List<CheckedResultModel>();
        foreach (MilestoneRequiredFieldModel requiredField in requiredFields)
        {
            CheckedResultModel checkedResultModel = GetDataForCheck(requiredField, milestoneLookupAttr, lookupValueToRelatedEntity, entitiesMetadata);

            if (checkedResultModel == null)
            {
                continue;
            }

            dataForCheck.Add(checkedResultModel);

            ExecuteFetchRequest checkEntityFieldRequest = GetCheckEntityFieldRequest(checkedResultModel, requiredField.RequiredField.BusinessRuleId, lookupValueToRelatedEntity);
            requestWithResults.Requests.Add(checkEntityFieldRequest);
        }

        var responseWithResults =
            (ExecuteMultipleResponse)_objCommon.GetOrgService(systemCall: true).Execute(requestWithResults);

        var dataForCheckWithResponses = new List<CheckedResultWithResponseModel>();
        foreach (ExecuteMultipleResponseItem response in responseWithResults.Responses)
        {
            CheckedResultModel checkedResultModel = dataForCheck[response.RequestIndex];

            if (response.Response == null)
            {
                continue;
            }

            var fetchXmlResult = ((ExecuteFetchResponse)response.Response).FetchXmlResult;
            MilestoneRequiredFieldModel requiredField = requiredFields.ElementAt(response.RequestIndex);

            dataForCheckWithResponses.Add(new CheckedResultWithResponseModel
            {
                CheckedResultModel = checkedResultModel,
                FetchXmlResult = fetchXmlResult,
                RequiredFieldWithMilestoneId = requiredField
            });
        }

        return dataForCheckWithResponses;
    }

    private CheckedResultModel GetDataForCheck(MilestoneRequiredFieldModel requiredField, OneToManyRelationshipMetadata milestoneLookupAttr, EntityReference lookupValueToRelatedEntity, IEnumerable<RetrieveEntityResponse> entitiesMetadata)
    {
        if (requiredField.RequiredField.TargetField != null)
        {
            var conditions = new List<string>
            {
                "<condition attribute='" + milestoneLookupAttr.ReferencedAttribute + "' operator='eq' value='" + lookupValueToRelatedEntity.Id + "' />"
                //new ConditionExpression(milestoneLookupAttr.ReferencedAttribute, ConditionOperator.Equal, lookupValueToRelatedEntity.Id)
            };

            return new CheckedResultModel
            {
                Entity = requiredField.RequiredField.MilestoneLookup.TargetEntity,
                Attribute = requiredField.RequiredField.TargetField,
                Conditions = conditions,
                AttributeIdLogicalName = milestoneLookupAttr.ReferencedAttribute
            };
        }
        else if (requiredField.RequiredField.ChildTargetEntity != null && requiredField.RequiredField.ChildTargetField != null && requiredField.RequiredField.Indicator != null)
        {
            OneToManyRelationshipMetadata relatedLookupAttr = GetLookupToRelatedEntity(requiredField.RequiredField.ChildTargetEntity.LogicalName,
                requiredField.RequiredField.MilestoneLookup.TargetEntity.LogicalName, isNotRegarding: false, entitiesMetadata: entitiesMetadata);
            OneToManyRelationshipMetadata indicatorLookupAttr = GetLookupToRelatedEntity(requiredField.RequiredField.ChildTargetEntity.LogicalName,
                "ddsm_indicator", isNotRegarding: true, entitiesMetadata: entitiesMetadata);

            if (relatedLookupAttr == null || indicatorLookupAttr == null)
            {
                return null;
            }

            var conditions = new List<string>
            {
                "<condition attribute='" + relatedLookupAttr.ReferencingAttribute + "' operator='eq' value='" + lookupValueToRelatedEntity.Id + "' />",
                "<condition attribute='" + indicatorLookupAttr.ReferencingAttribute + "' operator='eq' value='"
                    + requiredField.RequiredField.Indicator.Id + "' />"
                //new ConditionExpression(relatedLookupAttr.ReferencingAttribute, ConditionOperator.Equal, lookupValueToRelatedEntity.Id),
                //new ConditionExpression(indicatorLookupAttr.ReferencingAttribute, ConditionOperator.Equal, requiredField.RequiredField.Indicator.Id)
            };

            string relatedEntityAttributeId = GetAttributeIdLogicalName(requiredField.RequiredField.ChildTargetEntity.LogicalName, entitiesMetadata);

            return new CheckedResultModel
            {
                Entity = requiredField.RequiredField.ChildTargetEntity,
                Attribute = requiredField.RequiredField.ChildTargetField,
                Conditions = conditions,
                AttributeIdLogicalName = relatedEntityAttributeId
            };
        }

        return null;
    }

    private string GetAttributeDisplayName(MilestoneRequiredFieldModel requiredField, CheckedResultModel checkedResultModel)
    {
        if (requiredField.RequiredField.ChildTargetEntity != null && requiredField.RequiredField.ChildTargetEntity.LogicalName == "ddsm_indicatorvalue" && requiredField.RequiredField.Indicator != null)
        {
            return requiredField.RequiredField.Indicator.Name;
        }
        else if (requiredField.RequiredField.ChildTargetEntity != null && requiredField.RequiredField.ChildTargetEntity.LogicalName != "ddsm_indicatorvalue" && requiredField.RequiredField.Indicator != null)
        {
            return requiredField.RequiredField.Indicator.Name + " " + checkedResultModel.Attribute.DisplayName;
        }

        return checkedResultModel.Attribute.DisplayName;
    }
    
    private OneToManyRelationshipMetadata GetLookupToRelatedEntity(string primaryEntity, string relatedEntityLogicalName, bool isNotRegarding, IEnumerable<RetrieveEntityResponse> entitiesMetadata)
    {
        RetrieveEntityResponse retrieveBankEntityResponse = entitiesMetadata
            .SingleOrDefault(m => m.EntityMetadata.ManyToOneRelationships.FirstOrDefault().ReferencingEntity == primaryEntity);

        OneToManyRelationshipMetadata[] manyToOneRelationships =
            retrieveBankEntityResponse.EntityMetadata.ManyToOneRelationships;

        OneToManyRelationshipMetadata relation = null;
        if (isNotRegarding)
        {
            relation = manyToOneRelationships.FirstOrDefault(rel => rel.ReferencedEntity == relatedEntityLogicalName
                && rel.ReferencingAttribute != "regardingobjectid");
        }
        else
        {
            relation = manyToOneRelationships.FirstOrDefault(rel => rel.ReferencedEntity == relatedEntityLogicalName);
        }

        if (relation == null)
        {
            return null;
        }

        return relation;
    }

    private string GetAttributeIdLogicalName(string primaryEntity, IEnumerable<RetrieveEntityResponse> entitiesMetadata)
    {
        RetrieveEntityResponse retrieveBankEntityResponse = entitiesMetadata
            .SingleOrDefault(m => m.EntityMetadata.ManyToOneRelationships.FirstOrDefault().ReferencingEntity == primaryEntity);

        return retrieveBankEntityResponse.EntityMetadata.PrimaryIdAttribute;
    }

    private IEnumerable<RetrieveEntityResponse> GetEntitiesMetadata(IEnumerable<MilestoneRequiredFieldModel> requiredFields)
    {
        var requestWithResults = new ExecuteMultipleRequest()
        {
            // Assign settings that define execution behavior: continue on error, return responses. 
            Settings = new ExecuteMultipleSettings()
            {
                ContinueOnError = false,
                ReturnResponses = true
            },
            // Create an empty organization request collection.
            Requests = new OrganizationRequestCollection()
        };

        var entitiesLogicalName = requiredFields.Where(r => r.RequiredField.ChildTargetEntity != null)
            .Select(r => r.RequiredField.ChildTargetEntity.LogicalName).Distinct().ToList();

        foreach (string LogicalName in entitiesLogicalName)
        {
            requestWithResults.Requests.Add(new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = LogicalName
            });
        }

        // Execute all the requests in the request collection using a single web method call.
        var responseWithResults =
            (ExecuteMultipleResponse)_objCommon.GetOrgService(systemCall: true).Execute(requestWithResults);

        var responses = responseWithResults.Responses.Select(r => r.Response as RetrieveEntityResponse).Where(r => r != null).ToList();

        return responses;
    }

    private ExecuteFetchRequest GetCheckEntityFieldRequest(CheckedResultModel model, Guid? businessRuleId, EntityReference lookupValueToRelatedEntity)
    {
        //var query = new QueryExpression
        //{
        //    EntityName = model.Entity.LogicalName,
        //    ColumnSet = new ColumnSet(model.Attribute.LogicalName)
        //};
        //query.Criteria = new FilterExpression();
        //query.Criteria.Conditions.AddRange(model.Conditions);

        var request = new ExecuteFetchRequest
        {
            FetchXml =
                "<fetch>"
                    + "<entity name='" + model.Entity.LogicalName + "'>"
                        + "<attribute name='" + model.Attribute.LogicalName + "'/>"
                        + "<filter type='and'>"
                            + model.Conditions.Aggregate((result, item) => { return result + item; })
                        + "</filter>"
                    + "</entity>"
                + "</fetch>"
        };

        return request;
    }

    private RecordCheckedResultModel CheckEntityField(CheckedResultWithResponseModel model, EntityReference lookupValueToRelatedEntity, IEnumerable<DmnResultModel> dmnResults)
    {
        //var query = new QueryExpression
        //{
        //    EntityName = model.Entity.LogicalName,
        //    ColumnSet = new ColumnSet(model.Attribute.LogicalName)
        //};
        //query.Criteria = new FilterExpression();
        ////query.Criteria.Conditions.AddRange(model.Conditions);

        //Entity record = _objCommon.GetOrgService(systemCall: true).RetrieveMultiple(query).Entities.FirstOrDefault();

        XElement response = XElement.Parse(model.FetchXmlResult);
        XElement record = response.Element("result");

        if (record == null)
        {
            return new RecordCheckedResultModel
            {
                CheckedResult = false
            };
        }

        if (model.RequiredFieldWithMilestoneId.RequiredField.BusinessRuleId.HasValue)
        {
            bool dmnCheckedResult = CheckByBusinessRule(model.RequiredFieldWithMilestoneId.RequiredField.BusinessRuleId.Value, lookupValueToRelatedEntity,
                dmnResults);
            if (!dmnCheckedResult)
            {
                return new RecordCheckedResultModel
                {
                    CheckedResult = false
                };
            }
        }

        XElement attr = record.Element(model.CheckedResultModel.Attribute.LogicalName);
        if (attr != null)
        {
            return new RecordCheckedResultModel
            {
                CheckedResult = false
            };
        }

        //object obj = null;
        //bool result = !record.Attributes.TryGetValue(model.CheckedResultModel.Attribute.LogicalName, out obj);
        string strRecordId = record.Element(model.CheckedResultModel.AttributeIdLogicalName).Value.Replace("{", "").Replace("}", "");
        Guid recordId = Guid.Parse(strRecordId);

        var checkedResultModel = new RecordCheckedResultModel
        {
            CheckedResult = true,
            RecordId = recordId
        };

        return checkedResultModel;
    }

    private bool CheckByBusinessRule(Guid businessRuleId, EntityReference entity, IEnumerable<DmnResultModel> dmnResults)
    {
        DmnResultModel dmnResult = dmnResults.SingleOrDefault(dr => dr.Success && dr.EntityLogicalName == entity.LogicalName
            && dr.RecordId == entity.Id.ToString() && dr.BusinessRuleId.Contains(businessRuleId.ToString()) && dr.DecisionResult == "true");

        if (dmnResult == null)
        {
            return false;
        }

        return true;
    }

    private List<DmnResultModel> GetDmnResults(IEnumerable<MilestoneRequiredFieldModel> requiredFields, EntityReference entity)
    {
        var listOfCallDmnDto = requiredFields.Where(r => r.RequiredField.BusinessRuleId != null).Select(r =>
        {
            var callDmnDto = new CallDmnDto
            {
                UserId = _baseDto.WorkflowContext.UserId,
                TargetEntityLogicalName = entity.LogicalName,
                TargetRecordId = entity.Id
            };
            callDmnDto.BusinessRules.Add(new CallDmnDto.BusinessRule { Id = r.RequiredField.BusinessRuleId.Value });

            return callDmnDto;
        }).ToList();

        var data = JsonConvert.SerializeObject(listOfCallDmnDto);
        // Calling the Action - ddsm_CallDmn
        var req = new OrganizationRequest("ddsm_DDSMCallDmn")
        {
            ["Data"] = data,
            ["Target"] = entity
        };
        var response = _baseDto.OrgService.Execute(req);

        string jsonResult = (string)response.Results.FirstOrDefault().Value;
        var dmnResultModel = new List<DmnResultModel>();
        try
        {
            dmnResultModel = JsonConvert.DeserializeObject<List<DmnResultModel>>(jsonResult);
        }
        catch
        {
            var traceModel = new TraceDMNModel
            {
                DmnRequest = req,
                DmnResponse = response
            };
            var jsonTraceModel = JsonConvert.SerializeObject(traceModel);
            TracingService.Trace(jsonTraceModel);
        }

        return dmnResultModel;
    }
}
