﻿using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using DDSM.CommonProvider.Model;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Query;
using MileStonePlugin;

class MilestoneData
{
    public Guid Id { get; set; }
    public int Index { get; set; }
    public Guid RoleId { get; set; }
    public MilestoneStatus Status { get; set; }
}

public class GetRequiredFieldsWithConvertedData : BaseCodeActivity
{
    [Input("MilestoneId")]
    public InArgument<string> MilestoneId { get; set; }

    [Input("UserId")]
    public InArgument<string> UserId { get; set; }

    public override StatusFileDataUploading CurrentOperationStatus { get; set; }

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        var milestoneId = MilestoneId.Get<string>(executionContext);
        var userId = UserId.Get<string>(executionContext);

        if (!CheckUserPermission(milestoneId, userId, executionContext))
        {
            string requestDenied = "{\"NotAllowed\": true}";
            Result.Set(executionContext, requestDenied);
            return;
        }
            
        var requiredFieldsRequest = new OrganizationRequest("ddsm_DDSMGetRequiredFields")
        {
            ["MilestoneId"] = milestoneId
        };
        var requiredFieldsResponse = _objCommon.GetOrgService(systemCall: true).Execute(requiredFieldsRequest);

        if (ResponseIsSuccessful(requiredFieldsResponse))
        {
            Result.Set(executionContext, "[]");
            return;
        }

        string requiredFieldsJson = requiredFieldsResponse.Results.Values.FirstOrDefault().ToString();
        var convertDataRequest = new OrganizationRequest("ddsm_DDSMConvertRequiredFieldsToUIBuilderControls")
        {
            ["RequiredFieldsJson"] = requiredFieldsJson
        };
        var convertDataRequestResponse = _objCommon.GetOrgService(systemCall: true).Execute(convertDataRequest);

        if (ResponseIsSuccessful(convertDataRequestResponse))
        {
            Result.Set(executionContext, "[]");
            return;
        }

        string convertedDataJson = convertDataRequestResponse.Results.Values.FirstOrDefault().ToString();

        Result.Set(executionContext, convertedDataJson);
    }

    private bool ResponseIsSuccessful(OrganizationResponse response)
    {
        bool result = response == null || response.Results == null || response.Results.Count == 0;

        return result;
    }

    private bool CheckUserPermission(string milestoneId, string userId, CodeActivityContext executionContext)
    {
        var context = executionContext.GetExtension<IWorkflowContext>();
        var serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
        var service = serviceFactory.CreateOrganizationService(context.UserId);

        var userGuid = Guid.Parse(userId);
        var milestoneGuid = Guid.Parse(milestoneId);

        string rolesFetch = @"<fetch>
              <entity name='systemuserroles' >
                <attribute name='roleid' />
                <attribute name='systemuserroleid' />
                <attribute name='systemuserid' />
                <filter>
                  <condition attribute='systemuserid' operator='eq' value='" + userGuid + @"' />
                </filter>
              </entity>
            </fetch>";

        List<Guid> userRoleids = new List<Guid>();
        EntityCollection roles = service.RetrieveMultiple(new FetchExpression(rolesFetch));
        foreach (var role in roles.Entities)
        {
            var roleId = role.GetAttributeValue<Guid>("roleid");
            userRoleids.Add(roleId);
        }

        string milestonesFetch = @"<fetch>
          <entity name='ddsm_project' >
            <link-entity name='ddsm_milestone' from='ddsm_projectid' to='ddsm_projectid' >
              <filter>
                <condition attribute='ddsm_milestoneid' operator='eq' value='" + milestoneGuid + @"' />
              </filter>
            </link-entity>
            <link-entity name='ddsm_milestone' from='ddsm_projectid' to='ddsm_projectid' >
              <attribute name='ddsm_index' alias='ddsm_index' />
              <attribute name='ddsm_permitrole' alias='ddsm_permitrole' />
              <attribute name='ddsm_status' alias='ddsm_status' />
              <attribute name='ddsm_milestoneid' alias='ddsm_milestoneid' />
            </link-entity>
          </entity>
        </fetch>";

        EntityCollection milestones = service.RetrieveMultiple(new FetchExpression(milestonesFetch));

        var mappedMilestones = MapMilestones(milestones.Entities);
        var selectedMilstone = mappedMilestones.FirstOrDefault(m => m.Id == milestoneGuid);
        if (selectedMilstone != null)
        {
            var prevMilestone = mappedMilestones.FirstOrDefault(m => (m.Index == (selectedMilstone.Index - 1)) && m.Status != MilestoneStatus.Completed && m.Status != MilestoneStatus.Skipped);
            if (prevMilestone != null && !userRoleids.Contains(prevMilestone.RoleId) && prevMilestone.RoleId != Guid.Empty)
            {
                return false; // User doesn't have required role to complete previous Milestone 
            }
        }

        return true;
    }

    private IEnumerable<MilestoneData> MapMilestones(DataCollection<Entity> milestones)
    {
        List<MilestoneData> mappedMilestones = new List<MilestoneData>();

        foreach (var milestone in milestones)
        {
            Guid id = Guid.Empty;
            Guid roleId = Guid.Empty;
            int index = -1;
            MilestoneStatus status = MilestoneStatus.Empty;

            if (milestone.Contains("ddsm_milestoneid"))
                id = milestone.GetAttributeValue<Guid>("ddsm_milestoneid");

            if (milestone.Contains("ddsm_permitrole"))
                roleId = milestone.GetAttributeValue<EntityReference>("ddsm_permitrole").Id;

            if (milestone.Contains("ddsm_status"))
                status = (MilestoneStatus)milestone.GetAttributeValue<OptionSetValue>("ddsm_status").Value;

            if (milestone.Contains("ddsm_index"))
                index = milestone.GetAttributeValue<int>("ddsm_index");

            mappedMilestones.Add(new MilestoneData {
                Id = id,
                RoleId = roleId,
                Index = index,
                Status = status
            });
        }

        return mappedMilestones;
    }
}
