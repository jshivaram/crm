﻿using DDSM.CommonProvider.src;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using DDSM.CommonProvider.Model;
using DDSM.CommonProvider.JSON;
using System.Collections.Generic;
using MileStonePlugin.Model;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Metadata;

public class SaveFormData : BaseCodeActivity
{
    [Input("FormDataJson")]
    public InArgument<string> FormDataJson { get; set; }

    public override StatusFileDataUploading CurrentOperationStatus { get; set; }

    protected override void ExecuteActivity(CodeActivityContext executionContext)
    {
        var formDataJson = FormDataJson.Get<string>(executionContext);
        var formData = JsonConvert.DeserializeObject<IEnumerable<FormDataModel>>(formDataJson);

        //RemoveSavedData(formData);

        foreach (FormDataModel item in formData)
        {
            if (item.AdditionalData.AttributeType == AttributeTypeCode.String.ToString() || item.AdditionalData.AttributeType == AttributeTypeCode.Memo.ToString())
            {
                SaveData(item.AdditionalData, item.Value);
            }
            else if (item.AdditionalData.AttributeType == AttributeTypeCode.Picklist.ToString())
            {
                int integer;
                if (!int.TryParse(item.Value, out integer))
                {
                    continue;
                }
                var optionSetValue = new OptionSetValue
                {
                    Value = integer
                };
                SaveData(item.AdditionalData, optionSetValue);
            }
            else if (item.AdditionalData.AttributeType == AttributeTypeCode.Decimal.ToString())
            {
                decimal dec;
                if (!decimal.TryParse(item.Value, out dec))
                {
                    continue;
                }
                SaveData(item.AdditionalData, dec);
            }
            else if (item.AdditionalData.AttributeType == AttributeTypeCode.Integer.ToString())
            {
                int integer;
                if (!int.TryParse(item.Value, out integer))
                {
                    continue;
                }
                SaveData(item.AdditionalData, integer);
            }
            else if (item.AdditionalData.AttributeType == AttributeTypeCode.Money.ToString())
            {
                decimal dec;
                if (!decimal.TryParse(item.Value, out dec))
                {
                    continue;
                }
                var money = new Money(dec);
                SaveData(item.AdditionalData, money);
            }
            else if (item.AdditionalData.AttributeType == AttributeTypeCode.DateTime.ToString())
            {
                DateTime dateTime;
                if (!DateTime.TryParse(item.Value, out dateTime))
                {
                    continue;
                }
                SaveData(item.AdditionalData, dateTime);
            }
            else if (item.AdditionalData.AttributeType == AttributeTypeCode.Lookup.ToString())
            {
                Guid guid;
                if (!Guid.TryParse(item.Value, out guid))
                {
                    continue;
                }
                var entityReference = new EntityReference
                {
                    Id = guid
                };
                SaveData(item.AdditionalData, entityReference);
            }
        }
    }

    private void SaveData(AdditionalDataModel model, object obj)
    {
        Entity entity = _objCommon.GetOrgService(systemCall: true).Retrieve(model.EntityLogicalName, model.RecordId, new ColumnSet());

        entity.Attributes.Add(model.AttributeLogicalName, obj);

        try
        {
            _objCommon.GetOrgService(systemCall: true).Update(entity);
        }
        catch { }
    }

    private void RemoveSavedData(IEnumerable<FormDataModel> models)
    {
        foreach (FormDataModel model in models)
        {
            Entity entity = _objCommon.GetOrgService(systemCall: true).Retrieve(model.AdditionalData.EntityLogicalName, model.AdditionalData.RecordId, new ColumnSet(model.AdditionalData.AttributeLogicalName));

            entity[model.AdditionalData.AttributeLogicalName] = null;

            _objCommon.GetOrgService(systemCall: true).Update(entity);
        }
    }
}
