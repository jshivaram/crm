﻿using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using MileStonePlugin.Model;

namespace MileStonePlugin.Interfaces
{
    public interface IMapperHistory
    {
        IReadOnlyList<string> GetNewestMileStoneIdList(IReadOnlyList<Entity> entities, string lookupFieldName);

        IReadOnlyList<StepHistory> GetNewestBusinessRulesWithMileStones(IReadOnlyList<Entity> entities,
            string lookupFieldName, string businessRuleAlias);

        IReadOnlyList<StepHistory> GetStatusesFromEntities(IReadOnlyList<Entity> entities, string statusFieldName);

        IReadOnlyList<StepHistory> UpdateStepStatusesFromAnotherStepArray(IReadOnlyList<StepHistory> sourceStepArray,
            IReadOnlyList<StepHistory> targetStep);
    }
}