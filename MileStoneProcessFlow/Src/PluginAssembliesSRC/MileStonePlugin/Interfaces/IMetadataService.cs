﻿namespace MileStonePlugin.Interfaces
{
    public interface IMetadataService
    {
        string GetLookupFieldName(string referencingEntity, string referencedEntity);
    }
}