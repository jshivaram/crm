﻿using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using MileStonePlugin.Model;
using System;

namespace MileStonePlugin.Interfaces
{
    public interface IMileStoneMapper
    {
        IReadOnlyList<Step> GetMappedSteps(IReadOnlyList<Entity> entities, string businessRuleAlias, string requiredFieldAlias, IOrganizationService service, Guid recordGuid, Guid userGuid, string logicalName);
    }
}