﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace MileStonePlugin.Interfaces
{
    public interface IMileStoneService
    {
        IEnumerable<Entity> GetMileStoneWithRelatedBusinessRules(
            string entityLogicalName,
            string lookupFieldName,
            Guid lookupValueId,
            string businessRuleAlias,
            string requiredFieldAlias
        );
    }
}