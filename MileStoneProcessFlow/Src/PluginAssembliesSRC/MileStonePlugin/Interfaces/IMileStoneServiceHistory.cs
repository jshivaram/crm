﻿using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace MileStonePlugin.Interfaces
{
    public interface IMileStoneServiceHistory
    {
        IReadOnlyList<Entity> GetMileStoneHistoryStatuses(string entityLogicalName, string lookupAttributeName,
            IReadOnlyList<string> idArray);

        IReadOnlyList<Entity> GetMileStoneStatusesWithBusinessRules(string entityLogicalName, string attributeToCheck,
            string lookupAttributeName, string businessRuleAlias, IReadOnlyList<string> idArray);

        IReadOnlyList<Entity> GetMileStoneStatuses(string entityLogicalName, string primaryKeyField, string statusField,
            IReadOnlyList<string> idArray);
    }
}