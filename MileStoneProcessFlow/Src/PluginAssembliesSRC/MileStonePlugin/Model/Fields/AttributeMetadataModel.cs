﻿using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model
{
    public class AttributeMetadataModel
    {
        public string DisplayName { get; set; }
        public string LogicalName { get; set; }
        public string AttributeType { get; set; }
        public IEnumerable<Option> Options { get; set; }
        public string Target { get; set; }
    }
}
