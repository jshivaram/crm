﻿namespace MileStonePlugin.Model
{
    public class Option
    {
        public string Label { get; set; }
        public int? Value { get; set; }
    }
}