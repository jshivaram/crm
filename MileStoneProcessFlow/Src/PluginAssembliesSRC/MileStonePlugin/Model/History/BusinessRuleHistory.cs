﻿using System;

namespace MileStonePlugin.Model
{
    public class BusinessRuleHistory
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public int? Status { get; set; }
        public DateTime? Modified { get; set; }
    }
}