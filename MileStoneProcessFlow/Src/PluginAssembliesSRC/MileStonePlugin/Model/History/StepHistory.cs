﻿using System;
using System.Collections.Generic;

namespace MileStonePlugin.Model
{
    public class StepHistory
    {
        public Guid Id { get; set; }
        public int Status { get; set; }
        public string DisplayName { get; set; }
        public IEnumerable<BusinessRuleHistory> BusinessRules { get; set; }
    }
}