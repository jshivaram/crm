﻿
namespace MileStonePlugin.Model
{
    public class MilestoneLookupJson
    {
        public string DisplayName { get; set; }
        public string LogicalName { get; set; }
        public EntityMetadataModel TargetEntity { get; set; }
    }
}
