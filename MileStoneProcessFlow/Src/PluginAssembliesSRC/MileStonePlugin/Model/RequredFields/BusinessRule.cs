﻿using System;

namespace MileStonePlugin.Model
{
    public class BusinessRule
    {
        public Guid? Id { get; set; }
        public string DisplayName { get; set; }
    }
}