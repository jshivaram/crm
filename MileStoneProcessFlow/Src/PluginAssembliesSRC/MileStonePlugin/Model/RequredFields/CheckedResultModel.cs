﻿using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model
{
    public class CheckedResultModel
    {
        public EntityMetadataModel Entity { get; set; }
        public AttributeMetadataModel Attribute { get; set; }
        public IEnumerable<string> Conditions { get; set; }
        public string AttributeIdLogicalName { get; set; }
    }
}
