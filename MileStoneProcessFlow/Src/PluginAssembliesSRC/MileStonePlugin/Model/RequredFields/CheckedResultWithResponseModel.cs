﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model.RequredFields
{
    public class CheckedResultWithResponseModel
    {
        public CheckedResultModel CheckedResultModel { get; set; }
        public string FetchXmlResult { get; set; }
        public MilestoneRequiredFieldModel RequiredFieldWithMilestoneId { get; set; }
    }
}
