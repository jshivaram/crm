﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model
{
    public class DmnResultModel
    {
        public bool Success { get; set; }
        public string EntityLogicalName { get; set; }
        public string RecordId { get; set; }
        public List<string> BusinessRuleId { get; set; }
        public string DecisionResult { get; set; }
    }
}
