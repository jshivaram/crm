﻿using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model
{
    public class EntityFieldModel
    {
        public EntityMetadataModel Entity { get; set; }
        public AttributeMetadataModel Attribute { get; set; }
        public Guid RecordId { get; set; }
    }
}
