﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model
{
    public class EntityMetadataModel
    {
        public string DisplayName { get; set; }
        public string LogicalName { get; set; }
    }
}
