﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model.RequredFields
{
    public class MilestoneRequiredFieldModel
    {
        public Guid MilestoneId { get; set; }
        public RequiredFieldModel RequiredField { get; set; }
    }
}
