﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model
{
    public class ProcessFlowConfig
    {
        public class EntityConfigModel
        {
            public string Label { get; set; }
            public string LogicalName { get; set; }
        }

        public IEnumerable<EntityConfigModel> EntitiesForProcessFlow { get; set; }
    }
}
