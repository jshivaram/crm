﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model
{
    public class RecordCheckedResultModel
    {
        public bool CheckedResult { get; set; }
        public Guid RecordId { get; set; }
    }
}
