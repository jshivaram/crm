﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using DDSM.CommonProvider.Model;
using Microsoft.Xrm.Sdk.Metadata;

namespace MileStonePlugin.Model
{
    public class RequiredFieldModel
    {
        public class LookupModel
        {
            public string DisplayName { get; set; }
            public string LogicalName { get; set; }
            public EntityMetadataModel TargetEntity { get; set; }
        }

        public class RecordModel
        {
            //[JsonConverter(typeof(FakeGuidValueProvider))]
            public Guid Id { get; set; }
            public string Name { get; set; }
        }

        public LookupModel MilestoneLookup { get; set; }
        public AttributeMetadataModel TargetField { get; set; }

        public EntityMetadataModel ChildTargetEntity { get; set; }
        public AttributeMetadataModel ChildTargetField { get; set; }
        public RecordModel Indicator { get; set; }

        public Guid? BusinessRuleId { get; set; }

        public bool? Required { get; set; }
        public string DisplayName { get; set; }
        public Guid? Id { get; set; }
        public string Value { get; set; }
    }
}
