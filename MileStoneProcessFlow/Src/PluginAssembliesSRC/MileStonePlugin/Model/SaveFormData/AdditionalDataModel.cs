﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model
{
    public class AdditionalDataModel
    {
        public string EntityLogicalName { get; set; }
        public string AttributeLogicalName { get; set; }
        public string AttributeType { get; set; }
        public Guid RecordId { get; set; }
    }
}
