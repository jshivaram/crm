﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model
{
    public class FormDataModel
    {
        public AdditionalDataModel AdditionalData { get; set; }
        public string Value { get; set; }
    }
}
