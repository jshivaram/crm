﻿using System;
using System.Collections.Generic;

namespace MileStonePlugin.Model
{
    public class Step
    {
        public Guid Id { get; set; }
        public string DisplayName { get; set; }
        public int Index { get; set; }
        public bool IsAllowed { get; set; }
        public IEnumerable<BusinessRule> BusinessRules { get; set; }
        public IEnumerable<RequiredFieldModel> RequiredFields { get; set; }
    }
}