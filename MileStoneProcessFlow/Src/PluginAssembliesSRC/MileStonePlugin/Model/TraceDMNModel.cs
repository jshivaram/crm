﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model
{
    public class TraceDMNModel
    {
        public OrganizationRequest DmnRequest { get; set; }
        public OrganizationResponse DmnResponse { get; set; }
    }
}
