﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model.UIBuilderConvertor
{
    public class BaseControlModel
    {
        public string Label { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string DefaultValue { get; set; }
        public bool Required { get; set; }
        public byte? Order { get; set; }
        public string AdditionalData { get; set; }
    }
}
