﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model.UIBuilderConvertor
{
    public class ComboBoxControlModel : BaseControlModel
    {
        public IEnumerable<Option> LocalData { get; set; }
        public FieldsConfigModel FieldsConfig { get; set; }
    }
}
