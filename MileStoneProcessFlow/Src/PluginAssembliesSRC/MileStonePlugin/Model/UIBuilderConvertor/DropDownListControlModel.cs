﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model.UIBuilderConvertor
{
    public class DropDownListControlModel : BaseControlModel
    {
        public FieldsConfigModel FieldsConfig { get; set; }
        public RemoteDataModel RemoteData { get; set; }
    }
}
