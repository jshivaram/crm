﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model.UIBuilderConvertor
{
    public class FieldsConfigModel
    {
        public string TextField { get; set; }
        public string ValueField { get; set; }
    }
}
