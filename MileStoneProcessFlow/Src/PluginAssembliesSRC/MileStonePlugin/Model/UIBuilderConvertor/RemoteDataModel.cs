﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MileStonePlugin.Model.UIBuilderConvertor
{
    public class RemoteDataModel
    {
        public Dictionary<string, object> QueryOptions { get; set; }
        public string Uri { get; set; }
        public string ResponsePropertyPath { get; set; }
        public bool ConvertJsonToObject { get; set; }
    }
}
