﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using MileStonePlugin.Dto;

namespace MileStonePlugin.Service
{
    public class DataService
    {
        private readonly BaseDto _baseDto;

        private readonly IOrganizationService _orgService;

        public ConcurrentDictionary<Guid, Entity> EntityCache = new ConcurrentDictionary<Guid, Entity>();

        public ConcurrentDictionary<string, string> EntityDisplayName =
            new ConcurrentDictionary<string, string>();

        public ConcurrentDictionary<string, EntityMetadata> EntityMetadata =
            new ConcurrentDictionary<string, EntityMetadata>();

        public DataService(BaseDto baseDto)
        {
            EntityMetadata = new ConcurrentDictionary<string, EntityMetadata>();
            EntityCache = new ConcurrentDictionary<Guid, Entity>();
            EntityDisplayName = new ConcurrentDictionary<string, string>();
            _baseDto = baseDto;
            _orgService = baseDto.OrgService;
        }

        public EntityCollection GetMilestoneRequirements(Guid milestoneId)
        {
            var query = new QueryExpression("ddsm_milestonerequirements")
            {
                ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression("ddsm_parentmilestone", ConditionOperator.Equal, milestoneId)
                    }
                }
            };
            return _orgService.RetrieveMultiple(query);
        }


        /// <summary>
        ///     Execute Query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public EntityCollection RetrieveMultiple(QueryExpression query)
        {
            try
            {
                return _orgService.RetrieveMultiple(query);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        ///     Execute Query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public OrganizationResponse ExecuteQuery(ExecuteMultipleRequest query)
        {
            try
            {
                return _orgService.Execute(query);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        /// <summary>
        ///     Retreive entity
        /// </summary>
        /// <param name="logicalName"></param>
        /// <param name="recordId"></param>
        /// <returns></returns>
        public Entity RetreiveEntity(string logicalName, string recordId)
        {
            if (string.IsNullOrEmpty(recordId) || string.IsNullOrEmpty(logicalName))
                return null;
            return ExecuteRetreive(logicalName, new Guid(recordId));
        }

        /// <summary>
        ///     Retreive entity
        /// </summary>
        /// <param name="logicalName"></param>
        /// <param name="recordId"></param>
        /// <returns></returns>
        public Entity RetreiveEntity(string logicalName, Guid recordId)
        {
            if (string.IsNullOrEmpty(logicalName) || recordId == Guid.Empty)
                return null;
            return ExecuteRetreive(logicalName, recordId);
        }


        /// <summary>
        ///     Retreive Entity
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <param name="recordId"></param>
        /// <returns></returns>
        private Entity ExecuteRetreive(string entityLogicalName, Guid recordId)
        {
            try
            {
                var entityCache = EntityCache;
                if (entityCache.ContainsKey(recordId))
                {
                    var tmp = new Entity();
                    if (entityCache.TryGetValue(recordId, out tmp))
                        return tmp;
                }
                var entity = _orgService.Retrieve(entityLogicalName, recordId, new ColumnSet(true));
                entityCache.TryAdd(recordId, entity);
                return entity;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        /// <summary>
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <param name="attName"></param>
        /// <returns></returns>
        public IEnumerable<AttributeMetadata> GetAttributeMetadata(string entityLogicalName, string attName)
        {
            try
            {
                var entityMetadata = GetEntityMetadata(entityLogicalName);

                return entityMetadata?.Attributes.Where(att => att.LogicalName.ToLower() == attName.ToLower());
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public string GetEntityDisplayName(string entityLogicalName)
        {
            try
            {
                var entityMetadata = GetEntityMetadata(entityLogicalName);

                return entityMetadata.DisplayName.UserLocalizedLabel.Label;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void RetreiveBatchEntities(List<Guid> entitiesGuids, string entityLogicalName, string fieldName)
        {
            try
            {
                var entityCache = EntityCache;
                var query = new QueryExpression
                {
                    EntityName = entityLogicalName,
                    ColumnSet = new ColumnSet(true),
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression(fieldName, ConditionOperator.In, entitiesGuids)
                        }
                    }
                };
                var rs = _orgService.RetrieveMultiple(query).Entities;
                foreach (var item in rs)
                    entityCache.TryAdd(item.Id, item);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <returns></returns>
        private EntityMetadata GetEntityMetadata(string entityLogicalName)
        {
            var currentEntityMetadata = new EntityMetadata();

            try
            {
                var entityMetadata = EntityMetadata;
                if (entityMetadata.ContainsKey(entityLogicalName))
                {
                    var tmp = new EntityMetadata();
                    if (entityMetadata.TryGetValue(entityLogicalName, out tmp))
                        currentEntityMetadata = tmp;
                }
                else
                {
                    var request = new RetrieveEntityRequest
                    {
                        EntityFilters = EntityFilters.Attributes,
                        LogicalName = entityLogicalName,
                        RetrieveAsIfPublished = true
                    };

                    var response = (RetrieveEntityResponse) _orgService.Execute(request);
                    currentEntityMetadata = response.EntityMetadata;

                    entityMetadata.TryAdd(entityLogicalName, currentEntityMetadata);
                    EntityDisplayName.TryAdd(entityLogicalName,
                        response.EntityMetadata.DisplayName.UserLocalizedLabel.Label);
                }
                return currentEntityMetadata;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        /// <summary>
        ///     Update entity
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="attr"></param>
        /// <param name="value"></param>
        public void UpdateEntity(Entity entity, string attr, string value)
        {
            try
            {
                if (entity == null || string.IsNullOrEmpty(attr))
                    return;
                var attMetadata = GetAttributeMetadata(entity.LogicalName, attr).FirstOrDefault();
                var attrType = attMetadata?.AttributeType;
                if (attrType == null)
                    return;

                switch (attrType)
                {
                    case AttributeTypeCode.Integer:
                        entity.Attributes[attr] = (int) Math.Round(double.Parse(value));
                        break;
                    case AttributeTypeCode.Decimal:
                        decimal amount;
                        decimal.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out amount);
                        entity.Attributes[attr] = amount;
                        break;
                    case AttributeTypeCode.Double:
                        entity.Attributes[attr] = double.Parse(value);
                        break;
                    case AttributeTypeCode.Money:
                        entity.Attributes[attr] = new Money(decimal.Parse(value));
                        break;
                    case AttributeTypeCode.String:
                        entity.Attributes[attr] = value;
                        break;
                    case AttributeTypeCode.Boolean:
                        entity.Attributes[attr] = bool.Parse(value);
                        break;
                    case AttributeTypeCode.DateTime:
                        DateTime? date = null;
                        if (value == "''" || string.IsNullOrEmpty(value))
                        {
                            entity.Attributes[attr] = date;
                            break;
                        }
                        date = DateTime.Parse(value);
                        entity.Attributes[attr] = date;
                        break;
                    case AttributeTypeCode.Picklist:
                        if (value == "''")
                        {
                            entity.Attributes[attr] = null;
                            break;
                        }
                        var key = GetPickListKeyByValue(value, attMetadata);
                        var option = new OptionSetValue(key);
                        entity.Attributes[attr] = option;
                        break;
                    case AttributeTypeCode.Memo:
                        entity.Attributes[attr] = value;
                        break;
                    case AttributeTypeCode.Lookup:
                        var lookUpMetadata = (LookupAttributeMetadata) attMetadata;
                        if (lookUpMetadata == null || lookUpMetadata.Targets.Length == 0)
                            break;
                        var entityLN = lookUpMetadata.Targets[0];
                        entity.Attributes[attr] = new EntityReference(entityLN, new Guid(value));
                        break;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private int GetPickListKeyByValue(string value, AttributeMetadata fieldMetadata)
        {
            try
            {
                var attMetadata = (EnumAttributeMetadata) fieldMetadata;
                var optionMetadata = attMetadata.OptionSet.Options.FirstOrDefault();
                var key = optionMetadata.Value;

                var firstOrDefault = attMetadata.OptionSet.Options
                    .FirstOrDefault(x => x.Label.UserLocalizedLabel.Label == value);

                if (firstOrDefault != null)
                    key = firstOrDefault.Value;

                return (int) key;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public EntityCollection RetrieveMultipleQuery(FetchExpression query)
        {
            return _orgService.RetrieveMultiple(query);
        }


        /// <summary>
        ///     Convert list of ids  to list  of Guid
        /// </summary>
        /// <param name="listIds"></param>
        /// <returns></returns>
        public List<Guid> ConvertListIdToListGuid(List<string> listIds)
        {
            var rs = new List<Guid>();
            foreach (var id in listIds)
                rs.Add(new Guid(id));
            return rs;
        }

        /// <summary>
        ///     Execute multiple update request
        /// </summary>
        /// <param name="entities"></param>
        public void UpdateRequest(List<KeyValuePair<Guid, Entity>> entities)
        {
            try
            {
                var requestWithNoResults = new ExecuteMultipleRequest
                {
                    Settings = new ExecuteMultipleSettings
                    {
                        ContinueOnError = false,
                        ReturnResponses = false
                    },
                    Requests = new OrganizationRequestCollection()
                };

                foreach (var item in entities)
                {
                    var updateRequest = new UpdateRequest {Target = item.Value};
                    requestWithNoResults.Requests.Add(updateRequest);
                }
                _orgService.Execute(requestWithNoResults);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }
    }
}