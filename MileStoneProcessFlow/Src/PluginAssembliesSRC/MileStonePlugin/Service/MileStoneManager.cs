﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using MileStonePlugin.Interfaces;
using MileStonePlugin.Model;

namespace MileStonePlugin.Service
{
    public class MileStoneManager
    {
        private const string MILESTONE_ENTITY_LOGICAL_NAME = "ddsm_milestone";
        private const string BUSINESS_RULE_ALIAS = "businessRuleAlias_";
        private const string REQUIRED_FIELD_ALIAS = "requiredFieldAlias_";
        private readonly IMileStoneMapper _mapper;
        private readonly IMetadataService _metadataService;
        private readonly IMileStoneService _mileStoneService;

        public MileStoneManager(IMetadataService metadataService, IMileStoneMapper mapper,
            IMileStoneService milestoneService)
        {
            _metadataService = metadataService;
            _mapper = mapper;
            _mileStoneService = milestoneService;
        }

        public IReadOnlyList<Step> GetMilestones(string entityName, string recordId, string userId, IOrganizationService service)
        {
            var lookupFieldName = _metadataService.GetLookupFieldName(MILESTONE_ENTITY_LOGICAL_NAME, entityName);
            var recordGuid = new Guid(recordId);
            var userGuid = new Guid(userId);

            IReadOnlyList<Entity> entityListResponse = _mileStoneService
                .GetMileStoneWithRelatedBusinessRules(MILESTONE_ENTITY_LOGICAL_NAME, lookupFieldName, recordGuid,
                    BUSINESS_RULE_ALIAS, REQUIRED_FIELD_ALIAS)
                .ToList();

            IReadOnlyList<Step> mappedMileStones = _mapper.GetMappedSteps(entityListResponse, 
                    BUSINESS_RULE_ALIAS, REQUIRED_FIELD_ALIAS, service, recordGuid, userGuid, entityName)
                .OrderBy(step => step.Index)
                .ToList();

            return mappedMileStones;
        }
    }
}