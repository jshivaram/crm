﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using MileStonePlugin.Interfaces;
using MileStonePlugin.Model;

namespace MileStonePlugin.Service
{
    public class MileStoneManagerHistory
    {
        private const string MS_STATUS_ENTITY_NAME = "ddsm_historymsstatus";
        private const string MS_LOOKUP_TO_MS = "ddsm_milestone";
        private const string MS_STATUS_PRIMARY_KEY = "ddsm_historymsstatusid";
        private const string BUSINESS_RULE_ALIAS = "br_";
        private readonly IMapperHistory _mapper;
        private readonly IMileStoneServiceHistory _msService;

        public MileStoneManagerHistory(IMapperHistory mapper, IMileStoneServiceHistory serivce)
        {
            _mapper = mapper;
            _msService = serivce;
        }

        public IReadOnlyList<StepHistory> GetMilestoneHistory(IReadOnlyList<string> milestoneIdList)
        {
            try
            {
                var msWithStatus = _msService.GetMileStoneStatuses("ddsm_milestone", "ddsm_milestoneid", "ddsm_status",
                    milestoneIdList);
                var stepsWithActualStatus = _mapper.GetStatusesFromEntities(msWithStatus, "ddsm_status");

                if (stepsWithActualStatus == null || !stepsWithActualStatus.Any())
                    throw new Exception($"{nameof(milestoneIdList)} has fetched emtpy entity list");

                IReadOnlyList<Entity> response = _msService.GetMileStoneHistoryStatuses(
                        MS_STATUS_ENTITY_NAME,
                        MS_LOOKUP_TO_MS,
                        milestoneIdList)
                    .ToList();

                var newestMilestoneStatuses = _mapper.GetNewestMileStoneIdList(response, MS_LOOKUP_TO_MS);

                if (newestMilestoneStatuses == null || !newestMilestoneStatuses.Any())
                    return stepsWithActualStatus;

                IReadOnlyList<Entity> msWithBusinessRules = _msService.GetMileStoneStatusesWithBusinessRules(
                        MS_STATUS_ENTITY_NAME,
                        MS_STATUS_PRIMARY_KEY,
                        MS_LOOKUP_TO_MS,
                        BUSINESS_RULE_ALIAS,
                        newestMilestoneStatuses)
                    .ToList();

                var groupedMSWithBr = _mapper.GetNewestBusinessRulesWithMileStones(msWithBusinessRules, MS_LOOKUP_TO_MS,
                    BUSINESS_RULE_ALIAS);

                var allSteps = _mapper.UpdateStepStatusesFromAnotherStepArray(groupedMSWithBr, stepsWithActualStatus);

                return allSteps;
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                throw new Exception(message);
            }
        }
    }
}