﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using MileStonePlugin.Interfaces;
using MileStonePlugin.Model;

namespace MileStonePlugin
{
    public class MapperHistory : IMapperHistory
    {
        public IReadOnlyList<string> GetNewestMileStoneIdList(IReadOnlyList<Entity> entities, string lookupFieldName)
        {
            return entities
                .GroupBy(e => e.GetAttributeValue<EntityReference>(lookupFieldName).Id, e => e)
                .Select(g => g
                    .OrderBy(e => e.GetAttributeValue<DateTime>("modifienon"))
                    .LastOrDefault()
                    .Id.ToString())
                .ToList();
        }

        public IReadOnlyList<StepHistory> GetNewestBusinessRulesWithMileStones(IReadOnlyList<Entity> entities,
            string lookupFieldName, string businessRuleAlias)
        {
            return entities
                .GroupBy(e => e.GetAttributeValue<EntityReference>(lookupFieldName).Id, e => e)
                .Select(s => new StepHistory
                {
                    Id = s.Key,
                    Status = s.First().GetAttributeValue<OptionSetValue>("ddsm_status").Value,
                    DisplayName = s.First().GetAttributeValue<string>("ddsm_name"),
                    BusinessRules = s
                        .Select(br =>
                        {
                            return new
                            {
                                Id = br.GetAttributeValue<AliasedValue>(businessRuleAlias + ".ddsm_businessrule")?.Value,
                                DisplayName =
                                br.GetAttributeValue<AliasedValue>(businessRuleAlias + ".ddsm_name")?.Value,
                                Status = br.GetAttributeValue<AliasedValue>(businessRuleAlias + ".ddsm_status")?.Value,
                                Modified = br.GetAttributeValue<AliasedValue>(businessRuleAlias + ".modifiedon")?.Value
                            };
                        })
                        .Select(br =>
                        {
                            return new BusinessRuleHistory
                            {
                                Id = (br.Id as EntityReference)?.Id.ToString(),
                                DisplayName = br.DisplayName as string,
                                Status = (br.Status as OptionSetValue)?.Value,
                                Modified = br.Modified as DateTime?
                            };
                        })
                        .GroupBy(br => br.Id, br => br)
                        .Select(g => g
                            .OrderBy(br => br.Modified)
                            .LastOrDefault()
                        )
                        .ToList()
                })
                .ToList();
        }

        public IReadOnlyList<StepHistory> GetStatusesFromEntities(IReadOnlyList<Entity> entities, string statusFieldName)
        {
            return entities
                .Select(e =>
                {
                    return new StepHistory
                    {
                        Id = e.Id,
                        Status = e.GetAttributeValue<OptionSetValue>(statusFieldName).Value,
                        BusinessRules = new List<BusinessRuleHistory>()
                    };
                })
                .ToList();
        }

        public IReadOnlyList<StepHistory> UpdateStepStatusesFromAnotherStepArray(
            IReadOnlyList<StepHistory> sourceStepArray,
            IReadOnlyList<StepHistory> targetSteps)
        {
            for (var i = 0; i < targetSteps.Count - 1; i++)
                foreach (var step in sourceStepArray)
                    if (targetSteps[i].Id == step.Id)
                    {
                        targetSteps[i].DisplayName = step.DisplayName;
                        targetSteps[i].BusinessRules = step.BusinessRules;
                    }
            return targetSteps;
        }
    }
}