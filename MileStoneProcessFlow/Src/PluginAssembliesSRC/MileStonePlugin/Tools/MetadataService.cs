﻿using System;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using MileStonePlugin.Interfaces;

namespace MileStonePlugin
{
    public class CustomMetadataService : IMetadataService
    {
        private readonly IOrganizationService _service;

        public CustomMetadataService(IOrganizationService service)
        {
            _service = service;
        }

        public string GetLookupFieldName(string referencingEntity, string referencedEntity)
        {
            var retrieveEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = referencingEntity
            };

            var retrieveResponse =
                (RetrieveEntityResponse) _service.Execute(retrieveEntityRequest);
            var manyToOneRelationships = retrieveResponse.EntityMetadata.ManyToOneRelationships.ToList();

            var relation = manyToOneRelationships.FirstOrDefault(r => r.ReferencedEntity == referencedEntity);
            if (relation != null) return relation.ReferencingAttribute;

            throw new Exception($"{referencingEntity} does not have lookup to {referencedEntity} entity.");
        }
    }
}