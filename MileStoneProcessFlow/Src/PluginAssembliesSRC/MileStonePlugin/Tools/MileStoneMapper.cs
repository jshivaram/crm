﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using MileStonePlugin.Interfaces;
using MileStonePlugin.Model;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using DDSM.CommonProvider.JSON;
using System.Runtime.Remoting;
namespace MileStonePlugin
{
    public enum MilestoneStatus
    {
        Empty = 0,
        NotStarted = 962080000,
        Active = 962080001,
        Completed = 962080002,
        Skipped = 962080003
    }

    public class MileStoneMapper : IMileStoneMapper
    {
        public IReadOnlyList<Step> GetMappedSteps(IReadOnlyList<Entity> entities,
            string businessRuleAlias,
            string requiredFieldAlias,
            IOrganizationService service,
            Guid recordGuid,
            Guid userGuid,
            string logicalName)
        {
            IReadOnlyList<IGrouping<Guid, Entity>> groupedValues =
                entities.GroupBy(step => step.Id, step => step).ToList();

            var userRoles = GetUserRoles(userGuid, service);
            bool allowAll = true;

            var mappedValues = groupedValues
                .Select(stepGroup =>
                {
                    var step = stepGroup.FirstOrDefault();
                    var stepDisplayName = step?.GetAttributeValue<string>("ddsm_name");

                    var index = step?.GetAttributeValue<int?>("ddsm_index");

                    MilestoneStatus status = MilestoneStatus.Empty;
                    Guid roleId = Guid.Empty;

                    if (step != null && step.Contains("ddsm_permitrole"))
                    {
                        roleId = step.GetAttributeValue<EntityReference>("ddsm_permitrole").Id;
                    }

                    if (step != null && step.Contains("ddsm_status"))
                    {
                        status = (MilestoneStatus)step.GetAttributeValue<OptionSetValue>("ddsm_status").Value;
                    }

                    if (index == null)
                        throw new Exception("MileStone index is null.");

                    var stepModel = new Step
                    {
                        Id = stepGroup.Key,
                        DisplayName = stepDisplayName,
                        Index = index.Value,
                        IsAllowed = allowAll,
                        BusinessRules = _GetMappedBusinessRules(stepGroup, businessRuleAlias),
                        RequiredFields = _GetMappedRequiredFields(stepGroup, requiredFieldAlias, recordGuid, logicalName, service)
                    };

                    if (!userRoles.Contains(roleId) && (roleId != Guid.Empty) && (status != MilestoneStatus.Completed) && (status != MilestoneStatus.Skipped))
                    {
                        allowAll = false;
                    }

                    return stepModel;
                })
                .ToList();

            return mappedValues;
        }

        private IReadOnlyList<BusinessRule> _GetMappedBusinessRules(IGrouping<Guid, Entity> stepGroup, string businessRuleAlias)
        {
            var businessRules = stepGroup
                .Select(businessRule =>
                {
                    var businessRuleAliasId =
                        businessRule.GetAttributeValue<AliasedValue>($"{businessRuleAlias}.ddsm_businessruleid");
                    var businessRuleId = businessRuleAliasId?.Value as Guid?;

                    var businessRuleAliasDisplayName =
                        businessRule.GetAttributeValue<AliasedValue>($"{businessRuleAlias}.ddsm_name");
                    var businessRuleDisplayName = businessRuleAliasDisplayName?.Value as string;

                    var businessRuleModel = new BusinessRule
                    {
                        Id = businessRuleId,
                        DisplayName = businessRuleDisplayName
                    };

                    return businessRuleModel;
                })
                .Where(businessRule => businessRule.Id != null).GroupBy(x => x.Id).Select(x => x.First())
                .ToList();


            return businessRules;
        }

        private IReadOnlyList<RequiredFieldModel> _GetMappedRequiredFields(IGrouping<Guid, Entity> stepGroup, string requiredFieldAlias, Guid recordGuid, string logicalName, IOrganizationService service)
        {
            var requiredFields = stepGroup
                .GroupBy(x => x.GetAttributeValue<AliasedValue>("requiredFieldAlias_.ddsm_milestonerequirementsid")?.Value as Guid?)
                .Select(x => x.First())
                .ToList()
                .Select(requiredField =>
                {
                    AliasedValue requiredFieldAliasId =
                        requiredField.GetAttributeValue<AliasedValue>($"{requiredFieldAlias}.ddsm_milestonerequirementsid");
                    Guid? requiredFieldId = requiredFieldAliasId?.Value as Guid?;

                    AliasedValue requiredFieldAliasDisplayName =
                        requiredField.GetAttributeValue<AliasedValue>($"{requiredFieldAlias}.ddsm_name");
                    string requiredFieldDisplayName = requiredFieldAliasDisplayName?.Value as string;

                    AliasedValue requiredFieldAliasRequired =
                        requiredField.GetAttributeValue<AliasedValue>($"{requiredFieldAlias}.ddsm_required");
                    bool? requiredFieldRequired = requiredFieldAliasRequired?.Value as bool?;

                    AliasedValue requiredFieldAliasConfig =
                        requiredField.GetAttributeValue<AliasedValue>($"{requiredFieldAlias}.ddsm_config");
                    string requiredFieldConfig = requiredFieldAliasConfig?.Value as string;

                    RequiredFieldModel requiredFieldModel = null;

                    try
                    {
                        if (requiredFieldConfig != null)
                        {
                            requiredFieldModel = JsonConvert.DeserializeObject<RequiredFieldModel>(requiredFieldConfig);
                            requiredFieldModel.Required = requiredFieldRequired;
                            requiredFieldModel.DisplayName = requiredFieldDisplayName;
                            requiredFieldModel.Id = requiredFieldId;
                        }
                        else
                        {
                            requiredFieldModel = new RequiredFieldModel
                            {
                                Required = requiredFieldRequired,
                                DisplayName = requiredFieldDisplayName,
                                Id = requiredFieldId
                            };
                        }
                    }
                    catch (Exception e)
                    {
                        requiredFieldModel = new RequiredFieldModel();
                    }

                    return requiredFieldModel;
                })
                .Where(requiredField => requiredField.Id != null).GroupBy(x => x.Id).Select(x => x.First())
                .ToList();
            GetValueForRequiredField(requiredFields, service, recordGuid, logicalName);
            return requiredFields.OrderBy(requiredField => requiredField.DisplayName).ToList();
        }

        private void GetValueForRequiredField(List<RequiredFieldModel> requiredFields, IOrganizationService service, Guid recordGuid, string logicalName)
        {
            try
            {
                
                if (requiredFields.Count == 0)
                {
                    return;
                }

                #region With target field
                List<RequiredFieldModel> requiredFieldsWithLookupToCurrentEntity =
                    requiredFields.Where(requiredField => requiredField.TargetField != null && requiredField.MilestoneLookup?.TargetEntity?.LogicalName == logicalName).ToList();
                List<RequiredFieldModel> requiredFieldsWithLookupFromCurrentEntity =
                    requiredFields.Where(requiredField => requiredField.TargetField != null && requiredField.MilestoneLookup?.TargetEntity?.LogicalName != logicalName).ToList();

                List<string> curentEntityFields = requiredFieldsWithLookupToCurrentEntity.Select(requiredField => requiredField.TargetField?.LogicalName).ToList();
                curentEntityFields.AddRange(
                    requiredFieldsWithLookupFromCurrentEntity.Select(requiredField => requiredField.MilestoneLookup?.LogicalName));
                curentEntityFields = curentEntityFields.Distinct().ToList();

                if (curentEntityFields.Count != 0)
                {
                    Entity currentEntity = service.Retrieve(logicalName, recordGuid, new ColumnSet(curentEntityFields.ToArray()));
                    if (requiredFieldsWithLookupToCurrentEntity.Count != 0)
                    {
                        foreach (var requiredField in requiredFieldsWithLookupToCurrentEntity)
                        {
                            requiredField.Value = TryGetValue(currentEntity, requiredField.TargetField?.LogicalName);
                        }
                    }
                    if(requiredFieldsWithLookupFromCurrentEntity.Count != 0)
                    {
                        var references = new List<EntityReference>();
                        foreach(var requiredField in requiredFieldsWithLookupFromCurrentEntity)
                        {
                            EntityReference reference = currentEntity.GetAttributeValue<EntityReference>(requiredField.MilestoneLookup?.LogicalName);
                            if(reference != null)
                            {
                                references.Add(reference);
                            }
                        }
                        references = references.Distinct().ToList();

                        foreach(var reference in references)
                        {
                            List<RequiredFieldModel> requiredFieldsOfEntityRefference = 
                                requiredFieldsWithLookupFromCurrentEntity.Where(requiredField => requiredField.MilestoneLookup.TargetEntity?.LogicalName == reference.LogicalName).ToList();
                            List<string> fields =
                                requiredFieldsOfEntityRefference.Select(requiredField => requiredField.TargetField?.LogicalName).ToList();

                            Entity entity = service.Retrieve(reference.LogicalName, reference.Id, new ColumnSet(fields.ToArray()));

                            foreach(var requiredField in requiredFieldsOfEntityRefference)
                            {
                                requiredField.Value = TryGetValue(entity, requiredField.TargetField?.LogicalName);
                            }
                        }
                    }
                }

                
                #endregion

                #region With child entity
                List<RequiredFieldModel> requiredFieldsWithChildEntity = requiredFields.Where(t => t.ChildTargetEntity != null).ToList();
                if (requiredFieldsWithChildEntity != null || requiredFieldsWithChildEntity.Count != 0)
                {
                    #region Get indicators
                    List<Guid?> indicatorsId = requiredFieldsWithChildEntity.Select(field => field.Indicator?.Id).ToList();
                    if(indicatorsId == null || indicatorsId.Count == 0)
                    {
                        return;
                    }

                    var queryIndicator = new QueryExpression
                    {
                        Criteria = new FilterExpression
                        {
                            FilterOperator = LogicalOperator.Or,
                        },
                        ColumnSet = new ColumnSet("ddsm_targetfield", "ddsm_targetentity"),
                        EntityName = "ddsm_indicator"
                    };
                    foreach (var id in indicatorsId)
                    {
                        queryIndicator.Criteria.Conditions.Add(new ConditionExpression("ddsm_indicatorid", ConditionOperator.Equal, id));
                    }

                    List<Entity> indicators = service.RetrieveMultiple(queryIndicator).Entities.ToList();
                    if (indicators == null || indicators.Count == 0)
                    {
                        return;
                    }
                    #endregion

                    IEnumerable<string> entitiesLogicalNames = requiredFieldsWithChildEntity.Select(field => field.ChildTargetEntity?.LogicalName).Distinct();

                    foreach (var entityLogicalName in entitiesLogicalNames)
                    {
                        CustomMetadataService customMetadataService = new CustomMetadataService(service);
                        string projectRelationLogicalName = customMetadataService.GetLookupFieldName(entityLogicalName, logicalName);
                        string indicatorRelationLogicalName = indicators.Find(indicator => indicator.GetAttributeValue<string>("ddsm_targetentity") == entityLogicalName)?.GetAttributeValue<string>("ddsm_targetfield");

                        if (string.IsNullOrEmpty(indicatorRelationLogicalName) || string.IsNullOrEmpty(projectRelationLogicalName))
                        {
                            continue;
                        }

                        List<string> fields = requiredFieldsWithChildEntity
                            .Where(requiredField => requiredField?.ChildTargetEntity?.LogicalName == entityLogicalName)
                            .Select(requiredField => requiredField?.ChildTargetField?.LogicalName)
                            .Distinct()
                            .ToList();

                        if(fields == null || fields.Count == 0)
                        {
                            continue;
                        }

                        fields.Add(indicatorRelationLogicalName);

                        var query = new QueryExpression
                        {
                            Criteria = new FilterExpression(),
                            ColumnSet = new ColumnSet(fields.ToArray()),
                            EntityName = entityLogicalName
                        };

                        FilterExpression childFilter = new FilterExpression(LogicalOperator.Or);
                        childFilter.AddCondition(new ConditionExpression(indicatorRelationLogicalName, ConditionOperator.In, requiredFieldsWithChildEntity
                                        .Where(requiredField => requiredField?.ChildTargetEntity?.LogicalName == entityLogicalName)
                                        .Select(requiredField => requiredField?.Indicator?.Id.ToString()).ToArray()));
                        FilterExpression parentFilter = new FilterExpression(LogicalOperator.And);
                        parentFilter.AddCondition(new ConditionExpression(projectRelationLogicalName, ConditionOperator.Equal, recordGuid));
                        parentFilter.AddFilter(childFilter);

                        query.Criteria.Filters.Add(parentFilter);

                        EntityCollection response = service.RetrieveMultiple(query);

                        foreach (var entity in response.Entities)
                        {
                            IEnumerable<RequiredFieldModel> currentRequiredFields = requiredFields.Where(requiredField => requiredField?.Indicator?.Id == entity.GetAttributeValue<EntityReference>(indicatorRelationLogicalName)?.Id);
                            foreach (var currentRequiredField in currentRequiredFields)
                            {
                                currentRequiredField.Value = TryGetValue(entity, currentRequiredField.ChildTargetField.LogicalName);
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private string TryGetValue(Entity entity, string field)
        {
            try
            {
                if (entity.FormattedValues.Keys.Contains(field))
                {
                    return entity.FormattedValues[field];
                }
                else
                {
                    var value = entity.GetAttributeValue<object>(field);
                    if (value != null)
                    {
                        return value.ToString();
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private IEnumerable<Guid> GetUserRoles (Guid userId, IOrganizationService service)
        {
            string rolesFetch = @"<fetch>
              <entity name='systemuserroles' >
                <attribute name='roleid' />
                <attribute name='systemuserroleid' />
                <attribute name='systemuserid' />
                <filter>
                  <condition attribute='systemuserid' operator='eq' value='" + userId + @"' />
                </filter>
              </entity>
            </fetch>";

            List<Guid> userRoleids = new List<Guid>();

            try
            {
                EntityCollection roles = service.RetrieveMultiple(new FetchExpression(rolesFetch));
                foreach (var role in roles.Entities)
                {
                    var roleId = role.GetAttributeValue<Guid>("roleid");
                    userRoleids.Add(roleId);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return userRoleids;
        }
    }
}