﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using MileStonePlugin.Interfaces;

using System.Linq;

namespace MileStonePlugin
{
    public class MileStoneService : IMileStoneService
    {
        private readonly IOrganizationService _service;

        public MileStoneService(IOrganizationService service)
        {
            _service = service;
        }

        public IEnumerable<Entity> GetMileStoneWithRelatedBusinessRules(
            string entityLogicalName,
            string lookupFieldName,
            Guid lookupValueId,
            string businessRuleAlias,
            string requiredFieldAlias
        )
        {
            var queryExpression = new QueryExpression(entityLogicalName)
            {
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression(lookupFieldName, ConditionOperator.Equal, lookupValueId)
                    }
                },
                ColumnSet = new ColumnSet("ddsm_name", "ddsm_index", "ddsm_permitrole", "ddsm_status"),
                LinkEntities =
                {
                    new LinkEntity
                    {
                        LinkFromEntityName = entityLogicalName,
                        LinkFromAttributeName = "ddsm_milestoneid",
                        LinkToEntityName = "ddsm_milestonerequirements",
                        LinkToAttributeName = "ddsm_parentmilestone",
                        JoinOperator = JoinOperator.LeftOuter,
                        Columns = new ColumnSet("ddsm_name", "ddsm_required", "ddsm_milestonerequirementsid", "ddsm_config"),
                        EntityAlias = requiredFieldAlias
                    },
                    //Get MileStone Business Rule 1:N
                    new LinkEntity
                    {
                        LinkFromEntityName = entityLogicalName,
                        LinkFromAttributeName = "ddsm_milestoneid",
                        LinkToEntityName = "ddsm_milestonebusinessrule",
                        LinkToAttributeName = "ddsm_milestoneid",
                        JoinOperator = JoinOperator.LeftOuter,
                        LinkEntities =
                        {
                            //Get BR N:1
                            new LinkEntity
                            {
                                LinkFromEntityName = "ddsm_milestonebusinessrule",
                                LinkFromAttributeName = "ddsm_businessruleid",
                                LinkToEntityName = "ddsm_businessrule",
                                LinkToAttributeName = "ddsm_businessruleid",
                                JoinOperator = JoinOperator.LeftOuter,
                                EntityAlias = businessRuleAlias,
                                Columns = new ColumnSet("ddsm_name", "ddsm_businessruleid")
                            }
                        }
                    }
                }
            };
            return _service.RetrieveMultiple(queryExpression).Entities;
        }
    }
}