﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using MileStonePlugin.Interfaces;

namespace MileStonePlugin.Tools
{
    internal class MileStoneServiceHistory : IMileStoneServiceHistory
    {
        private readonly IOrganizationService _service;

        public MileStoneServiceHistory(IOrganizationService service)
        {
            _service = service;
        }

        public IReadOnlyList<Entity> GetMileStoneHistoryStatuses(string entityLogicalName, string lookupAttributeName,
            IReadOnlyList<string> idArray)
        {
            var query = new QueryExpression(entityLogicalName)
            {
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression(lookupAttributeName, ConditionOperator.In, idArray.ToArray())
                    }
                },
                ColumnSet = new ColumnSet("ddsm_name", lookupAttributeName, "modifiedon")
            };
            var response = _service.RetrieveMultiple(query).Entities.ToList();
            return response;
        }

        public IReadOnlyList<Entity> GetMileStoneStatuses(string entityLogicalName, string primaryKeyField,
            string statusField, IReadOnlyList<string> idArray)
        {
            var query = new QueryExpression(entityLogicalName)
            {
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression(primaryKeyField, ConditionOperator.In, idArray.ToArray())
                    }
                },
                ColumnSet = new ColumnSet("ddsm_name", statusField)
            };
            var response = _service.RetrieveMultiple(query).Entities.ToList();
            return response;
        }

        public IReadOnlyList<Entity> GetMileStoneStatusesWithBusinessRules(string entityLogicalName,
            string attributeToCheck, string lookupAttributeName, string businessRuleAlias, IReadOnlyList<string> idArray)
        {
            var query = new QueryExpression(entityLogicalName)
            {
                Criteria = new FilterExpression(LogicalOperator.And)
                {
                    Conditions =
                    {
                        new ConditionExpression(attributeToCheck, ConditionOperator.In, idArray.ToArray())
                    }
                },
                ColumnSet = new ColumnSet("ddsm_name", lookupAttributeName, "ddsm_status"),
                LinkEntities =
                {
                    new LinkEntity
                    {
                        LinkFromEntityName = entityLogicalName,
                        LinkFromAttributeName = "ddsm_historymsstatusid",
                        LinkToEntityName = "ddsm_ddsm_historymsstatus_ddsm_historymsbrst",
                        LinkToAttributeName = "ddsm_historymsstatusid",
                        JoinOperator = JoinOperator.Inner,
                        LinkEntities =
                        {
                            new LinkEntity
                            {
                                LinkFromEntityName = "ddsm_ddsm_historymsstatus_ddsm_historymsbrst",
                                LinkFromAttributeName = "ddsm_historymsbrstatusid",
                                LinkToEntityName = "ddsm_historymsbrstatus",
                                LinkToAttributeName = "ddsm_historymsbrstatusid",
                                JoinOperator = JoinOperator.Inner,
                                EntityAlias = businessRuleAlias,
                                Columns =
                                    new ColumnSet("ddsm_name", "modifiedon", "ddsm_status", "ddsm_businessrule",
                                        "ddsm_historymsbrstatusid")
                            }
                        }
                    }
                }
            };
            var response = _service.RetrieveMultiple(query).Entities.ToList();
            return response;
        }
    }
}