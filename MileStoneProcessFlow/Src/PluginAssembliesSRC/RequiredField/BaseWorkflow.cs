﻿using System;
using System.Activities;
using System.Collections.Generic;
using DDSM.CommonProvider.JSON;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using RequiredField.Dto;
using RequiredField.Service;

namespace RequiredField
{
    public abstract class BaseWorkflow : CodeActivity
    {
        public BaseDto BaseDto { get; set; }

        [Input("MilestoneId")]
        public InArgument<string> MilestoneId { get; set; }

        [Input("MilestoneIds")]
        public InArgument<string> MilestoneIds { get; set; }

        [Output("Result")]
        public OutArgument<string> Result { get; set; }

        /// <summary>
        ///     A plug-in that provide common implementation
        /// </summary>
        protected override void Execute(CodeActivityContext executionContext)
        {
            try
            {
                // The InputParameters collection contains all the data passed in the message request.
                if (!IsContextFull(executionContext))
                    return;

                PopulateBaseDto(executionContext);

                //Execute plug-in
                Main();
            }
            catch (Exception ex)
            {
                BaseDto.Tracer.Trace(ex.Message);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="executionContext"></param>
        private void PopulateBaseDto(CodeActivityContext executionContext)
        {
            BaseDto = new BaseDto(executionContext);
            var dataService = new DataService(BaseDto);
            BaseDto.DataService = dataService;
            BaseDto.RelationshipService = new RelationshipService(BaseDto, dataService);

            var milestoneId = MilestoneId.Get(executionContext);
            if (milestoneId != null)
            {
                BaseDto.MilestoneIds.Add(Guid.Parse(milestoneId));
                return;
            }
            var milestoneIds = MilestoneIds.Get(executionContext);
            if (milestoneIds == null) return;
            BaseDto.MilestoneIds = JsonConvert.DeserializeObject<List<Guid>>(milestoneIds);
        }


        /// <summary>
        ///     Cast Target to Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private Entity GetEntity(object entity)
        {
            try
            {
                if (entity == null)
                    throw new Exception("Can't get target from Context! entity is null");

                var target = entity;
                if (target is EntityReference)
                {
                    var sourceEntity = (EntityReference) entity;
                    return new Entity(sourceEntity.LogicalName, sourceEntity.Id);
                }

                if (target is Entity)
                    return (Entity) target;

                throw new Exception("Can't get target from Context! entity is null");
            }
            catch (Exception)
            {
                throw new Exception("Can't get target from Context");
            }
        }

        /// <summary>
        ///     Start Executing a plug-in
        /// </summary>
        private void Main()
        {
            //Run the specific plugin
            Run();
        }

        /// <summary>
        ///     Abstract method should be implemented in a child class
        /// </summary>
        protected abstract void Run();


        /// <summary>
        ///     Obtain the execution context from the service provider.
        /// </summary>
        /// <param name="executionContext"></param>
        /// <returns></returns>
        private bool IsContextFull(CodeActivityContext executionContext)
        {
            var tracer = executionContext.GetExtension<ITracingService>();
            if (executionContext == null) throw new ArgumentNullException(nameof(executionContext));
            var rs = true;
            try
            {
                if (executionContext.GetExtension<IWorkflowContext>() == null)
                    rs = false;
            }
            catch (Exception ex)
            {
                tracer.Trace("IsContextFull:" + ex.Message);
                return false;
            }
            return rs;
        }
    }
}