﻿using System;
using System.Activities;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using RequiredField.Service;

namespace RequiredField.Dto
{
    public class BaseDto
    {
        public BaseDto(CodeActivityContext executionContext)
        {
            ExecutionContext = executionContext;
            // Obtain the execution context from the service provider.
            WorkflowContext = executionContext.GetExtension<IWorkflowContext>();

            //Extract the tracing service for use in debugging sandboxed plug-ins.
            Tracer = executionContext.GetExtension<ITracingService>();

            // Obtain the organization service reference.
            var factory = executionContext.GetExtension<IOrganizationServiceFactory>();
            OrgService = factory.CreateOrganizationService(WorkflowContext.UserId);

            //targer entity
            Target = new Entity();
            MilestoneIds = new List<Guid>();
        }

        public CodeActivityContext ExecutionContext { get; }

        //Execution Context
        public IWorkflowContext WorkflowContext { get; }

        //Oganization Service
        public IOrganizationService OrgService { get; set; }

        //Target Entity
        public Entity Target { get; set; }

        //Tracing Service
        public ITracingService Tracer { get; }

        public List<Guid> MilestoneIds { get; set; }
        public DataService DataService { get; set; }
        public RelationshipService RelationshipService { get; set; }
    }
}