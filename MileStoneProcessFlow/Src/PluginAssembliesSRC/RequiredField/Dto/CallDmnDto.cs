﻿using System;
using System.Collections.Generic;

namespace RequiredField.Dto
{
    internal class CallDmnDto
    {
        public List<BusinessRule> BusinessRules;

        public CallDmnDto()
        {
            BusinessRules = new List<BusinessRule>();
            UserId = Guid.Empty;
            TargetRecordId= Guid.Empty;
        }

        public string TargetEntityLogicalName { get; set; }
        public Guid TargetRecordId { get; set; }
        public Guid UserId { get; set; }

        public class BusinessRule
        {
            public Guid Id;

            public BusinessRule()
            {
                Id = Guid.Empty;
            }
        }
    }
}