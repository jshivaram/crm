﻿using System.Collections.Generic;

namespace RequiredField.Dto
{
    public class FieldMetadataDto
    {
        public FieldMetadataDto()
        {
            LocalData = new List<ComboBox>();
        }

        public string Label { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string DefaultValue { get; set; }
        public List<ComboBox> LocalData { get; set; }


        public class ComboBox
        {
            public string DisplayName;
            public string Id;
        }
    }
}