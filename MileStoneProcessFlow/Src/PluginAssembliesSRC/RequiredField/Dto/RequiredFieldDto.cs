﻿using System;
using System.Collections.Generic;

namespace RequiredField.Dto
{
    public class RequiredFieldDto
    {
        public RequiredFieldDto()
        {
            MilestoneId = Guid.Empty;
            RequiredEntities = new List<RequiredEntity>();
        }

        public Guid MilestoneId { get; set; }
        public List<RequiredEntity> RequiredEntities { get; set; }


        public class RequiredEntity
        {
            public RequiredEntity()
            {
                RecordId = Guid.Empty;
                RequiredFieldMetadata = new List<FieldMetadataDto>();
            }

            public string EntityLogicalName { get; set; }
            public Guid RecordId { get; set; }

            public List<FieldMetadataDto> RequiredFieldMetadata { get; set; }
        }


        
    }
}