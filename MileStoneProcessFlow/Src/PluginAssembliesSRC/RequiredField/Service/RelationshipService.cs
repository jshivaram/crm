﻿using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using RequiredField.Dto;

namespace RequiredField.Service
{
    public class RelationshipService
    {
        private readonly BaseDto _baseDto;
        private readonly DataService _dataService;

        private readonly IOrganizationService _orgService;

        public RelationshipService(BaseDto baseDto, DataService dataService)
        {
            _baseDto = baseDto;
            _orgService = baseDto.OrgService;
            _dataService = dataService;
        }


        /// <summary>
        ///     Get Refarencing Attribute from entity metadata
        /// </summary>
        /// <param name="targetEntity"></param>
        /// <param name="referencingEntity"></param>
        /// <returns></returns>
        public OneToManyRelationshipMetadata GetRefarencingMetadata(string targetEntity, string referencingEntity)
        {
            var o2M = GetRefarencingAttrOneToMany(targetEntity, referencingEntity);
            if (o2M != null)
                return o2M;
            var m2O = GetRefarencingAttrManyToOne(targetEntity, referencingEntity);
            if (m2O != null)
                return m2O;
            return null;
        }

        public OneToManyRelationshipMetadata GetRefarencingAttrOneToMany(string targetEntity, string referencingEntity)
        {
            var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = targetEntity
            };

            var retrieveBankEntityResponse =
                (RetrieveEntityResponse) _orgService.Execute(retrieveBankAccountEntityRequest);

            var oneToManyRelationships = retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships;
            var rs =
                oneToManyRelationships.Where(r => r.ReferencingEntity == referencingEntity).ToList();

            return rs.Count == 0 ? null : rs[0];
        }

        public OneToManyRelationshipMetadata GetRefarencingAttrManyToOne(string targetEntity, string referencedEntity)
        {
            var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = targetEntity
            };

            var retrieveBankEntityResponse =
                (RetrieveEntityResponse) _orgService.Execute(retrieveBankAccountEntityRequest);

            var manyToOneRelationships = retrieveBankEntityResponse.EntityMetadata.ManyToOneRelationships;
            var rs =
                manyToOneRelationships.Where(r => r.ReferencedEntity == referencedEntity).ToList();

            return rs.Count == 0 ? null : rs[0];
        }

        public EntityCollection GetReferencingEntities(Entity referencedEntity, string relationshipName)
        {
            var metadata = GetOneToManyRelationshipMetadata(referencedEntity.LogicalName, relationshipName);
            if (metadata == null) return null;

            var query = new QueryExpression
            {
                ColumnSet = new ColumnSet(),
                EntityName = metadata.ReferencingEntity,
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression(metadata.ReferencingAttribute, ConditionOperator.Equal,
                            referencedEntity.Id)
                    }
                }
            };
            return _dataService.RetrieveMultiple(query);
        }

        public EntityCollection GetReferencingEntities(EntityReference referencedEntity, string relationshipName)
        {
            var metadata = GetOneToManyRelationshipMetadata(referencedEntity.LogicalName, relationshipName);
            if (metadata == null) return null;

            var query = new QueryExpression
            {
                ColumnSet = new ColumnSet(),
                EntityName = metadata.ReferencingEntity,
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression(metadata.ReferencingAttribute, ConditionOperator.Equal,
                            referencedEntity.Id)
                    }
                }
            };
            return _dataService.RetrieveMultiple(query);
        }

        public EntityCollection GetReferencingRequiredEntities(EntityReference referencedEntity, string relationshipName,
            EntityReference indicator)
        {
            var metadata = GetOneToManyRelationshipMetadata(referencedEntity.LogicalName, relationshipName);
            if (metadata == null) return null;

            var query = new QueryExpression
            {
                ColumnSet = new ColumnSet(),
                EntityName = metadata.ReferencingEntity,
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        new ConditionExpression(metadata.ReferencingAttribute, ConditionOperator.Equal,
                            referencedEntity.Id)
                    }
                }
            };

            if (indicator != null)
            {
                //not tested yet
                var relationShipMetadata = GetRefarencingMetadata(metadata.ReferencedEntity, "ddsm_indicator");
                if (relationShipMetadata == null) return null;

                query.Criteria.Conditions.Add(new ConditionExpression(metadata.ReferencingAttribute,
                    ConditionOperator.Equal,
                    indicator.Id));
            }


            return _dataService.RetrieveMultiple(query);
        }

        public OneToManyRelationshipMetadata GetOneToManyRelationshipMetadata(string referencedEntity,
            string relationshipName)
        {
            var retrieveBankAccountEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Relationships,
                LogicalName = referencedEntity
            };

            var retrieveBankEntityResponse =
                (RetrieveEntityResponse) _orgService.Execute(retrieveBankAccountEntityRequest);

            var oneToManyRelationships = retrieveBankEntityResponse.EntityMetadata.OneToManyRelationships;
            var rs =
                oneToManyRelationships.Where(r => r.SchemaName == relationshipName).ToList();
            if (rs.Count == 0)
                return null;
            return rs[0];
        }
    }
}