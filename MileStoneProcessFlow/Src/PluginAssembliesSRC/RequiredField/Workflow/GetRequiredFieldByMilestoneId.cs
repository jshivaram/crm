﻿using System;
using System.Collections.Generic;
using System.Linq;
using DDSM.CommonProvider.JSON;
using Microsoft.Xrm.Sdk;
using RequiredField;
using RequiredField.Dto;
using RequiredField.Service;

public class GetRequiredFieldByMilestoneId : BaseWorkflow
{
    private const string MilestoneEntityLogicalName = "ddsm_milestone";

    //CurrentMilestoneId,<explorerentityId, List<RequiredFieldDto>>
    private readonly Dictionary<Guid, Dictionary<Guid, RequiredFieldDto>> _requiredFieldsMetadata =
        new Dictionary<Guid, Dictionary<Guid, RequiredFieldDto>>();

    private readonly List<RequiredFieldDto> _requiredFiledList = new List<RequiredFieldDto>();

    private DataService _dataService;
    private RelationshipService _relationship;

    private Guid CurrentMilestoneId { get; set; } = Guid.Empty;


    protected override void Run()
    {
        _relationship = BaseDto.RelationshipService;
        _dataService = BaseDto.DataService;
        var milestoneIds = BaseDto.MilestoneIds;
        foreach (var milestoneId in milestoneIds)
        {
            var milestoneRequirements = GetMilestoneRequirements(milestoneId);
            CurrentMilestoneId = milestoneId;
            CheckRequiredEntities(milestoneRequirements);
        }

        var result = CreateResult();

        Result.Set(BaseDto.ExecutionContext, result);
    }

    /// <summary>
    /// </summary>
    /// <returns></returns>
    private string CreateResult()
    {
        var rs = new List<RequiredFieldDto>();
        foreach (var milestone in _requiredFieldsMetadata)
        foreach (var exploreEntity in milestone.Value)
            rs.Add(exploreEntity.Value);
        return JsonConvert.SerializeObject(rs);
    }

    /// <summary>
    /// </summary>
    /// <param name="milestoneRequirements"></param>
    private void CheckRequiredEntities(EntityCollection milestoneRequirements)
    {
        foreach (var milestoneRequirement in milestoneRequirements.Entities)
            PrepareCheckRequiredFileds(milestoneRequirement);
    }

    private void PrepareCheckRequiredFileds(Entity milestoneRequirement)
    {
        //ddsm_milestone is default entity where need do checks on required fields
        var searchEntity = new EntityReference(MilestoneEntityLogicalName, CurrentMilestoneId);

        //the field that contain link where need do checks on required fields
        //usually it's lookup into the milestone
        //in the milestone "ddsm_sourcefield"
        var searchFiled = GetSearchFiled(milestoneRequirement);

        //if searchFiled is not empty need modifiy searchEntity
        if (!string.IsNullOrEmpty(searchFiled))
        {
            var relatedEntity = _dataService.RetreiveEntity(MilestoneEntityLogicalName, CurrentMilestoneId);
            if (relatedEntity != null)
            {
                object relatedEntityTmp = null;
                if (relatedEntity.Attributes.TryGetValue(searchFiled, out relatedEntityTmp))
                    searchEntity = (EntityReference) relatedEntityTmp;
            }
        }
        //get required entity logical name
        var requiredEntityLogicalName = GetRequiredEntityLogicalName(milestoneRequirement);

        //get relationship metadata between searchEntity and reqEntity
        var parentEntity2RequiredEntity = BaseDto.RelationshipService.GetRefarencingMetadata(searchEntity.LogicalName,
            requiredEntityLogicalName);

        if (parentEntity2RequiredEntity == null) return;

        //get indicators from milestoneRequirement
        var indicator = GetRequiredIndicator(milestoneRequirement);


        //get childs related to search Entity where need to check conditions on required fields
        var explorerInstances = _relationship.GetReferencingRequiredEntities(searchEntity,
            parentEntity2RequiredEntity.ReferencedEntityNavigationPropertyName, indicator);

        //get required filed
        var requiredFiled = GetRequiredFiledLogicalName(milestoneRequirement);
        if (string.IsNullOrEmpty(requiredFiled)) return;

        foreach (var explorerInstance in explorerInstances.Entities)
            CheckRequiredFileds(explorerInstance, requiredFiled);
    }

    private void CheckRequiredFileds(Entity explorerInstance, string requiredFiled, Guid businessRuleId = default(Guid))
    {
        //get all data by explorer instance
        explorerInstance = _dataService.RetreiveEntity(explorerInstance.LogicalName, explorerInstance.Id);

        var br = GetBusinessRule(explorerInstance);
        if (br != null)
        {
            var callDmnDto = new CallDmnDto
            {
                UserId = BaseDto.WorkflowContext.UserId,
                TargetEntityLogicalName = explorerInstance.LogicalName,
                TargetRecordId = explorerInstance.Id
            };
            callDmnDto.BusinessRules.Add(new CallDmnDto.BusinessRule {Id = br.Id});

            // Calling the Action - ddsm_CallDmn
            var req = new OrganizationRequest("ddsm_CallDmn")
            {
                ["Data"] = JsonConvert.SerializeObject(callDmnDto),
                ["Target"] = explorerInstance
            };
            var response = BaseDto.OrgService.Execute(req);

            //to do check if true 
            //if false 
            //UpdateRequiredFiledList(explorerInstance, requiredFiled);
        }

        object reqFieldValueTmp = null;
        if (!explorerInstance.Attributes.TryGetValue(requiredFiled, out reqFieldValueTmp))
            UpdateRequiredFiledList(explorerInstance, requiredFiled);
    }

    private void UpdateRequiredFiledList(Entity explorerInstance, string requiredFiled, string value = null)
    {
        //get required field metadata
        var attMetadata =
            _dataService.GetAttributeMetadata(explorerInstance.LogicalName, requiredFiled).FirstOrDefault();
        var attrType = attMetadata?.AttributeType;

        if (_requiredFieldsMetadata.ContainsKey(CurrentMilestoneId))
        {
            var requiredEntity = _requiredFieldsMetadata[CurrentMilestoneId];
            if (requiredEntity.ContainsKey(explorerInstance.Id))
            {
                var requiredField = requiredEntity[explorerInstance.Id];
                var requiredEntityDto = new RequiredFieldDto.RequiredEntity
                {
                    EntityLogicalName = explorerInstance.LogicalName,
                    RecordId = explorerInstance.Id
                };

                var fieldMetadataDto = new FieldMetadataDto
                {
                    Type = attrType.ToString(),
                    Label = attMetadata.DisplayName.UserLocalizedLabel.Label,
                    Name = requiredFiled,
                    DefaultValue = value
                };
                requiredEntityDto.RequiredFieldMetadata.Add(fieldMetadataDto);
                requiredField.RequiredEntities.Add(requiredEntityDto);
            }
            else
            {
                var requiredEntityDto = new RequiredFieldDto.RequiredEntity
                {
                    EntityLogicalName = explorerInstance.LogicalName,
                    RecordId = explorerInstance.Id
                };

                var fieldMetadataDto = new FieldMetadataDto
                {
                    Type = attrType.ToString(),
                    Label = attMetadata.DisplayName.UserLocalizedLabel.Label,
                    Name = requiredFiled,
                    DefaultValue = value
                };
                requiredEntityDto.RequiredFieldMetadata.Add(fieldMetadataDto);
                var requiredFieldDto = new RequiredFieldDto {MilestoneId = CurrentMilestoneId};
                requiredFieldDto.RequiredEntities.Add(requiredEntityDto);
            }
        }
        else
        {
            CreateNewItemInDictionary(explorerInstance, requiredFiled, value);
        }
    }

    private void CreateNewItemInDictionary(Entity explorerInstance, string requiredFiled, string value = null)
    {
        var attMetadata =
            _dataService.GetAttributeMetadata(explorerInstance.LogicalName, requiredFiled).FirstOrDefault();
        var attrType = attMetadata?.AttributeType;
        var requiredFieldDto = new RequiredFieldDto {MilestoneId = CurrentMilestoneId};

        var requiredEntityDto = new RequiredFieldDto.RequiredEntity
        {
            EntityLogicalName = explorerInstance.LogicalName,
            RecordId = explorerInstance.Id
        };

        var fieldMetadataDto = new FieldMetadataDto
        {
            Type = attrType.ToString(),
            Label = attMetadata.DisplayName.UserLocalizedLabel.Label,
            Name = requiredFiled,
            DefaultValue = value
        };

        requiredEntityDto.RequiredFieldMetadata.Add(fieldMetadataDto);

        requiredFieldDto.RequiredEntities.Add(requiredEntityDto);
        var tmpDic = new Dictionary<Guid, RequiredFieldDto> {{explorerInstance.Id, requiredFieldDto}};
        _requiredFieldsMetadata.Add(CurrentMilestoneId, tmpDic);
    }

    /// <summary>
    ///     the field that contain link where need do checks on required fields
    ///     usually it's lookup into the milestone
    /// </summary>
    /// <param name="milestoneRequirement"></param>
    /// <returns></returns>
    private static string GetSearchFiled(Entity milestoneRequirement)
    {
        var rs = string.Empty;
        object searchFiledTmp = null;
        if (milestoneRequirement.Attributes.TryGetValue("ddsm_sourcefield", out searchFiledTmp))
            rs = searchFiledTmp.ToString();
        return rs;
    }


    /// <summary>
    ///     get required entity logical name
    /// </summary>
    /// <param name="milestoneRequirement"></param>
    /// <returns></returns>
    private static string GetRequiredEntityLogicalName(Entity milestoneRequirement)
    {
        var rs = string.Empty;
        object reqEntityTmp = null;
        if (milestoneRequirement.Attributes.TryGetValue("ddsm_targetentity", out reqEntityTmp))
            rs = reqEntityTmp.ToString();
        return rs;
    }

    /// <summary>
    /// </summary>
    /// <param name="milestoneRequirement"></param>
    /// <returns></returns>
    private static EntityReference GetRequiredIndicator(Entity milestoneRequirement)
    {
        object reqEntityTmp = null;
        if (milestoneRequirement.Attributes.TryGetValue("ddsm_indicator", out reqEntityTmp))
            return (EntityReference) reqEntityTmp;
        return null;
    }

    /// <summary>
    /// </summary>
    /// <param name="milestoneRequirement"></param>
    /// <returns></returns>
    private EntityReference GetBusinessRule(Entity milestoneRequirement)
    {
        var relationShipMetadata = BaseDto.RelationshipService.GetRefarencingMetadata(milestoneRequirement.LogicalName,
            "ddsm_businessrule");
        if (relationShipMetadata == null) return null;
        object brTmp = null;
        if (milestoneRequirement.Attributes.TryGetValue(relationShipMetadata.ReferencingAttribute, out brTmp))
            return (EntityReference) brTmp;
        return null;
    }

    /// <summary>
    /// </summary>
    /// <param name="milestoneRequirement"></param>
    /// <returns></returns>
    private static string GetRequiredFiledLogicalName(Entity milestoneRequirement)
    {
        var rs = string.Empty;
        object reqFieldTmp = null;
        if (milestoneRequirement.Attributes.TryGetValue("ddsm_requiredfield", out reqFieldTmp))
            rs = reqFieldTmp.ToString();
        return rs;
    }

    /// <summary>
    /// </summary>
    private EntityCollection GetMilestoneRequirements(Guid milestoneId)
    {
        return _dataService.GetMilestoneRequirements(milestoneId);
    }
}