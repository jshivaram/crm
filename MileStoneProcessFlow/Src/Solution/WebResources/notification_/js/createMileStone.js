/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 45);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
   value: true
});

exports.default = function (message) {
   console.error('' + _messageTemplateConfig2.default.moduleName + message);
};

var _messageTemplateConfig = __webpack_require__(6);

var _messageTemplateConfig2 = _interopRequireDefault(_messageTemplateConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getHtmlStep = __webpack_require__(34);

var _getHtmlStep2 = _interopRequireDefault(_getHtmlStep);

var _updateBusinessRulesStatuses = __webpack_require__(39);

var _updateBusinessRulesStatuses2 = _interopRequireDefault(_updateBusinessRulesStatuses);

var _cssClassByStepStatus = __webpack_require__(10);

var _cssClassByStepStatus2 = _interopRequireDefault(_cssClassByStepStatus);

var _stepStatus = __webpack_require__(5);

var _stepStatus2 = _interopRequireDefault(_stepStatus);

var _consoleError = __webpack_require__(0);

var _consoleError2 = _interopRequireDefault(_consoleError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var stringOfCssClasses = Object.keys(_cssClassByStepStatus2.default).map(function (key) {
    return _cssClassByStepStatus2.default[key];
}).join(' ');

function updateStepsStatuses(kendoBpfControl, stepsData) {
    window.milestoneStatuses = stepsData;
    stepsData.forEach(function (stepData) {

        var $htmlStep = (0, _getHtmlStep2.default)(stepData.Id, kendoBpfControl);

        if (!$htmlStep.length) {
            (0, _consoleError2.default)('Step with id - ' + stepData.Id + ' is not found in HTML');
            return;
        }

        var cssClass = _cssClassByStepStatus2.default[stepData.Status];
        if (!cssClass) {
            cssClass = _cssClassByStepStatus2.default[_stepStatus2.default.ReadyToCalculate];
        }

        if (stepData.Status === _stepStatus2.default.Active) {
            kendoBpfControl.$bpfControl.data('kendoTabStrip').activateTab($htmlStep);
            GlobalJs.crmContentPanel._Xrm.Page.data.refresh(false);
        }

        $htmlStep.removeClass(stringOfCssClasses);
        $htmlStep.addClass(cssClass);
        (0, _updateBusinessRulesStatuses2.default)(kendoBpfControl, $htmlStep, stepData.BusinessRules);
    });
}

exports.default = updateStepsStatuses;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var businessRuleStatus = {
    None: 0,
    NotStarted: 962080000,
    Active: 962080001,
    Done: 962080002,
    Error: 962080003,
    InProgress: 962080004
};

exports.default = businessRuleStatus;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _cssClassByBusinessRu;

var _businessRuleStatus = __webpack_require__(2);

var _businessRuleStatus2 = _interopRequireDefault(_businessRuleStatus);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var cssClassByBusinessRuleStatus = (_cssClassByBusinessRu = {}, _defineProperty(_cssClassByBusinessRu, _businessRuleStatus2.default.Done, 'bpf-br-status-done'), _defineProperty(_cssClassByBusinessRu, _businessRuleStatus2.default.Error, 'bpf-br-status-error'), _defineProperty(_cssClassByBusinessRu, _businessRuleStatus2.default.InProgress, 'bpf-br-status-inprogress'), _defineProperty(_cssClassByBusinessRu, _businessRuleStatus2.default.None, 'bpf-br-status-none'), _defineProperty(_cssClassByBusinessRu, _businessRuleStatus2.default.Active, 'bpf-br-status-active'), _cssClassByBusinessRu);

exports.default = cssClassByBusinessRuleStatus;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    DDSM_365: '365',
    DDSM_2016: '2016'
};

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var stepStatus = {
    ReadyToCalculate: 0,
    NotStarted: 962080000,
    Active: 962080001,
    Done: 962080002,
    Error: 962080003,
    InProgress: 962080004
};

exports.default = stepStatus;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    moduleName: 'MileStone Process Flow Module: '
};

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function ($bpfContainerData, convertedToHtmlSteps, stepData) {
    function onSelect(e) {

        setCheckedStatus($bpfContainerData, e.item);

        var currentElementClasses = e.item.classList.value;
        var isInclude = currentElementClasses.includes(_cssClassByStepStatus2.default[_stepStatus2.default.Active]) || currentElementClasses.includes(_cssClassByStepStatus2.default[_stepStatus2.default.Done]) || currentElementClasses.includes(_cssClassByStepStatus2.default[_stepStatus2.default.Error]) || currentElementClasses.includes(_cssClassByStepStatus2.default[_stepStatus2.default.InProgress]);

        if (isInclude) {
            return;
        }

        e.preventDefault();
        this.unbind("select");
        this.select(e.item);
        this.bind("select", onSelect);
        $(e.item).removeClass("k-state-active");

        var self = this;
        var selectedStep = SelectStep(e);

        if (!selectedStep) {
            (0, _consoleError2.default)('Unknow step id when click on tab.');
            return;
        } else if (!selectedStep.IsAllowed) {
            GlobalJs.Alert.show("You don't have permission to complete current milestone.", null, [{ label: "OK" }], "WARNING");
            return;
        }

        (0, _DialogMessage2.default)(function () {
            /* Remove ability to multiClick on steps */
            // kendoBpfControl.unbind('select', onSelect);
            /* ///////////////////////////////////// */

            /* Get current step form DOM */
            //let $selectedItem = $(e.item);
            //const id = $(e.contentElement).find(' ul > span').attr('id');
            /* ///////////////////////// */

            // let filteredArray = filterStepsForReset(stepData, id);
            // resetStatuses($bpfContainerData, filteredArray);
            var $selectedItem = $(e.item);
            var selectedStep = SelectStep(e);

            if (!selectedStep) {
                (0, _consoleError2.default)('Unknow step id when click on tab.');
                return;
            }

            // let enableOnSelect = function () {
            //     kendoBpfControl.bind('select', onSelect);
            // };

            (0, _createWindow2.default)($bpfContainerData, selectedStep, $selectedItem, self, onSelect);
        });
    }

    function SelectStep(e) {
        var id = $(e.contentElement).find(' ul > span').attr('id');
        var selectedStep = stepData.find(function (item) {
            return item.Id === id;
        });
        return selectedStep;
    }

    var kendoBpfControl = $bpfContainerData.$bpfControl.kendoTabStrip({
        animation: false,
        dataTextField: 'DisplayName',
        dataContentField: 'DataContent',
        dataSource: convertedToHtmlSteps,
        select: onSelect
    }).data('kendoTabStrip');
    (0, _setStepsWidth2.default)(kendoBpfControl);

    setStepsTitiles(kendoBpfControl);

    return kendoBpfControl;
};

var _consoleError = __webpack_require__(0);

var _consoleError2 = _interopRequireDefault(_consoleError);

var _DialogMessage = __webpack_require__(28);

var _DialogMessage2 = _interopRequireDefault(_DialogMessage);

var _filterStepsFromUnnessStatusReset = __webpack_require__(38);

var _filterStepsFromUnnessStatusReset2 = _interopRequireDefault(_filterStepsFromUnnessStatusReset);

var _createWindow = __webpack_require__(35);

var _createWindow2 = _interopRequireDefault(_createWindow);

var _setStepsWidth = __webpack_require__(37);

var _setStepsWidth2 = _interopRequireDefault(_setStepsWidth);

var _stepStatus = __webpack_require__(5);

var _stepStatus2 = _interopRequireDefault(_stepStatus);

var _cssClassByStepStatus = __webpack_require__(10);

var _cssClassByStepStatus2 = _interopRequireDefault(_cssClassByStepStatus);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function setCheckedStatus($bpfContainerData, selectedItem) {
    $bpfContainerData.$bpfControl.find(".k-tabstrip-items .k-item").removeClass("bpf-step-status-checked");
    $(selectedItem).addClass("bpf-step-status-checked");
}

function setStepsTitiles(kendoBpfControl) {
    var $control = kendoBpfControl.element;
    var $steps = $control.find(".k-tabstrip-items .k-item");
    $steps.each(function (i, step) {
        var $step = $(step);
        var text = $step.text();
        $step.attr("title", text);
    });
}

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    milestoneStatusActionName: 'ddsm_GetMileStoneHistory',
    callDmnEngine: 'ddsm_DDSMCallDmnByMilestone'
};

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    mileStoneWebSocketServerUrlAttributeName: 'milestoneWebsocketUrl'
};

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _cssClassByStepStatus;

var _stepStatus = __webpack_require__(5);

var _stepStatus2 = _interopRequireDefault(_stepStatus);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var cssClassByStepStatus = (_cssClassByStepStatus = {}, _defineProperty(_cssClassByStepStatus, _stepStatus2.default.Done, 'bpf-step-status-done'), _defineProperty(_cssClassByStepStatus, _stepStatus2.default.Error, 'bpf-step-status-error'), _defineProperty(_cssClassByStepStatus, _stepStatus2.default.Active, 'bpf-step-status-active'), _defineProperty(_cssClassByStepStatus, _stepStatus2.default.InProgress, 'bpf-step-status-inprogress'), _defineProperty(_cssClassByStepStatus, _stepStatus2.default.ReadyToCalculate, ''), _defineProperty(_cssClassByStepStatus, _stepStatus2.default.NotStarted, ''), _cssClassByStepStatus);

exports.default = cssClassByStepStatus;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (entityLogicalName, recordId, expectedWebsocketUrl, userId) {
    Process.callAction(actionName, [{
        key: "EntityLogicalName",
        type: Process.Type.String,
        value: entityLogicalName
    }, {
        key: "RecordId",
        type: Process.Type.String,
        value: recordId
    }, {
        key: "UserId",
        type: Process.Type.String,
        value: userId
    }],
    ///response handler transfered to another file
    (0, _actionSuccessHandler2.default)(expectedWebsocketUrl), function (errorResonse) {
        (0, _consoleError2.default)('Failed response to launch action "' + actionName + '"');
    });
};

var _consoleError = __webpack_require__(0);

var _consoleError2 = _interopRequireDefault(_consoleError);

var _actionSuccessHandler = __webpack_require__(18);

var _actionSuccessHandler2 = _interopRequireDefault(_actionSuccessHandler);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var actionName = 'ddsm_GetMileStonesWithBusinessRules';

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _convertBusinessRulesToHtml = __webpack_require__(31);

var _convertBusinessRulesToHtml2 = _interopRequireDefault(_convertBusinessRulesToHtml);

var _convertRequiredFieldsToHtml = __webpack_require__(32);

var _convertRequiredFieldsToHtml2 = _interopRequireDefault(_convertRequiredFieldsToHtml);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var toggleBusinessRules = '<div class="toggleBusinessRules" style="width:220px;"><div class="ms-crm-InlineTabExpander ms-crm-ImageStrip-tab_right" style="display:inline-block; overflow: visible;"><img src="/_imgs/imagestrips/transparent_spacer.gif?ver=1296525430" alt="Collapse this tab" title="Collapse this tab"/></div><div style="display:inline-block; font-size:13px">Business Rules and Required Fields</div></div>';
function getConvertedData(bpfData) {
    var mappedStep = null;
    var arrayOfSteps = bpfData.map(function (step) {
        mappedStep = Object.assign({}, step);
        mappedStep.DataContent = toggleBusinessRules + '<div class="businessRulesWithRequiredFields" style="display:none;">' + (0, _convertBusinessRulesToHtml2.default)(step.Id, step.BusinessRules) + (step.RequiredFields.length != 0 ? '<hr>' : '') + (0, _convertRequiredFieldsToHtml2.default)(step.Id, step.RequiredFields) + '</div>';
        return mappedStep;
    });

    return arrayOfSteps;
}

exports.default = getConvertedData;

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
function updateRequiredFields() {
    var requestentityId = {
        "EntityLogicalName": Xrm.Page.data.entity.getEntityName(),
        "RecordId": Xrm.Page.data.entity.getId(),
        "UserId": Xrm.Page.context.getUserId()
    };
    // var requestentityId = null;
    GlobalJs.WebAPI.ExecuteAction('ddsm_GetMileStonesWithBusinessRules', requestentityId).then(function (response) {
        if (!response || !response.Json || response.Json === "[]") {
            return;
        }
        var responseJson = response.Json;
        var steps = JSON.parse(responseJson);
        var requiredFields = [];
        steps.forEach(function (step) {
            requiredFields = requiredFields.concat(step.RequiredFields);
        });

        var used = {};
        var requiredFieldsWithIndicator = requiredFields.filter(function (field) {
            if (!!field.Indicator && !!field.Indicator.Id) {
                return field.Indicator.Id in used ? 0 : used[field.Indicator.Id] = 1;
            }
        });

        var value;

        requiredFieldsWithIndicator.forEach(function (element) {
            var $fields = $('[indicator="' + element.Indicator.Id + '"]');
            if (!!$fields.toArray().length) {
                $fields.toArray().forEach(function (field) {
                    value = element.Value != null && element.Value != "" ? element.Value : "";
                    $(field).html('<b>' + value + '</b>');
                });
            }
        });

        var currentEntityName = Xrm.Page.data.entity.getEntityName();
        used = {};
        var requiredFieldsWithTargetField = requiredFields.filter(function (field) {
            if (!!field.TargetField && !!field.TargetField.LogicalName && !!field.MilestoneLookup && !!field.MilestoneLookup.TargetEntity && field.MilestoneLookup.TargetEntity.LogicalName === currentEntityName) {
                return field.TargetField.LogicalName in used ? 0 : used[field.TargetField.LogicalName] = 1;
            }
        });

        requiredFieldsWithTargetField.forEach(function (element) {
            var $fields = $('[targetField="' + element.TargetField.LogicalName + '"]' + '[targetEntity="' + element.MilestoneLookup.TargetEntity.LogicalName + '"]');
            if (!!$fields.toArray().length) {
                $fields.toArray().forEach(function (field) {
                    value = element.Value != null && element.Value != "" ? element.Value : "";
                    $(field).html('<b>' + value + '</b>');
                });
            }
        });
    }, function (error) {
        console.error(error);
    });
}

exports.default = updateRequiredFields;

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
function mainConditionCheck(data) {
    var entityId = Xrm.Page.data.entity.getId().replace('{', '').replace('}', '').toLowerCase();
    var entityLogicalName = Xrm.Page.data.entity.getEntityName();

    var iterationResult = null;
    var isContainsEntity = !!data.Entities.find(function (entity) {

        iterationResult = entity.Id.toLowerCase() === entityId && entity.LogicalName === entityLogicalName;

        return iterationResult;
    });

    var result = isContainsEntity;
    return result;
}

exports.default = mainConditionCheck;

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _launchEnum = __webpack_require__(4);

var _launchEnum2 = _interopRequireDefault(_launchEnum);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Choose '2016' or '365' to select handle type
exports.default = _launchEnum2.default.DDSM_365;

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _chooseCreatingType = __webpack_require__(21);

var _chooseCreatingType2 = _interopRequireDefault(_chooseCreatingType);

var _launchEnum = __webpack_require__(4);

var _launchEnum2 = _interopRequireDefault(_launchEnum);

var _launchConfig = __webpack_require__(15);

var _launchConfig2 = _interopRequireDefault(_launchConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

GlobalJs.StartMilestone = function () {

    // debugger;
    if (!GlobalJs.crmContentPanel._isForm) {
        return;
    }

    // hide standart business process flow
    // $("#crmFormHeader").hide();

    var id = Xrm.Page.data.entity.getId();
    if (!id) {
        return;
    }

    switch (_launchConfig2.default) {
        case _launchEnum2.default.DDSM_365:
            (0, _chooseCreatingType2.default)();
            break;
        case _launchEnum2.default.DDSM_2016:
            if (!window.define) {
                consoleError('window.define is undefined');
                break;
            }
            window.define(["jquery", "kendo.all.min"], function ($, kendo) {
                (0, _chooseCreatingType2.default)();
            });
            break;
    }
};

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    businessProcessFlowSelector: '.headerDataContainerTable'
};

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (expectedWebsocketUrl) {
    return function (response) {
        if (!response || !response[0] || !response[0].value || response[0].value === "[]") {
            return;
        }
        var responseJson = response[0].value;
        var steps = JSON.parse(responseJson);
        var stepIdArray = steps.map(function (item) {
            return item.Id;
        });
        var request = (0, _statusRequest2.default)(stepIdArray);

        var bpfContainerData = (0, _bpfHtmlBuilder2.default)();
        var convertedToHtmlSteps = (0, _index2.default)(steps);

        (0, _createTabStrip2.default)(bpfContainerData, convertedToHtmlSteps, steps);

        (0, _resizeWindowHandler2.default)(bpfContainerData, convertedToHtmlSteps, steps);
        request.done(function (response) {
            if (!response || !response.Json || response.Json === "[]") {
                return;
            }
            try {
                var milestoneStatuses = JSON.parse(response.Json);
                (0, _updateStepsStatuses2.default)(bpfContainerData, milestoneStatuses);
                Xrm.Page.data.entity.addOnSave(_updateRequiredFields2.default);
            } catch (error) {
                (0, _consoleError2.default)(error);
            }
        }).fail(function (error) {
            (0, _consoleError2.default)('Failed to get milestone history.');
        }).always(function () {
            (0, _createWebsocket2.default)(bpfContainerData, expectedWebsocketUrl);
        });

        $(document).on('click', '.toggleBusinessRules', function (e) {
            e.stopImmediatePropagation();
            var checkDisplayStatus = $(".businessRulesWithRequiredFields").css("display");
            if (checkDisplayStatus == "none") {
                $('.businessRulesWithRequiredFields').css({ "display": "grid" });
            } else $('.businessRulesWithRequiredFields').css({ "display": "none" });
            $("div.ms-crm-InlineTabExpander").toggleClass('ms-crm-ImageStrip-tab_right').toggleClass("ms-crm-ImageStrip-tab_down").css({ overflow: "visible" });
        });
    };
};

var _index = __webpack_require__(12);

var _index2 = _interopRequireDefault(_index);

var _bpfHtmlBuilder = __webpack_require__(33);

var _bpfHtmlBuilder2 = _interopRequireDefault(_bpfHtmlBuilder);

var _createTabStrip = __webpack_require__(7);

var _createTabStrip2 = _interopRequireDefault(_createTabStrip);

var _createWebsocket = __webpack_require__(44);

var _createWebsocket2 = _interopRequireDefault(_createWebsocket);

var _updateStepsStatuses = __webpack_require__(1);

var _updateStepsStatuses2 = _interopRequireDefault(_updateStepsStatuses);

var _resizeWindowHandler = __webpack_require__(36);

var _resizeWindowHandler2 = _interopRequireDefault(_resizeWindowHandler);

var _updateRequiredFields = __webpack_require__(13);

var _updateRequiredFields2 = _interopRequireDefault(_updateRequiredFields);

var _statusRequest = __webpack_require__(24);

var _statusRequest2 = _interopRequireDefault(_statusRequest);

var _consoleError = __webpack_require__(0);

var _consoleError2 = _interopRequireDefault(_consoleError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (url) {
    return $.ajax({
        type: 'GET',
        url: url,
        headers: {
            'Accept': 'application/json',
            "Content-Type": "application/json; charset=utf-8",
            'OData-MaxVersion': "4.0",
            "OData-Version": "4.0"
        }
    });
};

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (url, data) {
    return $.ajax({
        type: 'POST',
        url: url,
        data: data,
        headers: {
            'Accept': 'application/json',
            "Content-Type": "application/json; charset=utf-8",
            'OData-MaxVersion': "4.0",
            "OData-Version": "4.0"
        }
    });
};

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function () {
    switch (_launchConfig2.default) {
        case _launchEnum2.default.DDSM_2016:
            {
                return (0, _type4.default)();
            }
        case _launchEnum2.default.DDSM_365:
            {
                return (0, _type2.default)();
            }
    }
};

var _launchEnum = __webpack_require__(4);

var _launchEnum2 = _interopRequireDefault(_launchEnum);

var _launchConfig = __webpack_require__(15);

var _launchConfig2 = _interopRequireDefault(_launchConfig);

var _type = __webpack_require__(27);

var _type2 = _interopRequireDefault(_type);

var _type3 = __webpack_require__(26);

var _type4 = _interopRequireDefault(_type3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (callback) {
    if (!GlobalJs || !GlobalJs.DDSM || !GlobalJs.DDSM.Settings || !GlobalJs.DDSM.Settings.GetSettings) {
        // console.error('Cannot get config object.');
        return;
    } else {
        GlobalJs.DDSM.Settings.GetSettings(callback);
    }
};

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
function requiredFieldsRequest(callBack, milestine, $bpfContainerData, userId) {

    var data = {
        MilestoneId: milestine.Id,
        UserId: userId
    };
    // GlobalJs.WebAPI.ExecuteAction('ddsm_DDSMGetRequiredFields', data).then(
    //     function (response) {
    //         var data = {
    //             RequiredFieldsJson: response.Result
    //         };
    //         GlobalJs.WebAPI.ExecuteAction('ddsm_DDSMConvertRequiredFieldsToUIBuilderControls', data).then(
    //             function (response) {
    //                 var controlsConfig = JSON.parse(response.Result);
    //                 callBack(controlsConfig);
    //             },
    //             function (error) {
    //                 kendo.ui.progress($bpfContainerData.$bpfWrapper, false);
    //                 console.error(error);
    //             }
    //         );
    //     },
    //     function (error) {
    //         kendo.ui.progress($bpfContainerData.$bpfWrapper, false);
    //         console.error(error);
    //     }
    // );
    GlobalJs.WebAPI.ExecuteAction('ddsm_DDSMGetRequiredFieldsWithConvertedData', data).then(function (response) {
        var controlsConfig = JSON.parse(response.Result);
        if (controlsConfig && controlsConfig.NotAllowed) {
            kendo.ui.progress($bpfContainerData.$bpfWrapper, false);
            GlobalJs.Alert.show("You don't have permission to complete current milestone.", null, [{ label: "OK" }], "WARNING");
            return;
        }
        callBack(controlsConfig);
    }, function (error) {
        kendo.ui.progress($bpfContainerData.$bpfWrapper, false);
        // console.error(error);
    });
}

exports.default = requiredFieldsRequest;

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (statusIdArray) {
    var clientUrl = Xrm.Page.context.getClientUrl();
    var baseUrl = '/api/data/v8.1/';
    var actionName = _actionNameConsts2.default.milestoneStatusActionName;

    var url = clientUrl + baseUrl + actionName;

    var stringedGuidArray = '';

    statusIdArray.forEach(function (item) {
        stringedGuidArray += item + ',';
    });
    stringedGuidArray = stringedGuidArray.substring(0, stringedGuidArray.length - 1);

    var data = {
        MileStoneIdArray: stringedGuidArray
    };
    var json = JSON.stringify(data);

    return (0, _basePostAjaxObject2.default)(url, json);
};

var _basePostAjaxObject = __webpack_require__(20);

var _basePostAjaxObject2 = _interopRequireDefault(_basePostAjaxObject);

var _actionNameConsts = __webpack_require__(8);

var _actionNameConsts2 = _interopRequireDefault(_actionNameConsts);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _actionNameConsts = __webpack_require__(8);

var _actionNameConsts2 = _interopRequireDefault(_actionNameConsts);

var _consoleError = __webpack_require__(0);

var _consoleError2 = _interopRequireDefault(_consoleError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function stepRequest(milestone) {
    var actionInputs = [{
        key: "Target",
        type: Process.Type.EntityReference,
        value: {
            id: Xrm.Page.data.entity.getId().replace('{', '').replace('}', ''),
            entityType: Xrm.Page.data.entity.getEntityName()
        }
    }, {
        key: "MilestoneId",
        type: Process.Type.String,
        value: milestone.Id
    }];
    try {
        Process.callAction(_actionNameConsts2.default.callDmnEngine, actionInputs, function (successResponse) {}, function (error, trace) {
            console.error(error);
        });
    } catch (error) {
        (0, _consoleError2.default)('Process.callAction is launched with error.');
    }
}

exports.default = stepRequest;

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _consoleError = __webpack_require__(0);

var _consoleError2 = _interopRequireDefault(_consoleError);

var _index = __webpack_require__(11);

var _index2 = _interopRequireDefault(_index);

var _baseGetAjaxObject = __webpack_require__(19);

var _baseGetAjaxObject2 = _interopRequireDefault(_baseGetAjaxObject);

var _adminDataConsts = __webpack_require__(9);

var _adminDataConsts2 = _interopRequireDefault(_adminDataConsts);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = window.$;

var dataRequest = function dataRequest() {
    var entityLogicalName = void 0;
    var url = void 0;
    var recordId = void 0;
    var userId = void 0;
    var processFlowExpectedAttributeValue = void 0;
    var organizationUrl = void 0;
    try {
        recordId = Xrm.Page.data.entity.getId();
        entityLogicalName = Xrm.Page.data.entity.getEntityName();
        userId = Xrm.Page.context.getUserId();
        processFlowExpectedAttributeValue = 'ddsm_' + entityLogicalName + '_processflow';
        organizationUrl = Xrm.Page.context.getClientUrl();
    } catch (error) {
        (0, _consoleError2.default)('Troubles with Xrm.Page.data.entity methods. Loading has been stopped.');
        throw error;
    }
    url = organizationUrl + '/api/data/v8.1/ddsm_admindatas?$select=' + processFlowExpectedAttributeValue + ',' + _adminDataConsts2.default.mileStoneWebSocketServerUrlAttributeName + '&$filter= ddsm_name eq \'Admin Data\'';

    (0, _baseGetAjaxObject2.default)(url).done(function (response) {
        try {
            var adminData = response.value[0];
            var boolResponse = adminData[processFlowExpectedAttributeValue];
            var expectedWebsocketUrl = adminData[_adminDataConsts2.default.mileStoneWebSocketServerUrlAttributeName];

            //Process flow is created only when Admin data has required field, and field value is "true"
            if (boolResponse && expectedWebsocketUrl) {
                (0, _index2.default)(entityLogicalName, recordId, expectedWebsocketUrl, userId);
            }
        } catch (error) {
            (0, _consoleError2.default)('Failed to create milestone process flow.');
        }
    }).fail(function (errorResponse) {
        (0, _consoleError2.default)('Failed to load Admin data "' + processFlowExpectedAttributeValue + '" field before create milestone processflow');
    });
};

exports.default = dataRequest;

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _consoleError = __webpack_require__(0);

var _consoleError2 = _interopRequireDefault(_consoleError);

var _index = __webpack_require__(11);

var _index2 = _interopRequireDefault(_index);

var _configRequest = __webpack_require__(22);

var _configRequest2 = _interopRequireDefault(_configRequest);

var _adminDataConsts = __webpack_require__(9);

var _adminDataConsts2 = _interopRequireDefault(_adminDataConsts);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = window.$;

var dataRequest = function dataRequest() {
    var entityLogicalName = Xrm.Page.data.entity.getEntityName();
    var recordId = Xrm.Page.data.entity.getId();
    var userId = Xrm.Page.context.getUserId();
    var processFlowExpectedAttributeValue = 'ddsm_' + entityLogicalName + '_processflow';

    var formBodyContainer = GlobalJs.crmContentPanel._document.getElementById("formBodyContainer");
    var milestoneIsAlreadyExist = formBodyContainer.getAttribute("milestoneIsAlreadyExist");
    var $milestoneControl = $("#milestone-process-flow");
    if ($milestoneControl.length || milestoneIsAlreadyExist) {
        return;
    }

    formBodyContainer.setAttribute("milestoneIsAlreadyExist", true);

    (0, _configRequest2.default)(function (response) {

        if (!response || !response.ProcessFlowConfig || !response.WSConfig) {
            // console.error("GloblJs.DDSM.Settings is empty or not configured");
            return;
        }
        var boolResponse = false;

        response.ProcessFlowConfig.EntitiesForProcessFlow.forEach(function (element) {

            if (element.LogicalName == entityLogicalName) {
                boolResponse = true;
                return;
            }
        });
        var expectedWebsocketUrl = response.WSConfig[_adminDataConsts2.default.mileStoneWebSocketServerUrlAttributeName];

        if (!boolResponse || !expectedWebsocketUrl) {
            // console.error(processFlowExpectedAttributeValue + " or " + adminDataConsts.mileStoneWebSocketServerUrlAttributeName + " is empty");
            // console.error(processFlowExpectedAttributeValue + " = " + boolResponse);
            // console.error(adminDataConsts.mileStoneWebSocketServerUrlAttributeName + " = " + expectedWebsocketUrl);
            return;
        }

        //Process flow is created only when Admin data has required field, and field value is "true"
        (0, _index2.default)(entityLogicalName, recordId, expectedWebsocketUrl, userId);
    });
};

exports.default = dataRequest;

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (yesCallback) {
    if (Alert && Alert.show) {
        Alert.show("Confirm Action", alertMessage, [{
            label: "Move",
            callback: yesCallback
        }, {
            label: "Cancel"
        }], "QUESTION", 500, 200);
    } else {
        (0, _consoleError2.default)('AlertJs is undefined, therefore used classic browser tools "confirm"');

        var response = confirm(alertMessage);

        if (response) {
            yesCallback();
        }
    }
};

var _consoleError = __webpack_require__(0);

var _consoleError2 = _interopRequireDefault(_consoleError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var alertMessage = "Do you want to move record to this Milestone? You can't undo this action.";

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
   value: true
});

exports.default = function (message) {
   //console.log(`${messageConfig.moduleName}${message}`);
};

var _messageTemplateConfig = __webpack_require__(6);

var _messageTemplateConfig2 = _interopRequireDefault(_messageTemplateConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
   value: true
});

exports.default = function (message) {
   console.warn('' + _messageTemplateConfig2.default.moduleName + message);
};

var _messageTemplateConfig = __webpack_require__(6);

var _messageTemplateConfig2 = _interopRequireDefault(_messageTemplateConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _cssClassByBusinessRuleStatus = __webpack_require__(3);

var _cssClassByBusinessRuleStatus2 = _interopRequireDefault(_cssClassByBusinessRuleStatus);

var _businessRuleStatus = __webpack_require__(2);

var _businessRuleStatus2 = _interopRequireDefault(_businessRuleStatus);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function convertBusinessRulesToHtml(stepId, businessRules) {

    var $businessRulesContainer = $('<ul/>', {
        class: 'bpf-br-container'
    });

    var $step = $('<span/>', {
        id: stepId
    });

    $businessRulesContainer.append($step);

    var $businessRuleElement = null;
    var cssClass = null;

    businessRules.forEach(function (businessRule) {

        cssClass = 'bpf-br-element ' + _cssClassByBusinessRuleStatus2.default[_businessRuleStatus2.default.None];

        $businessRuleElement = $('<li/>', {
            id: businessRule.Id,
            html: businessRule.DisplayName,
            class: cssClass
        });

        $businessRulesContainer.append($businessRuleElement);
    });

    var result = $businessRulesContainer[0].outerHTML;

    return result;
}

exports.default = convertBusinessRulesToHtml;

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _cssClassByBusinessRuleStatus = __webpack_require__(3);

var _cssClassByBusinessRuleStatus2 = _interopRequireDefault(_cssClassByBusinessRuleStatus);

var _businessRuleStatus = __webpack_require__(2);

var _businessRuleStatus2 = _interopRequireDefault(_businessRuleStatus);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function convertRequiredFieldsToHtml(stepId, requiredFields) {
    var $requiredFieldsContainer = $('<div/>', {
        style: "overflow:auto; display:flex"
    });
    var divContainer = $('<div/>', {
        style: "display: inline-table; float:left; width:25%; min-width:350px;"
    });
    var counter = 0;
    requiredFields.forEach(function (requiredField) {
        var indicatorName = '';
        var targetFieldName = '';
        var targetEntityName = '';

        if (!!requiredField.MilestoneLookup && !!requiredField.MilestoneLookup.TargetEntity && !!requiredField.MilestoneLookup.TargetEntity.LogicalName) {
            targetEntityName = requiredField.MilestoneLookup.TargetEntity.LogicalName;
        }
        if (!!requiredField.Indicator && !!requiredField.Indicator.Id) {
            indicatorName = requiredField.Indicator.Id.replace('{', '').replace('}', '');
        } else if (!!requiredField.TargetField && !!requiredField.TargetField.LogicalName) {
            targetFieldName = requiredField.TargetField.LogicalName;
        }

        var $requiredFieldbpf = $('<div/>', {
            style: "margin:5px"
        });
        var $requiredFieldLabel = $('<div/>', {
            style: "display: inline;",
            html: requiredField.Required && (requiredField.Value == null || requiredField.Value == "") ? "<span class='ms-crm-ImageStrip-inlineedit_warning' style= 'padding-left:20px;'>" + requiredField.DisplayName + "</span>" : requiredField.DisplayName
        });

        if (requiredField.Required) {
            var $image = $('<img/>', {
                alt: "Required",
                src: "/_imgs/frm_required.gif?ver=-1664140880",
                class: "ms-crm-ImageStrip-frm_required ms-crm-Inline-RequiredLevel"
            });
            $requiredFieldLabel.append($image);
        }

        var $requiredFieldValue = $('<div/>', {
            style: "display: inline; padding-left: 10px",
            indicator: indicatorName,
            targetField: targetFieldName,
            targetEntity: targetEntityName,
            html: requiredField.Value != null && requiredField.Value != "" ? "<b>" + requiredField.Value + "</b>" : ""
        });

        $requiredFieldbpf.append($requiredFieldLabel);
        $requiredFieldbpf.append($requiredFieldValue);

        divContainer.append($requiredFieldbpf);
        $requiredFieldsContainer.append(divContainer);

        counter++;
        if (counter === 4) {
            counter = 0;
            divContainer = $('<div/>', {
                style: "display: inline-table; float:left; width:25%; min-width:350px;"
            });
        }
    });

    var result = $requiredFieldsContainer[0].outerHTML;

    return result;
}

exports.default = convertRequiredFieldsToHtml;

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _htmlConsts = __webpack_require__(17);

var _htmlConsts2 = _interopRequireDefault(_htmlConsts);

var _consoleError = __webpack_require__(0);

var _consoleError2 = _interopRequireDefault(_consoleError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function bpfHtmlBuilder() {

    var $bpfContainer = $(_htmlConsts2.default.businessProcessFlowSelector);
    if (!$bpfContainer.length) {
        var errorMessage = '********* ERROR ********* BPF container is not found';
        (0, _consoleError2.default)(errorMessage);
        throw errorMessage;
    }

    var $bpfWrapper = $('<div/>', {
        class: 'bpf-wrapper',
        id: 'milestone-process-flow'
    });

    var $bpfControl = $('<div/>', {
        class: 'bpf-control',
        id: 'bpf-control'
    });
    $bpfWrapper.append($bpfControl);
    var $bpfWindow = $('<div/>', {
        class: 'bpf-window',
        id: 'bpf-window'
    });

    $bpfContainer.append($bpfWrapper);

    return {
        $bpfWrapper: $bpfWrapper,
        $bpfControl: $bpfControl,
        $bpfWindow: $bpfWindow
    };
}

exports.default = bpfHtmlBuilder;

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
function getHtmlStep(stepId, kendoBpfControl) {
    var $bpfControl = kendoBpfControl.$bpfWrapper;
    ;
    var $content = $bpfControl.find('#' + stepId).closest('div.k-content');
    var contentId = $content.attr('id');
    var $title = $('li[aria-controls="' + contentId + '"]');

    return $title;
}

exports.default = getHtmlStep;

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _requiredFieldsRequest = __webpack_require__(23);

var _requiredFieldsRequest2 = _interopRequireDefault(_requiredFieldsRequest);

var _stepRequest = __webpack_require__(25);

var _stepRequest2 = _interopRequireDefault(_stepRequest);

var _index = __webpack_require__(12);

var _index2 = _interopRequireDefault(_index);

var _updateStepsStatuses = __webpack_require__(1);

var _updateStepsStatuses2 = _interopRequireDefault(_updateStepsStatuses);

var _createTabStrip = __webpack_require__(7);

var _createTabStrip2 = _interopRequireDefault(_createTabStrip);

var _updateRequiredFields = __webpack_require__(13);

var _updateRequiredFields2 = _interopRequireDefault(_updateRequiredFields);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createWindow($bpfContainerData, milestone, $selectedItem, kendoBpfControl, onSelect) {

    var kWindow = $bpfContainerData.$bpfWindow.data("kendoWindow");
    if (kWindow) {
        kWindow.destroy();
    }
    $bpfContainerData.$bpfWindow.empty();

    kendo.ui.progress($bpfContainerData.$bpfWrapper, true);

    var handler = function handler(response) {

        kendo.ui.progress($bpfContainerData.$bpfWrapper, false);

        if (!response.Metadata.length) {
            (0, _stepRequest2.default)(milestone);
            return;
        }

        var $form = $("<form/>");
        $form.appendTo($bpfContainerData.$bpfWindow);

        uibuilder.buildControls($form, response);

        var kendoWindow = $bpfContainerData.$bpfWindow.kendoWindow({
            width: "800px",
            maxHeight: "600px",
            title: "Required Fields",
            actions: ["Maximize", "Close"],
            modal: true
        }).data("kendoWindow");
        kendoWindow.center().open();

        var validator = $form.kendoValidator({
            rules: {
                //implement your custom date validation
                dateValidation: function dateValidation(e) {
                    var input = e[0];
                    if (input.dataset.role !== "datetimepicker" || !input.value) {
                        return true;
                    }
                    var number = Math.floor(input.value);
                    if (!isNaN(number)) {
                        return false;
                    }
                    var currentDate = Date.parse(input.value);
                    //Check if Date parse is successful
                    if (!currentDate) {
                        return false;
                    }
                    return true;
                }
            },
            messages: {
                //Define your custom validation massages
                dateValidation: "Invalid date"
            }
        }).data("kendoValidator");

        var buttons = createButton($form);
        buttons.$btnSave.click(function () {
            if (!validator.validate()) {
                return;
            }

            var formData = getFormData($bpfContainerData.$bpfWindow, response.Metadata);
            if (!formData.length) {
                kendoWindow.close();
                return;
            }
            var formDataJson = JSON.stringify(formData);

            kendo.ui.progress($bpfContainerData.$bpfWindow, true);

            var requestData = {
                FormDataJson: formDataJson
            };

            GlobalJs.WebAPI.ExecuteAction('ddsm_DDSMSaveFormData', requestData).then(function (response) {
                (0, _updateRequiredFields2.default)();
            }, function (error) {
                // console.error(error);
            }).then(function () {
                kendo.ui.progress($bpfContainerData.$bpfWindow, false);
                kendoWindow.close();
            }, function () {
                kendo.ui.progress($bpfContainerData.$bpfWindow, false);
                kendoWindow.close();
            });
        });
        buttons.$btnCancel.click(function () {
            kendoWindow.close();
        });
    };

    var userId = Xrm.Page.context.getUserId();
    (0, _requiredFieldsRequest2.default)(handler, milestone, $bpfContainerData, userId);
}

function createButton($form) {
    var $table = $("<table/>", {
        width: "100%"
    });
    $table.appendTo($form);

    var $tr = $("<tr/>");
    $tr.appendTo($table);

    var $tdEmpty = $("<td/>", {
        width: "70%"
    });
    $tdEmpty.appendTo($tr);

    var $tdSave = $("<td/>", {
        width: "15%"
    });
    $tdSave.appendTo($tr);

    var $btnSave = $("<input/>", {
        type: "button",
        value: "Save",
        class: "k-button"
    });
    $btnSave.appendTo($tdSave);

    var $tdCancel = $("<td/>", {
        width: "15%"
    });
    $tdCancel.appendTo($tr);

    var $btnCancel = $("<input/>", {
        type: "button",
        value: "Cancel",
        class: "k-button"
    });
    $btnCancel.appendTo($tdCancel);

    var result = {
        $btnSave: $btnSave,
        $btnCancel: $btnCancel
    };

    return result;
}

function getFormData($bpfWindow, metadata) {
    var $form = $bpfWindow.find("form");
    var formData = $form.serializeArray();
    var result = [];

    metadata.forEach(function (item) {
        var $element = $form.find('[name="' + item.Name + '"]');

        var formItem = formData.find(function (formItem) {
            return formItem.name.includes(item.Name);
        });

        if (!formItem.value) {
            return;
        }

        var additionalDataJson = $element.attr("additional-data");
        var additionalData = JSON.parse(additionalDataJson);

        result.push({
            AdditionalData: additionalData,
            Value: formItem.value
        });
    });

    return result;
}

exports.default = createWindow;

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createTabStrip = __webpack_require__(7);

var _createTabStrip2 = _interopRequireDefault(_createTabStrip);

var _updateStepsStatuses = __webpack_require__(1);

var _updateStepsStatuses2 = _interopRequireDefault(_updateStepsStatuses);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function resizeWindowHandler(bpfContainerData, convertedToHtmlSteps, steps) {
    $(window).resize(function (e) {
        e.stopImmediatePropagation();
        var checkDisplayStatus = $(".businessRulesWithRequiredFields").css("display");
        var targetId = $('.k-content.k-state-active').find(".businessRulesWithRequiredFields").find(".bpf-br-container").find("span").attr("id");

        bpfContainerData.$bpfControl.data("kendoTabStrip").destroy();
        bpfContainerData.$bpfControl.empty();

        bpfContainerData.$bpfControl = $("#bpf-control");
        bpfContainerData.$bpfWrapper = $('#milestone-process-flow');
        bpfContainerData.$bpfWindow = $('#bpf-window');

        (0, _createTabStrip2.default)(bpfContainerData, convertedToHtmlSteps, steps);

        if (window.milestoneStatuses) {
            (0, _updateStepsStatuses2.default)(bpfContainerData, window.milestoneStatuses);
        }

        if (targetId != undefined) {
            var childrens = bpfContainerData.$bpfControl.find("div.k-content");
            var content = childrens.toArray().find(function (element) {
                var $BrRf = $(element).find(".businessRulesWithRequiredFields");
                var $span = $BrRf.find("span");
                var id = $span.attr("id");
                return id === targetId;
            });

            $(".k-content.k-state-active").removeClass("k-state-active").css({ "display": "none" });

            if (content.id != undefined) {
                bpfContainerData.$bpfControl.find("ul.k-tabstrip-items").find('li[aria-controls = "' + content.id + '"]').addClass("bpf-step-status-checked").addClass("k-tab-on-top").addClass("k-state-active");
                $("#" + content.id).addClass("k-state-active").css({ "display": "block" });
            } else $(content).addClass("k-state-active").css({ "display": "block" });
        }
        $(".businessRulesWithRequiredFields").css({ "display": checkDisplayStatus });

        if (checkDisplayStatus == "grid") {
            $("div.ms-crm-InlineTabExpander.ms-crm-ImageStrip-tab_right").removeClass('ms-crm-ImageStrip-tab_right').addClass('ms-crm-ImageStrip-tab_down');
        }
    });
}

exports.default = resizeWindowHandler;

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
function setStepsSize(kendoBpfControl) {
    var $wrapper = $(kendoBpfControl.wrapper);
    var $steps = $wrapper.find("li.k-item[role=tab]");

    var containerWidth = $wrapper.find(".k-tabstrip-items").width();
    var stepSize = $steps.first().width();
    var stepsCount = $steps.length;
    var allStepsWidth = stepSize * stepsCount;

    if (allStepsWidth > containerWidth) {
        return;
    }

    var stepMargin = parseInt($steps.css("margin-right")) * 2;
    var allStepsMargin = stepMargin * stepsCount;

    var newStepSize = (containerWidth - allStepsMargin) / stepsCount;
    $steps.width(newStepSize);
}

exports.default = setStepsSize;

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (sourceArray, selectedId) {
    var arrayToResetStatuses = [];
    var isReachedSelectedId = false;

    sourceArray.forEach(function (element) {
        if (isReachedSelectedId) {
            arrayToResetStatuses.push(element);
        } else {
            var isSameId = element.Id === selectedId;
            if (isSameId) {
                isReachedSelectedId = true;
                arrayToResetStatuses.push(element);
            }
        }
    });

    return arrayToResetStatuses;
};

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _cssClassByBusinessRuleStatus = __webpack_require__(3);

var _cssClassByBusinessRuleStatus2 = _interopRequireDefault(_cssClassByBusinessRuleStatus);

var _businessRuleStatus = __webpack_require__(2);

var _businessRuleStatus2 = _interopRequireDefault(_businessRuleStatus);

var _consoleError = __webpack_require__(0);

var _consoleError2 = _interopRequireDefault(_consoleError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var stringOfCssClasses = Object.keys(_cssClassByBusinessRuleStatus2.default).map(function (key) {
    return _cssClassByBusinessRuleStatus2.default[key];
}).join(' ');

function updateBusinessRulesStatuses(kendoBpfControl, $htmlStep, businessRulesData) {

    var $bpfControl = kendoBpfControl.$bpfWrapper;

    var stepContainerId = $htmlStep.attr('aria-controls');
    var $stepContainer = $bpfControl.find('#' + stepContainerId);

    businessRulesData.forEach(function (businessRule) {

        var $businessRule = $stepContainer.find('#' + businessRule.Id);

        if (!$businessRule.length) {
            (0, _consoleError2.default)('Business rule with id - ' + businessRule.Id + ' is not found in HTML (step - ' + $htmlStep.text() + ')');
            return;
        }

        var cssClass = _cssClassByBusinessRuleStatus2.default[businessRule.Status];
        if (!cssClass) {
            cssClass = _cssClassByBusinessRuleStatus2.default[_businessRuleStatus2.default.None];
        }

        $businessRule.removeClass(stringOfCssClasses);
        $businessRule.addClass(cssClass);
    });
}

exports.default = updateBusinessRulesStatuses;

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function () {
    (0, _consoleWarning2.default)('Websocket connection is closed.');
};

var _consoleWarning = __webpack_require__(30);

var _consoleWarning2 = _interopRequireDefault(_consoleWarning);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function () {
    (0, _consoleError2.default)('Websocket connection is not created because of error.');
};

var _consoleError = __webpack_require__(0);

var _consoleError2 = _interopRequireDefault(_consoleError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (kendoElement) {
    return function (response) {
        try {
            if (!response && !response.data) {
                return;
            }

            var data = JSON.parse(response.data);

            var conditionResult = (0, _mainConditionCheck2.default)(data);
            if (!conditionResult) {
                return;
            }

            (0, _updateStepsStatuses2.default)(kendoElement, data.Milestones);
        } catch (error) {
            (0, _consoleError2.default)(error.message);
        }
    };
};

var _updateStepsStatuses = __webpack_require__(1);

var _updateStepsStatuses2 = _interopRequireDefault(_updateStepsStatuses);

var _mainConditionCheck = __webpack_require__(14);

var _mainConditionCheck2 = _interopRequireDefault(_mainConditionCheck);

var _consoleError = __webpack_require__(0);

var _consoleError2 = _interopRequireDefault(_consoleError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function () {
    (0, _consoleMessage2.default)('Websocket connection is created successfully.');
};

var _consoleMessage = __webpack_require__(29);

var _consoleMessage2 = _interopRequireDefault(_consoleMessage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (kendoElement, expectedWebsocketUrl) {
    var ws = new WebSocket(expectedWebsocketUrl);

    ws.onmessage = (0, _onMessage2.default)(kendoElement);
    ws.onclose = _onClose2.default;
    ws.onopen = _onOpen2.default;
    ws.onerror = _onError2.default;
};

var _mainConditionCheck = __webpack_require__(14);

var _mainConditionCheck2 = _interopRequireDefault(_mainConditionCheck);

var _updateStepsStatuses = __webpack_require__(1);

var _updateStepsStatuses2 = _interopRequireDefault(_updateStepsStatuses);

var _onMessage = __webpack_require__(42);

var _onMessage2 = _interopRequireDefault(_onMessage);

var _onClose = __webpack_require__(40);

var _onClose2 = _interopRequireDefault(_onClose);

var _onError = __webpack_require__(41);

var _onError2 = _interopRequireDefault(_onError);

var _onOpen = __webpack_require__(43);

var _onOpen2 = _interopRequireDefault(_onOpen);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(16);


/***/ })
/******/ ]);
//# sourceMappingURL=createMileStone.js.map